package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR633
 * @version 1.0 generated on 30/08/09 07:22
 * @author Quipoz
 */
public class Sr633ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(661);
	public FixedLengthStringData dataFields = new FixedLengthStringData(309).isAPartOf(dataArea, 0);
	public FixedLengthStringData optdscs = new FixedLengthStringData(45).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] optdsc = FLSArrayPartOfStructure(3, 15, optdscs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(45).isAPartOf(optdscs, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01 = DD.optdsc.copy().isAPartOf(filler,0);
	public FixedLengthStringData optdsc02 = DD.optdsc.copy().isAPartOf(filler,15);
	public FixedLengthStringData optdsc03 = DD.optdsc.copy().isAPartOf(filler,30);
	public FixedLengthStringData optinds = new FixedLengthStringData(3).isAPartOf(dataFields, 45);
	public FixedLengthStringData[] optind = FLSArrayPartOfStructure(3, 1, optinds, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(3).isAPartOf(optinds, 0, FILLER_REDEFINE);
	public FixedLengthStringData optind01 = DD.optind.copy().isAPartOf(filler1,0);
	public FixedLengthStringData optind02 = DD.optind.copy().isAPartOf(filler1,1);
	public FixedLengthStringData optind03 = DD.optind.copy().isAPartOf(filler1,2);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(dataFields,48);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(dataFields,68);
	public FixedLengthStringData coverDesc = DD.cvdes.copy().isAPartOf(dataFields,78);
	public ZonedDecimalData dteapp = DD.dteapp.copyToZonedDecimal().isAPartOf(dataFields,108);
	public ZonedDecimalData dtetrm = DD.dtetrm.copyToZonedDecimal().isAPartOf(dataFields,116);
	public FixedLengthStringData optdscx = DD.optdscx.copy().isAPartOf(dataFields,124);
	public FixedLengthStringData paymth = DD.paymth.copy().isAPartOf(dataFields,139);
	public FixedLengthStringData pymdesc = DD.pymdesc.copy().isAPartOf(dataFields,141);
	public FixedLengthStringData statdets = DD.statdets.copy().isAPartOf(dataFields,171);
	public FixedLengthStringData xoptind = DD.xoptind.copy().isAPartOf(dataFields,201);
	public FixedLengthStringData zaracde = DD.zaracde.copy().isAPartOf(dataFields,202);
	public FixedLengthStringData zdesc = DD.zdesc.copy().isAPartOf(dataFields,210);
	public FixedLengthStringData zmednam = DD.zmednam.copy().isAPartOf(dataFields,230);
	public FixedLengthStringData zmednum = DD.zmednum.copy().isAPartOf(dataFields,290);
	public FixedLengthStringData zmedprv = DD.zmedprv.copy().isAPartOf(dataFields,298);
	public FixedLengthStringData zmedsts = DD.zmedsts.copy().isAPartOf(dataFields,308);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(88).isAPartOf(dataArea, 309);
	public FixedLengthStringData optdscsErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] optdscErr = FLSArrayPartOfStructure(3, 4, optdscsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(12).isAPartOf(optdscsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData optdsc02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData optdsc03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData optindsErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] optindErr = FLSArrayPartOfStructure(3, 4, optindsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(12).isAPartOf(optindsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optind01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData optind02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData optind03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData bankacckeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData bankkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cvdesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData dteappErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData dtetrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData optdscxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData paymthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData pymdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData statdetsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData xoptindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData zaracdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData zdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData zmednamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData zmednumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData zmedprvErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData zmedstsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(264).isAPartOf(dataArea, 397);
	public FixedLengthStringData optdscsOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] optdscOut = FLSArrayPartOfStructure(3, 12, optdscsOut, 0);
	public FixedLengthStringData[][] optdscO = FLSDArrayPartOfArrayStructure(12, 1, optdscOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(36).isAPartOf(optdscsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optdsc01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] optdsc02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] optdsc03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData optindsOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] optindOut = FLSArrayPartOfStructure(3, 12, optindsOut, 0);
	public FixedLengthStringData[][] optindO = FLSDArrayPartOfArrayStructure(12, 1, optindOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(36).isAPartOf(optindsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optind01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] optind02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] optind03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] bankacckeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] bankkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cvdesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] dteappOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] dtetrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] optdscxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] paymthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] pymdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] statdetsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] xoptindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] zaracdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] zdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] zmednamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] zmednumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] zmedprvOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] zmedstsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(146);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(64).isAPartOf(subfileArea, 0);
	public FixedLengthStringData hflag = DD.hflag.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData value = DD.value.copy().isAPartOf(subfileFields,31);
	public ZonedDecimalData zmedfee = DD.zmedfee.copyToZonedDecimal().isAPartOf(subfileFields,39);
	public FixedLengthStringData zmedtyp = DD.zmedtyp.copy().isAPartOf(subfileFields,56);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(20).isAPartOf(subfileArea, 64);
	public FixedLengthStringData hflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData valueErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData zmedfeeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData zmedtypErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(60).isAPartOf(subfileArea, 84);
	public FixedLengthStringData[] hflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] valueOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] zmedfeeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] zmedtypOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 144);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData dteappDisp = new FixedLengthStringData(10);
	public FixedLengthStringData dtetrmDisp = new FixedLengthStringData(10);

	public LongData Sr633screensflWritten = new LongData(0);
	public LongData Sr633screenctlWritten = new LongData(0);
	public LongData Sr633screenWritten = new LongData(0);
	public LongData Sr633protectWritten = new LongData(0);
	public GeneralTable sr633screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr633screensfl;
	}

	public Sr633ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zmedfeeOut,new String[] {"31","41","-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zmedtypOut,new String[] {"18","02","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zmednumOut,new String[] {"11","04","-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zmedstsOut,new String[] {"13","01","-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dteappOut,new String[] {"14","01","-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dtetrmOut,new String[] {"15","03","-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdescOut,new String[] {"17","01","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(xoptindOut,new String[] {null, "70",null, "50",null, null, null, null, null, null, null, null});
		fieldIndMap.put(paymthOut,new String[] {"21","01","-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zaracdeOut,new String[] {"41","05","-41",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind01Out,new String[] {"18","71","-18","51",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind02Out,new String[] {"19","72","-19","52",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind03Out,new String[] {"20","73","-20","53",null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {hflag, value, longdesc, zmedfee, zmedtyp};
		screenSflOutFields = new BaseData[][] {hflagOut, valueOut, longdescOut, zmedfeeOut, zmedtypOut};
		screenSflErrFields = new BaseData[] {hflagErr, valueErr, longdescErr, zmedfeeErr, zmedtypErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {zmednum, zmedsts, dteapp, dtetrm, zdesc, statdets, xoptind, zmedprv, optdscx, zmednam, paymth, pymdesc, bankkey, bankacckey, zaracde, coverDesc, optind01, optind02, optdsc01, optdsc02, optdsc03, optind03};
		screenOutFields = new BaseData[][] {zmednumOut, zmedstsOut, dteappOut, dtetrmOut, zdescOut, statdetsOut, xoptindOut, zmedprvOut, optdscxOut, zmednamOut, paymthOut, pymdescOut, bankkeyOut, bankacckeyOut, zaracdeOut, cvdesOut, optind01Out, optind02Out, optdsc01Out, optdsc02Out, optdsc03Out, optind03Out};
		screenErrFields = new BaseData[] {zmednumErr, zmedstsErr, dteappErr, dtetrmErr, zdescErr, statdetsErr, xoptindErr, zmedprvErr, optdscxErr, zmednamErr, paymthErr, pymdescErr, bankkeyErr, bankacckeyErr, zaracdeErr, cvdesErr, optind01Err, optind02Err, optdsc01Err, optdsc02Err, optdsc03Err, optind03Err};
		screenDateFields = new BaseData[] {dteapp, dtetrm};
		screenDateErrFields = new BaseData[] {dteappErr, dtetrmErr};
		screenDateDispFields = new BaseData[] {dteappDisp, dtetrmDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr633screen.class;
		screenSflRecord = Sr633screensfl.class;
		screenCtlRecord = Sr633screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr633protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr633screenctl.lrec.pageSubfile);
	}
}
