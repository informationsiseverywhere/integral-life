package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.MerxTemppfDAO;
import com.csc.life.productdefinition.dataaccess.model.MerxTemppf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class MerxTemppfDAOImpl extends BaseDAOImpl<MerxTemppf> implements MerxTemppfDAO{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MerxTemppfDAOImpl.class);
	
	public boolean insertMerxTempRecords(List<MerxTemppf> merxList, String tempTableName){
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO " );
		sb.append(tempTableName);
		sb.append("(CHDRCOY, CHDRNUM, SEQNO, PAIDBY, EXMCODE, CLNTNUM, NAME, ZMEDTYP, EFFDATE, INVREF, ZMEDFEE, DESC_T, MEMBER_NAME)  ");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;	
		boolean isUpdated = false;
		try{
			ps = getPrepareStatement(sb.toString());	
			for (MerxTemppf merx : merxList) {	
				ps.setString(1, merx.getChdrcoy());
				ps.setString(2,merx.getChdrnum());
				ps.setInt(3, merx.getSeqno());
				ps.setString(4, merx.getPaidby());
				ps.setString(5, merx.getExmcode());
				ps.setString(6, merx.getClntnum());
				ps.setString(7, merx.getName());
				ps.setString(8, merx.getZmedtyp());
				ps.setInt(9, merx.getEffdate());
				ps.setString(10, merx.getInvref());
				ps.setBigDecimal(11, merx.getZmedfee());
				ps.setString(12, merx.getDescT());
				ps.setString(13, merx.getMemberName());

			    ps.addBatch();				
			}		
			ps.executeBatch();
			isUpdated = true;
		}catch (SQLException e) {
			LOGGER.error("insertMerxTempRecords()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);			
		}	
		return isUpdated;
	}		

}

