package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:44
 * Description:
 * Copybook name: MEDISEQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mediseqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mediseqFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData mediseqKey = new FixedLengthStringData(256).isAPartOf(mediseqFileKey, 0, REDEFINE);
  	public FixedLengthStringData mediseqChdrcoy = new FixedLengthStringData(1).isAPartOf(mediseqKey, 0);
  	public FixedLengthStringData mediseqChdrnum = new FixedLengthStringData(8).isAPartOf(mediseqKey, 1);
  	public PackedDecimalData mediseqSeqno = new PackedDecimalData(2, 0).isAPartOf(mediseqKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(245).isAPartOf(mediseqKey, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mediseqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mediseqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}