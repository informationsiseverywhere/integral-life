package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sj699screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sj699ScreenVars sv = (Sj699ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sj699screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sj699ScreenVars screenVars = (Sj699ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.freqcy01.setClassString("");
		screenVars.freqcy02.setClassString("");
		screenVars.freqcy03.setClassString("");
		screenVars.freqcy05.setClassString("");
		screenVars.zlfact02.setClassString("");
		screenVars.zlfact01.setClassString("");
		screenVars.freqcy04.setClassString("");
		screenVars.freqcy07.setClassString("");
		screenVars.zlfact03.setClassString("");
		screenVars.zlfact04.setClassString("");
		screenVars.zlfact05.setClassString("");
		screenVars.zlfact06.setClassString("");
		screenVars.freqcy06.setClassString("");
		screenVars.freqcy08.setClassString("");
		screenVars.freqcy09.setClassString("");
		screenVars.zlfact07.setClassString("");
		screenVars.zlfact08.setClassString("");
		screenVars.zlfact09.setClassString("");
		screenVars.freqcy10.setClassString("");
		screenVars.freqcy11.setClassString("");
		screenVars.freqcy12.setClassString("");
		screenVars.zlfact10.setClassString("");
		screenVars.zlfact11.setClassString("");
		screenVars.zlfact12.setClassString("");
	}

/**
 * Clear all the variables in Sj699screen
 */
	public static void clear(VarModel pv) {
		Sj699ScreenVars screenVars = (Sj699ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.freqcy01.clear();
		screenVars.freqcy02.clear();
		screenVars.freqcy03.clear();
		screenVars.freqcy05.clear();
		screenVars.zlfact02.clear();
		screenVars.zlfact01.clear();
		screenVars.freqcy04.clear();
		screenVars.freqcy07.clear();
		screenVars.zlfact03.clear();
		screenVars.zlfact04.clear();
		screenVars.zlfact05.clear();
		screenVars.zlfact06.clear();
		screenVars.freqcy06.clear();
		screenVars.freqcy08.clear();
		screenVars.freqcy09.clear();
		screenVars.zlfact07.clear();
		screenVars.zlfact08.clear();
		screenVars.zlfact09.clear();
		screenVars.freqcy10.clear();
		screenVars.freqcy11.clear();
		screenVars.freqcy12.clear();
		screenVars.zlfact10.clear();
		screenVars.zlfact11.clear();
		screenVars.zlfact12.clear();
	}
}
