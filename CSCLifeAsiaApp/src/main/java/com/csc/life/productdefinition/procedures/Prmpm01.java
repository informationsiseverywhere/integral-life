/*
 * File: Prmpm01.java
 * Date: 30 August 2009 1:58:03
 * Author: Quipoz Limited
 * 
 * Class transformed from PRMPM01.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import com.csc.fsu.clients.dataaccess.ClexTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.tablestructures.T5658rec;
import com.csc.life.newbusiness.tablestructures.Th606rec;
import com.csc.life.newbusiness.tablestructures.Th609rec;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.recordstructures.Extprmrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.tablestructures.T5659rec;
import com.csc.life.productdefinition.tablestructures.Th549rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  PREMIUM CALCULATION METHOD 01 - SINGLE LIFE, TABLE LOOK-UP
*
*
*
* PROCESSING.
* ----------
*
* Initialise all working storage fields and set keys to read
* tables. Include a table (occurs 8) to hold the Options/Extras
* (LEXT) record details.
*
* Build a key, dependent on term (calculate TERM from effective
* date and premium Termination date, the difference, in years,
* rounded up to the nearest whole year). This key (see below)
* will read table T5658. This table contains the parameters to be
* used in the calculation of the
* Coverage/Rider components.
*
* The key is a concatenation of the following fields:-
*
* Coverage/Rider table code
* Term                     (calculated)
* Mortality Class
* Sex
*
* Access the required table by reading the table directly (ITDM).
* The contents of the table are then stored. This table is dated
* use:
*
*  1) Rating Date
*
* CALCULATE-BASIC-ANNNUAL-PREMIUM (and apply age rates)
* (Age, Sex & Duration taken from linkage)
*
* Obtain the age rates from the (LEXT) record.
*
*  - read all the LEXT records for this contract, life and
*  coverage into the working-storage table. Compute the
*  adjusted age as being the summation of the LEXT age
*  rates plus the ANB @ RCD.
*
* Use the age calculated above to access the table T5658 and
* check the following:
*
*
*  - that the basic annual premium (indexed by year) from
*  the T5658 table is not zero. If it is zero, then display
*  an error message and skip the additional procedures.
*  Otherwise store the premium as the (BAP).
*
* - we should now have an age rated BAP.
*
* APPLY STAFF-DISCOUNT
*
* - Use the contract owner or join owner to read client extra
*   details.
* - if staff flag is 'Y' then read TH609 with contract type
*   to get discount percentage.
* - discount percentage is classified into base premium rate or
*   instalment premium.
* - apply staff discount to BAP * (100 - staff discount) / 100
*
* APPLY-RATE-PER-MILLE-LOADINGS
*
* - sum the rates per mille from the LEXT W/S table.
*
*  - add rates per mille to the BAP
*
* - we should now have a BAP with rates / mille applied.
*
* APPLY-DISCOUNT.
*
* Access the discount table T5659 using the key:-
*
* - Discount method from T5658 concatenated with currency.
*
*  - check the sum insured against the volume band ranges
*  and when within a range store the discount amount.
*
*  - compute the BAP as the BAP - discount
*
* - we should now have a BAP with discount applied.
*
* APPLY-PREMIUM-UNIT
*
* - Obtain the risk-unit from T5658
*
*  - multiply BAP by the sum-insured and divide
*    by the risk-unit
*
* - we should now have a BAP with premium applied.
* APPLY-MORTALITY-LOADINGS
* - Get the mortality loading from LEXT
* - Get the reason code & subroutine, smoker/non-smoker code
*   by reading TH549.
*
* - Read TH606 using concatenated key Coverage + TH549-indic      <V42010
*
* - Compute WSAA-BAP = WSAA-BAP + EXTP-LOADING
*
* APPLY-PERCENTAGE-LOADINGS
*
* - from the LEXT working-storage (W/S) table apply the
* percentage loadings. For each loading entry on the table
* compute the BAP as follows:
*
*  BAP = BAP * loading percentage / 100.
*
* - we should now have a loaded BAP.
*
* CALCULATE-INSTALMENT-PREMIUM.
*
* Determine which billing frequency to use.
*
* compute the basic-instalment-premium (BIP) as:-
*
* basic-premium * factor (FACTOR is from T5658).
*
* CALCULATE-ROUNDING.
*
* - round up depending on the rounding factor (obtained from
* the T5659 table).
*
* - if the prem-unit from T5658 is greater than zero, then
* compute the BIP as the rounded number / the premium-unit
* (from T5658). The premium unit is the quantity in which the
* currency is denominated.
*
* CALCULATE-THE-ANNUAL-PREMIUM.
*
* There is no need to calculate the annual premium, because
* at issue time, when the COVR records are being created from
*the COVT records, the annual premium will then be calculated.
*
*****************************************************************
* </pre>
*/
public class Prmpm01 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "PRMPM01";
		/* ERRORS */
	private static final String e107 = "E107";
	private static final String f264 = "F264";
	private static final String f265 = "F265";
	private static final String f272 = "F272";
	private static final String f110 = "F110";
	private static final String hl26 = "HL26";
	private static final String hl27 = "HL27";

		/* FORMATS */
	private static final String clexrec = "CLEXREC";
	private static final String chdrlnbrec = "CHDRLNBREC";

		/* WSAA-LEXT-OPPC-RECS */
	private FixedLengthStringData[] wsaaLextOppcs = FLSInittedArray (8, 3);
	private PackedDecimalData[] wsaaLextOppc = PDArrayPartOfArrayStructure(5, 2, wsaaLextOppcs, 0);

		/* WSAA-LEXT-ZMORTPCT-RECS */
	private FixedLengthStringData[] wsaaLextZmortpcts = FLSInittedArray (8, 2);
	private PackedDecimalData[] wsaaLextZmortpct = PDArrayPartOfArrayStructure(3, 0, wsaaLextZmortpcts, 0, UNSIGNED_TRUE);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaAgerateTot = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaRatesPerMillieTot = new PackedDecimalData(7, 0).init(0);
	private PackedDecimalData wsaaAdjustedAge = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaDiscountAmt = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaBap = new PackedDecimalData(19, 2);
	private PackedDecimalData wsaaBapBuff = new PackedDecimalData(19, 2);
	private PackedDecimalData wsaaBip = new PackedDecimalData(19, 2);
	private PackedDecimalData wsaaModalFactor = new PackedDecimalData(5, 4);
	private String wsaaBasicPremium = "";
	private String wsaaMortalityLoad = "";
	private ZonedDecimalData wsaaStaffDiscount = new ZonedDecimalData(5, 2).init(0);
	private FixedLengthStringData wsaaStaffFlag = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaRoundNum = new ZonedDecimalData(19, 2).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(19).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound10000 = new ZonedDecimalData(7, 2).isAPartOf(filler2, 12).setUnsigned();

	private FixedLengthStringData filler4 = new FixedLengthStringData(19).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound1000 = new ZonedDecimalData(6, 2).isAPartOf(filler4, 13).setUnsigned();

	private FixedLengthStringData filler6 = new FixedLengthStringData(19).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound100 = new ZonedDecimalData(5, 2).isAPartOf(filler6, 14).setUnsigned();

	private FixedLengthStringData filler8 = new FixedLengthStringData(19).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound10 = new ZonedDecimalData(4, 2).isAPartOf(filler8, 15).setUnsigned();

	private FixedLengthStringData filler10 = new FixedLengthStringData(19).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound1 = new ZonedDecimalData(3, 2).isAPartOf(filler10, 16).setUnsigned();

	private FixedLengthStringData filler12 = new FixedLengthStringData(19).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRoundDec = new ZonedDecimalData(2, 2).isAPartOf(filler12, 17);

	private FixedLengthStringData wsaaT5658Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5658Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5658Key, 0);
	private FixedLengthStringData wsaaT5658Ageterm = new FixedLengthStringData(2).isAPartOf(wsaaT5658Key, 4);
	private FixedLengthStringData wsaaT5658Mortcls = new FixedLengthStringData(1).isAPartOf(wsaaT5658Key, 6);
	private FixedLengthStringData wsaaT5658Sex = new FixedLengthStringData(1).isAPartOf(wsaaT5658Key, 7);

	private FixedLengthStringData wsaaT5659Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaDisccntmeth = new FixedLengthStringData(4).isAPartOf(wsaaT5659Key, 0);
	private FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(3).isAPartOf(wsaaT5659Key, 4);

	private FixedLengthStringData wsaaTh606Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTh606Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTh606Key, 0);
	private FixedLengthStringData wsaaTh606Ageterm = new FixedLengthStringData(2).isAPartOf(wsaaTh606Key, 4);
	private FixedLengthStringData wsaaTh606Indic = new FixedLengthStringData(2).isAPartOf(wsaaTh606Key, 6);

	private FixedLengthStringData wsaaTh549Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTh549Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTh549Key, 0);
	private ZonedDecimalData wsaaTh549Zmortpct = new ZonedDecimalData(3, 0).isAPartOf(wsaaTh549Key, 4).setUnsigned();
	private FixedLengthStringData wsaaTh549Zsexmort = new FixedLengthStringData(1).isAPartOf(wsaaTh549Key, 7);
	private PackedDecimalData wsaaMortRate = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaMortFactor = new PackedDecimalData(5, 4);
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(1, 0).setUnsigned();

		/* WSAA-LEXT-OPCDA-RECS */
	private FixedLengthStringData[] wsaaLextOpcdas = FLSInittedArray (8, 2);
	private FixedLengthStringData[] wsaaLextOpcda = FLSDArrayPartOfArrayStructure(2, wsaaLextOpcdas, 0);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ClexTableDAM clexIO = new ClexTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private T5658rec t5658rec = new T5658rec();
	private T5659rec t5659rec = new T5659rec();
	private Th609rec th609rec = new Th609rec();
	private Extprmrec extprmrec = new Extprmrec();
	private Th549rec th549rec = new Th549rec();
	private Th606rec th606rec = new Th606rec();
	private Premiumrec premiumrec = new Premiumrec();	
	/*BRD-306 START */
	private BigDecimal wsaaPremiumAdjustTot;
	boolean isLoadingAva = false;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private LextpfDAO lextpfDAO = getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private int itmfrm;
	private Lextpf lextpf;
	private Iterator<Lextpf> lextItr;
	/*BRD-306 END */
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		t5658120, 
		loopForAdjustedAge220, 
		checkT5658Insprm230, 
		exit490, 
		calcMortLoadings950, 
		callSubroutine980, 
		exit950, 
		a150GetModalFactor, 
		a150Exit
	}

	public Prmpm01() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		premiumrec.premiumRec = convertAndSetParam(premiumrec.premiumRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		premiumrec.statuz.set("****");
		syserrrec.subrname.set(wsaaSubr);
		wsaaBapBuff.set(0);		
		wsaaBasicPremium = "N";
		
		if (isEQ(premiumrec.ratingdate, ZERO)) {
			itmfrm=99999999;
		}
		else {
			itmfrm=premiumrec.ratingdate.toInt();
		}
		
		initialize100();
		if (isEQ(premiumrec.statuz, "****")) {
			r100StaffDiscount();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			basicAnnualPremium200();
			/*BRD-306 START */
			premiumrec.adjustageamt.set(wsaaBap);
			/*BRD-306 END */
		}
		if (isEQ(premiumrec.statuz, "****")
		&& isEQ(wsaaStaffFlag, "Y")
		&& isNE(wsaaStaffDiscount, ZERO)
		&& isEQ(th609rec.indic, "B")) {
			r300BaseRateStaffDisc();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			ratesPerMillieLoadings300();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			volumeDiscountBap1400();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			premiumUnit500();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			/*     PERFORM 900-MORTALITY-LOADINGS.                   <V42010>*/
			/*       PERFORM 950-MORTALITY-LOADINGS.                   <LA4467>*/
			mortalityLoadings950();
			wsaaBapBuff.set(wsaaBap);
		}
		if (isEQ(premiumrec.statuz, "****")) {
			percentageLoadings600();			
		}		
		if (isEQ(premiumrec.statuz, "****")) {
			instalmentPremium700();
		}
		if (isEQ(premiumrec.statuz, "****")
		&& isEQ(wsaaStaffFlag, "Y")
		&& isNE(wsaaStaffDiscount, ZERO)
		&& isEQ(th609rec.indic, "I")) {
			r400InstalmentStaffDisc();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			rounding800();
		}
		/*BRD-306 START */
		if (isEQ(premiumrec.statuz, "****")) {
			premiumAdjustedLoadings900();
		}
		/*BRD-306 END */
		wsaaBasicPremium = "Y";
		if (isEQ(premiumrec.statuz, "****")) {
			basicAnnualPremium200();
			/*BRD-306 START */
			//Calculate adjusted age amount
			compute(premiumrec.adjustageamt, 2).set(sub(premiumrec.adjustageamt, wsaaBap));
			/*BRD-306 END */
		}
		if (isEQ(premiumrec.statuz, "****")
		&& isEQ(wsaaStaffFlag, "Y")
		&& isNE(wsaaStaffDiscount, ZERO)
		&& isEQ(th609rec.indic, "B")) {
			r300BaseRateStaffDisc();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			volumeDiscountBap1400();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			premiumUnit500();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			instalmentPremium700();
		}
		if (isEQ(premiumrec.statuz, "****")
		&& isEQ(wsaaStaffFlag, "Y")
		&& isNE(wsaaStaffDiscount, ZERO)
		&& isEQ(th609rec.indic, "I")) {
			r400InstalmentStaffDisc();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			rounding800();
		}
		/* Calculate the loaded premium as the gross premium minus the     */
		/* basic premium.                                                  */
		compute(premiumrec.calcLoaPrem, 2).set(sub(premiumrec.calcPrem, premiumrec.calcBasPrem));
		
		/*BRD-306 START */
		if(isLoadingAva == true)
			compute(premiumrec.loadper, 2).set(sub(premiumrec.calcLoaPrem,(add(wsaaRatesPerMillieTot,wsaaPremiumAdjustTot,premiumrec.adjustageamt))));
		/*BRD-306 END */
		
		if (isNE(wsaaBapBuff, 0)
		&& isEQ(wsaaAdjustedAge, 0)) {
			if (isLT(premiumrec.calcPrem, premiumrec.calcBasPrem)) {
				premiumrec.calcPrem.set(premiumrec.calcBasPrem);
				compute(premiumrec.calcLoaPrem, 2).set(sub(add(premiumrec.calcPrem, wsaaBapBuff), premiumrec.calcBasPrem));
				if (isGT(premiumrec.calcLoaPrem, premiumrec.calcPrem)) {
					premiumrec.calcLoaPrem.set(wsaaBapBuff);
				}
			}
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialize100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para100();
				case t5658120: 
					t5658Again125();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para100()
	{
		/* Initialise all working storage fields and set keys to read*/
		/* tables. Include a table (occurs 8) to hold the Options/Extras*/
		/* (LEXT) record details.*/
		wsaaRatesPerMillieTot.set(ZERO);
		/*BRD-306 START */
		wsaaPremiumAdjustTot = BigDecimal.ZERO;
		/*BRD-306 END */
		wsaaBap.set(ZERO);
		wsaaBip.set(ZERO);
		wsaaAdjustedAge.set(ZERO);
		wsaaDiscountAmt.set(ZERO);
		wsaaAgerateTot.set(ZERO);
		wsaaSub.set(ZERO);
		wsaaT5659Key.set(SPACES);
		/*BRD-306 START */
		premiumrec.fltmort.set(ZERO);
		premiumrec.loadper.set(ZERO);
		/*BRD-306 END */
		for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
			clearLextRecs110();
		}
		goTo(GotoLabel.t5658120);
	}

protected void clearLextRecs110()
	{
		wsaaSub.add(1);
		wsaaLextOppc[wsaaSub.toInt()].set(ZERO);
		wsaaLextZmortpct[wsaaSub.toInt()].set(ZERO);
		wsaaLextOpcda[wsaaSub.toInt()].set(SPACES);
	}

protected void t5658Again125()
	{
	
	wsaaT5658Crtable.set(premiumrec.crtable);
	wsaaT5658Mortcls.set(premiumrec.mortcls);
	wsaaT5658Sex.set(premiumrec.lsex);
	/*MOVE CPRM-DURATION          TO WSAA-T5658-AGETERM.           */
	wsaaT5658Ageterm.set(premiumrec.duration);	
	
	List<Itempf> list = itemDAO.getItdmByFrmdate(premiumrec.chdrChdrcoy.toString(),"T5658", wsaaT5658Key.toString(), itmfrm);
	
	if(list == null || list.size() <=0 ) {
		premiumrec.statuz.set(f265);
	 }else {
			t5658rec.t5658Rec.set(StringUtil.rawToString(list.get(0).getGenarea()));
	 }
	}

protected void basicAnnualPremium200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					setupLextKey200();
					readLext210();
				case loopForAdjustedAge220: 
					loopForAdjustedAge220(); 
				case checkT5658Insprm230: 
					checkT5658Insprm230();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setupLextKey200()
{
	/* If calculating the basic premium, do not adjust the age.        */
	if (isEQ(wsaaBasicPremium, "Y")) {
		wsaaAdjustedAge.set(premiumrec.lage);
		goTo(GotoLabel.checkT5658Insprm230);
	}
	/* Obtain the age rates from the (LEXT) record.*/
	/*  - read all the LEXT records for this contract, life and*/
	/*  coverage into the working-storage table. Compute the*/
	/*  adjusted age as being the summation of the LEXT age*/
	/*  rates plus the ANB @ RCD.*/
	lextpf = new Lextpf();
	lextpf.setChdrcoy(premiumrec.chdrChdrcoy.toString());
	lextpf.setChdrnum(premiumrec.chdrChdrnum.toString());
	lextpf.setLife(premiumrec.lifeLife.toString());
	lextpf.setCoverage(premiumrec.covrCoverage.toString());
	lextpf.setRider(premiumrec.covrRider.toString());
	lextpf.setSeqnbr(0);
	wsaaSub.set(0);
	List<Lextpf> list = lextpfDAO.getLextData(lextpf);
	if(list == null || list.size() == 0) {
		goTo(GotoLabel.loopForAdjustedAge220);
	}
	lextItr = list.iterator();
	
	}
protected void readLext210()
	{

		if (lextItr.hasNext()) {
			/*NEXT_SENTENCE*/
			lextpf = lextItr.next();
		}
		else {
			goTo(GotoLabel.loopForAdjustedAge220);
		}
		if (isEQ(premiumrec.reasind, "2")
		&& "1".equals(lextpf.getReasind())) {
			goTo(GotoLabel.loopForAdjustedAge220);
		}
		if (isNE(premiumrec.reasind, "2")
		&& "2".equals(lextpf.getReasind())) {
			goTo(GotoLabel.loopForAdjustedAge220);
		}
		/*  Skip any expired special terms.*/
		if (isLTE(lextpf.getExtCessDate(), premiumrec.reRateDate)) {
			readLext210();
			return ;
		}
		wsaaSub.add(1);
		wsaaLextOppc[wsaaSub.toInt()].set(lextpf.getOppc());
		wsaaLextZmortpct[wsaaSub.toInt()].set(lextpf.getZmortpct());
		wsaaLextOpcda[wsaaSub.toInt()].set(lextpf.getOpcda());
		wsaaRatesPerMillieTot.add(lextpf.getInsprm());
		/*BRD-306 START */
		wsaaPremiumAdjustTot = wsaaPremiumAdjustTot.add(lextpf.getPremadj());
		/*BRD-306 END */
		wsaaAgerateTot.add(lextpf.getAgerate());
		readLext210();
		return ;
	}

protected void loopForAdjustedAge220()
	{
		compute(wsaaAdjustedAge, 0).set((add(wsaaAgerateTot, premiumrec.lage)));
	}

protected void checkT5658Insprm230()
	{
		/* Use the age calculated above to access the table T5658 and*/
		/* check the following:*/
		/*  - that the basic annual premium (indexed by year) from*/
		/*  the T5658 table is not zero. If it is zero, then display*/
		/*  an error message and skip the additional procedures.*/
		/*  Otherwise store the premium as the (BAP).*/
		/*    IF WSAA-ADJUSTED-AGE        < 1                     <V42L018>*/
		if (isLT(wsaaAdjustedAge, 0)) {
			/*       MOVE 100                 TO WSAA-ADJUSTED-AGE     <V73L03>*/
			wsaaAdjustedAge.set(110);
		}
		/*    IF WSAA-ADJUSTED-AGE        < 1 OR                           */
		if (isLT(wsaaAdjustedAge, 0)
		|| isGT(wsaaAdjustedAge, 110)) {
			premiumrec.statuz.set(e107);
			return ;
		}
		/*  Check for adjusted age = 0; move the premium rate              */
		if (isEQ(wsaaAdjustedAge, 0)) {
			if (isEQ(t5658rec.insprem, ZERO)) {
				premiumrec.statuz.set(e107);
			}
			else {
				wsaaBap.set(t5658rec.insprem);
			}
			return ;
		}
		/*  Note - only 99 out of the 100 rates fit in the occurs table.*/
		/*    IF WSAA-ADJUSTED-AGE = 100                                   */
		/*       IF T5658-INSTPR = ZERO                                    */
		/*          MOVE E107                TO CPRM-STATUZ                */
		/*       ELSE                                                      */
		/*          MOVE T5658-INSTPR        TO WSAA-BAP                   */
		/*  Extend the age band to 110.                                    */
		if (isGTE(wsaaAdjustedAge, 100)
		&& isLTE(wsaaAdjustedAge, 110)) {
			compute(wsaaIndex, 0).set(sub(wsaaAdjustedAge, 99));
			if (isEQ(t5658rec.instpr[wsaaIndex.toInt()], ZERO)) {
				premiumrec.statuz.set(e107);
			}
			else {
				wsaaBap.set(t5658rec.instpr[wsaaIndex.toInt()]);
			}
		}
		else {
			if (isEQ(t5658rec.insprm[wsaaAdjustedAge.toInt()], ZERO)) {
				premiumrec.statuz.set(e107);
			}
			else {
				wsaaBap.set(t5658rec.insprm[wsaaAdjustedAge.toInt()]);
			}
		}
	}

protected void ratesPerMillieLoadings300()
	{
		/*PARA*/
		/* APPLY-RATE-PER-MILLE-LOADINGS*/
		/* - sum the rates per mille from the LEXT W/S table.*/
		/*  - add rates per mille to the BAP*/
		compute(wsaaBap, 2).set(add(wsaaRatesPerMillieTot, wsaaBap));
		/*EXIT*/
		/*BRD-306 START */
		compute(premiumrec.rateadj, 2).set(wsaaRatesPerMillieTot);
		/*BRD-306 END */
	}

protected void volumeDiscountBap1400()
	{
		try {
			readT5659410();
			checkSumInsuredRange430();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readT5659410()
	{
		/* APPLY-DISCOUNT.*/
		/* Access the discount table T5659 using the key:-*/
		/* - Discount method from T5658 concatenated with currency.*/
		/*  - check the sum insurred against the volume band ranges*/
		/*  and when within a range store the discount amount.*/
		/*  - compute the BAP as the BAP - discount*/
	  wsaaDisccntmeth.set(t5658rec.disccntmeth);
	  wsaaCurrcode.set(premiumrec.currcode);	
	
	  List<Itempf> list = itemDAO.getItdmByFrmdate(premiumrec.chdrChdrcoy.toString(),"T5659", wsaaT5659Key.toString(), itmfrm);
		
		
		if (list == null || list.size() == 0) {
			/*     MOVE F264                TO CPRM-STATUZ                   */
			/*     MOVE WSAA-T5658-KEY      TO SYSR-PARAMS              <002>*/
			/*     MOVE F264                TO SYSR-STATUZ              <002>*/
			/*     PERFORM 9000-FATAL-ERROR                             <002>*/
			premiumrec.statuz.set(f264);
			goTo(GotoLabel.exit490);
		}
		t5659rec.t5659Rec.set(StringUtil.rawToString(list.get(0).getGenarea()));
		wsaaSub.set(0);
	}

protected void checkSumInsuredRange430()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 4)) {
			return ;
		}
		if (isLT(premiumrec.sumin, t5659rec.volbanfr[wsaaSub.toInt()])
		|| isGT(premiumrec.sumin, t5659rec.volbanto[wsaaSub.toInt()])) {
			checkSumInsuredRange430();
			return ;
		}
		else {
			wsaaDiscountAmt.set(t5659rec.volbanle[wsaaSub.toInt()]);
		}
		compute(wsaaBap, 2).set(sub(wsaaBap, wsaaDiscountAmt));
	}

protected void premiumUnit500()
	{
		/*PARA*/
		/* APPLY-PREMIUM-UNIT*/
		/* - Obtain the risk-unit from T5658*/
		/*  - multiply BAP by the sum-insured and divide*/
		/*    by the risk-unit*/
		/* COMPUTE WSAA-BAP = ((WSAA-BAP * CPRM-SUMIN) / T5658-UNIT)   .*/
		if (isEQ(t5658rec.unit, ZERO)) {
			premiumrec.statuz.set(f110);
		}
		else {
			compute(wsaaBap, 2).set((div((mult(wsaaBap, premiumrec.sumin)), t5658rec.unit)));
		}
		/*EXIT*/
	}

protected void percentageLoadings600()
	{
		para600();
		calculateLoadings610();
	}

protected void para600()
	{
		/* APPLY-PERCENTAGE-LOADINGS*/
		/* - from the LEXT working-storage (W/S) table apply the*/
		/* percentage loadings. For each loading entry on the table*/
		/* compute the BAP as follows:*/
		/*  BAP = BAP * loading percentage / 100.*/
		wsaaSub.set(0);
	}

protected void calculateLoadings610()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 8)) {
			return ;
		}
		if (isNE(wsaaLextOppc[wsaaSub.toInt()], 0)) {
			compute(wsaaBap, 2).set((div((mult(wsaaBap, wsaaLextOppc[wsaaSub.toInt()])), 100)));
			isLoadingAva = true;
		}
		calculateLoadings610();		
		return ;
	}

protected void instalmentPremium700()
	{
		para700();
		instalmentPrem710();
	}

protected void para700()
	{
		/* CALCULATE-INSTALMENT-PREMIUM.*/
		/* Determine which billing frequency to use.*/
		/* compute the basic-instalment-premium (BIP) as:-*/
		/* basic-premium * factor (FACTOR is from T5658).*/
		wsaaBip.set(0);
	}

protected void instalmentPrem710()
	{
		wsaaModalFactor.set(0);
		if (isEQ(premiumrec.billfreq, "01")
		|| isEQ(premiumrec.billfreq, "00")) {
			wsaaModalFactor.set(t5658rec.mfacty);
		}
		else {
			if (isEQ(premiumrec.billfreq, "02")) {
				wsaaModalFactor.set(t5658rec.mfacthy);
			}
			else {
				if (isEQ(premiumrec.billfreq, "04")) {
					wsaaModalFactor.set(t5658rec.mfactq);
				}
				else {
					if (isEQ(premiumrec.billfreq, "12")) {
						wsaaModalFactor.set(t5658rec.mfactm);
					}
					else {
						if (isEQ(premiumrec.billfreq, "13")) {
							wsaaModalFactor.set(t5658rec.mfact4w);
						}
						else {
							if (isEQ(premiumrec.billfreq, "24")) {
								wsaaModalFactor.set(t5658rec.mfacthm);
							}
							else {
								if (isEQ(premiumrec.billfreq, "26")) {
									wsaaModalFactor.set(t5658rec.mfact2w);
								}
								else {
									if (isEQ(premiumrec.billfreq, "52")) {
										wsaaModalFactor.set(t5658rec.mfactw);
									}
								}
							}
						}
					}
				}
			}
		}
		if (isEQ(wsaaModalFactor, 0)) {
			premiumrec.statuz.set(f272);
		}
		else {
			compute(wsaaBip, 4).set(mult(wsaaBap, wsaaModalFactor));
		}
	}

protected void rounding800()
	{
		para800();
		rounded820();
	}

protected void para800()
	{
		/* CALCULATE-ROUNDING.*/
		/* - round up depending on the rounding factor (obtained from*/
		/* the T5659 table).*/
		/* - if the prem-unit from T5658 is greater than zero, then*/
		/* compute the BIP as the rounded number / the premium-unit*/
		/* (from T5658). The premium unit is the quantity in which the*/
		/* currency is dominated in.*/
		wsaaRoundNum.set(wsaaBip);
		if (isEQ(t5659rec.rndfact, 1)
		|| isEQ(t5659rec.rndfact, 0)) {
			if (isLT(wsaaRoundDec, .5)) {
				wsaaRoundDec.set(0);
			}
			else {
				wsaaRoundNum.add(1);
				wsaaRoundDec.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact, 10)) {
			if (isLT(wsaaRound1, 5)) {
				wsaaRound1.set(0);
			}
			else {
				wsaaRoundNum.add(10);
				wsaaRound1.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact, 100)) {
			if (isLT(wsaaRound10, 50)) {
				wsaaRound10.set(0);
			}
			else {
				wsaaRoundNum.add(100);
				wsaaRound10.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact, 1000)) {
			if (isLT(wsaaRound100, 500)) {
				wsaaRound100.set(0);
			}
			else {
				/*          ADD 100               TO WSAA-ROUND-NUM                */
				wsaaRoundNum.add(1000);
				wsaaRound100.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact, 10000)) {
			if (isLT(wsaaRound1000, 5000)) {
				wsaaRound1000.set(0);
			}
			else {
				wsaaRoundNum.add(10000);
				wsaaRound1000.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact, 100000)) {
			if (isLT(wsaaRound10000, 50000)) {
				wsaaRound10000.set(0);
			}
			else {
				wsaaRoundNum.add(100000);
				wsaaRound10000.set(0);
			}
		}
	}

protected void rounded820()
	{
		wsaaBip.set(wsaaRoundNum);
		if (isEQ(t5658rec.premUnit, 0)) {
			premiumrec.calcPrem.set(wsaaBip);
		}
		else {
			if (isEQ(wsaaBasicPremium, "Y")) {
				compute(premiumrec.calcBasPrem, 2).set((div(wsaaBip, t5658rec.premUnit)));
			}
			else {
				compute(premiumrec.calcPrem, 2).set((div(wsaaBip, t5658rec.premUnit)));
			}
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*                                                         <V42010>
	* New Flat Mortality logic computation                    <V42010>
	*                                                         <V42010>
	*900-MORTALITY-LOADINGS SECTION.                          <V42010>
	********************************                          <V42010>
	*                                                         <V42010>
	*900-PARA.                                                <V42010>
	*                                                         <V42010>
	*  First check if there is a need to perform this section at4all0>
	*                                                         <V42010>
	*    MOVE 'N'                    TO WSAA-MORTALITY-LOAD.  <V42010>
	*    PERFORM VARYING WSAA-SUB    FROM 1 BY 1 UNTIL WSAA-SUBV>2810>
	*                                OR WSAA-MORTALITY-LOAD = 'Y'2010>
	*       IF  WSAA-LEXT-ZMORTPCT (WSAA-SUB) NOT = 0         <V42010>
	*           MOVE 'Y'             TO WSAA-MORTALITY-LOAD   <V42010>
	*       END-IF                                            <V42010>
	*    END-PERFORM.                                         <V42010>
	*                                                         <V42010>
	*    IF  WSAA-MORTALITY-LOAD     = 'N'                    <V42010>
	*        GO TO 990-EXIT                                   <V42010>
	*    END-IF.                                              <V42010>
	*                                                         <V42010>
	* APPLY-SEX/MORTALITY LOADINGS                            <V42010>
	*                                                         <V42010>
	* Read TH606 using concatenated key CPRM-LSEX + CPRM-MORTPCT42010>
	* to obtain the Sex/Mortality Class Indicator.            <V42010>
	* For each LEXT-ZMORTPCT kept in working-storage (w/s) table42010>
	* read TH549 using concatenated key CPRM-CRTABLE + w/s-ZMORTPCT10>
	* using concatenated key CPRM-CRTABLE + LEXT-ZMORTPCT +   <V42010>
	* + TH606-ZSEXMORT to compute BAP as:                     <V42010>
	*                                                         <V42010>
	*  BAP = BAP + TH549-rate * sum insured / risk unit / prem<unit10>
	*                                                         <V42010>
	*    MOVE SPACES                 TO ITEM-DATA-KEY.        <V42010>
	*    MOVE 'IT'                   TO ITEM-ITEMPFX.         <V42010>
	*    MOVE CPRM-CHDR-CHDRCOY      TO ITEM-ITEMCOY.         <V42010>
	*    MOVE TH606                  TO ITEM-ITEMTABL.        <V42010>
	*    MOVE CPRM-LSEX              TO WSAA-TH606-SEX.       <V42010>
	*    MOVE CPRM-MORTCLS           TO WSAA-TH606-MORTCLS.   <V42010>
	*    MOVE WSAA-TH606-KEY         TO ITEM-ITEMITEM.        <V42010>
	*                                                         <V42010>
	*    MOVE ITEMREC                TO ITEM-FORMAT.          <V42010>
	*    MOVE 'READR'                TO ITEM-FUNCTION.        <V42010>
	*                                                         <V42010>
	*    CALL 'ITEMIO'               USING ITEM-PARAMS.       <V42010>
	*                                                         <V42010>
	*    IF  ITEM-STATUZ             NOT = '****'             <V42010>
	*    AND ITEM-STATUZ             NOT = 'MRNF'             <V42010>
	*       MOVE ITEM-PARAMS         TO SYSR-PARAMS           <V42010>
	*       PERFORM 9000-FATAL-ERROR                          <V42010>
	*    END-IF.                                              <V42010>
	*                                                         <V42010>
	*    IF  ITEM-STATUZ             = 'MRNF'                 <V42010>
	*        MOVE HL26               TO CPRM-STATUZ           <V42010>
	*        GO TO 990-EXIT                                   <V42010>
	*    END-IF.                                              <V42010>
	*                                                         <V42010>
	*    MOVE ITEM-GENAREA           TO TH606-TH606-REC.      <V42010>
	*                                                         <V42010>
	*    MOVE 0                      TO WSAA-SUB.             <V42010>
	*                                                         <V42010>
	*910-CALC-MORT-LOADINGS.                                  <V42010>
	*    ADD 1                       TO WSAA-SUB.             <V42010>
	*    IF  WSAA-SUB                > 8                      <V42010>
	*        GO TO 990-EXIT                                   <V42010>
	*    END-IF.                                              <V42010>
	*                                                         <V42010>
	*    IF  WSAA-LEXT-ZMORTPCT (WSAA-SUB) = 0                <V42010>
	*        GO TO 910-CALC-MORT-LOADINGS                     <V42010>
	*    END-IF.                                              <V42010>
	*                                                         <V42010>
	*    MOVE SPACES                 TO ITDM-DATA-KEY.        <V42010>
	*    MOVE CPRM-CHDR-CHDRCOY      TO ITDM-ITEMCOY.         <V42010>
	*    MOVE TH549                  TO ITDM-ITEMTABL.        <V42010>
	*    MOVE CPRM-CRTABLE           TO WSAA-TH549-CRTABLE.   <V42010>
	*    MOVE WSAA-LEXT-ZMORTPCT (WSAA-SUB)                   <V42010>
	*                                TO WSAA-TH549-ZMORTPCT.  <V42010>
	*    MOVE TH606-ZSEXMORT         TO WSAA-TH549-ZSEXMORT.  <V42010>
	*    MOVE WSAA-TH549-KEY         TO ITDM-ITEMITEM.        <V42010>
	*    IF  CPRM-RATINGDATE         = ZEROS                  <V42010>
	*        MOVE  99999999          TO ITDM-ITMFRM           <V42010>
	*    ELSE                                                 <V42010>
	*        MOVE CPRM-RATINGDATE    TO ITDM-ITMFRM           <V42010>
	*    END-IF.                                              <V42010>
	*    MOVE 'BEGN'                 TO ITDM-FUNCTION.        <V42010>
	*                                                         <V42010>
	*    CALL 'ITDMIO' USING         ITDM-PARAMS.             <V42010>
	*                                                         <V42010>
	*    IF ITDM-STATUZ              NOT = '****' AND         <V42010>
	*       ITDM-STATUZ              NOT = 'ENDP'             <V42010>
	*       MOVE ITDM-PARAMS         TO SYSR-PARAMS           <V42010>
	*       PERFORM 9000-FATAL-ERROR.                         <V42010>
	*                                                         <V42010>
	*    IF WSAA-TH549-KEY           NOT = ITDM-ITEMITEM      <V42010>
	*     OR CPRM-CHDR-CHDRCOY       NOT = ITDM-ITEMCOY       <V42010>
	*     OR ITDM-ITEMTABL           NOT = TH549              <V42010>
	*     OR ITDM-STATUZ             = 'ENDP'                 <V42010>
	*       MOVE HL27                TO CPRM-STATUZ           <V42010>
	*       GO TO 990-EXIT                                    <V42010>
	*    ELSE                                                 <V42010>
	*       MOVE ITDM-GENAREA        TO T5664-T5664-REC.      <V42010>
	*                                                         <V42010>
	*    COMPUTE WSAA-BAP =                                   <V42010>
	*            WSAA-BAP + (((T5664-INSPRM (CPRM-LAGE) *     <V42010>
	*            CPRM-SUMIN) / T5664-UNIT) *                  <V42010>
	*                        (WSAA-LEXT-ZMORTPCT (WSAA-SUB) / 100)).0>
	*                                                         <V42010>
	*    GO TO 910-CALC-MORT-LOADINGS.                        <V42010>
	*                                                         <V42010>
	*990-EXIT.                                                <V42010>
	*     EXIT.                                               <V42010>
	* </pre>
	*/
protected void mortalityLoadings950()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para950();
				case calcMortLoadings950: 
					calcMortLoadings950();
					calcMortLoadings960();
				case callSubroutine980: 
					callSubroutine980();
				case exit950: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para950()
	{
		/*  First check if there is a need to perform this section at all  */
		/*  SINCE THIS IS BASE ON TERM TH606 TABLE MUST BE SETUP W/ TERM   */
		premiumrec.fltmort.set(ZERO);
		wsaaMortalityLoad = "N";
		for (wsaaSub.set(1); !(isGT(wsaaSub, 8)
		|| isEQ(wsaaMortalityLoad, "Y")); wsaaSub.add(1)){
			if (isNE(wsaaLextZmortpct[wsaaSub.toInt()], 0)) {
				wsaaMortalityLoad = "Y";
			}
		}
		if (isEQ(wsaaMortalityLoad, "N")) {
			goTo(GotoLabel.exit950);
		}
		wsaaSub.set(0);
	}

protected void calcMortLoadings950()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 8)) {
			goTo(GotoLabel.exit950);
		}
		if (isEQ(wsaaLextZmortpct[wsaaSub.toInt()], 0)) {
			calcMortLoadings950();
			return ;
		}
		/*  Read TH549 table to get Calculation routine for Flat Mortality */
		wsaaTh549Crtable.set(premiumrec.crtable);
		wsaaTh549Zmortpct.set(wsaaLextZmortpct[wsaaSub.toInt()]);
		wsaaTh549Zsexmort.set(premiumrec.lsex);
		
		List<Itempf> list = itemDAO.getItdmByFrmdate(premiumrec.chdrChdrcoy.toString(),"TH549", wsaaTh549Key.toString(), itmfrm);
		
	    if(list == null || list.size() ==0) {
	    	premiumrec.statuz.set(hl27);
			goTo(GotoLabel.exit950);
	    }
		th549rec.th549Rec.set(StringUtil.rawToString(list.get(0).getGenarea()));
		wsaaTh606Key.set(SPACES);
		a100ReadLife();
		/* Read TH606 using concatenated key Coverage + TH549-indic        */
		wsaaTh606Ageterm.set(premiumrec.duration);
		wsaaTh606Crtable.set(premiumrec.crtable);
		
		list = itemDAO.getItdmByFrmdate(premiumrec.chdrChdrcoy.toString(),"TH606", wsaaTh606Key.toString(), itmfrm);
	
		if ( list == null || list.size() == 0) {
			premiumrec.statuz.set(hl26);
			wsaaMortFactor.set(ZERO);
			wsaaMortRate.set(ZERO);
			goTo(GotoLabel.callSubroutine980);
		}
		else {
			th606rec.th606Rec.set(StringUtil.rawToString(list.get(0).getGenarea()));
		}
	}

protected void calcMortLoadings960()
	{
		if (isEQ(wsaaLextZmortpct[wsaaSub.toInt()], 0)) {
			goTo(GotoLabel.calcMortLoadings950);
		}
		a150GetValues();
	}

protected void callSubroutine980()
	{
		extprmrec.freqFactor.set(wsaaMortFactor);
		extprmrec.mortRate.set(wsaaMortRate);
		extprmrec.premium.set(premiumrec.calcPrem);
		extprmrec.sumass.set(premiumrec.sumin);
		/* MOVE    1                   TO EXTP-EX-FACTOR        <V42010>*/
		extprmrec.exFactor.set(th549rec.expfactor);
		extprmrec.percent.set(wsaaLextZmortpct[wsaaSub.toInt()]);
		extprmrec.term.set(premiumrec.duration);
		extprmrec.freq.set(premiumrec.billfreq);
		extprmrec.loading.set(ZERO);
		extprmrec.statuz.set("****");
		for (wsaaCount.set(1); !(isGT(wsaaCount, 5)
		|| isEQ(th549rec.opcda[wsaaCount.toInt()], SPACES)); wsaaCount.add(1)){
			if (isEQ(th549rec.opcda[wsaaCount.toInt()], wsaaLextOpcda[wsaaSub.toInt()])) {
				callProgram(th549rec.subrtn[wsaaCount.toInt()], extprmrec.parmRec);
				wsaaCount.set(6);
			}
			else {
				if (isEQ(th549rec.opcda[wsaaCount.toInt()], "**")) {
					callProgram(th549rec.subrtn[wsaaCount.toInt()], extprmrec.parmRec);
					wsaaCount.set(6);
				}
			}
		}
		compute(wsaaBap, 2).set(add(wsaaBap, extprmrec.loading));
		/*BRD-306 START */
		premiumrec.fltmort.set(div(extprmrec.loading,t5658rec.premUnit)); 
		/*BRD-306 END */
		goTo(GotoLabel.calcMortLoadings950);
	}
/*BRD-306 START */
protected void premiumAdjustedLoadings900()
	{
		/*PARA*/
		/* APPLY-PREMIUM-ADJUSTED-LOADINGS*/
		/* - sum os the Premium adjusted records from the LEXT W/S table.*/
		/*  - add rates per mille to the BAP*/
		compute(premiumrec.calcPrem, 2).set(add(wsaaPremiumAdjustTot, premiumrec.calcPrem));
		
		/*EXIT*/
		
		compute(premiumrec.premadj,2).set(wsaaPremiumAdjustTot);
	}
/*BRD-306 END */
protected void r100StaffDiscount()
	{
		r110ReadChdrlnb();
		r120ReadClex();
	}

protected void r110ReadChdrlnb()
	{
		/* Read CHDRLNB for CHDRLNB-COWNNUM or CHDRLNB-JOWNNUM.            */
		/*    Retrieve contract header information.                        */
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError9000();
		}
		/* In some pgms that call the premium calc. routine CHDRLNB        */
		/* is not kept stored(KEEPS/READS) so we'll have to read the       */
		/* CHDRLNB                                                         */
		if (isEQ(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			chdrlnbIO.setChdrcoy(premiumrec.chdrChdrcoy);
			chdrlnbIO.setChdrnum(premiumrec.chdrChdrnum);
			chdrlnbIO.setFunction(varcom.readr);
			chdrlnbIO.setFormat(chdrlnbrec);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlnbIO.getParams());
				fatalError9000();
			}
		}
	}

protected void r120ReadClex()
	{
		if (isEQ(chdrlnbIO.getCownnum(), SPACES)) {
			return ;
		}
		/* To check staff flag - Read the client extra details.            */
		clexIO.setClntcoy(chdrlnbIO.getCowncoy());
		clexIO.setClntnum(chdrlnbIO.getCownnum());
		clexIO.setClntpfx(chdrlnbIO.getCownpfx());
		clexIO.setFunction(varcom.readr);
		clexIO.setFormat(clexrec);
		SmartFileCode.execute(appVars, clexIO);
		if (isNE(clexIO.getStatuz(), varcom.oK)
		&& isNE(clexIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(clexIO.getParams());
			fatalError9000();
		}
		if (isNE(clexIO.getRstaflag(), "Y")
		&& isNE(chdrlnbIO.getJownnum(), SPACES)) {
			clexIO.setClntcoy(chdrlnbIO.getCowncoy());
			clexIO.setClntnum(chdrlnbIO.getJownnum());
			clexIO.setClntpfx(chdrlnbIO.getCownpfx());
			clexIO.setFormat(clexrec);
			clexIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, clexIO);
			if (isNE(clexIO.getStatuz(), varcom.oK)
			&& isNE(clexIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(clexIO.getParams());
				fatalError9000();
			}
		}
		/* If the contract owner is a corporate client, check the          */
		/* life assured                                                    */
		if (isEQ(clexIO.getRstaflag(), "Y")) {
			r200ReadTh609();
		}
		wsaaStaffFlag.set(clexIO.getRstaflag());
	}

protected void r200ReadTh609()
	{
		r210ReadStaffDiscount();
	}

	/**
	* <pre>
	* Read TH609 staff discount percentage.                           
	* </pre>
	*/
protected void r210ReadStaffDiscount()
	{
	    Itempf itempf = new Itempf();
	    itempf.setItempfx("IT");
	    itempf.setItemcoy(premiumrec.chdrChdrcoy.toString());
	    itempf.setItemtabl("TH609");
	    itempf.setItemitem(chdrlnbIO.getCnttype().toString());
	    itempf = itemDAO.getItemRecordByItemkey(itempf);

		
		if (itempf == null) {
			itempf.setItempfx("IT");
		    itempf.setItemcoy(premiumrec.chdrChdrcoy.toString());
		    itempf.setItemtabl("TH609");
		    itempf.setItemitem("***");
		    itempf = itemDAO.getItemRecordByItemkey(itempf);
		}
		
		if (itempf == null) {
			wsaaStaffDiscount.set(ZERO);
			th609rec.th609Rec.set(SPACES);
		}
		else {
			th609rec.th609Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			wsaaStaffDiscount.set(th609rec.prcnt);
		}
	}

protected void r300BaseRateStaffDisc()
	{
		/*R300-START*/
		/* To calculate WSAA-BAP  after staff discount.                    */
		compute(wsaaBap, 3).setRounded(mult(wsaaBap, (div((sub(100, wsaaStaffDiscount)), 100))));
		/*R399-EXIT*/
		
	}

protected void r400InstalmentStaffDisc()
	{
		/*R400-START*/
		/* To calculate WSAA-BIP after staff discount.                     */
		compute(wsaaBip, 3).setRounded(mult(wsaaBip, (div((sub(100, wsaaStaffDiscount)), 100))));
		/*R499-EXIT*/
	}

protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz, "BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		premiumrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}

protected void a100ReadLife()
	{
		a100ReadLifePara();
	}

protected void a100ReadLifePara()
	{
	    Lifepf lifepf = new Lifepf();
	    lifepf.setChdrcoy(premiumrec.chdrChdrcoy.toString());
	    lifepf.setChdrnum(premiumrec.chdrChdrnum.toString());
	    lifepf.setLife(premiumrec.lifeLife.toString());
	    lifepf.setJlife(premiumrec.lifeJlife.toString());
	    lifepf.setCurrfrom(varcom.vrcmMaxDate.toInt());
	    lifepf = lifepfDAO.getLifeRecordByKey(lifepf);

		if (lifepf == null ) {
			syserrrec.params.set(premiumrec.chdrChdrcoy.toString().concat(premiumrec.chdrChdrnum.toString()).concat(premiumrec.lifeLife.toString()));
			fatalError9000();
		}
		if ("S".equals(lifepf.getSmoking())) {
			wsaaTh606Indic.set(th549rec.indc01);
		}
		else {
			wsaaTh606Indic.set(th549rec.indc02);
		}
	}

protected void a150GetValues()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					a150GetValuesPara();
				case a150GetModalFactor: 
					a150GetModalFactor();
				case a150Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a150GetValuesPara()
	{
		/*  Get the Mortality Rate                                         */
		/*    IF WSAA-ADJUSTED-AGE        < 1                     <V42L018>*/
		if (isLT(wsaaAdjustedAge, 0)) {
			/*       MOVE 100                 TO WSAA-ADJUSTED-AGE     <V73L03>*/
			wsaaAdjustedAge.set(110);
		}
		/*    IF WSAA-ADJUSTED-AGE        < 1 OR                  <V42L018>*/
		if (isLT(wsaaAdjustedAge, 0)
		|| isGT(wsaaAdjustedAge, 110)) {
			premiumrec.statuz.set(e107);
			/*       GO TO 290-EXIT.                                  <V42L018>*/
			goTo(GotoLabel.a150Exit);
		}
		/*  Check for adjusted age = 0; move the premium rate              */
		if (isEQ(wsaaAdjustedAge, 0)) {
			if (isEQ(th606rec.insprem, ZERO)) {
				premiumrec.statuz.set(e107);
				goTo(GotoLabel.a150Exit);
			}
			else {
				wsaaBap.set(th606rec.insprem);
				wsaaMortRate.set(th606rec.insprem);
				goTo(GotoLabel.a150GetModalFactor);
			}
		}
		/*  Note - only 99 out of the 100 rates fit in the occurs table.   */
		/*    IF WSAA-ADJUSTED-AGE = 100                           <V73L03>*/
		/*       IF TH606-INSTPR = ZERO                            <V73L03>*/
		/*          MOVE E107                TO CPRM-STATUZ        <V73L03>*/
		/*       ELSE                                              <V73L03>*/
		/*          MOVE TH606-INSTPR        TO WSAA-MORT-RATE     <V73L03>*/
		/*       END-IF                                            <V73L03>*/
		/*  Extend the age band to 110.                                    */
		if (isGTE(wsaaAdjustedAge, 100)
		&& isLTE(wsaaAdjustedAge, 110)) {
			compute(wsaaIndex, 0).set(sub(wsaaAdjustedAge, 99));
			if (isEQ(th606rec.instpr[wsaaIndex.toInt()], ZERO)) {
				premiumrec.statuz.set(e107);
			}
			else {
				wsaaMortRate.set(th606rec.instpr[wsaaIndex.toInt()]);
			}
		}
		else {
			if (isEQ(th606rec.insprm[wsaaAdjustedAge.toInt()], ZERO)) {
				premiumrec.statuz.set(e107);
			}
			else {
				wsaaMortRate.set(th606rec.insprm[wsaaAdjustedAge.toInt()]);
			}
		}
	}

protected void a150GetModalFactor()
	{
		/*  Get the Modal Factor                                           */
		wsaaMortFactor.set(0);
		if (isEQ(premiumrec.billfreq, "01")
		|| isEQ(premiumrec.billfreq, "00")) {
			wsaaMortFactor.set(th606rec.mfacty);
		}
		else {
			if (isEQ(premiumrec.billfreq, "02")) {
				wsaaMortFactor.set(th606rec.mfacthy);
			}
			else {
				if (isEQ(premiumrec.billfreq, "04")) {
					wsaaMortFactor.set(th606rec.mfactq);
				}
				else {
					if (isEQ(premiumrec.billfreq, "12")) {
						wsaaMortFactor.set(th606rec.mfactm);
					}
					else {
						if (isEQ(premiumrec.billfreq, "13")) {
							wsaaMortFactor.set(th606rec.mfact4w);
						}
						else {
							if (isEQ(premiumrec.billfreq, "24")) {
								wsaaMortFactor.set(th606rec.mfacthm);
							}
							else {
								if (isEQ(premiumrec.billfreq, "26")) {
									wsaaMortFactor.set(th606rec.mfact2w);
								}
								else {
									if (isEQ(premiumrec.billfreq, "52")) {
										wsaaMortFactor.set(th606rec.mfactw);
									}
								}
							}
						}
					}
				}
			}
		}
		if (isEQ(wsaaMortFactor, 0)) {
			premiumrec.statuz.set(f272);
		}
	}
}
