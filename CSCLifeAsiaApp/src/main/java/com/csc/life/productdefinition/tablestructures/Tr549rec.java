package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:07
 * Description:
 * Copybook name: TR549REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr549rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr549Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData mdblacts = new FixedLengthStringData(4).isAPartOf(tr549Rec, 0);
  	public FixedLengthStringData[] mdblact = FLSArrayPartOfStructure(4, 1, mdblacts, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(mdblacts, 0, FILLER_REDEFINE);
  	public FixedLengthStringData mdblact01 = new FixedLengthStringData(1).isAPartOf(filler, 0);
  	public FixedLengthStringData mdblact02 = new FixedLengthStringData(1).isAPartOf(filler, 1);
  	public FixedLengthStringData mdblact03 = new FixedLengthStringData(1).isAPartOf(filler, 2);
  	public FixedLengthStringData mdblact04 = new FixedLengthStringData(1).isAPartOf(filler, 3);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(496).isAPartOf(tr549Rec, 4, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr549Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr549Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}