/*
 * File: Pr635.java
 * Date: 30 August 2009 1:50:14
 * Author: Quipoz Limited
 * 
 * Class transformed from PR635.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.life.productdefinition.dataaccess.ZmpvTableDAM;
import com.csc.life.productdefinition.dataaccess.ZmtmTableDAM;
import com.csc.life.productdefinition.screens.Sr635ScreenVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*   Termination Reason
*   ------------------
*
*   This program is used to maintain the termination reason. This
*   is free format text.
*
*
***********************************************************************
* </pre>
*/
public class Pr635 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR635");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData wsaaSeqno = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaRemainder = new ZonedDecimalData(5, 0).setUnsigned();
	private PackedDecimalData wsaaPrvDtetrm = new PackedDecimalData(8, 0);
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private String zmtmrec = "ZMTMREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Medical Provider Details File*/
	private ZmpvTableDAM zmpvIO = new ZmpvTableDAM();
		/*Medical Provider Terminated Reason*/
	private ZmtmTableDAM zmtmIO = new ZmtmTableDAM();
	private Sr635ScreenVars sv = ScreenProgram.getScreenVars( Sr635ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1290, 
		preExit, 
		exit2090, 
		exit3090
	}

	public Pr635() {
		super();
		screenVars = sv;
		new ScreenModel("Sr635", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		a1000CallSr635io();
		scrnparams.subfileRrn.set(1);
		scrnparams.subfileMore.set("Y");
		zmpvIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(zmpvIO.getStatuz());
			syserrrec.params.set(zmpvIO.getParams());
			fatalError600();
		}
		sv.clntnum.set(zmpvIO.getZmednum());
		sv.zmedprv.set(zmpvIO.getZmedprv());
		sv.clntnam.set(subString(zmpvIO.getZmednam(), 1, 30));
		zmtmIO.setParams(SPACES);
		zmtmIO.setStatuz(varcom.oK);
		zmtmIO.setZmedcoy(wsspcomn.company);
		zmtmIO.setZmedprv(zmpvIO.getZmedprv());
		zmtmIO.setSeqno(1);
		zmtmIO.setFormat(zmtmrec);
		zmtmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zmtmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zmtmIO.setFitKeysSearch("ZMEDCOY", "ZMEDPRV");
		zmtmIO.setStatuz(varcom.oK);
		wsaaCount.set(ZERO);
		while ( !(isEQ(zmtmIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaCount,sv.subfilePage))) {
			loadSubfile1200();
		}
		
		compute(wsaaRemainder, 0).set(sub(sv.subfilePage,wsaaCount));
		for (ix.set(1); !(isGT(ix,wsaaRemainder)); ix.add(1)){
			loadRemainder1300();
		}
	}

protected void loadSubfile1200()
	{
		try {
			begin1210();
		}
		catch (GOTOException e){
		}
	}

protected void begin1210()
	{
		SmartFileCode.execute(appVars, zmtmIO);
		if (isNE(zmtmIO.getStatuz(),varcom.oK)
		&& isNE(zmtmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(zmtmIO.getStatuz());
			syserrrec.params.set(zmtmIO.getParams());
			fatalError600();
		}
		if (isNE(zmtmIO.getStatuz(),varcom.oK)
		|| isNE(zmtmIO.getZmedcoy(),wsspcomn.company)
		|| isNE(zmtmIO.getZmedprv(),zmpvIO.getZmedprv())) {
			zmtmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1290);
		}
		sv.txtline.set(zmtmIO.getTxtline());
		wsaaCount.add(1);
		if (isEQ(wsaaCount,sv.subfilePage)) {
			scrnparams.subfileMore.set("Y");
		}
		if (isEQ(wsspcomn.flag,"I")) {
			sv.txtlineOut[varcom.pr.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.sadd);
		a1000CallSr635io();
		zmtmIO.setFunction(varcom.nextr);
	}

protected void loadRemainder1300()
	{
		/*BEGIN*/
		sv.txtline.set(SPACES);
		scrnparams.function.set(varcom.sadd);
		a1000CallSr635io();
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
			checkForErrors2050();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			wsaaCount.set(ZERO);
			while ( !(isEQ(zmtmIO.getStatuz(),varcom.endp)
			|| isEQ(wsaaCount,sv.subfilePage))) {
				loadSubfile1200();
			}
			
			compute(wsaaRemainder, 0).set(sub(sv.subfilePage,wsaaCount));
			for (ix.set(1); !(isGT(ix,wsaaRemainder)); ix.add(1)){
				loadRemainder1300();
			}
			scrnparams.subfileMore.set("Y");
			wsspcomn.edterror.set("Y");
			if (isEQ(wsspcomn.flag,"I")
			&& isEQ(sv.txtline,SPACES)) {
				scrnparams.subfileMore.set("N");
			}
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.srnch);
		processScreen("SR635", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
	}

protected void validateSubfile2600()
	{
		/*VALIDATION*/
		/*UPDATE-ERROR-INDICATORS*/
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		a1000CallSr635io();
		/*READ-NEXT-MODIFIED-RECORD*/
		scrnparams.function.set(varcom.srnch);
		a1000CallSr635io();
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)
		|| isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		a1000CallSr635io();
		wsaaSeqno.set(1);
		while ( !(isNE(scrnparams.statuz,varcom.oK))) {
			if (isNE(sv.txtline,SPACES)) {
				zmtmIO.setDataKey(SPACES);
				zmtmIO.setZmedcoy(wsspcomn.company);
				zmtmIO.setZmedprv(sv.zmedprv);
				zmtmIO.setSeqno(wsaaSeqno);
				zmtmIO.setFunction(varcom.readh);
				b1000CallZmtm();
				zmtmIO.setFunction(varcom.updat);
				zmtmIO.setTxtline(sv.txtline);
				b1000CallZmtm();
				wsaaSeqno.add(1);
			}
			scrnparams.function.set(varcom.srdn);
			processScreen("SR635", sv);
			if (isNE(scrnparams.statuz,varcom.oK)
			&& isNE(scrnparams.statuz,varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		
		zmtmIO.setSeqno(wsaaSeqno);
		zmtmIO.setFunction(varcom.begnh);
		b1000CallZmtm();
		while ( !(isNE(zmtmIO.getStatuz(),varcom.oK)
		|| isNE(zmtmIO.getZmedcoy(),wsspcomn.company)
		|| isNE(zmtmIO.getZmedprv(),sv.zmedprv))) {
			zmtmIO.setFunction(varcom.delet);
			b1000CallZmtm();
			zmtmIO.setFunction(varcom.nextr);
			b1000CallZmtm();
		}
		
		zmtmIO.setFunction(varcom.rlse);
		b1000CallZmtm();
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void a1000CallSr635io()
	{
		/*A1010-BEGIN*/
		processScreen("SR635", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*A1090-EXIT*/
	}

protected void b1000CallZmtm()
	{
		/*B1010-BEGIN*/
		zmtmIO.setFormat(zmtmrec);
		SmartFileCode.execute(appVars, zmtmIO);
		if (isNE(zmtmIO.getStatuz(),varcom.oK)
		&& isNE(zmtmIO.getStatuz(),varcom.endp)
		&& isNE(zmtmIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(zmtmIO.getStatuz());
			syserrrec.params.set(zmtmIO.getParams());
			fatalError600();
		}
		/*B1090-EXIT*/
	}
}
