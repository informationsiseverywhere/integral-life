package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.csc.life.productdefinition.dataaccess.dao.MedxTemppfDAO;
import com.csc.life.productdefinition.dataaccess.model.MedxTemppf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class MedxTemppfDAOImpl extends BaseDAOImpl<MedxTemppf> implements MedxTemppfDAO{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MedxTemppfDAOImpl.class);

	public boolean insertMedxTempRecords(List<MedxTemppf> medxList, String tempTableName){
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO " );
		sb.append(tempTableName);
		sb.append("(CHDRCOY, CHDRNUM, SEQNO, EXMCODE, ZMEDTYP, PAIDBY, LIFE, JLIFE, EFFDATE, INVREF, ZMEDFEE, CLNTNUM, DTETRM, DESC_T, MEMBER_NAME) ");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;	
		boolean isUpdated = false;
		try{
			ps = getPrepareStatement(sb.toString());	
			for (MedxTemppf medx : medxList) {	
				ps.setString(1, medx.getChdrcoy());
				ps.setString(2,medx.getChdrnum());
				ps.setInt(3, medx.getSeqno());
				ps.setString(4, medx.getExmcode());
				ps.setString(5, medx.getZmedtyp());
				ps.setString(6, medx.getPaidby());
				ps.setString(7, medx.getLife());
				ps.setString(8, medx.getJlife());
				ps.setInt(9, medx.getEffdate());
				ps.setString(10, medx.getInvref());
				ps.setBigDecimal(11, medx.getZmedfee());
				ps.setString(12, medx.getClntnum());
				ps.setInt(13, medx.getDtetrm());
				ps.setString(14, medx.getDescT());
				ps.setString(15, medx.getMemberName());
			    
			    ps.addBatch();				
			}		
			ps.executeBatch();
			isUpdated = true;
		}catch (SQLException e) {
			LOGGER.error("insertMedxTempRecords()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);			
		}	
		return isUpdated;
	}	
}
