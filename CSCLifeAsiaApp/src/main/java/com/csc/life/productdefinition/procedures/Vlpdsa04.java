/*
 * File: Vlpdsa04.java
 * Date: 30 August 2009 2:54:05
 * Author: Quipoz Limited
 * 
 * Class transformed from VLPDSA04.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.clients.dataaccess.SalhTableDAM;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Vlpdsubrec;
import com.csc.life.productdefinition.tablestructures.Tr51erec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* Subroutine VLPDSA04.
* ====================
*
*   This program will validate the total of the component's sum assured
*   is dependent on the amount of the client's annual income as specified
*   in TR51E
*
*   - Read table TR51E, using VLSB-CNTTYPE + VLSB-RID1-CRTABLE as keys, i
*     not found then read again by use '***' + VLSB-RID1-CRTABLE
*   - Calculate age by using call 'AGECALC'
*   - Calculate income history
*   - Accumulate SA of basic ,VLSB-INPUT-SA(of RID1) and all riders
*     (VLSB-INPUT-SA(xx)) that VLSB-INPUT-CRTABLE(xx) in list of
*     rider(TR51E-CRTABLE) in TR51E
*   - Get maximum SA (TR51E-SA) and operator (TR51E-INDC) from compare
*     AGEC-AGERATING with TR51E-AGE
*   - Compare validation  total of SA with condition from TR51E
*
***********************************************************************
* </pre>
*/
public class Vlpdsa04 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "VLPDSA04";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaErdSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private int wsaaErrSeq = 0;
	private ZonedDecimalData wsaaVlsbSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaTr51eSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaErrCode = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaInputAllSpace = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaInputCrtable = new FixedLengthStringData(4);
	private final int wsaaMaxRow = 10;
	private final int wsaaMaxCrtable = 20;
	private final int wsaaMaxError = 20;
	private final int wsaaMaxInput = 50;
	private ZonedDecimalData wsaaInputZsa = new ZonedDecimalData(17, 2).setUnsigned();
	private ZonedDecimalData wsaaBasicSa = new ZonedDecimalData(17, 2).setUnsigned();
	private ZonedDecimalData wsaaLimitIncome = new ZonedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaIncome = new PackedDecimalData(17, 0);

	private FixedLengthStringData wsaaErrDetails = new FixedLengthStringData(80);
	private FixedLengthStringData[] wsaaErrRec = FLSArrayPartOfStructure(20, 4, wsaaErrDetails, 0);
	private FixedLengthStringData[] wsaaErrDetail = FLSDArrayPartOfArrayStructure(4, wsaaErrRec, 0);

	private FixedLengthStringData wsaaEffdate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaEffdate, 0).setUnsigned();
	private ZonedDecimalData wsaaMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffdate, 4).setUnsigned();
	private ZonedDecimalData wsaaDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffdate, 6).setUnsigned();

	private FixedLengthStringData wsaaTr51eItmkey = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTr51eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr51eItmkey, 0).init(SPACES);
	private FixedLengthStringData wsaaTr51eComponent = new FixedLengthStringData(4).isAPartOf(wsaaTr51eItmkey, 3).init(SPACES);

		/* WSAA-TR51E-ARRAY */
	private FixedLengthStringData[] wsaaOccur10times = FLSInittedArray (10, 8);
	private ZonedDecimalData[] wsaaTr51eAge = ZDArrayPartOfArrayStructure(3, 0, wsaaOccur10times, 0);
	private FixedLengthStringData[] wsaaTr51eIndc = FLSDArrayPartOfArrayStructure(2, wsaaOccur10times, 3);
	private ZonedDecimalData[] wsaaTr51eOverdueMina = ZDArrayPartOfArrayStructure(3, 0, wsaaOccur10times, 5);

	private FixedLengthStringData[] wsaaTr51eRec = FLSInittedArray (20, 4);
	private FixedLengthStringData[] wsaaTr51eKey = FLSDArrayPartOfArrayStructure(4, wsaaTr51eRec, 0);
	private FixedLengthStringData[] wsaaTr51eCrtable = FLSDArrayPartOfArrayStructure(4, wsaaTr51eKey, 0, HIVALUE);
	private static final String itemrec = "ITEMREC";
	private static final String lifelnbrec = "LIFELNBREC";
	private static final String salhrec = "SALHREC";
		/* TABLES */
	private static final String tr51e = "TR51E";
	private static final String t1693 = "T1693";
		/* ERRORS */
	private static final String rlbf = "RLBF";
	private static final String rlbd = "RLBD";
	private static final String rlbi = "RLBI";
	private static final String rlbj = "RLBJ";
	private static final String e984 = "E984";
	private IntegerData wsaaTr51eIx = new IntegerData();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private SalhTableDAM salhIO = new SalhTableDAM();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Tr51erec tr51erec = new Tr51erec();
	private T1693rec t1693rec = new T1693rec();
	private Vlpdsubrec vlpdsubrec = new Vlpdsubrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		loadTr51e180, 
		exit190, 
		exit590
	}

	public Vlpdsa04() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		vlpdsubrec.validRec = convertAndSetParam(vlpdsubrec.validRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main000()
	{
		begin010();
		exit090();
	}

protected void begin010()
	{
		vlpdsubrec.statuz.set(varcom.oK);
		if (isEQ(vlpdsubrec.rid1Life, SPACES)
		&& isEQ(vlpdsubrec.rid1Jlife, SPACES)
		&& isEQ(vlpdsubrec.rid1Coverage, SPACES)
		&& isEQ(vlpdsubrec.rid1Rider, SPACES)
		&& isEQ(vlpdsubrec.rid1Crtable, SPACES)) {
			return ;
		}
		readTables100();
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		wsaaEffdate.set(vlpdsubrec.effdate);
		readFiles200();
		validation300();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readTables100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begin110();
					contractTypeComponent120();
					xxxComponent130();
				case loadTr51e180: 
					loadTr51e180();
				case exit190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin110()
	{
		/*  Identify FSU Company*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(vlpdsubrec.chdrcoy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	*  Load table TR51E.
	* </pre>
	*/
protected void contractTypeComponent120()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(vlpdsubrec.chdrcoy);
		itemIO.setItemtabl(tr51e);
		wsaaTr51eCnttype.set(vlpdsubrec.cnttype);
		wsaaTr51eComponent.set(vlpdsubrec.rid1Crtable);
		itemIO.setItemitem(wsaaTr51eItmkey);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.loadTr51e180);
		}
	}

protected void xxxComponent130()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(vlpdsubrec.chdrcoy);
		itemIO.setItemtabl(tr51e);
		wsaaTr51eCnttype.set("***");
		wsaaTr51eComponent.set(vlpdsubrec.rid1Crtable);
		itemIO.setItemitem(wsaaTr51eItmkey);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.exit190);
		}
	}

protected void loadTr51e180()
	{
		tr51erec.tr51eRec.set(itemIO.getGenarea());
		for (wsaaTr51eSeq.set(1); !(isGT(wsaaTr51eSeq, wsaaMaxRow)); wsaaTr51eSeq.add(1)){
			wsaaTr51eAge[wsaaTr51eSeq.toInt()].set(tr51erec.age[wsaaTr51eSeq.toInt()]);
			wsaaTr51eIndc[wsaaTr51eSeq.toInt()].set(tr51erec.indc[wsaaTr51eSeq.toInt()]);
			wsaaTr51eOverdueMina[wsaaTr51eSeq.toInt()].set(tr51erec.overdueMina[wsaaTr51eSeq.toInt()]);
		}
		for (wsaaTr51eSeq.set(1); !(isGT(wsaaTr51eSeq, wsaaMaxCrtable)); wsaaTr51eSeq.add(1)){
			wsaaTr51eCrtable[wsaaTr51eSeq.toInt()].set(tr51erec.crtable[wsaaTr51eSeq.toInt()]);
		}
	}

protected void readFiles200()
	{
		begin210();
	}

protected void begin210()
	{
		/*  Read LIFELNB file.*/
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(vlpdsubrec.chdrcoy);
		lifelnbIO.setChdrnum(vlpdsubrec.chdrnum);
		lifelnbIO.setLife(vlpdsubrec.rid1Life);
		lifelnbIO.setJlife(vlpdsubrec.rid1Jlife);
		if (isEQ(vlpdsubrec.rid1Jlife, SPACES)) {
			lifelnbIO.setJlife("00");
		}
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		/*  Read SALH file.*/
		salhIO.setParams(SPACES);
		salhIO.setStatuz(varcom.oK);
		wsaaIncome.set(0);
		salhIO.setClntcoy(t1693rec.fsuco);
		salhIO.setClntnum(lifelnbIO.getLifcnum());
		salhIO.setTaxYear(wsaaYear);
		salhIO.setIncomeSeqNo(0);
		salhIO.setFormat(salhrec);
		salhIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		salhIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		salhIO.setFitKeysSearch("CLNTCOY" , "CLNTNUM", "TAXYR");
		while ( !(isNE(salhIO.getStatuz(),varcom.oK))) {
			callSalhio200a();
		}
		
	}

protected void callSalhio200a()
	{
		/*A-BEGIN*/
		SmartFileCode.execute(appVars, salhIO);
		if (isNE(salhIO.getStatuz(), varcom.oK)
		|| isNE(salhIO.getClntcoy(), t1693rec.fsuco)
		|| isNE(salhIO.getClntnum(), lifelnbIO.getLifcnum())
		|| isNE(salhIO.getTaxYear(), wsaaYear)) {
			salhIO.setStatuz(varcom.endp);
			return ;
		}
		wsaaIncome.add(salhIO.getDeclGrSalary());
		salhIO.setFunction(varcom.nextr);
		/*A-EXIT*/
	}

protected void validation300()
	{
		begin310();
	}

protected void begin310()
	{
		initialize(wsaaErrDetails);
		wsaaInputAllSpace.set(SPACES);
		wsaaErdSeq.set(1);
		wsaaInputZsa.set(0);
		for (wsaaVlsbSeq.set(1); !(isGT(wsaaVlsbSeq, wsaaMaxInput)
		|| isEQ(wsaaInputAllSpace, "Y")); wsaaVlsbSeq.add(1)){
			sumassured400();
		}
		/* Total Basic Sum Assured + Rider Sum Assured*/
		compute(wsaaInputZsa, 2).set(add(wsaaInputZsa, wsaaBasicSa));
		/* Validation Sum Assured.*/
		if (isEQ(wsaaInputZsa, 0)) {
			return ;
		}
		/* Set up AGECALC to calculate age.*/
		initialize(agecalcrec.agecalcRec);
		agecalcrec.statuz.set(varcom.oK);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(vlpdsubrec.language);
		agecalcrec.cnttype.set(vlpdsubrec.cnttype);
		agecalcrec.intDate1.set(lifelnbIO.getCltdob());
		agecalcrec.intDate2.set(vlpdsubrec.effdate);
		agecalcrec.company.set(t1693rec.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
		wsaaErrCode.set(SPACES);
		wsaaErdSeq.set(1);
		for (wsaaTr51eSeq.set(1); !(isGT(wsaaTr51eSeq, wsaaMaxRow)); wsaaTr51eSeq.add(1)){
			if (isLTE(agecalcrec.agerating, wsaaTr51eAge[wsaaTr51eSeq.toInt()])) {
				chkSaLimit500();
				wsaaTr51eSeq.set(wsaaMaxRow);
			}
		}
	}

protected void sumassured400()
	{
		begin410();
	}

protected void begin410()
	{
		if (isEQ(vlpdsubrec.inputLife[wsaaVlsbSeq.toInt()], SPACES)
		&& isEQ(vlpdsubrec.inputJlife[wsaaVlsbSeq.toInt()], SPACES)
		&& isEQ(vlpdsubrec.inputCoverage[wsaaVlsbSeq.toInt()], SPACES)
		&& isEQ(vlpdsubrec.inputRider[wsaaVlsbSeq.toInt()], SPACES)
		&& isEQ(vlpdsubrec.inputCrtable[wsaaVlsbSeq.toInt()], SPACES)) {
			wsaaInputAllSpace.set("Y");
			return ;
		}
		if (isEQ(vlpdsubrec.inputCrtable[wsaaVlsbSeq.toInt()], vlpdsubrec.rid1Crtable)) {
			wsaaBasicSa.set(vlpdsubrec.inputSa[wsaaVlsbSeq.toInt()]);
		}
		wsaaInputCrtable.set(vlpdsubrec.inputCrtable[wsaaVlsbSeq.toInt()]);
		wsaaTr51eIx.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaTr51eIx, wsaaTr51eRec.length); wsaaTr51eIx.add(1)){
				if (isEQ(wsaaTr51eKey[wsaaTr51eIx.toInt()], wsaaInputCrtable)) {
					break searchlabel1;
				}
			}
			return ;
		}
		wsaaErrDetail[wsaaErdSeq.toInt()].set(SPACES);
		wsaaErdSeq.add(1);
		compute(wsaaInputZsa, 2).set(add(wsaaInputZsa, vlpdsubrec.inputSa[wsaaVlsbSeq.toInt()]));
	}

protected void chkSaLimit500()
	{
		try {
			begin510();
			moveError540();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void begin510()
	{
		if (isEQ(wsaaIncome, 0)) {
			vlpdsubrec.errCode[wsaaErdSeq.toInt()].set(rlbd);
			goTo(GotoLabel.exit590);
		}
		compute(wsaaLimitIncome, 2).set((div(mult(wsaaIncome, wsaaTr51eOverdueMina[wsaaTr51eSeq.toInt()]), 100)));
		if (isEQ(wsaaTr51eIndc[wsaaTr51eSeq.toInt()], "EQ")){
			if (isNE(wsaaInputZsa, wsaaLimitIncome)) {
				wsaaErrCode.set(rlbi);
			}
		}
		else if (isEQ(wsaaTr51eIndc[wsaaTr51eSeq.toInt()], "LE")){
			if (isGT(wsaaInputZsa, wsaaLimitIncome)) {
				wsaaErrCode.set(rlbf);
			}
		}
		else if (isEQ(wsaaTr51eIndc[wsaaTr51eSeq.toInt()], "LT")){
			if (isGTE(wsaaInputZsa, wsaaLimitIncome)) {
				wsaaErrCode.set(rlbj);
			}
		}
		else{
			wsaaErrCode.set(e984);
		}
		if (isEQ(wsaaErrCode, SPACES)) {
			goTo(GotoLabel.exit590);
		}
	}

protected void moveError540()
	{
		if (isEQ(wsaaErrDetails, SPACES)) {
			vlpdsubrec.errDet[wsaaErdSeq.toInt()].set(SPACES);
			vlpdsubrec.errCode[wsaaErdSeq.toInt()].set(wsaaErrCode);
			return ;
		}
		for (wsaaErdSeq.set(1); !(isGT(wsaaErdSeq, wsaaMaxError)); wsaaErdSeq.add(1)){
			vlpdsubrec.errCode[wsaaErdSeq.toInt()].set(wsaaErrCode);
			vlpdsubrec.errDet[wsaaErdSeq.toInt()].set(wsaaErrDetail[wsaaErdSeq.toInt()]);
		}
	}

protected void fatalError600()
	{
		/*FATAL-ERRORS*/
		syserrrec.subrname.set(wsaaSubr);
		vlpdsubrec.statuz.set(syserrrec.statuz);
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
