package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:39
 * Description:
 * Copybook name: T5662REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5662rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5662Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData bnyfctn = new FixedLengthStringData(5).isAPartOf(t5662Rec, 0);
  	public ZonedDecimalData bnypc = new ZonedDecimalData(5, 2).isAPartOf(t5662Rec, 5);
  	public FixedLengthStringData filler = new FixedLengthStringData(490).isAPartOf(t5662Rec, 10, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5662Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5662Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}