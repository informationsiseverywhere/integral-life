package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:19
 * Description:
 * Copybook name: TR640REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr640rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr640Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData zmedfee = new ZonedDecimalData(17, 2).isAPartOf(tr640Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(483).isAPartOf(tr640Rec, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr640Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr640Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}