/*
 * File: Br649.java
 * Date: 29 August 2009 22:32:57
 * Author: Quipoz Limited
 * 
 * Class transformed from BR649.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.productdefinition.dataaccess.dao.Br649DAO;
import com.csc.life.productdefinition.dataaccess.model.MerxTemppf;
import com.csc.life.productdefinition.reports.Rr649Report;
import com.csc.smart.procedures.Contot;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*   Medical Payment Exception Listing.
*    This batch program is to provide Medical Bill Payment
*    Exception Listing. Program will extract from Exception
*    file. Sort by Contract no., having total end of report.
*
*    Read exception file, sort by Contract No.
*    Total at end of report.
*
*   CONTROL TOTALS:
*     01  -  Medical bill exception listing pages amount
*     02  -  No of bills read.
*     03  -  No of bills extracted.
*
***********************************************************************
* </pre>
*/
public class Br649 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Rr649Report printerFile = new Rr649Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(250);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR649");
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(7, 0).setUnsigned();
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");
	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData rr649H01 = new FixedLengthStringData(51);
	private FixedLengthStringData rr649h01O = new FixedLengthStringData(51).isAPartOf(rr649H01, 0);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rr649h01O, 0);
	private FixedLengthStringData rh01Repdate = new FixedLengthStringData(10).isAPartOf(rr649h01O, 10);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rr649h01O, 20);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rr649h01O, 21);
	private FixedLengthStringData rr649D01 = new FixedLengthStringData(155);
	private FixedLengthStringData rr649d01O = new FixedLengthStringData(155).isAPartOf(rr649D01, 0);
	private ZonedDecimalData rd01Nofmbr = new ZonedDecimalData(7, 0).isAPartOf(rr649d01O, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(rr649d01O, 7);
	private FixedLengthStringData rd01Zmedtyp = new FixedLengthStringData(8).isAPartOf(rr649d01O, 15);
	private FixedLengthStringData rd01Exmcode = new FixedLengthStringData(10).isAPartOf(rr649d01O, 23);
	private FixedLengthStringData rd01Effdate = new FixedLengthStringData(10).isAPartOf(rr649d01O, 33);
	private FixedLengthStringData rd01Invref = new FixedLengthStringData(15).isAPartOf(rr649d01O, 43);
	private ZonedDecimalData rd01Zmedfee = new ZonedDecimalData(17, 2).isAPartOf(rr649d01O, 58);
	private FixedLengthStringData rd01Name = new FixedLengthStringData(30).isAPartOf(rr649d01O, 75);
	private FixedLengthStringData rd01Desc = new FixedLengthStringData(50).isAPartOf(rr649d01O, 105);
	private FixedLengthStringData rr649T01 = new FixedLengthStringData(17);
	private FixedLengthStringData rr649t01O = new FixedLengthStringData(17).isAPartOf(rr649T01, 0);
	private ZonedDecimalData rt01Zmedfee = new ZonedDecimalData(17, 2).isAPartOf(rr649t01O, 0);
	private FixedLengthStringData rr649E01 = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	
	/*Batch Upgrade Variables*/
	private int intBatchID;
	private int intBatchExtractSize;	
	private int intBatchStep;
	private String tranDesc;
	private int ctrCT01; 
	private int ctrCT02;
	private int ctrCT03;
	private BigDecimal TotalZmedfee;
	private boolean noRecordFound;
	
	/*PF Used*/
	private MerxTemppf merxTemppf = new MerxTemppf();

	private List<MerxTemppf> merxList;
	private List<Descpf> t1693List;
	private Iterator<MerxTemppf> iteratorList;
	
	/*DAO Used*/
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private Br649DAO br649DAO =  getApplicationContext().getBean("br649DAO", Br649DAO.class);
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Br649.class);	

	public Br649() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000(){
	intBatchID = 0;
	intBatchStep = 0;
	noRecordFound=false;
	merxList = new ArrayList<MerxTemppf>();
	
	if (bprdIO.systemParam01.isNumeric())
		if (bprdIO.systemParam01.toInt() > 0)
			intBatchExtractSize = bprdIO.systemParam01.toInt();
		else
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
	else
		intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();		
	
	StringBuilder merxTempTableName = new StringBuilder("");
	merxTempTableName.append("MERX");
	merxTempTableName.append(bprdIO.getSystemParam04().toString().trim().toUpperCase());
	merxTempTableName.append(String.format("%0"+ (4 - bsscIO.getScheduleNumber().toString().trim().length())+"d%s",0 , bsscIO.getScheduleNumber().toString().trim()));
	
	int extractRows = br649DAO.populateBr649Temp(intBatchExtractSize, merxTempTableName.toString());
	if (extractRows > 0) {
		performDataChunk();	
		
		if (!merxList.isEmpty()){
			initialise1010();			
			readSmartTables(bsprIO.getCompany().toString().trim());
			readT1693Desc();
		}			
	}
	else{
		LOGGER.info("No Medipay records retrieved for processing.");
		noRecordFound=true;
		return;
	}	
}

private void readSmartTables(String company){
	t1693List = descDAO.getItems(smtpfxcpy.item.toString(), "0", "T1693");
}

protected void readT1693Desc(){
	boolean itemFound = false;
	for (Descpf descItem : t1693List) {
		if (descItem.getDescitem().trim().equals(bsprIO.getCompany().toString().trim())){
			tranDesc = descItem.getLongdesc();
			itemFound=true;
			break;
		}
	}
	if (!itemFound) {
		fatalError600();	
	}	
}

protected void performDataChunk(){
	intBatchID = intBatchStep;		
	if (merxList.size() > 0) merxList.clear();
	merxList = br649DAO.loadDataByBatch(intBatchID);
	if (merxList.isEmpty()){
		printEof3500();
		wsspEdterror.set(varcom.endp);
	}else  iteratorList = merxList.iterator();		
}

protected void initialise1010(){
	wsspEdterror.set(varcom.oK);
	wsaaCount.set(ZERO);
	printerFile.openOutput();
	rh01Company.set(bsprIO.getCompany());
	rh01Companynm.set(tranDesc);
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	rh01Sdate.set(datcon1rec.extDate);
	datcon1rec.function.set(varcom.conv);
	datcon1rec.intDate.set(bsscIO.getEffectiveDate());
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	rh01Repdate.set(datcon1rec.extDate);
	
	t1693List = new ArrayList<Descpf>();	
	
	ctrCT01=0;
	ctrCT02=0;
	ctrCT03=0;
}

protected void readFile2000(){		
	TotalZmedfee=BigDecimal.ZERO; //smalchi2 for ILIFE-3203
	if (!merxList.isEmpty()){
		if (!iteratorList.hasNext()) {
			intBatchStep++;	
			performDataChunk();
			if (!merxList.isEmpty()){
				merxTemppf = new MerxTemppf();
				merxTemppf = iteratorList.next();
			}
		}else {		
			merxTemppf = new MerxTemppf();
			merxTemppf = iteratorList.next();
		}		
	}else {
		initialise1010();
		writeDetail3080();
	//	TotalZmedfee=BigDecimal.ZERO;
		printEof3500();
		wsspEdterror.set(varcom.endp);
	}
}

protected void edit2500(){
	ctrCT02++;
}

protected void commitControlTotals(){
	//ct01
	contotrec.totno.set(ct01);
	contotrec.totval.set(ctrCT01);
	callProgram(Contot.class, contotrec.contotRec);	
	ctrCT01 = 0;
	
	//ct02
	contotrec.totno.set(ct02);
	contotrec.totval.set(ctrCT02);
	callProgram(Contot.class, contotrec.contotRec);		
	ctrCT02 = 0;	
	
	//ct03
	contotrec.totno.set(ct03);
	contotrec.totval.set(ctrCT03);
	callProgram(Contot.class, contotrec.contotRec);		
	ctrCT03 = 0;
}

protected void update3000(){
	update3010();
	writeDetail3080();
}

protected void update3010(){
	wsaaCount.add(1);
	rd01Nofmbr.set(wsaaCount);
	rd01Chdrnum.set(merxTemppf.getChdrnum());
	rd01Zmedtyp.set(merxTemppf.getZmedtyp());
	rd01Exmcode.set(merxTemppf.getExmcode());
	datcon1rec.function.set(varcom.conv);
	datcon1rec.intDate.set(merxTemppf.getEffdate());
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	rd01Effdate.set(datcon1rec.extDate);
	rd01Invref.set(merxTemppf.getInvref());
	rd01Zmedfee.set(merxTemppf.getZmedfee());
	rd01Name.set(merxTemppf.getName());
	rd01Desc.set(merxTemppf.getDescT());
	if(merxTemppf.getZmedfee().toString() != null && TotalZmedfee.toString() !=null) //smalchi2 for ILIFE-3203
		TotalZmedfee = TotalZmedfee.add(merxTemppf.getZmedfee());
	ctrCT03++;
}

protected void writeDetail3080(){
	if (newPageReq.isTrue()) {
		ctrCT01++;
		printerFile.printRr649h01(rr649H01, indicArea);
		wsaaOverflow.set("N");
	}
	printerFile.printRr649d01(rr649D01, indicArea);
}

protected void printEof3500(){
	start3510();
}

protected void start3510(){
	if (newPageReq.isTrue()) {
		ctrCT01++;
		printerFile.printRr649h01(rr649H01, indicArea);
		wsaaOverflow.set("N");
	}
	rt01Zmedfee.set(TotalZmedfee);
	printerFile.printRr649t01(rr649T01, indicArea);
	if (newPageReq.isTrue()) {
		ctrCT01++;
		printerFile.printRr649h01(rr649H01, indicArea);
		wsaaOverflow.set("N");
	}
	printerFile.printRr649e01(rr649E01, indicArea);
}

protected void commit3500(){
	if (!noRecordFound){
		commitControlTotals();
	}
}

protected void rollback3600(){
	/*ROLLBACK*/
	/*EXIT*/
}

protected void close4000(){
	printerFile.close();
	if (!merxList.isEmpty()){
		t1693List.clear();
		t1693List=null;

		lsaaStatuz.set(varcom.oK);
	}
}
}
