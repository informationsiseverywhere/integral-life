package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: 
 * @version:
 * Creation Date: Mon, 30 July 2012 03:09:00
 * Description:
 * Copybook name: VPMFMTREC 
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vpmfmtrec extends ExternalData {

  	public FixedLengthStringData vpmfmtRec = new FixedLengthStringData(3066);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(vpmfmtRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(vpmfmtRec, 5);
  	public FixedLengthStringData copybook = new FixedLengthStringData(12).isAPartOf(vpmfmtRec, 9);
  	public FixedLengthStringData result = new FixedLengthStringData(115).isAPartOf(vpmfmtRec, 21);
  	public FixedLengthStringData resultField = new FixedLengthStringData(35).isAPartOf(result, 0);
  	public FixedLengthStringData resultValue = new FixedLengthStringData(80).isAPartOf(result, 35);
  	public FixedLengthStringData dataRec = new FixedLengthStringData(358).isAPartOf(vpmfmtRec, 136);
  	public FixedLengthStringData extkey = new FixedLengthStringData(30).isAPartOf(dataRec, 0);
  	public FixedLengthStringData commonArea = new FixedLengthStringData(412).isAPartOf(vpmfmtRec, 166);//344
  	public FixedLengthStringData dates = new FixedLengthStringData(64).isAPartOf(commonArea, 0);
  	public ZonedDecimalData[] date = ZDArrayPartOfStructure(8, 8, 0, dates, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(64).isAPartOf(dates, 0, FILLER_REDEFINE);
  	public ZonedDecimalData date01 = new ZonedDecimalData(8, 0).isAPartOf(filler1, 0);  
  	public ZonedDecimalData date02 = new ZonedDecimalData(8, 0).isAPartOf(filler1, 8);  
  	public ZonedDecimalData date03 = new ZonedDecimalData(8, 0).isAPartOf(filler1, 16); 
  	public ZonedDecimalData date04 = new ZonedDecimalData(8, 0).isAPartOf(filler1, 24); 
  	public ZonedDecimalData date05 = new ZonedDecimalData(8, 0).isAPartOf(filler1, 32); 
  	public ZonedDecimalData date06 = new ZonedDecimalData(8, 0).isAPartOf(filler1, 40); 
  	public ZonedDecimalData date07 = new ZonedDecimalData(8, 0).isAPartOf(filler1, 48); 
  	public ZonedDecimalData date08 = new ZonedDecimalData(8, 0).isAPartOf(filler1, 56); 
  	public FixedLengthStringData amounts = new FixedLengthStringData(204).isAPartOf(commonArea, 64);
  	public ZonedDecimalData[] amount = ZDArrayPartOfStructure(8, 17, 2, amounts, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(204).isAPartOf(amounts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData amount01 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 0);  
  	public ZonedDecimalData amount02 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 17); 
  	public ZonedDecimalData amount03 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 34); 
  	public ZonedDecimalData amount04 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 51); 
  	public ZonedDecimalData amount05 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 68); 
  	public ZonedDecimalData amount06 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 85); 
  	public ZonedDecimalData amount07 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 102);
  	public ZonedDecimalData amount08 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 119);
  	public ZonedDecimalData amount09 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 136);//ALS-4706
  	public ZonedDecimalData amount10 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 153);//ALS-4706
  	public ZonedDecimalData amount11 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 170);
  	public ZonedDecimalData amount12 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 187);
  	public FixedLengthStringData rates = new FixedLengthStringData(144).isAPartOf(commonArea, 268);
  	public ZonedDecimalData[] rate = ZDArrayPartOfStructure(8, 18, 9, rates, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(144).isAPartOf(rates, 0, FILLER_REDEFINE);
  	public ZonedDecimalData rate01 = new ZonedDecimalData(18, 9).isAPartOf(filler3, 0);  
  	public ZonedDecimalData rate02 = new ZonedDecimalData(18, 9).isAPartOf(filler3, 18); 
  	public ZonedDecimalData rate03 = new ZonedDecimalData(18, 9).isAPartOf(filler3, 36); 
  	public ZonedDecimalData rate04 = new ZonedDecimalData(18, 9).isAPartOf(filler3, 54); 
  	public ZonedDecimalData rate05 = new ZonedDecimalData(18, 9).isAPartOf(filler3, 72); 
  	public ZonedDecimalData rate06 = new ZonedDecimalData(18, 9).isAPartOf(filler3, 90); 
  	public ZonedDecimalData rate07 = new ZonedDecimalData(18, 9).isAPartOf(filler3, 108);
  	public ZonedDecimalData rate08 = new ZonedDecimalData(18, 9).isAPartOf(filler3, 126);
  	public FixedLengthStringData previousSum = new FixedLengthStringData(2).isAPartOf(vpmfmtRec, 578);
  	public FixedLengthStringData state = new FixedLengthStringData(5).isAPartOf(vpmfmtRec, 580);
  	public ZonedDecimalData riskComDate = new ZonedDecimalData(8 , 0).isAPartOf(vpmfmtRec, 585);
  	
  	public FixedLengthStringData filler = new FixedLengthStringData(2473).isAPartOf(vpmfmtRec, 593, FILLER);
  	
	public void initialize() {
		COBOLFunctions.initialize(vpmfmtRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			vpmfmtRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}