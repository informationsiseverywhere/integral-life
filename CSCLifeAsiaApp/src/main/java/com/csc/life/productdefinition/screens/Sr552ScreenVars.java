package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR552
 * @version 1.0 generated on 30/08/09 07:19
 * @author Quipoz
 */
public class Sr552ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(202);
	public FixedLengthStringData dataFields = new FixedLengthStringData(122).isAPartOf(dataArea, 0);
	public FixedLengthStringData actn = DD.actn.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData clntnaml = DD.clntnaml.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(dataFields,52);
	public FixedLengthStringData cname = DD.cname.copy().isAPartOf(dataFields,60);
	public FixedLengthStringData securityno = DD.securityno.copy().isAPartOf(dataFields,107);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 122);
	public FixedLengthStringData actnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData clntnamlErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData securitynoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 142);
	public FixedLengthStringData[] actnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] clntnamlOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] securitynoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(138);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(72).isAPartOf(subfileArea, 0);
	public FixedLengthStringData clntlname = DD.clntlname.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData idno = DD.idno.copy().isAPartOf(subfileFields,40);
	public FixedLengthStringData mlentity = DD.mlentity.copy().isAPartOf(subfileFields,55);
	public FixedLengthStringData optind = DD.optind.copy().isAPartOf(subfileFields,71);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(16).isAPartOf(subfileArea, 72);
	public FixedLengthStringData clntlnameErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData idnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData mlentityErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData optindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(48).isAPartOf(subfileArea, 88);
	public FixedLengthStringData[] clntlnameOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] idnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] mlentityOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] optindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 136);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData Sr552screensflWritten = new LongData(0);
	public LongData Sr552screenctlWritten = new LongData(0);
	public LongData Sr552screenWritten = new LongData(0);
	public LongData Sr552protectWritten = new LongData(0);
	public GeneralTable sr552screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr552screensfl;
	}

	public Sr552ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(optindOut,new String[] {"02","03","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(actnOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {optind, clntlname, idno, mlentity};
		screenSflOutFields = new BaseData[][] {optindOut, clntlnameOut, idnoOut, mlentityOut};
		screenSflErrFields = new BaseData[] {optindErr, clntlnameErr, idnoErr, mlentityErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {clntnum, cname, securityno, actn, clntnaml};
		screenOutFields = new BaseData[][] {clntnumOut, cnameOut, securitynoOut, actnOut, clntnamlOut};
		screenErrFields = new BaseData[] {clntnumErr, cnameErr, securitynoErr, actnErr, clntnamlErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr552screen.class;
		screenSflRecord = Sr552screensfl.class;
		screenCtlRecord = Sr552screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr552protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr552screenctl.lrec.pageSubfile);
	}
}
