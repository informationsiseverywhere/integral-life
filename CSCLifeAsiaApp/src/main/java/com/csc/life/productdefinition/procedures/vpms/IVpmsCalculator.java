/**
 * 
 */
package com.csc.life.productdefinition.procedures.vpms;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Map;

import javax.xml.rpc.ServiceException;

/**
 * @author sle3
 *
 */
public interface IVpmsCalculator {
	Map<String, ComputeResult> compute(Map<String,String> inputMap, Map<String,String> calcMap, Collection<String> outputs) throws MalformedURLException, ServiceException, RemoteException;;
}
