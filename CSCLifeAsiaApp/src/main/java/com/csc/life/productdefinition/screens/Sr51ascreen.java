package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr51ascreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr51aScreenVars sv = (Sr51aScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr51ascreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr51aScreenVars screenVars = (Sr51aScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.age01.setClassString("");
		screenVars.overdueMina01.setClassString("");
		screenVars.sumins01.setClassString("");
		screenVars.age02.setClassString("");
		screenVars.overdueMina02.setClassString("");
		screenVars.sumins02.setClassString("");
		screenVars.age03.setClassString("");
		screenVars.overdueMina03.setClassString("");
		screenVars.sumins03.setClassString("");
		screenVars.age04.setClassString("");
		screenVars.overdueMina04.setClassString("");
		screenVars.sumins04.setClassString("");
		screenVars.age05.setClassString("");
		screenVars.overdueMina05.setClassString("");
		screenVars.sumins05.setClassString("");
		screenVars.age06.setClassString("");
		screenVars.overdueMina06.setClassString("");
		screenVars.sumins06.setClassString("");
		screenVars.age07.setClassString("");
		screenVars.overdueMina07.setClassString("");
		screenVars.sumins07.setClassString("");
		screenVars.age08.setClassString("");
		screenVars.overdueMina08.setClassString("");
		screenVars.sumins08.setClassString("");
		screenVars.age09.setClassString("");
		screenVars.overdueMina09.setClassString("");
		screenVars.sumins09.setClassString("");
		screenVars.age10.setClassString("");
		screenVars.overdueMina10.setClassString("");
		screenVars.sumins10.setClassString("");
		screenVars.crtable01.setClassString("");
		screenVars.crtable02.setClassString("");
		screenVars.crtable03.setClassString("");
		screenVars.crtable04.setClassString("");
		screenVars.crtable05.setClassString("");
		screenVars.crtable06.setClassString("");
		screenVars.crtable07.setClassString("");
		screenVars.crtable08.setClassString("");
		screenVars.crtable09.setClassString("");
		screenVars.crtable10.setClassString("");
		screenVars.crtable11.setClassString("");
		screenVars.crtable12.setClassString("");
		screenVars.crtable13.setClassString("");
		screenVars.crtable14.setClassString("");
		screenVars.crtable15.setClassString("");
		screenVars.crtable16.setClassString("");
		screenVars.crtable17.setClassString("");
		screenVars.crtable18.setClassString("");
		screenVars.crtable19.setClassString("");
		screenVars.crtable20.setClassString("");
		screenVars.indc01.setClassString("");
		screenVars.indc02.setClassString("");
		screenVars.indc03.setClassString("");
		screenVars.indc04.setClassString("");
		screenVars.indc05.setClassString("");
		screenVars.indc06.setClassString("");
		screenVars.indc07.setClassString("");
		screenVars.indc08.setClassString("");
		screenVars.indc09.setClassString("");
		screenVars.indc10.setClassString("");
	}

/**
 * Clear all the variables in Sr51ascreen
 */
	public static void clear(VarModel pv) {
		Sr51aScreenVars screenVars = (Sr51aScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.age01.clear();
		screenVars.overdueMina01.clear();
		screenVars.sumins01.clear();
		screenVars.age02.clear();
		screenVars.overdueMina02.clear();
		screenVars.sumins02.clear();
		screenVars.age03.clear();
		screenVars.overdueMina03.clear();
		screenVars.sumins03.clear();
		screenVars.age04.clear();
		screenVars.overdueMina04.clear();
		screenVars.sumins04.clear();
		screenVars.age05.clear();
		screenVars.overdueMina05.clear();
		screenVars.sumins05.clear();
		screenVars.age06.clear();
		screenVars.overdueMina06.clear();
		screenVars.sumins06.clear();
		screenVars.age07.clear();
		screenVars.overdueMina07.clear();
		screenVars.sumins07.clear();
		screenVars.age08.clear();
		screenVars.overdueMina08.clear();
		screenVars.sumins08.clear();
		screenVars.age09.clear();
		screenVars.overdueMina09.clear();
		screenVars.sumins09.clear();
		screenVars.age10.clear();
		screenVars.overdueMina10.clear();
		screenVars.sumins10.clear();
		screenVars.crtable01.clear();
		screenVars.crtable02.clear();
		screenVars.crtable03.clear();
		screenVars.crtable04.clear();
		screenVars.crtable05.clear();
		screenVars.crtable06.clear();
		screenVars.crtable07.clear();
		screenVars.crtable08.clear();
		screenVars.crtable09.clear();
		screenVars.crtable10.clear();
		screenVars.crtable11.clear();
		screenVars.crtable12.clear();
		screenVars.crtable13.clear();
		screenVars.crtable14.clear();
		screenVars.crtable15.clear();
		screenVars.crtable16.clear();
		screenVars.crtable17.clear();
		screenVars.crtable18.clear();
		screenVars.crtable19.clear();
		screenVars.crtable20.clear();
		screenVars.indc01.clear();
		screenVars.indc02.clear();
		screenVars.indc03.clear();
		screenVars.indc04.clear();
		screenVars.indc05.clear();
		screenVars.indc06.clear();
		screenVars.indc07.clear();
		screenVars.indc08.clear();
		screenVars.indc09.clear();
		screenVars.indc10.clear();
	}
}
