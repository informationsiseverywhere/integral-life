package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class RegppfDAOImpl extends BaseDAOImpl<Regppf> implements RegppfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegppfDAOImpl.class);

    public Map<Long, List<Regppf>> searchRegpRecord(List<Long> annyUQ) {
        StringBuilder sqlRegpSelect1 = new StringBuilder(
                "SELECT RG.RGPYMOP,RG.FPAYDATE,RG.NPAYDATE,RG.LPAYDATE,RG.PYMT, AY.UNIQUE_NUMBER ");
        sqlRegpSelect1
                .append("FROM ANNYPF AY,REGPPF RG WHERE RG.CHDRCOY=AY.CHDRCOY AND RG.CHDRNUM = AY.CHDRNUM AND RG.LIFE=AY.LIFE AND RG.COVERAGE=AY.COVERAGE AND RG.RIDER=AY.RIDER AND RG.VALIDFLAG = '1' AND RG.RGPYNUM>=1 AND ");
        sqlRegpSelect1.append(getSqlInLong("AY.UNIQUE_NUMBER", annyUQ));

        sqlRegpSelect1
                .append(" ORDER BY RG.CHDRCOY ASC, RG.CHDRNUM ASC, RG.LIFE ASC, RG.COVERAGE ASC, RG.RIDER ASC, RG.RGPYNUM ASC, RG.UNIQUE_NUMBER DESC ");

        PreparedStatement psRegpSelect = getPrepareStatement(sqlRegpSelect1.toString());
        ResultSet sqlregppf1rs = null;
        Map<Long, List<Regppf>> regppfSearchResult = new HashMap<Long, List<Regppf>>();
        try {
            sqlregppf1rs = executeQuery(psRegpSelect);
            while (sqlregppf1rs.next()) {
                Regppf regppf = new Regppf();
                regppf.setRgpymop(sqlregppf1rs.getString(1));
                regppf.setFirstPaydate(sqlregppf1rs.getInt(2));
                regppf.setNextPaydate(sqlregppf1rs.getInt(3));
                regppf.setLastPaydate(sqlregppf1rs.getInt(4));
                regppf.setPymt(sqlregppf1rs.getBigDecimal(5));
                long uniqueNumber = sqlregppf1rs.getLong(6);
                if (regppfSearchResult.containsKey(uniqueNumber)) {
                    regppfSearchResult.get(uniqueNumber).add(regppf);
                } else {
                    List<Regppf> regppfList = new ArrayList<Regppf>();
                    regppfList.add(regppf);
                    regppfSearchResult.put(uniqueNumber, regppfList);
                }
            }

        } catch (SQLException e) {
			LOGGER.error("searchRegpRecord()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(psRegpSelect, sqlregppf1rs);
        }
        return regppfSearchResult;

    }
    
	 public Map<String,List<Regppf>> searchRegppfByChdrnum(List<String> chdrnumList){

	        StringBuilder sqlRegpSelect1 = new StringBuilder(
	                "SELECT CHDRCOY,CHDRNUM,PLNSFX,LIFE,COVERAGE,RIDER,RGPYNUM,VALIDFLAG,TRANNO,SACSCODE,SACSTYPE,GLACT,DEBCRED,DESTKEY,PAYCLT,RGPYMOP,REGPAYFREQ,CURRCD,PYMT,PRCNT,TOTAMNT,RGPYSTAT,PAYREASON,CLAIMEVD,BANKKEY,BANKACCKEY,CRTDATE,APRVDATE,FPAYDATE,NPAYDATE,REVDTE,LPAYDATE,EPAYDATE,ANVDATE,CANCELDATE,RGPYTYPE,CRTABLE,TERMID,TRDT,TRTM,PAYCOY,CERTDATE,CLAMPARTY,ZCLMRECD,INCURDT,UNIQUE_NUMBER,WDRGPYMOP,WDBANKKEY,WDBANKACCKEY,User_t,ADJAMT,NETAMT,INTERESTRATE,INTERESTAMT,INTERESTDAYS ");
	        sqlRegpSelect1.append("FROM REGPPF WHERE VALIDFLAG = '1' AND ");
	        sqlRegpSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
	        sqlRegpSelect1
	                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, RGPYNUM ASC, UNIQUE_NUMBER DESC ");
	        PreparedStatement psRegpSelect = getPrepareStatement(sqlRegpSelect1.toString());
	        ResultSet sqlregppf1rs = null;
	        Map<String, List<Regppf>> regpMap = new HashMap<>();
	        try {
	            sqlregppf1rs = executeQuery(psRegpSelect);
	            while (sqlregppf1rs.next()) {
	                Regppf regppf = new Regppf();
	                regppf.setChdrcoy(sqlregppf1rs.getString("Chdrcoy"));
	                regppf.setChdrnum(sqlregppf1rs.getString("Chdrnum"));
	                regppf.setPlanSuffix(sqlregppf1rs.getInt("Plnsfx"));
	                regppf.setLife(sqlregppf1rs.getString("Life"));
	                regppf.setCoverage(sqlregppf1rs.getString("Coverage"));
	                regppf.setRider(sqlregppf1rs.getString("Rider"));
	                regppf.setRgpynum(sqlregppf1rs.getInt("Rgpynum"));
	                regppf.setValidflag(sqlregppf1rs.getString("Validflag"));
	                regppf.setTranno(sqlregppf1rs.getInt("Tranno"));
	                regppf.setSacscode(sqlregppf1rs.getString("Sacscode"));
	                regppf.setSacstype(sqlregppf1rs.getString("Sacstype"));
	                regppf.setGlact(sqlregppf1rs.getString("Glact"));
	                regppf.setDebcred(sqlregppf1rs.getString("Debcred"));
	                regppf.setDestkey(sqlregppf1rs.getString("Destkey"));
	                regppf.setPayclt(sqlregppf1rs.getString("Payclt"));
	                regppf.setRgpymop(sqlregppf1rs.getString("Rgpymop"));
	                regppf.setRegpayfreq(sqlregppf1rs.getString("Regpayfreq"));
	                regppf.setCurrcd(sqlregppf1rs.getString("Currcd"));
	                regppf.setPymt(sqlregppf1rs.getBigDecimal("Pymt"));
	                regppf.setPrcnt(sqlregppf1rs.getBigDecimal("Prcnt"));
	                regppf.setTotamnt(sqlregppf1rs.getBigDecimal("Totamnt"));
	                regppf.setRgpystat(sqlregppf1rs.getString("Rgpystat"));
	                regppf.setPayreason(sqlregppf1rs.getString("Payreason"));
	                regppf.setClaimevd(sqlregppf1rs.getString("Claimevd"));
	                regppf.setBankkey(sqlregppf1rs.getString("Bankkey"));
	                regppf.setBankacckey(sqlregppf1rs.getString("Bankacckey"));
	                regppf.setCrtdate(sqlregppf1rs.getInt("Crtdate"));
	                regppf.setAprvdate(sqlregppf1rs.getInt("Aprvdate"));
	                regppf.setFirstPaydate(sqlregppf1rs.getInt("Fpaydate"));
	                regppf.setNextPaydate(sqlregppf1rs.getInt("Npaydate"));
	                regppf.setRevdte(sqlregppf1rs.getInt("Revdte"));
	                regppf.setLastPaydate(sqlregppf1rs.getInt("Lpaydate"));
	                regppf.setFinalPaydate(sqlregppf1rs.getInt("Epaydate"));
	                regppf.setAnvdate(sqlregppf1rs.getInt("Anvdate"));
	                regppf.setCancelDate(sqlregppf1rs.getInt("Canceldate"));
	                regppf.setRgpytype(sqlregppf1rs.getString("Rgpytype"));
	                regppf.setCrtable(sqlregppf1rs.getString("Crtable"));
	                regppf.setTermid(sqlregppf1rs.getString("Termid"));
	                regppf.setTransactionDate(sqlregppf1rs.getInt("Trdt"));
	                regppf.setTransactionTime(sqlregppf1rs.getInt("Trtm"));
	                regppf.setPaycoy(sqlregppf1rs.getString("Paycoy"));
	                regppf.setCertdate(sqlregppf1rs.getInt("Certdate"));
	                regppf.setClamparty(sqlregppf1rs.getString("Clamparty"));
	                regppf.setRecvdDate(sqlregppf1rs.getInt("Zclmrecd"));
	                regppf.setIncurdt(sqlregppf1rs.getInt("Incurdt"));
	                regppf.setUniqueNumber(sqlregppf1rs.getLong("UNIQUE_NUMBER"));
	                regppf.setWdrgpymop(sqlregppf1rs.getString("WDRGPYMOP"));
	                regppf.setWdbankkey(sqlregppf1rs.getString("WDBANKKEY"));
	                regppf.setWdbankacckey(sqlregppf1rs.getString("WDBANKACCKEY"));
	                regppf.setUser(sqlregppf1rs.getInt("User_t"));
	                regppf.setAdjamt(sqlregppf1rs.getBigDecimal("ADJAMT"));
	                regppf.setNetamt(sqlregppf1rs.getBigDecimal("NETAMT"));
	                regppf.setIntrate(sqlregppf1rs.getBigDecimal("INTERESTRATE"));
	                regppf.setIntamt(sqlregppf1rs.getBigDecimal("INTERESTAMT"));
	                regppf.setIntdays(sqlregppf1rs.getInt("INTERESTDAYS"));
	                
	                if (regpMap.containsKey(regppf.getChdrnum())) {
	                    regpMap.get(regppf.getChdrnum()).add(regppf);
	                } else {
	                    List<Regppf> chdrList = new ArrayList<>();
	                    chdrList.add(regppf);
	                    regpMap.put(regppf.getChdrnum(), chdrList);
	                }
	            }

	        } catch (SQLException e) {
			LOGGER.error("searchRegppfByChdrnum()", e); /* IJTI-1479 */
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(psRegpSelect, sqlregppf1rs);
	        }
	        return regpMap;
	 }       
	 
	public void updateRegpRecord(String validflag , List<Long> regpBulkOpList) {
		if (regpBulkOpList != null && !regpBulkOpList.isEmpty()) {
			StringBuilder sql = new StringBuilder("UPDATE REGPPF SET VALIDFLAG=?,JOBNM=?,USRPRF=?,DATIME=? WHERE ");
			sql.append(getSqlInLong("UNIQUE_NUMBER", regpBulkOpList));
			try(PreparedStatement psRegpUpdate = getPrepareStatement(sql.toString())) {
					psRegpUpdate.setString(1, validflag);
					psRegpUpdate.setString(2, getJobnm());
					psRegpUpdate.setString(3, getUsrprf());
					psRegpUpdate.setTimestamp(4, getDatime());
					psRegpUpdate.executeUpdate();
			} catch (SQLException e) {
				LOGGER.error("updateRegpRecord()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			}
		}
	}
	
	public void updateRegpfRecord(List<Regppf> regpBulkOpList) {
		if (regpBulkOpList != null && !regpBulkOpList.isEmpty()) {
			String SQL_UPDATE = "UPDATE REGPPF SET VALIDFLAG=?,JOBNM=?,USRPRF=?,DATIME=?,LPAYDATE=?,NPAYDATE=? WHERE UNIQUE_NUMBER=? ";

			PreparedStatement psRegpUpdate = getPrepareStatement(SQL_UPDATE);
			try {
				for (Regppf r : regpBulkOpList) {
					psRegpUpdate.setString(1, r.getValidflag());
					psRegpUpdate.setString(2, getJobnm());
					psRegpUpdate.setString(3, getUsrprf());
					psRegpUpdate.setTimestamp(4, getDatime());
					psRegpUpdate.setInt(5, r.getLastPaydate());
					psRegpUpdate.setInt(6, r.getNextPaydate());
					psRegpUpdate.setLong(7, r.getUniqueNumber());
					psRegpUpdate.addBatch();
				}
				psRegpUpdate.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("updateRegpRecord()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psRegpUpdate, null);
			}
		}
	}
	
	public void insertRegppfRecord(List<Regppf> insertRegppfList) {
		if (insertRegppfList != null && !insertRegppfList.isEmpty()) {
			StringBuilder insertSql = new StringBuilder();
			insertSql
					.append(" INSERT INTO REGPPF (CHDRCOY,CHDRNUM,PLNSFX,LIFE,COVERAGE,RIDER,RGPYNUM,VALIDFLAG,TRANNO,SACSCODE,SACSTYPE,GLACT,DEBCRED,DESTKEY,PAYCLT,RGPYMOP,REGPAYFREQ,CURRCD,PYMT,PRCNT,TOTAMNT,RGPYSTAT,PAYREASON,CLAIMEVD,BANKKEY,BANKACCKEY,CRTDATE,APRVDATE,FPAYDATE,NPAYDATE,REVDTE,LPAYDATE,EPAYDATE,ANVDATE,CANCELDATE,RGPYTYPE,CRTABLE,TERMID,TRDT,TRTM,USER_T,PAYCOY,CERTDATE,CLAMPARTY,ZCLMRECD,INCURDT,USRPRF,JOBNM,DATIME,WDRGPYMOP,WDBANKKEY,WDBANKACCKEY,ADJAMT,NETAMT,INTERESTRATE,INTERESTAMT,INTERESTDAYS)");
			insertSql
					.append(" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			PreparedStatement ps = getPrepareStatement(insertSql.toString());
			try {
				for (Regppf r : insertRegppfList) {
					ps.setString(1, r.getChdrcoy());
					ps.setString(2, r.getChdrnum());
					ps.setInt(3, r.getPlanSuffix());
					ps.setString(4, r.getLife());
					ps.setString(5, r.getCoverage());
					ps.setString(6, r.getRider());
					ps.setInt(7, r.getRgpynum());
					ps.setString(8, r.getValidflag());
					ps.setInt(9, r.getTranno());
					ps.setString(10, r.getSacscode());
					ps.setString(11, r.getSacstype());
					ps.setString(12, r.getGlact());
					ps.setString(13, r.getDebcred());
					ps.setString(14, r.getDestkey());
					ps.setString(15, r.getPayclt());
					ps.setString(16, r.getRgpymop());
					ps.setString(17, r.getRegpayfreq());
					ps.setString(18, r.getCurrcd());
					ps.setBigDecimal(19, r.getPymt());
					ps.setBigDecimal(20, r.getPrcnt());
					ps.setBigDecimal(21, r.getTotamnt());
					ps.setString(22, r.getRgpystat());
					ps.setString(23, r.getPayreason());
					ps.setString(24, r.getClaimevd());
					ps.setString(25, r.getBankkey());
					ps.setString(26, r.getBankacckey());
					ps.setInt(27, r.getCrtdate());
					ps.setInt(28, r.getAprvdate());
					ps.setInt(29, r.getFirstPaydate());
					ps.setInt(30, r.getNextPaydate());
					ps.setInt(31, r.getRevdte());
					ps.setInt(32, r.getLastPaydate());
					ps.setInt(33, r.getFinalPaydate());
					ps.setInt(34, r.getAnvdate());
					ps.setInt(35, r.getCancelDate());
					ps.setString(36, r.getRgpytype());
					ps.setString(37, r.getCrtable());
					ps.setString(38, r.getTermid());
					ps.setInt(39, r.getTransactionDate());
					ps.setInt(40, r.getTransactionTime());
					ps.setInt(41, r.getUser());
					ps.setString(42, r.getPaycoy());
					ps.setInt(43, r.getCertdate());
					ps.setString(44, r.getClamparty());
					ps.setInt(45, r.getRecvdDate());
					ps.setInt(46, r.getIncurdt());
					ps.setString(47, getUsrprf());
					ps.setString(48, getJobnm());
					ps.setTimestamp(49, getDatime());
					ps.setString(50,r.getWdrgpymop());
					ps.setString(51, r.getWdbankkey());
					ps.setString(52, r.getWdbankacckey());
					ps.setBigDecimal(53, r.getAdjamt());
					ps.setBigDecimal(54, r.getNetamt());
					ps.setBigDecimal(55, r.getIntrate());
					ps.setBigDecimal(56, r.getIntamt());
					ps.setInt(57, r.getIntdays());
					ps.addBatch();
				}
				ps.executeBatch();

			} catch (SQLException e) {
				LOGGER.error("insertRegppfRecord", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);
			}
		}
	}
	
	public void updateRegpStatRecord(List<Regppf> regpBulkOpList) {
		if (regpBulkOpList != null && !regpBulkOpList.isEmpty()) {
			String SQL_UPDATE = "UPDATE REGPPF SET RGPYSTAT=?,CERTDATE=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";

			PreparedStatement psRegpUpdate = getPrepareStatement(SQL_UPDATE);
			try {
				for (Regppf r : regpBulkOpList) {
					psRegpUpdate.setString(1, r.getRgpystat());
					psRegpUpdate.setInt(2, r.getCertdate());
					psRegpUpdate.setString(3, getJobnm());
					psRegpUpdate.setString(4, getUsrprf());
					psRegpUpdate.setTimestamp(5, getDatime());
					psRegpUpdate.setLong(6, r.getUniqueNumber()); //ILIFE-5457
					psRegpUpdate.addBatch();
				}
				psRegpUpdate.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("updateRegpStatRecord()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psRegpUpdate, null);
			}
		}
	}
	
	public List<Regppf> searchRegppfRecord(String chdrcoy, int certdate, String validflag, String tableName, int batchExtractSize, int batchID) {
		StringBuilder sqlRegpSelect = new StringBuilder();
		sqlRegpSelect.append(" SELECT * FROM ( ");
		sqlRegpSelect.append(" SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, RGPYNUM, PYMT, CURRCD, VALIDFLAG, PRCNT, RGPYTYPE, PAYREASON, RGPYSTAT, CERTDATE, REVDTE, REGPAYFREQ, CRTABLE ");
		sqlRegpSelect.append(" ,FLOOR((ROW_NUMBER()OVER( ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, RGPYNUM )-1)/?) BATCHNUM ");
		sqlRegpSelect.append(" FROM ");
		sqlRegpSelect.append(tableName);
		sqlRegpSelect.append(" WHERE CHDRCOY = ?  AND CERTDATE <= ? AND VALIDFLAG = ? ");
		sqlRegpSelect.append(" ) MAIN WHERE BATCHNUM = ? ");

		PreparedStatement psRegpSelect = getPrepareStatement(sqlRegpSelect.toString());
		ResultSet sqlregppf1rs = null;
		List<Regppf> regppfList = new ArrayList<>();
		try {
			psRegpSelect.setInt(1, batchExtractSize);
			psRegpSelect.setString(2, chdrcoy);
			psRegpSelect.setInt(3, certdate);
			psRegpSelect.setString(4, validflag);
			psRegpSelect.setInt(5, batchID);

			sqlregppf1rs = executeQuery(psRegpSelect);
			while (sqlregppf1rs.next()) {
				Regppf regppf = new Regppf();
				regppf.setChdrcoy(sqlregppf1rs.getString("CHDRCOY"));
				regppf.setChdrnum(sqlregppf1rs.getString("CHDRNUM"));
				regppf.setLife(sqlregppf1rs.getString("LIFE"));
				regppf.setCoverage(sqlregppf1rs.getString("COVERAGE"));
				regppf.setRider(sqlregppf1rs.getString("RIDER"));
				regppf.setRgpynum(sqlregppf1rs.getInt("RGPYNUM"));
				regppf.setPymt(sqlregppf1rs.getBigDecimal("Pymt"));
				regppf.setCurrcd(sqlregppf1rs.getString("Currcd"));
				regppf.setValidflag(sqlregppf1rs.getString("Validflag"));
				regppf.setPrcnt(sqlregppf1rs.getBigDecimal("Prcnt"));
				regppf.setRgpytype(sqlregppf1rs.getString("Rgpytype"));
				regppf.setPayreason(sqlregppf1rs.getString("Payreason"));
				regppf.setRgpystat(sqlregppf1rs.getString("Rgpystat"));
				regppf.setCertdate(sqlregppf1rs.getInt("Certdate"));
				regppf.setRevdte(sqlregppf1rs.getInt("Revdte"));
				regppf.setRegpayfreq(sqlregppf1rs.getString("Regpayfreq"));
				regppf.setCrtable(sqlregppf1rs.getString("Crtable"));
				regppfList.add(regppf);
			}
		} catch (SQLException e) {
			LOGGER.error("searchRegppfRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psRegpSelect, sqlregppf1rs);
		}
		return regppfList;
	}
	
	@Override
	public Map<String, List<Regppf>> searchRecords(List<String> chdrnumList) {
		StringBuilder sqlSelect1 = new StringBuilder();
		sqlSelect1.append("SELECT * ");
	    sqlSelect1.append("FROM regppf WHERE  ");
	    sqlSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
	    sqlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, UNIQUE_NUMBER DESC ");
	
	    PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
	    ResultSet sqlrs = null;
	    Map<String, List<Regppf>> resultMap = new HashMap<>();
	    
	    try {
	    	sqlrs = executeQuery(psSelect);
	        while (sqlrs.next()) {
	        	Regppf regppf = new Regppf();
				regppf.setUniqueNumber(sqlrs.getLong("Unique_number"));
				regppf.setChdrcoy(sqlrs.getString("Chdrcoy"));
				regppf.setChdrnum(sqlrs.getString("Chdrnum"));
				regppf.setLife(sqlrs.getString("LIFE"));
				regppf.setCoverage(sqlrs.getString("Coverage"));
				regppf.setRider(sqlrs.getString("Rider"));
				regppf.setRgpynum(sqlrs.getInt("Rgpynum"));
				regppf.setPymt(sqlrs.getBigDecimal("Pymt"));
				regppf.setCurrcd(sqlrs.getString("Currcd"));
				regppf.setPrcnt(sqlrs.getBigDecimal("Prcnt"));
				regppf.setPayreason(sqlrs.getString("Payreason"));
				regppf.setRevdte(sqlrs.getInt("Revdte"));
				regppf.setFirstPaydate(sqlrs.getInt("FPAYDATE"));
				regppf.setLastPaydate(sqlrs.getInt("LPAYDATE"));
				regppf.setTranno(sqlrs.getInt("Tranno"));
				regppf.setTermid(sqlrs.getString("Termid"));
				regppf.setTransactionDate(sqlrs.getInt("Trdt"));
				regppf.setTransactionTime(sqlrs.getInt("Trtm"));
				regppf.setUser(sqlrs.getInt("User_t"));
				regppf.setValidflag(sqlrs.getString("VALIDFLAG"));                                     //ILIFE-5788
				regppf.setDestkey(sqlrs.getString("DESTKEY")!=null ? sqlrs.getString("DESTKEY") : ""); //ILIFE-5788
				regppf.setNetamt(sqlrs.getBigDecimal("NETAMT"));
	            regppf.setIntrate(sqlrs.getBigDecimal("INTERESTRATE"));
	            regppf.setIntamt(sqlrs.getBigDecimal("INTERESTAMT"));
	            regppf.setIntdays(sqlrs.getInt("INTERESTDAYS"));
				
				if (resultMap.containsKey(regppf.getChdrnum())) {
					resultMap.get(regppf.getChdrnum()).add(regppf);
				} else {
					List<Regppf> regppfList = new ArrayList<>();
					regppfList.add(regppf);
					resultMap.put(regppf.getChdrnum(), regppfList);
				}
	        }
	    } catch (SQLException e) {
			LOGGER.error("searchRecords()", e); /* IJTI-1479 */
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(psSelect, sqlrs);
	    }
		return resultMap;
	}
	
	@Override
	public void updateValidflag(List<Regppf> list) {							
        String sqlStr = "UPDATE REGPPF SET VALIDFLAG=2, USER_T=?, TRDT=?, TRTM=?, JOBNM=?, USRPRF=?, DATIME=? WHERE UNIQUE_NUMBER=? ";
		PreparedStatement ps = getPrepareStatement(sqlStr);
		try {
			for (Regppf c : list) {
				ps.setInt(1, c.getUser());
				ps.setInt(2, c.getTransactionDate());
				ps.setInt(3, c.getTransactionTime());
				ps.setString(4, getJobnm());
				ps.setString(5, getUsrprf());
				ps.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
				ps.setLong(7, c.getUniqueNumber());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("updateValidflag()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
		
	}
	
	@Override
	public void insertRecords(List<Regppf> list) {
		String sqlStr =	"INSERT INTO REGPPF(CHDRCOY,CHDRNUM,PLNSFX,LIFE,COVERAGE,RIDER,RGPYNUM,VALIDFLAG,TRANNO,SACSCODE,SACSTYPE,GLACT,DEBCRED,DESTKEY,PAYCLT,RGPYMOP,REGPAYFREQ,CURRCD,PYMT,PRCNT,TOTAMNT,RGPYSTAT,PAYREASON,CLAIMEVD,BANKKEY,BANKACCKEY,CRTDATE,APRVDATE,FPAYDATE,NPAYDATE,REVDTE,LPAYDATE,EPAYDATE,ANVDATE,CANCELDATE,RGPYTYPE,CRTABLE,TERMID,TRDT,TRTM,USER_T,PAYCOY,CERTDATE,CLAMPARTY,ZCLMRECD,INCURDT,USRPRF,JOBNM,DATIME,NETAMT,INTERESTRATE,INTERESTAMT,INTERESTDAYS) SELECT CHDRCOY,CHDRNUM,PLNSFX,LIFE,COVERAGE,RIDER,RGPYNUM,?,?,SACSCODE,SACSTYPE,GLACT,DEBCRED,DESTKEY,PAYCLT,RGPYMOP,REGPAYFREQ,CURRCD,PYMT,PRCNT,TOTAMNT,?,PAYREASON,CLAIMEVD,BANKKEY,BANKACCKEY,CRTDATE,?,FPAYDATE,NPAYDATE,REVDTE,LPAYDATE,EPAYDATE,ANVDATE,CANCELDATE,RGPYTYPE,CRTABLE,TERMID,TRDT,TRTM,USER_T,PAYCOY,CERTDATE,CLAMPARTY,ZCLMRECD,INCURDT,?,?,?,NETAMT,INTERESTRATE,INTERESTAMT,INTERESTDAYS FROM REGPPF WHERE UNIQUE_NUMBER=?";
        PreparedStatement ps = getPrepareStatement(sqlStr);
        try {
            for (Regppf c : list) {
                ps.setString(1, c.getValidflag());
                ps.setInt(2, c.getTranno());
                ps.setString(3, c.getRgpystat());
                ps.setInt(4, c.getAprvdate());
                // JOBNM,USRPRF,DATIME
				ps.setString(5, getUsrprf());
                ps.setString(6, getJobnm());
                ps.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
				ps.setLong(8, c.getUniqueNumber());
			    ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
			LOGGER.error("insertRecords()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
	}
	
	
	 public List<Regppf> readRegpfRecord(String chdrcoy,String chdrnum,String life,String coverage,String rider, String sacscode, String sacstype){
			PreparedStatement psRegpSelect = null;
			ResultSet sqlregppf1rs = null;
			StringBuilder sqlRegpSelectBuilder = new StringBuilder();
			List<Regppf> list = new ArrayList<Regppf>();
			sqlRegpSelectBuilder .append("SELECT * FROM REGPPF ");
			sqlRegpSelectBuilder .append(" WHERE CHDRCOY = ? AND CHDRNUM = ? and life=? and coverage=? and rider=? and sacscode=? and sacstype=? and validflag = '1' order by rgpynum");
			psRegpSelect = getPrepareStatement(sqlRegpSelectBuilder.toString());
			
			try {
				psRegpSelect.setString(1, chdrcoy);
				psRegpSelect.setString(2, chdrnum);
				psRegpSelect.setString(3, life);
				psRegpSelect.setString(4, coverage);
				psRegpSelect.setString(5, rider);
				psRegpSelect.setString(6, sacscode);
				psRegpSelect.setString(7, sacstype);
				sqlregppf1rs = executeQuery(psRegpSelect);
				while (sqlregppf1rs.next()) {
					Regppf regppf = new Regppf();
					regppf.setUniqueNumber(sqlregppf1rs.getLong("Unique_number"));
					regppf.setChdrcoy(sqlregppf1rs.getString("Chdrcoy"));
					regppf.setChdrnum(sqlregppf1rs.getString("Chdrnum"));
					regppf.setPlanSuffix(sqlregppf1rs.getInt("PLNSFX"));
					regppf.setLife(sqlregppf1rs.getString("LIFE"));
					regppf.setCoverage(sqlregppf1rs.getString("Coverage"));
					regppf.setRider(sqlregppf1rs.getString("Rider"));
					regppf.setRgpynum(sqlregppf1rs.getInt("Rgpynum"));
					regppf.setValidflag(sqlregppf1rs.getString("VALIDFLAG"));
					regppf.setSacscode(sqlregppf1rs.getString("SACSCODE"));
					regppf.setSacstype(sqlregppf1rs.getString("SACSTYPE"));
	                regppf.setGlact(sqlregppf1rs.getString("GLACT"));
	                regppf.setDebcred(sqlregppf1rs.getString("DEBCRED"));
	                regppf.setDestkey(sqlregppf1rs.getString("DESTKEY"));
	                regppf.setPayclt(sqlregppf1rs.getString("PAYCLT"));
	                regppf.setRgpymop(sqlregppf1rs.getString("RGPYMOP"));
	                regppf.setRegpayfreq(sqlregppf1rs.getString("REGPAYFREQ"));
					regppf.setPymt(sqlregppf1rs.getBigDecimal("Pymt"));
					regppf.setCurrcd(sqlregppf1rs.getString("Currcd"));
					regppf.setPrcnt(sqlregppf1rs.getBigDecimal("Prcnt"));
					regppf.setTotamnt(sqlregppf1rs.getBigDecimal("TOTAMNT"));
					regppf.setPayreason(sqlregppf1rs.getString("Payreason"));
					regppf.setClaimevd(sqlregppf1rs.getString("CLAIMEVD"));
	                regppf.setBankkey(sqlregppf1rs.getString("BANKKEY"));
	                regppf.setBankacckey(sqlregppf1rs.getString("BANKACCKEY"));
	                regppf.setCrtdate(sqlregppf1rs.getInt("CRTDATE"));
	                regppf.setAprvdate(sqlregppf1rs.getInt("APRVDATE"));
	                regppf.setNextPaydate(sqlregppf1rs.getInt("NPAYDATE"));	
					regppf.setRevdte(sqlregppf1rs.getInt("Revdte"));
					regppf.setFirstPaydate(sqlregppf1rs.getInt("FPAYDATE"));
					regppf.setLastPaydate(sqlregppf1rs.getInt("LPAYDATE"));
					regppf.setAnvdate(sqlregppf1rs.getInt("ANVDATE"));
	                regppf.setCancelDate(sqlregppf1rs.getInt("CANCELDATE"));
	                regppf.setCrtable(sqlregppf1rs.getString("CRTABLE"));
					regppf.setTranno(sqlregppf1rs.getInt("Tranno"));
					regppf.setTermid(sqlregppf1rs.getString("Termid"));
					regppf.setTransactionDate(sqlregppf1rs.getInt("Trdt"));
					regppf.setTransactionTime(sqlregppf1rs.getInt("Trtm"));
					regppf.setPaycoy(sqlregppf1rs.getString("PAYCOY"));
	                regppf.setCertdate(sqlregppf1rs.getInt("Certdate"));
	                regppf.setClamparty(sqlregppf1rs.getString("Clamparty"));
	                regppf.setRecvdDate(sqlregppf1rs.getInt("Zclmrecd"));
	                regppf.setIncurdt(sqlregppf1rs.getInt("INCURDT"));
					regppf.setRgpytype(sqlregppf1rs.getString("RGPYTYPE"));
					regppf.setFinalPaydate(sqlregppf1rs.getInt("Epaydate"));
					regppf.setRgpystat(sqlregppf1rs.getString("RGPYSTAT"));
					regppf.setUser(sqlregppf1rs.getInt("User_t"));
				    regppf.setWdrgpymop(sqlregppf1rs.getString("WDRGPYMOP")==null?"":sqlregppf1rs.getString("WDRGPYMOP"));
		            regppf.setWdbankkey(sqlregppf1rs.getString("WDBANKKEY"));
		            regppf.setWdbankacckey(sqlregppf1rs.getString("WDBANKACCKEY"));
					list.add(regppf);
				}

			} catch (SQLException e) {
				LOGGER.error("readRegpfRecord()", e); 
				throw new SQLRuntimeException(e);
			} finally {
				close(psRegpSelect, sqlregppf1rs);
			}
			return list;

		}
	
	
	
	
	public List<Regppf> readRecord(String chdrcoy,String chdrnum,String life,String coverage,String rider){
		PreparedStatement psRegpSelect = null;
		ResultSet sqlregppf1rs = null;
		StringBuilder sqlRegpSelectBuilder = new StringBuilder();
		List<Regppf> list = new ArrayList<Regppf>();
		sqlRegpSelectBuilder .append("SELECT * FROM REGPPF ");
		sqlRegpSelectBuilder .append(" WHERE CHDRCOY = ? AND CHDRNUM = ? and life=? and coverage=? and rider=? and validflag = '1' order by rgpynum");
		psRegpSelect = getPrepareStatement(sqlRegpSelectBuilder.toString());
		
		try {
			psRegpSelect.setString(1, chdrcoy);
			psRegpSelect.setString(2, chdrnum);
			psRegpSelect.setString(3, life);
			psRegpSelect.setString(4, coverage);
			psRegpSelect.setString(5, rider);
			sqlregppf1rs = executeQuery(psRegpSelect);
			while (sqlregppf1rs.next()) {
				Regppf regppf = new Regppf();
				regppf.setUniqueNumber(sqlregppf1rs.getLong("Unique_number"));
				regppf.setChdrcoy(sqlregppf1rs.getString("Chdrcoy"));
				regppf.setChdrnum(sqlregppf1rs.getString("Chdrnum"));
				regppf.setPlanSuffix(sqlregppf1rs.getInt("PLNSFX"));
				regppf.setLife(sqlregppf1rs.getString("LIFE"));
				regppf.setCoverage(sqlregppf1rs.getString("Coverage"));
				regppf.setRider(sqlregppf1rs.getString("Rider"));
				regppf.setRgpynum(sqlregppf1rs.getInt("Rgpynum"));
				regppf.setValidflag(sqlregppf1rs.getString("VALIDFLAG"));
				regppf.setSacscode(sqlregppf1rs.getString("SACSCODE"));
				regppf.setSacstype(sqlregppf1rs.getString("SACSTYPE"));
                regppf.setGlact(sqlregppf1rs.getString("GLACT"));
                regppf.setDebcred(sqlregppf1rs.getString("DEBCRED"));
                regppf.setDestkey(sqlregppf1rs.getString("DESTKEY"));
                regppf.setPayclt(sqlregppf1rs.getString("PAYCLT"));
                regppf.setRgpymop(sqlregppf1rs.getString("RGPYMOP"));
                regppf.setRegpayfreq(sqlregppf1rs.getString("REGPAYFREQ"));
				regppf.setPymt(sqlregppf1rs.getBigDecimal("Pymt"));
				regppf.setCurrcd(sqlregppf1rs.getString("Currcd"));
				regppf.setPrcnt(sqlregppf1rs.getBigDecimal("Prcnt"));
				regppf.setTotamnt(sqlregppf1rs.getBigDecimal("TOTAMNT"));
				regppf.setPayreason(sqlregppf1rs.getString("Payreason"));
				regppf.setClaimevd(sqlregppf1rs.getString("CLAIMEVD"));
                regppf.setBankkey(sqlregppf1rs.getString("BANKKEY"));
                regppf.setBankacckey(sqlregppf1rs.getString("BANKACCKEY"));
                regppf.setCrtdate(sqlregppf1rs.getInt("CRTDATE"));
                regppf.setAprvdate(sqlregppf1rs.getInt("APRVDATE"));
                regppf.setNextPaydate(sqlregppf1rs.getInt("NPAYDATE"));	
				regppf.setRevdte(sqlregppf1rs.getInt("Revdte"));
				regppf.setFirstPaydate(sqlregppf1rs.getInt("FPAYDATE"));
				regppf.setLastPaydate(sqlregppf1rs.getInt("LPAYDATE"));
				regppf.setAnvdate(sqlregppf1rs.getInt("ANVDATE"));
                regppf.setCancelDate(sqlregppf1rs.getInt("CANCELDATE"));
                regppf.setCrtable(sqlregppf1rs.getString("CRTABLE"));
				regppf.setTranno(sqlregppf1rs.getInt("Tranno"));
				regppf.setTermid(sqlregppf1rs.getString("Termid"));
				regppf.setTransactionDate(sqlregppf1rs.getInt("Trdt"));
				regppf.setTransactionTime(sqlregppf1rs.getInt("Trtm"));
				regppf.setPaycoy(sqlregppf1rs.getString("PAYCOY"));
                regppf.setCertdate(sqlregppf1rs.getInt("Certdate"));
                regppf.setClamparty(sqlregppf1rs.getString("Clamparty"));
                regppf.setRecvdDate(sqlregppf1rs.getInt("Zclmrecd"));
                regppf.setIncurdt(sqlregppf1rs.getInt("INCURDT"));
				regppf.setRgpytype(sqlregppf1rs.getString("RGPYTYPE"));
				regppf.setFinalPaydate(sqlregppf1rs.getInt("Epaydate"));
				regppf.setRgpystat(sqlregppf1rs.getString("RGPYSTAT"));
				regppf.setUser(sqlregppf1rs.getInt("User_t"));
			    regppf.setWdrgpymop(sqlregppf1rs.getString("WDRGPYMOP")==null?"":sqlregppf1rs.getString("WDRGPYMOP"));
	            regppf.setWdbankkey(sqlregppf1rs.getString("WDBANKKEY"));
	            regppf.setWdbankacckey(sqlregppf1rs.getString("WDBANKACCKEY"));
				list.add(regppf);
			}

		} catch (SQLException e) {
			LOGGER.error("readRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psRegpSelect, sqlregppf1rs);
		}
		return list;

	}
	public List<Regppf> readRegpRecord(String chdrcoy,String chdrnum){
		PreparedStatement psRegpSelect = null;
		ResultSet sqlregppf1rs = null;
		StringBuilder sqlRegpSelectBuilder = new StringBuilder();
		List<Regppf> list = new ArrayList<Regppf>();
		sqlRegpSelectBuilder .append("SELECT * FROM REGPPF ");
		sqlRegpSelectBuilder .append(" WHERE CHDRCOY = ? AND CHDRNUM = ?");
		psRegpSelect = getPrepareStatement(sqlRegpSelectBuilder.toString());
		
		try {
			psRegpSelect.setString(1, chdrcoy);
			psRegpSelect.setString(2, chdrnum);
			sqlregppf1rs = executeQuery(psRegpSelect);
			while (sqlregppf1rs.next()) {
				Regppf regppf = new Regppf();
				regppf.setUniqueNumber(sqlregppf1rs.getLong("Unique_number"));
				regppf.setChdrcoy(sqlregppf1rs.getString("Chdrcoy"));
				regppf.setChdrnum(sqlregppf1rs.getString("Chdrnum"));
				regppf.setPlanSuffix(sqlregppf1rs.getInt("PLNSFX"));
				regppf.setLife(sqlregppf1rs.getString("LIFE"));
				regppf.setCoverage(sqlregppf1rs.getString("Coverage"));
				regppf.setRider(sqlregppf1rs.getString("Rider"));
				regppf.setRgpynum(sqlregppf1rs.getInt("Rgpynum"));
				regppf.setPymt(sqlregppf1rs.getBigDecimal("Pymt"));
				regppf.setCurrcd(sqlregppf1rs.getString("Currcd"));
				regppf.setPrcnt(sqlregppf1rs.getBigDecimal("Prcnt"));
				regppf.setPayreason(sqlregppf1rs.getString("Payreason"));
				regppf.setRevdte(sqlregppf1rs.getInt("Revdte"));
				regppf.setFirstPaydate(sqlregppf1rs.getInt("FPAYDATE"));
				regppf.setLastPaydate(sqlregppf1rs.getInt("LPAYDATE"));
				regppf.setTranno(sqlregppf1rs.getInt("Tranno"));
				regppf.setTermid(sqlregppf1rs.getString("Termid"));
				regppf.setTransactionDate(sqlregppf1rs.getInt("Trdt"));
				regppf.setTransactionTime(sqlregppf1rs.getInt("Trtm"));
				regppf.setRgpytype(sqlregppf1rs.getString("RGPYTYPE"));
				regppf.setFinalPaydate(sqlregppf1rs.getInt("Epaydate"));
				regppf.setRgpystat(sqlregppf1rs.getString("RGPYSTAT"));
				regppf.setUser(sqlregppf1rs.getInt("User_t"));
				list.add(regppf);
			}

		} catch (SQLException e) {
			LOGGER.error("readRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psRegpSelect, sqlregppf1rs);
		}
		return list;

	}
	
	public Regppf getRegpRecord(Regppf regppf1) {
        String query = "SELECT * FROM REGPPF WHERE CHDRNUM=? and VALIDFLAG =1";

        PreparedStatement stmt = null;
        ResultSet rs = null;
        Regppf regppf = null ;
        try {
               stmt = getPrepareStatement(query);
               stmt.setString(1, regppf1.getChdrnum());
               rs = stmt.executeQuery();
               while (rs.next()) {
                     regppf = new Regppf();
                     regppf.setChdrcoy(rs.getString("CHDRCOY"));
                     regppf.setChdrnum(rs.getString("CHDRNUM"));
                     regppf.setLife(rs.getString("LIFE"));
                     regppf.setCoverage(rs.getString("COVERAGE"));
                     regppf.setRider(rs.getString("RIDER"));
                     //regppf.setSeqnbr(rs.getInt("SEQNBR"));
                     regppf.setRgpynum(rs.getInt("RGPYNUM"));
                     regppf.setSacscode(rs.getString("SACSCODE"));
                     regppf.setSacstype(rs.getString("SACSTYPE"));
                     regppf.setGlact(rs.getString("GLACT"));
                     regppf.setDebcred(rs.getString("DEBCRED"));
                     regppf.setDestkey(rs.getString("DESTKEY"));
                     regppf.setPayclt(rs.getString("PAYCLT"));
                     regppf.setRgpymop(rs.getString("RGPYMOP"));
                     regppf.setRegpayfreq(rs.getString("REGPAYFREQ"));
                     regppf.setCurrcd(rs.getString("CURRCD"));
                     regppf.setPymt(rs.getBigDecimal("PYMT"));
                     regppf.setTotamnt(rs.getBigDecimal("TOTAMNT"));
                     regppf.setRgpystat(rs.getString("RGPYSTAT"));
                     regppf.setPayreason(rs.getString("PAYREASON"));
                     regppf.setClaimevd(rs.getString("CLAIMEVD"));
                     regppf.setBankkey(rs.getString("BANKKEY"));
                     regppf.setBankacckey(rs.getString("BANKACCKEY"));
                     regppf.setCrtdate(rs.getInt("CRTDATE"));
                     regppf.setAprvdate(rs.getInt("APRVDATE"));
                     regppf.setFirstPaydate(rs.getInt("FPAYDATE"));
                     regppf.setNextPaydate(rs.getInt("NPAYDATE"));
                     regppf.setRevdte(rs.getInt("REVDTE"));
                     regppf.setLastPaydate(rs.getInt("LPAYDATE"));
                     regppf.setFinalPaydate(rs.getInt("EPAYDATE"));
                     regppf.setAnvdate(rs.getInt("ANVDATE"));
                     regppf.setCancelDate(rs.getInt("CANCELDATE"));
                     regppf.setRgpytype(rs.getString("RGPYTYPE"));
                     regppf.setCrtable(rs.getString("CRTABLE"));
                     regppf.setTermid(rs.getString("Termid"));
                     regppf.setTransactionDate(rs.getInt("Trdt"));
     				 regppf.setTransactionTime(rs.getInt("Trtm"));
                     regppf.setPaycoy(rs.getString("PAYCOY"));
                     regppf.setCertdate(rs.getInt("Certdate"));
                     regppf.setClaimevd(rs.getString("ZCLMRECD"));
                     regppf.setIncurdt(rs.getInt("INCURDT"));
                     regppf.setUserProfile(rs.getString("USRPRF"));
                     regppf.setJobName(rs.getString("JOBNM"));
                     regppf.setDatime(rs.getString("DATIME"));
                     regppf.setPrcnt(rs.getBigDecimal("PRCNT"));
                     regppf.setPlanSuffix(rs.getInt("PLNSFX"));
                     regppf.setUniqueNumber(rs.getInt("UNIQUE_NUMBER"));
                     regppf.setTransactionDate(rs.getInt("TRDT"));
                     regppf.setTransactionTime(rs.getInt("TRTM"));
                     regppf.setWdrgpymop(rs.getString("WDRGPYMOP")==null?"":rs.getString("WDRGPYMOP"));
 	                 regppf.setWdbankkey(rs.getString("WDBANKKEY"));
 	                 regppf.setWdbankacckey(rs.getString("WDBANKACCKEY"));
					 regppf.setTranno(rs.getInt("TRANNO"));
					 
                     //regppf.setPayoutopt(rs.getString("PAYOUTOPT")); 
               }
        } catch (SQLException e) {
			LOGGER.error("getRegpRecord()", e); /* IJTI-1479 */
               throw new SQLRuntimeException(e);
        } finally {
               close(stmt, rs);
        }
        return regppf;
 }
	
	public void updateRegpWDDet(List<Regppf> regpBulkOpList) {
		if (regpBulkOpList != null && !regpBulkOpList.isEmpty()) {
			String SQL_UPDATE = "UPDATE REGPPF SET TRANNO=?,JOBNM=?,USRPRF=?,DATIME=?,WDRGPYMOP=?,WDBANKKEY=?,WDBANKACCKEY=? WHERE UNIQUE_NUMBER=? ";

			PreparedStatement psRegpUpdate = getPrepareStatement(SQL_UPDATE);
			try {
				for (Regppf r : regpBulkOpList) {
					psRegpUpdate.setInt(1, r.getTranno());
					psRegpUpdate.setString(2, getJobnm());
					psRegpUpdate.setString(3, getUsrprf());
					psRegpUpdate.setTimestamp(4, getDatime());
					psRegpUpdate.setString(5, r.getWdrgpymop());
					psRegpUpdate.setString(6, r.getWdbankkey());
					psRegpUpdate.setString(7, r.getWdbankacckey());
					psRegpUpdate.setLong(8, r.getUniqueNumber());
					psRegpUpdate.addBatch();
				}
				psRegpUpdate.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("updateRegpWDDet()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psRegpUpdate, null);
			}
		}
	}
	public boolean updateRegpValidFlag(Regppf regppf) {
		
		String SQL_UPDATE = "UPDATE REGPPF SET VALIDFLAG=? WHERE UNIQUE_NUMBER=? ";

		PreparedStatement psRegpUpdate = getPrepareStatement(SQL_UPDATE);
		try {
				psRegpUpdate.setString(1, regppf.getValidflag());
				psRegpUpdate.setLong(2, regppf.getUniqueNumber());
				psRegpUpdate.executeUpdate();
			

		} catch (SQLException e) {
			LOGGER.error("updateRegpValidFlag()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psRegpUpdate, null);
		}
		return true;
	}
public List<Regppf> readRegpByRgpytype(String chdrnum,String crtable){
		PreparedStatement psRegpSelect = null;
		ResultSet sqlregppf1rs = null;
		StringBuilder sqlRegpSelectBuilder = new StringBuilder();
		List<Regppf> list = new ArrayList<Regppf>();
		sqlRegpSelectBuilder .append("SELECT * FROM REGP ");
		sqlRegpSelectBuilder .append(" WHERE CHDRNUM = ? AND CRTABLE = ?");
		psRegpSelect = getPrepareStatement(sqlRegpSelectBuilder.toString());
		
		try {
			psRegpSelect.setString(1, chdrnum);
			psRegpSelect.setString(2, crtable);
			sqlregppf1rs = executeQuery(psRegpSelect);
			while (sqlregppf1rs.next()) {
				Regppf regppf = new Regppf();
				regppf.setUniqueNumber(sqlregppf1rs.getLong("Unique_number"));
				regppf.setChdrcoy(sqlregppf1rs.getString("Chdrcoy"));
				regppf.setChdrnum(sqlregppf1rs.getString("Chdrnum"));
				regppf.setPlanSuffix(sqlregppf1rs.getInt("PLNSFX"));
				regppf.setLife(sqlregppf1rs.getString("LIFE"));
				regppf.setCoverage(sqlregppf1rs.getString("Coverage"));
				regppf.setRider(sqlregppf1rs.getString("Rider"));
				regppf.setRgpynum(sqlregppf1rs.getInt("Rgpynum"));
				regppf.setPymt(sqlregppf1rs.getBigDecimal("Pymt"));
				regppf.setCurrcd(sqlregppf1rs.getString("Currcd"));
				regppf.setPrcnt(sqlregppf1rs.getBigDecimal("Prcnt"));
				regppf.setPayreason(sqlregppf1rs.getString("Payreason"));
				regppf.setRevdte(sqlregppf1rs.getInt("Revdte"));
				regppf.setFirstPaydate(sqlregppf1rs.getInt("FPAYDATE"));
				regppf.setLastPaydate(sqlregppf1rs.getInt("LPAYDATE"));
				regppf.setTranno(sqlregppf1rs.getInt("Tranno"));
				regppf.setTermid(sqlregppf1rs.getString("Termid"));
				regppf.setTransactionDate(sqlregppf1rs.getInt("Trdt"));
				regppf.setTransactionTime(sqlregppf1rs.getInt("Trtm"));
				regppf.setRgpytype(sqlregppf1rs.getString("RGPYTYPE"));
				regppf.setFinalPaydate(sqlregppf1rs.getInt("Epaydate"));
				regppf.setRgpystat(sqlregppf1rs.getString("RGPYSTAT"));
				regppf.setRegpayfreq(sqlregppf1rs.getString("REGPAYFREQ"));
				regppf.setUser(sqlregppf1rs.getInt("User_t"));
				regppf.setAdjamt(sqlregppf1rs.getBigDecimal("ADJAMT") != null ? sqlregppf1rs.getBigDecimal("ADJAMT") :BigDecimal.ZERO);
                regppf.setNetamt(sqlregppf1rs.getBigDecimal("NETAMT") != null ? sqlregppf1rs.getBigDecimal("NETAMT") :BigDecimal.ZERO);
                regppf.setValidflag(sqlregppf1rs.getString("VALIDFLAG"));                
				list.add(regppf);
			}

		} catch (SQLException e) {
			LOGGER.error("readRegpByRgpytype()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psRegpSelect, sqlregppf1rs);
		}
		return list;

	}


	
	@Override
	public void updatePymtPrcnt(List<Regppf> list) {							
        String sqlStr = "UPDATE REGPPF SET PYMT=?, PRCNT=?, USER_T=?, TRDT=?, TRTM=?, JOBNM=?, USRPRF=?, DATIME=?, ADJAMT=?,NETAMT=? WHERE UNIQUE_NUMBER=? ";
		PreparedStatement ps = getPrepareStatement(sqlStr);
		try {
			for (Regppf c : list) {
				ps.setBigDecimal(1, c.getPymt());
				ps.setBigDecimal(2, c.getPrcnt());
				ps.setInt(3, c.getUser());
				ps.setInt(4, c.getTransactionDate());
				ps.setInt(5, c.getTransactionTime());
				ps.setString(6, getJobnm());
				ps.setString(7, getUsrprf());
				ps.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
				ps.setBigDecimal(9, c.getAdjamt());
				ps.setBigDecimal(10, c.getNetamt());
				ps.setLong(11, c.getUniqueNumber());
				
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("updatePymtPrcnt()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
		
	}

	/*ILIFE-8299 : Starts*/
	@Override
	public List<Regppf> getReasonByKey(String chdrnum, String life, String coverage, String rider, String crtable) {
		String sqlStr = "SELECT * FROM REGPPF WHERE CHDRNUM = ? and LIFE = ? and COVERAGE = ? and RIDER = ? and CRTABLE = ? and VALIDFLAG = 1";
		PreparedStatement ps = getPrepareStatement(sqlStr);
		ResultSet rs = null;
		List<Regppf> listRegppf = new ArrayList<>();
		try {
			ps.setString(1, chdrnum);
			ps.setString(2, life);
			ps.setString(3, coverage);
			ps.setString(4, rider);
			ps.setString(5, crtable);
			
			rs = ps.executeQuery();

			while (rs.next()) {
				Regppf regppf = new Regppf();
				regppf.setPayreason(rs.getString("PAYREASON"));
				listRegppf.add(regppf);
			}
		} catch (SQLException e) {
			LOGGER.error("getReasonByKey()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return listRegppf;
	}
	/*ILIFE-8299 : Ends*/
	
	
	@Override
	public void deleteRegppfRecord(String chdrcoy,String chdrnum,String life,String coverage,String rider, String sacscode, String sacstype) {
		StringBuilder sb = new StringBuilder("");
		sb.append("DELETE FROM REGPPF ");
		sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? and life=? and coverage=? and rider=? and sacscode=? and sacstype=? and validflag = '1'");		
		PreparedStatement psRegpSelect = getPrepareStatement(sb.toString());		
		try {
			psRegpSelect.setString(1, chdrcoy);
			psRegpSelect.setString(2, chdrnum);
			psRegpSelect.setString(3, life);
			psRegpSelect.setString(4, coverage);
			psRegpSelect.setString(5, rider);
			psRegpSelect.setString(6, sacscode);
			psRegpSelect.setString(7, sacstype);
			psRegpSelect.executeUpdate();			
		} catch (SQLException e) {
			LOGGER.error("deleteRegppfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psRegpSelect, null);
		}
	}
	
	
	public void insertRegppfRecord(Regppf regppf) {
			StringBuilder insertSql = new StringBuilder();
			insertSql
					.append(" INSERT INTO REGPPF (CHDRCOY,CHDRNUM,PLNSFX,LIFE,COVERAGE,RIDER,RGPYNUM,VALIDFLAG,TRANNO,SACSCODE,SACSTYPE,GLACT,DEBCRED,DESTKEY,PAYCLT,RGPYMOP,REGPAYFREQ,CURRCD,PYMT,PRCNT,TOTAMNT,RGPYSTAT,PAYREASON,CLAIMEVD,BANKKEY,BANKACCKEY,CRTDATE,APRVDATE,FPAYDATE,NPAYDATE,REVDTE,LPAYDATE,EPAYDATE,ANVDATE,CANCELDATE,RGPYTYPE,CRTABLE,TERMID,TRDT,TRTM,USER_T,PAYCOY,CERTDATE,CLAMPARTY,ZCLMRECD,INCURDT,USRPRF,JOBNM,DATIME,WDRGPYMOP,WDBANKKEY,WDBANKACCKEY)");
			insertSql
					.append(" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			PreparedStatement ps = getPrepareStatement(insertSql.toString());
			try {
					int i=1;
					ps.setString(i++, regppf.getChdrcoy());
					ps.setString(i++, regppf.getChdrnum());
					ps.setInt(i++, regppf.getPlanSuffix());
					ps.setString(i++, regppf.getLife());
					ps.setString(i++, regppf.getCoverage());
					ps.setString(i++, regppf.getRider());
					ps.setInt(i++, regppf.getRgpynum());
					ps.setString(i++, regppf.getValidflag());
					ps.setInt(i++, regppf.getTranno());
					ps.setString(i++, regppf.getSacscode());
					ps.setString(i++, regppf.getSacstype());
					ps.setString(i++, regppf.getGlact());
					ps.setString(i++, regppf.getDebcred());
					ps.setString(i++, regppf.getDestkey());
					ps.setString(i++, regppf.getPayclt());
					ps.setString(i++, regppf.getRgpymop());
					ps.setString(i++, regppf.getRegpayfreq());
					ps.setString(i++, regppf.getCurrcd());
					ps.setBigDecimal(i++, regppf.getPymt());
					ps.setBigDecimal(i++, regppf.getPrcnt());
					ps.setBigDecimal(i++, regppf.getTotamnt());
					ps.setString(i++, regppf.getRgpystat());
					ps.setString(i++, regppf.getPayreason());
					ps.setString(i++, regppf.getClaimevd());
					ps.setString(i++, regppf.getBankkey());
					ps.setString(i++, regppf.getBankacckey());
					ps.setInt(i++, regppf.getCrtdate());
					ps.setInt(i++, regppf.getAprvdate());
					ps.setInt(i++, regppf.getFirstPaydate());
					ps.setInt(i++, regppf.getNextPaydate());
					ps.setInt(i++, regppf.getRevdte());
					ps.setInt(i++, regppf.getLastPaydate());
					ps.setInt(i++, regppf.getFinalPaydate());
					ps.setInt(i++, regppf.getAnvdate());
					ps.setInt(i++, regppf.getCancelDate());
					ps.setString(i++, regppf.getRgpytype());
					ps.setString(i++, regppf.getCrtable());
					ps.setString(i++, regppf.getTermid());
					ps.setInt(i++, regppf.getTransactionDate());
					ps.setInt(i++, regppf.getTransactionTime());
					ps.setInt(i++, regppf.getUser());
					ps.setString(i++, regppf.getPaycoy());
					ps.setInt(i++, regppf.getCertdate());
					ps.setString(i++, regppf.getClamparty());
					ps.setInt(i++, regppf.getRecvdDate());
					ps.setInt(i++, regppf.getIncurdt());
					ps.setString(i++, this.getUsrprf());
					ps.setString(i++, this.getJobnm());
					ps.setTimestamp(i++,  new Timestamp(System.currentTimeMillis()));
					ps.setString(i++, regppf.getWdrgpymop());
					ps.setString(i++, regppf.getWdbankkey());
					ps.setString(i++, regppf.getWdbankacckey());
				
					ps.executeUpdate();

			} catch (SQLException e) {
				LOGGER.error("insertRegppfRecord", e); 
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);
			}
		}
	
	public void updateNextDate(Regppf regppf){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE REGPPF SET NPAYDATE=? ");
		sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? AND FPAYDATE>? ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	

				ps.setInt(1, regppf.getNextPaydate());			    
			    ps.setString(2, regppf.getChdrcoy());
			    ps.setString(3, regppf.getChdrnum());
			    ps.setInt(4, regppf.getFirstPaydate());
			    
			    ps.executeUpdate();				
				
			
		}catch (SQLException e) {
			LOGGER.error("updateNextDate()",e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
		
	}
	
	public void updatePayee(Regppf regppf){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE REGPPF SET PAYCLT=?, BANKACCKEY=?, BANKKEY=? ");
		sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ?  AND VALIDFLAG='1' ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	

				ps.setString(1, regppf.getPayclt());
				ps.setString(2, regppf.getBankacckey());
				ps.setString(3, regppf.getBankkey());
			    ps.setString(4, regppf.getChdrcoy());
			    ps.setString(5, regppf.getChdrnum());
			    
			    ps.executeUpdate();				
				
			
		}catch (SQLException e) {
			LOGGER.error("updatePayee()",e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
		
	}
	
	public void updateNDate(Regppf regppf){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE REGPPF SET NPAYDATE=?, PYMT=? ");
		sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? AND RGPYNUM=? ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	

				ps.setInt(1, regppf.getNextPaydate());	
				ps.setBigDecimal(2, regppf.getPymt());
			    ps.setString(3, regppf.getChdrcoy());
			    ps.setString(4, regppf.getChdrnum());
			    ps.setInt(5, regppf.getRgpynum());
			    
			    ps.executeUpdate();				
				
			
		}catch (SQLException e) {
			LOGGER.error("updateNDate()",e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
		
	}
	//ILJ-686 starts
	public void updatepymnt(Regppf regppf){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE REGPPF SET PYMT=? ");
		sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? AND RGPYNUM=? ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	

				ps.setBigDecimal(1, regppf.getPymt());
			    ps.setString(2, regppf.getChdrcoy());
			    ps.setString(3, regppf.getChdrnum());
			    ps.setInt(4, regppf.getRgpynum());
			    
			    ps.executeUpdate();				
				
			
		}catch (SQLException e) {
			LOGGER.error("updatepymnt()",e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
		
	}
	//ILJ-686 ends
	
}