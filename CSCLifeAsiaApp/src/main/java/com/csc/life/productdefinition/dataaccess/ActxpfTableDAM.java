package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ActxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:40
 * Class transformed from ACTXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ActxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 29;
	public FixedLengthStringData actxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData actxpfRecord = actxrec;
	
	public FixedLengthStringData chdrpfx = DD.chdrpfx.copy().isAPartOf(actxrec);
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(actxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(actxrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(actxrec);
	public PackedDecimalData occdate = DD.occdate.copy().isAPartOf(actxrec);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(actxrec);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(actxrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(actxrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(actxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ActxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for ActxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ActxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ActxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ActxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ActxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ActxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ACTXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRPFX, " +
							"CHDRCOY, " +
							"CHDRNUM, " +
							"CNTTYPE, " +
							"OCCDATE, " +
							"STATCODE, " +
							"PSTATCODE, " +
							"CURRFROM, " +
							"VALIDFLAG, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrpfx,
                                     chdrcoy,
                                     chdrnum,
                                     cnttype,
                                     occdate,
                                     statcode,
                                     pstatcode,
                                     currfrom,
                                     validflag,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrpfx.clear();
  		chdrcoy.clear();
  		chdrnum.clear();
  		cnttype.clear();
  		occdate.clear();
  		statcode.clear();
  		pstatcode.clear();
  		currfrom.clear();
  		validflag.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getActxrec() {
  		return actxrec;
	}

	public FixedLengthStringData getActxpfRecord() {
  		return actxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setActxrec(what);
	}

	public void setActxrec(Object what) {
  		this.actxrec.set(what);
	}

	public void setActxpfRecord(Object what) {
  		this.actxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(actxrec.getLength());
		result.set(actxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}