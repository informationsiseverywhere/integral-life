package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CoevpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:32
 * Class transformed from COEVPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CoevpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 883;
	public FixedLengthStringData coevrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData coevpfRecord = coevrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(coevrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(coevrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(coevrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(coevrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(coevrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(coevrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(coevrec);
	public PackedDecimalData sumins = DD.sumins.copy().isAPartOf(coevrec);
	public PackedDecimalData riskCessTerm = DD.rcestrm.copy().isAPartOf(coevrec);
	public PackedDecimalData premCessTerm = DD.pcestrm.copy().isAPartOf(coevrec);
	public PackedDecimalData crrcd = DD.crrcd.copy().isAPartOf(coevrec);
	public PackedDecimalData zlinstprem = DD.zlinstprem.copy().isAPartOf(coevrec);
	public PackedDecimalData instprem = DD.instprem.copy().isAPartOf(coevrec);
	public PackedDecimalData aextprm = DD.aextprm.copy().isAPartOf(coevrec);
	public PackedDecimalData aprem = DD.aprem.copy().isAPartOf(coevrec);
	public FixedLengthStringData opcda01 = DD.opcda.copy().isAPartOf(coevrec);
	public FixedLengthStringData opcda02 = DD.opcda.copy().isAPartOf(coevrec);
	public FixedLengthStringData opcda03 = DD.opcda.copy().isAPartOf(coevrec);
	public FixedLengthStringData opcda04 = DD.opcda.copy().isAPartOf(coevrec);
	public FixedLengthStringData opcda05 = DD.opcda.copy().isAPartOf(coevrec);
	public PackedDecimalData oppc01 = DD.oppc.copy().isAPartOf(coevrec);
	public PackedDecimalData oppc02 = DD.oppc.copy().isAPartOf(coevrec);
	public PackedDecimalData oppc03 = DD.oppc.copy().isAPartOf(coevrec);
	public PackedDecimalData oppc04 = DD.oppc.copy().isAPartOf(coevrec);
	public PackedDecimalData oppc05 = DD.oppc.copy().isAPartOf(coevrec);
	public PackedDecimalData agerate01 = DD.agerate.copy().isAPartOf(coevrec);
	public PackedDecimalData agerate02 = DD.agerate.copy().isAPartOf(coevrec);
	public PackedDecimalData agerate03 = DD.agerate.copy().isAPartOf(coevrec);
	public PackedDecimalData agerate04 = DD.agerate.copy().isAPartOf(coevrec);
	public PackedDecimalData agerate05 = DD.agerate.copy().isAPartOf(coevrec);
	public PackedDecimalData insprm01 = DD.insprm.copy().isAPartOf(coevrec);
	public PackedDecimalData insprm02 = DD.insprm.copy().isAPartOf(coevrec);
	public PackedDecimalData insprm03 = DD.insprm.copy().isAPartOf(coevrec);
	public PackedDecimalData insprm04 = DD.insprm.copy().isAPartOf(coevrec);
	public PackedDecimalData insprm05 = DD.insprm.copy().isAPartOf(coevrec);
	public PackedDecimalData extCessTerm01 = DD.ecestrm.copy().isAPartOf(coevrec);
	public PackedDecimalData extCessTerm02 = DD.ecestrm.copy().isAPartOf(coevrec);
	public PackedDecimalData extCessTerm03 = DD.ecestrm.copy().isAPartOf(coevrec);
	public PackedDecimalData extCessTerm04 = DD.ecestrm.copy().isAPartOf(coevrec);
	public PackedDecimalData extCessTerm05 = DD.ecestrm.copy().isAPartOf(coevrec);
	public FixedLengthStringData unitVirtualFund01 = DD.vrtfnd.copy().isAPartOf(coevrec);
	public FixedLengthStringData unitVirtualFund02 = DD.vrtfnd.copy().isAPartOf(coevrec);
	public FixedLengthStringData unitVirtualFund03 = DD.vrtfnd.copy().isAPartOf(coevrec);
	public FixedLengthStringData unitVirtualFund04 = DD.vrtfnd.copy().isAPartOf(coevrec);
	public FixedLengthStringData unitVirtualFund05 = DD.vrtfnd.copy().isAPartOf(coevrec);
	public PackedDecimalData fundAmount01 = DD.fundamnt.copy().isAPartOf(coevrec);
	public PackedDecimalData fundAmount02 = DD.fundamnt.copy().isAPartOf(coevrec);
	public PackedDecimalData fundAmount03 = DD.fundamnt.copy().isAPartOf(coevrec);
	public PackedDecimalData fundAmount04 = DD.fundamnt.copy().isAPartOf(coevrec);
	public PackedDecimalData fundAmount05 = DD.fundamnt.copy().isAPartOf(coevrec);
	public FixedLengthStringData rgpymop01 = DD.rgpymop.copy().isAPartOf(coevrec);
	public FixedLengthStringData rgpymop02 = DD.rgpymop.copy().isAPartOf(coevrec);
	public FixedLengthStringData rgpymop03 = DD.rgpymop.copy().isAPartOf(coevrec);
	public FixedLengthStringData rgpymop04 = DD.rgpymop.copy().isAPartOf(coevrec);
	public FixedLengthStringData rgpymop05 = DD.rgpymop.copy().isAPartOf(coevrec);
	public PackedDecimalData pymt01 = DD.pymt.copy().isAPartOf(coevrec);
	public PackedDecimalData pymt02 = DD.pymt.copy().isAPartOf(coevrec);
	public PackedDecimalData pymt03 = DD.pymt.copy().isAPartOf(coevrec);
	public PackedDecimalData pymt04 = DD.pymt.copy().isAPartOf(coevrec);
	public PackedDecimalData pymt05 = DD.pymt.copy().isAPartOf(coevrec);
	public FixedLengthStringData anname01 = DD.anname.copy().isAPartOf(coevrec);
	public FixedLengthStringData anname02 = DD.anname.copy().isAPartOf(coevrec);
	public FixedLengthStringData anname03 = DD.anname.copy().isAPartOf(coevrec);
	public FixedLengthStringData anname04 = DD.anname.copy().isAPartOf(coevrec);
	public FixedLengthStringData anname05 = DD.anname.copy().isAPartOf(coevrec);
	public PackedDecimalData annage01 = DD.annage.copy().isAPartOf(coevrec);
	public PackedDecimalData annage02 = DD.annage.copy().isAPartOf(coevrec);
	public PackedDecimalData annage03 = DD.annage.copy().isAPartOf(coevrec);
	public PackedDecimalData annage04 = DD.annage.copy().isAPartOf(coevrec);
	public PackedDecimalData annage05 = DD.annage.copy().isAPartOf(coevrec);
	public FixedLengthStringData sanname01 = DD.sanname.copy().isAPartOf(coevrec);
	public FixedLengthStringData sanname02 = DD.sanname.copy().isAPartOf(coevrec);
	public FixedLengthStringData sanname03 = DD.sanname.copy().isAPartOf(coevrec);
	public FixedLengthStringData sanname04 = DD.sanname.copy().isAPartOf(coevrec);
	public FixedLengthStringData sanname05 = DD.sanname.copy().isAPartOf(coevrec);
	public PackedDecimalData sannage01 = DD.sannage.copy().isAPartOf(coevrec);
	public PackedDecimalData sannage02 = DD.sannage.copy().isAPartOf(coevrec);
	public PackedDecimalData sannage03 = DD.sannage.copy().isAPartOf(coevrec);
	public PackedDecimalData sannage04 = DD.sannage.copy().isAPartOf(coevrec);
	public PackedDecimalData sannage05 = DD.sannage.copy().isAPartOf(coevrec);
	public PackedDecimalData firstPaydate01 = DD.fpaydate.copy().isAPartOf(coevrec);
	public PackedDecimalData firstPaydate02 = DD.fpaydate.copy().isAPartOf(coevrec);
	public PackedDecimalData firstPaydate03 = DD.fpaydate.copy().isAPartOf(coevrec);
	public PackedDecimalData firstPaydate04 = DD.fpaydate.copy().isAPartOf(coevrec);
	public PackedDecimalData firstPaydate05 = DD.fpaydate.copy().isAPartOf(coevrec);
	public PackedDecimalData nextPaydate01 = DD.npaydate.copy().isAPartOf(coevrec);
	public PackedDecimalData nextPaydate02 = DD.npaydate.copy().isAPartOf(coevrec);
	public PackedDecimalData nextPaydate03 = DD.npaydate.copy().isAPartOf(coevrec);
	public PackedDecimalData nextPaydate04 = DD.npaydate.copy().isAPartOf(coevrec);
	public PackedDecimalData nextPaydate05 = DD.npaydate.copy().isAPartOf(coevrec);
	public PackedDecimalData finalPaydate01 = DD.epaydate.copy().isAPartOf(coevrec);
	public PackedDecimalData finalPaydate02 = DD.epaydate.copy().isAPartOf(coevrec);
	public PackedDecimalData finalPaydate03 = DD.epaydate.copy().isAPartOf(coevrec);
	public PackedDecimalData finalPaydate04 = DD.epaydate.copy().isAPartOf(coevrec);
	public PackedDecimalData finalPaydate05 = DD.epaydate.copy().isAPartOf(coevrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(coevrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(coevrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(coevrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(coevrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(coevrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public CoevpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for CoevpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public CoevpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for CoevpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public CoevpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for CoevpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public CoevpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("COEVPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"EFFDATE, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"CRTABLE, " +
							"SUMINS, " +
							"RCESTRM, " +
							"PCESTRM, " +
							"CRRCD, " +
							"ZLINSTPREM, " +
							"INSTPREM, " +
							"AEXTPRM, " +
							"APREM, " +
							"OPCDA01, " +
							"OPCDA02, " +
							"OPCDA03, " +
							"OPCDA04, " +
							"OPCDA05, " +
							"OPPC01, " +
							"OPPC02, " +
							"OPPC03, " +
							"OPPC04, " +
							"OPPC05, " +
							"AGERATE01, " +
							"AGERATE02, " +
							"AGERATE03, " +
							"AGERATE04, " +
							"AGERATE05, " +
							"INSPRM01, " +
							"INSPRM02, " +
							"INSPRM03, " +
							"INSPRM04, " +
							"INSPRM05, " +
							"ECESTRM01, " +
							"ECESTRM02, " +
							"ECESTRM03, " +
							"ECESTRM04, " +
							"ECESTRM05, " +
							"VRTFND01, " +
							"VRTFND02, " +
							"VRTFND03, " +
							"VRTFND04, " +
							"VRTFND05, " +
							"FUNDAMNT01, " +
							"FUNDAMNT02, " +
							"FUNDAMNT03, " +
							"FUNDAMNT04, " +
							"FUNDAMNT05, " +
							"RGPYMOP01, " +
							"RGPYMOP02, " +
							"RGPYMOP03, " +
							"RGPYMOP04, " +
							"RGPYMOP05, " +
							"PYMT01, " +
							"PYMT02, " +
							"PYMT03, " +
							"PYMT04, " +
							"PYMT05, " +
							"ANNAME01, " +
							"ANNAME02, " +
							"ANNAME03, " +
							"ANNAME04, " +
							"ANNAME05, " +
							"ANNAGE01, " +
							"ANNAGE02, " +
							"ANNAGE03, " +
							"ANNAGE04, " +
							"ANNAGE05, " +
							"SANNAME01, " +
							"SANNAME02, " +
							"SANNAME03, " +
							"SANNAME04, " +
							"SANNAME05, " +
							"SANNAGE01, " +
							"SANNAGE02, " +
							"SANNAGE03, " +
							"SANNAGE04, " +
							"SANNAGE05, " +
							"FPAYDATE01, " +
							"FPAYDATE02, " +
							"FPAYDATE03, " +
							"FPAYDATE04, " +
							"FPAYDATE05, " +
							"NPAYDATE01, " +
							"NPAYDATE02, " +
							"NPAYDATE03, " +
							"NPAYDATE04, " +
							"NPAYDATE05, " +
							"EPAYDATE01, " +
							"EPAYDATE02, " +
							"EPAYDATE03, " +
							"EPAYDATE04, " +
							"EPAYDATE05, " +
							"CURRFROM, " +
							"VALIDFLAG, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     effdate,
                                     life,
                                     coverage,
                                     rider,
                                     crtable,
                                     sumins,
                                     riskCessTerm,
                                     premCessTerm,
                                     crrcd,
                                     zlinstprem,
                                     instprem,
                                     aextprm,
                                     aprem,
                                     opcda01,
                                     opcda02,
                                     opcda03,
                                     opcda04,
                                     opcda05,
                                     oppc01,
                                     oppc02,
                                     oppc03,
                                     oppc04,
                                     oppc05,
                                     agerate01,
                                     agerate02,
                                     agerate03,
                                     agerate04,
                                     agerate05,
                                     insprm01,
                                     insprm02,
                                     insprm03,
                                     insprm04,
                                     insprm05,
                                     extCessTerm01,
                                     extCessTerm02,
                                     extCessTerm03,
                                     extCessTerm04,
                                     extCessTerm05,
                                     unitVirtualFund01,
                                     unitVirtualFund02,
                                     unitVirtualFund03,
                                     unitVirtualFund04,
                                     unitVirtualFund05,
                                     fundAmount01,
                                     fundAmount02,
                                     fundAmount03,
                                     fundAmount04,
                                     fundAmount05,
                                     rgpymop01,
                                     rgpymop02,
                                     rgpymop03,
                                     rgpymop04,
                                     rgpymop05,
                                     pymt01,
                                     pymt02,
                                     pymt03,
                                     pymt04,
                                     pymt05,
                                     anname01,
                                     anname02,
                                     anname03,
                                     anname04,
                                     anname05,
                                     annage01,
                                     annage02,
                                     annage03,
                                     annage04,
                                     annage05,
                                     sanname01,
                                     sanname02,
                                     sanname03,
                                     sanname04,
                                     sanname05,
                                     sannage01,
                                     sannage02,
                                     sannage03,
                                     sannage04,
                                     sannage05,
                                     firstPaydate01,
                                     firstPaydate02,
                                     firstPaydate03,
                                     firstPaydate04,
                                     firstPaydate05,
                                     nextPaydate01,
                                     nextPaydate02,
                                     nextPaydate03,
                                     nextPaydate04,
                                     nextPaydate05,
                                     finalPaydate01,
                                     finalPaydate02,
                                     finalPaydate03,
                                     finalPaydate04,
                                     finalPaydate05,
                                     currfrom,
                                     validflag,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		effdate.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		crtable.clear();
  		sumins.clear();
  		riskCessTerm.clear();
  		premCessTerm.clear();
  		crrcd.clear();
  		zlinstprem.clear();
  		instprem.clear();
  		aextprm.clear();
  		aprem.clear();
  		opcda01.clear();
  		opcda02.clear();
  		opcda03.clear();
  		opcda04.clear();
  		opcda05.clear();
  		oppc01.clear();
  		oppc02.clear();
  		oppc03.clear();
  		oppc04.clear();
  		oppc05.clear();
  		agerate01.clear();
  		agerate02.clear();
  		agerate03.clear();
  		agerate04.clear();
  		agerate05.clear();
  		insprm01.clear();
  		insprm02.clear();
  		insprm03.clear();
  		insprm04.clear();
  		insprm05.clear();
  		extCessTerm01.clear();
  		extCessTerm02.clear();
  		extCessTerm03.clear();
  		extCessTerm04.clear();
  		extCessTerm05.clear();
  		unitVirtualFund01.clear();
  		unitVirtualFund02.clear();
  		unitVirtualFund03.clear();
  		unitVirtualFund04.clear();
  		unitVirtualFund05.clear();
  		fundAmount01.clear();
  		fundAmount02.clear();
  		fundAmount03.clear();
  		fundAmount04.clear();
  		fundAmount05.clear();
  		rgpymop01.clear();
  		rgpymop02.clear();
  		rgpymop03.clear();
  		rgpymop04.clear();
  		rgpymop05.clear();
  		pymt01.clear();
  		pymt02.clear();
  		pymt03.clear();
  		pymt04.clear();
  		pymt05.clear();
  		anname01.clear();
  		anname02.clear();
  		anname03.clear();
  		anname04.clear();
  		anname05.clear();
  		annage01.clear();
  		annage02.clear();
  		annage03.clear();
  		annage04.clear();
  		annage05.clear();
  		sanname01.clear();
  		sanname02.clear();
  		sanname03.clear();
  		sanname04.clear();
  		sanname05.clear();
  		sannage01.clear();
  		sannage02.clear();
  		sannage03.clear();
  		sannage04.clear();
  		sannage05.clear();
  		firstPaydate01.clear();
  		firstPaydate02.clear();
  		firstPaydate03.clear();
  		firstPaydate04.clear();
  		firstPaydate05.clear();
  		nextPaydate01.clear();
  		nextPaydate02.clear();
  		nextPaydate03.clear();
  		nextPaydate04.clear();
  		nextPaydate05.clear();
  		finalPaydate01.clear();
  		finalPaydate02.clear();
  		finalPaydate03.clear();
  		finalPaydate04.clear();
  		finalPaydate05.clear();
  		currfrom.clear();
  		validflag.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getCoevrec() {
  		return coevrec;
	}

	public FixedLengthStringData getCoevpfRecord() {
  		return coevpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setCoevrec(what);
	}

	public void setCoevrec(Object what) {
  		this.coevrec.set(what);
	}

	public void setCoevpfRecord(Object what) {
  		this.coevpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(coevrec.getLength());
		result.set(coevrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}