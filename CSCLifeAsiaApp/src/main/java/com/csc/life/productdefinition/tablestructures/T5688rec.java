package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:59
 * Description:
 * Copybook name: T5688REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5688rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5688Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData anbrqd = new FixedLengthStringData(1).isAPartOf(t5688Rec, 0);
  	public FixedLengthStringData bankcode = new FixedLengthStringData(2).isAPartOf(t5688Rec, 1);
  	public ZonedDecimalData cfiLim = new ZonedDecimalData(3, 0).isAPartOf(t5688Rec, 3);
  	public FixedLengthStringData comlvlacc = new FixedLengthStringData(1).isAPartOf(t5688Rec, 6);
  	public FixedLengthStringData defFupMeth = new FixedLengthStringData(4).isAPartOf(t5688Rec, 7);
  	public FixedLengthStringData feemeth = new FixedLengthStringData(4).isAPartOf(t5688Rec, 11);
  	public ZonedDecimalData jlifemax = new ZonedDecimalData(2, 0).isAPartOf(t5688Rec, 15);
  	public ZonedDecimalData jlifemin = new ZonedDecimalData(2, 0).isAPartOf(t5688Rec, 17);
  	public ZonedDecimalData lifemax = new ZonedDecimalData(2, 0).isAPartOf(t5688Rec, 19);
  	public ZonedDecimalData lifemin = new ZonedDecimalData(2, 0).isAPartOf(t5688Rec, 21);
  	public FixedLengthStringData minDepMth = new FixedLengthStringData(4).isAPartOf(t5688Rec, 23);
  	public FixedLengthStringData nbusallw = new FixedLengthStringData(1).isAPartOf(t5688Rec, 27);
  	public FixedLengthStringData occrqd = new FixedLengthStringData(1).isAPartOf(t5688Rec, 28);
  	public ZonedDecimalData poldef = new ZonedDecimalData(2, 0).isAPartOf(t5688Rec, 29);
  	public ZonedDecimalData polmax = new ZonedDecimalData(2, 0).isAPartOf(t5688Rec, 31);
  	public ZonedDecimalData polmin = new ZonedDecimalData(2, 0).isAPartOf(t5688Rec, 33);
  	public FixedLengthStringData revacc = new FixedLengthStringData(1).isAPartOf(t5688Rec, 35);
  	public FixedLengthStringData smkrqd = new FixedLengthStringData(1).isAPartOf(t5688Rec, 36);
  	public FixedLengthStringData taxrelmth = new FixedLengthStringData(4).isAPartOf(t5688Rec, 37);
  	public FixedLengthStringData zcertall = new FixedLengthStringData(1).isAPartOf(t5688Rec, 41);
  	public FixedLengthStringData zcfidtall = new FixedLengthStringData(1).isAPartOf(t5688Rec, 42);
  	public FixedLengthStringData znfopt = new FixedLengthStringData(3).isAPartOf(t5688Rec, 43);
  	//ILIFE-1137 starts
  	public FixedLengthStringData dflexmeth = new FixedLengthStringData(10).isAPartOf(t5688Rec, 46);
  	public FixedLengthStringData bfreqallw = new FixedLengthStringData(1).isAPartOf(t5688Rec, 56);
//  public FixedLengthStringData filler = new FixedLengthStringData(454).isAPartOf(t5688Rec, 46, FILLER);
  	public FixedLengthStringData filler = new FixedLengthStringData(443).isAPartOf(t5688Rec, 57, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5688Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5688Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}