/*
 * File: Py501.java
 * Date: 30 August 2009 0:34:01
 * Author: Quipoz Limited
 * 
 * Class transformed from Py501.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.screens.Sy501ScreenVars;
import com.csc.life.productdefinition.tablestructures.Ty501rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class Py501 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PY501");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
		/* ERRORS */
	private String e031 = "E031";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String descrec = "DESCREC";
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Ty501rec ty501rec = new Ty501rec();
	private Desckey wsaaDesckey = new Desckey();
	private Itemkey wsaaItemkey = new Itemkey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sy501ScreenVars sv = ScreenProgram.getScreenVars( Sy501ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		cont1190, 
		preExit, 
		exit2090, 
		exit3900, 
		exit5090
	}

	public Py501() {
		super();
		screenVars = sv;
		new ScreenModel("Sy501", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1001();
				}
				case cont1190: {
					cont1190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1001()
	{
		sv.dataArea.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		wsaaItemkey.set(wsspsmart.itemkey);
		wsaaDesckey.descDescpfx.set(wsaaItemkey.itemItempfx);
		wsaaDesckey.descDesccoy.set(wsaaItemkey.itemItemcoy);
		wsaaDesckey.descDesctabl.set(wsaaItemkey.itemItemtabl);
		wsaaDesckey.descDescitem.set(wsaaItemkey.itemItemitem);
		wsaaDesckey.descItemseq.set(wsaaItemkey.itemItemseq);
		wsaaDesckey.descLanguage.set(wsspcomn.language);
		sv.company.set(wsaaItemkey.itemItemcoy);
		sv.tabl.set(wsaaItemkey.itemItemtabl);
		sv.item.set(wsaaItemkey.itemItemitem);
		descIO.setDataKey(wsaaDesckey);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setDataArea(SPACES);
		}
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.longdesc.set(descIO.getLongdesc());
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			itemIO.setStatuz(e031);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		ty501rec.ty501Rec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(),SPACES)) {
			goTo(GotoLabel.cont1190);
		}
	}

protected void cont1190()
	{
		sv.cnPremStats.set(ty501rec.cnPremStats);
		sv.cnRiskStats.set(ty501rec.cnRiskStats);
		sv.covPremStats.set(ty501rec.covPremStats);
		sv.covRiskStats.set(ty501rec.covRiskStats);
		sv.jlifeStats.set(ty501rec.jlifeStats);
		sv.lifeStats.set(ty501rec.lifeStats);
		sv.ridPremStats.set(ty501rec.ridPremStats);
		sv.ridRiskStats.set(ty501rec.ridRiskStats);
		sv.setCnPremStat.set(ty501rec.setCnPremStat);
		sv.setCnRiskStat.set(ty501rec.setCnRiskStat);
		sv.setCovPremStat.set(ty501rec.setCovPremStat);
		sv.setCovRiskStat.set(ty501rec.setCovRiskStat);
		sv.setJlifeStat.set(ty501rec.setJlifeStat);
		sv.setLifeStat.set(ty501rec.setLifeStat);
		sv.setRidPremStat.set(ty501rec.setRidPremStat);
		sv.setRidRiskStat.set(ty501rec.setRidRiskStat);
		sv.setSngpCnStat.set(ty501rec.setSngpCnStat);
		sv.setSngpCovStat.set(ty501rec.setSngpCovStat);
		sv.setSngpRidStat.set(ty501rec.setSngpRidStat);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2001();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2001()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			wsspcomn.edterror.set(varcom.oK);
		}
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void update3000()
	{
		try {
			loadWsspFields3100();
		}
		catch (GOTOException e){
		}
	}

protected void loadWsspFields3100()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3900);
		}
		updateRec5000();
		/*COMMIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void updateRec5000()
	{
		try {
			para5000();
			compareFields5020();
			writeRecord5080();
		}
		catch (GOTOException e){
		}
	}

protected void para5000()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		/*READ-RECORD*/
		callItemio5100();
	}

protected void compareFields5020()
	{
		wsaaUpdateFlag = "N";
		checkChanges5300();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.exit5090);
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
		itemIO.setGenarea(ty501rec.ty501Rec);
	}

protected void writeRecord5080()
	{
		itemIO.setFunction(varcom.rewrt);
		callItemio5100();
	}

protected void callItemio5100()
	{
		/*PARA*/
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.dbparams.set(itemIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkChanges5300()
	{
		para5300();
	}

protected void para5300()
	{
		if (isNE(sv.cnPremStats,ty501rec.cnPremStats)) {
			ty501rec.cnPremStats.set(sv.cnPremStats);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.cnRiskStats,ty501rec.cnRiskStats)) {
			ty501rec.cnRiskStats.set(sv.cnRiskStats);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.covPremStats,ty501rec.covPremStats)) {
			ty501rec.covPremStats.set(sv.covPremStats);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.covRiskStats,ty501rec.covRiskStats)) {
			ty501rec.covRiskStats.set(sv.covRiskStats);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.jlifeStats,ty501rec.jlifeStats)) {
			ty501rec.jlifeStats.set(sv.jlifeStats);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.lifeStats,ty501rec.lifeStats)) {
			ty501rec.lifeStats.set(sv.lifeStats);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.ridPremStats,ty501rec.ridPremStats)) {
			ty501rec.ridPremStats.set(sv.ridPremStats);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.ridRiskStats,ty501rec.ridRiskStats)) {
			ty501rec.ridRiskStats.set(sv.ridRiskStats);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setCnPremStat,ty501rec.setCnPremStat)) {
			ty501rec.setCnPremStat.set(sv.setCnPremStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setCnRiskStat,ty501rec.setCnRiskStat)) {
			ty501rec.setCnRiskStat.set(sv.setCnRiskStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setCovPremStat,ty501rec.setCovPremStat)) {
			ty501rec.setCovPremStat.set(sv.setCovPremStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setCovRiskStat,ty501rec.setCovRiskStat)) {
			ty501rec.setCovRiskStat.set(sv.setCovRiskStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setJlifeStat,ty501rec.setJlifeStat)) {
			ty501rec.setJlifeStat.set(sv.setJlifeStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setLifeStat,ty501rec.setLifeStat)) {
			ty501rec.setLifeStat.set(sv.setLifeStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setRidPremStat,ty501rec.setRidPremStat)) {
			ty501rec.setRidPremStat.set(sv.setRidPremStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setRidRiskStat,ty501rec.setRidRiskStat)) {
			ty501rec.setRidRiskStat.set(sv.setRidRiskStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setSngpCnStat,ty501rec.setSngpCnStat)) {
			ty501rec.setSngpCnStat.set(sv.setSngpCnStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setSngpCovStat,ty501rec.setSngpCovStat)) {
			ty501rec.setSngpCovStat.set(sv.setSngpCovStat);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.setSngpRidStat,ty501rec.setSngpRidStat)) {
			ty501rec.setSngpRidStat.set(sv.setSngpRidStat);
			wsaaUpdateFlag = "Y";
		}
	}
}
