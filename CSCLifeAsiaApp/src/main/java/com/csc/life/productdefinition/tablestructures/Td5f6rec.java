package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: asirvya
 * @version
 * Creation Date: Sun, 18 April 2018 03:15:54
 * Description:
 * Copybook name:  Td5f6REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class  Td5f6rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData td5f6Rec = new FixedLengthStringData(67);
  	
  	public FixedLengthStringData fundPoolPris = new FixedLengthStringData(13).isAPartOf(td5f6Rec, 0);
	public FixedLengthStringData[] fundPoolPri = FLSArrayPartOfStructure(13, 1, fundPoolPris, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(13).isAPartOf(fundPoolPris, 0, FILLER_REDEFINE);
	public FixedLengthStringData fundPoolPri01 = DD.fundPoolPriority.copy().isAPartOf(filler,0);
	public FixedLengthStringData fundPoolPri02 = DD.fundPoolPriority.copy().isAPartOf(filler,1);
	public FixedLengthStringData fundPoolPri03 = DD.fundPoolPriority.copy().isAPartOf(filler,2);
	public FixedLengthStringData fundPoolPri04 = DD.fundPoolPriority.copy().isAPartOf(filler,3);
	public FixedLengthStringData fundPoolPri05 = DD.fundPoolPriority.copy().isAPartOf(filler,4);
	public FixedLengthStringData fundPoolPri06 = DD.fundPoolPriority.copy().isAPartOf(filler,5);
	public FixedLengthStringData fundPoolPri07 = DD.fundPoolPriority.copy().isAPartOf(filler,6);
	public FixedLengthStringData fundPoolPri08 = DD.fundPoolPriority.copy().isAPartOf(filler,7);
	public FixedLengthStringData fundPoolPri09 = DD.fundPoolPriority.copy().isAPartOf(filler,8);
	public FixedLengthStringData fundPoolPri10 = DD.fundPoolPriority.copy().isAPartOf(filler,9);
	public FixedLengthStringData fundPoolPri11 = DD.fundPoolPriority.copy().isAPartOf(filler,10);
	public FixedLengthStringData fundPoolPri12 = DD.fundPoolPriority.copy().isAPartOf(filler,11);
	public FixedLengthStringData fhfp = DD.fundPoolPriority.copy().isAPartOf(filler, 12);
	
	public FixedLengthStringData company = DD.company.copy().isAPartOf(td5f6Rec,13);		
	public FixedLengthStringData item = DD.item.copy().isAPartOf(td5f6Rec,14);	
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(td5f6Rec,22);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(td5f6Rec,52);	
	
	public FixedLengthStringData fhbcl = DD.fhbcl.copy().isAPartOf(td5f6Rec, 57);
  	public FixedLengthStringData fhscc = DD.fhscc.copy().isAPartOf(td5f6Rec, 58);
	public FixedLengthStringData dhbcl = DD.dhbcl.copy().isAPartOf(td5f6Rec, 62);
  	public FixedLengthStringData dhscc = DD.dhscc.copy().isAPartOf(td5f6Rec, 63);
  	


	public void initialize() {
		COBOLFunctions.initialize(td5f6Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		td5f6Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}