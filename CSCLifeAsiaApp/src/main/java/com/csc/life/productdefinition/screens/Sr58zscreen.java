package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 17/08/2016 05:43
 * @author CSCUSER
 */
public class Sr58zscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 17, 22, 4, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 19, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr58zScreenVars sv = (Sr58zScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr58zscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr58zScreenVars screenVars = (Sr58zScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		
		screenVars.billfreq01.setClassString("");
		screenVars.billfreq02.setClassString("");
		screenVars.billfreq03.setClassString("");
		screenVars.billfreq04.setClassString("");
		screenVars.billfreq05.setClassString("");
		screenVars.billfreq06.setClassString("");
		screenVars.billfreq07.setClassString("");
		screenVars.billfreq08.setClassString("");
		screenVars.billfreq09.setClassString("");
		screenVars.billfreq10.setClassString("");
		screenVars.billfreq11.setClassString("");
		screenVars.billfreq12.setClassString("");
		screenVars.billfreq13.setClassString("");
		screenVars.billfreq14.setClassString("");
		screenVars.billfreq15.setClassString("");
		screenVars.billfreq16.setClassString("");
		screenVars.billfreq17.setClassString("");
		screenVars.billfreq18.setClassString("");
		screenVars.billfreq19.setClassString("");
		screenVars.billfreq20.setClassString("");
		screenVars.billfreq21.setClassString("");
		screenVars.billfreq22.setClassString("");
		screenVars.billfreq23.setClassString("");
		screenVars.billfreq24.setClassString("");
		screenVars.billfreq25.setClassString("");
		screenVars.billfreq26.setClassString("");
		screenVars.billfreq27.setClassString("");
		screenVars.billfreq28.setClassString("");
		screenVars.billfreq29.setClassString("");
		screenVars.billfreq30.setClassString("");
	}

/**
 * Clear all the variables in S5689screen
 */
	public static void clear(VarModel pv) {
		Sr58zScreenVars screenVars = (Sr58zScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.billfreq01.clear();
		screenVars.billfreq02.clear();
		screenVars.billfreq03.clear();
		screenVars.billfreq04.clear();
		screenVars.billfreq05.clear();
		screenVars.billfreq06.clear();
		screenVars.billfreq07.clear();
		screenVars.billfreq08.clear();
		screenVars.billfreq09.clear();
		screenVars.billfreq10.clear();
		screenVars.billfreq11.clear();
		screenVars.billfreq12.clear();
		screenVars.billfreq13.clear();
		screenVars.billfreq14.clear();
		screenVars.billfreq15.clear();
		screenVars.billfreq16.clear();
		screenVars.billfreq17.clear();
		screenVars.billfreq18.clear();
		screenVars.billfreq19.clear();
		screenVars.billfreq20.clear();
		screenVars.billfreq21.clear();
		screenVars.billfreq22.clear();
		screenVars.billfreq23.clear();
		screenVars.billfreq24.clear();
		screenVars.billfreq25.clear();
		screenVars.billfreq26.clear();
		screenVars.billfreq27.clear();
		screenVars.billfreq28.clear();
		screenVars.billfreq29.clear();
		screenVars.billfreq30.clear();	
	}
}
