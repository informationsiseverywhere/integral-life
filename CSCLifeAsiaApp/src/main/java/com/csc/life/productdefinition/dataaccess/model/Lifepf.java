package com.csc.life.productdefinition.dataaccess.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;

public class Lifepf implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8903808297016121410L;
	@Id
	@Column(name = "UNIQUE_NUMBER")
	private long uniqueNumber;
    private String chdrcoy;
    private String chdrnum;
    private String validflag;
    private String statcode;
    private Integer tranno;
    private Integer currfrom;
    private Integer currto;
    private String life;
    private String jlife;
    private Integer lcdte;
    private String lifcnum;
    private String liferel;
    private Integer cltdob;
    private String cltsex;
    private Integer anbCcd;
    private String ageadm;
    private String selection;
    private String smoking;
    private String occup;
    private String pursuit01;
    private String pursuit02;
    private String martal;
    private String sbstdl;
    private String termid;
    private Integer trdt;
    private Integer trtm;
    private Integer user_t;
    private String relation;//ICIL-357
    private Timestamp datime;	//IBPLIFE-6370
    public Lifepf() {
	}
    public Lifepf(Lifepf lifepf){
    	this.uniqueNumber=lifepf.uniqueNumber;
    	this.chdrcoy=lifepf.chdrcoy;
    	this.chdrnum=lifepf.chdrnum;
    	this.validflag=lifepf.validflag;
    	this.statcode=lifepf.statcode;
    	this.tranno=lifepf.tranno;
    	this.currfrom=lifepf.currfrom;
    	this.currto=lifepf.currto;
    	this.life=lifepf.life;
    	this.jlife=lifepf.jlife;
    	this.lcdte=lifepf.lcdte;
    	this.lifcnum=lifepf.lifcnum;
    	this.liferel=lifepf.liferel;
    	this.cltdob=lifepf.cltdob;
    	this.cltsex=lifepf.cltsex;
    	this.anbCcd=lifepf.anbCcd;
    	this.ageadm=lifepf.ageadm;
    	this.selection=lifepf.selection;
    	this.smoking=lifepf.smoking;
    	this.occup=lifepf.occup;
    	this.pursuit01=lifepf.pursuit01;
    	this.pursuit02=lifepf.pursuit02;
    	this.martal=lifepf.martal;
    	this.sbstdl=lifepf.sbstdl;
    	this.termid=lifepf.termid;
    	this.trdt=lifepf.trdt;
    	this.trtm=lifepf.trtm;
    	this.relation=lifepf.relation;
    	this.user_t=lifepf.user_t;
    	this.datime=lifepf.datime;
    }

    public String getChdrcoy() {
        return chdrcoy;
    }

    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }

    public String getChdrnum() {
        return chdrnum;
    }

    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }

    public String getValidflag() {
        return validflag;
    }

    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }

    public String getStatcode() {
        return statcode;
    }

    public void setStatcode(String statcode) {
        this.statcode = statcode;
    }

    public int getTranno() {
        return tranno == null ? 0 : tranno;
    }

    public void setTranno(int tranno) {
        this.tranno = tranno;
    }

    public int getCurrfrom() {
        return currfrom == null ? 0 : currfrom;
    }

    public void setCurrfrom(int currfrom) {
        this.currfrom = currfrom;
    }

    public int getCurrto() {
        return currto == null ? 0 : currto;
    }

    public void setCurrto(int currto) {
        this.currto = currto;
    }

    public String getLife() {
        return life;
    }

    public void setLife(String life) {
        this.life = life;
    }

    public String getJlife() {
        return jlife;
    }

    public void setJlife(String jlife) {
        this.jlife = jlife;
    }

    public int getLifeCommDate() {
        return lcdte == null ? 0 : lcdte;
    }

    public void setLifeCommDate(int lifeCommDate) {
        this.lcdte = lifeCommDate;
    }

    public String getLifcnum() {
        return lifcnum;
    }

    public void setLifcnum(String lifcnum) {
        this.lifcnum = lifcnum;
    }

    public String getLiferel() {
        return liferel;
    }

    public void setLiferel(String liferel) {
        this.liferel = liferel;
    }

    public int getCltdob() {
        return cltdob == null ? 0 : cltdob;
    }

    public void setCltdob(int cltdob) {
        this.cltdob = cltdob;
    }

    public String getCltsex() {
        return cltsex;
    }

    public void setCltsex(String cltsex) {
        this.cltsex = cltsex;
    }

    public int getAnbAtCcd() {
        return anbCcd  == null ? 0 : anbCcd;
    }

    public void setAnbAtCcd(int anbAtCcd) {
        this.anbCcd = anbAtCcd;
    }

    public String getAgeadm() {
        return ageadm;
    }

    public void setAgeadm(String ageadm) {
        this.ageadm = ageadm;
    }

    public String getSelection() {
        return selection;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }

    public String getSmoking() {
        return smoking;
    }

    public void setSmoking(String smoking) {
        this.smoking = smoking;
    }

    public String getOccup() {
        return occup;
    }

    public void setOccup(String occup) {
        this.occup = occup;
    }

    public String getPursuit01() {
        return pursuit01;
    }

    public void setPursuit01(String pursuit01) {
        this.pursuit01 = pursuit01;
    }

    public String getPursuit02() {
        return pursuit02;
    }

    public void setPursuit02(String pursuit02) {
        this.pursuit02 = pursuit02;
    }

    public String getMaritalState() {
        return martal;
    }

    public void setMaritalState(String maritalState) {
        this.martal = maritalState;
    }

    public String getSbstdl() {
        return sbstdl;
    }

    public void setSbstdl(String sbstdl) {
        this.sbstdl = sbstdl;
    }

    public String getTermid() {
        return termid;
    }

    public void setTermid(String termid) {
        this.termid = termid;
    }

    public int getTransactionDate() {
        return trdt == null ? 0 : trdt;
    }

    public void setTransactionDate(int transactionDate) {
        this.trdt = transactionDate;
    }

    public int getTransactionTime() {
        return trtm == null ? 0 : trtm;
    }

    public void setTransactionTime(int transactionTime) {
        this.trtm = transactionTime;
    }

    public int getUser() {
        return user_t == null ? 0 : user_t;
    }

    public void setUser(int user) {
        this.user_t = user;
    }
	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	//IBPLIFE-6370 Starts
	public Timestamp getDatime() {
		return datime;
	}
	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}
	//IBPLIFE-6370 End
	
}