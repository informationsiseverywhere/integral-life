package com.csc.life.productdefinition.procedures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Vpxutrsrec extends ExternalData {

	public FixedLengthStringData vpxutrsRec = new FixedLengthStringData(162);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(vpxutrsRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(vpxutrsRec, 5);
  	public FixedLengthStringData copybook = new FixedLengthStringData(12).isAPartOf(vpxutrsRec, 9);
  	public FixedLengthStringData result = new FixedLengthStringData(115).isAPartOf(vpxutrsRec, 21);
  	public FixedLengthStringData resultField = new FixedLengthStringData(35).isAPartOf(result, 0);
  	public FixedLengthStringData resultValue = new FixedLengthStringData(80).isAPartOf(result, 35);
  	public FixedLengthStringData dataRec = new FixedLengthStringData(26).isAPartOf(vpxutrsRec, 136);
  	//Ticket #IVE-165 - RUL Calculations - Regular Withdrawal start
  	public PackedDecimalData curduntbal = new PackedDecimalData(16, 5).isAPartOf(dataRec, 0);
  	public ZonedDecimalData ccdate = new ZonedDecimalData(8).isAPartOf(dataRec, 16);
  	//Ticket #IVE-165 - RUL Calculations - Regular Withdrawal end
  	public FixedLengthStringData frequency = new FixedLengthStringData(2).isAPartOf(dataRec, 24);
  	
	public void initialize() {
		COBOLFunctions.initialize(vpxutrsRec);
	}	
	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			vpxutrsRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}

}
