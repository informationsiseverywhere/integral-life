package com.csc.life.productdefinition.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ChevTableDAM.java
 * Date: Sun, 30 Aug 2009 03:33:18
 * Class transformed from CHEV.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ChevTableDAM extends ChevpfTableDAM {

	public ChevTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("CHEV");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", EFFDATE"
		             + ", CURRFROM"
		             + ", VALIDFLAG";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "EFFDATE, " +
		            "CURRFROM, " +
		            "VALIDFLAG, " +
		            "CNTTYPE, " +
		            "RCDATE, " +
		            "OWNNAM, " +
		            "PAYORNAME, " +
		            "HOISSDTE, " +
		            "HISSDTE, " +
		            "AGENTNO, " +
		            "AGTYPE, " +
		            "BILLFREQ, " +
		            "STATCODE, " +
		            "PSTATCODE, " +
		            "PTDATE, " +
		            "BTDATE, " +
		            "TOTLPREM, " +
		            "INSTPREM, " +
		            "CSHMT, " +
		            "LOANVALUE, " +
		            "SURRCHG, " +
		            "SURRVAL, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "EFFDATE DESC, " +
		            "CURRFROM DESC, " +
		            "VALIDFLAG ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "EFFDATE ASC, " +
		            "CURRFROM ASC, " +
		            "VALIDFLAG DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               effdate,
                               currfrom,
                               validflag,
                               cnttype,
                               rcdate,
                               ownnam,
                               payorname,
                               hoissdte,
                               hissdte,
                               agentno,
                               agtype,
                               billfreq,
                               statcode,
                               pstatcode,
                               ptdate,
                               btdate,
                               totlprem,
                               instprem,
                               cshmt,
                               loanvalue,
                               surrchg,
                               surrval,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(44);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getEffdate().toInternal()
					+ getCurrfrom().toInternal()
					+ getValidflag().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(5);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(5);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(1);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(effdate.toInternal());
	nonKeyFiller40.setInternal(currfrom.toInternal());
	nonKeyFiller50.setInternal(validflag.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(226);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ getCnttype().toInternal()
					+ getRcdate().toInternal()
					+ getOwnnam().toInternal()
					+ getPayorname().toInternal()
					+ getHoissdte().toInternal()
					+ getHissdte().toInternal()
					+ getAgentno().toInternal()
					+ getAgtype().toInternal()
					+ getBillfreq().toInternal()
					+ getStatcode().toInternal()
					+ getPstatcode().toInternal()
					+ getPtdate().toInternal()
					+ getBtdate().toInternal()
					+ getTotlprem().toInternal()
					+ getInstprem().toInternal()
					+ getCshmt().toInternal()
					+ getLoanvalue().toInternal()
					+ getSurrchg().toInternal()
					+ getSurrval().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, rcdate);
			what = ExternalData.chop(what, ownnam);
			what = ExternalData.chop(what, payorname);
			what = ExternalData.chop(what, hoissdte);
			what = ExternalData.chop(what, hissdte);
			what = ExternalData.chop(what, agentno);
			what = ExternalData.chop(what, agtype);
			what = ExternalData.chop(what, billfreq);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, ptdate);
			what = ExternalData.chop(what, btdate);
			what = ExternalData.chop(what, totlprem);
			what = ExternalData.chop(what, instprem);
			what = ExternalData.chop(what, cshmt);
			what = ExternalData.chop(what, loanvalue);
			what = ExternalData.chop(what, surrchg);
			what = ExternalData.chop(what, surrval);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}	
	public PackedDecimalData getRcdate() {
		return rcdate;
	}
	public void setRcdate(Object what) {
		setRcdate(what, false);
	}
	public void setRcdate(Object what, boolean rounded) {
		if (rounded)
			rcdate.setRounded(what);
		else
			rcdate.set(what);
	}	
	public FixedLengthStringData getOwnnam() {
		return ownnam;
	}
	public void setOwnnam(Object what) {
		ownnam.set(what);
	}	
	public FixedLengthStringData getPayorname() {
		return payorname;
	}
	public void setPayorname(Object what) {
		payorname.set(what);
	}	
	public PackedDecimalData getHoissdte() {
		return hoissdte;
	}
	public void setHoissdte(Object what) {
		setHoissdte(what, false);
	}
	public void setHoissdte(Object what, boolean rounded) {
		if (rounded)
			hoissdte.setRounded(what);
		else
			hoissdte.set(what);
	}	
	public PackedDecimalData getHissdte() {
		return hissdte;
	}
	public void setHissdte(Object what) {
		setHissdte(what, false);
	}
	public void setHissdte(Object what, boolean rounded) {
		if (rounded)
			hissdte.setRounded(what);
		else
			hissdte.set(what);
	}	
	public FixedLengthStringData getAgentno() {
		return agentno;
	}
	public void setAgentno(Object what) {
		agentno.set(what);
	}	
	public FixedLengthStringData getAgtype() {
		return agtype;
	}
	public void setAgtype(Object what) {
		agtype.set(what);
	}	
	public FixedLengthStringData getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(Object what) {
		billfreq.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}	
	public PackedDecimalData getPtdate() {
		return ptdate;
	}
	public void setPtdate(Object what) {
		setPtdate(what, false);
	}
	public void setPtdate(Object what, boolean rounded) {
		if (rounded)
			ptdate.setRounded(what);
		else
			ptdate.set(what);
	}	
	public PackedDecimalData getBtdate() {
		return btdate;
	}
	public void setBtdate(Object what) {
		setBtdate(what, false);
	}
	public void setBtdate(Object what, boolean rounded) {
		if (rounded)
			btdate.setRounded(what);
		else
			btdate.set(what);
	}	
	public PackedDecimalData getTotlprem() {
		return totlprem;
	}
	public void setTotlprem(Object what) {
		setTotlprem(what, false);
	}
	public void setTotlprem(Object what, boolean rounded) {
		if (rounded)
			totlprem.setRounded(what);
		else
			totlprem.set(what);
	}	
	public PackedDecimalData getInstprem() {
		return instprem;
	}
	public void setInstprem(Object what) {
		setInstprem(what, false);
	}
	public void setInstprem(Object what, boolean rounded) {
		if (rounded)
			instprem.setRounded(what);
		else
			instprem.set(what);
	}	
	public PackedDecimalData getCshmt() {
		return cshmt;
	}
	public void setCshmt(Object what) {
		setCshmt(what, false);
	}
	public void setCshmt(Object what, boolean rounded) {
		if (rounded)
			cshmt.setRounded(what);
		else
			cshmt.set(what);
	}	
	public PackedDecimalData getLoanvalue() {
		return loanvalue;
	}
	public void setLoanvalue(Object what) {
		setLoanvalue(what, false);
	}
	public void setLoanvalue(Object what, boolean rounded) {
		if (rounded)
			loanvalue.setRounded(what);
		else
			loanvalue.set(what);
	}	
	public PackedDecimalData getSurrchg() {
		return surrchg;
	}
	public void setSurrchg(Object what) {
		setSurrchg(what, false);
	}
	public void setSurrchg(Object what, boolean rounded) {
		if (rounded)
			surrchg.setRounded(what);
		else
			surrchg.set(what);
	}	
	public PackedDecimalData getSurrval() {
		return surrval;
	}
	public void setSurrval(Object what) {
		setSurrval(what, false);
	}
	public void setSurrval(Object what, boolean rounded) {
		if (rounded)
			surrval.setRounded(what);
		else
			surrval.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		effdate.clear();
		currfrom.clear();
		validflag.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		cnttype.clear();
		rcdate.clear();
		ownnam.clear();
		payorname.clear();
		hoissdte.clear();
		hissdte.clear();
		agentno.clear();
		agtype.clear();
		billfreq.clear();
		statcode.clear();
		pstatcode.clear();
		ptdate.clear();
		btdate.clear();
		totlprem.clear();
		instprem.clear();
		cshmt.clear();
		loanvalue.clear();
		surrchg.clear();
		surrval.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}