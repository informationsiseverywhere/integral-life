package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sr28y
 * @version 1.0 generated on 30/08/09 06:48
 * @author Quipoz
 */
public class Sr28yScreenVars extends SmartVarModel { 

	
	public FixedLengthStringData dataArea = new FixedLengthStringData(1882);
	public FixedLengthStringData dataFields = new FixedLengthStringData(1242).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData vpmsprop = DD.vpmsprop.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,99);
	public FixedLengthStringData maptofield = DD.maptofield.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData copybk = DD.copybk.copy().isAPartOf(dataFields,139);
	public FixedLengthStringData numoccurs = DD.numoccurs.copy().isAPartOf(dataFields,151);
	public FixedLengthStringData attrslbls = new FixedLengthStringData(960).isAPartOf(dataFields, 154);
	public FixedLengthStringData[] attrslbl = FLSArrayPartOfStructure(16, 60, attrslbls, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(960).isAPartOf(attrslbls, 0, FILLER_REDEFINE);
	public FixedLengthStringData attrslbl01 = DD.attrslbl.copy().isAPartOf(filler, 0);   
	public FixedLengthStringData attrslbl02 = DD.attrslbl.copy().isAPartOf(filler, 60);  
	public FixedLengthStringData attrslbl03 = DD.attrslbl.copy().isAPartOf(filler, 120); 
	public FixedLengthStringData attrslbl04 = DD.attrslbl.copy().isAPartOf(filler, 180); 
	public FixedLengthStringData attrslbl05 = DD.attrslbl.copy().isAPartOf(filler, 240); 
	public FixedLengthStringData attrslbl06 = DD.attrslbl.copy().isAPartOf(filler, 300); 
	public FixedLengthStringData attrslbl07 = DD.attrslbl.copy().isAPartOf(filler, 360); 
	public FixedLengthStringData attrslbl08 = DD.attrslbl.copy().isAPartOf(filler, 420); 
	public FixedLengthStringData attrslbl09 = DD.attrslbl.copy().isAPartOf(filler, 480); 
	public FixedLengthStringData attrslbl10 = DD.attrslbl.copy().isAPartOf(filler, 540); 
	public FixedLengthStringData attrslbl11 = DD.attrslbl.copy().isAPartOf(filler, 600); 
	public FixedLengthStringData attrslbl12 = DD.attrslbl.copy().isAPartOf(filler, 660); 
	public FixedLengthStringData attrslbl13 = DD.attrslbl.copy().isAPartOf(filler, 720); 
	public FixedLengthStringData attrslbl14 = DD.attrslbl.copy().isAPartOf(filler, 780); 
	public FixedLengthStringData attrslbl15 = DD.attrslbl.copy().isAPartOf(filler, 840); 
	public FixedLengthStringData attrslbl16 = DD.attrslbl.copy().isAPartOf(filler, 900);   
	public FixedLengthStringData attrsvalues = new FixedLengthStringData(128).isAPartOf(dataFields, 1114);
	public FixedLengthStringData[] attrsvalue = FLSArrayPartOfStructure(16, 8, attrsvalues, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(128).isAPartOf(attrsvalues, 0, FILLER_REDEFINE);
	public FixedLengthStringData attrsvalue01 = DD.attrsvalue.copy().isAPartOf(filler1, 0);  
	public FixedLengthStringData attrsvalue02 = DD.attrsvalue.copy().isAPartOf(filler1, 8);  
	public FixedLengthStringData attrsvalue03 = DD.attrsvalue.copy().isAPartOf(filler1, 16); 
	public FixedLengthStringData attrsvalue04 = DD.attrsvalue.copy().isAPartOf(filler1, 24); 
	public FixedLengthStringData attrsvalue05 = DD.attrsvalue.copy().isAPartOf(filler1, 32); 
	public FixedLengthStringData attrsvalue06 = DD.attrsvalue.copy().isAPartOf(filler1, 40); 
	public FixedLengthStringData attrsvalue07 = DD.attrsvalue.copy().isAPartOf(filler1, 48); 
	public FixedLengthStringData attrsvalue08 = DD.attrsvalue.copy().isAPartOf(filler1, 56); 
	public FixedLengthStringData attrsvalue09 = DD.attrsvalue.copy().isAPartOf(filler1, 64); 
	public FixedLengthStringData attrsvalue10 = DD.attrsvalue.copy().isAPartOf(filler1, 72); 
	public FixedLengthStringData attrsvalue11 = DD.attrsvalue.copy().isAPartOf(filler1, 80); 
	public FixedLengthStringData attrsvalue12 = DD.attrsvalue.copy().isAPartOf(filler1, 88); 
	public FixedLengthStringData attrsvalue13 = DD.attrsvalue.copy().isAPartOf(filler1, 96); 
	public FixedLengthStringData attrsvalue14 = DD.attrsvalue.copy().isAPartOf(filler1, 104);
	public FixedLengthStringData attrsvalue15 = DD.attrsvalue.copy().isAPartOf(filler1, 112);
	public FixedLengthStringData attrsvalue16 = DD.attrsvalue.copy().isAPartOf(filler1, 120);  
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(160).isAPartOf(dataArea, 1242);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData vpmspropErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData maptofieldErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData copybkErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData numoccursErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData attrslblsErr = new FixedLengthStringData(64).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData[] attrslblErr = FLSArrayPartOfStructure(16, 4, attrslblsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(64).isAPartOf(attrslblsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData attrslbl01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0); 
	public FixedLengthStringData attrslbl02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4); 
	public FixedLengthStringData attrslbl03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8); 
	public FixedLengthStringData attrslbl04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData attrslbl05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData attrslbl06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData attrslbl07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData attrslbl08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData attrslbl09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData attrslbl10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData attrslbl11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData attrslbl12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData attrslbl13Err = new FixedLengthStringData(4).isAPartOf(filler3, 48);
	public FixedLengthStringData attrslbl14Err = new FixedLengthStringData(4).isAPartOf(filler3, 52);
	public FixedLengthStringData attrslbl15Err = new FixedLengthStringData(4).isAPartOf(filler3, 56);
	public FixedLengthStringData attrslbl16Err = new FixedLengthStringData(4).isAPartOf(filler3, 60);
	public FixedLengthStringData attrsvaluesErr = new FixedLengthStringData(64).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData[] attrsvalueErr = FLSArrayPartOfStructure(16, 4, attrsvaluesErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(64).isAPartOf(attrsvaluesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData attrsvalue01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0); 
	public FixedLengthStringData attrsvalue02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4); 
	public FixedLengthStringData attrsvalue03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8); 
	public FixedLengthStringData attrsvalue04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData attrsvalue05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData attrsvalue06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData attrsvalue07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData attrsvalue08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData attrsvalue09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData attrsvalue10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData attrsvalue11Err = new FixedLengthStringData(4).isAPartOf(filler4, 40);
	public FixedLengthStringData attrsvalue12Err = new FixedLengthStringData(4).isAPartOf(filler4, 44);
	public FixedLengthStringData attrsvalue13Err = new FixedLengthStringData(4).isAPartOf(filler4, 48);
	public FixedLengthStringData attrsvalue14Err = new FixedLengthStringData(4).isAPartOf(filler4, 52);
	public FixedLengthStringData attrsvalue15Err = new FixedLengthStringData(4).isAPartOf(filler4, 56);
	public FixedLengthStringData attrsvalue16Err = new FixedLengthStringData(4).isAPartOf(filler4, 60);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(480).isAPartOf(dataArea, 1402);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] vpmspropOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] maptofieldOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] copybkOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] numoccursOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData attrslblsOut = new FixedLengthStringData(192).isAPartOf(outputIndicators, 96);
	public FixedLengthStringData[] attrslblOut = FLSArrayPartOfStructure(16, 12, attrslblsOut, 0);
	public FixedLengthStringData[][] attrslblO = FLSDArrayPartOfArrayStructure(12, 1, attrslblOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(192).isAPartOf(attrslblsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] attrslbl01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);  
	public FixedLengthStringData[] attrslbl02Out = FLSArrayPartOfStructure(12, 1, filler5, 12); 
	public FixedLengthStringData[] attrslbl03Out = FLSArrayPartOfStructure(12, 1, filler5, 24); 
	public FixedLengthStringData[] attrslbl04Out = FLSArrayPartOfStructure(12, 1, filler5, 36); 
	public FixedLengthStringData[] attrslbl05Out = FLSArrayPartOfStructure(12, 1, filler5, 48); 
	public FixedLengthStringData[] attrslbl06Out = FLSArrayPartOfStructure(12, 1, filler5, 60); 
	public FixedLengthStringData[] attrslbl07Out = FLSArrayPartOfStructure(12, 1, filler5, 72); 
	public FixedLengthStringData[] attrslbl08Out = FLSArrayPartOfStructure(12, 1, filler5, 84); 
	public FixedLengthStringData[] attrslbl09Out = FLSArrayPartOfStructure(12, 1, filler5, 96); 
	public FixedLengthStringData[] attrslbl10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] attrslbl11Out = FLSArrayPartOfStructure(12, 1, filler5, 120);
	public FixedLengthStringData[] attrslbl12Out = FLSArrayPartOfStructure(12, 1, filler5, 132);
	public FixedLengthStringData[] attrslbl13Out = FLSArrayPartOfStructure(12, 1, filler5, 144);
	public FixedLengthStringData[] attrslbl14Out = FLSArrayPartOfStructure(12, 1, filler5, 156);
	public FixedLengthStringData[] attrslbl15Out = FLSArrayPartOfStructure(12, 1, filler5, 168);
	public FixedLengthStringData[] attrslbl16Out = FLSArrayPartOfStructure(12, 1, filler5, 180);
	public FixedLengthStringData attrsvaluesOut = new FixedLengthStringData(192).isAPartOf(outputIndicators, 288);
	public FixedLengthStringData[] attrsvalueOut = FLSArrayPartOfStructure(16, 12, attrsvaluesOut, 0);
	public FixedLengthStringData[][] attrsvalueO = FLSDArrayPartOfArrayStructure(12, 1, attrsvalueOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(192).isAPartOf(attrsvaluesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] attrsvalue01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);   
	public FixedLengthStringData[] attrsvalue02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);  
	public FixedLengthStringData[] attrsvalue03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);  
	public FixedLengthStringData[] attrsvalue04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);  
	public FixedLengthStringData[] attrsvalue05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);  
	public FixedLengthStringData[] attrsvalue06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);  
	public FixedLengthStringData[] attrsvalue07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);  
	public FixedLengthStringData[] attrsvalue08Out = FLSArrayPartOfStructure(12, 1, filler6, 84);  
	public FixedLengthStringData[] attrsvalue09Out = FLSArrayPartOfStructure(12, 1, filler6, 96);  
	public FixedLengthStringData[] attrsvalue10Out = FLSArrayPartOfStructure(12, 1, filler6, 108); 
	public FixedLengthStringData[] attrsvalue11Out = FLSArrayPartOfStructure(12, 1, filler6, 120); 
	public FixedLengthStringData[] attrsvalue12Out = FLSArrayPartOfStructure(12, 1, filler6, 132); 
	public FixedLengthStringData[] attrsvalue13Out = FLSArrayPartOfStructure(12, 1, filler6, 144); 
	public FixedLengthStringData[] attrsvalue14Out = FLSArrayPartOfStructure(12, 1, filler6, 156); 
	public FixedLengthStringData[] attrsvalue15Out = FLSArrayPartOfStructure(12, 1, filler6, 168); 
	public FixedLengthStringData[] attrsvalue16Out = FLSArrayPartOfStructure(12, 1, filler6, 180); 

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr28yscreenWritten = new LongData(0);
	public LongData Sr28yprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr28yScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(vpmspropOut,new String[] {"20","21","-20",null, null, null, null, null, null, null, null, null});

		screenFields = new BaseData[] {company, tabl, item, longdesc, vpmsprop, maptofield, copybk, numoccurs, attrslbl01, attrsvalue01, attrslbl02, attrsvalue02, attrslbl03, attrsvalue03, attrslbl04, attrsvalue04, attrslbl05,	 attrsvalue05,  attrslbl06,	 attrsvalue06,  attrslbl07,	 attrsvalue07,  attrslbl08,	 attrsvalue08,  attrslbl09,	 attrsvalue09,  attrslbl10,	 attrsvalue10,  attrslbl11,	 attrsvalue11,  attrslbl12,	 attrsvalue12,  attrslbl13,	 attrsvalue13,  attrslbl14,	 attrsvalue14,  attrslbl15,	 attrsvalue15,  attrslbl16};    
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, vpmspropOut, maptofieldOut, copybkOut, numoccursOut, attrslbl01Out, attrsvalue01Out, attrslbl02Out, attrsvalue02Out, attrslbl03Out, attrsvalue03Out, attrslbl04Out, attrsvalue04Out, attrslbl05Out,	attrsvalue05Out, attrslbl06Out,	attrsvalue06Out, attrslbl07Out,	attrsvalue07Out, attrslbl08Out,	attrsvalue08Out, attrslbl09Out,	attrsvalue09Out, attrslbl10Out,	attrsvalue10Out, attrslbl11Out,	attrsvalue11Out, attrslbl12Out,	attrsvalue12Out, attrslbl13Out,	attrsvalue13Out, attrslbl14Out,	attrsvalue14Out, attrslbl15Out,	attrsvalue15Out, attrslbl16Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, vpmspropErr, maptofieldErr, copybkErr, numoccursErr, attrslbl01Err, attrsvalue01Err, attrslbl02Err, attrsvalue02Err, attrslbl03Err, attrsvalue03Err, attrslbl04Err, attrsvalue04Err,attrslbl05Err, attrsvalue05Err,  attrslbl06Err, attrsvalue06Err,  attrslbl07Err, attrsvalue07Err,  attrslbl08Err, attrsvalue08Err,  attrslbl09Err, attrsvalue09Err,  attrslbl10Err, attrsvalue10Err,  attrslbl11Err, attrsvalue11Err,  attrslbl12Err, attrsvalue12Err,  attrslbl13Err, attrsvalue13Err,  attrslbl14Err, attrsvalue14Err,  attrslbl15Err, attrsvalue15Err,  attrslbl16Err, attrsvalue16Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr28yscreen.class;
		protectRecord = Sr28yprotect.class;
	}

}
