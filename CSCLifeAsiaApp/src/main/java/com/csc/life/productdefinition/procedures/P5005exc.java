/*
 * File: P5005exc.java
 * Date: 29 August 2009 23:53:46
 * Author: Quipoz Limited
 * 
 * Class transformed from P5005EXC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.dataaccess.MliasecTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.MliapfDAO;
import com.csc.life.underwriting.dataaccess.dao.UndqpfDAO;
import com.csc.life.underwriting.dataaccess.model.Undqpf;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 *
 *  This  subroutine  is  called  from  OPTSWCH Table T1661. It
 *  determines whether any LIA records exist for the component
 *  being enquired upon in PM526.
 *
 *
 ***********************************************************************
 *                                                                     *
 * </pre>
 */
public class P5005exc extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P5005EXC");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	/* FORMATS */
	private String cltsrec = "CLTSREC";
	private String mliasecrec = "MLIASECREC";
	private PackedDecimalData optsDteeff = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaChkpStatuz = new FixedLengthStringData(4);
	/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	/*Contract Enquiry - Coverage Details.*/
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	/*Substandard & Hospitalisation on IDNO*/
	private MliasecTableDAM mliasecIO = new MliasecTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private Clntpf clntpf;
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private MliapfDAO mliapfDAO = getApplicationContext().getBean("mliapfDAO", MliapfDAO.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		errorProg610
	}

	public P5005exc() {
		super();
	}

	public void mainline(Object... parmArray)
	{
		wsaaChkpStatuz = convertAndSetParam(wsaaChkpStatuz, parmArray, 1);
		optsDteeff = convertAndSetParam(optsDteeff, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

	protected void mainline000()
	{
		try {
			start010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

	protected void start010()
	{
		lifelnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
				&& isNE(lifelnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}

		clntpf = clntpfDAO.searchClntRecord("CN",lifelnbIO.getChdrcoy().toString(),lifelnbIO.getLifcnum().toString());

		if (null == clntpf) {
			wsaaChkpStatuz.set("NFND");
			goTo(GotoLabel.exit090);
		}

		boolean isExist = false;

		isExist = mliapfDAO.isExistMliasec(cltsIO.getSecuityno().toString());

		if (isExist) {
			wsaaChkpStatuz.set("DFND");
		}
		else {
			wsaaChkpStatuz.set("NFND");
			goTo(GotoLabel.exit090);
		}
	}

	protected void exit090()
	{
		exitProgram();
	}

	protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

	protected void errorProg610()
	{
		wsaaChkpStatuz.set(varcom.bomb);
		exitProgram();
	}

	protected void exit690()
	{
		exitProgram();
	}
}
