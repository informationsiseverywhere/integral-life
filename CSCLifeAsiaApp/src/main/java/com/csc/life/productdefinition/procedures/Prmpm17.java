/*
 * File: Prmpm17.java
 * Date: 30 August 2009 1:59:06
 * Author: Quipoz Limited
 * 
 * Class transformed from PRMPM17.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.ClexTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.tablestructures.T5658rec;
import com.csc.life.newbusiness.tablestructures.Th609rec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.tablestructures.T5585rec;
import com.csc.life.productdefinition.tablestructures.T5659rec;
import com.csc.life.productdefinition.tablestructures.Tj698rec;
import com.csc.life.productdefinition.tablestructures.Tj699rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*          PREMIUM CALCULATION METHOD 17
*
* Currently this program is called by the 'PREMIUM' subroutine,
* using the premium method as the access mode. The PREMIUM
* program wll be replaced by an ITEM entry on table T5675
* (PRMPM02 will be one of these).
*
* This calculation will be used for joint-life cases.
*
* PROCESSING.
* ----------
*
* - Read the LIFE file in order to access the J/L ages and sex:
*
*       - read the LIFE details using 'LIFELNB' (life number from
*         COVTTRM, joint-life number '00') store the life age AND
*         sex. Read the 'LIFELNB' (joint-life number '01') and
*         store the joint-life's age and sex.
*
*
* - Access table T5585 (keyed on coverage/rider)
* - This table may be dated, if so, use the version:
*
*        1) Rating date.
*
* Calculate an equivalent equal age as follows:
*
*         - One of the lives, both or none of the lives may be
*           adjusted.
*
*  An example follows:
*
*         - If the sex of both parties is female and the sex
*           adjustment indicator on T5585 is 'F', then both ages
*           must be adjusted . This adjustment value will be
*           found on T5585.
*
*         - If only one of the parties is female, then only her
*           age will be adjusted.
*
*         - If both parties are male and the sex adjustment
*           value on T5585 is 'F', then the ages will not be
*           adjusted.
*
*         End of example.
*
*         - Adjust the age of the life by the adjustment amount,
*           if an adjustment applies (i.e. if the sex of the life
*           equals the sex of the life to be adjusted (T5585).
*
*         - The reverse will also be true for males.
*
* Calculate the highest / lowest adjusted age.
*
*         - if the life-age is greater than the joint-life age,
*              - subtract J/L age from the life-age
*              otherwise
*              - subtract the life-age from the J/L age.
*
* Translate this difference in ages.
*
*         - This difference will be used to look up T5585 and
*           the 'addition to age' obtained.
*         - The age to be applied indicator will also be
*           obtained.
*
* Apply adjustment.
*
*         - Apply the adjustment age (addition to age) to the
*           indicated age be it the higher or the lower age.
*
*         - This age will now become the ANB @ RCD.
*
*
* CALCULATE-BASIC-ANNNUAL-PREMIUM (and apply age rates)
* (Age, Sex & Duration taken from linkage)
*
* Obtain the age rates from the (LEXT) record.
*
*  - read all the LEXT records for this contract, life and
*  coverage into the working-storage table. Compute the
*  adjusted age as being the summation of the LEXT age
*  rates plus the ANB @ RCD.
*
* Use the age calculated above to access the table T5658 and
* check the following:
*
*
*  - that the basic annual premium (indexed by year) from
*  the T5658 table is not zero. If it is zero, then display
*  an error message and skip the additional procedures.
*  Otherwise store the premium as the (BAP).
*
* - we should now have an age rated BAP.
*
* APPLY STAFF-DISCOUNT
*
* - Use the contract owner or join owner to read client extra
*   details.
* - if staff flag is 'Y' then read TH609 with contract type
*   to get discount percentage.
* - discount percentage is classified into base premium rate or
*   instalment premium.
* - apply staff discount to BAP * (100 - staff discount) / 100
*
* APPLY-RATE-PER-MILLE-LOADINGS
*
* - sum the rates per mille from the LEXT W/S table.
*
*  - add rates per mille to the BAP
*
* - we should now have a BAP with rates / mille applied.
*
* APPLY-DISCOUNT.
*
* Access the discount table T5659 using the key:-
*
* - Discount method from T5658 concatenated with currency.
*
*  - check the sum insured against the volume band ranges
*  and when within a range store the discount amount.
*
*  - compute the BAP as the BAP - discount
*
* - we should now have a BAP with discount applied.
*
* APPLY-REBATE% based on frequency .
*
*  - Read TJ698 for the component and retrieve Rebate %  for
*
*  - the frequency and apply it on BAP
*
* - we should now have a BAP with rebate given.
*
* APPLY-PREMIUM-UNIT
*
* - Obtain the risk-unit from T5658
*
*  - multiply BAP by the sum-insured and divide
*    by the risk-unit
*
* - we should now have a BAP with premium applied.
*
* APPLY-PERCENTAGE-LOADINGS
*
* - from the LEXT working-storage (W/S) table apply the
* percentage loadings. For each loading entry on the table
* compute the BAP as follows:
*
*  BAP = BAP * loading percentage / 100.
*
* - we should now have a loaded BAP.
*
* CALCULATE-INSTALMENT-PREMIUM.
*
* Determine which billing frequency to use.
*
* compute the basic-instalment-premium (BIP) as:-
*
* basic-premium * factor (FACTOR is from T5658).
*
* CALCULATE-ROUNDING.
*
* - round up depending on the rounding factor (obtained from
* the T5659 table).
*
* - if the prem-unit from T5658 is greater than zero, then
* compute the BIP as the rounded number / the premium-unit
* (from T5658). The premium unit is the quantity in which the
* currency is denominated.
*
* CALCULATE-THE-ANNUAL-PREMIUM.
*
* There is no need to calculate the annual premium, because
* at issue time, when the COVR records are being created from
*the COVT records, the annual premium will then be calculated.
*
*
******************************************************************
*
* </pre>
*/
public class Prmpm17 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "PRMPM02";
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0);
	private ZonedDecimalData wsaaAge00Adjusted = new ZonedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaRebatePcnt = new PackedDecimalData(6, 4);
	private ZonedDecimalData wsaaAge01Adjusted = new ZonedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaAgeDifference = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private String wsaaBasicPremium = "";
		/* ERRORS */
	private static final String e107 = "E107";
	private static final String f261 = "F261";
	private static final String f264 = "F264";
	private static final String f265 = "F265";
	private static final String h043 = "H043";
	private static final String f262 = "F262";
	private static final String f110 = "F110";
	private static final String jl00 = "JL00";
	private static final String jl01 = "JL01";
		/* TABLES */
	private static final String t5585 = "T5585";
	private static final String t5658 = "T5658";
	private static final String t5659 = "T5659";
	private static final String th609 = "TH609";
	private static final String tj698 = "TJ698";
	private static final String tj699 = "TJ699";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String lextrec = "LEXTREC";
	private static final String clexrec = "CLEXREC";
	private static final String chdrlnbrec = "CHDRLNBREC";

		/* WSAA-LEXT-OPPC-RECS */
	private FixedLengthStringData[] wsaaLextOppcs = FLSInittedArray (8, 3);
	private PackedDecimalData[] wsaaLextOppc = PDArrayPartOfArrayStructure(5, 2, wsaaLextOppcs, 0);
	private PackedDecimalData wsaaSub00 = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaSub01 = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaAgerateTot = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaRatesPerMillieTot = new PackedDecimalData(7, 0).init(0);
	private PackedDecimalData wsaaAdjustedAge = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaAgeNoLoad = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaDiscountAmt = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaBap = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaBip = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaModalFactor = new PackedDecimalData(6, 4);
	private ZonedDecimalData wsaaStaffDiscount = new ZonedDecimalData(5, 2).init(0);
	private FixedLengthStringData wsaaStaffFlag = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaRoundNum = new ZonedDecimalData(13, 2).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(13).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound100 = new ZonedDecimalData(5, 2).isAPartOf(filler, 8).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(13).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound10 = new ZonedDecimalData(4, 2).isAPartOf(filler2, 9).setUnsigned();

	private FixedLengthStringData filler4 = new FixedLengthStringData(13).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRound1 = new ZonedDecimalData(3, 2).isAPartOf(filler4, 10).setUnsigned();

	private FixedLengthStringData filler6 = new FixedLengthStringData(13).isAPartOf(wsaaRoundNum, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRoundDec = new ZonedDecimalData(2, 2).isAPartOf(filler6, 11);

	private FixedLengthStringData wsaaTj699Key = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaTj699Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTj699Key, 0);
	private FixedLengthStringData wsaaTj699Mop = new FixedLengthStringData(1).isAPartOf(wsaaTj699Key, 4);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaT5658Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5658Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5658Key, 0);
	private ZonedDecimalData wsaaT5658Ageterm = new ZonedDecimalData(2, 0).isAPartOf(wsaaT5658Key, 4).setUnsigned();
	private FixedLengthStringData wsaaT5658Mortcls = new FixedLengthStringData(1).isAPartOf(wsaaT5658Key, 6);
	private FixedLengthStringData wsaaT5658Sex = new FixedLengthStringData(1).isAPartOf(wsaaT5658Key, 7);

	private FixedLengthStringData wsaaT5659Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaDisccntmeth = new FixedLengthStringData(4).isAPartOf(wsaaT5659Key, 0);
	private FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(3).isAPartOf(wsaaT5659Key, 4);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ClexTableDAM clexIO = new ClexTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T5585rec t5585rec = new T5585rec();
	private Varcom varcom = new Varcom();
	private T5658rec t5658rec = new T5658rec();
	private T5659rec t5659rec = new T5659rec();
	private Th609rec th609rec = new Th609rec();
	private Tj698rec tj698rec = new Tj698rec();
	private Tj699rec tj699rec = new Tj699rec();
	private Premiumrec premiumrec = new Premiumrec();
	/*BRD-306 START */
	private PackedDecimalData wsaaPremiumAdjustTot = new PackedDecimalData(7, 0).init(0);
	boolean isLoadingAva = false;
	/*BRD-306 END */
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		t5585115, 
		adjust192, 
		adjust196, 
		go198, 
		readLext210, 
		checkT5658Insprm230, 
		checkSumInsuredRange430, 
		exit490, 
		calculateLoadings610, 
		a201Read
	}

	public Prmpm17() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		premiumrec.premiumRec = convertAndSetParam(premiumrec.premiumRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		/*INIT*/
		premiumrec.statuz.set("****");
		syserrrec.subrname.set(wsaaSubr);
		calculate050();
		/*EXIT*/
		exitProgram();
	}

protected void calculate050()
	{
		para051();
	}

protected void para051()
	{
		initialize100();
		wsaaBasicPremium = "N";
		if (isEQ(premiumrec.statuz, "****")) {
			r100StaffDiscount();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			basicAnnualPremium200(); 
			/*BRD-306 START */
			premiumrec.adjustageamt.set(wsaaBap);
			/*BRD-306 END */
		}
		if (isEQ(premiumrec.statuz, "****")
		&& isEQ(wsaaStaffFlag, "Y")
		&& isNE(wsaaStaffDiscount, ZERO)
		&& isEQ(th609rec.indic, "B")) {
			r300BaseRateStaffDisc();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			ratesPerMillieLoadings300();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			volumeDiscountBap1400();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			rebateOnFrequency320();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			premiumUnit500();
			/*BRD-306 START */
			compute(premiumrec.loadper, 2).set(wsaaBap);
			/*BRD-306 END */
		}
		if (isEQ(premiumrec.statuz, "****")) {
			percentageLoadings600();			
		}
		if (isEQ(premiumrec.statuz, "****")) {
			instalmentPremium700();
		}
		if (isEQ(premiumrec.statuz, "****")
		&& isEQ(wsaaStaffFlag, "Y")
		&& isNE(wsaaStaffDiscount, ZERO)
		&& isEQ(th609rec.indic, "I")) {
			r400InstalmentStaffDisc();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			rounding800();
		}
		/*BRD-306 START */
		if (isEQ(premiumrec.statuz, "****")) {
			premiumAdjustedLoadings900();
		}
		/*BRD-306 END */
		wsaaBasicPremium = "Y";
		if (isEQ(premiumrec.statuz, "****")) {
			basicAnnualPremium200();
			/*BRD-306 START */
			//Calculate adjusted age amount
			compute(premiumrec.adjustageamt, 2).set(sub(premiumrec.adjustageamt, wsaaBap));
			/*BRD-306 END */
		}
		if (isEQ(premiumrec.statuz, "****")
		&& isEQ(wsaaStaffFlag, "Y")
		&& isNE(wsaaStaffDiscount, ZERO)
		&& isEQ(th609rec.indic, "B")) {
			r300BaseRateStaffDisc();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			volumeDiscountBap1400();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			rebateOnFrequency320();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			premiumUnit500();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			instalmentPremium700();
		}
		if (isEQ(premiumrec.statuz, "****")
		&& isEQ(wsaaStaffFlag, "Y")
		&& isNE(wsaaStaffDiscount, ZERO)
		&& isEQ(th609rec.indic, "I")) {
			r400InstalmentStaffDisc();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			rounding800();
		}
		/* Calculate the loaded premium as the gross premium minus the*/
		/* basic premium.*/
		compute(premiumrec.calcLoaPrem, 2).set(sub(premiumrec.calcPrem, premiumrec.calcBasPrem));
		/*BRD-306 START */
		if(isLoadingAva == true)
		compute(premiumrec.loadper, 2).set(sub(premiumrec.calcLoaPrem,(add(wsaaRatesPerMillieTot,wsaaPremiumAdjustTot,premiumrec.adjustageamt))));
		
		/*BRD-306 END */
	}

protected void initialize100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para110();
				case t5585115: 
					t5585115();
					readT5658140();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para110()
	{
		/* Initialise all working storage fields and set keys to read*/
		/* tables. Include a table (occurs 8) to hold the Options/Extras*/
		/* (LEXT) record details.*/
		wsaaRatesPerMillieTot.set(ZERO);
		/*BRD-306 START */
		wsaaPremiumAdjustTot.set(ZERO);
		/*BRD-306 END */
		wsaaBap.set(ZERO);
		wsaaBip.set(ZERO);
		wsaaAdjustedAge.set(ZERO);
		wsaaAgeNoLoad.set(ZERO);
		wsaaDiscountAmt.set(ZERO);
		wsaaAgerateTot.set(ZERO);
		wsaaSub.set(ZERO);
		/*BRD-306 START */
		premiumrec.fltmort.set(ZERO);
		premiumrec.loadper.set(ZERO);
		/*BRD-306 END */
		wsaaT5659Key.set(SPACES);
		for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
			clearLextRecs110();
		}
		goTo(GotoLabel.t5585115);
	}

protected void clearLextRecs110()
	{
		wsaaSub.add(1);
		wsaaLextOppc[wsaaSub.toInt()].set(ZERO);
	}

protected void t5585115()
	{
		readT5585135();
		wsaaSub00.set(ZERO);
		wsaaSub01.set(ZERO);
		wsaaAge00Adjusted.set(premiumrec.lage);
		wsaaAge01Adjusted.set(premiumrec.jlage);
		if (isEQ(premiumrec.lsex, t5585rec.sexageadj)) {
			adjustAge00191();
		}
		if (isEQ(premiumrec.jlsex, t5585rec.sexageadj)) {
			adjustAge01195();
		}
		compute(wsaaAgeDifference, 0).set(sub(wsaaAge00Adjusted, wsaaAge01Adjusted));
		wsaaSub.set(0);
		ageDifferance198();
	}

protected void readT5658140()
	{
		/*    READ TABLE T5658. THIS TABLE CONTAINS THE PARAMETERS TO BE*/
		/*    USED IN THE CALCULATION OF THE BASIC ANNUAL PREMIUM FOR*/
		/*    COVERAGE/RIDER COMPONENTS.*/
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5658);
		wsaaT5658Crtable.set(premiumrec.crtable);
		wsaaT5658Ageterm.set(premiumrec.duration);
		wsaaT5658Mortcls.set(premiumrec.mortcls);
		wsaaT5658Sex.set(SPACES);
		itdmIO.setItemitem(wsaaT5658Key);
		itdmIO.setItmfrm(premiumrec.ratingdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), "****")
		&& isNE(itdmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(wsaaT5658Key, itdmIO.getItemitem())
		|| isNE(premiumrec.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5658)
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			premiumrec.statuz.set(f265);
		}
		else {
			t5658rec.t5658Rec.set(itdmIO.getGenarea());
		}
	}

protected void readT5585135()
	{
		readTable136();
	}

protected void readTable136()
	{
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5585);
		itdmIO.setItemitem(premiumrec.crtable);
		itdmIO.setItmfrm(premiumrec.ratingdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), "****")
		&& isNE(itdmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(premiumrec.crtable, itdmIO.getItemitem())
		|| isNE(itdmIO.getItemtabl(), t5585)
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(premiumrec.crtable);
			syserrrec.statuz.set(f261);
			fatalError9000();
		}
		t5585rec.t5585Rec.set(itdmIO.getGenarea());
		wsaaSub.set(0);
	}

protected void adjustAge00191()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case adjust192: 
					adjust192();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void adjust192()
	{
		wsaaSub00.add(1);
		if (isGT(wsaaSub00, 9)) {
			premiumrec.statuz.set(h043);
			return ;
		}
		if (isLTE(premiumrec.lage, t5585rec.agelimit[wsaaSub00.toInt()])) {
			compute(wsaaAge00Adjusted, 0).set(add(premiumrec.lage, t5585rec.ageadj[wsaaSub00.toInt()]));
		}
		else {
			goTo(GotoLabel.adjust192);
		}
		/*EXIT*/
	}

protected void adjustAge01195()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case adjust196: 
					adjust196();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void adjust196()
	{
		wsaaSub01.add(1);
		if (isGT(wsaaSub01, 9)) {
			premiumrec.statuz.set(h043);
			return ;
		}
		if (isLTE(premiumrec.jlage, t5585rec.agelimit[wsaaSub01.toInt()])) {
			compute(wsaaAge01Adjusted, 0).set(add(premiumrec.jlage, t5585rec.ageadj[wsaaSub01.toInt()]));
		}
		else {
			goTo(GotoLabel.adjust196);
		}
		/*EXIT*/
	}

protected void ageDifferance198()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case go198: 
					go198();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go198()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 18)) {
			premiumrec.statuz.set(f262);
			return ;
		}
		if (isGT(wsaaAgeDifference, t5585rec.agedif[wsaaSub.toInt()])) {
			goTo(GotoLabel.go198);
		}
		if (isEQ(t5585rec.hghlowage, "H")) {
			if (isGT(wsaaAge00Adjusted, wsaaAge01Adjusted)) {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()], wsaaAge00Adjusted));
			}
			else {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()], wsaaAge01Adjusted));
			}
		}
		if (isEQ(t5585rec.hghlowage, "L")) {
			if (isLTE(wsaaAge00Adjusted, wsaaAge01Adjusted)) {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()], wsaaAge00Adjusted));
			}
			else {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()], wsaaAge01Adjusted));
			}
		}
	}

protected void basicAnnualPremium200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					setupLextKey200();
				case readLext210: 
					readLext210();
				case checkT5658Insprm230: 
					checkT5658Insprm230();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setupLextKey200()
	{
		/* If calculating the basic premium, do not adjust the age.*/
		if (isEQ(wsaaBasicPremium, "Y")) {
			/*     MOVE CPRM-LAGE          TO WSAA-ADJUSTED-AGE             */
			/* Calculate base on the age before loading has already added   */
			wsaaAdjustedAge.set(wsaaAgeNoLoad);
			goTo(GotoLabel.checkT5658Insprm230);
		}
		else {
			wsaaAgeNoLoad.set(wsaaAdjustedAge);
		}
		/* Obtain the age rates from the (LEXT) record.*/
		/*  - read all the LEXT records for this contract, life and*/
		/*  coverage into the working-storage table. Compute the*/
		/*  adjusted age as being the summation of the LEXT age*/
		/*  rates plus the ANB @ RCD.*/
		lextIO.setChdrcoy(premiumrec.chdrChdrcoy);
		lextIO.setChdrnum(premiumrec.chdrChdrnum);
		lextIO.setLife(premiumrec.lifeLife);
		lextIO.setCoverage(premiumrec.covrCoverage);
		lextIO.setRider(premiumrec.covrRider);
		lextIO.setSeqnbr(ZERO);
		lextIO.setFormat(lextrec);
		lextIO.setFunction("BEGN");
		wsaaSub.set(0);
	}

protected void readLext210()
	{
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), "****")
		&& isNE(lextIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(lextIO.getParams());
			fatalError9000();
		}
		if (isEQ(lextIO.getStatuz(), "ENDP")) {
			goTo(GotoLabel.checkT5658Insprm230);
		}
		if (isEQ(lextIO.getChdrcoy(), premiumrec.chdrChdrcoy)
		&& isEQ(lextIO.getChdrnum(), premiumrec.chdrChdrnum)
		&& isEQ(lextIO.getLife(), premiumrec.lifeLife)
		&& isEQ(lextIO.getCoverage(), premiumrec.covrCoverage)
		&& isEQ(lextIO.getRider(), premiumrec.covrRider)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.checkT5658Insprm230);
		}
		if (isEQ(lextIO.getStatuz(), "****")) {
			if (isEQ(premiumrec.reasind, "2")
			&& isEQ(lextIO.getReasind(), "1")) {
				goTo(GotoLabel.checkT5658Insprm230);
			}
			else {
				if (isNE(premiumrec.reasind, "2")
				&& isEQ(lextIO.getReasind(), "2")) {
					goTo(GotoLabel.checkT5658Insprm230);
				}
			}
		}
		/*  Skip any expired special terms.*/
		lextIO.setFunction("NEXTR");
		if (isLTE(lextIO.getExtCessDate(), premiumrec.reRateDate)) {
			goTo(GotoLabel.readLext210);
		}
		wsaaSub.add(1);
		wsaaLextOppc[wsaaSub.toInt()].set(lextIO.getOppc());
		wsaaRatesPerMillieTot.add(lextIO.getInsprm());
		/*BRD-306 START */
		wsaaPremiumAdjustTot.add(lextIO.getPremadj());
		/*BRD-306 END */
		wsaaAgerateTot.add(lextIO.getAgerate());
		goTo(GotoLabel.readLext210);
	}

protected void checkT5658Insprm230()
	{
		/* Use the age calculated above to access the table T5658 and*/
		/* check the following:*/
		/*  - that the basic annual premium (indexed by year) from*/
		/*  the T5658 table is not zero. If it is zero, then display*/
		/*  an error message and skip the additional procedures.*/
		/*  Otherwise store the premium as the (BAP).*/
		/*    ADJUST AGE*/
		wsaaAdjustedAge.add(wsaaAgerateTot);
		/*    IF WSAA-ADJUSTED-AGE        < 1 OR*/
		if (isLT(wsaaAdjustedAge, 0)
		|| isGT(wsaaAdjustedAge, 110)) {
			premiumrec.statuz.set(e107);
			return ;
		}
		/*  Check for adjusted age = 0; move the premium rate*/
		if (isEQ(wsaaAdjustedAge, 0)) {
			if (isEQ(t5658rec.insprem, ZERO)) {
				premiumrec.statuz.set(e107);
			}
			else {
				wsaaBap.set(t5658rec.insprem);
			}
			return ;
		}
		/*  Note - only 99 out of the 100 rates fit in the occurs table.*/
		/*    IF WSAA-ADJUSTED-AGE = 100                                   */
		/*       IF T5658-INSTPR = ZERO                                    */
		/*          MOVE E107                TO CPRM-STATUZ                */
		/*       ELSE                                                      */
		/*          MOVE T5658-INSTPR        TO WSAA-BAP                   */
		/*  Extend the age band to 110.                                    */
		if (isGTE(wsaaAdjustedAge, 100)
		&& isLTE(wsaaAdjustedAge, 110)) {
			if ((setPrecision(t5658rec.instpr[sub(wsaaAdjustedAge, 99).toInt()], 0)
			&& isEQ(t5658rec.instpr[sub(wsaaAdjustedAge, 99).toInt()], ZERO))) {
				premiumrec.statuz.set(e107);
			}
			else {
				compute(wsaaBap, 2).set(t5658rec.instpr[sub(wsaaAdjustedAge, 99).toInt()]);
			}
		}
		else {
			if (isEQ(t5658rec.insprm[wsaaAdjustedAge.toInt()], ZERO)) {
				premiumrec.statuz.set(e107);
			}
			else {
				/*        Base on age without loading                            */
				if (isEQ(wsaaBasicPremium, "Y")) {
					wsaaBap.set(t5658rec.insprm[wsaaAgeNoLoad.toInt()]);
				}
				else {
					wsaaBap.set(t5658rec.insprm[wsaaAdjustedAge.toInt()]);
				}
			}
		}
	}

protected void ratesPerMillieLoadings300()
	{
		/*PARA*/
		/* APPLY-RATE-PER-MILLE-LOADINGS*/
		/* - sum the rates per mille from the LEXT W/S table.*/
		/*  - add rates per mille to the BAP*/
		compute(wsaaBap, 2).set(add(wsaaRatesPerMillieTot, wsaaBap));
		/*BRD-306 START */
		compute(premiumrec.rateadj, 2).set(wsaaRatesPerMillieTot);
		/*BRD-306 END */
		/*EXIT*/
	}

protected void rebateOnFrequency320()
	{
		/*PARA*/
		/* APPLY-REBATE % FROM TJ698*/
		wsaaRebatePcnt.set(ZERO);
		a190ReadTj698();
		compute(wsaaBap, 4).set(div(mult(wsaaBap, (sub(100, wsaaRebatePcnt))), 100));
		/*EXIT*/
	}

protected void volumeDiscountBap1400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readT5659410();
				case checkSumInsuredRange430: 
					checkSumInsuredRange430();
				case exit490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readT5659410()
	{
		/* APPLY-DISCOUNT.*/
		/* Access the discount table T5659 using the key:-*/
		/* - Discount method from T5658 concatenated with currency.*/
		/*  - check the sum insurred against the volume band ranges*/
		/*  and when within a range store the discount amount.*/
		/*  - compute the BAP as the BAP - discount*/
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5659);
		wsaaDisccntmeth.set(t5658rec.disccntmeth);
		wsaaCurrcode.set(premiumrec.currcode);
		itdmIO.setItemitem(wsaaT5659Key);
		itdmIO.setItmfrm(premiumrec.ratingdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), "****")
		&& isNE(itdmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(premiumrec.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5659)
		|| isNE(wsaaT5659Key, itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			premiumrec.statuz.set(f264);
			goTo(GotoLabel.exit490);
		}
		t5659rec.t5659Rec.set(itdmIO.getGenarea());
		wsaaSub.set(0);
	}

protected void checkSumInsuredRange430()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 4)) {
			return ;
		}
		if (isLT(premiumrec.sumin, t5659rec.volbanfr[wsaaSub.toInt()])
		|| isGT(premiumrec.sumin, t5659rec.volbanto[wsaaSub.toInt()])) {
			goTo(GotoLabel.checkSumInsuredRange430);
		}
		else {
			wsaaDiscountAmt.set(t5659rec.volbanle[wsaaSub.toInt()]);
		}
		compute(wsaaBap, 2).set(sub(wsaaBap, wsaaDiscountAmt));
	}

protected void premiumUnit500()
	{
		/*PARA*/
		/* APPLY-PREMIUM-UNIT*/
		/* - Obtain the risk-unit from T5658*/
		/*  - multiply BAP by the sum-insured and divide*/
		/*    by the risk-unit*/
		if (isEQ(t5658rec.unit, ZERO)) {
			premiumrec.statuz.set(f110);
		}
		else {
			compute(wsaaBap, 2).set((div((mult(wsaaBap, premiumrec.sumin)), t5658rec.unit)));
		}
		/*EXIT*/
	}

protected void percentageLoadings600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para600();
				case calculateLoadings610: 
					calculateLoadings610();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para600()
	{
		/* APPLY-PERCENTAGE-LOADINGS*/
		/* - from the LEXT working-storage (W/S) table apply the*/
		/* percentage loadings. For each loading entry on the table*/
		/* compute the BAP as follows:*/
		/*  BAP = BAP * loading percentage / 100.*/
		wsaaSub.set(0);
	}

protected void calculateLoadings610()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 8)) {
			return ;
		}
		if (isNE(wsaaLextOppc[wsaaSub.toInt()], 0)) {
			compute(wsaaBap, 2).set((div((mult(wsaaBap, wsaaLextOppc[wsaaSub.toInt()])), 100)));
			isLoadingAva = true;
		}
		goTo(GotoLabel.calculateLoadings610);
	}

protected void instalmentPremium700()
	{
		/*PARA*/
		/* CALCULATE-INSTALMENT-PREMIUM.*/
		/* Determine which billing frequency to use.*/
		/* compute the basic-instalment-premium (BIP) as:-*/
		/* basic-premium * factor (FACTOR is from T5658).*/
		wsaaBip.set(0);
		/*INSTALMENT-PREM*/
		wsaaModalFactor.set(0);
		a200ReadTj699();
		if (isEQ(wsaaModalFactor, 0)) {
			premiumrec.statuz.set(jl00);
		}
		else {
			compute(wsaaBip, 4).set(mult(wsaaBap, wsaaModalFactor));
		}
		/*EXIT*/
	}

protected void rounding800()
	{
		para800();
		rounded820();
	}

protected void para800()
	{
		/* CALCULATE-ROUNDING.*/
		/* - round up depending on the rounding factor (obtained from*/
		/* the T5659 table).*/
		/* - if the prem-unit from T5658 is greater than zero, then*/
		/* compute the BIP as the rounded number / the premium-unit*/
		/* (from T5658). The premium unit is the quantity in which the*/
		/* currency is dominated in.*/
		wsaaRoundNum.set(wsaaBip);
		if (isEQ(t5659rec.rndfact, 1)
		|| isEQ(t5659rec.rndfact, 0)) {
			if (isLT(wsaaRoundDec, .5)) {
				wsaaRoundDec.set(0);
			}
			else {
				wsaaRoundNum.add(1);
				wsaaRoundDec.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact, 10)) {
			if (isLT(wsaaRound1, 5)) {
				wsaaRound1.set(0);
			}
			else {
				wsaaRoundNum.add(10);
				wsaaRound1.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact, 100)) {
			if (isLT(wsaaRound10, 50)) {
				wsaaRound10.set(0);
			}
			else {
				wsaaRoundNum.add(100);
				wsaaRound10.set(0);
			}
		}
		if (isEQ(t5659rec.rndfact, 1000)) {
			if (isLT(wsaaRound100, 500)) {
				wsaaRound100.set(0);
			}
			else {
				wsaaRoundNum.add(100);
				wsaaRound100.set(0);
			}
		}
	}

protected void rounded820()
	{
		wsaaBip.set(wsaaRoundNum);
		if (isEQ(t5658rec.premUnit, 0)) {
			premiumrec.calcPrem.set(wsaaBip);
		}
		else {
			if (isEQ(wsaaBasicPremium, "Y")) {
				compute(premiumrec.calcBasPrem, 2).set((div(wsaaBip, t5658rec.premUnit)));
			}
			else {
				compute(premiumrec.calcPrem, 2).set((div(wsaaBip, t5658rec.premUnit)));
			}
		}
		/*EXIT*/
	}

/*BRD-306 START */
protected void premiumAdjustedLoadings900()
	{
		/*PARA*/
		/* APPLY-PREMIUM-ADJUSTED-LOADINGS*/
		/* - sum os the Premium adjusted records from the LEXT W/S table.*/
		/*  - add rates per mille to the BAP*/		
		compute(premiumrec.calcPrem, 2).set(add(wsaaPremiumAdjustTot, premiumrec.calcPrem));
		/*EXIT*/
		
		compute(premiumrec.premadj,2).set(wsaaPremiumAdjustTot);
	}
/*BRD-306 END */
protected void r100StaffDiscount()
	{
		r110ReadChdrlnb();
		r120ReadClex();
	}

protected void r110ReadChdrlnb()
	{
		/* Read CHDRLNB for CHDRLNB-COWNNUM or CHDRLNB-JOWNNUM.*/
		/*    Retrieve contract header information.*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError9000();
		}
		/* In some pgms that call the premium calc. routine CHDRLNB*/
		/* is not kept stored(KEEPS/READS) so we'll have to read the*/
		/* CHDRLNB*/
		if (isEQ(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			chdrlnbIO.setChdrcoy(premiumrec.chdrChdrcoy);
			chdrlnbIO.setChdrnum(premiumrec.chdrChdrnum);
			chdrlnbIO.setFunction(varcom.readr);
			chdrlnbIO.setFormat(chdrlnbrec);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlnbIO.getParams());
				fatalError9000();
			}
		}
	}

protected void r120ReadClex()
	{
		if (isEQ(chdrlnbIO.getCownnum(), SPACES)) {
			return ;
		}
		/* To check staff flag - Read the client extra details.*/
		clexIO.setClntcoy(chdrlnbIO.getCowncoy());
		clexIO.setClntnum(chdrlnbIO.getCownnum());
		clexIO.setClntpfx(chdrlnbIO.getCownpfx());
		clexIO.setFunction(varcom.readr);
		clexIO.setFormat(clexrec);
		SmartFileCode.execute(appVars, clexIO);
		if (isNE(clexIO.getStatuz(), varcom.oK)
		&& isNE(clexIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(clexIO.getParams());
			fatalError9000();
		}
		if (isNE(clexIO.getRstaflag(), "Y")
		&& isNE(chdrlnbIO.getJownnum(), SPACES)) {
			clexIO.setClntcoy(chdrlnbIO.getCowncoy());
			clexIO.setClntnum(chdrlnbIO.getJownnum());
			clexIO.setClntpfx(chdrlnbIO.getCownpfx());
			clexIO.setFormat(clexrec);
			clexIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, clexIO);
			if (isNE(clexIO.getStatuz(), varcom.oK)
			&& isNE(clexIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(clexIO.getParams());
				fatalError9000();
			}
		}
		/* If the contract owner is a corporate client, check the*/
		/* life assured*/
		if (isEQ(clexIO.getRstaflag(), "Y")) {
			r200ReadTh609();
		}
		wsaaStaffFlag.set(clexIO.getRstaflag());
	}

protected void r200ReadTh609()
	{
		r210ReadStaffDiscount();
	}

	/**
	* <pre>
	* Read TH609 staff discount percentage.
	* </pre>
	*/
protected void r210ReadStaffDiscount()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(premiumrec.chdrChdrcoy);
		itemIO.setItemtabl(th609);
		itemIO.setItemitem(chdrlnbIO.getCnttype());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError9000();
			}
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaStaffDiscount.set(ZERO);
			th609rec.th609Rec.set(SPACES);
		}
		else {
			th609rec.th609Rec.set(itemIO.getGenarea());
			wsaaStaffDiscount.set(th609rec.prcnt);
		}
	}

protected void r300BaseRateStaffDisc()
	{
		/*R300-START*/
		/* To calculate WSAA-BAP  after staff discount.*/
		compute(wsaaBap, 3).setRounded(mult(wsaaBap, (div((sub(100, wsaaStaffDiscount)), 100))));
		/*R399-EXIT*/
	}

protected void r400InstalmentStaffDisc()
	{
		/*R400-START*/
		/* To calculate WSAA-BIP after staff discount.*/
		compute(wsaaBip, 3).setRounded(mult(wsaaBip, (div((sub(100, wsaaStaffDiscount)), 100))));
		/*R499-EXIT*/
	}

protected void a190ReadTj698()
	{
		a191Read();
	}

protected void a191Read()
	{
		itdmIO.setDataKey(SPACES);
		ix.set(ZERO);
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(tj698);
		itdmIO.setItemitem(premiumrec.crtable);
		if (isEQ(premiumrec.ratingdate, ZERO)) {
			itdmIO.setItmfrm(99999999);
		}
		else {
			itdmIO.setItmfrm(premiumrec.ratingdate);
		}
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(premiumrec.crtable, itdmIO.getItemitem())
		|| isNE(premiumrec.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), tj698)
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			return ;
		}
		else {
			tj698rec.tj698Rec.set(itdmIO.getGenarea());
		}
		for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
			if (isEQ(tj698rec.freqcy[ix.toInt()], premiumrec.billfreq)) {
				wsaaRebatePcnt.set(tj698rec.zlfact[ix.toInt()]);
				ix.set(13);
			}
		}
	}

protected void a200ReadTj699()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					a200SetVariable();
				case a201Read: 
					a201Read();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a200SetVariable()
	{
		wsaaCrtable.set(premiumrec.crtable);
	}

protected void a201Read()
	{
		itdmIO.setDataKey(SPACES);
		ix.set(ZERO);
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(tj699);
		wsaaTj699Crtable.set(wsaaCrtable);
		wsaaTj699Mop.set(premiumrec.mop);
		itdmIO.setItemitem(wsaaTj699Key);
		if (isEQ(premiumrec.ratingdate, ZERO)) {
			itdmIO.setItmfrm(99999999);
		}
		else {
			itdmIO.setItmfrm(premiumrec.ratingdate);
		}
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaTj699Key, itdmIO.getItemitem())
		|| isNE(premiumrec.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), tj699)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			if (isEQ(wsaaTj699Crtable, "****")) {
				premiumrec.statuz.set(jl01);
				return ;
			}
			wsaaCrtable.set("****");
			goTo(GotoLabel.a201Read);
		}
		else {
			tj699rec.tj699Rec.set(itdmIO.getGenarea());
		}
		for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
			if (isEQ(tj699rec.freqcy[ix.toInt()], premiumrec.billfreq)) {
				wsaaModalFactor.set(tj699rec.zlfact[ix.toInt()]);
				ix.set(13);
			}
		}
	}

protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz, "BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		premiumrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
