package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:18
 * Description:
 * Copybook name: ZMPFKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zmpfkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zmpfFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData zmpfKey = new FixedLengthStringData(256).isAPartOf(zmpfFileKey, 0, REDEFINE);
  	public FixedLengthStringData zmpfZmedcoy = new FixedLengthStringData(1).isAPartOf(zmpfKey, 0);
  	public FixedLengthStringData zmpfZmedprv = new FixedLengthStringData(10).isAPartOf(zmpfKey, 1);
  	public PackedDecimalData zmpfSeqno = new PackedDecimalData(2, 0).isAPartOf(zmpfKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(zmpfKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zmpfFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zmpfFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}