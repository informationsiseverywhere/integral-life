package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:09
 * Description:
 * Copybook name: LIFRTRNREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lifrtrnrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData lifrtrnRec = new FixedLengthStringData(231);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(lifrtrnRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(lifrtrnRec, 5);
  	public FixedLengthStringData batckey = new FixedLengthStringData(19).isAPartOf(lifrtrnRec, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(batckey, 0, FILLER);
  	public FixedLengthStringData batccoy = new FixedLengthStringData(1).isAPartOf(batckey, 2);
  	public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(batckey, 3);
  	public PackedDecimalData batcactyr = new PackedDecimalData(4, 0).isAPartOf(batckey, 5);
  	public PackedDecimalData batcactmn = new PackedDecimalData(2, 0).isAPartOf(batckey, 8);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(batckey, 10);
  	public FixedLengthStringData batcbatch = new FixedLengthStringData(5).isAPartOf(batckey, 14);
  	public FixedLengthStringData rdocnum = new FixedLengthStringData(9).isAPartOf(lifrtrnRec, 28);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(lifrtrnRec, 37);
  	public PackedDecimalData jrnseq = new PackedDecimalData(3, 0).isAPartOf(lifrtrnRec, 40);
  	public FixedLengthStringData rldgcoy = new FixedLengthStringData(1).isAPartOf(lifrtrnRec, 42);
  	public FixedLengthStringData sacscode = new FixedLengthStringData(2).isAPartOf(lifrtrnRec, 43);
  	public FixedLengthStringData rldgacct = new FixedLengthStringData(16).isAPartOf(lifrtrnRec, 45);
  	public FixedLengthStringData origcurr = new FixedLengthStringData(3).isAPartOf(lifrtrnRec, 61);
  	public FixedLengthStringData sacstyp = new FixedLengthStringData(2).isAPartOf(lifrtrnRec, 64);
  	public PackedDecimalData origamt = new PackedDecimalData(17, 2).isAPartOf(lifrtrnRec, 66);
  	public FixedLengthStringData tranref = new FixedLengthStringData(30).isAPartOf(lifrtrnRec, 75);
  	public FixedLengthStringData trandesc = new FixedLengthStringData(30).isAPartOf(lifrtrnRec, 105);
  	public PackedDecimalData crate = new PackedDecimalData(18, 9).isAPartOf(lifrtrnRec, 135);
  	public PackedDecimalData acctamt = new PackedDecimalData(17, 2).isAPartOf(lifrtrnRec, 145);
  	public FixedLengthStringData genlcoy = new FixedLengthStringData(1).isAPartOf(lifrtrnRec, 154);
  	public FixedLengthStringData genlcur = new FixedLengthStringData(3).isAPartOf(lifrtrnRec, 155);
  	public FixedLengthStringData glcode = new FixedLengthStringData(14).isAPartOf(lifrtrnRec, 158);
  	public FixedLengthStringData glsign = new FixedLengthStringData(1).isAPartOf(lifrtrnRec, 172);
  	public PackedDecimalData contot = new PackedDecimalData(2, 0).isAPartOf(lifrtrnRec, 173);
  	public FixedLengthStringData postyear = new FixedLengthStringData(4).isAPartOf(lifrtrnRec, 175);
  	public FixedLengthStringData postmonth = new FixedLengthStringData(2).isAPartOf(lifrtrnRec, 179);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(lifrtrnRec, 181);
  	public PackedDecimalData rcamt = new PackedDecimalData(17, 2).isAPartOf(lifrtrnRec, 186);
  	public PackedDecimalData frcdate = new PackedDecimalData(8, 0).isAPartOf(lifrtrnRec, 195);
  	public PackedDecimalData transactionDate = new PackedDecimalData(6, 0).isAPartOf(lifrtrnRec, 200);
  	public PackedDecimalData transactionTime = new PackedDecimalData(6, 0).isAPartOf(lifrtrnRec, 204);
  	public PackedDecimalData user = new PackedDecimalData(6, 0).isAPartOf(lifrtrnRec, 208);
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(lifrtrnRec, 212);
  	public FixedLengthStringData substituteCodes = new FixedLengthStringData(15).isAPartOf(lifrtrnRec, 216);
  	public FixedLengthStringData[] substituteCode = FLSArrayPartOfStructure(5, 3, substituteCodes, 0);


	public void initialize() {
		COBOLFunctions.initialize(lifrtrnRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lifrtrnRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}