package com.csc.life.productdefinition.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: PtevTableDAM.java
 * Date: Sun, 30 Aug 2009 03:44:52
 * Class transformed from PTEV.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PtevTableDAM extends PtevpfTableDAM {

	public PtevTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("PTEV");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", EFFDATE"
		             + ", PTRNEFF"
		             + ", VALIDFLAG";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "EFFDATE, " +
		            "PTRNEFF, " +
		            "TRANNO, " +
		            "VALIDFLAG, " +
		            "BATCACTYR, " +
		            "BATCACTMN, " +
		            "BATCTRCDE, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "EFFDATE DESC, " +
		            "PTRNEFF DESC, " +
		            "VALIDFLAG ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "EFFDATE ASC, " +
		            "PTRNEFF ASC, " +
		            "VALIDFLAG DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               effdate,
                               ptrneff,
                               tranno,
                               validflag,
                               batcactyr,
                               batcactmn,
                               batctrcde,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(44);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getEffdate().toInternal()
					+ getPtrneff().toInternal()
					+ getValidflag().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, ptrneff);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(5);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(5);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(1);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(effdate.toInternal());
	nonKeyFiller40.setInternal(ptrneff.toInternal());
	nonKeyFiller60.setInternal(validflag.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(78);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ getTranno().toInternal()
					+ nonKeyFiller60.toInternal()
					+ getBatcactyr().toInternal()
					+ getBatcactmn().toInternal()
					+ getBatctrcde().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, batcactyr);
			what = ExternalData.chop(what, batcactmn);
			what = ExternalData.chop(what, batctrcde);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}
	public PackedDecimalData getPtrneff() {
		return ptrneff;
	}
	public void setPtrneff(Object what) {
		setPtrneff(what, false);
	}
	public void setPtrneff(Object what, boolean rounded) {
		if (rounded)
			ptrneff.setRounded(what);
		else
			ptrneff.set(what);
	}
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public PackedDecimalData getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Object what) {
		setBatcactyr(what, false);
	}
	public void setBatcactyr(Object what, boolean rounded) {
		if (rounded)
			batcactyr.setRounded(what);
		else
			batcactyr.set(what);
	}	
	public PackedDecimalData getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Object what) {
		setBatcactmn(what, false);
	}
	public void setBatcactmn(Object what, boolean rounded) {
		if (rounded)
			batcactmn.setRounded(what);
		else
			batcactmn.set(what);
	}	
	public FixedLengthStringData getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(Object what) {
		batctrcde.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		effdate.clear();
		ptrneff.clear();
		validflag.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		tranno.clear();
		nonKeyFiller60.clear();
		batcactyr.clear();
		batcactmn.clear();
		batctrcde.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}