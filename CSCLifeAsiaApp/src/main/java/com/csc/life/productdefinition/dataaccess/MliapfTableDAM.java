package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MliapfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:50
 * Class transformed from MLIAPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MliapfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 198;
	public FixedLengthStringData mliarec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData mliapfRecord = mliarec;
	
	public FixedLengthStringData mlcoycde = DD.mlcoycde.copy().isAPartOf(mliarec);
	public FixedLengthStringData mlentity = DD.mlentity.copy().isAPartOf(mliarec);
	public FixedLengthStringData securityno = DD.securityno.copy().isAPartOf(mliarec);
	public FixedLengthStringData mloldic = DD.mloldic.copy().isAPartOf(mliarec);
	public FixedLengthStringData mlothic = DD.mlothic.copy().isAPartOf(mliarec);
	public FixedLengthStringData clntnaml = DD.clntnaml.copy().isAPartOf(mliarec);
	public PackedDecimalData dob = DD.dob.copy().isAPartOf(mliarec);
	public FixedLengthStringData rskflg = DD.rskflg.copy().isAPartOf(mliarec);
	public PackedDecimalData hpropdte = DD.hpropdte.copy().isAPartOf(mliarec);
	public PackedDecimalData mlhsperd = DD.mlhsperd.copy().isAPartOf(mliarec);
	public FixedLengthStringData mlmedcde01 = DD.mlmedcde.copy().isAPartOf(mliarec);
	public FixedLengthStringData mlmedcde02 = DD.mlmedcde.copy().isAPartOf(mliarec);
	public FixedLengthStringData mlmedcde03 = DD.mlmedcde.copy().isAPartOf(mliarec);
	public FixedLengthStringData actn = DD.actn.copy().isAPartOf(mliarec);
	public FixedLengthStringData indc = DD.indc.copy().isAPartOf(mliarec);
	public FixedLengthStringData mind = DD.mind.copy().isAPartOf(mliarec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(mliarec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(mliarec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(mliarec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(mliarec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public MliapfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for MliapfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public MliapfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for MliapfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public MliapfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for MliapfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public MliapfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("MLIAPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"MLCOYCDE, " +
							"MLENTITY, " +
							"SECURITYNO, " +
							"MLOLDIC, " +
							"MLOTHIC, " +
							"CLNTNAML, " +
							"DOB, " +
							"RSKFLG, " +
							"HPROPDTE, " +
							"MLHSPERD, " +
							"MLMEDCDE01, " +
							"MLMEDCDE02, " +
							"MLMEDCDE03, " +
							"ACTN, " +
							"INDC, " +
							"MIND, " +
							"EFFDATE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     mlcoycde,
                                     mlentity,
                                     securityno,
                                     mloldic,
                                     mlothic,
                                     clntnaml,
                                     dob,
                                     rskflg,
                                     hpropdte,
                                     mlhsperd,
                                     mlmedcde01,
                                     mlmedcde02,
                                     mlmedcde03,
                                     actn,
                                     indc,
                                     mind,
                                     effdate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		mlcoycde.clear();
  		mlentity.clear();
  		securityno.clear();
  		mloldic.clear();
  		mlothic.clear();
  		clntnaml.clear();
  		dob.clear();
  		rskflg.clear();
  		hpropdte.clear();
  		mlhsperd.clear();
  		mlmedcde01.clear();
  		mlmedcde02.clear();
  		mlmedcde03.clear();
  		actn.clear();
  		indc.clear();
  		mind.clear();
  		effdate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getMliarec() {
  		return mliarec;
	}

	public FixedLengthStringData getMliapfRecord() {
  		return mliapfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setMliarec(what);
	}

	public void setMliarec(Object what) {
  		this.mliarec.set(what);
	}

	public void setMliapfRecord(Object what) {
  		this.mliapfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(mliarec.getLength());
		result.set(mliarec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}