/*
 * File: Pr28y.java
 * Date: 29 Sep 2012 1:08:55
 * Author: Quipoz Limited
 * 
 * Class transformed from Pr28y.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.screens.Sr28yScreenVars;
import com.csc.life.productdefinition.tablestructures.Tr28yrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*            Pr28y MAINLINE.
*
*   Parameter prompt program
*
*****************************************************************
* </pre>
*/
public class Pr28y extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR28Y");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "";
	private String wsaaUpdateCommonFieldFlag = "";
	private String wsaaFirstTime = "Y";
	private ZonedDecimalData wsaaSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSeq1 = new ZonedDecimalData(2, 0).setUnsigned();
		/* ERRORS */
	private String e026 = "E026";
	private String e027 = "E027";
		/* FORMATS */
	private String itemrec = "ITEMREC";
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();	
	private Tr28yrec tr28yrec = new Tr28yrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr28yScreenVars sv = ScreenProgram.getScreenVars( Sr28yScreenVars.class);
	private String f147 = "F147";
	private String e186 = "E186";

	private FixedLengthStringData wsaaVpmsprop = new FixedLengthStringData(60);
	private FixedLengthStringData wsaaCopybk = new FixedLengthStringData(12);
	private FixedLengthStringData wsaaMaptofield = new FixedLengthStringData(35);
	private FixedLengthStringData wsaaNumoccurs = new FixedLengthStringData(3);

	private FixedLengthStringData wsaaTr28yVpmsprop = new FixedLengthStringData(60);
	private FixedLengthStringData wsaaTr28yCopybk = new FixedLengthStringData(12);
	private FixedLengthStringData wsaaTr28yMaptofield = new FixedLengthStringData(35);
	private FixedLengthStringData wsaaTr28yNumoccurs = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaTr28yAttrLbls = new FixedLengthStringData(960);
	private FixedLengthStringData[] wsaaTr28yAttrLbl = FLSArrayPartOfStructure(16, 60, wsaaTr28yAttrLbls, 0);
	private FixedLengthStringData wsaaTr28yAttrValues = new FixedLengthStringData(128);
	private FixedLengthStringData[] wsaaTr28yAttrValue = FLSArrayPartOfStructure(16, 8, wsaaTr28yAttrValues, 0);
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readPrimaryRecord1020, 
		generalArea1045,
		other1080,
		preExit,
		exit2090, 
		other3080, 
		exit3090,
		continue4020, 
		other4080
	}

	public Pr28y() {
		super();
		screenVars = sv;
		new ScreenModel("Sr28y", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				initialise1010();
			}
			case readPrimaryRecord1020: {
				readPrimaryRecord1020();
				readRecord1031();
				moveToScreen1040();
			}
			case generalArea1045: {
				generalArea1045();
			}
			case other1080: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void initialise1010()
{
	for (int i = 1; i <= 16; i++){
		wsaaTr28yAttrLbl[i].set(SPACES);
		wsaaTr28yAttrValue[i].set(SPACES);
	}
	
	if (isNE(wsaaFirstTime,"Y")) {
		goTo(GotoLabel.readPrimaryRecord1020);
	}
	
	wsaaTr28yVpmsprop.set(SPACES);
	wsaaTr28yCopybk.set(SPACES);
	wsaaTr28yMaptofield.set(SPACES);
	wsaaTr28yNumoccurs.set(SPACES);
	
	if (isEQ(itemIO.getItemseq(),SPACES)) {
		wsaaSeq.set(ZERO);
	}
	else {
		wsaaSeq.set(itemIO.getItemseq());
	}
	/*INITIALISE-SCREEN*/
	sv.dataArea.set(SPACES);
	/*READ-PRIMARY-RECORD*/
	
}

protected void readPrimaryRecord1020()
{
	wsaaSeq1.set(wsaaSeq.toInt() * 4);
	for (int i = 0; i < 4; i++){
		
		itemIO.setDataKey(wsspsmart.itemkey);
		if (isEQ(wsaaSeq1, ZERO)) {
			itemIO.setItemseq(SPACES);
		}
		else {
			itemIO.setItemseq(wsaaSeq1);
		}
		/*READ-RECORD*/
		
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
			&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)){
			tr28yrec.tr28yRec.set(itemIO.getGenarea());
			if (isEQ(wsaaSeq1, ZERO)){
				wsaaTr28yVpmsprop.set(tr28yrec.vpmsprop);
				wsaaTr28yCopybk.set(tr28yrec.copybk);
				wsaaTr28yMaptofield.set(tr28yrec.maptofield);
				wsaaTr28yNumoccurs.set(tr28yrec.numoccurs);
			}
			for (int j = 1; j <= 4; j++){
				wsaaTr28yAttrLbl[i * 4 + j].set(tr28yrec.attrslbl[j]);
				wsaaTr28yAttrValue[i * 4 + j].set(tr28yrec.attrsvalue[j]);
			}
		}
		tr28yrec.tr28yRec.set(SPACES);
		if (isEQ(wsaaSeq1, ZERO)){
			sv.company.set(itemIO.getItemcoy());
			sv.tabl.set(itemIO.getItemtabl());
			sv.item.set(itemIO.getItemitem());
		}
        wsaaSeq1.add(1);
	}
	//reset to Itemseq if in page 00
	if (isEQ(wsaaSeq,ZERO)) {
		itemIO.setItemseq(SPACES);
	}
		/*READ-SECONDARY-RECORDS*/
}

protected void readRecord1031()
{
	descIO.setDescpfx(itemIO.getItempfx());
	descIO.setDesccoy(itemIO.getItemcoy());
	descIO.setDesctabl(itemIO.getItemtabl());
	descIO.setDescitem(itemIO.getItemitem());
	descIO.setItemseq(SPACES);
	descIO.setLanguage(wsspcomn.language);
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(),varcom.oK)
	&& isNE(descIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		fatalError600();
	}
	wsaaFirstTime = "N";
}

protected void moveToScreen1040()
{
//	if (isEQ(itemIO.getStatuz(),varcom.mrnf)
//			&& isEQ(itemIO.getItemseq(),SPACES)) {
//		syserrrec.params.set(itemIO.getParams());
//		fatalError600();
//	}
	if (isEQ(itemIO.getStatuz(),varcom.mrnf)
			&& isEmptyPage1041() 
			&& isEQ(wsspcomn.flag,"I")) {
		scrnparams.errorCode.set(e026);
		wsaaSeq.subtract(1);
		goTo(GotoLabel.other1080);
	}
	if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
		itemIO.setGenarea(SPACES);
	}
	sv.longdesc.set(descIO.getLongdesc());
	if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
		sv.longdesc.set(SPACES);
	}
	if (isNE(itemIO.getGenarea(),SPACES)) {
		goTo(GotoLabel.generalArea1045);
	}
}

protected boolean isEmptyPage1041()
{
	for (int i = 0; i < 4; i++){
		for (int j = 1; j <= 4; j++){
			if (isNE(wsaaTr28yAttrLbl[i * 4 + j], SPACES)){
				return false;
			}	
		}
	}
	return true;
}

protected void generalArea1045()
{
	//If not creating a new item, populate common area with those values of last seen screen
	if (isNE(wsaaVpmsprop,SPACES)
			|| isNE(wsaaCopybk,SPACES)
			|| isNE(wsaaMaptofield,SPACES)
			|| isNE(wsaaNumoccurs,SPACES))
	{
		sv.vpmsprop.set(wsaaVpmsprop);
		sv.copybk.set(wsaaCopybk);
		sv.maptofield.set(wsaaMaptofield);
		sv.numoccurs.set(wsaaNumoccurs);
	} else {
		//In creating a new item
		sv.vpmsprop.set(wsaaTr28yVpmsprop);
		sv.copybk.set(wsaaTr28yCopybk);
		sv.maptofield.set(wsaaTr28yMaptofield);
		sv.numoccurs.set(wsaaTr28yNumoccurs);
		//Transfer screen values to temporary variables
		wsaaVpmsprop.set(sv.vpmsprop);
		wsaaCopybk.set(sv.copybk);
		wsaaMaptofield.set(sv.maptofield);
		wsaaNumoccurs.set(sv.numoccurs);
	}
	for (int i = 0; i < 4; i++){
		for (int j = 1; j <= 4; j++){
			sv.attrslbl[i * 4 + j].set(wsaaTr28yAttrLbl[i * 4 + j]);
			sv.attrsvalue[i * 4 + j].set(wsaaTr28yAttrValue[i * 4 + j]);
		}
	}
	/*CONFIRMATION-FIELDS*/
	/*OTHER*/
	/*EXIT*/
}

protected void preScreenEdit()
{
	try {
		preStart();
	}
	catch (GOTOException e){
	}
}

protected void preStart()
{
	if (isEQ(wsspcomn.flag,"I")) {
		scrnparams.function.set(varcom.prot);
	}
	goTo(GotoLabel.preExit);
}

protected void screenEdit2000()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				screenIo2010();
				checkVpmsProperty9010();
				checkCopyBook9020();
				checkMapField9030();
				checkNumOccurences9040();
				checkAttrsMapping9050();
			}
			case exit2090: {
				exit2090();
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void screenIo2010()
{
	wsspcomn.edterror.set(varcom.oK);
	/*VALIDATE*/
	if (isEQ(scrnparams.statuz,varcom.rold)
	&& isEQ(itemIO.getItemseq(),SPACES)) {
		scrnparams.errorCode.set(e027);
		wsspcomn.edterror.set("Y");
	}
	if (isEQ(scrnparams.statuz,varcom.rolu)
	&& isEQ(itemIO.getItemseq(),"25")) {//Notes: 100 : 4(number of items on a page) = 25 pages maximum
		scrnparams.errorCode.set(e026);
		wsspcomn.edterror.set("Y");
	}
	if (isEQ(wsspcomn.flag,"I")) {
		goTo(GotoLabel.exit2090);
	}
}
protected void checkVpmsProperty9010()
{
	if (isEQ(sv.vpmsprop,SPACES)) {
		sv.vpmspropErr.set(e186);
		goTo(GotoLabel.exit2090);
	}
}
protected void checkCopyBook9020()
{
	if (isEQ(sv.copybk,SPACES)) {
		sv.copybkErr.set(e186);
		goTo(GotoLabel.exit2090);
	}
}
protected void checkMapField9030()
{
	if (isEQ(sv.maptofield,SPACES)) {
		sv.maptofieldErr.set(e186);
		goTo(GotoLabel.exit2090);
	}
}
protected void checkNumOccurences9040()
{
	try {
		new Integer(sv.numoccurs.getData().trim());/* IJTI-1523 */
	} catch (NumberFormatException e) {
		sv.numoccursErr.set(f147);
		goTo(GotoLabel.exit2090);
	}
}
protected void checkAttrsMapping9050()
{
	//For all other rows, enter both, or none.
	if (isEQ(wsaaSeq, ZERO) && (isEQ(sv.attrslbl01,SPACES) || isEQ(sv.attrsvalue01,SPACES))) {
		sv.attrslbl01Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.attrslbl02,SPACES) && isNE(sv.attrsvalue02,SPACES) 
			|| isNE(sv.attrslbl02,SPACES) && isEQ(sv.attrsvalue02,SPACES) ) {
		sv.attrslbl02Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.attrslbl03,SPACES) && isNE(sv.attrsvalue03,SPACES) 
			|| isNE(sv.attrslbl03,SPACES) && isEQ(sv.attrsvalue03,SPACES) ) {
		sv.attrslbl01Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.attrslbl04,SPACES) && isNE(sv.attrsvalue04,SPACES) 
			|| isNE(sv.attrslbl04,SPACES) && isEQ(sv.attrsvalue04,SPACES) ) {
		sv.attrslbl04Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.attrslbl05,SPACES) && isNE(sv.attrsvalue05,SPACES) 
			|| isNE(sv.attrslbl05,SPACES) && isEQ(sv.attrsvalue05,SPACES) ) {
		sv.attrslbl05Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.attrslbl06,SPACES) && isNE(sv.attrsvalue06,SPACES) 
			|| isNE(sv.attrslbl06,SPACES) && isEQ(sv.attrsvalue06,SPACES) ) {
		sv.attrslbl06Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.attrslbl07,SPACES) && isNE(sv.attrsvalue07,SPACES) 
			|| isNE(sv.attrslbl07,SPACES) && isEQ(sv.attrsvalue07,SPACES) ) {
		sv.attrslbl07Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.attrslbl08,SPACES) && isNE(sv.attrsvalue08,SPACES) 
			|| isNE(sv.attrslbl08,SPACES) && isEQ(sv.attrsvalue08,SPACES) ) {
		sv.attrslbl08Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.attrslbl09,SPACES) && isNE(sv.attrsvalue09,SPACES) 
			|| isNE(sv.attrslbl09,SPACES) && isEQ(sv.attrsvalue09,SPACES) ) {
		sv.attrslbl09Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.attrslbl10,SPACES) && isNE(sv.attrsvalue10,SPACES) 
			|| isNE(sv.attrslbl10,SPACES) && isEQ(sv.attrsvalue10,SPACES) ) {
		sv.attrslbl10Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.attrslbl11,SPACES) && isNE(sv.attrsvalue11,SPACES) 
			|| isNE(sv.attrslbl11,SPACES) && isEQ(sv.attrsvalue11,SPACES) ) {
		sv.attrslbl11Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.attrslbl12,SPACES) && isNE(sv.attrsvalue12,SPACES) 
			|| isNE(sv.attrslbl12,SPACES) && isEQ(sv.attrsvalue12,SPACES) ) {
		sv.attrslbl12Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.attrslbl13,SPACES) && isNE(sv.attrsvalue13,SPACES) 
			|| isNE(sv.attrslbl13,SPACES) && isEQ(sv.attrsvalue13,SPACES) ) {
		sv.attrslbl13Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.attrslbl14,SPACES) && isNE(sv.attrsvalue14,SPACES) 
			|| isNE(sv.attrslbl14,SPACES) && isEQ(sv.attrsvalue14,SPACES) ) {
		sv.attrslbl14Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.attrslbl15,SPACES) && isNE(sv.attrsvalue15,SPACES) 
			|| isNE(sv.attrslbl15,SPACES) && isEQ(sv.attrsvalue15,SPACES) ) {
		sv.attrslbl15Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
	if (isEQ(sv.attrslbl16,SPACES) && isNE(sv.attrsvalue16,SPACES) 
			|| isNE(sv.attrslbl16,SPACES) && isEQ(sv.attrsvalue16,SPACES) ) {
		sv.attrslbl16Err.set(e186);
		goTo(GotoLabel.exit2090);
	}
}

protected void exit2090()
{
	if (isNE(sv.errorIndicators,SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	/*EXIT*/
}

protected void update3000()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				preparation3010();
				updatePrimaryRecord3050();
			}
			case other3080: {
			}
			case exit3090: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void preparation3010()
{
	for (int i = 1; i <= 16; i++){
		wsaaTr28yAttrLbl[i].set(SPACES);
		wsaaTr28yAttrValue[i].set(SPACES);
	}
	
	if (isEQ(wsspcomn.flag,"I")) {
		goTo(GotoLabel.exit3090);
	}
}

protected void updatePrimaryRecord3050()
{
	wsaaSeq1.set(wsaaSeq.toInt() * 4);
	for (int i = 0; i < 4; i++){
		
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		if (isEQ(wsaaSeq1,ZERO)) {
			itemIO.setItemseq(SPACES);
		}
		else {
			itemIO.setItemseq(wsaaSeq1);
		}
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK) 
				&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)){
			tr28yrec.tr28yRec.set(itemIO.getGenarea());
			//Save common data of all pages for Tr28y
			if (isEQ(wsaaSeq1,ZERO)){
				wsaaTr28yVpmsprop.set(tr28yrec.vpmsprop);
				wsaaTr28yCopybk.set(tr28yrec.copybk);
				wsaaTr28yMaptofield.set(tr28yrec.maptofield);
				wsaaTr28yNumoccurs.set(tr28yrec.numoccurs);
			}
		}
		
		updateRecord3055(i);
		
		tr28yrec.tr28yRec.set(SPACES);
		wsaaSeq1.add(1);
		
	}
	//reset to Itemseq if in page 00
	if (isEQ(wsaaSeq,ZERO)) {
		itemIO.setItemseq(SPACES);
	}
	//Transfer modified screen values to temporary variables
	wsaaVpmsprop.set(sv.vpmsprop);
	wsaaCopybk.set(sv.copybk);
	wsaaMaptofield.set(sv.maptofield);
	wsaaNumoccurs.set(sv.numoccurs);
	
	varcom.vrcmTranid.set(wsspcomn.tranid);
	varcom.vrcmCompTermid.set(varcom.vrcmTermid);
	varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
	itemIO.setTranid(varcom.vrcmCompTranid);
}

protected void updateRecord3055(int i)
{
		wsaaUpdateFlag = "N";
		wsaaUpdateCommonFieldFlag = "N";
		tr28yrec.vpmsprop.set(wsaaTr28yVpmsprop);
		tr28yrec.copybk.set(wsaaTr28yCopybk);
		tr28yrec.maptofield.set(wsaaTr28yMaptofield);
		tr28yrec.numoccurs.set(wsaaTr28yNumoccurs);
		boolean emptyAttrsFlag = isEmptyAttrs3110(i);

		checkChangesCommonFields3130();
		checkChanges3100(i);
		if (isNE(wsaaUpdateFlag,"Y") && (emptyAttrsFlag || isNE(wsaaUpdateCommonFieldFlag,"Y"))) {
			return;
		}
		if (isEQ(wsaaSeq1,ZERO)) {
			itemIO.setItemseq(SPACES);
		}
		else {
			itemIO.setItemseq(wsaaSeq1);
		}
		itemIO.setGenarea(tr28yrec.tr28yRec);
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setFunction(varcom.writr);
			itemIO.setFormat(itemrec);
		}
		else {
			itemIO.setFunction(varcom.rewrt);
		}
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
}


protected void checkChanges3100(int i)
{
	/*CHECK*/
	for (int j = 1; j <= 4; j++){
		if (isNE(sv.attrslbl[i * 4 + j], tr28yrec.attrslbl[j])) {
			tr28yrec.attrslbl[j].set(sv.attrslbl[i * 4 + j]);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.attrsvalue[i * 4 + j], tr28yrec.attrsvalue[j])) {
			tr28yrec.attrsvalue[j].set(sv.attrsvalue[i * 4 + j]);
			wsaaUpdateFlag = "Y";
		}
	}
	
	/*EXIT*/
}

protected void checkChangesCommonFields3130()
{
	if (isNE(sv.vpmsprop,tr28yrec.vpmsprop)) {
		tr28yrec.vpmsprop.set(sv.vpmsprop);
		wsaaUpdateCommonFieldFlag = "Y";
	}
	if (isNE(sv.maptofield,tr28yrec.maptofield)) {
		tr28yrec.maptofield.set(sv.maptofield);
		wsaaUpdateCommonFieldFlag = "Y";
	}
	if (isNE(sv.copybk,tr28yrec.copybk)) {
		tr28yrec.copybk.set(sv.copybk);
		wsaaUpdateCommonFieldFlag = "Y";
	}
	if (isNE(sv.numoccurs,tr28yrec.numoccurs)) {
		tr28yrec.numoccurs.set(sv.numoccurs);
		wsaaUpdateCommonFieldFlag = "Y";
	}
}

//Check if all 4 attributes are originally empty
protected boolean isEmptyAttrs3110(int i)
{
	for (int j = 1; j <= 4; j++){
		if (isNE(sv.attrslbl[i * 4 + j], SPACES) || isNE(tr28yrec.attrslbl[j], SPACES)) {
			return false;
		}
	}
	return true;
}

protected void whereNext4000()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				preparation4020();
			}
			case continue4020: {
				continue4020();
			}
			case other4080: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void preparation4020()
{
	if (isNE(scrnparams.statuz,varcom.rolu)
	&& isNE(scrnparams.statuz,varcom.rold)) {
		goTo(GotoLabel.continue4020);
	}
	if (isEQ(scrnparams.statuz,varcom.rolu)) {
		wsaaSeq.add(1);
	}
	else {
		wsaaSeq.subtract(1);
	}
	goTo(GotoLabel.other4080);
}

protected void continue4020()
{
	wsaaVpmsprop.set(SPACES);
	wsaaCopybk.set(SPACES);
	wsaaMaptofield.set(SPACES);
	wsaaNumoccurs.set(SPACES);

	wsspcomn.programPtr.add(1);
	wsaaSeq.set(ZERO);
}
}
