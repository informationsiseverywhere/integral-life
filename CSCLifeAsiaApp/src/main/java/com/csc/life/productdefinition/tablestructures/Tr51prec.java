package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:19:52
 * Description:
 * Copybook name: TR51PREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr51prec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr51pRec = new FixedLengthStringData(500);
  	public ZonedDecimalData durationa = new ZonedDecimalData(4, 0).isAPartOf(tr51pRec, 0);
  	public ZonedDecimalData minmthif = new ZonedDecimalData(3, 0).isAPartOf(tr51pRec, 4);
  	public FixedLengthStringData premind = new FixedLengthStringData(1).isAPartOf(tr51pRec, 7);
  	public FixedLengthStringData reinstflg = new FixedLengthStringData(1).isAPartOf(tr51pRec, 8);
  	public FixedLengthStringData znfopt = new FixedLengthStringData(3).isAPartOf(tr51pRec, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(488).isAPartOf(tr51pRec, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr51pRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr51pRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}