package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


import com.csc.fsu.accounting.dataaccess.dao.RtrnpfDAO;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.Th5agrec;
import com.csc.life.annuities.recordstructures.Prasrec;
import com.csc.life.annuities.tablestructures.T6687rec;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.tablestructures.T3615rec;
import com.csc.life.productdefinition.recordstructures.FirstRcpPremRec;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.model.Rtrnpf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class FirstRcpPrem extends SMARTCodeModel{

	protected CovtpfDAO covtpfDAO = getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	protected RtrnpfDAO rtrnpfDAO = getApplicationContext().getBean("rtrnpfDAO", RtrnpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();	
	private static final String wsaaSubr = "FIRSTRCPPREM";	
	private TablesInner tablesInner = new TablesInner();
	private Itempf itempf = new Itempf();
	protected ItemTableDAM itemIO = new ItemTableDAM();	
	private List<Itempf> itdmpfList = null;
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();	
	protected T5688rec t5688rec = new T5688rec();
	private T5674rec t5674rec = new T5674rec();
	protected T6687rec t6687rec = new T6687rec();
	protected Mgfeelrec mgfeelrec = new Mgfeelrec();
	protected Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	protected Prasrec prasrec = new Prasrec();
	private Syserrrec syserrrec = new Syserrrec();	
	protected Varcom varcom = new Varcom();
	private int firstPremRcpDt;
	private Itempf itdmpf = null;
	
	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private PackedDecimalData totalPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData totalTax = new PackedDecimalData(17, 2);
	private PackedDecimalData cntFee = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRegprem =  new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSingp =  new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSpTax =  new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRpTax =  new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaInstprm =  new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPremTax =  new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTaxrelamt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaModalPremium = new PackedDecimalData(15, 2);
	protected PackedDecimalData wsaaTotalPremium = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaTotalTax = new PackedDecimalData(17, 2);
	protected ZonedDecimalData wsaaSingpFee = new ZonedDecimalData(17, 2);	
	private PackedDecimalData wsaaTax = new PackedDecimalData(17, 2);	
	private PackedDecimalData wsaaCntfeeTax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaWavCntfeeTax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSingpfeeTax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCompTax = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaCntfee = new ZonedDecimalData(17, 2);
	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	protected PackedDecimalData wsaaFactor = new PackedDecimalData(11, 5);
	public FixedLengthStringData batcBatctrcde = new FixedLengthStringData(4);
	private FirstRcpPremRec firstRcpPremRec = new FirstRcpPremRec();
	protected String wsaaSingPrmInd = "N";
	protected Th5agrec th5agrec = new Th5agrec();
	private Datcon1rec datcon1 = new Datcon1rec();//IBPLIFE-2470
	private FixedLengthStringData currentDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaTodayDate = new ZonedDecimalData(8).isAPartOf(currentDate);
	private ZonedDecimalData wsaaTodayYear = new ZonedDecimalData(4).isAPartOf(currentDate, 0);
	private ZonedDecimalData wsaaTodayMnth = new ZonedDecimalData(2).isAPartOf(currentDate);
	private String th5ag = "Th5ag"; //IBPLIFE-2470
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);//IBPLIFE-2470
	private T3615rec t3615rec = new T3615rec();//IBPLIFE-2470
	private String t3615 = "T3615"; //IBPLIFE-2470
	
	public FirstRcpPrem() {
		super();
	}
	
	@Override
	public void mainline(Object... parmArray)
	{		
		firstRcpPremRec = (FirstRcpPremRec) convertAndSetParam(firstRcpPremRec, parmArray, 0);		
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
		
	}
	
	protected void startSubr010()
	{	
		chdrlnbIO = firstRcpPremRec.getChdrlnbIO();
		batcBatctrcde.set(firstRcpPremRec.getBatcBatctrcde());		
		syserrrec.subrname.set(wsaaSubr);
		initialise();
		getFirstRcpPremDt();
		exitProgram();
	}
	
	
	protected void initialise(){
		wsaaSingp.set(ZERO);
		wsaaRegprem.set(ZERO);
		wsaaSingp.set(ZERO);
		wsaaSpTax.set(ZERO);
		wsaaRpTax.set(ZERO);
		wsaaInstprm.set(ZERO);
		wsaaFactor.set(0);
		totalPremium.set(0);
		totalTax.set(0);
		cntFee.set(0);	
		firstPremRcpDt=0;
		wsaaTotalTax.set(0);
		wsaaTotalPremium.set(0);		
		wsaaSingpfeeTax.set(0);		
		wsaaSingPrmInd = "N";
		wsaaSingpFee.set(ZERO);					
		wsaaModalPremium.set(0);
		wsaaTax.set(0);
		wsaaTotalTax.set(0);
		wsaaCntfeeTax.set(0);
		wsaaWavCntfeeTax.set(0);
		wsaaCntfee.set(0);				
	}
	
	protected void getFirstRcpPremDt(){   
			updateFirstRcpPremDt();
			firstRcpPremRec.setFirstpremdate(firstPremRcpDt);	 
	    	calcContractPremium();
		}
	
	protected void calcPremium()
	{
		List<Covtpf> covtpfList = null;		
		covtpfList = covtpfDAO.searchCovtRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		if(null == covtpfList){
			return;
		}
		for(Covtpf covtpf : covtpfList){
			readseqCovtlnb(covtpf);
		}		
		adjustRegularPrem();
		return;
	}
	
	protected void adjustRegularPrem()
	{
		/* Look up the tax relief method on T5688.                         */
		itdmpf = new Itempf();
		itdmpfList = itemDAO.getItdmByFrmdate(chdrlnbIO.getChdrcoy().toString(), tablesInner.t5688.toString(), chdrlnbIO.getCnttype().toString(), firstRcpPremRec.getOccdate().toInt());

		if(itdmpfList != null && itdmpfList.size() !=0 ){
			itdmpf = itdmpfList.get(0);
		}

		t5688rec.t5688Rec.set(StringUtil.rawToString(itdmpf.getGenarea()));
		/* Look up the subroutine on T6687.                               */
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(chdrlnbIO.getChdrcoy().toString());
		itempf.setItemtabl(tablesInner.t6687.toString());
		itempf.setItemitem(t5688rec.taxrelmth.toString());
		itempf = itemDAO. getItempfRecord(itempf);

		if (null != itempf) {
			t6687rec.t6687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			t6687rec.t6687Rec.set(SPACES);
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* Adjust the regular premiums (if not zero) by the number         */
		/* of frequencies required prior to issue.                         */
		/* If the contract has a tax relief method deduct the tax          */
		/* relief amount from the amount due.                              */
		/* Also check if there is enough money in suspense to issue        */
		/* this contract.        
		 *                                           */
		if(isNE(chdrlnbIO.getCownnum(), SPACES)){
			adjustPremium();
		}
		
	}
	
	protected void adjustPremium()
	{

		/*    Get the frequency Factor from DATCON3.                       */
		if (isNE(wsaaRegprem, ZERO)) {
			datcon3rec.intDate1.set(firstRcpPremRec.getOccdate());
			datcon3rec.intDate2.set(chdrlnbIO.getBillcd());		
			datcon3rec.frequency.set(chdrlnbIO.getBillfreq());
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz, Varcom.oK)) {
				syserrrec.statuz.set(datcon3rec.statuz);
				syserr();
			}			
			wsaaFactor.set(datcon3rec.freqFactor);
			compute(wsaaInstprm, 5).set(mult(wsaaRegprem, wsaaFactor));
		}
		/* Add in the single premium.                                      */
		wsaaInstprm.add(wsaaSingp);
		/* If the tax relief method is not spaces calculate the tax        */
		/* relief amount and deduct it from the premium.                   */
		if (isNE(t5688rec.taxrelmth, SPACES)) {
			prasrec.clntnum.set(chdrlnbIO.getCownnum());
			prasrec.clntcoy.set(chdrlnbIO.getChdrcoy());
			prasrec.incomeSeqNo.set(ZERO);
			prasrec.cnttype.set(chdrlnbIO.getCnttype());
			prasrec.taxrelmth.set(t5688rec.taxrelmth);
			/* Use the due date unless a receipt exists with a date later      */
			/* then the due date.                                            */			
			prasrec.effdate.set(firstRcpPremRec.getOccdate());//
			prasrec.company.set(chdrlnbIO.getChdrcoy());
			prasrec.grossprem.set(wsaaInstprm);
			prasrec.statuz.set(Varcom.oK);
			callProgram(t6687rec.taxrelsub, prasrec.prascalcRec);
			if (isNE(prasrec.statuz, Varcom.oK)) {
				syserrrec.statuz.set(prasrec.statuz);
				syserrrec.subrname.set(t6687rec.taxrelsub);
				syserr();
			}
			wsaaInstprm.subtract(prasrec.taxrelamt);
		}
		wsaaTotalPremium.add(wsaaInstprm);
		/* Add the contract fee to the instalment premium for              */
		/* payer No. 1.                                                    */
		/* IF SINGLE PREMIUM IS APPLIED CHECK IF ANY FEE IS NEEDED         */
		singlePremiumFeeCustomerSpecific();
		compute(wsaaPremTax, 6).setRounded(add((mult(wsaaRpTax, wsaaFactor)), wsaaSpTax));
		compute(wsaaTotalTax, 3).setRounded(add(wsaaTotalTax, wsaaPremTax));
		if (isEQ(wsaaInstprm, ZERO)) {
			cntFee.set(ZERO);
		}
		else {
			compute(cntFee, 5).set(mult(cntFee, wsaaFactor));
		}
		cntFee.add(wsaaSingpFee);
		wsaaInstprm.add(cntFee);
		if (isNE(wsaaSingpFee, ZERO)) {
			wsaaCntfee.set(wsaaSingpFee);
			checkCalcContTax();
			wsaaSingpfeeTax.set(wsaaTax);
		}
		compute(wsaaCntfeeTax, 6).setRounded(mult(wsaaCntfeeTax, wsaaFactor));
		compute(wsaaTotalTax, 3).setRounded(add(add(wsaaTotalTax, wsaaSingpfeeTax), wsaaCntfeeTax));
	
		zrdecplrec.amountIn.set(wsaaTotalTax);
		zrdecplrec.currency.set(chdrlnbIO.getCntcurr());
		callRounding();
		wsaaTotalTax.set(zrdecplrec.amountOut);		
		wsaaInstprm.add(wsaaTotalTax);
		 
		compute(totalPremium, 2).set(add(wsaaTotalPremium,cntFee));
		compute(totalPremium, 2).add(wsaaTotalTax);	
	}
	
	protected void singlePremiumFeeCustomerSpecific() {
		if (isEQ(wsaaSingPrmInd, "Y") && isNE(t5688rec.feemeth, SPACES)) {
			singPremFee();
		}
	}
	
	protected void singPremFee()
	{
		if (isEQ(t5674rec.commsubr, SPACES)) {
			return ;
		}
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(chdrlnbIO.getCnttype());
		mgfeelrec.billfreq.set("00");
		mgfeelrec.effdate.set(firstRcpPremRec.getOccdate());
		mgfeelrec.cntcurr.set(chdrlnbIO.getCntcurr());
		mgfeelrec.company.set(chdrlnbIO.getChdrcoy());
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz, Varcom.oK)
				&& isNE(mgfeelrec.statuz, Varcom.endp)) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			syserr();
		}		
		wsaaSingpFee.set(mgfeelrec.mgfee);
	}

	
	protected void readseqCovtlnb(Covtpf covtpf)
	{
		/*    Read each record and accumulate all single and regular*/
		/*    premiums payable.*/		
		
		if (isNE(covtpf.getSingp(), 0)) {
			wsaaSingPrmInd = "Y";
		}
		if (isEQ(chdrlnbIO.getBillfreq(), "00")) {
			wsaaSingp.add(PackedDecimalData.parseObject(covtpf.getSingp()));
			wsaaModalPremium.add(PackedDecimalData.parseObject(covtpf.getSingp()));
		}
		if (isEQ(covtpf.getInstprem(), 0)) {			
			checkCalcCompTax(covtpf);
			return;
		}		
		if (isEQ(chdrlnbIO.getBillfreq(), "00")) {			
			checkCalcCompTax(covtpf);
			return;
		}
		wsaaRegprem.add(PackedDecimalData.parseObject(covtpf.getInstprem()));
		wsaaModalPremium.add(PackedDecimalData.parseObject(covtpf.getInstprem()));
		checkCalcCompTax(covtpf);
		return;
	}


	protected void calcFee()
	{
		/*    Reference T5674 to obtain the subroutine required to work*/
		/*    out the Fee amount by the correct method.*/

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy("2");
		itempf.setItemtabl(tablesInner.t5674.toString());
		itempf.setItemitem(t5688rec.feemeth.toString());
		itempf = itemDAO.getItempfRecord(itempf);
		if (null == itempf) {
			syserrrec.params.set(itemIO.getParams());			
			return ;
		}
		t5674rec.t5674Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		/* Check subroutine NOT = SPACES before attempting call.           */
		if (isEQ(t5674rec.commsubr, SPACES)) {
			return ;
		}
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(chdrlnbIO.getCnttype());
		/* MOVE CHDRLNB-BILLFREQ       TO MGFL-BILLFREQ.                */
		mgfeelrec.billfreq.set(chdrlnbIO.getBillfreq());
		mgfeelrec.effdate.set(firstRcpPremRec.getOccdate());
		mgfeelrec.cntcurr.set(chdrlnbIO.getCntcurr());
		mgfeelrec.company.set("2");
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz, Varcom.oK)
				&& isNE(mgfeelrec.statuz, Varcom.endp)) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			syserrrec.statuz.set(mgfeelrec.statuz);
			syserr();			
		}
		zrdecplrec.amountIn.set(mgfeelrec.mgfee);
		zrdecplrec.currency.set(chdrlnbIO.getCntcurr());
		callRounding();
		mgfeelrec.mgfee.set(zrdecplrec.amountOut);
		if (isEQ(chdrlnbIO.getBillcd(), firstRcpPremRec.getOccdate())) {
			cntFee.set(ZERO);
		}
		else {			
			cntFee.set(mgfeelrec.mgfee);
		}
		if (isGT(cntFee, ZERO)) {
			wsaaCntfee.set(cntFee);
			checkCalcContTax();
			wsaaCntfeeTax.set(wsaaTax);
			wsaaWavCntfeeTax.set(wsaaTax);

		}
	}
	
	protected void callRounding()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set("2");
		zrdecplrec.statuz.set(Varcom.oK);
		/*    MOVE CHDRLNB-CNTCURR        TO ZRDP-CURRENCY.                */
		zrdecplrec.batctrcde.set(batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			syserr();
		}
		/*EXIT*/
	}

		
	protected void updateFirstRcpPremDt(){
		getFirstRcpFrmRtrnpf();
		hpadpfDAO.updateFirstprmrcpdate(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString(), firstPremRcpDt);
	}
	
	protected void getFirstRcpFrmRtrnpf(){
		if(firstRcpPremRec.getSrcebus() != null && isEQ(firstRcpPremRec.getOnePCashlessFlag(),'Y') &&
				isEQ(firstRcpPremRec.getMop(),'D')) {
				datcon1.function.set(Varcom.tday);
				Datcon1.getInstance().mainline(datcon1.datcon1Rec);
				currentDate.set(datcon1.intDate);
					
					
				itempf = new Itempf();
				itempf.setItempfx("IT");
				itempf.setItemcoy("9");
				itempf.setItemtabl(th5ag);
				itempf.setItemitem(wsaaTodayYear.toString().trim().concat(firstRcpPremRec.getMop().toString()
							.trim()).concat(firstRcpPremRec.getFacthouse().toString()));
				itempf.setValidflag("1");
				itempf = itemDAO.getItemRecordByItemkey(itempf);			
				if (itempf != null) {
					th5agrec.th5agRec.set(StringUtil.rawToString(itempf.getGenarea()));			
				}
					
				if(isLTE(currentDate ,th5agrec.prcbilldte[wsaaTodayMnth.toInt()])) {
					firstPremRcpDt = th5agrec.bnktransfer[wsaaTodayMnth.toInt()].toInt();
				}
				else {
					firstPremRcpDt = th5agrec.bnktransfer[wsaaTodayMnth.toInt()+1].toInt();
				}
		}
		else {
			List<Rtrnpf> listRtrnpf=new ArrayList<>();
			BigDecimal totalPrem = totalPremium.getbigdata();
			if(chdrlnbIO.getChdrnum().toString() != null){
				listRtrnpf = rtrnpfDAO.findRtrnRecord(chdrlnbIO.getChdrnum().toString());
				BigDecimal origAmt = new BigDecimal(0);
				if (listRtrnpf != null && listRtrnpf.size() != 0) {
					for(int i=0; i< listRtrnpf.size(); i++){
						origAmt = origAmt.add(listRtrnpf.get(i).getOrigamt());
						if(origAmt.compareTo(totalPrem) >= 0 && isEQ(listRtrnpf.get(i).getSacstyp(),"SK")){
							firstPremRcpDt = listRtrnpf.get(i).getEffdate();
							break;
						}
					}
				}	
			}
		}
	}	
   
	
	protected void calcContractPremium()
	{
		/*    Read table TR52D using CHDRLNB-REGISTER as key               */

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy("2");
		itempf.setItemtabl(tablesInner.tr52d.toString());
		itempf.setItemitem(chdrlnbIO.getRegister().toString());
		itempf = itemDAO.getItempfRecord(itempf);

		if (null == itempf) {
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy("2");
			itempf.setItemtabl(tablesInner.tr52d.toString());
			itempf.setItemitem("***");
			itempf = itemDAO. getItempfRecord(itempf);
		}
		tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));
		/*    Read the contract definition details T5688 for the contract*/
		/*    type held on the contract header.*/	
		itdmpfList = itemDAO.getItdmByFrmdate("2", tablesInner.t5688.toString(), chdrlnbIO.getCnttype().toString(), firstRcpPremRec.getOccdate().toInt());

		if(itdmpfList != null && itdmpfList.size() != 0){

			t5688rec.t5688Rec.set(StringUtil.rawToString(itdmpfList.get(0).getGenarea()));
		}
		/*    Calculate the contract fee by using the subroutine found in*/
		/*    table T5674. See 1200 Section.*/
		if (isNE(t5688rec.feemeth, SPACES)) {
			calcFee();
		}
		else {
			mgfeelrec.mgfee.set(ZERO);
			cntFee.set(ZERO);
		}
		calcPremium();
		return;
	}

	
	protected void checkCalcCompTax(Covtpf covtpf)
	{
		wsaaCompTax.set(0);
		if (isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}
		// changed by yy for ILIFE-3960
		if (covtpf.getSingp().compareTo(BigDecimal.ZERO)==0
				&& covtpf.getInstprem().compareTo(BigDecimal.ZERO)==0) {
			return ;
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set(covtpf.getCrtable());
		readTr52e7200();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			readTr52e7200();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e7200();
		}
		/* Call TR52D tax subroutine                                       */
		if (isNE(tr52erec.taxind01, "Y")) {
			return ;
		}
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(Varcom.oK);
		txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		txcalcrec.life.set(covtpf.getLife());
		txcalcrec.coverage.set(covtpf.getCoverage());
		txcalcrec.rider.set(covtpf.getRider());
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.crtable.set(covtpf.getCrtable());
		txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
		txcalcrec.register.set(chdrlnbIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
		wsaaCntCurr.set(chdrlnbIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.effdate.set(firstRcpPremRec.getOccdate());
		txcalcrec.transType.set("PREM");
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		if (isNE(covtpf.getInstprem(), ZERO)) {
			if (isEQ(tr52erec.zbastyp, "Y")) {
				txcalcrec.amountIn.set(covtpf.getZbinstprem());
			}
			else {
				txcalcrec.amountIn.set(covtpf.getInstprem());
			}
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, Varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				syserr();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
					|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaRpTax.add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaRpTax.add(txcalcrec.taxAmt[2]);
				}
			}
		}
		// changed by yy for ILIFE-3960
		if (covtpf.getSingp().compareTo(BigDecimal.ZERO)!=0) {
			txcalcrec.amountIn.set(covtpf.getSingp());
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, Varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				syserr();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
					|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaSpTax.add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaSpTax.add(txcalcrec.taxAmt[2]);
				}
			}
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
				|| isGT(txcalcrec.taxAmt[2], ZERO)) {
					if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
						wsaaCompTax.add(txcalcrec.taxAmt[1]);
					}
					if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
						wsaaCompTax.add(txcalcrec.taxAmt[2]);
					}
				}
	}
	
	protected void checkCalcContTax()
	{
		wsaaTax.set(0);
		if (isEQ(mgfeelrec.mgfee, ZERO)) {
			return ;
		}
		if (isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set("****");
		readTr52e7200();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e7200();
		}
		if (isNE(tr52erec.taxind02, "Y")) {
			return ;
		}
		/* Call TR52D tax subroutine                                       */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(Varcom.oK);
		txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		txcalcrec.life.set(SPACES);
		txcalcrec.coverage.set(SPACES);
		txcalcrec.rider.set(SPACES);
		txcalcrec.crtable.set(SPACES);
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
		txcalcrec.register.set(chdrlnbIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
		wsaaCntCurr.set(chdrlnbIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.amountIn.set(wsaaCntfee);
		txcalcrec.effdate.set(firstRcpPremRec.getOccdate());
		txcalcrec.transType.set("CNTF");
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, Varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			syserr();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
				|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}

	
	protected void readTr52e7200()
	{
		tr52erec.tr52eRec.set(SPACES);
		Itempf itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy("2");
		itempf.setItmfrm(new BigDecimal(firstRcpPremRec.getOccdate().toString()));
		itempf.setItmto(new BigDecimal(firstRcpPremRec.getOccdate().toString()));
		itempf.setItemtabl(tablesInner.tr52e.toString());
		itempf.setItemitem( wsaaTr52eKey.toString());
		itempf.setValidflag("1");
		List<Itempf> itempfList = itemDAO.findByItemDates(itempf);	
		
		if (itempfList != null && itempfList.size() != 0 ) {
			tr52erec.tr52eRec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
	}
	
	protected void syserr()
	{
		/*PARA*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);		
		/*EXIT*/
		exitProgram();
	}
	
	private static final class TablesInner {
		/* TABLES */	
		private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
		private FixedLengthStringData t5674 = new FixedLengthStringData(5).init("T5674");		
		private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
		private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");	
		private FixedLengthStringData t6687 = new FixedLengthStringData(5).init("T6687");
	}

		
}