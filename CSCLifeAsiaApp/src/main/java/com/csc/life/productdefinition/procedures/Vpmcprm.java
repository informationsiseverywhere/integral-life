/*
 * File: Vpmcprm.java
 * Date: 30 August 2009 1:58:03
 * Author: Quipoz Limited
 * 
 * Class transformed from PRMPM01.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.life.agents.dataaccess.LifecmcTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  VPMS PREMIUM CALCULATION 
*

* </pre>
*/
public class Vpmcprm extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VPMCPRM";
		/* ERRORS */
	private String e107 = "E107";
	private String h053 = "H053";
	private String t5687 = "T5687";
	
	private String itemrec = "ITEMREC";
	private String chdrrec = "CHDRREC";
	private String chdrlnbrec = "CHDRLNBREC";
	private String lifecmcrec = "LIFECMCREC";
	
	private Premiumrec premiumrec = new Premiumrec();
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();	
	
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private T5687rec t5687rec = new T5687rec();
	private Varcom varcom = new Varcom();
	/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private LifecmcTableDAM lifecmcIO = new LifecmcTableDAM();
	
	private static final String SUBROUTINE_PREFIX = "CPRM";

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, exit1090, exit0190, 
	}

	public Vpmcprm() {
		super();
	}

public void mainline(Object... parmArray)
	{
		premiumrec.premiumRec = convertAndSetParam(premiumrec.premiumRec, parmArray, 0);
		try {
			startSubr0110();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr0110()
	{
		vpmcalcrec.vpmcalcRec.set(SPACES);
		premiumrec.statuz.set(varcom.oK);
		vpmcalcrec.statuz.set(varcom.oK);
		
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					setupKey1000();
					callVpmcalc200();
				}
				case exit0190: {
					exit0190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setupKey1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start1010();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1010()
{
	
	itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
	itdmIO.setItemtabl(t5687);
	itdmIO.setItemitem(premiumrec.crtable);
	itdmIO.setItmfrm(premiumrec.effectdt);
	itdmIO.setFormat(itemrec);
	itdmIO.setFunction(varcom.begn);
	itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.statuz, varcom.oK)
	|| isNE(premiumrec.chdrChdrcoy,itdmIO.getItemcoy())
	|| isNE(t5687,itdmIO.getItemtabl())
	|| isNE(premiumrec.crtable,itdmIO.getItemitem())) {
		premiumrec.statuz.set(h053);
		goTo(GotoLabel.exit1090);
	}
	t5687rec.t5687Rec.set(itdmIO.getGenarea());
	
	//---READ CHDR IF THE CNTTYPE IS BLANK
	if (isEQ(premiumrec.cnttype, SPACE)){
		chdrIO.setChdrpfx(fsupfxcpy.chdr);
		chdrIO.setChdrcoy(premiumrec.chdrChdrcoy);
		chdrIO.setChdrnum(premiumrec.chdrChdrnum);
		chdrIO.setValidflag("1");
		chdrIO.setCurrfrom(varcom.maxdate);
		chdrIO.setFormat(chdrrec);
		chdrIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, chdrIO);
		if (isEQ(chdrIO.statuz, varcom.oK)
				&& isEQ(fsupfxcpy.chdr,chdrIO.getChdrpfx())
				&& isEQ(premiumrec.chdrChdrcoy,chdrIO.getChdrcoy())
				&& isEQ(premiumrec.chdrChdrnum,chdrIO.getChdrnum())) {
			premiumrec.cnttype.set(chdrIO.getCnttype());
		} else {
			chdrlnbIO.setFormat(chdrlnbrec);
			chdrlnbIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isEQ(chdrlnbIO.statuz, varcom.oK)){
				premiumrec.cnttype.set(chdrlnbIO.getCnttype());
			}
		}
	}
	
	//FOR JOINT LIFE CASES, NEED TO DETERMINE WHICH CALC METHOD TO USE  
	vpmcalcrec.extkey.set(t5687rec.premmeth);
	if (isNE(t5687rec.jlPremMeth, SPACE)
			&& (isNE(t5687rec.jlPremMeth, t5687rec.premmeth))){
		lifecmcIO.setChdrcoy(premiumrec.chdrChdrcoy);
		lifecmcIO.setChdrnum(premiumrec.chdrChdrnum);
		lifecmcIO.setLife(premiumrec.lifeLife);
		lifecmcIO.setJlife("01");
		lifecmcIO.setFormat(lifecmcrec);
		lifecmcIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifecmcIO);
		if (isEQ(lifecmcIO.statuz, varcom.oK)){
			vpmcalcrec.extkey.set(t5687rec.jlPremMeth);
		}
	}
	vpmcalcrec.vpmskey.set(SPACES);
	vpmcalcrec.vpmscoy.set(premiumrec.chdrChdrcoy);
	vpmcalcrec.vpmstabl.set("TR28X");
	vpmcalcrec.vpmsitem.set(SUBROUTINE_PREFIX + premiumrec.cnttype.getData().trim());/* IJTI-1523 */
//	vpmcalcrec.vpmsitem.set(SUBROUTINE_PREFIX + premiumrec.crtable.getData().toString().trim().substring(0,3));//TODO
//	if (isEQ(premiumrec.lifeJlife, "01")){
//		vpmcalcrec.extkey.set(t5687rec.jlPremMeth);
//	} else {
//		vpmcalcrec.extkey.set(t5687rec.premmeth);
//	}

}
protected void callVpmcalc200()
	{
		if (isNE(premiumrec.statuz, varcom.oK)){
			goTo(GotoLabel.exit0190);
		}
		vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
		callProgram(Calccprm.class, vpmcalcrec.vpmcalcRec);
		premiumrec.premiumRec.set(vpmcalcrec.linkageArea);
		
		if (isNE(vpmcalcrec.statuz, varcom.oK)){
			premiumrec.statuz.set(vpmcalcrec.statuz);
		}
	}

protected void exit0190()
	{
		exitProgram();
		goBack();
	}


}
