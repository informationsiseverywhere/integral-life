/*
 * File: Vpxchdr.java
 * Date: 20 August 2012 22:49:52
 * Author: Quipoz Limited
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpxliferec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* This routine performs data extraction from LIFE for the
* purpose of passing the said data to the VP/MS calculation
* subroutine.
*
* Functions:
* ---------
* INIT   - returns the various fields needed from LIFE
* GETV   - Returns the value of a single variable to the caller.
*
* Outputs:
* --------
* LSMOKING - Main Life Smoking Indicator
* JSMOKING - Joint Life Smoking Indicator
* LCLTDOB  - Main Life Date Of Birth
* JCLTDOB  - Joint Life Date Of Birth
*
* Status:
*
*      **** - Everything checks out ok.
*      E049 - Invalid Function
*      H832 - Field not valid
*      MRNF - Main LIFE record not found.
*
***********************************************************************
*                                                                     *
* ......... New Version of the Amendment History.                     *
*                                                                     *
***********************************************************************
*           AMENDMENT  HISTORY                                        *
***********************************************************************
* DATE.... VSN/MOD  WORK UNIT    BY....                               *
*                                                                     *
* 16/05/12  01/01   V77F01       Jessico Nocon/FSG/CSC (Singapo       *
*           Initial Version                                           *
*                                                                     *
**DD/MM/YY*************************************************************
* </pre>
*/
public class Vpxlife extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VPXLIFE";
		/* FORMATS */
	private String liferec = "LIFEREC";
		/* ERRORS */
	private String e049 = "E049";
	private String h832 = "H832";
	
	private Syserrrec syserrrec = new Syserrrec();
	private Vpxliferec vpxliferec= new Vpxliferec();
	private Premiumrec premiumrec = new Premiumrec();
	private Varcom varcom = new Varcom();
	
	/*Life and joint life details - new busine*/
	private LifeTableDAM lifeIO = new LifeTableDAM();
	
	public FixedLengthStringData wsaaNumFmt = new FixedLengthStringData(20);
	public FixedLengthStringData wsaaNumArrs = new FixedLengthStringData(20).isAPartOf(wsaaNumFmt, 0, REDEFINE);
  	public FixedLengthStringData[] wsaaNumArr = FLSArrayPartOfStructure(20, 1, wsaaNumArrs, 0);
  	public ZonedDecimalData wsaaNumD0 = new ZonedDecimalData(18, 0).isAPartOf(wsaaNumFmt, 0, REDEFINE).setUnsigned();
  	public ZonedDecimalData wsaaNumD1 = new ZonedDecimalData(17, 1).isAPartOf(wsaaNumFmt, 0, REDEFINE);
  	public ZonedDecimalData wsaaNumD2 = new ZonedDecimalData(16, 2).isAPartOf(wsaaNumFmt, 0, REDEFINE);
  	public ZonedDecimalData wsaaNumD3 = new ZonedDecimalData(15, 3).isAPartOf(wsaaNumFmt, 0, REDEFINE);
  	public ZonedDecimalData wsaaNumD4 = new ZonedDecimalData(14, 4).isAPartOf(wsaaNumFmt, 0, REDEFINE);
  	public ZonedDecimalData wsaaNumD5 = new ZonedDecimalData(13, 5).isAPartOf(wsaaNumFmt, 0, REDEFINE);
  	public ZonedDecimalData wsaaNumD6 = new ZonedDecimalData(12, 6).isAPartOf(wsaaNumFmt, 0, REDEFINE);
  	public ZonedDecimalData wsaaNumD7 = new ZonedDecimalData(11, 7).isAPartOf(wsaaNumFmt, 0, REDEFINE);
  	public ZonedDecimalData wsaaNumD8 = new ZonedDecimalData(10, 8).isAPartOf(wsaaNumFmt, 0, REDEFINE);
  	public ZonedDecimalData wsaaNumD9 = new ZonedDecimalData(9, 9).isAPartOf(wsaaNumFmt, 0, REDEFINE);
  	
	private PackedDecimalData ia = new PackedDecimalData(5, 0);
	private PackedDecimalData ib = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaStart = new FixedLengthStringData(1);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit9020, 
		exit400, 
		nextLife300, exit100, exit150
	}

	public Vpxlife() {
		super();
	}

public void mainline(Object... parmArray)
	{
		vpxliferec = (Vpxliferec) parmArray[1];
		premiumrec.premiumRec = convertAndSetParam(premiumrec.premiumRec, parmArray, 0);
		try {
			para000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void exit1010()
{
	exitProgram();
}

protected void para000()
{
	syserrrec.subrname.set(wsaaSubr);
	vpxliferec.statuz.set(varcom.oK);
	if (isEQ(vpxliferec.function,"INIT")){
		initialize050();
		findLife100();
	}
	else if (isEQ(vpxliferec.function,"GETV")) {
		returnValue200();
	}
	else {
		syserrrec.statuz.set(e049);
		vpxliferec.statuz.set(e049);
		systemError900();
	}
//	para010();
	exit000();
}

protected void initialize050()
{
	vpxliferec.lcltdob.set(varcom.maxdate);
	vpxliferec.jcltdob.set(varcom.maxdate);
	vpxliferec.lsSmoking.set(SPACE);
	vpxliferec.jsSmoking.set(SPACE);
}

protected void findLife100()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				search100();
			}
			case exit100: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void search100()
{
	//Set-up keys
	lifeIO.setDataKey(SPACES);
	lifeIO.setChdrcoy(premiumrec.chdrChdrcoy);
	lifeIO.setChdrnum(premiumrec.chdrChdrnum);
	lifeIO.setLife(premiumrec.lifeLife);
	lifeIO.setJlife("00");
	lifeIO.setCurrfrom(varcom.maxdate);
	
	//Read the file
	readLife100x();

	//Unexpected result
	if (isNE(lifeIO.getStatuz(),varcom.oK)
	&& isNE(lifeIO.getStatuz(),varcom.mrnf)) {
		syserrrec.statuz.set(lifeIO.getStatuz());
		syserrrec.params.set(lifeIO.getParams());
		dbError999();
	}
	
	//Record not found - no joint life
	if (isEQ(lifeIO.getStatuz(),varcom.endp)
			&& isNE(lifeIO.getChdrcoy(), premiumrec.chdrChdrcoy)
			&& isNE(lifeIO.getChdrnum(), premiumrec.chdrChdrnum)
			&& isNE(lifeIO.getLife(), premiumrec.lifeLife)
			&& isNE(lifeIO.getJlife(), "00")){
		vpxliferec.statuz.set(varcom.mrnf);
		goTo(GotoLabel.exit100);
	}

	//Set up output field for JLife
	vpxliferec.lsSmoking.set(lifeIO.smoking);
	vpxliferec.lcltdob.set(lifeIO.cltdob);
	findJLife150();
}

protected void findJLife150()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				search150();
			}
			case exit150: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void search150()
{
	//Set-up keys
	lifeIO.setDataKey(SPACES);
	lifeIO.setChdrcoy(premiumrec.chdrChdrcoy);
	lifeIO.setChdrnum(premiumrec.chdrChdrnum);
	lifeIO.setLife(premiumrec.lifeLife);
	lifeIO.setJlife("01");
	lifeIO.setCurrfrom(varcom.maxdate);
	
	//Read the file
	readLife100x();
	
	//Unexpected result
	if (isNE(lifeIO.getStatuz(),varcom.oK)
			&& isNE(lifeIO.getStatuz(),varcom.mrnf)) {
		syserrrec.statuz.set(lifeIO.getStatuz());
		syserrrec.params.set(lifeIO.getParams());
		dbError999();
	}
	//Record not found - no joint life
	if (isEQ(lifeIO.getStatuz(),varcom.endp)
			&& isNE(lifeIO.getChdrcoy(), premiumrec.chdrChdrcoy)
			&& isNE(lifeIO.getChdrnum(), premiumrec.chdrChdrnum)
			&& isNE(lifeIO.getLife(), premiumrec.lifeLife)
			&& isNE(lifeIO.getJlife(), "01")){
		goTo(GotoLabel.exit150);
	}
	
	//Set up output field for JLife
	vpxliferec.jsSmoking.set(lifeIO.smoking);
	vpxliferec.jcltdob.set(lifeIO.cltdob);
}

protected void readLife100x()
	{
		lifeIO.setStatuz(varcom.oK);
		lifeIO.setFunction(varcom.begn);
		lifeIO.setFormat(liferec);
		SmartFileCode.execute(appVars, lifeIO);
	}

protected void returnValue200()
	{
		start210();
	}
	
	
protected void start210()
	{
	vpxliferec.resultValue.set(SPACES);
	String field = vpxliferec.resultField.getData();/* IJTI-1523 */
	if ("lcltdob".equals(field)){
		vpxliferec.resultValue.set(vpxliferec.lcltdob);
	//	wsaaNumD0.set(vpxliferec.occdate);
	//	formatNumbera100();
	}
	else if("jcltdob".equals(field)){
		vpxliferec.resultValue.set(vpxliferec.jcltdob);
	}
	else if("lsmoking".equals(field)){
		vpxliferec.resultValue.set(vpxliferec.lsSmoking);
	}
	else if("jsSmoking".equals(field)){
		vpxliferec.resultValue.set(vpxliferec.jsSmoking);
	}
	else {
		vpxliferec.statuz.set(h832);
	}
}
	
protected void systemError900()
{
	/*PARA*/
	syserrrec.syserrStatuz.set(syserrrec.statuz);
	syserrrec.syserrType.set("2");
	callProgram(Syserr.class, syserrrec.syserrRec);
	exit000();
	/*EXIT*/
}
protected void dbError999()
{
	/*PARA*/
	syserrrec.syserrType.set("1");
	callProgram(Syserr.class, syserrrec.syserrRec);
	exit000();
	/*EXIT*/
}

protected void exit000()
{
	exitProgram();
}

protected void formatNumbera100(){
	starta110();
}

protected void starta110(){
	wsaaStart.set("N");
	ia.set(ZERO);
	ib.set(ZERO);
	StringBuilder sb = new StringBuilder();
	do {
		ia.add(1);
		if (isGT(wsaaNumArr[ia.toInt()], 0) 
				&& isLTE(wsaaNumArr[ia.toInt()], 9)){
			wsaaStart.set("Y");
		}
		if (isEQ(wsaaNumArr[ia.toInt()], ".")) {
			if (isEQ(wsaaStart, "N")) {
				ib.add(1);
	//			vpxliferec.resultValue.set("0");//TO DO
				sb.append("0");
			}
			wsaaStart.set("Y");
		}
		if (isEQ(wsaaStart, "Y")
				&& (isNE(wsaaNumArr[ia.toInt()], SPACES) 
				|| isEQ(wsaaNumArr[ia.toInt()], "-"))){
			ib.add(1);
	//		vpxliferec.resultValue.set(wsaaNumArr[ia.toInt()]);//TO DO
			sb.append(wsaaNumArr[ia.toInt()]);
		}
	} while (isLTE(ia, 20));
	
	vpxliferec.resultValue.set(sb.toString());
	
	if (isEQ(wsaaStart, "N")){
		vpxliferec.resultValue.set("0");
	}
	wsaaNumFmt.set(SPACES);
	}
}
