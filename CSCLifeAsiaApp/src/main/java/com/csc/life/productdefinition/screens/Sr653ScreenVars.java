package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR653
 * @version 1.0 generated on 30/08/09 07:22
 * @author Quipoz
 */
public class Sr653ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(262);
	public FixedLengthStringData dataFields = new FixedLengthStringData(102).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData exmcode = DD.exmcode.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData mdblactf = DD.mdblactf.copy().isAPartOf(dataFields,20);
	public FixedLengthStringData paidby = DD.paidby.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData pgmnam = DD.pgmnam.copy().isAPartOf(dataFields,22);
	public FixedLengthStringData zdocno = DD.zdocno.copy().isAPartOf(dataFields,27);
	public FixedLengthStringData optdscs = new FixedLengthStringData(60).isAPartOf(dataFields, 42);
	public FixedLengthStringData[] optdsc = FLSArrayPartOfStructure(4, 15, optdscs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(optdscs, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01 = DD.optdsc.copy().isAPartOf(filler,0);
	public FixedLengthStringData optdsc02 = DD.optdsc.copy().isAPartOf(filler,15);
	public FixedLengthStringData optdsc03 = DD.optdsc.copy().isAPartOf(filler,30);
	public FixedLengthStringData optdsc04 = DD.optdsc.copy().isAPartOf(filler,45);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 102);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData exmcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData mdblactfErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData paidbyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData pgmnamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData zdocnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData optdscsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] optdscErr = FLSArrayPartOfStructure(4, 4, optdscsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(16).isAPartOf(optdscsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData optdsc02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData optdsc03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData optdsc04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 142);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] exmcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] mdblactfOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] paidbyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] pgmnamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] zdocnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData optdscsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] optdscOut = FLSArrayPartOfStructure(4, 12, optdscsOut, 0);
	public FixedLengthStringData[][] optdscO = FLSDArrayPartOfArrayStructure(12, 1, optdscOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(optdscsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optdsc01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] optdsc02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] optdsc03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] optdsc04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(372);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(114).isAPartOf(subfileArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData descript = DD.descript.copy().isAPartOf(subfileFields,8);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,32);
	public FixedLengthStringData ent = DD.ent.copy().isAPartOf(subfileFields,40);
	public FixedLengthStringData invref = DD.invref.copy().isAPartOf(subfileFields,50);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(subfileFields,65);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,67);
	public FixedLengthStringData mdblact = DD.mdblact.copy().isAPartOf(subfileFields,69);
	public ZonedDecimalData paydte = DD.paydte.copyToZonedDecimal().isAPartOf(subfileFields,70);
	public FixedLengthStringData payto = DD.payto.copy().isAPartOf(subfileFields,78);
	public ZonedDecimalData premium = DD.premium.copyToZonedDecimal().isAPartOf(subfileFields,79);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,92);
	public ZonedDecimalData seqno = DD.seqno.copyToZonedDecimal().isAPartOf(subfileFields,93);
	public FixedLengthStringData shortdesc = DD.shortdesc.copy().isAPartOf(subfileFields,95);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(subfileFields,105);
	public FixedLengthStringData zmedtyp = DD.zmedtyp.copy().isAPartOf(subfileFields,106);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(64).isAPartOf(subfileArea, 114);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData descriptErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData entErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData invrefErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData mdblactErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData paydteErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData paytoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData premiumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData seqnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData shortdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	public FixedLengthStringData validflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 56);
	public FixedLengthStringData zmedtypErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 60);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(192).isAPartOf(subfileArea, 178);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] descriptOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] entOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] invrefOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] mdblactOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] paydteOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] paytoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] premiumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] seqnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] shortdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	public FixedLengthStringData[] validflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 168);
	public FixedLengthStringData[] zmedtypOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 180);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 370);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData paydteDisp = new FixedLengthStringData(10);

	public LongData Sr653screensflWritten = new LongData(0);
	public LongData Sr653screenctlWritten = new LongData(0);
	public LongData Sr653screenWritten = new LongData(0);
	public LongData Sr653protectWritten = new LongData(0);
	public GeneralTable sr653screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr653screensfl;
	}

	public Sr653ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mdblactfOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrselOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdocnoOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(paidbyOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(exmcodeOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {seqno, validflag, select, mdblact, shortdesc, chdrnum, invref, payto, ent, descript, life, jlife, zmedtyp, effdate, premium, paydte};
		screenSflOutFields = new BaseData[][] {seqnoOut, validflagOut, selectOut, mdblactOut, shortdescOut, chdrnumOut, invrefOut, paytoOut, entOut, descriptOut, lifeOut, jlifeOut, zmedtypOut, effdateOut, premiumOut, paydteOut};
		screenSflErrFields = new BaseData[] {seqnoErr, validflagErr, selectErr, mdblactErr, shortdescErr, chdrnumErr, invrefErr, paytoErr, entErr, descriptErr, lifeErr, jlifeErr, zmedtypErr, effdateErr, premiumErr, paydteErr};
		screenSflDateFields = new BaseData[] {effdate, paydte};
		screenSflDateErrFields = new BaseData[] {effdateErr, paydteErr};
		screenSflDateDispFields = new BaseData[] {effdateDisp, paydteDisp};

		screenFields = new BaseData[] {optdsc01, optdsc02, optdsc03, optdsc04, pgmnam, mdblactf, chdrsel, zdocno, paidby, exmcode};
		screenOutFields = new BaseData[][] {optdsc01Out, optdsc02Out, optdsc03Out, optdsc04Out, pgmnamOut, mdblactfOut, chdrselOut, zdocnoOut, paidbyOut, exmcodeOut};
		screenErrFields = new BaseData[] {optdsc01Err, optdsc02Err, optdsc03Err, optdsc04Err, pgmnamErr, mdblactfErr, chdrselErr, zdocnoErr, paidbyErr, exmcodeErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr653screen.class;
		screenSflRecord = Sr653screensfl.class;
		screenCtlRecord = Sr653screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr653protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr653screenctl.lrec.pageSubfile);
	}
}
