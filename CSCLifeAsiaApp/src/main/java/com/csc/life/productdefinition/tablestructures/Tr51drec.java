package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:19:44
 * Description:
 * Copybook name: TR51DREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr51drec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr51dRec = new FixedLengthStringData(500);
  	public FixedLengthStringData srcebuss = new FixedLengthStringData(100).isAPartOf(tr51dRec, 0);
  	public FixedLengthStringData[] srcebus = FLSArrayPartOfStructure(50, 2, srcebuss, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(100).isAPartOf(srcebuss, 0, FILLER_REDEFINE);
  	public FixedLengthStringData srcebus01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData srcebus02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData srcebus03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData srcebus04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData srcebus05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData srcebus06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData srcebus07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData srcebus08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData srcebus09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData srcebus10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData srcebus11 = new FixedLengthStringData(2).isAPartOf(filler, 20);
  	public FixedLengthStringData srcebus12 = new FixedLengthStringData(2).isAPartOf(filler, 22);
  	public FixedLengthStringData srcebus13 = new FixedLengthStringData(2).isAPartOf(filler, 24);
  	public FixedLengthStringData srcebus14 = new FixedLengthStringData(2).isAPartOf(filler, 26);
  	public FixedLengthStringData srcebus15 = new FixedLengthStringData(2).isAPartOf(filler, 28);
  	public FixedLengthStringData srcebus16 = new FixedLengthStringData(2).isAPartOf(filler, 30);
  	public FixedLengthStringData srcebus17 = new FixedLengthStringData(2).isAPartOf(filler, 32);
  	public FixedLengthStringData srcebus18 = new FixedLengthStringData(2).isAPartOf(filler, 34);
  	public FixedLengthStringData srcebus19 = new FixedLengthStringData(2).isAPartOf(filler, 36);
  	public FixedLengthStringData srcebus20 = new FixedLengthStringData(2).isAPartOf(filler, 38);
  	public FixedLengthStringData srcebus21 = new FixedLengthStringData(2).isAPartOf(filler, 40);
  	public FixedLengthStringData srcebus22 = new FixedLengthStringData(2).isAPartOf(filler, 42);
  	public FixedLengthStringData srcebus23 = new FixedLengthStringData(2).isAPartOf(filler, 44);
  	public FixedLengthStringData srcebus24 = new FixedLengthStringData(2).isAPartOf(filler, 46);
  	public FixedLengthStringData srcebus25 = new FixedLengthStringData(2).isAPartOf(filler, 48);
  	public FixedLengthStringData srcebus26 = new FixedLengthStringData(2).isAPartOf(filler, 50);
  	public FixedLengthStringData srcebus27 = new FixedLengthStringData(2).isAPartOf(filler, 52);
  	public FixedLengthStringData srcebus28 = new FixedLengthStringData(2).isAPartOf(filler, 54);
  	public FixedLengthStringData srcebus29 = new FixedLengthStringData(2).isAPartOf(filler, 56);
  	public FixedLengthStringData srcebus30 = new FixedLengthStringData(2).isAPartOf(filler, 58);
  	public FixedLengthStringData srcebus31 = new FixedLengthStringData(2).isAPartOf(filler, 60);
  	public FixedLengthStringData srcebus32 = new FixedLengthStringData(2).isAPartOf(filler, 62);
  	public FixedLengthStringData srcebus33 = new FixedLengthStringData(2).isAPartOf(filler, 64);
  	public FixedLengthStringData srcebus34 = new FixedLengthStringData(2).isAPartOf(filler, 66);
  	public FixedLengthStringData srcebus35 = new FixedLengthStringData(2).isAPartOf(filler, 68);
  	public FixedLengthStringData srcebus36 = new FixedLengthStringData(2).isAPartOf(filler, 70);
  	public FixedLengthStringData srcebus37 = new FixedLengthStringData(2).isAPartOf(filler, 72);
  	public FixedLengthStringData srcebus38 = new FixedLengthStringData(2).isAPartOf(filler, 74);
  	public FixedLengthStringData srcebus39 = new FixedLengthStringData(2).isAPartOf(filler, 76);
  	public FixedLengthStringData srcebus40 = new FixedLengthStringData(2).isAPartOf(filler, 78);
  	public FixedLengthStringData srcebus41 = new FixedLengthStringData(2).isAPartOf(filler, 80);
  	public FixedLengthStringData srcebus42 = new FixedLengthStringData(2).isAPartOf(filler, 82);
  	public FixedLengthStringData srcebus43 = new FixedLengthStringData(2).isAPartOf(filler, 84);
  	public FixedLengthStringData srcebus44 = new FixedLengthStringData(2).isAPartOf(filler, 86);
  	public FixedLengthStringData srcebus45 = new FixedLengthStringData(2).isAPartOf(filler, 88);
  	public FixedLengthStringData srcebus46 = new FixedLengthStringData(2).isAPartOf(filler, 90);
  	public FixedLengthStringData srcebus47 = new FixedLengthStringData(2).isAPartOf(filler, 92);
  	public FixedLengthStringData srcebus48 = new FixedLengthStringData(2).isAPartOf(filler, 94);
  	public FixedLengthStringData srcebus49 = new FixedLengthStringData(2).isAPartOf(filler, 96);
  	public FixedLengthStringData srcebus50 = new FixedLengthStringData(2).isAPartOf(filler, 98);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(400).isAPartOf(tr51dRec, 100, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr51dRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr51dRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}