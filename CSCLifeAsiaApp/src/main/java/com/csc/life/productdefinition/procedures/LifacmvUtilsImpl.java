/*********************  */
/*Author  :Liwei				  		*/
/*Purpose :Instead of subroutine Lifacmv*/
/*Date    :2018.10.26				*/
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceAll;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceFirst;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.dao.DglxpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.accounting.dataaccess.model.Dglxpf;
import com.csc.fsu.accounting.tablestructures.Tr362rec;
import com.csc.fsu.general.dataaccess.dao.AcmvpfDAO;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.model.Acmvpf;
import com.csc.fsu.general.procedures.PostyymmPojo;
import com.csc.fsu.general.procedures.PostyymmUtils;
import com.csc.fsu.general.procedures.ZrdecplcPojo;
import com.csc.fsu.general.procedures.ZrdecplcUtils;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.general.tablestructures.Trcdecpy;
import com.csc.integral.context.IntegralApplicationContext;
import com.csc.life.agents.dataaccess.dao.AgpypfDAO;
import com.csc.life.agents.dataaccess.model.Agpypf;
import com.csc.life.statistics.procedures.RlagprdPojo;
import com.csc.life.statistics.procedures.RlagprdUtils;
import com.csc.smart.procedures.BatcupPojo;
import com.csc.smart.procedures.BatcupUtils;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class LifacmvUtilsImpl implements LifacmvUtils {
	private static final Logger LOGGER = LoggerFactory.getLogger(LifacmvUtilsImpl.class);
	public static final String ROUTINE = QPUtilities.getThisClass();

		/* WSAA-TABLE */
	private FixedLengthStringData[] wsaaT3695Entries = FLSInittedArray (50, 3);
	private FixedLengthStringData[] wsaaT3695Sacstyp = FLSDArrayPartOfArrayStructure(2, wsaaT3695Entries, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT3695Intextind = FLSDArrayPartOfArrayStructure(1, wsaaT3695Entries, 2);
	private int iz = 0;
	private String wsaaLastOrigCcy = "";
	private String wsaaLedgerCcy = "";
	private BigDecimal wsaaNominalRate = BigDecimal.ZERO;
	private BigDecimal wsaaOrigamt = BigDecimal.ZERO;
	private String wsaaSubscode = "";
	private String wsaaSubscode01;
	private String wsaaSubscode02;
	private String wsaaSubscode03;
	private String wsaaSubscode04;
	private String wsaaMachineDate;

	private String wsaaMachineTimex;
	private T3629rec t3629rec = new T3629rec();
	private T3695rec t3695rec = new T3695rec();
	private Tr362rec tr362rec = new Tr362rec();
	private Varcom varcom = new Varcom();
	private Syserr syserr = new Syserr();
	private Trcdecpy trcdecpy = new Trcdecpy();
	protected Syserrrec syserrrec = new Syserrrec();
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private AcmvpfDAO acmvDAO = getApplicationContext().getBean("acmvpfDAO", AcmvpfDAO.class);
	private DglxpfDAO dglxpfDAO = getApplicationContext().getBean("dglxpfDAO",DglxpfDAO.class); 
	private AgpypfDAO agpypfDAO = getApplicationContext().getBean("agpypfDAO",AgpypfDAO.class); 
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrDAO", ChdrpfDAO.class);
	private AcblpfDAO acblDAO = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class); 
	private PostyymmUtils postyymmUtils = getApplicationContext().getBean("postyymmUtils", PostyymmUtils.class);
	private PostyymmPojo postyymmPojo = null;
	private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
	private ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
	private BatcupUtils batcupUtils = getApplicationContext().getBean("batcupUtils", BatcupUtils.class);
	private BatcupPojo batcupPojo = null;
	private RlagprdUtils rlagprdUtils = getApplicationContext().getBean("rlagprdUtils", RlagprdUtils.class);
	private RlagprdPojo rlagprdPojo = null;
	
	private List<Itempf> itempfList = null;
	private Chdrpf chdrlif = null;
	private Acmvpf acmvpf = null;
	private Dglxpf dglxpf = null;
	private Agpypf agpyagt = null;
	private Acblpf acblpf = null;
	public void calcLifacmv(LifacmvPojo lifacmvPojo) {
		lifacmvPojo.setStatuz("****");
		syserrrec.subrname.set("LIFACMV");
		if (!"PSTW".equals(lifacmvPojo.getFunction()) && !"NPSTW".equals(lifacmvPojo.getFunction())) {
			lifacmvPojo.setStatuz("E049");
			return ;
		}
		checkSchedule24x7Setup4000(lifacmvPojo);
		writeAcmv1000(lifacmvPojo);
		if ("Y".equals(t3695rec.comind.toString()) && lifacmvPojo.getFrcdate() == 99999999 && 
				"E".equals(t3695rec.intextind.toString()) && "LA".equals(lifacmvPojo.getSacscode())) {
			writeAgpyagt1300(acmvpf);
		}
		if ("Y".equals(t3695rec.tlprmflg.toString().trim())|| "Y".equals(t3695rec.tlcomflg.toString().trim())){
			chdrlif = readContractDetails800(lifacmvPojo);
			rlagprdPojo = new RlagprdPojo();
			rlagprdPojo.setAgntcoy(lifacmvPojo.getBatccoy());
			rlagprdPojo.setAgntnum(chdrlif.getAgntnum());
			rlagprdPojo.setActyear(lifacmvPojo.getBatcactyr());
			rlagprdPojo.setActmnth(lifacmvPojo.getBatcactmn());
			rlagprdPojo.setAgtype("");
			/* To use issue date to check against MACFPRD*/
			rlagprdPojo.setChdrnum(chdrlif.getChdrnum());
			rlagprdPojo.setOccdate(chdrlif.getOccdate());
			rlagprdPojo.setCnttype(chdrlif.getCnttype());
			rlagprdPojo.setChdrcoy(chdrlif.getChdrcoy().toString());
			rlagprdPojo.setBatctrcde(lifacmvPojo.getBatctrcde());
			rlagprdPojo.setEffdate(lifacmvPojo.getEffdate());
			/* Check the LIFA-GLSIGN.*/
			wsaaOrigamt = lifacmvPojo.getOrigamt();
			rlagprdPojo.setStatuz("****");
			if (isEQ(t3695rec.tlprmflg, "Y")) {
				rlagprdPojo.setPrdflg("P");
				if ("-".equals(lifacmvPojo.getGlsign())) {
					wsaaOrigamt = lifacmvPojo.getOrigamt().multiply(new BigDecimal(-1)).setScale(2);
				}
				wsaaOrigamt = lifacmvPojo.getOrigamt().multiply(new BigDecimal(-1)).setScale(2);
			}
			else {
				rlagprdPojo.setPrdflg("C");
			}
			rlagprdPojo.setOrigamt(wsaaOrigamt);
			rlagprdUtils.callRlagprd(rlagprdPojo);
			if (isEQ(rlagprdPojo.getStatuz(), varcom.bomb.toString())) {
				syserrrec.statuz.set(varcom.bomb);
				syserr570(lifacmvPojo,"s");
			}
		}	
		if (("PSTW".equals(lifacmvPojo.getFunction()) && !"".equals(lifacmvPojo.getSacscode()))) {
			updateAcbl2000(lifacmvPojo);
		}
		callBatcup3000(lifacmvPojo);
	}
	
	protected void updateAcbl2000(LifacmvPojo lifacmvPojo){
		acblpf = acblDAO.getAcblpfRecord(lifacmvPojo.getRldgcoy(), lifacmvPojo.getSacscode(), lifacmvPojo.getRldgacct(),lifacmvPojo.getSacstyp(),lifacmvPojo.getOrigcurr() );
		if (acblpf == null) {
			acblpf = new Acblpf();
			acblpf.setRldgcoy(lifacmvPojo.getRldgcoy());
			acblpf.setSacscode(lifacmvPojo.getSacscode());
			acblpf.setRldgacct(lifacmvPojo.getRldgacct());
			acblpf.setSacstyp(lifacmvPojo.getSacstyp());
			acblpf.setOrigcurr(lifacmvPojo.getOrigcurr());
			acblpf.setSacscurbal(BigDecimal.ZERO);
		}
		if ("-".equals(lifacmvPojo.getGlsign())) {
			setPrecision(acblpf.getSacscurbal(), 2);
			acblpf.setSacscurbal(acblpf.getSacscurbal().subtract(lifacmvPojo.getOrigamt()));
		}
		else {
			setPrecision(acblpf.getSacscurbal(), 2);
			acblpf.setSacscurbal(acblpf.getSacscurbal().add(lifacmvPojo.getOrigamt()));
		}
		
		zrdecplcPojo.setAmountIn(acblpf.getSacscurbal());
		zrdecplcPojo.setCurrency(acblpf.getOrigcurr());
		a000CallRounding(lifacmvPojo);
		acblpf.setSacscurbal(new BigDecimal(zrdecplcPojo.getAmountOut().toString().replace("}", "")));
		List<Acblpf> acblpfList = new ArrayList<Acblpf>();
		acblpfList.add(acblpf);
		if(acblpf.getUnique_number() == null || "".equals(acblpf.getUnique_number())){
			acblDAO.insertAcblpfList(acblpfList);
		}else{
			acblDAO.updateAcblRecord(acblpfList);
		}
	}
	public void checkSchedule24x7Setup4000(LifacmvPojo lifacmvPojo){
		itempfList = itemDao.getAllItemitem("IT",lifacmvPojo.getBatccoy(), "TR362","***");
		if (itempfList != null && itempfList.size()> 0 ) {
			tr362rec.tr362Rec.set((StringUtil.rawToString(itempfList.get(0).getGenarea())));
		}
		else {
			tr362rec.tr362Rec.set(SPACES);
		}
	}
	public void writeAcmv1000(LifacmvPojo lifacmvPojo){
		glSubstitution1011(lifacmvPojo);
		
		/*  If accounting currency details not entered,*/
		/*   and the original currency has changed since the last time*/
		/*     - look up the nominal exchange rate.*/

		if ((lifacmvPojo.getGenlcur() == null || "".equals(lifacmvPojo.getGenlcur())) && isNE(lifacmvPojo.getOrigcurr(),wsaaLastOrigCcy)) {
			getRate1100(lifacmvPojo,lifacmvPojo.getOrigcurr());
		}
		/*  Go and read T3695 to get the Internal/External Indicator for*/
		/*  the Subaccount Type being posted to.*/
		if (!"".equals(lifacmvPojo.getSacstyp())) {
			getIntextind1200(lifacmvPojo);
		}
		else {
			t3695rec.intextind.set(SPACES);
		}
		wsaaMachineDate = getCobolDate();
		wsaaMachineTimex = getCobolTime();
		acmvpf = new Acmvpf();
		acmvpf.setBatccoy(lifacmvPojo.getBatccoy());
		acmvpf.setBatcbrn(lifacmvPojo.getBatcbrn());
		acmvpf.setBatcactyr(lifacmvPojo.getBatcactyr());
		acmvpf.setBatcactmn(lifacmvPojo.getBatcactmn());
		acmvpf.setBatctrcde(lifacmvPojo.getBatctrcde());
		acmvpf.setBatcbatch(lifacmvPojo.getBatcbatch());
		acmvpf.setRdocnum(lifacmvPojo.getRdocnum());
		acmvpf.setRdoccoy(lifacmvPojo.getRldgcoy());
		acmvpf.setTranno(lifacmvPojo.getTranno());
		acmvpf.setJrnseq(lifacmvPojo.getJrnseq());
		acmvpf.setRdocnum(lifacmvPojo.getRdocnum());
		acmvpf.setRldgcoy(lifacmvPojo.getRldgcoy());
		acmvpf.setSacscode(lifacmvPojo.getSacscode());
		acmvpf.setRldgacct(lifacmvPojo.getRldgacct());
		acmvpf.setOrigcurr(lifacmvPojo.getOrigcurr());
		acmvpf.setSacstyp(lifacmvPojo.getSacstyp());
		acmvpf.setOrigamt(lifacmvPojo.getOrigamt());
		acmvpf.setTranref(lifacmvPojo.getTranref());
		acmvpf.setTrandesc(lifacmvPojo.getTrandesc());
		acmvpf.setCrate(lifacmvPojo.getCrate().floatValue());
		acmvpf.setAcctamt(lifacmvPojo.getAcctamt());
		acmvpf.setGenlcoy(lifacmvPojo.getGenlcoy());
		acmvpf.setGenlcur(lifacmvPojo.getGenlcur());
		acmvpf.setGlcode(lifacmvPojo.getGlcode());
		acmvpf.setGlsign(lifacmvPojo.getGlsign());

		postyymmPojo = new PostyymmPojo();
		postyymmPojo.setFunction("GET");
		postyymmPojo.setBatccoy(lifacmvPojo.getBatccoy());
		postyymmPojo.setBatcbrn(lifacmvPojo.getBatcbrn());
		postyymmPojo.setBatcactyr(lifacmvPojo.getBatcactyr());
		postyymmPojo.setBatcactmn(lifacmvPojo.getBatcactmn());
		postyymmPojo.setBatctrcde(lifacmvPojo.getBatctrcde());
		postyymmPojo.setBatcbatch(lifacmvPojo.getBatcbatch());
		postyymmPojo.setEffdate(lifacmvPojo.getEffdate());
		postyymmUtils.callPostyymm(postyymmPojo);
		if (isNE(postyymmPojo.getStatuz(), varcom.oK)) {
			syserrrec.params.set(postyymmPojo);
			syserrrec.statuz.set(postyymmPojo.getStatuz());
			syserr570(lifacmvPojo,"s");
		}
		acmvpf.setPostyear(postyymmPojo.getPostyear());
		acmvpf.setPostmonth(postyymmPojo.getPostmonth());
		acmvpf.setEffdate(lifacmvPojo.getEffdate());
		acmvpf.setRcamt(lifacmvPojo.getRcamt());
		acmvpf.setFrcdate(lifacmvPojo.getFrcdate());
		//ILIFE-7901 start by dpuhawan
		//acmvpf.setTrdt(Integer.parseInt(wsaaMachineDate));
		//acmvpf.setTrtm(Integer.parseInt(wsaaMachineTimex));
		acmvpf.setTrdt(Integer.parseInt(wsaaMachineDate.substring(0, 6)));
		acmvpf.setTrtm(Integer.parseInt(wsaaMachineTimex.substring(0, 6)));
		//ILIFE-7901 end
		acmvpf.setUser_t(lifacmvPojo.getUser());
		acmvpf.setTermid(lifacmvPojo.getTermid());
		acmvpf.setSuprflg(lifacmvPojo.getSuprflag());
		acmvpf.setIntextind(t3695rec.intextind.toString());
		if (lifacmvPojo.getGenlcur() == null || "".equals(lifacmvPojo.getGenlcur())) {
			acmvpf.setCrate(wsaaNominalRate.floatValue());
			acmvpf.setGenlcur(wsaaLedgerCcy);
			if (isEQ(lifacmvPojo.getAcctamt(), ZERO)) {
				setPrecision(acmvpf.getAcctamt(), 10);
				acmvpf.setAcctamt(lifacmvPojo.getOrigamt().multiply(new BigDecimal(wsaaNominalRate.toString())));
				zrdecplcPojo.setAmountIn(acmvpf.getAcctamt());
				zrdecplcPojo.setCurrency(acmvpf.getGenlcur());
				a000CallRounding(lifacmvPojo);
				acmvpf.setAcctamt(zrdecplcPojo.getAmountOut());
			}
		}

		zrdecplcPojo.setAmountIn(acmvpf.getOrigamt());
		zrdecplcPojo.setCurrency(acmvpf.getOrigcurr());
		a000CallRounding(lifacmvPojo);
		acmvpf.setOrigamt(zrdecplcPojo.getAmountOut());
		if(lifacmvPojo.getTranno() == 0){
			if("G".equals(lifacmvPojo.getInd())){
				acmvpf.setRldgpfx(lifacmvPojo.getPrefix());
			}
			if("D".equals(lifacmvPojo.getInd()))
			{
				acmvpf.setRdocpfx(lifacmvPojo.getPrefix());
			}
		}
		if (isEQ(acmvpf.getPostmonth(), SPACES)) {
			acmvpf.setPostmonth(String.valueOf(acmvpf.getBatcactmn()));
		}
		if (isEQ(acmvpf.getPostyear(), SPACES)) {
			acmvpf.setPostyear(String.valueOf(acmvpf.getPostyear()));
		}
		if(null == acmvpf.getRdocpfx()){
			acmvpf.setRdocpfx(SPACES.toString());
		}
		acmvpf.setReconref(0);
		acmvpf.setCreddte(99999999);
		acmvDAO.insertAcmvpfRecord(acmvpf);

		if (isEQ(tr362rec.appflag, "Y")) {
			diaryUpdate5000(acmvpf);
		}
	}
	
	public void glSubstitution1011(LifacmvPojo lifacmvPojo){
		lifacmvPojo.setGlcode(inspectReplaceAll(lifacmvPojo.getGlcode(), "?", lifacmvPojo.getBatccoy()));
		lifacmvPojo.setGlcode(inspectReplaceAll(lifacmvPojo.getGlcode(), "##", lifacmvPojo.getBatcbrn()));
		lifacmvPojo.setGlcode(inspectReplaceAll(lifacmvPojo.getGlcode(), "&&&", lifacmvPojo.getOrigcurr()));
		/* NB. Assume LIFA-SUBSTITUTE-CODE is filled left justified,*/
		/* ie. 'A  ' NOT '  A'.*/
		if (lifacmvPojo.getSubstituteCodes()[0] != null && !"".equals(lifacmvPojo.getSubstituteCodes()[0])) {
			wsaaSubscode = lifacmvPojo.getSubstituteCodes()[0];
			wsaaSubscode01 = wsaaSubscode.substring(0,1);
			wsaaSubscode02 = wsaaSubscode.substring(1,2);
			wsaaSubscode03 = wsaaSubscode.substring(2,3);
			wsaaSubscode04 = wsaaSubscode.length() >= 4 ? wsaaSubscode.substring(3,4) : "";
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "@", wsaaSubscode01));
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "@", wsaaSubscode02));
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "@", wsaaSubscode03));
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "@", wsaaSubscode04));
		}
		if ((lifacmvPojo.getSubstituteCodes()[1] != null && !"".equals(lifacmvPojo.getSubstituteCodes()[1]))) {
			wsaaSubscode = lifacmvPojo.getSubstituteCodes()[1];
			wsaaSubscode01 = wsaaSubscode.substring(0,1);
			wsaaSubscode02 = wsaaSubscode.substring(1,2);
			wsaaSubscode03 = wsaaSubscode.substring(2,3);
			wsaaSubscode04 = wsaaSubscode.length() >= 4 ? wsaaSubscode.substring(3,4) : "";
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "*", wsaaSubscode01));
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "*", wsaaSubscode02));
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "*", wsaaSubscode03));
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "*", wsaaSubscode04));
		}
		if ((lifacmvPojo.getSubstituteCodes()[2] != null && !"".equals(lifacmvPojo.getSubstituteCodes()[2]))) {
			wsaaSubscode = lifacmvPojo.getSubstituteCodes()[2];
			wsaaSubscode01 = wsaaSubscode.substring(0,1);
			wsaaSubscode02 = wsaaSubscode.substring(1,2);
			wsaaSubscode03 = wsaaSubscode.substring(2,3);
			wsaaSubscode04 = wsaaSubscode.length() >= 4 ? wsaaSubscode.substring(3,4) : "";
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "%", wsaaSubscode01));
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "%", wsaaSubscode02));
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "%", wsaaSubscode03));
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "%", wsaaSubscode04));
		}
		if ((lifacmvPojo.getSubstituteCodes()[3] != null && !"".equals(lifacmvPojo.getSubstituteCodes()[3]))) {
			wsaaSubscode = lifacmvPojo.getSubstituteCodes()[3];
			wsaaSubscode01 = wsaaSubscode.substring(0,1);
			wsaaSubscode02 = wsaaSubscode.substring(1,2);
			wsaaSubscode03 = wsaaSubscode.substring(2,3);
			wsaaSubscode04 = wsaaSubscode.length() >= 4 ? wsaaSubscode.substring(3,4) : "";
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "!", wsaaSubscode01));
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "!", wsaaSubscode02));
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "!", wsaaSubscode03));
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "!", wsaaSubscode04));
		}
		if ((lifacmvPojo.getSubstituteCodes()[4] != null && !"".equals(lifacmvPojo.getSubstituteCodes()[4]))) {
			wsaaSubscode = lifacmvPojo.getSubstituteCodes()[4];
			wsaaSubscode01 = wsaaSubscode.substring(0,1);
			wsaaSubscode02 = wsaaSubscode.substring(1,2);
			wsaaSubscode03 = wsaaSubscode.substring(2,3);
			wsaaSubscode04 = wsaaSubscode.length() >= 4 ? wsaaSubscode.substring(3,4) : "";
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "+", wsaaSubscode01));
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "+", wsaaSubscode02));
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "+", wsaaSubscode03));
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "+", wsaaSubscode04));
		}
		/* New substitution code No 6  - Coverage/Rider table*/
		if ((lifacmvPojo.getSubstituteCodes()[5] != null && !"".equals(lifacmvPojo.getSubstituteCodes()[5]))) {
			wsaaSubscode = lifacmvPojo.getSubstituteCodes()[5];
			wsaaSubscode01 = wsaaSubscode.substring(0,1);
			wsaaSubscode02 = wsaaSubscode.substring(1,2);
			wsaaSubscode03 = wsaaSubscode.substring(2,3);
			wsaaSubscode04 = wsaaSubscode.length() >= 4 ? wsaaSubscode.substring(3,4) : "";
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "=", wsaaSubscode01));
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "=", wsaaSubscode02));
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "=", wsaaSubscode03));
			lifacmvPojo.setGlcode(inspectReplaceFirst(lifacmvPojo.getGlcode(), "=", wsaaSubscode04));
		}
	}
	
	public void diaryUpdate5000(Acmvpf acmvpf){
		if (acmvpf.getOrigamt() == BigDecimal.ZERO && acmvpf.getAcctamt()  == BigDecimal.ZERO ) {
			return ;
		}
		if (isEQ(acmvpf.getGlcode(), SPACES)) {
			return ;
		}
		dglxpf = new Dglxpf();
		dglxpf.setBatccoy(acmvpf.getBatccoy());
		dglxpf.setBatcbrn(acmvpf.getBatcbrn());
		dglxpf.setBatcactyr(acmvpf.getBatcactyr());
		dglxpf.setBatcactmn(acmvpf.getBatcactmn());
		dglxpf.setBatctrcde(acmvpf.getBatctrcde());
		dglxpf.setBatcbatch(acmvpf.getBatcbatch());
		dglxpf.setGenlcoy(acmvpf.getGenlcoy());
		dglxpf.setGenlcur(acmvpf.getGenlcur());
		dglxpf.setGenlcde(acmvpf.getGlcode());
		dglxpf.setEffdate(acmvpf.getEffdate());
		dglxpf.setOrigcurr(acmvpf.getOrigcurr());
		dglxpf.setPostyear(acmvpf.getPostyear());
		dglxpf.setPostmonth(acmvpf.getPostmonth());
		dglxpf.setRldgacct(acmvpf.getRldgacct());
		if (isEQ(acmvpf.getGlsign(), "-")) {
			setPrecision(dglxpf.getOrigamt(), 2);
			dglxpf.setOrigamt(new BigDecimal(sub(0, acmvpf.getOrigamt()).toString()));
			setPrecision(dglxpf.getAcctamt(), 2);
			dglxpf.setAcctamt(new BigDecimal(sub(0, acmvpf.getAcctamt()).toString()));
		}
		else {
			dglxpf.setOrigamt(acmvpf.getOrigamt());
			dglxpf.setAcctamt(acmvpf.getAcctamt());
		}
		dglxpf.setCrtdatime(new Timestamp(System.currentTimeMillis()).toString());
		dglxpfDAO.insertDglxRecord(dglxpf);
	}
	
	public void a000CallRounding(LifacmvPojo lifacmvnPojo) {
		zrdecplcPojo.setFunction(SPACES.toString());
		zrdecplcPojo.setStatuz(Varcom.oK.toString());
		zrdecplcPojo.setCompany(lifacmvnPojo.getBatccoy());
		zrdecplcPojo.setBatctrcde(lifacmvnPojo.getBatctrcde());
		zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
		if (isNE(zrdecplcPojo.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zrdecplcPojo.getStatuz());
			syserrrec.params.set(zrdecplcPojo.toString());
			syserr570(lifacmvnPojo,"s");
		}
	}
	
	public void getIntextind1200(LifacmvPojo lifacmvPojo){
		for (iz = 1;  !(isGT(iz, 50)|| isEQ(wsaaT3695Sacstyp[iz], HIVALUE)|| isEQ(wsaaT3695Sacstyp[iz], lifacmvPojo.getSacstyp())); iz++){
					/*CONTINUE_STMT*/
			}
		if (isEQ(wsaaT3695Intextind[iz], lifacmvPojo.getSacstyp())) {
			t3695rec.intextind.set(wsaaT3695Intextind[iz]);
			return;
		}
		itempfList = itemDao.getAllItemitem("IT",lifacmvPojo.getBatccoy(), "T3695",lifacmvPojo.getSacstyp());
		if (itempfList == null && itempfList.size() <= 0 ) {
			syserrrec.params.set("T3695" + lifacmvPojo.getSacstyp());
			syserr570(lifacmvPojo,"1");
		}
		t3695rec.t3695Rec.set((StringUtil.rawToString(itempfList.get(0).getGenarea())));
		if (iz < 52) {
			wsaaT3695Sacstyp[iz].set(lifacmvPojo.getSacstyp());
			wsaaT3695Intextind[iz].set(t3695rec.intextind);
		}
	}

	public void getRate1100(LifacmvPojo lifacmvPojo, String itemkey){
		Itempf item3629 = readCurrencyRecord1110(itemkey,lifacmvPojo.getBatccoy());
		if(item3629 != null){
			t3629rec.t3629Rec.set(StringUtil.rawToString(item3629.getGenarea()));
			wsaaLastOrigCcy = lifacmvPojo.getOrigcurr();
			wsaaLedgerCcy = t3629rec.ledgcurr.toString();
			for(int i = 1; (i <=7 || isEQ(wsaaNominalRate,ZERO)); i++ ){
				if (isGTE(lifacmvPojo.getEffdate(),t3629rec.frmdate[i])
						&& isLTE(lifacmvPojo.getEffdate(),t3629rec.todate[i])) {
							wsaaNominalRate = t3629rec.scrate[i].getbigdata();
							break;
						}
			}
			if (wsaaNominalRate.compareTo(BigDecimal.ZERO) == 0) {
				if (isNE(t3629rec.contitem,SPACES)) {
					item3629 = readCurrencyRecord1110(t3629rec.contitem.toString(),lifacmvPojo.getBatccoy());
					getRate1100(lifacmvPojo,t3629rec.contitem.toString());
				}
				else {
					syserrrec.statuz.set("G418");
					syserr570(lifacmvPojo,"s");
				}
			}
		}
	}
	
	public void writeAgpyagt1300(Acmvpf acmvpf){
		agpyagt = new Agpypf();
		agpyagt.setBatccoy(acmvpf.getBatccoy());
		agpyagt.setBatcbrn(acmvpf.getBatcbrn());
		agpyagt.setBatcactyr(acmvpf.getBatcactyr());
		agpyagt.setBatcactmn(acmvpf.getBatcactmn());
		agpyagt.setBatctrcde(acmvpf.getBatctrcde());
		agpyagt.setBatcbatch(acmvpf.getBatcbatch());
		agpyagt.setRdocnum(acmvpf.getRdocnum());
		agpyagt.setTranno(acmvpf.getTranno());
		agpyagt.setJrnseq(acmvpf.getJrnseq());
		agpyagt.setRldgcoy(acmvpf.getRldgcoy());
		agpyagt.setRldgacct(acmvpf.getRldgacct());
		agpyagt.setOrigcurr(acmvpf.getOrigcurr());
		agpyagt.setSacscode(acmvpf.getSacscode());
		agpyagt.setSacstyp(acmvpf.getSacstyp());
		agpyagt.setOrigamt(acmvpf.getOrigamt());
		agpyagt.setTranref(acmvpf.getTranref());
		agpyagt.setTrandesc(acmvpf.getTrandesc());
		agpyagt.setCrate(acmvpf.getCrate());
		agpyagt.setAcctamt(acmvpf.getAcctamt());
		agpyagt.setGenlcoy(acmvpf.getGenlcoy());
		agpyagt.setGenlcur(acmvpf.getGenlcur());
		agpyagt.setGlcode(acmvpf.getGlcode());
		agpyagt.setGlsign(acmvpf.getGlsign());
		agpyagt.setPostyear(acmvpf.getPostyear());
		agpyagt.setPostmonth(acmvpf.getPostmonth());
		agpyagt.setEffdate(acmvpf.getEffdate());
		agpyagt.setRcamt(acmvpf.getRcamt());
		agpyagt.setFrcdate(acmvpf.getFrcdate());
		agpyagt.setTrdt(acmvpf.getTrdt());
		agpyagt.setTrtm(acmvpf.getTrtm());
		agpyagt.setUserT(acmvpf.getUser_t());
		agpyagt.setTermid(acmvpf.getTermid());
		agpyagt.setSuprflg(acmvpf.getSuprflg());
		agpypfDAO.insertAgpypfRecord(agpyagt);
	}
	public Itempf readCurrencyRecord1110(String itemitem,String coy){
		itempfList = itemDao.getAllItemitem("IT",coy, "T3629",itemitem);
		if (itempfList != null && itempfList.size()> 0 ) {
			return itempfList.get(0);
		}
		return null;
	}
	
	public Chdrpf readContractDetails800(LifacmvPojo lifacmvPojo){
		trcdecpy.trancodeCpy.set(lifacmvPojo.getBatctrcde());
		String chdrnum = lifacmvPojo.getRdocnum();
		if (trcdecpy.cheqCodes.isTrue()|| trcdecpy.cashCodes.isTrue()|| trcdecpy.jrnlCodes.isTrue()) {
			chdrnum = subString(lifacmvPojo.getRldgacct(), 1, 8).toString();
		}
		return chdrpfDAO.getChdrlifRecord(lifacmvPojo.getBatccoy(), chdrnum);
	}
	
protected void callBatcup3000(LifacmvPojo lifacmvPojo){
		
		batcupPojo  = new BatcupPojo();
		batcupPojo.setBatcpfx("BA");
		batcupPojo.setBatccoy(lifacmvPojo.getBatccoy());
		batcupPojo.setBatcbrn(lifacmvPojo.getBatcbrn());
		batcupPojo.setBatcactyr(lifacmvPojo.getBatcactyr());
		batcupPojo.setBatcactmn(lifacmvPojo.getBatcactmn());
		batcupPojo.setBatctrcde(lifacmvPojo.getBatctrcde());
		batcupPojo.setBatcbatch(lifacmvPojo.getBatcbatch());
		batcupPojo.setTrancnt(0);
		batcupPojo.setEtreqcnt(0);
		batcupPojo.setSub(lifacmvPojo.getContot());
		batcupPojo.setAscnt(0);
		batcupPojo.setBcnt(1);
		/* MOVE LIFR-ACCTAMT           TO BCUP-BVAL.                    */
		batcupPojo.setBval(acmvpf.getAcctamt().intValue());
		batcupPojo.setFunction("KEEPS");
		batcupUtils.callBatcup(batcupPojo);
		
		if (isNE(batcupPojo.getStatuz(), varcom.oK)) {
			lifacmvPojo.setStatuz(batcupPojo.getStatuz());
			syserrrec.statuz.set(batcupPojo.getStatuz());
			syserr570(lifacmvPojo,"s");
		}
	}
	
	public void syserr570(LifacmvPojo lifacmvPojo,String errtype){
		/*PARA*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set(errtype);
		syserr.mainline(new Object[] { syserrrec.syserrRec });
		lifacmvPojo.setStatuz(varcom.bomb.toString());
		LOGGER.error("COBOLConvCodeModel.callProgram exception", new COBOLExitProgramException());
		throw new RuntimeException(new COBOLExitProgramException());
	}
	protected ApplicationContext getApplicationContext() {
		return IntegralApplicationContext.getApplicationContext();
	}
}
