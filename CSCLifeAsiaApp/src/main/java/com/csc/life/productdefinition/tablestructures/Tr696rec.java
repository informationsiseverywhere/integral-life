package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:24
 * Description:
 * Copybook name: TR696REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr696rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr696Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData frsuminss = new FixedLengthStringData(170).isAPartOf(tr696Rec, 0);
  	public ZonedDecimalData[] frsumins = ZDArrayPartOfStructure(10, 17, 2, frsuminss, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(170).isAPartOf(frsuminss, 0, FILLER_REDEFINE);
  	public ZonedDecimalData frsumins01 = new ZonedDecimalData(17, 2).isAPartOf(filler, 0);
  	public ZonedDecimalData frsumins02 = new ZonedDecimalData(17, 2).isAPartOf(filler, 17);
  	public ZonedDecimalData frsumins03 = new ZonedDecimalData(17, 2).isAPartOf(filler, 34);
  	public ZonedDecimalData frsumins04 = new ZonedDecimalData(17, 2).isAPartOf(filler, 51);
  	public ZonedDecimalData frsumins05 = new ZonedDecimalData(17, 2).isAPartOf(filler, 68);
  	public ZonedDecimalData frsumins06 = new ZonedDecimalData(17, 2).isAPartOf(filler, 85);
  	public ZonedDecimalData frsumins07 = new ZonedDecimalData(17, 2).isAPartOf(filler, 102);
  	public ZonedDecimalData frsumins08 = new ZonedDecimalData(17, 2).isAPartOf(filler, 119);
  	public ZonedDecimalData frsumins09 = new ZonedDecimalData(17, 2).isAPartOf(filler, 136);
  	public ZonedDecimalData frsumins10 = new ZonedDecimalData(17, 2).isAPartOf(filler, 153);
  	public FixedLengthStringData sabands = new FixedLengthStringData(10).isAPartOf(tr696Rec, 170);
  	public FixedLengthStringData[] saband = FLSArrayPartOfStructure(10, 1, sabands, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(10).isAPartOf(sabands, 0, FILLER_REDEFINE);
  	public FixedLengthStringData saband01 = new FixedLengthStringData(1).isAPartOf(filler1, 0);
  	public FixedLengthStringData saband02 = new FixedLengthStringData(1).isAPartOf(filler1, 1);
  	public FixedLengthStringData saband03 = new FixedLengthStringData(1).isAPartOf(filler1, 2);
  	public FixedLengthStringData saband04 = new FixedLengthStringData(1).isAPartOf(filler1, 3);
  	public FixedLengthStringData saband05 = new FixedLengthStringData(1).isAPartOf(filler1, 4);
  	public FixedLengthStringData saband06 = new FixedLengthStringData(1).isAPartOf(filler1, 5);
  	public FixedLengthStringData saband07 = new FixedLengthStringData(1).isAPartOf(filler1, 6);
  	public FixedLengthStringData saband08 = new FixedLengthStringData(1).isAPartOf(filler1, 7);
  	public FixedLengthStringData saband09 = new FixedLengthStringData(1).isAPartOf(filler1, 8);
  	public FixedLengthStringData saband10 = new FixedLengthStringData(1).isAPartOf(filler1, 9);
  	public FixedLengthStringData simcrtable = new FixedLengthStringData(2).isAPartOf(tr696Rec, 180);
  	public FixedLengthStringData tosuminss = new FixedLengthStringData(170).isAPartOf(tr696Rec, 182);
  	public ZonedDecimalData[] tosumins = ZDArrayPartOfStructure(10, 17, 2, tosuminss, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(170).isAPartOf(tosuminss, 0, FILLER_REDEFINE);
  	public ZonedDecimalData tosumins01 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 0);
  	public ZonedDecimalData tosumins02 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 17);
  	public ZonedDecimalData tosumins03 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 34);
  	public ZonedDecimalData tosumins04 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 51);
  	public ZonedDecimalData tosumins05 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 68);
  	public ZonedDecimalData tosumins06 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 85);
  	public ZonedDecimalData tosumins07 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 102);
  	public ZonedDecimalData tosumins08 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 119);
  	public ZonedDecimalData tosumins09 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 136);
  	public ZonedDecimalData tosumins10 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 153);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(148).isAPartOf(tr696Rec, 352, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr696Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr696Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}