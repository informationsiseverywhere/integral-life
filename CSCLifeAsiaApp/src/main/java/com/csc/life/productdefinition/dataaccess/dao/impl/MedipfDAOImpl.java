package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.MedipfDAO;
import com.csc.life.productdefinition.dataaccess.model.Medipf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class MedipfDAOImpl extends BaseDAOImpl<Medipf> implements MedipfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(MedipfDAOImpl.class);	
	
	public boolean updateMedipf(List<Medipf> mediList){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE MEDIPF  ");
		sb.append("SET ");
		sb.append("CHDRCOY=?, ");
		sb.append("CHDRNUM=?, ");
		sb.append("LIFE=?, ");
		sb.append("JLIFE=?, ");
		sb.append("SEQNO=?, ");
		sb.append("ZMEDTYP=?, ");
		sb.append("EXMCODE=?, ");
		sb.append("ZMEDFEE=?, ");
		sb.append("PAIDBY=?, ");
		sb.append("INVREF=?, ");
		sb.append("EFFDATE=?, ");
		sb.append("ACTIND=?, ");
		sb.append("PAYDTE=?, ");
		sb.append("TRANNO=?, ");
		sb.append("USRPRF=?, ");
		sb.append("JOBNM=?, ");
		sb.append("DATIME=?  ");
		sb.append("WHERE UNIQUE_NUMBER=?");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		boolean isUpdated = false;
		try{
			ps = getPrepareStatement(sb.toString());	
			for (Medipf medi : mediList) {
				ps.setString(1, medi.getChdrcoy());
				ps.setString(2, medi.getChdrnum());
				ps.setString(3, medi.getLife());
				ps.setString(4, medi.getJlife());
				ps.setInt(5, medi.getSeqno());
				ps.setString(6, medi.getZmedtyp());
				ps.setString(7, medi.getExmcode());
				ps.setBigDecimal(8, medi.getZmedfee());
				ps.setString(9, medi.getPaidby());
				ps.setString(10, medi.getInvref());
				ps.setInt(11, medi.getEffdate());
				ps.setString(12, medi.getActind());
				ps.setInt(13, medi.getPaydte());
				ps.setInt(14, medi.getTranno());
			    ps.setString(15, getUsrprf());			    
			    ps.setString(16, getJobnm());		
			    ps.setTimestamp(17, new Timestamp(System.currentTimeMillis()));					    
			    ps.setLong(18, medi.getUniqueNumber());
			    
			    ps.addBatch();	
			    			    
			}		
			ps.executeBatch();
			isUpdated = true;
		}catch (SQLException e) {
			LOGGER.error("updateMedipf()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);			
		}		
		return isUpdated;
	}		

	public List<Medipf> searchMediRecord(String chdrcoy, String chdrnum) {

		StringBuilder sqlSelect1 = new StringBuilder();
		sqlSelect1.append("SELECT * FROM MEDI ");
		sqlSelect1.append(" WHERE CHDRNUM = ? AND CHDRCOY = ? ");
		sqlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, SEQNO ASC, UNIQUE_NUMBER ASC");

		PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
		ResultSet sqlrs = null;
		List<Medipf> resultList = new LinkedList<>();

		try {
			psSelect.setString(1, chdrnum);
			psSelect.setString(2, chdrcoy);

			sqlrs = executeQuery(psSelect);

			while (sqlrs.next()) {
				Medipf m = new Medipf();
				m.setChdrcoy(sqlrs.getString("Chdrcoy"));
				m.setChdrnum(sqlrs.getString("Chdrnum"));
				m.setLife(sqlrs.getString("Life"));
				m.setJlife(sqlrs.getString("Jlife"));
				m.setSeqno(sqlrs.getInt("Seqno"));
				m.setZmedtyp(sqlrs.getString("Zmedtyp"));
				m.setExmcode(sqlrs.getString("Exmcode"));
				m.setZmedfee(sqlrs.getBigDecimal("Zmedfee"));
				m.setPaidby(sqlrs.getString("Paidby"));
				m.setInvref(sqlrs.getString("Invref"));
				m.setEffdate(sqlrs.getInt("Effdate"));
				m.setActind(sqlrs.getString("Actind"));
				m.setPaydte(sqlrs.getInt("Paydte"));
				m.setTranno(sqlrs.getInt("Tranno"));
				m.setUniqueNumber(sqlrs.getLong("Unique_Number"));
				resultList.add(m);
			}

		} catch (SQLException e) {
			LOGGER.error("searchMediRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psSelect, sqlrs);
		}
		return resultList;

	}
}
