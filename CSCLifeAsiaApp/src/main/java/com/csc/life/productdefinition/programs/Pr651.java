/*
 * File: Pr651.java
 * Date: 30 August 2009 1:51:14
 * Author: Quipoz Limited
 * 
 * Class transformed from PR651.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.tablestructures.T3568rec;
import com.csc.smart.dataaccess.FlddTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Nxtprogrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainf;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
*  F I E L D   B A S E D   W I N D O W   S W I T C H I N G
*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
*
* This program positionally occupies position 1 in the
* set of programs to be executed for table windowing on
* the field dictionary for fields which require various
* windows given a different value in another field on the
* screen.
*
* For example, within cash receipting a field
* called ENTITY is able to window on General Ledger Codes,
* Suspense In Cash Receipts, Agent Numbers, or Contract
* Owners; the defining field is the subaccount code field.
*
* On table T3568, which is keyed by program number (passed
* to this program through additional fields as a hidden
* field), there are entries for each subaccount code which
* requires windowing. These entries consist of the next
* four programs to load and execute which is the exact
* processing that this program sets out to accomplish.
*
* After loading the next four programs to execute, this
* program sets up NEXTPROG and then executes it. This
* program will not re-execute going back up the ladder
* as part of the processing is to replace NEXT1PROG in
* WSSP which is where this program initially lives.
*
* If the defining field contains a value which has not been
* defined on the table or if the field name has not been
* defined, then the executiion of this program will be
* transparent to the user as no windowing will appear to
* occur.
/
* If you wish to use this program for an application like
* the one detailed above, you must ensure that certain
* steps are followed:
*
* 1) Determine the name of the field upon which you will
*    be windowing and the name of the defining field which
*    will contain the value upon which the windowing is
*    depending.
*
*    Check the field dictionary to determine which screens
*    and printer files use the field you are windowing on.
*
*    IF THERE ARE SCREENS IN THIS LIST WHICH DO NOT REQUIRE
*    THIS SORT OF WINDOWING, THEN YOU ARE BEST OFF CREATING
*    A NEW FIELD FOR YOUR DDS - OTHERWISE, YOU ARE RESPONSIBLE
*    FOR UNPREDICTABLE RESULTS WHEN THE OTHER SCREENS GET
*    REGENERATED IN THE FUTURE. YOUR WORKMATES WILL NOT GET
*    IRATE WITH YOU IF YOU FOLLOW THIS STEP CAREFULLY!
*
* 2) Set up the field dictionary for the field upon which
*    you wish to window as follows:
*
*    The first slot in the table windowing suite must be
*    occupied by this program PR561. If you enter values
*    in the other three slots, this program will over-
*    write them so DON'T DO IT!
*
*    The window type and subtype must be set to 'W' for
*    General Window and 'G' for Dynamic Allocation.
*
*    The first additional field must contain the field
*    which is hidden on your screen and contains the name
*    of the program. This field is expected to be 5 bytes
*    long.
*
*    If you have multiple fields on the same screen which
*    invoke this type of windowing, use the same field for
*    passing the program name.
*
*    The second additional field must contain the defining
*    field, ie, the one which contains the varying value
*    upon which the various windows will be invoked. The
*    field itself may be a normal field name as contained on
*    the field dictionary. The value passed within this field
*    must be alphanumeric and up to 8 bytes. Numerics, signed
*    or unsigned, are not supported in this version.
*
*    If you have a field which you wish to pass to one of the
*    windows you are invoking, then you may define a third
*    additional field which contains that data. For example,
*    general ledger account windowing requires currency to be
*    passed.
/
* 3) Table T3568 must contain an item keyed on your program
*    number. On the extra data screen for this item, each
*    field must have all valid values specified. The field
*    name must be specified as well, for if there are multiple
*    fields from the same calling program which require this
*    type of windowing, then we must be able to qualify them.
*
*    The field name only needs to be defined once for a group
*    of values - this program is smart enough to assume that
*    if the subsequent field name fields are spaces, that we
*    wish to use the last field name specified. Also, this
*    program knows all about continuation items and will check
*    all subsequent items looking for valid values for the
*    field until it finds an item with no continuation item
*    specified. Only then, if the value has not been found,
*    will this program spit the dummy.
*
* 4) Define in WSSPWINDOW the additional fields.
*
*003 NEW SCREEN GENERATORS MEANS WSSPWINDOW NOT NEEDS CHANGING !!!
*003 FROM SMART/400 RELEASE 9305.                              !!!
*
*    If you have multiple invocations of this type of win-
*    dowing from the same screen and have defined your fields
*    so that they all point at the same field containing the
*    program name (do this to avoid having to move it to
*    multiple hidden fields) WSSPWINDOW will still cope: the
*    IO module ALWAYS will qualify the variable name by
*    referring to WSSP-PGMNAM OF xxxx-FIELDS.
*
*    Note that a few assumptions are made here. The first
*    5 bytes of additional fields within WSSPWINDOW will
*    contain the key into the table (program name) and the
*    next eight will contain the alphanumeric defining
*    field. If your defining field is not eight bytes long
*    don't worry too much: COBOL automatically pads with
*    spaces.
*
*    If you are passing additional data, then you must pad out
*    the eight bytes mentioned above and set the area starting
*    at byte fourteen of ADDITIONAL-FIELDS to be the addition-
*    al data area. Please note that any windows which require
*    additional fields and are used with this type of windowing
*    must havce their own linkage designed to fit this scheme.
*    See GL Account Windowing P2577.
*
* 5) Define the hidden field which contains the program
*    name in the screen DDS. Condition it for non-display
*    as follows:
*
*    A            PGMNAM         5   O 13 68DSPATR(ND)
*
* 6) Your application program must move your program name
*    (WSAA-PROG) to the non-display field on your screen
*    which is passed through additional fields. This should
*    be done in the 1000 section.
/
***********************************************************************
* </pre>
*/
public class Pr651 extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR561");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaAdditionalFields = new FixedLengthStringData(194);
	private FixedLengthStringData wsaaInvokpgm = new FixedLengthStringData(5).isAPartOf(wsaaAdditionalFields, 0);
	private FixedLengthStringData wsaaDefinitm = new FixedLengthStringData(2).isAPartOf(wsaaAdditionalFields, 5);

	private FixedLengthStringData windFields = new FixedLengthStringData(23);
	private FixedLengthStringData windPgmnam = new FixedLengthStringData(5).isAPartOf(windFields, 0);
	private FixedLengthStringData windPaidby = new FixedLengthStringData(1).isAPartOf(windFields, 5);
	private FixedLengthStringData windOrigccy = new FixedLengthStringData(6).isAPartOf(windFields, 6);

	private FixedLengthStringData filler2 = new FixedLengthStringData(23).isAPartOf(windFields, 0, FILLER_REDEFINE);
	private FixedLengthStringData windInvokpgm = new FixedLengthStringData(5).isAPartOf(filler2, 0);
	private ZonedDecimalData wsaaKeyLen = new ZonedDecimalData(2, 0).setUnsigned();
	private int wsaaMax = 7;
	private FixedLengthStringData wsaaSaveFdid = new FixedLengthStringData(10);

	private FixedLengthStringData wsaaValueFoundFlag = new FixedLengthStringData(1).init("N");
	private Validator wsaaValueFound = new Validator(wsaaValueFoundFlag, "Y");

	private FixedLengthStringData wsaaNoResultFlag = new FixedLengthStringData(1).init("N");
	private Validator wsaaNoResult = new Validator(wsaaNoResultFlag, "Y");
	private String flddrec = "FLDDREC";
		/* TABLES */
	private String t3568 = "T3568";
		/*Field dictionary - access by field*/
	private FlddTableDAM flddIO = new FlddTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Nxtprogrec nxtprogrec = new Nxtprogrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T3568rec t3568rec = new T3568rec();
	private Itemkey wsaaItemkey = new Itemkey();
	private Wsspwindow wsspwindow = new Wsspwindow();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nxtpGoingUp, 
		nxtpEnd, 
		nxtpExit, 
		readT35681010, 
		find1050, 
		exit1900, 
		exit3900, 
		exit4900
	}

	public Pr651() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void nxtprog()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nxtpMainline();
					nxtpGoingDown();
				}
				case nxtpGoingUp: {
					nxtpGoingUp();
				}
				case nxtpEnd: {
					nxtpEnd();
				}
				case nxtpExit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nxtpMainline()
	{
		nxtprogrec.statuz.set("****");
		if (isEQ(wsspcomn.submenu,"P0075")) {
			wsspcomn.programPtr.add(1);
			wsspcomn.nextprog.set(wsaaProg);
			goTo(GotoLabel.nxtpExit);
		}
		if (isEQ(wsspcomn.nextprog,scrnparams.scrname)) {
			wsspcomn.lastprog.set(wsaaProg);
		}
		else {
			wsspcomn.lastprog.set(wsspcomn.nextprog);
		}
		wsspcomn.nextprog.set(SPACES);
		if (isEQ(nxtprogrec.function,"U")) {
			goTo(GotoLabel.nxtpGoingUp);
		}
	}

protected void nxtpGoingDown()
	{
		if (isEQ(wsspcomn.lastprog,wsspcomn.next3prog)) {
			wsspcomn.nextprog.set(wsspcomn.next4prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog,wsspcomn.next2prog)) {
			wsspcomn.nextprog.set(wsspcomn.next3prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog,wsspcomn.next1prog)) {
			wsspcomn.nextprog.set(wsspcomn.next2prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog,wsspcomn.submenu)) {
			wsspcomn.nextprog.set(wsspcomn.next1prog);
			goTo(GotoLabel.nxtpEnd);
		}
	}

protected void nxtpGoingUp()
	{
		if (isEQ(wsspcomn.lastprog,wsspcomn.next2prog)) {
			wsspcomn.nextprog.set(wsspcomn.next1prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog,wsspcomn.next3prog)) {
			wsspcomn.nextprog.set(wsspcomn.next2prog);
			goTo(GotoLabel.nxtpEnd);
		}
		if (isEQ(wsspcomn.lastprog,wsspcomn.next4prog)) {
			wsspcomn.nextprog.set(wsspcomn.next3prog);
			goTo(GotoLabel.nxtpEnd);
		}
	}

protected void nxtpEnd()
	{
		if (isEQ(wsspcomn.nextprog,SPACES)
		&& isNE(wsspcomn.flag,"W")) {
			wsspcomn.nextprog.set(wsspcomn.submenu);
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1001();
				}
				case readT35681010: {
					readT35681010();
					readField1020();
				}
				case find1050: {
					find1050();
				}
				case exit1900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1001()
	{
		windFields.set(wsspwindow.additionalFields);
		wsaaAdditionalFields.set(wsspwindow.additionalFields);
		scrnparams.scrname.set("SR651");
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(wsspcomn.company);
		wsaaItemkey.itemItemtabl.set(t3568);
		wsaaItemkey.itemItemitem.set(windInvokpgm);
		itemIO.setDataKey(wsaaItemkey.itemKey);
	}

protected void readT35681010()
	{
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3568rec.t3568Rec.set(itemIO.getGenarea());
	}

protected void readField1020()
	{
		flddIO.setParams(SPACES);
		flddIO.setFunction(varcom.readr);
		flddIO.setFormat(flddrec);
		flddIO.setFieldId(wsspwindow.windowField);
		SmartFileCode.execute(appVars, flddIO);
		if (isNE(flddIO.getStatuz(),varcom.oK)) {
			wsaaKeyLen.set(8);
			goTo(GotoLabel.find1050);
		}
		flddIO.setFieldId(flddIO.getAddField02());
		SmartFileCode.execute(appVars, flddIO);
		if (isNE(flddIO.getStatuz(),varcom.oK)) {
			wsaaKeyLen.set(8);
			goTo(GotoLabel.find1050);
		}
		wsaaKeyLen.set(flddIO.getFieldLength());
	}

protected void find1050()
	{
		for (wsaaSub.set(1); !(isGT(wsaaSub,wsaaMax)
		|| wsaaValueFound.isTrue()); wsaaSub.add(1)){
			a010FindValue();
		}
		if (wsaaValueFound.isTrue()) {
			wsaaValueFoundFlag.set("N");
			wsaaSub.subtract(1);
			wsspwindow.additionalFields.set(windFields);
			goTo(GotoLabel.exit1900);
		}
		else {
			/*NEXT_SENTENCE*/
		}
		if (isEQ(t3568rec.contitem,SPACES)) {
			wsaaNoResultFlag.set("Y");
		}
		else {
			wsaaItemkey.itemItemitem.set(t3568rec.contitem);
			itemIO.setDataKey(wsaaItemkey.itemKey);
			goTo(GotoLabel.readT35681010);
		}
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			para3001();
		}
		catch (GOTOException e){
		}
	}

protected void para3001()
	{
		if (wsaaNoResult.isTrue()) {
			goTo(GotoLabel.exit3900);
		}
		else {
			/*NEXT_SENTENCE*/
		}
		wsspcomn.next1prog.set(t3568rec.nxt1prog[wsaaSub.toInt()]);
		wsspwindow.saveNext1prog.set(t3568rec.nxt1prog[wsaaSub.toInt()]);
		wsspcomn.next2prog.set(t3568rec.nxt2prog[wsaaSub.toInt()]);
		wsspwindow.saveNext2prog.set(t3568rec.nxt2prog[wsaaSub.toInt()]);
		wsspcomn.next3prog.set(t3568rec.nxt3prog[wsaaSub.toInt()]);
		wsspwindow.saveNext3prog.set(t3568rec.nxt3prog[wsaaSub.toInt()]);
		wsspcomn.next4prog.set(t3568rec.nxt4prog[wsaaSub.toInt()]);
		wsspwindow.saveNext4prog.set(t3568rec.nxt4prog[wsaaSub.toInt()]);
		wsspwindow.windowType.set(t3568rec.wnpt[wsaaSub.toInt()]);
		wsspwindow.windowSubType.set(t3568rec.windowSubType[wsaaSub.toInt()]);
		wsspwindow.confirmation.set(t3568rec.windowConfirmation[wsaaSub.toInt()]);
	}

protected void whereNext4000()
	{
		try {
			nextProgram4100();
		}
		catch (GOTOException e){
		}
	}

protected void nextProgram4100()
	{
		if (wsaaNoResult.isTrue()) {
			wsaaNoResultFlag.set("N");
			nxtprogrec.function.set("U");
			nxtprog();
			goTo(GotoLabel.exit4900);
		}
		else {
			/*NEXT_SENTENCE*/
		}
		wsspcomn.nextprog.set(wsspcomn.next1prog);
	}

protected void a010FindValue()
	{
		/*A011-PARA*/
		if (isEQ(t3568rec.fieldId[wsaaSub.toInt()],SPACES)) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaSaveFdid.set(t3568rec.fieldId[wsaaSub.toInt()]);
		}
		if (isEQ(wsaaSaveFdid,wsspwindow.windowField)) {
			if (isEQ(subString(t3568rec.value[wsaaSub.toInt()], 1, wsaaKeyLen),subString(wsaaDefinitm, 1, wsaaKeyLen))) {
				wsaaValueFoundFlag.set("Y");
			}
			else {
				/*NEXT_SENTENCE*/
			}
		}
		else {
			/*NEXT_SENTENCE*/
		}
		/*A019-EXIT*/
	}
}
