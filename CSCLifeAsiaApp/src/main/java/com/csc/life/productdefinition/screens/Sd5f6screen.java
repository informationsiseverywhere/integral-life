package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 18/04/18 05:43
 * @author asirvya
 */
public class  Sd5f6screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 17, 22, 4, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 1, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		 Sd5f6ScreenVars sv = ( Sd5f6ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sd5f6screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sd5f6ScreenVars screenVars = ( Sd5f6ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.fundPoolPri01.setClassString("");
		screenVars.fundPoolPri02.setClassString("");
		screenVars.fundPoolPri03.setClassString("");
		screenVars.fundPoolPri04.setClassString("");
		screenVars.fundPoolPri05.setClassString("");
		screenVars.fundPoolPri06.setClassString("");
		screenVars.fundPoolPri07.setClassString("");
		screenVars.fundPoolPri08.setClassString("");
		screenVars.fundPoolPri09.setClassString("");
		screenVars.fundPoolPri10.setClassString("");
		screenVars.fundPoolPri11.setClassString("");
		screenVars.fundPoolPri12.setClassString("");
		screenVars.fhbcl.setClassString("");
		screenVars.fhscc.setClassString("");
		screenVars.fhfp.setClassString("");
		screenVars.dhbcl.setClassString("");
		screenVars.dhscc.setClassString("");
		
	}

/**
 * Clear all the variables in  Sd5f6screen
 */
	public static void clear(VarModel pv) {
		Sd5f6ScreenVars screenVars = ( Sd5f6ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.fundPoolPri01.setClassString("");
		screenVars.fundPoolPri02.setClassString("");
		screenVars.fundPoolPri03.setClassString("");
		screenVars.fundPoolPri04.setClassString("");
		screenVars.fundPoolPri05.setClassString("");
		screenVars.fundPoolPri06.setClassString("");
		screenVars.fundPoolPri07.setClassString("");
		screenVars.fundPoolPri08.setClassString("");
		screenVars.fundPoolPri09.setClassString("");
		screenVars.fundPoolPri10.setClassString("");
		screenVars.fundPoolPri11.setClassString("");
		screenVars.fundPoolPri12.setClassString("");
		screenVars.fhbcl.setClassString("");
		screenVars.fhscc.setClassString("");
		screenVars.fhfp.setClassString("");
		screenVars.dhbcl.setClassString("");
		screenVars.dhscc.setClassString("");	
	}
}
