package com.csc.life.productdefinition.dataaccess.model;

import java.math.BigDecimal;

public class Utrspf {
	private long uniqueNumber;
    private String chdrcoy;
    private String chdrnum;
    private String life;
    private String coverage;
    private String rider;
    private String unitVirtualFund;
    private int planSuffix;
    private String unitType;
    private BigDecimal currentUnitBal = BigDecimal.ZERO;
    private BigDecimal currentDunitBal = BigDecimal.ZERO;
    private boolean updateFlag;
    private String fundpool;
    
    public boolean isUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(boolean updateFlag) {
		this.updateFlag = updateFlag;
	}

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrcoy() {
        return chdrcoy;
    }

    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }

    public String getChdrnum() {
        return chdrnum;
    }

    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }

    public String getLife() {
        return life;
    }

    public void setLife(String life) {
        this.life = life;
    }

    public String getCoverage() {
        return coverage;
    }

    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }

    public String getRider() {
        return rider;
    }

    public void setRider(String rider) {
        this.rider = rider;
    }

    public String getUnitVirtualFund() {
        return unitVirtualFund;
    }

    public void setUnitVirtualFund(String unitVirtualFund) {
        this.unitVirtualFund = unitVirtualFund;
    }

    public int getPlanSuffix() {
        return planSuffix;
    }

    public void setPlanSuffix(int planSuffix) {
        this.planSuffix = planSuffix;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public BigDecimal getCurrentUnitBal() {
        return currentUnitBal;
    }

    public void setCurrentUnitBal(BigDecimal currentUnitBal) {
        this.currentUnitBal = currentUnitBal;
    }

    public BigDecimal getCurrentDunitBal() {
        return currentDunitBal;
    }

    public void setCurrentDunitBal(BigDecimal currentDunitBal) {
        this.currentDunitBal = currentDunitBal;
    }
    public String getFundPool() {
        return fundpool;
    }

    public void setFundPool(String fundpool) {
        this.fundpool = fundpool;
    }

	@Override
	public String toString() {
		return "Utrspf [uniqueNumber=" + uniqueNumber + ", chdrcoy=" + chdrcoy + ", chdrnum=" + chdrnum + ", life="
				+ life + ", coverage=" + coverage + ", rider=" + rider + ", unitVirtualFund=" + unitVirtualFund
				+ ", planSuffix=" + planSuffix + ", unitType=" + unitType + ", currentUnitBal=" + currentUnitBal
				+ ", currentDunitBal=" + currentDunitBal + ", updateFlag=" + updateFlag + "]";
	}
}