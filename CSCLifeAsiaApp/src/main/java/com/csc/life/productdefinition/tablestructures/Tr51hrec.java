package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:19:51
 * Description:
 * Copybook name: TR51HREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr51hrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr51hRec = new FixedLengthStringData(500);
  	public FixedLengthStringData crtables = new FixedLengthStringData(80).isAPartOf(tr51hRec, 0);
  	public FixedLengthStringData[] crtable = FLSArrayPartOfStructure(20, 4, crtables, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(80).isAPartOf(crtables, 0, FILLER_REDEFINE);
  	public FixedLengthStringData crtable01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData crtable02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData crtable03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData crtable04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData crtable05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData crtable06 = new FixedLengthStringData(4).isAPartOf(filler, 20);
  	public FixedLengthStringData crtable07 = new FixedLengthStringData(4).isAPartOf(filler, 24);
  	public FixedLengthStringData crtable08 = new FixedLengthStringData(4).isAPartOf(filler, 28);
  	public FixedLengthStringData crtable09 = new FixedLengthStringData(4).isAPartOf(filler, 32);
  	public FixedLengthStringData crtable10 = new FixedLengthStringData(4).isAPartOf(filler, 36);
  	public FixedLengthStringData crtable11 = new FixedLengthStringData(4).isAPartOf(filler, 40);
  	public FixedLengthStringData crtable12 = new FixedLengthStringData(4).isAPartOf(filler, 44);
  	public FixedLengthStringData crtable13 = new FixedLengthStringData(4).isAPartOf(filler, 48);
  	public FixedLengthStringData crtable14 = new FixedLengthStringData(4).isAPartOf(filler, 52);
  	public FixedLengthStringData crtable15 = new FixedLengthStringData(4).isAPartOf(filler, 56);
  	public FixedLengthStringData crtable16 = new FixedLengthStringData(4).isAPartOf(filler, 60);
  	public FixedLengthStringData crtable17 = new FixedLengthStringData(4).isAPartOf(filler, 64);
  	public FixedLengthStringData crtable18 = new FixedLengthStringData(4).isAPartOf(filler, 68);
  	public FixedLengthStringData crtable19 = new FixedLengthStringData(4).isAPartOf(filler, 72);
  	public FixedLengthStringData crtable20 = new FixedLengthStringData(4).isAPartOf(filler, 76);
  	public FixedLengthStringData ctables = new FixedLengthStringData(80).isAPartOf(tr51hRec, 80);
  	public FixedLengthStringData[] ctable = FLSArrayPartOfStructure(20, 4, ctables, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(80).isAPartOf(ctables, 0, FILLER_REDEFINE);
  	public FixedLengthStringData ctable01 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
  	public FixedLengthStringData ctable02 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
  	public FixedLengthStringData ctable03 = new FixedLengthStringData(4).isAPartOf(filler1, 8);
  	public FixedLengthStringData ctable04 = new FixedLengthStringData(4).isAPartOf(filler1, 12);
  	public FixedLengthStringData ctable05 = new FixedLengthStringData(4).isAPartOf(filler1, 16);
  	public FixedLengthStringData ctable06 = new FixedLengthStringData(4).isAPartOf(filler1, 20);
  	public FixedLengthStringData ctable07 = new FixedLengthStringData(4).isAPartOf(filler1, 24);
  	public FixedLengthStringData ctable08 = new FixedLengthStringData(4).isAPartOf(filler1, 28);
  	public FixedLengthStringData ctable09 = new FixedLengthStringData(4).isAPartOf(filler1, 32);
  	public FixedLengthStringData ctable10 = new FixedLengthStringData(4).isAPartOf(filler1, 36);
  	public FixedLengthStringData ctable11 = new FixedLengthStringData(4).isAPartOf(filler1, 40);
  	public FixedLengthStringData ctable12 = new FixedLengthStringData(4).isAPartOf(filler1, 44);
  	public FixedLengthStringData ctable13 = new FixedLengthStringData(4).isAPartOf(filler1, 48);
  	public FixedLengthStringData ctable14 = new FixedLengthStringData(4).isAPartOf(filler1, 52);
  	public FixedLengthStringData ctable15 = new FixedLengthStringData(4).isAPartOf(filler1, 56);
  	public FixedLengthStringData ctable16 = new FixedLengthStringData(4).isAPartOf(filler1, 60);
  	public FixedLengthStringData ctable17 = new FixedLengthStringData(4).isAPartOf(filler1, 64);
  	public FixedLengthStringData ctable18 = new FixedLengthStringData(4).isAPartOf(filler1, 68);
  	public FixedLengthStringData ctable19 = new FixedLengthStringData(4).isAPartOf(filler1, 72);
  	public FixedLengthStringData ctable20 = new FixedLengthStringData(4).isAPartOf(filler1, 76);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(340).isAPartOf(tr51hRec, 160, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr51hRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr51hRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}