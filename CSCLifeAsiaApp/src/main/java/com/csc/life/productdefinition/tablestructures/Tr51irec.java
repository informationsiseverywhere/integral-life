package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:19:52
 * Description:
 * Copybook name: TR51IREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr51irec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr51iRec = new FixedLengthStringData(500);
  	public FixedLengthStringData indcs = new FixedLengthStringData(4).isAPartOf(tr51iRec, 0);
  	public FixedLengthStringData[] indc = FLSArrayPartOfStructure(2, 2, indcs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(indcs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData indc01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData indc02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(496).isAPartOf(tr51iRec, 4, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr51iRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr51iRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}