package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:43
 * Description:
 * Copybook name: TT503REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tt503rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tt503Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData bvolumes = new FixedLengthStringData(96).isAPartOf(tt503Rec, 0);
  	public ZonedDecimalData[] bvolume = ZDArrayPartOfStructure(16, 6, 0, bvolumes, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(96).isAPartOf(bvolumes, 0, FILLER_REDEFINE);
  	public ZonedDecimalData bvolume01 = new ZonedDecimalData(6, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData bvolume02 = new ZonedDecimalData(6, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData bvolume03 = new ZonedDecimalData(6, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData bvolume04 = new ZonedDecimalData(6, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData bvolume05 = new ZonedDecimalData(6, 0).isAPartOf(filler, 24);
  	public ZonedDecimalData bvolume06 = new ZonedDecimalData(6, 0).isAPartOf(filler, 30);
  	public ZonedDecimalData bvolume07 = new ZonedDecimalData(6, 0).isAPartOf(filler, 36);
  	public ZonedDecimalData bvolume08 = new ZonedDecimalData(6, 0).isAPartOf(filler, 42);
  	public ZonedDecimalData bvolume09 = new ZonedDecimalData(6, 0).isAPartOf(filler, 48);
  	public ZonedDecimalData bvolume10 = new ZonedDecimalData(6, 0).isAPartOf(filler, 54);
  	public ZonedDecimalData bvolume11 = new ZonedDecimalData(6, 0).isAPartOf(filler, 60);
  	public ZonedDecimalData bvolume12 = new ZonedDecimalData(6, 0).isAPartOf(filler, 66);
  	public ZonedDecimalData bvolume13 = new ZonedDecimalData(6, 0).isAPartOf(filler, 72);
  	public ZonedDecimalData bvolume14 = new ZonedDecimalData(6, 0).isAPartOf(filler, 78);
  	public ZonedDecimalData bvolume15 = new ZonedDecimalData(6, 0).isAPartOf(filler, 84);
  	public ZonedDecimalData bvolume16 = new ZonedDecimalData(6, 0).isAPartOf(filler, 90);
  	public FixedLengthStringData insprms = new FixedLengthStringData(96).isAPartOf(tt503Rec, 96);
  	public ZonedDecimalData[] insprm = ZDArrayPartOfStructure(16, 6, 0, insprms, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(96).isAPartOf(insprms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData insprm01 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData insprm02 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 6);
  	public ZonedDecimalData insprm03 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData insprm04 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 18);
  	public ZonedDecimalData insprm05 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 24);
  	public ZonedDecimalData insprm06 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 30);
  	public ZonedDecimalData insprm07 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 36);
  	public ZonedDecimalData insprm08 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 42);
  	public ZonedDecimalData insprm09 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 48);
  	public ZonedDecimalData insprm10 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 54);
  	public ZonedDecimalData insprm11 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 60);
  	public ZonedDecimalData insprm12 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 66);
  	public ZonedDecimalData insprm13 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 72);
  	public ZonedDecimalData insprm14 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 78);
  	public ZonedDecimalData insprm15 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 84);
  	public ZonedDecimalData insprm16 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 90);
  	public ZonedDecimalData riskunit = new ZonedDecimalData(6, 0).isAPartOf(tt503Rec, 192);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(302).isAPartOf(tt503Rec, 198, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tt503Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tt503Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}