package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.productdefinition.dataaccess.model.Hitrpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface HitrpfDAO extends BaseDAO<Hitrpf> {
    public Map<Long, List<Hitrpf>> searchHitrRecord(List<Long> hitdUQ);
    public Map<String, List<Hitrpf>> searchHitrRecordByChdrnum(List<String> chdrnumList);
    public Map<String, List<Hitrpf>> searchHitrintRecordByChdrnum(List<String> chdrnumList);
    public void updatedHitrRecord(List<Hitrpf> hitrBulkOpList);
    public void insertHitrpfRecord(List<Hitrpf> hitrBulkOpList);

    public Hitrpf getHitrpf(Hitrpf hitrpf);
    public void bulkUpdatedHitralo(List<Hitrpf> hitrBulkOpList);
    public List<String> checkHitrrnlRecordByChdrnum(String coy, List<String> chdrnumList);
    
    public List<Hitrpf> searchHitrrnlRecord(String coy, String chdrnum);
	public List<Hitrpf> searchHitrRecord(String coy, String chdrnum);
	
	public List<Hitrpf> searchHitraloRecordForDel(String coy, String chdrnum);
	public void deleteHitrpfRecord(List<Hitrpf> hitrpfList);
	public List<Hitrpf> searchHitrRecord(Hitrpf hitr); //ILIFE-8786
	public void updatedHitrpfRecord(List<Hitrpf> hitrList); //ILIFE-8786
	public List<Hitrpf> searchHitrdryRecord(String chdrcoy, String chdrnum);
}