
package com.csc.life.productdefinition.procedures;


import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.smart400framework.parent.SMARTCodeModel;

/** 
*This subroutine does not perform any calculation.
*/

public class Prmpm00 extends SMARTCodeModel {
    private Premiumrec premiumrec = new Premiumrec();	

	public Prmpm00() {
		super();
	}


public void mainline(Object... parmArray)
	{
		premiumrec.premiumRec = convertAndSetParam(premiumrec.premiumRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
	return;
	}

protected void exit090()
	{
		exitProgram();
	}

}
