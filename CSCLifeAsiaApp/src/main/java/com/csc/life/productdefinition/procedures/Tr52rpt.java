/*
 * File: Tr52rpt.java
 * Date: December 3, 2013 3:59:36 AM ICT
 * Author: CSC
 * 
 * Class transformed from TR52RPT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.Tr52rrec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR52R.
*
*
*
****************************************************************** ****
* </pre>
*/
public class Tr52rpt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tr52rrec tr52rrec = new Tr52rrec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr52rpt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr52rrec.tr52rRec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		generalCopyLinesInner.fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		generalCopyLinesInner.fieldNo006.set(datcon1rec.extDate);
		generalCopyLinesInner.fieldNo007.set(tr52rrec.agelimit);
		generalCopyLinesInner.fieldNo008.set(tr52rrec.toterm01);
		generalCopyLinesInner.fieldNo009.set(tr52rrec.factorsa01);
		generalCopyLinesInner.fieldNo010.set(tr52rrec.factsamn01);
		generalCopyLinesInner.fieldNo011.set(tr52rrec.factsamx01);
		generalCopyLinesInner.fieldNo012.set(tr52rrec.toterm02);
		generalCopyLinesInner.fieldNo013.set(tr52rrec.factorsa02);
		generalCopyLinesInner.fieldNo014.set(tr52rrec.factsamn02);
		generalCopyLinesInner.fieldNo015.set(tr52rrec.factsamx02);
		generalCopyLinesInner.fieldNo016.set(tr52rrec.toterm03);
		generalCopyLinesInner.fieldNo017.set(tr52rrec.factorsa03);
		generalCopyLinesInner.fieldNo018.set(tr52rrec.factsamn03);
		generalCopyLinesInner.fieldNo019.set(tr52rrec.factsamx03);
		generalCopyLinesInner.fieldNo020.set(tr52rrec.toterm04);
		generalCopyLinesInner.fieldNo021.set(tr52rrec.factorsa04);
		generalCopyLinesInner.fieldNo022.set(tr52rrec.factsamn04);
		generalCopyLinesInner.fieldNo023.set(tr52rrec.factsamx04);
		generalCopyLinesInner.fieldNo024.set(tr52rrec.toterm05);
		generalCopyLinesInner.fieldNo025.set(tr52rrec.factorsa05);
		generalCopyLinesInner.fieldNo026.set(tr52rrec.factsamn05);
		generalCopyLinesInner.fieldNo027.set(tr52rrec.factsamx05);
		generalCopyLinesInner.fieldNo028.set(tr52rrec.toterm06);
		generalCopyLinesInner.fieldNo029.set(tr52rrec.factorsa06);
		generalCopyLinesInner.fieldNo030.set(tr52rrec.factsamn06);
		generalCopyLinesInner.fieldNo031.set(tr52rrec.factsamx06);
		generalCopyLinesInner.fieldNo032.set(tr52rrec.toterm07);
		generalCopyLinesInner.fieldNo033.set(tr52rrec.factorsa07);
		generalCopyLinesInner.fieldNo034.set(tr52rrec.factsamn07);
		generalCopyLinesInner.fieldNo035.set(tr52rrec.factsamx07);
		generalCopyLinesInner.fieldNo036.set(tr52rrec.toterm08);
		generalCopyLinesInner.fieldNo037.set(tr52rrec.factorsa08);
		generalCopyLinesInner.fieldNo038.set(tr52rrec.factsamn08);
		generalCopyLinesInner.fieldNo039.set(tr52rrec.factsamx08);
		generalCopyLinesInner.fieldNo040.set(tr52rrec.toterm09);
		generalCopyLinesInner.fieldNo041.set(tr52rrec.factorsa09);
		generalCopyLinesInner.fieldNo042.set(tr52rrec.factsamn09);
		generalCopyLinesInner.fieldNo043.set(tr52rrec.factsamx09);
		generalCopyLinesInner.fieldNo044.set(tr52rrec.toterm10);
		generalCopyLinesInner.fieldNo045.set(tr52rrec.factorsa10);
		generalCopyLinesInner.fieldNo046.set(tr52rrec.factsamn10);
		generalCopyLinesInner.fieldNo047.set(tr52rrec.factsamx10);
		generalCopyLinesInner.fieldNo048.set(tr52rrec.dsifact);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine017);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(52).isAPartOf(wsaaPrtLine001, 24, FILLER).init("Frequency Alteration Basis                     SR52R");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(74);
	private FixedLengthStringData filler9 = new FixedLengthStringData(59).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine004, 59, FILLER).init("Age Limit :");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine004, 71).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(67);
	private FixedLengthStringData filler11 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(60).isAPartOf(wsaaPrtLine005, 7, FILLER).init("Up to Term    AP/SP Factor      Min factor        Max Factor");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(65);
	private FixedLengthStringData filler13 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 8).setPattern("ZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine006, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine006, 21).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine006, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine006, 39).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine006, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine006, 57).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(65);
	private FixedLengthStringData filler17 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 8).setPattern("ZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine007, 21).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine007, 39).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine007, 57).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(65);
	private FixedLengthStringData filler21 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 8).setPattern("ZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine008, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine008, 21).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine008, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine008, 39).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine008, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine008, 57).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(65);
	private FixedLengthStringData filler25 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 8).setPattern("ZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine009, 21).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine009, 39).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine009, 57).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(65);
	private FixedLengthStringData filler29 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 8).setPattern("ZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine010, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine010, 21).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine010, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine010, 39).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine010, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine010, 57).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(65);
	private FixedLengthStringData filler33 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 8).setPattern("ZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine011, 21).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine011, 39).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine011, 57).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(65);
	private FixedLengthStringData filler37 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 8).setPattern("ZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine012, 21).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine012, 39).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine012, 57).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(65);
	private FixedLengthStringData filler41 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 8).setPattern("ZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine013, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine013, 21).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine013, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine013, 39).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine013, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine013, 57).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(65);
	private FixedLengthStringData filler45 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 8).setPattern("ZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine014, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine014, 21).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine014, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine014, 39).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine014, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine014, 57).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(65);
	private FixedLengthStringData filler49 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 8).setPattern("ZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine015, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine015, 21).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine015, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine015, 39).setPattern("ZZZ.ZZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine015, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine015, 57).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(46);
	private FixedLengthStringData filler53 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine016, 0, FILLER).init(" Casual Premium SA qualifying factor:");
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(7, 4).isAPartOf(wsaaPrtLine016, 38).setPattern("ZZZ.ZZZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(28);
	private FixedLengthStringData filler54 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine017, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
}
}
