package com.csc.life.productdefinition.dataaccess.model;

import java.math.BigDecimal;

public class Mrtapf {
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private BigDecimal prat;
	private int coverc;
	private String prmdetails;
	private String mlinsopt;
	private String mlresind;
	private String mbnkref;
	private int loandur;
	private String intcaltype;
	private int phunitvalue;
	private String userProfile;
	private String jobName;
	private String datime;

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public BigDecimal getPrat() {
		return prat;
	}

	public void setPrat(BigDecimal prat) {
		this.prat = prat;
	}

	public int getCoverc() {
		return coverc;
	}

	public void setCoverc(int coverc) {
		this.coverc = coverc;
	}

	public String getPrmdetails() {
		return prmdetails;
	}

	public void setPrmdetails(String prmdetails) {
		this.prmdetails = prmdetails;
	}

	public String getMlinsopt() {
		return mlinsopt;
	}

	public void setMlinsopt(String mlinsopt) {
		this.mlinsopt = mlinsopt;
	}

	public String getMlresind() {
		return mlresind;
	}

	public void setMlresind(String mlresind) {
		this.mlresind = mlresind;
	}

	public String getMbnkref() {
		return mbnkref;
	}

	public void setMbnkref(String mbnkref) {
		this.mbnkref = mbnkref;
	}

	public int getLoandur() {
		return loandur;
	}

	public void setLoandur(int loandur) {
		this.loandur = loandur;
	}

	public String getIntcalType() {
		return intcaltype;
	}

	public void setIntcalType(String intcaltype) {
		this.intcaltype = intcaltype;
	}

	public int getPhunitvalue() {
		return phunitvalue;
	}

	public void setPhunitvalue(int phunitvalue) {
		this.phunitvalue = phunitvalue;
	}

	public String getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getDatime() {
		return datime;
	}

	public void setDatime(String datime) {
		this.datime = datime;
	}

}
