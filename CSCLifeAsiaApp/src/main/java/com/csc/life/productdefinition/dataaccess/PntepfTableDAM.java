package com.csc.life.productdefinition.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: PntepfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:06
 * Class transformed from PNTEPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PntepfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 158;
	public FixedLengthStringData pnterec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData pntepfRecord = pnterec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(pnterec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(pnterec);
	public PackedDecimalData trandate = DD.trandate.copy().isAPartOf(pnterec);
	public PackedDecimalData trantime = DD.trantime.copy().isAPartOf(pnterec);
	public PackedDecimalData seqno = DD.zseqno.copy().isAPartOf(pnterec);
	public FixedLengthStringData message = DD.message.copy().isAPartOf(pnterec);
	public FixedLengthStringData pnotecat = DD.pnotecat.copy().isAPartOf(pnterec);
	public FixedLengthStringData userid = DD.userid.copy().isAPartOf(pnterec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(pnterec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(pnterec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(pnterec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public PntepfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for PntepfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public PntepfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for PntepfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public PntepfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for PntepfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public PntepfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("PNTEPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"TRANDATE, " +
							"TRANTIME, " +
							"ZSEQNO, " +
							"MESSAGE, " +
							"PNOTECAT, " +
							"USERID, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     trandate,
                                     trantime,
                                     seqno,
                                     message,
                                     pnotecat,
                                     userid,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		trandate.clear();
  		trantime.clear();
  		seqno.clear();
  		message.clear();
  		pnotecat.clear();
  		userid.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getPnterec() {
  		return pnterec;
	}

	public FixedLengthStringData getPntepfRecord() {
  		return pntepfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setPnterec(what);
	}

	public void setPnterec(Object what) {
  		this.pnterec.set(what);
	}

	public void setPntepfRecord(Object what) {
  		this.pntepfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(pnterec.getLength());
		result.set(pnterec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}