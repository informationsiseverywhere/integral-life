/*
 * File: Tr51ept.java
 * Date: 30 August 2009 2:42:13
 * Author: Quipoz Limited
 * 
 * Class transformed from TR51EPT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.Tr51erec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR51E.
*
*
*
***********************************************************************
* </pre>
*/
public class Tr51ept extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(45).isAPartOf(wsaaPrtLine001, 31, FILLER).init("SA of Rider & Basic <= %Income          SR51E");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(47);
	private FixedLengthStringData filler7 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine003, 7, FILLER).init("Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 21);
	private FixedLengthStringData filler9 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 31, FILLER).init("  To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 37);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(18);
	private FixedLengthStringData filler10 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine004, 7, FILLER).init("Component :");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(74);
	private FixedLengthStringData filler12 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 7);
	private FixedLengthStringData filler13 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 11, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 14);
	private FixedLengthStringData filler14 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 18, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 21);
	private FixedLengthStringData filler15 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 28);
	private FixedLengthStringData filler16 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 35);
	private FixedLengthStringData filler17 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 42);
	private FixedLengthStringData filler18 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 49);
	private FixedLengthStringData filler19 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 56);
	private FixedLengthStringData filler20 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 60, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 63);
	private FixedLengthStringData filler21 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 67, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 70);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(74);
	private FixedLengthStringData filler22 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 7);
	private FixedLengthStringData filler23 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 11, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 14);
	private FixedLengthStringData filler24 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 18, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 21);
	private FixedLengthStringData filler25 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 28);
	private FixedLengthStringData filler26 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 35);
	private FixedLengthStringData filler27 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 42);
	private FixedLengthStringData filler28 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 49);
	private FixedLengthStringData filler29 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 56);
	private FixedLengthStringData filler30 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 60, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 63);
	private FixedLengthStringData filler31 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 67, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 70);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(60);
	private FixedLengthStringData filler32 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler33 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine007, 28, FILLER).init("Comparison       Life Ann Income");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(55);
	private FixedLengthStringData filler34 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler35 = new FixedLengthStringData(43).isAPartOf(wsaaPrtLine008, 12, FILLER).init("Age Band        Operator            Up to %");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(53);
	private FixedLengthStringData filler36 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler37 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 12, FILLER).init("<=");
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 16).setPattern("ZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 31);
	private FixedLengthStringData filler39 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine009, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 50).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(53);
	private FixedLengthStringData filler40 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler41 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 12, FILLER).init("<=");
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 16).setPattern("ZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine010, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 31);
	private FixedLengthStringData filler43 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine010, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 50).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(53);
	private FixedLengthStringData filler44 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler45 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 12, FILLER).init("<=");
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 16).setPattern("ZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine011, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 31);
	private FixedLengthStringData filler47 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine011, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 50).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(53);
	private FixedLengthStringData filler48 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler49 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 12, FILLER).init("<=");
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 16).setPattern("ZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine012, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 31);
	private FixedLengthStringData filler51 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine012, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 50).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(53);
	private FixedLengthStringData filler52 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler53 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 12, FILLER).init("<=");
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 16).setPattern("ZZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine013, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 31);
	private FixedLengthStringData filler55 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine013, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 50).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(53);
	private FixedLengthStringData filler56 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler57 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 12, FILLER).init("<=");
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 16).setPattern("ZZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine014, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 31);
	private FixedLengthStringData filler59 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine014, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 50).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(53);
	private FixedLengthStringData filler60 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler61 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 12, FILLER).init("<=");
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 16).setPattern("ZZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine015, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 31);
	private FixedLengthStringData filler63 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine015, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 50).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(53);
	private FixedLengthStringData filler64 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler65 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 12, FILLER).init("<=");
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 16).setPattern("ZZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine016, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 31);
	private FixedLengthStringData filler67 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine016, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 50).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(53);
	private FixedLengthStringData filler68 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler69 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine017, 12, FILLER).init("<=");
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine017, 16).setPattern("ZZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine017, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 31);
	private FixedLengthStringData filler71 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine017, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine017, 50).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(53);
	private FixedLengthStringData filler72 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine018, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler73 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine018, 12, FILLER).init("<=");
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine018, 16).setPattern("ZZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine018, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine018, 31);
	private FixedLengthStringData filler75 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine018, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine018, 50).setPattern("ZZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tr51erec tr51erec = new Tr51erec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr51ept() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr51erec.tr51eRec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo027.set(tr51erec.age01);
		fieldNo029.set(tr51erec.overdueMina01);
		fieldNo030.set(tr51erec.age02);
		fieldNo032.set(tr51erec.overdueMina02);
		fieldNo033.set(tr51erec.age03);
		fieldNo035.set(tr51erec.overdueMina03);
		fieldNo036.set(tr51erec.age04);
		fieldNo038.set(tr51erec.overdueMina04);
		fieldNo039.set(tr51erec.age05);
		fieldNo041.set(tr51erec.overdueMina05);
		fieldNo042.set(tr51erec.age06);
		fieldNo044.set(tr51erec.overdueMina06);
		fieldNo045.set(tr51erec.age07);
		fieldNo047.set(tr51erec.overdueMina07);
		fieldNo048.set(tr51erec.age08);
		fieldNo050.set(tr51erec.overdueMina08);
		fieldNo051.set(tr51erec.age09);
		fieldNo053.set(tr51erec.overdueMina09);
		fieldNo054.set(tr51erec.age10);
		fieldNo056.set(tr51erec.overdueMina10);
		fieldNo007.set(tr51erec.crtable01);
		fieldNo008.set(tr51erec.crtable02);
		fieldNo009.set(tr51erec.crtable03);
		fieldNo010.set(tr51erec.crtable04);
		fieldNo011.set(tr51erec.crtable05);
		fieldNo012.set(tr51erec.crtable06);
		fieldNo013.set(tr51erec.crtable07);
		fieldNo014.set(tr51erec.crtable08);
		fieldNo015.set(tr51erec.crtable09);
		fieldNo016.set(tr51erec.crtable10);
		fieldNo017.set(tr51erec.crtable11);
		fieldNo018.set(tr51erec.crtable12);
		fieldNo019.set(tr51erec.crtable13);
		fieldNo020.set(tr51erec.crtable14);
		fieldNo021.set(tr51erec.crtable15);
		fieldNo022.set(tr51erec.crtable16);
		fieldNo023.set(tr51erec.crtable17);
		fieldNo024.set(tr51erec.crtable18);
		fieldNo025.set(tr51erec.crtable19);
		fieldNo026.set(tr51erec.crtable20);
		fieldNo028.set(tr51erec.indc01);
		fieldNo031.set(tr51erec.indc02);
		fieldNo034.set(tr51erec.indc03);
		fieldNo037.set(tr51erec.indc04);
		fieldNo040.set(tr51erec.indc05);
		fieldNo043.set(tr51erec.indc06);
		fieldNo046.set(tr51erec.indc07);
		fieldNo049.set(tr51erec.indc08);
		fieldNo052.set(tr51erec.indc09);
		fieldNo055.set(tr51erec.indc10);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
