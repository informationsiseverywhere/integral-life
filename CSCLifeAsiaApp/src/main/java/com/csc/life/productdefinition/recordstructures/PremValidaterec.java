package com.csc.life.productdefinition.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

public class PremValidaterec extends ExternalData {
	
	public FixedLengthStringData premValidateRec = new FixedLengthStringData(getPremValidateRecSize());
	public FixedLengthStringData billFreq = new FixedLengthStringData(2).isAPartOf(premValidateRec, 0);
	public FixedLengthStringData currCode = new FixedLengthStringData(3).isAPartOf(premValidateRec, 2);
	public PackedDecimalData inputPrem = new PackedDecimalData(17, 2).isAPartOf(premValidateRec, 5);
	public FixedLengthStringData covrgCode = new FixedLengthStringData(4).isAPartOf(premValidateRec, 14);
	public PackedDecimalData ratingDate = new PackedDecimalData(8, 0).isAPartOf(premValidateRec, 18);
	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(premValidateRec, 23);
	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(premValidateRec, 26); 
	
	
	public void initialize() {
		COBOLFunctions.initialize(premValidateRec);
	}	

	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			premValidateRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	
	public int getPremValidateRecSize()
	{
		return 30;
	}

}
