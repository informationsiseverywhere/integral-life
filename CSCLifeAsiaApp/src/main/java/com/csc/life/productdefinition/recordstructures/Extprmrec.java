package com.csc.life.productdefinition.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:35
 * Description:
 * Copybook name: EXTPRMREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Extprmrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData parmRec = new FixedLengthStringData(81);
  	public ZonedDecimalData freqFactor = new ZonedDecimalData(5, 4).isAPartOf(parmRec, 0);
  	public ZonedDecimalData mortRate = new ZonedDecimalData(6, 2).isAPartOf(parmRec, 5);
  	public ZonedDecimalData premium = new ZonedDecimalData(17, 2).isAPartOf(parmRec, 11);
  	public ZonedDecimalData sumass = new ZonedDecimalData(17, 2).isAPartOf(parmRec, 28);
  	public ZonedDecimalData exFactor = new ZonedDecimalData(6, 5).isAPartOf(parmRec, 45);
  	public ZonedDecimalData percent = new ZonedDecimalData(5, 2).isAPartOf(parmRec, 51);
  	public ZonedDecimalData term = new ZonedDecimalData(2, 0).isAPartOf(parmRec, 56).setUnsigned();
  	public FixedLengthStringData freq = new FixedLengthStringData(2).isAPartOf(parmRec, 58);
  	public ZonedDecimalData loading = new ZonedDecimalData(17, 2).isAPartOf(parmRec, 60);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(parmRec, 77);


	public void initialize() {
		COBOLFunctions.initialize(parmRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}