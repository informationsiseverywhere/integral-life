package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.dao.ClexpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clexpf;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ElogpfDAO;
import com.csc.smart400framework.dataaccess.model.Elogpf;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class VpxchdrUtil extends COBOLConvCodeModel{
	
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Premiumrec premiumrec = new Premiumrec();
	private VpxchdrPojo vpxchdrPojo = new VpxchdrPojo();
	private String chdrlnbrec = "CHDRLNBREC";
	private String wsaaSubr = "VPXCHDRUTIL";
	/*Contract Header Life Fields*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private Clexpf clexpf = null;
	private ClexpfDAO clexpfDAO =  getApplicationContext().getBean("clexpfDAO", ClexpfDAO.class);
    private ElogpfDAO elogpfDAO =  getApplicationContext().getBean("elogpfDAO",ElogpfDAO.class);
    private int wsaaErrnum = 0;
    private Elogpf elogpf = null;
	private FixedLengthStringData temp = new FixedLengthStringData(39).init(SPACES);
	
   public VpxchdrPojo callInit(Vpmcalcrec vpmcalcrec) {
	syserrrec.subrname.set(wsaaSubr);
	vpxchdrPojo.setStatuz("****");
	premiumrec.premiumRec.set(vpmcalcrec.linkageArea);
	initializVpxchdrPojo();
	findChdrlif100();
	return vpxchdrPojo;
   }
   
   protected void findChdrlif100() {
	    chdrlnbIO.setDataKey(SPACES);
		chdrlnbIO.setChdrcoy(premiumrec.chdrChdrcoy);
		chdrlnbIO.setChdrnum(premiumrec.chdrChdrnum);
		
		chdrlnbIO.setStatuz(varcom.oK);
		chdrlnbIO.setFunction(varcom.readr);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		//Unexpected result
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			systemError();
	   	   	return ;
		}
		
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			systemError();
	   	   	return ;
		}
		if (isEQ(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			chdrlnbIO.setChdrcoy(premiumrec.chdrChdrcoy);
			chdrlnbIO.setChdrnum(premiumrec.chdrChdrnum);
			chdrlnbIO.setFunction(varcom.readr);
			chdrlnbIO.setFormat(chdrlnbrec);
			SmartFileCode.execute(appVars, chdrlnbIO);
			//Record not found
			if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
				vpxchdrPojo.setStatuz("MRNF");
				return ;
			}
		}
		
		//Set up output fields
		vpxchdrPojo.setCopybook("VPXCHDRREC");
		vpxchdrPojo.setOccdate(chdrlnbIO.occdate.toInt());
		vpxchdrPojo.setBillchnl(chdrlnbIO.billchnl.toString());
		vpxchdrPojo.setCownnum(chdrlnbIO.cownnum.toString());
		vpxchdrPojo.setJownnum(chdrlnbIO.jownnum.toString());
		vpxchdrPojo.setCnttype(chdrlnbIO.cnttype.toString());
		
		if (isNE(chdrlnbIO.getCownnum(), SPACES)){
			findClex300();
		}
		
   }
   
   protected void findClex300()
   {

   	clexpf = clexpfDAO.getClexpfByClntkey(chdrlnbIO.getCownpfx().toString(), chdrlnbIO.getCowncoy().toString(), chdrlnbIO.getCownnum().toString());

   	if (clexpf == null) {
   		syserrrec.statuz.set("MRNF");
   		syserrrec.params.set(chdrlnbIO.getCownpfx().toString().concat(chdrlnbIO.getCowncoy().toString()).concat(chdrlnbIO.getCownnum().toString()));
   		systemError();
   	   	return ;
   	}
   	if (isNE(clexpf.getRstaflag(),"Y")
   			&& isNE(chdrlnbIO.getJownnum(),SPACES)) {
   		clexpf = clexpfDAO.getClexpfByClntkey(chdrlnbIO.getCownpfx().toString(), chdrlnbIO.getCowncoy().toString(), chdrlnbIO.getJownnum().toString()); 	
   		if (clexpf == null) {
   			syserrrec.statuz.set("MRNF");
   			syserrrec.params.set(chdrlnbIO.getCownpfx().toString().concat(chdrlnbIO.getCowncoy().toString()).concat(chdrlnbIO.getJownnum().toString()));
   			systemError();
   	   	   	return ;
   		}
   	}
   	//Set up output fields
   	if (isEQ(clexpf.getRstaflag(),"Y")){
   		vpxchdrPojo.setRstaflag("Y");
   	} else {
   		vpxchdrPojo.setRstaflag("N");
   	}
   	/* EXIT */
   }
   
   protected void initializVpxchdrPojo()
   {
	 vpxchdrPojo.setOccdate(varcom.maxdate.toInt());
	 vpxchdrPojo.setBillchnl(" ");
	 vpxchdrPojo.setCownnum(" ");
	 vpxchdrPojo.setJownnum(" ");
	 vpxchdrPojo.setCnttype(" ");
	 vpxchdrPojo.setRstaflag("  ");
   }
   
   protected void systemError() {
	   String maxErorNum= elogpfDAO.findMaxErorNum();
	   if(maxErorNum == null) {
		   wsaaErrnum = 0;
	   }else if(StringUtil.isEmptyOrSpace(maxErorNum)){
			wsaaErrnum = 0;
		}else {
			wsaaErrnum = Integer.parseInt(maxErorNum);
		}
	   wsaaErrnum++;
	   
	   elogpf = new Elogpf();
	   elogpf.setErrnum(String.valueOf(wsaaErrnum));
	   elogpf.setFlogdata(wsaaSubr.concat(temp.toString()).concat("1").concat(syserrrec.params.toString()));
	   elogpf.setTerminalid(varcom.vrcmTerminal==null?"":varcom.vrcmTerminal.toString());
	   elogpf.setWsspcomn(" ");
	   elogpf.setWsspuser(" ");
	   elogpfDAO.insertElogpf(elogpf);
	   
   }
   
 
}
