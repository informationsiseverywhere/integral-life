package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.interestbearing.dataaccess.HitraloTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.tablestructures.T6640rec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.regularprocessing.recordstructures.Bonusrec;
import com.csc.life.regularprocessing.tablestructures.T6639rec;
import com.csc.life.terminationclaims.dataaccess.CovrclmTableDAM;
import com.csc.life.terminationclaims.recordstructures.Deathrec;
import com.csc.life.terminationclaims.recordstructures.Vpxdtavrec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrndthTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrssurTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1692rec;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;


public class Vpxdtav extends COBOLConvCodeModel {
    private static final Logger LOGGER = LoggerFactory.getLogger(Vpxdtav.class);
    public static final String ROUTINE = QPUtilities.getThisClass();
    private FixedLengthStringData wsaaSubr = new FixedLengthStringData(7).init("VPXDTAV");

    private static final String E049 = "E049";
    private static final String H832 = "H832";
    private static final String E652 = "E652";
    private static final String G408 = "G408";

    // Format
    private static final String ACBLREC = "ACBLREC";
    private static final String CHDRLIFREC = "CHDRLIFREC";
    private static final String COVRREC = "COVRREC";
    private static final String COVRCLMREC = "COVRCLMREC";
    private static final String DESCREC = "DESCREC";
    private static final String HITRALOREC = "HITRALOREC";
    private static final String HITSREC = "HITSREC";
    private static final String ITEMREC = "ITEMREC";
    private static final String UTRNDTHREC = "UTRNDTHREC";
    private static final String UTRSSURREC = "UTRSSURREC";

    // Tables
    private static final String T1692 = "T1692";
    private static final String T1693 = "T1693";
    private static final String T3629 = "T3629";
    private static final String T5515 = "T5515";
    private static final String T5645 = "T5645";
    private static final String T5679 = "T5679";
    private static final String T5687 = "T5687";
    private static final String T6639 = "T6639";
    private static final String T6640 = "T6640";
    private static final String T6644 = "T6644";


    private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).init(SPACES);
    private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).init(SPACES);
    private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
    private FixedLengthStringData wsaaDescDescitem = new FixedLengthStringData(8).init(SPACES);
    private FixedLengthStringData wsaaDthMth = new FixedLengthStringData(4).init(SPACES);
    private FixedLengthStringData wsaaFund = new FixedLengthStringData(4).init(SPACES);
    private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
    private FixedLengthStringData wsaaProcessedRvBon = new FixedLengthStringData(1).init("N");
    private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
    private FixedLengthStringData wsaaType = new FixedLengthStringData(1).init(SPACES);
    private FixedLengthStringData wsaaUnitType = new FixedLengthStringData(1).init(SPACES);
    private FixedLengthStringData wsaaValidStatuz = new FixedLengthStringData(1).init(SPACES);
    private FixedLengthStringData wsaaVirtualFund = new FixedLengthStringData(4).init(SPACES);
    private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2).init(0);
    private FixedLengthStringData wsaaT6639Key = new FixedLengthStringData(6);
    private FixedLengthStringData wsaaT6639Dcmeth = new FixedLengthStringData(4).isAPartOf(wsaaT6639Key, 0);
    private FixedLengthStringData wsaaPremStat = new FixedLengthStringData(2).isAPartOf(wsaaT6639Key, 4);
    private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
    private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
    private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
    private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
    private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
    private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
    private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
    // temp ?????
    private FixedLengthStringData wsaaSwitch = new FixedLengthStringData(1).init("Y");
    private Validator firstTime = new Validator(wsaaSwitch, "Y");

    private FixedLengthStringData wsaa_PlanSuff = new FixedLengthStringData(8);
    private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).isAPartOf(wsaa_PlanSuff, 0).setUnsigned();
    private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaa_PlanSuff, 0, REDEFINE);
    private ZonedDecimalData wsaaFiller = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 0).setUnsigned();
    private ZonedDecimalData wsaaPlanSuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

    private static final FixedLengthStringData wsccConstants = new FixedLengthStringData(41);
    private static final FixedLengthStringData wscc1 = new FixedLengthStringData(1).isAPartOf(wsccConstants, 0).init("1");
    private static final FixedLengthStringData wscc2 = new FixedLengthStringData(1).isAPartOf(wsccConstants, 1).init("2");
    private static final FixedLengthStringData wscc00 = new FixedLengthStringData(2).isAPartOf(wsccConstants, 2).init("00");
    private static final FixedLengthStringData wscc01 = new FixedLengthStringData(2).isAPartOf(wsccConstants, 4).init("01");
    private static final FixedLengthStringData wsccA = new FixedLengthStringData(1).isAPartOf(wsccConstants, 6).init("A");
    private static final FixedLengthStringData wsccB = new FixedLengthStringData(1).isAPartOf(wsccConstants, 7).init("B");
    private static final FixedLengthStringData wsccC = new FixedLengthStringData(1).isAPartOf(wsccConstants, 8).init("C");
    private static final FixedLengthStringData wsccD = new FixedLengthStringData(1).isAPartOf(wsccConstants, 9).init("D");
    private static final FixedLengthStringData wsccI = new FixedLengthStringData(1).isAPartOf(wsccConstants, 10).init("I");
    private static final FixedLengthStringData wsccL = new FixedLengthStringData(1).isAPartOf(wsccConstants, 11).init("L");
    private static final FixedLengthStringData wsccM = new FixedLengthStringData(1).isAPartOf(wsccConstants, 12).init("M");
    private static final FixedLengthStringData wsccN = new FixedLengthStringData(1).isAPartOf(wsccConstants, 13).init("N");
    private static final FixedLengthStringData wsccS = new FixedLengthStringData(1).isAPartOf(wsccConstants, 14).init("S");
    private static final FixedLengthStringData wsccT = new FixedLengthStringData(1).isAPartOf(wsccConstants, 15).init("T");
    private static final FixedLengthStringData wsccX = new FixedLengthStringData(1).isAPartOf(wsccConstants, 16).init("X");
    private static final FixedLengthStringData wsccY = new FixedLengthStringData(1).isAPartOf(wsccConstants, 17).init("Y");
    private static final FixedLengthStringData wsccVd01 = new FixedLengthStringData(4).isAPartOf(wsccConstants, 18).init("VD01");
    private static final FixedLengthStringData wsccVd08 = new FixedLengthStringData(4).isAPartOf(wsccConstants, 22).init("VD08");
    private static final FixedLengthStringData wsccEXT = new FixedLengthStringData(3).isAPartOf(wsccConstants, 26).init("EXT");
    private static final FixedLengthStringData wsccINT = new FixedLengthStringData(3).isAPartOf(wsccConstants, 29).init("INT");
    private static final FixedLengthStringData wsccLOW = new FixedLengthStringData(3).isAPartOf(wsccConstants, 32).init("LOW");
    private static final FixedLengthStringData wsccREV = new FixedLengthStringData(3).isAPartOf(wsccConstants, 35).init("REV");
    private static final FixedLengthStringData wsccTRM = new FixedLengthStringData(3).isAPartOf(wsccConstants, 38).init("TRM");

    private Batckey wassBatckey = new Batckey();
    private AcblTableDAM acblIO = new AcblTableDAM();
    private Bonusrec bonusrec = new Bonusrec();
    private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
    private CovrTableDAM covrIO = new CovrTableDAM();
    private CovrclmTableDAM covrclmIO = new CovrclmTableDAM();
    private Datcon3rec datcon3rec = new Datcon3rec();
    private DescTableDAM descIO = new DescTableDAM();
    private HitraloTableDAM hitraloIO = new HitraloTableDAM();
    private HitsTableDAM hitsIO = new HitsTableDAM();
    private ItdmTableDAM itdmIO = new ItdmTableDAM();
    private ItemTableDAM itemIO = new ItemTableDAM();
    private UtrndthTableDAM utrndthIO = new UtrndthTableDAM();
    private UtrssurTableDAM utrssurIO = new UtrssurTableDAM();
    private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
    private Syserrrec syserrrec = new Syserrrec();
    private T1692rec t1692rec = new T1692rec();
    private T1693rec t1693rec = new T1693rec();
    private T3629rec t3629rec = new T3629rec();
    private T5515rec t5515rec = new T5515rec();
    private T5645rec t5645rec = new T5645rec();
    private T5679rec t5679rec = new T5679rec();
    private T5687rec t5687rec = new T5687rec();
    private T6639rec t6639rec = new T6639rec();
    private T6640rec t6640rec = new T6640rec();
    private Varcom varcom = new Varcom();

    private Deathrec deathrec = new Deathrec();
    private Vpxdtavrec vpxdtavrec = new Vpxdtavrec();
    private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();

  	private enum GotoLabel implements GOTOInterface {
		DEFAULT, vd08105, exit109, call110, exit110, nextr110, call310, exit310, call360, exit360, exit000, 
	}
  	
    public Vpxdtav() {
        super();
    }

    public void mainline (Object... parmArray) {
        vpxdtavrec = (Vpxdtavrec) parmArray[1];
		vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
        deathrec.deathRec.set(vpmcalcrec.linkageArea);
        main000();
		vpmcalcrec.linkageArea.set(deathrec.deathRec);
    }
    
    protected void main000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para000();
				}
				case exit000: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

    private void para000() {
        syserrrec.subrname.set(wsaaSubr);
        vpxdtavrec.statuz.set(Varcom.oK);
        if (isEQ(vpxdtavrec.function,"INIT")){
            init050();
            findRecord100();
        }
        else if (isEQ(vpxdtavrec.function,"GETV")) {
            returnValue200();
        }
        else {
            syserrrec.statuz.set(E049);
            vpxdtavrec.statuz.set(E049);
            systemError900();
        }
//        exitProgram();
    }
    
    private void init050() {
        vpxdtavrec.curduntbal.set(ZERO);
        vpxdtavrec.unprocessed.set(ZERO);
        deathrec.actualVal.set(ZERO);
        deathrec.estimatedVal.set(ZERO);

        vpxdtavrec.dthMth.set(SPACES);
        deathrec.description.set(SPACES);
        deathrec.element.set(SPACES);
        deathrec.fieldType.set(SPACES);
    }

    private void findRecord100() {
    	GotoLabel nextMethod = GotoLabel.DEFAULT;
    	while (true) {
    		try {
    			switch (nextMethod) {
    			case DEFAULT: {
    				start100();
    				vd01103();
    			}
    			case vd08105: {
    				vd08105();
    			}
    			case exit109: {
    			}
    			}
    			break;
    		}
    		catch (GOTOException e){
    			nextMethod = (GotoLabel) e.getNextMethod();
    		}
    	}
    }
    
    private void start100() {
        if (firstTime.isTrue()) {
            initialize(itdmIO.getDataArea());
            itdmIO.itemcoy.set(deathrec.chdrChdrcoy);
            itdmIO.itempfx.set(smtpfxcpy.item);

            itdmIO.itemtabl.set(T5687);
            itdmIO.itemitem.set(deathrec.crtable);
            itdmIO.itmfrm.set(deathrec.effdate);

            itdmIO.function.set(Varcom.begn);
            itdmioA50();

            if (!isEQ(itdmIO.itemcoy, deathrec.chdrChdrcoy)
                    || !isEQ(itdmIO.itemtabl, T5687)
                    || !isEQ(itdmIO.itemitem, deathrec.crtable)
                    || !isEQ(itdmIO.statuz, Varcom.oK)) {
                syserrrec.statuz.set(E652);
                vpxdtavrec.statuz.set(E652);
                syserrrec.params.set(itdmIO.getParams());
                systemError900();
            } else {
                t5687rec.t5687Rec.set(itdmIO.genarea);
            }
            wsaaDthMth.set(t5687rec.dcmeth);
        }

        vpxdtavrec.dthMth.set(wsaaDthMth);

        if (isNE(vpxdtavrec.dthMth, wsccVd01)) {
            // 103-DC01. Exit return
        	if (isEQ(vpxdtavrec.dthMth, wsccVd08)) {
        		goTo(GotoLabel.vd08105);
        	} else {
        		deathrec.status.set(Varcom.endp);
        		vpxdtavrec.currcode.set(deathrec.currcode);
        		goTo(GotoLabel.exit109);
        	}
        }
    }

    private void vd01103() {
    	if (firstTime.isTrue()) {
            wsaaSumins.set(ZERO);
            readCovr110();
            readTables150();
        }
        if (isEQ(deathrec.status, Varcom.oK)) {
            checkMethods160();
        }
        // GO TO EXIT-109
        goTo(GotoLabel.exit109);
    }
    
    private void vd08105() {
        // 105-DC08
        if (firstTime.isTrue()) {
            wsaaChdrcoy.set(deathrec.chdrChdrcoy);
            wsaaChdrnum.set(deathrec.chdrChdrnum);
            wsaaCoverage.set(deathrec.covrCoverage);
            wsaaRider.set(deathrec.covrRider);
            wsaaLife.set(deathrec.lifeLife);
            wsaaVirtualFund.set(SPACES);
            wsaaUnitType.set(SPACES);
        }

        if (isEQ(deathrec.endf, wsccY)) {
            deathrec.status.set(Varcom.endp);
            vpxdtavrec.currcode.set(deathrec.currcode);
            wsaaSwitch.set(wsccY);
            goTo(GotoLabel.exit109);
        }

        if (isEQ(deathrec.endf, wsccS)) {
            vpxdtavrec.currcode.set(deathrec.currcode);
            wsaaSwitch.set(wsccY);
            wsaaSumins.set(ZERO);
            readCovr110();
            deathrec.element.set(SPACES);

            if (!isEQ(t5687rec.dcmeth, SPACES)
                    && isEQ(deathrec.status, Varcom.oK)) {
                deathrec.actualVal.set(wsaaSumins);
                readDesc170();
                deathrec.fieldType.set(wsccS);
            }
            deathrec.endf.set(wsccY);
            goTo(GotoLabel.exit109);
        }

        if (isEQ(deathrec.endf, wsccI)) {
            intBear350();
        } else {
            unitLinked300();
        }
    }
    
	private void readCovr110() {
    	GotoLabel nextMethod = GotoLabel.DEFAULT;
    	while (true) {
    		try {
    			switch (nextMethod) {
    			case DEFAULT: {
    				start110();
    			}
    			case call110: {
    				call110();
    			}
    			case nextr110: {
    				nextr110();
    			}
    			case exit110: {
    			}
    			}
    			break;
    		}
    		catch (GOTOException e){
    			nextMethod = (GotoLabel) e.getNextMethod();
    		}
    	}
	}
	
	
    private void start110() {

        if (firstTime.isTrue()) {
            initialize(covrIO.getDataArea());
            covrIO.chdrcoy.set(deathrec.chdrChdrcoy);
            covrIO.chdrnum.set(deathrec.chdrChdrnum);
            covrIO.life.set(deathrec.lifeLife);
            covrIO.coverage.set(deathrec.covrCoverage);
            covrIO.rider.set(deathrec.covrRider);
            covrIO.planSuffix.set(0);
            covrIO.function.set(Varcom.begn);
        } else {
            covrIO.function.set(Varcom.nextr);
        }
    }
    private void call110() {
        covrioA00();
        wsaaSwitch.set(wsccN);
        if (!isEQ(covrIO.chdrcoy, deathrec.chdrChdrcoy)
                || !isEQ(covrIO.chdrnum, deathrec.chdrChdrnum)
                || !isEQ(covrIO.life, deathrec.lifeLife)
                || !isEQ(covrIO.coverage, deathrec.covrCoverage)
                || !isEQ(covrIO.rider, deathrec.covrRider)
                || isEQ(covrIO.statuz, Varcom.endp)) {
            covrIO.statuz.set(Varcom.endp);
            deathrec.status.set(Varcom.endp);
            wsaaSwitch.set(wsccY);
            goTo(GotoLabel.exit110);
        }
        wsaaValidStatuz.set(SPACES);
        checkStatcode120();

        if (!isEQ(wsaaValidStatuz, wsccY)) {
            goTo(GotoLabel.nextr110);
        }

        if (isEQ(covrIO.validflag, wscc1)) {
            wsaaSumins.set(covrIO.sumins);
            wsaaPlan.set(covrIO.planSuffix);
            deathrec.element.set(wsaaPlanSuff);
            goTo(GotoLabel.exit110);
        }
    }

    private void nextr110(){
    	covrIO.function.set(Varcom.nextr);
    	goTo(GotoLabel.call110);
    }
    
    private void checkStatcode120(){
        wassBatckey.set(deathrec.batckey);
        itemIO.setDataArea(SPACES);
        itemIO.itempfx.set(smtpfxcpy);
        itemIO.itemcoy.set(deathrec.chdrChdrcoy);
        itemIO.itemtabl.set(T5679);
        itemIO.itemitem.set(wassBatckey.batcBatctrcde);
        itemIO.function.set(Varcom.readr);
        itemioA40();
        t5679rec.t5679Rec.set(itemIO.genarea);
        if (isEQ(covrIO.rider,wscc00)) {
            for (wsaaSub.set(1); ((wsaaSub.toInt() <= 12) && (isNE(wsaaValidStatuz, wsccY)) ); wsaaSub.add(1)) {
            	checkCoverage130();
            }

        } else {
            for (wsaaSub.set(1); ((wsaaSub.toInt() <= 12) && (isNE(wsaaValidStatuz, wsccY)) ); wsaaSub.add(1)) {
                checkRider140();
            }
        }
    }
    
    private void checkCoverage130() {
        if (isEQ(t5679rec.covRiskStat[wsaaSub.toInt()], covrIO.statcode)) {
            for (wsaaSub.set(1); ((wsaaSub.toInt() <= 12) && (isNE(wsaaValidStatuz, wsccY)) ); wsaaSub.add(1)) {
                if (isEQ(t5679rec.covPremStat[wsaaSub.toInt()], covrIO.pstatcode)) {
                    wsaaValidStatuz.set(wsccY);
                }
            }
        }
    }
    
    private void checkRider140() {
        if(isEQ(t5679rec.ridRiskStat[wsaaSub.toInt()],covrIO.statcode)){
            for (wsaaSub.set(1); ((wsaaSub.toInt() <= 12) && (isNE(wsaaValidStatuz, wsccY)) ); wsaaSub.add(1)) {
                if(isEQ(t5679rec.ridPremStat[wsaaSub.toInt()], covrIO.pstatcode)){
                    wsaaValidStatuz.set(wsccY);
                }
            }
        }
    }

    private void readTables150() {

        if (isEQ(covrIO.statuz, Varcom.endp)) {
            return;
        }

        /**** Read T6640 - Traditional Component Details ******/
        itdmIO.setDataArea(SPACES);
        itdmIO.itemcoy.set(deathrec.chdrChdrcoy);
        itdmIO.itemtabl.set(T6640);
        itdmIO.itemitem.set(deathrec.crtable);
        itdmIO.itmfrm.set(deathrec.effdate);
        itdmIO.function.set(Varcom.begn);

        itdmioA50();

        if(!isEQ(itdmIO.itemcoy,deathrec.chdrChdrcoy) ||
               !isEQ(itdmIO.itemtabl,T6640) ||
               !isEQ(itdmIO.itemitem,deathrec.crtable) ||
               isEQ(itdmIO.statuz, Varcom.endp)) {
            t6640rec.t6640Rec.set(SPACES);
        } else {
            t6640rec.t6640Rec.set(itdmIO.genarea);
        }

        if (isEQ(t6640rec.revBonusMeth, SPACES)) {
            t6639rec.t6639Rec.set(SPACES);
            return;
        }

        /**** Read T6639 - Trad Bus Bonus Calc Routines ****/
        wsaaT6639Dcmeth.set(t5687rec.dcmeth);
        wsaaPremStat.set(covrIO.pstatcode);
        itdmIO.setDataArea(SPACES);
        itdmIO.itemcoy.set(deathrec.chdrChdrcoy);
        
        itdmIO.itemtabl.set(T6639);
        itdmIO.itemitem.set(wsaaT6639Key);
        itdmIO.itmfrm.set(deathrec.effdate);
        itdmIO.function.set(Varcom.begn);

        itdmioA50();

        if(!isEQ(itdmIO.itemcoy,deathrec.chdrChdrcoy) ||
                !isEQ(itdmIO.itemtabl,T6639) ||
                !isEQ(itdmIO.itemitem,wsaaT6639Key) ||
                 isEQ(itdmIO.statuz,Varcom.endp)){
	         itdmIO.itemitem.set(wsaaT6639Key);
	         syserrrec.params.set(itdmIO.getParams());
	         syserrrec.statuz.set(G408);
	         dbError999();
         } else {
             t6639rec.t6639Rec.set(itdmIO.genarea);
         }

    }
   
    private void checkMethods160() {
        vpxdtavrec.currcode.set(deathrec.currcode);
        readDesc170();
        if (!isEQ(t5687rec.dcmeth, SPACES)) {
            deathrec.actualVal.set(wsaaSumins);
            deathrec.fieldType.set(wsccS);
            t5687rec.dcmeth.set(SPACES);
            return;
        }
        if (isEQ(t5687rec.dcmeth, SPACES)) {
            if (!isEQ(t6640rec.revBonusMeth, SPACES)
                    && isEQ(wsaaProcessedRvBon, wsccN)) {
                readT5645A20();
                deathrec.fieldType.set(wsccB);
                wsaaDescDescitem.set(wsccREV);
                getDescriptionA10();
                wsaaProcessedRvBon.set(wsccY);
                return;
            }

            if (!isEQ(t6640rec.revBonusMeth, SPACES)
                    && !isEQ(t6639rec.intBonusProg, SPACES)) {
                deathrec.fieldType.set(wsccM);
                wsaaDescDescitem.set(wsccINT);
                getDescriptionA10();
                deathrec.actualVal.set(ZERO);
                t6639rec.intBonusProg.set(SPACES);
                return;
            }

            if (!isEQ(t6640rec.revBonusMeth, SPACES)
                    && !isEQ(t6639rec.trmBonusProg, SPACES)) {
                deathrec.fieldType.set(wsccT);
                wsaaDescDescitem.set(wsccTRM);
                getDescriptionA10();
                deathrec.actualVal.set(ZERO);
                t6639rec.trmBonusProg.set(SPACES);
                return;
            }

            if (!isEQ(t6640rec.revBonusMeth, SPACES)
                    && !isEQ(t6639rec.addBonusProg, SPACES)) {
                deathrec.fieldType.set(wsccX);
                wsaaDescDescitem.set(wsccEXT);
                getDescriptionA10();
                deathrec.actualVal.set(ZERO);
                t6639rec.addBonusProg.set(SPACES);
                return;
            }

            if (!isEQ(t6640rec.revBonusMeth, SPACES)
                    && !isEQ(t6640rec.lcsubr, SPACES)
                    && !isEQ(t6640rec.lcrider, SPACES)) {
                deathrec.fieldType.set(wsccL);
                wsaaDescDescitem.set(wsccLOW);
                getDescriptionA10();
                deathrec.actualVal.set(ZERO);
                return;
            }
            
            wsaaSwitch.set(wsccN);
            readCovr110();
            readTables150();
        }
    }
    
    private void readDesc170() {
        initialize(descIO.getDataArea());
        descIO.descitem.set(deathrec.crtable);
        descIO.desctabl.set(T5687);
        descIO.descpfx.set(smtpfxcpy.item);
        descIO.desccoy.set(deathrec.chdrChdrcoy);
        descIO.language.set(deathrec.language);
        descIO.function.set(Varcom.readr);
        descIO.format.set(DESCREC);
        SmartFileCode.execute(appVars, descIO);
        if (!isEQ(descIO.statuz, Varcom.oK) && !isEQ(descIO.statuz, Varcom.mrnf)) {
            syserrrec.statuz.set(descIO.statuz);
            vpxdtavrec.statuz.set(descIO.statuz);
            syserrrec.params.set(descIO.getParams());
            systemError900();
        }

        if (isEQ(descIO.statuz, Varcom.oK)) {
            if (isEQ(wsaaDthMth, wsccVd01)) {
                deathrec.description.set(descIO.shortdesc);
            } else {
                deathrec.description.set(descIO.longdesc);
            }

        } else {
            deathrec.description.set(SPACES);
        }

    }

    private void returnValue200() {
        vpxdtavrec.resultValue.set(SPACES);
        if (isEQ(vpxdtavrec.resultField, "curduntbal")) {
        	vpxdtavrec.resultValue.set(vpxdtavrec.curduntbal.getbigdata().toString());
        } else if (isEQ(vpxdtavrec.resultField, "unprocessed")) {
        	vpxdtavrec.resultValue.set(vpxdtavrec.unprocessed.getbigdata().toString());
        } else if (isEQ(vpxdtavrec.resultField, "dthMth")) {
            vpxdtavrec.resultValue.set(vpxdtavrec.dthMth);
            LOGGER.debug("vpxdtavrec.dthMth ", vpxdtavrec.dthMth.getData());//IJTI-1561
        } else if (isEQ(vpxdtavrec.resultField, "currcode")) {
            vpxdtavrec.resultValue.set(vpxdtavrec.currcode);
            LOGGER.debug("vpxdtavrec.currcode ", vpxdtavrec.currcode.getData());//IJTI-1561
        } else {
            vpxdtavrec.statuz.set(H832);
        }

    }
 
    private void unitLinked300() {
        initialize(utrssurIO.getDataArea());
        utrssurIO.chdrcoy.set(wsaaChdrcoy);
        utrssurIO.chdrnum.set(wsaaChdrnum);
        utrssurIO.life.set(wsaaLife);
        utrssurIO.coverage.set(wsaaCoverage);
        utrssurIO.rider.set(wsaaRider);
        utrssurIO.unitVirtualFund.set(wsaaVirtualFund);
        utrssurIO.unitType.set(wsaaUnitType);
        utrssurIO.planSuffix.set(0);
        utrssurIO.function.set(Varcom.begn);
        utrssurioA60();
        if (isEQ(utrssurIO.statuz, Varcom.endp)) {
            vpxdtavrec.currcode.set(deathrec.currcode);
            initialize(utrndthIO.getDataArea());
            utrndthIO.unitVirtualFund.set(SPACES);
            utrndthIO.unitType.set(SPACES);
            wsaaFund.set(SPACES);
            readUtrndth310();
            deathrec.endf.set(wsccI);
            wsaaChdrcoy.set(deathrec.chdrChdrcoy);
            wsaaChdrnum.set(deathrec.chdrChdrnum);
            wsaaCoverage.set(deathrec.covrCoverage);
            wsaaRider.set(deathrec.covrRider);
            wsaaLife.set(deathrec.lifeLife);
            wsaaVirtualFund.set(SPACES);
            wsaaSwitch.set(wsccN);
            return;
        }
        processUtrs320();
        wsaaSwitch.set(wsccN);
        if (isEQ(deathrec.endf, wsccI)) {
            wsaaVirtualFund.set(SPACES);
        }
    }
    
    private void readUtrndth310() {
    	GotoLabel nextMethod = GotoLabel.DEFAULT;
    	while (true) {
    		try {
    			switch (nextMethod) {
    			case DEFAULT: {
    				start310();
    			}
    			case call310: {
    				call310();
    			}
    			case exit310: {
    			}
    			}
    			break;
    		}
    		catch (GOTOException e){
    			nextMethod = (GotoLabel) e.getNextMethod();
    		}
    	}   	
    }
    private void start310() {
        // START
        utrndthIO.chdrcoy.set(deathrec.chdrChdrcoy);
        utrndthIO.chdrnum.set(deathrec.chdrChdrnum);
        utrndthIO.life.set(deathrec.lifeLife);
        utrndthIO.coverage.set(deathrec.covrCoverage);
        utrndthIO.rider.set(deathrec.covrRider);
        utrndthIO.planSuffix.set("9999");
        utrndthIO.procSeqNo.set(ZERO);
        utrndthIO.tranno.set(ZERO);
        utrndthIO.function.set(Varcom.begn);
        utrndthIO.format.set(UTRNDTHREC);
    }
    
    private void call310() {
        // CALL 310
        SmartFileCode.execute(appVars, utrndthIO);

        if(!isEQ(utrndthIO.statuz, Varcom.oK) && !isEQ(utrndthIO.statuz, Varcom.endp)){
            syserrrec.statuz.set(utrndthIO.statuz);
            syserrrec.params.set(utrndthIO.getParams());
            dbError999();
        }

        if(!isEQ(utrndthIO.chdrcoy, deathrec.chdrChdrcoy)
                || !isEQ(utrndthIO.chdrnum, deathrec.chdrChdrnum)
                || !isEQ(utrndthIO.life, deathrec.lifeLife)
                || !isEQ(utrndthIO.coverage, deathrec.covrCoverage)
                || !isEQ(utrndthIO.rider,deathrec.covrRider)
                || isEQ(utrndthIO.statuz, Varcom.endp)){
        	 goTo(GotoLabel.exit310);
        }

        if(isEQ(wsaaFund, SPACES)
                || (!isEQ(wsaaFund, SPACES)
                        && isEQ(wsaaFund, utrndthIO.unitVirtualFund))){
            if(isEQ(utrndthIO.svp,ZERO)){
                vpxdtavrec.unprocessed.add(utrndthIO.contractAmount);
            }
        }
        utrndthIO.function.set(Varcom.nextr);
        goTo(GotoLabel.call310);
    }
    
    private void processUtrs320() {
        deathrec.element.set(utrssurIO.unitVirtualFund);
        wsaaFund.set(utrssurIO.unitVirtualFund);
        deathrec.fieldType.set(wsaaType);
        initialize(utrssurIO.getDataArea());
        utrndthIO.unitVirtualFund.set(utrssurIO.unitVirtualFund);
        utrndthIO.unitType.set(utrssurIO.unitType);
        readUtrndth310();
        
        while (isEQ(utrssurIO.statuz, Varcom.oK)
                && isEQ(utrssurIO.unitVirtualFund, deathrec.element)
                && isEQ(wsaaType, deathrec.fieldType) ) {
            //  ADD UTRSSUR-CURRENT-DUNIT-BAL TO VPXDTAV-CURDUNTBAL
            vpxdtavrec.curduntbal.add(utrssurIO.currentDunitBal);
            utrssurIO.function.set(Varcom.nextr);
            utrssurioA60();
        }
        readItem330();
        deathrec.valueType.set(wsccS);
        wsaaVirtualFund.set(utrssurIO.unitVirtualFund);
        wsaaUnitType.set(utrssurIO.unitType);

    }

    private void readItem330() {
        // START
        initialize(descIO.getDataArea());
        descIO.descitem.set(deathrec.element);
        descIO.desctabl.set(T5515);
        descIO.descpfx.set(smtpfxcpy.item);
        descIO.desccoy.set(wsaaChdrcoy);
        descIO.language.set(deathrec.language);
        descIO.function.set(Varcom.readr);
        descIO.format.set(DESCREC);

        SmartFileCode.execute(appVars, descIO);
        if (!isEQ(descIO.statuz, Varcom.oK)
                && !isEQ(descIO.statuz, Varcom.endp)) {
            syserrrec.statuz.set(descIO.statuz);
            vpxdtavrec.statuz.set(descIO.statuz);
            syserrrec.params.set(descIO.getParams());
            systemError900();
        }

        if (isEQ(descIO.statuz, Varcom.oK)) {
            deathrec.description.set(descIO.longdesc);
        } else {
            deathrec.description.set(SPACES);
        }

        initialize(itdmIO.getDataArea());
        itdmIO.itemcoy.set(wsaaChdrcoy);
        itdmIO.itemtabl.set(T5515);
        itdmIO.itemitem.set(deathrec.element);
        itdmIO.itmfrm.set(deathrec.effdate);
        itdmIO.function.set(Varcom.begn);
        itdmioA50();

        if (isEQ(itdmIO.statuz, Varcom.endp)
                || !isEQ(itdmIO.itemcoy, wsaaChdrcoy)
                || !isEQ(itdmIO.itemtabl, T5515)
                || !isEQ(itdmIO.itemitem, deathrec.element)) {
            t5515rec.t5515Rec.set(SPACES);
        } else {
            t5515rec.t5515Rec.set(itdmIO.genarea);
        }
        vpxdtavrec.currcode.set(t5515rec.currcode);
    }

    private void intBear350() {
    	initialize(hitsIO.getDataArea());
        hitsIO.chdrcoy.set(wsaaChdrcoy);
        hitsIO.chdrnum.set(wsaaChdrnum);
        hitsIO.life.set(wsaaLife);
        hitsIO.coverage.set(wsaaCoverage);
        hitsIO.rider.set(wsaaRider);
        hitsIO.zintbfnd.set(wsaaVirtualFund);
        hitsIO.planSuffix.set(0);
        hitsIO.function.set(Varcom.begn);
        hitsioA70();
        if(isEQ(hitsIO.statuz , Varcom.endp) 
        		|| !isEQ(hitsIO.chdrcoy , wsaaChdrcoy)
                || !isEQ(hitsIO.chdrnum , wsaaChdrnum)
                || !isEQ(hitsIO.life , wsaaLife)
                || !isEQ(hitsIO.coverage , wsaaCoverage)
                || !isEQ(hitsIO.rider, wsaaRider)) {
            vpxdtavrec.currcode.set(deathrec.currcode);
            initialize(hitraloIO.getDataArea());
            hitraloIO.zintbfnd.set(SPACES);
            wsaaFund.set(SPACES);
            readHitralo360();
            deathrec.endf.set(wsccS);
            wsaaSwitch.set(wsccN);
            // goTo(GotoLabel.exit350);
            return;
            //
        }
        wsaaVirtualFund.set(hitsIO.zintbfnd);
        processHits370();
        wsaaSwitch.set(wsccN);

    }

    private void readHitralo360() {
    	GotoLabel nextMethod = GotoLabel.DEFAULT;
    	while (true) {
    		try {
    			switch (nextMethod) {
    			case DEFAULT: {
    				start360();
    			}
    			case call360: {
    				call360();
    			}
    			case exit360: {
    			}
    			}
    			break;
    		}
    		catch (GOTOException e){
    			nextMethod = (GotoLabel) e.getNextMethod();
    		}
    	} 
    }
    
    private void start360() {
        hitraloIO.chdrcoy.set(deathrec.chdrChdrcoy);
        hitraloIO.chdrnum.set(deathrec.chdrChdrnum);
        hitraloIO.life.set(deathrec.lifeLife);
        hitraloIO.coverage.set(deathrec.covrCoverage);
        hitraloIO.rider.set(deathrec.covrRider);
        hitraloIO.planSuffix.set(ZERO);
        hitraloIO.procSeqNo.set(ZERO);
        hitraloIO.tranno.set(ZERO);
        hitraloIO.effdate.set(ZERO);
        hitraloIO.function.set(Varcom.begn);
        hitraloIO.format.set(HITRALOREC);
    }
    private void call360() {
        SmartFileCode.execute(appVars, hitraloIO);
        if (!isEQ(hitraloIO.statuz, Varcom.oK)
                && !isEQ(hitraloIO.statuz, Varcom.endp)) {
            syserrrec.statuz.set(hitraloIO.statuz);
            syserrrec.params.set(hitraloIO.getParams());
            dbError999();
        }

        if (!isEQ(hitraloIO.chdrcoy, deathrec.chdrChdrcoy)
                || !isEQ(hitraloIO.chdrnum, deathrec.chdrChdrnum)
                || !isEQ(hitraloIO.life, deathrec.lifeLife)
                || !isEQ(hitraloIO.coverage, deathrec.covrCoverage)
                || !isEQ(hitraloIO.rider, deathrec.covrRider)
                || isEQ(hitraloIO.statuz, Varcom.endp)) {
            goTo(GotoLabel.exit360);
        }

        if (isEQ(wsaaFund, SPACES)
                || (!isEQ(wsaaFund, SPACES) && isEQ(wsaaFund,
                        hitraloIO.zintbfnd))) {
            if (isEQ(hitraloIO.svp, ZERO)) {
                vpxdtavrec.unprocessed.add(hitraloIO.contractAmount);
            }
        }
        hitraloIO.function.set(Varcom.nextr);
        goTo(GotoLabel.call360);
    }
    
    private void processHits370() {
        deathrec.element.set(hitsIO.zintbfnd);
        wsaaFund.set(hitsIO.zintbfnd);
        initialize(hitraloIO.getDataArea());
        hitraloIO.zintbfnd.set(hitsIO.zintbfnd);
        readHitralo360();
        while (isEQ(hitsIO.statuz, Varcom.oK) && isEQ(hitsIO.statuz, deathrec.element)) {
            vpxdtavrec.curduntbal.set(hitsIO.zcurprmbal);
        	hitsIO.function.set(Varcom.nextr);
            hitsioA70();
        }

        readItem330();
        deathrec.fieldType.set(wsccD);
        deathrec.valueType.set(wsccS);
    }
    

    private void covrioA00() {
        covrIO.format.set(COVRREC);
        SmartFileCode.execute(appVars, covrIO);
        if(!isEQ(covrIO.statuz,Varcom.oK)
                && !isEQ(covrIO.statuz , Varcom.endp)){
	        syserrrec.params.set(covrIO.getParams());
	        syserrrec.statuz.set(covrIO.statuz);
	        dbError999();

        }

    }

	private void getDescriptionA10 () {
        descIO.desctabl.set(T6644);
        descIO.descitem.set(wsaaDescDescitem);
        descIO.descpfx.set(smtpfxcpy.item);
        descIO.desccoy.set(deathrec.chdrChdrcoy);
        descIO.language.set(deathrec.language);
        descIO.function.set(Varcom.readr);
        descIO.format.set(DESCREC);
        SmartFileCode.execute(appVars, descIO);

        if(!isEQ(descIO.statuz , Varcom.oK)
                && !isEQ(descIO.statuz, Varcom.mrnf)) {
            syserrrec.params.set(descIO.getParams());
            dbError999();
        }

        if (isEQ(descIO.statuz, Varcom.oK)) {
            deathrec.description.set(descIO.shortdesc);
        } else {
            deathrec.description.set("?");
        }
    }
	
    private void readT5645A20() {
        itemIO.setDataArea(SPACES);
        itemIO.itempfx.set(smtpfxcpy.item);
        itemIO.itemcoy.set(deathrec.chdrChdrcoy);
        itemIO.itemtabl.set(T5645);
        itemIO.itemitem.set(wsaaSubr);
        itemIO.function.set(Varcom.readr);
        
        itemioA40();
        
        t5645rec.t5645Rec.set(itemIO.genarea);
        
        acblIO.setParams(SPACES);
        wsaaRldgChdrnum.set(deathrec.chdrChdrnum);
        wsaaRldgLife.set(deathrec.lifeLife);
        wsaaRldgCoverage.set(deathrec.covrCoverage);
        wsaaRldgRider.set(deathrec.covrRider);
        wsaaPlan.set(covrIO.planSuffix);
        wsaaRldgPlanSuffix.set(wsaaPlanSuff);
        acblIO.rldgacct.set(wsaaRldgacct);
        acblIO.rldgcoy.set(deathrec.chdrChdrcoy);
        getContractInfoA30();
        acblIO.origcurr.set(chdrlifIO.cntcurr);
        acblIO.sacscode.set(t5645rec.sacscode01);
        acblIO.sacstyp.set(t5645rec.sacstype01);
        acblIO.function.set(Varcom.readr);
        acblIO.format.set(ACBLREC);

        SmartFileCode.execute(appVars, acblIO);
        if(!isEQ(acblIO.statuz , Varcom.oK)
				&& !isEQ(acblIO.statuz, Varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.statuz);
			dbError999();

		}

		if (isEQ(acblIO.statuz, Varcom.mrnf)) {
			deathrec.actualVal.set(ZERO);
		} else {
			deathrec.actualVal.set(acblIO.sacscurbal);
        }
    }

    private void getContractInfoA30() {
    	chdrlifIO.setDataArea(SPACES);
    	chdrlifIO.chdrcoy.set(deathrec.chdrChdrcoy);
    	chdrlifIO.chdrnum.set(deathrec.chdrChdrnum);
    	chdrlifIO.function.set(Varcom.readr);
    	chdrlifIO.format.set(CHDRLIFREC);
    	SmartFileCode.execute(appVars, chdrlifIO);
    	if(!isEQ(chdrlifIO.statuz , Varcom.oK)){
	    	syserrrec.params.set(chdrlifIO.getParams());
	    	syserrrec.statuz.set(chdrlifIO.statuz);
	    	dbError999();	
	    }
    }

	private void itemioA40() {
		itemIO.format.set(ITEMREC);
		SmartFileCode.execute(appVars, itemIO);
		if (!isEQ(itemIO.statuz, Varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			dbError999();
		}
	}

	private void itdmioA50() {
		SmartFileCode.execute(appVars, itdmIO);
		if (!isEQ(itdmIO.statuz, Varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError999();
		}
	}

    private void utrssurioA60() {
    	utrssurIO.format.set(UTRSSURREC);
    	
    	SmartFileCode.execute(appVars, utrssurIO);
    	
		if (!isEQ(utrssurIO.statuz, Varcom.oK)
				&& !isEQ(utrssurIO.statuz, Varcom.endp)) {
			syserrrec.params.set(utrssurIO.getParams());
			dbError999();
		}

    	if(isEQ(utrssurIO.statuz , Varcom.endp)
    			|| !isEQ(utrssurIO.chdrcoy , wsaaChdrcoy)
    			|| !isEQ(utrssurIO.chdrnum , wsaaChdrnum)
    			|| !isEQ(utrssurIO.life , wsaaLife)
    			|| !isEQ(utrssurIO.coverage , wsaaCoverage)
				|| !isEQ(utrssurIO.rider, wsaaRider)) {
			utrssurIO.statuz.set(Varcom.endp);
			deathrec.endf.set(wsccI);
			return;
		}

		if (isEQ(utrssurIO.unitType, wsccI)) {
			wsaaType.set(wsccI);
		} else {
    		wsaaType.set(wsccA);
    	}
    }
    
    private void hitsioA70() {
    	hitsIO.format.set(HITSREC);
    	
    	SmartFileCode.execute(appVars, hitsIO);
    	
    	if(!isEQ(hitsIO.statuz , Varcom.oK)
				&& !isEQ(hitsIO.statuz, Varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			dbError999();
		}

    	if(isEQ(hitsIO.statuz , Varcom.endp)
    			|| !isEQ(hitsIO.chdrcoy , wsaaChdrcoy)
    			|| !isEQ(hitsIO.chdrnum , wsaaChdrnum)
    			|| !isEQ(hitsIO.life , wsaaLife)
    			|| !isEQ(hitsIO.coverage , wsaaCoverage)
				|| !isEQ(hitsIO.rider, wsaaRider)) {
			hitsIO.statuz.set(Varcom.endp);
			deathrec.endf.set(wsccS);
		}
    }

	private void systemError900() {
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set(wscc2);
		callProgram(Syserr.class, syserrrec.syserrRec);
		goTo(GotoLabel.exit000);
	}
	
    protected void dbError999() {
        syserrrec.syserrType.set("1");
        callProgram(Syserr.class, syserrrec.syserrRec);
        goTo(GotoLabel.exit000);
    }
}
