/*
 * File: Br648.java
 * Date: 29 August 2009 22:32:27
 * Author: Quipoz Limited
 *
 * Class transformed from BR648.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.fsu.clients.tablestructures.Tr393rec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.life.productdefinition.dataaccess.dao.Br64xDAO;
import com.csc.life.productdefinition.dataaccess.dao.MedipfDAO;
import com.csc.life.productdefinition.dataaccess.model.Br64xDTO;
import com.csc.life.productdefinition.dataaccess.model.Medipf;
import com.csc.life.productdefinition.dataaccess.model.MedxTemppf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.procedures.Contot;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*  Medical Bill Reversal - Medical Provider
*  ----------------------------------------
*
*    This batch program is to extract Medical bill records
*    reversed paid by medical provider.
*
*    Read from MEDXPF.
*    Program to extract MEDILNB records and PAIDBY = 'D' and
*    medical fees amount < 0 into sort file, sort by ZEXCODE &
*    CHDRNUM.
*    Update CHDR with increment 1 for TRANNO.
*    Write PTRN transaction history records.
*    Call LIFACMV for accounting posting.  Use T5645 accounting
*    rule set-up. Line 01 for LP ME. & line 02 for LP MP.
*    Update MEDILNB set ACTIND = '4', Set Payment Date to
*    processing date & TRANNO.
*
*   CONTROL TOTALS:
*     01  -  No of bills read.
*     02  -  No of payments made.
*
***********************************************************************
* </pre>
*/
public class Br648 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR648");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).setUnsigned();
	private FixedLengthStringData wsaaAllocateNo = new FixedLengthStringData(1).init("Y");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private T5645rec t5645rec = new T5645rec();
	private Tr393rec tr393rec = new Tr393rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	
	/*Batch Upgrade Variables*/
	private static final String h134 = "H134";
	private P6671par p6671par = new P6671par();
	private int intBatchID;
	private int intBatchExtractSize;	
	private int intBatchStep;
	private String strEffDate;	
	private String tranDesc;
	private String chdrTranno;
	private int ctrCT01; 
	private int ctrCT02;
	private boolean noRecordFound;
	
	/*PF Used*/
	private Chdrpf chdrpf;
	private Ptrnpf ptrnpf;
	private Medipf medipf = new Medipf();
	private MedxTemppf medxTemppf = new MedxTemppf();
	private Br64xDTO br64xDTO = new Br64xDTO();
	
	private List<MedxTemppf> medxList;
	private List<Chdrpf> chdrBulkUpdtList;
	private List<Ptrnpf> ptrnBulkInsList;
	private List<Medipf> mediBulkUpdList;
	private Map<String, List<Itempf>> t5645ListMap;
	private Map<String, List<Itempf>> tr393ListMap;
	private List<Descpf> t1688List;
	private Iterator<MedxTemppf> iteratorList;
	
	/*DAO Used*/
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Br64xDAO br64xDAO =  getApplicationContext().getBean("br64xDAO", Br64xDAO.class);
	private ChdrpfDAO chdrpfDAO =  getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private MedipfDAO medipfDAO = getApplicationContext().getBean("medipfDAO", MedipfDAO.class);	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Br648.class);	

	public Br648() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000(){
	p6671par.parmRecord.set(bupaIO.getParmarea());
	strEffDate = bsscIO.getEffectiveDate().toString();	
	intBatchID = 0;
	intBatchStep = 0;
	noRecordFound=false;
	medxList = new ArrayList<MedxTemppf>();
	
	if (bprdIO.systemParam01.isNumeric())
		if (bprdIO.systemParam01.toInt() > 0)
			intBatchExtractSize = bprdIO.systemParam01.toInt();
		else
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
	else
		intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();		
	
	StringBuilder medxTempTableName = new StringBuilder("");
	medxTempTableName.append("MEDX");
	medxTempTableName.append(bprdIO.getSystemParam04().toString().trim().toUpperCase());
	medxTempTableName.append(String.format("%0"+ (4 - bsscIO.getScheduleNumber().toString().trim().length())+"d%s",0 , bsscIO.getScheduleNumber().toString().trim()));
	
	int extractRows = br64xDAO.populateBr64xTemp(intBatchExtractSize, medxTempTableName.toString(), p6671par.chdrnum.toString(), p6671par.chdrnum1.toString(),"D");
	if (extractRows > 0) {
		performDataChunk();	
		
		if (!medxList.isEmpty()){
			initialise1010();
			readSmartTables(bsprIO.getCompany().toString().trim());
			readT5645();
			readTr393();
			readT1688Desc();
		}			
	}
	else{
		LOGGER.info("No Medipay records retrieved for processing.");
		noRecordFound=true;
		return;
	}	
}

private void readSmartTables(String company){
	t5645ListMap = itemDAO.loadSmartTable("IT", company, "T5645");
	tr393ListMap = itemDAO.loadSmartTable("IT", company, "TR393");
	t1688List = descDAO.getItems(smtpfxcpy.item.toString(), bsprIO.getCompany().toString(), "T1688");
}

protected void readT1688Desc(){
	boolean itemFound = false;
	for (Descpf descItem : t1688List) {
		if (descItem.getDescitem().trim().equals(bprdIO.getAuthCode().toString().trim())){
			tranDesc = descItem.getLongdesc();
			itemFound=true;
			break;
		}
	}
	if (!itemFound) {
		fatalError600();	
	}	
}

protected void readT5645(){
	String keyItemitem = wsaaProg.toString().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5645ListMap.containsKey(keyItemitem)){	
		itempfList = t5645ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(t5645rec.t5645Rec);
		syserrrec.statuz.set(h134);
		fatalError600();		
	}	
}

protected void readTr393(){
	String keyItemitem = bsprIO.getFsuco().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (tr393ListMap.containsKey(keyItemitem)){	
		itempfList = tr393ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					tr393rec.tr393Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				tr393rec.tr393Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(tr393rec.tr393Rec);
		fatalError600();		
	}	
}

protected void performDataChunk(){
	intBatchID = intBatchStep;		
	if (medxList.size() > 0) medxList.clear();
	medxList = br64xDAO.loadDataByBatch(intBatchID);
	if (medxList.isEmpty()) wsspEdterror.set(varcom.endp);
	else iteratorList = medxList.iterator();		
}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		wsaaBatckey.batcKey.set(batcdorrec.batchkey);
		wsaaAllocateNo.set("Y");
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		
		chdrBulkUpdtList = new ArrayList<Chdrpf>();	
		ptrnBulkInsList = new ArrayList<Ptrnpf>();
		mediBulkUpdList = new ArrayList<Medipf>(); 
		t5645ListMap =  new HashMap<String, List<Itempf>>();
		tr393ListMap = new HashMap<String, List<Itempf>>();
		t1688List = new ArrayList<Descpf>();			
		
		ctrCT01=0;
		ctrCT02=0;			

	}

protected void readFile2000(){
	if (!medxList.isEmpty()){
		if (!iteratorList.hasNext()) {
			intBatchStep++;	
			performDataChunk();
			if (!medxList.isEmpty()){
				medxTemppf = new MedxTemppf();
				medxTemppf = iteratorList.next();
				br64xDTO = medxTemppf.getBr64xDTO();			
			}
		}else {		
			medxTemppf = new MedxTemppf();
			medxTemppf = iteratorList.next();
			br64xDTO = medxTemppf.getBr64xDTO();	
		}	
	}else wsspEdterror.set(varcom.endp);
}

protected void edit2500(){
	ctrCT01++;
}

protected void commitControlTotals(){
	//ct01
	contotrec.totno.set(ct01);
	contotrec.totval.set(ctrCT01);
	callProgram(Contot.class, contotrec.contotRec);	
	ctrCT01 = 0;
	
	//ct02
	contotrec.totno.set(ct02);
	contotrec.totval.set(ctrCT02);
	callProgram(Contot.class, contotrec.contotRec);		
	ctrCT02 = 0;	
}

protected void update3000()	{
	updateChdr();
	writePtrnRecord();
	updateAcmv3620();
	updateMediRecord();
}

protected void updateChdr(){
	chdrpf = new Chdrpf();
	chdrpf.setUniqueNumber(br64xDTO.getChdrUniqueNo());
	chdrpf.setTranno(br64xDTO.getChdrtranno() + 1);
	chdrTranno = Long.toString(br64xDTO.getChdrtranno() + 1);
	chdrBulkUpdtList.add(chdrpf);		
}

protected void commitChdrBulkUpdate()
{	 
	boolean isUpdateChdr = chdrpfDAO.updateChdrTrannoByUniqueNo(chdrBulkUpdtList);
	if (!isUpdateChdr) {
		LOGGER.error("Update CHDR record failed.");
		fatalError600();
	}else chdrBulkUpdtList.clear();
}

protected void writePtrnRecord(){

	ptrnpf = new Ptrnpf();
	ptrnpf.setChdrpfx(br64xDTO.getChdrpfx());
	ptrnpf.setChdrcoy(medxTemppf.getChdrcoy());
	ptrnpf.setChdrnum(medxTemppf.getChdrnum());
	ptrnpf.setTranno(Integer.parseInt(chdrTranno));
	ptrnpf.setTrdt(Integer.parseInt(getCobolDate()));
	ptrnpf.setTrtm(varcom.vrcmTime.toInt());
	ptrnpf.setTermid(varcom.vrcmTermid.toString());
	ptrnpf.setUserT(0);
	ptrnpf.setPtrneff(bsscIO.getEffectiveDate().toInt());
	ptrnpf.setDatesub(bsscIO.getEffectiveDate().toInt());	
	ptrnpf.setValidflag("1");	
	ptrnpf.setBatccoy(batcdorrec.company.toString());
	ptrnpf.setBatcpfx(batcdorrec.prefix.toString());
	ptrnpf.setBatcbrn(batcdorrec.branch.toString());
	ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
	ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
	ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
	ptrnpf.setBatcbatch(batcdorrec.batch.toString());
	ptrnBulkInsList.add(ptrnpf);
}

protected void commitPtrnBulkInsert(){	 
	 boolean isInsertPtrnPF = ptrnpfDAO.insertPtrnPF(ptrnBulkInsList);	
	if (!isInsertPtrnPF) {
		LOGGER.error("Insert PtrnPF record failed.");
		fatalError600();
	}else ptrnBulkInsList.clear();
}

protected void updateAcmv3620()
	{
		start3620();
	}

protected void start3620()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(medxTemppf.getChdrnum());
		lifacmvrec.rldgacct.set(medxTemppf.getChdrnum());
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		b2000CallLifacmv();
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(medxTemppf.getChdrnum());
		lifacmvrec.rldgacct.set(medxTemppf.getChdrnum());
		lifacmvrec.sacscode.set(t5645rec.sacscode02);
		lifacmvrec.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec.glcode.set(t5645rec.glmap02);
		lifacmvrec.glsign.set(t5645rec.sign02);
		lifacmvrec.contot.set(t5645rec.cnttot02);
		b2000CallLifacmv();
	}

protected void updateMediRecord(){	
	medipf = new Medipf();
	//readh portion of the old code
	medipf.setChdrcoy(medxTemppf.getChdrcoy());
	medipf.setChdrnum(medxTemppf.getChdrnum());
	medipf.setLife(medxTemppf.getLife());
	medipf.setJlife(medxTemppf.getJlife());
	medipf.setSeqno(medxTemppf.getSeqno());
	medipf.setZmedtyp(medxTemppf.getZmedtyp());
	medipf.setExmcode(medxTemppf.getExmcode());
	medipf.setZmedfee(medxTemppf.getZmedfee());
	medipf.setPaidby(medxTemppf.getPaidby());
	medipf.setInvref(medxTemppf.getInvref());
	medipf.setEffdate(medxTemppf.getEffdate());
	//end readh	
	medipf.setActind("4");
	medipf.setPaydte(bsscIO.getEffectiveDate().toInt());
	medipf.setTranno(Integer.parseInt(chdrTranno));
	medipf.setUniqueNumber(br64xDTO.getMediUniqueNo());
	mediBulkUpdList.add(medipf);
	ctrCT02++;
}

protected void commitMediUpdate(){
	boolean isUpdateMedipf;
	isUpdateMedipf = medipfDAO.updateMedipf(mediBulkUpdList);
	if (!isUpdateMedipf) {
		LOGGER.error("Update Medipf record failed.");
		fatalError600();
	}else mediBulkUpdList.clear();	
}

protected void commit3500(){
	if (!noRecordFound){
		commitControlTotals();
		commitChdrBulkUpdate();
		commitPtrnBulkInsert();
		commitMediUpdate();		
	}
}

protected void rollback3600(){
	/*ROLLBACK*/
	/** Place any additional rollback processing in here.*/
	/*EXIT*/
}

protected void close4000(){
	if (!medxList.isEmpty()){
		chdrBulkUpdtList.clear();
		chdrBulkUpdtList = null;
		
		ptrnBulkInsList.clear();
		ptrnBulkInsList = null;
		
		mediBulkUpdList.clear();
		mediBulkUpdList=null;
		
		t5645ListMap.clear();
		t5645ListMap = null;
		
		tr393ListMap.clear();
		tr393ListMap = null;
		
		t1688List.clear();
		t1688List=null;
		
		lsaaStatuz.set(varcom.oK);
	}
}

protected void b2000CallLifacmv()
	{
		b2000Begin();
	}

protected void b2000Begin()
	{
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(batcdorrec.batchkey);
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.rldgcoy.set(medxTemppf.getChdrcoy());
		lifacmvrec.genlcoy.set(medxTemppf.getChdrcoy());
		lifacmvrec.origcurr.set(br64xDTO.getCntcurr());
		lifacmvrec.origamt.set(medxTemppf.getZmedfee());
		lifacmvrec.tranref.set(medxTemppf.getChdrnum());
		lifacmvrec.trandesc.set(tranDesc);
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.transactionDate.set(getCobolDate());
		lifacmvrec.transactionTime.set(getCobolTime());
		lifacmvrec.substituteCode[1].set(br64xDTO.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError600();
		}
	}
}
