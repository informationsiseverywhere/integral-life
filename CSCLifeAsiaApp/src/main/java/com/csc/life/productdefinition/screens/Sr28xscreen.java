package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class Sr28xscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 12, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr28xScreenVars sv = (Sr28xScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr28xscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr28xScreenVars screenVars = (Sr28xScreenVars)pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.dataextract01.clear();
		screenVars.dataextract02.clear();
		screenVars.dataextract03.clear();
		screenVars.dataextract04.clear();
		screenVars.dataextract05.clear();
		screenVars.dataextract06.clear();
		screenVars.dataextract07.clear();
		screenVars.dataextract08.clear();
		screenVars.dataextract09.clear();
		
		screenVars.func01.clear();
		screenVars.func02.clear();
		screenVars.func03.clear();
		screenVars.func04.clear();
		screenVars.func05.clear();
		screenVars.func06.clear();
		screenVars.func07.clear();
		screenVars.func08.clear();
		screenVars.func09.clear();

		screenVars.dataupdate01.clear();
		screenVars.dataupdate02.clear();
		screenVars.dataupdate03.clear();
		screenVars.dataupdate04.clear();
		screenVars.dataupdate05.clear();

		screenVars.vpmmodel.clear();
		screenVars.linkstatuz.clear();
		screenVars.linkcopybk.clear();
		screenVars.vpmsubr.clear();

		screenVars.extfldid01.clear();
		screenVars.extfldid02.clear();
		screenVars.extfldid03.clear();
		screenVars.extfldid04.clear();
		screenVars.extfldid05.clear();
		screenVars.extfldid06.clear();
		screenVars.extfldid07.clear();
		screenVars.extfldid08.clear();
		screenVars.extfldid09.clear();
		screenVars.extfldid10.clear();
		screenVars.extfldid11.clear();
		screenVars.extfldid12.clear();
		screenVars.extfldid13.clear();
		screenVars.extfldid14.clear();
		screenVars.extfldid15.clear();
		screenVars.extfldid16.clear();
		screenVars.extfldid17.clear();
		screenVars.extfldid18.clear();
		screenVars.extfldid19.clear();
		screenVars.extfldid20.clear();
		screenVars.extfldid21.clear();
		screenVars.extfldid22.clear();
		screenVars.extfldid23.clear();
		screenVars.extfldid24.clear();
		screenVars.extfldid25.clear();

		screenVars.dataLog.clear();
		
	}

/**
 * Clear all the variables in S5675screen
 */
	public static void clear(VarModel pv) {
		Sr28xScreenVars screenVars = (Sr28xScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.dataextract01.clear();
		screenVars.dataextract02.clear();
		screenVars.dataextract03.clear();
		screenVars.dataextract04.clear();
		screenVars.dataextract05.clear();
		screenVars.dataextract06.clear();
		screenVars.dataextract07.clear();
		screenVars.dataextract08.clear();
		screenVars.dataextract09.clear();
		
		screenVars.func01.clear();
		screenVars.func02.clear();
		screenVars.func03.clear();
		screenVars.func04.clear();
		screenVars.func05.clear();
		screenVars.func06.clear();
		screenVars.func07.clear();
		screenVars.func08.clear();
		screenVars.func09.clear();

		screenVars.dataupdate01.clear();
		screenVars.dataupdate02.clear();
		screenVars.dataupdate03.clear();
		screenVars.dataupdate04.clear();
		screenVars.dataupdate05.clear();

		screenVars.vpmmodel.clear();
		screenVars.linkstatuz.clear();
		screenVars.linkcopybk.clear();
		screenVars.vpmsubr.clear();
		
		screenVars.extfldid01.clear();
		screenVars.extfldid02.clear();
		screenVars.extfldid03.clear();
		screenVars.extfldid04.clear();
		screenVars.extfldid05.clear();
		screenVars.extfldid06.clear();
		screenVars.extfldid07.clear();
		screenVars.extfldid08.clear();
		screenVars.extfldid09.clear();
		screenVars.extfldid10.clear();
		screenVars.extfldid11.clear();
		screenVars.extfldid12.clear();
		screenVars.extfldid13.clear();
		screenVars.extfldid14.clear();
		screenVars.extfldid15.clear();
		screenVars.extfldid16.clear();
		screenVars.extfldid17.clear();
		screenVars.extfldid18.clear();
		screenVars.extfldid19.clear();
		screenVars.extfldid20.clear();
		screenVars.extfldid21.clear();
		screenVars.extfldid22.clear();
		screenVars.extfldid23.clear();
		screenVars.extfldid24.clear();
		screenVars.extfldid25.clear();	
		
		screenVars.dataLog.clear();
	}
}