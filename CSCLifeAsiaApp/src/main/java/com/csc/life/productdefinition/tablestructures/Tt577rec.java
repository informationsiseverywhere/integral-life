package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:14
 * Description:
 * Copybook name: TT577REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tt577rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tt577Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData subprog = new FixedLengthStringData(10).isAPartOf(tt577Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(490).isAPartOf(tt577Rec, 10, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tt577Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tt577Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}