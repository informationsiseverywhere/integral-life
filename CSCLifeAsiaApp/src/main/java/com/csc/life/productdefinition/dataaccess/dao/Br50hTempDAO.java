package com.csc.life.productdefinition.dataaccess.dao;

import java.util.List;

import com.csc.life.productdefinition.dataaccess.model.Br50hDTO;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;


public interface Br50hTempDAO extends BaseDAO<Br50hDTO> {

	public  int getNTURecords(String company, int batchExtractSize);
	public List<Br50hDTO> getUwlmtchlClrrChdrRecords(String chdrnum, String clntpfx, String clntcoy, String clrrole, String clntnum);
	public List<Br50hDTO> getUwlmtchlCovtCovrRecords(String chdrcoy, String chdrnum, String life);//ILB-1114
	public List<Br50hDTO> loadDataByBatch(int batchID);
	public List<Br50hDTO> loadDataByBatch(int batchID, List<String> list);
	public int checkstautsfailedByBatch(List<String> list);
 
	 
	
}
