package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:52
 * Description:
 * Copybook name: TR28YREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr28yrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr28yRec = new FixedLengthStringData(500);
  	public FixedLengthStringData vpmsprop = new FixedLengthStringData(60).isAPartOf(tr28yRec, 0);
  	public FixedLengthStringData maptofield = new FixedLengthStringData(35).isAPartOf(tr28yRec, 60);
  	public FixedLengthStringData copybk = new FixedLengthStringData(12).isAPartOf(tr28yRec, 95);
  	public FixedLengthStringData numoccurs = new FixedLengthStringData(3).isAPartOf(tr28yRec, 107);
  	public FixedLengthStringData attrslbls = new FixedLengthStringData(240).isAPartOf(tr28yRec, 110);
  	public FixedLengthStringData[] attrslbl = FLSArrayPartOfStructure(4, 60, attrslbls, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(240).isAPartOf(attrslbls, 0, FILLER_REDEFINE);
    public FixedLengthStringData attrslbl01 = new FixedLengthStringData(60).isAPartOf(filler, 0);   
 	public FixedLengthStringData attrslbl02 = new FixedLengthStringData(60).isAPartOf(filler,  60); 
 	public FixedLengthStringData attrslbl03 = new FixedLengthStringData(60).isAPartOf(filler,  120); 
 	public FixedLengthStringData attrslbl04 = new FixedLengthStringData(60).isAPartOf(filler,  180); 
  	public FixedLengthStringData attrsvalues = new FixedLengthStringData(32).isAPartOf(tr28yRec, 350);
  	public FixedLengthStringData[] attrsvalue = FLSArrayPartOfStructure(4, 8, attrsvalues, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(32).isAPartOf(attrsvalues, 0, FILLER_REDEFINE);
  	public FixedLengthStringData attrsvalue01 = new FixedLengthStringData(8).isAPartOf(filler1, 0);   
  	public FixedLengthStringData attrsvalue02 = new FixedLengthStringData(8).isAPartOf(filler1, 8);   
  	public FixedLengthStringData attrsvalue03 = new FixedLengthStringData(8).isAPartOf(filler1, 16);  
  	public FixedLengthStringData attrsvalue04 = new FixedLengthStringData(8).isAPartOf(filler1, 24);  
  	public FixedLengthStringData filler2 = new FixedLengthStringData(118).isAPartOf(tr28yRec, 382, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr28yRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			tr28yRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}