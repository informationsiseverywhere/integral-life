package com.csc.life.productdefinition.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.LfevpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lfevpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class LfevpfDAOImpl extends BaseDAOImpl<Lfevpf> implements LfevpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(LfevpfDAOImpl.class);

    public void insertLfevpfRecord(List<Lfevpf> lfevpfList) {
        if (lfevpfList != null) {
            String SQL_LFEV_INSERT = "INSERT INTO LFEVPF(CHDRCOY,CHDRNUM,LIFE,EFFDATE,ALNAME01,ALNAME02,ALNAME03,ALNAME04,ALNAME05,JLNAME01,JLNAME02,JLNAME03,JLNAME04,JLNAME05,SMOKEIND01,SMOKEIND02,SMOKEIND03,SMOKEIND04,SMOKEIND05,SMKRQD01,SMKRQD02,SMKRQD03,SMKRQD04,SMKRQD05,ANBCCD01,ANBCCD02,ANBCCD03,ANBCCD04,ANBCCD05,JANBCCD01,JANBCCD02,JANBCCD03,JANBCCD04,JANBCCD05,ANBISD01,ANBISD02,ANBISD03,ANBISD04,ANBISD05,JANBISD01,JANBISD02,JANBISD03,JANBISD04,JANBISD05,SEX01,SEX02,SEX03,SEX04,SEX05,JLSEX01,JLSEX02,JLSEX03,JLSEX04,JLSEX05,CURRFROM,VALIDFLAG,USRPRF,JOBNM,DATIME) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement psLfevInsert = getPrepareStatement(SQL_LFEV_INSERT);
            try {
                for (Lfevpf l : lfevpfList) {
                    psLfevInsert.setString(1, l.getChdrcoy());
                    psLfevInsert.setString(2, l.getChdrnum());
                    psLfevInsert.setString(3, l.getLife());
                    psLfevInsert.setInt(4, l.getEffdate());
                    psLfevInsert.setString(5, l.getAlname01());
                    psLfevInsert.setString(6, l.getAlname02());
                    psLfevInsert.setString(7, l.getAlname03());
                    psLfevInsert.setString(8, l.getAlname04());
                    psLfevInsert.setString(9, l.getAlname05());
                    psLfevInsert.setString(10, l.getJlname01());
                    psLfevInsert.setString(11, l.getJlname02());
                    psLfevInsert.setString(12, l.getJlname03());
                    psLfevInsert.setString(13, l.getJlname04());
                    psLfevInsert.setString(14, l.getJlname05());
                    psLfevInsert.setString(15, validSubStr(l.getSmokeind01(), 0, 1));
                    psLfevInsert.setString(16, validSubStr(l.getSmokeind02(), 0, 1));
                    psLfevInsert.setString(17, validSubStr(l.getSmokeind03(), 0, 1));
                    psLfevInsert.setString(18, validSubStr(l.getSmokeind04(), 0, 1));
                    psLfevInsert.setString(19, validSubStr(l.getSmokeind05(), 0, 1));
                    psLfevInsert.setString(20, l.getSmkrqd01());
                    psLfevInsert.setString(21, l.getSmkrqd02());
                    psLfevInsert.setString(22, l.getSmkrqd03());
                    psLfevInsert.setString(23, l.getSmkrqd04());
                    psLfevInsert.setString(24, l.getSmkrqd05());
                    psLfevInsert.setInt(25, l.getAnbAtCcd01());
                    psLfevInsert.setInt(26, l.getAnbAtCcd02());
                    psLfevInsert.setInt(27, l.getAnbAtCcd03());
                    psLfevInsert.setInt(28, l.getAnbAtCcd04());
                    psLfevInsert.setInt(29, l.getAnbAtCcd05());
                    psLfevInsert.setInt(30, l.getJanbccd01());
                    psLfevInsert.setInt(31, l.getJanbccd02());
                    psLfevInsert.setInt(32, l.getJanbccd03());
                    psLfevInsert.setInt(33, l.getJanbccd04());
                    psLfevInsert.setInt(34, l.getJanbccd05());
                    psLfevInsert.setInt(35, l.getAnbisd01());
                    psLfevInsert.setInt(36, l.getAnbisd02());
                    psLfevInsert.setInt(37, l.getAnbisd03());
                    psLfevInsert.setInt(38, l.getAnbisd04());
                    psLfevInsert.setInt(39, l.getAnbisd05());
                    psLfevInsert.setInt(40, l.getJanbisd01());
                    psLfevInsert.setInt(41, l.getJanbisd02());
                    psLfevInsert.setInt(42, l.getJanbisd03());
                    psLfevInsert.setInt(43, l.getJanbisd04());
                    psLfevInsert.setInt(44, l.getJanbisd05());
                    psLfevInsert.setString(45, l.getSex01());
                    psLfevInsert.setString(46, l.getSex02());
                    psLfevInsert.setString(47, l.getSex03());
                    psLfevInsert.setString(48, l.getSex04());
                    psLfevInsert.setString(49, l.getSex05());
                    psLfevInsert.setString(50, l.getJlsex01());
                    psLfevInsert.setString(51, l.getJlsex02());
                    psLfevInsert.setString(52, l.getJlsex03());
                    psLfevInsert.setString(53, l.getJlsex04());
                    psLfevInsert.setString(54, l.getJlsex05());
                    psLfevInsert.setInt(55, l.getCurrfrom());
                    psLfevInsert.setString(56, l.getValidflag());
                    psLfevInsert.setString(57, getUsrprf());
                    psLfevInsert.setString(58, getJobnm());
                    psLfevInsert.setTimestamp(59, new Timestamp(System.currentTimeMillis()));
                    psLfevInsert.addBatch();
                }
                psLfevInsert.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("insertLfevpfRecord()", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(psLfevInsert, null);
            }
        }
    }
    
    private String validSubStr(String s, int startIx, int endIx) {
        if (s != null && s.length() >= endIx) {
            return s.substring(startIx, endIx);
        }
        return null;
    }
}