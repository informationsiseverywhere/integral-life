package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:44
 * Description:
 * Copybook name: TH623REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th623rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th623Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData noofinst = new ZonedDecimalData(3, 0).isAPartOf(th623Rec, 0);
  	public ZonedDecimalData prmdepst = new ZonedDecimalData(17, 2).isAPartOf(th623Rec, 3);
  	public FixedLengthStringData filler = new FixedLengthStringData(480).isAPartOf(th623Rec, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th623Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th623Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}