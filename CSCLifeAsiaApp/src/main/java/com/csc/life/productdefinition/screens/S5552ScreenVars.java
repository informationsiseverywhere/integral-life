package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5552
 * @version 1.0 generated on 30/08/09 06:44
 * @author Quipoz
 */
public class S5552ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(197);
	public FixedLengthStringData dataFields = new FixedLengthStringData(53).isAPartOf(dataArea, 0);
	public ZonedDecimalData bsmthsaf = DD.bsmthsaf.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData bsmthsbf = DD.bsmthsbf.copyToZonedDecimal().isAPartOf(dataFields,2);
	public FixedLengthStringData bteqpt = DD.bteqpt.copy().isAPartOf(dataFields,4);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,5);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,14);
	public ZonedDecimalData ptmthsaf = DD.ptmthsaf.copyToZonedDecimal().isAPartOf(dataFields,44);
	public ZonedDecimalData ptmthsbf = DD.ptmthsbf.copyToZonedDecimal().isAPartOf(dataFields,46);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,48);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 53);
	public FixedLengthStringData bsmthsafErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bsmthsbfErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bteqptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ptmthsafErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData ptmthsbfErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 89);
	public FixedLengthStringData[] bsmthsafOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bsmthsbfOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bteqptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ptmthsafOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] ptmthsbfOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5552screenWritten = new LongData(0);
	public LongData S5552protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5552ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(bsmthsbfOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bsmthsafOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ptmthsbfOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ptmthsafOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, bsmthsbf, bsmthsaf, ptmthsbf, ptmthsaf, bteqpt};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, bsmthsbfOut, bsmthsafOut, ptmthsbfOut, ptmthsafOut, bteqptOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, bsmthsbfErr, bsmthsafErr, ptmthsbfErr, ptmthsafErr, bteqptErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5552screen.class;
		protectRecord = S5552protect.class;
	}

}
