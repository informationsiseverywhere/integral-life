package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr643screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 7, 16, 1, 2, 12, 3, 21}; 
	public static int maxRecords = 15;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {8, 4, 5, 6, 7, 3, 1, 2}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {8, 21, 3, 63}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr643ScreenVars sv = (Sr643ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr643screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr643screensfl, 
			sv.Sr643screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr643ScreenVars sv = (Sr643ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr643screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr643ScreenVars sv = (Sr643ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr643screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr643screensflWritten.gt(0))
		{
			sv.sr643screensfl.setCurrentIndex(0);
			sv.Sr643screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr643ScreenVars sv = (Sr643ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr643screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr643ScreenVars screenVars = (Sr643ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.seqno.setFieldName("seqno");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.chdrsel.setFieldName("chdrsel");
				screenVars.life.setFieldName("life");
				screenVars.jlife.setFieldName("jlife");
				screenVars.zmedtyp.setFieldName("zmedtyp");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.zmedfee.setFieldName("zmedfee");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.seqno.set(dm.getField("seqno"));
			screenVars.chdrnum.set(dm.getField("chdrnum"));
			screenVars.chdrsel.set(dm.getField("chdrsel"));
			screenVars.life.set(dm.getField("life"));
			screenVars.jlife.set(dm.getField("jlife"));
			screenVars.zmedtyp.set(dm.getField("zmedtyp"));
			screenVars.effdateDisp.set(dm.getField("effdateDisp"));
			screenVars.zmedfee.set(dm.getField("zmedfee"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr643ScreenVars screenVars = (Sr643ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.seqno.setFieldName("seqno");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.chdrsel.setFieldName("chdrsel");
				screenVars.life.setFieldName("life");
				screenVars.jlife.setFieldName("jlife");
				screenVars.zmedtyp.setFieldName("zmedtyp");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.zmedfee.setFieldName("zmedfee");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("seqno").set(screenVars.seqno);
			dm.getField("chdrnum").set(screenVars.chdrnum);
			dm.getField("chdrsel").set(screenVars.chdrsel);
			dm.getField("life").set(screenVars.life);
			dm.getField("jlife").set(screenVars.jlife);
			dm.getField("zmedtyp").set(screenVars.zmedtyp);
			dm.getField("effdateDisp").set(screenVars.effdateDisp);
			dm.getField("zmedfee").set(screenVars.zmedfee);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr643screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr643ScreenVars screenVars = (Sr643ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.seqno.clearFormatting();
		screenVars.chdrnum.clearFormatting();
		screenVars.chdrsel.clearFormatting();
		screenVars.life.clearFormatting();
		screenVars.jlife.clearFormatting();
		screenVars.zmedtyp.clearFormatting();
		screenVars.effdateDisp.clearFormatting();
		screenVars.zmedfee.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr643ScreenVars screenVars = (Sr643ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.seqno.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.chdrsel.setClassString("");
		screenVars.life.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.zmedtyp.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.zmedfee.setClassString("");
	}

/**
 * Clear all the variables in Sr643screensfl
 */
	public static void clear(VarModel pv) {
		Sr643ScreenVars screenVars = (Sr643ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.seqno.clear();
		screenVars.chdrnum.clear();
		screenVars.chdrsel.clear();
		screenVars.life.clear();
		screenVars.jlife.clear();
		screenVars.zmedtyp.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.zmedfee.clear();
	}
}
