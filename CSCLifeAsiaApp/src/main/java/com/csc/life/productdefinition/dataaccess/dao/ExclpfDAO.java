package com.csc.life.productdefinition.dataaccess.dao;

import java.util.LinkedList;
import java.util.List;

import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ExclpfDAO extends BaseDAO<Exclpf>{
	public Exclpf readRecord(String chdrcoy,String chdrnum,String comp,String life,String coverage,String rider);
	public LinkedList<Exclpf> getExclpfRecord(String chdrnum,String comp,String life,String coverage,String rider);
	public LinkedList<Exclpf> getRecordByContract(String chdrnum);
	public Exclpf readRecordByCode(String chdrnum,String comp,String excode,String life,String coverage,String rider);
	public void insertRecord(Exclpf exclpf);
	public void updateRecord(Exclpf exclpf,String excda);
	public void updateStatus(Exclpf exclpf);
	public void deleteRecord(Exclpf exclpf);
	//PINNACLE-2855
	public void updateRecordToInvalidate(Exclpf exclpf, boolean trannoUpdate);
	public List<Exclpf> getExclpfRecordInReverseOrder(String chdrcoy, String chdrnum, Integer tranno,String life, String coverage, String rider);
	public void deleteRecordByUniqueNumber(Exclpf exclpf);
	public int getMaxSeqnbr(String chdrcoy, String chdrnum,String life, String coverage, String rider);
}
