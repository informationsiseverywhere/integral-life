package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.PDInittedArray;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: 
 * @version:
 * Creation Date: Wed, 3 Oct 2012 03:09:00
 * Description: VPMS-Bonus surrender calculation..                                                
 * Copybook name: Vpxsurcrec
 *
 * Copyright (2012) CSC Asia, all rights reserved
 *
 */
public class Vpxsurcrec extends ExternalData {

  
  	public FixedLengthStringData vpxsurcrec = new FixedLengthStringData(208);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(vpxsurcrec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(vpxsurcrec, 5);
  	public FixedLengthStringData copybook = new FixedLengthStringData(12).isAPartOf(vpxsurcrec, 9);
  	public FixedLengthStringData result = new FixedLengthStringData(115).isAPartOf(vpxsurcrec, 21);
  	public FixedLengthStringData resultField = new FixedLengthStringData(35).isAPartOf(result, 0);
  	public FixedLengthStringData resultValue = new FixedLengthStringData(80).isAPartOf(result, 35);
  	public FixedLengthStringData dataRec = new FixedLengthStringData(72).isAPartOf(vpxsurcrec, 136);
  	public PackedDecimalData curduntbal = new PackedDecimalData(17, 5).isAPartOf(dataRec, 0);
  	public FixedLengthStringData chargeCalc = new FixedLengthStringData(4).isAPartOf(dataRec, 17);
  	public PackedDecimalData modPrem = new PackedDecimalData(17, 2).isAPartOf(dataRec, 21);
  	public PackedDecimalData paidPrem = new PackedDecimalData(17, 2).isAPartOf(dataRec, 38);
  	public PackedDecimalData totalEstSurrValue = new PackedDecimalData(17, 2).isAPartOf(dataRec, 55);
  	
    //ILIFE-7396 start
  	public PackedDecimalData lage = new PackedDecimalData(3, 0);
  	public PackedDecimalData jlage = new PackedDecimalData(3, 0);
  	public FixedLengthStringData lsex = new FixedLengthStringData(1);
  	public FixedLengthStringData jlsex = new FixedLengthStringData(1);
  	public FixedLengthStringData lifeInd = new FixedLengthStringData(1);
  	public ZonedDecimalData covrcnt = new ZonedDecimalData(3, 0).setUnsigned();
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3);
  	
  	public PackedDecimalData covrrcomdate00=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrrcomdate01=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrrcomdate02=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrrcomdate03=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrrcomdate04=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrrcomdate05=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrrcomdate06=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrrcomdate07=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrrcomdate08=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrrcomdate09=new PackedDecimalData(8, 0);
  	
  	public PackedDecimalData covrrcesdate00=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrrcesdate01=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrrcesdate02=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrrcesdate03=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrrcesdate04=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrrcesdate05=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrrcesdate06=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrrcesdate07=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrrcesdate08=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrrcesdate09=new PackedDecimalData(8, 0);
  	
  	public PackedDecimalData covrpcesdate00=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrpcesdate01=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrpcesdate02=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrpcesdate03=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrpcesdate04=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrpcesdate05=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrpcesdate06=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrpcesdate07=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrpcesdate08=new PackedDecimalData(8, 0);
  	public PackedDecimalData covrpcesdate09=new PackedDecimalData(8, 0);

  	public PackedDecimalData covrsumins00=new PackedDecimalData(17, 2);
  	public PackedDecimalData covrsumins01=new PackedDecimalData(17, 2);
  	public PackedDecimalData covrsumins02=new PackedDecimalData(17, 2);
  	public PackedDecimalData covrsumins03=new PackedDecimalData(17, 2);
  	public PackedDecimalData covrsumins04=new PackedDecimalData(17, 2);
  	public PackedDecimalData covrsumins05=new PackedDecimalData(17, 2);
  	public PackedDecimalData covrsumins06=new PackedDecimalData(17, 2);
  	public PackedDecimalData covrsumins07=new PackedDecimalData(17, 2);
  	public PackedDecimalData covrsumins08=new PackedDecimalData(17, 2);
  	public PackedDecimalData covrsumins09=new PackedDecimalData(17, 2);

  	public PackedDecimalData acblsacscurbal00=new PackedDecimalData(17, 2);
  	public PackedDecimalData acblsacscurbal01=new PackedDecimalData(17, 2);
  	public PackedDecimalData acblsacscurbal02=new PackedDecimalData(17, 2);
  	public PackedDecimalData acblsacscurbal03=new PackedDecimalData(17, 2);
  	public PackedDecimalData acblsacscurbal04=new PackedDecimalData(17, 2);
  	public PackedDecimalData acblsacscurbal05=new PackedDecimalData(17, 2);
  	public PackedDecimalData acblsacscurbal06=new PackedDecimalData(17, 2);
  	public PackedDecimalData acblsacscurbal07=new PackedDecimalData(17, 2);
  	public PackedDecimalData acblsacscurbal08=new PackedDecimalData(17, 2);
  	public PackedDecimalData acblsacscurbal09=new PackedDecimalData(17, 2);
   //ILIFE-7396 end
  	
	//ILIFE-8931
  	public FixedLengthStringData crtable00 = new FixedLengthStringData(4);
  	public FixedLengthStringData crtable01 = new FixedLengthStringData(4);
  	public FixedLengthStringData crtable02 = new FixedLengthStringData(4);
  	public FixedLengthStringData crtable03 = new FixedLengthStringData(4);
  	public FixedLengthStringData crtable04 = new FixedLengthStringData(4);
  	public FixedLengthStringData crtable05 = new FixedLengthStringData(4);
  	public FixedLengthStringData crtable06 = new FixedLengthStringData(4);
  	public FixedLengthStringData crtable07 = new FixedLengthStringData(4);
  	public FixedLengthStringData crtable08 = new FixedLengthStringData(4);
  	public FixedLengthStringData crtable09 = new FixedLengthStringData(4);
  	
  	public PackedDecimalData dividendAmt00 = new PackedDecimalData(18, 3);
  	public PackedDecimalData dividendAmt01 = new PackedDecimalData(18, 3);
  	public PackedDecimalData dividendAmt02 = new PackedDecimalData(18, 3);
  	public PackedDecimalData dividendAmt03 = new PackedDecimalData(18, 3);
  	public PackedDecimalData dividendAmt04 = new PackedDecimalData(18, 3);
  	public PackedDecimalData dividendAmt05 = new PackedDecimalData(18, 3);
  	public PackedDecimalData dividendAmt06 = new PackedDecimalData(18, 3);
  	public PackedDecimalData dividendAmt07 = new PackedDecimalData(18, 3);
  	public PackedDecimalData dividendAmt08 = new PackedDecimalData(18, 3);
  	public PackedDecimalData dividendAmt09 = new PackedDecimalData(18, 3);
  	
  	public PackedDecimalData age00 = new PackedDecimalData(3, 0);
  	public PackedDecimalData age01 = new PackedDecimalData(3, 0);
  	public PackedDecimalData age02 = new PackedDecimalData(3, 0);
  	public PackedDecimalData age03 = new PackedDecimalData(3, 0);
  	public PackedDecimalData age04 = new PackedDecimalData(3, 0);
  	public PackedDecimalData age05 = new PackedDecimalData(3, 0);
  	public PackedDecimalData age06 = new PackedDecimalData(3, 0);
  	public PackedDecimalData age07 = new PackedDecimalData(3, 0);
  	public PackedDecimalData age08 = new PackedDecimalData(3, 0);
  	public PackedDecimalData age09 = new PackedDecimalData(3, 0);
  	
	public void initialize() {
		COBOLFunctions.initialize(vpxsurcrec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			vpxsurcrec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}