/*
 * File: T5659pt.java
 * Date: 30 August 2009 2:25:07
 * Author: Quipoz Limited
 * 
 * Class transformed from T5659PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.T5659rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5659.
*
*
*****************************************************************
* </pre>
*/
public class T5659pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(54).isAPartOf(wsaaPrtLine001, 22, FILLER).init("Basic Annual Premium Discounts - Term            S5659");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(74);
	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 0, FILLER).init("Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 9);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 10, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 20);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 25, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 33);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 44);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(43);
	private FixedLengthStringData filler7 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 13);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 23, FILLER).init(SPACES);
	private FixedLengthStringData filler9 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine003, 29, FILLER).init("To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 33);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(65);
	private FixedLengthStringData filler10 = new FixedLengthStringData(65).isAPartOf(wsaaPrtLine004, 0, FILLER).init("  Volume Bands: From                 To                     Less");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(66);
	private FixedLengthStringData filler11 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine005, 9, FILLER).init("(1)");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine005, 17).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine005, 38).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine005, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine005, 61).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(66);
	private FixedLengthStringData filler15 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler16 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 9, FILLER).init("(2)");
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine006, 17).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine006, 38).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine006, 61).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(66);
	private FixedLengthStringData filler19 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler20 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 9, FILLER).init("(3)");
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine007, 17).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine007, 38).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 61).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(66);
	private FixedLengthStringData filler23 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler24 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 9, FILLER).init("(4)");
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine008, 17).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine008, 38).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 61).setPattern("ZZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(41);
	private FixedLengthStringData filler27 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine009, 0, FILLER).init("  Rounding Factor:");
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 20).setPattern("ZZZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine009, 26, FILLER).init("  per prem unit");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5659rec t5659rec = new T5659rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5659pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5659rec.t5659Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5659rec.volbanfr01);
		fieldNo008.set(t5659rec.volbanto01);
		fieldNo009.set(t5659rec.volbanle01);
		fieldNo010.set(t5659rec.volbanfr02);
		fieldNo011.set(t5659rec.volbanto02);
		fieldNo012.set(t5659rec.volbanle02);
		fieldNo013.set(t5659rec.volbanfr03);
		fieldNo014.set(t5659rec.volbanto03);
		fieldNo015.set(t5659rec.volbanle03);
		fieldNo016.set(t5659rec.volbanfr04);
		fieldNo017.set(t5659rec.volbanto04);
		fieldNo018.set(t5659rec.volbanle04);
		fieldNo019.set(t5659rec.rndfact);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
