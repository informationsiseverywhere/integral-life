package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5662
 * @version 1.0 generated on 30/08/09 06:47
 * @author Quipoz
 */
public class S5662ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(150);
	public FixedLengthStringData dataFields = new FixedLengthStringData(54).isAPartOf(dataArea, 0);
	public FixedLengthStringData bnyfctn = DD.bnyfctn.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData bnypc = DD.bnypc.copyToZonedDecimal().isAPartOf(dataFields,5);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 54);
	public FixedLengthStringData bnyfctnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bnypcErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 78);
	public FixedLengthStringData[] bnyfctnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bnypcOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5662screenWritten = new LongData(0);
	public LongData S5662protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5662ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(bnyfctnOut,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bnypcOut,new String[] {"32",null, "-32",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, bnyfctn, bnypc};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, bnyfctnOut, bnypcOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, bnyfctnErr, bnypcErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5662screen.class;
		protectRecord = S5662protect.class;
	}

}
