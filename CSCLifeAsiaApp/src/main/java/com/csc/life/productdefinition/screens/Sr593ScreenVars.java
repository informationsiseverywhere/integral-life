package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR593
 * @version 1.0 generated on 30/08/09 07:21
 * @author Quipoz
 */
public class Sr593ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(868);
	public FixedLengthStringData dataFields = new FixedLengthStringData(164).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrtypes = new FixedLengthStringData(120).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] chdrtype = FLSArrayPartOfStructure(40, 3, chdrtypes, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(120).isAPartOf(chdrtypes, 0, FILLER_REDEFINE);
	public FixedLengthStringData chdrtype01 = DD.chdrtype.copy().isAPartOf(filler,0);
	public FixedLengthStringData chdrtype02 = DD.chdrtype.copy().isAPartOf(filler,3);
	public FixedLengthStringData chdrtype03 = DD.chdrtype.copy().isAPartOf(filler,6);
	public FixedLengthStringData chdrtype04 = DD.chdrtype.copy().isAPartOf(filler,9);
	public FixedLengthStringData chdrtype05 = DD.chdrtype.copy().isAPartOf(filler,12);
	public FixedLengthStringData chdrtype06 = DD.chdrtype.copy().isAPartOf(filler,15);
	public FixedLengthStringData chdrtype07 = DD.chdrtype.copy().isAPartOf(filler,18);
	public FixedLengthStringData chdrtype08 = DD.chdrtype.copy().isAPartOf(filler,21);
	public FixedLengthStringData chdrtype09 = DD.chdrtype.copy().isAPartOf(filler,24);
	public FixedLengthStringData chdrtype10 = DD.chdrtype.copy().isAPartOf(filler,27);
	public FixedLengthStringData chdrtype11 = DD.chdrtype.copy().isAPartOf(filler,30);
	public FixedLengthStringData chdrtype12 = DD.chdrtype.copy().isAPartOf(filler,33);
	public FixedLengthStringData chdrtype13 = DD.chdrtype.copy().isAPartOf(filler,36);
	public FixedLengthStringData chdrtype14 = DD.chdrtype.copy().isAPartOf(filler,39);
	public FixedLengthStringData chdrtype15 = DD.chdrtype.copy().isAPartOf(filler,42);
	public FixedLengthStringData chdrtype16 = DD.chdrtype.copy().isAPartOf(filler,45);
	public FixedLengthStringData chdrtype17 = DD.chdrtype.copy().isAPartOf(filler,48);
	public FixedLengthStringData chdrtype18 = DD.chdrtype.copy().isAPartOf(filler,51);
	public FixedLengthStringData chdrtype19 = DD.chdrtype.copy().isAPartOf(filler,54);
	public FixedLengthStringData chdrtype20 = DD.chdrtype.copy().isAPartOf(filler,57);
	public FixedLengthStringData chdrtype21 = DD.chdrtype.copy().isAPartOf(filler,60);
	public FixedLengthStringData chdrtype22 = DD.chdrtype.copy().isAPartOf(filler,63);
	public FixedLengthStringData chdrtype23 = DD.chdrtype.copy().isAPartOf(filler,66);
	public FixedLengthStringData chdrtype24 = DD.chdrtype.copy().isAPartOf(filler,69);
	public FixedLengthStringData chdrtype25 = DD.chdrtype.copy().isAPartOf(filler,72);
	public FixedLengthStringData chdrtype26 = DD.chdrtype.copy().isAPartOf(filler,75);
	public FixedLengthStringData chdrtype27 = DD.chdrtype.copy().isAPartOf(filler,78);
	public FixedLengthStringData chdrtype28 = DD.chdrtype.copy().isAPartOf(filler,81);
	public FixedLengthStringData chdrtype29 = DD.chdrtype.copy().isAPartOf(filler,84);
	public FixedLengthStringData chdrtype30 = DD.chdrtype.copy().isAPartOf(filler,87);
	public FixedLengthStringData chdrtype31 = DD.chdrtype.copy().isAPartOf(filler,90);
	public FixedLengthStringData chdrtype32 = DD.chdrtype.copy().isAPartOf(filler,93);
	public FixedLengthStringData chdrtype33 = DD.chdrtype.copy().isAPartOf(filler,96);
	public FixedLengthStringData chdrtype34 = DD.chdrtype.copy().isAPartOf(filler,99);
	public FixedLengthStringData chdrtype35 = DD.chdrtype.copy().isAPartOf(filler,102);
	public FixedLengthStringData chdrtype36 = DD.chdrtype.copy().isAPartOf(filler,105);
	public FixedLengthStringData chdrtype37 = DD.chdrtype.copy().isAPartOf(filler,108);
	public FixedLengthStringData chdrtype38 = DD.chdrtype.copy().isAPartOf(filler,111);
	public FixedLengthStringData chdrtype39 = DD.chdrtype.copy().isAPartOf(filler,114);
	public FixedLengthStringData chdrtype40 = DD.chdrtype.copy().isAPartOf(filler,117);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,120);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,121);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,129);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,159);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(176).isAPartOf(dataArea, 164);
	public FixedLengthStringData chdrtypesErr = new FixedLengthStringData(160).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] chdrtypeErr = FLSArrayPartOfStructure(40, 4, chdrtypesErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(160).isAPartOf(chdrtypesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData chdrtype01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData chdrtype02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData chdrtype03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData chdrtype04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData chdrtype05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData chdrtype06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData chdrtype07Err = new FixedLengthStringData(4).isAPartOf(filler1, 24);
	public FixedLengthStringData chdrtype08Err = new FixedLengthStringData(4).isAPartOf(filler1, 28);
	public FixedLengthStringData chdrtype09Err = new FixedLengthStringData(4).isAPartOf(filler1, 32);
	public FixedLengthStringData chdrtype10Err = new FixedLengthStringData(4).isAPartOf(filler1, 36);
	public FixedLengthStringData chdrtype11Err = new FixedLengthStringData(4).isAPartOf(filler1, 40);
	public FixedLengthStringData chdrtype12Err = new FixedLengthStringData(4).isAPartOf(filler1, 44);
	public FixedLengthStringData chdrtype13Err = new FixedLengthStringData(4).isAPartOf(filler1, 48);
	public FixedLengthStringData chdrtype14Err = new FixedLengthStringData(4).isAPartOf(filler1, 52);
	public FixedLengthStringData chdrtype15Err = new FixedLengthStringData(4).isAPartOf(filler1, 56);
	public FixedLengthStringData chdrtype16Err = new FixedLengthStringData(4).isAPartOf(filler1, 60);
	public FixedLengthStringData chdrtype17Err = new FixedLengthStringData(4).isAPartOf(filler1, 64);
	public FixedLengthStringData chdrtype18Err = new FixedLengthStringData(4).isAPartOf(filler1, 68);
	public FixedLengthStringData chdrtype19Err = new FixedLengthStringData(4).isAPartOf(filler1, 72);
	public FixedLengthStringData chdrtype20Err = new FixedLengthStringData(4).isAPartOf(filler1, 76);
	public FixedLengthStringData chdrtype21Err = new FixedLengthStringData(4).isAPartOf(filler1, 80);
	public FixedLengthStringData chdrtype22Err = new FixedLengthStringData(4).isAPartOf(filler1, 84);
	public FixedLengthStringData chdrtype23Err = new FixedLengthStringData(4).isAPartOf(filler1, 88);
	public FixedLengthStringData chdrtype24Err = new FixedLengthStringData(4).isAPartOf(filler1, 92);
	public FixedLengthStringData chdrtype25Err = new FixedLengthStringData(4).isAPartOf(filler1, 96);
	public FixedLengthStringData chdrtype26Err = new FixedLengthStringData(4).isAPartOf(filler1, 100);
	public FixedLengthStringData chdrtype27Err = new FixedLengthStringData(4).isAPartOf(filler1, 104);
	public FixedLengthStringData chdrtype28Err = new FixedLengthStringData(4).isAPartOf(filler1, 108);
	public FixedLengthStringData chdrtype29Err = new FixedLengthStringData(4).isAPartOf(filler1, 112);
	public FixedLengthStringData chdrtype30Err = new FixedLengthStringData(4).isAPartOf(filler1, 116);
	public FixedLengthStringData chdrtype31Err = new FixedLengthStringData(4).isAPartOf(filler1, 120);
	public FixedLengthStringData chdrtype32Err = new FixedLengthStringData(4).isAPartOf(filler1, 124);
	public FixedLengthStringData chdrtype33Err = new FixedLengthStringData(4).isAPartOf(filler1, 128);
	public FixedLengthStringData chdrtype34Err = new FixedLengthStringData(4).isAPartOf(filler1, 132);
	public FixedLengthStringData chdrtype35Err = new FixedLengthStringData(4).isAPartOf(filler1, 136);
	public FixedLengthStringData chdrtype36Err = new FixedLengthStringData(4).isAPartOf(filler1, 140);
	public FixedLengthStringData chdrtype37Err = new FixedLengthStringData(4).isAPartOf(filler1, 144);
	public FixedLengthStringData chdrtype38Err = new FixedLengthStringData(4).isAPartOf(filler1, 148);
	public FixedLengthStringData chdrtype39Err = new FixedLengthStringData(4).isAPartOf(filler1, 152);
	public FixedLengthStringData chdrtype40Err = new FixedLengthStringData(4).isAPartOf(filler1, 156);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(528).isAPartOf(dataArea, 340);
	public FixedLengthStringData chdrtypesOut = new FixedLengthStringData(480).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] chdrtypeOut = FLSArrayPartOfStructure(40, 12, chdrtypesOut, 0);
	public FixedLengthStringData[][] chdrtypeO = FLSDArrayPartOfArrayStructure(12, 1, chdrtypeOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(480).isAPartOf(chdrtypesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] chdrtype01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] chdrtype02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] chdrtype03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] chdrtype04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] chdrtype05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] chdrtype06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] chdrtype07Out = FLSArrayPartOfStructure(12, 1, filler2, 72);
	public FixedLengthStringData[] chdrtype08Out = FLSArrayPartOfStructure(12, 1, filler2, 84);
	public FixedLengthStringData[] chdrtype09Out = FLSArrayPartOfStructure(12, 1, filler2, 96);
	public FixedLengthStringData[] chdrtype10Out = FLSArrayPartOfStructure(12, 1, filler2, 108);
	public FixedLengthStringData[] chdrtype11Out = FLSArrayPartOfStructure(12, 1, filler2, 120);
	public FixedLengthStringData[] chdrtype12Out = FLSArrayPartOfStructure(12, 1, filler2, 132);
	public FixedLengthStringData[] chdrtype13Out = FLSArrayPartOfStructure(12, 1, filler2, 144);
	public FixedLengthStringData[] chdrtype14Out = FLSArrayPartOfStructure(12, 1, filler2, 156);
	public FixedLengthStringData[] chdrtype15Out = FLSArrayPartOfStructure(12, 1, filler2, 168);
	public FixedLengthStringData[] chdrtype16Out = FLSArrayPartOfStructure(12, 1, filler2, 180);
	public FixedLengthStringData[] chdrtype17Out = FLSArrayPartOfStructure(12, 1, filler2, 192);
	public FixedLengthStringData[] chdrtype18Out = FLSArrayPartOfStructure(12, 1, filler2, 204);
	public FixedLengthStringData[] chdrtype19Out = FLSArrayPartOfStructure(12, 1, filler2, 216);
	public FixedLengthStringData[] chdrtype20Out = FLSArrayPartOfStructure(12, 1, filler2, 228);
	public FixedLengthStringData[] chdrtype21Out = FLSArrayPartOfStructure(12, 1, filler2, 240);
	public FixedLengthStringData[] chdrtype22Out = FLSArrayPartOfStructure(12, 1, filler2, 252);
	public FixedLengthStringData[] chdrtype23Out = FLSArrayPartOfStructure(12, 1, filler2, 264);
	public FixedLengthStringData[] chdrtype24Out = FLSArrayPartOfStructure(12, 1, filler2, 276);
	public FixedLengthStringData[] chdrtype25Out = FLSArrayPartOfStructure(12, 1, filler2, 288);
	public FixedLengthStringData[] chdrtype26Out = FLSArrayPartOfStructure(12, 1, filler2, 300);
	public FixedLengthStringData[] chdrtype27Out = FLSArrayPartOfStructure(12, 1, filler2, 312);
	public FixedLengthStringData[] chdrtype28Out = FLSArrayPartOfStructure(12, 1, filler2, 324);
	public FixedLengthStringData[] chdrtype29Out = FLSArrayPartOfStructure(12, 1, filler2, 336);
	public FixedLengthStringData[] chdrtype30Out = FLSArrayPartOfStructure(12, 1, filler2, 348);
	public FixedLengthStringData[] chdrtype31Out = FLSArrayPartOfStructure(12, 1, filler2, 360);
	public FixedLengthStringData[] chdrtype32Out = FLSArrayPartOfStructure(12, 1, filler2, 372);
	public FixedLengthStringData[] chdrtype33Out = FLSArrayPartOfStructure(12, 1, filler2, 384);
	public FixedLengthStringData[] chdrtype34Out = FLSArrayPartOfStructure(12, 1, filler2, 396);
	public FixedLengthStringData[] chdrtype35Out = FLSArrayPartOfStructure(12, 1, filler2, 408);
	public FixedLengthStringData[] chdrtype36Out = FLSArrayPartOfStructure(12, 1, filler2, 420);
	public FixedLengthStringData[] chdrtype37Out = FLSArrayPartOfStructure(12, 1, filler2, 432);
	public FixedLengthStringData[] chdrtype38Out = FLSArrayPartOfStructure(12, 1, filler2, 444);
	public FixedLengthStringData[] chdrtype39Out = FLSArrayPartOfStructure(12, 1, filler2, 456);
	public FixedLengthStringData[] chdrtype40Out = FLSArrayPartOfStructure(12, 1, filler2, 468);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr593screenWritten = new LongData(0);
	public LongData Sr593protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr593ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, chdrtype01, chdrtype02, chdrtype03, chdrtype04, chdrtype05, chdrtype06, chdrtype07, chdrtype08, chdrtype09, chdrtype10, chdrtype11, chdrtype12, chdrtype13, chdrtype14, chdrtype15, chdrtype16, chdrtype17, chdrtype18, chdrtype19, chdrtype20, chdrtype21, chdrtype22, chdrtype23, chdrtype24, chdrtype25, chdrtype26, chdrtype27, chdrtype28, chdrtype29, chdrtype30, chdrtype31, chdrtype32, chdrtype33, chdrtype34, chdrtype35, chdrtype36, chdrtype37, chdrtype38, chdrtype39, chdrtype40};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, chdrtype01Out, chdrtype02Out, chdrtype03Out, chdrtype04Out, chdrtype05Out, chdrtype06Out, chdrtype07Out, chdrtype08Out, chdrtype09Out, chdrtype10Out, chdrtype11Out, chdrtype12Out, chdrtype13Out, chdrtype14Out, chdrtype15Out, chdrtype16Out, chdrtype17Out, chdrtype18Out, chdrtype19Out, chdrtype20Out, chdrtype21Out, chdrtype22Out, chdrtype23Out, chdrtype24Out, chdrtype25Out, chdrtype26Out, chdrtype27Out, chdrtype28Out, chdrtype29Out, chdrtype30Out, chdrtype31Out, chdrtype32Out, chdrtype33Out, chdrtype34Out, chdrtype35Out, chdrtype36Out, chdrtype37Out, chdrtype38Out, chdrtype39Out, chdrtype40Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, chdrtype01Err, chdrtype02Err, chdrtype03Err, chdrtype04Err, chdrtype05Err, chdrtype06Err, chdrtype07Err, chdrtype08Err, chdrtype09Err, chdrtype10Err, chdrtype11Err, chdrtype12Err, chdrtype13Err, chdrtype14Err, chdrtype15Err, chdrtype16Err, chdrtype17Err, chdrtype18Err, chdrtype19Err, chdrtype20Err, chdrtype21Err, chdrtype22Err, chdrtype23Err, chdrtype24Err, chdrtype25Err, chdrtype26Err, chdrtype27Err, chdrtype28Err, chdrtype29Err, chdrtype30Err, chdrtype31Err, chdrtype32Err, chdrtype33Err, chdrtype34Err, chdrtype35Err, chdrtype36Err, chdrtype37Err, chdrtype38Err, chdrtype39Err, chdrtype40Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr593screen.class;
		protectRecord = Sr593protect.class;
	}

}
