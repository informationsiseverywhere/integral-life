/*
 * File: Pr633.java
 * Date: 30 August 2009 1:49:43
 * Author: Quipoz Limited
 * 
 * Class transformed from PR633.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.recordstructures.Clntkey;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.life.agents.tablestructures.T5690rec;
import com.csc.life.productdefinition.dataaccess.ZmpdTableDAM;
import com.csc.life.productdefinition.dataaccess.ZmpfTableDAM;
import com.csc.life.productdefinition.dataaccess.ZmpvTableDAM;
import com.csc.life.productdefinition.dataaccess.ZmtmTableDAM;
import com.csc.life.productdefinition.screens.Sr633ScreenVars;
import com.csc.life.productdefinition.tablestructures.Tr640rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*    Medical Provider Register/Maintain/Enquiry
*    ------------------------------------------
*    Action "A" and "B" allow user to create/maintain Medical
*    Provider Details.
*
*    Action "C" is for Medical Provider Details quiry.
*
*    Client details can be windowed upon client number.
*
*    Doctor details will pop-up Doctor details
*    maintenance allowed user to maintain doctor information.
*
*    "+" sign indicate details for client & doctor.
*
*    For medical provider more than one branch, user required to
*    create difference client for difference branch and attached
*    this client into difference Medical provider code.
*
*    If Terminated date entered, system will defaulted "X" to the
*    Reason field to pop-up Terminated Reason screen to capture
*    termination reason. All Doctors cease date will be updated
*    to termination date. "+" sign will indicate details for
*    termination reason. Not allowed for termination reason if
*    no terminated date.
*
*    For Medical Provider reinstated, user required to change
*    provider status, removed the termination date, update the
*    Doctor cease date and system will removed termination reason
*    details.
*
*    It is not mandatory for Medical Provider to have the
*    facilities and doctors attached, but if in the event it is
*    required, it is one-to-one or one-to-many link.
*
*
****************************************************************** ****
* </pre>
*/
public class Pr633 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR633");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData wsaaRemainder = new ZonedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData iy = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaFoundCheckBox = new FixedLengthStringData(1);
	private Validator foundCheckBox = new Validator(wsaaFoundCheckBox, "Y");

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaRecordFound = new FixedLengthStringData(1);
	private Validator recordFound = new Validator(wsaaRecordFound, "Y");

	private FixedLengthStringData wsaaDuplicateFound = new FixedLengthStringData(1);
	private Validator duplicateFound = new Validator(wsaaDuplicateFound, "Y");
	private ZonedDecimalData wsaaSeqno = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSubfileRrn = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaSubfileRrnD = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaTempZmedtyp = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaOrgFlag = new FixedLengthStringData(1);
	private PackedDecimalData wsaaDtetrm = new PackedDecimalData(8, 0);
		/* TABLES */
	private static final String tr638 = "TR638";
	private static final String tr639 = "TR639";
	private static final String t5690 = "T5690";
	private static final String tr640 = "TR640";
		/* FORMATS */
	private static final String clntrec = "CLNTREC";
	private static final String zmpdrec = "ZMPDREC";
	private static final String zmpvrec = "ZMPVREC";
	private static final String zmpfrec = "ZMPFREC";
	private static final String zmtmrec = "ZMTMREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private ClntTableDAM clntIO = new ClntTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ZmpdTableDAM zmpdIO = new ZmpdTableDAM();
	private ZmpfTableDAM zmpfIO = new ZmpfTableDAM();
	private ZmpvTableDAM zmpvIO = new ZmpvTableDAM();
	private ZmtmTableDAM zmtmIO = new ZmtmTableDAM();
	private Clntkey wsaaClntkey = new Clntkey();
	private Optswchrec optswchrec = new Optswchrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T5690rec t5690rec = new T5690rec();
	private Tr640rec tr640rec = new Tr640rec();
	private Sr633ScreenVars sv = ScreenProgram.getScreenVars( Sr633ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		exit1390, 
		exit2090, 
		cont2180, 
		exit2190, 
		stck4070, 
		exit4090
	}

	public Pr633() {
		super();
		screenVars = sv;
		new ScreenModel("Sr633", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		try {
			initialise1010();
			loadSubfile1050();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		initialize(sv.dataArea);
		sv.initialiseSubfileArea();
		sv.dteapp.set(varcom.vrcmMaxDate);
		sv.dtetrm.set(varcom.vrcmMaxDate);
		sv.zmedfee.set(ZERO);
		wsaaFirstTime.set("Y");
		scrnparams.subfileMore.set("Y");
		wsaaOrgFlag.set(wsspcomn.flag);
		scrnparams.function.set(varcom.sclr);
		c1000CallSr633io();
		scrnparams.subfileRrn.set(1);
		/*    Retrieve ZMPV*/
		zmpvIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zmpvIO.getParams());
			syserrrec.statuz.set(zmpvIO.getStatuz());
			fatalError600();
		}
		sv.bankkey.set(zmpvIO.getBankkey());
		sv.bankacckey.set(zmpvIO.getBankacckey());
		sv.zmedprv.set(zmpvIO.getZmedprv());
		sv.zmednum.set(zmpvIO.getZmednum());
		sv.zmednam.set(zmpvIO.getZmednam());
		sv.zmedsts.set(zmpvIO.getZmedsts());
		sv.dteapp.set(zmpvIO.getDteapp());
		sv.dtetrm.set(zmpvIO.getDtetrm());
		sv.zdesc.set(zmpvIO.getZdesc());
		sv.paymth.set(zmpvIO.getPaymth());
		sv.zaracde.set(zmpvIO.getZaracde());
		/*    Get the status description.*/
		if (isNE(sv.zmedsts, SPACES)) {
			descIO.setDataKey(SPACES);
			descIO.setDesctabl(tr638);
			descIO.setDescitem(sv.zmedsts);
			a1000GetDesc();
			sv.statdets.set(descIO.getLongdesc());
		}
		/*    Get the Pay method description.*/
		if (isNE(sv.paymth, SPACES)) {
			descIO.setDataKey(SPACES);
			descIO.setDesctabl(t5690);
			descIO.setDescitem(sv.paymth);
			a1000GetDesc();
			sv.pymdesc.set(descIO.getLongdesc());
		}
		if (isEQ(sv.dtetrm, varcom.vrcmMaxDate)) {
			sv.xoptindOut[varcom.pr.toInt()].set("Y");
		}
		/*    Get the area code description.*/
		if (isNE(sv.zaracde, SPACES)) {
			descIO.setDataKey(SPACES);
			descIO.setDesctabl(tr639);
			descIO.setDescitem(sv.zaracde);
			a1000GetDesc();
			sv.coverDesc.set(descIO.getLongdesc());
		}
		initOptswch1100();
	}

protected void loadSubfile1050()
	{
		initSubfile1200();
		/*    Read ZMPF*/
		zmpfIO.setDataKey(SPACES);
		zmpfIO.setZmedcoy(zmpvIO.getZmedcoy());
		zmpfIO.setZmedprv(zmpvIO.getZmedprv());
		zmpfIO.setSeqno(1);
		zmpfIO.setFormat(zmpfrec);
		zmpfIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zmpfIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zmpfIO.setFitKeysSearch("ZMEDCOY", "ZMEDPRV");
		zmpfIO.setStatuz(varcom.oK);
		wsaaCount.set(ZERO);
		while ( !(isEQ(zmpfIO.getStatuz(), varcom.endp)
		|| isEQ(wsaaCount, sv.subfilePage))) {
			loadSubfile1300();
		}
		
		compute(wsaaRemainder, 0).set(sub(sv.subfilePage, wsaaCount));
		if (isNE(wsspcomn.flag, "I")) {
			for (ix.set(1); !(isGT(ix, wsaaRemainder)); ix.add(1)){
				loadRemainder1400();
			}
		}
		if (isEQ(wsspcomn.flag, "I")
		&& isEQ(sv.zmedtyp, SPACES)) {
			scrnparams.subfileMore.set("N");
		}
		if (isEQ(wsspcomn.flag, "I")
		&& isNE(wsaaCount, sv.subfilePage)) {
			scrnparams.subfileMore.set("N");
		}
		/*    Inquiry Mode*/
		if (isEQ(wsspcomn.flag, "I")) {
			sv.zmedstsOut[varcom.pr.toInt()].set("Y");
			sv.dtetrmOut[varcom.pr.toInt()].set("Y");
			sv.zmednumOut[varcom.pr.toInt()].set("Y");
			sv.zaracdeOut[varcom.pr.toInt()].set("Y");
			sv.paymthOut[varcom.pr.toInt()].set("Y");
			/*    If there is no information behind the check boxes then*/
			/*    'protect' them in inquiry (only for Termination Reason*/
			/*    and Doctor details)*/
			if (isEQ(sv.optind[2], SPACES)) {
				sv.optindO[2][varcom.pr.toInt()].set("Y");
			}
			if (isEQ(sv.optind[3], SPACES)) {
				sv.optindO[3][varcom.pr.toInt()].set("Y");
			}
		}
		/*    User not allow to enter the Termination Date & Reason for*/
		/*    Action 'A'*/
		if (isEQ(wsspcomn.flag, "C")) {
			sv.dtetrmOut[varcom.pr.toInt()].set("Y");
		}
		/*    Client No should be protected fro action 'B'*/
		if (isEQ(wsspcomn.flag, "M")) {
			sv.zmednumOut[varcom.pr.toInt()].set("Y");
		}
		/*    Pass WSSP-CLNTKEY to P2466*/
		wsaaClntkey.clntClntpfx.set(fsupfxcpy.clnt);
		wsaaClntkey.clntClntcoy.set(wsspcomn.fsuco);
		wsaaClntkey.clntClntnum.set(sv.zmednum);
		wsspcomn.clntkey.set(wsaaClntkey.clntFileKey);
		/* If Payment method is CQ protect Bank Details*/
		if (isEQ(sv.paymth, "CQ")) {
			sv.optind03.set(SPACES);
			sv.optindO[3][varcom.pr.toInt()].set("Y");
		}
		//readClnt2100();
	}

protected void initOptswch1100()
	{
		start1110();
	}

protected void start1110()
	{
		/*    Retrieving the Switching Options.*/
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)) {
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		/*    Load OPTSWCH description*/
		sv.optdscx.set(optswchrec.optsDsc[1]);
		sv.xoptind.set(optswchrec.optsInd[1]);
		iy.set(1);
		for (ix.set(2); !(isGT(ix, 4)); ix.add(1)){
			if (isEQ(optswchrec.optsType[ix.toInt()], "X")) {
				sv.optdsc[iy.toInt()].set(optswchrec.optsDsc[ix.toInt()]);
				sv.optind[iy.toInt()].set(optswchrec.optsInd[ix.toInt()]);
				iy.add(1);
			}
		}
		/*    Protect and hide unused checkbox screen*/
		if (isEQ(sv.optdscx, SPACES)) {
			sv.xoptindOut[varcom.pr.toInt()].set("Y");
			sv.xoptindOut[varcom.nd.toInt()].set("Y");
		}
		for (ix.set(1); !(isGT(ix, 3)); ix.add(1)){
			if (isEQ(sv.optdsc[ix.toInt()], SPACES)) {
				sv.optindO[ix.toInt()][varcom.pr.toInt()].set("Y");
				sv.optindO[ix.toInt()][varcom.nd.toInt()].set("Y");
			}
		}
		sv.optind01.set("+");
	}

protected void initSubfile1200()
	{
		/*START*/
		scrnparams.function.set(varcom.sclr);
		processScreen("SR633", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void loadSubfile1300()
	{
		try {
			start1310();
			addSubfile1380();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start1310()
	{
		SmartFileCode.execute(appVars, zmpfIO);
		if (isNE(zmpfIO.getStatuz(), varcom.oK)
		&& isNE(zmpfIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(zmpfIO.getParams());
			syserrrec.statuz.set(zmpfIO.getStatuz());
			fatalError600();
		}
		if (isEQ(zmpfIO.getStatuz(), varcom.endp)
		|| isNE(zmpfIO.getZmedcoy(), zmpvIO.getZmedcoy())
		|| isNE(zmpfIO.getZmedprv(), zmpvIO.getZmedprv())) {
			zmpfIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1390);
		}
		sv.zmedtyp.set(zmpfIO.getZmedtyp());
		sv.value.set(zmpfIO.getZmedtyp());
		sv.zmedfee.set(zmpfIO.getZmedfee());
		/*    Medical Provider Facilities Description*/
		if (isNE(sv.zmedtyp, SPACES)) {
			descIO.setDataKey(SPACES);
			descIO.setDesctabl(tr640);
			descIO.setDescitem(sv.zmedtyp);
			a1000GetDesc();
			sv.longdesc.set(descIO.getLongdesc());
		}
		sv.hflag.set("N");
		wsaaCount.add(1);
		if (isEQ(wsaaCount, sv.subfilePage)) {
			scrnparams.subfileMore.set("Y");
		}
		/*    Protect in Inquiry mode*/
		if (isEQ(wsspcomn.flag, "I")
		|| isNE(sv.dtetrm, varcom.vrcmMaxDate)) {
			sv.zmedtypOut[varcom.pr.toInt()].set("Y");
			sv.zmedfeeOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void addSubfile1380()
	{
		scrnparams.function.set(varcom.sadd);
		c1000CallSr633io();
		zmpfIO.setFunction(varcom.nextr);
	}

protected void loadRemainder1400()
	{
		/*START*/
		sv.zmedtyp.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.hflag.set(SPACES);
		sv.zmedfee.set(ZERO);
		if (isNE(sv.dtetrm, varcom.vrcmMaxDate)) {
			sv.zmedtypOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(wsspcomn.flag, "I")) {
			sv.zmedtypOut[varcom.pr.toInt()].set("Y");
			sv.zmedfeeOut[varcom.pr.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.sadd);
		c1000CallSr633io();
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
			checkForErrors2050();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateScreen2010()
	{
		/*    Refresh Screen*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			wsaaCount.set(ZERO);
			scrnparams.subfileMore.set("N");
			while ( !(isEQ(zmpfIO.getStatuz(), varcom.endp)
			|| isEQ(wsaaCount, sv.subfilePage))) {
				loadSubfile1300();
			}
			
			compute(wsaaRemainder, 0).set(sub(sv.subfilePage, wsaaCount));
			if (isNE(wsspcomn.flag, "I")) {
				for (ix.set(1); !(isGT(ix, wsaaRemainder)); ix.add(1)){
					loadRemainder1400();
				}
			}
			scrnparams.subfileMore.set("Y");
			wsspcomn.edterror.set("Y");
			if (isEQ(wsspcomn.flag, "I")
			&& isNE(wsaaCount, sv.subfilePage)) {
				scrnparams.subfileMore.set("N");
			}
			goTo(GotoLabel.exit2090);
		}
		/*    Skip validation if F12 or Inquiry mode*/
		if (isEQ(scrnparams.statuz, varcom.kill)
		|| isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2090);
		}
		/*    Must be Corporate client*/
		if (isEQ(sv.zmednum, SPACES)) {
			sv.zmednam.set(SPACES);
			sv.zmednumErr.set(errorsInner.e186);
		}
		else {
			wsaaClntkey.clntClntpfx.set(fsupfxcpy.clnt);
			wsaaClntkey.clntClntcoy.set(wsspcomn.fsuco);
			wsaaClntkey.clntClntnum.set(sv.zmednum);
			readClnt2100();
			/*    Pass WSSP-CLNTKEY to P2466*/
			wsaaClntkey.clntClntpfx.set(fsupfxcpy.clnt);
			wsaaClntkey.clntClntcoy.set(wsspcomn.fsuco);
			wsaaClntkey.clntClntnum.set(sv.zmednum);
			wsspcomn.clntkey.set(wsaaClntkey.clntFileKey);
		}
		/*    Status must exist in TR638*/
		if (isEQ(sv.zmedsts, SPACES)) {
			sv.zmedstsErr.set(errorsInner.e186);
		}
		else {
			itemIO.setParams(SPACES);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr638);
			itemIO.setItemitem(sv.zmedsts);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				sv.zmedstsErr.set(errorsInner.rl64);
			}
			else {
				descIO.setDataKey(SPACES);
				descIO.setDesctabl(tr638);
				descIO.setDescitem(sv.zmedsts);
				a1000GetDesc();
				sv.statdets.set(descIO.getLongdesc());
			}
		}
		/*    For Action 'A', not allowed to enter Termination date*/
		/*    and Reason.*/
		/*    Date Appointed must <= Date Terminated.*/
		if (isEQ(sv.dteapp, varcom.vrcmMaxDate)) {
			sv.dteappErr.set(errorsInner.e186);
		}
		if (isLT(sv.dtetrm, sv.dteapp)) {
			sv.dtetrmErr.set(errorsInner.h067);
		}
		if (isEQ(sv.paymth, SPACES)) {
			sv.paymthErr.set(errorsInner.f020);
		}
		else {
			descIO.setDataKey(SPACES);
			descIO.setDesctabl(t5690);
			descIO.setDescitem(sv.paymth);
			a1000GetDesc();
			sv.pymdesc.set(descIO.getLongdesc());
			itemIO.setParams(SPACES);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(t5690);
			itemIO.setItemitem(sv.paymth);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
			t5690rec.t5690Rec.set(itemIO.getGenarea());
			if (isEQ(sv.paymth, "CQ")) {
				sv.optind03.set(SPACES);
				sv.optindO[3][varcom.pr.toInt()].set("Y");
			}
			if (isEQ(t5690rec.bankreq, "Y")) {
				if ((isEQ(sv.bankkey, SPACES)
				|| isEQ(sv.bankacckey, SPACES))
				&& isEQ(sv.optind03, SPACES)) {
					sv.optind03.set("X");
				}
			}
			else {
				sv.bankkey.set(SPACES);
				sv.bankacckey.set(SPACES);
			}
		}
		/*    Termination Date*/
		if (firstTime.isTrue()) {
			if (isNE(sv.dtetrm, varcom.vrcmMaxDate)
			&& isEQ(sv.xoptind, SPACES)) {
				sv.xoptind.set("X");
				sv.xoptindOut[varcom.pr.toInt()].set("N");
				wsaaFirstTime.set("N");
			}
		}
		if (isEQ(sv.dtetrm, varcom.vrcmMaxDate)) {
			wsaaFirstTime.set("Y");
			sv.xoptind.set(SPACES);
			sv.xoptindOut[varcom.pr.toInt()].set("Y");
		}
		/*    If termination date is entered, Doctor cease date will*/
		/*    be updated with termination date.*/
		if (isNE(sv.dtetrm, varcom.vrcmMaxDate)) {
			updateCeaseDate2300();
		}
		/*    For Medical Provider Reinstated, user required to change*/
		/*    provider status, removed the termination date, update*/
		/*    the Doctors cease date and system will removed termination*/
		/*    reason details.*/
		if (isEQ(sv.dtetrm, varcom.vrcmMaxDate)) {
			deleteZmtm2400();
		}
		/*    Check Option*/
		if (isNE(sv.xoptind, SPACES)) {
			optswchrec.optsInd[1].set(sv.xoptind);
			chckOpts2200();
			if (isNE(optswchrec.optsStatuz, varcom.oK)) {
				sv.xoptindErr.set(optswchrec.optsStatuz);
			}
			optswchrec.optsInd[1].set(SPACES);
		}
		iy.set(1);
		for (ix.set(2); !(isGT(ix, 3)); ix.add(1)){
			if (isNE(sv.optind[iy.toInt()], SPACES)) {
				optswchrec.optsInd[ix.toInt()].set(sv.optind[iy.toInt()]);
				chckOpts2200();
				if (isNE(optswchrec.optsStatuz, varcom.oK)) {
					sv.optindErr[iy.toInt()].set(optswchrec.optsStatuz);
				}
				optswchrec.optsInd[ix.toInt()].set(SPACES);
				iy.add(1);
			}
		}
		/*    Area Code must exist in TR639*/
		if (isEQ(sv.zaracde, SPACES)) {
			sv.zaracdeErr.set(errorsInner.rl65);
		}
		else {
			itemIO.setParams(SPACES);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr639);
			itemIO.setItemitem(sv.zaracde);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				sv.zaracdeErr.set(errorsInner.rl65);
			}
			else {
				descIO.setDataKey(SPACES);
				descIO.setDesctabl(tr639);
				descIO.setDescitem(sv.zaracde);
				a1000GetDesc();
				sv.coverDesc.set(descIO.getLongdesc());
			}
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		protectSubfile2800();
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		c1000CallSr633io();
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}
		
		checkDuplicate2700();
	}

protected void readClnt2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start2110();
				case cont2180: 
					cont2180();
				case exit2190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2110()
	{
		clntIO.setDataKey(SPACES);
		clntIO.setClntpfx(wsaaClntkey.clntClntpfx);
		clntIO.setClntcoy(wsaaClntkey.clntClntcoy);
		clntIO.setClntnum(wsaaClntkey.clntClntnum);
		clntIO.setFunction(varcom.readr);
		clntIO.setFormat(clntrec);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(), varcom.oK)
		&& isNE(clntIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(clntIO.getParams());
			syserrrec.statuz.set(clntIO.getStatuz());
			fatalError600();
		}
		if (isEQ(clntIO.getStatuz(), varcom.mrnf)) {
			sv.zmednumErr.set(errorsInner.e655);
			sv.zmednam.set(SPACES);
			goTo(GotoLabel.exit2190);
		}
		if (isNE(clntIO.getClttype(), "C")) {
			sv.zmednumErr.set(errorsInner.rl90);
			goTo(GotoLabel.cont2180);
		}
		if (isEQ(clntIO.getCltind(), "L")) {
			sv.zmednumErr.set(errorsInner.g499);
			goTo(GotoLabel.cont2180);
		}
		if (isEQ(clntIO.getCltind(), "D")) {
			sv.zmednumErr.set(errorsInner.h939);
			goTo(GotoLabel.cont2180);
		}
	}

protected void cont2180()
	{
		if (isEQ(clntIO.getStatuz(), varcom.oK)) {
			/*     STRING CLNT-LSURNAME       DELIMITED BY ' '              */
			/*            ' '                 DELIMITED BY SIZE             */
			/*            CLNT-LGIVNAME       DELIMITED BY ' '              */
			/*                           INTO SR633-ZMEDNAM                 */
			/*     END-STRING                                               */
			if (isNE(clntIO.getEthorig(), "2")) {
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression(clntIO.getLsurname(), "  ");
				stringVariable1.addExpression(" ", "  ");
				stringVariable1.addExpression(clntIO.getLgivname(), "  ");
				stringVariable1.addExpression(" ", "  ");
				stringVariable1.setStringInto(sv.zmednam);
			}
			if (isEQ(clntIO.getEthorig(), "2")) {
				StringUtil stringVariable2 = new StringUtil();
				stringVariable2.addExpression(clntIO.getLgivname(), "  ");
				stringVariable2.addExpression(" ", "  ");
				stringVariable2.addExpression(clntIO.getLsurname(), "  ");
				stringVariable2.addExpression(" ", "  ");
				stringVariable2.setStringInto(sv.zmednam);
			}
		}
	}

protected void chckOpts2200()
	{
		/*START*/
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set("CBOX");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		/*EXIT*/
	}

protected void updateCeaseDate2300()
	{
		start2310();
	}

protected void start2310()
	{
		zmpdIO.setParams(SPACES);
		zmpdIO.setZmedcoy(wsspcomn.company);
		zmpdIO.setZmedprv(sv.zmedprv);
		zmpdIO.setSeqno(1);
		zmpdIO.setFunction(varcom.begnh);
		a4000CallZmpd();
		while ( !(isNE(zmpdIO.getStatuz(), varcom.oK)
		|| isNE(zmpdIO.getZmedcoy(), wsspcomn.company)
		|| isNE(zmpdIO.getZmedprv(), sv.zmedprv))) {
			if (isNE(sv.dtetrm, varcom.vrcmMaxDate)
			&& isEQ(zmpdIO.getDtetrm(), varcom.vrcmMaxDate)) {
				zmpdIO.setDtetrm(sv.dtetrm);
				zmpdIO.setFunction(varcom.rewrt);
				a4000CallZmpd();
			}
			zmpdIO.setFunction(varcom.nextr);
			a4000CallZmpd();
		}
		
	}

protected void deleteZmtm2400()
	{
		start2410();
	}

protected void start2410()
	{
		zmtmIO.setParams(SPACES);
		zmtmIO.setZmedcoy(wsspcomn.company);
		zmtmIO.setZmedprv(sv.zmedprv);
		zmtmIO.setSeqno(1);
		zmtmIO.setFunction(varcom.begnh);
		a5000CallZmtm();
		while ( !(isNE(zmtmIO.getStatuz(), varcom.oK)
		|| isNE(zmtmIO.getZmedcoy(), wsspcomn.company)
		|| isNE(zmtmIO.getZmedprv(), sv.zmedprv))) {
			zmtmIO.setFunction(varcom.delet);
			a5000CallZmtm();
			zmtmIO.setFunction(varcom.nextr);
			a5000CallZmtm();
		}
		
		zmtmIO.setFunction(varcom.rlse);
		a5000CallZmtm();
	}

protected void validateSubfile2600()
	{
		validation2610();
		updateErrorIndicators2670();
	}

protected void validation2610()
	{
		if (isEQ(sv.zmedtyp, SPACES)) {
			sv.longdesc.set(SPACES);
		}
		/*    Facility is windowing to table TR639.*/
		if (isNE(sv.zmedtyp, SPACES)) {
			itemIO.setParams(SPACES);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr640);
			itemIO.setItemitem(sv.zmedtyp);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				sv.zmedtypErr.set(errorsInner.rl66);
				return ;
			}
			else {
				tr640rec.tr640Rec.set(itemIO.getGenarea());
				descIO.setDataKey(SPACES);
				descIO.setDesctabl(tr640);
				descIO.setDescitem(sv.zmedtyp);
				a1000GetDesc();
				sv.longdesc.set(descIO.getLongdesc());
			}
		}
		if (isNE(sv.zmedfee, ZERO)
		&& isEQ(sv.zmedtyp, SPACES)) {
			sv.zmedfeeErr.set(errorsInner.rl89);
		}
		if (isNE(sv.zmedtyp, SPACES)
		&& isEQ(sv.zmedfee, ZERO)) {
			sv.zmedfee.set(tr640rec.zmedfee);
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		c1000CallSr633io();
		scrnparams.function.set(varcom.srdn);
		c1000CallSr633io();
		/*EXIT*/
	}

protected void checkDuplicate2700()
	{
		start2710();
	}

protected void start2710()
	{
		wsaaSubfileRrn.set(1);
		scrnparams.subfileRrn.set(wsaaSubfileRrn);
		scrnparams.function.set(varcom.sread);
		c1000CallSr633io();
		wsaaDuplicateFound.set("N");
		while ( !(isNE(scrnparams.statuz, varcom.oK)
		|| duplicateFound.isTrue())) {
			wsaaTempZmedtyp.set(sv.zmedtyp);
			if (isNE(sv.zmedtyp, SPACES)) {
				wsaaSubfileRrnD.set(wsaaSubfileRrn);
				wsaaSubfileRrnD.add(1);
				scrnparams.subfileRrn.set(wsaaSubfileRrnD);
				scrnparams.function.set(varcom.sread);
				c1000CallSr633io();
				while ( !(isNE(scrnparams.statuz, varcom.oK)
				|| duplicateFound.isTrue())) {
					if (isNE(wsaaSubfileRrn, scrnparams.subfileRrn)
					&& isEQ(wsaaTempZmedtyp, sv.zmedtyp)) {
						wsspcomn.edterror.set("Y");
						wsaaDuplicateFound.set("Y");
						sv.zmedtypErr.set(errorsInner.e048);
						if (isNE(sv.errorSubfile, SPACES)) {
							wsspcomn.edterror.set("Y");
						}
						scrnparams.function.set(varcom.supd);
						c1000CallSr633io();
					}
					else {
						wsaaSubfileRrnD.add(1);
						scrnparams.subfileRrn.set(wsaaSubfileRrnD);
						scrnparams.function.set(varcom.sread);
						c1000CallSr633io();
					}
				}
				
			}
			if (!duplicateFound.isTrue()) {
				wsaaSubfileRrn.add(1);
				scrnparams.subfileRrn.set(wsaaSubfileRrn);
				scrnparams.function.set(varcom.sread);
				c1000CallSr633io();
			}
		}
		
	}

protected void protectSubfile2800()
	{
		start2810();
	}

protected void start2810()
	{
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		c1000CallSr633io();
		while ( !(isNE(scrnparams.statuz, varcom.oK))) {
			if (isNE(sv.dtetrm, varcom.vrcmMaxDate)) {
				sv.zmedtypOut[varcom.pr.toInt()].set("Y");
			}
			else {
				sv.zmedtypOut[varcom.pr.toInt()].set("N");
			}
			scrnparams.function.set(varcom.supd);
			c1000CallSr633io();
			scrnparams.function.set(varcom.srdn);
			c1000CallSr633io();
		}
		
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz, varcom.kill)
		|| isEQ(wsspcomn.flag, "I")
		|| isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")
		|| (isNE(sv.xoptind, SPACES)
		&& isNE(sv.xoptind, "+"))
		|| (isNE(sv.optind01, SPACES)
		&& isNE(sv.optind01, "+"))
		|| (isNE(sv.optind02, SPACES)
		&& isNE(sv.optind02, "+"))
		|| (isNE(sv.optind03, SPACES)
		&& isNE(sv.optind03, "+"))) {
			return ;
		}
		zmpvIO.setFunction(varcom.rlse);
		a2000CallZmpv();
		/*    Update ZMPV*/
		zmpvIO.setParams(SPACES);
		zmpvIO.setStatuz(varcom.oK);
		zmpvIO.setZmedcoy(wsspcomn.company);
		zmpvIO.setZmedprv(sv.zmedprv);
		zmpvIO.setFunction(varcom.readh);
		a2000CallZmpv();
		zmpvIO.setZmedcoy(wsspcomn.company);
		zmpvIO.setZmedprv(sv.zmedprv);
		zmpvIO.setZmednum(sv.zmednum);
		zmpvIO.setZmednam(sv.zmednam);
		zmpvIO.setZmedsts(sv.zmedsts);
		zmpvIO.setDteapp(sv.dteapp);
		zmpvIO.setDtetrm(sv.dtetrm);
		zmpvIO.setZdesc(sv.zdesc);
		zmpvIO.setPaymth(sv.paymth);
		zmpvIO.setBankkey(sv.bankkey);
		zmpvIO.setBankacckey(sv.bankacckey);
		zmpvIO.setZaracde(sv.zaracde);
		if (isEQ(zmpvIO.getStatuz(), varcom.oK)) {
			zmpvIO.setFunction(varcom.rewrt);
		}
		else {
			zmpvIO.setFunction(varcom.writr);
		}
		zmpvIO.setFormat(zmpvrec);
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zmpvIO.getParams());
			syserrrec.statuz.set(zmpvIO.getStatuz());
			fatalError600();
		}
		/*    Update ZMPF*/
		zmpfIO.setParams(SPACES);
		zmpfIO.setZmedcoy(wsspcomn.company);
		zmpfIO.setZmedprv(sv.zmedprv);
		zmpfIO.setSeqno(1);
		zmpfIO.setFunction(varcom.begnh);
		a3000CallZmpf();
		while ( !(isNE(zmpfIO.getStatuz(), varcom.oK)
		|| isNE(zmpfIO.getZmedcoy(), wsspcomn.company)
		|| isNE(zmpfIO.getZmedprv(), sv.zmedprv))) {
			zmpfIO.setFunction(varcom.delet);
			a3000CallZmpf();
			zmpfIO.setFunction(varcom.nextr);
			a3000CallZmpf();
		}
		
		zmpfIO.setFunction(varcom.rlse);
		a3000CallZmpf();
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		c1000CallSr633io();
		wsaaSeqno.set(1);
		while ( !(isNE(scrnparams.statuz, varcom.oK))) {
			if (isNE(sv.zmedtyp, SPACES)) {
				zmpfIO.setZmedcoy(wsspcomn.company);
				zmpfIO.setZmedprv(sv.zmedprv);
				zmpfIO.setSeqno(wsaaSeqno);
				zmpfIO.setZmedtyp(sv.zmedtyp);
				zmpfIO.setZmedfee(sv.zmedfee);
				zmpfIO.setFunction(varcom.writr);
				a3000CallZmpf();
				wsaaSeqno.add(1);
			}
			scrnparams.function.set(varcom.srdn);
			processScreen("SR633", sv);
			if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		
		/*    Unlock Provider*/
		unlockProvider3300();
	}

protected void callZmpf3200()
	{
		/*START*/
		SmartFileCode.execute(appVars, zmpfIO);
		if (isNE(zmpfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zmpfIO.getParams());
			syserrrec.statuz.set(zmpfIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void unlockProvider3300()
	{
		start3310();
	}

protected void start3310()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(sv.zmedprv);
		sftlockrec.enttyp.set("MP");
		sftlockrec.transaction.set(wsspcomn.batchkey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					nextProgram4010();
				case stck4070: 
					stck4070();
					indics4080();
				case exit4090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			zmpvIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, zmpvIO);
			if (isNE(zmpvIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(zmpvIO.getParams());
				syserrrec.statuz.set(zmpvIO.getStatuz());
				fatalError600();
			}
			sv.bankkey.set(zmpvIO.getBankkey());
			sv.bankacckey.set(zmpvIO.getBankacckey());
		}
		wsspcomn.nextprog.set(wsaaProg);
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		/* Termination Reason Check Box.*/
		if (isEQ(sv.xoptind, "X")) {
			zmpvIO.setZmednum(sv.zmednum);
			zmpvIO.setDtetrm(sv.dtetrm);
			zmpvIO.setFunction(varcom.keeps);
			a2000CallZmpv();
			optswchrec.optsInd[1].set(sv.xoptind);
			sv.xoptind.set(SPACES);
			goTo(GotoLabel.stck4070);
		}
		/*    Check Box at Bottom Screen*/
		wsaaFoundCheckBox.set("N");
		iy.set(2);
		for (ix.set(1); !(isGT(ix, 3)
		|| foundCheckBox.isTrue()); ix.add(1)){
			if (isEQ(sv.optind[ix.toInt()], "X")) {
				optswchrec.optsInd[iy.toInt()].set(sv.optind[ix.toInt()]);
				sv.optind[ix.toInt()].set(SPACES);
				wsaaFoundCheckBox.set("Y");
				if (isEQ(ix, 1)) {
					wsspcomn.flag.set("I");
				}
				if (isEQ(ix, 2)
				|| isEQ(ix, 3)) {
					zmpvIO.setFunction(varcom.rlse);
					a2000CallZmpv();
					zmpvIO.setZmednam(sv.zmednam);
					zmpvIO.setZmednum(sv.zmednum);
					zmpvIO.setDtetrm(sv.dtetrm);
					zmpvIO.setDteapp(sv.dteapp);
					zmpvIO.setFunction(varcom.keeps);
					a2000CallZmpv();
				}
			}
			iy.add(1);
		}
	}

protected void stck4070()
	{
		optswchrec.optsSelType.set(SPACES);
		optswchrec.optsSelOptno.set(ZERO);
		optswchrec.optsSelCode.set("CBOX");
		optswchrec.optsFunction.set("STCK");
		b1000CallOptswch();
	}

protected void indics4080()
	{
		/*    Puts back the '+' or spaces if returning back*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], " ")
		&& isEQ(optswchrec.optsStatuz, varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
			optswchrec.optsFunction.set("INIT");
			optswchrec.optsCallingProg.set(wsaaProg);
			optswchrec.optsDteeff.set(ZERO);
			optswchrec.optsCompany.set(wsspcomn.company);
			optswchrec.optsLanguage.set(wsspcomn.language);
			varcom.vrcmTranid.set(wsspcomn.tranid);
			optswchrec.optsUser.set(varcom.vrcmUser);
			b1000CallOptswch();
			sv.xoptind.set(optswchrec.optsInd[1]);
			iy.set(1);
			for (ix.set(2); !(isGT(ix, 4)
			|| isEQ(optswchrec.optsType[ix.toInt()], SPACES)); ix.add(1)){
				sv.optind[iy.toInt()].set(optswchrec.optsInd[ix.toInt()]);
				iy.add(1);
			}
			wsspcomn.flag.set(wsaaOrgFlag);
			sv.optind01.set("+");
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

protected void keepsZmpv4100()
	{
		/*START*/
		zmpvIO.setDataKey(SPACES);
		zmpvIO.setStatuz(varcom.oK);
		zmpvIO.setZmedcoy(wsspcomn.company);
		zmpvIO.setZmedprv(sv.zmedprv);
		zmpvIO.setFormat(zmpvrec);
		zmpvIO.setFunction(varcom.readr);
		a2000CallZmpv();
		zmpvIO.setFunction(varcom.keeps);
		a2000CallZmpv();
		/*EXIT*/
	}

protected void a1000GetDesc()
	{
		a1010Start();
	}

protected void a1010Start()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc("??????????????????????????????");
		}
	}

protected void a2000CallZmpv()
	{
		/*A2010-START*/
		zmpvIO.setFormat(zmpvrec);
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(), varcom.oK)
		&& isNE(zmpvIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(zmpvIO.getParams());
			syserrrec.statuz.set(zmpvIO.getStatuz());
			fatalError600();
		}
		/*A2090-EXIT*/
	}

protected void a3000CallZmpf()
	{
		/*A3010-START*/
		zmpfIO.setFormat(zmpfrec);
		SmartFileCode.execute(appVars, zmpfIO);
		if (isNE(zmpfIO.getStatuz(), varcom.oK)
		&& isNE(zmpfIO.getStatuz(), varcom.endp)
		&& isNE(zmpfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(zmpfIO.getParams());
			syserrrec.statuz.set(zmpfIO.getStatuz());
			fatalError600();
		}
		/*A3090-EXIT*/
	}

protected void a4000CallZmpd()
	{
		/*A4010-START*/
		zmpdIO.setFormat(zmpdrec);
		SmartFileCode.execute(appVars, zmpdIO);
		if (isNE(zmpdIO.getStatuz(), varcom.oK)
		&& isNE(zmpdIO.getStatuz(), varcom.endp)
		&& isNE(zmpdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(zmpdIO.getParams());
			syserrrec.statuz.set(zmpdIO.getStatuz());
			fatalError600();
		}
		/*A4090-EXIT*/
	}

protected void a5000CallZmtm()
	{
		/*A5010-START*/
		zmtmIO.setFormat(zmtmrec);
		SmartFileCode.execute(appVars, zmtmIO);
		if (isNE(zmtmIO.getStatuz(), varcom.oK)
		&& isNE(zmtmIO.getStatuz(), varcom.endp)
		&& isNE(zmtmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(zmtmIO.getParams());
			syserrrec.statuz.set(zmtmIO.getStatuz());
			fatalError600();
		}
		/*A5090-EXIT*/
	}

protected void b1000CallOptswch()
	{
		/*B1010-START*/
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)
		&& isNE(optswchrec.optsStatuz, varcom.endp)) {
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		/*B1090-EXIT*/
	}

protected void c1000CallSr633io()
	{
		/*C1010-START*/
		processScreen("SR633", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)
		&& isNE(scrnparams.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*C1090-EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e048 = new FixedLengthStringData(4).init("E048");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e655 = new FixedLengthStringData(4).init("E655");
	private FixedLengthStringData f020 = new FixedLengthStringData(4).init("F020");
	private FixedLengthStringData g499 = new FixedLengthStringData(4).init("G499");
	private FixedLengthStringData h067 = new FixedLengthStringData(4).init("H067");
	private FixedLengthStringData h939 = new FixedLengthStringData(4).init("H939");
	private FixedLengthStringData rl64 = new FixedLengthStringData(4).init("RL64");
	private FixedLengthStringData rl65 = new FixedLengthStringData(4).init("RL65");
	private FixedLengthStringData rl66 = new FixedLengthStringData(4).init("RL66");
	private FixedLengthStringData rl89 = new FixedLengthStringData(4).init("RL89");
	private FixedLengthStringData rl90 = new FixedLengthStringData(4).init("RL90");
}
}
