package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr562screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	//public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr562ScreenVars sv = (Sr562ScreenVars) pv;
		//clearInds(av, pfInds);
		write(lrec, sv.Sr562screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr562ScreenVars screenVars = (Sr562ScreenVars)pv;
		/*screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.descrip.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.cnRiskStat.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.register.setClassString("");
		screenVars.cnPremStat.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.prat.setClassString("");
		screenVars.prmdetails.setClassString("");
		screenVars.mlinsopt.setClassString("");
		screenVars.mlresind.setClassString("");
		screenVars.coverc.setClassString("");
		screenVars.mbnkref.setClassString("");
		BRD-139
		screenVars.loandur.setClassString("");
		screenVars.intcaltype.setClassString("");
		screenVars.mlresindvpms.setClassString("");*/
		/*BRD-139*/
		ScreenRecord.setClassStringFormatting(pv);
	}

/**
 * Clear all the variables in Sr562screen
 */
	public static void clear(VarModel pv) {
		Sr562ScreenVars screenVars = (Sr562ScreenVars) pv;
		/*screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.descrip.clear();
		screenVars.cntcurr.clear();
		screenVars.cnRiskStat.clear();
		screenVars.rstate.clear();
		screenVars.register.clear();
		screenVars.cnPremStat.clear();
		screenVars.pstate.clear();
		screenVars.prat.clear();
		screenVars.prmdetails.clear();
		screenVars.mlinsopt.clear();
		screenVars.mlresind.clear();
		screenVars.coverc.clear();
		screenVars.mbnkref.clear();
		BRD-139
		screenVars.loandur.clear();
		screenVars.intcaltype.clear();
		screenVars.mlresindvpms.clear();*/
		/*BRD-139*/
		ScreenRecord.clear(pv);
	}
}
