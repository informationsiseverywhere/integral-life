package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6650screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6650ScreenVars sv = (S6650ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6650screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6650ScreenVars screenVars = (S6650ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.sumin01.setClassString("");
		screenVars.fact01.setClassString("");
		screenVars.rrat01.setClassString("");
		screenVars.sumin02.setClassString("");
		screenVars.fact02.setClassString("");
		screenVars.rrat02.setClassString("");
		screenVars.sumin03.setClassString("");
		screenVars.fact03.setClassString("");
		screenVars.rrat03.setClassString("");
		screenVars.sumin04.setClassString("");
		screenVars.fact04.setClassString("");
		screenVars.rrat04.setClassString("");
		screenVars.sumin05.setClassString("");
		screenVars.fact05.setClassString("");
		screenVars.rrat05.setClassString("");
		screenVars.sumin06.setClassString("");
		screenVars.fact06.setClassString("");
		screenVars.rrat06.setClassString("");
	}

/**
 * Clear all the variables in S6650screen
 */
	public static void clear(VarModel pv) {
		S6650ScreenVars screenVars = (S6650ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.sumin01.clear();
		screenVars.fact01.clear();
		screenVars.rrat01.clear();
		screenVars.sumin02.clear();
		screenVars.fact02.clear();
		screenVars.rrat02.clear();
		screenVars.sumin03.clear();
		screenVars.fact03.clear();
		screenVars.rrat03.clear();
		screenVars.sumin04.clear();
		screenVars.fact04.clear();
		screenVars.rrat04.clear();
		screenVars.sumin05.clear();
		screenVars.fact05.clear();
		screenVars.rrat05.clear();
		screenVars.sumin06.clear();
		screenVars.fact06.clear();
		screenVars.rrat06.clear();
	}
}
