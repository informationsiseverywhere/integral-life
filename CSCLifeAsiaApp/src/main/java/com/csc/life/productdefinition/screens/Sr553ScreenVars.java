package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR553
 * @version 1.0 generated on 30/08/09 07:19
 * @author Quipoz
 */
public class Sr553ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(108);
	public FixedLengthStringData dataFields = new FixedLengthStringData(60).isAPartOf(dataArea, 0);
	public FixedLengthStringData actn = DD.actn.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData clntnaml = DD.clntnaml.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData reptname = DD.reptname.copy().isAPartOf(dataFields,52);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 60);
	public FixedLengthStringData actnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData clntnamlErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData reptnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 72);
	public FixedLengthStringData[] actnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] clntnamlOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] reptnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(155);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(73).isAPartOf(subfileArea, 0);
	public FixedLengthStringData clntlname = DD.clntlname.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData flag = DD.flag.copy().isAPartOf(subfileFields,40);
	public FixedLengthStringData idno = DD.idno.copy().isAPartOf(subfileFields,41);
	public FixedLengthStringData mlentity = DD.mlentity.copy().isAPartOf(subfileFields,56);
	public FixedLengthStringData optind = DD.optind.copy().isAPartOf(subfileFields,72);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(20).isAPartOf(subfileArea, 73);
	public FixedLengthStringData clntlnameErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData flagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData idnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData mlentityErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData optindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(60).isAPartOf(subfileArea, 93);
	public FixedLengthStringData[] clntlnameOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] flagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] idnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] mlentityOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] optindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 153);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData Sr553screensflWritten = new LongData(0);
	public LongData Sr553screenctlWritten = new LongData(0);
	public LongData Sr553screenWritten = new LongData(0);
	public LongData Sr553protectWritten = new LongData(0);
	public GeneralTable sr553screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr553screensfl;
	}

	public Sr553ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(optindOut,new String[] {"02","03","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(actnOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {optind, clntlname, idno, mlentity, flag};
		screenSflOutFields = new BaseData[][] {optindOut, clntlnameOut, idnoOut, mlentityOut, flagOut};
		screenSflErrFields = new BaseData[] {optindErr, clntlnameErr, idnoErr, mlentityErr, flagErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {actn, clntnaml, reptname};
		screenOutFields = new BaseData[][] {actnOut, clntnamlOut, reptnameOut};
		screenErrFields = new BaseData[] {actnErr, clntnamlErr, reptnameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr553screen.class;
		screenSflRecord = Sr553screensfl.class;
		screenCtlRecord = Sr553screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr553protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr553screenctl.lrec.pageSubfile);
	}
}
