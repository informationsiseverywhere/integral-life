package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5667
 * @version 1.0 generated on 30/08/09 06:47
 * @author Quipoz
 */
public class S5667ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1489);
	public FixedLengthStringData dataFields = new FixedLengthStringData(529).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData freqs = new FixedLengthStringData(22).isAPartOf(dataFields, 1);
	public FixedLengthStringData[] freq = FLSArrayPartOfStructure(11, 2, freqs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(22).isAPartOf(freqs, 0, FILLER_REDEFINE);
	public FixedLengthStringData freq01 = DD.freq.copy().isAPartOf(filler,0);
	public FixedLengthStringData freq02 = DD.freq.copy().isAPartOf(filler,2);
	public FixedLengthStringData freq03 = DD.freq.copy().isAPartOf(filler,4);
	public FixedLengthStringData freq04 = DD.freq.copy().isAPartOf(filler,6);
	public FixedLengthStringData freq05 = DD.freq.copy().isAPartOf(filler,8);
	public FixedLengthStringData freq06 = DD.freq.copy().isAPartOf(filler,10);
	public FixedLengthStringData freq07 = DD.freq.copy().isAPartOf(filler,12);
	public FixedLengthStringData freq08 = DD.freq.copy().isAPartOf(filler,14);
	public FixedLengthStringData freq09 = DD.freq.copy().isAPartOf(filler,16);
	public FixedLengthStringData freq10 = DD.freq.copy().isAPartOf(filler,18);
	public FixedLengthStringData freq11 = DD.freq.copy().isAPartOf(filler,20);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,23);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,31);
	public FixedLengthStringData maxAmounts = new FixedLengthStringData(187).isAPartOf(dataFields, 61);
	public ZonedDecimalData[] maxAmount = ZDArrayPartOfStructure(11, 17, 2, maxAmounts, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(187).isAPartOf(maxAmounts, 0, FILLER_REDEFINE);
	public ZonedDecimalData maxAmount01 = DD.maxamnt.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData maxAmount02 = DD.maxamnt.copyToZonedDecimal().isAPartOf(filler1,17);
	public ZonedDecimalData maxAmount03 = DD.maxamnt.copyToZonedDecimal().isAPartOf(filler1,34);
	public ZonedDecimalData maxAmount04 = DD.maxamnt.copyToZonedDecimal().isAPartOf(filler1,51);
	public ZonedDecimalData maxAmount05 = DD.maxamnt.copyToZonedDecimal().isAPartOf(filler1,68);
	public ZonedDecimalData maxAmount06 = DD.maxamnt.copyToZonedDecimal().isAPartOf(filler1,85);
	public ZonedDecimalData maxAmount07 = DD.maxamnt.copyToZonedDecimal().isAPartOf(filler1,102);
	public ZonedDecimalData maxAmount08 = DD.maxamnt.copyToZonedDecimal().isAPartOf(filler1,119);
	public ZonedDecimalData maxAmount09 = DD.maxamnt.copyToZonedDecimal().isAPartOf(filler1,136);
	public ZonedDecimalData maxAmount10 = DD.maxamnt.copyToZonedDecimal().isAPartOf(filler1,153);
	public ZonedDecimalData maxAmount11 = DD.maxamnt.copyToZonedDecimal().isAPartOf(filler1,170);
	public FixedLengthStringData maxamts = new FixedLengthStringData(187).isAPartOf(dataFields, 248);
	public ZonedDecimalData[] maxamt = ZDArrayPartOfStructure(11, 17, 2, maxamts, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(187).isAPartOf(maxamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData maxamt01 = DD.maxamt.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData maxamt02 = DD.maxamt.copyToZonedDecimal().isAPartOf(filler2,17);
	public ZonedDecimalData maxamt03 = DD.maxamt.copyToZonedDecimal().isAPartOf(filler2,34);
	public ZonedDecimalData maxamt04 = DD.maxamt.copyToZonedDecimal().isAPartOf(filler2,51);
	public ZonedDecimalData maxamt05 = DD.maxamt.copyToZonedDecimal().isAPartOf(filler2,68);
	public ZonedDecimalData maxamt06 = DD.maxamt.copyToZonedDecimal().isAPartOf(filler2,85);
	public ZonedDecimalData maxamt07 = DD.maxamt.copyToZonedDecimal().isAPartOf(filler2,102);
	public ZonedDecimalData maxamt08 = DD.maxamt.copyToZonedDecimal().isAPartOf(filler2,119);
	public ZonedDecimalData maxamt09 = DD.maxamt.copyToZonedDecimal().isAPartOf(filler2,136);
	public ZonedDecimalData maxamt10 = DD.maxamt.copyToZonedDecimal().isAPartOf(filler2,153);
	public ZonedDecimalData maxamt11 = DD.maxamt.copyToZonedDecimal().isAPartOf(filler2,170);
	public FixedLengthStringData prmtolns = new FixedLengthStringData(44).isAPartOf(dataFields, 435);
	public ZonedDecimalData[] prmtoln = ZDArrayPartOfStructure(11, 4, 2, prmtolns, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(44).isAPartOf(prmtolns, 0, FILLER_REDEFINE);
	public ZonedDecimalData prmtoln01 = DD.prmtoln.copyToZonedDecimal().isAPartOf(filler3,0);
	public ZonedDecimalData prmtoln02 = DD.prmtoln.copyToZonedDecimal().isAPartOf(filler3,4);
	public ZonedDecimalData prmtoln03 = DD.prmtoln.copyToZonedDecimal().isAPartOf(filler3,8);
	public ZonedDecimalData prmtoln04 = DD.prmtoln.copyToZonedDecimal().isAPartOf(filler3,12);
	public ZonedDecimalData prmtoln05 = DD.prmtoln.copyToZonedDecimal().isAPartOf(filler3,16);
	public ZonedDecimalData prmtoln06 = DD.prmtoln.copyToZonedDecimal().isAPartOf(filler3,20);
	public ZonedDecimalData prmtoln07 = DD.prmtoln.copyToZonedDecimal().isAPartOf(filler3,24);
	public ZonedDecimalData prmtoln08 = DD.prmtoln.copyToZonedDecimal().isAPartOf(filler3,28);
	public ZonedDecimalData prmtoln09 = DD.prmtoln.copyToZonedDecimal().isAPartOf(filler3,32);
	public ZonedDecimalData prmtoln10 = DD.prmtoln.copyToZonedDecimal().isAPartOf(filler3,36);
	public ZonedDecimalData prmtoln11 = DD.prmtoln.copyToZonedDecimal().isAPartOf(filler3,40);
	public FixedLengthStringData prmtols = new FixedLengthStringData(44).isAPartOf(dataFields, 479);
	public ZonedDecimalData[] prmtol = ZDArrayPartOfStructure(11, 4, 2, prmtols, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(44).isAPartOf(prmtols, 0, FILLER_REDEFINE);
	public ZonedDecimalData prmtol01 = DD.prmtol.copyToZonedDecimal().isAPartOf(filler4,0);
	public ZonedDecimalData prmtol02 = DD.prmtol.copyToZonedDecimal().isAPartOf(filler4,4);
	public ZonedDecimalData prmtol03 = DD.prmtol.copyToZonedDecimal().isAPartOf(filler4,8);
	public ZonedDecimalData prmtol04 = DD.prmtol.copyToZonedDecimal().isAPartOf(filler4,12);
	public ZonedDecimalData prmtol05 = DD.prmtol.copyToZonedDecimal().isAPartOf(filler4,16);
	public ZonedDecimalData prmtol06 = DD.prmtol.copyToZonedDecimal().isAPartOf(filler4,20);
	public ZonedDecimalData prmtol07 = DD.prmtol.copyToZonedDecimal().isAPartOf(filler4,24);
	public ZonedDecimalData prmtol08 = DD.prmtol.copyToZonedDecimal().isAPartOf(filler4,28);
	public ZonedDecimalData prmtol09 = DD.prmtol.copyToZonedDecimal().isAPartOf(filler4,32);
	public ZonedDecimalData prmtol10 = DD.prmtol.copyToZonedDecimal().isAPartOf(filler4,36);
	public ZonedDecimalData prmtol11 = DD.prmtol.copyToZonedDecimal().isAPartOf(filler4,40);
	public FixedLengthStringData sfind = DD.sfind.copy().isAPartOf(dataFields,523);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,524);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(240).isAPartOf(dataArea, 529);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData freqsErr = new FixedLengthStringData(44).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] freqErr = FLSArrayPartOfStructure(11, 4, freqsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(44).isAPartOf(freqsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData freq01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData freq02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData freq03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData freq04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData freq05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData freq06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData freq07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData freq08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData freq09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData freq10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData freq11Err = new FixedLengthStringData(4).isAPartOf(filler5, 40);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData maxamntsErr = new FixedLengthStringData(44).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData[] maxamntErr = FLSArrayPartOfStructure(11, 4, maxamntsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(44).isAPartOf(maxamntsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData maxamnt01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData maxamnt02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData maxamnt03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData maxamnt04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData maxamnt05Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);
	public FixedLengthStringData maxamnt06Err = new FixedLengthStringData(4).isAPartOf(filler6, 20);
	public FixedLengthStringData maxamnt07Err = new FixedLengthStringData(4).isAPartOf(filler6, 24);
	public FixedLengthStringData maxamnt08Err = new FixedLengthStringData(4).isAPartOf(filler6, 28);
	public FixedLengthStringData maxamnt09Err = new FixedLengthStringData(4).isAPartOf(filler6, 32);
	public FixedLengthStringData maxamnt10Err = new FixedLengthStringData(4).isAPartOf(filler6, 36);
	public FixedLengthStringData maxamnt11Err = new FixedLengthStringData(4).isAPartOf(filler6, 40);
	public FixedLengthStringData maxamtsErr = new FixedLengthStringData(44).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData[] maxamtErr = FLSArrayPartOfStructure(11, 4, maxamtsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(44).isAPartOf(maxamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData maxamt01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData maxamt02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData maxamt03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData maxamt04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData maxamt05Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);
	public FixedLengthStringData maxamt06Err = new FixedLengthStringData(4).isAPartOf(filler7, 20);
	public FixedLengthStringData maxamt07Err = new FixedLengthStringData(4).isAPartOf(filler7, 24);
	public FixedLengthStringData maxamt08Err = new FixedLengthStringData(4).isAPartOf(filler7, 28);
	public FixedLengthStringData maxamt09Err = new FixedLengthStringData(4).isAPartOf(filler7, 32);
	public FixedLengthStringData maxamt10Err = new FixedLengthStringData(4).isAPartOf(filler7, 36);
	public FixedLengthStringData maxamt11Err = new FixedLengthStringData(4).isAPartOf(filler7, 40);
	public FixedLengthStringData prmtolnsErr = new FixedLengthStringData(44).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData[] prmtolnErr = FLSArrayPartOfStructure(11, 4, prmtolnsErr, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(44).isAPartOf(prmtolnsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData prmtoln01Err = new FixedLengthStringData(4).isAPartOf(filler8, 0);
	public FixedLengthStringData prmtoln02Err = new FixedLengthStringData(4).isAPartOf(filler8, 4);
	public FixedLengthStringData prmtoln03Err = new FixedLengthStringData(4).isAPartOf(filler8, 8);
	public FixedLengthStringData prmtoln04Err = new FixedLengthStringData(4).isAPartOf(filler8, 12);
	public FixedLengthStringData prmtoln05Err = new FixedLengthStringData(4).isAPartOf(filler8, 16);
	public FixedLengthStringData prmtoln06Err = new FixedLengthStringData(4).isAPartOf(filler8, 20);
	public FixedLengthStringData prmtoln07Err = new FixedLengthStringData(4).isAPartOf(filler8, 24);
	public FixedLengthStringData prmtoln08Err = new FixedLengthStringData(4).isAPartOf(filler8, 28);
	public FixedLengthStringData prmtoln09Err = new FixedLengthStringData(4).isAPartOf(filler8, 32);
	public FixedLengthStringData prmtoln10Err = new FixedLengthStringData(4).isAPartOf(filler8, 36);
	public FixedLengthStringData prmtoln11Err = new FixedLengthStringData(4).isAPartOf(filler8, 40);
	public FixedLengthStringData prmtolsErr = new FixedLengthStringData(44).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData[] prmtolErr = FLSArrayPartOfStructure(11, 4, prmtolsErr, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(44).isAPartOf(prmtolsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData prmtol01Err = new FixedLengthStringData(4).isAPartOf(filler9, 0);
	public FixedLengthStringData prmtol02Err = new FixedLengthStringData(4).isAPartOf(filler9, 4);
	public FixedLengthStringData prmtol03Err = new FixedLengthStringData(4).isAPartOf(filler9, 8);
	public FixedLengthStringData prmtol04Err = new FixedLengthStringData(4).isAPartOf(filler9, 12);
	public FixedLengthStringData prmtol05Err = new FixedLengthStringData(4).isAPartOf(filler9, 16);
	public FixedLengthStringData prmtol06Err = new FixedLengthStringData(4).isAPartOf(filler9, 20);
	public FixedLengthStringData prmtol07Err = new FixedLengthStringData(4).isAPartOf(filler9, 24);
	public FixedLengthStringData prmtol08Err = new FixedLengthStringData(4).isAPartOf(filler9, 28);
	public FixedLengthStringData prmtol09Err = new FixedLengthStringData(4).isAPartOf(filler9, 32);
	public FixedLengthStringData prmtol10Err = new FixedLengthStringData(4).isAPartOf(filler9, 36);
	public FixedLengthStringData prmtol11Err = new FixedLengthStringData(4).isAPartOf(filler9, 40);
	public FixedLengthStringData sfindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 232);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 236);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(720).isAPartOf(dataArea, 769);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData freqsOut = new FixedLengthStringData(132).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] freqOut = FLSArrayPartOfStructure(11, 12, freqsOut, 0);
	public FixedLengthStringData[][] freqO = FLSDArrayPartOfArrayStructure(12, 1, freqOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(132).isAPartOf(freqsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] freq01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] freq02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] freq03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] freq04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] freq05Out = FLSArrayPartOfStructure(12, 1, filler10, 48);
	public FixedLengthStringData[] freq06Out = FLSArrayPartOfStructure(12, 1, filler10, 60);
	public FixedLengthStringData[] freq07Out = FLSArrayPartOfStructure(12, 1, filler10, 72);
	public FixedLengthStringData[] freq08Out = FLSArrayPartOfStructure(12, 1, filler10, 84);
	public FixedLengthStringData[] freq09Out = FLSArrayPartOfStructure(12, 1, filler10, 96);
	public FixedLengthStringData[] freq10Out = FLSArrayPartOfStructure(12, 1, filler10, 108);
	public FixedLengthStringData[] freq11Out = FLSArrayPartOfStructure(12, 1, filler10, 120);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData maxamntsOut = new FixedLengthStringData(132).isAPartOf(outputIndicators, 168);
	public FixedLengthStringData[] maxamntOut = FLSArrayPartOfStructure(11, 12, maxamntsOut, 0);
	public FixedLengthStringData[][] maxamntO = FLSDArrayPartOfArrayStructure(12, 1, maxamntOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(132).isAPartOf(maxamntsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] maxamnt01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] maxamnt02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] maxamnt03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] maxamnt04Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public FixedLengthStringData[] maxamnt05Out = FLSArrayPartOfStructure(12, 1, filler11, 48);
	public FixedLengthStringData[] maxamnt06Out = FLSArrayPartOfStructure(12, 1, filler11, 60);
	public FixedLengthStringData[] maxamnt07Out = FLSArrayPartOfStructure(12, 1, filler11, 72);
	public FixedLengthStringData[] maxamnt08Out = FLSArrayPartOfStructure(12, 1, filler11, 84);
	public FixedLengthStringData[] maxamnt09Out = FLSArrayPartOfStructure(12, 1, filler11, 96);
	public FixedLengthStringData[] maxamnt10Out = FLSArrayPartOfStructure(12, 1, filler11, 108);
	public FixedLengthStringData[] maxamnt11Out = FLSArrayPartOfStructure(12, 1, filler11, 120);
	public FixedLengthStringData maxamtsOut = new FixedLengthStringData(132).isAPartOf(outputIndicators, 300);
	public FixedLengthStringData[] maxamtOut = FLSArrayPartOfStructure(11, 12, maxamtsOut, 0);
	public FixedLengthStringData[][] maxamtO = FLSDArrayPartOfArrayStructure(12, 1, maxamtOut, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(132).isAPartOf(maxamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] maxamt01Out = FLSArrayPartOfStructure(12, 1, filler12, 0);
	public FixedLengthStringData[] maxamt02Out = FLSArrayPartOfStructure(12, 1, filler12, 12);
	public FixedLengthStringData[] maxamt03Out = FLSArrayPartOfStructure(12, 1, filler12, 24);
	public FixedLengthStringData[] maxamt04Out = FLSArrayPartOfStructure(12, 1, filler12, 36);
	public FixedLengthStringData[] maxamt05Out = FLSArrayPartOfStructure(12, 1, filler12, 48);
	public FixedLengthStringData[] maxamt06Out = FLSArrayPartOfStructure(12, 1, filler12, 60);
	public FixedLengthStringData[] maxamt07Out = FLSArrayPartOfStructure(12, 1, filler12, 72);
	public FixedLengthStringData[] maxamt08Out = FLSArrayPartOfStructure(12, 1, filler12, 84);
	public FixedLengthStringData[] maxamt09Out = FLSArrayPartOfStructure(12, 1, filler12, 96);
	public FixedLengthStringData[] maxamt10Out = FLSArrayPartOfStructure(12, 1, filler12, 108);
	public FixedLengthStringData[] maxamt11Out = FLSArrayPartOfStructure(12, 1, filler12, 120);
	public FixedLengthStringData prmtolnsOut = new FixedLengthStringData(132).isAPartOf(outputIndicators, 432);
	public FixedLengthStringData[] prmtolnOut = FLSArrayPartOfStructure(11, 12, prmtolnsOut, 0);
	public FixedLengthStringData[][] prmtolnO = FLSDArrayPartOfArrayStructure(12, 1, prmtolnOut, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(132).isAPartOf(prmtolnsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] prmtoln01Out = FLSArrayPartOfStructure(12, 1, filler13, 0);
	public FixedLengthStringData[] prmtoln02Out = FLSArrayPartOfStructure(12, 1, filler13, 12);
	public FixedLengthStringData[] prmtoln03Out = FLSArrayPartOfStructure(12, 1, filler13, 24);
	public FixedLengthStringData[] prmtoln04Out = FLSArrayPartOfStructure(12, 1, filler13, 36);
	public FixedLengthStringData[] prmtoln05Out = FLSArrayPartOfStructure(12, 1, filler13, 48);
	public FixedLengthStringData[] prmtoln06Out = FLSArrayPartOfStructure(12, 1, filler13, 60);
	public FixedLengthStringData[] prmtoln07Out = FLSArrayPartOfStructure(12, 1, filler13, 72);
	public FixedLengthStringData[] prmtoln08Out = FLSArrayPartOfStructure(12, 1, filler13, 84);
	public FixedLengthStringData[] prmtoln09Out = FLSArrayPartOfStructure(12, 1, filler13, 96);
	public FixedLengthStringData[] prmtoln10Out = FLSArrayPartOfStructure(12, 1, filler13, 108);
	public FixedLengthStringData[] prmtoln11Out = FLSArrayPartOfStructure(12, 1, filler13, 120);
	public FixedLengthStringData prmtolsOut = new FixedLengthStringData(132).isAPartOf(outputIndicators, 564);
	public FixedLengthStringData[] prmtolOut = FLSArrayPartOfStructure(11, 12, prmtolsOut, 0);
	public FixedLengthStringData[][] prmtolO = FLSDArrayPartOfArrayStructure(12, 1, prmtolOut, 0);
	public FixedLengthStringData filler14 = new FixedLengthStringData(132).isAPartOf(prmtolsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] prmtol01Out = FLSArrayPartOfStructure(12, 1, filler14, 0);
	public FixedLengthStringData[] prmtol02Out = FLSArrayPartOfStructure(12, 1, filler14, 12);
	public FixedLengthStringData[] prmtol03Out = FLSArrayPartOfStructure(12, 1, filler14, 24);
	public FixedLengthStringData[] prmtol04Out = FLSArrayPartOfStructure(12, 1, filler14, 36);
	public FixedLengthStringData[] prmtol05Out = FLSArrayPartOfStructure(12, 1, filler14, 48);
	public FixedLengthStringData[] prmtol06Out = FLSArrayPartOfStructure(12, 1, filler14, 60);
	public FixedLengthStringData[] prmtol07Out = FLSArrayPartOfStructure(12, 1, filler14, 72);
	public FixedLengthStringData[] prmtol08Out = FLSArrayPartOfStructure(12, 1, filler14, 84);
	public FixedLengthStringData[] prmtol09Out = FLSArrayPartOfStructure(12, 1, filler14, 96);
	public FixedLengthStringData[] prmtol10Out = FLSArrayPartOfStructure(12, 1, filler14, 108);
	public FixedLengthStringData[] prmtol11Out = FLSArrayPartOfStructure(12, 1, filler14, 120);
	public FixedLengthStringData[] sfindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 696);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 708);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5667screenWritten = new LongData(0);
	public LongData S5667protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5667ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(prmtol01Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamnt01Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtol02Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamnt02Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtol03Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamnt03Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtol04Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamnt04Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtol05Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamnt05Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtol06Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamnt06Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtol07Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamnt07Out,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtol08Out,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamnt08Out,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtol09Out,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamnt09Out,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtol10Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamnt10Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtol11Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamnt11Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freq01Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freq02Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freq03Out,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freq04Out,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freq05Out,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freq06Out,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freq07Out,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freq08Out,new String[] {"32",null, "-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freq09Out,new String[] {"33",null, "-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freq10Out,new String[] {"34",null, "-34",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freq11Out,new String[] {"35",null, "-35",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtoln01Out,new String[] {"36",null, "-36",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamt01Out,new String[] {"47",null, "-47",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtoln02Out,new String[] {"37",null, "-37",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamt02Out,new String[] {"48",null, "-48",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtoln03Out,new String[] {"38",null, "-38",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamt03Out,new String[] {"49",null, "-49",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtoln04Out,new String[] {"39",null, "-39",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamt04Out,new String[] {"50",null, "-50",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtoln05Out,new String[] {"40",null, "-40",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamt05Out,new String[] {"51",null, "-51",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtoln06Out,new String[] {"41",null, "-41",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamt06Out,new String[] {"52",null, "-52",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtoln07Out,new String[] {"42",null, "-42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamt07Out,new String[] {"53",null, "-53",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtoln08Out,new String[] {"43",null, "-43",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamt08Out,new String[] {"54",null, "-54",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtoln09Out,new String[] {"44",null, "-44",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamt09Out,new String[] {"55",null, "-55",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtoln10Out,new String[] {"45",null, "-45",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamt10Out,new String[] {"56",null, "-56",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmtoln11Out,new String[] {"46",null, "-46",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxamt11Out,new String[] {"57",null, "-57",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sfindOut,new String[] {"58",null, "-58",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, prmtol01, maxAmount01, prmtol02, maxAmount02, prmtol03, maxAmount03, prmtol04, maxAmount04, prmtol05, maxAmount05, prmtol06, maxAmount06, prmtol07, maxAmount07, prmtol08, maxAmount08, prmtol09, maxAmount09, prmtol10, maxAmount10, prmtol11, maxAmount11, freq01, freq02, freq03, freq04, freq05, freq06, freq07, freq08, freq09, freq10, freq11, prmtoln01, maxamt01, prmtoln02, maxamt02, prmtoln03, maxamt03, prmtoln04, maxamt04, prmtoln05, maxamt05, prmtoln06, maxamt06, prmtoln07, maxamt07, prmtoln08, maxamt08, prmtoln09, maxamt09, prmtoln10, maxamt10, prmtoln11, maxamt11, sfind};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, prmtol01Out, maxamnt01Out, prmtol02Out, maxamnt02Out, prmtol03Out, maxamnt03Out, prmtol04Out, maxamnt04Out, prmtol05Out, maxamnt05Out, prmtol06Out, maxamnt06Out, prmtol07Out, maxamnt07Out, prmtol08Out, maxamnt08Out, prmtol09Out, maxamnt09Out, prmtol10Out, maxamnt10Out, prmtol11Out, maxamnt11Out, freq01Out, freq02Out, freq03Out, freq04Out, freq05Out, freq06Out, freq07Out, freq08Out, freq09Out, freq10Out, freq11Out, prmtoln01Out, maxamt01Out, prmtoln02Out, maxamt02Out, prmtoln03Out, maxamt03Out, prmtoln04Out, maxamt04Out, prmtoln05Out, maxamt05Out, prmtoln06Out, maxamt06Out, prmtoln07Out, maxamt07Out, prmtoln08Out, maxamt08Out, prmtoln09Out, maxamt09Out, prmtoln10Out, maxamt10Out, prmtoln11Out, maxamt11Out, sfindOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, prmtol01Err, maxamnt01Err, prmtol02Err, maxamnt02Err, prmtol03Err, maxamnt03Err, prmtol04Err, maxamnt04Err, prmtol05Err, maxamnt05Err, prmtol06Err, maxamnt06Err, prmtol07Err, maxamnt07Err, prmtol08Err, maxamnt08Err, prmtol09Err, maxamnt09Err, prmtol10Err, maxamnt10Err, prmtol11Err, maxamnt11Err, freq01Err, freq02Err, freq03Err, freq04Err, freq05Err, freq06Err, freq07Err, freq08Err, freq09Err, freq10Err, freq11Err, prmtoln01Err, maxamt01Err, prmtoln02Err, maxamt02Err, prmtoln03Err, maxamt03Err, prmtoln04Err, maxamt04Err, prmtoln05Err, maxamt05Err, prmtoln06Err, maxamt06Err, prmtoln07Err, maxamt07Err, prmtoln08Err, maxamt08Err, prmtoln09Err, maxamt09Err, prmtoln10Err, maxamt10Err, prmtoln11Err, maxamt11Err, sfindErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5667screen.class;
		protectRecord = S5667protect.class;
	}

}
