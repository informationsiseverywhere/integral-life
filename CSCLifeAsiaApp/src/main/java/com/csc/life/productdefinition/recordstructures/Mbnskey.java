package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:42
 * Description:
 * Copybook name: MBNSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mbnskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mbnsFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData mbnsKey = new FixedLengthStringData(256).isAPartOf(mbnsFileKey, 0, REDEFINE);
  	public FixedLengthStringData mbnsChdrcoy = new FixedLengthStringData(1).isAPartOf(mbnsKey, 0);
  	public FixedLengthStringData mbnsChdrnum = new FixedLengthStringData(8).isAPartOf(mbnsKey, 1);
  	public FixedLengthStringData mbnsLife = new FixedLengthStringData(2).isAPartOf(mbnsKey, 9);
  	public FixedLengthStringData mbnsCoverage = new FixedLengthStringData(2).isAPartOf(mbnsKey, 11);
  	public FixedLengthStringData mbnsRider = new FixedLengthStringData(2).isAPartOf(mbnsKey, 13);
  	public PackedDecimalData mbnsYrsinf = new PackedDecimalData(3, 0).isAPartOf(mbnsKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(239).isAPartOf(mbnsKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mbnsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mbnsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}