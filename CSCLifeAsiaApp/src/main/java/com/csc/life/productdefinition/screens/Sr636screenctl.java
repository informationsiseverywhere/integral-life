package com.csc.life.productdefinition.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr636screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sr636screensfl";
		lrec.subfileClass = Sr636screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 12;
		lrec.pageSubfile = 11;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 7, 1, 76}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr636ScreenVars sv = (Sr636ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr636screenctlWritten, sv.Sr636screensflWritten, av, sv.sr636screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr636ScreenVars screenVars = (Sr636ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.sel02.setClassString("");
		screenVars.search02.setClassString("");
		screenVars.zaracde02.setClassString("");
		screenVars.zmedtyp02.setClassString("");
		screenVars.sex02.setClassString("");
		screenVars.zmedsts02.setClassString("");
		screenVars.sel01.setClassString("");
		screenVars.search01.setClassString("");
		screenVars.zaracde01.setClassString("");
		screenVars.sex01.setClassString("");
		screenVars.zmedsts01.setClassString("");
		screenVars.zmedtyp01.setClassString("");
	}

/**
 * Clear all the variables in Sr636screenctl
 */
	public static void clear(VarModel pv) {
		Sr636ScreenVars screenVars = (Sr636ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.sel02.clear();
		screenVars.search02.clear();
		screenVars.zaracde02.clear();
		screenVars.zmedtyp02.clear();
		screenVars.sex02.clear();
		screenVars.zmedsts02.clear();
		screenVars.sel01.clear();
		screenVars.search01.clear();
		screenVars.zaracde01.clear();
		screenVars.sex01.clear();
		screenVars.zmedsts01.clear();
		screenVars.zmedtyp01.clear();
	}
}
