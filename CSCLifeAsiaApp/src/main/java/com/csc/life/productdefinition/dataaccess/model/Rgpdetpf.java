
package com.csc.life.productdefinition.dataaccess.model;

import java.math.BigDecimal;
import java.util.Date;

import com.csc.life.anticipatedendowment.dataaccess.model.Bd5huDTO;
import com.csc.life.anticipatedendowment.dataaccess.model.Bd5hvDTO;

public class Rgpdetpf {


	//member variables for columns
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private Integer plnsfx;
	private String life;
	private String coverage;
	private String rider;
	private String currcd; 
	private BigDecimal apcaplamt;
	private BigDecimal apintamt;
	private Integer aplstcapdate;
	private Integer apnxtcapdate;
	private Integer aplstintbdte;
	private Integer apnxtintbdte;
	private String validflag;
	private String tranno;
	private String usrprf;
	private String jobnm;
	private Date datime;
	private Bd5hvDTO bd5hvDTO;
	private Bd5huDTO bd5huDTO;

	// Constructor
	public Rgpdetpf ( ) {};

	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public String getChdrcoy(){
		return this.chdrcoy;
	}
	public String getChdrnum(){
		return this.chdrnum;
	}
	public Integer getPlnsfx(){
		return this.plnsfx;
	}
	public String getLife(){
		return this.life;
	}
	public String getCoverage(){
		return this.coverage;
	}
	public String getRider(){
		return this.rider;
	}
	public BigDecimal getApcaplamt(){
		return this.apcaplamt;
	}
	public Integer getAplstcapdate(){
		return this.aplstcapdate;
	}
	public Integer getApnxtcapdate(){
		return this.apnxtcapdate;
	}
	public Integer getAplstintbdte(){
		return this.aplstintbdte;
	}
	public Integer getApnxtintbdte(){
		return this.apnxtintbdte;
	}
	public String getValidflag(){
		return this.validflag;
	}
	public String getTranno(){
		return this.tranno;
	}
	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public Date getDatime(){
		return this.datime;
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public String getCurrcd() {
		return currcd;
	}

	public void setCurrcd(String currcd) {
		this.currcd = currcd;
	}

	public void setChdrcoy( String chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setChdrnum( String chdrnum ){
		 this.chdrnum = chdrnum;
	}
	public void setPlnsfx( Integer plnsfx ){
		 this.plnsfx = plnsfx;
	}
	public void setLife( String life ){
		 this.life = life;
	}
	public void setCoverage( String coverage ){
		 this.coverage = coverage;
	}
	public void setRider( String rider ){
		 this.rider = rider;
	}
	public void setApcaplamt( BigDecimal apcaplamt ){
		 this.apcaplamt = apcaplamt;
	}
	public BigDecimal getApintamt() {
		return apintamt;
	}

	public void setApintamt(BigDecimal apintlamt) {
		this.apintamt = apintlamt;
	}

	public void setAplstcapdate( Integer aplstcapdate ){
		 this.aplstcapdate = aplstcapdate;
	}
	public void setApnxtcapdate( Integer apnxtcapdate ){
		 this.apnxtcapdate = apnxtcapdate;
	}
	public void setAplstintbdte( Integer aplstintbdte ){
		 this.aplstintbdte = aplstintbdte;
	}
	public void setApnxtintbdte( Integer apnxtintbdte ){
		 this.apnxtintbdte = apnxtintbdte;
	}
	public void setValidflag( String validflag ){
		 this.validflag = validflag;
	}
	public void setTranno( String tranno ){
		 this.tranno = tranno;
	}
	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime( Date datime ){
		 this.datime = datime;
	}
	public Bd5huDTO getBd5huDTO() {
		return bd5huDTO;
	}

	public void setBd5huDTO(Bd5huDTO bd5huDTO) {
		this.bd5huDTO = bd5huDTO;
	}

	public Bd5hvDTO getBd5hvDTO() {
		return bd5hvDTO;
	}

	public void setBd5hvDTO(Bd5hvDTO bd5hvDTO) {
		this.bd5hvDTO = bd5hvDTO;
	}
	// ToString method
	public String toString(){

		StringBuilder output = new StringBuilder();

		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("CHDRCOY:		");
		output.append(getChdrcoy());
		output.append("\r\n");
		output.append("CHDRNUM:		");
		output.append(getChdrnum());
		output.append("\r\n");
		output.append("PLNSFX:		");
		output.append(getPlnsfx());
		output.append("\r\n");
		output.append("LIFE:		");
		output.append(getLife());
		output.append("\r\n");
		output.append("COVERAGE:		");
		output.append(getCoverage());
		output.append("\r\n");
		output.append("RIDER:		");
		output.append(getRider());
		output.append("\r\n");
		output.append("APCAPLAMT:		");
		output.append(getApcaplamt());
		output.append("\r\n");
		output.append("APINTAMT:		");
		output.append(getApintamt());
		output.append("\r\n");
		output.append("APLSTCAPDATE:		");
		output.append(getAplstcapdate());
		output.append("\r\n");
		output.append("APNXTCAPDATE:		");
		output.append(getApnxtcapdate());
		output.append("\r\n");
		output.append("APLSTINTBDTE:		");
		output.append(getAplstintbdte());
		output.append("\r\n");
		output.append("APNXTINTBDTE:		");
		output.append(getApnxtintbdte());
		output.append("\r\n");
		output.append("VALIDFLAG:		");
		output.append(getValidflag());
		output.append("\r\n");
		output.append("TRANNO:		");
		output.append(getTranno());
		output.append("\r\n");
		output.append("USRPRF:		");
		output.append(getUsrprf());
		output.append("\r\n");
		output.append("JOBNM:		");
		output.append(getJobnm());
		output.append("\r\n");
		output.append("DATIME:		");
		output.append(getDatime());
		output.append("\r\n");
		
		return output.toString();
	}


}
