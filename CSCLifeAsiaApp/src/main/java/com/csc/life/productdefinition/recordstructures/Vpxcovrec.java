package com.csc.life.productdefinition.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: 
 * @version:
 * Creation Date: Mon, 22 Aug 2012 15:31:00
 * Description:
 * Copybook name: Vpxcovrec 
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Vpxcovrec extends ExternalData {


	  //*******************************
	  //Attribute Declarations
	  //*******************************
	
	public FixedLengthStringData vpxcovRec = new FixedLengthStringData(1080);
  	public FixedLengthStringData lifes = new FixedLengthStringData(60).isAPartOf(vpxcovRec, 0);
  	public FixedLengthStringData[] life = FLSArrayPartOfStructure(30, 2, lifes, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(60).isAPartOf(lifes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData life01 = new FixedLengthStringData(2).isAPartOf(filler1, 0);
  	public FixedLengthStringData life02 = new FixedLengthStringData(2).isAPartOf(filler1, 2);
  	public FixedLengthStringData life03 = new FixedLengthStringData(2).isAPartOf(filler1, 4);
  	public FixedLengthStringData life04 = new FixedLengthStringData(2).isAPartOf(filler1, 6);
  	public FixedLengthStringData life05 = new FixedLengthStringData(2).isAPartOf(filler1, 8);
  	public FixedLengthStringData life06 = new FixedLengthStringData(2).isAPartOf(filler1, 10);
  	public FixedLengthStringData life07 = new FixedLengthStringData(2).isAPartOf(filler1, 12);
  	public FixedLengthStringData life08 = new FixedLengthStringData(2).isAPartOf(filler1, 14);
  	public FixedLengthStringData life09 = new FixedLengthStringData(2).isAPartOf(filler1, 16);
  	public FixedLengthStringData life10 = new FixedLengthStringData(2).isAPartOf(filler1, 18);
  	public FixedLengthStringData life11 = new FixedLengthStringData(2).isAPartOf(filler1, 20);
  	public FixedLengthStringData life12 = new FixedLengthStringData(2).isAPartOf(filler1, 22);
  	public FixedLengthStringData life13 = new FixedLengthStringData(2).isAPartOf(filler1, 24);
  	public FixedLengthStringData life14 = new FixedLengthStringData(2).isAPartOf(filler1, 26);
  	public FixedLengthStringData life15 = new FixedLengthStringData(2).isAPartOf(filler1, 28);
  	public FixedLengthStringData life16 = new FixedLengthStringData(2).isAPartOf(filler1, 30);
  	public FixedLengthStringData life17 = new FixedLengthStringData(2).isAPartOf(filler1, 32);
  	public FixedLengthStringData life18 = new FixedLengthStringData(2).isAPartOf(filler1, 34);
  	public FixedLengthStringData life19 = new FixedLengthStringData(2).isAPartOf(filler1, 36);
  	public FixedLengthStringData life20 = new FixedLengthStringData(2).isAPartOf(filler1, 38);
  	public FixedLengthStringData life21 = new FixedLengthStringData(2).isAPartOf(filler1, 40);
  	public FixedLengthStringData life22 = new FixedLengthStringData(2).isAPartOf(filler1, 42);
  	public FixedLengthStringData life23 = new FixedLengthStringData(2).isAPartOf(filler1, 44);
  	public FixedLengthStringData life24 = new FixedLengthStringData(2).isAPartOf(filler1, 46);
  	public FixedLengthStringData life25 = new FixedLengthStringData(2).isAPartOf(filler1, 48);
  	public FixedLengthStringData life26 = new FixedLengthStringData(2).isAPartOf(filler1, 50);
  	public FixedLengthStringData life27 = new FixedLengthStringData(2).isAPartOf(filler1, 52);
  	public FixedLengthStringData life28 = new FixedLengthStringData(2).isAPartOf(filler1, 54);
  	public FixedLengthStringData life29 = new FixedLengthStringData(2).isAPartOf(filler1, 56);
  	public FixedLengthStringData life30 = new FixedLengthStringData(2).isAPartOf(filler1, 58);
  	
  	public FixedLengthStringData jlifes = new FixedLengthStringData(60).isAPartOf(vpxcovRec, 60);
  	public FixedLengthStringData[] jlife = FLSArrayPartOfStructure(30, 2, jlifes, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(60).isAPartOf(jlifes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData jlife01 = new FixedLengthStringData(2).isAPartOf(filler2, 0);
  	public FixedLengthStringData jlife02 = new FixedLengthStringData(2).isAPartOf(filler2, 2);
  	public FixedLengthStringData jlife03 = new FixedLengthStringData(2).isAPartOf(filler2, 4);
  	public FixedLengthStringData jlife04 = new FixedLengthStringData(2).isAPartOf(filler2, 6);
  	public FixedLengthStringData jlife05 = new FixedLengthStringData(2).isAPartOf(filler2, 8);
  	public FixedLengthStringData jlife06 = new FixedLengthStringData(2).isAPartOf(filler2, 10);
  	public FixedLengthStringData jlife07 = new FixedLengthStringData(2).isAPartOf(filler2, 12);
  	public FixedLengthStringData jlife08 = new FixedLengthStringData(2).isAPartOf(filler2, 14);
  	public FixedLengthStringData jlife09 = new FixedLengthStringData(2).isAPartOf(filler2, 16);
  	public FixedLengthStringData jlife10 = new FixedLengthStringData(2).isAPartOf(filler2, 18);
  	public FixedLengthStringData jlife11 = new FixedLengthStringData(2).isAPartOf(filler2, 20);
  	public FixedLengthStringData jlife12 = new FixedLengthStringData(2).isAPartOf(filler2, 22);
  	public FixedLengthStringData jlife13 = new FixedLengthStringData(2).isAPartOf(filler2, 24);
  	public FixedLengthStringData jlife14 = new FixedLengthStringData(2).isAPartOf(filler2, 26);
  	public FixedLengthStringData jlife15 = new FixedLengthStringData(2).isAPartOf(filler2, 28);
  	public FixedLengthStringData jlife16 = new FixedLengthStringData(2).isAPartOf(filler2, 30);
  	public FixedLengthStringData jlife17 = new FixedLengthStringData(2).isAPartOf(filler2, 32);
  	public FixedLengthStringData jlife18 = new FixedLengthStringData(2).isAPartOf(filler2, 34);
  	public FixedLengthStringData jlife19 = new FixedLengthStringData(2).isAPartOf(filler2, 36);
  	public FixedLengthStringData jlife20 = new FixedLengthStringData(2).isAPartOf(filler2, 38);
  	public FixedLengthStringData jlife21 = new FixedLengthStringData(2).isAPartOf(filler2, 40);
  	public FixedLengthStringData jlife22 = new FixedLengthStringData(2).isAPartOf(filler2, 42);
  	public FixedLengthStringData jlife23 = new FixedLengthStringData(2).isAPartOf(filler2, 44);
  	public FixedLengthStringData jlife24 = new FixedLengthStringData(2).isAPartOf(filler2, 46);
  	public FixedLengthStringData jlife25 = new FixedLengthStringData(2).isAPartOf(filler2, 48);
  	public FixedLengthStringData jlife26 = new FixedLengthStringData(2).isAPartOf(filler2, 50);
  	public FixedLengthStringData jlife27 = new FixedLengthStringData(2).isAPartOf(filler2, 52);
  	public FixedLengthStringData jlife28 = new FixedLengthStringData(2).isAPartOf(filler2, 54);
  	public FixedLengthStringData jlife29 = new FixedLengthStringData(2).isAPartOf(filler2, 56);
  	public FixedLengthStringData jlife30 = new FixedLengthStringData(2).isAPartOf(filler2, 58);
  	
  	public FixedLengthStringData coverages = new FixedLengthStringData(60).isAPartOf(vpxcovRec, 120);
  	public FixedLengthStringData[] coverage = FLSArrayPartOfStructure(30, 2, coverages, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(60).isAPartOf(coverages, 0, FILLER_REDEFINE);
  	public FixedLengthStringData coverage01 = new FixedLengthStringData(2).isAPartOf(filler3, 0);
  	public FixedLengthStringData coverage02 = new FixedLengthStringData(2).isAPartOf(filler3, 2);
  	public FixedLengthStringData coverage03 = new FixedLengthStringData(2).isAPartOf(filler3, 4);
  	public FixedLengthStringData coverage04 = new FixedLengthStringData(2).isAPartOf(filler3, 6);
  	public FixedLengthStringData coverage05 = new FixedLengthStringData(2).isAPartOf(filler3, 8);
  	public FixedLengthStringData coverage06 = new FixedLengthStringData(2).isAPartOf(filler3, 10);
  	public FixedLengthStringData coverage07 = new FixedLengthStringData(2).isAPartOf(filler3, 12);
  	public FixedLengthStringData coverage08 = new FixedLengthStringData(2).isAPartOf(filler3, 14);
  	public FixedLengthStringData coverage09 = new FixedLengthStringData(2).isAPartOf(filler3, 16);
  	public FixedLengthStringData coverage10 = new FixedLengthStringData(2).isAPartOf(filler3, 18);
  	public FixedLengthStringData coverage11 = new FixedLengthStringData(2).isAPartOf(filler3, 20);
  	public FixedLengthStringData coverage12 = new FixedLengthStringData(2).isAPartOf(filler3, 22);
  	public FixedLengthStringData coverage13 = new FixedLengthStringData(2).isAPartOf(filler3, 24);
  	public FixedLengthStringData coverage14 = new FixedLengthStringData(2).isAPartOf(filler3, 26);
  	public FixedLengthStringData coverage15 = new FixedLengthStringData(2).isAPartOf(filler3, 28);
  	public FixedLengthStringData coverage16 = new FixedLengthStringData(2).isAPartOf(filler3, 30);
  	public FixedLengthStringData coverage17 = new FixedLengthStringData(2).isAPartOf(filler3, 32);
  	public FixedLengthStringData coverage18 = new FixedLengthStringData(2).isAPartOf(filler3, 34);
  	public FixedLengthStringData coverage19 = new FixedLengthStringData(2).isAPartOf(filler3, 36);
  	public FixedLengthStringData coverage20 = new FixedLengthStringData(2).isAPartOf(filler3, 38);
  	public FixedLengthStringData coverage21 = new FixedLengthStringData(2).isAPartOf(filler3, 40);
  	public FixedLengthStringData coverage22 = new FixedLengthStringData(2).isAPartOf(filler3, 42);
  	public FixedLengthStringData coverage23 = new FixedLengthStringData(2).isAPartOf(filler3, 44);
  	public FixedLengthStringData coverage24 = new FixedLengthStringData(2).isAPartOf(filler3, 46);
  	public FixedLengthStringData coverage25 = new FixedLengthStringData(2).isAPartOf(filler3, 48);
  	public FixedLengthStringData coverage26 = new FixedLengthStringData(2).isAPartOf(filler3, 50);
  	public FixedLengthStringData coverage27 = new FixedLengthStringData(2).isAPartOf(filler3, 52);
  	public FixedLengthStringData coverage28 = new FixedLengthStringData(2).isAPartOf(filler3, 54);
  	public FixedLengthStringData coverage29 = new FixedLengthStringData(2).isAPartOf(filler3, 56);
  	public FixedLengthStringData coverage30 = new FixedLengthStringData(2).isAPartOf(filler3, 58);
  	
  	public FixedLengthStringData riders = new FixedLengthStringData(60).isAPartOf(vpxcovRec, 180);
  	public FixedLengthStringData[] rider = FLSArrayPartOfStructure(30, 2, riders, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(60).isAPartOf(riders, 0, FILLER_REDEFINE);
  	public FixedLengthStringData rider01 = new FixedLengthStringData(2).isAPartOf(filler4, 0);
  	public FixedLengthStringData rider02 = new FixedLengthStringData(2).isAPartOf(filler4, 2);
  	public FixedLengthStringData rider03 = new FixedLengthStringData(2).isAPartOf(filler4, 4);
  	public FixedLengthStringData rider04 = new FixedLengthStringData(2).isAPartOf(filler4, 6);
  	public FixedLengthStringData rider05 = new FixedLengthStringData(2).isAPartOf(filler4, 8);
  	public FixedLengthStringData rider06 = new FixedLengthStringData(2).isAPartOf(filler4, 10);
  	public FixedLengthStringData rider07 = new FixedLengthStringData(2).isAPartOf(filler4, 12);
  	public FixedLengthStringData rider08 = new FixedLengthStringData(2).isAPartOf(filler4, 14);
  	public FixedLengthStringData rider09 = new FixedLengthStringData(2).isAPartOf(filler4, 16);
  	public FixedLengthStringData rider10 = new FixedLengthStringData(2).isAPartOf(filler4, 18);
  	public FixedLengthStringData rider11 = new FixedLengthStringData(2).isAPartOf(filler4, 20);
  	public FixedLengthStringData rider12 = new FixedLengthStringData(2).isAPartOf(filler4, 22);
  	public FixedLengthStringData rider13 = new FixedLengthStringData(2).isAPartOf(filler4, 24);
  	public FixedLengthStringData rider14 = new FixedLengthStringData(2).isAPartOf(filler4, 26);
  	public FixedLengthStringData rider15 = new FixedLengthStringData(2).isAPartOf(filler4, 28);
  	public FixedLengthStringData rider16 = new FixedLengthStringData(2).isAPartOf(filler4, 30);
  	public FixedLengthStringData rider17 = new FixedLengthStringData(2).isAPartOf(filler4, 32);
  	public FixedLengthStringData rider18 = new FixedLengthStringData(2).isAPartOf(filler4, 34);
  	public FixedLengthStringData rider19 = new FixedLengthStringData(2).isAPartOf(filler4, 36);
  	public FixedLengthStringData rider20 = new FixedLengthStringData(2).isAPartOf(filler4, 38);
  	public FixedLengthStringData rider21 = new FixedLengthStringData(2).isAPartOf(filler4, 40);
  	public FixedLengthStringData rider22 = new FixedLengthStringData(2).isAPartOf(filler4, 42);
  	public FixedLengthStringData rider23 = new FixedLengthStringData(2).isAPartOf(filler4, 44);
  	public FixedLengthStringData rider24 = new FixedLengthStringData(2).isAPartOf(filler4, 46);
  	public FixedLengthStringData rider25 = new FixedLengthStringData(2).isAPartOf(filler4, 48);
  	public FixedLengthStringData rider26 = new FixedLengthStringData(2).isAPartOf(filler4, 50);
  	public FixedLengthStringData rider27 = new FixedLengthStringData(2).isAPartOf(filler4, 52);
  	public FixedLengthStringData rider28 = new FixedLengthStringData(2).isAPartOf(filler4, 54);
  	public FixedLengthStringData rider29 = new FixedLengthStringData(2).isAPartOf(filler4, 56);
  	public FixedLengthStringData rider30 = new FixedLengthStringData(2).isAPartOf(filler4, 58);
  	
  	public FixedLengthStringData crtables = new FixedLengthStringData(120).isAPartOf(vpxcovRec, 240);
  	public FixedLengthStringData[] crtable = FLSArrayPartOfStructure(30, 4, crtables, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(120).isAPartOf(crtables, 0, FILLER_REDEFINE);
  	public FixedLengthStringData crtable01 = new FixedLengthStringData(4).isAPartOf(filler5, 0);
  	public FixedLengthStringData crtable02 = new FixedLengthStringData(4).isAPartOf(filler5, 4);
  	public FixedLengthStringData crtable03 = new FixedLengthStringData(4).isAPartOf(filler5, 8);
  	public FixedLengthStringData crtable04 = new FixedLengthStringData(4).isAPartOf(filler5, 12);
  	public FixedLengthStringData crtable05 = new FixedLengthStringData(4).isAPartOf(filler5, 16);
  	public FixedLengthStringData crtable06 = new FixedLengthStringData(4).isAPartOf(filler5, 20);
  	public FixedLengthStringData crtable07 = new FixedLengthStringData(4).isAPartOf(filler5, 24);
  	public FixedLengthStringData crtable08 = new FixedLengthStringData(4).isAPartOf(filler5, 28);
  	public FixedLengthStringData crtable09 = new FixedLengthStringData(4).isAPartOf(filler5, 32);
  	public FixedLengthStringData crtable10 = new FixedLengthStringData(4).isAPartOf(filler5, 36);
  	public FixedLengthStringData crtable11 = new FixedLengthStringData(4).isAPartOf(filler5, 40);
  	public FixedLengthStringData crtable12 = new FixedLengthStringData(4).isAPartOf(filler5, 44);
  	public FixedLengthStringData crtable13 = new FixedLengthStringData(4).isAPartOf(filler5, 48);
  	public FixedLengthStringData crtable14 = new FixedLengthStringData(4).isAPartOf(filler5, 52);
  	public FixedLengthStringData crtable15 = new FixedLengthStringData(4).isAPartOf(filler5, 56);
  	public FixedLengthStringData crtable16 = new FixedLengthStringData(4).isAPartOf(filler5, 60);
  	public FixedLengthStringData crtable17 = new FixedLengthStringData(4).isAPartOf(filler5, 64);
  	public FixedLengthStringData crtable18 = new FixedLengthStringData(4).isAPartOf(filler5, 68);
  	public FixedLengthStringData crtable19 = new FixedLengthStringData(4).isAPartOf(filler5, 72);
  	public FixedLengthStringData crtable20 = new FixedLengthStringData(4).isAPartOf(filler5, 76);
  	public FixedLengthStringData crtable21 = new FixedLengthStringData(4).isAPartOf(filler5, 80);
  	public FixedLengthStringData crtable22 = new FixedLengthStringData(4).isAPartOf(filler5, 84);
  	public FixedLengthStringData crtable23 = new FixedLengthStringData(4).isAPartOf(filler5, 88);
  	public FixedLengthStringData crtable24 = new FixedLengthStringData(4).isAPartOf(filler5, 92);
  	public FixedLengthStringData crtable25 = new FixedLengthStringData(4).isAPartOf(filler5, 96);
  	public FixedLengthStringData crtable26 = new FixedLengthStringData(4).isAPartOf(filler5, 100);
  	public FixedLengthStringData crtable27 = new FixedLengthStringData(4).isAPartOf(filler5, 104);
  	public FixedLengthStringData crtable28 = new FixedLengthStringData(4).isAPartOf(filler5, 108);
  	public FixedLengthStringData crtable29 = new FixedLengthStringData(4).isAPartOf(filler5, 112);
  	public FixedLengthStringData crtable30 = new FixedLengthStringData(4).isAPartOf(filler5, 116);
  	
  	public FixedLengthStringData singPrems = new FixedLengthStringData(240).isAPartOf(vpxcovRec, 360);
  	public PackedDecimalData[] singPrem = PDArrayPartOfStructure(30, 15, 2, singPrems, 0);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(120).isAPartOf(singPrems, 0, FILLER_REDEFINE); 	
  	public PackedDecimalData singPrem01 = new PackedDecimalData(15, 2).isAPartOf(filler6,0  );
  	public PackedDecimalData singPrem02 = new PackedDecimalData(15, 2).isAPartOf(filler6,8  );
  	public PackedDecimalData singPrem03 = new PackedDecimalData(15, 2).isAPartOf(filler6,16 );
  	public PackedDecimalData singPrem04 = new PackedDecimalData(15, 2).isAPartOf(filler6,24 );
  	public PackedDecimalData singPrem05 = new PackedDecimalData(15, 2).isAPartOf(filler6,32 );
  	public PackedDecimalData singPrem06 = new PackedDecimalData(15, 2).isAPartOf(filler6,40 );
  	public PackedDecimalData singPrem07 = new PackedDecimalData(15, 2).isAPartOf(filler6,48 );
  	public PackedDecimalData singPrem08 = new PackedDecimalData(15, 2).isAPartOf(filler6,56 );
  	public PackedDecimalData singPrem09 = new PackedDecimalData(15, 2).isAPartOf(filler6,64 );
  	public PackedDecimalData singPrem10 = new PackedDecimalData(15, 2).isAPartOf(filler6,72 );
  	public PackedDecimalData singPrem11 = new PackedDecimalData(15, 2).isAPartOf(filler6,80 );
  	public PackedDecimalData singPrem12 = new PackedDecimalData(15, 2).isAPartOf(filler6,88 );
  	public PackedDecimalData singPrem13 = new PackedDecimalData(15, 2).isAPartOf(filler6,96 );
  	public PackedDecimalData singPrem14 = new PackedDecimalData(15, 2).isAPartOf(filler6,104);
  	public PackedDecimalData singPrem15 = new PackedDecimalData(15, 2).isAPartOf(filler6,112);
  	public PackedDecimalData singPrem16 = new PackedDecimalData(15, 2).isAPartOf(filler6,120);
  	public PackedDecimalData singPrem17 = new PackedDecimalData(15, 2).isAPartOf(filler6,128);
  	public PackedDecimalData singPrem18 = new PackedDecimalData(15, 2).isAPartOf(filler6,136);
  	public PackedDecimalData singPrem19 = new PackedDecimalData(15, 2).isAPartOf(filler6,144);
  	public PackedDecimalData singPrem20 = new PackedDecimalData(15, 2).isAPartOf(filler6,152);
  	public PackedDecimalData singPrem21 = new PackedDecimalData(15, 2).isAPartOf(filler6,160);
  	public PackedDecimalData singPrem22 = new PackedDecimalData(15, 2).isAPartOf(filler6,168);
  	public PackedDecimalData singPrem23 = new PackedDecimalData(15, 2).isAPartOf(filler6,176);
  	public PackedDecimalData singPrem24 = new PackedDecimalData(15, 2).isAPartOf(filler6,184);
  	public PackedDecimalData singPrem25 = new PackedDecimalData(15, 2).isAPartOf(filler6,192);
  	public PackedDecimalData singPrem26 = new PackedDecimalData(15, 2).isAPartOf(filler6,200);
  	public PackedDecimalData singPrem27 = new PackedDecimalData(15, 2).isAPartOf(filler6,208);
  	public PackedDecimalData singPrem28 = new PackedDecimalData(15, 2).isAPartOf(filler6,216);
  	public PackedDecimalData singPrem29 = new PackedDecimalData(15, 2).isAPartOf(filler6,224);
  	public PackedDecimalData singPrem30 = new PackedDecimalData(15, 2).isAPartOf(filler6,232);
  	
  	public FixedLengthStringData regPrems = new FixedLengthStringData(240).isAPartOf(vpxcovRec, 600);
  	public PackedDecimalData[] regPrem = PDArrayPartOfStructure(30, 15, 2, regPrems, 0);
  	public FixedLengthStringData filler7 = new FixedLengthStringData(120).isAPartOf(regPrems, 0, FILLER_REDEFINE); 	
  	public PackedDecimalData regPrem01 = new PackedDecimalData(15, 2).isAPartOf(filler7,0  );
  	public PackedDecimalData regPrem02 = new PackedDecimalData(15, 2).isAPartOf(filler7,8  );
  	public PackedDecimalData regPrem03 = new PackedDecimalData(15, 2).isAPartOf(filler7,16 );
  	public PackedDecimalData regPrem04 = new PackedDecimalData(15, 2).isAPartOf(filler7,24 );
  	public PackedDecimalData regPrem05 = new PackedDecimalData(15, 2).isAPartOf(filler7,32 );
  	public PackedDecimalData regPrem06 = new PackedDecimalData(15, 2).isAPartOf(filler7,40 );
  	public PackedDecimalData regPrem07 = new PackedDecimalData(15, 2).isAPartOf(filler7,48 );
  	public PackedDecimalData regPrem08 = new PackedDecimalData(15, 2).isAPartOf(filler7,56 );
  	public PackedDecimalData regPrem09 = new PackedDecimalData(15, 2).isAPartOf(filler7,64 );
  	public PackedDecimalData regPrem10 = new PackedDecimalData(15, 2).isAPartOf(filler7,72 );
  	public PackedDecimalData regPrem11 = new PackedDecimalData(15, 2).isAPartOf(filler7,80 );
  	public PackedDecimalData regPrem12 = new PackedDecimalData(15, 2).isAPartOf(filler7,88 );
  	public PackedDecimalData regPrem13 = new PackedDecimalData(15, 2).isAPartOf(filler7,96 );
  	public PackedDecimalData regPrem14 = new PackedDecimalData(15, 2).isAPartOf(filler7,104);
  	public PackedDecimalData regPrem15 = new PackedDecimalData(15, 2).isAPartOf(filler7,112);
  	public PackedDecimalData regPrem16 = new PackedDecimalData(15, 2).isAPartOf(filler7,120);
  	public PackedDecimalData regPrem17 = new PackedDecimalData(15, 2).isAPartOf(filler7,128);
  	public PackedDecimalData regPrem18 = new PackedDecimalData(15, 2).isAPartOf(filler7,136);
  	public PackedDecimalData regPrem19 = new PackedDecimalData(15, 2).isAPartOf(filler7,144);
  	public PackedDecimalData regPrem20 = new PackedDecimalData(15, 2).isAPartOf(filler7,152);
  	public PackedDecimalData regPrem21 = new PackedDecimalData(15, 2).isAPartOf(filler7,160);
  	public PackedDecimalData regPrem22 = new PackedDecimalData(15, 2).isAPartOf(filler7,168);
  	public PackedDecimalData regPrem23 = new PackedDecimalData(15, 2).isAPartOf(filler7,176);
  	public PackedDecimalData regPrem24 = new PackedDecimalData(15, 2).isAPartOf(filler7,184);
  	public PackedDecimalData regPrem25 = new PackedDecimalData(15, 2).isAPartOf(filler7,192);
  	public PackedDecimalData regPrem26 = new PackedDecimalData(15, 2).isAPartOf(filler7,200);
  	public PackedDecimalData regPrem27 = new PackedDecimalData(15, 2).isAPartOf(filler7,208);
  	public PackedDecimalData regPrem28 = new PackedDecimalData(15, 2).isAPartOf(filler7,216);
  	public PackedDecimalData regPrem29 = new PackedDecimalData(15, 2).isAPartOf(filler7,224);
  	public PackedDecimalData regPrem30 = new PackedDecimalData(15, 2).isAPartOf(filler7,232);
  	
  	public FixedLengthStringData basPrems = new FixedLengthStringData(240).isAPartOf(vpxcovRec, 840);
  	public PackedDecimalData[] basPrem = PDArrayPartOfStructure(30, 15, 2, basPrems, 0);
  	public FixedLengthStringData filler8 = new FixedLengthStringData(120).isAPartOf(basPrems, 0, FILLER_REDEFINE); 	
  	public PackedDecimalData basPrem01 = new PackedDecimalData(15, 2).isAPartOf(filler8,0  );
  	public PackedDecimalData basPrem02 = new PackedDecimalData(15, 2).isAPartOf(filler8,8  );
  	public PackedDecimalData basPrem03 = new PackedDecimalData(15, 2).isAPartOf(filler8,16 );
  	public PackedDecimalData basPrem04 = new PackedDecimalData(15, 2).isAPartOf(filler8,24 );
  	public PackedDecimalData basPrem05 = new PackedDecimalData(15, 2).isAPartOf(filler8,32 );
  	public PackedDecimalData basPrem06 = new PackedDecimalData(15, 2).isAPartOf(filler8,40 );
  	public PackedDecimalData basPrem07 = new PackedDecimalData(15, 2).isAPartOf(filler8,48 );
  	public PackedDecimalData basPrem08 = new PackedDecimalData(15, 2).isAPartOf(filler8,56 );
  	public PackedDecimalData basPrem09 = new PackedDecimalData(15, 2).isAPartOf(filler8,64 );
  	public PackedDecimalData basPrem10 = new PackedDecimalData(15, 2).isAPartOf(filler8,72 );
  	public PackedDecimalData basPrem11 = new PackedDecimalData(15, 2).isAPartOf(filler8,80 );
  	public PackedDecimalData basPrem12 = new PackedDecimalData(15, 2).isAPartOf(filler8,88 );
  	public PackedDecimalData basPrem13 = new PackedDecimalData(15, 2).isAPartOf(filler8,96 );
  	public PackedDecimalData basPrem14 = new PackedDecimalData(15, 2).isAPartOf(filler8,104);
  	public PackedDecimalData basPrem15 = new PackedDecimalData(15, 2).isAPartOf(filler8,112);
  	public PackedDecimalData basPrem16 = new PackedDecimalData(15, 2).isAPartOf(filler8,120);
  	public PackedDecimalData basPrem17 = new PackedDecimalData(15, 2).isAPartOf(filler8,128);
  	public PackedDecimalData basPrem18 = new PackedDecimalData(15, 2).isAPartOf(filler8,136);
  	public PackedDecimalData basPrem19 = new PackedDecimalData(15, 2).isAPartOf(filler8,144);
  	public PackedDecimalData basPrem20 = new PackedDecimalData(15, 2).isAPartOf(filler8,152);
  	public PackedDecimalData basPrem21 = new PackedDecimalData(15, 2).isAPartOf(filler8,160);
  	public PackedDecimalData basPrem22 = new PackedDecimalData(15, 2).isAPartOf(filler8,168);
  	public PackedDecimalData basPrem23 = new PackedDecimalData(15, 2).isAPartOf(filler8,176);
  	public PackedDecimalData basPrem24 = new PackedDecimalData(15, 2).isAPartOf(filler8,184);
  	public PackedDecimalData basPrem25 = new PackedDecimalData(15, 2).isAPartOf(filler8,192);
  	public PackedDecimalData basPrem26 = new PackedDecimalData(15, 2).isAPartOf(filler8,200);
  	public PackedDecimalData basPrem27 = new PackedDecimalData(15, 2).isAPartOf(filler8,208);
  	public PackedDecimalData basPrem28 = new PackedDecimalData(15, 2).isAPartOf(filler8,216);
  	public PackedDecimalData basPrem29 = new PackedDecimalData(15, 2).isAPartOf(filler8,224);
  	public PackedDecimalData basPrem30 = new PackedDecimalData(15, 2).isAPartOf(filler8,232);
  	
  	
	public void initialize() {
		COBOLFunctions.initialize(vpxcovRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			vpxcovRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}