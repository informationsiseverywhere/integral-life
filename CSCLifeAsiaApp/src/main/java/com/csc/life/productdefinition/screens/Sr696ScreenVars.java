package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR696
 * @version 1.0 generated on 30/08/09 07:24
 * @author Quipoz
 */
public class Sr696ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1004);
	public FixedLengthStringData dataFields = new FixedLengthStringData(412).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData frsuminss = new FixedLengthStringData(170).isAPartOf(dataFields, 1);
	public ZonedDecimalData[] frsumins = ZDArrayPartOfStructure(10, 17, 2, frsuminss, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(170).isAPartOf(frsuminss, 0, FILLER_REDEFINE);
	public ZonedDecimalData frsumins01 = DD.frsumins.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData frsumins02 = DD.frsumins.copyToZonedDecimal().isAPartOf(filler,17);
	public ZonedDecimalData frsumins03 = DD.frsumins.copyToZonedDecimal().isAPartOf(filler,34);
	public ZonedDecimalData frsumins04 = DD.frsumins.copyToZonedDecimal().isAPartOf(filler,51);
	public ZonedDecimalData frsumins05 = DD.frsumins.copyToZonedDecimal().isAPartOf(filler,68);
	public ZonedDecimalData frsumins06 = DD.frsumins.copyToZonedDecimal().isAPartOf(filler,85);
	public ZonedDecimalData frsumins07 = DD.frsumins.copyToZonedDecimal().isAPartOf(filler,102);
	public ZonedDecimalData frsumins08 = DD.frsumins.copyToZonedDecimal().isAPartOf(filler,119);
	public ZonedDecimalData frsumins09 = DD.frsumins.copyToZonedDecimal().isAPartOf(filler,136);
	public ZonedDecimalData frsumins10 = DD.frsumins.copyToZonedDecimal().isAPartOf(filler,153);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,171);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,179);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,187);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,195);
	public FixedLengthStringData sabands = new FixedLengthStringData(10).isAPartOf(dataFields, 225);
	public FixedLengthStringData[] saband = FLSArrayPartOfStructure(10, 1, sabands, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(10).isAPartOf(sabands, 0, FILLER_REDEFINE);
	public FixedLengthStringData saband01 = DD.saband.copy().isAPartOf(filler1,0);
	public FixedLengthStringData saband02 = DD.saband.copy().isAPartOf(filler1,1);
	public FixedLengthStringData saband03 = DD.saband.copy().isAPartOf(filler1,2);
	public FixedLengthStringData saband04 = DD.saband.copy().isAPartOf(filler1,3);
	public FixedLengthStringData saband05 = DD.saband.copy().isAPartOf(filler1,4);
	public FixedLengthStringData saband06 = DD.saband.copy().isAPartOf(filler1,5);
	public FixedLengthStringData saband07 = DD.saband.copy().isAPartOf(filler1,6);
	public FixedLengthStringData saband08 = DD.saband.copy().isAPartOf(filler1,7);
	public FixedLengthStringData saband09 = DD.saband.copy().isAPartOf(filler1,8);
	public FixedLengthStringData saband10 = DD.saband.copy().isAPartOf(filler1,9);
	public FixedLengthStringData simcrtable = DD.simcrtable.copy().isAPartOf(dataFields,235);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,237);
	public FixedLengthStringData tosuminss = new FixedLengthStringData(170).isAPartOf(dataFields, 242);
	public ZonedDecimalData[] tosumins = ZDArrayPartOfStructure(10, 17, 2, tosuminss, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(170).isAPartOf(tosuminss, 0, FILLER_REDEFINE);
	public ZonedDecimalData tosumins01 = DD.tosumins.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData tosumins02 = DD.tosumins.copyToZonedDecimal().isAPartOf(filler2,17);
	public ZonedDecimalData tosumins03 = DD.tosumins.copyToZonedDecimal().isAPartOf(filler2,34);
	public ZonedDecimalData tosumins04 = DD.tosumins.copyToZonedDecimal().isAPartOf(filler2,51);
	public ZonedDecimalData tosumins05 = DD.tosumins.copyToZonedDecimal().isAPartOf(filler2,68);
	public ZonedDecimalData tosumins06 = DD.tosumins.copyToZonedDecimal().isAPartOf(filler2,85);
	public ZonedDecimalData tosumins07 = DD.tosumins.copyToZonedDecimal().isAPartOf(filler2,102);
	public ZonedDecimalData tosumins08 = DD.tosumins.copyToZonedDecimal().isAPartOf(filler2,119);
	public ZonedDecimalData tosumins09 = DD.tosumins.copyToZonedDecimal().isAPartOf(filler2,136);
	public ZonedDecimalData tosumins10 = DD.tosumins.copyToZonedDecimal().isAPartOf(filler2,153);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(148).isAPartOf(dataArea, 412);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData frsuminssErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] frsuminsErr = FLSArrayPartOfStructure(10, 4, frsuminssErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(frsuminssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData frsumins01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData frsumins02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData frsumins03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData frsumins04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData frsumins05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData frsumins06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData frsumins07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData frsumins08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData frsumins09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData frsumins10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData sabandsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData[] sabandErr = FLSArrayPartOfStructure(10, 4, sabandsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(40).isAPartOf(sabandsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData saband01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData saband02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData saband03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData saband04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData saband05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData saband06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData saband07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData saband08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData saband09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData saband10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData simcrtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData tosuminssErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData[] tosuminsErr = FLSArrayPartOfStructure(10, 4, tosuminssErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(tosuminssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData tosumins01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData tosumins02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData tosumins03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData tosumins04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData tosumins05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData tosumins06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData tosumins07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData tosumins08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData tosumins09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData tosumins10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(444).isAPartOf(dataArea, 560);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData frsuminssOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] frsuminsOut = FLSArrayPartOfStructure(10, 12, frsuminssOut, 0);
	public FixedLengthStringData[][] frsuminsO = FLSDArrayPartOfArrayStructure(12, 1, frsuminsOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(120).isAPartOf(frsuminssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] frsumins01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] frsumins02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] frsumins03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] frsumins04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] frsumins05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] frsumins06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] frsumins07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);
	public FixedLengthStringData[] frsumins08Out = FLSArrayPartOfStructure(12, 1, filler6, 84);
	public FixedLengthStringData[] frsumins09Out = FLSArrayPartOfStructure(12, 1, filler6, 96);
	public FixedLengthStringData[] frsumins10Out = FLSArrayPartOfStructure(12, 1, filler6, 108);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData sabandsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 180);
	public FixedLengthStringData[] sabandOut = FLSArrayPartOfStructure(10, 12, sabandsOut, 0);
	public FixedLengthStringData[][] sabandO = FLSDArrayPartOfArrayStructure(12, 1, sabandOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(120).isAPartOf(sabandsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] saband01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] saband02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] saband03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] saband04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] saband05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] saband06Out = FLSArrayPartOfStructure(12, 1, filler7, 60);
	public FixedLengthStringData[] saband07Out = FLSArrayPartOfStructure(12, 1, filler7, 72);
	public FixedLengthStringData[] saband08Out = FLSArrayPartOfStructure(12, 1, filler7, 84);
	public FixedLengthStringData[] saband09Out = FLSArrayPartOfStructure(12, 1, filler7, 96);
	public FixedLengthStringData[] saband10Out = FLSArrayPartOfStructure(12, 1, filler7, 108);
	public FixedLengthStringData[] simcrtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData tosuminssOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 324);
	public FixedLengthStringData[] tosuminsOut = FLSArrayPartOfStructure(10, 12, tosuminssOut, 0);
	public FixedLengthStringData[][] tosuminsO = FLSDArrayPartOfArrayStructure(12, 1, tosuminsOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(120).isAPartOf(tosuminssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] tosumins01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] tosumins02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] tosumins03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] tosumins04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] tosumins05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] tosumins06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] tosumins07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] tosumins08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] tosumins09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] tosumins10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sr696screenWritten = new LongData(0);
	public LongData Sr696protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr696ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(itemOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(simcrtableOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frsumins01Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tosumins01Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(saband01Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tosumins02Out,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tosumins03Out,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tosumins04Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tosumins05Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tosumins06Out,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tosumins07Out,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tosumins08Out,new String[] {"34",null, "-34",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tosumins09Out,new String[] {"37",null, "-37",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tosumins10Out,new String[] {"40",null, "-40",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frsumins02Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frsumins03Out,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frsumins04Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frsumins05Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frsumins06Out,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frsumins07Out,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frsumins08Out,new String[] {"33",null, "-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frsumins09Out,new String[] {"36",null, "-36",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frsumins10Out,new String[] {"39",null, "-39",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(saband02Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(saband03Out,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(saband04Out,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(saband05Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(saband06Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(saband07Out,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(saband08Out,new String[] {"32",null, "-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(saband09Out,new String[] {"35",null, "-35",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(saband10Out,new String[] {"38",null, "-38",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, simcrtable, frsumins01, tosumins01, saband01, tosumins02, tosumins03, tosumins04, tosumins05, tosumins06, tosumins07, tosumins08, tosumins09, tosumins10, frsumins02, frsumins03, frsumins04, frsumins05, frsumins06, frsumins07, frsumins08, frsumins09, frsumins10, saband02, saband03, saband04, saband05, saband06, saband07, saband08, saband09, saband10};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, simcrtableOut, frsumins01Out, tosumins01Out, saband01Out, tosumins02Out, tosumins03Out, tosumins04Out, tosumins05Out, tosumins06Out, tosumins07Out, tosumins08Out, tosumins09Out, tosumins10Out, frsumins02Out, frsumins03Out, frsumins04Out, frsumins05Out, frsumins06Out, frsumins07Out, frsumins08Out, frsumins09Out, frsumins10Out, saband02Out, saband03Out, saband04Out, saband05Out, saband06Out, saband07Out, saband08Out, saband09Out, saband10Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, simcrtableErr, frsumins01Err, tosumins01Err, saband01Err, tosumins02Err, tosumins03Err, tosumins04Err, tosumins05Err, tosumins06Err, tosumins07Err, tosumins08Err, tosumins09Err, tosumins10Err, frsumins02Err, frsumins03Err, frsumins04Err, frsumins05Err, frsumins06Err, frsumins07Err, frsumins08Err, frsumins09Err, frsumins10Err, saband02Err, saband03Err, saband04Err, saband05Err, saband06Err, saband07Err, saband08Err, saband09Err, saband10Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr696screen.class;
		protectRecord = Sr696protect.class;
	}

}
