package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.regularprocessing.recordstructures.Ubblallpar;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

public class Vpmcbbl extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final String T5687 = "T5687";
	private static final String TR28X = "TR28X";
	private static final String F294 = "F294";
	private static final String ITEMREC = "ITEMREC";

	private T5687rec t5687rec = new T5687rec();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();

	private static final String SUBROUTINE_PREFIX = "CBBL";

	// COPY BOOK
	private Ubblallpar ubblallpar = new Ubblallpar();
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1090, 
		exit0190
	}

    public Vpmcbbl () {
        super();
    }

    public void mainline (Object... parmArray) {
		ubblallpar.ubblallRec = convertAndSetParam(ubblallpar.ubblallRec, parmArray, 0);
		
		vpmcalcrec.set(SPACES);
		ubblallpar.statuz.set(Varcom.oK);
		vpmcalcrec.statuz.set(Varcom.oK);
		
		setupKey1000();
		if(!isEQ(ubblallpar.statuz, Varcom.oK)){
			goTo(GotoLabel.exit0190);

		}
		vpmcalcrec.linkageArea.set(ubblallpar.ubblallRec);
		callProgram(Calcubbl.class , vpmcalcrec);
		ubblallpar.ubblallRec.set(vpmcalcrec.linkageArea);
		
		if(!isEQ(vpmcalcrec.statuz , Varcom.oK)) {
			ubblallpar.statuz.set(vpmcalcrec.statuz);
		}
		exitProgram();
    }

	private void setupKey1000() {
		//Read T5687 to get the benefit billing method.
		itdmIO.itemcoy.set(ubblallpar.chdrChdrcoy);
		itdmIO.itemtabl.set(T5687);
		itdmIO.itemitem.set(ubblallpar.crtable);
		itdmIO.itmfrm.set(ubblallpar.effdate);
		itdmIO.format.set(ITEMREC);
		itdmIO.function.set(Varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if( !isEQ(itdmIO.statuz , Varcom.oK)
				|| !isEQ(itdmIO.itemcoy , ubblallpar.chdrChdrcoy)
				|| !isEQ(itdmIO.itemtabl, T5687)
				|| !isEQ(itdmIO.itemitem, ubblallpar.crtable)){
			ubblallpar.statuz.set(F294);
			goTo(GotoLabel.exit1090);
		}
		t5687rec.t5687Rec.set(itdmIO.genarea);
		vpmcalcrec.vpmskey.set(SPACES);
		vpmcalcrec.vpmscoy.set(ubblallpar.chdrChdrcoy);
		vpmcalcrec.vpmstabl.set(TR28X);
		vpmcalcrec.vpmsitem.set(SUBROUTINE_PREFIX + ubblallpar.cnttype.toString().trim());
		vpmcalcrec.extkey.set(t5687rec.bbmeth);
	}

}
