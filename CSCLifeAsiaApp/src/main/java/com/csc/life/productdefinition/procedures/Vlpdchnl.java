/*
 * File: Vlpdchnl.java
 * Date: 30 August 2009 2:53:43
 * Author: Quipoz Limited
 * 
 * Class transformed from VLPDCHNL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.recordstructures.Vlpdsubrec;
import com.csc.life.productdefinition.tablestructures.Tr51drec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*    This subroutine will be processed to validate product agaist
*    table TR51D
*    - Validate product can only be sold by certain channel
*
***********************************************************************
* </pre>
*/
public class Vlpdchnl extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "VLPDCHNL";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaErrSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaErrCode = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaErrDet = new FixedLengthStringData(4);
	private int wsaaMaxError = 20;

	private FixedLengthStringData wsaaTr51dItmkey = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTr51dCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr51dItmkey, 0).init(SPACES);
	private FixedLengthStringData wsaaTr51dComponent = new FixedLengthStringData(4).isAPartOf(wsaaTr51dItmkey, 3).init(SPACES);
		/* FORMATS */
	private String itemrec = "ITEMREC";
		/* TABLES */
	private String tr51d = "TR51D";
		/* ERRORS */
	private String rlbp = "RLBP";
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Tr51drec tr51drec = new Tr51drec();
	private Varcom varcom = new Varcom();
	private Vlpdsubrec vlpdsubrec = new Vlpdsubrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		exit190, 
		exit490, 
		exit690
	}

	public Vlpdchnl() {
		super();
	}

public void mainline(Object... parmArray)
	{
		vlpdsubrec.validRec = convertAndSetParam(vlpdsubrec.validRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main000()
	{
		try {
			begin010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void begin010()
	{
		vlpdsubrec.statuz.set(varcom.oK);
		if (isEQ(vlpdsubrec.rid1Life,SPACES)
		&& isEQ(vlpdsubrec.rid1Jlife,SPACES)
		&& isEQ(vlpdsubrec.rid1Coverage,SPACES)
		&& isEQ(vlpdsubrec.rid1Rider,SPACES)
		&& isEQ(vlpdsubrec.rid1Crtable,SPACES)) {
			goTo(GotoLabel.exit090);
		}
		initialize100();
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit090);
		}
		validation200();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialize100()
	{
		try {
			begin110();
		}
		catch (GOTOException e){
		}
	}

protected void begin110()
	{
		wsaaErrSeq.set(1);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(vlpdsubrec.chdrcoy);
		itemIO.setItemtabl(tr51d);
		wsaaTr51dCnttype.set(vlpdsubrec.cnttype);
		wsaaTr51dComponent.set(vlpdsubrec.rid1Crtable);
		itemIO.setItemitem(wsaaTr51dItmkey);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit190);
		}
		tr51drec.tr51dRec.set(itemIO.getGenarea());
	}

protected void validation200()
	{
		/*BEGIN*/
		wsaaErrCode.set(rlbp);
		wsaaErrDet.set(SPACES);
		for (wsaaIx.set(1); !(isGT(wsaaIx,50)); wsaaIx.add(1)){
			if (isEQ(vlpdsubrec.srcebus,tr51drec.srcebus[wsaaIx.toInt()])) {
				wsaaErrCode.set(SPACES);
				wsaaIx.set(50);
			}
		}
		moveError400();
		/*EXIT*/
	}

protected void moveError400()
	{
		try {
			begin410();
		}
		catch (GOTOException e){
		}
	}

protected void begin410()
	{
		if (isGT(wsaaErrSeq,wsaaMaxError)
		|| isEQ(wsaaErrCode,SPACES)) {
			goTo(GotoLabel.exit490);
		}
		vlpdsubrec.errCode[wsaaErrSeq.toInt()].set(wsaaErrCode);
		vlpdsubrec.errDet[wsaaErrSeq.toInt()].set(wsaaErrDet);
		wsaaErrSeq.add(1);
	}

protected void fatalError600()
	{
		try {
			fatalErrors610();
		}
		catch (GOTOException e){
		}
		finally{
			exit690();
		}
	}

protected void fatalErrors610()
	{
		syserrrec.subrname.set(wsaaSubr);
		vlpdsubrec.statuz.set(syserrrec.statuz);
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit690);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit690()
	{
		exitProgram();
	}
}
