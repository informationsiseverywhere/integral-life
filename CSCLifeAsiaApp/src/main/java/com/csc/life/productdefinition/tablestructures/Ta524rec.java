package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:19:52
 * Description:
 * Copybook name: TA524REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ta524rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData ta524Rec = new FixedLengthStringData(10);
  	public ZonedDecimalData minmthif = new ZonedDecimalData(3, 0).isAPartOf(ta524Rec, 0);
  	public ZonedDecimalData maxphprd = new ZonedDecimalData(3, 0).isAPartOf(ta524Rec, 3);
  	public FixedLengthStringData prmstatus = new FixedLengthStringData(1).isAPartOf(ta524Rec, 6).init("Y");
  	public FixedLengthStringData znfopt = new FixedLengthStringData(3).isAPartOf(ta524Rec, 7);


	public void initialize() {
		COBOLFunctions.initialize(ta524Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ta524Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}