package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR50X
 * @version 1.0 generated on 30/08/09 07:15
 * @author Quipoz
 */
public class Sr50xScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(468);
	public FixedLengthStringData dataFields = new FixedLengthStringData(164).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData premsubrs = new FixedLengthStringData(120).isAPartOf(dataFields, 39);
	public FixedLengthStringData[] premsubr = FLSArrayPartOfStructure(15, 8, premsubrs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(120).isAPartOf(premsubrs, 0, FILLER_REDEFINE);
	public FixedLengthStringData premsubr01 = DD.subr.copy().isAPartOf(filler,0);
	public FixedLengthStringData premsubr02 = DD.subr.copy().isAPartOf(filler,8);
	public FixedLengthStringData premsubr03 = DD.subr.copy().isAPartOf(filler,16);
	public FixedLengthStringData premsubr04 = DD.subr.copy().isAPartOf(filler,24);
	public FixedLengthStringData premsubr05 = DD.subr.copy().isAPartOf(filler,32);
	public FixedLengthStringData premsubr06 = DD.subr.copy().isAPartOf(filler,40);
	public FixedLengthStringData premsubr07 = DD.subr.copy().isAPartOf(filler,48);
	public FixedLengthStringData premsubr08 = DD.subr.copy().isAPartOf(filler,56);
	public FixedLengthStringData premsubr09 = DD.subr.copy().isAPartOf(filler,64);
	public FixedLengthStringData premsubr10 = DD.subr.copy().isAPartOf(filler,72);
	public FixedLengthStringData premsubr11 = DD.subr.copy().isAPartOf(filler,80);
	public FixedLengthStringData premsubr12 = DD.subr.copy().isAPartOf(filler,88);
	public FixedLengthStringData premsubr13 = DD.subr.copy().isAPartOf(filler,96);
	public FixedLengthStringData premsubr14 = DD.subr.copy().isAPartOf(filler,104);
	public FixedLengthStringData premsubr15 = DD.subr.copy().isAPartOf(filler,112);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,159);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(76).isAPartOf(dataArea, 164);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData subrsErr = new FixedLengthStringData(60).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] subrErr = FLSArrayPartOfStructure(15, 4, subrsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(60).isAPartOf(subrsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData subr01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData subr02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData subr03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData subr04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData subr05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData subr06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData subr07Err = new FixedLengthStringData(4).isAPartOf(filler1, 24);
	public FixedLengthStringData subr08Err = new FixedLengthStringData(4).isAPartOf(filler1, 28);
	public FixedLengthStringData subr09Err = new FixedLengthStringData(4).isAPartOf(filler1, 32);
	public FixedLengthStringData subr10Err = new FixedLengthStringData(4).isAPartOf(filler1, 36);
	public FixedLengthStringData subr11Err = new FixedLengthStringData(4).isAPartOf(filler1, 40);
	public FixedLengthStringData subr12Err = new FixedLengthStringData(4).isAPartOf(filler1, 44);
	public FixedLengthStringData subr13Err = new FixedLengthStringData(4).isAPartOf(filler1, 48);
	public FixedLengthStringData subr14Err = new FixedLengthStringData(4).isAPartOf(filler1, 52);
	public FixedLengthStringData subr15Err = new FixedLengthStringData(4).isAPartOf(filler1, 56);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(228).isAPartOf(dataArea, 240);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData subrsOut = new FixedLengthStringData(180).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] subrOut = FLSArrayPartOfStructure(15, 12, subrsOut, 0);
	public FixedLengthStringData[][] subrO = FLSDArrayPartOfArrayStructure(12, 1, subrOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(180).isAPartOf(subrsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] subr01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] subr02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] subr03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] subr04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] subr05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] subr06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] subr07Out = FLSArrayPartOfStructure(12, 1, filler2, 72);
	public FixedLengthStringData[] subr08Out = FLSArrayPartOfStructure(12, 1, filler2, 84);
	public FixedLengthStringData[] subr09Out = FLSArrayPartOfStructure(12, 1, filler2, 96);
	public FixedLengthStringData[] subr10Out = FLSArrayPartOfStructure(12, 1, filler2, 108);
	public FixedLengthStringData[] subr11Out = FLSArrayPartOfStructure(12, 1, filler2, 120);
	public FixedLengthStringData[] subr12Out = FLSArrayPartOfStructure(12, 1, filler2, 132);
	public FixedLengthStringData[] subr13Out = FLSArrayPartOfStructure(12, 1, filler2, 144);
	public FixedLengthStringData[] subr14Out = FLSArrayPartOfStructure(12, 1, filler2, 156);
	public FixedLengthStringData[] subr15Out = FLSArrayPartOfStructure(12, 1, filler2, 168);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr50xscreenWritten = new LongData(0);
	public LongData Sr50xprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr50xScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(subr01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subr02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subr03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subr04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subr05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subr06Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subr07Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subr08Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subr09Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subr10Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subr11Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subr12Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subr13Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subr14Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subr15Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, premsubr01, premsubr02, premsubr03, premsubr04, premsubr05, premsubr06, premsubr07, premsubr08, premsubr09, premsubr10, premsubr11, premsubr12, premsubr13, premsubr14, premsubr15};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, subr01Out, subr02Out, subr03Out, subr04Out, subr05Out, subr06Out, subr07Out, subr08Out, subr09Out, subr10Out, subr11Out, subr12Out, subr13Out, subr14Out, subr15Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, subr01Err, subr02Err, subr03Err, subr04Err, subr05Err, subr06Err, subr07Err, subr08Err, subr09Err, subr10Err, subr11Err, subr12Err, subr13Err, subr14Err, subr15Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr50xscreen.class;
		protectRecord = Sr50xprotect.class;
	}

}
