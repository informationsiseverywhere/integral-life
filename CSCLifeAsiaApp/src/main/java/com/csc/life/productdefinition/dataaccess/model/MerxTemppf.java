package com.csc.life.productdefinition.dataaccess.model;

import java.math.BigDecimal;

public class MerxTemppf {
	
 	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private int seqno;
	private String paidby;
	private String exmcode;
	private String clntnum;
	private String name;
	private String zmedtyp;
	private int effdate;
	private String invref;
	private BigDecimal zmedfee;
	private String descT;
	private String memberName;
		
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public int getSeqno() {
		return seqno;
	}
	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}
	public String getPaidby() {
		return paidby;
	}
	public void setPaidby(String paidby) {
		this.paidby = paidby;
	}
	public String getExmcode() {
		return exmcode;
	}
	public void setExmcode(String exmcode) {
		this.exmcode = exmcode;
	}
	public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getZmedtyp() {
		return zmedtyp;
	}
	public void setZmedtyp(String zmedtyp) {
		this.zmedtyp = zmedtyp;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getInvref() {
		return invref;
	}
	public void setInvref(String invref) {
		this.invref = invref;
	}
	public BigDecimal getZmedfee() {
		return zmedfee;
	}
	public void setZmedfee(BigDecimal zmedfee) {
		this.zmedfee = zmedfee;
	}
	public String getDescT() {
		return descT;
	}
	public void setDescT(String descT) {
		this.descT = descT;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
}
