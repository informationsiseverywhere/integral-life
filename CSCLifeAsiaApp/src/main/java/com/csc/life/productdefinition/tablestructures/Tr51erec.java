package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:19:46
 * Description:
 * Copybook name: TR51EREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr51erec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr51eRec = new FixedLengthStringData(500);
  	public FixedLengthStringData ages = new FixedLengthStringData(30).isAPartOf(tr51eRec, 0);
  	public ZonedDecimalData[] age = ZDArrayPartOfStructure(10, 3, 0, ages, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(ages, 0, FILLER_REDEFINE);
  	public ZonedDecimalData age01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData age02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData age03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData age04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public ZonedDecimalData age05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData age06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData age07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData age08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
  	public ZonedDecimalData age09 = new ZonedDecimalData(3, 0).isAPartOf(filler, 24);
  	public ZonedDecimalData age10 = new ZonedDecimalData(3, 0).isAPartOf(filler, 27);
  	public FixedLengthStringData crtables = new FixedLengthStringData(80).isAPartOf(tr51eRec, 30);
  	public FixedLengthStringData[] crtable = FLSArrayPartOfStructure(20, 4, crtables, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(80).isAPartOf(crtables, 0, FILLER_REDEFINE);
  	public FixedLengthStringData crtable01 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
  	public FixedLengthStringData crtable02 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
  	public FixedLengthStringData crtable03 = new FixedLengthStringData(4).isAPartOf(filler1, 8);
  	public FixedLengthStringData crtable04 = new FixedLengthStringData(4).isAPartOf(filler1, 12);
  	public FixedLengthStringData crtable05 = new FixedLengthStringData(4).isAPartOf(filler1, 16);
  	public FixedLengthStringData crtable06 = new FixedLengthStringData(4).isAPartOf(filler1, 20);
  	public FixedLengthStringData crtable07 = new FixedLengthStringData(4).isAPartOf(filler1, 24);
  	public FixedLengthStringData crtable08 = new FixedLengthStringData(4).isAPartOf(filler1, 28);
  	public FixedLengthStringData crtable09 = new FixedLengthStringData(4).isAPartOf(filler1, 32);
  	public FixedLengthStringData crtable10 = new FixedLengthStringData(4).isAPartOf(filler1, 36);
  	public FixedLengthStringData crtable11 = new FixedLengthStringData(4).isAPartOf(filler1, 40);
  	public FixedLengthStringData crtable12 = new FixedLengthStringData(4).isAPartOf(filler1, 44);
  	public FixedLengthStringData crtable13 = new FixedLengthStringData(4).isAPartOf(filler1, 48);
  	public FixedLengthStringData crtable14 = new FixedLengthStringData(4).isAPartOf(filler1, 52);
  	public FixedLengthStringData crtable15 = new FixedLengthStringData(4).isAPartOf(filler1, 56);
  	public FixedLengthStringData crtable16 = new FixedLengthStringData(4).isAPartOf(filler1, 60);
  	public FixedLengthStringData crtable17 = new FixedLengthStringData(4).isAPartOf(filler1, 64);
  	public FixedLengthStringData crtable18 = new FixedLengthStringData(4).isAPartOf(filler1, 68);
  	public FixedLengthStringData crtable19 = new FixedLengthStringData(4).isAPartOf(filler1, 72);
  	public FixedLengthStringData crtable20 = new FixedLengthStringData(4).isAPartOf(filler1, 76);
  	public FixedLengthStringData indcs = new FixedLengthStringData(20).isAPartOf(tr51eRec, 110);
  	public FixedLengthStringData[] indc = FLSArrayPartOfStructure(10, 2, indcs, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(20).isAPartOf(indcs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData indc01 = new FixedLengthStringData(2).isAPartOf(filler2, 0);
  	public FixedLengthStringData indc02 = new FixedLengthStringData(2).isAPartOf(filler2, 2);
  	public FixedLengthStringData indc03 = new FixedLengthStringData(2).isAPartOf(filler2, 4);
  	public FixedLengthStringData indc04 = new FixedLengthStringData(2).isAPartOf(filler2, 6);
  	public FixedLengthStringData indc05 = new FixedLengthStringData(2).isAPartOf(filler2, 8);
  	public FixedLengthStringData indc06 = new FixedLengthStringData(2).isAPartOf(filler2, 10);
  	public FixedLengthStringData indc07 = new FixedLengthStringData(2).isAPartOf(filler2, 12);
  	public FixedLengthStringData indc08 = new FixedLengthStringData(2).isAPartOf(filler2, 14);
  	public FixedLengthStringData indc09 = new FixedLengthStringData(2).isAPartOf(filler2, 16);
  	public FixedLengthStringData indc10 = new FixedLengthStringData(2).isAPartOf(filler2, 18);
  	public FixedLengthStringData overdueMinas = new FixedLengthStringData(30).isAPartOf(tr51eRec, 130);
  	public ZonedDecimalData[] overdueMina = ZDArrayPartOfStructure(10, 3, 0, overdueMinas, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(30).isAPartOf(overdueMinas, 0, FILLER_REDEFINE);
  	public ZonedDecimalData overdueMina01 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 0);
  	public ZonedDecimalData overdueMina02 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 3);
  	public ZonedDecimalData overdueMina03 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 6);
  	public ZonedDecimalData overdueMina04 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 9);
  	public ZonedDecimalData overdueMina05 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 12);
  	public ZonedDecimalData overdueMina06 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 15);
  	public ZonedDecimalData overdueMina07 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 18);
  	public ZonedDecimalData overdueMina08 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 21);
  	public ZonedDecimalData overdueMina09 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 24);
  	public ZonedDecimalData overdueMina10 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 27);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(340).isAPartOf(tr51eRec, 160, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr51eRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr51eRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}