package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH549
 * @version 1.0 generated on 30/08/09 07:03
 * @author Quipoz
 */
public class Sh549ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(424);
	public FixedLengthStringData dataFields = new FixedLengthStringData(120).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData expfactor = DD.expfactor.copyToZonedDecimal().isAPartOf(dataFields,1);
	public FixedLengthStringData indcs = new FixedLengthStringData(4).isAPartOf(dataFields, 7);
	public FixedLengthStringData[] indc = FLSArrayPartOfStructure(2, 2, indcs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(indcs, 0, FILLER_REDEFINE);
	public FixedLengthStringData indc01 = DD.indc.copy().isAPartOf(filler,0);
	public FixedLengthStringData indc02 = DD.indc.copy().isAPartOf(filler,2);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,11);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,19);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,27);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,35);
	public FixedLengthStringData opcdas = new FixedLengthStringData(10).isAPartOf(dataFields, 65);
	public FixedLengthStringData[] opcda = FLSArrayPartOfStructure(5, 2, opcdas, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(10).isAPartOf(opcdas, 0, FILLER_REDEFINE);
	public FixedLengthStringData opcda01 = DD.opcda.copy().isAPartOf(filler1,0);
	public FixedLengthStringData opcda02 = DD.opcda.copy().isAPartOf(filler1,2);
	public FixedLengthStringData opcda03 = DD.opcda.copy().isAPartOf(filler1,4);
	public FixedLengthStringData opcda04 = DD.opcda.copy().isAPartOf(filler1,6);
	public FixedLengthStringData opcda05 = DD.opcda.copy().isAPartOf(filler1,8);
	public FixedLengthStringData subrtns = new FixedLengthStringData(40).isAPartOf(dataFields, 75);
	public FixedLengthStringData[] subrtn = FLSArrayPartOfStructure(5, 8, subrtns, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(subrtns, 0, FILLER_REDEFINE);
	public FixedLengthStringData subrtn01 = DD.subrtn.copy().isAPartOf(filler2,0);
	public FixedLengthStringData subrtn02 = DD.subrtn.copy().isAPartOf(filler2,8);
	public FixedLengthStringData subrtn03 = DD.subrtn.copy().isAPartOf(filler2,16);
	public FixedLengthStringData subrtn04 = DD.subrtn.copy().isAPartOf(filler2,24);
	public FixedLengthStringData subrtn05 = DD.subrtn.copy().isAPartOf(filler2,32);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,115);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(76).isAPartOf(dataArea, 120);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData expfactorErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData indcsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData[] indcErr = FLSArrayPartOfStructure(2, 4, indcsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(indcsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData indc01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData indc02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData opcdasErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData[] opcdaErr = FLSArrayPartOfStructure(5, 4, opcdasErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(20).isAPartOf(opcdasErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData opcda01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData opcda02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData opcda03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData opcda04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData opcda05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData subrtnsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData[] subrtnErr = FLSArrayPartOfStructure(5, 4, subrtnsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(20).isAPartOf(subrtnsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData subrtn01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData subrtn02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData subrtn03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData subrtn04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData subrtn05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(228).isAPartOf(dataArea, 196);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] expfactorOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData indcsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 24);
	public FixedLengthStringData[] indcOut = FLSArrayPartOfStructure(2, 12, indcsOut, 0);
	public FixedLengthStringData[][] indcO = FLSDArrayPartOfArrayStructure(12, 1, indcOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(24).isAPartOf(indcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] indc01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] indc02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData opcdasOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 96);
	public FixedLengthStringData[] opcdaOut = FLSArrayPartOfStructure(5, 12, opcdasOut, 0);
	public FixedLengthStringData[][] opcdaO = FLSDArrayPartOfArrayStructure(12, 1, opcdaOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(60).isAPartOf(opcdasOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] opcda01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] opcda02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] opcda03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] opcda04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] opcda05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData subrtnsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 156);
	public FixedLengthStringData[] subrtnOut = FLSArrayPartOfStructure(5, 12, subrtnsOut, 0);
	public FixedLengthStringData[][] subrtnO = FLSDArrayPartOfArrayStructure(12, 1, subrtnOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(60).isAPartOf(subrtnsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] subrtn01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] subrtn02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] subrtn03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] subrtn04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] subrtn05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sh549screenWritten = new LongData(0);
	public LongData Sh549protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh549ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(indc01Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(indc02Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(opcda01Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(opcda02Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(opcda03Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(opcda04Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(opcda05Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subrtn01Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subrtn02Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subrtn03Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subrtn04Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subrtn05Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, indc01, indc02, opcda01, opcda02, opcda03, opcda04, opcda05, subrtn01, subrtn02, subrtn03, subrtn04, subrtn05, expfactor};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, indc01Out, indc02Out, opcda01Out, opcda02Out, opcda03Out, opcda04Out, opcda05Out, subrtn01Out, subrtn02Out, subrtn03Out, subrtn04Out, subrtn05Out, expfactorOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, indc01Err, indc02Err, opcda01Err, opcda02Err, opcda03Err, opcda04Err, opcda05Err, subrtn01Err, subrtn02Err, subrtn03Err, subrtn04Err, subrtn05Err, expfactorErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh549screen.class;
		protectRecord = Sh549protect.class;
	}

}
