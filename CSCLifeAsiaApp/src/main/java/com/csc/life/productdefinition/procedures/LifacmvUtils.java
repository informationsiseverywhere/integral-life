/*********************  */
/*Author  :Liwei				  		*/
/*Purpose :Instead of subroutine Lifacmv*/
/*Date    :2018.10.26				*/
package com.csc.life.productdefinition.procedures;


public interface LifacmvUtils {
	public void calcLifacmv(LifacmvPojo lifacmvPojo) ;
}
