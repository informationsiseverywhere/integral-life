/*
 * File: Vpxsurc.java
 * Date: 4 Oct 2012 9:49:52
 * Author: Quipoz Limited
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.CovrpenTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsclmTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* VPMS - Full Surrender Calculation Update Routine
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*   This subroutine extracts data for the purpose of passing the
*   said data to the VPMS calculation subroutine.
*
*   First calls, this subroutine performs data extraction from UTRS
*   or UTRSCLM (Unit linked funds) depending on PLAN-SWITCH.
*
*   Next calls, this subroutine performs data extraction from HITS
*   (Interest bearing funds)
*
*   Functions:
*   ----------
*   INIT       - returns the various fields needed from files
*   GETV       - returns the value of a single variable to the caller
*
*   Outputs:
*   --------
*   CURDUNTBAL - Current Deemed Unit Bal(VFUND/UNITYP level)
*
*   Status:
*   -------
*   ****       - Everything checks out OK
*   E049       - Invalid Function
*   H832       - Field not valid
*
***********************************************************************
* </pre>
*/
public class Vpxsurc extends COBOLConvCodeModel {
	public static final String ROUTINE = QPUtilities.getThisClass();
	private String wsaaSubr = "VPXSURC";
	public FixedLengthStringData wsaaStart = new FixedLengthStringData(1);
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	public FixedLengthStringData wsaaChdrCoy = new FixedLengthStringData(1).init(SPACES);
	public FixedLengthStringData wsaaChdrNum = new FixedLengthStringData(8).init(SPACES);
	public FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	public FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	public FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	public FixedLengthStringData wsaaSvType = new FixedLengthStringData(1).init(SPACES);
	public FixedLengthStringData wsaaType = new FixedLengthStringData(1).init(SPACES);
	public FixedLengthStringData wsaaUnitType = new FixedLengthStringData(1).init(SPACES);
	public FixedLengthStringData wsaaVirtualFund = new FixedLengthStringData(4).init(SPACES);
	
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData wsaaPlanSuffixx = new ZonedDecimalData(4, 0).isAPartOf(wsaaPlanSuffix, 0, REDEFINE).setUnsigned();
	private ZonedDecimalData filler = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanSuffixx, 0, FILLER).setUnsigned();
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanSuffixx, 2).setUnsigned();
	
	private ZonedDecimalData wsaaPlanSfx = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator policy1 = new Validator(wsaaPlanSfx, "1");

	private ZonedDecimalData wsaaPlanSwitch = new ZonedDecimalData(1, 0).setUnsigned();
	private Validator wholePlan = new Validator(wsaaPlanSwitch, "1");
	private Validator partPlan = new Validator(wsaaPlanSwitch, "2");
	private Validator summaryPart = new Validator(wsaaPlanSwitch, "3");
	
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlnsfx = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	
	public FixedLengthStringData wsaaSwitch = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaSwitch, "Y");
	
	private FixedLengthStringData wsaaT5551Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5551Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5551Item, 0);
	private FixedLengthStringData wsaaT5551Key = new FixedLengthStringData(1).isAPartOf(wsaaT5551Item, 4);
	private FixedLengthStringData wsaaT5551Currcode = new FixedLengthStringData(3).isAPartOf(wsaaT5551Item, 5);

	private FixedLengthStringData wsaaT5515Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5515Fund = new FixedLengthStringData(4).isAPartOf(wsaaT5515Key, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(wsaaT5515Key, 4, FILLER);
	
	private FixedLengthStringData wsaaConstants = new FixedLengthStringData(16);
	private FixedLengthStringData wsccA = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 0).init("A");
	private FixedLengthStringData wsccAskterisk = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 1).init("*");
	private FixedLengthStringData wsccC = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 2).init("C");
	private FixedLengthStringData wsccD = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 3).init("D");
	private FixedLengthStringData wsccF = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 4).init("F");
	private FixedLengthStringData wsccI = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 5).init("I");
	private FixedLengthStringData wsccJ = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 6).init("J");
	private FixedLengthStringData wsccL = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 7).init("L");
	private FixedLengthStringData wsccLE = new FixedLengthStringData(2).isAPartOf(wsaaConstants, 8).init("LE");
	private FixedLengthStringData wsccNegative = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 10).init("-");
	private FixedLengthStringData wsccP = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 11).init("P");
	private FixedLengthStringData wsccY = new FixedLengthStringData(1).isAPartOf(wsaaConstants, 12).init("Y");
	private FixedLengthStringData wsccPen = new FixedLengthStringData(3).isAPartOf(wsaaConstants, 13).init("PEN");
	
	/* ERRORS */
	private String e049 = "E049";	
	private String h832 = "H832";	
	
	private String t5515 = "T5515";
	private String t5551 = "T5551";
	private String t6644 = "T6644";

	/* FORMATS */
	private String acblrec = "ACBLREC";
	private String covrpenrec = "COVRPENREC";
	private String descrec = "DESCREC";
	private String hitsrec = "HITSREC";
	private String itemrec = "ITEMREC";
	private String utrsrec = "UTRSREC";
	private String utrsclmrec = "UTRSCLMREC";
	private String vprnudlrec = "VPRNUDLREC";
	
	/* COPYBOOK */
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
	private Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5515rec t5515rec = new T5515rec();
	private T5551rec t5551rec = new T5551rec();
	private T5645rec t5645rec = new T5645rec();
	private T6598rec t6598rec = new T6598rec();

	private CovrpenTableDAM covrpenIO = new CovrpenTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private UtrsclmTableDAM utrsclmIO = new UtrsclmTableDAM();
	
	//ILIFE6594
	private List<Itempf> t5645List;
	private List<Itempf> t6598List;
	private Map<String,Descpf>  t6644Map;
	private Map<String,Descpf>  t5515Map;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	private Acblpf acblpf = null;
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit100, 
		exit000, 
		partial120, 
		intBearing150, 
		endp170, 
		endp190, 
		readT5551181, 
		exit180, 
		exitB190, 
		exitB259, 
		exitX190, 
		exitX290, 
		exitX390
	}

	public Vpxsurc() {
		super();
	}

	public void mainline(Object... parmArray)
	{
		vpxsurcrec = (Vpxsurcrec) parmArray[1];
		vpmcalcrec.vpmcalcRec = convertAndSetParam(vpmcalcrec.vpmcalcRec, parmArray, 0);
		srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
		
		try {
			main000();
			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para000();
				}
				case exit000: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para000()
{
	syserrrec.subrname.set(wsaaSubr);
	vpxsurcrec.statuz.set(varcom.oK);
	
	if (isEQ(vpxsurcrec.function,"INIT")){
		initialize050();
		findRecord100();
	}
	else if (isEQ(vpxsurcrec.function,"GETV")) {
		returnValue200();
	}
	else {
		syserrrec.statuz.set(e049);
		vpxsurcrec.statuz.set(e049);
		systemError900();
	}	
}

protected void initSmartTable(){
	String coy = srcalcpy.chdrChdrcoy.toString().trim();
	String pfx = smtpfxcpy.item.toString().trim();
//	t5515List = itemDAO.getAllitems("IT", atmodrec.company.toString(), "T5515");
//	t5551List = itemDAO.getAllitems("IT", atmodrec.company.toString(), "T5551");
	t5645List = itemDAO.getAllitems(pfx,coy, "T5645");
	t6598List = itemDAO.getAllitems(pfx,coy, "T6598");
	t6644Map = descDAO.getItems(pfx, coy, "T6644",  srcalcpy.language.toString().trim());
	t5515Map = descDAO.getItems(pfx, coy, "T5515",  srcalcpy.language.toString().trim());
}
protected void initialize050()
{
	//Initialize the output fields.
	vpxsurcrec.curduntbal.set(ZERO);
	vpxsurcrec.modPrem.set(ZERO);
	vpxsurcrec.paidPrem.set(ZERO);
	vpxsurcrec.chargeCalc.set(SPACES);
	wsaaSvType.set(srcalcpy.type);
	initSmartTable();
}

protected void findRecord100()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				search100();
			}
			case partial120: {
				partial120();
			}
			case intBearing150: {
				intBearing150();
			}
			case endp170: {
				endp170();
			}
			case endp190: {
				endp190();
			}
			case exit100: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void search100()
{
	if (firstTime.isTrue()){
		wsaaChdrCoy.set(srcalcpy.chdrChdrcoy);
		wsaaChdrNum.set(srcalcpy.chdrChdrnum);
		wsaaCoverage.set(srcalcpy.covrCoverage);
		wsaaRider.set(srcalcpy.covrRider);
		wsaaLife.set(srcalcpy.lifeLife);
		wsaaPlanSuffix.set(srcalcpy.planSuffix);
		wsaaVirtualFund.set(SPACES);
		wsaaUnitType.set(SPACES);
		if (isEQ(srcalcpy.planSuffix, ZERO)){
			wsaaPlanSwitch.set(1);
		} 
		else {
			if (!isGT(srcalcpy.planSuffix, srcalcpy.polsum) 
					&& isNE(srcalcpy.polsum, 1)){
				wsaaPlanSuffix.set(0);
				wsaaPlanSwitch.set(3);
				if (isEQ(srcalcpy.planSuffix, 1)){
					wsaaPlanSfx.set(1);
				} else {
					wsaaPlanSfx.set(0);
				}
				
			} else {
				wsaaPlanSwitch.set(2);
			}
		}
	}
	
	if (summaryPart.isTrue()){
		srcalcpy.planSuffix.set(0);
	}
	
	/*IVE-796 RUL Product - Partial Surrender Calculation started*/
	if(isEQ(srcalcpy.endf, wsccL)){
		goTo(GotoLabel.endp190);
	}
	/*IVE-796 RUL Product - Partial Surrender Calculation end*/
	if (isEQ(srcalcpy.endf, wsccY)){
		if (isEQ(wsaaSvType, wsccF)){
			goTo(GotoLabel.endp170);
		} else {
			if (isEQ(wsaaSvType, wsccP)){
				goTo(GotoLabel.endp190);
			}
		}
	}
	
	if (isEQ(srcalcpy.endf, wsccJ)){
		goTo(GotoLabel.endp190);
	}
	
	if (isEQ(srcalcpy.endf, wsccI)){
		goTo(GotoLabel.intBearing150);
	}
	
	if (!wholePlan.isTrue()){
		goTo(GotoLabel.partial120);
	}
	
	//Set-up keys
	initialize(utrsIO.getDataArea());
	utrsIO.setChdrcoy(wsaaChdrCoy);
	utrsIO.setChdrnum(wsaaChdrNum);
	utrsIO.setLife(wsaaLife);
	utrsIO.setCoverage(wsaaCoverage);
	utrsIO.setRider(wsaaRider);
	utrsIO.setPlanSuffix(ZERO);
	utrsIO.setUnitVirtualFund(wsaaVirtualFund);
	utrsIO.setUnitType(wsaaUnitType);
	utrsIO.setFunction(varcom.begn);
	//Call FILE-IO
	callUtrsIOX100();

	vpxsurcrec.curduntbal.set(ZERO);
	vpxsurcrec.chargeCalc.set(SPACES);
	
	if (isEQ(utrsIO.getStatuz(),varcom.oK)){
		processUtrs110();
	} else {
		wsaaVirtualFund.set(SPACES);
	}
	goTo(GotoLabel.exit100);
}

protected void partial120()
{
	//Set-up keys
	initialize(utrsclmIO.getDataArea());
	utrsclmIO.setChdrcoy(wsaaChdrCoy);
	utrsclmIO.setChdrnum(wsaaChdrNum);
	utrsclmIO.setLife(wsaaLife);
	utrsclmIO.setCoverage(wsaaCoverage);
	utrsclmIO.setRider(wsaaRider);
	utrsclmIO.setPlanSuffix(wsaaPlanSuffix);
	utrsclmIO.setUnitVirtualFund(wsaaVirtualFund);
	utrsclmIO.setUnitType(wsaaUnitType);
	utrsclmIO.setFunction(varcom.begn);
	//Call FILE-IO
	callUtrsclmIOX200();
	
	vpxsurcrec.curduntbal.set(ZERO);
	vpxsurcrec.chargeCalc.set(SPACES);
	
	if (isEQ(utrsclmIO.getStatuz(),varcom.oK)){
		processUtrsclm130();
	} else {
		wsaaVirtualFund.set(SPACES);
	}
	goTo(GotoLabel.exit100);
}

protected void intBearing150()
{
	//Set-up keys
	initialize(hitsIO.getDataArea());
	hitsIO.setChdrcoy(wsaaChdrCoy);
	hitsIO.setChdrnum(wsaaChdrNum);
	hitsIO.setLife(wsaaLife);
	hitsIO.setCoverage(wsaaCoverage);
	hitsIO.setRider(wsaaRider);
	hitsIO.setPlanSuffix(ZERO);
	hitsIO.setZintbfnd(wsaaVirtualFund);
	hitsIO.setFunction(varcom.begn);
	//Call FILE-IO
	callHitsIOX300();
	
	vpxsurcrec.curduntbal.set(ZERO);
	vpxsurcrec.chargeCalc.set(SPACES);
	
	while (isEQ(hitsIO.getStatuz(),varcom.oK)){
		processHits160();
	} 
	//ILIFE-9125
	vpxsurcrec.statuz.set(varcom.endp);
	
	goTo(GotoLabel.exit100);
}

protected void endp170()
{
	if (summaryPart.isTrue()){
		if (policy1.isTrue()){
			compute(srcalcpy.singp, 2).set(sub(srcalcpy.singp, 
					div(mult(srcalcpy.singp, sub(srcalcpy.polsum, 1)), srcalcpy.polsum)));
		} else {
			compute(srcalcpy.singp, 2).setRounded(div(srcalcpy.singp, srcalcpy.polsum));
		}
	}
		
	if (isNE(srcalcpy.type, wsccF)
				&& isNE(srcalcpy.type, wsccP)){
		vpxsurcrec.chargeCalc.set(SPACES);
	} else {
		checkChargeCalc180();
	}
	
	if(t6644Map != null){
		if(t6644Map.get(wsccPen.toString()) != null){
			srcalcpy.description.set(t6644Map.get(wsccPen.toString()).getShortdesc());
		}else{
			srcalcpy.description.fill(SPACES);
		}
	}else{
		srcalcpy.description.fill(SPACES);
	}
	
	if (isEQ(wsaaSvType, wsccF)){
		srcalcpy.status.set(varcom.endp);
		wsaaSwitch.set(wsccY);
		wsaaVirtualFund.set(SPACES);
		srcalcpy.currcode.set(SPACES);
		
		if (isEQ(vpxsurcrec.chargeCalc, SPACES)){
			srcalcpy.type.set(SPACES);
		} else {
			srcalcpy.type.set(wsccC);
		}
	} else {
		if (isEQ(wsaaSvType, wsccP)){
			srcalcpy.currcode.set(srcalcpy.chdrCurr);
			srcalcpy.type.set(wsccJ);
			srcalcpy.endf.set(wsccJ);
		} 	
	}
	srcalcpy.fund.set(SPACES);
	goTo(GotoLabel.exit100);
}

protected void endp190()
{
	if (summaryPart.isTrue()){
		if (policy1.isTrue()){
			compute(srcalcpy.singp, 2).set(sub(srcalcpy.singp, 
					div(mult(srcalcpy.singp, sub(srcalcpy.polsum, 1)), srcalcpy.polsum)));
		} else {
			compute(srcalcpy.singp, 2).setRounded(div(srcalcpy.singp, srcalcpy.polsum));
		}
	}
	
	if(isEQ(srcalcpy.type,wsccJ) && isNE(srcalcpy.endf,wsccL))
	{	
		srcalcpy.type.set(wsccL);
		srcalcpy.currcode.set(SPACES);
		wsaaVirtualFund.set(SPACES);
		srcalcpy.endf.set(SPACES);
		srcalcpy.fund.set(SPACES);
		
		wsaaSwitch.set(wsccY);
	}
	else if(isEQ(srcalcpy.endf,wsccL))
	{	
		srcalcpy.type.set(wsccL);
		srcalcpy.currcode.set(SPACES);
		wsaaVirtualFund.set(SPACES);
		srcalcpy.endf.set(SPACES);
		srcalcpy.fund.set(SPACES);
		
		wsaaSwitch.set(wsccY);
	}
	else if(isEQ(srcalcpy.type,wsccL))
	{	
		srcalcpy.type.set(wsccL);
		srcalcpy.currcode.set(SPACES);
		wsaaVirtualFund.set(SPACES);
		srcalcpy.endf.set(SPACES);
		srcalcpy.fund.set(SPACES);
		
		wsaaSwitch.set(wsccY);
	}
	else
	{
	
		if (isEQ(wsaaSvType, wsccF)){
			srcalcpy.status.set(varcom.endp);
			wsaaSwitch.set(wsccY);
			wsaaVirtualFund.set(SPACES);
			srcalcpy.currcode.set(SPACES);
			
			if (isEQ(vpxsurcrec.chargeCalc, SPACES)){
				srcalcpy.type.set(SPACES);
			} else {
				srcalcpy.type.set(wsccC);
			}
		} else {
			if (isEQ(wsaaSvType, wsccP)){
				srcalcpy.currcode.set(srcalcpy.chdrCurr);
				srcalcpy.type.set(wsccJ);
				srcalcpy.endf.set(wsccJ);
			} 	
		}
		srcalcpy.fund.set(SPACES);
	}
	
	
	
}

protected void processUtrs110()
{
	srcalcpy.element.set(utrsIO.getUnitVirtualFund());
	srcalcpy.fund.set(utrsIO.getUnitVirtualFund());
	srcalcpy.type.set(wsaaType);
	
	while (!(isNE(utrsIO.getStatuz(), varcom.oK)
			|| isNE(utrsIO.getUnitVirtualFund(), srcalcpy.fund)
			|| isNE(wsaaType, srcalcpy.type))){
		vpxsurcrec.curduntbal.add(utrsIO.getCurrentDunitBal());
		utrsIO.setFunction(varcom.nextr);
		callUtrsIOX100();
	}
	item140();
	wsaaVirtualFund.set(utrsIO.getUnitVirtualFund());
	wsaaUnitType.set(utrsIO.getUnitType());
	wsaaLife.set(utrsIO.getLife());
	wsaaRider.set(utrsIO.getRider());
	wsaaCoverage.set(utrsIO.getCoverage());
	wsaaChdrNum.set(utrsIO.getChdrnum());
	
	if (isEQ(srcalcpy.endf, "I")){
		wsaaVirtualFund.set(SPACES);
		wsaaChdrNum.set(srcalcpy.chdrChdrnum);
		wsaaCoverage.set(srcalcpy.covrCoverage);
		wsaaRider.set(srcalcpy.covrRider);
		wsaaLife.set(srcalcpy.lifeLife);
	}
	if(isNE(vpxsurcrec.statuz,varcom.endp))
	{
		wsaaSwitch.set("N");
	}
}

protected void processUtrsclm130()
{
	srcalcpy.element.set(utrsclmIO.getUnitVirtualFund());
	srcalcpy.fund.set(utrsclmIO.getUnitVirtualFund());
	srcalcpy.type.set(wsaaType);
	vpxsurcrec.curduntbal.set(ZERO);
	
	while (!(isNE(utrsclmIO.getStatuz(), varcom.oK)
			|| isNE(utrsclmIO.getUnitVirtualFund(), srcalcpy.fund)
			|| isNE(wsaaType, srcalcpy.type))){
		vpxsurcrec.curduntbal.add(utrsclmIO.getCurrentDunitBal());
		utrsclmIO.setFunction(varcom.nextr);
		callUtrsclmIOX200();
	}
	item140();
	wsaaVirtualFund.set(utrsclmIO.getUnitVirtualFund());
	wsaaUnitType.set(utrsclmIO.getUnitType());
	wsaaLife.set(utrsclmIO.getLife());
	wsaaRider.set(utrsclmIO.getRider());
	wsaaCoverage.set(utrsclmIO.getCoverage());
	wsaaChdrNum.set(utrsclmIO.getChdrnum());
	
	if (isEQ(srcalcpy.endf, "I")){
		wsaaVirtualFund.set(SPACES);
		wsaaChdrNum.set(srcalcpy.chdrChdrnum);
		wsaaCoverage.set(srcalcpy.covrCoverage);
		wsaaRider.set(srcalcpy.covrRider);
		wsaaLife.set(srcalcpy.lifeLife);
	}
	wsaaSwitch.set("N");
}

protected void item140()
{
	initialize(itdmIO.getDataArea());
	itdmIO.setItemtabl(t5515);
	itdmIO.setItemcoy(wsaaChdrCoy);
	wsaaT5515Key.set(SPACES);
	wsaaT5515Fund.set(srcalcpy.fund);
	itdmIO.setItemitem(wsaaT5515Key);
	itdmIO.setItmfrm(srcalcpy.effdate);
	itdmIO.setFunction(varcom.begn);
	
	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)){
		syserrrec.statuz.set(itdmIO.getStatuz());
		syserrrec.params.set(itdmIO.getParams());
		dbError999();
	}
	if (isNE(itdmIO.getStatuz(), varcom.oK)
			|| isNE(wsaaChdrCoy, itdmIO.getItemcoy())
			|| isNE(t5515, itdmIO.getItemtabl())
			|| isNE(wsaaT5515Key, itdmIO.getItemitem())) {
		syserrrec.statuz.set(itdmIO.getStatuz());
		vpxsurcrec.statuz.set(itdmIO.getStatuz());
		syserrrec.params.set(itdmIO.getParams());
		systemError900();
	}
	t5515rec.t5515Rec.set(itdmIO.getGenarea());
	srcalcpy.currcode.set(t5515rec.currcode);

	if(t5515Map != null){
		if(t5515Map.get(srcalcpy.fund.toString()) != null){
			srcalcpy.description.set(t5515Map.get(srcalcpy.fund.toString()).getLongdesc());
		}else{
			srcalcpy.description.fill(SPACES);
		}
	}else {
		srcalcpy.description.fill(SPACES);
	}
}

protected void processHits160()
{
	srcalcpy.element.set(hitsIO.getZintbfnd());
	srcalcpy.fund.set(hitsIO.getZintbfnd());
	srcalcpy.type.set(wsccD);
	vpxsurcrec.curduntbal.set(ZERO);
	
	while (!(isNE(hitsIO.getStatuz(), varcom.oK)
			|| isNE(hitsIO.getZintbfnd(), srcalcpy.fund))){
		vpxsurcrec.curduntbal.add(hitsIO.getZcurprmbal());
		hitsIO.setFunction(varcom.nextr);
		callHitsIOX300();
	}
	item140();
}

protected void checkChargeCalc180()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				start180();
			}
			case readT5551181: {
				readT5551181();
			}
			case exit180: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}	
}


protected void start180()
{
	vpxsurcrec.chargeCalc.set(SPACES);
	wsaaT5551Key.set(srcalcpy.language);
}

protected void readT5551181()
{
	initialize(itdmIO.getDataArea());
	itdmIO.setItemcoy(wsaaChdrCoy);
	itdmIO.setItemtabl(t5551);
	wsaaT5551Crtable.set(srcalcpy.crtable);
	wsaaT5551Currcode.set(srcalcpy.chdrCurr);
	itdmIO.setItemitem(wsaaT5551Item);
	itdmIO.setItmfrm(srcalcpy.crrcd);
	itdmIO.setFunction(varcom.begn);
	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)){
		syserrrec.statuz.set(itdmIO.getStatuz());
		syserrrec.params.set(itdmIO.getParams());
		dbError999();
	}
	
	if (isEQ(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getItemitem(), wsaaT5551Item)){
		wsaaT5551Key.set(wsccAskterisk);
		goTo(GotoLabel.readT5551181);
	}
	
	if (isNE(itdmIO.getStatuz(), varcom.oK)
			|| isNE(wsaaChdrCoy, itdmIO.getItemcoy())
			|| isNE(t5551, itdmIO.getItemtabl())
			|| isNE(wsaaT5551Item, itdmIO.getItemitem())) {
		goTo(GotoLabel.exit180);		
	} else {
		t5551rec.t5551Rec.set(itdmIO.getGenarea());
	}
	
	if (isEQ(t5551rec.svcmeth, SPACES)){
		goTo(GotoLabel.exit180);
	}
	
	Iterator<Itempf> itemIterator = t6598List.iterator();
	boolean itemfound = false;
	Itempf itempf = null;
	while(itemIterator.hasNext()){
			itempf=itemIterator.next();
			if(itempf.getItemitem().trim().equals(t5551rec.svcmeth.toString().trim())){
				t6598rec.t6598Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemfound = true;
				break;
			}
	}
	if(!itemfound){
		t6598rec.t6598Rec.set(SPACES);
	}
	if (isNE(t6598rec.calcprog, SPACES)){
		vpxsurcrec.chargeCalc.set(itempf.getItemitem());
	}
	
	if (isNE(vpxsurcrec.chargeCalc, SPACES)){
		getModalPremB100();
		getPaidPremB200();
	}
}

protected void getModalPremB100()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				startB100();
			}
			case exitB190: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void startB100()
{
	if (!(isEQ(srcalcpy.planSuffix, ZERO)
			&& !summaryPart.isTrue())){
		vpxsurcrec.modPrem.set(srcalcpy.singp);
		goTo(GotoLabel.exitB190);
	}
	
		
	initialize(covrpenIO.getDataArea());	
	covrpenIO.setStatuz(varcom.oK);
	covrpenIO.setChdrcoy(srcalcpy.chdrChdrcoy);
	covrpenIO.setChdrnum(srcalcpy.chdrChdrnum);
	covrpenIO.setLife(srcalcpy.lifeLife);
	covrpenIO.setCoverage(srcalcpy.covrCoverage);
	covrpenIO.setRider(srcalcpy.covrRider);
	covrpenIO.setFunction(varcom.begn);
	covrpenIO.setFormat(covrpenrec);
	
	callCovrpenIOX400();
	while (isEQ(covrpenIO.getStatuz(), varcom.oK)){
		compute(vpxsurcrec.modPrem, 2).set(add(vpxsurcrec.modPrem, covrpenIO.getInstprem()));
		covrpenIO.setFunction(varcom.nextr);
		callCovrpenIOX400();
	}
}

protected void getPaidPremB200()
{
	Iterator<Itempf> itemIterator = t5645List.iterator();
	boolean itemfound = false;
	while(itemIterator.hasNext()){
			Itempf itempf=itemIterator.next();
			if(itempf.getItemitem().trim().equals(t6598rec.calcprog.toString().trim())){
				t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemfound = true;
				break;
			}
	}
	if(!itemfound){
		t6598rec.t6598Rec.set(SPACES);
	}
	for (ix.set(1);!(isGT(ix, 15) 
			|| isEQ(t5645rec.sacscode[ix.toInt()], SPACES)); ix.add(1)){
		accumAcblB250();
	}
}

protected void accumAcblB250()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				startB251();
			}
			case exitB259: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void startB251(){
	String rldgacct = "";
	if (isEQ(t5645rec.sacscode[ix.toInt()], wsccLE)){
		wsaaRldgChdrnum.set(srcalcpy.chdrChdrnum);
		wsaaRldgLife.set(srcalcpy.lifeLife);
		wsaaRldgCoverage.set(srcalcpy.covrCoverage);
		wsaaRldgRider.set(srcalcpy.covrRider);
		wsaaPlanSuffix.set(srcalcpy.planSuffix);
		wsaaRldgPlnsfx.set(wsaaPlnsfx);
		rldgacct = wsaaRldgacct.toString().trim();
	} else {
		rldgacct = srcalcpy.chdrChdrnum.toString().trim();
	}
	
	//ILIFE-6594
	acblpf = acblDao.getAcblpfRecord(srcalcpy.chdrChdrcoy.toString().trim(), t5645rec.sacscode[ix.toInt()].toString().trim(), rldgacct,
			t5645rec.sacstype[ix.toInt()].toString().trim(),srcalcpy.chdrCurr.toString().trim());

	if (acblpf == null){
		goTo(GotoLabel.exitB259);
	}
	
	if (isEQ(t5645rec.sign[ix.toInt()], wsccNegative)){
		compute(vpxsurcrec.paidPrem, 2).set(sub(vpxsurcrec.paidPrem, acblpf.getSacscurbal()));
	} else {
		compute(vpxsurcrec.paidPrem, 2).set(add(vpxsurcrec.paidPrem, acblpf.getSacscurbal()));
	}
}

protected void callUtrsIOX100()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				startX110();
			}
			case exitX190: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}


protected void startX110()
{
	utrsIO.setFormat(utrsrec);
	//performance improvement --  Niharika Modi 
	utrsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	utrsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
	SmartFileCode.execute(appVars, utrsIO);

	if (isNE(utrsIO.getStatuz(), varcom.oK)
			&& isNE(utrsIO.getStatuz(), varcom.endp)){
		syserrrec.statuz.set(utrsIO.getStatuz());
		syserrrec.params.set(utrsIO.getParams());
		dbError999();
	}
	
	if (isEQ(utrsIO.getStatuz(), varcom.endp)
			|| isNE(wsaaChdrCoy, utrsIO.getChdrcoy())
			|| isNE(wsaaChdrNum, utrsIO.getChdrnum())
			|| isNE(wsaaLife, utrsIO.getLife())
			|| isNE(wsaaCoverage, utrsIO.getCoverage())
			|| isNE(srcalcpy.planSuffix, utrsIO.getPlanSuffix())//TODO
			|| isNE(wsaaRider, utrsIO.getRider())) {
		utrsIO.setStatuz(varcom.endp);
		
		//wsaaSwitch.set(wsccY);
		/*IVE-796 RUL Product - Full Surrender Calculation started*/
		/*ILIFE-9125 start*/
		if(isNE(utrsIO.getFunction(),varcom.nextr)){
			srcalcpy.endf.set(wsccI);
		}
		else {
			vpxsurcrec.statuz.set(varcom.endp);
		}
		/*ILIFE-9125 end*/
		/*IVE-796 RUL Product - Full Surrender Calculation end*/
		goTo(GotoLabel.exitX190);
	}
	
	if (isEQ(utrsIO.getUnitType(), wsccI)){
		wsaaType.set(wsccI);
	} else {
		wsaaType.set(wsccA);
	}
}

protected void callUtrsclmIOX200()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				startX210();
			}
			case exitX290: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}


protected void startX210()
{
	utrsclmIO.setFormat(utrsclmrec);
	SmartFileCode.execute(appVars, utrsclmIO);

	if (isNE(utrsclmIO.getStatuz(), varcom.oK)
			&& isNE(utrsclmIO.getStatuz(), varcom.endp)){
		syserrrec.statuz.set(utrsclmIO.getStatuz());
		syserrrec.params.set(utrsclmIO.getParams());
		dbError999();
	}
	
	if (isEQ(utrsclmIO.getStatuz(), varcom.endp)
			|| isNE(wsaaChdrCoy, utrsclmIO.getChdrcoy())
			|| isNE(wsaaChdrNum, utrsclmIO.getChdrnum())
			|| isNE(wsaaLife, utrsclmIO.getLife())
			|| isNE(wsaaCoverage, utrsclmIO.getCoverage())
			|| isNE(wsaaRider, utrsclmIO.getRider())
			|| isNE(wsaaPlanSuffix, utrsclmIO.getPlanSuffix())) {
		utrsclmIO.setStatuz(varcom.endp);
		srcalcpy.endf.set(wsccI);
		goTo(GotoLabel.exitX290);
	}
	
	if (isEQ(utrsclmIO.getUnitType(), wsccI)){
		wsaaType.set(wsccI);
	} else {
		wsaaType.set(wsccA);
	}
}


protected void callHitsIOX300()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				startX310();
			}
			case exitX390: {
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}


protected void startX310()
{
	hitsIO.setFormat(hitsrec);
	SmartFileCode.execute(appVars, hitsIO);

	if (isNE(hitsIO.getStatuz(), varcom.oK)
			&& isNE(hitsIO.getStatuz(), varcom.endp)){
		syserrrec.statuz.set(hitsIO.getStatuz());
		syserrrec.params.set(hitsIO.getParams());
		dbError999();
	}
	if (isEQ(hitsIO.getStatuz(), varcom.endp)
			|| isNE(wsaaChdrCoy, hitsIO.getChdrcoy())
			|| isNE(wsaaChdrNum, hitsIO.getChdrnum())
			|| isNE(wsaaLife, hitsIO.getLife())
			|| isNE(wsaaCoverage, hitsIO.getCoverage())
			|| isNE(wsaaRider, hitsIO.getRider())){
		hitsIO.setStatuz(varcom.endp);
		if (isEQ(wsaaSvType, wsccF)){
			srcalcpy.endf.set(wsccY);
		} else {
			if (isEQ(wsaaSvType, wsccP)){
				srcalcpy.endf.set(wsccJ);
			} 
		}
		goTo(GotoLabel.exitX390);
	}
}

protected void callCovrpenIOX400()
{
	SmartFileCode.execute(appVars, covrpenIO);
	if (isNE(covrpenIO.getStatuz(), varcom.oK)
			&& isNE(covrpenIO.getStatuz(), varcom.endp)){
		syserrrec.statuz.set(covrpenIO.getStatuz());
		syserrrec.params.set(covrpenIO.getParams());
		dbError999();
	}
	if (isEQ(covrpenIO.getStatuz(), varcom.endp)
			|| isNE(srcalcpy.chdrChdrcoy, covrpenIO.getChdrcoy())
			|| isNE(srcalcpy.chdrChdrnum, covrpenIO.getChdrnum())
			|| isNE(srcalcpy.lifeLife, covrpenIO.getLife())
			|| isNE(srcalcpy.covrCoverage, covrpenIO.getCoverage())
			|| isNE(srcalcpy.covrRider, covrpenIO.getRider())){
		covrpenIO.setStatuz(varcom.endp);
	}
}

protected void returnValue200()
{
	vpxsurcrec.resultValue.set(SPACES);
	String field = vpxsurcrec.resultField.getData().trim();/* IJTI-1523 */
	
	if ("curduntbal".equals(field)){
		vpxsurcrec.resultValue.set(vpxsurcrec.curduntbal.getbigdata().toString());
	}
	else if("chargeCalc".equals(field)){
		vpxsurcrec.resultValue.set(vpxsurcrec.chargeCalc);
	}
	else if("modPrem".equals(field)){
		vpxsurcrec.resultValue.set(vpxsurcrec.modPrem.getbigdata().toString());
	}
	else if("paidPrem".equals(field)){
		vpxsurcrec.resultValue.set(vpxsurcrec.paidPrem.getbigdata().toString());
	}
	else {
		vpxsurcrec.statuz.set(h832);
	}
}

protected void systemError900()
{
	/*PARA*/
	syserrrec.syserrStatuz.set(syserrrec.statuz);
	syserrrec.syserrType.set("2");
	callProgram(Syserr.class, syserrrec.syserrRec);
	exit000();
	/*EXIT*/
}

protected void dbError999()
{
	/*PARA*/
	syserrrec.syserrType.set("1");
	callProgram(Syserr.class, syserrrec.syserrRec);
	exit000();
	/*EXIT*/
}

protected void exit000()
{
	exitProgram();
}

}
