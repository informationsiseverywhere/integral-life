/*
 * File: T5687pt.java
 * Date: 30 August 2009 2:26:04
 * Author: Quipoz Limited
 * 
 * Class transformed from T5687PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5687.
*
*
*****************************************************************
* </pre>
*/
public class T5687pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(75);
	private FixedLengthStringData filler1 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine001, 27, FILLER).init("General Cov/Rider Details                  S5687");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(75);
	private FixedLengthStringData filler3 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 10);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 11, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 21);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 26, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 34);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 45);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(44);
	private FixedLengthStringData filler7 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Valid from:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 14);
	private FixedLengthStringData filler8 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 24, FILLER).init("    To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 34);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(10);
	private FixedLengthStringData filler9 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Premiums:");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(75);
	private FixedLengthStringData filler10 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine005, 0, FILLER).init("   Calculation method:");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 24);
	private FixedLengthStringData filler11 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine005, 28, FILLER).init("   J/Life method:");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 48);
	private FixedLengthStringData filler12 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine005, 52, FILLER).init("    J/Life Allowed:");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 74);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(75);
	private FixedLengthStringData filler13 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine006, 0, FILLER).init("   Guarantee period:");
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 25).setPattern("ZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine006, 28, FILLER).init("   Re-calc. freq.");
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 48).setPattern("ZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 50, FILLER).init(SPACES);
	private FixedLengthStringData filler16 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine006, 56, FILLER).init("Single Payment:");
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 74);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(75);
	private FixedLengthStringData filler17 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine007, 0, FILLER).init("   Stamp Duty calc:");
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 24);
	private FixedLengthStringData filler18 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine007, 28, FILLER).init("   Stamp Duty pay:");
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 48);
	private FixedLengthStringData filler19 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine007, 52, FILLER).init("    Reducing Term:");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 74);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(75);
	private FixedLengthStringData filler20 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine008, 0, FILLER).init("   Benefit billing:");
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 24);
	private FixedLengthStringData filler21 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine008, 28, FILLER).init("   Non-forfeiture:");
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 48);
	private FixedLengthStringData filler22 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine008, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 74);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(78);
	private FixedLengthStringData filler23 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine009, 0, FILLER).init("   Anniversary Proc:");
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 24);
	private FixedLengthStringData filler24 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine009, 28, FILLER).init("   Part Surrender:");
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 48);
	private FixedLengthStringData filler25 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine009, 52, FILLER).init("    Freq Alt Basis");
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 74);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(60);
	private FixedLengthStringData filler26 = new FixedLengthStringData(60).isAPartOf(wsaaPrtLine010, 0, FILLER).init(" Commissions:              Regular     Single      Top-up");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(59);
	private FixedLengthStringData filler27 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine011, 0, FILLER).init("   Basic Calc Method:");
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 31);
	private FixedLengthStringData filler28 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 43);
	private FixedLengthStringData filler29 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 55);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(59);
	private FixedLengthStringData filler30 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine012, 0, FILLER).init("   Basic Pay Method:");
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 31);
	private FixedLengthStringData filler31 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 43);
	private FixedLengthStringData filler32 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 55);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(59);
	private FixedLengthStringData filler33 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine013, 0, FILLER).init("   OR Calc Method:");
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 31);
	private FixedLengthStringData filler34 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine013, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 43);
	private FixedLengthStringData filler35 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine013, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 55);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(59);
	private FixedLengthStringData filler36 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine014, 0, FILLER).init("   OR Pay  Method:");
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 31);
	private FixedLengthStringData filler37 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 43);
	private FixedLengthStringData filler38 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 55);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(35);
	private FixedLengthStringData filler39 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine015, 0, FILLER).init("   Servicing Pay Method:");
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 31);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(77);
	private FixedLengthStringData filler40 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine016, 0, FILLER).init("   Renewal Pay Method:");
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 31);
	private FixedLengthStringData filler41 = new FixedLengthStringData(41).isAPartOf(wsaaPrtLine016, 35, FILLER).init("  Commission paid on Basic Premium Only:");
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 76);

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(75);
	private FixedLengthStringData filler42 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine017, 0, FILLER).init(" Default follow-ups:");
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine017, 24);
	private FixedLengthStringData filler43 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine017, 28, FILLER).init("   Schedule Code:");
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine017, 48);
	private FixedLengthStringData filler44 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine017, 60, FILLER).init("  Prem Code :");
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 74);

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(78);
	private FixedLengthStringData filler45 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine018, 0, FILLER).init(" Statutory fund:");
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 27);
	private FixedLengthStringData filler46 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine018, 28, FILLER).init("   Section:");
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine018, 48);
	private FixedLengthStringData filler47 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine018, 50, FILLER).init(SPACES);
	private FixedLengthStringData filler48 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine018, 56, FILLER).init("Sub-section:");
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine018, 74);

	private FixedLengthStringData wsaaPrtLine019 = new FixedLengthStringData(58);
	private FixedLengthStringData filler49 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine019, 0, FILLER).init(" Reporting codes:");
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine019, 25);
	private FixedLengthStringData filler50 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine019, 28, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine019, 31);
	private FixedLengthStringData filler51 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine019, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine019, 37);
	private FixedLengthStringData filler52 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine019, 40, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine019, 43);
	private FixedLengthStringData filler53 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine019, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine019, 49);
	private FixedLengthStringData filler54 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine019, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine019, 55);

	private FixedLengthStringData wsaaPrtLine020 = new FixedLengthStringData(78);
	private FixedLengthStringData filler55 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine020, 0, FILLER).init(" Maturity calc:");
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine020, 24);
	private FixedLengthStringData filler56 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine020, 28, FILLER).init("   Loan Method:");
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine020, 48);
	private FixedLengthStringData filler57 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine020, 52, FILLER).init("    Benefit Sch Calc:");
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine020, 74);

	private FixedLengthStringData wsaaPrtLine021 = new FixedLengthStringData(78);
	private FixedLengthStringData filler58 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine021, 0, FILLER).init(" Surrender calc:");
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine021, 24);
	private FixedLengthStringData filler59 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine021, 28, FILLER).init("   Death calc:");
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine021, 48);
	private FixedLengthStringData filler60 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine021, 52, FILLER).init("    Paid-up calc:");
	private FixedLengthStringData fieldNo054 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine021, 74);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5687rec t5687rec = new T5687rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5687pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5687rec.t5687Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5687rec.premmeth);
		fieldNo008.set(t5687rec.jlPremMeth);
		fieldNo009.set(t5687rec.jlifePresent);
		fieldNo010.set(t5687rec.premGuarPeriod);
		fieldNo011.set(t5687rec.rtrnwfreq);
		fieldNo012.set(t5687rec.singlePremInd);
		fieldNo013.set(t5687rec.stampDutyMeth);
		fieldNo014.set(t5687rec.sdtyPayMeth);
		fieldNo015.set(t5687rec.zsredtrm);
		fieldNo016.set(t5687rec.bbmeth);
		fieldNo017.set(t5687rec.nonForfeitMethod);
		fieldNo018.set(t5687rec.riind);
		fieldNo019.set(t5687rec.anniversaryMethod);
		fieldNo020.set(t5687rec.partsurr);
		fieldNo022.set(t5687rec.basicCommMeth);
		fieldNo023.set(t5687rec.basscmth);
		fieldNo024.set(t5687rec.bastcmth);
		fieldNo025.set(t5687rec.bascpy);
		fieldNo026.set(t5687rec.basscpy);
		fieldNo027.set(t5687rec.bastcpy);
		fieldNo034.set(t5687rec.srvcpy);
		fieldNo035.set(t5687rec.rnwcpy);
		fieldNo037.set(t5687rec.defFupMeth);
		fieldNo038.set(t5687rec.schedCode);
		fieldNo039.set(t5687rec.zszprmcd);
		fieldNo040.set(t5687rec.statFund);
		fieldNo041.set(t5687rec.statSect);
		fieldNo042.set(t5687rec.statSubSect);
		fieldNo043.set(t5687rec.reptcd01);
		fieldNo044.set(t5687rec.reptcd02);
		fieldNo045.set(t5687rec.reptcd03);
		fieldNo046.set(t5687rec.reptcd04);
		fieldNo047.set(t5687rec.reptcd05);
		fieldNo048.set(t5687rec.reptcd06);
		fieldNo049.set(t5687rec.maturityCalcMeth);
		fieldNo052.set(t5687rec.svMethod);
		fieldNo053.set(t5687rec.dcmeth);
		fieldNo054.set(t5687rec.pumeth);
		fieldNo021.set(t5687rec.xfreqAltBasis);
		fieldNo050.set(t5687rec.loanmeth);
		fieldNo028.set(t5687rec.zrorcmrg);
		fieldNo031.set(t5687rec.zrorpmrg);
		fieldNo029.set(t5687rec.zrorcmsp);
		fieldNo032.set(t5687rec.zrorpmsp);
		fieldNo030.set(t5687rec.zrorcmtu);
		fieldNo033.set(t5687rec.zrorpmtu);
		fieldNo036.set(t5687rec.zrrcombas);
		fieldNo051.set(t5687rec.zsbsmeth);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine019);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine020);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine021);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
