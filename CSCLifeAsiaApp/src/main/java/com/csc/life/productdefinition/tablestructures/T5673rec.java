package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:48
 * Description:
 * Copybook name: T5673REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5673rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5673Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData creqs = new FixedLengthStringData(8).isAPartOf(t5673Rec, 0);
  	public FixedLengthStringData[] creq = FLSArrayPartOfStructure(8, 1, creqs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(creqs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData creq01 = new FixedLengthStringData(1).isAPartOf(filler, 0);
  	public FixedLengthStringData creq02 = new FixedLengthStringData(1).isAPartOf(filler, 1);
  	public FixedLengthStringData creq03 = new FixedLengthStringData(1).isAPartOf(filler, 2);
  	public FixedLengthStringData creq04 = new FixedLengthStringData(1).isAPartOf(filler, 3);
  	public FixedLengthStringData creq05 = new FixedLengthStringData(1).isAPartOf(filler, 4);
  	public FixedLengthStringData creq06 = new FixedLengthStringData(1).isAPartOf(filler, 5);
  	public FixedLengthStringData creq07 = new FixedLengthStringData(1).isAPartOf(filler, 6);
  	public FixedLengthStringData creq08 = new FixedLengthStringData(1).isAPartOf(filler, 7);
  	public FixedLengthStringData ctables = new FixedLengthStringData(32).isAPartOf(t5673Rec, 8);
  	public FixedLengthStringData[] ctable = FLSArrayPartOfStructure(8, 4, ctables, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(32).isAPartOf(ctables, 0, FILLER_REDEFINE);
  	public FixedLengthStringData ctable01 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
  	public FixedLengthStringData ctable02 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
  	public FixedLengthStringData ctable03 = new FixedLengthStringData(4).isAPartOf(filler1, 8);
  	public FixedLengthStringData ctable04 = new FixedLengthStringData(4).isAPartOf(filler1, 12);
  	public FixedLengthStringData ctable05 = new FixedLengthStringData(4).isAPartOf(filler1, 16);
  	public FixedLengthStringData ctable06 = new FixedLengthStringData(4).isAPartOf(filler1, 20);
  	public FixedLengthStringData ctable07 = new FixedLengthStringData(4).isAPartOf(filler1, 24);
  	public FixedLengthStringData ctable08 = new FixedLengthStringData(4).isAPartOf(filler1, 28);
  	public FixedLengthStringData ctmaxcovs = new FixedLengthStringData(16).isAPartOf(t5673Rec, 40);
  	public ZonedDecimalData[] ctmaxcov = ZDArrayPartOfStructure(8, 2, 0, ctmaxcovs, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(16).isAPartOf(ctmaxcovs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData ctmaxcov01 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData ctmaxcov02 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 2);
  	public ZonedDecimalData ctmaxcov03 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4);
  	public ZonedDecimalData ctmaxcov04 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 6);
  	public ZonedDecimalData ctmaxcov05 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 8);
  	public ZonedDecimalData ctmaxcov06 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 10);
  	public ZonedDecimalData ctmaxcov07 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 12);
  	public ZonedDecimalData ctmaxcov08 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 14);
  	public FixedLengthStringData gitem = new FixedLengthStringData(8).isAPartOf(t5673Rec, 56);
  	public FixedLengthStringData rreqs = new FixedLengthStringData(48).isAPartOf(t5673Rec, 64);
  	public FixedLengthStringData[] rreq = FLSArrayPartOfStructure(48, 1, rreqs, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(48).isAPartOf(rreqs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData rreq01 = new FixedLengthStringData(1).isAPartOf(filler3, 0);
  	public FixedLengthStringData rreq02 = new FixedLengthStringData(1).isAPartOf(filler3, 1);
  	public FixedLengthStringData rreq03 = new FixedLengthStringData(1).isAPartOf(filler3, 2);
  	public FixedLengthStringData rreq04 = new FixedLengthStringData(1).isAPartOf(filler3, 3);
  	public FixedLengthStringData rreq05 = new FixedLengthStringData(1).isAPartOf(filler3, 4);
  	public FixedLengthStringData rreq06 = new FixedLengthStringData(1).isAPartOf(filler3, 5);
  	public FixedLengthStringData rreq07 = new FixedLengthStringData(1).isAPartOf(filler3, 6);
  	public FixedLengthStringData rreq08 = new FixedLengthStringData(1).isAPartOf(filler3, 7);
  	public FixedLengthStringData rreq09 = new FixedLengthStringData(1).isAPartOf(filler3, 8);
  	public FixedLengthStringData rreq10 = new FixedLengthStringData(1).isAPartOf(filler3, 9);
  	public FixedLengthStringData rreq11 = new FixedLengthStringData(1).isAPartOf(filler3, 10);
  	public FixedLengthStringData rreq12 = new FixedLengthStringData(1).isAPartOf(filler3, 11);
  	public FixedLengthStringData rreq13 = new FixedLengthStringData(1).isAPartOf(filler3, 12);
  	public FixedLengthStringData rreq14 = new FixedLengthStringData(1).isAPartOf(filler3, 13);
  	public FixedLengthStringData rreq15 = new FixedLengthStringData(1).isAPartOf(filler3, 14);
  	public FixedLengthStringData rreq16 = new FixedLengthStringData(1).isAPartOf(filler3, 15);
  	public FixedLengthStringData rreq17 = new FixedLengthStringData(1).isAPartOf(filler3, 16);
  	public FixedLengthStringData rreq18 = new FixedLengthStringData(1).isAPartOf(filler3, 17);
  	public FixedLengthStringData rreq19 = new FixedLengthStringData(1).isAPartOf(filler3, 18);
  	public FixedLengthStringData rreq20 = new FixedLengthStringData(1).isAPartOf(filler3, 19);
  	public FixedLengthStringData rreq21 = new FixedLengthStringData(1).isAPartOf(filler3, 20);
  	public FixedLengthStringData rreq22 = new FixedLengthStringData(1).isAPartOf(filler3, 21);
  	public FixedLengthStringData rreq23 = new FixedLengthStringData(1).isAPartOf(filler3, 22);
  	public FixedLengthStringData rreq24 = new FixedLengthStringData(1).isAPartOf(filler3, 23);
  	public FixedLengthStringData rreq25 = new FixedLengthStringData(1).isAPartOf(filler3, 24);
  	public FixedLengthStringData rreq26 = new FixedLengthStringData(1).isAPartOf(filler3, 25);
  	public FixedLengthStringData rreq27 = new FixedLengthStringData(1).isAPartOf(filler3, 26);
  	public FixedLengthStringData rreq28 = new FixedLengthStringData(1).isAPartOf(filler3, 27);
  	public FixedLengthStringData rreq29 = new FixedLengthStringData(1).isAPartOf(filler3, 28);
  	public FixedLengthStringData rreq30 = new FixedLengthStringData(1).isAPartOf(filler3, 29);
  	public FixedLengthStringData rreq31 = new FixedLengthStringData(1).isAPartOf(filler3, 30);
  	public FixedLengthStringData rreq32 = new FixedLengthStringData(1).isAPartOf(filler3, 31);
  	public FixedLengthStringData rreq33 = new FixedLengthStringData(1).isAPartOf(filler3, 32);
  	public FixedLengthStringData rreq34 = new FixedLengthStringData(1).isAPartOf(filler3, 33);
  	public FixedLengthStringData rreq35 = new FixedLengthStringData(1).isAPartOf(filler3, 34);
  	public FixedLengthStringData rreq36 = new FixedLengthStringData(1).isAPartOf(filler3, 35);
  	public FixedLengthStringData rreq37 = new FixedLengthStringData(1).isAPartOf(filler3, 36);
  	public FixedLengthStringData rreq38 = new FixedLengthStringData(1).isAPartOf(filler3, 37);
  	public FixedLengthStringData rreq39 = new FixedLengthStringData(1).isAPartOf(filler3, 38);
  	public FixedLengthStringData rreq40 = new FixedLengthStringData(1).isAPartOf(filler3, 39);
  	public FixedLengthStringData rreq41 = new FixedLengthStringData(1).isAPartOf(filler3, 40);
  	public FixedLengthStringData rreq42 = new FixedLengthStringData(1).isAPartOf(filler3, 41);
  	public FixedLengthStringData rreq43 = new FixedLengthStringData(1).isAPartOf(filler3, 42);
  	public FixedLengthStringData rreq44 = new FixedLengthStringData(1).isAPartOf(filler3, 43);
  	public FixedLengthStringData rreq45 = new FixedLengthStringData(1).isAPartOf(filler3, 44);
  	public FixedLengthStringData rreq46 = new FixedLengthStringData(1).isAPartOf(filler3, 45);
  	public FixedLengthStringData rreq47 = new FixedLengthStringData(1).isAPartOf(filler3, 46);
  	public FixedLengthStringData rreq48 = new FixedLengthStringData(1).isAPartOf(filler3, 47);
  	public FixedLengthStringData rtables = new FixedLengthStringData(192).isAPartOf(t5673Rec, 112);
  	public FixedLengthStringData[] rtable = FLSArrayPartOfStructure(48, 4, rtables, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(192).isAPartOf(rtables, 0, FILLER_REDEFINE);
  	public FixedLengthStringData rtable01 = new FixedLengthStringData(4).isAPartOf(filler4, 0);
  	public FixedLengthStringData rtable02 = new FixedLengthStringData(4).isAPartOf(filler4, 4);
  	public FixedLengthStringData rtable03 = new FixedLengthStringData(4).isAPartOf(filler4, 8);
  	public FixedLengthStringData rtable04 = new FixedLengthStringData(4).isAPartOf(filler4, 12);
  	public FixedLengthStringData rtable05 = new FixedLengthStringData(4).isAPartOf(filler4, 16);
  	public FixedLengthStringData rtable06 = new FixedLengthStringData(4).isAPartOf(filler4, 20);
  	public FixedLengthStringData rtable07 = new FixedLengthStringData(4).isAPartOf(filler4, 24);
  	public FixedLengthStringData rtable08 = new FixedLengthStringData(4).isAPartOf(filler4, 28);
  	public FixedLengthStringData rtable09 = new FixedLengthStringData(4).isAPartOf(filler4, 32);
  	public FixedLengthStringData rtable10 = new FixedLengthStringData(4).isAPartOf(filler4, 36);
  	public FixedLengthStringData rtable11 = new FixedLengthStringData(4).isAPartOf(filler4, 40);
  	public FixedLengthStringData rtable12 = new FixedLengthStringData(4).isAPartOf(filler4, 44);
  	public FixedLengthStringData rtable13 = new FixedLengthStringData(4).isAPartOf(filler4, 48);
  	public FixedLengthStringData rtable14 = new FixedLengthStringData(4).isAPartOf(filler4, 52);
  	public FixedLengthStringData rtable15 = new FixedLengthStringData(4).isAPartOf(filler4, 56);
  	public FixedLengthStringData rtable16 = new FixedLengthStringData(4).isAPartOf(filler4, 60);
  	public FixedLengthStringData rtable17 = new FixedLengthStringData(4).isAPartOf(filler4, 64);
  	public FixedLengthStringData rtable18 = new FixedLengthStringData(4).isAPartOf(filler4, 68);
  	public FixedLengthStringData rtable19 = new FixedLengthStringData(4).isAPartOf(filler4, 72);
  	public FixedLengthStringData rtable20 = new FixedLengthStringData(4).isAPartOf(filler4, 76);
  	public FixedLengthStringData rtable21 = new FixedLengthStringData(4).isAPartOf(filler4, 80);
  	public FixedLengthStringData rtable22 = new FixedLengthStringData(4).isAPartOf(filler4, 84);
  	public FixedLengthStringData rtable23 = new FixedLengthStringData(4).isAPartOf(filler4, 88);
  	public FixedLengthStringData rtable24 = new FixedLengthStringData(4).isAPartOf(filler4, 92);
  	public FixedLengthStringData rtable25 = new FixedLengthStringData(4).isAPartOf(filler4, 96);
  	public FixedLengthStringData rtable26 = new FixedLengthStringData(4).isAPartOf(filler4, 100);
  	public FixedLengthStringData rtable27 = new FixedLengthStringData(4).isAPartOf(filler4, 104);
  	public FixedLengthStringData rtable28 = new FixedLengthStringData(4).isAPartOf(filler4, 108);
  	public FixedLengthStringData rtable29 = new FixedLengthStringData(4).isAPartOf(filler4, 112);
  	public FixedLengthStringData rtable30 = new FixedLengthStringData(4).isAPartOf(filler4, 116);
  	public FixedLengthStringData rtable31 = new FixedLengthStringData(4).isAPartOf(filler4, 120);
  	public FixedLengthStringData rtable32 = new FixedLengthStringData(4).isAPartOf(filler4, 124);
  	public FixedLengthStringData rtable33 = new FixedLengthStringData(4).isAPartOf(filler4, 128);
  	public FixedLengthStringData rtable34 = new FixedLengthStringData(4).isAPartOf(filler4, 132);
  	public FixedLengthStringData rtable35 = new FixedLengthStringData(4).isAPartOf(filler4, 136);
  	public FixedLengthStringData rtable36 = new FixedLengthStringData(4).isAPartOf(filler4, 140);
  	public FixedLengthStringData rtable37 = new FixedLengthStringData(4).isAPartOf(filler4, 144);
  	public FixedLengthStringData rtable38 = new FixedLengthStringData(4).isAPartOf(filler4, 148);
  	public FixedLengthStringData rtable39 = new FixedLengthStringData(4).isAPartOf(filler4, 152);
  	public FixedLengthStringData rtable40 = new FixedLengthStringData(4).isAPartOf(filler4, 156);
  	public FixedLengthStringData rtable41 = new FixedLengthStringData(4).isAPartOf(filler4, 160);
  	public FixedLengthStringData rtable42 = new FixedLengthStringData(4).isAPartOf(filler4, 164);
  	public FixedLengthStringData rtable43 = new FixedLengthStringData(4).isAPartOf(filler4, 168);
  	public FixedLengthStringData rtable44 = new FixedLengthStringData(4).isAPartOf(filler4, 172);
  	public FixedLengthStringData rtable45 = new FixedLengthStringData(4).isAPartOf(filler4, 176);
  	public FixedLengthStringData rtable46 = new FixedLengthStringData(4).isAPartOf(filler4, 180);
  	public FixedLengthStringData rtable47 = new FixedLengthStringData(4).isAPartOf(filler4, 184);
  	public FixedLengthStringData rtable48 = new FixedLengthStringData(4).isAPartOf(filler4, 188);
  	public FixedLengthStringData zrlifinds = new FixedLengthStringData(8).isAPartOf(t5673Rec, 304);
  	public FixedLengthStringData[] zrlifind = FLSArrayPartOfStructure(8, 1, zrlifinds, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(zrlifinds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData zrlifind01 = new FixedLengthStringData(1).isAPartOf(filler5, 0);
  	public FixedLengthStringData zrlifind02 = new FixedLengthStringData(1).isAPartOf(filler5, 1);
  	public FixedLengthStringData zrlifind03 = new FixedLengthStringData(1).isAPartOf(filler5, 2);
  	public FixedLengthStringData zrlifind04 = new FixedLengthStringData(1).isAPartOf(filler5, 3);
  	public FixedLengthStringData zrlifind05 = new FixedLengthStringData(1).isAPartOf(filler5, 4);
  	public FixedLengthStringData zrlifind06 = new FixedLengthStringData(1).isAPartOf(filler5, 5);
  	public FixedLengthStringData zrlifind07 = new FixedLengthStringData(1).isAPartOf(filler5, 6);
  	public FixedLengthStringData zrlifind08 = new FixedLengthStringData(1).isAPartOf(filler5, 7);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(188).isAPartOf(t5673Rec, 312, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5673Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5673Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}