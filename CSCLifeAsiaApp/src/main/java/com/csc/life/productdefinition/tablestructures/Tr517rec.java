package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:19:36
 * Description:
 * Copybook name: TR517REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr517rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr517Rec = new FixedLengthStringData(502);
  	public FixedLengthStringData ctables = new FixedLengthStringData(200).isAPartOf(tr517Rec, 0);
  	public FixedLengthStringData[] ctable = FLSArrayPartOfStructure(50, 4, ctables, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(200).isAPartOf(ctables, 0, FILLER_REDEFINE);
  	public FixedLengthStringData ctable01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData ctable02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData ctable03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData ctable04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData ctable05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData ctable06 = new FixedLengthStringData(4).isAPartOf(filler, 20);
  	public FixedLengthStringData ctable07 = new FixedLengthStringData(4).isAPartOf(filler, 24);
  	public FixedLengthStringData ctable08 = new FixedLengthStringData(4).isAPartOf(filler, 28);
  	public FixedLengthStringData ctable09 = new FixedLengthStringData(4).isAPartOf(filler, 32);
  	public FixedLengthStringData ctable10 = new FixedLengthStringData(4).isAPartOf(filler, 36);
  	public FixedLengthStringData ctable11 = new FixedLengthStringData(4).isAPartOf(filler, 40);
  	public FixedLengthStringData ctable12 = new FixedLengthStringData(4).isAPartOf(filler, 44);
  	public FixedLengthStringData ctable13 = new FixedLengthStringData(4).isAPartOf(filler, 48);
  	public FixedLengthStringData ctable14 = new FixedLengthStringData(4).isAPartOf(filler, 52);
  	public FixedLengthStringData ctable15 = new FixedLengthStringData(4).isAPartOf(filler, 56);
  	public FixedLengthStringData ctable16 = new FixedLengthStringData(4).isAPartOf(filler, 60);
  	public FixedLengthStringData ctable17 = new FixedLengthStringData(4).isAPartOf(filler, 64);
  	public FixedLengthStringData ctable18 = new FixedLengthStringData(4).isAPartOf(filler, 68);
  	public FixedLengthStringData ctable19 = new FixedLengthStringData(4).isAPartOf(filler, 72);
  	public FixedLengthStringData ctable20 = new FixedLengthStringData(4).isAPartOf(filler, 76);
  	public FixedLengthStringData ctable21 = new FixedLengthStringData(4).isAPartOf(filler, 80);
  	public FixedLengthStringData ctable22 = new FixedLengthStringData(4).isAPartOf(filler, 84);
  	public FixedLengthStringData ctable23 = new FixedLengthStringData(4).isAPartOf(filler, 88);
  	public FixedLengthStringData ctable24 = new FixedLengthStringData(4).isAPartOf(filler, 92);
  	public FixedLengthStringData ctable25 = new FixedLengthStringData(4).isAPartOf(filler, 96);
  	public FixedLengthStringData ctable26 = new FixedLengthStringData(4).isAPartOf(filler, 100);
  	public FixedLengthStringData ctable27 = new FixedLengthStringData(4).isAPartOf(filler, 104);
  	public FixedLengthStringData ctable28 = new FixedLengthStringData(4).isAPartOf(filler, 108);
  	public FixedLengthStringData ctable29 = new FixedLengthStringData(4).isAPartOf(filler, 112);
  	public FixedLengthStringData ctable30 = new FixedLengthStringData(4).isAPartOf(filler, 116);
  	public FixedLengthStringData ctable31 = new FixedLengthStringData(4).isAPartOf(filler, 120);
  	public FixedLengthStringData ctable32 = new FixedLengthStringData(4).isAPartOf(filler, 124);
  	public FixedLengthStringData ctable33 = new FixedLengthStringData(4).isAPartOf(filler, 128);
  	public FixedLengthStringData ctable34 = new FixedLengthStringData(4).isAPartOf(filler, 132);
  	public FixedLengthStringData ctable35 = new FixedLengthStringData(4).isAPartOf(filler, 136);
  	public FixedLengthStringData ctable36 = new FixedLengthStringData(4).isAPartOf(filler, 140);
  	public FixedLengthStringData ctable37 = new FixedLengthStringData(4).isAPartOf(filler, 144);
  	public FixedLengthStringData ctable38 = new FixedLengthStringData(4).isAPartOf(filler, 148);
  	public FixedLengthStringData ctable39 = new FixedLengthStringData(4).isAPartOf(filler, 152);
  	public FixedLengthStringData ctable40 = new FixedLengthStringData(4).isAPartOf(filler, 156);
  	public FixedLengthStringData ctable41 = new FixedLengthStringData(4).isAPartOf(filler, 160);
  	public FixedLengthStringData ctable42 = new FixedLengthStringData(4).isAPartOf(filler, 164);
  	public FixedLengthStringData ctable43 = new FixedLengthStringData(4).isAPartOf(filler, 168);
  	public FixedLengthStringData ctable44 = new FixedLengthStringData(4).isAPartOf(filler, 172);
  	public FixedLengthStringData ctable45 = new FixedLengthStringData(4).isAPartOf(filler, 176);
  	public FixedLengthStringData ctable46 = new FixedLengthStringData(4).isAPartOf(filler, 180);
  	public FixedLengthStringData ctable47 = new FixedLengthStringData(4).isAPartOf(filler, 184);
  	public FixedLengthStringData ctable48 = new FixedLengthStringData(4).isAPartOf(filler, 188);
  	public FixedLengthStringData ctable49 = new FixedLengthStringData(4).isAPartOf(filler, 192);
  	public FixedLengthStringData ctable50 = new FixedLengthStringData(4).isAPartOf(filler, 196);
  	public FixedLengthStringData zrwvflgs = new FixedLengthStringData(5).isAPartOf(tr517Rec, 200);
  	public FixedLengthStringData[] zrwvflg = FLSArrayPartOfStructure(5, 1, zrwvflgs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(5).isAPartOf(zrwvflgs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData zrwvflg01 = new FixedLengthStringData(1).isAPartOf(filler1, 0);
  	public FixedLengthStringData zrwvflg02 = new FixedLengthStringData(1).isAPartOf(filler1, 1);
  	public FixedLengthStringData zrwvflg03 = new FixedLengthStringData(1).isAPartOf(filler1, 2);
  	public FixedLengthStringData zrwvflg04 = new FixedLengthStringData(1).isAPartOf(filler1, 3);
  	public FixedLengthStringData zrwvflg05 = new FixedLengthStringData(1).isAPartOf(filler1, 4);
  	public FixedLengthStringData contitem = new FixedLengthStringData(8).isAPartOf(tr517Rec, 205);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(289).isAPartOf(tr517Rec, 213, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr517Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr517Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}