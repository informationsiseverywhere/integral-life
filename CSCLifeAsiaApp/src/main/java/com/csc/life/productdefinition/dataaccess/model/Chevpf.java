package com.csc.life.productdefinition.dataaccess.model;

import java.math.BigDecimal;


public class Chevpf {
	public String chdrcoy;
	public String chdrnum;
	public int effdate;
	public String cnttype;
	public int rcdate;
	public String ownnam;
	public String payorname;
	public int hoissdte;
	public int hissdte;
	public String agentno;
	public String agtype;
	public String billfreq;
	public String statcode;
	public String pstatcode;
	public int ptdate;
	public int btdate;
	public BigDecimal totlprem;
	public BigDecimal instprem;
	public BigDecimal cshmt;
	public BigDecimal loanvalue;
	public BigDecimal surrchg;
	public BigDecimal surrval;
	public int currfrom;
	public String validflag;
	public String userProfile;
	public String jobName;
	public String datime;
    public String getChdrcoy() {
        return chdrcoy;
    }
    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }
    public String getChdrnum() {
        return chdrnum;
    }
    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }
    public int getEffdate() {
        return effdate;
    }
    public void setEffdate(int effdate) {
        this.effdate = effdate;
    }
    public String getCnttype() {
        return cnttype;
    }
    public void setCnttype(String cnttype) {
        this.cnttype = cnttype;
    }
    public int getRcdate() {
        return rcdate;
    }
    public void setRcdate(int rcdate) {
        this.rcdate = rcdate;
    }
    public String getOwnnam() {
        return ownnam;
    }
    public void setOwnnam(String ownnam) {
        this.ownnam = ownnam;
    }
    public String getPayorname() {
        return payorname;
    }
    public void setPayorname(String payorname) {
        this.payorname = payorname;
    }
    public int getHoissdte() {
        return hoissdte;
    }
    public void setHoissdte(int hoissdte) {
        this.hoissdte = hoissdte;
    }
    public int getHissdte() {
        return hissdte;
    }
    public void setHissdte(int hissdte) {
        this.hissdte = hissdte;
    }
    public String getAgentno() {
        return agentno;
    }
    public void setAgentno(String agentno) {
        this.agentno = agentno;
    }
    public String getAgtype() {
        return agtype;
    }
    public void setAgtype(String agtype) {
        this.agtype = agtype;
    }
    public String getBillfreq() {
        return billfreq;
    }
    public void setBillfreq(String billfreq) {
        this.billfreq = billfreq;
    }
    public String getStatcode() {
        return statcode;
    }
    public void setStatcode(String statcode) {
        this.statcode = statcode;
    }
    public String getPstatcode() {
        return pstatcode;
    }
    public void setPstatcode(String pstatcode) {
        this.pstatcode = pstatcode;
    }
    public int getPtdate() {
        return ptdate;
    }
    public void setPtdate(int ptdate) {
        this.ptdate = ptdate;
    }
    public int getBtdate() {
        return btdate;
    }
    public void setBtdate(int btdate) {
        this.btdate = btdate;
    }
    public BigDecimal getTotlprem() {
        return totlprem;
    }
    public void setTotlprem(BigDecimal totlprem) {
        this.totlprem = totlprem;
    }
    public BigDecimal getInstprem() {
        return instprem;
    }
    public void setInstprem(BigDecimal instprem) {
        this.instprem = instprem;
    }
    public BigDecimal getCshmt() {
        return cshmt;
    }
    public void setCshmt(BigDecimal cshmt) {
        this.cshmt = cshmt;
    }
    public BigDecimal getLoanvalue() {
        return loanvalue;
    }
    public void setLoanvalue(BigDecimal loanvalue) {
        this.loanvalue = loanvalue;
    }
    public BigDecimal getSurrchg() {
        return surrchg;
    }
    public void setSurrchg(BigDecimal surrchg) {
        this.surrchg = surrchg;
    }
    public BigDecimal getSurrval() {
        return surrval;
    }
    public void setSurrval(BigDecimal surrval) {
        this.surrval = surrval;
    }
    public int getCurrfrom() {
        return currfrom;
    }
    public void setCurrfrom(int currfrom) {
        this.currfrom = currfrom;
    }
    public String getValidflag() {
        return validflag;
    }
    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }
    public String getUserProfile() {
        return userProfile;
    }
    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }
    public String getJobName() {
        return jobName;
    }
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
    public String getDatime() {
        return datime;
    }
    public void setDatime(String datime) {
        this.datime = datime;
    }
}