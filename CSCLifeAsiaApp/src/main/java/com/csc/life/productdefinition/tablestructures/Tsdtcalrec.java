package com.csc.life.productdefinition.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:40
 * Description:
 * Copybook name: TSDTCALREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tsdtcalrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tsdtcalRec = new FixedLengthStringData(169);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(tsdtcalRec, 0);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(tsdtcalRec, 4);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(tsdtcalRec, 5);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(tsdtcalRec, 13);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(tsdtcalRec, 16).setUnsigned();
  	public FixedLengthStringData trancde = new FixedLengthStringData(4).isAPartOf(tsdtcalRec, 24);
  	public ZonedDecimalData amount = new ZonedDecimalData(17, 2).isAPartOf(tsdtcalRec, 28);
  	public ZonedDecimalData sdutyRate = new ZonedDecimalData(7, 2).isAPartOf(tsdtcalRec, 45);
  	public ZonedDecimalData sdutyAmount = new ZonedDecimalData(17, 2).isAPartOf(tsdtcalRec, 52);
  	public FixedLengthStringData filler = new FixedLengthStringData(100).isAPartOf(tsdtcalRec, 69, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tsdtcalRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tsdtcalRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}