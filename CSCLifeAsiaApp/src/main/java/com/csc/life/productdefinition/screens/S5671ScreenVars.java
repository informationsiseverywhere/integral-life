package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5671
 * @version 1.0 generated on 30/08/09 06:48
 * @author Quipoz
 */
public class S5671ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(484);
	public FixedLengthStringData dataFields = new FixedLengthStringData(164).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData edtitms = new FixedLengthStringData(20).isAPartOf(dataFields, 1);
	public FixedLengthStringData[] edtitm = FLSArrayPartOfStructure(4, 5, edtitms, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(edtitms, 0, FILLER_REDEFINE);
	public FixedLengthStringData edtitm01 = DD.edtitm.copy().isAPartOf(filler,0);
	public FixedLengthStringData edtitm02 = DD.edtitm.copy().isAPartOf(filler,5);
	public FixedLengthStringData edtitm03 = DD.edtitm.copy().isAPartOf(filler,10);
	public FixedLengthStringData edtitm04 = DD.edtitm.copy().isAPartOf(filler,15);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,29);
	public FixedLengthStringData pgms = new FixedLengthStringData(20).isAPartOf(dataFields, 59);
	public FixedLengthStringData[] pgm = FLSArrayPartOfStructure(4, 5, pgms, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(pgms, 0, FILLER_REDEFINE);
	public FixedLengthStringData pgm01 = DD.pgm.copy().isAPartOf(filler1,0);
	public FixedLengthStringData pgm02 = DD.pgm.copy().isAPartOf(filler1,5);
	public FixedLengthStringData pgm03 = DD.pgm.copy().isAPartOf(filler1,10);
	public FixedLengthStringData pgm04 = DD.pgm.copy().isAPartOf(filler1,15);
	public FixedLengthStringData subprogs = new FixedLengthStringData(40).isAPartOf(dataFields, 79);
	public FixedLengthStringData[] subprog = FLSArrayPartOfStructure(4, 10, subprogs, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(subprogs, 0, FILLER_REDEFINE);
	public FixedLengthStringData subprog01 = DD.subprog.copy().isAPartOf(filler2,0);
	public FixedLengthStringData subprog02 = DD.subprog.copy().isAPartOf(filler2,10);
	public FixedLengthStringData subprog03 = DD.subprog.copy().isAPartOf(filler2,20);
	public FixedLengthStringData subprog04 = DD.subprog.copy().isAPartOf(filler2,30);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,119);
	public FixedLengthStringData trevsubs = new FixedLengthStringData(40).isAPartOf(dataFields, 124);
	public FixedLengthStringData[] trevsub = FLSArrayPartOfStructure(4, 10, trevsubs, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(trevsubs, 0, FILLER_REDEFINE);
	public FixedLengthStringData trevsub01 = DD.trevsub.copy().isAPartOf(filler3,0);
	public FixedLengthStringData trevsub02 = DD.trevsub.copy().isAPartOf(filler3,10);
	public FixedLengthStringData trevsub03 = DD.trevsub.copy().isAPartOf(filler3,20);
	public FixedLengthStringData trevsub04 = DD.trevsub.copy().isAPartOf(filler3,30);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(80).isAPartOf(dataArea, 164);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData edtitmsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] edtitmErr = FLSArrayPartOfStructure(4, 4, edtitmsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(16).isAPartOf(edtitmsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData edtitm01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData edtitm02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData edtitm03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData edtitm04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData pgmsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData[] pgmErr = FLSArrayPartOfStructure(4, 4, pgmsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(16).isAPartOf(pgmsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pgm01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData pgm02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData pgm03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData pgm04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData subprogsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData[] subprogErr = FLSArrayPartOfStructure(4, 4, subprogsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(16).isAPartOf(subprogsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData subprog01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData subprog02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData subprog03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData subprog04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData trevsubsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData[] trevsubErr = FLSArrayPartOfStructure(4, 4, trevsubsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(16).isAPartOf(trevsubsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData trevsub01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData trevsub02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData trevsub03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData trevsub04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(240).isAPartOf(dataArea, 244);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData edtitmsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] edtitmOut = FLSArrayPartOfStructure(4, 12, edtitmsOut, 0);
	public FixedLengthStringData[][] edtitmO = FLSDArrayPartOfArrayStructure(12, 1, edtitmOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(48).isAPartOf(edtitmsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] edtitm01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] edtitm02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] edtitm03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] edtitm04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData pgmsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 84);
	public FixedLengthStringData[] pgmOut = FLSArrayPartOfStructure(4, 12, pgmsOut, 0);
	public FixedLengthStringData[][] pgmO = FLSDArrayPartOfArrayStructure(12, 1, pgmOut, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(48).isAPartOf(pgmsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pgm01Out = FLSArrayPartOfStructure(12, 1, filler9, 0);
	public FixedLengthStringData[] pgm02Out = FLSArrayPartOfStructure(12, 1, filler9, 12);
	public FixedLengthStringData[] pgm03Out = FLSArrayPartOfStructure(12, 1, filler9, 24);
	public FixedLengthStringData[] pgm04Out = FLSArrayPartOfStructure(12, 1, filler9, 36);
	public FixedLengthStringData subprogsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 132);
	public FixedLengthStringData[] subprogOut = FLSArrayPartOfStructure(4, 12, subprogsOut, 0);
	public FixedLengthStringData[][] subprogO = FLSDArrayPartOfArrayStructure(12, 1, subprogOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(48).isAPartOf(subprogsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] subprog01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] subprog02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] subprog03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] subprog04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData trevsubsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 192);
	public FixedLengthStringData[] trevsubOut = FLSArrayPartOfStructure(4, 12, trevsubsOut, 0);
	public FixedLengthStringData[][] trevsubO = FLSDArrayPartOfArrayStructure(12, 1, trevsubOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(48).isAPartOf(trevsubsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] trevsub01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] trevsub02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] trevsub03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] trevsub04Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5671screenWritten = new LongData(0);
	public LongData S5671protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5671ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, pgm01, pgm02, pgm03, pgm04, subprog01, subprog02, subprog03, subprog04, edtitm01, edtitm02, edtitm03, edtitm04, trevsub01, trevsub02, trevsub03, trevsub04};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, pgm01Out, pgm02Out, pgm03Out, pgm04Out, subprog01Out, subprog02Out, subprog03Out, subprog04Out, edtitm01Out, edtitm02Out, edtitm03Out, edtitm04Out, trevsub01Out, trevsub02Out, trevsub03Out, trevsub04Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, pgm01Err, pgm02Err, pgm03Err, pgm04Err, subprog01Err, subprog02Err, subprog03Err, subprog04Err, edtitm01Err, edtitm02Err, edtitm03Err, edtitm04Err, trevsub01Err, trevsub02Err, trevsub03Err, trevsub04Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5671screen.class;
		protectRecord = S5671protect.class;
	}

}
