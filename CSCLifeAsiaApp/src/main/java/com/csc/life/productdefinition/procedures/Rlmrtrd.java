/*
 * File: Rlmrtrd.java
 * Date: 30 August 2009 2:13:08
 * Author: Quipoz Limited
 *
 * Class transformed from RLMRTRD.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.productdefinition.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.power;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.recordstructures.Rlrdtrmrec;
import com.csc.life.productdefinition.dataaccess.MbnsTableDAM;
import com.csc.life.productdefinition.dataaccess.MrtaTableDAM;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.tablestructures.Th615rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  This subroutine calculates the benefit schedule for Mortgage
*  Reducing Term for Monthly Rest.
*
*  Logic:
*  The Sum Assured at each policy year n is:
*      Reduced SA = SA * ((T - n + c + 1) / T)
*
*      where T is the Risk Term of the policy
*            c is the number of coverages on the policy
*            SA is the original Sum Assured
*
*  Each calculated Reduced SA per year is written to MBNS.
*
*****************************************************************
* </pre>
*/
public class Rlmrtrd extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1);
	protected ZonedDecimalData wsaaRiskTerm = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(2, 0).setUnsigned();
	protected ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	protected ZonedDecimalData wsaaIndex1 = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaSumins1 = new PackedDecimalData(17, 7).setUnsigned();
	protected PackedDecimalData wsaaRdtsum = new PackedDecimalData(15, 0).setUnsigned();
	private PackedDecimalData wsaaJ = new PackedDecimalData(13, 10);
	private PackedDecimalData wsaaVj = new PackedDecimalData(13, 10);
	private PackedDecimalData wsaaP = new PackedDecimalData(18, 10);
	private PackedDecimalData wsaaP1 = new PackedDecimalData(18, 10);
		/* TABLES */
	private String th615 = "TH615";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String mrtarec = "MRTAREC";
	private String mbnsrec = "MBNSREC";
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Benefit Schedule Details*/
	private MbnsTableDAM mbnsIO = new MbnsTableDAM();
		/*Reducing Term Assurance details*/
	private MrtaTableDAM mrtaIO = new MrtaTableDAM();
	protected Rlrdtrmrec rlrdtrmrec = new Rlrdtrmrec();
	private Th615rec th615rec = new Th615rec();
	private Varcom varcom = new Varcom();
	//ILIFE-3713-STARTS
	private boolean mrt1Permission = false;
	private boolean benSchdPermission = false;//ILIFE-7521
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	//ILIFE-3713-ENDS
	private ExternalisedRules er = new ExternalisedRules();
	private String company="";
	private Vpxlextrec vpxlextrec = new Vpxlextrec();
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit090
	}

	public Rlmrtrd() {
		super();
	}

public void mainline(Object... parmArray)
	{
		rlrdtrmrec.rlrdtrmRec = convertAndSetParam(rlrdtrmrec.rlrdtrmRec, parmArray, 0);
		try {
			company=rlrdtrmrec.chdrcoy.toString().trim();
			benSchdPermission = FeaConfg.isFeatureExist(company,"NBPRP093", appVars, "IT");
			if(benSchdPermission){
				vpxlextrec = (Vpxlextrec) parmArray[1];
			}
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
		try {
			para010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void para010()
	{
		initialize(wsaaFlag);
		initialize(wsaaIndex);
		initialize(wsaaIndex1);
		initialize(wsaaSumins);
		initialize(wsaaSumins1);
		initialize(wsaaRdtsum);
		initialize(wsaaJ);
		initialize(wsaaVj);
		initialize(wsaaP);
		initialize(wsaaP1);
		initialize(wsaaTerm);
		initialize(wsaaRiskTerm);
		rlrdtrmrec.statuz.set(varcom.oK);
		mrt1Permission = FeaConfg.isFeatureExist(rlrdtrmrec.chdrcoy.toString(), "NBPRP010", appVars,smtpfxcpy.item.toString());//ILIFE-3713
		readMrta300();
		if (isEQ(wsaaFlag,"Y")) {
			goTo(GotoLabel.exit090);
		}
		if(!mrt1Permission){//ILIFE-3713
		readTh615Table200();
		}
		if (isNE(rlrdtrmrec.statuz,varcom.oK)
		|| isEQ(rlrdtrmrec.statuz,varcom.endp)
		|| isEQ(rlrdtrmrec.statuz,varcom.mrnf)) {
			rlrdtrmrec.statuz.set(varcom.bomb);
			goTo(GotoLabel.exit090);
		}
		if (isEQ(th615rec.premind,"Y")) {
			goTo(GotoLabel.exit090);
		}
		wsaaRiskTerm.set(rlrdtrmrec.riskTerm);
		compute(wsaaTerm, 0).set(sub(rlrdtrmrec.riskTerm,mrtaIO.getCoverc()));
		if (isEQ(mrtaIO.getPrat(),ZERO)) {
			if (isEQ(mrtaIO.getCoverc(),ZERO)) {
				wsaaIndex1.set(1);
			}
			else {
				for (wsaaIndex1.set(1); !(isGT(wsaaIndex1,mrtaIO.getCoverc())); wsaaIndex1.add(1)){
					interimCover700();
				}
			}
			for (wsaaIndex.set(wsaaIndex1); !(isGT(wsaaIndex,wsaaRiskTerm)); wsaaIndex.add(1)){
				calc400();
			}
		}
		else {
			if (isEQ(mrtaIO.getCoverc(),ZERO)) {
				wsaaIndex1.set(1);
			}
			else {
				for (wsaaIndex1.set(1); !(isGT(wsaaIndex1,mrtaIO.getCoverc())); wsaaIndex1.add(1)){
					interimCover700();
				}
			}
			assign500();
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readTh615Table200()
	{
		/*START*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(rlrdtrmrec.chdrcoy);
		itemIO.setItemtabl(th615);
		itemIO.setItemitem(mrtaIO.getMlresind());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)
		|| isNE(itemIO.getStatuz(),varcom.oK)) {
			rlrdtrmrec.statuz.set(itemIO.getStatuz());
		}
		th615rec.th615Rec.set(itemIO.getGenarea());
		/*EXIT*/
	}

protected void readMrta300()
	{
		/*START*/
		mrtaIO.setChdrcoy(rlrdtrmrec.chdrcoy);
		mrtaIO.setChdrnum(rlrdtrmrec.chdrnum);
		mrtaIO.setFormat(mrtarec);
		mrtaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mrtaIO);
		if (isNE(mrtaIO.getStatuz(),varcom.oK)) {
			wsaaFlag.set("Y");
		}
		/*EXIT*/
	}

protected void calc400()
	{
		/*START*/
		compute(wsaaSumins, 3).setRounded(mult(10000,(div((add(add(sub(wsaaTerm,wsaaIndex),mrtaIO.getCoverc()),1)),wsaaTerm))));
		compute(wsaaSumins1, 7).set(div(wsaaSumins,10000));
		compute(wsaaRdtsum, 7).set(mult(rlrdtrmrec.sumins,wsaaSumins1));
		write600();
		/*EXIT*/
	}

protected void assign500()
	{
		/*START*/
		compute(wsaaJ, 10).set(div((div(mrtaIO.getPrat(),100)),12));
		compute(wsaaVj, 10).set(div(1,(add(1,wsaaJ))));
		//ILIFE-7521 starts
		if(!(benSchdPermission && AppVars.getInstance().getAppConfig().isVpmsEnable()  && er.isCallExternal("VPMRLMRTRD")))
		{
			for (wsaaIndex.set(wsaaIndex1); !(isGT(wsaaIndex,wsaaRiskTerm)); wsaaIndex.add(1)){
				calc550();
			}
		}
		else
		{
			callProgram("VPMRLMRTRD",rlrdtrmrec,vpxlextrec);				      
			if (isEQ(rlrdtrmrec.statuz, Varcom.oK)) 
			{
				int index=1;
				for(int i=0; !isGT(i,(mult(wsaaRiskTerm,12)));i++)
				{
					wsaaRdtsum.set(rlrdtrmrec.reducedSumins.get(i));
					mbnsIO.setYrsinf(index);
					index++;
					write600();
				}
			}
		}
		//ILIFE-7521 ends
		
		/*EXIT*/
	}

protected void calc550()
	{
		/*START*/
		initialize(wsaaP);
		initialize(wsaaP1);
		compute(wsaaP, 10).set((add(sub(add((mult(12,wsaaTerm)),(mult(12,mrtaIO.getCoverc()))),(mult(12,wsaaIndex))),12)));
		compute(wsaaP1, 10).set((mult(12,wsaaTerm)));
		compute(wsaaSumins, 11).setRounded(mult(10000,(div((sub(1,power(wsaaVj,wsaaP))),(sub(1,power(wsaaVj,wsaaP1)))))));
		compute(wsaaSumins1, 7).set(div(wsaaSumins,10000));
		compute(wsaaRdtsum, 7).set(mult(rlrdtrmrec.sumins,wsaaSumins1));
		write600();
		/*EXIT*/
	}

protected void write600()
	{
		start600();
	}

protected void start600()
	{
		mbnsIO.setChdrcoy(rlrdtrmrec.chdrcoy);
		mbnsIO.setChdrnum(rlrdtrmrec.chdrnum);
		mbnsIO.setLife(rlrdtrmrec.life);
		mbnsIO.setCoverage(rlrdtrmrec.coverage);
		mbnsIO.setRider(rlrdtrmrec.rider);
		if(!(benSchdPermission && AppVars.getInstance().getAppConfig().isVpmsEnable()  && er.isCallExternal("VPMRLMRTRD")))//ILIFE-7521
			mbnsIO.setYrsinf(wsaaIndex);
		mbnsIO.setFormat(mbnsrec);
		mbnsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(),varcom.oK)
		|| isEQ(mbnsIO.getStatuz(),varcom.mrnf)) {
			mbnsIO.setFunction(varcom.writr);
		}
		else {
			mbnsIO.setFunction(varcom.rewrt);
		}
		mbnsIO.setSumins(wsaaRdtsum);
		mbnsIO.setSurrval(ZERO);
		mbnsIO.setFormat(mbnsrec);
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(),varcom.oK)) {
			rlrdtrmrec.statuz.set(mbnsIO.getStatuz());
		}
	}

protected void interimCover700()
	{
		start700();
	}

protected void start700()
	{
		mbnsIO.setChdrcoy(rlrdtrmrec.chdrcoy);
		mbnsIO.setChdrnum(rlrdtrmrec.chdrnum);
		mbnsIO.setLife(rlrdtrmrec.life);
		mbnsIO.setCoverage(rlrdtrmrec.coverage);
		mbnsIO.setRider(rlrdtrmrec.rider);
		mbnsIO.setYrsinf(wsaaIndex1);
		mbnsIO.setFormat(mbnsrec);
		mbnsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(),varcom.oK)
		|| isEQ(mbnsIO.getStatuz(),varcom.mrnf)) {
			mbnsIO.setFunction(varcom.writr);
		}
		else {
			mbnsIO.setFunction(varcom.rewrt);
		}
		wsaaRdtsum.set(rlrdtrmrec.sumins);
		mbnsIO.setSumins(wsaaRdtsum);
		mbnsIO.setSurrval(ZERO);
		mbnsIO.setFormat(mbnsrec);
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(),varcom.oK)) {
			rlrdtrmrec.statuz.set(mbnsIO.getStatuz());
		}
	}
}
