package com.csc.life.productdefinition.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5661
 * @version 1.0 generated on 30/08/09 06:47
 * @author Quipoz
 */
public class S5661ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData fuposss = new FixedLengthStringData(10).isAPartOf(dataFields, 1);
	public FixedLengthStringData[] fuposs = FLSArrayPartOfStructure(10, 1, fuposss, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(10).isAPartOf(fuposss, 0, FILLER_REDEFINE);
	public FixedLengthStringData fuposs01 = DD.fuposs.copy().isAPartOf(filler,0);
	public FixedLengthStringData fuposs02 = DD.fuposs.copy().isAPartOf(filler,1);
	public FixedLengthStringData fuposs03 = DD.fuposs.copy().isAPartOf(filler,2);
	public FixedLengthStringData fuposs04 = DD.fuposs.copy().isAPartOf(filler,3);
	public FixedLengthStringData fuposs05 = DD.fuposs.copy().isAPartOf(filler,4);
	public FixedLengthStringData fuposs06 = DD.fuposs.copy().isAPartOf(filler,5);
	public FixedLengthStringData fuposs07 = DD.fuposs.copy().isAPartOf(filler,6);
	public FixedLengthStringData fuposs08 = DD.fuposs.copy().isAPartOf(filler,7);
	public FixedLengthStringData fuposs09 = DD.fuposs.copy().isAPartOf(filler,8);
	public FixedLengthStringData fuposs10 = DD.fuposs.copy().isAPartOf(filler,9);
	public FixedLengthStringData fupstat = DD.fupsts.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,20);
	public FixedLengthStringData messages = new FixedLengthStringData(390).isAPartOf(dataFields, 50);
	public FixedLengthStringData[] message = FLSArrayPartOfStructure(5, 78, messages, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(390).isAPartOf(messages, 0, FILLER_REDEFINE);
	public FixedLengthStringData message01 = DD.message.copy().isAPartOf(filler1,0);
	public FixedLengthStringData message02 = DD.message.copy().isAPartOf(filler1,78);
	public FixedLengthStringData message03 = DD.message.copy().isAPartOf(filler1,156);
	public FixedLengthStringData message04 = DD.message.copy().isAPartOf(filler1,234);
	public FixedLengthStringData message05 = DD.message.copy().isAPartOf(filler1,312);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,440);
	public FixedLengthStringData zdalind = DD.zdalind.copy().isAPartOf(dataFields,445);
	public FixedLengthStringData zdocind = DD.zdocind.copy().isAPartOf(dataFields,446);
	public ZonedDecimalData zelpdays = DD.zelpdays.copyToZonedDecimal().isAPartOf(dataFields,447);
	public ZonedDecimalData zleadays = DD.zleadays.copyToZonedDecimal().isAPartOf(dataFields,450);
	public FixedLengthStringData zletstat = DD.zletstat.copy().isAPartOf(dataFields,453);
	public FixedLengthStringData zletstatr = DD.zletstatr.copy().isAPartOf(dataFields,454);
	public FixedLengthStringData zlettype = DD.zlettype.copy().isAPartOf(dataFields,455);
	public FixedLengthStringData zlettyper = DD.zlettyper.copy().isAPartOf(dataFields,463);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData fuposssErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] fupossErr = FLSArrayPartOfStructure(10, 4, fuposssErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(fuposssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData fuposs01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData fuposs02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData fuposs03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData fuposs04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData fuposs05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData fuposs06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData fuposs07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData fuposs08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData fuposs09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData fuposs10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData fupstsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData messagesErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData[] messageErr = FLSArrayPartOfStructure(5, 4, messagesErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(20).isAPartOf(messagesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData message01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData message02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData message03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData message04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData message05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData zdalindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData zdocindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData zelpdaysErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData zleadaysErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData zletstatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData zletstatrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData zlettypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData zlettyperErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData fuposssOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] fupossOut = FLSArrayPartOfStructure(10, 12, fuposssOut, 0);
	public FixedLengthStringData[][] fupossO = FLSDArrayPartOfArrayStructure(12, 1, fupossOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(120).isAPartOf(fuposssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] fuposs01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] fuposs02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] fuposs03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] fuposs04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] fuposs05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] fuposs06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] fuposs07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] fuposs08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] fuposs09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] fuposs10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData[] fupstsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData messagesOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 168);
	public FixedLengthStringData[] messageOut = FLSArrayPartOfStructure(5, 12, messagesOut, 0);
	public FixedLengthStringData[][] messageO = FLSDArrayPartOfArrayStructure(12, 1, messageOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(60).isAPartOf(messagesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] message01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] message02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] message03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] message04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] message05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] zdalindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] zdocindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] zelpdaysOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] zleadaysOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] zletstatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] zletstatrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] zlettypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] zlettyperOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5661screenWritten = new LongData(0);
	public LongData S5661protectWritten = new LongData(0);

	
	public static int[] screenPfInds = new int[] {1, 2, 3, 4, 5, 6, 15, 16, 17, 18, 20, 21, 22, 23, 24, 60, 61, 62}; 
	
	public boolean hasSubfile() {
		return false;
	}


	public S5661ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(fupstsOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuposs01Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuposs02Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuposs03Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuposs04Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuposs05Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuposs06Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuposs07Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuposs08Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuposs09Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuposs10Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5661screen.class;
		protectRecord = S5661protect.class;
	}
	
	
	
	public int getDataAreaSize()
	{
		return 919;
	}
	

	public int getDataFieldsSize()
	{
		return 471;
	}
	

	public int getErrorIndicatorSize()
	{
		return 112;
	}
	
	public int getOutputFieldSize()
	{
		return 336;
	}
	
	
	public BaseData[] getscreenFields()
	{
		return new BaseData[] {company, tabl, item, longdesc, fupstat, fuposs01, fuposs02, fuposs03, fuposs04, fuposs05, fuposs06, fuposs07, fuposs08, fuposs09, fuposs10, zleadays, zelpdays, zlettype, zlettyper, zletstat, zletstatr, zdalind, zdocind, message01, message02, message03, message04, message05};
	}
	
	public BaseData[][] getscreenOutFields()
	{
		return new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, fupstsOut, fuposs01Out, fuposs02Out, fuposs03Out, fuposs04Out, fuposs05Out, fuposs06Out, fuposs07Out, fuposs08Out, fuposs09Out, fuposs10Out, zleadaysOut, zelpdaysOut, zlettypeOut, zlettyperOut, zletstatOut, zletstatrOut, zdalindOut, zdocindOut, message01Out, message02Out, message03Out, message04Out, message05Out};	
	}
	
	public BaseData[] getscreenErrFields()
	{
		return new BaseData[] {companyErr, tablErr, itemErr, longdescErr, fupstsErr, fuposs01Err, fuposs02Err, fuposs03Err, fuposs04Err, fuposs05Err, fuposs06Err, fuposs07Err, fuposs08Err, fuposs09Err, fuposs10Err, zleadaysErr, zelpdaysErr, zlettypeErr, zlettyperErr, zletstatErr, zletstatrErr, zdalindErr, zdocindErr, message01Err, message02Err, message03Err, message04Err, message05Err};
	}
	

	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {};
	}
	

	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {};
	}
	

	public BaseData[] getscreenDateErrFields()
	{
		return  new BaseData[] {};
	}
	

	public static int[] getScreenPfInds()
	{
		return screenPfInds;
	}
	
}
