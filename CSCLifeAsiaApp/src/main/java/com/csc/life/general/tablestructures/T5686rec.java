package com.csc.life.general.tablestructures;

import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import java.util.Arrays;

import com.quipoz.COBOLFramework.COBOLFunctions;

public class T5686rec extends ExternalData {

	public FixedLengthStringData t5686Rec = new FixedLengthStringData(59); 
        
	public FixedLengthStringData companys = new FixedLengthStringData(1).isAPartOf(t5686Rec,0);
	public FixedLengthStringData[] company = FLSArrayPartOfStructure(1, 1, companys, 0);
	public FixedLengthStringData items = new FixedLengthStringData(8).isAPartOf(t5686Rec,1);
	public FixedLengthStringData[] item = FLSArrayPartOfStructure(8, 1, items, 0);
	public FixedLengthStringData longdescs = new FixedLengthStringData(30).isAPartOf(t5686Rec,9);
	public FixedLengthStringData[] longdesc = FLSArrayPartOfStructure(30, 1, longdescs, 0);
	public FixedLengthStringData tabls = new FixedLengthStringData(5).isAPartOf(t5686Rec,39);
	public FixedLengthStringData[] tabl = FLSArrayPartOfStructure(5, 1, tabls, 0);
	public FixedLengthStringData zdmsions = new FixedLengthStringData(15).isAPartOf(t5686Rec,44);
	public FixedLengthStringData[] zdmsion = FLSArrayPartOfStructure(15, 1, zdmsions, 0);

    public String[] fieldnamelist={"companys","items","longdescs","tabls","zdmsions"};
		
	public T5686rec(){
	}
	
	public void initialize() {
		COBOLFunctions.initialize(t5686Rec);
	}	

	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5686Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	public String[] getFieldNames(){
		return Arrays.copyOf(fieldnamelist, fieldnamelist.length);//IJTI-316
    }
}
