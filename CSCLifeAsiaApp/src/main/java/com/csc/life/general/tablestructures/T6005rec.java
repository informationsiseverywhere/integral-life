package com.csc.life.general.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:21
 * Description:
 * Copybook name: T6005REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6005rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6005Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData bappmeths = new FixedLengthStringData(20).isAPartOf(t6005Rec, 0);
  	public FixedLengthStringData[] bappmeth = FLSArrayPartOfStructure(5, 4, bappmeths, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(bappmeths, 0, FILLER_REDEFINE);
  	public FixedLengthStringData bappmeth01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData bappmeth02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData bappmeth03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData bappmeth04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData bappmeth05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData ind = new FixedLengthStringData(1).isAPartOf(t6005Rec, 20);
  	public FixedLengthStringData indics = new FixedLengthStringData(3).isAPartOf(t6005Rec, 21);
  	public FixedLengthStringData[] indic = FLSArrayPartOfStructure(3, 1, indics, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(3).isAPartOf(indics, 0, FILLER_REDEFINE);
  	public FixedLengthStringData indic01 = new FixedLengthStringData(1).isAPartOf(filler1, 0);
  	public FixedLengthStringData indic02 = new FixedLengthStringData(1).isAPartOf(filler1, 1);
  	public FixedLengthStringData indic03 = new FixedLengthStringData(1).isAPartOf(filler1, 2);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(476).isAPartOf(t6005Rec, 24, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6005Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6005Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}