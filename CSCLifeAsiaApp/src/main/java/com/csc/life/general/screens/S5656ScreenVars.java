package com.csc.life.general.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5656
 * @version 1.0 generated on 30/08/09 06:46
 * @author Quipoz
 */
public class S5656ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(313);
	public FixedLengthStringData dataFields = new FixedLengthStringData(105).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData sumbfacs = new FixedLengthStringData(42).isAPartOf(dataFields, 55);
	public ZonedDecimalData[] sumbfac = ZDArrayPartOfStructure(6, 7, 3, sumbfacs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(sumbfacs, 0, FILLER_REDEFINE);
	public ZonedDecimalData sumbfac01 = DD.sumbfac.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData sumbfac02 = DD.sumbfac.copyToZonedDecimal().isAPartOf(filler,7);
	public ZonedDecimalData sumbfac03 = DD.sumbfac.copyToZonedDecimal().isAPartOf(filler,14);
	public ZonedDecimalData sumbfac04 = DD.sumbfac.copyToZonedDecimal().isAPartOf(filler,21);
	public ZonedDecimalData sumbfac05 = DD.sumbfac.copyToZonedDecimal().isAPartOf(filler,28);
	public ZonedDecimalData sumbfac06 = DD.sumbfac.copyToZonedDecimal().isAPartOf(filler,35);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,97);
	public ZonedDecimalData termMin = DD.termmin.copyToZonedDecimal().isAPartOf(dataFields,102);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(52).isAPartOf(dataArea, 105);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData sumbfacsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData[] sumbfacErr = FLSArrayPartOfStructure(6, 4, sumbfacsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(sumbfacsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData sumbfac01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData sumbfac02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData sumbfac03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData sumbfac04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData sumbfac05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData sumbfac06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData termminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(156).isAPartOf(dataArea, 157);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData sumbfacsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 60);
	public FixedLengthStringData[] sumbfacOut = FLSArrayPartOfStructure(6, 12, sumbfacsOut, 0);
	public FixedLengthStringData[][] sumbfacO = FLSDArrayPartOfArrayStructure(12, 1, sumbfacOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(72).isAPartOf(sumbfacsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] sumbfac01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] sumbfac02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] sumbfac03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] sumbfac04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] sumbfac05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] sumbfac06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] termminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5656screenWritten = new LongData(0);
	public LongData S5656protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5656ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, termMin, sumbfac01, sumbfac02, sumbfac03, sumbfac04, sumbfac05, sumbfac06};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, termminOut, sumbfac01Out, sumbfac02Out, sumbfac03Out, sumbfac04Out, sumbfac05Out, sumbfac06Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, termminErr, sumbfac01Err, sumbfac02Err, sumbfac03Err, sumbfac04Err, sumbfac05Err, sumbfac06Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5656screen.class;
		protectRecord = S5656protect.class;
	}

}
