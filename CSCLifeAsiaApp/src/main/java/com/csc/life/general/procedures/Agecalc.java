/*
 * File: Agecalc.java
 * Date: 29 August 2009 20:13:00
 * Author: Quipoz Limited
 * 
 * Class transformed from AGECALC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.general.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* This program calculates the Age Next birthday,
* Age Nearest birthday or Age Last birthday.
*
*
* Basic Principles.
*
* Given two dates, DATE1(Birthdate) and DATE2(Risk Commencment
* Date), DATCON3 is called with a frequency of 01 and returns a
* returns a factor which is the number of times (or years) the
* frequency occurs between DATE1 and DATE2.
*
* The returned factor from DATCON3 consists of a 6 digit integer
* and 5 decimal places.  Depending on the Age calculation rule
* held on T2240 the following is done:
*  for Age Next    Birthday   .99999 is added to frequency factor
*      Age Last    Birthday  nothing is added to frequency factor
*
*  for Age Near    Birthday, it is handled differently.
*      Store Frequency (ie. no. of years) returned by the first
*      DATCON3 call.
*      Call DATCON3 again with a frequency of 12 and returns a
*      factor which is the number of months that occurs between
*      DATE1 and DATE2.
*      Find remaining months by doing this:
*         Total no. of months - (Total no. of years * 12)
*      If remaining months >= 6, add 1 to total no. of years
*
* AGECALC also returns codes from DATCON3 in case errors are
* required by calling program.
*
* Linkage area:  (These fields are contained in AGECALCREC.)
*
*     Recieved Info:
*         FUNCTION               PIC X(05)
*         CONTRACT-TYPE          PIC X(03)
*         INTERNAL-DATE-1        PIC 9(08)
*         INTERNAL-DATE-2        PIC 9(08)
*         COMPANY                PIC X(01)
*     Returned Info:
*         CALCULATED-AGE         PIC S9(03)
*         STATUZ                 PIC X(04)
*
*
* Functions available:
*
*        'CALCP' - for calls to calculate age by Online programs
*        'CALCB' - for calls to calculate age by Batch programs
*
* Possible Status Codes:
*
*        O-K     - Calculation successful
*        BOMB    - Fatal error
*        'IVFD'  - Returned from DATCON3
*        'IVSD'  - Returned from DATCON3
*        'IVFQ'  - Returned from DATCON3
*
*
* </pre>
*/
public class Agecalc extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("AGECALC");
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaCompleteNoYr = new ZonedDecimalData(6, 0).setUnsigned();
	private ZonedDecimalData wsaaCompleteNoMth = new ZonedDecimalData(6, 0).setUnsigned();
	private ZonedDecimalData wsaaOsMth = new ZonedDecimalData(6, 0).setUnsigned();
	private ZonedDecimalData wsaaAgeComplete = new ZonedDecimalData(6, 0).setUnsigned();
		/* TABLES */
	private static final String t2240 = "T2240";
		/* ERRORS */
	private static final String d008 = "D008";
	private static final String e049 = "E049";
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T2240rec t2240rec = new T2240rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Agecalcrec agecalcrec = new Agecalcrec();
	/* MTL002 */
	private FixedLengthStringData wsbbDateValue = new FixedLengthStringData(16);
	private FixedLengthStringData wsbbDate1 = new FixedLengthStringData(8).isAPartOf(wsbbDateValue, 0);
	private FixedLengthStringData wsbbDate2 = new FixedLengthStringData(8).isAPartOf(wsbbDateValue, 8);
	/* MTL002 */

	public Agecalc() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		agecalcrec.agecalcRec = convertAndSetParam(agecalcrec.agecalcRec, parmArray, 0);
		try {
			main1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main1000()
	{
		init1010();
		exit1090();
	}

protected void init1010()
	{
		agecalcrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaProg);
		if (isNE(agecalcrec.function, "CALCP")
		&& isNE(agecalcrec.function, "CALCB")) {
			syserrrec.statuz.set(e049);
			fatalError6000();
		}
		readAgeCalcTable2000();
		callDatcon33000();
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			return ;
		}
		if (isNE(t2240rec.agecode01, SPACES)) {
			calculateAgeNext3100();
		}
		else {
			if (isNE(t2240rec.agecode02, SPACES)) {
				calculateAgeNearest3200();
			}
			else {
				if (isNE(t2240rec.agecode03, SPACES)) {
					calculateAgeLast3300();
				}
				/*MTL002*/
				else {
					if (isNE(t2240rec.agecode04, SPACES)) {
						calculateAgeCalendarYear3400();
					}
				}	
				/*MTL002*/
			}
		}
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void readAgeCalcTable2000()
	{
		init2010();
	}

protected void init2010()
	{
		/*  Read Product Age Calculation rule*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(agecalcrec.company);
		itemIO.setItemtabl(t2240);
		/* MOVE AGEC-CNTTYPE           TO ITEM-ITEMITEM.                */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(agecalcrec.language);
		stringVariable1.addExpression(agecalcrec.cnttype);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError6000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			/*  Read Default Age Calculation rule if Product not found*/
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(agecalcrec.company);
			itemIO.setItemtabl(t2240);
			/*     MOVE '***'              TO ITEM-ITEMITEM                 */
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(agecalcrec.language);
			stringVariable2.addExpression("***");
			stringVariable2.setStringInto(itemIO.getItemitem());
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError6000();
			}
		}
		validateTableEntry4000();
	}

protected void callDatcon33000()
	{
		init3010();
	}

protected void init3010()
	{
		datcon3rec.intDate1.set(agecalcrec.intDate1);
		datcon3rec.intDate2.set(agecalcrec.intDate2);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isEQ(datcon3rec.statuz, "IVFD")
		|| isEQ(datcon3rec.statuz, "IVSD")
		|| isEQ(datcon3rec.statuz, "IVFQ")) {
			agecalcrec.statuz.set(datcon3rec.statuz);
			return ;
		}
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			/*    MOVE DTC3-DATCON3-REC   TO SYSR-PARAMS*/
			syserrrec.dbioStatuz.set(datcon3rec.statuz);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError6000();
		}
	}

protected void calculateAgeNext3100()
	{
		/*CALCULATE-AGE*/
		compute(agecalcrec.agerating, 5).set(add(.99999, datcon3rec.freqFactor));
		/*EXIT*/
	}

protected void calculateAgeNearest3200()
	{
		calculateAge3210();
	}

protected void calculateAge3210()
	{
		wsaaCompleteNoYr.set(datcon3rec.freqFactor);
		datcon3rec.intDate1.set(agecalcrec.intDate1);
		datcon3rec.intDate2.set(agecalcrec.intDate2);
		datcon3rec.frequency.set("12");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isEQ(datcon3rec.statuz, "IVFD")
		|| isEQ(datcon3rec.statuz, "IVSD")
		|| isEQ(datcon3rec.statuz, "IVFQ")) {
			agecalcrec.statuz.set(datcon3rec.statuz);
			return ;
			/****     GO TO 3090-EXIT                                   <S19FIX>*/
		}
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.dbioStatuz.set(datcon3rec.statuz);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError6000();
		}
		wsaaCompleteNoMth.set(datcon3rec.freqFactor);
		compute(wsaaOsMth, 0).set(sub(wsaaCompleteNoMth, (mult(wsaaCompleteNoYr, 12))));
		if (isGTE(wsaaOsMth, 6)) {
			compute(agecalcrec.agerating, 0).set(add(wsaaCompleteNoYr, 1));
		}
		else {
			agecalcrec.agerating.set(wsaaCompleteNoYr);
		}
	}

protected void calculateAgeLast3300()
	{
		/*CALCULATE-AGE*/
		agecalcrec.agerating.set(datcon3rec.freqFactor);
		/*EXIT*/
	}
/*MTL002*/
protected void calculateAgeCalendarYear3400()
{
	/*CALCULATE-AGE*/
	wsbbDate1.set(agecalcrec.intDate1);
	wsbbDate2.set(agecalcrec.intDate2);
   	agecalcrec.year1.set(wsbbDate1.substring(0,4));
   	agecalcrec.year2.set(wsbbDate2.substring(0,4));
	compute(agecalcrec.agerating, 0).set(sub(agecalcrec.year2,agecalcrec.year1));
	/*EXIT*/
}
/*MTL002*/

protected void validateTableEntry4000()
	{
		/*INIT*/
		/* Stringent check of Table is important because of the*/
		/* impact a false contract age can have especially on batch*/
		/* jobs.*/
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		wsaaCount.set(0);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 4)); wsaaIndex.add(1)){
			if (isNE(t2240rec.agecode[wsaaIndex.toInt()], SPACES)) {
				wsaaCount.add(1);
			}
		}
		if (isNE(wsaaCount, 1)) {
			syserrrec.params.set(itemIO.getDataArea());
			syserrrec.statuz.set(d008);
			fatalError6000();
		}
		/*EXIT*/
	}

protected void fatalError6000()
	{
		error6000();
		exit6010();
	}

protected void error6000()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")
		&& isEQ(agecalcrec.function, "CALCP")) {
			syserrrec.syserrType.set("1");
		}
		if (isNE(syserrrec.syserrType, "4")
		&& isEQ(agecalcrec.function, "CALCB")) {
			syserrrec.syserrType.set("3");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit6010()
	{
		agecalcrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
