/*
 * File: T5600pt.java
 * Date: 30 August 2009 2:23:28
 * Author: Quipoz Limited
 * 
 * Class transformed from T5600PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.general.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.general.tablestructures.T5600rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5600.
*
*
*****************************************************************
* </pre>
*/
public class T5600pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine001, 28, FILLER).init("Komponente Zustande Details                S5600");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(48);
	private FixedLengthStringData filler7 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine003, 0, FILLER).init("   Zustand          Risk Term       Benefit Term");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(42);
	private FixedLengthStringData filler8 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine004, 0, FILLER).init("    01  :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 24);
	private FixedLengthStringData filler9 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine004, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 41);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(42);
	private FixedLengthStringData filler10 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine005, 0, FILLER).init("    02  :");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 24);
	private FixedLengthStringData filler11 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine005, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 41);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(42);
	private FixedLengthStringData filler12 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine006, 0, FILLER).init("    03  :");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 24);
	private FixedLengthStringData filler13 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine006, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 41);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(42);
	private FixedLengthStringData filler14 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine007, 0, FILLER).init("    04  :");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 24);
	private FixedLengthStringData filler15 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine007, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 41);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(42);
	private FixedLengthStringData filler16 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine008, 0, FILLER).init("    05  :");
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 24);
	private FixedLengthStringData filler17 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine008, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 41);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(42);
	private FixedLengthStringData filler18 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine009, 0, FILLER).init("    06  :");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 24);
	private FixedLengthStringData filler19 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine009, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 41);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(42);
	private FixedLengthStringData filler20 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine010, 0, FILLER).init("    07  :");
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 24);
	private FixedLengthStringData filler21 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine010, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 41);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(42);
	private FixedLengthStringData filler22 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine011, 0, FILLER).init("    08  :");
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 24);
	private FixedLengthStringData filler23 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine011, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 41);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(42);
	private FixedLengthStringData filler24 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine012, 0, FILLER).init("    09  :");
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 24);
	private FixedLengthStringData filler25 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine012, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 41);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(42);
	private FixedLengthStringData filler26 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine013, 0, FILLER).init("    10  :");
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 24);
	private FixedLengthStringData filler27 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine013, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 41);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(42);
	private FixedLengthStringData filler28 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine014, 0, FILLER).init("    11  :");
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 24);
	private FixedLengthStringData filler29 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine014, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 41);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(42);
	private FixedLengthStringData filler30 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine015, 0, FILLER).init("    12  :");
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 24);
	private FixedLengthStringData filler31 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine015, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 41);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(42);
	private FixedLengthStringData filler32 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine016, 0, FILLER).init("    13  :");
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 24);
	private FixedLengthStringData filler33 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine016, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 41);

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(42);
	private FixedLengthStringData filler34 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine017, 0, FILLER).init("    14  :");
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 24);
	private FixedLengthStringData filler35 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine017, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 41);

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(42);
	private FixedLengthStringData filler36 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine018, 0, FILLER).init("    15  :");
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 24);
	private FixedLengthStringData filler37 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine018, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 41);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5600rec t5600rec = new T5600rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5600pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5600rec.t5600Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(t5600rec.ind01);
		fieldNo006.set(t5600rec.indic01);
		fieldNo007.set(t5600rec.ind02);
		fieldNo009.set(t5600rec.ind03);
		fieldNo011.set(t5600rec.ind04);
		fieldNo013.set(t5600rec.ind05);
		fieldNo015.set(t5600rec.ind06);
		fieldNo017.set(t5600rec.ind07);
		fieldNo019.set(t5600rec.ind08);
		fieldNo021.set(t5600rec.ind09);
		fieldNo023.set(t5600rec.ind10);
		fieldNo025.set(t5600rec.ind11);
		fieldNo027.set(t5600rec.ind12);
		fieldNo029.set(t5600rec.ind13);
		fieldNo031.set(t5600rec.ind14);
		fieldNo033.set(t5600rec.ind15);
		fieldNo008.set(t5600rec.indic02);
		fieldNo010.set(t5600rec.indic03);
		fieldNo012.set(t5600rec.indic04);
		fieldNo014.set(t5600rec.indic05);
		fieldNo016.set(t5600rec.indic06);
		fieldNo018.set(t5600rec.indic07);
		fieldNo020.set(t5600rec.indic08);
		fieldNo022.set(t5600rec.indic09);
		fieldNo024.set(t5600rec.indic10);
		fieldNo026.set(t5600rec.indic11);
		fieldNo028.set(t5600rec.indic12);
		fieldNo030.set(t5600rec.indic13);
		fieldNo032.set(t5600rec.indic14);
		fieldNo034.set(t5600rec.indic15);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
