package com.csc.life.general.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen Variables for Sac03
 * @version 1.0 Generated on Fri Sep 06 12:27:03 SGT 2013
 * @author CSC
 */
public class Sr57xScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(918);
	public FixedLengthStringData dataFields = new FixedLengthStringData(134).isAPartOf(dataArea, 0);
	//---------------data datafield contents
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0); 
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1); 
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9); 
	public FixedLengthStringData rdocpfxs = new FixedLengthStringData(30).isAPartOf(dataFields, 39);
	public FixedLengthStringData[] rdocpfx = FLSArrayPartOfStructure(15, 2, rdocpfxs, 0);
	public FixedLengthStringData rdocpfx01 = DD.rdocpfx.copy().isAPartOf(rdocpfxs,0); 
	public FixedLengthStringData rdocpfx02 = DD.rdocpfx.copy().isAPartOf(rdocpfxs,2); 
	public FixedLengthStringData rdocpfx03 = DD.rdocpfx.copy().isAPartOf(rdocpfxs,4); 
	public FixedLengthStringData rdocpfx04 = DD.rdocpfx.copy().isAPartOf(rdocpfxs,6); 
	public FixedLengthStringData rdocpfx05 = DD.rdocpfx.copy().isAPartOf(rdocpfxs,8); 
	public FixedLengthStringData rdocpfx06 = DD.rdocpfx.copy().isAPartOf(rdocpfxs,10); 
	public FixedLengthStringData rdocpfx07 = DD.rdocpfx.copy().isAPartOf(rdocpfxs,12); 
	public FixedLengthStringData rdocpfx08 = DD.rdocpfx.copy().isAPartOf(rdocpfxs,14); 
	public FixedLengthStringData rdocpfx09 = DD.rdocpfx.copy().isAPartOf(rdocpfxs,16); 
	public FixedLengthStringData rdocpfx10 = DD.rdocpfx.copy().isAPartOf(rdocpfxs,18); 
	public FixedLengthStringData rdocpfx11 = DD.rdocpfx.copy().isAPartOf(rdocpfxs,20); 
	public FixedLengthStringData rdocpfx12 = DD.rdocpfx.copy().isAPartOf(rdocpfxs,22); 
	public FixedLengthStringData rdocpfx13 = DD.rdocpfx.copy().isAPartOf(rdocpfxs,24); 
	public FixedLengthStringData rdocpfx14 = DD.rdocpfx.copy().isAPartOf(rdocpfxs,26); 
	public FixedLengthStringData rdocpfx15 = DD.rdocpfx.copy().isAPartOf(rdocpfxs,28); 
	public FixedLengthStringData sacscodes = new FixedLengthStringData(30).isAPartOf(dataFields, 69);
	public FixedLengthStringData[] sacscode = FLSArrayPartOfStructure(15, 2, sacscodes, 0);
	public FixedLengthStringData sacscode01 = DD.sacscode.copy().isAPartOf(sacscodes,0); 
	public FixedLengthStringData sacscode02 = DD.sacscode.copy().isAPartOf(sacscodes,2); 
	public FixedLengthStringData sacscode03 = DD.sacscode.copy().isAPartOf(sacscodes,4); 
	public FixedLengthStringData sacscode04 = DD.sacscode.copy().isAPartOf(sacscodes,6); 
	public FixedLengthStringData sacscode05 = DD.sacscode.copy().isAPartOf(sacscodes,8); 
	public FixedLengthStringData sacscode06 = DD.sacscode.copy().isAPartOf(sacscodes,10); 
	public FixedLengthStringData sacscode07 = DD.sacscode.copy().isAPartOf(sacscodes,12); 
	public FixedLengthStringData sacscode08 = DD.sacscode.copy().isAPartOf(sacscodes,14); 
	public FixedLengthStringData sacscode09 = DD.sacscode.copy().isAPartOf(sacscodes,16); 
	public FixedLengthStringData sacscode10 = DD.sacscode.copy().isAPartOf(sacscodes,18); 
	public FixedLengthStringData sacscode11 = DD.sacscode.copy().isAPartOf(sacscodes,20); 
	public FixedLengthStringData sacscode12 = DD.sacscode.copy().isAPartOf(sacscodes,22); 
	public FixedLengthStringData sacscode13 = DD.sacscode.copy().isAPartOf(sacscodes,24); 
	public FixedLengthStringData sacscode14 = DD.sacscode.copy().isAPartOf(sacscodes,26); 
	public FixedLengthStringData sacscode15 = DD.sacscode.copy().isAPartOf(sacscodes,28); 
	public FixedLengthStringData sacstypes = new FixedLengthStringData(30).isAPartOf(dataFields, 99);
	public FixedLengthStringData[] sacstype = FLSArrayPartOfStructure(15, 2, sacstypes, 0);
	public FixedLengthStringData sacstype01 = DD.sacstype.copy().isAPartOf(sacstypes,0); 
	public FixedLengthStringData sacstype02 = DD.sacstype.copy().isAPartOf(sacstypes,2); 
	public FixedLengthStringData sacstype03 = DD.sacstype.copy().isAPartOf(sacstypes,4); 
	public FixedLengthStringData sacstype04 = DD.sacstype.copy().isAPartOf(sacstypes,6); 
	public FixedLengthStringData sacstype05 = DD.sacstype.copy().isAPartOf(sacstypes,8); 
	public FixedLengthStringData sacstype06 = DD.sacstype.copy().isAPartOf(sacstypes,10); 
	public FixedLengthStringData sacstype07 = DD.sacstype.copy().isAPartOf(sacstypes,12); 
	public FixedLengthStringData sacstype08 = DD.sacstype.copy().isAPartOf(sacstypes,14); 
	public FixedLengthStringData sacstype09 = DD.sacstype.copy().isAPartOf(sacstypes,16); 
	public FixedLengthStringData sacstype10 = DD.sacstype.copy().isAPartOf(sacstypes,18); 
	public FixedLengthStringData sacstype11 = DD.sacstype.copy().isAPartOf(sacstypes,20); 
	public FixedLengthStringData sacstype12 = DD.sacstype.copy().isAPartOf(sacstypes,22); 
	public FixedLengthStringData sacstype13 = DD.sacstype.copy().isAPartOf(sacstypes,24); 
	public FixedLengthStringData sacstype14 = DD.sacstype.copy().isAPartOf(sacstypes,26); 
	public FixedLengthStringData sacstype15 = DD.sacstype.copy().isAPartOf(sacstypes,28); 
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,129); 

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(196).isAPartOf(dataArea, 134);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData rdocpfxsErr = new FixedLengthStringData(60).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] rdocpfxErr = FLSArrayPartOfStructure(15, 4, rdocpfxsErr, 0);
	public FixedLengthStringData rdocpfx01Err =new FixedLengthStringData(4).isAPartOf(rdocpfxsErr,0); 
	public FixedLengthStringData rdocpfx02Err =new FixedLengthStringData(4).isAPartOf(rdocpfxsErr,4); 
	public FixedLengthStringData rdocpfx03Err =new FixedLengthStringData(4).isAPartOf(rdocpfxsErr,8); 
	public FixedLengthStringData rdocpfx04Err =new FixedLengthStringData(4).isAPartOf(rdocpfxsErr,12); 
	public FixedLengthStringData rdocpfx05Err =new FixedLengthStringData(4).isAPartOf(rdocpfxsErr,16); 
	public FixedLengthStringData rdocpfx06Err =new FixedLengthStringData(4).isAPartOf(rdocpfxsErr,20); 
	public FixedLengthStringData rdocpfx07Err =new FixedLengthStringData(4).isAPartOf(rdocpfxsErr,24); 
	public FixedLengthStringData rdocpfx08Err =new FixedLengthStringData(4).isAPartOf(rdocpfxsErr,28); 
	public FixedLengthStringData rdocpfx09Err =new FixedLengthStringData(4).isAPartOf(rdocpfxsErr,32); 
	public FixedLengthStringData rdocpfx10Err =new FixedLengthStringData(4).isAPartOf(rdocpfxsErr,36); 
	public FixedLengthStringData rdocpfx11Err =new FixedLengthStringData(4).isAPartOf(rdocpfxsErr,40); 
	public FixedLengthStringData rdocpfx12Err =new FixedLengthStringData(4).isAPartOf(rdocpfxsErr,44); 
	public FixedLengthStringData rdocpfx13Err =new FixedLengthStringData(4).isAPartOf(rdocpfxsErr,48); 
	public FixedLengthStringData rdocpfx14Err =new FixedLengthStringData(4).isAPartOf(rdocpfxsErr,52); 
	public FixedLengthStringData rdocpfx15Err =new FixedLengthStringData(4).isAPartOf(rdocpfxsErr,56); 
	public FixedLengthStringData sacscodesErr = new FixedLengthStringData(60).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData[] sacscodeErr = FLSArrayPartOfStructure(15, 4, sacscodesErr, 0);
	public FixedLengthStringData sacscode01Err =new FixedLengthStringData(4).isAPartOf(sacscodesErr,0); 
	public FixedLengthStringData sacscode02Err =new FixedLengthStringData(4).isAPartOf(sacscodesErr,4); 
	public FixedLengthStringData sacscode03Err =new FixedLengthStringData(4).isAPartOf(sacscodesErr,8); 
	public FixedLengthStringData sacscode04Err =new FixedLengthStringData(4).isAPartOf(sacscodesErr,12); 
	public FixedLengthStringData sacscode05Err =new FixedLengthStringData(4).isAPartOf(sacscodesErr,16); 
	public FixedLengthStringData sacscode06Err =new FixedLengthStringData(4).isAPartOf(sacscodesErr,20); 
	public FixedLengthStringData sacscode07Err =new FixedLengthStringData(4).isAPartOf(sacscodesErr,24); 
	public FixedLengthStringData sacscode08Err =new FixedLengthStringData(4).isAPartOf(sacscodesErr,28); 
	public FixedLengthStringData sacscode09Err =new FixedLengthStringData(4).isAPartOf(sacscodesErr,32); 
	public FixedLengthStringData sacscode10Err =new FixedLengthStringData(4).isAPartOf(sacscodesErr,36); 
	public FixedLengthStringData sacscode11Err =new FixedLengthStringData(4).isAPartOf(sacscodesErr,40); 
	public FixedLengthStringData sacscode12Err =new FixedLengthStringData(4).isAPartOf(sacscodesErr,44); 
	public FixedLengthStringData sacscode13Err =new FixedLengthStringData(4).isAPartOf(sacscodesErr,48); 
	public FixedLengthStringData sacscode14Err =new FixedLengthStringData(4).isAPartOf(sacscodesErr,52); 
	public FixedLengthStringData sacscode15Err =new FixedLengthStringData(4).isAPartOf(sacscodesErr,56); 
	public FixedLengthStringData sacstypesErr = new FixedLengthStringData(60).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData[] sacstypeErr = FLSArrayPartOfStructure(15, 4, sacstypesErr, 0);
	public FixedLengthStringData sacstype01Err =new FixedLengthStringData(4).isAPartOf(sacstypesErr,0); 
	public FixedLengthStringData sacstype02Err =new FixedLengthStringData(4).isAPartOf(sacstypesErr,4); 
	public FixedLengthStringData sacstype03Err =new FixedLengthStringData(4).isAPartOf(sacstypesErr,8); 
	public FixedLengthStringData sacstype04Err =new FixedLengthStringData(4).isAPartOf(sacstypesErr,12); 
	public FixedLengthStringData sacstype05Err =new FixedLengthStringData(4).isAPartOf(sacstypesErr,16); 
	public FixedLengthStringData sacstype06Err =new FixedLengthStringData(4).isAPartOf(sacstypesErr,20); 
	public FixedLengthStringData sacstype07Err =new FixedLengthStringData(4).isAPartOf(sacstypesErr,24); 
	public FixedLengthStringData sacstype08Err =new FixedLengthStringData(4).isAPartOf(sacstypesErr,28); 
	public FixedLengthStringData sacstype09Err =new FixedLengthStringData(4).isAPartOf(sacstypesErr,32); 
	public FixedLengthStringData sacstype10Err =new FixedLengthStringData(4).isAPartOf(sacstypesErr,36); 
	public FixedLengthStringData sacstype11Err =new FixedLengthStringData(4).isAPartOf(sacstypesErr,40); 
	public FixedLengthStringData sacstype12Err =new FixedLengthStringData(4).isAPartOf(sacstypesErr,44); 
	public FixedLengthStringData sacstype13Err =new FixedLengthStringData(4).isAPartOf(sacstypesErr,48); 
	public FixedLengthStringData sacstype14Err =new FixedLengthStringData(4).isAPartOf(sacstypesErr,52); 
	public FixedLengthStringData sacstype15Err =new FixedLengthStringData(4).isAPartOf(sacstypesErr,56); 
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(588).isAPartOf(dataArea,330);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData rdocpfxsOut = new FixedLengthStringData(180).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] rdocpfxOut = FLSArrayPartOfStructure(15, 12, rdocpfxsOut, 0);
	public FixedLengthStringData[][] rdocpfxO = FLSDArrayPartOfArrayStructure(12, 1, rdocpfxOut, 0);
	public FixedLengthStringData[] rdocpfx01Out = FLSArrayPartOfStructure(12, 1, rdocpfxsOut, 0);
	public FixedLengthStringData[] rdocpfx02Out = FLSArrayPartOfStructure(12, 1, rdocpfxsOut, 12);
	public FixedLengthStringData[] rdocpfx03Out = FLSArrayPartOfStructure(12, 1, rdocpfxsOut, 24);
	public FixedLengthStringData[] rdocpfx04Out = FLSArrayPartOfStructure(12, 1, rdocpfxsOut, 36);
	public FixedLengthStringData[] rdocpfx05Out = FLSArrayPartOfStructure(12, 1, rdocpfxsOut, 48);
	public FixedLengthStringData[] rdocpfx06Out = FLSArrayPartOfStructure(12, 1, rdocpfxsOut, 60);
	public FixedLengthStringData[] rdocpfx07Out = FLSArrayPartOfStructure(12, 1, rdocpfxsOut, 72);
	public FixedLengthStringData[] rdocpfx08Out = FLSArrayPartOfStructure(12, 1, rdocpfxsOut, 84);
	public FixedLengthStringData[] rdocpfx09Out = FLSArrayPartOfStructure(12, 1, rdocpfxsOut, 96);
	public FixedLengthStringData[] rdocpfx10Out = FLSArrayPartOfStructure(12, 1, rdocpfxsOut, 108);
	public FixedLengthStringData[] rdocpfx11Out = FLSArrayPartOfStructure(12, 1, rdocpfxsOut, 120);
	public FixedLengthStringData[] rdocpfx12Out = FLSArrayPartOfStructure(12, 1, rdocpfxsOut, 132);
	public FixedLengthStringData[] rdocpfx13Out = FLSArrayPartOfStructure(12, 1, rdocpfxsOut, 144);
	public FixedLengthStringData[] rdocpfx14Out = FLSArrayPartOfStructure(12, 1, rdocpfxsOut, 156);
	public FixedLengthStringData[] rdocpfx15Out = FLSArrayPartOfStructure(12, 1, rdocpfxsOut, 168);
	public FixedLengthStringData sacscodesOut = new FixedLengthStringData(180).isAPartOf(outputIndicators, 216);
	public FixedLengthStringData[] sacscodeOut = FLSArrayPartOfStructure(15, 12, sacscodesOut, 0);
	public FixedLengthStringData[][] sacscodeO = FLSDArrayPartOfArrayStructure(12, 1, sacscodeOut, 0);
	public FixedLengthStringData[] sacscode01Out = FLSArrayPartOfStructure(12, 1, sacscodesOut, 0);
	public FixedLengthStringData[] sacscode02Out = FLSArrayPartOfStructure(12, 1, sacscodesOut, 12);
	public FixedLengthStringData[] sacscode03Out = FLSArrayPartOfStructure(12, 1, sacscodesOut, 24);
	public FixedLengthStringData[] sacscode04Out = FLSArrayPartOfStructure(12, 1, sacscodesOut, 36);
	public FixedLengthStringData[] sacscode05Out = FLSArrayPartOfStructure(12, 1, sacscodesOut, 48);
	public FixedLengthStringData[] sacscode06Out = FLSArrayPartOfStructure(12, 1, sacscodesOut, 60);
	public FixedLengthStringData[] sacscode07Out = FLSArrayPartOfStructure(12, 1, sacscodesOut, 72);
	public FixedLengthStringData[] sacscode08Out = FLSArrayPartOfStructure(12, 1, sacscodesOut, 84);
	public FixedLengthStringData[] sacscode09Out = FLSArrayPartOfStructure(12, 1, sacscodesOut, 96);
	public FixedLengthStringData[] sacscode10Out = FLSArrayPartOfStructure(12, 1, sacscodesOut, 108);
	public FixedLengthStringData[] sacscode11Out = FLSArrayPartOfStructure(12, 1, sacscodesOut, 120);
	public FixedLengthStringData[] sacscode12Out = FLSArrayPartOfStructure(12, 1, sacscodesOut, 132);
	public FixedLengthStringData[] sacscode13Out = FLSArrayPartOfStructure(12, 1, sacscodesOut, 144);
	public FixedLengthStringData[] sacscode14Out = FLSArrayPartOfStructure(12, 1, sacscodesOut, 156);
	public FixedLengthStringData[] sacscode15Out = FLSArrayPartOfStructure(12, 1, sacscodesOut, 168);
	public FixedLengthStringData sacstypesOut = new FixedLengthStringData(180).isAPartOf(outputIndicators, 396);
	public FixedLengthStringData[] sacstypeOut = FLSArrayPartOfStructure(15, 12, sacstypesOut, 0);
	public FixedLengthStringData[][] sacstypeO = FLSDArrayPartOfArrayStructure(12, 1, sacstypeOut, 0);
	public FixedLengthStringData[] sacstype01Out = FLSArrayPartOfStructure(12, 1, sacstypesOut, 0);
	public FixedLengthStringData[] sacstype02Out = FLSArrayPartOfStructure(12, 1, sacstypesOut, 12);
	public FixedLengthStringData[] sacstype03Out = FLSArrayPartOfStructure(12, 1, sacstypesOut, 24);
	public FixedLengthStringData[] sacstype04Out = FLSArrayPartOfStructure(12, 1, sacstypesOut, 36);
	public FixedLengthStringData[] sacstype05Out = FLSArrayPartOfStructure(12, 1, sacstypesOut, 48);
	public FixedLengthStringData[] sacstype06Out = FLSArrayPartOfStructure(12, 1, sacstypesOut, 60);
	public FixedLengthStringData[] sacstype07Out = FLSArrayPartOfStructure(12, 1, sacstypesOut, 72);
	public FixedLengthStringData[] sacstype08Out = FLSArrayPartOfStructure(12, 1, sacstypesOut, 84);
	public FixedLengthStringData[] sacstype09Out = FLSArrayPartOfStructure(12, 1, sacstypesOut, 96);
	public FixedLengthStringData[] sacstype10Out = FLSArrayPartOfStructure(12, 1, sacstypesOut, 108);
	public FixedLengthStringData[] sacstype11Out = FLSArrayPartOfStructure(12, 1, sacstypesOut, 120);
	public FixedLengthStringData[] sacstype12Out = FLSArrayPartOfStructure(12, 1, sacstypesOut, 132);
	public FixedLengthStringData[] sacstype13Out = FLSArrayPartOfStructure(12, 1, sacstypesOut, 144);
	public FixedLengthStringData[] sacstype14Out = FLSArrayPartOfStructure(12, 1, sacstypesOut, 156);
	public FixedLengthStringData[] sacstype15Out = FLSArrayPartOfStructure(12, 1, sacstypesOut, 168);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public GeneralTable sr57xscreensfl = new GeneralTable(AppVars.getInstance());
	public LongData Sr57xscreenctlWritten = new LongData(0);
	public LongData Sr57xscreensflWritten = new LongData(0);
	public LongData Sr57xscreenWritten = new LongData(0);
	public LongData Sr57xwindowWritten = new LongData(0);
	public LongData Sr57xhideWritten = new LongData(0);
	public LongData Sr57xprotectWritten = new LongData(0);

	public boolean hasSubfile() { 
		return false;
	}

	public Sr57xScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(companyOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(itemOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(longdescOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rdocpfx01Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rdocpfx02Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rdocpfx03Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rdocpfx04Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rdocpfx05Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rdocpfx06Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rdocpfx07Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rdocpfx08Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rdocpfx09Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rdocpfx10Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rdocpfx11Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rdocpfx12Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rdocpfx13Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rdocpfx14Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rdocpfx15Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacscode01Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacscode02Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacscode03Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacscode04Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacscode05Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacscode06Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacscode07Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacscode08Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacscode09Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacscode10Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacscode11Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacscode12Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacscode13Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacscode14Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacscode15Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstype01Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstype02Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstype03Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstype04Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstype05Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstype06Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstype07Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstype08Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstype09Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstype10Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstype11Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstype12Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstype13Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstype14Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstype15Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tablOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	
		screenFields = new BaseData[] { company,item,longdesc,rdocpfx01,rdocpfx02,rdocpfx03,rdocpfx04,rdocpfx05,rdocpfx06,rdocpfx07,rdocpfx08,rdocpfx09,rdocpfx10,rdocpfx11,rdocpfx12,rdocpfx13,rdocpfx14,rdocpfx15,sacscode01,sacscode02,sacscode03,sacscode04,sacscode05,sacscode06,sacscode07,sacscode08,sacscode09,sacscode10,sacscode11,sacscode12,sacscode13,sacscode14,sacscode15,sacstype01,sacstype02,sacstype03,sacstype04,sacstype05,sacstype06,sacstype07,sacstype08,sacstype09,sacstype10,sacstype11,sacstype12,sacstype13,sacstype14,sacstype15,tabl };
		screenOutFields = new BaseData[][] { companyOut,itemOut,longdescOut,rdocpfx01Out,rdocpfx02Out,rdocpfx03Out,rdocpfx04Out,rdocpfx05Out,rdocpfx06Out,rdocpfx07Out,rdocpfx08Out,rdocpfx09Out,rdocpfx10Out,rdocpfx11Out,rdocpfx12Out,rdocpfx13Out,rdocpfx14Out,rdocpfx15Out,sacscode01Out,sacscode02Out,sacscode03Out,sacscode04Out,sacscode05Out,sacscode06Out,sacscode07Out,sacscode08Out,sacscode09Out,sacscode10Out,sacscode11Out,sacscode12Out,sacscode13Out,sacscode14Out,sacscode15Out,sacstype01Out,sacstype02Out,sacstype03Out,sacstype04Out,sacstype05Out,sacstype06Out,sacstype07Out,sacstype08Out,sacstype09Out,sacstype10Out,sacstype11Out,sacstype12Out,sacstype13Out,sacstype14Out,sacstype15Out,tablOut };
		screenErrFields = new BaseData[] {  companyErr,itemErr,longdescErr,rdocpfx01Err,rdocpfx02Err,rdocpfx03Err,rdocpfx04Err,rdocpfx05Err,rdocpfx06Err,rdocpfx07Err,rdocpfx08Err,rdocpfx09Err,rdocpfx10Err,rdocpfx11Err,rdocpfx12Err,rdocpfx13Err,rdocpfx14Err,rdocpfx15Err,sacscode01Err,sacscode02Err,sacscode03Err,sacscode04Err,sacscode05Err,sacscode06Err,sacscode07Err,sacscode08Err,sacscode09Err,sacscode10Err,sacscode11Err,sacscode12Err,sacscode13Err,sacscode14Err,sacscode15Err,sacstype01Err,sacstype02Err,sacstype03Err,sacstype04Err,sacstype05Err,sacstype06Err,sacstype07Err,sacstype08Err,sacstype09Err,sacstype10Err,sacstype11Err,sacstype12Err,sacstype13Err,sacstype14Err,sacstype15Err,tablErr  };
		screenDateFields = new BaseData[] {   };
		screenDateErrFields = new BaseData[] {   };
		screenDateDispFields = new BaseData[] {    };
	
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr57xscreen.class;
		protectRecord = Sr57xprotect.class;
	}

 

}
