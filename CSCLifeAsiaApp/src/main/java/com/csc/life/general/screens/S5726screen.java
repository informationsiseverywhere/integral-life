package com.csc.life.general.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S5726screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 19, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5726ScreenVars sv = (S5726ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5726screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5726ScreenVars screenVars = (S5726ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.indic01.setClassString("");
		screenVars.indic14.setClassString("");
		screenVars.indic02.setClassString("");
		screenVars.indic15.setClassString("");
		screenVars.indic03.setClassString("");
		screenVars.indic16.setClassString("");
		screenVars.indic04.setClassString("");
		screenVars.indic17.setClassString("");
		screenVars.indic05.setClassString("");
		screenVars.indic18.setClassString("");
		screenVars.indic06.setClassString("");
		screenVars.indic19.setClassString("");
		screenVars.indic07.setClassString("");
		screenVars.indic20.setClassString("");
		screenVars.indic08.setClassString("");
		screenVars.indic21.setClassString("");
		screenVars.indic09.setClassString("");
		screenVars.indic22.setClassString("");
		screenVars.indic10.setClassString("");
		screenVars.indic23.setClassString("");
		screenVars.indic11.setClassString("");
		screenVars.indic24.setClassString("");
		screenVars.indic12.setClassString("");
		screenVars.indic25.setClassString("");
		screenVars.indic13.setClassString("");
	}

/**
 * Clear all the variables in S5726screen
 */
	public static void clear(VarModel pv) {
		S5726ScreenVars screenVars = (S5726ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.indic01.clear();
		screenVars.indic14.clear();
		screenVars.indic02.clear();
		screenVars.indic15.clear();
		screenVars.indic03.clear();
		screenVars.indic16.clear();
		screenVars.indic04.clear();
		screenVars.indic17.clear();
		screenVars.indic05.clear();
		screenVars.indic18.clear();
		screenVars.indic06.clear();
		screenVars.indic19.clear();
		screenVars.indic07.clear();
		screenVars.indic20.clear();
		screenVars.indic08.clear();
		screenVars.indic21.clear();
		screenVars.indic09.clear();
		screenVars.indic22.clear();
		screenVars.indic10.clear();
		screenVars.indic23.clear();
		screenVars.indic11.clear();
		screenVars.indic24.clear();
		screenVars.indic12.clear();
		screenVars.indic25.clear();
		screenVars.indic13.clear();
	}
}
