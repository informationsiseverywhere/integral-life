package com.csc.life.general.tablestructures;

import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import java.util.Arrays;

import com.quipoz.COBOLFramework.COBOLFunctions;

public class Tr57wrec extends ExternalData {

	public FixedLengthStringData tr57wRec = new FixedLengthStringData(500); 
        
	public FixedLengthStringData companys = new FixedLengthStringData(1).isAPartOf(tr57wRec,0);
	public FixedLengthStringData[] company = FLSArrayPartOfStructure(1, 1, companys, 0);
	
	public FixedLengthStringData items = new FixedLengthStringData(8).isAPartOf(tr57wRec,1);
	public FixedLengthStringData[] item = FLSArrayPartOfStructure(8, 1, items, 0);
	
	public FixedLengthStringData longdescs = new FixedLengthStringData(30).isAPartOf(tr57wRec,9);
	public FixedLengthStringData[] longdesc = FLSArrayPartOfStructure(30, 1, longdescs, 0);
	
	public FixedLengthStringData tabls = new FixedLengthStringData(5).isAPartOf(tr57wRec,39);
	public FixedLengthStringData[] tabl = FLSArrayPartOfStructure(5, 1, tabls, 0);
	
	public FixedLengthStringData zdmsions = new FixedLengthStringData(30).isAPartOf(tr57wRec,44);
	public FixedLengthStringData[] zdmsion = FLSArrayPartOfStructure(2,15,zdmsions, 0);
	public FixedLengthStringData zdmsion01 = new FixedLengthStringData(15).isAPartOf(zdmsions, 0);
	public FixedLengthStringData zdmsion02 = new FixedLengthStringData(15).isAPartOf(zdmsions, 15);
	public FixedLengthStringData filler = new FixedLengthStringData(426).isAPartOf(tr57wRec, 74, FILLER);

    public String[] fieldnamelist={"companys","items","longdescs","tabls","zdmsion01s","zdmsion02s"};
		
	public Tr57wrec(){
	}
	
	public void initialize() {
		COBOLFunctions.initialize(tr57wRec);
	}	

	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr57wRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	public String[] getFieldNames(){
		return Arrays.copyOf(fieldnamelist, fieldnamelist.length);//IJTI-316
    }
}
