package com.csc.life.general.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:08
 * Description:
 * Copybook name: T5604REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5604rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5604Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData maxpcnt = new ZonedDecimalData(5, 2).isAPartOf(t5604Rec, 0);
  	public ZonedDecimalData minpcnt = new ZonedDecimalData(5, 2).isAPartOf(t5604Rec, 5);
  	public FixedLengthStringData simpleInd = new FixedLengthStringData(1).isAPartOf(t5604Rec, 10);
  	public FixedLengthStringData xfreqAltBasis = new FixedLengthStringData(4).isAPartOf(t5604Rec, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(485).isAPartOf(t5604Rec, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5604Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5604Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}