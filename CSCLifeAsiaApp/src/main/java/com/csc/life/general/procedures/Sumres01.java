/*
 * File: Sumres01.java
 * Date: 30 August 2009 2:16:10
 * Author: Quipoz Limited
 * 
 * Class transformed from SUMRES01.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.general.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsuframework.cls.FsuCLFunctions;
import com.csc.life.general.dataaccess.PovrgenTableDAM;
import com.csc.life.general.recordstructures.Clvd3ba;
import com.csc.life.general.recordstructures.Clvd3ek;
import com.csc.life.general.recordstructures.Sumcalcrec;
import com.csc.life.general.tablestructures.T5600rec;
import com.csc.life.general.tablestructures.T5601rec;
import com.csc.life.general.tablestructures.T5602rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  Reserve Calculation Method Using The External Calculation
*  Module SUM.............
*
*
* PROCESSING.
* ----------
*
* Call the first external module supplied by software house
* 'Berata'. This returns the required KOMPONENTE and their
* relationships with each other for a given Baustein (Coverage).
*
* Format the call to SUM.
* Loop round the KOMPONENTE retreived by calling the first module
* and complete the portion of the copy-book relating to that
* Komponente.
*
* Having filled in the copy-book for all KOMPONENTE, do the same
* for the ZUSTANDE relating to the Baustein. Note that there
* may be up to 15 ZUSTANDE per KOMPONENTE but there are
* only 15 ZUSTANDE occurances per BAUSTEIN on the copy-book.
*
* The SUM module is then called and status checking made on
* return....... error messages must be output if
* appropriate and passed back to the parent program.
*
* The reserve returned by SUM is then returned to the calling
* program.........
*
*****************************************************************
* </pre>
*/
public class Sumres01 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "SUMRES1";

	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaStatuzD = new FixedLengthStringData(1).isAPartOf(wsaaStatuz, 0).init("D");
	private ZonedDecimalData wsaaStatuzLast3 = new ZonedDecimalData(3, 0).isAPartOf(wsaaStatuz, 1).setUnsigned();
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5600Item = new FixedLengthStringData(8);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaZustandSub = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaT5601Sub = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaExclSub = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaSub3 = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaSub3X = new FixedLengthStringData(3).isAPartOf(wsaaSub3, 0, REDEFINE);
	private FixedLengthStringData wsaaSub3Lastx = new FixedLengthStringData(1).isAPartOf(wsaaSub3X, 2);
	private ZonedDecimalData wsaaBillfreq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaFreqann = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaRegbenSuminStore = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaT5601Item = new FixedLengthStringData(8);
	private PackedDecimalData wsaaSlotsUsedSub = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaSlotsUsed = new FixedLengthStringData(50);
	private ZonedDecimalData[] wsaaSlotUsed = ZDArrayPartOfStructure(25, 2, 0, wsaaSlotsUsed, 0, UNSIGNED_TRUE);
	private String wsaaExcludeLoading = "";
	private ZonedDecimalData wsaaWholeYears = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaIntDate9 = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaIntDate2 = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaIntDate2Yyyy = new ZonedDecimalData(4, 0).isAPartOf(wsaaIntDate2, 0).setUnsigned();

	private FixedLengthStringData wsaaJulianYyddd = new FixedLengthStringData(5);
	private ZonedDecimalData wsaaJulianYy = new ZonedDecimalData(2, 0).isAPartOf(wsaaJulianYyddd, 0).setUnsigned();
	private ZonedDecimalData wsaaJulianDdd = new ZonedDecimalData(3, 0).isAPartOf(wsaaJulianYyddd, 2).setUnsigned();

	private FixedLengthStringData wsaaDateYyyymmdd = new FixedLengthStringData(8);
	private ZonedDecimalData filler2 = new ZonedDecimalData(2, 0).isAPartOf(wsaaDateYyyymmdd, 0, FILLER).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaDateYymmdd = new FixedLengthStringData(6).isAPartOf(wsaaDateYyyymmdd, 2);
	private ZonedDecimalData wsaaDateYy = new ZonedDecimalData(2, 0).isAPartOf(wsaaDateYymmdd, 0).setUnsigned();
	private ZonedDecimalData wsaaDateMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaDateYymmdd, 2).setUnsigned();
	private FixedLengthStringData wsaaDtc5From = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaDtc5Frfmt = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaDtc5Tofmt = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaDtc5To = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaDtc5Origin = new FixedLengthStringData(4).init("PROG");
	private String d003 = "D003";
	private String d004 = "D004";
		/* TABLES */
	private String t5600 = "T5600";
	private String t5601 = "T5601";
	private String t5602 = "T5602";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String povrgenrec = "POVRGENREC";
	private Clvd3ba clvd3ba = new Clvd3ba();
	private Clvd3ek clvd3ek = new Clvd3ek();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*General LF on Prem.Breakdown file.*/
	private PovrgenTableDAM povrgenIO = new PovrgenTableDAM();
	private Sumcalcrec sumcalcrec = new Sumcalcrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5600rec t5600rec = new T5600rec();
	private T5601rec t5601rec = new T5601rec();
	private T5602rec t5602rec = new T5602rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		exit2290, 
		continue3720, 
		exit9020
	}

	public Sumres01() {
		super();
	}

public void mainline(Object... parmArray)
	{
		sumcalcrec.surrenderRec = convertAndSetParam(sumcalcrec.surrenderRec, parmArray, 0);
		try {
			main1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main1000()
	{
		try {
			start1010();
		}
		catch (GOTOException e){
		}
		finally{
			exit1090();
		}
	}

protected void start1010()
	{
		sumcalcrec.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		initialisation100();
		callPlvd3ek200();
		if (isNE(clvd3ek.lvdekFehler,ZERO)) {
			wsaaStatuzLast3.set(clvd3ek.lvdekFehler);
			checkBErrors3000();
			sumcalcrec.status.set(wsaaStatuz);
			goTo(GotoLabel.exit1090);
		}
		if (isLTE(sumcalcrec.calcPrem,ZERO)) {
			checkPovrgen2200();
		}
		if (isGT(sumcalcrec.singp,ZERO)
		&& isNE(sumcalcrec.function,"XPLUS")) {
			compute(sumcalcrec.calcPrem, 2).set(add(sumcalcrec.calcPrem,sumcalcrec.singp));
		}
		formatSumCall300();
		if (isEQ(clvd3ba.lvdbaFehler,LOVALUES)) {
			clvd3ba.lvdbaFehler.set(ZERO);
		}
		else {
			if (isNE(clvd3ba.lvdbaFehler,ZERO)) {
				wsaaStatuzLast3.set(clvd3ba.lvdbaFehler);
				checkBErrors3000();
				sumcalcrec.status.set(wsaaStatuz);
				goTo(GotoLabel.exit1090);
			}
		}
		checkNulls3200();
		sumcalcrec.actualVal.set(clvd3ba.lvdbaDk);
		if (isNE(sumcalcrec.freqann,SPACES)
		&& isNE(sumcalcrec.freqann,"00")) {
			compute(sumcalcrec.sumin, 2).set(div(sumcalcrec.sumin,wsaaFreqann));
		}
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void initialisation100()
	{
		/*START*/
		clvd3ba.record.set(LOVALUES);
		checkT56022100();
		wsaaCrtable.set(t5602rec.excluscd);
		if (isNE(sumcalcrec.freqann,SPACES)
		&& isNE(sumcalcrec.freqann,"00")) {
			wsaaFreqann.set(sumcalcrec.freqann);
			wsaaRegbenSuminStore.set(sumcalcrec.sumin);
			compute(sumcalcrec.sumin, 2).set(mult(wsaaRegbenSuminStore,wsaaFreqann));
		}
		else {
			wsaaFreqann.set(ZERO);
		}
		/*EXIT*/
	}

protected void callPlvd3ek200()
	{
		/*START*/
		clvd3ek.record.set(SPACES);
		clvd3ek.lvdekTabEnde.set(ZERO);
		clvd3ek.lvdekFehler.set(ZERO);
		clvd3ek.lvdekBaustein.set(wsaaCrtable);
		/*EXIT*/
	}

protected void formatSumCall300()
	{
		start310();
	}

protected void start310()
	{
		clvd3ba.lvdbaBaustein.set(wsaaCrtable);
		clvd3ba.lvdbaEbBaustein.set(wsaaCrtable);
		clvd3ba.lvdbaEbBaubeg.set(ZERO);
		clvd3ba.lvdbaEbRealzustbeg.set(ZERO);
		clvd3ba.lvdbaBaulfdnr.set(1);
		clvd3ba.lvdbaEbBaulfdnr.set(1);
		clvd3ba.lvdbaEbRealzust.set("1");
		getDuration3700();
		for (wsaaSub.set(1); !(isGT(wsaaSub,15)
		|| isGT(wsaaSub,clvd3ek.lvdekTabEnde)); wsaaSub.add(1)){
			if (isEQ(clvd3ek.lvdekKomptyp[wsaaSub.toInt()],"B")) {
				clvd3ba.lvdbaEbkKoausschlussAussen[wsaaSub.toInt()].set(SPACES);
			}
			else {
				wsaaT5601Item.set(clvd3ek.lvdekKomponente[wsaaSub.toInt()]);
				checkT56013400();
			}
			clvd3ba.lvdbaEbkBaustein[wsaaSub.toInt()].set(wsaaCrtable);
			clvd3ba.lvdbaEbkBaulfdnr[wsaaSub.toInt()].set(1);
			clvd3ba.lvdbaEbkKomponente[wsaaSub.toInt()].set(clvd3ek.lvdekKomponente[wsaaSub.toInt()]);
			if (isEQ(clvd3ek.lvdekKomptyp[wsaaSub.toInt()],"L")) {
				clvd3ba.lvdbaEbkGrundbetrag[wsaaSub.toInt()].set(sumcalcrec.sumin);
			}
			if (isEQ(clvd3ek.lvdekKomptyp[wsaaSub.toInt()],"B")) {
				clvd3ba.lvdbaEbkGrundbetrag[wsaaSub.toInt()].set(sumcalcrec.calcPrem);
			}
		}
		wsaaZustandSub.set(1);
		for (wsaaSub.set(1); !(isGT(wsaaSub,15)
		|| isGT(wsaaSub,clvd3ek.lvdekTabEnde)); wsaaSub.add(1)){
			if (isEQ(clvd3ek.lvdekKomptyp[wsaaSub.toInt()],"K")) {
				wsaaT5600Item.set(clvd3ek.lvdekBezkomp[wsaaSub.toInt()]);
			}
			else {
				wsaaT5600Item.set(clvd3ek.lvdekKomponente[wsaaSub.toInt()]);
			}
			checkT56002000();
			if (isNE(itemIO.getStatuz(),varcom.oK)
			&& isEQ(clvd3ek.lvdekKomptyp[wsaaSub.toInt()],"L")) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(d003);
				fatalError9000();
			}
			if (isEQ(clvd3ek.lvdekKomptyp[wsaaSub.toInt()],"B")
			|| isEQ(clvd3ek.lvdekKomptyp[wsaaSub.toInt()],"K")) {
				clvd3ba.lvdbaEbkzBaustein[wsaaZustandSub.toInt()].set(clvd3ek.lvdekBaustein);
				clvd3ba.lvdbaEbkzKomponente[wsaaZustandSub.toInt()].set(clvd3ek.lvdekKomponente[wsaaSub.toInt()]);
				clvd3ba.lvdbaEbkzOblrechles[wsaaZustandSub.toInt()].set("R");
				clvd3ba.lvdbaEbkzZustand[wsaaZustandSub.toInt()].set("1");
				clvd3ba.lvdbaEbkzBaulfdnr[wsaaZustandSub.toInt()].set(1);
				if (isEQ(clvd3ek.lvdekKomptyp[wsaaSub.toInt()],"B")) {
					clvd3ba.lvdbaEbkzDauer[wsaaZustandSub.toInt()].set(sumcalcrec.duration);
				}
				wsaaZustandSub.add(1);
			}
			else {
				for (wsaaSub3.set(1); !(isGT(wsaaSub3,15)
				|| (isEQ(t5600rec.ind[wsaaSub3.toInt()],SPACES)
				&& isEQ(t5600rec.indic[wsaaSub3.toInt()],SPACES))); wsaaSub3.add(1)){
					clvd3ba.lvdbaEbkzBaustein[wsaaZustandSub.toInt()].set(clvd3ek.lvdekBaustein);
					clvd3ba.lvdbaEbkzKomponente[wsaaZustandSub.toInt()].set(clvd3ek.lvdekKomponente[wsaaSub.toInt()]);
					clvd3ba.lvdbaEbkzZustand[wsaaZustandSub.toInt()].set(wsaaSub3Lastx);
					clvd3ba.lvdbaEbkzOblrechles[wsaaZustandSub.toInt()].set("R");
					clvd3ba.lvdbaEbkzBaulfdnr[wsaaZustandSub.toInt()].set(1);
					if (isNE(t5600rec.indic[wsaaSub3.toInt()],SPACES)) {
						clvd3ba.lvdbaEbkzDauer[wsaaZustandSub.toInt()].set(sumcalcrec.benCessTerm);
					}
					else {
						clvd3ba.lvdbaEbkzDauer[wsaaZustandSub.toInt()].set(sumcalcrec.riskCessTerm);
					}
					wsaaZustandSub.add(1);
				}
			}
		}
		clvd3ba.lvdbaEbvpBart[1].set("VP01");
		clvd3ba.lvdbaEbvpTeinalt[1].set(sumcalcrec.lage);
		clvd3ba.lvdbaEbvpReinalt[1].set(sumcalcrec.lage);
		clvd3ba.lvdbaEbvpBaustein[1].set(wsaaCrtable);
		clvd3ba.lvdbaEbvpKalkperson[1].set("A");
		clvd3ba.lvdbaEbvpBaulfdnr[1].set(1);
		if (isEQ(sumcalcrec.lsex,"F")) {
			clvd3ba.lvdbaEbvpPerseigkombi[1].set("W");
		}
		else {
			clvd3ba.lvdbaEbvpPerseigkombi[1].set("M");
		}
		if (isNE(sumcalcrec.jlage,ZERO)
		&& isNE(sumcalcrec.jlage,999)) {
			clvd3ba.lvdbaEbvpKalkperson[2].set("A");
			clvd3ba.lvdbaEbvpBart[2].set("VP01");
			clvd3ba.lvdbaEbvpTeinalt[2].set(sumcalcrec.jlage);
			clvd3ba.lvdbaEbvpReinalt[2].set(sumcalcrec.jlage);
			clvd3ba.lvdbaEbvpBaustein[2].set(wsaaCrtable);
			clvd3ba.lvdbaEbvpBaulfdnr[2].set(1);
			if (isEQ(sumcalcrec.jlsex,"F")) {
				clvd3ba.lvdbaEbvpPerseigkombi[2].set("W");
			}
			else {
				clvd3ba.lvdbaEbvpPerseigkombi[2].set("M");
			}
		}
		clvd3ba.lvdbaLeseart.set("S");
	}

protected void checkT56002000()
	{
		start2010();
	}

protected void start2010()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(sumcalcrec.chdrChdrcoy);
		itemIO.setItemtabl(t5600);
		itemIO.setItemitem(wsaaT5600Item);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		else {
			if (isEQ(itemIO.getStatuz(),varcom.oK)) {
				t5600rec.t5600Rec.set(itemIO.getGenarea());
			}
		}
	}

protected void checkT56022100()
	{
		start2110();
	}

protected void start2110()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(sumcalcrec.chdrChdrcoy);
		itemIO.setItemtabl(t5602);
		itemIO.setItemitem(sumcalcrec.crtable);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			t5602rec.t5602Rec.set(itemIO.getGenarea());
		}
		else {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
	}

protected void checkPovrgen2200()
	{
		try {
			start2210();
		}
		catch (GOTOException e){
		}
	}

protected void start2210()
	{
		povrgenIO.setParams(SPACES);
		povrgenIO.setChdrcoy(sumcalcrec.chdrChdrcoy);
		povrgenIO.setChdrnum(sumcalcrec.chdrChdrnum);
		povrgenIO.setLife(sumcalcrec.lifeLife);
		povrgenIO.setCoverage(sumcalcrec.covrCoverage);
		povrgenIO.setRider(sumcalcrec.covrRider);
		povrgenIO.setPlanSuffix(ZERO);
		povrgenIO.setFormat(povrgenrec);
		povrgenIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		povrgenIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		povrgenIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		while ( !(isEQ(povrgenIO.getStatuz(),varcom.endp))) {
			getPovrgen2300();
		}
		
		if (isEQ(sumcalcrec.function,"XPLUS")) {
			sumcalcrec.calcPrem.set(povrgenIO.getXplusone());
			goTo(GotoLabel.exit2290);
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			wsaaSlotUsed[wsaaSub.toInt()].set(ZERO);
		}
		for (wsaaSub.set(2); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			compute(sumcalcrec.calcPrem, 2).set(add(sumcalcrec.calcPrem,povrgenIO.getAnnamnt(wsaaSub)));
		}
		wsaaSlotsUsedSub.set(ZERO);
		for (wsaaSub.set(1); !(isGT(wsaaSub,15)
		|| isGT(wsaaSub,clvd3ek.lvdekTabEnde)); wsaaSub.add(1)){
			if (isNE(clvd3ek.lvdekKomptyp[wsaaSub.toInt()],"B")) {
				wsaaT5601Item.set(clvd3ek.lvdekKomponente[wsaaSub.toInt()]);
				checkT56013400();
			}
		}
	}

protected void getPovrgen2300()
	{
		start2310();
	}

protected void start2310()
	{
		SmartFileCode.execute(appVars, povrgenIO);
		if (isNE(povrgenIO.getStatuz(),varcom.oK)
		&& isNE(povrgenIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(povrgenIO.getParams());
			syserrrec.statuz.set(povrgenIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(povrgenIO.getStatuz(),varcom.endp)
		|| isNE(povrgenIO.getChdrcoy(),sumcalcrec.chdrChdrcoy)
		|| isNE(povrgenIO.getChdrnum(),sumcalcrec.chdrChdrnum)
		|| isNE(povrgenIO.getLife(),sumcalcrec.lifeLife)
		|| isNE(povrgenIO.getCoverage(),sumcalcrec.covrCoverage)
		|| isNE(povrgenIO.getRider(),sumcalcrec.covrRider)) {
			syserrrec.params.set(povrgenIO.getParams());
			syserrrec.statuz.set(povrgenIO.getStatuz());
			fatalError9000();
		}
		if ((isEQ(sumcalcrec.function,"MALT")
		&& isEQ(povrgenIO.getValidflag(),"2"))
		|| (isNE(sumcalcrec.function,"MALT")
		&& isEQ(povrgenIO.getValidflag(),"1"))) {
			povrgenIO.setStatuz(varcom.endp);
		}
		else {
			povrgenIO.setFunction(varcom.nextr);
		}
	}

protected void checkBErrors3000()
	{
		/*START*/
		if ((isGTE(wsaaStatuzLast3,211)
		&& isLTE(wsaaStatuzLast3,228))
		|| isEQ(wsaaStatuzLast3,350)) {
			wsaaStatuzLast3.set(350);
		}
		if ((isGTE(wsaaStatuzLast3,231)
		&& isLTE(wsaaStatuzLast3,249))
		|| isEQ(wsaaStatuzLast3,351)) {
			wsaaStatuzLast3.set(351);
		}
		if (isGTE(wsaaStatuzLast3,291)
		&& isLTE(wsaaStatuzLast3,309)) {
			wsaaStatuzLast3.set(309);
		}
		/*EXIT*/
	}

protected void checkNulls3200()
	{
		start3210();
	}

protected void start3210()
	{
		if (isEQ(clvd3ba.lvdbaVnr,LOVALUES)) {
			clvd3ba.lvdbaVnr.set(ZERO);
		}
		if (isEQ(clvd3ba.lvdbaBaulfdnr,LOVALUES)) {
			clvd3ba.lvdbaBaulfdnr.set(ZERO);
		}
		if (isEQ(clvd3ba.lvdbaKalkzpkt,LOVALUES)) {
			clvd3ba.lvdbaKalkzpkt.set(ZERO);
		}
		if (isEQ(clvd3ba.lvdbaEbVnr,LOVALUES)) {
			clvd3ba.lvdbaEbVnr.set(ZERO);
		}
		if (isEQ(clvd3ba.lvdbaEbBaulfdnr,LOVALUES)) {
			clvd3ba.lvdbaEbBaulfdnr.set(ZERO);
		}
		if (isEQ(clvd3ba.lvdbaEbBaubeg,LOVALUES)) {
			clvd3ba.lvdbaEbBaubeg.set(ZERO);
		}
		if (isEQ(clvd3ba.lvdbaEbRealzustbeg,LOVALUES)) {
			clvd3ba.lvdbaEbRealzustbeg.set(ZERO);
		}
		if (isEQ(clvd3ba.lvdbaFehler,LOVALUES)) {
			clvd3ba.lvdbaFehler.set(ZERO);
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub,15)); wsaaSub.add(1)){
			if (isEQ(clvd3ba.lvdbaEbkVnr[wsaaSub.toInt()],LOVALUES)) {
				clvd3ba.lvdbaEbkVnr[wsaaSub.toInt()].set(ZERO);
			}
			if (isEQ(clvd3ba.lvdbaEbkBaulfdnr[wsaaSub.toInt()],LOVALUES)) {
				clvd3ba.lvdbaEbkBaulfdnr[wsaaSub.toInt()].set(ZERO);
			}
			if (isEQ(clvd3ba.lvdbaEbkzVnr[wsaaSub.toInt()],LOVALUES)) {
				clvd3ba.lvdbaEbkzVnr[wsaaSub.toInt()].set(ZERO);
			}
			if (isEQ(clvd3ba.lvdbaEbkzBaulfdnr[wsaaSub.toInt()],LOVALUES)) {
				clvd3ba.lvdbaEbkzBaulfdnr[wsaaSub.toInt()].set(ZERO);
			}
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub,3)); wsaaSub.add(1)){
			if (isEQ(clvd3ba.lvdbaEbvpVnr[wsaaSub.toInt()],LOVALUES)) {
				clvd3ba.lvdbaEbvpVnr[wsaaSub.toInt()].set(ZERO);
			}
			if (isEQ(clvd3ba.lvdbaEbvpBaulfdnr[wsaaSub.toInt()],LOVALUES)) {
				clvd3ba.lvdbaEbvpBaulfdnr[wsaaSub.toInt()].set(ZERO);
			}
		}
	}

protected void checkT56013400()
	{
		start3410();
	}

protected void start3410()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5601);
		itemIO.setItemcoy(sumcalcrec.chdrChdrcoy);
		itemIO.setItemitem(wsaaT5601Item);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			t5601rec.t5601Rec.set(itemIO.getGenarea());
		}
		else {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(d004);
			fatalError9000();
		}
	}

protected void checkExclusion3500()
	{
		/*START*/
		wsaaExcludeLoading = "Y";
		for (wsaaExclSub.set(1); !(isGT(wsaaExclSub,25)); wsaaExclSub.add(1)){
			if (isEQ(wsaaT5601Sub,wsaaSlotUsed[wsaaExclSub.toInt()])) {
				wsaaExcludeLoading = "N";
			}
		}
		if (isEQ(wsaaExcludeLoading,"Y")) {
			wsaaSlotsUsedSub.add(1);
			wsaaSlotUsed[wsaaSlotsUsedSub.toInt()].set(wsaaT5601Sub);
			compute(sumcalcrec.calcPrem, 2).set(sub(sumcalcrec.calcPrem,povrgenIO.getAnnamnt(wsaaT5601Sub)));
		}
		/*EXIT*/
	}

protected void getDuration3700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3710();
				}
				case continue3720: {
					continue3720();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3710()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.function.set("CMDF");
		datcon3rec.intDate1.set(sumcalcrec.crrcd);
		datcon3rec.intDate2.set(sumcalcrec.effdate);
		datcon3rec.frequency.set("01");
		datcon3rec.freqFactor.set(ZERO);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isEQ(datcon3rec.statuz,varcom.oK)) {
			wsaaWholeYears.set(datcon3rec.freqFactor);
			wsaaJulianYy.set(datcon3rec.freqFactor);
		}
		else {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError9000();
		}
		wsaaIntDate9.set(sumcalcrec.effdate);
		wsaaIntDate2.set(wsaaIntDate9);
		compute(wsaaIntDate2Yyyy, 0).set((sub(wsaaIntDate2Yyyy,wsaaWholeYears)));
		datcon3rec.function.set(SPACES);
		datcon3rec.intDate2.set(wsaaIntDate2);
		datcon3rec.frequency.set("DY");
		datcon3rec.freqFactor.set(ZERO);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isEQ(datcon3rec.statuz,varcom.oK)) {
			wsaaJulianDdd.set(datcon3rec.freqFactor);
		}
		else {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError9000();
		}
		if (isEQ(wsaaJulianDdd,ZERO)) {
			wsaaDateYyyymmdd.fill("0");
			wsaaDateYy.set(wsaaWholeYears);
			goTo(GotoLabel.continue3720);
		}
		wsaaDtc5From.set(wsaaJulianYyddd);
		wsaaDtc5Frfmt.set("*JUL");
		wsaaDtc5Tofmt.set("*YMD");
		wsaaDtc5To.set(SPACES);
		FsuCLFunctions.datcon5(wsaaDtc5Origin, wsaaDtc5From, wsaaDtc5Frfmt, wsaaDtc5Tofmt, wsaaDtc5To);
		wsaaDateYymmdd.set(wsaaDtc5To);
		wsaaDateMm.subtract(1);
	}

protected void continue3720()
	{
		clvd3ba.lvdbaKalkzpkt.set(wsaaDateYyyymmdd);
		/*EXIT*/
	}

protected void fatalError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9010();
				}
				case exit9020: {
					exit9020();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9010()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit9020);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		sumcalcrec.status.set(syserrrec.statuz);
		/*EXIT*/
		exitProgram();
	}
}
