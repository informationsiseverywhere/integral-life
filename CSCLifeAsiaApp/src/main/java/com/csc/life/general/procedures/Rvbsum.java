/*
 * File: Rvbsum.java
 * Date: 30 August 2009 2:14:16
 * Author: Quipoz Limited
 * 
 * Class transformed from RVBSUM.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.general.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.general.dataaccess.PovrgenTableDAM;
import com.csc.life.general.recordstructures.Sumcalcrec;
import com.csc.life.general.tablestructures.T5656rec;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.productdefinition.tablestructures.T5567rec;
import com.csc.life.regularprocessing.dataaccess.CovrbonTableDAM;
import com.csc.life.regularprocessing.recordstructures.Bonusrec;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  Reversionary Bonus Calculation subroutine using SUM.
*
* PROCESSING.
* ----------
*
* This program is called by the new program RVBSUM1 which in turn
* is a copy of the reversionary bonus subroutine RVBFLYR.
*
* This program keeps all SUM coding isolated within the one prog.
*
* PROCESSING
* ----------
* Read table T5656 and get the BONUS calculation parameters.
*
* Check the period of calculation against the wait period on
* T5656. If bonus is not applicable then zero the amount and
* return to the calling program.
*
* Check fields SUMBFAC01 through SUMBFAC06 on table T5656. These
* fields are multiplication factors for each of the six options
* available for the calculation of bonus. If a value is present
* then multiply this table value by the corresponding amount to
* get the bonus. e.g., The first value is multiplied by the
* calculated reserve as at the last anniversary.
* Note that the table multiplication factors are percentages.
*
* Test each SUMFAC in turn and summate the total of all six
* calculated values. If only one value is specified on the table
* then this will be the final bonus. If three values are speci-
* fied on the table then the final bonus returned to the calling
* program will be the sum of the three calculated values.
*
* Return to the calling program.
*
*****************************************************************
* </pre>
*/
public class Rvbsum extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaFreqann = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaT5606Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5606Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5606Item, 0);
	private FixedLengthStringData wsaaT5606Unknown = new FixedLengthStringData(1).isAPartOf(wsaaT5606Item, 4);
	private FixedLengthStringData wsaaT5606Currcode = new FixedLengthStringData(3).isAPartOf(wsaaT5606Item, 5);

	private FixedLengthStringData wsaaT5656Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5656Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5656Item, 0);
	private FixedLengthStringData wsaaT5656Pstatcode = new FixedLengthStringData(2).isAPartOf(wsaaT5656Item, 4);
	private FixedLengthStringData wsaaT5656Sex = new FixedLengthStringData(1).isAPartOf(wsaaT5656Item, 6);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaT5656Item, 7, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5567Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5567Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT5567Item, 0);
	private FixedLengthStringData wsaaT5567Cntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT5567Item, 3);
	private FixedLengthStringData filler1 = new FixedLengthStringData(2).isAPartOf(wsaaT5567Item, 6, FILLER).init(SPACES);
	private PackedDecimalData wsaaTotalBonus = new PackedDecimalData(18, 2);

	private FixedLengthStringData wsaaStoredAmounts = new FixedLengthStringData(114);
	private PackedDecimalData[] wsaaRawAmount = PDArrayPartOfStructure(6, 17, 2, wsaaStoredAmounts, 0);
	private PackedDecimalData[] wsaaBonusAmount = PDArrayPartOfStructure(6, 18, 2, wsaaStoredAmounts, 54);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaJlsex = new FixedLengthStringData(1);
	private PackedDecimalData wsaaAge = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaAgeprem = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaConvUnits = new PackedDecimalData(8, 0).init(ZERO);
	private FixedLengthStringData wsaaElement = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaDescription = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaType = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaFund = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaEndf = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaNeUnits = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaTmUnits = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaPsNotAllwd = new FixedLengthStringData(1).init(SPACES);
		/* ERRORS */
	private String h966 = "H966";
	private String g528 = "G528";
	private String d038 = "D038";
		/* TABLES */
	private String t5567 = "T5567";
	private String t5606 = "T5606";
	private String t5656 = "T5656";
	private String chdrenqrec = "CHDRENQREC";
	private String covrbonrec = "COVRBONREC";
	private String liferec = "LIFEREC";
	private String povrgenrec = "POVRGENREC";
	private Bonusrec bonusrec = new Bonusrec();
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Logical view on COVR file for Bonus Subr*/
	private CovrbonTableDAM covrbonIO = new CovrbonTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life record*/
	private LifeTableDAM lifeIO = new LifeTableDAM();
		/*General LF on Prem.Breakdown file.*/
	private PovrgenTableDAM povrgenIO = new PovrgenTableDAM();
	private Sumcalcrec sumcalcrec = new Sumcalcrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5567rec t5567rec = new T5567rec();
	private T5606rec t5606rec = new T5606rec();
	private T5656rec t5656rec = new T5656rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit190, 
		exit870, 
		exit970
	}

	public Rvbsum() {
		super();
	}

public void mainline(Object... parmArray)
	{
		bonusrec.bonusRec = convertAndSetParam(bonusrec.bonusRec, parmArray, 0);
		try {
			main100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main100()
	{
		try {
			start110();
		}
		catch (GOTOException e){
		}
		finally{
			exit190();
		}
	}

protected void start110()
	{
		initialisation1000();
		bonusrec.rvBonusSa.set(ZERO);
		bonusrec.rvBonusBon.set(ZERO);
		bonusrec.rvBalLy.set(ZERO);
		if (isLT(bonusrec.termInForce,t5656rec.termMin)) {
			goTo(GotoLabel.exit190);
		}
		if (isGT(t5656rec.sumbfac01,ZERO)
		|| isGT(t5656rec.sumbfac02,ZERO)
		|| isGT(t5656rec.sumbfac03,ZERO)) {
			completeCopybook3000();
		}
		if (isGT(t5656rec.sumbfac01,ZERO)) {
			reserveMinusone2100();
		}
		else {
			wsaaRawAmount[1].set(ZERO);
		}
		if (isGT(t5656rec.sumbfac02,ZERO)) {
			reserveToday2200();
		}
		else {
			wsaaRawAmount[2].set(ZERO);
		}
		if (isGT(t5656rec.sumbfac03,ZERO)) {
			reservePlusone2300();
		}
		else {
			wsaaRawAmount[3].set(ZERO);
		}
		if (isGT(t5656rec.sumbfac04,ZERO)) {
			netAnnprem2400();
		}
		else {
			wsaaRawAmount[4].set(ZERO);
		}
		if (isGT(t5656rec.sumbfac05,ZERO)) {
			annualisedFee2500();
		}
		else {
			wsaaRawAmount[5].set(ZERO);
		}
		if (isGT(t5656rec.sumbfac06,ZERO)) {
			sumAssured2600();
		}
		else {
			wsaaRawAmount[6].set(ZERO);
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub,6)); wsaaSub.add(1)){
			if (isGT(t5656rec.sumbfac[wsaaSub.toInt()],ZERO)
			&& isGT(wsaaRawAmount[wsaaSub.toInt()],ZERO)) {
				compute(wsaaBonusAmount[wsaaSub.toInt()], 4).setRounded((div(mult(wsaaRawAmount[wsaaSub.toInt()],t5656rec.sumbfac[wsaaSub.toInt()]),100)));
			}
			else {
				wsaaBonusAmount[wsaaSub.toInt()].set(ZERO);
			}
			compute(wsaaTotalBonus, 2).set((add(wsaaTotalBonus,wsaaBonusAmount[wsaaSub.toInt()])));
		}
		bonusrec.rvBonusSa.set(wsaaTotalBonus);
		bonusrec.statuz.set(varcom.oK);
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialisation1000()
	{
		start1010();
	}

protected void start1010()
	{
		wsaaTotalBonus.set(ZERO);
		for (wsaaSub.set(1); !(isGT(wsaaSub,6)); wsaaSub.add(1)){
			wsaaRawAmount[wsaaSub.toInt()].set(ZERO);
			wsaaBonusAmount[wsaaSub.toInt()].set(ZERO);
		}
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(bonusrec.chdrChdrcoy);
		chdrenqIO.setChdrnum(bonusrec.chdrChdrnum);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)
		&& isNE(chdrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			systemError900();
		}
		if (isEQ(chdrenqIO.getStatuz(),varcom.oK)) {
			while ( !((isEQ(chdrenqIO.getStatuz(),varcom.oK)
			&& isEQ(chdrenqIO.getChdrcoy(),bonusrec.chdrChdrcoy)
			&& isEQ(chdrenqIO.getChdrnum(),bonusrec.chdrChdrnum)
			&& isEQ(chdrenqIO.getValidflag(),"1"))
			|| isEQ(chdrenqIO.getStatuz(),varcom.endp))) {
				loopChdrenq3200();
			}
			
		}
		if (isEQ(chdrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			databaseError800();
		}
		covrbonIO.setParams(SPACES);
		covrbonIO.setChdrcoy(chdrenqIO.getChdrcoy());
		covrbonIO.setChdrnum(chdrenqIO.getChdrnum());
		covrbonIO.setLife(bonusrec.lifeLife);
		covrbonIO.setCoverage(bonusrec.covrCoverage);
		covrbonIO.setRider(bonusrec.covrRider);
		covrbonIO.setPlanSuffix(ZERO);
		covrbonIO.setFormat(covrbonrec);
		covrbonIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, covrbonIO);
		if (isNE(covrbonIO.getStatuz(),varcom.oK)
		&& isNE(covrbonIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrbonIO.getParams());
			syserrrec.statuz.set(covrbonIO.getStatuz());
			systemError900();
		}
		if (isEQ(covrbonIO.getStatuz(),varcom.oK)) {
			while ( !((isEQ(covrbonIO.getStatuz(),varcom.oK)
			&& isEQ(covrbonIO.getChdrcoy(),chdrenqIO.getChdrcoy())
			&& isEQ(covrbonIO.getChdrnum(),chdrenqIO.getChdrnum())
			&& isEQ(covrbonIO.getLife(),bonusrec.lifeLife)
			&& isEQ(covrbonIO.getCoverage(),bonusrec.covrCoverage)
			&& isEQ(covrbonIO.getRider(),bonusrec.covrRider)
			&& isEQ(covrbonIO.getValidflag(),"1"))
			|| isEQ(covrbonIO.getStatuz(),varcom.endp))) {
				loopCovrbon3300();
			}
			
		}
		if (isEQ(covrbonIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrbonIO.getParams());
			syserrrec.statuz.set(covrbonIO.getStatuz());
			databaseError800();
		}
		else {
			if (isGT(covrbonIO.getBenCessTerm(),ZERO)
			&& isLT(covrbonIO.getBenCessTerm(),120)) {
				getBenfreq3100();
				wsaaFreqann.set(t5606rec.benfreq);
			}
			else {
				wsaaFreqann.set(ZERO);
			}
		}
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(bonusrec.chdrChdrcoy);
		lifeIO.setChdrnum(bonusrec.chdrChdrnum);
		lifeIO.setLife(covrbonIO.getLife());
		lifeIO.setJlife("00");
		lifeIO.setCurrfrom(varcom.maxdate);
		lifeIO.setFormat(liferec);
		lifeIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)
		&& isNE(lifeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			systemError900();
		}
		if (isEQ(lifeIO.getStatuz(),varcom.oK)
		&& isEQ(lifeIO.getChdrcoy(),bonusrec.chdrChdrcoy)
		&& isEQ(lifeIO.getChdrnum(),bonusrec.chdrChdrnum)
		&& isEQ(lifeIO.getLife(),covrbonIO.getLife())
		&& isEQ(lifeIO.getJlife(),"00")) {
			wsaaSex.set(lifeIO.getCltsex());
			wsaaAge.set(lifeIO.getAnbAtCcd());
		}
		else {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			databaseError800();
		}
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeIO.setLife(covrbonIO.getLife());
		lifeIO.setJlife("01");
		lifeIO.setCurrfrom(varcom.maxdate);
		lifeIO.setFormat(liferec);
		lifeIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)
		&& isNE(lifeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			systemError900();
		}
		if (isEQ(lifeIO.getStatuz(),varcom.oK)
		&& isEQ(lifeIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		&& isEQ(lifeIO.getChdrnum(),chdrenqIO.getChdrnum())
		&& isEQ(lifeIO.getLife(),covrbonIO.getLife())
		&& isEQ(lifeIO.getJlife(),"01")) {
			wsaaJlsex.set(lifeIO.getCltsex());
			wsaaAgeprem.set(lifeIO.getAnbAtCcd());
		}
		else {
			wsaaJlsex.set(SPACES);
			wsaaAgeprem.set(ZERO);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemtabl(t5656);
		itdmIO.setItemcoy(bonusrec.chdrChdrcoy);
		wsaaT5656Crtable.set(bonusrec.crtable);
		wsaaT5656Pstatcode.set(bonusrec.premStatus);
		wsaaT5656Sex.set(wsaaSex);
		itdmIO.setItemitem(wsaaT5656Item);
		itdmIO.setItmfrm(bonusrec.effectiveDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError900();
		}
		else {
			if (isEQ(itdmIO.getStatuz(),varcom.endp)
			|| isNE(itdmIO.getItemtabl(),t5656)
			|| isNE(itdmIO.getItemcoy(),bonusrec.chdrChdrcoy)
			|| isNE(itdmIO.getItemitem(),wsaaT5656Item)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(d038);
				databaseError800();
			}
			else {
				t5656rec.t5656Rec.set(itdmIO.getGenarea());
			}
		}
	}

protected void reserveMinusone2100()
	{
		start2110();
	}

protected void start2110()
	{
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(bonusrec.effectiveDate);
		datcon2rec.intDate2.set(ZERO);
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			systemError900();
		}
		else {
			sumcalcrec.effdate.set(datcon2rec.intDate2);
		}
		callProgram(Sumres01.class, sumcalcrec.surrenderRec);
		if (isNE(sumcalcrec.status,"****")
		&& isNE(sumcalcrec.status,SPACES)) {
			syserrrec.statuz.set(sumcalcrec.status);
			systemError900();
		}
		else {
			wsaaRawAmount[1].set(sumcalcrec.actualVal);
		}
	}

protected void reserveToday2200()
	{
		/*START*/
		callProgram(Sumres01.class, sumcalcrec.surrenderRec);
		if (isNE(sumcalcrec.status,"****")
		&& isNE(sumcalcrec.status,SPACES)) {
			syserrrec.statuz.set(sumcalcrec.status);
			systemError900();
		}
		else {
			wsaaRawAmount[2].set(sumcalcrec.actualVal);
		}
		/*EXIT*/
	}

protected void reservePlusone2300()
	{
		start2310();
	}

protected void start2310()
	{
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(bonusrec.effectiveDate);
		datcon2rec.intDate2.set(ZERO);
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			systemError900();
		}
		else {
			sumcalcrec.effdate.set(datcon2rec.intDate2);
		}
		callProgram(Sumres01.class, sumcalcrec.surrenderRec);
		if (isNE(sumcalcrec.status,"****")
		&& isNE(sumcalcrec.status,SPACES)) {
			syserrrec.statuz.set(sumcalcrec.status);
			systemError900();
		}
		else {
			wsaaRawAmount[3].set(sumcalcrec.actualVal);
		}
	}

protected void netAnnprem2400()
	{
		start2410();
	}

protected void start2410()
	{
		povrgenIO.setParams(SPACES);
		povrgenIO.setChdrcoy(chdrenqIO.getChdrcoy());
		povrgenIO.setChdrnum(chdrenqIO.getChdrnum());
		povrgenIO.setLife(covrbonIO.getLife());
		povrgenIO.setCoverage(covrbonIO.getCoverage());
		povrgenIO.setRider(covrbonIO.getRider());
		povrgenIO.setPlanSuffix(ZERO);
		povrgenIO.setFormat(povrgenrec);
		povrgenIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		povrgenIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		povrgenIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		while ( !(isEQ(povrgenIO.getStatuz(),varcom.endp))) {
			getPovrgen3400();
		}
		
		wsaaRawAmount[4].set(ZERO);
		for (wsaaSub.set(2); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			compute(wsaaRawAmount[4], 2).set((add(wsaaRawAmount[4],povrgenIO.getAnnamnt(wsaaSub))));
		}
	}

protected void annualisedFee2500()
	{
		start2510();
	}

protected void start2510()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemtabl(t5567);
		itdmIO.setItemcoy(chdrenqIO.getChdrcoy());
		wsaaT5567Cnttype.set(chdrenqIO.getCnttype());
		wsaaT5567Cntcurr.set(chdrenqIO.getCntcurr());
		itdmIO.setItemitem(wsaaT5567Item);
		itdmIO.setItmfrm(bonusrec.effectiveDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError900();
		}
		else {
			if (isEQ(itdmIO.getStatuz(),varcom.endp)
			|| isNE(itdmIO.getItemtabl(),t5567)
			|| isNE(itdmIO.getItemcoy(),chdrenqIO.getChdrcoy())
			|| isNE(itdmIO.getItemitem(),wsaaT5567Item)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(h966);
				databaseError800();
			}
			else {
				t5567rec.t5567Rec.set(itdmIO.getGenarea());
			}
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub,10)); wsaaSub.add(1)){
			if (isEQ(t5567rec.billfreq[wsaaSub.toInt()],"01")) {
				wsaaRawAmount[5].set(t5567rec.cntfee[wsaaSub.toInt()]);
				wsaaSub.set(99);
			}
		}
		if (isLTE(wsaaSub,12)) {
			wsaaRawAmount[5].set(ZERO);
		}
	}

protected void sumAssured2600()
	{
		/*START*/
		if (isGT(wsaaFreqann,ZERO)) {
			compute(wsaaRawAmount[6], 2).set((mult(covrbonIO.getSumins(),wsaaFreqann)));
		}
		else {
			wsaaRawAmount[6].set(covrbonIO.getSumins());
		}
		/*EXIT*/
	}

protected void completeCopybook3000()
	{
		start3010();
	}

protected void start3010()
	{
		sumcalcrec.calcPrem.set(ZERO);
		sumcalcrec.tsvtot.set(ZERO);
		sumcalcrec.tsv1tot.set(ZERO);
		sumcalcrec.chdrChdrcoy.set(chdrenqIO.getChdrcoy());
		sumcalcrec.chdrChdrnum.set(chdrenqIO.getChdrnum());
		sumcalcrec.planSuffix.set(covrbonIO.getPlanSuffix());
		sumcalcrec.polsum.set(chdrenqIO.getPolsum());
		sumcalcrec.lifeLife.set(covrbonIO.getLife());
		sumcalcrec.lifeJlife.set(covrbonIO.getJlife());
		sumcalcrec.covrCoverage.set(covrbonIO.getCoverage());
		sumcalcrec.covrRider.set(covrbonIO.getRider());
		sumcalcrec.crtable.set(covrbonIO.getCrtable());
		sumcalcrec.crrcd.set(covrbonIO.getCrrcd());
		sumcalcrec.ptdate.set(chdrenqIO.getPtdate());
		sumcalcrec.effdate.set(bonusrec.effectiveDate);
		sumcalcrec.convUnits.set(wsaaConvUnits);
		sumcalcrec.language.set(bonusrec.language);
		sumcalcrec.estimatedVal.set(ZERO);
		sumcalcrec.actualVal.set(ZERO);
		sumcalcrec.currcode.set(chdrenqIO.getCntcurr());
		sumcalcrec.chdrCurr.set(chdrenqIO.getCntcurr());
		sumcalcrec.pstatcode.set(covrbonIO.getPstatcode());
		sumcalcrec.element.set(wsaaElement);
		sumcalcrec.description.set(wsaaDescription);
		sumcalcrec.billfreq.set(chdrenqIO.getBillfreq());
		sumcalcrec.type.set(wsaaType);
		sumcalcrec.fund.set(wsaaFund);
		sumcalcrec.status.set(varcom.oK);
		sumcalcrec.endf.set(wsaaEndf);
		sumcalcrec.neUnits.set(wsaaNeUnits);
		sumcalcrec.tmUnits.set(wsaaTmUnits);
		sumcalcrec.psNotAllwd.set(wsaaPsNotAllwd);
		sumcalcrec.trancde.set(bonusrec.transcd);
		sumcalcrec.sumin.set(covrbonIO.getSumins());
		sumcalcrec.lage.set(wsaaAge);
		sumcalcrec.jlage.set(wsaaAgeprem);
		sumcalcrec.lsex.set(wsaaSex);
		sumcalcrec.jlsex.set(wsaaJlsex);
		sumcalcrec.duration.set(covrbonIO.getPremCessTerm());
		sumcalcrec.riskCessTerm.set(covrbonIO.getRiskCessTerm());
		sumcalcrec.benCessTerm.set(covrbonIO.getBenCessTerm());
		sumcalcrec.chdrtype.set(chdrenqIO.getCnttype());
		sumcalcrec.freqann.set(wsaaFreqann);
		if (isGT(covrbonIO.getSingp(),ZERO)) {
			sumcalcrec.singp.set(covrbonIO.getSingp());
		}
		else {
			sumcalcrec.singp.set(ZERO);
		}
	}

protected void getBenfreq3100()
	{
		start3110();
	}

protected void start3110()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemtabl(t5606);
		itdmIO.setItemcoy(chdrenqIO.getChdrcoy());
		wsaaT5606Crtable.set(covrbonIO.getCrtable());
		wsaaT5606Unknown.set("*");
		wsaaT5606Currcode.set(chdrenqIO.getCntcurr());
		itdmIO.setItemitem(wsaaT5606Item);
		itdmIO.setItmfrm(bonusrec.effectiveDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError900();
		}
		else {
			if (isEQ(itdmIO.getStatuz(),varcom.endp)
			|| isNE(itdmIO.getItemtabl(),t5606)
			|| isNE(itdmIO.getItemcoy(),bonusrec.chdrChdrcoy)
			|| isNE(itdmIO.getItemitem(),wsaaT5606Item)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(g528);
				databaseError800();
			}
			else {
				t5606rec.t5606Rec.set(itdmIO.getGenarea());
			}
		}
	}

protected void loopChdrenq3200()
	{
		/*START*/
		chdrenqIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)
		&& isNE(chdrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			systemError900();
		}
		if (isEQ(chdrenqIO.getStatuz(),varcom.endp)
		|| isNE(chdrenqIO.getChdrcoy(),bonusrec.chdrChdrcoy)
		|| isNE(chdrenqIO.getChdrnum(),bonusrec.chdrChdrnum)) {
			chdrenqIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void loopCovrbon3300()
	{
		/*START*/
		covrbonIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrbonIO);
		if (isNE(covrbonIO.getStatuz(),varcom.oK)
		&& isNE(covrbonIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrbonIO.getParams());
			syserrrec.statuz.set(covrbonIO.getStatuz());
			systemError900();
		}
		if (isEQ(covrbonIO.getStatuz(),varcom.endp)
		|| isNE(covrbonIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(covrbonIO.getChdrnum(),chdrenqIO.getChdrnum())
		|| isNE(covrbonIO.getLife(),bonusrec.lifeLife)
		|| isNE(covrbonIO.getCoverage(),bonusrec.covrCoverage)
		|| isNE(covrbonIO.getRider(),bonusrec.covrRider)) {
			covrbonIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void getPovrgen3400()
	{
		start3410();
	}

protected void start3410()
	{
		SmartFileCode.execute(appVars, povrgenIO);
		if (isNE(povrgenIO.getStatuz(),varcom.oK)
		&& isNE(povrgenIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(povrgenIO.getParams());
			syserrrec.statuz.set(povrgenIO.getStatuz());
			systemError900();
		}
		if (isEQ(povrgenIO.getStatuz(),varcom.endp)
		|| isNE(povrgenIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(povrgenIO.getChdrnum(),chdrenqIO.getChdrnum())
		|| isNE(povrgenIO.getLife(),covrbonIO.getLife())
		|| isNE(povrgenIO.getCoverage(),covrbonIO.getCoverage())
		|| isNE(povrgenIO.getRider(),covrbonIO.getRider())) {
			syserrrec.params.set(povrgenIO.getParams());
			syserrrec.statuz.set(povrgenIO.getStatuz());
			databaseError800();
		}
		if (isEQ(povrgenIO.getValidflag(),"1")) {
			povrgenIO.setStatuz(varcom.endp);
		}
		else {
			povrgenIO.setFunction(varcom.nextr);
		}
	}

protected void databaseError800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start810();
				}
				case exit870: {
					exit870();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start810()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit870);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit870()
	{
		bonusrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void systemError900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start910();
				}
				case exit970: {
					exit970();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start910()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit970);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit970()
	{
		bonusrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
