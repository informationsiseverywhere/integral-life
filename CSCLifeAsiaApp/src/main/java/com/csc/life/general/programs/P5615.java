/*
 * File: P5615.java
 * Date: 30 August 2009 0:32:11
 * Author: Quipoz Limited
 * 
 * Class transformed from P5615.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.general.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceAll;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.contractservicing.tablestructures.T5541rec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.general.procedures.Sumres01;
import com.csc.life.general.recordstructures.Sumcalcrec;
import com.csc.life.general.screens.S5615ScreenVars;
import com.csc.life.general.tablestructures.T5602rec;
import com.csc.life.general.tablestructures.T5604rec;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*                  RESERVE CALCULATION
*
*   This screen/program will show the parameters required to
*   call the SUM reserve calculation routine SUMRES01.
*
*   It is a test rig for the routine which will become an
*   integrated part of the system.
*
*****************************************************************
* </pre>
*/
public class P5615 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5615");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaDateX = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaCentury = new ZonedDecimalData(2, 0).isAPartOf(wsaaDateX, 0).init(19).setUnsigned();
	private FixedLengthStringData wsaaDate = new FixedLengthStringData(6).isAPartOf(wsaaDateX, 2);
	private ZonedDecimalData wsaaYy = new ZonedDecimalData(2, 0).isAPartOf(wsaaDate, 0).setUnsigned();
	private ZonedDecimalData wsaaMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaDate, 2).setUnsigned();
	private ZonedDecimalData wsaaDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaDate, 4).setUnsigned();
	private PackedDecimalData wsaaS1 = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaS2 = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaBillfreq = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaFreqMultiple = new PackedDecimalData(5, 4);
	private FixedLengthStringData wsaaCoverageStore = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRiderStore = new FixedLengthStringData(2);

	private FixedLengthStringData wsbbTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTranscd = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 0);
	private FixedLengthStringData wsbbCrtable = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 4);

	private FixedLengthStringData wsbbTranCurrency = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTran = new FixedLengthStringData(5).isAPartOf(wsbbTranCurrency, 0);
	private FixedLengthStringData wsbbCurrency = new FixedLengthStringData(3).isAPartOf(wsbbTranCurrency, 5);

	private FixedLengthStringData wsaaChdrsel = new FixedLengthStringData(10);
	private FixedLengthStringData[] wsaaChdrchar = FLSArrayPartOfStructure(10, 1, wsaaChdrsel, 0);

	private FixedLengthStringData filler = new FixedLengthStringData(10).isAPartOf(wsaaChdrsel, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaChdrselLast8 = new FixedLengthStringData(8).isAPartOf(filler, 2);
		/* TABLES */
	private String t5602 = "T5602";
	private String t5604 = "T5604";
	private String t5606 = "T5606";
	private String t5541 = "T5541";
	private String t5671 = "T5671";
	private String itemrec = "ITEMREC";
	private String chdrenqrec = "CHDRENQREC";
	private String covrrec = "COVRREC";
	private String liferec = "LIFEREC";
		/* ERRORS */
	private String f200 = "F200";
	private String t075 = "T075";
	private String t076 = "T076";
	private String d020 = "D020";
	private String d024 = "D024";
	private String d027 = "D027";
	private String d034 = "D034";
	private String d035 = "D035";
	private String e348 = "E348";
	private String e355 = "E355";

	private FixedLengthStringData wsaaFixedParms = new FixedLengthStringData(49);
	private FixedLengthStringData wsaaChdrChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaFixedParms, 0);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(wsaaFixedParms, 1);
	private PackedDecimalData wsaaPolsum = new PackedDecimalData(4, 0).isAPartOf(wsaaFixedParms, 4);
	private PackedDecimalData wsaaConvUnits = new PackedDecimalData(8, 0).isAPartOf(wsaaFixedParms, 7);
	private FixedLengthStringData wsaaElement = new FixedLengthStringData(1).isAPartOf(wsaaFixedParms, 12);
	private FixedLengthStringData wsaaDescription = new FixedLengthStringData(30).isAPartOf(wsaaFixedParms, 13);
	private FixedLengthStringData wsaaType = new FixedLengthStringData(1).isAPartOf(wsaaFixedParms, 43);
	private FixedLengthStringData wsaaFund = new FixedLengthStringData(1).isAPartOf(wsaaFixedParms, 44);
	private FixedLengthStringData wsaaEndf = new FixedLengthStringData(1).isAPartOf(wsaaFixedParms, 45);
	private FixedLengthStringData wsaaNeUnits = new FixedLengthStringData(1).isAPartOf(wsaaFixedParms, 46);
	private FixedLengthStringData wsaaTmUnits = new FixedLengthStringData(1).isAPartOf(wsaaFixedParms, 47);
	private FixedLengthStringData wsaaPsNotAllwd = new FixedLengthStringData(1).isAPartOf(wsaaFixedParms, 48);
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life record*/
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private Sumcalcrec sumcalcrec = new Sumcalcrec();
	private T5541rec t5541rec = new T5541rec();
	private T5602rec t5602rec = new T5602rec();
	private T5604rec t5604rec = new T5604rec();
	private T5606rec t5606rec = new T5606rec();
	private T5671rec t5671rec = new T5671rec();
	private Batckey wsaaBatckey = new Batckey();
	private S5615ScreenVars sv = ScreenProgram.getScreenVars( S5615ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		chdrenqNotFound1020, 
		covrNotFound1030, 
		lifeNotFound1040, 
		jlifeNotFound1050, 
		continue1060, 
		preExit, 
		exit2090, 
		exit2290, 
		exit2490
	}

	public P5615() {
		super();
		screenVars = sv;
		new ScreenModel("S5615", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start1010();
				}
				case chdrenqNotFound1020: {
					chdrenqNotFound1020();
				}
				case covrNotFound1030: {
					covrNotFound1030();
				}
				case lifeNotFound1040: {
					lifeNotFound1040();
				}
				case jlifeNotFound1050: {
					jlifeNotFound1050();
				}
				case continue1060: {
					continue1060();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1010()
	{
		sv.dataArea.set(SPACES);
		wsaaBatckey.set(wsspcomn.batchkey);
		initialize(wsaaFixedParms);
		wsaaChdrChdrcoy.set("2");
		wsaaDate.set(getCobolDate());
		if (isGT(wsaaYy,40)) {
			wsaaCentury.set(19);
		}
		else {
			wsaaCentury.set(20);
		}
		sv.effdate.set(wsaaDateX);
		sv.language.set("D");
		sv.trancde.set("T761");
		sv.annamnt.set(ZERO);
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			sv.chdrselOut[varcom.nd.toInt()].set("Y");
			sv.currcdOut[varcom.nd.toInt()].set("Y");
			sv.coverageOut[varcom.nd.toInt()].set("Y");
			sv.coverageOut[varcom.pr.toInt()].set("Y");
			sv.riderOut[varcom.nd.toInt()].set("Y");
			sv.riderOut[varcom.pr.toInt()].set("Y");
			sv.lifeOut[varcom.nd.toInt()].set("Y");
			sv.jlifeOut[varcom.nd.toInt()].set("Y");
			sv.pstatcodeOut[varcom.nd.toInt()].set("Y");
			goTo(GotoLabel.chdrenqNotFound1020);
		}
		sv.chdrtypeOut[varcom.pr.toInt()].set("Y");
		sv.billfreqOut[varcom.pr.toInt()].set("Y");
		sv.suminsOut[varcom.pr.toInt()].set("Y");
		sv.crtableOut[varcom.pr.toInt()].set("Y");
		sv.crrcdOut[varcom.pr.toInt()].set("Y");
		sv.sexOut[varcom.pr.toInt()].set("Y");
		sv.ageOut[varcom.pr.toInt()].set("Y");
		sv.jlsexOut[varcom.pr.toInt()].set("Y");
		sv.agepremOut[varcom.pr.toInt()].set("Y");
		sv.annamntOut[varcom.nd.toInt()].set("Y");
		sv.annamntOut[varcom.pr.toInt()].set("Y");
		sv.chdrsel.set(chdrenqIO.getChdrnum());
		sv.chdrtype.set(chdrenqIO.getCnttype());
		sv.currcd.set(chdrenqIO.getCntcurr());
		sv.currcode.set(chdrenqIO.getCntcurr());
		sv.billfreq.set(chdrenqIO.getBillfreq());
		sv.ptdate.set(chdrenqIO.getPtdate());
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(chdrenqIO.getChdrcoy());
		covrIO.setChdrnum(chdrenqIO.getChdrnum());
		covrIO.setLife("01");
		covrIO.setPlanSuffix(ZERO);
		covrIO.setCoverage("01");
		covrIO.setRider("00");
		covrIO.setFormat(covrrec);
		covrIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, covrIO);
		if (isEQ(covrIO.getStatuz(),varcom.oK)
		&& isEQ(covrIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		&& isEQ(covrIO.getChdrnum(),chdrenqIO.getChdrnum())
		&& isEQ(covrIO.getLife(),"01")
		&& isEQ(covrIO.getCoverage(),"01")
		&& isEQ(covrIO.getRider(),"00")) {
			sv.coverage.set(covrIO.getCoverage());
			wsaaCoverageStore.set(covrIO.getCoverage());
			sv.rider.set(covrIO.getRider());
			wsaaRiderStore.set(covrIO.getRider());
			sv.crtable.set(covrIO.getCrtable());
			sv.crrcd.set(covrIO.getCrrcd());
			sv.pstatcode.set(covrIO.getPstatcode());
			sv.singp.set(covrIO.getSingp());
			sv.sumins.set(covrIO.getSumins());
			sv.premterm.set(covrIO.getPremCessTerm());
			sv.riskCessTerm.set(covrIO.getRiskCessTerm());
			sv.benterm.set(covrIO.getBenCessTerm());
			if (isGT(covrIO.getBenCessTerm(),ZERO)
			&& isLT(covrIO.getBenCessTerm(),120)) {
				getBenfreq2800();
			}
			else {
				sv.freqann.set("00");
				sv.benterm.set(999);
			}
		}
		else {
			goTo(GotoLabel.covrNotFound1030);
		}
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeIO.setLife("01");
		lifeIO.setJlife("00");
		lifeIO.setCurrfrom(varcom.maxdate);
		lifeIO.setFormat(liferec);
		lifeIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, lifeIO);
		if (isEQ(lifeIO.getStatuz(),varcom.oK)
		&& isEQ(lifeIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		&& isEQ(lifeIO.getChdrnum(),chdrenqIO.getChdrnum())
		&& isEQ(lifeIO.getLife(),"01")
		&& isEQ(lifeIO.getJlife(),"00")) {
			sv.life.set("01");
			sv.sex.set(lifeIO.getCltsex());
			sv.age.set(lifeIO.getAnbAtCcd());
		}
		else {
			goTo(GotoLabel.lifeNotFound1040);
		}
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeIO.setLife("01");
		lifeIO.setJlife("01");
		lifeIO.setCurrfrom(varcom.maxdate);
		lifeIO.setFormat(liferec);
		lifeIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)
		&& isNE(lifeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifeIO.getStatuz(),varcom.oK)
		&& isEQ(lifeIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		&& isEQ(lifeIO.getChdrnum(),chdrenqIO.getChdrnum())
		&& isEQ(lifeIO.getLife(),"01")
		&& isEQ(lifeIO.getJlife(),"01")) {
			sv.jlife.set("01");
			sv.jlsex.set(lifeIO.getCltsex());
			sv.ageprem.set(lifeIO.getAnbAtCcd());
			goTo(GotoLabel.continue1060);
		}
		else {
			goTo(GotoLabel.jlifeNotFound1050);
		}
	}

protected void chdrenqNotFound1020()
	{
		sv.chdrsel.set("0000000000");
		sv.chdrtype.set("KLV");
		sv.currcd.set("DM");
		sv.currcode.set("DM");
		sv.billfreq.set("12");
		sv.ptdate.set(wsaaDateX);
	}

protected void covrNotFound1030()
	{
		sv.coverage.set("01");
		wsaaCoverageStore.set("01");
		sv.rider.set("00");
		wsaaRiderStore.set("00");
		sv.crtable.set("TF11");
		sv.pstatcode.set("PP");
		sv.crrcd.set(wsaaDateX);
		sv.premterm.set(20);
		sv.riskCessTerm.set(20);
		sv.sumins.set(100000);
		sv.singp.set(ZERO);
		sv.freqann.set("00");
		sv.benterm.set(999);
	}

protected void lifeNotFound1040()
	{
		sv.life.set("01");
		sv.sex.set(SPACES);
		sv.age.set(ZERO);
	}

protected void jlifeNotFound1050()
	{
		sv.jlife.set("00");
		sv.jlsex.set(SPACES);
		sv.ageprem.set(999);
	}

protected void continue1060()
	{
		sv.statuz.set(SPACES);
		sv.resamt.set(ZERO);
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			start2010();
		}
		catch (GOTOException e){
		}
	}

protected void start2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		shuntChdrsel2200();
		if (isNE(wsaaChdrsel,"0000000000")) {
			if (isNE(wsaaChdrsel,NUMERIC)) {
				sv.chdrselErr.set(f200);
			}
		}
		if (isEQ(wsaaChdrsel,"0000000000")
		&& isLTE(sv.annamnt,ZERO)) {
			sv.annamntErr.set(d020);
		}
		if (isEQ(sv.chdrtype,SPACES)) {
			sv.chdrtypeErr.set(d020);
		}
		else {
			checkT56042500();
		}
		if (isEQ(sv.coverage,SPACES)
		|| isNE(sv.coverage,NUMERIC)) {
			sv.coverageErr.set(d020);
		}
		if (isEQ(sv.rider,SPACES)
		|| isNE(sv.rider,NUMERIC)) {
			sv.riderErr.set(d020);
		}
		if (isEQ(sv.errorIndicators,SPACES)
		&& isNE(wsaaChdrsel,"0000000000")
		&& (isNE(sv.coverage,wsaaCoverageStore)
		|| isNE(sv.rider,wsaaRiderStore))) {
			checkCoverage2400();
			scrnparams.errorCode.set(d027);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.life,SPACES)
		|| isNE(sv.life,NUMERIC)) {
			sv.lifeErr.set(d020);
		}
		if (isEQ(sv.jlife,SPACES)
		|| isNE(sv.jlife,NUMERIC)) {
			sv.jlifeErr.set(d020);
		}
		if (isEQ(sv.crtable,SPACES)) {
			sv.crtableErr.set(d020);
		}
		else {
			checkT56022600();
		}
		if (isEQ(sv.effdate,ZERO)
		|| isEQ(sv.effdate,varcom.maxdate)) {
			sv.effdateErr.set(d020);
		}
		if (isNE(sv.billfreq,NUMERIC)
		|| isEQ(sv.billfreq,ZERO)) {
			sv.billfreqErr.set(d020);
		}
		if (isEQ(sv.sumins,ZERO)) {
			sv.suminsErr.set(d020);
		}
		if (isEQ(sv.age,ZERO)
		|| isEQ(sv.age,999)
		|| isGT(sv.age,80)) {
			sv.ageErr.set(d034);
		}
		if (isNE(sv.jlsex,SPACES)
		&& (isEQ(sv.ageprem,ZERO)
		|| isEQ(sv.ageprem,999))) {
			sv.agepremErr.set(d020);
		}
		if (isNE(sv.ageprem,ZERO)
		&& isNE(sv.ageprem,999)) {
			if (isEQ(sv.jlsex,SPACES)) {
				sv.jlsexErr.set(d020);
			}
			if (isGT(sv.ageprem,80)) {
				sv.agepremErr.set(d034);
			}
		}
		if (isEQ(sv.sex,SPACES)) {
			sv.sexErr.set(d020);
		}
		if ((isEQ(sv.premterm,ZERO)
		|| isEQ(sv.premterm,99))
		&& (isEQ(sv.riskCessTerm,ZERO)
		|| isEQ(sv.riskCessTerm,999))
		&& (isEQ(sv.benterm,ZERO)
		|| isEQ(sv.benterm,999))) {
			sv.premtermErr.set(d020);
			sv.rcestrmErr.set(d020);
			sv.bentermErr.set(d020);
		}
		if (isNE(sv.freqann,NUMERIC)) {
			sv.freqannErr.set(d020);
		}
		else {
			if (isGT(sv.freqann,ZERO)) {
				if (isGT(sv.benterm,80)) {
					sv.bentermErr.set(d020);
				}
			}
		}
		if (isEQ(sv.crrcd,ZERO)
		|| isEQ(sv.crrcd,varcom.maxdate)
		|| isEQ(sv.crrcd,99999999)) {
			sv.crrcdErr.set(d020);
		}
		if (isEQ(sv.ptdate,ZERO)
		|| isEQ(sv.ptdate,varcom.maxdate)
		|| isEQ(sv.ptdate,99999999)) {
			sv.ptdateErr.set(d020);
		}
		wsspcomn.edterror.set("Y");
		if (isNE(sv.errorIndicators,SPACES)) {
			goTo(GotoLabel.exit2090);
		}
		fillCopybook2100();
		callProgram(Sumres01.class, sumcalcrec.surrenderRec);
		if (isNE(sumcalcrec.status,varcom.oK)
		&& isNE(sumcalcrec.status,SPACES)) {
			sv.statuz.set(sumcalcrec.status);
			sv.statuzErr.set(sumcalcrec.status);
			sv.resamt.set(ZERO);
		}
		else {
			sv.statuz.set(SPACES);
			sv.resamt.set(sumcalcrec.actualVal);
		}
	}

protected void fillCopybook2100()
	{
		start2110();
	}

protected void start2110()
	{
		if (isGT(sv.annamnt,ZERO)) {
			wsaaBillfreq.set(sv.billfreq);
			checkT55412300();
			compute(sumcalcrec.calcPrem, 4).set((div((mult(wsaaBillfreq,sv.annamnt)),wsaaFreqMultiple)));
		}
		else {
			sumcalcrec.calcPrem.set(ZERO);
		}
		sumcalcrec.tsvtot.set(ZERO);
		sumcalcrec.tsv1tot.set(ZERO);
		sumcalcrec.chdrChdrcoy.set(wsaaChdrChdrcoy);
		sumcalcrec.chdrChdrnum.set(wsaaChdrselLast8);
		sumcalcrec.planSuffix.set(wsaaPlanSuffix);
		sumcalcrec.polsum.set(wsaaPolsum);
		sumcalcrec.lifeLife.set(sv.life);
		sumcalcrec.lifeJlife.set(sv.jlife);
		sumcalcrec.covrCoverage.set(sv.coverage);
		wsaaCoverageStore.set(sv.coverage);
		sumcalcrec.covrRider.set(sv.rider);
		wsaaRiderStore.set(sv.rider);
		sumcalcrec.crtable.set(sv.crtable);
		sumcalcrec.crrcd.set(sv.crrcd);
		sumcalcrec.ptdate.set(sv.ptdate);
		sumcalcrec.effdate.set(sv.ptdate);
		sumcalcrec.convUnits.set(wsaaConvUnits);
		sumcalcrec.language.set(sv.language);
		sumcalcrec.estimatedVal.set(ZERO);
		sumcalcrec.actualVal.set(sv.resamt);
		sumcalcrec.currcode.set(sv.currcode);
		sumcalcrec.chdrCurr.set(sv.currcd);
		sumcalcrec.pstatcode.set(sv.pstatcode);
		sumcalcrec.element.set(wsaaElement);
		sumcalcrec.description.set(wsaaDescription);
		sumcalcrec.singp.set(sv.singp);
		sumcalcrec.billfreq.set(sv.billfreq);
		sumcalcrec.type.set(wsaaType);
		sumcalcrec.fund.set(wsaaFund);
		sumcalcrec.status.set(sv.statuz);
		sumcalcrec.endf.set(wsaaEndf);
		sumcalcrec.neUnits.set(wsaaNeUnits);
		sumcalcrec.tmUnits.set(wsaaTmUnits);
		sumcalcrec.psNotAllwd.set(wsaaPsNotAllwd);
		sumcalcrec.trancde.set(sv.trancde);
		sumcalcrec.sumin.set(sv.sumins);
		sumcalcrec.lage.set(sv.age);
		sumcalcrec.jlage.set(sv.ageprem);
		sumcalcrec.lsex.set(sv.sex);
		sumcalcrec.jlsex.set(sv.jlsex);
		sumcalcrec.duration.set(sv.premterm);
		sumcalcrec.riskCessTerm.set(sv.riskCessTerm);
		sumcalcrec.benCessTerm.set(sv.benterm);
		sumcalcrec.chdrtype.set(sv.chdrtype);
		sumcalcrec.freqann.set(sv.freqann);
	}

protected void shuntChdrsel2200()
	{
		try {
			start2210();
		}
		catch (GOTOException e){
		}
	}

protected void start2210()
	{
		if (isEQ(sv.chdrsel,SPACES)) {
			wsaaChdrsel.set("0000000000");
			goTo(GotoLabel.exit2290);
		}
		else {
			wsaaChdrsel.set(sv.chdrsel);
		}
		for (wsaaS1.set(10); !(isLT(wsaaS1,1)); wsaaS1.add(-1)){
			if (isEQ(wsaaChdrchar[wsaaS1.toInt()],SPACES)) {
				for (wsaaS2.set(wsaaS1); !(isLT(wsaaS2,2)); wsaaS2.add(-1)){
					compute(wsaaChdrchar[wsaaS2.toInt()], 0).set(wsaaChdrchar[sub(wsaaS2,1).toInt()]);
				}
				wsaaS1.set(11);
			}
		}
		wsaaChdrsel.set(inspectReplaceAll(wsaaChdrsel, SPACES, "0"));
	}

protected void checkT55412300()
	{
		start2310();
	}

protected void start2310()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemtabl(t5541);
		itdmIO.setItemcoy(wsaaChdrChdrcoy);
		itdmIO.setItemitem(t5604rec.xfreqAltBasis);
		itdmIO.setItmfrm(sv.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			if (isEQ(itdmIO.getStatuz(),varcom.endp)
			|| isNE(itdmIO.getItemcoy(),wsaaChdrChdrcoy)
			|| isNE(itdmIO.getItemtabl(),t5541)
			|| isNE(itdmIO.getItemitem(),t5604rec.xfreqAltBasis)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(t075);
				fatalError600();
			}
			else {
				t5541rec.t5541Rec.set(itdmIO.getGenarea());
			}
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub,12)); wsaaSub.add(1)){
			if (isEQ(t5541rec.freqcy[wsaaSub.toInt()],sv.billfreq)) {
				wsaaFreqMultiple.set(t5541rec.lfact[wsaaSub.toInt()]);
				wsaaSub.set(13);
			}
			else {
				if (isGT(wsaaSub,11)) {
					syserrrec.params.set(itdmIO.getParams());
					syserrrec.statuz.set(t076);
					fatalError600();
				}
			}
		}
	}

protected void checkCoverage2400()
	{
		try {
			start2410();
		}
		catch (GOTOException e){
		}
	}

protected void start2410()
	{
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(chdrenqIO.getChdrcoy());
		covrIO.setChdrnum(chdrenqIO.getChdrnum());
		covrIO.setLife(sv.life);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setCoverage(sv.coverage);
		covrIO.setRider(sv.rider);
		covrIO.setFormat(covrrec);
		covrIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(covrIO.getStatuz(),varcom.oK)) {
			while ( !((isEQ(covrIO.getStatuz(),varcom.oK)
			&& isEQ(covrIO.getChdrcoy(),chdrenqIO.getChdrcoy())
			&& isEQ(covrIO.getChdrnum(),chdrenqIO.getChdrnum())
			&& isEQ(covrIO.getCoverage(),sv.coverage)
			&& isEQ(covrIO.getRider(),sv.rider)
			&& isEQ(covrIO.getValidflag(),"1"))
			|| isEQ(covrIO.getStatuz(),varcom.endp))) {
				loopCovr2700();
			}
			
		}
		if (isEQ(covrIO.getStatuz(),varcom.endp)) {
			sv.coverageErr.set(e348);
			sv.riderErr.set(e348);
			goTo(GotoLabel.exit2490);
		}
		else {
			sv.ptdate.set(chdrenqIO.getPtdate());
			sv.life.set(covrIO.getLife());
			sv.jlife.set(covrIO.getJlife());
			sv.coverage.set(covrIO.getCoverage());
			wsaaCoverageStore.set(covrIO.getCoverage());
			sv.rider.set(covrIO.getRider());
			wsaaRiderStore.set(covrIO.getRider());
			sv.crtable.set(covrIO.getCrtable());
			sv.crrcd.set(covrIO.getCrrcd());
			sv.pstatcode.set(covrIO.getPstatcode());
			sv.singp.set(covrIO.getSingp());
			sv.sumins.set(covrIO.getSumins());
			sv.premterm.set(covrIO.getPremCessTerm());
			sv.riskCessTerm.set(covrIO.getRiskCessTerm());
			sv.benterm.set(covrIO.getBenCessTerm());
			if (isGT(covrIO.getBenCessTerm(),ZERO)
			&& isLT(covrIO.getBenCessTerm(),120)) {
				getBenfreq2800();
			}
			else {
				sv.freqann.set("00");
				sv.benterm.set(999);
			}
		}
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeIO.setLife(covrIO.getLife());
		lifeIO.setJlife("00");
		lifeIO.setCurrfrom(varcom.maxdate);
		lifeIO.setFormat(liferec);
		lifeIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)
		&& isNE(lifeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifeIO.getStatuz(),varcom.endp)
		|| isNE(lifeIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(lifeIO.getChdrnum(),chdrenqIO.getChdrnum())
		|| isNE(lifeIO.getLife(),covrIO.getLife())
		|| isNE(lifeIO.getJlife(),"00")) {
			sv.lifeErr.set(e355);
			goTo(GotoLabel.exit2490);
		}
		else {
			sv.life.set(covrIO.getLife());
			sv.sex.set(lifeIO.getCltsex());
			sv.age.set(lifeIO.getAnbAtCcd());
		}
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeIO.setLife(covrIO.getLife());
		lifeIO.setJlife(covrIO.getJlife());
		lifeIO.setCurrfrom(varcom.maxdate);
		lifeIO.setFormat(liferec);
		lifeIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)
		&& isNE(lifeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			fatalError600();
		}
		if (isNE(lifeIO.getStatuz(),varcom.endp)
		&& isNE(lifeIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		&& isNE(lifeIO.getChdrnum(),chdrenqIO.getChdrnum())
		&& isNE(lifeIO.getLife(),covrIO.getLife())
		&& isNE(lifeIO.getJlife(),covrIO.getJlife())) {
			sv.jlife.set(covrIO.getJlife());
			sv.jlsex.set(lifeIO.getCltsex());
			sv.ageprem.set(lifeIO.getAnbAtCcd());
		}
		else {
			sv.jlife.set("00");
			sv.jlsex.set(SPACES);
			sv.ageprem.set(999);
		}
	}

protected void checkT56042500()
	{
		start2510();
	}

protected void start2510()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemtabl(t5604);
		itdmIO.setItemcoy(wsaaChdrChdrcoy);
		itdmIO.setItemitem(sv.chdrtype);
		itdmIO.setItmfrm(sv.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			if (isEQ(itdmIO.getStatuz(),varcom.endp)
			|| isNE(itdmIO.getItemcoy(),wsaaChdrChdrcoy)
			|| isNE(itdmIO.getItemtabl(),t5604)
			|| isNE(itdmIO.getItemitem(),sv.chdrtype)) {
				sv.chdrtypeErr.set(d035);
			}
			else {
				t5604rec.t5604Rec.set(itdmIO.getGenarea());
			}
		}
	}

protected void checkT56022600()
	{
		start2610();
	}

protected void start2610()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsaaChdrChdrcoy);
		itemIO.setItemtabl(t5602);
		itemIO.setItemitem(sv.crtable);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			sv.crtableErr.set(d024);
		}
		else {
			t5602rec.t5602Rec.set(itemIO.getGenarea());
		}
	}

protected void loopCovr2700()
	{
		/*START*/
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(covrIO.getStatuz(),varcom.endp)
		|| isNE(covrIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(),chdrenqIO.getChdrnum())
		|| isNE(covrIO.getCoverage(),sv.coverage)
		|| isNE(covrIO.getRider(),sv.rider)) {
			covrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void getBenfreq2800()
	{
		start2810();
	}

protected void start2810()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsaaChdrChdrcoy);
		itemIO.setItemtabl(t5671);
		wsbbTranscd.set(wsaaBatckey.batcBatctrcde);
		wsbbCrtable.set(sv.crtable);
		itemIO.setItemitem(wsbbTranCrtable);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		itdmIO.setItemtabl(t5606);
		if (isEQ(t5671rec.pgm[1],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
			wsbbTran.set(t5671rec.edtitm[1]);
		}
		else {
			if (isEQ(t5671rec.pgm[2],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
				wsbbTran.set(t5671rec.edtitm[2]);
			}
			else {
				if (isEQ(t5671rec.pgm[3],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
					wsbbTran.set(t5671rec.edtitm[3]);
				}
				else {
					wsbbTran.set(t5671rec.edtitm[4]);
				}
			}
		}
		wsbbCurrency.set(sv.currcd);
		itdmIO.setItemitem(wsbbTranCurrency);
		itdmIO.setItmfrm(sv.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			if (isNE(itdmIO.getStatuz(),varcom.endp)
			&& isEQ(itdmIO.getItemtabl(),t5606)
			&& isEQ(itdmIO.getItemitem(),wsbbTranCurrency)) {
				t5606rec.t5606Rec.set(itdmIO.getGenarea());
				sv.freqann.set(t5606rec.benfreq);
			}
			else {
				sv.freqann.set("00");
			}
		}
	}

protected void update3000()
	{
		/*START*/
		if (isEQ(chdrenqIO.getStatuz(),varcom.oK)) {
			chdrenqIO.setFormat(chdrenqrec);
			chdrenqIO.setFunction("RLSE");
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrenqIO.getParams());
				syserrrec.statuz.set(chdrenqIO.getStatuz());
				fatalError600();
			}
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
