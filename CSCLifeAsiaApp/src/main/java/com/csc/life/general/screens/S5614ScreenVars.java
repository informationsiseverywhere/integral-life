package com.csc.life.general.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5614
 * @version 1.0 generated on 30/08/09 06:45
 * @author Quipoz
 */
public class S5614ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(517);
	public FixedLengthStringData dataFields = new FixedLengthStringData(117).isAPartOf(dataArea, 0);
	public ZonedDecimalData age = DD.age.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData ageprem = DD.ageprem.copyToZonedDecimal().isAPartOf(dataFields,3);
	public ZonedDecimalData annamnt = DD.annamnt.copyToZonedDecimal().isAPartOf(dataFields,6);
	public ZonedDecimalData benterm = DD.benterm.copyToZonedDecimal().isAPartOf(dataFields,23);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,26);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,28);
	public FixedLengthStringData chdrtype = DD.chdrtype.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,43);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(dataFields,50);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,53);
	public FixedLengthStringData freqann = DD.freqann.copy().isAPartOf(dataFields,61);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData jlsex = DD.jlsex.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData language = DD.language.copy().isAPartOf(dataFields,66);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,67);
	public FixedLengthStringData pbind = DD.pbind.copy().isAPartOf(dataFields,69);
	public ZonedDecimalData premterm = DD.premterm.copyToZonedDecimal().isAPartOf(dataFields,70);
	public ZonedDecimalData riskCessTerm = DD.rcestrm.copyToZonedDecimal().isAPartOf(dataFields,73);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,76);
	public FixedLengthStringData sex = DD.sex.copy().isAPartOf(dataFields,78);
	public ZonedDecimalData singp = DD.singp.copyToZonedDecimal().isAPartOf(dataFields,79);
	public ZonedDecimalData sumins = DD.sumins.copyToZonedDecimal().isAPartOf(dataFields,96);
	public FixedLengthStringData trancde = DD.trancde.copy().isAPartOf(dataFields,113);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(100).isAPartOf(dataArea, 117);
	public FixedLengthStringData ageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agepremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData annamntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bentermErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData chdrtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData freqannErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData jlsexErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData languageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData pbindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData premtermErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData rcestrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData sexErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData singpErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData suminsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData trancdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(300).isAPartOf(dataArea, 217);
	public FixedLengthStringData[] ageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agepremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] annamntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bentermOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] chdrtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] freqannOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] jlsexOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] languageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] pbindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] premtermOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] rcestrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] sexOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] singpOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] trancdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData S5614screenWritten = new LongData(0);
	public LongData S5614protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5614ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(effdateOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrselOut,new String[] {"01",null, "-01","24",null, null, null, null, null, null, null, null});
		fieldIndMap.put(lifeOut,new String[] {"02","27","-02","27",null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrtypeOut,new String[] {"17","26","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sexOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(currcdOut,new String[] {"08","26","-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ageOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreqOut,new String[] {"09","26","-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jlifeOut,new String[] {"03","27","-03","27",null, null, null, null, null, null, null, null});
		fieldIndMap.put(coverageOut,new String[] {"04","-26","-04","-26",null, null, null, null, null, null, null, null});
		fieldIndMap.put(jlsexOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(riderOut,new String[] {"05","-26","-05","-26",null, null, null, null, null, null, null, null});
		fieldIndMap.put(agepremOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtableOut,new String[] {"06","26","-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(premtermOut,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcestrmOut,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(suminsOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqannOut,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bentermOut,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(annamntOut,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pbindOut,new String[] {"23","25","-23","25",null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {trancde, currcode, language, singp, effdate, chdrsel, life, chdrtype, sex, currcd, age, billfreq, jlife, coverage, jlsex, rider, ageprem, crtable, premterm, riskCessTerm, sumins, freqann, benterm, annamnt, pbind};
		screenOutFields = new BaseData[][] {trancdeOut, currcodeOut, languageOut, singpOut, effdateOut, chdrselOut, lifeOut, chdrtypeOut, sexOut, currcdOut, ageOut, billfreqOut, jlifeOut, coverageOut, jlsexOut, riderOut, agepremOut, crtableOut, premtermOut, rcestrmOut, suminsOut, freqannOut, bentermOut, annamntOut, pbindOut};
		screenErrFields = new BaseData[] {trancdeErr, currcodeErr, languageErr, singpErr, effdateErr, chdrselErr, lifeErr, chdrtypeErr, sexErr, currcdErr, ageErr, billfreqErr, jlifeErr, coverageErr, jlsexErr, riderErr, agepremErr, crtableErr, premtermErr, rcestrmErr, suminsErr, freqannErr, bentermErr, annamntErr, pbindErr};
		screenDateFields = new BaseData[] {effdate};
		screenDateErrFields = new BaseData[] {effdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5614screen.class;
		protectRecord = S5614protect.class;
	}

}
