package com.csc.life.general.dataaccess.dao;

import com.csc.life.general.dataaccess.model.Povrpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface PovrpfDAO extends BaseDAO<Povrpf> {
	public Povrpf getPovrRecord(Povrpf povrpf);
}
