package com.csc.life.general.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import com.csc.fsu.general.recordstructures.Zsdmcde;
import com.csc.life.general.tablestructures.T5684rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescpfDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.ItempfDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Sub-routine to extract the code from T5684 for SUN Dimension 3 Product Name code 
 * and return the code value to SUN Dimension main routine. This is applicable to 
 * various GL transactions. 
 * 
 * @author vhukumagrawa
 *
 */
public class Zd3t5684 extends COBOLConvCodeModel{

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaSubr = new FixedLengthStringData(8).init("ZD3T5684");
	
	private FixedLengthStringData wsaaT5684Array = new FixedLengthStringData(201600);
	private FixedLengthStringData[] wsaaT5684Rec = FLSArrayPartOfStructure(400, 504, wsaaT5684Array, 0);
	private FixedLengthStringData[] wsaaT5684Key = FLSDArrayPartOfArrayStructure(4, wsaaT5684Rec, 0);
	private FixedLengthStringData[] wsaaT5684stssct = FLSDArrayPartOfArrayStructure(4, wsaaT5684Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5684Data = FLSDArrayPartOfArrayStructure(500, wsaaT5684Rec, 4);
	private FixedLengthStringData[] wsaaT5684Genarea = FLSDArrayPartOfArrayStructure(500, wsaaT5684Data, 0);
	private int wsaaT5684Size = 400;
	private ZonedDecimalData wsaaT5684Ix = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	
	private Zsdmcde zsdmcde = new Zsdmcde();
	private T5684rec t5684rec = new T5684rec();
	
	private ItemTableDAM itemIO = new ItemTableDAM();
	
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);	
	private DescpfDAO descDAO =  DAOFactory.getDescpfDAO();
	List<Descpf> t5684List = null;
	private Map<String, List<Itempf>> t5684ListMap;
	

	private Map<String,String> t5684Map = new LinkedHashMap<String,String>();
	
	private String tableName;
	private Itempf itempf;
	private List<Itempf> itemAl = new ArrayList<Itempf>();
	private ItempfDAO itempfDAO =  DAOFactory.getItempfDAO();
	
	private String strCompany;
	private String strEffDate;
	private static final String rp50 = "RP50";
	
	private String t5684 = "T5684";
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit620
	}
	
	public void mainline(Object... parmArray)
	{
		zsdmcde.zsdmcdeRec = convertAndSetParam(zsdmcde.zsdmcdeRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}
	
	protected void startSubr010()
	{
		syserrrec.subrname.set(wsaaSubr);
		zsdmcde.statuz.set(varcom.oK);
		if(isEQ(wsaaFirstTime, SPACES)){
			loadTable1000();
			wsaaFirstTime.set("Y");
		}
		
		if(isEQ(zsdmcde.stssct, SPACES)){
			/*EXIT*/
			exitProgram();
		}
		readT5684();
	//	for(wsaaT5684Ix.set(1); !isGT(wsaaT5684Ix, wsaaT5684Size); wsaaT5684Ix.add(1)){
			
			//if(isEQ(tr57yrec.item[wsaaT5684Ix.toInt()].toString(), zsdmcde.stssct.toString())){
				//tr57yrec.tr57yRec.set(wsaaT5684Genarea[wsaaT5684Ix.toInt()]);
		String zdmsions = t5684Map.get(zsdmcde.stssct.toString().trim());
		if(zdmsions != null && !"".equals(zdmsions.trim()))
		{
				zsdmcde.rtzdmsion.set(zdmsions);
				
		}
				
				//break;
			//}
			
			
		//}
		
		/*EXIT*/
		exitProgram();
	}
	
	private void loadTable1000(){
		initialize(wsaaT5684Array);
		
		strCompany = zsdmcde.batccoy.toString();
		strEffDate = zsdmcde.beffdate.toString();	
		
		t5684ListMap = itemDAO.loadSmartTable("IT", strCompany, "T5684");
		//readT5684();
		
		/*itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(zsdmcde.batccoy);
		itemIO.setItemtabl(t5684);
		itemIO.setItemitem(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setFunction(varcom.begn);
		//itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		wsaaT5684Ix.set(1);
		while(isNE(itemIO.getStatuz(), varcom.endp)){
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)
				&& isNE(itemIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(itemIO.getParams());
					fatalError600();
			}
			
			if(isEQ(itemIO.getStatuz(), varcom.endp)
				|| isNE(itemIO.getItempfx(), "IT")
				|| isNE(itemIO.getItemcoy(), zsdmcde.batccoy)
				|| isNE(itemIO.getItemtabl(), t5684)){
					itemIO.setStatuz(varcom.endp);
			}
			if(isEQ(itemIO.getStatuz(), varcom.oK)){
				/* Item Found 
				wsaaT5684stssct[wsaaT5684Ix.toInt()].set(itemIO.getItemitem());
				wsaaT5684Genarea[wsaaT5684Ix.toInt()].set(itemIO.getGenarea());
				/* Go to next item 
				wsaaT5684Ix.add(1);
				itemIO.setFunction(varcom.nextr);
			}
		}*/
			}
	protected void readT5684(){
tableName = "T5684";
		
		
		itempf = new Itempf();
		itemAl.clear();
		itempf.setItempfx("IT");
		itempf.setItemcoy(zsdmcde.batccoy.toString().trim());
		itempf.setItemtabl(tableName);
		itempf.setValidflag("1");
		//itempf.setItemitem(stssct);
		itemAl = itempfDAO.findByExample(itempf);
		if(itemAl.isEmpty())
		{
			syserrrec.params.set(itempf.getItemitem());
			fatalError600();
		}	
		Iterator<Itempf> it = itemAl.iterator();
		while(it.hasNext())
		{
			itempf = it.next();
			t5684rec.t5684Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			t5684Map.put(itempf.getItemitem().trim(), t5684rec.zdmsions.toString().trim());
		}	
	}
	
	protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error610();
				}
				case exit620: {
					exit620();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void error610()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit620);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

	protected void exit620()
	{
		zsdmcde.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
