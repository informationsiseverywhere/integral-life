package com.csc.life.general.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:17
 * Description:
 * Copybook name: ANRLCNTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Anrlcntkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData anrlcntFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData anrlcntKey = new FixedLengthStringData(64).isAPartOf(anrlcntFileKey, 0, REDEFINE);
  	public FixedLengthStringData anrlcntChdrcoy = new FixedLengthStringData(1).isAPartOf(anrlcntKey, 0);
  	public FixedLengthStringData anrlcntChdrnum = new FixedLengthStringData(8).isAPartOf(anrlcntKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(anrlcntKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(anrlcntFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		anrlcntFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}