package com.csc.life.general.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5614screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5614ScreenVars sv = (S5614ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5614screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5614ScreenVars screenVars = (S5614ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.trancde.setClassString("");
		screenVars.currcode.setClassString("");
		screenVars.language.setClassString("");
		screenVars.singp.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.chdrsel.setClassString("");
		screenVars.life.setClassString("");
		screenVars.chdrtype.setClassString("");
		screenVars.sex.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.age.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.jlsex.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.ageprem.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.premterm.setClassString("");
		screenVars.riskCessTerm.setClassString("");
		screenVars.sumins.setClassString("");
		screenVars.freqann.setClassString("");
		screenVars.benterm.setClassString("");
		screenVars.annamnt.setClassString("");
		screenVars.pbind.setClassString("");
	}

/**
 * Clear all the variables in S5614screen
 */
	public static void clear(VarModel pv) {
		S5614ScreenVars screenVars = (S5614ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.trancde.clear();
		screenVars.currcode.clear();
		screenVars.language.clear();
		screenVars.singp.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.chdrsel.clear();
		screenVars.life.clear();
		screenVars.chdrtype.clear();
		screenVars.sex.clear();
		screenVars.currcd.clear();
		screenVars.age.clear();
		screenVars.billfreq.clear();
		screenVars.jlife.clear();
		screenVars.coverage.clear();
		screenVars.jlsex.clear();
		screenVars.rider.clear();
		screenVars.ageprem.clear();
		screenVars.crtable.clear();
		screenVars.premterm.clear();
		screenVars.riskCessTerm.clear();
		screenVars.sumins.clear();
		screenVars.freqann.clear();
		screenVars.benterm.clear();
		screenVars.annamnt.clear();
		screenVars.pbind.clear();
	}
}
