package com.csc.life.general.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:09
 * Description:
 * Copybook name: CLVD3EK
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Clvd3ek extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData record = new FixedLengthStringData(359);
  	public FixedLengthStringData lvdekBaustein = new FixedLengthStringData(8).isAPartOf(record, 0);
  	public FixedLengthStringData lvdekTabAusgabe = new FixedLengthStringData(344).isAPartOf(record, 8);
  	public ZonedDecimalData lvdekTabEnde = new ZonedDecimalData(4, 0).isAPartOf(lvdekTabAusgabe, 0);
  	public FixedLengthStringData[] lvdekTabZeile = FLSArrayPartOfStructure(20, 17, lvdekTabAusgabe, 4);
  	public FixedLengthStringData[] lvdekKomponente = FLSDArrayPartOfArrayStructure(4, lvdekTabZeile, 0);
  	public FixedLengthStringData[] lvdekKomptypspez = FLSDArrayPartOfArrayStructure(8, lvdekTabZeile, 4);
  	public FixedLengthStringData[] lvdekKomptyp = FLSDArrayPartOfArrayStructure(1, lvdekTabZeile, 12);
  	public FixedLengthStringData[] lvdekBezkomp = FLSDArrayPartOfArrayStructure(4, lvdekTabZeile, 13);
  	public FixedLengthStringData lvdekLeseart = new FixedLengthStringData(1).isAPartOf(record, 352);
  	public ZonedDecimalData lvdekFehler = new ZonedDecimalData(4, 0).isAPartOf(record, 353).setUnsigned();
  	public FixedLengthStringData lvdekFehlerherkunft = new FixedLengthStringData(2).isAPartOf(record, 357);


	public void initialize() {
		COBOLFunctions.initialize(record);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		record.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}