package com.csc.life.general.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:08
 * Description:
 * Copybook name: T5603REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5603rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5603Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData indxrate = new ZonedDecimalData(5, 2).isAPartOf(t5603Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(495).isAPartOf(t5603Rec, 5, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5603Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5603Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}