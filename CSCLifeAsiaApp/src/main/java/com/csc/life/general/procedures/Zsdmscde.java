package com.csc.life.general.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;

import com.csc.fsu.financials.dataaccess.CheqTableDAM;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;

import com.csc.fsu.general.recordstructures.Zsdmcde;
import com.csc.life.general.tablestructures.Tr57vrec;
import com.csc.life.general.tablestructures.Tr57xrec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovtTableDAM;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

//smalchi2
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quipoz.COBOLFramework.util.StringUtil;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescpfDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.ItempfDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
/**
 * SUN Dimension code extraction main routine. This main routine will be called from B3610 GL Transaction
 * Update batch program. Subsequently, it will call all the corresponding subroutines set in TAC01 table to
 * retrieve the relevant Dimension code value. After gather all required Dimension code value, this main routine
 * will return the SUN 7 Dimension coded to GL Update Batch program to create the GL Transaction with the SUN
 * Dimension codes.
 * 
 * @author vhukumagrawa
 *
 */
public class Zsdmscde extends COBOLConvCodeModel{
	private static final Logger LOGGER = LoggerFactory.getLogger(Zsdmscde.class);
	
	public static final String ROUTINE = QPUtilities.getThisClass();

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaZdmFnd = new FixedLengthStringData(1);
	private PackedDecimalData wsaaZdmSrh = new PackedDecimalData(2,0);
	private FixedLengthStringData wsaaTranscdty = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTranscd1 = new FixedLengthStringData(4).isAPartOf(wsaaTranscdty, 0);
	private FixedLengthStringData wsaaTranscd2 = new FixedLengthStringData(2).isAPartOf(wsaaTranscdty, 4);
	private FixedLengthStringData wsaaTranscd3 = new FixedLengthStringData(2).isAPartOf(wsaaTranscdty, 6);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCovr = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAgnt = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaNoPol = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSubr = new FixedLengthStringData(8).init("ZSDMSCDE");
	
	private FixedLengthStringData wsaaTAC01Array = new FixedLengthStringData(203200);
	private FixedLengthStringData[] wsaaTAC01Rec = FLSArrayPartOfStructure(400, 508, wsaaTAC01Array, 0);
	private FixedLengthStringData[] wsaaTAC01Key = FLSDArrayPartOfArrayStructure(8, wsaaTAC01Rec, 0);
	private FixedLengthStringData[] wsaaTAC01ItemItem = FLSDArrayPartOfArrayStructure(8, wsaaTAC01Key, 0, SPACES);
	private FixedLengthStringData[] wsaaTAC01Data = FLSDArrayPartOfArrayStructure(500, wsaaTAC01Rec, 8);
	private FixedLengthStringData[] wsaaTAC01Genarea = FLSDArrayPartOfArrayStructure(500, wsaaTAC01Data, 0);
	private int wsaaTAC01Size = 400;
	private ZonedDecimalData wsaaTAC01Ix = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	
	private FixedLengthStringData wsaaTAC01Dft = new FixedLengthStringData(500);
	
	private FixedLengthStringData wsaaT5687Array = new FixedLengthStringData(204800);
	private FixedLengthStringData[] wsaaT5687Rec = FLSArrayPartOfStructure(400, 512, wsaaT5687Array, 0);
	private FixedLengthStringData[] wsaaT5687Key = FLSDArrayPartOfArrayStructure(4, wsaaT5687Rec, 0);
	private FixedLengthStringData[] wsaaT5687Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5687Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5687Data = FLSDArrayPartOfArrayStructure(508, wsaaT5687Rec, 4);
	private PackedDecimalData[] wsaaT5687Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5687Data, 0);
	private FixedLengthStringData[] wsaaT5687Genarea = FLSDArrayPartOfArrayStructure(500, wsaaT5687Data, 8);
	private int wsaaT5687Size = 400;
	private ZonedDecimalData wsaaT5687Ix = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	
	private FixedLengthStringData wsaaTAC03Array = new FixedLengthStringData(180);
	private FixedLengthStringData[] wsaaTAC03Data = FLSArrayPartOfStructure(30, 6, wsaaTAC03Array, 0);
	//private FixedLengthStringData[] wsaaTAC03Data = FLSDArrayPartOfArrayStructure(6, wsaaTAC03Rec, 0);
	private FixedLengthStringData[] wsaaTAC03Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaTAC03Data, 0);
	private FixedLengthStringData[] wsaaTAC03Rdocpfx = FLSDArrayPartOfArrayStructure(2, wsaaTAC03Data, 2);
	private FixedLengthStringData[] wsaaTAC03Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaTAC03Data, 4);
	private int wsaaTAC03Size = 15;
	private ZonedDecimalData wsaaTAC03Ix = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	
	private FixedLengthStringData wsaaRdocnum = new FixedLengthStringData();
	
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	
	private CovtTableDAM covtIO = new CovtTableDAM();
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
	private CheqTableDAM cheqIO = new CheqTableDAM();
	private AglfTableDAM aglfIO = new AglfTableDAM();
	
	private Zsdmcde zsdmcde = new Zsdmcde();
	private Tr57vrec tr57vrec = new Tr57vrec();
	private Tr57xrec tr57xrec = new Tr57xrec();
	private T5687rec t5687rec = new T5687rec();
	
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescpfDAO descDAO =  DAOFactory.getDescpfDAO();
	
	private Map<String, List<Itempf>> t5687ListMap;
	private Map<String, List<Itempf>> tr57vListMap;
	private Map<String, List<Itempf>> tr57xListMap;
	
	List<Descpf> t5687List = null;
	List<Descpf> tr57xList = null;
	List<Descpf> tr57vList = null;
	
	private String strCompany;
	private String strEffDate;
	private static final String rp50 = "RP50";
	
	private String tr57v = "TR57V";
	private String tr57x = "TR57X";
	private String t5687 = "T5687";
	private String chdrlifrec = "CHDRLIFREC";
	private String covrrec = "COVRREC";
	private String covtrec = "COVTREC";
	private String agntlagrec = "AGNTLAGREC";
	
	private String h791 = "H791";
	
	private Map<String,String> tr57vMap = new LinkedHashMap<String,String>();

	private List<Itempf> itemAl = new ArrayList<Itempf>();
	private ItempfDAO itempfDAO =  DAOFactory.getItempfDAO();
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090,
		exit2190,
		exit2290,
		exit2390,
		exit2490,
		exit620
	}
	
	public Zsdmscde(){
		super();
	}
	
	public void mainline(Object... parmArray)
	{
		zsdmcde.zsdmcdeRec = convertAndSetParam(zsdmcde.zsdmcdeRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e1) {
			LOGGER.debug("Flow Ended");
		}
	}
	
	protected void startSubr010()
	{
		syserrrec.subrname.set(wsaaSubr);
		/* initialize zsdmcde */
		zsdmcde.statuz.set(varcom.oK);
		zsdmcde.cnttype.set(SPACES);
		zsdmcde.crtable.set(SPACES);
		zsdmcde.stfund.set(SPACES);
		zsdmcde.stsect.set(SPACES);
		zsdmcde.stssct.set(SPACES);
		zsdmcde.agtype.set(SPACES);
		zsdmcde.other1.set(SPACES);
		zsdmcde.other2.set(SPACES);
		zsdmcde.zdmsion01.set(SPACES);
		zsdmcde.zdmsion02.set(SPACES);
		zsdmcde.zdmsion03.set(SPACES);
		zsdmcde.zdmsion04.set(SPACES);
		zsdmcde.zdmsion05.set(SPACES);
		zsdmcde.zdmsion06.set(SPACES);
		zsdmcde.zdmsion07.set(SPACES);
		zsdmcde.rtzdmsion.set(SPACES);
		zsdmcde.branch.set(SPACES);
		zsdmcde.agbrn.set(SPACES);
		
		if(isEQ(wsaaFirstTime,SPACES)){
			loadTable1000();
			wsaaFirstTime.set("Y");
		}
		
		StringBuilder strVar1 = new StringBuilder();
		strVar1.append(zsdmcde.batctrcde);
		strVar1.append(zsdmcde.sacscode);
		strVar1.append(zsdmcde.sacstyp);
		wsaaTranscdty.set(strVar1);
		
		wsaaZdmFnd.set("N");
		wsaaZdmSrh.set(1);
		while(!(isGT(wsaaZdmSrh, 3)) && isNE(wsaaZdmFnd, "Y")){
			
			searchlabel:
			for(wsaaTAC01Ix.set(1); !(isGT(wsaaTAC01Ix,wsaaTAC01Size)); wsaaTAC01Ix.add(1)){
				readTr57v(zsdmcde.sacscode.toString());
				
				//	tr57vrec.tr57vRec.set(wsaaTAC01Genarea[wsaaTAC01Ix.toInt()]);
					wsaaZdmFnd.set("Y");
					break searchlabel;
				
			}
			
			if(isEQ(wsaaZdmFnd, "N")){
				wsaaZdmSrh.add(1);
			}
			if(isEQ(wsaaZdmSrh, 2)){
				wsaaTranscd1.set("****");
			}
			if(isEQ(wsaaZdmSrh, 3)){
				wsaaTranscd3.set("**");
			}
		}
		
		if(isEQ(wsaaZdmFnd, "N")){
			tr57vrec.tr57vRec.set(wsaaTAC01Dft);
			setDmval2000();
			//goTo(GotoLabel.exit2090);
			/*EXIT*/
			exitProgram();
		}
		
		wsaaChdrnum.set(SPACES);
		wsaaLife.set(SPACES);
		wsaaCovr.set(SPACES);
		wsaaRider.set(SPACES);
		wsaaAgnt.set(SPACES);
		setChdrnum2100();
		setEntity2200();
		setAgntnum2300();
		setPayBank2400();
		callZsdmrtn3000();
		
		/*EXIT*/
		exitProgram();
	}
	
	protected void loadTable1000(){
		initialize(wsaaTAC01Array);
		initialize(wsaaT5687Array);
		
		strCompany = zsdmcde.batccoy.toString();
		strEffDate = zsdmcde.beffdate.toString();	
		
		/*itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(zsdmcde.batccoy);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(SPACES);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		wsaaT5687Ix.set(1);
		while ( !(isEQ(itdmIO.getStatuz(),varcom.endp))) {
			loadT5687();
		}*/
		
		t5687ListMap = itemDAO.loadSmartTable("IT", strCompany, "T5687");
		
		
		tr57vListMap = itemDAO.loadSmartTable("IT", strCompany, "TR57V");
	//	readTr57v();
		
		
		tr57xListMap = itemDAO.loadSmartTable("IT", strCompany, "TR57X");
		
		
	/*	itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(zsdmcde.batccoy);
		itemIO.setItemtabl(tr57v);
		itemIO.setItemitem(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setFunction(varcom.begn);
		//itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		wsaaTAC01Ix.set(1);
		while ( !(isEQ(itemIO.getStatuz(),varcom.endp))) {
			loadTR57V();
		}*/
		
	/*	itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(zsdmcde.batccoy);
		itemIO.setItemtabl(tr57v);
		itemIO.setItemitem("********");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if(isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)){
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		
		if(isEQ(itemIO.getStatuz(), varcom.oK)){
			wsaaTAC01Dft.set(itemIO.getGenarea());
		} else {
			wsaaTAC01Dft.set(SPACES);
		}
		
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(zsdmcde.batccoy);
		itemIO.setItemtabl(tr57x);
		itemIO.setItemitem(zsdmcde.batccoy);
		itemIO.setStatuz(varcom.oK);
		itemIO.setFunction(varcom.begn);
		//itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMCOY");
		wsaaTAC03Ix.set(0);
		while ( isNE(itemIO.getStatuz(),varcom.endp)
				&& !(isGTE(wsaaTAC03Ix, wsaaTAC03Size))) {
			loadTR57V();
		}*/
			
	}
	
	protected void readT5687(){
		
		//t5687List = descDAO.getByDesctabl(zsdmcde.batccoy.toString().trim(), zsdmcde.language.toString().trim(), "T5687");
		boolean itemFound = false;
		//String keyItemitem = null;
		//for (Descpf descItem : t5687List) {		
		//	keyItemitem = descItem.getShortdesc();
		//	break;
			
		//}
		
		List<Itempf> itempfList = new ArrayList<Itempf>();
		
		if (t5687ListMap.containsKey(zsdmcde.crtable)){	
			itempfList = t5687ListMap.get(zsdmcde.crtable);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
							&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
						t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
						itemFound = true;
					}
				}else{
					t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;					
				}				
			}		
		}
		if (!itemFound) {
			syserrrec.params.set(t5687rec.t5687Rec);
			syserrrec.statuz.set(rp50);
			fatalError600();		
		}	
	}
	
	
	protected void readTr57v(String sacscode){
		boolean itemFound = false;
		
		List<Itempf> itempfList = new ArrayList<Itempf>();



		for(String sacscod : tr57vListMap.keySet())
		{
			//sacscod =	sacscod.replace("*", "");
			//sacscod = sacscod.trim();
			
			if((sacscod.replace("*", "").trim()).equals(sacscode))
			{
				sacscode = sacscod;
				break;
			}
		}


		
		if (tr57vListMap.containsKey(sacscode)){	
			itempfList = tr57vListMap.get(sacscode);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
							&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
						tr57vrec.tr57vRec.set(StringUtil.rawToString(itempf.getGenarea()));		
					
						itemFound = true;
					}
				}else{
					tr57vrec.tr57vRec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;					
				}				
			}		
		}
		if (!itemFound) {
			syserrrec.params.set(tr57xrec.tr57xRec);
			syserrrec.statuz.set(rp50);
			fatalError600();		
		}	
	}
	
	protected void readTr57x(){
		//tr57xList = descDAO.getByDesctabl("IT", zsdmcde.batccoy.toString().trim(), "TR57X");
		boolean itemFound = false;
	//	String keyItemitem = null;
	//	for (Descpf descItem : tr57xList) {		
		//	keyItemitem = descItem.getShortdesc();
		//	break;
			
	//	}
		List<Itempf> itempfList = new ArrayList<Itempf>();
		
		if (tr57xListMap.containsKey(zsdmcde.batccoy)){	
			itempfList = tr57xListMap.get(zsdmcde.batccoy);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
							&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
						tr57xrec.tr57xRec.set(StringUtil.rawToString(itempf.getGenarea()));		
					//	for(int i = 1; i <15; i++){
						//	wsaaTAC03Ix.add(1);
							//wsaaTAC03Sacscode[wsaaTAC03Ix.toInt()].set(tr57xrec.sacscode[i]);
							//wsaaTAC03Rdocpfx[wsaaTAC03Ix.toInt()].set(tr57xrec.rdocpfx[i]);
							//wsaaTAC03Sacstype[wsaaTAC03Ix.toInt()].set(tr57xrec.sacstype[i]);
						//}
						itemFound = true;
					}
				}else{
					tr57xrec.tr57xRec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;					
				}				
			}		
		}
		if (!itemFound) {
			syserrrec.params.set(tr57xrec.tr57xRec);
			syserrrec.statuz.set(rp50);
			fatalError600();		
		}	
	}
	
/*	protected void loadT5687(){
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),zsdmcde.batccoy)
		|| isNE(itdmIO.getItemtabl(),t5687)) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isGT(wsaaT5687Ix,wsaaT5687Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(t5687);
			fatalError600();
		}
		if(isEQ(itdmIO.getStatuz(), varcom.oK)){
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
			if(isLTE(itdmIO.getItmfrm(),zsdmcde.beffdate)
					&& isGT(itdmIO.getItmto(),zsdmcde.beffdate)){
				wsaaT5687Crtable[wsaaT5687Ix.toInt()].set(itdmIO.getItemitem());
				wsaaT5687Genarea[wsaaT5687Ix.toInt()].set(itdmIO.getGenarea());
			}
			wsaaT5687Ix.add(1);
			itdmIO.setFunction(varcom.nextr);
		}
	}*/
	
/*	protected void loadTR57V(){
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
			&& isNE(itemIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.endp)
			|| isNE(itemIO.getItempfx(), "IT")
			|| isNE(itemIO.getItemcoy(),zsdmcde.batccoy)
			|| isNE(itemIO.getItemtabl(),tr57v)) {
				itemIO.setStatuz(varcom.endp);
		}
		if (isGT(wsaaTAC01Ix,wsaaTAC01Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(tr57v);
			fatalError600();
		}
		//if(isEQ(itemIO.getStatuz(), varcom.oK)){
		//	tr57vrec.tr57vRec.set(itemIO.getGenarea());
		//	wsaaTAC01ItemItem[wsaaTAC01Ix.toInt()].set(tr57vrec.item);
		//	wsaaTAC01Genarea[wsaaTAC01Ix.toInt()].set(itemIO.getGenarea());
			//wsaaTAC01Ix.add(1);
			//itemIO.setFunction(varcom.nextr);
		//}
	}*/
	
	/*protected void loadTAC03(){
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
				&& isNE(itemIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(itemIO.getParams());
					fatalError600();
		}
		
		if (isGT(wsaaTAC03Ix,wsaaTAC03Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(tr57v);
			fatalError600();
		}
		
		if (isEQ(itemIO.getStatuz(),varcom.endp)
			|| isNE(itemIO.getItempfx(), "IT")
			|| isNE(itemIO.getItemcoy(),zsdmcde.batccoy)
			|| isNE(itemIO.getItemtabl(),tr57x)
			|| isNE(itemIO.getItemitem(), zsdmcde.batccoy)) {
				itemIO.setStatuz(varcom.endp);
		}
		if(isEQ(itemIO.getStatuz(), varcom.oK)){
			tr57xrec.tr57xRec.set(itemIO.getGenarea());
			for(int i = 1; i <15; i++){
				wsaaTAC03Ix.add(1);
				wsaaTAC03Sacscode[wsaaTAC03Ix.toInt()].set(tr57xrec.sacscode[i]);
				wsaaTAC03Rdocpfx[wsaaTAC03Ix.toInt()].set(tr57xrec.rdocpfx[i]);
				wsaaTAC03Sacstype[wsaaTAC03Ix.toInt()].set(tr57xrec.sacstype[i]);
			}
			itemIO.setFunction(varcom.nextr);
		}
	}*/
	
	protected void setDmval2000(){
		zsdmcde.zdmsion01.set(tr57vrec.zdmsion01);
		zsdmcde.zdmsion02.set(tr57vrec.zdmsion02);
		zsdmcde.zdmsion03.set(tr57vrec.zdmsion03);
		zsdmcde.zdmsion04.set(tr57vrec.zdmsion04);
		zsdmcde.zdmsion05.set(tr57vrec.zdmsion05);
		zsdmcde.zdmsion06.set(tr57vrec.zdmsion06);
		zsdmcde.zdmsion07.set(tr57vrec.zdmsion07);
	}
	
	protected void setChdrnum2100(){
		try {
			setChdrnum2150();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
	}
	
	protected void setChdrnum2150(){
		
		if(isEQ(tr57vrec.subrtn01, SPACES)
			&& isEQ(tr57vrec.subrtn02, SPACES)
			&& isEQ(tr57vrec.subrtn03, SPACES)
			&& isEQ(tr57vrec.subrtn04, SPACES)){
				goTo(GotoLabel.exit2190);
		} 
		
		if(isEQ(zsdmcde.sacscode, "LR")
			&& isEQ(zsdmcde.rdocnum.toString().substring(0, 8), zsdmcde.rldgacct.toString().substring(0, 8))
			&& isEQ(zsdmcde.tranref, SPACES)){
				goTo(GotoLabel.exit2190);
		}
		
		wsaaNoPol.set(SPACES);
		wsaaTAC03Ix.set(1);
		readTr57x();
		while(!isGT(wsaaTAC03Ix, wsaaTAC03Size)
				&& !isEQ(wsaaNoPol, "Y")
				&& !isEQ(tr57xrec.sacscode[wsaaTAC03Ix.toInt()], SPACES)){
			
			if(isNE(tr57xrec.sacscode[wsaaTAC03Ix.toInt()], SPACES)
				&& isEQ(tr57xrec.sacscode[wsaaTAC03Ix.toInt()], zsdmcde.sacscode)){
				if(isNE(tr57xrec.rdocpfx[wsaaTAC03Ix.toInt()], SPACES)){
					if(isEQ(tr57xrec.rdocpfx[wsaaTAC03Ix.toInt()], zsdmcde.rdocpfx)){
						wsaaNoPol.set("Y");
					}
				} else {
					if(isNE(tr57xrec.sacstype[wsaaTAC03Ix.toInt()], SPACES)){
						if(isEQ(tr57xrec.sacstype[wsaaTAC03Ix.toInt()], zsdmcde.sacstyp)){
							wsaaNoPol.set("Y");
						}
					} else {
						wsaaNoPol.set("Y");
					}
				}
					
			}
			wsaaTAC03Ix.add(1);
		}
		
		if(isEQ(wsaaNoPol, "Y")){
			goTo(GotoLabel.exit2190);
		}
		
		if(isEQ(zsdmcde.sacscode, "LE")
			|| isEQ(zsdmcde.sacscode, "LP")
			|| isEQ(zsdmcde.sacscode, "LN")
			|| isEQ(zsdmcde.sacscode, "LC")
			|| (isEQ(zsdmcde.sacscode, "SC") && isNE(zsdmcde.rdocpfx, "CA"))
			//TMLII-2102 START
//			|| (isEQ(zsdmcde.sacscode, "GL") && isEQ(zsdmcde.sacstyp, "WV"))){
			|| (isEQ(zsdmcde.sacscode, "GL") && isNE(zsdmcde.sacstyp, "A") && isNE(zsdmcde.sacstyp, "BL"))){
			//TMLII-2102 END
			wsaaChdrnum.set(zsdmcde.rldgacct.toString().substring(0, 8));
		}
		
		if(isEQ(zsdmcde.sacscode, "LA")){
			wsaaChdrnum.set(zsdmcde.tranref.toString().substring(0, 8));
		}
		
		if(isEQ(zsdmcde.sacscode, "LR")){
			wsaaChdrnum.set(zsdmcde.rdocnum.toString().substring(0, 8));
		}
		
		if(isEQ(wsaaChdrnum, SPACES)){
			goTo(GotoLabel.exit2190);
		}
		
		readChdrlif();
		zsdmcde.cnttype.set(chdrlifIO.getCnttype());
		zsdmcde.branch.set(chdrlifIO.getRegister());
	}
	
	protected void readChdrlif(){
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(zsdmcde.batccoy);
		chdrlifIO.setChdrnum(wsaaChdrnum);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if(isNE(chdrlifIO.getStatuz(), varcom.oK)
			&& isNE(chdrlifIO.getStatuz(), varcom.mrnf)){
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
		}
		
		if(isEQ(chdrlifIO.getStatuz(), varcom.mrnf)){
			wsaaChdrnum.set(SPACES);
			goTo(GotoLabel.exit2190);
		}
	}
	
	protected void setEntity2200(){
		try {
			setEntity2250();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
	}
	protected void setEntity2250(){
		if(isEQ(wsaaChdrnum, SPACES)){
			goTo(GotoLabel.exit2290);
		}
		
		if(isEQ(zsdmcde.sacscode, "LN")
			|| isEQ(zsdmcde.sacscode, "LC")
			|| isEQ(zsdmcde.sacscode, "LR")
			|| (isEQ(zsdmcde.sacscode, "SC") && isNE(zsdmcde.rdocpfx, "CA"))
			|| (isEQ(zsdmcde.sacscode, "LA") && isEQ(zsdmcde.tranref.toString().substring(8, 14), SPACES))
			|| (isEQ(zsdmcde.sacscode, "LE") && isEQ(zsdmcde.rldgacct.toString().substring(8, 14), SPACES))
			//TMLII-2102 STRAT
//			|| (isEQ(zsdmcde.sacscode, "GL") && isEQ(zsdmcde.sacstyp, "WV") && 
			|| (isEQ(zsdmcde.sacscode, "GL") && isNE(zsdmcde.sacstyp, "A") && isNE(zsdmcde.sacstyp, "BL") &&
			//TMLII-2102 END
					isEQ(zsdmcde.rldgacct.toString().substring(8, 14), SPACES))){
				wsaaLife.set("01");
				wsaaCovr.set("01");
				wsaaRider.set("00");
				//TLMII-1549 - issue 2
//				goTo(GotoLabel.exit2290);
		}
		
		if(isEQ(zsdmcde.sacscode, "LA")){
			wsaaLife.set(zsdmcde.tranref.toString().substring(8,10));
			wsaaCovr.set(zsdmcde.tranref.toString().substring(10,12));
			wsaaRider.set(zsdmcde.tranref.toString().substring(12,14));
		}
		
		if(isEQ(zsdmcde.sacscode, "LE")
			//TMLII-2102 STRAT
//			|| (isEQ(zsdmcde.sacscode, "GL") && isEQ(zsdmcde.sacstyp, "WV"))){
			|| (isEQ(zsdmcde.sacscode, "GL") && isNE(zsdmcde.sacstyp, "A") && isNE(zsdmcde.sacstyp, "BL"))){
			//TMLII-2102 END
			wsaaLife.set(zsdmcde.rldgacct.toString().substring(8,10));
			wsaaCovr.set(zsdmcde.rldgacct.toString().substring(10,12));
			wsaaRider.set(zsdmcde.rldgacct.toString().substring(12,14));
		}
		
		if(isEQ(zsdmcde.sacscode, "LP")){
			if(isNE(zsdmcde.rldgacct.toString().substring(8, 14), SPACES)){
				wsaaLife.set(zsdmcde.rldgacct.toString().substring(8,10));
				wsaaCovr.set(zsdmcde.rldgacct.toString().substring(10,12));
				wsaaRider.set(zsdmcde.rldgacct.toString().substring(12,14));
			} else{
				wsaaLife.set("01");
				wsaaCovr.set("01");
				wsaaRider.set("00");
			}
		}
		
		if(isEQ(wsaaLife, SPACES)){
			wsaaLife.set("01");
			wsaaCovr.set("01");
			wsaaRider.set("00");
		}
		
		if(isEQ(wsaaCovr, SPACES)){
			wsaaCovr.set("01");
			wsaaRider.set("00");
		}
		
		if(isEQ(wsaaRider, SPACES)){
			wsaaRider.set("00");
		}
		
		readCovr();
		if(isEQ(covrIO.getStatuz(),varcom.oK)){
			zsdmcde.crtable.set(covrIO.getCrtable());
		} else {
			readCovt();
			if(isEQ(covtIO.getStatuz(), varcom.oK)){
				zsdmcde.crtable.set(covtIO.getCrtable());
			}
		}
		if(isEQ(zsdmcde.crtable, SPACES)){
			goTo(GotoLabel.exit2290);
		}
		
		//wsaaT5687Ix.set(1);
		initialize(t5687rec.t5687Rec);
		readT5687();
		//while(!isGT(wsaaT5687Ix, wsaaT5687Size)){
			
		//	if(isEQ(t5687rec[wsaaT5687Ix.toInt()], zsdmcde.crtable)){
			//	t5687rec.t5687Rec.set(wsaaT5687Genarea[wsaaT5687Ix.toInt()]);
				//break;
			//}
			
			//if(isEQ(wsaaT5687Crtable[wsaaT5687Ix.toInt()], SPACES)){
			//	break;
			//}
			//wsaaT5687Ix.add(1);
		//}
		
		zsdmcde.stfund.set(t5687rec.statFund);
		zsdmcde.stsect.set(t5687rec.statSect);
		zsdmcde.stssct.set(t5687rec.statSubSect);
	}
	
	protected void readCovr(){
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(zsdmcde.batccoy);
		covrIO.setChdrnum(wsaaChdrnum);
		covrIO.setLife(wsaaLife);
		covrIO.setCoverage(wsaaCovr);
		covrIO.setRider(wsaaRider);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.begn);
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		
		if(isEQ(covrIO.getStatuz(), varcom.endp)
			|| isNE(covrIO.getChdrcoy(), zsdmcde.batccoy)
			|| isNE(covrIO.getChdrnum(), wsaaChdrnum)
			|| isNE(covrIO.getLife(), wsaaLife)
			|| isNE(covrIO.getCoverage(), wsaaCovr)
			|| isNE(covrIO.getRider(), wsaaRider)){
				covrIO.setStatuz(varcom.endp);
		}
	}
	
	protected void readCovt(){

		covtIO.setParams(SPACES);
		covtIO.setChdrcoy(zsdmcde.batccoy);
		covtIO.setChdrnum(wsaaChdrnum);
		covtIO.setLife(wsaaLife);
		covtIO.setCoverage(wsaaCovr);
		covtIO.setRider(wsaaRider);
		//covtIO.setPlanSuffix(9999);
		covtIO.setFormat(covtrec);
		covtIO.setFunction(varcom.begn);
		/*covtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		*/SmartFileCode.execute(appVars, covtIO);
		if (isNE(covtIO.getStatuz(),varcom.oK)
		&& isNE(covtIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtIO.getParams());
			fatalError600();
		}
		
		if(isEQ(covtIO.getStatuz(), varcom.endp)
			|| isNE(covtIO.getChdrcoy(), zsdmcde.batccoy)
			|| isNE(covtIO.getChdrnum(), wsaaChdrnum)
			|| isNE(covtIO.getLife(), wsaaLife)
			|| isNE(covtIO.getCoverage(), wsaaCovr)
			|| isNE(covtIO.getRider(), wsaaRider)){
				covtIO.setStatuz(varcom.endp);
		}
	
	}
	
	protected void setAgntnum2300(){
		try {
			setAgntnum2350();
		}
		catch (GOTOException e){
		LOGGER.debug("Catched Goto");
		}
	}
	
	protected void setAgntnum2350(){
		if((isEQ(zsdmcde.sacscode, "LE") && isEQ(zsdmcde.sacstyp, "RP"))
			|| (isEQ(zsdmcde.sacscode, "LE") && isEQ(zsdmcde.sacstyp, "RI"))
			|| (isEQ(zsdmcde.sacscode, "SC") && isEQ(zsdmcde.rdocpfx, "CA"))
			|| isEQ(zsdmcde.sacscode, "LR")
			|| isEQ(zsdmcde.sacscode, "CN")
			|| isEQ(zsdmcde.sacscode, "BK")){
				goTo(GotoLabel.exit2390);
		}
		
		if((isEQ(zsdmcde.sacscode, "LE") && isEQ(zsdmcde.tranno, ZERO))){
			wsaaAgnt.set(zsdmcde.tranref.toString().substring(0, 8));
		}
		
		if(isEQ(zsdmcde.sacscode, "LP")
			|| isEQ(zsdmcde.sacscode, "LN")
			|| isEQ(zsdmcde.sacscode, "LC")
			|| (isEQ(zsdmcde.sacscode, "LE") && isNE(zsdmcde.tranno, ZERO))
			|| (isEQ(zsdmcde.sacscode, "SC") && isNE(zsdmcde.rdocpfx, "CA"))
			//TMLII-2102 STRAT
//			|| (isEQ(zsdmcde.sacscode, "GL") && isEQ(zsdmcde.sacstyp, "WV"))){
			|| (isEQ(zsdmcde.sacscode, "GL") && isNE(zsdmcde.sacstyp, "A") && isNE(zsdmcde.sacstyp, "BL"))){
			//TMLII-2102 END
				
				if(isNE(wsaaChdrnum, SPACES)){
					wsaaAgnt.set(chdrlifIO.getAgntnum());
				}
		}
		
		if(isEQ(zsdmcde.sacscode, "LA")
			|| isEQ(zsdmcde.sacscode, "AG")
			|| (isEQ(zsdmcde.sacscode, "LE") && isEQ(zsdmcde.sacstyp, "EL"))){
				wsaaAgnt.set(zsdmcde.rldgacct.toString().substring(0, 8));
				zsdmcde.agbrn.set("Y");
		}
		
		if(isEQ(zsdmcde.sacscode, "LE") && isEQ(zsdmcde.sacstyp, "OE")){
			wsaaAgnt.set(zsdmcde.tranref.toString().substring(0, 8));
		}
		if(isNE(wsaaAgnt,SPACE)  && isEQ(zsdmcde.agbrn,"Y"))
		{
			readAglf();
			if(isEQ(aglfIO.getStatuz(),Varcom.oK))
			{
				zsdmcde.branch.set(aglfIO.getAracde());
			}
		}
		if(isNE(wsaaAgnt, SPACES)){
			readAgntlag();
			if(isEQ(agntlagIO.getStatuz(), varcom.oK)){
				zsdmcde.agtype.set(agntlagIO.getAgtype());
				goTo(GotoLabel.exit2390);
			}
		}
		
		if(isNE(wsaaChdrnum, SPACES)){
			wsaaAgnt.set(chdrlifIO.getAgntnum());
			readAgntlag();
			if(isEQ(agntlagIO.getStatuz(), varcom.oK)){
				zsdmcde.agtype.set(agntlagIO.getAgtype());
			}
		}
	}
	
	private void readAgntlag(){
		agntlagIO.setParams(SPACES);
		agntlagIO.setAgntcoy(zsdmcde.batccoy);
		agntlagIO.setAgntnum(wsaaAgnt);
		agntlagIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntlagIO);
		if(isNE(agntlagIO.getStatuz(), varcom.oK)
			&& isNE(agntlagIO.getStatuz(), varcom.mrnf)){
			syserrrec.params.set(agntlagIO.getParams());
			syserrrec.statuz.set(agntlagIO.getStatuz());
			fatalError600();
		}
	}
	
	
	private void readAglf(){
		aglfIO.setParams(SPACES);
		aglfIO.setAgntcoy(zsdmcde.batccoy);
		aglfIO.setAgntnum(wsaaAgnt);
		aglfIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if(isNE(aglfIO.getStatuz(), varcom.oK)
			&& isNE(aglfIO.getStatuz(), Varcom.mrnf)){
			syserrrec.params.set(aglfIO.getParams());
			syserrrec.statuz.set(aglfIO.getStatuz());
			fatalError600();
		}
	}
	
	protected void setPayBank2400(){
		try {
			setPayBank2450();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
			
		}
	}
	protected void setPayBank2450(){
		if(isNE(zsdmcde.sacscode, "BK")
			|| isNE(zsdmcde.rdocpfx, "RQ")){
			goTo(GotoLabel.exit2490);
		}
		
		/* Read CHEQ */
		cheqIO.setParams(SPACES);
		cheqIO.setReqncoy(zsdmcde.batccoy);
		cheqIO.setReqnno(zsdmcde.rdocnum);
		cheqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cheqIO);
		if(isNE(cheqIO.getStatuz(), varcom.oK)
			&& isNE(cheqIO.getStatuz(), varcom.mrnf)){
//				zsdmcde.set(cheqIO.getReqnbcde());
			syserrrec.params.set(cheqIO.getParams());
			syserrrec.statuz.set(cheqIO.getStatuz());
			fatalError600();
		}
		
		if(isEQ(cheqIO.getStatuz(), varcom.oK)){
			zsdmcde.bankcode.set(cheqIO.getReqnbcde());
		}
	}
	
	protected void callZsdmrtn3000(){
		
		if(isEQ(tr57vrec.subrtn01, SPACES)){
			zsdmcde.zdmsion01.set(tr57vrec.zdmsion01);
		} else {
			zsdmcde.rtzdmsion.set(tr57vrec.zdmsion01);
			callProgram(tr57vrec.subrtn01, zsdmcde.zsdmcdeRec);
			zsdmcde.zdmsion01.set(zsdmcde.rtzdmsion);
		}
		
		if(isEQ(tr57vrec.subrtn02, SPACES)){
			zsdmcde.zdmsion02.set(tr57vrec.zdmsion02);
		} else {
			zsdmcde.rtzdmsion.set(tr57vrec.zdmsion02);
			callProgram(tr57vrec.subrtn02, zsdmcde.zsdmcdeRec);
			zsdmcde.zdmsion02.set(zsdmcde.rtzdmsion);
		}
		
		if(isEQ(tr57vrec.subrtn03, SPACES)){
			zsdmcde.zdmsion03.set(tr57vrec.zdmsion03);
		} else {
			zsdmcde.rtzdmsion.set(tr57vrec.zdmsion03);
			callProgram(tr57vrec.subrtn03, zsdmcde.zsdmcdeRec);
			zsdmcde.zdmsion03.set(zsdmcde.rtzdmsion);
		}
		
		if(isEQ(tr57vrec.subrtn04, SPACES)){
			zsdmcde.zdmsion04.set(tr57vrec.zdmsion04);
		} else {
			zsdmcde.rtzdmsion.set(tr57vrec.zdmsion04);
			callProgram(tr57vrec.subrtn04, zsdmcde.zsdmcdeRec);
			zsdmcde.zdmsion04.set(zsdmcde.rtzdmsion);
		}
		
		if(isEQ(tr57vrec.subrtn05, SPACES)){
			zsdmcde.zdmsion05.set(tr57vrec.zdmsion05);
		} else {
			zsdmcde.rtzdmsion.set(tr57vrec.zdmsion05);
			callProgram(tr57vrec.subrtn05, zsdmcde.zsdmcdeRec);
			zsdmcde.zdmsion05.set(zsdmcde.rtzdmsion);
		}
		
		if(isEQ(tr57vrec.subrtn06, SPACES)){
			zsdmcde.zdmsion06.set(tr57vrec.zdmsion06);
		} else {
			zsdmcde.rtzdmsion.set(tr57vrec.zdmsion06);
			callProgram(tr57vrec.subrtn06, zsdmcde.zsdmcdeRec);
			zsdmcde.zdmsion06.set(zsdmcde.rtzdmsion);
		}
		
		if(isEQ(tr57vrec.subrtn07, SPACES)){
			zsdmcde.zdmsion07.set(tr57vrec.zdmsion07);
		} else {
			zsdmcde.rtzdmsion.set(tr57vrec.zdmsion07);
			callProgram(tr57vrec.subrtn07, zsdmcde.zsdmcdeRec);
			zsdmcde.zdmsion07.set(zsdmcde.rtzdmsion);
		}
		
	}
	
	protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error610();
				}
				case exit620: {
					exit620();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void error610()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit620);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

	protected void exit620()
	{
		zsdmcde.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
