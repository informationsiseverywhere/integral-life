/*
 * File: Bh5ge.java
 */
package com.csc.life.general.batchprograms;

import static com.csc.smart.recordstructures.Varcom.oK;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.dao.GcrtpfDAO;
import com.csc.fsu.general.dataaccess.model.Gcrtpf;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Bh5ge extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH5GE");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaLine = new ZonedDecimalData(6, 0).setUnsigned();
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(9);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER)
			.init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);


	private static final Logger LOGGER = LoggerFactory.getLogger(Bh5ge.class);

	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private String processName = "";
	
	List<Gcrtpf> gcrtpfList = null;
	private GcrtpfDAO gcrtpfDAO = getApplicationContext().getBean("gcrtpfDAO", GcrtpfDAO.class);
	private StringBuilder tableName = null;
	private File inputTextDir = null;
	public static String fileName;
	private final String FOLDER = "AIAIPRETURN";
	private File[] allCsvInsideUploadFolder = null;
	private int len = 0;
	private File file;
	
	public Bh5ge() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}



	/**
	* The mainline method is the default entry point of the program when called by other programs using the
	* Quipoz runtime framework.
	*/
@Override
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(oK);
		processName = bprdIO.getProcessName().toString();
//		softLock();
        tableName = new StringBuilder();
		tableName.append(bprdIO.getSystemParam01().trim());
		tableName.append(bprdIO.getSystemParam03().trim());
		tableName.append(String.format("%04d", bsscIO.getScheduleNumber().toInt()));
		gcrtpfList = new ArrayList<Gcrtpf>();
		allCsvInsideUploadFolder = getAllFiles();
		
	}

private void softLock() {
	/*    Ensure only one copy of this program is running at once. We*/
	/*    need to do this because a second program running at the same*/
	/*    time will 'pick' out BEXTs which we should be using in the*/
	/*    summerisation processes. Not only must we softlock the proces*/
	/*    we must also set it to an error otherwise MAINB will release*/
	/*    the softlock via a UALL function.*/
	sftlockrec.enttyp.set("PR");
	sftlockrec.company.set(bsprIO.getFsuco());
	sftlockrec.user.set(999999);
	sftlockrec.entity.set("LGCCRBH5GE      ");
	sftlockrec.transaction.set(bprdIO.getAuthCode());
	sftlockrec.function.set("LOCK");
	callProgram(Sftlock.class, sftlockrec.sftlockRec);
	if (isEQ(sftlockrec.statuz, "LOCK")) {
		syserrrec.statuz.set(ErrorsInner.plck);
		syserrrec.params.set(wsaaProg);
		fatalError600();
	}
	else {
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}
}



private void unlock() {
	/*    Remove the lock on this process*/
	sftlockrec = new Sftlockrec();
	sftlockrec.enttyp.set("PR");
	sftlockrec.company.set(bsprIO.getFsuco());
	sftlockrec.user.set(999999);
	sftlockrec.entity.set("LGCCRBH5GE      ");
	sftlockrec.function.set("UNLK");
	/* MOVE WSAA-PROG                 TO SFTL-ENTITY                */
	callProgram(Sftlock.class, sftlockrec.sftlockRec);
	if (isNE(sftlockrec.statuz, varcom.oK)) {
		syserrrec.params.set(sftlockrec.sftlockRec);
		syserrrec.statuz.set(sftlockrec.statuz);
		fatalError600();
	}
}


public File getInputTextDir() {
	return inputTextDir;
}

public void setInputTextDir(File inputTextDir) {
	this.inputTextDir = inputTextDir;
}

public String getFileName() {
	return fileName;
}

public static void setFileName(String fileName) {
	Bh5ge.fileName = fileName;
}



protected void readFile2000()
	{
	
	if(len < allCsvInsideUploadFolder.length) {
		file = allCsvInsideUploadFolder[len++];
		wsspEdterror.set(Varcom.oK);
	}
	else
	{
		wsspEdterror.set(Varcom.endp);
	}
}

private void addtoGcrtpfList(String data){
	String [] splittedArray = data.split(",");
	//Ticket #PINNACLE-1085 : L2RETGCCR file aborted
//	if(isHeaderRow(splittedArray)) {
//	    return;
//	}
	Gcrtpf gcrtpf = new Gcrtpf();
	gcrtpf.setChdrnum(splittedArray[0].trim());
	gcrtpf.setVind(splittedArray[1].trim());
	gcrtpf.setAmount(Double.parseDouble(splittedArray[2]));
	gcrtpf.setDupldsr(splittedArray[3].trim());
	gcrtpf.setReason(splittedArray[4].trim());
	gcrtpf.setJobno(bsscIO.getScheduleNumber().toInt());
	gcrtpf.setThreadno(Integer.parseInt(bsprIO.getProcessOccNum().toString().trim()));
	gcrtpfList.add(gcrtpf);
}
//Ticket #:PINNACLE-1085
private boolean isHeaderRow(String[] splittedArray) {
	String tmp = new String(splittedArray[0].replaceAll("\\s","").trim());
    return tmp.equalsIgnoreCase("POLNUM");
}


private File[] getAllFiles() {
	AppVars appVars = AppVars.getInstance();
	AppConfig appConfig = appVars.getAppConfig();
	//Ticket #PINNACLE-1129 : Sonar qube vulnerabilities
	File uploadFolder =  FileSystems.getDefault().getPath(appConfig.getUploadFolder() + File.separator+ FOLDER).toFile();
	return uploadFolder.listFiles();
}

protected void edit2500()
	{
		/* Edit code goes here */ 
	}


protected void update3000()
	{
		try(BufferedReader br = new BufferedReader(new FileReader(file))){
			if(file.length() != 0){
				fileName = file.getName();
				String data;
				//Ticket #PINNACLE-1085 : L2RETGCCR file aborted
				boolean isFirstRow = false;
				while((data = br.readLine())!=null){
					//Ticket #PINNACLE-1085 : L2RETGCCR file aborted
					if(!isFirstRow) {
						isFirstRow = true;
					}else {
						addtoGcrtpfList(data);
					}
				}
			}
			}
			catch (final IOException e) {
				LOGGER.error("error in readFile2010() try block>>>>", e);
			}
	}

/**
 * write csv data into temp table ex: GCRTL20001
 */
protected void writeCsvData() {
	
}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
		if(!gcrtpfList.isEmpty()){
			gcrtpfDAO.writeGcrtpfList(gcrtpfList, tableName.toString());
			gcrtpfList.clear();
		}
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		writeCsvData();	
		lsaaStatuz.set(oK);
//		unlock();
	}

/*
 * Class transformed  from Data Structure ERRORS_INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	public static FixedLengthStringData ivrm = new FixedLengthStringData(4).init("IVRM");
	public static FixedLengthStringData h791 = new FixedLengthStringData(4).init("H791");
	public static FixedLengthStringData h842 = new FixedLengthStringData(4).init("H842");
	public static FixedLengthStringData nfnd = new FixedLengthStringData(4).init("NFND");
	public static FixedLengthStringData plck = new FixedLengthStringData(4).init("PLCK");
	public static FixedLengthStringData rpgn = new FixedLengthStringData(4).init("RPGN");
	public static FixedLengthStringData rpgo = new FixedLengthStringData(4).init("RPGO");
	public static FixedLengthStringData rpgp = new FixedLengthStringData(4).init("RPGP");
	public static FixedLengthStringData rpgq = new FixedLengthStringData(4).init("RPGQ");
	public static FixedLengthStringData rfk5 = new FixedLengthStringData(4).init("RFK5");
}

}
