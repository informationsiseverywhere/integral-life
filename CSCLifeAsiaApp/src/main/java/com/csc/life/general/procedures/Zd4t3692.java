package com.csc.life.general.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import com.csc.fsu.general.tablestructures.T3692rec;
import com.csc.fsu.general.recordstructures.Zsdmcde;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescpfDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.ItempfDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Sub-routine to extract the code from T3692 for SUN Dimension 4 Distribution Channel 
 * code and return the code value to SUN Dimension main routine. This is applicable to 
 * various GL transactions.
 * 
 * @author vhukumagrawa
 *
 */
public class Zd4t3692 extends COBOLConvCodeModel{

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaSubr = new FixedLengthStringData(8).init("ZD4T3692");
	
	private FixedLengthStringData wsaaT3692Array = new FixedLengthStringData(30120);
	private FixedLengthStringData[] wsaaT3692Rec = FLSArrayPartOfStructure(60, 502, wsaaT3692Array, 0);
	private FixedLengthStringData[] wsaaT3692Key = FLSDArrayPartOfArrayStructure(2, wsaaT3692Rec, 0);
	private FixedLengthStringData[] wsaaT3692Agtype = FLSDArrayPartOfArrayStructure(2, wsaaT3692Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT3692Data = FLSDArrayPartOfArrayStructure(500, wsaaT3692Rec, 2);
	private FixedLengthStringData[] wsaaT3692Genarea = FLSDArrayPartOfArrayStructure(500, wsaaT3692Data, 0);
	private int wsaaT3692Size = 60;
	private ZonedDecimalData wsaaT3692Ix = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	
	private Zsdmcde zsdmcde = new Zsdmcde();
	private T3692rec t3692rec = new T3692rec();
	
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);	
	private DescpfDAO descDAO =  DAOFactory.getDescpfDAO();
	private Map<String, List<Itempf>> t3692ListMap;
	List<Descpf> t3692List = null;
	
	private String strCompany;
	private String strEffDate;
	private static final String rp50 = "RP50";
	
	private String t3692 = "T3692";
	
	private Map<String,String> t3692Map = new LinkedHashMap<String,String>();
	private String tableName;
	private Itempf itempf;
	private List<Itempf> itemAl = new ArrayList<Itempf>();
	private ItempfDAO itempfDAO =  DAOFactory.getItempfDAO();
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit620
	}
	
	public void mainline(Object... parmArray)
	{
		zsdmcde.zsdmcdeRec = convertAndSetParam(zsdmcde.zsdmcdeRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}
	
	protected void startSubr010()
	{
		syserrrec.subrname.set(wsaaSubr);
		zsdmcde.statuz.set(varcom.oK);
		if(isEQ(wsaaFirstTime, SPACES)){
			loadTable1000();
			wsaaFirstTime.set("Y");
		}
		
		if(isEQ(zsdmcde.agtype, SPACES)){
			/*EXIT*/
			exitProgram();
		}
		
		//for(wsaaT3692Ix.set(1); !isGT(wsaaT3692Ix, wsaaT3692Size); wsaaT3692Ix.add(1)){
			readT3692();
			String zdmsions = t3692Map.get(zsdmcde.agtype.toString().trim());
			if(zdmsions != null && !"".equals(zdmsions.trim()))
			{
					zsdmcde.rtzdmsion.set(zdmsions);
					exitProgram();
			}
				//t3692rec.t3692Rec.set(wsaaT3692Genarea[wsaaT3692Ix.toInt()]);
				
				
			
			
		//	if(isEQ(wsaaT3692Agtype[wsaaT3692Ix.toInt()], SPACES)){
			//	break;
		//	}
			
	
		
		/*EXIT*/
		exitProgram();
	}
	
	private void loadTable1000(){
		initialize(wsaaT3692Array);
		
		strCompany = zsdmcde.batccoy.toString();
		strEffDate = zsdmcde.beffdate.toString();	
		
		t3692ListMap = itemDAO.loadSmartTable("IT", strCompany, "T3692");
		//readT3692();
		
	/*	itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(zsdmcde.batccoy);
		itemIO.setItemtabl(t3692);
		itemIO.setItemitem(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setFunction(varcom.begn);
		//itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		wsaaT3692Ix.set(1);
		while(isNE(itemIO.getStatuz(), varcom.endp)){
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)
				&& isNE(itemIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(itemIO.getParams());
					fatalError600();
			}
			
			if(isEQ(itemIO.getStatuz(), varcom.endp)
				|| isNE(itemIO.getItempfx(), "IT")
				|| isNE(itemIO.getItemcoy(), zsdmcde.batccoy)
				|| isNE(itemIO.getItemtabl(), t3692)){
					itemIO.setStatuz(varcom.endp);
			}
			if(isEQ(itemIO.getStatuz(), varcom.oK)){
				/* Item Found 
				wsaaT3692Agtype[wsaaT3692Ix.toInt()].set(itemIO.getItemitem());
				wsaaT3692Genarea[wsaaT3692Ix.toInt()].set(itemIO.getGenarea());
				/* Go to next item 
				wsaaT3692Ix.add(1);
				itemIO.setFunction(varcom.nextr);
			}
		}*/
		
	}
	
	protected void readT3692(){
		
		
tableName = "T3692";
		
		
		itempf = new Itempf();
		itemAl.clear();
		itempf.setItempfx("IT");
		itempf.setItemcoy(zsdmcde.batccoy.toString().trim());
		itempf.setItemtabl(tableName);
		itempf.setValidflag("1");
	//	itempf.setItemitem(agentType);
		itemAl = itempfDAO.findByExample(itempf);
		if(itemAl.isEmpty())
		{
			syserrrec.params.set(itempf.getItemitem());
			fatalError600();
		}	
		Iterator<Itempf> it = itemAl.iterator();
		while(it.hasNext())
		{
			itempf = it.next();
			t3692rec.t3692Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			t3692Map.put(itempf.getItemitem().trim(), t3692rec.zdmsion.toString().trim());
		}
	}
	
	protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error610();
				}
				case exit620: {
					exit620();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void error610()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit620);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

	protected void exit620()
	{
		zsdmcde.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
