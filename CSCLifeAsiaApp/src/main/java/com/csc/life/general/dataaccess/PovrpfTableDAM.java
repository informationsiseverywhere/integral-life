package com.csc.life.general.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: PovrpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:07
 * Class transformed from POVRPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PovrpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 311;
	public FixedLengthStringData povrrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData povrpfRecord = povrrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(povrrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(povrrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(povrrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(povrrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(povrrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(povrrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(povrrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(povrrec);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt01 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt02 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt03 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt04 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt05 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt06 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt07 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt08 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt09 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt10 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt11 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt12 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt13 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt14 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt15 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt16 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt17 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt18 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt19 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt20 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt21 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt22 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt23 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt24 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData annamnt25 = DD.annamnt.copy().isAPartOf(povrrec);
	public PackedDecimalData xplusone = DD.xplusone.copy().isAPartOf(povrrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(povrrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(povrrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(povrrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public PovrpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for PovrpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public PovrpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for PovrpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public PovrpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for PovrpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public PovrpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("POVRPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"VALIDFLAG, " +
							"CRTABLE, " +
							"LIFCNUM, " +
							"ANNAMNT01, " +
							"ANNAMNT02, " +
							"ANNAMNT03, " +
							"ANNAMNT04, " +
							"ANNAMNT05, " +
							"ANNAMNT06, " +
							"ANNAMNT07, " +
							"ANNAMNT08, " +
							"ANNAMNT09, " +
							"ANNAMNT10, " +
							"ANNAMNT11, " +
							"ANNAMNT12, " +
							"ANNAMNT13, " +
							"ANNAMNT14, " +
							"ANNAMNT15, " +
							"ANNAMNT16, " +
							"ANNAMNT17, " +
							"ANNAMNT18, " +
							"ANNAMNT19, " +
							"ANNAMNT20, " +
							"ANNAMNT21, " +
							"ANNAMNT22, " +
							"ANNAMNT23, " +
							"ANNAMNT24, " +
							"ANNAMNT25, " +
							"XPLUSONE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     validflag,
                                     crtable,
                                     lifcnum,
                                     annamnt01,
                                     annamnt02,
                                     annamnt03,
                                     annamnt04,
                                     annamnt05,
                                     annamnt06,
                                     annamnt07,
                                     annamnt08,
                                     annamnt09,
                                     annamnt10,
                                     annamnt11,
                                     annamnt12,
                                     annamnt13,
                                     annamnt14,
                                     annamnt15,
                                     annamnt16,
                                     annamnt17,
                                     annamnt18,
                                     annamnt19,
                                     annamnt20,
                                     annamnt21,
                                     annamnt22,
                                     annamnt23,
                                     annamnt24,
                                     annamnt25,
                                     xplusone,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		validflag.clear();
  		crtable.clear();
  		lifcnum.clear();
  		annamnt01.clear();
  		annamnt02.clear();
  		annamnt03.clear();
  		annamnt04.clear();
  		annamnt05.clear();
  		annamnt06.clear();
  		annamnt07.clear();
  		annamnt08.clear();
  		annamnt09.clear();
  		annamnt10.clear();
  		annamnt11.clear();
  		annamnt12.clear();
  		annamnt13.clear();
  		annamnt14.clear();
  		annamnt15.clear();
  		annamnt16.clear();
  		annamnt17.clear();
  		annamnt18.clear();
  		annamnt19.clear();
  		annamnt20.clear();
  		annamnt21.clear();
  		annamnt22.clear();
  		annamnt23.clear();
  		annamnt24.clear();
  		annamnt25.clear();
  		xplusone.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getPovrrec() {
  		return povrrec;
	}

	public FixedLengthStringData getPovrpfRecord() {
  		return povrpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setPovrrec(what);
	}

	public void setPovrrec(Object what) {
  		this.povrrec.set(what);
	}

	public void setPovrpfRecord(Object what) {
  		this.povrpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(povrrec.getLength());
		result.set(povrrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}