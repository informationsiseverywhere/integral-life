package com.csc.life.general.batchprograms;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.csv.CSVGenerator;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.dao.CrispfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.dataaccess.model.Crispf;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.regularprocessing.recordstructures.Pa642par;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;



/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   This batch program is used for CSV file generation 
*   When user run the batch with the option “1.All”: 
*   record will be all put in the extracted file.When user run the batch with the option “2. 
*   Delta” and user input 10/12/2018 as the From Date, system need to extract any valid records
*   created/updated from 10/12/2018 till Business Date in the file.
*
***********************************************************************
* </pre>
*/
public class Ba639 extends Mainb {

	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BA639");
	private static final Logger LOGGER = LoggerFactory.getLogger(Ba639.class);
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private Wsspcomn wsspcomn = new Wsspcomn();	
	private List<Crispf> crispfList = null;
    private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO",ClntpfDAO.class); //IFSU-1883
    private CrispfDAO crispfDAO = getApplicationContext().getBean("crispfDAO",CrispfDAO.class);
	private Clntpf clnt ; 
	private Descpf descpf;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private List<String> tapeRecodData=null;
	private static final String outgoing = "OUTGOING";
	private String fileName = "";
	private FixedLengthStringData wsaaRecordProcessed = new FixedLengthStringData(1).init("N");
	final String COMMA_DELIMITER = "|";
	private Pa642par pa642par = new Pa642par();
	private ZonedDecimalData fromdate = new ZonedDecimalData(8, 0);
	FixedLengthStringData option = new FixedLengthStringData(8);
	private static final String Tr2a4 = "TR2A4";
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	@Override
	protected FixedLengthStringData getLsaaStatuz() {
		
		return lsaaStatuz;
	}

	@Override
	protected FixedLengthStringData getLsaaBsscrec() {
		
		return lsaaBsscrec;
	}

	@Override
	protected FixedLengthStringData getLsaaBsprrec() {
		
		return lsaaBsprrec;
	}

	@Override
	protected FixedLengthStringData getLsaaBprdrec() {
		
		return lsaaBprdrec;
	}

	@Override
	protected FixedLengthStringData getLsaaBuparec() {
		
		return lsaaBuparec;
	}

	@Override
	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		
		this.lsaaStatuz.set(lsaaStatuz);
	}

	@Override
	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;

	}

	@Override
	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) 		
	{
		this.lsaaBsprrec = lsaaBsprrec;
	}

	@Override
	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		
		this.lsaaBprdrec = lsaaBprdrec;
	}

	@Override
	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		
		this.lsaaBuparec = lsaaBuparec;
	}
	
	@Override
	public void mainline(Object... parmArray) {
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		super.mainline();
	}
	
	@Override
	protected void restart0900() {
		// implemented the override metho
	}

	protected void initialise1000() {
		pa642par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(Varcom.oK);		
		 option=pa642par.option;
		 fromdate=pa642par.fromdate;		
		crispfList = crispfDAO.readCrisByValidflag(bsscIO.getEffectiveDate().toInt(),fromdate.toInt(),option.toString());		
	}

	
	protected void readFile2000() {
 		if (null != crispfList && !crispfList.isEmpty()) {
 			getWsspEdterror().set(Varcom.oK);
		} else {
			wsaaRecordProcessed.set("N");
			getWsspEdterror().set(Varcom.endp);
		}
	}

	protected void update3000() {
		processCrispfRecords(crispfList);
		getWsspEdterror().set(Varcom.endp);
	}

	 protected void processCrispfRecords(List<Crispf> crispfList) {
	        //Delimiter used in CSV file
	       
	         fileName="CSV_OUT_"+bsscIO.getEffectiveDate()+"_"+bsscIO.getScheduleNumber().toInt();
	         tapeRecodData = new ArrayList<String>();
	         StringBuilder sb=null;
	         processHeader();
	        for (Crispf crispf : crispfList) {
	        	sb= new StringBuilder();      
	        	sb.append((crispf.getClientNo() == null || crispf.getClientNo().trim().isEmpty()) ? addPadding(8,"") : addPadding(8,crispf.getClientNo()));
	        	sb.append(COMMA_DELIMITER);
	        	try{
	        	processClientType(crispf.getClientNo());
	        	sb.append((descpf.getLongdesc() == null || descpf.getLongdesc().trim().isEmpty()) ? addPadding(8,"") : addPadding(8,descpf.getLongdesc()));
	        	sb.append(COMMA_DELIMITER);
	        	}catch(Exception e){
	        		LOGGER.info("Error for Client ID and Number ", e.getMessage());//IJTI-1561
	        	}
	        	
	        	sb.append((clnt.getSecuityno() == null || clnt.getSecuityno().trim().isEmpty()) ? addPadding(24,"") : addPadding(24,clnt.getSecuityno()));
	        	sb.append(COMMA_DELIMITER);
	        	sb.append((crispf.getFullName() == null || crispf.getFullName().trim().isEmpty()) ? addPadding(60,"") : addPadding(60,crispf.getFullName()));
	        	sb.append(COMMA_DELIMITER);
	        	sb.append((crispf.getTaxaccountType() == null || crispf.getTaxaccountType().trim().isEmpty()) ? addPadding(2,"") : addPadding(2,crispf.getTaxaccountType()));
	        	sb.append(COMMA_DELIMITER);
	        	sb.append((crispf.getSurName() == null || crispf.getSurName().trim().isEmpty()) ? addPadding(60,"") : addPadding(60,crispf.getSurName()));
	        	sb.append(COMMA_DELIMITER);
	        	sb.append((crispf.getGivenName() == null || crispf.getGivenName().trim().isEmpty()) ? addPadding(60,"") : addPadding(60,crispf.getGivenName()));
	        	sb.append(COMMA_DELIMITER);
	        	sb.append(getFormattedDate(crispf.getDob()));
	        	sb.append(COMMA_DELIMITER);
	        	sb.append((crispf.getResidenceCountryChn() == null || crispf.getResidenceCountryChn().trim().isEmpty()) ? addPadding(3,"") : addPadding(3,crispf.getResidenceCountryChn()));
	        	sb.append(COMMA_DELIMITER);
	        	sb.append((crispf.getResidenceProvinceChn() == null || crispf.getResidenceProvinceChn().trim().isEmpty()) ? addPadding(2,"") : addPadding(2,crispf.getResidenceProvinceChn()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getResidenceCityChn() == null || crispf.getResidenceCityChn().trim().isEmpty()) ? addPadding(4,"") : addPadding(4,crispf.getResidenceCityChn()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getResidenceAddressChn() == null || crispf.getResidenceAddressChn().trim().isEmpty()) ? addPadding(120,"") : addPadding(120,crispf.getResidenceAddressChn()));
                sb.append(COMMA_DELIMITER);               
                sb.append((crispf.getResidenceCountry() == null || crispf.getResidenceCountry().trim().isEmpty() ) ? addPadding(3,"") : addPadding(3,crispf.getResidenceCountry()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getResidenceProvince() == null || crispf.getResidenceProvince().trim().isEmpty()) ? addPadding(2,"") : addPadding(2,crispf.getResidenceProvince()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getResidenceCity() == null || crispf.getResidenceCity().trim().isEmpty()) ? addPadding(4,"") : addPadding(4,crispf.getResidenceCity()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getResidenceAddress() == null || crispf.getResidenceAddress().trim().isEmpty()) ? addPadding(120,"") : addPadding(120,crispf.getResidenceAddress()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getBirthCountryChn() == null || crispf.getBirthCountryChn().trim().isEmpty()) ? addPadding(3,"") : addPadding(3,crispf.getBirthCountryChn()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getBirthProvinceChn()== null || crispf.getBirthProvinceChn().trim().isEmpty()) ? addPadding(2,"") : addPadding(2,crispf.getBirthProvinceChn()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getBirthCityChn()== null || crispf.getBirthCityChn().trim().isEmpty()) ? addPadding(4,"") : addPadding(4,crispf.getBirthCityChn()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getBirthAddressChn()== null || crispf.getBirthAddressChn().trim().isEmpty()) ? addPadding(120,"") : addPadding(120,crispf.getBirthAddressChn()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getBirthCountry() == null || crispf.getBirthCountry().trim().isEmpty()) ? addPadding(3,"") : addPadding(3,crispf.getBirthCountry()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getBirthProvince() == null || crispf.getBirthProvince().trim().isEmpty()) ? addPadding(2,"") : addPadding(2,crispf.getBirthProvince()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getBirthCity() == null || crispf.getBirthCity().trim().isEmpty()) ? addPadding(4,"") : addPadding(4,crispf.getBirthCity()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getBirthAddress() == null || crispf.getBirthAddress().trim().isEmpty()) ? addPadding(120,"") : addPadding(120,crispf.getBirthAddress()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getCountry1() == null || crispf.getCountry1().trim().isEmpty()) ? addPadding(3,"") : addPadding(3,crispf.getCountry1()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getTaxid1() == null || crispf.getTaxid1().trim().isEmpty()) ? addPadding(20,"") : addPadding(20,crispf.getTaxid1()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getCountry2() == null || crispf.getCountry2().trim().isEmpty()) ? addPadding(3,"") : addPadding(3,crispf.getCountry2()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getTaxid2() == null || crispf.getTaxid2().trim().isEmpty()) ? addPadding(20,"") : addPadding(20,crispf.getTaxid2()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getCountry3() == null || crispf.getCountry3().trim().isEmpty()) ? addPadding(3,"") : addPadding(3,crispf.getCountry3()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getTaxid3() == null || crispf.getTaxid3().trim().isEmpty()) ? addPadding(20,"") : addPadding(20,crispf.getTaxid3()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getIndicator()  == null || crispf.getIndicator() .trim().isEmpty()) ? addPadding(1,"") : addPadding(1,crispf.getIndicator()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getUtoindicator() == null || crispf.getUtoindicator().trim().isEmpty()) ? addPadding(1,"") : addPadding(1,crispf.getUtoindicator()));
                sb.append(COMMA_DELIMITER);
                sb.append((crispf.getReason() == null || crispf.getReason().trim().isEmpty()) ? addPadding(60,"") : addPadding(60,crispf.getReason()));
                sb.append(COMMA_DELIMITER);
                sb.append(crispf.getDatime());
	        	
	        	tapeRecodData.add(sb.toString());
	        }	        
	        CSVGenerator.produceCSVfile(fileName, outgoing, tapeRecodData);		

	    }
	 
	 
	 private String addPadding(int len,String val) 
	 {
		return StringUtils.rightPad(val, len, " ");
	 }


		private void processHeader() {
	    	StringBuilder sb= new StringBuilder();
	    	sb.append("Client Number");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("ID Type");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("ID number");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Name");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Tax Account Type");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Sur name");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Given name");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Date of Birth");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Country Code Chinese");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Province Code Chinese");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("City Code Chinese");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Detail Address Chinese");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Country Code");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Province Code");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("City Code");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Detail Address");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Birth Country Code Chinese");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Birth Province Code Chinese");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Birth City Code Chinese");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Birth Address Chinese");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Birth Country Code");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Birth Province Code");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Birth City Code");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Birth Address");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Country Code 1");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Tax ID 1");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Country Code 2");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Tax ID 2");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Country Code 3");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Tax ID 3");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("NA");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append(" UTO");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("REASON");
	    	sb.append(COMMA_DELIMITER);
	    	sb.append("Timestamp");
	    	tapeRecodData.add(sb.toString());
		
	}

		private String getFormattedDate(String dob) {
		String dateString = "";
		try {
			Date parse = new SimpleDateFormat("yyyyMMdd").parse(dob);
			dateString = new SimpleDateFormat("yyyy-MM-dd").format(parse);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateString;
	}

		private void processClientType(String clntNum) {
	    	clnt = getClntpf(clntNum);
	    	readRecord1031();
			
			
		}

		private Clntpf getClntpf(String clientNo) {
	    	return clntpfDAO.findClientByClntnum(clientNo);		
		}
	   	    
	    protected void readRecord1031()
		{		
				descpf=descDAO.getdescData("IT", Tr2a4, clnt.getIdtype().trim(), bsprIO.getFsuco().toString(), "E");
				if(descpf==null){
					descpf=new Descpf();
					descpf.setLongdesc(" ");
					descpf.setShortdesc(" ");
				}
			
		}
	
	protected void error(String statuz, String params) {
		syserrrec.statuz.set(statuz);
		syserrrec.params.set(params);
		fatalError600();
	}

	protected void commit3500() {
		
	}

	@Override
	protected void rollback3600() {
		// implemented the override metho
	}

	@Override
	protected void close4000() {
		// implemented the override metho
	}
	

	@Override
	protected void edit2500() {
		// implemented the override metho

	}

}
