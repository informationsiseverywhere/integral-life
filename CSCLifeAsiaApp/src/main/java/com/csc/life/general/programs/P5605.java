/*
 * File: P5605.java
 * Date: 30 August 2009 0:31:32
 * Author: Quipoz Limited
 * 
 * Class transformed from P5605.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.general.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.general.dataaccess.AnrlcntTableDAM;
import com.csc.life.general.screens.S5605ScreenVars;
import com.csc.life.general.tablestructures.T5603rec;
import com.csc.life.general.tablestructures.T5604rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Anniversary Rules Window.
*
*****************************************************************
* </pre>
*/
public class P5605 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5605");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaFound = "";
	private String wsaaKeep = "";
		/* ERRORS */
	private static final String d011 = "D011";
	private static final String d012 = "D012";
	private static final String d013 = "D013";
	private static final String d014 = "D014";
	private static final String d015 = "D015";
		/* TABLES */
	private static final String t5603 = "T5603";
	private static final String t5604 = "T5604";
		/* FORMATS */
	private static final String anrlcntrec = "ANRLCNTREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private AnrlcntTableDAM anrlcntIO = new AnrlcntTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private T5603rec t5603rec = new T5603rec();
	private T5604rec t5604rec = new T5604rec();
	private S5605ScreenVars sv = ScreenProgram.getScreenVars( S5605ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		init1110
	}

	public P5605() {
		super();
		screenVars = sv;
		new ScreenModel("S5605", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/* Screen field initilisation.*/
		sv.dataArea.set(SPACES);
		sv.autorate.set(ZERO);
		ratesTable1200();
		rulesTable1300();
		anrlcntIO.setParams(SPACES);
		anrlcntIO.setPlanSuffix(ZERO);
		anrlcntIO.setAutorate(ZERO);
		anrlcntIO.setChdrcoy(wsspcomn.chdrChdrcoy);
		anrlcntIO.setChdrnum(wsspcomn.chdrChdrnum);
		anrlcntIO.setFunction(varcom.begnh);
		getAnrlcnt1100();
		/* Initialize ANRL if anniversary details don't exist*/
		if (isEQ(wsaaFound, "N")) {
			anrlcntIO.setParams(SPACES);
			anrlcntIO.setPlanSuffix(ZERO);
			anrlcntIO.setAutorate(ZERO);
			anrlcntIO.setChdrcoy(wsspcomn.chdrChdrcoy);
			anrlcntIO.setChdrnum(wsspcomn.chdrChdrnum);
		}
		sv.premind.set(anrlcntIO.getPremind());
		sv.sumasind.set(anrlcntIO.getSumasind());
		sv.indxflg.set(anrlcntIO.getIndxflg());
		sv.autorate.set(anrlcntIO.getAutorate());
	}

protected void getAnrlcnt1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case init1110: 
					init1110();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void init1110()
	{
		/* Find valid record.*/
		anrlcntIO.setFormat(anrlcntrec);
		SmartFileCode.execute(appVars, anrlcntIO);
		if (isNE(anrlcntIO.getStatuz(), varcom.oK)
		&& isNE(anrlcntIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(anrlcntIO.getParams());
			fatalError600();
		}
		wsaaFound = "N";
		if (isEQ(anrlcntIO.getStatuz(), varcom.endp)
		|| isNE(anrlcntIO.getChdrcoy(), wsspcomn.chdrChdrcoy)
		|| isNE(anrlcntIO.getChdrnum(), wsspcomn.chdrChdrnum)) {
			return ;
		}
		if (isEQ(anrlcntIO.getValidflag(), "1")) {
			wsaaFound = "Y";
			return ;
		}
		anrlcntIO.setFunction(varcom.nextr);
		goTo(GotoLabel.init1110);
	}

protected void ratesTable1200()
	{
		init1210();
	}

protected void init1210()
	{
		/*  Read Indexation Rates*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.chdrChdrcoy);
		itdmIO.setItemtabl(t5603);
		itdmIO.setItemitem(wsspcomn.chdrCnttype);
		itdmIO.setItmfrm(wsspcomn.currfrom);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5603)
		|| isNE(itdmIO.getItemitem(), wsspcomn.chdrCnttype)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			/*  Read Default Indexation Rates*/
			/*  if Product not found.*/
			itdmIO.setDataKey(SPACES);
			itdmIO.setItemcoy(wsspcomn.chdrChdrcoy);
			itdmIO.setItemtabl(t5603);
			itdmIO.setItemitem("***");
			itdmIO.setItmfrm(wsspcomn.currfrom);
			itdmIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			|| isNE(itdmIO.getItemcoy(), wsspcomn.chdrChdrcoy)
			|| isNE(itdmIO.getItemtabl(), t5603)
			|| isNE(itdmIO.getItemitem(), "***")) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
		}
		t5603rec.t5603Rec.set(itdmIO.getGenarea());
	}

protected void rulesTable1300()
	{
		init1310();
	}

protected void init1310()
	{
		/*  Read Anniversary Rules*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.chdrChdrcoy);
		itdmIO.setItemtabl(t5604);
		itdmIO.setItemitem(wsspcomn.chdrCnttype);
		itdmIO.setItmfrm(wsspcomn.currfrom);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5604)
		|| isNE(itdmIO.getItemitem(), wsspcomn.chdrCnttype)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5604rec.t5604Rec.set(itdmIO.getGenarea());
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validate2020();
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		/*    CALL 'S5605IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5605-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		/* Validate fields.*/
		if (isEQ(sv.premind, SPACES)
		&& isEQ(sv.sumasind, SPACES)
		&& isEQ(sv.indxflg, SPACES)
		&& isEQ(sv.autorate, ZERO)) {
			/* indicates no entries therefore record will not be retained*/
			wsaaKeep = "N";
			return ;
		}
		wsaaKeep = "Y";
		if (isNE(sv.premind, SPACES)
		&& isNE(sv.sumasind, SPACES)) {
			sv.premindErr.set(d011);
			sv.sumasindErr.set(d011);
		}
		if (isEQ(sv.premind, SPACES)
		&& isEQ(sv.sumasind, SPACES)) {
			sv.premindErr.set(d012);
			sv.sumasindErr.set(d012);
		}
		if (isNE(sv.indxflg, SPACES)
		&& isNE(sv.autorate, ZERO)) {
			sv.indxflgErr.set(d011);
			sv.autorateErr.set(d011);
		}
		if (isEQ(sv.indxflg, SPACES)
		&& isEQ(sv.autorate, ZERO)) {
			sv.indxflgErr.set(d013);
			sv.autorateErr.set(d013);
		}
		if (isNE(sv.autorate, ZERO)) {
			if (isLT(sv.autorate, t5604rec.minpcnt)
			|| isGT(sv.autorate, t5604rec.maxpcnt)) {
				sv.autorateErr.set(d014);
			}
		}
		if (isNE(sv.indxflg, SPACES)
		&& isEQ(t5603rec.indxrate, ZERO)) {
			sv.indxflgErr.set(d015);
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		init3010();
	}

protected void init3010()
	{
		/* Update database files as required / WSSP.*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/* A Historical record is kept with a valid flag of '2' if*/
		/* anniversary rules pre-exist and contract has been issued.*/
		if (isEQ(wsaaFound, "Y")
		&& isEQ(wsaaKeep, "Y")
		&& isNE(wsspcomn.chdrValidflag, "3")) {
			anrlcntIO.setValidflag("2");
			anrlcntIO.setFunction(varcom.rewrt);
			anrlcntIO.setFormat(anrlcntrec);
			SmartFileCode.execute(appVars, anrlcntIO);
			if (isNE(anrlcntIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(anrlcntIO.getParams());
				fatalError600();
			}
		}
		anrlcntIO.setPremind(sv.premind);
		anrlcntIO.setSumasind(sv.sumasind);
		anrlcntIO.setIndxflg(sv.indxflg);
		anrlcntIO.setAutorate(sv.autorate);
		anrlcntIO.setValidflag("1");
		anrlcntIO.setFunction(varcom.writr);
		if (isEQ(wsspcomn.chdrValidflag, "3")
		&& isEQ(wsaaFound, "Y")) {
			anrlcntIO.setFunction(varcom.rewrt);
		}
		if (isEQ(wsaaKeep, "N")) {
			if (isEQ(wsaaFound, "N")) {
				return ;
			}
			else {
				anrlcntIO.setFunction(varcom.delet);
			}
		}
		anrlcntIO.setFormat(anrlcntrec);
		SmartFileCode.execute(appVars, anrlcntIO);
		if (isNE(anrlcntIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(anrlcntIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		scrnparams.function.set("HIDEW");
		/*EXIT*/
	}
}
