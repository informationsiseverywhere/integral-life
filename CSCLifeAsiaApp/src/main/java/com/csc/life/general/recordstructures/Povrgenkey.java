package com.csc.life.general.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:51
 * Description:
 * Copybook name: POVRGENKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Povrgenkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData povrgenFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData povrgenKey = new FixedLengthStringData(64).isAPartOf(povrgenFileKey, 0, REDEFINE);
  	public FixedLengthStringData povrgenChdrcoy = new FixedLengthStringData(1).isAPartOf(povrgenKey, 0);
  	public FixedLengthStringData povrgenChdrnum = new FixedLengthStringData(8).isAPartOf(povrgenKey, 1);
  	public FixedLengthStringData povrgenLife = new FixedLengthStringData(2).isAPartOf(povrgenKey, 9);
  	public FixedLengthStringData povrgenCoverage = new FixedLengthStringData(2).isAPartOf(povrgenKey, 11);
  	public FixedLengthStringData povrgenRider = new FixedLengthStringData(2).isAPartOf(povrgenKey, 13);
  	public PackedDecimalData povrgenPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(povrgenKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(povrgenKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(povrgenFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		povrgenFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}