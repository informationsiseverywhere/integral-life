/*
 * File: Clcsur1.java
 * Date: 29 August 2009 22:40:15
 * Author: Quipoz Limited
 *
 * Class transformed from CLCSUR1.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.general.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.general.recordstructures.Sumcalcrec;
import com.csc.life.general.tablestructures.T5611rec;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  Surrender Calculation Method Calling The Reserve Calculation
*  Module SUMRES01 which in turn calls the 'SUM' calc. package.
*
*
* PROCESSING.
* ----------
*
* Read table T5611 and get the regional surrender parameters.
* This table is keyed on the coverage name and premium status.
*
* Check field INDIC01 on T5611 and if it is set then calculation
* method 'X' is required. This method obtains the Reserve on the
* coverage by calling routine SUMRES01 using actual data at the
* time of calculation. This value is then stored.
*
* Check field INDIC02 on T5611 and if it is set then calculation
* method 'X + 1' is required. This method obtains the Reserve on
* the cover by calling routine SUMRES01 using data where the ages
* have been incremented by one and the terms have been reduced by
* one. The value is then stored.
*
* If field INDIC03 is set then the reserve will be calculated as
* a percentage of the sum assured. Field PERSASSD holds the
* percentage......................
*
* NOTE 1.. Check interpolation between payment dates and ensure
*          the reserve calculated is inclusive of all relevent
*          payments and the to and from dates are correct..
*
* CALCULATE THE RESERVES AND STORE.
*
* Check the life ages against field AGEMAX on T5611. If at least
* one of the lives on the coverage is equal to or older than
* AGEMAX then check the remaining risk term on the coverage. If
* it is less than or equal to feild TERMMIM then surrend. charges
* will not be levied against this coverage.
*
* Test the stored reserve amount for each possible method.
* If calculation of charges is applicable then check whether the
* charge will be a percentage of base amount (PERBSAMT > ZERO)
* or a percentage of the sum at risk (PERSRISK > ZERO). Note that
* if neither is specified on T5611 the charge will remain zero.
*
* Add the surrender charges calculated to each of the reserves
* calculated. These amounts constitute the 'raw surrender values'
*
* ***********************
* Note that BONUS calculations
*           Policy loans
*           Loan interest
*           Tax
*   need to be integrated here at a later point..JS 16/2/1995.
* ***********************
*
* If INDIC05 is set to 'non-space' on T5611 then add the raw
* surrender values to get the final surrender value. Note that
* this WILL include the charges on each method if charges are
* to be levied.
*
* If INDIC05 is not set then check INDIC04. If this is set then
* the final value is the lowest raw value, if not the higher of
* the raw values is the default.
*
* Return to the calling program.
*
*****************************************************************
* </pre>
*/
public class Clcsur1 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "CLCSUR1";
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaFreqann = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaOldestLife = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaLowestValue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaHighestValue = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaT5606Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5606Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5606Item, 0);
	private FixedLengthStringData wsaaT5606Unknown = new FixedLengthStringData(1).isAPartOf(wsaaT5606Item, 4);
	private FixedLengthStringData wsaaT5606Currcode = new FixedLengthStringData(3).isAPartOf(wsaaT5606Item, 5);

	private FixedLengthStringData wsaaT5611Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5611Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5611Item, 0);
	private FixedLengthStringData wsaaT5611Pstatcode = new FixedLengthStringData(2).isAPartOf(wsaaT5611Item, 4);
	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaT5611Item, 6, FILLER).init(SPACES);

	private FixedLengthStringData wsaaStoredAmounts = new FixedLengthStringData(81);
	private PackedDecimalData[] wsaaBaseAmount = PDArrayPartOfStructure(3, 17, 2, wsaaStoredAmounts, 0);
	private PackedDecimalData[] wsaaChargeAmount = PDArrayPartOfStructure(3, 17, 2, wsaaStoredAmounts, 27);
	private PackedDecimalData[] wsaaRawAmount = PDArrayPartOfStructure(3, 17, 2, wsaaStoredAmounts, 54);
		/* ERRORS */
	private String e350 = "E350";
	private String e355 = "E355";
	private String g528 = "G528";
	private String d025 = "D025";
		/* TABLES */
	private String t5606 = "T5606";
	private String t5611 = "T5611";
	private String chdrenqrec = "CHDRENQREC";
	private String covrrec = "COVRREC";
	private String liferec = "LIFEREC";
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life record*/
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Sumcalcrec sumcalcrec = new Sumcalcrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5606rec t5606rec = new T5606rec();
	private T5611rec t5611rec = new T5611rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		continue120,
		continue130,
		exit190,
		exit2190,
		exit9020
	}

	public Clcsur1() {
		super();
	}

public void mainline(Object... parmArray)
	{
		srcalcpy.surrenderRec = convertAndSetParam(srcalcpy.surrenderRec, parmArray, 0);
		try {
			main100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start110();
				}
				case continue120: {
					continue120();
				}
				case continue130: {
					continue130();
				}
				case exit190: {
					exit190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start110()
	{
		srcalcpy.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		initialisation1000();
		wsaaBaseAmount[1].set(ZERO);
		wsaaBaseAmount[2].set(ZERO);
		wsaaBaseAmount[3].set(ZERO);
		wsaaRawAmount[1].set(ZERO);
		wsaaRawAmount[2].set(ZERO);
		wsaaRawAmount[3].set(ZERO);
		if (isNE(t5611rec.indic01,SPACES)) {
			completeCopybook2100();
			callProgram(Sumres01.class, sumcalcrec.surrenderRec);
			if (isNE(sumcalcrec.status,"****")
			&& isNE(sumcalcrec.status,SPACES)) {
				srcalcpy.status.set(sumcalcrec.status);
				goTo(GotoLabel.exit190);
			}
			else {
				wsaaBaseAmount[1].set(sumcalcrec.actualVal);
				wsaaRawAmount[1].set(sumcalcrec.actualVal);
			}
		}
		if (isNE(t5611rec.indic02,SPACES)) {
			checkXplusone2300();
			if (isLTE(datcon3rec.freqFactor,1)) {
				wsaaBaseAmount[2].set(ZERO);
				wsaaRawAmount[2].set(ZERO);
			}
			else {
				completeCopybook2100();
				xplusoneChanges2200();
				callProgram(Sumres01.class, sumcalcrec.surrenderRec);
				if (isNE(sumcalcrec.status,"****")
				&& isNE(sumcalcrec.status,SPACES)) {
					srcalcpy.status.set(sumcalcrec.status);
					goTo(GotoLabel.exit190);
				}
				else {
					wsaaBaseAmount[2].set(sumcalcrec.actualVal);
					wsaaRawAmount[2].set(sumcalcrec.actualVal);
				}
			}
		}
		if (isNE(t5611rec.indic03,SPACES)) {
			if (isEQ(sumcalcrec.freqann,SPACES)
			|| isEQ(sumcalcrec.freqann,"00")) {
				compute(wsaaBaseAmount[3], 3).set((div(mult(covrIO.getSumins(),t5611rec.persassd),100)));
				wsaaRawAmount[3].set(wsaaBaseAmount[3]);
			}
			else {
				compute(wsaaBaseAmount[3], 3).set((div(mult(mult(covrIO.getSumins(),wsaaFreqann),t5611rec.persassd),100)));
				wsaaRawAmount[3].set(wsaaBaseAmount[3]);
			}
		}
		wsaaChargeAmount[1].set(ZERO);
		wsaaChargeAmount[2].set(ZERO);
		wsaaChargeAmount[3].set(ZERO);
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(srcalcpy.effdate);
		datcon3rec.intDate2.set(covrIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		datcon3rec.freqFactor.set(ZERO);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError9000();
		}
		if (isGTE(wsaaOldestLife,t5611rec.agemax)
		&& isLTE(datcon3rec.freqFactor,t5611rec.termMin)) {
			goTo(GotoLabel.continue120);
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub,3)); wsaaSub.add(1)){
			if (isGT(wsaaBaseAmount[wsaaSub.toInt()],ZERO)) {
				if (isGT(t5611rec.perbsamt[wsaaSub.toInt()],ZERO)) {
					compute(wsaaChargeAmount[wsaaSub.toInt()], 3).set((div(mult(wsaaBaseAmount[wsaaSub.toInt()],t5611rec.perbsamt[wsaaSub.toInt()]),100)));
					compute(wsaaRawAmount[wsaaSub.toInt()], 2).set((sub(wsaaBaseAmount[wsaaSub.toInt()],wsaaChargeAmount[wsaaSub.toInt()])));
				}
				else {
					if (isGT(t5611rec.persrisk[wsaaSub.toInt()],ZERO)) {
						compute(wsaaChargeAmount[wsaaSub.toInt()], 3).set((div(mult((sub(covrIO.getSumins(),wsaaBaseAmount[wsaaSub.toInt()])),t5611rec.persrisk[wsaaSub.toInt()]),100)));
						compute(wsaaRawAmount[wsaaSub.toInt()], 2).set((sub(wsaaBaseAmount[wsaaSub.toInt()],wsaaChargeAmount[wsaaSub.toInt()])));
					}
				}
			}
		}
	}

protected void continue120()
	{
		if (isNE(t5611rec.indic05,SPACES)) {
			compute(srcalcpy.actualVal, 2).set((add(add(wsaaRawAmount[1],wsaaRawAmount[2]),wsaaRawAmount[3])));
			goTo(GotoLabel.continue130);
		}
		if (isNE(t5611rec.indic04,SPACES)) {
			wsaaLowestValue.set("999999999999999");
			if (isNE(t5611rec.indic01,SPACES)
			&& isLT(wsaaRawAmount[1],wsaaLowestValue)) {
				wsaaLowestValue.set(wsaaRawAmount[1]);
			}
			if (isNE(t5611rec.indic02,SPACES)
			&& isLT(wsaaRawAmount[2],wsaaLowestValue)) {
				wsaaLowestValue.set(wsaaRawAmount[2]);
			}
			if (isNE(t5611rec.indic03,SPACES)
			&& isLT(wsaaRawAmount[3],wsaaLowestValue)) {
				wsaaLowestValue.set(wsaaRawAmount[3]);
			}
			if (isEQ(wsaaLowestValue,"999999999999999")) {
				srcalcpy.actualVal.set(ZERO);
			}
			else {
				srcalcpy.actualVal.set(wsaaLowestValue);
			}
		}
		else {
			wsaaHighestValue.set("-9999999999999999");
			if (isNE(t5611rec.indic01,SPACES)
			&& isGT(wsaaRawAmount[1],wsaaHighestValue)) {
				wsaaHighestValue.set(wsaaRawAmount[1]);
			}
			if (isNE(t5611rec.indic02,SPACES)
			&& isGT(wsaaRawAmount[2],wsaaHighestValue)) {
				wsaaHighestValue.set(wsaaRawAmount[2]);
			}
			if (isNE(t5611rec.indic03,SPACES)
			&& isGT(wsaaRawAmount[3],wsaaHighestValue)) {
				wsaaHighestValue.set(wsaaRawAmount[3]);
			}
			if (isEQ(wsaaHighestValue,"-99999999999999")) {
				srcalcpy.actualVal.set(ZERO);
			}
			else {
				srcalcpy.actualVal.set(wsaaHighestValue);
			}
		}
	}

protected void continue130()
	{
		srcalcpy.status.set(varcom.endp);
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialisation1000()
	{
		start1010();
	}

protected void start1010()
	{
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemtabl(t5611);
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		wsaaT5611Crtable.set(srcalcpy.crtable);
		if (isEQ(srcalcpy.pstatcode,SPACES)) {
			wsaaT5611Pstatcode.set(covrIO.getPstatcode());
		}
		else {
			wsaaT5611Pstatcode.set(srcalcpy.pstatcode);
		}
		itdmIO.setItemitem(wsaaT5611Item);
		itdmIO.setItmfrm(srcalcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		else {
			if (isEQ(itdmIO.getStatuz(),varcom.endp)
			|| isNE(itdmIO.getItemtabl(),t5611)
			|| isNE(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
			|| isNE(itdmIO.getItemitem(),wsaaT5611Item)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(d025);
				fatalError9000();
			}
			else {
				t5611rec.t5611Rec.set(itdmIO.getGenarea());
			}
		}
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		chdrenqIO.setChdrnum(srcalcpy.chdrChdrnum);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)
		&& isNE(chdrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(chdrenqIO.getStatuz(),varcom.oK)) {
			while ( !((isEQ(chdrenqIO.getStatuz(),varcom.oK)
			&& isEQ(chdrenqIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
			&& isEQ(chdrenqIO.getChdrnum(),srcalcpy.chdrChdrnum)
			&& isEQ(chdrenqIO.getValidflag(),"1"))
			|| isEQ(chdrenqIO.getStatuz(),varcom.endp))) {
				loopChdrenq3200();
			}

		}
		if (isEQ(chdrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError9000();
		}
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(chdrenqIO.getChdrcoy());
		covrIO.setChdrnum(chdrenqIO.getChdrnum());
		covrIO.setLife(srcalcpy.lifeLife);
		covrIO.setCoverage(srcalcpy.covrCoverage);
		covrIO.setRider(srcalcpy.covrRider);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(covrrec);
		covrIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(covrIO.getStatuz(),varcom.oK)) {
			while ( !((isEQ(covrIO.getStatuz(),varcom.oK)
			&& isEQ(covrIO.getChdrcoy(),chdrenqIO.getChdrcoy())
			&& isEQ(covrIO.getChdrnum(),chdrenqIO.getChdrnum())
			&& isEQ(covrIO.getLife(),srcalcpy.lifeLife)
			&& isEQ(covrIO.getCoverage(),srcalcpy.covrCoverage)
			&& isEQ(covrIO.getRider(),srcalcpy.covrRider)
			&& isEQ(covrIO.getValidflag(),"1"))
			|| isEQ(covrIO.getStatuz(),varcom.endp))) {
				loopCovr3300();
			}

		}
		if (isEQ(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError9000();
		}
		else {
			if (isGT(covrIO.getBenCessTerm(),ZERO)
			&& isLT(covrIO.getBenCessTerm(),120)) {
				getBenfreq3100();
				sumcalcrec.freqann.set(t5606rec.benfreq);
				wsaaFreqann.set(t5606rec.benfreq);
			}
			else {
				sumcalcrec.freqann.set("00");
				wsaaFreqann.set(ZERO);
			}
		}
	}

protected void completeCopybook2100()
	{
		try {
			start2110();
		}
		catch (GOTOException e){
		}
	}

protected void start2110()
	{
		sumcalcrec.surrenderRec.set(srcalcpy.surrenderRec);
		if (isGT(covrIO.getSingp(),ZERO)) {
			sumcalcrec.singp.set(covrIO.getSingp());
		}
		else {
			sumcalcrec.singp.set(ZERO);
		}
		sumcalcrec.trancde.set("****");
		sumcalcrec.sumin.set(covrIO.getSumins());
		sumcalcrec.duration.set(covrIO.getPremCessTerm());
		sumcalcrec.riskCessTerm.set(covrIO.getRiskCessTerm());
		sumcalcrec.benCessTerm.set(covrIO.getBenCessTerm());
		sumcalcrec.chdrtype.set(chdrenqIO.getCnttype());
		sumcalcrec.calcPrem.set(ZERO);
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeIO.setLife(srcalcpy.lifeLife);
		lifeIO.setJlife("00");
		lifeIO.setCurrfrom(varcom.maxdate);
		lifeIO.setFormat(liferec);
		lifeIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)
		&& isNE(lifeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(lifeIO.getStatuz(),varcom.oK)
		&& isEQ(lifeIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		&& isEQ(lifeIO.getChdrnum(),chdrenqIO.getChdrnum())
		&& isEQ(lifeIO.getLife(),srcalcpy.lifeLife)
		&& isEQ(lifeIO.getJlife(),"00")) {
			sumcalcrec.lsex.set(lifeIO.getCltsex());
			sumcalcrec.lage.set(lifeIO.getAnbAtCcd());
			wsaaOldestLife.set(lifeIO.getAnbAtCcd());
		}
		else {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(e355);
			fatalError9000();
		}
		if (isEQ(srcalcpy.lifeJlife,SPACES)
		|| isEQ(srcalcpy.lifeJlife,"00")) {
			sumcalcrec.jlsex.set(SPACES);
			sumcalcrec.jlage.set(999);
			goTo(GotoLabel.exit2190);
		}
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeIO.setLife(srcalcpy.lifeLife);
		lifeIO.setJlife(srcalcpy.lifeJlife);
		lifeIO.setCurrfrom(varcom.maxdate);
		lifeIO.setFormat(liferec);
		lifeIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)
		&& isNE(lifeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(lifeIO.getStatuz(),varcom.oK)
		&& isEQ(lifeIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		&& isEQ(lifeIO.getChdrnum(),chdrenqIO.getChdrnum())
		&& isEQ(lifeIO.getLife(),srcalcpy.lifeLife)
		&& isEQ(lifeIO.getJlife(),srcalcpy.lifeJlife)) {
			sumcalcrec.jlsex.set(lifeIO.getCltsex());
			sumcalcrec.jlage.set(lifeIO.getAnbAtCcd());
			if (isGT(lifeIO.getAnbAtCcd(),wsaaOldestLife)) {
				wsaaOldestLife.set(lifeIO.getAnbAtCcd());
			}
		}
		else {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(e350);
			fatalError9000();
		}
	}

protected void xplusoneChanges2200()
	{
		start2210();
	}

protected void start2210()
	{
		sumcalcrec.function.set("XPLUS");
		if (isGT(sumcalcrec.lage,ZERO)
		&& isLT(sumcalcrec.lage,120)) {
			compute(sumcalcrec.lage, 0).set(sub(sumcalcrec.lage,1));
		}
		if (isGT(sumcalcrec.jlage,ZERO)
		&& isLT(sumcalcrec.jlage,120)) {
			compute(sumcalcrec.jlage, 0).set(sub(sumcalcrec.jlage,1));
		}
		else {
			sumcalcrec.jlage.set(ZERO);
		}
		if (isGT(sumcalcrec.duration,1)
		&& isLT(sumcalcrec.duration,99)) {
			compute(sumcalcrec.duration, 0).set(sub(sumcalcrec.duration,1));
		}
		else {
			sumcalcrec.duration.set(ZERO);
		}
		if (isGT(sumcalcrec.riskCessTerm,1)
		&& isLT(sumcalcrec.riskCessTerm,100)) {
			compute(sumcalcrec.riskCessTerm, 0).set(sub(sumcalcrec.riskCessTerm,1));
		}
		else {
			sumcalcrec.riskCessTerm.set(ZERO);
		}
		if (isGT(sumcalcrec.benCessTerm,1)
		&& isLT(sumcalcrec.benCessTerm,100)) {
			compute(sumcalcrec.benCessTerm, 0).set(sub(sumcalcrec.benCessTerm,1));
		}
		else {
			sumcalcrec.benCessTerm.set(ZERO);
		}
	}

protected void checkXplusone2300()
	{
		/*START*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(srcalcpy.crrcd);
		datcon3rec.intDate2.set(srcalcpy.effdate);
		datcon3rec.frequency.set("01");
		datcon3rec.freqFactor.set(ZERO);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError9000();
		}
		/*EXIT*/
	}

protected void getBenfreq3100()
	{
		start3110();
	}

protected void start3110()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemtabl(t5606);
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		wsaaT5606Crtable.set(srcalcpy.crtable);
		wsaaT5606Unknown.set("*");
		wsaaT5606Currcode.set(srcalcpy.currcode);
		itdmIO.setItemitem(wsaaT5606Item);
		itdmIO.setItmfrm(srcalcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		else {
			if (isEQ(itdmIO.getStatuz(),varcom.endp)
			|| isNE(itdmIO.getItemtabl(),t5606)
			|| isNE(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
			|| isNE(itdmIO.getItemitem(),wsaaT5606Item)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(g528);
				fatalError9000();
			}
			else {
				t5606rec.t5606Rec.set(itdmIO.getGenarea());
			}
		}
	}

protected void loopChdrenq3200()
	{
		/*START*/
		chdrenqIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)
		&& isNE(chdrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(chdrenqIO.getStatuz(),varcom.endp)
		|| isNE(chdrenqIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(chdrenqIO.getChdrnum(),srcalcpy.chdrChdrnum)) {
			chdrenqIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void loopCovr3300()
	{
		/*START*/
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(covrIO.getStatuz(),varcom.endp)
		|| isNE(covrIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(),chdrenqIO.getChdrnum())
		|| isNE(covrIO.getLife(),srcalcpy.lifeLife)
		|| isNE(covrIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(covrIO.getRider(),srcalcpy.covrRider)) {
			covrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void fatalError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9010();
				}
				case exit9020: {
					exit9020();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9010()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit9020);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		srcalcpy.status.set(syserrrec.statuz);
		/*EXIT*/
		exitProgram();
	}
}
