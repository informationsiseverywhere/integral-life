/*
 * File: Bh5gk.java
 */
package com.csc.life.general.batchprograms;

import static com.csc.smart.recordstructures.Varcom.endp;
import static com.csc.smart.recordstructures.Varcom.oK;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.financials.dataaccess.dao.RbnkpfDAO;
import com.csc.fsu.financials.dataaccess.dao.model.Rbnkpf;
import com.csc.fsu.financials.procedures.Addrtrn;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.DactpfDAO;
import com.csc.fsu.general.dataaccess.dao.DddepfDAO;
import com.csc.fsu.general.dataaccess.dao.DshnpfDAO;
import com.csc.fsu.general.dataaccess.dao.GcrtpfDAO;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Dactpf;
import com.csc.fsu.general.dataaccess.model.Dddepf;
import com.csc.fsu.general.dataaccess.model.Dshnpf;
import com.csc.fsu.general.dataaccess.model.Gcrtpf;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.fsu.general.recordstructures.Addrtrnrec;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Cashtypcpy;
import com.csc.fsu.general.recordstructures.Pr21zpar;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.general.tablestructures.T3678rec;
import com.csc.fsu.general.tablestructures.T3688rec;
import com.csc.fsu.general.tablestructures.T3698rec;
import com.csc.fsu.general.tablestructures.Tr22arec;
import com.csc.fsu.general.tablestructures.Tr22brec;
import com.csc.fsu.segment.dataaccess.dao.DescpfDAO;
import com.csc.fsu.segment.dataaccess.dao.impl.DescpfDAOImpl;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Bh5gk extends Mainb {

    public static final String ROUTINE = QPUtilities.getThisClass();
    public int numberOfParameters = 0;
    private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Bh5gk");
    private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
    private ZonedDecimalData wsaaLine = new ZonedDecimalData(6, 0).setUnsigned();
    private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
    private Alocnorec alocnorec = new Alocnorec();
    private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
    private Cashtypcpy cashtypcpy = new Cashtypcpy();
    private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

    private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);

    private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(9);
    private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER)
	    .init("THREAD");
    private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

    private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
    private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

    private static final Logger LOGGER = LoggerFactory.getLogger(Bh5gk.class);

    private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
    private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
    private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
    private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
    private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
    private ZonedDecimalData wsaaAddrJrnseq = new ZonedDecimalData(4, 0).setUnsigned();
    private FixedLengthStringData wsaaAddrTranseq = new FixedLengthStringData(4).isAPartOf(wsaaAddrJrnseq, 0, REDEFINE);
    private T3688rec t3688rec = new T3688rec();
    private T3698rec t3698rec = new T3698rec();
    private Addrtrnrec addrtrnrec = new Addrtrnrec();
    List<Gcrtpf> gcrtpfList = null;
    HashMap<String, Dddepf> dddepfMap = null;
    List<Itempf> itempfList = null;
    private final int wsaaMaxCnt = 500;
    List<Itempf> itempfTr22aList = null;
    private GcrtpfDAO gcrtpfDAO = getApplicationContext().getBean("gcrtpfDAO", GcrtpfDAO.class);
    private LinspfDAO linspfDAO = getApplicationContext().getBean("linspfDAO", LinspfDAO.class);
    private DddepfDAO dddepfDAO = getApplicationContext().getBean("dddepFDAO", DddepfDAO.class);
    protected ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
    protected DshnpfDAO dshnpfDao = getApplicationContext().getBean("dshnpfDao", DshnpfDAO.class);
    private DactpfDAO dactpfDAO = getApplicationContext().getBean("dactpfDAO", DactpfDAO.class);
    private DescpfDAO descpfDAO = new DescpfDAOImpl();
    private StringBuilder tableName = null;
    private Pr21zpar pr21zpar = new Pr21zpar();
    private T3629rec t3629rec = new T3629rec();
    private Tr22arec tr22arec = new Tr22arec();
    private Tr22brec tr22brec = new Tr22brec();
    Map<String,String> responseMessageMap = new HashMap<>();
    protected T3678rec t3678rec = new T3678rec();
    private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
    private FixedLengthStringData wsaaFilePath = new FixedLengthStringData(50);
    private PackedDecimalData wsaaNominalRate = new PackedDecimalData(18, 9).setUnsigned();
    private PackedDecimalData wsaaX = new PackedDecimalData(3, 0);
    List<Itempf> item3629list;
    private FixedLengthStringData wsaaDoctkey = new FixedLengthStringData(10);
    private FixedLengthStringData wsaaRdoccoy = new FixedLengthStringData(1).isAPartOf(wsaaDoctkey, 0);
    private FixedLengthStringData wsaaRdocnum = new FixedLengthStringData(9).isAPartOf(wsaaDoctkey, 1);
    private FixedLengthStringData wsaaSign = new FixedLengthStringData(1);
    private List<Itempf> itempfTr22bList;
    private int counter =0;
    private Gcrtpf gcrtpf;
    private Iterator<Gcrtpf> iterator;
    private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);//IBPLIFE-8679

    public Bh5gk() {
	super();
    }

    protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
    }

    protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
    }

    protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
    }

    protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
    }

    protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
    }

    protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
    }

    protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
    }

    protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
    }

    protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
    }

    protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
    }

    protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
    }

    protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
    }

    protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
    }

    protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
    }

    /**
     * The mainline method is the default entry point of the program when called by
     * other programs using the Quipoz runtime framework.
     */
    @Override
    public void mainline(Object... parmArray) {
	lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
	lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
	lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
	lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
	lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
	try {
	    super.mainline();
	} catch (COBOLExitProgramException e) {
	    // Expected exception for control flow purposes
	}
    }

    protected void restart0900() {
	/* RESTART */
	/** Place any additional restart processing in here. */
	/* EXIT */
    }

    protected void initialise1000() {
	initialise1010();
    }

    protected void initialise1010() {
//    	softLock();
	wsspEdterror.set(oK);
	tableName = new StringBuilder();
	tableName.append(bprdIO.getSystemParam01().trim());
	tableName.append(bprdIO.getSystemParam03().trim());
	tableName.append(String.format("%04d", bsscIO.getScheduleNumber().toInt()));
	readTTables();
	prepareResponseMessageMap();
	getGcrtpfData();
	prepareDddepfMap();
	readT3684();
	iterator = gcrtpfList.iterator();
    }

    protected void readFile2000() {
	readFile2010();
	
    }

    protected void readFile2010() {
	if(iterator.hasNext()) {
	    gcrtpf = iterator.next();
	}else {
	    wsspEdterror.set(endp);
	}
	
    }

    private void prepareResponseMessageMap() {

	for (int i = 0; i < itempfTr22aList.size(); i++) {
	    Itempf tr22a = itempfTr22aList.get(i);
	    tr22arec.tr22aRec.set(StringUtil.rawToString(tr22a.getGenarea()));
	    for (int x = 2; x<25 && !( isEQ(tr22arec.resptext[x], SPACES)); x++) { //PINNACLE-1938
		responseMessageMap.put(tr22arec.respcode[x].trim(), getResponseText(tr22arec.respcode[x].trim()));
	    }
	}
    }

    private String getResponseText(String x) {
	for(Itempf item:itempfTr22bList) {
	    if(item.getItemitem().trim().equalsIgnoreCase("E96"+x)) {
		tr22brec.tr22bRec.set(StringUtil.rawToString(item.getGenarea()));
		return tr22brec.longdsc01.trim();
	    }
	}
	return "****";
    }
    
    protected String getRespondText(Integer responseCode)
    {
	//PINNACLE-2582
	StringBuilder dishonorCode = new StringBuilder(responseCode +"");
	while(dishonorCode.length()<2) {
	    dishonorCode.insert(0, "0");
	}
    	if(responseMessageMap.containsKey(dishonorCode.toString()))
    		return responseMessageMap.get(dishonorCode.toString());
    	else
    		return "*****";
    }

    protected void readT3684() {
	itempfList = itempfDAO.getItempf("IT", bsprIO.getFsuco().toString(), "T3678", "10");
	if (!itempfList.isEmpty()) {
	    t3678rec.t3678Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
	}
    }
    
    

    protected void prepareDddepfMap() {
	dddepfMap = new HashMap<String, Dddepf>();
	for (Gcrtpf gcrtpf1 : gcrtpfList) {
	    getDddepfData(gcrtpf1);
	}
    }

    protected void getGcrtpfData() {
	// read temp table data
	gcrtpfList = gcrtpfDAO.getAllDataFromTempTable(tableName.toString());
    }

    protected void getDddepfData(Gcrtpf gcrtpf) {
	// read data from dddepf
	Dddepf dddepf = new Dddepf();
	dddepf.setChdrnum(gcrtpf.getChdrnum());
	dddepf.setInstamt06(BigDecimal.valueOf(gcrtpf.getAmount()));
	Dddepf dddepfObj = dddepfDAO.searchDddepfByChdrAndJobnm(dddepf);
	if (null != dddepfObj) {
	    dddepfMap.put(gcrtpf.getChdrnum(), dddepfObj);
	}
    }

    protected void edit2500() {
    	/* EDIT CODE GOES HERE */
    }

    protected void update3000() {
	update3010();
	counter++;
    }

    protected void update3010() {
	
	    if (dddepfMap.containsKey(gcrtpf.getChdrnum())) {
		writeDshnpf(gcrtpf);
		updateDishnr(gcrtpf.getChdrnum()); //IBPLIFE-8679
	    }
	    updateRtrnpf(gcrtpf);
	    updateDactpf(gcrtpf);
    }

    protected void writeDshnpf(Gcrtpf gcrtpf) {
    	Dddepf dddepf = dddepfMap.get(gcrtpf.getChdrnum());
    	if(dddepf == null) {
    		return ;
    	}
    	Double instamt06 = Double.parseDouble(dddepf.getInstamt06()+"");
    	//Ticket #PINNACLE - 1363 starts
    	List<Linspf> linsRecordByCoyAndNumAndflag = linspfDAO.getPaidLinspfRecordList(dddepf.getChdrnum().trim());
    	for(Linspf lins:linsRecordByCoyAndNumAndflag) {
    	 if(instamt06.intValue()<Double.valueOf(Double.parseDouble(lins.getInstamt06()+"")).intValue()) {
    			break;
    		}
    		instamt06 = instamt06 - Double.parseDouble(lins.getInstamt06()+"");
    		//Ticket #PINNACLE - 1363 starts
    		Dshnpf dshnpf = new Dshnpf();
    		dshnpf.setSubdat(0);
    		dshnpf.setPayrcoy(dddepf.getPayrcoy());
    		dshnpf.setPayrnum(dddepf.getPayrnum());
    		dshnpf.setCompany(dddepf.getCompany());
    		dshnpf.setJobno(dddepf.getJobno());
    		dshnpf.setDhnflag("2");
    		dshnpf.setLapday(t3678rec.lapseper.toByte());
    		/* MOVE S3284-LAPDAY TO DSHNLF1-LAPDAY. */
    		dshnpf.setEffdate(bsscIO.getEffectiveDate().toInt());
    		 dshnpf.setMandref("");
    		dshnpf.setBankkey(dddepf.getBankkey());
    		dshnpf.setBankacckey(dddepf.getBankacckey());
    		
    		dshnpf.setBillcd(lins.getBillcd()); //LINS
    		
    		dshnpf.setChdrcoy(dddepf.getChdrcoy());
    		dshnpf.setChdrnum(dddepf.getChdrnum());
    		dshnpf.setServunit(dddepf.getServunit());
    		dshnpf.setCnttype(dddepf.getCnttype());
    		dshnpf.setCntcurr(dddepf.getCntcurr());
    		dshnpf.setOccdate(dddepf.getOccdate());
    		dshnpf.setCcdate(dddepf.getCcdate());
    		
    		dshnpf.setPtdate(dddepf.getPtdate());
    		dshnpf.setBtdate(dddepf.getBtdate());
    		
    		dshnpf.setBilldate(dddepf.getBilldate());
    		dshnpf.setBillchnl(dddepf.getBillchnl());
    		dshnpf.setBankcode(dddepf.getBankcode());
    		
    		dshnpf.setInstfrom(lins.getInstfrom()); //LINS
    		dshnpf.setInstto(lins.getInstto()); //LINS
    		
    		dshnpf.setInstbchnl(dddepf.getInstbchnl());
    		dshnpf.setInstcchnl(dddepf.getInstcchnl());
    		
    		dshnpf.setInstfreq(lins.getInstfreq()); //LINS
    		
    		dshnpf.setInstamt01(lins.getInstamt01());
    		dshnpf.setInstamt02(lins.getInstamt02());
    		dshnpf.setInstamt03(lins.getInstamt03());
    		dshnpf.setInstamt04(lins.getInstamt04());
    		dshnpf.setInstamt05(lins.getInstamt05());
    		dshnpf.setInstamt06(lins.getInstamt06());

    		dshnpf.setGrupkey(dddepf.getGrupkey());
    		dshnpf.setMembsel(dddepf.getMembsel());
    		dshnpf.setFacthous(dddepf.getFacthous());
    		dshnpf.setCowncoy(dddepf.getCowncoy());
    		dshnpf.setCownnum(dddepf.getCownnum());
    		dshnpf.setCntbranch(dddepf.getCntbranch());
    		dshnpf.setAgntpfx(dddepf.getAgntpfx());
    		dshnpf.setAgntcoy(dddepf.getAgntcoy());
    		dshnpf.setAgntnum(dddepf.getAgntnum());
    		dshnpf.setPayflag(dddepf.getPayflag());
    		dshnpf.setBilflag(dddepf.getBilflag());
    		dshnpf.setOutflag(dddepf.getOutflag());
    		dshnpf.setSupflag(dddepf.getSupflag());
    		dshnpf.setSacscode(dddepf.getSacscode());
    		dshnpf.setSacstyp(dddepf.getSacstyp());
    		dshnpf.setGlmap(dddepf.getGlmap());

    		dshnpf.setProcaction("2");
    		// dshnpf.setFdducdt(sv.fdducdt); ??
    		dshnpf.setResptext(getRespondText(Integer.parseInt(gcrtpf.getReason().trim())).substring(0, 5)); // DACT //PINNACLE-1938
    		dshnpf.setRespcode("R"); // DACT
    		dshnpf.setMandref("");
    		/* Write record to DSHNLF1. */
    		//added extra space for jenkin failure
			if (dshnpfDao.getDshnpfRecord(dshnpf)) {
    			//Next_Statement
    		} else {
    			dshnpfDao.writeIntoDshnpf(dshnpf);
			}	
    	}
	

    }
	
	protected void updateDishnr(String chdrnum) {
		Chdrpf chdr = chdrpfDAO.getLastestChdrByChdrnum(chdrnum);
		if (chdr != null) {
			chdrpfDAO.updateDishnrcnt(chdrnum, chdr.getDishnrcnt()==null?1:chdr.getDishnrcnt()+1);
		}
	}

    protected void updateRtrnpf(Gcrtpf gcrtpf) {
	generateFirstRtrn(gcrtpf);
	generateSecondRtrn(gcrtpf);

    }

    private void readTTables() {
	/* Read T3629 for bank code. */
	item3629list = itempfDAO.getAllItemitem("IT", bprdIO.getCompany().toString(), "T3629", "AUD");
	if (CollectionUtils.isEmpty(item3629list)) {
	    fatalError600();
	}
	t3629rec.t3629Rec.set(StringUtil.rawToString(item3629list.get(0).getGenarea()));

	List<Itempf> item3688list = itempfDAO.getAllItemitem("IT", bprdIO.getCompany().toString(), "T3688",
		t3629rec.bankcode.trim());
	if (CollectionUtils.isEmpty(item3688list)) {
	    fatalError600();
	}
	t3688rec.t3688Rec.set(StringUtil.rawToString(item3688list.get(0).getGenarea()));

	List<Itempf> item3698list = itempfDAO.getAllItemitem("IT", bprdIO.getCompany().toString(), "T3698", "T350BK");

	if (CollectionUtils.isEmpty(item3698list)) {
	    fatalError600();
	} else {
	    t3698rec.t3698Rec.set(StringUtil.rawToString(item3698list.get(0).getGenarea()));
	}
	
	itempfTr22aList = itempfDAO.getItempf("IT", bsprIO.getFsuco().toString(), "TR22A", "96");
	
	itempfTr22bList = itempfDAO.getItempfLikeItemItem("IT", bsprIO.getFsuco().toString(), "TR22B", "E96");
	
    }

    private void generateFirstRtrn(Gcrtpf gcrtpf) {
	addrtrnrec.addrtrnRec.set(SPACES);
	Dddepf dddepf = dddepfMap.get(gcrtpf.getChdrnum());
	if(dddepf == null) {
		return ;
	}
	Descpf descpf = descpfDAO.getItem("T3688", bprdIO.getCompany().toString(), "IT", "21");
	if (descpf == null) {
	    fatalError600();
	}
	/* Allocate next automatic Receipt Number. */
	alocnorec.function.set("NEXT ");
	alocnorec.prefix.set("CA");
	/* MOVE T3629-BANKCODE TO ALNO-GENKEY . <PAX908> */
	if (isEQ(t3688rec.bbalpfx, SPACES)) {
	    alocnorec.genkey.set("21");
	} else {
	    alocnorec.genkey.set(t3688rec.bbalpfx);
	}
	alocnorec.company.set(bprdIO.getCompany().trim());
	callProgram(Alocno.class, alocnorec.alocnoRec);
	if (isNE(alocnorec.statuz, oK)) {
	    syserrrec.statuz.set(alocnorec.statuz);
	    fatalError600();
	}
	wsaaRdocnum.set(alocnorec.alocNo);
	wsaaRdoccoy.set(bprdIO.getCompany().trim());
	setGLSignFromCashtypcpy1();
	setFields11();
	
	/* MOVE WSSP-ACCTYEAR TO ADDR-BATCACTYR. */
	addrtrnrec.batcactyr.set(bsscIO.getAcctMonth());
	addrtrnrec.postyear.set(bsscIO.getAcctYear());
	/* MOVE WSSP-ACCTMONTH TO ADDR-BATCACTMN. */
	addrtrnrec.batcactmn.set(bsscIO.getAcctMonth());
	addrtrnrec.postmonth.set(bsscIO.getAcctMonth());
	setValuesToBatcDorRec(dddepf);
	addrtrnrec.batckey.set(batcdorrec.batchkey);
	/* MOVE BATD-BATCH TO ADDR-BATCBATCH. */
	addrtrnrec.batcbrn.set(bprdIO.getDefaultBranch().trim());
	/* MOVE 'CA' TO ADDR-SACSCODE. */
	addrtrnrec.sacscode.set("CN");
	/* MOVE '1 ' TO ADDR-SACSTYP */
	addrtrnrec.sacstyp.set(SPACES);
	if (isNE(dddepf.getInstamt06(), ZERO)) {
	    /* MOVE '3 ' TO ADDR-SACSTYP <V76F13> */
	    addrtrnrec.sacstyp.set(cashtypcpy.cashtype);
	} else {
	    addrtrnrec.sacstyp.set("4 ");
	}
	/*
	 * addrtrnrec.sacscode.set(dddepf.getSacscode());
	 * addrtrnrec.sacstyp.set(dddepf.getSacstyp());
	 */
	addrtrnrec.batctrcde.set("T350");
	/* MOVE WSAA-CURRCODE TO LIFR-ORIGCURR. */
	addrtrnrec.origccy.set("AUD");
	/* MOVE WSAA-CURRCODE TO ADDR-GENLCUR. <CAS1.0> */
	addrtrnrec.genlcur.set(t3629rec.ledgcurr);
	addrtrnrec.genlpfx.set(fsupfxcpy.genl);
	addrtrnrec.origamt.set(dddepf.getInstamt06());
	compute(addrtrnrec.origamt, 2).set(mult(addrtrnrec.origamt, -1));
	/* MOVE 0 TO LIFR-RCAMT */
	/* LIFR-CRATE */
	addrtrnrec.tranno.set(0);
	/* MOVE VRCM-MAX-DATE TO LIFR-FRCDATE. */
	addrtrnrec.effdate.set(bsscIO.getEffectiveDate());
	/* MOVE WSAA-PAYRNUM TO LIFR-TRANREF. */
	addrtrnrec.glsign.set(wsaaSign);
	addrtrnrec.bankcode.set(t3629rec.bankcode);
	/*--Get GL Account Key to read T3688 Credit Card GL account<V76F13>*/
	/*--if Payment type = '9' Credit card.                     <V76F13>*/
	if (cashtypcpy.creditCard.isTrue()) {
	    addrtrnrec.glcode.set(t3688rec.glmap09);
	} else {
	    addrtrnrec.glcode.set(t3688rec.glmap01);
	}
	/* MOVE VRCM-USER TO LIFR-USER. */
	/* MOVE T3688-CDCONTOT TO ADDR-CONTOT. */
	addrtrnrec.contot.set(t3698rec.cnttot01);
	if(descpf!=null)
	addrtrnrec.trandesc.set(descpf.getLongdesc());
	/* MOVE T3688-BANKACCKEY TO ADDR-RLDGACCT. */
	addrtrnrec.rldgacct.set(dddepf.getPayrnum().trim());
	addrtrnrec.rldgcoy.set(dddepf.getPayrcoy().trim());
	/* MOVE S3284-EFFDATE TO LIFR-TRANSACTION-DATE. */
	/* MOVE VRCM-TIME TO LIFR-TRANSACTION-TIME. */
	/* MOVE T3629-SCRATE-01 TO ADDR-CURRRATE. */
	//PINNACLE-2373
	wsaaNominalRate.set(0);
	wsaaX.set(1);
	while (!(isNE(wsaaNominalRate, ZERO) || isGT(wsaaX, 7))) {
	    findRate6150();
	}
	addrtrnrec.currrate.set(wsaaNominalRate);
	/* MOVE T3688-CURRCODE TO ADDR-ACCTCCY. <008> */
	compute(addrtrnrec.acctamt, 9).set(mult(addrtrnrec.origamt, addrtrnrec.currrate));
	addrtrnrec.function.set("NPSTW");
	//addrtrnrec.mode.set("BATCH");
	callProgram(Addrtrn.class, addrtrnrec.addrtrnRec);
	if (isNE(addrtrnrec.statuz, oK)) {
	    syserrrec.statuz.set(addrtrnrec.statuz);
	    fatalError600();
	}
    }

    private void setFields11() {
    	/* Set up ADDRTRN parameters and call 'ADDRTRN' to post the cash */
    	/* to the bank account.(In billing currency) */
    	addrtrnrec.addrtrnRec.set(SPACES);
    	/* MOVE WSAA-PAYRNUM TO ADDR-RDOCNUM. */
    	/* MOVE WSAA-PAYRCOY TO ADDR-RDOCCOY. <V4G411> */
    	addrtrnrec.rdocnum.set(wsaaRdocnum);
    	addrtrnrec.rdoccoy.set(wsaaRdoccoy);
    	/* MOVE WSAA-SEQUENCE-NO TO LIFR-JRNSEQ. */
    	addrtrnrec.transeq.set(wsaaAddrTranseq);	
    	wsaaAddrJrnseq.add(1);
    	/* MOVE PRFX-CLNT TO ADDR-RDOCPFX <V4G411> */
    	/* ADDR-RLDGPFX. <V4G411> */
    	addrtrnrec.rdocpfx.set("CA");
    	addrtrnrec.rldgpfx.set(fsupfxcpy.clnt);
    	addrtrnrec.batccoy.set(bprdIO.getCompany().trim());
    	addrtrnrec.rldgcoy.set(bprdIO.getCompany().trim());
    	addrtrnrec.genlcoy.set(bprdIO.getCompany().trim());

	}

	private void setGLSignFromCashtypcpy1() {
    	/* Move opposite sign into work field before calling RTRN */
    	/* IF T3688-SIGN-01 = '+' */
    	/* MOVE '-' TO WSAA-SIGN */
    	/* ELSE */
    	/* MOVE '+' TO WSAA-SIGN */
    	/* END-IF. */
    	/* Do move opposite sign as we reverse the amount sign. */
    	/*--Get GL Sign to read T3688 Credit Card GL account if Payment    */
    	/*--type = '9' Credit card.                                        */
    	if (cashtypcpy.creditCard.isTrue()) {
    	    if (isEQ(t3688rec.sign09, "+")) {
    		wsaaSign.set("+");
    	    } else {
    		wsaaSign.set("-");
    	    }
    	} else {
    	    if (isEQ(t3688rec.sign01, "+")) {
    		wsaaSign.set("+");
    	    } else {
    		wsaaSign.set("-");
    	    }
    	}		
	}

	private void setValuesToBatcDorRec(Dddepf dddepf) {

	batcdorrec.function.set("AUTO");
	batcdorrec.company.set(bsprIO.getCompany());
	batcdorrec.branch.set(dddepf.getCntbranch());
	batcdorrec.actyear.set(bsscIO.getAcctYear());
	batcdorrec.actmonth.set(bsscIO.getAcctMonth());
	batcdorrec.trcde.set("T350");
	callProgram(Batcdor.class, batcdorrec.batcdorRec);
	if (isNE(batcdorrec.statuz, oK)) {
		syserrrec.params.set(batcdorrec.batcdorRec);
		syserrrec.statuz.set(batcdorrec.statuz);
		fatalError600();
	}	
    }

    private void generateSecondRtrn(Gcrtpf gcrtpf) {
	Dddepf dddepf = dddepfMap.get(gcrtpf.getChdrnum());
	if(dddepf == null) {
		return ;
	}
	Descpf descpf = descpfDAO.getItem("T1688", bprdIO.getCompany().toString(), "IT", "T350");
	if (descpf == null) {
	    fatalError600();
	} else {
	addrtrnrec.trandesc.set(descpf.getLongdesc());
	}
	addrtrnrec.origamt.set(dddepf.getInstamt06());
	compute(addrtrnrec.origamt, 2).set(mult(addrtrnrec.origamt, -1));
	/* MOVE WSAA-SEQUENCE-NO TO ADDR-JRNSEQ. */
	/* ADD 1 TO WSAA-SEQUENCE-NO. */
	addrtrnrec.transeq.set(wsaaAddrTranseq);
	wsaaAddrJrnseq.add(1);
	/* MOVE PRFX-CHDR TO ADDR-RDOCPFX <V4G411> */
	/* ADDR-RLDGPFX. <V4G411> */
	addrtrnrec.rdocpfx.set("CA");
	addrtrnrec.rldgpfx.set(fsupfxcpy.chdr);
	addrtrnrec.batccoy.set(dddepf.getChdrcoy());
	addrtrnrec.rldgcoy.set(dddepf.getChdrcoy());
	addrtrnrec.rdoccoy.set(dddepf.getChdrcoy());
	addrtrnrec.genlcoy.set(dddepf.getChdrcoy());
	addrtrnrec.sacscode.set(dddepf.getSacscode());
	addrtrnrec.sacstyp.set(dddepf.getSacstyp());
	if (isEQ(dddepf.getServunit(), "GP") && isNE(dddepf.getMembsel(), SPACES)) {
	    StringUtil stringVariable1 = new StringUtil();
	    stringVariable1.addExpression(dddepf.getChdrnum(), SPACES);
	    stringVariable1.addExpression("-", SPACES);
	    stringVariable1.addExpression(dddepf.getMembsel(), SPACES);
	    stringVariable1.setStringInto(addrtrnrec.rldgacct);
	} else {
	    addrtrnrec.rldgacct.set(dddepf.getChdrnum());
	}
	/* MOVE DDDELF1-CHDRNUM TO ADDR-RLDGACCT. */
	/* MOVE DDDELF1-CHDRNUM TO ADDR-RDOCNUM. */
	/* Allocate next automatic Receipt Number. */
	alocnorec.function.set("NEXT ");
	alocnorec.prefix.set("CA");
	/* MOVE T3629-BANKCODE TO ALNO-GENKEY . <PAX908> */
	wsaaNominalRate.set(0);
	wsaaX.set(1);
	while (!(isNE(wsaaNominalRate, ZERO) || isGT(wsaaX, 7))) {
	    findRate6150();
	}

	addrtrnrec.rdocnum.set(wsaaRdocnum);

	addrtrnrec.glcode.set(dddepf.getGlmap());
	/* MOVE DDDELF1-CNTCURR TO ADDR-GENLCUR. <CAS1.0> */
	addrtrnrec.genlcur.set(t3629rec.ledgcurr);
	addrtrnrec.genlpfx.set(fsupfxcpy.genl);
	/* MOVE T5645-SIGN-01 TO LIFR-GLSIGN. */
	/* MOVE T5645-CNTTOT-01 TO LIFR-CONTOT. */
	/* IF T3688-SIGN-01 = '+' <V64F08> */
	/* MOVE '+' TO ADDR-GLSIGN <V64F08> */
	/* ELSE <V64F08> */
	/* MOVE '-' TO ADDR-GLSIGN. <V64F08> */
	/* MOVE '-' TO ADDR-GLSIGN <003> */
	/* ELSE <003> */
	/* MOVE '+' TO ADDR-GLSIGN. <003> */
	/* MOVE T3688-CDCONTOT TO ADDR-CONTOT. <A06345> */
	/*--Get GL Sign to read T3688 Credit Card GL account if Payment    */
	/*--type = '9' Credit card.                                        */
	RbnkpfDAO rbnkDao = getApplicationContext().getBean("rbnkpfDAO", RbnkpfDAO.class);
	List<Rbnkpf> rbnkRecords = rbnkDao.getRbnkRecords(dddepf.getRdocpfx(), dddepf.getRdoccoy(),
		dddepf.getRdocnum());
	if (CollectionUtils.isEmpty(rbnkRecords)) {
	    fatalError600();
	}
	cashtypcpy.cashtype.set(rbnkRecords.get(0).getPaytype());
	setGLSignfromCashtypcpy2();
	addrtrnrec.contot.set(t3698rec.cnttot01);
	/* MOVE T3629-SCRATE-01 TO ADDR-CURRRATE. */
	addrtrnrec.currrate.set(wsaaNominalRate);
	/* MOVE T3688-CURRCODE TO ADDR-ACCTCCY. <008> */
	compute(addrtrnrec.acctamt, 9).set(mult(addrtrnrec.origamt, addrtrnrec.currrate));
	addrtrnrec.function.set("PSTW");
	callProgram(Addrtrn.class, addrtrnrec.addrtrnRec);
	if (isNE(addrtrnrec.statuz, oK)) {
	    syserrrec.params.set(addrtrnrec.addrtrnRec);
	    fatalError600();
	}
    }

    private void setGLSignfromCashtypcpy2() {
    	if (cashtypcpy.creditCard.isTrue()) {
    	    if (isEQ(t3688rec.sign09, "+")) {
    		addrtrnrec.glsign.set("-");
    	    } else {
    		addrtrnrec.glsign.set("+");
    	    }
    	} else {
    	    if (isEQ(t3688rec.sign01, "+")) {
    		addrtrnrec.glsign.set("-");
    	    } else {
    		addrtrnrec.glsign.set("+");
    	    }
    	}
		
	}

	protected void findRate6150() {
	if (isGTE(bsscIO.getEffectiveDate(), t3629rec.frmdate[wsaaX.toInt()])
		&& isLTE(bsscIO.getEffectiveDate(), t3629rec.todate[wsaaX.toInt()])) {
	    wsaaNominalRate.set(t3629rec.scrate[wsaaX.toInt()]);
	} else {
	    wsaaX.add(1);
	}
    }

    protected void updateDactpf(Gcrtpf gcrtpf) {
	// resptyp
	// restext
	if (!dddepfMap.isEmpty()) {
	    Dddepf dddepf = dddepfMap.get(gcrtpf.getChdrnum());
	    Dactpf dactpf = dactpfDAO.getDactpfOfHighestJobNumber(dddepf.getChdrpfx(), dddepf.getChdrcoy(),
		    gcrtpf.getChdrnum());
	    dactpf.setRespmsg(getRespondText(Integer.parseInt(gcrtpf.getReason().trim())));
	    dactpf.setRespType("R");
	    dactpf.setReturnFile(Bh5ge.fileName);
	    dactpfDAO.updateReturnFileForCCRecurring(dactpf);
	}
    }

    protected void commit3500() {
	/* COMMIT */
	/** Place any additional commitment processing in here. */
	/* EXIT */
    }

    protected void rollback3600() {
	/* ROLLBACK */
	/** Place any additional rollback processing in here. */
	/* EXIT */
    }

    protected void close4000() {
	lsaaStatuz.set(varcom.oK);
//	unlock();
    }
    
    private void unlock() {
    	/*    Remove the lock on this process*/
		sftlockrec.enttyp.set("PR");
		sftlockrec.company.set(bsprIO.getFsuco());
		sftlockrec.user.set(999999);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.entity.set(bprdIO.getProcessName());
		sftlockrec.function.set("UNLK");
		/* MOVE WSAA-PROG                 TO SFTL-ENTITY                */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaProg);
		stringVariable1.addExpression(bprdIO.getSystemParam03());
		stringVariable1.setStringInto(sftlockrec.entity);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
    }
    
    private void softLock() {
    	/*    Ensure only one copy of this program is running at once. We*/
    	/*    need to do this because a second program running at the same*/
    	/*    time will 'pick' out BEXTs which we should be using in the*/
    	/*    summerisation processes. Not only must we softlock the proces*/
    	/*    we must also set it to an error otherwise MAINB will release*/
    	/*    the softlock via a UALL function.*/
    	sftlockrec.enttyp.set("PR");
    	sftlockrec.company.set(bsprIO.getFsuco());
    	sftlockrec.user.set(999999);
    	sftlockrec.transaction.set(bprdIO.getAuthCode());
    	sftlockrec.entity.set(bprdIO.getProcessName());
    	sftlockrec.function.set("LOCK");
    	/* MOVE WSAA-PROG                 TO SFTL-ENTITY                */
    	StringUtil stringVariable1 = new StringUtil();
    	stringVariable1.addExpression(wsaaProg);
    	stringVariable1.addExpression(bprdIO.getSystemParam03());
    	stringVariable1.setStringInto(sftlockrec.entity);
    	callProgram(Sftlock.class, sftlockrec.sftlockRec);
    	if (isEQ(sftlockrec.statuz, "LOCK")) {
    		syserrrec.statuz.set(ErrorsInner.plck);
    		syserrrec.params.set(wsaaProg);
    		fatalError600();
    	}
    	else {
    		if (isNE(sftlockrec.statuz, varcom.oK)) {
    			syserrrec.params.set(sftlockrec.sftlockRec);
    			syserrrec.statuz.set(sftlockrec.statuz);
    			fatalError600();
    		}
    	}
    }
    
    
    /*
     * Class transformed  from Data Structure ERRORS_INNER
     */
    private static final class ErrorsInner { 
    		/* ERRORS */
    	public static FixedLengthStringData ivrm = new FixedLengthStringData(4).init("IVRM");
    	public static FixedLengthStringData h791 = new FixedLengthStringData(4).init("H791");
    	public static FixedLengthStringData h842 = new FixedLengthStringData(4).init("H842");
    	public static FixedLengthStringData nfnd = new FixedLengthStringData(4).init("NFND");
    	public static FixedLengthStringData plck = new FixedLengthStringData(4).init("PLCK");
    	public static FixedLengthStringData rpgn = new FixedLengthStringData(4).init("RPGN");
    	public static FixedLengthStringData rpgo = new FixedLengthStringData(4).init("RPGO");
    	public static FixedLengthStringData rpgp = new FixedLengthStringData(4).init("RPGP");
    	public static FixedLengthStringData rpgq = new FixedLengthStringData(4).init("RPGQ");
    	public static FixedLengthStringData rfk5 = new FixedLengthStringData(4).init("RFK5");
    }
    
    
    
  

}
