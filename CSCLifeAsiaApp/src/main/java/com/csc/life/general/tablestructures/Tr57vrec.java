package com.csc.life.general.tablestructures;

import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import java.util.Arrays;

import com.quipoz.COBOLFramework.COBOLFunctions;

public class Tr57vrec extends ExternalData {

	public FixedLengthStringData tr57vRec = new FixedLengthStringData(500); 
	
	public FixedLengthStringData companys = new FixedLengthStringData(1).isAPartOf(tr57vRec,0);
	public FixedLengthStringData[] company = FLSArrayPartOfStructure(1, 1, companys, 0);
	
	public FixedLengthStringData items = new FixedLengthStringData(8).isAPartOf(tr57vRec,1);
	public FixedLengthStringData[] item = FLSArrayPartOfStructure(8, 1, items, 0);
	
	public FixedLengthStringData longdescs = new FixedLengthStringData(30).isAPartOf(tr57vRec,9);
	public FixedLengthStringData[] longdesc = FLSArrayPartOfStructure(30, 1, longdescs, 0);
	
	public FixedLengthStringData subrtns = new FixedLengthStringData(56).isAPartOf(tr57vRec,39);
	public FixedLengthStringData[] subrtn = FLSArrayPartOfStructure(7,8,subrtns, 0);
	public FixedLengthStringData subrtn01 = new FixedLengthStringData(8).isAPartOf(subrtns, 0);
	public FixedLengthStringData subrtn02 = new FixedLengthStringData(8).isAPartOf(subrtns, 8);
	public FixedLengthStringData subrtn03 = new FixedLengthStringData(8).isAPartOf(subrtns, 16);
	public FixedLengthStringData subrtn04 = new FixedLengthStringData(8).isAPartOf(subrtns, 24);
	public FixedLengthStringData subrtn05 = new FixedLengthStringData(8).isAPartOf(subrtns, 32);
	public FixedLengthStringData subrtn06 = new FixedLengthStringData(8).isAPartOf(subrtns, 40);
	public FixedLengthStringData subrtn07 = new FixedLengthStringData(8).isAPartOf(subrtns, 48);
	
	public FixedLengthStringData tabls = new FixedLengthStringData(5).isAPartOf(tr57vRec,95);
	public FixedLengthStringData[] tabl = FLSArrayPartOfStructure(5, 1, tabls, 0);
	
	public FixedLengthStringData zdmsions = new FixedLengthStringData(105).isAPartOf(tr57vRec,100);
	public FixedLengthStringData[] zdmsion = FLSArrayPartOfStructure(7,15,zdmsions, 0);
	public FixedLengthStringData zdmsion01 = new FixedLengthStringData(15).isAPartOf(zdmsions, 0);
	public FixedLengthStringData zdmsion02 = new FixedLengthStringData(15).isAPartOf(zdmsions, 15);
	public FixedLengthStringData zdmsion03 = new FixedLengthStringData(15).isAPartOf(zdmsions, 30);
	public FixedLengthStringData zdmsion04 = new FixedLengthStringData(15).isAPartOf(zdmsions, 45);
	public FixedLengthStringData zdmsion05 = new FixedLengthStringData(15).isAPartOf(zdmsions, 60);
	public FixedLengthStringData zdmsion06 = new FixedLengthStringData(15).isAPartOf(zdmsions, 75);
	public FixedLengthStringData zdmsion07 = new FixedLengthStringData(15).isAPartOf(zdmsions, 90);
	public FixedLengthStringData filler = new FixedLengthStringData(295).isAPartOf(tr57vRec, 205, FILLER);
		
    public String[] fieldnamelist={"companys","items","longdescs","subrtn01s","subrtn02s","subrtn03s","subrtn04s","subrtn05s","subrtn06s","subrtn07s","tabls","zdmsion01s","zdmsion02s","zdmsion03s","zdmsion04s","zdmsion05s","zdmsion06s","zdmsion07s"};
		
	public Tr57vrec(){
	}
	
	public void initialize() {
		COBOLFunctions.initialize(tr57vRec);
	}	

	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr57vRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	public String[] getFieldNames(){
		return Arrays.copyOf(fieldnamelist, fieldnamelist.length);//IJTI-316
    }
}
