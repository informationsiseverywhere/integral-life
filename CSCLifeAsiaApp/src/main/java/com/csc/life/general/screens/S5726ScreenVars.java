package com.csc.life.general.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5726
 * @version 1.0 generated on 30/08/09 06:50
 * @author Quipoz
 */
public class S5726ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(533);
	public FixedLengthStringData dataFields = new FixedLengthStringData(69).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData indics = new FixedLengthStringData(25).isAPartOf(dataFields, 1);
	public FixedLengthStringData[] indic = FLSArrayPartOfStructure(25, 1, indics, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(25).isAPartOf(indics, 0, FILLER_REDEFINE);
	public FixedLengthStringData indic01 = DD.indic.copy().isAPartOf(filler,0);
	public FixedLengthStringData indic02 = DD.indic.copy().isAPartOf(filler,1);
	public FixedLengthStringData indic03 = DD.indic.copy().isAPartOf(filler,2);
	public FixedLengthStringData indic04 = DD.indic.copy().isAPartOf(filler,3);
	public FixedLengthStringData indic05 = DD.indic.copy().isAPartOf(filler,4);
	public FixedLengthStringData indic06 = DD.indic.copy().isAPartOf(filler,5);
	public FixedLengthStringData indic07 = DD.indic.copy().isAPartOf(filler,6);
	public FixedLengthStringData indic08 = DD.indic.copy().isAPartOf(filler,7);
	public FixedLengthStringData indic09 = DD.indic.copy().isAPartOf(filler,8);
	public FixedLengthStringData indic10 = DD.indic.copy().isAPartOf(filler,9);
	public FixedLengthStringData indic11 = DD.indic.copy().isAPartOf(filler,10);
	public FixedLengthStringData indic12 = DD.indic.copy().isAPartOf(filler,11);
	public FixedLengthStringData indic13 = DD.indic.copy().isAPartOf(filler,12);
	public FixedLengthStringData indic14 = DD.indic.copy().isAPartOf(filler,13);
	public FixedLengthStringData indic15 = DD.indic.copy().isAPartOf(filler,14);
	public FixedLengthStringData indic16 = DD.indic.copy().isAPartOf(filler,15);
	public FixedLengthStringData indic17 = DD.indic.copy().isAPartOf(filler,16);
	public FixedLengthStringData indic18 = DD.indic.copy().isAPartOf(filler,17);
	public FixedLengthStringData indic19 = DD.indic.copy().isAPartOf(filler,18);
	public FixedLengthStringData indic20 = DD.indic.copy().isAPartOf(filler,19);
	public FixedLengthStringData indic21 = DD.indic.copy().isAPartOf(filler,20);
	public FixedLengthStringData indic22 = DD.indic.copy().isAPartOf(filler,21);
	public FixedLengthStringData indic23 = DD.indic.copy().isAPartOf(filler,22);
	public FixedLengthStringData indic24 = DD.indic.copy().isAPartOf(filler,23);
	public FixedLengthStringData indic25 = DD.indic.copy().isAPartOf(filler,24);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,26);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,34);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,64);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(116).isAPartOf(dataArea, 69);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData indicsErr = new FixedLengthStringData(100).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] indicErr = FLSArrayPartOfStructure(25, 4, indicsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(100).isAPartOf(indicsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData indic01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData indic02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData indic03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData indic04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData indic05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData indic06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData indic07Err = new FixedLengthStringData(4).isAPartOf(filler1, 24);
	public FixedLengthStringData indic08Err = new FixedLengthStringData(4).isAPartOf(filler1, 28);
	public FixedLengthStringData indic09Err = new FixedLengthStringData(4).isAPartOf(filler1, 32);
	public FixedLengthStringData indic10Err = new FixedLengthStringData(4).isAPartOf(filler1, 36);
	public FixedLengthStringData indic11Err = new FixedLengthStringData(4).isAPartOf(filler1, 40);
	public FixedLengthStringData indic12Err = new FixedLengthStringData(4).isAPartOf(filler1, 44);
	public FixedLengthStringData indic13Err = new FixedLengthStringData(4).isAPartOf(filler1, 48);
	public FixedLengthStringData indic14Err = new FixedLengthStringData(4).isAPartOf(filler1, 52);
	public FixedLengthStringData indic15Err = new FixedLengthStringData(4).isAPartOf(filler1, 56);
	public FixedLengthStringData indic16Err = new FixedLengthStringData(4).isAPartOf(filler1, 60);
	public FixedLengthStringData indic17Err = new FixedLengthStringData(4).isAPartOf(filler1, 64);
	public FixedLengthStringData indic18Err = new FixedLengthStringData(4).isAPartOf(filler1, 68);
	public FixedLengthStringData indic19Err = new FixedLengthStringData(4).isAPartOf(filler1, 72);
	public FixedLengthStringData indic20Err = new FixedLengthStringData(4).isAPartOf(filler1, 76);
	public FixedLengthStringData indic21Err = new FixedLengthStringData(4).isAPartOf(filler1, 80);
	public FixedLengthStringData indic22Err = new FixedLengthStringData(4).isAPartOf(filler1, 84);
	public FixedLengthStringData indic23Err = new FixedLengthStringData(4).isAPartOf(filler1, 88);
	public FixedLengthStringData indic24Err = new FixedLengthStringData(4).isAPartOf(filler1, 92);
	public FixedLengthStringData indic25Err = new FixedLengthStringData(4).isAPartOf(filler1, 96);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(348).isAPartOf(dataArea, 185);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData indicsOut = new FixedLengthStringData(300).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] indicOut = FLSArrayPartOfStructure(25, 12, indicsOut, 0);
	public FixedLengthStringData[][] indicO = FLSDArrayPartOfArrayStructure(12, 1, indicOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(300).isAPartOf(indicsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] indic01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] indic02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] indic03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] indic04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] indic05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] indic06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] indic07Out = FLSArrayPartOfStructure(12, 1, filler2, 72);
	public FixedLengthStringData[] indic08Out = FLSArrayPartOfStructure(12, 1, filler2, 84);
	public FixedLengthStringData[] indic09Out = FLSArrayPartOfStructure(12, 1, filler2, 96);
	public FixedLengthStringData[] indic10Out = FLSArrayPartOfStructure(12, 1, filler2, 108);
	public FixedLengthStringData[] indic11Out = FLSArrayPartOfStructure(12, 1, filler2, 120);
	public FixedLengthStringData[] indic12Out = FLSArrayPartOfStructure(12, 1, filler2, 132);
	public FixedLengthStringData[] indic13Out = FLSArrayPartOfStructure(12, 1, filler2, 144);
	public FixedLengthStringData[] indic14Out = FLSArrayPartOfStructure(12, 1, filler2, 156);
	public FixedLengthStringData[] indic15Out = FLSArrayPartOfStructure(12, 1, filler2, 168);
	public FixedLengthStringData[] indic16Out = FLSArrayPartOfStructure(12, 1, filler2, 180);
	public FixedLengthStringData[] indic17Out = FLSArrayPartOfStructure(12, 1, filler2, 192);
	public FixedLengthStringData[] indic18Out = FLSArrayPartOfStructure(12, 1, filler2, 204);
	public FixedLengthStringData[] indic19Out = FLSArrayPartOfStructure(12, 1, filler2, 216);
	public FixedLengthStringData[] indic20Out = FLSArrayPartOfStructure(12, 1, filler2, 228);
	public FixedLengthStringData[] indic21Out = FLSArrayPartOfStructure(12, 1, filler2, 240);
	public FixedLengthStringData[] indic22Out = FLSArrayPartOfStructure(12, 1, filler2, 252);
	public FixedLengthStringData[] indic23Out = FLSArrayPartOfStructure(12, 1, filler2, 264);
	public FixedLengthStringData[] indic24Out = FLSArrayPartOfStructure(12, 1, filler2, 276);
	public FixedLengthStringData[] indic25Out = FLSArrayPartOfStructure(12, 1, filler2, 288);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5726screenWritten = new LongData(0);
	public LongData S5726protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5726ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, indic01, indic14, indic02, indic15, indic03, indic16, indic04, indic17, indic05, indic18, indic06, indic19, indic07, indic20, indic08, indic21, indic09, indic22, indic10, indic23, indic11, indic24, indic12, indic25, indic13};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, indic01Out, indic14Out, indic02Out, indic15Out, indic03Out, indic16Out, indic04Out, indic17Out, indic05Out, indic18Out, indic06Out, indic19Out, indic07Out, indic20Out, indic08Out, indic21Out, indic09Out, indic22Out, indic10Out, indic23Out, indic11Out, indic24Out, indic12Out, indic25Out, indic13Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, indic01Err, indic14Err, indic02Err, indic15Err, indic03Err, indic16Err, indic04Err, indic17Err, indic05Err, indic18Err, indic06Err, indic19Err, indic07Err, indic20Err, indic08Err, indic21Err, indic09Err, indic22Err, indic10Err, indic23Err, indic11Err, indic24Err, indic12Err, indic25Err, indic13Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5726screen.class;
		protectRecord = S5726protect.class;
	}

}
