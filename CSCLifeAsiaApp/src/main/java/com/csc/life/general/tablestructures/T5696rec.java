package com.csc.life.general.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class T5696rec extends ExternalData {

	public FixedLengthStringData t5696Rec = new FixedLengthStringData(500); 
	public FixedLengthStringData zdmsion = new FixedLengthStringData(15).isAPartOf(t5696Rec,0);
	public FixedLengthStringData flagbancass = new FixedLengthStringData(1).isAPartOf(t5696Rec,15);
	public FixedLengthStringData filler = new FixedLengthStringData(484).isAPartOf(t5696Rec, 16, FILLER);

    public void initialize() {
		COBOLFunctions.initialize(t5696Rec);
	}	


	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			t5696Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}

}
