package com.csc.life.general.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:11:55
 * Description:
 * Copybook name: SUMCALCREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Sumcalcrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData surrenderRec = new FixedLengthStringData(207);
  	public FixedLengthStringData chdrChdrcoy = new FixedLengthStringData(1).isAPartOf(surrenderRec, 0);
  	public FixedLengthStringData chdrChdrnum = new FixedLengthStringData(8).isAPartOf(surrenderRec, 1);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(surrenderRec, 9);
  	public PackedDecimalData polsum = new PackedDecimalData(4, 0).isAPartOf(surrenderRec, 12);
  	public FixedLengthStringData lifeLife = new FixedLengthStringData(2).isAPartOf(surrenderRec, 15);
  	public FixedLengthStringData lifeJlife = new FixedLengthStringData(2).isAPartOf(surrenderRec, 17);
  	public FixedLengthStringData covrCoverage = new FixedLengthStringData(2).isAPartOf(surrenderRec, 19);
  	public FixedLengthStringData covrRider = new FixedLengthStringData(2).isAPartOf(surrenderRec, 21);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(surrenderRec, 23);
  	public PackedDecimalData crrcd = new PackedDecimalData(8, 0).isAPartOf(surrenderRec, 27);
  	public PackedDecimalData ptdate = new PackedDecimalData(8, 0).isAPartOf(surrenderRec, 32);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(surrenderRec, 37);
  	public PackedDecimalData convUnits = new PackedDecimalData(8, 0).isAPartOf(surrenderRec, 42);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(surrenderRec, 47);
  	public PackedDecimalData estimatedVal = new PackedDecimalData(17, 2).isAPartOf(surrenderRec, 48);
  	public PackedDecimalData actualVal = new PackedDecimalData(17, 2).isAPartOf(surrenderRec, 57);
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(surrenderRec, 66);
  	public FixedLengthStringData chdrCurr = new FixedLengthStringData(3).isAPartOf(surrenderRec, 69);
  	public FixedLengthStringData pstatcode = new FixedLengthStringData(2).isAPartOf(surrenderRec, 72);
  	public FixedLengthStringData element = new FixedLengthStringData(1).isAPartOf(surrenderRec, 74);
  	public FixedLengthStringData description = new FixedLengthStringData(30).isAPartOf(surrenderRec, 75);
  	public PackedDecimalData singp = new PackedDecimalData(17, 2).isAPartOf(surrenderRec, 105);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(surrenderRec, 114);
  	public FixedLengthStringData type = new FixedLengthStringData(1).isAPartOf(surrenderRec, 116);
  	public FixedLengthStringData fund = new FixedLengthStringData(4).isAPartOf(surrenderRec, 117);
  	public FixedLengthStringData status = new FixedLengthStringData(4).isAPartOf(surrenderRec, 121);
  	public FixedLengthStringData endf = new FixedLengthStringData(1).isAPartOf(surrenderRec, 125);
  	public FixedLengthStringData neUnits = new FixedLengthStringData(1).isAPartOf(surrenderRec, 126);
  	public FixedLengthStringData tmUnits = new FixedLengthStringData(1).isAPartOf(surrenderRec, 127);
  	public FixedLengthStringData psNotAllwd = new FixedLengthStringData(1).isAPartOf(surrenderRec, 128);
  	public ZonedDecimalData tsvtot = new ZonedDecimalData(17, 2).isAPartOf(surrenderRec, 129);
  	public ZonedDecimalData tsv1tot = new ZonedDecimalData(17, 2).isAPartOf(surrenderRec, 146);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(surrenderRec, 163);
  	public FixedLengthStringData trancde = new FixedLengthStringData(4).isAPartOf(surrenderRec, 168);
  	public PackedDecimalData sumin = new PackedDecimalData(17, 2).isAPartOf(surrenderRec, 172);
  	public PackedDecimalData lage = new PackedDecimalData(3, 0).isAPartOf(surrenderRec, 181);
  	public PackedDecimalData jlage = new PackedDecimalData(3, 0).isAPartOf(surrenderRec, 183);
  	public FixedLengthStringData lsex = new FixedLengthStringData(1).isAPartOf(surrenderRec, 185);
  	public FixedLengthStringData jlsex = new FixedLengthStringData(1).isAPartOf(surrenderRec, 186);
  	public ZonedDecimalData duration = new ZonedDecimalData(2, 0).isAPartOf(surrenderRec, 187).setUnsigned();
  	public PackedDecimalData riskCessTerm = new PackedDecimalData(3, 0).isAPartOf(surrenderRec, 189);
  	public PackedDecimalData benCessTerm = new PackedDecimalData(3, 0).isAPartOf(surrenderRec, 191);
  	public FixedLengthStringData chdrtype = new FixedLengthStringData(3).isAPartOf(surrenderRec, 193);
  	public FixedLengthStringData freqann = new FixedLengthStringData(2).isAPartOf(surrenderRec, 196);
  	public PackedDecimalData calcPrem = new PackedDecimalData(17, 2).isAPartOf(surrenderRec, 198);


	public void initialize() {
		COBOLFunctions.initialize(surrenderRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		surrenderRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}