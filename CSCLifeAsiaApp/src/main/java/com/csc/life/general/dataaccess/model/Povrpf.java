package com.csc.life.general.dataaccess.model;

import java.math.BigDecimal;

public class Povrpf{
	
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int planSuffix;
	private String validflag;
	private String crtable;
	private String lifcnum;
	private BigDecimal annamnt01;
	private BigDecimal annamnt02;
	private BigDecimal annamnt03;
	private BigDecimal annamnt04;
	private BigDecimal annamnt05;
	private BigDecimal annamnt06;
	private BigDecimal annamnt07;
	private BigDecimal annamnt08;
	private BigDecimal annamnt09;
	private BigDecimal annamnt10;
	private BigDecimal annamnt11;
	private BigDecimal annamnt12;
	private BigDecimal annamnt13;
	private BigDecimal annamnt14;
	private BigDecimal annamnt15;
	private BigDecimal annamnt16;
	private BigDecimal annamnt17;
	private BigDecimal annamnt18;
	private BigDecimal annamnt19;
	private BigDecimal annamnt20;
	private BigDecimal annamnt21;
	private BigDecimal annamnt22;
	private BigDecimal annamnt23;
	private BigDecimal annamnt24;
	private BigDecimal annamnt25;
	private BigDecimal xplusone;
	private String userProfile;
	private String jobName;
	private String datime;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getLifcnum() {
		return lifcnum;
	}
	public void setLifcnum(String lifcnum) {
		this.lifcnum = lifcnum;
	}
	public BigDecimal getAnnamnt01() {
		return annamnt01;
	}
	public void setAnnamnt01(BigDecimal annamnt01) {
		this.annamnt01 = annamnt01;
	}
	public BigDecimal getAnnamnt02() {
		return annamnt02;
	}
	public void setAnnamnt02(BigDecimal annamnt02) {
		this.annamnt02 = annamnt02;
	}
	public BigDecimal getAnnamnt03() {
		return annamnt03;
	}
	public void setAnnamnt03(BigDecimal annamnt03) {
		this.annamnt03 = annamnt03;
	}
	public BigDecimal getAnnamnt04() {
		return annamnt04;
	}
	public void setAnnamnt04(BigDecimal annamnt04) {
		this.annamnt04 = annamnt04;
	}
	public BigDecimal getAnnamnt05() {
		return annamnt05;
	}
	public void setAnnamnt05(BigDecimal annamnt05) {
		this.annamnt05 = annamnt05;
	}
	public BigDecimal getAnnamnt06() {
		return annamnt06;
	}
	public void setAnnamnt06(BigDecimal annamnt06) {
		this.annamnt06 = annamnt06;
	}
	public BigDecimal getAnnamnt07() {
		return annamnt07;
	}
	public void setAnnamnt07(BigDecimal annamnt07) {
		this.annamnt07 = annamnt07;
	}
	public BigDecimal getAnnamnt08() {
		return annamnt08;
	}
	public void setAnnamnt08(BigDecimal annamnt08) {
		this.annamnt08 = annamnt08;
	}
	public BigDecimal getAnnamnt09() {
		return annamnt09;
	}
	public void setAnnamnt09(BigDecimal annamnt09) {
		this.annamnt09 = annamnt09;
	}
	public BigDecimal getAnnamnt10() {
		return annamnt10;
	}
	public void setAnnamnt10(BigDecimal annamnt10) {
		this.annamnt10 = annamnt10;
	}
	public BigDecimal getAnnamnt11() {
		return annamnt11;
	}
	public void setAnnamnt11(BigDecimal annamnt11) {
		this.annamnt11 = annamnt11;
	}
	public BigDecimal getAnnamnt12() {
		return annamnt12;
	}
	public void setAnnamnt12(BigDecimal annamnt12) {
		this.annamnt12 = annamnt12;
	}
	public BigDecimal getAnnamnt13() {
		return annamnt13;
	}
	public void setAnnamnt13(BigDecimal annamnt13) {
		this.annamnt13 = annamnt13;
	}
	public BigDecimal getAnnamnt14() {
		return annamnt14;
	}
	public void setAnnamnt14(BigDecimal annamnt14) {
		this.annamnt14 = annamnt14;
	}
	public BigDecimal getAnnamnt15() {
		return annamnt15;
	}
	public void setAnnamnt15(BigDecimal annamnt15) {
		this.annamnt15 = annamnt15;
	}
	public BigDecimal getAnnamnt16() {
		return annamnt16;
	}
	public void setAnnamnt16(BigDecimal annamnt16) {
		this.annamnt16 = annamnt16;
	}
	public BigDecimal getAnnamnt17() {
		return annamnt17;
	}
	public void setAnnamnt17(BigDecimal annamnt17) {
		this.annamnt17 = annamnt17;
	}
	public BigDecimal getAnnamnt18() {
		return annamnt18;
	}
	public void setAnnamnt18(BigDecimal annamnt18) {
		this.annamnt18 = annamnt18;
	}
	public BigDecimal getAnnamnt19() {
		return annamnt19;
	}
	public void setAnnamnt19(BigDecimal annamnt19) {
		this.annamnt19 = annamnt19;
	}
	public BigDecimal getAnnamnt20() {
		return annamnt20;
	}
	public void setAnnamnt20(BigDecimal annamnt20) {
		this.annamnt20 = annamnt20;
	}
	public BigDecimal getAnnamnt21() {
		return annamnt21;
	}
	public void setAnnamnt21(BigDecimal annamnt21) {
		this.annamnt21 = annamnt21;
	}
	public BigDecimal getAnnamnt22() {
		return annamnt22;
	}
	public void setAnnamnt22(BigDecimal annamnt22) {
		this.annamnt22 = annamnt22;
	}
	public BigDecimal getAnnamnt23() {
		return annamnt23;
	}
	public void setAnnamnt23(BigDecimal annamnt23) {
		this.annamnt23 = annamnt23;
	}
	public BigDecimal getAnnamnt24() {
		return annamnt24;
	}
	public void setAnnamnt24(BigDecimal annamnt24) {
		this.annamnt24 = annamnt24;
	}
	public BigDecimal getAnnamnt25() {
		return annamnt25;
	}
	public void setAnnamnt25(BigDecimal annamnt25) {
		this.annamnt25 = annamnt25;
	}
	public BigDecimal getXplusone() {
		return xplusone;
	}
	public void setXplusone(BigDecimal xplusone) {
		this.xplusone = xplusone;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
}