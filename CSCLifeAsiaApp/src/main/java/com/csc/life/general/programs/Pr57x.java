/*
 * File: Pr57x.java
 * Date: {{dd-mm-yyyy}}
 * Author: Autoated Code template
 * 
 * Class transformed from {{template name}}
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
//package life.general;
package com.csc.life.general.programs;

import com.quipoz.framework.datatype.*;
import com.quipoz.framework.exception.ServerException;
import com.quipoz.framework.exception.TransferProgramException;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.sessionfacade.JTAWrapper;
import com.quipoz.framework.util.*;
import com.quipoz.COBOLFramework.util.*;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import static com.quipoz.COBOLFramework.util.CLFunctions.*;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.ExitSection;
//import com.csc.smart400framework.utility.ValueRange;
import java.util.ArrayList;
import com.quipoz.framework.util.log.QPLogger;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.life.general.tablestructures.Tr57xrec;	
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.life.general.screens.Sr57xScreenVars;	
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.life.general.procedures.Tr57xpt;	
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
/********************************************
 * includes for IO classes
*******************************************/

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* Sun Product Dimension not Applicable
*
*
*****************************************************************
* </pre>
*/
public class Pr57x extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	//private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH552");	// change on 4th April
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR57X");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "";
	private String wsaaFirstTime = "Y";
	private ZonedDecimalData wsaaSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/* ERRORS */
	private String e026 = "E026";
	private String e027 = "E027";
         
    private FixedLengthStringData errors = new FixedLengthStringData(8);
    private FixedLengthStringData F838 = new FixedLengthStringData(4).isAPartOf(errors, 0).init("F838");

		/* FORMATS */
	private String itemrec = "ITEMREC";
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Tr57xrec tr57xrec = new Tr57xrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr57xScreenVars sv = ScreenProgram.getScreenVars( Sr57xScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readPrimaryRecord1020, 
		moveToScreen1040, 
		generalArea1045, 
		other1080, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090, 
		continue4020, 
		other4080
	}

	public Pr57x() {
		super();
		screenVars = sv;
		new ScreenModel("Sr57x", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

	protected void initialise1000()
	{
//		initialise1010();
//		readPrimaryRecord1020();
//		//readSecondaryRecords1030();
//		readRecord1031();
//		moveToScreen1040();
//		generalArea1045();

		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
				}
				case readPrimaryRecord1020: {
					readPrimaryRecord1020();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				case other1080: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}

	}

	protected void initialise1010()
	{
		if (isNE(wsaaFirstTime,"Y")) {
			//readPrimaryRecord1020();
			goTo(GotoLabel.readPrimaryRecord1020);
		}
		if (isEQ(itemIO.getItemseq(),SPACES)) {
			wsaaSeq.set(ZERO);
		}
		else {
			wsaaSeq.set(itemIO.getItemseq());
		}
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
	}

	protected void readPrimaryRecord1020()
	{
		itemIO.setDataKey(wsspsmart.itemkey);
		if (isEQ(wsaaSeq,ZERO)) {
			itemIO.setItemseq(SPACES);
		}
		else {
			itemIO.setItemseq(wsaaSeq);
		}
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}


	protected void readRecord1031()
	{
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		wsaaFirstTime = "N";
	}

	protected void moveToScreen1040()
	{
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)
		&& isEQ(itemIO.getItemseq(),SPACES)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)
		&& isEQ(wsspcomn.flag,"I")) {
			scrnparams.errorCode.set(e026);
			wsaaSeq.subtract(1);
			goTo(GotoLabel.other1080);
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
		}
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		tr57xrec.tr57xRec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
	}

	protected void generalArea1045()
	{
		sv.sacscodes.set(tr57xrec.sacscodes);
		sv.rdocpfxs.set(tr57xrec.rdocpfxs);
		sv.sacstypes.set(tr57xrec.sacstypes);
	}

	 protected void preScreenEdit()
	 {
		 try {
			if (isEQ(wsspcomn.flag,"I")) {
				scrnparams.function.set(varcom.prot);
			}
			goTo(GotoLabel.preExit);
		} catch (GOTOException e){
		}
	
	 }


 	protected void screenEdit2000()
	{
	  //screenIo2010();
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}

	}

	protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(scrnparams.statuz,varcom.rold)
		&& isEQ(itemIO.getItemseq(),SPACES)) {
			scrnparams.errorCode.set(e027);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)
		&& isEQ(itemIO.getItemseq(),"99")) {
			scrnparams.errorCode.set(e026);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
			//return;
		}
	}

	protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
//		if (isEQ(wsspcomn.flag,"I")) {
//			return;
//		}
//		/*CHECK-CHANGES*/
//		wsaaUpdateFlag = "N";
//		if (isEQ(wsspcomn.flag,"C")) {
//			wsaaUpdateFlag = "Y";
//		}
//		checkChanges3100();
//		if (isNE(wsaaUpdateFlag,"Y")) {
//			return;
//		}
//		updatePrimaryRecord3050();
//		updateRecord3055();
	
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			} catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}

	}

	protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}


protected void updatePrimaryRecord3050()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		if (isEQ(wsaaSeq,ZERO)) {
			itemIO.setItemseq(SPACES);
		}
		else {
			itemIO.setItemseq(wsaaSeq);
		}
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itemIO.setTableprog(wsaaProg);
		itemIO.setGenarea(tr57xrec.tr57xRec);  // ** must SEt to the Table Record
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setFunction(varcom.writr);
			itemIO.setFormat(itemrec);
		}
		else {
			itemIO.setFunction(varcom.rewrt);
		}
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		if (isNE(sv.sacscodes,tr57xrec.sacscodes)) {
			tr57xrec.sacscodes.set(sv.sacscodes);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.rdocpfxs,tr57xrec.rdocpfxs)) {
			tr57xrec.rdocpfxs.set(sv.rdocpfxs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.sacstypes,tr57xrec.sacstypes)) {
			tr57xrec.sacstypes.set(sv.sacstypes);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation4020();
				}
				case continue4020: {
					continue4020();
				}
				case other4080: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		} 
		
		//wsspcomn.programPtr.add(1);
	}

protected void preparation4020()
	{
		if (isNE(scrnparams.statuz,varcom.rolu)
		&& isNE(scrnparams.statuz,varcom.rold)) {
			goTo(GotoLabel.continue4020);
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			wsaaSeq.add(1);
		}
		else {
			wsaaSeq.subtract(1);
		}
		goTo(GotoLabel.other4080);
	}

protected void continue4020()
	{
		wsspcomn.programPtr.add(1);
		wsaaSeq.set(ZERO);
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Tr57xpt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
