/*
 * File: S1692protect.java
 * Date: {{DATETIME}}
 * Author: Automated protect Code template
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
package com.csc.life.general.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.*;
import com.quipoz.COBOLFramework.util.COBOLAppVars;

/**
 * Screen record for PROTECT S1692
 * @version 1.0 generated on {{TIMESTAMP}}
 * @author csc
 */
public class S1692protect extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {}; 
	public static final RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {-1, -1, -1, -1}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S1692ScreenVars sv = ( S1692ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S1692protectWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S1692ScreenVars screenVars = ( S1692ScreenVars)pv;
	}
	
	/**
	 * Clear all the variables in S2500screen
	 */
	public static void clear(VarModel pv) {
		S1692ScreenVars screenVars = (S1692ScreenVars) pv;
	}
}
