/*
 * File: T6005pt.java
 * Date: 30 August 2009 2:27:15
 * Author: Quipoz Limited
 * 
 * Class transformed from T6005PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.general.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.general.tablestructures.T6005rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6005.
*
*
*****************************************************************
* </pre>
*/
public class T6005pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(45).isAPartOf(wsaaPrtLine001, 31, FILLER).init("Bonus Application Edit Rules            S6005");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(52);
	private FixedLengthStringData filler7 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine003, 7, FILLER).init("Valid Methods :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine003, 24);
	private FixedLengthStringData filler9 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine003, 28, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine003, 30);
	private FixedLengthStringData filler10 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine003, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine003, 36);
	private FixedLengthStringData filler11 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine003, 40, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine003, 42);
	private FixedLengthStringData filler12 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine003, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine003, 48);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(25);
	private FixedLengthStringData filler13 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine004, 7, FILLER).init("Default Indic.:");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 24);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(35);
	private FixedLengthStringData filler15 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler16 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine005, 9, FILLER).init("End Of Term Bonus Defaults");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(38);
	private FixedLengthStringData filler17 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler18 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine006, 6, FILLER).init("Mid-Term Surrender Pay Client:");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 37);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(38);
	private FixedLengthStringData filler19 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler20 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine007, 6, FILLER).init("Death Claim Pay Client       :");
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 37);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(38);
	private FixedLengthStringData filler21 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler22 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine008, 6, FILLER).init("Paid-Up Increase PUP Value   :");
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 37);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6005rec t6005rec = new T6005rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6005pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6005rec.t6005Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(t6005rec.bappmeth01);
		fieldNo006.set(t6005rec.bappmeth02);
		fieldNo007.set(t6005rec.bappmeth03);
		fieldNo008.set(t6005rec.bappmeth04);
		fieldNo009.set(t6005rec.bappmeth05);
		fieldNo010.set(t6005rec.ind);
		fieldNo011.set(t6005rec.indic01);
		fieldNo012.set(t6005rec.indic02);
		fieldNo013.set(t6005rec.indic03);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
