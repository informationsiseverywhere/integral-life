package com.csc.life.general.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: PovrTableDAM.java
 * Date: Sun, 30 Aug 2009 03:44:39
 * Class transformed from POVR.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PovrTableDAM extends PovrpfTableDAM {

	public PovrTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("POVR");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", PLNSFX";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "VALIDFLAG, " +
		            "CRTABLE, " +
		            "LIFCNUM, " +
		            "ANNAMNT01, " +
		            "ANNAMNT02, " +
		            "ANNAMNT03, " +
		            "ANNAMNT04, " +
		            "ANNAMNT05, " +
		            "ANNAMNT06, " +
		            "ANNAMNT07, " +
		            "ANNAMNT08, " +
		            "ANNAMNT09, " +
		            "ANNAMNT10, " +
		            "ANNAMNT11, " +
		            "ANNAMNT12, " +
		            "ANNAMNT13, " +
		            "ANNAMNT14, " +
		            "ANNAMNT15, " +
		            "ANNAMNT16, " +
		            "ANNAMNT17, " +
		            "ANNAMNT18, " +
		            "ANNAMNT19, " +
		            "ANNAMNT20, " +
		            "ANNAMNT21, " +
		            "ANNAMNT22, " +
		            "ANNAMNT23, " +
		            "ANNAMNT24, " +
		            "ANNAMNT25, " +
		            "XPLUSONE, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "PLNSFX ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "PLNSFX DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               validflag,
                               crtable,
                               lifcnum,
                               annamnt01,
                               annamnt02,
                               annamnt03,
                               annamnt04,
                               annamnt05,
                               annamnt06,
                               annamnt07,
                               annamnt08,
                               annamnt09,
                               annamnt10,
                               annamnt11,
                               annamnt12,
                               annamnt13,
                               annamnt14,
                               annamnt15,
                               annamnt16,
                               annamnt17,
                               annamnt18,
                               annamnt19,
                               annamnt20,
                               annamnt21,
                               annamnt22,
                               annamnt23,
                               annamnt24,
                               annamnt25,
                               xplusone,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(46);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(life.toInternal());
	nonKeyFiller4.setInternal(coverage.toInternal());
	nonKeyFiller5.setInternal(rider.toInternal());
	nonKeyFiller6.setInternal(planSuffix.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(311);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ nonKeyFiller5.toInternal()
					+ nonKeyFiller6.toInternal()
					+ getValidflag().toInternal()
					+ getCrtable().toInternal()
					+ getLifcnum().toInternal()
					+ getAnnamnt01().toInternal()
					+ getAnnamnt02().toInternal()
					+ getAnnamnt03().toInternal()
					+ getAnnamnt04().toInternal()
					+ getAnnamnt05().toInternal()
					+ getAnnamnt06().toInternal()
					+ getAnnamnt07().toInternal()
					+ getAnnamnt08().toInternal()
					+ getAnnamnt09().toInternal()
					+ getAnnamnt10().toInternal()
					+ getAnnamnt11().toInternal()
					+ getAnnamnt12().toInternal()
					+ getAnnamnt13().toInternal()
					+ getAnnamnt14().toInternal()
					+ getAnnamnt15().toInternal()
					+ getAnnamnt16().toInternal()
					+ getAnnamnt17().toInternal()
					+ getAnnamnt18().toInternal()
					+ getAnnamnt19().toInternal()
					+ getAnnamnt20().toInternal()
					+ getAnnamnt21().toInternal()
					+ getAnnamnt22().toInternal()
					+ getAnnamnt23().toInternal()
					+ getAnnamnt24().toInternal()
					+ getAnnamnt25().toInternal()
					+ getXplusone().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, lifcnum);
			what = ExternalData.chop(what, annamnt01);
			what = ExternalData.chop(what, annamnt02);
			what = ExternalData.chop(what, annamnt03);
			what = ExternalData.chop(what, annamnt04);
			what = ExternalData.chop(what, annamnt05);
			what = ExternalData.chop(what, annamnt06);
			what = ExternalData.chop(what, annamnt07);
			what = ExternalData.chop(what, annamnt08);
			what = ExternalData.chop(what, annamnt09);
			what = ExternalData.chop(what, annamnt10);
			what = ExternalData.chop(what, annamnt11);
			what = ExternalData.chop(what, annamnt12);
			what = ExternalData.chop(what, annamnt13);
			what = ExternalData.chop(what, annamnt14);
			what = ExternalData.chop(what, annamnt15);
			what = ExternalData.chop(what, annamnt16);
			what = ExternalData.chop(what, annamnt17);
			what = ExternalData.chop(what, annamnt18);
			what = ExternalData.chop(what, annamnt19);
			what = ExternalData.chop(what, annamnt20);
			what = ExternalData.chop(what, annamnt21);
			what = ExternalData.chop(what, annamnt22);
			what = ExternalData.chop(what, annamnt23);
			what = ExternalData.chop(what, annamnt24);
			what = ExternalData.chop(what, annamnt25);
			what = ExternalData.chop(what, xplusone);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public FixedLengthStringData getLifcnum() {
		return lifcnum;
	}
	public void setLifcnum(Object what) {
		lifcnum.set(what);
	}	
	public PackedDecimalData getAnnamnt01() {
		return annamnt01;
	}
	public void setAnnamnt01(Object what) {
		setAnnamnt01(what, false);
	}
	public void setAnnamnt01(Object what, boolean rounded) {
		if (rounded)
			annamnt01.setRounded(what);
		else
			annamnt01.set(what);
	}	
	public PackedDecimalData getAnnamnt02() {
		return annamnt02;
	}
	public void setAnnamnt02(Object what) {
		setAnnamnt02(what, false);
	}
	public void setAnnamnt02(Object what, boolean rounded) {
		if (rounded)
			annamnt02.setRounded(what);
		else
			annamnt02.set(what);
	}	
	public PackedDecimalData getAnnamnt03() {
		return annamnt03;
	}
	public void setAnnamnt03(Object what) {
		setAnnamnt03(what, false);
	}
	public void setAnnamnt03(Object what, boolean rounded) {
		if (rounded)
			annamnt03.setRounded(what);
		else
			annamnt03.set(what);
	}	
	public PackedDecimalData getAnnamnt04() {
		return annamnt04;
	}
	public void setAnnamnt04(Object what) {
		setAnnamnt04(what, false);
	}
	public void setAnnamnt04(Object what, boolean rounded) {
		if (rounded)
			annamnt04.setRounded(what);
		else
			annamnt04.set(what);
	}	
	public PackedDecimalData getAnnamnt05() {
		return annamnt05;
	}
	public void setAnnamnt05(Object what) {
		setAnnamnt05(what, false);
	}
	public void setAnnamnt05(Object what, boolean rounded) {
		if (rounded)
			annamnt05.setRounded(what);
		else
			annamnt05.set(what);
	}	
	public PackedDecimalData getAnnamnt06() {
		return annamnt06;
	}
	public void setAnnamnt06(Object what) {
		setAnnamnt06(what, false);
	}
	public void setAnnamnt06(Object what, boolean rounded) {
		if (rounded)
			annamnt06.setRounded(what);
		else
			annamnt06.set(what);
	}	
	public PackedDecimalData getAnnamnt07() {
		return annamnt07;
	}
	public void setAnnamnt07(Object what) {
		setAnnamnt07(what, false);
	}
	public void setAnnamnt07(Object what, boolean rounded) {
		if (rounded)
			annamnt07.setRounded(what);
		else
			annamnt07.set(what);
	}	
	public PackedDecimalData getAnnamnt08() {
		return annamnt08;
	}
	public void setAnnamnt08(Object what) {
		setAnnamnt08(what, false);
	}
	public void setAnnamnt08(Object what, boolean rounded) {
		if (rounded)
			annamnt08.setRounded(what);
		else
			annamnt08.set(what);
	}	
	public PackedDecimalData getAnnamnt09() {
		return annamnt09;
	}
	public void setAnnamnt09(Object what) {
		setAnnamnt09(what, false);
	}
	public void setAnnamnt09(Object what, boolean rounded) {
		if (rounded)
			annamnt09.setRounded(what);
		else
			annamnt09.set(what);
	}	
	public PackedDecimalData getAnnamnt10() {
		return annamnt10;
	}
	public void setAnnamnt10(Object what) {
		setAnnamnt10(what, false);
	}
	public void setAnnamnt10(Object what, boolean rounded) {
		if (rounded)
			annamnt10.setRounded(what);
		else
			annamnt10.set(what);
	}	
	public PackedDecimalData getAnnamnt11() {
		return annamnt11;
	}
	public void setAnnamnt11(Object what) {
		setAnnamnt11(what, false);
	}
	public void setAnnamnt11(Object what, boolean rounded) {
		if (rounded)
			annamnt11.setRounded(what);
		else
			annamnt11.set(what);
	}	
	public PackedDecimalData getAnnamnt12() {
		return annamnt12;
	}
	public void setAnnamnt12(Object what) {
		setAnnamnt12(what, false);
	}
	public void setAnnamnt12(Object what, boolean rounded) {
		if (rounded)
			annamnt12.setRounded(what);
		else
			annamnt12.set(what);
	}	
	public PackedDecimalData getAnnamnt13() {
		return annamnt13;
	}
	public void setAnnamnt13(Object what) {
		setAnnamnt13(what, false);
	}
	public void setAnnamnt13(Object what, boolean rounded) {
		if (rounded)
			annamnt13.setRounded(what);
		else
			annamnt13.set(what);
	}	
	public PackedDecimalData getAnnamnt14() {
		return annamnt14;
	}
	public void setAnnamnt14(Object what) {
		setAnnamnt14(what, false);
	}
	public void setAnnamnt14(Object what, boolean rounded) {
		if (rounded)
			annamnt14.setRounded(what);
		else
			annamnt14.set(what);
	}	
	public PackedDecimalData getAnnamnt15() {
		return annamnt15;
	}
	public void setAnnamnt15(Object what) {
		setAnnamnt15(what, false);
	}
	public void setAnnamnt15(Object what, boolean rounded) {
		if (rounded)
			annamnt15.setRounded(what);
		else
			annamnt15.set(what);
	}	
	public PackedDecimalData getAnnamnt16() {
		return annamnt16;
	}
	public void setAnnamnt16(Object what) {
		setAnnamnt16(what, false);
	}
	public void setAnnamnt16(Object what, boolean rounded) {
		if (rounded)
			annamnt16.setRounded(what);
		else
			annamnt16.set(what);
	}	
	public PackedDecimalData getAnnamnt17() {
		return annamnt17;
	}
	public void setAnnamnt17(Object what) {
		setAnnamnt17(what, false);
	}
	public void setAnnamnt17(Object what, boolean rounded) {
		if (rounded)
			annamnt17.setRounded(what);
		else
			annamnt17.set(what);
	}	
	public PackedDecimalData getAnnamnt18() {
		return annamnt18;
	}
	public void setAnnamnt18(Object what) {
		setAnnamnt18(what, false);
	}
	public void setAnnamnt18(Object what, boolean rounded) {
		if (rounded)
			annamnt18.setRounded(what);
		else
			annamnt18.set(what);
	}	
	public PackedDecimalData getAnnamnt19() {
		return annamnt19;
	}
	public void setAnnamnt19(Object what) {
		setAnnamnt19(what, false);
	}
	public void setAnnamnt19(Object what, boolean rounded) {
		if (rounded)
			annamnt19.setRounded(what);
		else
			annamnt19.set(what);
	}	
	public PackedDecimalData getAnnamnt20() {
		return annamnt20;
	}
	public void setAnnamnt20(Object what) {
		setAnnamnt20(what, false);
	}
	public void setAnnamnt20(Object what, boolean rounded) {
		if (rounded)
			annamnt20.setRounded(what);
		else
			annamnt20.set(what);
	}	
	public PackedDecimalData getAnnamnt21() {
		return annamnt21;
	}
	public void setAnnamnt21(Object what) {
		setAnnamnt21(what, false);
	}
	public void setAnnamnt21(Object what, boolean rounded) {
		if (rounded)
			annamnt21.setRounded(what);
		else
			annamnt21.set(what);
	}	
	public PackedDecimalData getAnnamnt22() {
		return annamnt22;
	}
	public void setAnnamnt22(Object what) {
		setAnnamnt22(what, false);
	}
	public void setAnnamnt22(Object what, boolean rounded) {
		if (rounded)
			annamnt22.setRounded(what);
		else
			annamnt22.set(what);
	}	
	public PackedDecimalData getAnnamnt23() {
		return annamnt23;
	}
	public void setAnnamnt23(Object what) {
		setAnnamnt23(what, false);
	}
	public void setAnnamnt23(Object what, boolean rounded) {
		if (rounded)
			annamnt23.setRounded(what);
		else
			annamnt23.set(what);
	}	
	public PackedDecimalData getAnnamnt24() {
		return annamnt24;
	}
	public void setAnnamnt24(Object what) {
		setAnnamnt24(what, false);
	}
	public void setAnnamnt24(Object what, boolean rounded) {
		if (rounded)
			annamnt24.setRounded(what);
		else
			annamnt24.set(what);
	}	
	public PackedDecimalData getAnnamnt25() {
		return annamnt25;
	}
	public void setAnnamnt25(Object what) {
		setAnnamnt25(what, false);
	}
	public void setAnnamnt25(Object what, boolean rounded) {
		if (rounded)
			annamnt25.setRounded(what);
		else
			annamnt25.set(what);
	}	
	public PackedDecimalData getXplusone() {
		return xplusone;
	}
	public void setXplusone(Object what) {
		setXplusone(what, false);
	}
	public void setXplusone(Object what, boolean rounded) {
		if (rounded)
			xplusone.setRounded(what);
		else
			xplusone.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getAnnamnts() {
		return new FixedLengthStringData(annamnt01.toInternal()
										+ annamnt02.toInternal()
										+ annamnt03.toInternal()
										+ annamnt04.toInternal()
										+ annamnt05.toInternal()
										+ annamnt06.toInternal()
										+ annamnt07.toInternal()
										+ annamnt08.toInternal()
										+ annamnt09.toInternal()
										+ annamnt10.toInternal()
										+ annamnt11.toInternal()
										+ annamnt12.toInternal()
										+ annamnt13.toInternal()
										+ annamnt14.toInternal()
										+ annamnt15.toInternal()
										+ annamnt16.toInternal()
										+ annamnt17.toInternal()
										+ annamnt18.toInternal()
										+ annamnt19.toInternal()
										+ annamnt20.toInternal()
										+ annamnt21.toInternal()
										+ annamnt22.toInternal()
										+ annamnt23.toInternal()
										+ annamnt24.toInternal()
										+ annamnt25.toInternal());
	}
	public void setAnnamnts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getAnnamnts().getLength()).init(obj);
	
		what = ExternalData.chop(what, annamnt01);
		what = ExternalData.chop(what, annamnt02);
		what = ExternalData.chop(what, annamnt03);
		what = ExternalData.chop(what, annamnt04);
		what = ExternalData.chop(what, annamnt05);
		what = ExternalData.chop(what, annamnt06);
		what = ExternalData.chop(what, annamnt07);
		what = ExternalData.chop(what, annamnt08);
		what = ExternalData.chop(what, annamnt09);
		what = ExternalData.chop(what, annamnt10);
		what = ExternalData.chop(what, annamnt11);
		what = ExternalData.chop(what, annamnt12);
		what = ExternalData.chop(what, annamnt13);
		what = ExternalData.chop(what, annamnt14);
		what = ExternalData.chop(what, annamnt15);
		what = ExternalData.chop(what, annamnt16);
		what = ExternalData.chop(what, annamnt17);
		what = ExternalData.chop(what, annamnt18);
		what = ExternalData.chop(what, annamnt19);
		what = ExternalData.chop(what, annamnt20);
		what = ExternalData.chop(what, annamnt21);
		what = ExternalData.chop(what, annamnt22);
		what = ExternalData.chop(what, annamnt23);
		what = ExternalData.chop(what, annamnt24);
		what = ExternalData.chop(what, annamnt25);
	}
	public PackedDecimalData getAnnamnt(BaseData indx) {
		return getAnnamnt(indx.toInt());
	}
	public PackedDecimalData getAnnamnt(int indx) {

		switch (indx) {
			case 1 : return annamnt01;
			case 2 : return annamnt02;
			case 3 : return annamnt03;
			case 4 : return annamnt04;
			case 5 : return annamnt05;
			case 6 : return annamnt06;
			case 7 : return annamnt07;
			case 8 : return annamnt08;
			case 9 : return annamnt09;
			case 10 : return annamnt10;
			case 11 : return annamnt11;
			case 12 : return annamnt12;
			case 13 : return annamnt13;
			case 14 : return annamnt14;
			case 15 : return annamnt15;
			case 16 : return annamnt16;
			case 17 : return annamnt17;
			case 18 : return annamnt18;
			case 19 : return annamnt19;
			case 20 : return annamnt20;
			case 21 : return annamnt21;
			case 22 : return annamnt22;
			case 23 : return annamnt23;
			case 24 : return annamnt24;
			case 25 : return annamnt25;
			default: return null; // Throw error instead?
		}
	
	}
	public void setAnnamnt(BaseData indx, Object what) {
		setAnnamnt(indx, what, false);
	}
	public void setAnnamnt(BaseData indx, Object what, boolean rounded) {
		setAnnamnt(indx.toInt(), what, rounded);
	}
	public void setAnnamnt(int indx, Object what) {
		setAnnamnt(indx, what, false);
	}
	public void setAnnamnt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setAnnamnt01(what, rounded);
					 break;
			case 2 : setAnnamnt02(what, rounded);
					 break;
			case 3 : setAnnamnt03(what, rounded);
					 break;
			case 4 : setAnnamnt04(what, rounded);
					 break;
			case 5 : setAnnamnt05(what, rounded);
					 break;
			case 6 : setAnnamnt06(what, rounded);
					 break;
			case 7 : setAnnamnt07(what, rounded);
					 break;
			case 8 : setAnnamnt08(what, rounded);
					 break;
			case 9 : setAnnamnt09(what, rounded);
					 break;
			case 10 : setAnnamnt10(what, rounded);
					 break;
			case 11 : setAnnamnt11(what, rounded);
					 break;
			case 12 : setAnnamnt12(what, rounded);
					 break;
			case 13 : setAnnamnt13(what, rounded);
					 break;
			case 14 : setAnnamnt14(what, rounded);
					 break;
			case 15 : setAnnamnt15(what, rounded);
					 break;
			case 16 : setAnnamnt16(what, rounded);
					 break;
			case 17 : setAnnamnt17(what, rounded);
					 break;
			case 18 : setAnnamnt18(what, rounded);
					 break;
			case 19 : setAnnamnt19(what, rounded);
					 break;
			case 20 : setAnnamnt20(what, rounded);
					 break;
			case 21 : setAnnamnt21(what, rounded);
					 break;
			case 22 : setAnnamnt22(what, rounded);
					 break;
			case 23 : setAnnamnt23(what, rounded);
					 break;
			case 24 : setAnnamnt24(what, rounded);
					 break;
			case 25 : setAnnamnt25(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		nonKeyFiller5.clear();
		nonKeyFiller6.clear();
		validflag.clear();
		crtable.clear();
		lifcnum.clear();
		annamnt01.clear();
		annamnt02.clear();
		annamnt03.clear();
		annamnt04.clear();
		annamnt05.clear();
		annamnt06.clear();
		annamnt07.clear();
		annamnt08.clear();
		annamnt09.clear();
		annamnt10.clear();
		annamnt11.clear();
		annamnt12.clear();
		annamnt13.clear();
		annamnt14.clear();
		annamnt15.clear();
		annamnt16.clear();
		annamnt17.clear();
		annamnt18.clear();
		annamnt19.clear();
		annamnt20.clear();
		annamnt21.clear();
		annamnt22.clear();
		annamnt23.clear();
		annamnt24.clear();
		annamnt25.clear();
		xplusone.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}