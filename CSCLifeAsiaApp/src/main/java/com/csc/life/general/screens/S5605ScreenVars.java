package com.csc.life.general.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5605
 * @version 1.0 generated on 30/08/09 06:44
 * @author Quipoz
 */
public class S5605ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(72);
	public FixedLengthStringData dataFields = new FixedLengthStringData(8).isAPartOf(dataArea, 0);
	public ZonedDecimalData autorate = DD.autorate.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData indxflg = DD.indxflg.copy().isAPartOf(dataFields,5);
	public FixedLengthStringData premind = DD.premind.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData sumasind = DD.sumasind.copy().isAPartOf(dataFields,7);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(16).isAPartOf(dataArea, 8);
	public FixedLengthStringData autorateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData indxflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData premindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData sumasindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 24);
	public FixedLengthStringData[] autorateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] indxflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] premindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] sumasindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData windowRow = DD.windrow.copyToZonedDecimal();
	public ZonedDecimalData windowColumn = DD.windcolm.copyToZonedDecimal();


	public LongData S5605screenWritten = new LongData(0);
	public LongData S5605windowWritten = new LongData(0);
	public LongData S5605hideWritten = new LongData(0);
	public LongData S5605protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5605ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(premindOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumasindOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(indxflgOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(autorateOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {premind, sumasind, indxflg, autorate};
		screenOutFields = new BaseData[][] {premindOut, sumasindOut, indxflgOut, autorateOut};
		screenErrFields = new BaseData[] {premindErr, sumasindErr, indxflgErr, autorateErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5605screen.class;
		protectRecord = S5605protect.class;
		hideRecord = S5605hide.class;
	}

}
