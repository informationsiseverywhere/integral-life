package com.csc.life.general.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.datatype.*;
import com.csc.smart400framework.SmartVarModel;
import com.csc.common.DD;

/**
 * Screen Variables for s5696
 * @version 1.0 Generated on Thu Jul 04 15:41:50 SGT 2013
 * @author CSC
 */
public class S5696ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(156);
	public FixedLengthStringData dataFields = new FixedLengthStringData(60).isAPartOf(dataArea, 0);
	//---------------data datafield contents
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0); 
	public FixedLengthStringData zdmsion = DD.zdmsion.copy().isAPartOf(dataFields,1); 
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,16); 
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,46); 
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,51); 
	public FixedLengthStringData flagbancass = new FixedLengthStringData(1).isAPartOf(dataFields, 59); // flag bancass

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 60); 
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData zdmsionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData flagbancassErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20); // TMLI Bancass
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea,84);

	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] zdmsionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	
	public FixedLengthStringData[] flagbancassOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60); // TMLI Bancass
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public GeneralTable S5696screensfl = new GeneralTable(AppVars.getInstance());
	public LongData S5696screenctlWritten = new LongData(0);
	public LongData S5696screensflWritten = new LongData(0);
	public LongData S5696screenWritten = new LongData(0);
	public LongData S5696windowWritten = new LongData(0);
	public LongData S5696hideWritten = new LongData(0);
	public LongData S5696protectWritten = new LongData(0);

	public boolean hasSubfile() { 
		return false;
	}


	public S5696ScreenVars() {
		super();
		initialiseScreenVars();
	}

	
	protected void initialiseScreenVars() {
	
	fieldIndMap.put(companyOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(zdmsionOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});	
	fieldIndMap.put(itemOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(longdescOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(tablOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(flagbancassOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	
	
	screenFields = new BaseData[] { company,zdmsion,item,longdesc,tabl,flagbancass };
	screenOutFields = new BaseData[][] { companyOut,zdmsionOut,itemOut,longdescOut,tablOut,flagbancassOut};
	screenErrFields = new BaseData[] {  companyErr,zdmsionErr,itemErr,longdescErr,tablErr,flagbancassErr};
	screenDateFields = new BaseData[] {};
	screenDateErrFields = new BaseData[] {};
	screenDateDispFields = new BaseData[] {};
	screenDataArea = dataArea;
	errorInds = errorIndicators;
	screenRecord = S5696screen.class;
	protectRecord = S5696protect.class;

	}

 

}
