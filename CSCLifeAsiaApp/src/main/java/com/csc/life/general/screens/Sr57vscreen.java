/*
 * File: Sr57vscreen.java
 * Date: {{DATETIME}}
 * Author: Automated screen Code template
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
package com.csc.life.general.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.*;
import com.quipoz.COBOLFramework.util.COBOLAppVars;

/**
 * Screen record for SCREEN Sr57v
 * @version 1.0 generated on {{TIMESTAMP}}
 * @author CSC
 */
public class Sr57vscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {0, 0, 0, 0}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr57vScreenVars sv = (Sr57vScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr57vscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr57vScreenVars screenVars = ( Sr57vScreenVars )pv;
	}
	
	/**
	 * Clear all the variables in S2500screen
	 */
	public static void clear(VarModel pv) {
		Sr57vScreenVars screenVars = (Sr57vScreenVars) pv;
	}
}	
