package com.csc.life.general.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5616screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 5, 18, 24, 15, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 17, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5616ScreenVars sv = (S5616ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5616screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5616ScreenVars screenVars = (S5616ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.currcode.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.language.setClassString("");
		screenVars.chdrsel.setClassString("");
		screenVars.pstatcode.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.singp.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.crrcdDisp.setClassString("");
		screenVars.life.setClassString("");
		screenVars.statuz.setClassString("");
		screenVars.clamant.setClassString("");
		screenVars.otheradjst.setClassString("");
		screenVars.resamt.setClassString("");
	}

/**
 * Clear all the variables in S5616screen
 */
	public static void clear(VarModel pv) {
		S5616ScreenVars screenVars = (S5616ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.currcode.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.language.clear();
		screenVars.chdrsel.clear();
		screenVars.pstatcode.clear();
		screenVars.currcd.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.billfreq.clear();
		screenVars.singp.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.crtable.clear();
		screenVars.jlife.clear();
		screenVars.crrcdDisp.clear();
		screenVars.crrcd.clear();
		screenVars.life.clear();
		screenVars.statuz.clear();
		screenVars.clamant.clear();
		screenVars.otheradjst.clear();
		screenVars.resamt.clear();
	}
}
