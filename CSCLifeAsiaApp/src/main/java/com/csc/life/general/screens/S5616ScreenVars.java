package com.csc.life.general.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5616
 * @version 1.0 generated on 30/08/09 06:46
 * @author Quipoz
 */
public class S5616ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(433);
	public FixedLengthStringData dataFields = new FixedLengthStringData(129).isAPartOf(dataArea, 0);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,2);
	public ZonedDecimalData clamant = DD.clamant.copyToZonedDecimal().isAPartOf(dataFields,12);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,29);
	public ZonedDecimalData crrcd = DD.crrcd.copyToZonedDecimal().isAPartOf(dataFields,31);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,43);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(dataFields,46);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,49);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData language = DD.language.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,60);
	public ZonedDecimalData otheradjst = DD.otheradjst.copyToZonedDecimal().isAPartOf(dataFields,62);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(dataFields,79);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,81);
	public ZonedDecimalData resamt = DD.resamt.copyToZonedDecimal().isAPartOf(dataFields,89);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,106);
	public ZonedDecimalData singp = DD.singp.copyToZonedDecimal().isAPartOf(dataFields,108);
	public FixedLengthStringData statuz = DD.statuz.copy().isAPartOf(dataFields,125);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(76).isAPartOf(dataArea, 129);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData clamantErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData crrcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData languageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData otheradjstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData pstatcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData resamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData singpErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData statuzErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(228).isAPartOf(dataArea, 205);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] clamantOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] crrcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] languageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] otheradjstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] pstatcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] resamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] singpOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] statuzOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData crrcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData S5616screenWritten = new LongData(0);
	public LongData S5616protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5616ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrselOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pstatcodeOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(currcdOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ptdateOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreqOut,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(singpOut,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(coverageOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(riderOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtableOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jlifeOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crrcdOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lifeOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clamantOut,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(otheradjstOut,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(resamtOut,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {currcode, effdate, language, chdrsel, pstatcode, currcd, ptdate, billfreq, singp, coverage, rider, crtable, jlife, crrcd, life, statuz, clamant, otheradjst, resamt};
		screenOutFields = new BaseData[][] {currcodeOut, effdateOut, languageOut, chdrselOut, pstatcodeOut, currcdOut, ptdateOut, billfreqOut, singpOut, coverageOut, riderOut, crtableOut, jlifeOut, crrcdOut, lifeOut, statuzOut, clamantOut, otheradjstOut, resamtOut};
		screenErrFields = new BaseData[] {currcodeErr, effdateErr, languageErr, chdrselErr, pstatcodeErr, currcdErr, ptdateErr, billfreqErr, singpErr, coverageErr, riderErr, crtableErr, jlifeErr, crrcdErr, lifeErr, statuzErr, clamantErr, otheradjstErr, resamtErr};
		screenDateFields = new BaseData[] {effdate, ptdate, crrcd};
		screenDateErrFields = new BaseData[] {effdateErr, ptdateErr, crrcdErr};
		screenDateDispFields = new BaseData[] {effdateDisp, ptdateDisp, crrcdDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5616screen.class;
		protectRecord = S5616protect.class;
	}

}
