package com.csc.life.general.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.csc.fsu.accounting.tablestructures.T3589rec;
import com.csc.life.general.tablestructures.T5696rec;
import com.csc.fsu.general.recordstructures.Zsdmcde;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.ItempfDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
//import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;


/**
 * Sub-routine to extract the code from T1692 for SUN Dimension 5 Work area Branch code 
 * and return the code value to SUN Dimension main routine. This is applicable to various 
 * GL transactions.
 * 
 * @author vhukumagrawa
 *
 */
public class Zd5t1692 extends COBOLConvCodeModel{

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaSubr = new FixedLengthStringData(8).init("ZD5T1692");
	
//	private FixedLengthStringData wsaaT1692Array = new FixedLengthStringData(100400);
//	private FixedLengthStringData[] wsaaT1692Rec = FLSArrayPartOfStructure(200, 502, wsaaT1692Array, 0);
//	private FixedLengthStringData[] wsaaT1692Key = FLSDArrayPartOfArrayStructure(2, wsaaT1692Rec, 0);
//	private FixedLengthStringData[] wsaaT1692Branch = FLSDArrayPartOfArrayStructure(2, wsaaT1692Key, 0, SPACES);
//	private FixedLengthStringData[] wsaaT1692Data = FLSDArrayPartOfArrayStructure(500, wsaaT1692Rec, 2);
//	private FixedLengthStringData[] wsaaT1692Genarea = FLSDArrayPartOfArrayStructure(500, wsaaT1692Data, 0);
//	private int wsaaT1692Size = 200;
//	private ZonedDecimalData wsaaT1692Ix = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	
	private Zsdmcde zsdmcde = new Zsdmcde();
	private Itempf itempf;
	private List<Itempf> itemAl = new ArrayList<Itempf>();
	private ItempfDAO itempfDAO =  DAOFactory.getItempfDAO();
	//private T1692rec t1692rec = new T1692rec();
	
	//private ItemTableDAM itemIO = new ItemTableDAM();
	
	//private String t1692 = "T1692";
	private String tableName;
	private T5696rec t5696rec = new T5696rec();
	private T3589rec t3589rec = new T3589rec();
	private Map<String,String> t5696Map = new LinkedHashMap<String,String>();
	private Map<String,String> t3589Map = new LinkedHashMap<String,String>();
	private String zdmsion;
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit620
	}
	
	public void mainline(Object... parmArray)
	{
		zsdmcde.zsdmcdeRec = convertAndSetParam(zsdmcde.zsdmcdeRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}
	
	protected void startSubr010()
	{
		syserrrec.subrname.set(wsaaSubr);
		zsdmcde.statuz.set(varcom.oK);
		if(isEQ(wsaaFirstTime, SPACES)){
			//loadTable1000();
			readT3589();
			readT5696();
			wsaaFirstTime.set("Y");
		}
		
		if(isEQ(zsdmcde.branch, SPACES)){
			/*EXIT*/
			exitProgram();
		}
				
		if(isEQ(zsdmcde.agbrn,"Y"))
		{
			zdmsion = t5696Map.get(zsdmcde.branch.toString().trim());
			if(zdmsion != null && !"".equals(zdmsion.trim()))
			{
				zsdmcde.rtzdmsion.set(zdmsion);
			}
		}
		else
		{
			zdmsion = t3589Map.get(zsdmcde.branch.toString().trim());
			if(zdmsion != null && !"".equals(zdmsion.trim()))
			{
				zsdmcde.rtzdmsion.set(zdmsion);
			}
		}
//		for(wsaaT1692Ix.set(1); !isGT(wsaaT1692Ix, wsaaT1692Size); wsaaT1692Ix.add(1)){
//			if(isEQ(wsaaT1692Branch[wsaaT1692Ix.toInt()], zsdmcde.batcbrn)){
//				t1692rec.t1692Rec.set(wsaaT1692Genarea[wsaaT1692Ix.toInt()]);
//				if(isNE(t1692rec.zdmsions, SPACES)){
//					zsdmcde.rtzdmsion.set(t1692rec.zdmsions);
//				}
//				break;
//			}
//			
//			if(isEQ(wsaaT1692Branch[wsaaT1692Ix.toInt()], SPACES)){
//				break;
//			}
//			
//		}
		
		/*EXIT*/
		exitProgram();
	}
	
	protected void readT5696()
	{
		tableName = "T5696";
		readItempf();
		Iterator<Itempf> it = itemAl.iterator();
		while(it.hasNext())
		{
			itempf = it.next();
			t5696rec.t5696Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			t5696Map.put(itempf.getItemitem().trim(), t5696rec.zdmsion.toString().trim());
		}
	}
	
	protected void readT3589()
	{
		tableName = "T3589";
		readItempf();
		Iterator<Itempf> it = itemAl.iterator();
		while(it.hasNext())
		{
			itempf = it.next();
			t3589rec.t3589Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			t3589Map.put(itempf.getItemitem().trim(), t3589rec.zdmsion.toString().trim());
		}
	}
	
	protected void readItempf()
	{		
		itempf = new Itempf();
		itemAl.clear();
		itempf.setItempfx("IT");
		itempf.setItemcoy(zsdmcde.batccoy.toString().trim());
		itempf.setItemtabl(tableName);
		itempf.setValidflag("1");
		itemAl = itempfDAO.findByExample(itempf);
		if(itemAl.isEmpty())
		{
			syserrrec.params.set(itempf.getItemitem());
			fatalError600();
		}		
	}
	
//	private void loadTable1000(){
//		initialize(wsaaT1692Array);
//		
//		itemIO.setParams(SPACES);
//		itemIO.setItempfx("IT");
//		itemIO.setItemcoy(zsdmcde.batccoy);
//		itemIO.setItemtabl(t1692);
//		itemIO.setItemitem(SPACES);
//		itemIO.setStatuz(varcom.oK);
//		itemIO.setFunction(varcom.begn);
//		//itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
//		wsaaT1692Ix.set(1);
//		while(isNE(itemIO.getStatuz(), varcom.endp)){
//			SmartFileCode.execute(appVars, itemIO);
//			if (isNE(itemIO.getStatuz(),varcom.oK)
//				&& isNE(itemIO.getStatuz(),varcom.endp)) {
//					syserrrec.params.set(itemIO.getParams());
//					fatalError600();
//			}
//			
//			if(isEQ(itemIO.getStatuz(), varcom.endp)
//				|| isNE(itemIO.getItempfx(), "IT")
//				|| isNE(itemIO.getItemcoy(), zsdmcde.batccoy)
//				|| isNE(itemIO.getItemtabl(), t1692)){
//					itemIO.setStatuz(varcom.endp);
//			}
//			if(isEQ(itemIO.getStatuz(), varcom.oK)){
//				/* Item Found */
//				wsaaT1692Branch[wsaaT1692Ix.toInt()].set(itemIO.getItemitem());
//				wsaaT1692Genarea[wsaaT1692Ix.toInt()].set(itemIO.getGenarea());
//				/* Go to next item */
//				wsaaT1692Ix.add(1);
//				itemIO.setFunction(varcom.nextr);
//			}
//		}
//	}
	
	protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error610();
				}
				case exit620: {
					exit620();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void error610()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit620);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

	protected void exit620()
	{
		zsdmcde.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
