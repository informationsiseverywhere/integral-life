/*
 * File: P5161.java
 * Date: 30 August 2009 0:17:07
 * Author: Quipoz Limited
 * 
 * Class transformed from P5161.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.general.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.general.dataaccess.PovrTableDAM;
import com.csc.life.general.dataaccess.PovrgenTableDAM;
import com.csc.life.general.screens.S5161ScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*                  PREMIUM BREAKDOWN
*
*   This screen/program will show the broken down premium
*    with the data found on the POVR file. This program should
*    only be called in the event of a POVR record existing for
*    the coverage.
*
*****************************************************************
* </pre>
*/
public class P5161 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5161");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String povrrec = "POVRREC";
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaBillfreqX = new FixedLengthStringData(2);

	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaBillfreqX, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaBillfreq9 = new ZonedDecimalData(2, 0).isAPartOf(filler, 0).setUnsigned();
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private PovrTableDAM povrIO = new PovrTableDAM();
	private PovrgenTableDAM povrgenIO = new PovrgenTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private S5161ScreenVars sv = ScreenProgram.getScreenVars( S5161ScreenVars.class);

	public P5161() {
		super();
		screenVars = sv;
		new ScreenModel("S5161", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void initialise1000()
	{
		start1010();
	}

protected void start1010()
	{
		sv.dataArea.set(SPACES);
		sv.grossprem.set(ZERO);
		sv.agrossprem.set(ZERO);
		wsaaBatchkey.set(wsspcomn.batchkey);
		/*PERFORM VARYING WSAA-SUB FROM 1 BY 1 UNTIL WSAA-SUB > 25*/
		/*   MOVE ZERO                TO S5161-INSTAMNT(WSAA-SUB)*/
		/*                               S5161-ANNAMNT (WSAA-SUB)*/
		/*END-PERFORM.*/
		/*  Read the CHDR as we need to know the billing frequency.*/
		/*  Note that the logical to retrieve will be LNB for new business*/
		/*  and ENQ if this program is called from Contract Enquiry.*/
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isEQ(chdrlnbIO.getStatuz(), varcom.oK)) {
			wsaaBillfreqX.set(chdrlnbIO.getBillfreq());
		}
		else {
			chdrenqIO.setParams(SPACES);
			chdrenqIO.setFormat(chdrenqrec);
			chdrenqIO.setFunction("RETRV");
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrenqIO.getParams());
				syserrrec.statuz.set(chdrenqIO.getStatuz());
				fatalError600();
			}
			else {
				wsaaBillfreqX.set(chdrenqIO.getBillfreq());
			}
		}
		/*  Get the associated POVR record.*/
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(wsspcomn.chdrChdrcoy);
		povrIO.setChdrnum(wsspcomn.chdrChdrnum);
		povrIO.setFormat(povrrec);
		povrIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		sv.chdrnum.set(povrIO.getChdrnum());
		sv.coverage.set(povrIO.getCoverage());
		sv.rider.set(povrIO.getRider());
		sv.life.set(povrIO.getLife());
		movePovrToScreen1100();
	}

protected void movePovrToScreen1100()
	{
		/*START*/
		/*  Note that the values stored on the POVRPF file are annual.*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 25)); wsaaSub.add(1)){
			sv.annamnt[wsaaSub.toInt()].set(povrIO.getAnnamnt(wsaaSub));
			compute(sv.instamnt[wsaaSub.toInt()], 3).setRounded((div(povrIO.getAnnamnt(wsaaSub), wsaaBillfreq9)));
			zrdecplrec.amountIn.set(sv.instamnt[wsaaSub.toInt()]);
			callRounding5000();
			sv.instamnt[wsaaSub.toInt()].set(zrdecplrec.amountOut);
			sv.grossprem.add(sv.instamnt[wsaaSub.toInt()]);
			sv.agrossprem.add(sv.annamnt[wsaaSub.toInt()]);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/* Display the screen.                                             */
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*START*/
		/*    CALL 'S5161IO' USING SCRN-SCREEN-PARAMS, S5161-DATA-AREA.    */
		/* Under certain circumstances the contract needs to be released.. */
		/*    IF SCRN-STATUZ = SUBM AND CHDRENQ-STATUZ = O-K               */
		/*       MOVE CHDRENQREC          TO CHDRENQ-FORMAT                */
		/*       MOVE 'RLSE'              TO CHDRENQ-FUNCTION              */
		/*       CALL 'CHDRENQIO' USING CHDRENQ-PARAMS                     */
		/*       IF CHDRENQ-STATUZ NOT = O-K                               */
		/*          MOVE CHDRENQ-PARAMS   TO SYSR-PARAMS                   */
		/*          MOVE CHDRENQ-STATUZ   TO SYSR-STATUZ                   */
		/*          PERFORM 600-FATAL-ERROR                                */
		/*       ELSE                                                      */
		/*          MOVE POVR-PARAMS      TO POVRGEN-PARAMS                */
		/*          MOVE POVRGENREC       TO POVRGEN-FORMAT                */
		/*          MOVE 'RLSE'           TO POVRGEN-FUNCTION              */
		/*          CALL 'POVRGENIO' USING POVRGEN-PARAMS                  */
		/*          IF POVRGEN-STATUZ NOT = O-K NEXT SENTENCE.             */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*START*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		/*  Decide which transaction program is next.*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrlnbIO.getCntcurr());
		zrdecplrec.batctrcde.set(wsaaBatchkey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}
}
