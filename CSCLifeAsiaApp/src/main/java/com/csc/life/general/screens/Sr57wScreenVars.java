package com.csc.life.general.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen Variables for Sr57w
 * @version 1.0 Generated on Fri Sep 06 10:56:11 SGT 2013
 * @author CSC
 */
public class Sr57wScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(170);
	public FixedLengthStringData dataFields = new FixedLengthStringData(74).isAPartOf(dataArea, 0);
	//---------------data datafield contents
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0); 
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1); 
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9); 
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,39); 
	public FixedLengthStringData zdmsions = new FixedLengthStringData(30).isAPartOf(dataFields, 44);
	public FixedLengthStringData[] zdmsion = FLSArrayPartOfStructure(2, 15, zdmsions, 0);
	public FixedLengthStringData zdmsion01 = DD.zdmsion.copy().isAPartOf(zdmsions,0); 
	public FixedLengthStringData zdmsion02 = DD.zdmsion.copy().isAPartOf(zdmsions,15); 

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 74);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData zdmsionsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData[] zdmsionErr = FLSArrayPartOfStructure(2, 4, zdmsionsErr, 0);
	public FixedLengthStringData zdmsion01Err =new FixedLengthStringData(4).isAPartOf(zdmsionsErr,0); 
	public FixedLengthStringData zdmsion02Err =new FixedLengthStringData(4).isAPartOf(zdmsionsErr,4); 

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea,98);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData zdmsionsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] zdmsionOut = FLSArrayPartOfStructure(2, 12, zdmsionsOut, 0);
	public FixedLengthStringData[][] zdmsionO = FLSDArrayPartOfArrayStructure(12, 1, zdmsionOut, 0);
	public FixedLengthStringData[] zdmsion01Out = FLSArrayPartOfStructure(12, 1, zdmsionsOut, 0);
	public FixedLengthStringData[] zdmsion02Out = FLSArrayPartOfStructure(12, 1, zdmsionsOut, 12);

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public GeneralTable sr57wscreensfl = new GeneralTable(AppVars.getInstance());
	public LongData Sr57wscreenctlWritten = new LongData(0);
	public LongData Sr57wscreensflWritten = new LongData(0);
	public LongData Sr57wscreenWritten = new LongData(0);
	public LongData Sr57wwindowWritten = new LongData(0);
	public LongData Sr57whideWritten = new LongData(0);
	public LongData Sr57wprotectWritten = new LongData(0);

	public boolean hasSubfile() { 
		return false;
	}


	public Sr57wScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	protected void initialiseScreenVars() {
		fieldIndMap.put(companyOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(itemOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(longdescOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tablOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdmsion01Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdmsion02Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	
		screenFields = new BaseData[] { company,item,longdesc,tabl,zdmsion01,zdmsion02 };
		screenOutFields = new BaseData[][] { companyOut,itemOut,longdescOut,tablOut,zdmsion01Out,zdmsion02Out };
		screenErrFields = new BaseData[] {  companyErr,itemErr,longdescErr,tablErr,zdmsion01Err,zdmsion02Err  };
		screenDateFields = new BaseData[] {   };
		screenDateErrFields = new BaseData[] {   };
		screenDateDispFields = new BaseData[] {    };
	
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr57wscreen.class;
		protectRecord = Sr57wprotect.class;
	}

 

}
