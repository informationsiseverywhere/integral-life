package com.csc.life.general.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:07
 * Description:
 * Copybook name: T5601REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5601rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5601Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData indics = new FixedLengthStringData(25).isAPartOf(t5601Rec, 0);
  	public FixedLengthStringData[] indic = FLSArrayPartOfStructure(25, 1, indics, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(25).isAPartOf(indics, 0, FILLER_REDEFINE);
  	public FixedLengthStringData indic01 = new FixedLengthStringData(1).isAPartOf(filler, 0);
  	public FixedLengthStringData indic02 = new FixedLengthStringData(1).isAPartOf(filler, 1);
  	public FixedLengthStringData indic03 = new FixedLengthStringData(1).isAPartOf(filler, 2);
  	public FixedLengthStringData indic04 = new FixedLengthStringData(1).isAPartOf(filler, 3);
  	public FixedLengthStringData indic05 = new FixedLengthStringData(1).isAPartOf(filler, 4);
  	public FixedLengthStringData indic06 = new FixedLengthStringData(1).isAPartOf(filler, 5);
  	public FixedLengthStringData indic07 = new FixedLengthStringData(1).isAPartOf(filler, 6);
  	public FixedLengthStringData indic08 = new FixedLengthStringData(1).isAPartOf(filler, 7);
  	public FixedLengthStringData indic09 = new FixedLengthStringData(1).isAPartOf(filler, 8);
  	public FixedLengthStringData indic10 = new FixedLengthStringData(1).isAPartOf(filler, 9);
  	public FixedLengthStringData indic11 = new FixedLengthStringData(1).isAPartOf(filler, 10);
  	public FixedLengthStringData indic12 = new FixedLengthStringData(1).isAPartOf(filler, 11);
  	public FixedLengthStringData indic13 = new FixedLengthStringData(1).isAPartOf(filler, 12);
  	public FixedLengthStringData indic14 = new FixedLengthStringData(1).isAPartOf(filler, 13);
  	public FixedLengthStringData indic15 = new FixedLengthStringData(1).isAPartOf(filler, 14);
  	public FixedLengthStringData indic16 = new FixedLengthStringData(1).isAPartOf(filler, 15);
  	public FixedLengthStringData indic17 = new FixedLengthStringData(1).isAPartOf(filler, 16);
  	public FixedLengthStringData indic18 = new FixedLengthStringData(1).isAPartOf(filler, 17);
  	public FixedLengthStringData indic19 = new FixedLengthStringData(1).isAPartOf(filler, 18);
  	public FixedLengthStringData indic20 = new FixedLengthStringData(1).isAPartOf(filler, 19);
  	public FixedLengthStringData indic21 = new FixedLengthStringData(1).isAPartOf(filler, 20);
  	public FixedLengthStringData indic22 = new FixedLengthStringData(1).isAPartOf(filler, 21);
  	public FixedLengthStringData indic23 = new FixedLengthStringData(1).isAPartOf(filler, 22);
  	public FixedLengthStringData indic24 = new FixedLengthStringData(1).isAPartOf(filler, 23);
  	public FixedLengthStringData indic25 = new FixedLengthStringData(1).isAPartOf(filler, 24);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(475).isAPartOf(t5601Rec, 25, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5601Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5601Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}