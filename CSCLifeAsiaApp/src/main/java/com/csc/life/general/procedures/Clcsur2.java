/*
 * File: Clcsur2.java
 * Date: 29 August 2009 22:40:21
 * Author: Quipoz Limited
 *
 * Class transformed from CLCSUR2.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.general.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* This subroutine is called by surrender programs after obtaining
* the base surrender value for a coverage ..........
*
* Check the effective date against the paid to date and apply
* any refunded or additional premiums to the surrender value
* accordingly............
*
*****************************************************************
* </pre>
*/
public class Clcsur2 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "CLCSUR2";
	private ZonedDecimalData wsaaWholeMonths = new ZonedDecimalData(6, 0).setUnsigned();
	private PackedDecimalData wsaaMonthlyAmount = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaBillfreq = new ZonedDecimalData(2, 0).setUnsigned();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit9020
	}

	public Clcsur2() {
		super();
	}

public void mainline(Object... parmArray)
	{
		srcalcpy.surrenderRec = convertAndSetParam(srcalcpy.surrenderRec, parmArray, 0);
		try {
			main100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main100()
	{
		start110();
		exit190();
	}

protected void start110()
	{
		srcalcpy.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(srcalcpy.ptdate);
		datcon3rec.intDate2.set(srcalcpy.effdate);
		datcon3rec.frequency.set("12");
		datcon3rec.freqFactor.set(ZERO);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError9000();
		}
		wsaaBillfreq.set(srcalcpy.billfreq);
		compute(wsaaMonthlyAmount, 2).set((div(mult(wsaaBillfreq,srcalcpy.estimatedVal),12)));
		if (isLT(srcalcpy.effdate,srcalcpy.ptdate)) {
			wsaaWholeMonths.set(datcon3rec.freqFactor);
			compute(srcalcpy.actualVal, 2).set(add(srcalcpy.actualVal,(mult(wsaaWholeMonths,wsaaMonthlyAmount))));
		}
		else {
			datcon3rec.freqFactor.add(0.99999);
			wsaaWholeMonths.set(datcon3rec.freqFactor);
			compute(srcalcpy.actualVal, 2).set(sub(srcalcpy.actualVal,(mult(wsaaWholeMonths,wsaaMonthlyAmount))));
		}
		srcalcpy.status.set(varcom.endp);
	}

protected void exit190()
	{
		exitProgram();
	}

protected void fatalError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9010();
				}
				case exit9020: {
					exit9020();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9010()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit9020);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		srcalcpy.status.set(syserrrec.statuz);
		/*EXIT*/
		exitProgram();
	}
}
