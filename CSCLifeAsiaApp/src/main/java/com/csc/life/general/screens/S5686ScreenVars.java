package com.csc.life.general.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.datatype.*;
import com.csc.smart400framework.SmartVarModel;
import com.csc.common.DD;

/**
 * Screen Variables for S5686
 * @version 1.0 Generated on Mon Sep 09 14:17:17 SGT 2013
 * @author CSC
 */
public class S5686ScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(139);
	public FixedLengthStringData dataFields = new FixedLengthStringData(59).isAPartOf(dataArea, 0);
	//---------------data datafield contents
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0); 
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1); 
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9); 
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,39); 
	public FixedLengthStringData zdmsion = DD.zdmsion.copy().isAPartOf(dataFields,44); 

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 59);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData zdmsionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);


	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea,79);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] zdmsionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public GeneralTable s5686screensfl = new GeneralTable(AppVars.getInstance());
	public LongData S5686screenctlWritten = new LongData(0);
	public LongData S5686screensflWritten = new LongData(0);
	public LongData S5686screenWritten = new LongData(0);
	public LongData S5686windowWritten = new LongData(0);
	public LongData S5686hideWritten = new LongData(0);
	public LongData S5686protectWritten = new LongData(0);

	public boolean hasSubfile() { 
		return false;
	}

	public S5686ScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	protected void initialiseScreenVars() {
		fieldIndMap.put(companyOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(itemOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(longdescOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tablOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdmsionOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	
		screenFields = new BaseData[] { company,item,longdesc,tabl,zdmsion };
		screenOutFields = new BaseData[][] { companyOut,itemOut,longdescOut,tablOut,zdmsionOut };
		screenErrFields = new BaseData[] {  companyErr,itemErr,longdescErr,tablErr,zdmsionErr  };
		screenDateFields = new BaseData[] {   };
		screenDateErrFields = new BaseData[] {   };
		screenDateDispFields = new BaseData[] {    };
	
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5686screen.class;
		protectRecord = S5686protect.class;
	}

 

}
