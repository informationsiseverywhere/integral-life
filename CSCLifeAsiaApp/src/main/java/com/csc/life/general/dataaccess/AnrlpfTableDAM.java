package com.csc.life.general.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AnrlpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:54
 * Class transformed from ANRLPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AnrlpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 71;
	public FixedLengthStringData anrlrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData anrlpfRecord = anrlrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(anrlrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(anrlrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(anrlrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(anrlrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(anrlrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(anrlrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(anrlrec);
	public FixedLengthStringData premind = DD.premind.copy().isAPartOf(anrlrec);
	public FixedLengthStringData sumasind = DD.sumasind.copy().isAPartOf(anrlrec);
	public FixedLengthStringData indxflg = DD.indxflg.copy().isAPartOf(anrlrec);
	public PackedDecimalData autorate = DD.autorate.copy().isAPartOf(anrlrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(anrlrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(anrlrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(anrlrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AnrlpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AnrlpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AnrlpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AnrlpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AnrlpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AnrlpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AnrlpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ANRLPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"VALIDFLAG, " +
							"PREMIND, " +
							"SUMASIND, " +
							"INDXFLG, " +
							"AUTORATE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     validflag,
                                     premind,
                                     sumasind,
                                     indxflg,
                                     autorate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		validflag.clear();
  		premind.clear();
  		sumasind.clear();
  		indxflg.clear();
  		autorate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAnrlrec() {
  		return anrlrec;
	}

	public FixedLengthStringData getAnrlpfRecord() {
  		return anrlpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAnrlrec(what);
	}

	public void setAnrlrec(Object what) {
  		this.anrlrec.set(what);
	}

	public void setAnrlpfRecord(Object what) {
  		this.anrlpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(anrlrec.getLength());
		result.set(anrlrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}