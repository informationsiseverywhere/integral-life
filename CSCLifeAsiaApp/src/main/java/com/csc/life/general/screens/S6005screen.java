package com.csc.life.general.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6005screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 18, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6005ScreenVars sv = (S6005ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6005screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6005ScreenVars screenVars = (S6005ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.bappmeth01.setClassString("");
		screenVars.bappmeth02.setClassString("");
		screenVars.bappmeth03.setClassString("");
		screenVars.bappmeth04.setClassString("");
		screenVars.bappmeth05.setClassString("");
		screenVars.ind.setClassString("");
		screenVars.indic01.setClassString("");
		screenVars.indic02.setClassString("");
		screenVars.indic03.setClassString("");
	}

/**
 * Clear all the variables in S6005screen
 */
	public static void clear(VarModel pv) {
		S6005ScreenVars screenVars = (S6005ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.bappmeth01.clear();
		screenVars.bappmeth02.clear();
		screenVars.bappmeth03.clear();
		screenVars.bappmeth04.clear();
		screenVars.bappmeth05.clear();
		screenVars.ind.clear();
		screenVars.indic01.clear();
		screenVars.indic02.clear();
		screenVars.indic03.clear();
	}
}
