package com.csc.life.general.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import com.csc.fsu.general.recordstructures.Zsdmcde;
import com.csc.life.general.tablestructures.T5685rec;
import com.csc.life.general.tablestructures.T5686rec;
import com.csc.life.general.tablestructures.Tr57wrec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescpfDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.ItempfDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Sub-routine to extract the code from T5686 or TAC02 for SUN Dimension 1 Product Type code 
 * and return the code value to SUN Dimension main routine. This is applicable to various 
 * GL transactions.
 * 
 * @author vhukumagrawa
 *
 */
public class Zd1t5686 extends COBOLConvCodeModel{

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaSubr = new FixedLengthStringData(8).init("ZD1T5686");
	private FixedLengthStringData wsaaItemItem = new FixedLengthStringData(8);
	//TMLII-1549 - issue 1
//	private FixedLengthStringData wsaaTempItem = new FixedLengthStringData(1).isAPartOf(wsaaItemItem, 4);
	private FixedLengthStringData wsaaTempItem = new FixedLengthStringData(1).isAPartOf(wsaaItemItem, 3);
	private FixedLengthStringData wsaaStfund = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBnftyp = new FixedLengthStringData(1);
	private PackedDecimalData wsaaShno = new PackedDecimalData(2, 0);
	private FixedLengthStringData wsaaFnd = new FixedLengthStringData(1);
	
	private FixedLengthStringData wsaaT5686Array = new FixedLengthStringData(30060);
	private FixedLengthStringData[] wsaaT5686Rec = FLSArrayPartOfStructure(60, 501, wsaaT5686Array, 0);
	private FixedLengthStringData[] wsaaT5686Key = FLSDArrayPartOfArrayStructure(1, wsaaT5686Rec, 0);
	private FixedLengthStringData[] wsaaT5686Stfund = FLSDArrayPartOfArrayStructure(1, wsaaT5686Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5686Data = FLSDArrayPartOfArrayStructure(500, wsaaT5686Rec, 1);
	private FixedLengthStringData[] wsaaT5686Genarea = FLSDArrayPartOfArrayStructure(500, wsaaT5686Data, 0);
	private int wsaaT5686Size = 60;
	private ZonedDecimalData wsaaT5686Ix = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	
	private FixedLengthStringData wsaaTAC02Array = new FixedLengthStringData(152400);
	private FixedLengthStringData[] wsaaTAC02Rec = FLSArrayPartOfStructure(300, 508, wsaaTAC02Array, 0);
	private FixedLengthStringData[] wsaaTAC02Key = FLSDArrayPartOfArrayStructure(8, wsaaTAC02Rec, 0);
	private FixedLengthStringData[] wsaaTAC02Itemitem = FLSDArrayPartOfArrayStructure(8, wsaaTAC02Key, 0, SPACES);
	private FixedLengthStringData[] wsaaTAC02Data = FLSDArrayPartOfArrayStructure(500, wsaaTAC02Rec, 8);
	private FixedLengthStringData[] wsaaTAC02Genarea = FLSDArrayPartOfArrayStructure(500, wsaaTAC02Data, 0);
	private int wsaaTAC02Size = 300;
	private ZonedDecimalData wsaaTAC02Ix = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	
	private Zsdmcde zsdmcde = new Zsdmcde();
	private T5686rec t5686rec = new T5686rec();
	private Tr57wrec tr57wrec = new Tr57wrec();
	
	private ItemTableDAM itemIO = new ItemTableDAM();
	
	private String t5686 = "T5686";
	private String tr57w = "TR57W";
	
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);	
	private DescpfDAO descDAO =  DAOFactory.getDescpfDAO();
	private Map<String, List<Itempf>> t5686ListMap;
	private Map<String, List<Itempf>> tr57wListMap;
	
	List<Descpf> tr57wList = null;
	List<Descpf> t5686List = null;
	private String strCompany;
	private String strEffDate;
	private static final String rp50 = "RP50";
	
	private Map<String,String> t5686Map = new LinkedHashMap<String,String>();
	private Map<String,String> tr57wMap = new LinkedHashMap<String,String>();
	private String tableName;
	private Itempf itempf;
	private List<Itempf> itemAl = new ArrayList<Itempf>();
	private ItempfDAO itempfDAO =  DAOFactory.getItempfDAO();
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit620
	}
	
	public void mainline(Object... parmArray)
	{
		zsdmcde.zsdmcdeRec = convertAndSetParam(zsdmcde.zsdmcdeRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}
	
	protected void startSubr010()
	{
		syserrrec.subrname.set(wsaaSubr);
		zsdmcde.statuz.set(varcom.oK);
		if(isEQ(wsaaFirstTime, SPACES)){
			loadTable1000();
			wsaaFirstTime.set("Y");
		}
		
		if(isEQ(zsdmcde.stfund, SPACES)){
			/*EXIT*/
			exitProgram();
		}
		
		wsaaStfund.set(zsdmcde.stfund);
		if(isNE(zsdmcde.tranref.toString().substring(28, 29), SPACES)){
			wsaaStfund.set(zsdmcde.tranref.toString().substring(28, 29));
		}
		
		wsaaBnftyp.set(zsdmcde.tranref.toString().substring(29, 30));
		wsaaFnd.set("N");
		wsaaShno.set(1);
		/* Initializing wsaaItemItem */
		StringBuilder sbr1 = new StringBuilder();
		sbr1.append(zsdmcde.sacstyp);
		sbr1.append(wsaaStfund);
		sbr1.append(zsdmcde.glsign);
		sbr1.append(wsaaBnftyp);
		wsaaItemItem.set(sbr1);
		
	//	while(!isGT(wsaaShno, 2)
			//	&& isNE(wsaaFnd, "Y")){
			
		//	search1:
		//	for(wsaaTAC02Ix.set(1); !isGT(wsaaTAC02Ix, wsaaTAC02Size); wsaaTAC02Ix.add(1)){
				readTr57w();
			//	if(tr57wrec.tr57wRec.length() > 0)
			//	{
				//	wsaaFnd.set("Y");
					//break search1;
			///	}
				String zdmsions = tr57wMap.get(wsaaItemItem.toString().trim());
				if(zdmsions != null && !"".equals(zdmsions.trim()))
				{
						zsdmcde.rtzdmsion.set(zdmsions);
						exitProgram();
				}
			//	if(isNE(tr57wrec.zdmsion01, SPACES)){
				//	zsdmcde.rtzdmsion.set(tr57wrec.zdmsion01);
				//	exitProgram();
			//	}
		//	}
		
			//if(isEQ(wsaaFnd, "N")){
			//	wsaaShno.add(1);
			//}
			//if(isEQ(wsaaShno, 2)){
			//	wsaaTempItem.set("*");
			//}
	//	}
		
		//if(isEQ(wsaaFnd, "Y")){
			
			/*EXIT*/
			
		//}
		readT5686();
		//for(wsaaT5686Ix.set(1); !isGT(wsaaT5686Ix, wsaaT5686Size); wsaaT5686Ix.add(1)){
			
			//if(isEQ(t5686rec.zdmsion[wsaaT5686Ix.toInt()], zsdmcde.stfund)){
			String zdmsion = t5686Map.get(zsdmcde.stfund.toString().trim());
			if(zdmsion != null && !"".equals(zdmsion.trim()))
			{
					zsdmcde.rtzdmsion.set(zdmsion);
					exitProgram();
			}
					//break;
				//}		
			
			
		//}
		/*EXIT*/
		//exitProgram();
	}
	
	private void loadTable1000(){
		/* Load items of T5686 */
		initialize(wsaaT5686Array);
		initialize(wsaaTAC02Array);
		
		strCompany = zsdmcde.batccoy.toString();
		strEffDate = zsdmcde.beffdate.toString();	
		/*itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(zsdmcde.batccoy);
		itemIO.setItemtabl(t5686);
		itemIO.setItemitem(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setFunction(varcom.begn);
		//itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		wsaaT5686Ix.set(1);
		while(isNE(itemIO.getStatuz(), varcom.endp)){
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)
				&& isNE(itemIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(itemIO.getParams());
					fatalError600();
			}
			
			if(isEQ(itemIO.getStatuz(), varcom.endp)
				|| isNE(itemIO.getItempfx(), "IT")
				|| isNE(itemIO.getItemcoy(), zsdmcde.batccoy)
				|| isNE(itemIO.getItemtabl(), t5686)){
					itemIO.setStatuz(varcom.endp);
			}
			if(isEQ(itemIO.getStatuz(), varcom.oK)){
				
				wsaaT5686Stfund[wsaaT5686Ix.toInt()].set(itemIO.getItemitem());
				wsaaT5686Genarea[wsaaT5686Ix.toInt()].set(itemIO.getGenarea());
				
				wsaaT5686Ix.add(1);
				itemIO.setFunction(varcom.nextr);
			}
		}
		
		 Load items of TAC02 
		
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(zsdmcde.batccoy);
		itemIO.setItemtabl(tr57w);
		itemIO.setItemitem(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setFunction(varcom.begn);
		//itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		wsaaTAC02Ix.set(1);
		while(isNE(itemIO.getStatuz(), varcom.endp)){
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)
				&& isNE(itemIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(itemIO.getParams());
					fatalError600();
			}
			
			if(isEQ(itemIO.getStatuz(), varcom.endp)
				|| isNE(itemIO.getItempfx(), "IT")
				|| isNE(itemIO.getItemcoy(), zsdmcde.batccoy)
				|| isNE(itemIO.getItemtabl(), tr57w)){
					itemIO.setStatuz(varcom.endp);
			}
			if(isEQ(itemIO.getStatuz(), varcom.oK)){
				/* Item Found
				wsaaTAC02Itemitem[wsaaTAC02Ix.toInt()].set(itemIO.getItemitem());
				wsaaTAC02Genarea[wsaaTAC02Ix.toInt()].set(itemIO.getGenarea());
				// Go to next item 
				wsaaTAC02Ix.add(1);
				itemIO.setFunction(varcom.nextr);
			}
			
		}*/
		t5686ListMap = itemDAO.loadSmartTable("IT", strCompany, "T5686");
		//readT5686();
		
		tr57wListMap = itemDAO.loadSmartTable("IT", strCompany, "TR57W");
		//readTr57w();		
		
	}
	
	protected void readT5686(){
tableName = "T5686";
		
		
		itempf = new Itempf();
		itemAl.clear();
		itempf.setItempfx("IT");
		itempf.setItemcoy(zsdmcde.batccoy.toString().trim());
		itempf.setItemtabl(tableName);
		itempf.setValidflag("1");
		//itempf.setItemitem(stFund);
		itemAl = itempfDAO.findByExample(itempf);
		if(itemAl.isEmpty())
		{
			syserrrec.params.set(itempf.getItemitem());
			fatalError600();
		}	
		Iterator<Itempf> it = itemAl.iterator();
		while(it.hasNext())
		{
			itempf = it.next();
			t5686rec.t5686Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			t5686Map.put(itempf.getItemitem().trim(), t5686rec.zdmsions.toString().trim());
		}
	}
	
	protected void readTr57w(){
		tableName = "TR57W";
		
		
		itempf = new Itempf();
		itemAl.clear();
		itempf.setItempfx("IT");
		itempf.setItemcoy(zsdmcde.batccoy.toString().trim());
		itempf.setItemtabl(tableName);
		itempf.setValidflag("1");
		//itempf.setItemitem(itemItem);
		itemAl = itempfDAO.findByExample(itempf);
		if(itemAl.isEmpty())
		{
			syserrrec.params.set(itempf.getItemitem());
			fatalError600();
		}	
		Iterator<Itempf> it = itemAl.iterator();
		while(it.hasNext())
		{
			itempf = it.next();
			tr57wrec.tr57wRec.set(StringUtil.rawToString(itempf.getGenarea()));
			tr57wMap.put(itempf.getItemitem().trim(), tr57wrec.zdmsion01.toString().trim());
		}
	}
	
	protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error610();
				}
				case exit620: {
					exit620();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void error610()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit620);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

	protected void exit620()
	{
		zsdmcde.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
