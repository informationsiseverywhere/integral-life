package com.csc.life.general.tablestructures;

import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import java.util.Arrays;

import com.quipoz.COBOLFramework.COBOLFunctions;

public class Tr57xrec extends ExternalData {

	public FixedLengthStringData tr57xRec = new FixedLengthStringData(500); 

        
	public FixedLengthStringData companys = new FixedLengthStringData(1).isAPartOf(tr57xRec,0);
	public FixedLengthStringData[] company = FLSArrayPartOfStructure(1, 1, companys, 0);
	
	public FixedLengthStringData items = new FixedLengthStringData(8).isAPartOf(tr57xRec,1);
	public FixedLengthStringData[] item = FLSArrayPartOfStructure(8, 1, items, 0);
	
	public FixedLengthStringData longdescs = new FixedLengthStringData(30).isAPartOf(tr57xRec,9);
	public FixedLengthStringData[] longdesc = FLSArrayPartOfStructure(30, 1, longdescs, 0);
	
	public FixedLengthStringData rdocpfxs = new FixedLengthStringData(30).isAPartOf(tr57xRec,39);
	public FixedLengthStringData[] rdocpfx = FLSArrayPartOfStructure(15,2,rdocpfxs, 0);
	public FixedLengthStringData rdocpfx01 = new FixedLengthStringData(2).isAPartOf(rdocpfxs, 0);
	public FixedLengthStringData rdocpfx02 = new FixedLengthStringData(2).isAPartOf(rdocpfxs, 2);
	public FixedLengthStringData rdocpfx03 = new FixedLengthStringData(2).isAPartOf(rdocpfxs, 4);
	public FixedLengthStringData rdocpfx04 = new FixedLengthStringData(2).isAPartOf(rdocpfxs, 6);
	public FixedLengthStringData rdocpfx05 = new FixedLengthStringData(2).isAPartOf(rdocpfxs, 8);
	public FixedLengthStringData rdocpfx06 = new FixedLengthStringData(2).isAPartOf(rdocpfxs, 10);
	public FixedLengthStringData rdocpfx07 = new FixedLengthStringData(2).isAPartOf(rdocpfxs, 12);
	public FixedLengthStringData rdocpfx08 = new FixedLengthStringData(2).isAPartOf(rdocpfxs, 14);
	public FixedLengthStringData rdocpfx09 = new FixedLengthStringData(2).isAPartOf(rdocpfxs, 16);
	public FixedLengthStringData rdocpfx10 = new FixedLengthStringData(2).isAPartOf(rdocpfxs, 18);
	public FixedLengthStringData rdocpfx11 = new FixedLengthStringData(2).isAPartOf(rdocpfxs, 20);
	public FixedLengthStringData rdocpfx12 = new FixedLengthStringData(2).isAPartOf(rdocpfxs, 22);
	public FixedLengthStringData rdocpfx13 = new FixedLengthStringData(2).isAPartOf(rdocpfxs, 24);
	public FixedLengthStringData rdocpfx14 = new FixedLengthStringData(2).isAPartOf(rdocpfxs, 26);
	public FixedLengthStringData rdocpfx15 = new FixedLengthStringData(2).isAPartOf(rdocpfxs, 28);

	public FixedLengthStringData sacscodes = new FixedLengthStringData(30).isAPartOf(tr57xRec,69);
	public FixedLengthStringData[] sacscode = FLSArrayPartOfStructure(15,2,sacscodes, 0);
	public FixedLengthStringData sacscode01 = new FixedLengthStringData(2).isAPartOf(sacscodes, 0);
	public FixedLengthStringData sacscode02 = new FixedLengthStringData(2).isAPartOf(sacscodes, 2);
	public FixedLengthStringData sacscode03 = new FixedLengthStringData(2).isAPartOf(sacscodes, 4);
	public FixedLengthStringData sacscode04 = new FixedLengthStringData(2).isAPartOf(sacscodes, 6);
	public FixedLengthStringData sacscode05 = new FixedLengthStringData(2).isAPartOf(sacscodes, 8);
	public FixedLengthStringData sacscode06 = new FixedLengthStringData(2).isAPartOf(sacscodes, 10);
	public FixedLengthStringData sacscode07 = new FixedLengthStringData(2).isAPartOf(sacscodes, 12);
	public FixedLengthStringData sacscode08 = new FixedLengthStringData(2).isAPartOf(sacscodes, 14);
	public FixedLengthStringData sacscode09 = new FixedLengthStringData(2).isAPartOf(sacscodes, 16);
	public FixedLengthStringData sacscode10 = new FixedLengthStringData(2).isAPartOf(sacscodes, 18);
	public FixedLengthStringData sacscode11 = new FixedLengthStringData(2).isAPartOf(sacscodes, 20);
	public FixedLengthStringData sacscode12 = new FixedLengthStringData(2).isAPartOf(sacscodes, 22);
	public FixedLengthStringData sacscode13 = new FixedLengthStringData(2).isAPartOf(sacscodes, 24);
	public FixedLengthStringData sacscode14 = new FixedLengthStringData(2).isAPartOf(sacscodes, 26);
	public FixedLengthStringData sacscode15 = new FixedLengthStringData(2).isAPartOf(sacscodes, 28);
	
	public FixedLengthStringData sacstypes = new FixedLengthStringData(30).isAPartOf(tr57xRec,99);
	public FixedLengthStringData[] sacstype = FLSArrayPartOfStructure(15,2,sacstypes, 0);
	public FixedLengthStringData sacstype01 = new FixedLengthStringData(2).isAPartOf(sacstypes, 0);
	public FixedLengthStringData sacstype02 = new FixedLengthStringData(2).isAPartOf(sacstypes, 2);
	public FixedLengthStringData sacstype03 = new FixedLengthStringData(2).isAPartOf(sacstypes, 4);
	public FixedLengthStringData sacstype04 = new FixedLengthStringData(2).isAPartOf(sacstypes, 6);
	public FixedLengthStringData sacstype05 = new FixedLengthStringData(2).isAPartOf(sacstypes, 8);
	public FixedLengthStringData sacstype06 = new FixedLengthStringData(2).isAPartOf(sacstypes, 10);
	public FixedLengthStringData sacstype07 = new FixedLengthStringData(2).isAPartOf(sacstypes, 12);
	public FixedLengthStringData sacstype08 = new FixedLengthStringData(2).isAPartOf(sacstypes, 14);
	public FixedLengthStringData sacstype09 = new FixedLengthStringData(2).isAPartOf(sacstypes, 16);
	public FixedLengthStringData sacstype10 = new FixedLengthStringData(2).isAPartOf(sacstypes, 18);
	public FixedLengthStringData sacstype11 = new FixedLengthStringData(2).isAPartOf(sacstypes, 20);
	public FixedLengthStringData sacstype12 = new FixedLengthStringData(2).isAPartOf(sacstypes, 22);
	public FixedLengthStringData sacstype13 = new FixedLengthStringData(2).isAPartOf(sacstypes, 24);
	public FixedLengthStringData sacstype14 = new FixedLengthStringData(2).isAPartOf(sacstypes, 26);
	public FixedLengthStringData sacstype15 = new FixedLengthStringData(2).isAPartOf(sacstypes, 28);
	
	public FixedLengthStringData tabls = new FixedLengthStringData(5).isAPartOf(tr57xRec,129);
	public FixedLengthStringData[] tabl = FLSArrayPartOfStructure(5, 1, tabls, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(366).isAPartOf(tr57xRec, 134, FILLER);
		
    public String[] fieldnamelist={"companys","items","longdescs","rdocpfx01s","rdocpfx02s","rdocpfx03s","rdocpfx04s","rdocpfx05s","rdocpfx06s","rdocpfx07s","rdocpfx08s","rdocpfx09s","rdocpfx10s","rdocpfx11s","rdocpfx12s","rdocpfx13s","rdocpfx14s","rdocpfx15s","sacscode01s","sacscode02s","sacscode03s","sacscode04s","sacscode05s","sacscode06s","sacscode07s","sacscode08s","sacscode09s","sacscode10s","sacscode11s","sacscode12s","sacscode13s","sacscode14s","sacscode15s","sacstype01s","sacstype02s","sacstype03s","sacstype04s","sacstype05s","sacstype06s","sacstype07s","sacstype08s","sacstype09s","sacstype10s","sacstype11s","sacstype12s","sacstype13s","sacstype14s","sacstype15s","tabls"};
		
	public Tr57xrec(){
	}
	
	public void initialize() {
		COBOLFunctions.initialize(tr57xRec);
	}	

	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr57xRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	public String[] getFieldNames(){
		return Arrays.copyOf(fieldnamelist, fieldnamelist.length);//IJTI-316
    }
}
