package com.csc.life.general.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5615
 * @version 1.0 generated on 30/08/09 06:46
 * @author Quipoz
 */
public class S5615ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(619);
	public FixedLengthStringData dataFields = new FixedLengthStringData(155).isAPartOf(dataArea, 0);
	public ZonedDecimalData age = DD.age.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData ageprem = DD.ageprem.copyToZonedDecimal().isAPartOf(dataFields,3);
	public ZonedDecimalData annamnt = DD.annamnt.copyToZonedDecimal().isAPartOf(dataFields,6);
	public ZonedDecimalData benterm = DD.benterm.copyToZonedDecimal().isAPartOf(dataFields,23);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,26);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,28);
	public FixedLengthStringData chdrtype = DD.chdrtype.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,41);
	public ZonedDecimalData crrcd = DD.crrcd.copyToZonedDecimal().isAPartOf(dataFields,43);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,51);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(dataFields,58);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,61);
	public FixedLengthStringData freqann = DD.freqann.copy().isAPartOf(dataFields,69);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(dataFields,71);
	public FixedLengthStringData jlsex = DD.jlsex.copy().isAPartOf(dataFields,73);
	public FixedLengthStringData language = DD.language.copy().isAPartOf(dataFields,74);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,75);
	public ZonedDecimalData premterm = DD.premterm.copyToZonedDecimal().isAPartOf(dataFields,77);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(dataFields,80);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,82);
	public ZonedDecimalData riskCessTerm = DD.rcestrm.copyToZonedDecimal().isAPartOf(dataFields,90);
	public ZonedDecimalData resamt = DD.resamt.copyToZonedDecimal().isAPartOf(dataFields,93);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,110);
	public FixedLengthStringData sex = DD.sex.copy().isAPartOf(dataFields,112);
	public ZonedDecimalData singp = DD.singp.copyToZonedDecimal().isAPartOf(dataFields,113);
	public FixedLengthStringData statuz = DD.statuz.copy().isAPartOf(dataFields,130);
	public ZonedDecimalData sumins = DD.sumins.copyToZonedDecimal().isAPartOf(dataFields,134);
	public FixedLengthStringData trancde = DD.trancde.copy().isAPartOf(dataFields,151);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(116).isAPartOf(dataArea, 155);
	public FixedLengthStringData ageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agepremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData annamntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bentermErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData chdrtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData crrcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData freqannErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData jlsexErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData languageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData premtermErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData pstatcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData rcestrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData resamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData sexErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData singpErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData statuzErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData suminsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData trancdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(348).isAPartOf(dataArea, 271);
	public FixedLengthStringData[] ageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agepremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] annamntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bentermOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] chdrtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] crrcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] freqannOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] jlsexOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] languageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] premtermOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] pstatcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] rcestrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] resamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] sexOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] singpOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] statuzOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] trancdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData crrcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData S5615screenWritten = new LongData(0);
	public LongData S5615protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5615ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrselOut,new String[] {"01",null, "-01","32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrtypeOut,new String[] {"23","31","-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pstatcodeOut,new String[] {"13",null, "-13","32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(currcdOut,new String[] {"12",null, "-12","32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(ptdateOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreqOut,new String[] {"14","31","-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(annamntOut,new String[] {"30","31","-30","31",null, null, null, null, null, null, null, null});
		fieldIndMap.put(suminsOut,new String[] {"27","31","-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(premtermOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(coverageOut,new String[] {"04","32","-04","32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcestrmOut,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(riderOut,new String[] {"05","32","-05","32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtableOut,new String[] {"06","31","-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jlifeOut,new String[] {"03",null, "-03","32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(crrcdOut,new String[] {"07","31","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jlsexOut,new String[] {"19","31","-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agepremOut,new String[] {"17","31","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lifeOut,new String[] {"02",null, "-02","32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(sexOut,new String[] {"18","31","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bentermOut,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ageOut,new String[] {"16","31","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqannOut,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(statuzOut,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(resamtOut,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {trancde, currcode, effdate, language, singp, chdrsel, chdrtype, pstatcode, currcd, ptdate, billfreq, annamnt, sumins, premterm, coverage, riskCessTerm, rider, crtable, jlife, crrcd, jlsex, ageprem, life, sex, benterm, age, freqann, statuz, resamt};
		screenOutFields = new BaseData[][] {trancdeOut, currcodeOut, effdateOut, languageOut, singpOut, chdrselOut, chdrtypeOut, pstatcodeOut, currcdOut, ptdateOut, billfreqOut, annamntOut, suminsOut, premtermOut, coverageOut, rcestrmOut, riderOut, crtableOut, jlifeOut, crrcdOut, jlsexOut, agepremOut, lifeOut, sexOut, bentermOut, ageOut, freqannOut, statuzOut, resamtOut};
		screenErrFields = new BaseData[] {trancdeErr, currcodeErr, effdateErr, languageErr, singpErr, chdrselErr, chdrtypeErr, pstatcodeErr, currcdErr, ptdateErr, billfreqErr, annamntErr, suminsErr, premtermErr, coverageErr, rcestrmErr, riderErr, crtableErr, jlifeErr, crrcdErr, jlsexErr, agepremErr, lifeErr, sexErr, bentermErr, ageErr, freqannErr, statuzErr, resamtErr};
		screenDateFields = new BaseData[] {effdate, ptdate, crrcd};
		screenDateErrFields = new BaseData[] {effdateErr, ptdateErr, crrcdErr};
		screenDateDispFields = new BaseData[] {effdateDisp, ptdateDisp, crrcdDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5615screen.class;
		protectRecord = S5615protect.class;
	}

}
