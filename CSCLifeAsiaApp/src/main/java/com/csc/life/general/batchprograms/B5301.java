/*
 * File: B5301.java
 * Date: 29 August 2009 21:02:55
 * Author: Quipoz Limited
 *
 * Class transformed from B5301.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.general.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.financials.procedures.Addrtrn;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Chkcurr;
import com.csc.fsu.general.procedures.Glkey;
import com.csc.fsu.general.recordstructures.Addrtrnrec;
import com.csc.fsu.general.recordstructures.Chkcurrrec;
import com.csc.fsu.general.recordstructures.Glkeyrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.general.tablestructures.T5637rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*  PREMIUM DEPOSIT INTEREST CALCULATION.
*
*  - Calculates Interest on Premium Deposit contracts.
*
*    Premium Deposit accounts are identified by contracts
*        that have a payment method type which is held on
*        table T5637.
*
*
*  1000-section.
*
*    Initialise fields.
*
*    Read T5637 for Premium Deposit details.
*
*    Check Due-date (held on T5637) is greater effective
*        date otherwise interest payments not yet due
*        - no processing to take place.
*
*  2000-section.
*
*    Start CHDRLIF (BEGN).
*
*    Read CHDRLIF (NEXTR) searching for contracts with
*        payment type equal to that held on T5637, a
*        validflag of '1', a service unit equal 'LP'.
*
*    If above criteria is met soft-lock contract.
*
*    Read (READR) the ACBL file building the key from the
*    CHDR file and Sub-Account type and code held on
*    table T5645, item 1.
*
*    Calculate Interest Amount based on Interest rate
*        held on T5637 and ACBL-SACSCURBAL.
*
*    Add Interest amount to ACBL-SACSCURBAL.
*
*    Add 1 to CHDRLIF-TRANNO.
*
*  3000-section.
*
*    Rewrite (UPDAT) CHDRLIF and ACBL files.
*
*    Write (WRITR) two RTRN's using subroutine ADDRTRN.
*        One record for the bankaccount and the second
*        for premium suspense. Sub-Account types and codes
*        are held on table T5645, items 2 & 3.
*
*    Write (WRITR) a PTRN record.
*
*    Release contract from soft-lock.
*
*    Perform    Until End of File
*
*
*   Control totals:
*     01  -  Number of Contracts read
*     02  -  Number of Contracts updated
*     03  -  Number of non-updates due to locked CHDR
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*****************************************************************
* </pre>
*/
public class B5301 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5301");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData wsaaSuspenseInterest = new PackedDecimalData(10, 2);
	private FixedLengthStringData wsaaTrandesc = new FixedLengthStringData(30);

	private FixedLengthStringData wsaaGenlKey = new FixedLengthStringData(17);
	private FixedLengthStringData wsaaGenlGenlpfx = new FixedLengthStringData(2).isAPartOf(wsaaGenlKey, 0);
	private FixedLengthStringData wsaaGenlGenlcoy = new FixedLengthStringData(1).isAPartOf(wsaaGenlKey, 2);
	private FixedLengthStringData wsaaGenlGenlcde = new FixedLengthStringData(14).isAPartOf(wsaaGenlKey, 3);
	private String wsaaChdrValid = "";
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private String itemrec = "ITEMREC";
	private String chdrlifrec = "CHDRLIFREC";
	private String acblrec = "ACBLREC";
	private String ptrnrec = "PTRNREC";
		/* TABLES */
	private String t1688 = "T1688";
	private String t5637 = "T5637";
	private String t5645 = "T5645";
	private String t5679 = "T5679";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Subsidiary account balance*/
	private AcblTableDAM acblIO = new AcblTableDAM();
	private Addrtrnrec addrtrnrec = new Addrtrnrec();
		/*Contract Header Life Fields*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private Chkcurrrec chkcurrrec = new Chkcurrrec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Glkeyrec glkeyrec = new Glkeyrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private T5637rec t5637rec = new T5637rec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit2590
	}

	public B5301() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(t5637);
		itdmIO.setItemitem("***");
		itdmIO.setItmfrm(bsscIO.getEffectiveDate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itdmIO.getItemtabl(),t5637)
		|| isNE(itdmIO.getItemitem(),"***")
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(bsprIO.getCompany());
			itdmIO.setItemtabl(t5637);
			itdmIO.setItemitem("***");
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5637rec.t5637Rec.set(itdmIO.getGenarea());
		chdrlifIO.setDataKey(SPACES);
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.begn);
		if (isLT(bsscIO.getEffectiveDate(),t5637rec.datedue)) {
			wsspEdterror.set(SPACES);
		}
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1688);
		descIO.setDescitem(bprdIO.getAuthCode());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		wsaaTrandesc.set(descIO.getLongdesc());
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		chdrIo2100();
		if (isEQ(chdrlifIO.getStatuz(),varcom.endp)) {
			wsaaEof.set("Y");
		}
		if (isEQ(wsaaEof,"Y")) {
			wsspEdterror.set(varcom.endp);
		}
		chdrlifIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void chdrIo2100()
	{
		/*CHDR-IO*/
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)
		&& isNE(chdrlifIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void edit2500()
	{
		try {
			edit2510();
		}
		catch (GOTOException e){
		}
	}

protected void edit2510()
	{
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		wsspEdterror.set(varcom.oK);
		if (isNE(chdrlifIO.getChdrcoy(),bsprIO.getCompany())
		|| isNE(chdrlifIO.getServunit(),"LP")
		|| isNE(chdrlifIO.getBillchnl(),t5637rec.mop)
		|| isNE(chdrlifIO.getValidflag(),"1")) {
			chdrlifIO.setStatuz(SPACES);
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit2590);
		}
		wsaaChdrValid = "N";
		for (wsaaSub.set(1); !(isGT(wsaaSub,12)); wsaaSub.add(1)){
			validateChdrStatus2700();
		}
		if (isNE(wsaaChdrValid,"Y")) {
			goTo(GotoLabel.exit2590);
		}
		softlockContract5000();
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			conlogrec.error.set(sftlockrec.statuz);
			conlogrec.params.set(sftlockrec.sftlockRec);
			callConlog003();
			contotrec.totno.set(ct03);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit2590);
		}
		readAcbl2600();
		compute(wsaaSuspenseInterest, 4).setRounded(div(mult(acblIO.getSacscurbal(),t5637rec.depint),100));
		setPrecision(acblIO.getSacscurbal(), 2);
		acblIO.setSacscurbal(add(acblIO.getSacscurbal(),wsaaSuspenseInterest));
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(add(chdrlifIO.getTranno(),1));
	}

protected void readAcbl2600()
	{
		para2610();
	}

protected void para2610()
	{
		acblIO.setDataArea(SPACES);
		acblIO.setRldgacct(ZERO);
		acblIO.setRldgcoy(bsprIO.getCompany());
		acblIO.setRldgacct(chdrlifIO.getChdrnum());
		acblIO.setOrigcurr(chdrlifIO.getCntcurr());
		acblIO.setSacscode(t5645rec.sacscode01);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(acblIO.getParams());
			fatalError600();
		}
	}

protected void validateChdrStatus2700()
	{
		/*CHECK*/
		if (isEQ(t5679rec.cnRiskStat[wsaaSub.toInt()],chdrlifIO.getStatcode())) {
			for (wsaaSub.set(1); !(isGT(wsaaSub,12)); wsaaSub.add(1)){
				validateChdrPremStatus2720();
			}
		}
		/*EXIT*/
	}

protected void validateChdrPremStatus2720()
	{
		/*CHECK*/
		if (isEQ(t5679rec.cnPremStat[wsaaSub.toInt()],chdrlifIO.getPstatcode())) {
			wsaaSub.set(13);
			wsaaChdrValid = "Y";
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		updatChdr3100();
		updatAcbl3200();
		writrRtrn3300();
		writrPtrn3400();
		releaseSoftlock5100();
		repositionChdr5200();
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		/*EXIT*/
	}

protected void updatChdr3100()
	{
		/*PARA*/
		chdrlifIO.setFunction(varcom.updat);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void updatAcbl3200()
	{
		/*PARA*/
		acblIO.setFunction(varcom.updat);
		acblIO.setFormat(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(acblIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void writrRtrn3300()
	{
		para3310();
	}

protected void para3310()
	{
		addrtrnrec.addrtrnRec.set(SPACES);
		addrtrnrec.transeq.set("0000");
		addrtrnrec.function.set("NPSTW");
		addrtrnrec.sacscode.set(t5645rec.sacscode02);
		addrtrnrec.sacstyp.set(t5645rec.sacstype02);
		getGenlkey3700();
		addrtrnrec.genlpfx.set(wsaaGenlGenlpfx);
		addrtrnrec.genlcoy.set(wsaaGenlGenlcoy);
		addrtrnrec.glcode.set(wsaaGenlGenlcde);
		addrtrnrec.batcpfx.set(batcdorrec.prefix);
		addrtrnrec.batccoy.set(batcdorrec.company);
		addrtrnrec.batcbrn.set(batcdorrec.branch);
		addrtrnrec.batcactyr.set(batcdorrec.actyear);
		addrtrnrec.batcactmn.set(batcdorrec.actmonth);
		addrtrnrec.batctrcde.set(batcdorrec.trcde);
		addrtrnrec.batcbatch.set(batcdorrec.batch);
		addrtrnrec.rdocpfx.set(chdrlifIO.getChdrpfx());
		addrtrnrec.rdoccoy.set(chdrlifIO.getChdrcoy());
		addrtrnrec.rdocnum.set(chdrlifIO.getChdrnum());
		addrtrnrec.rldgpfx.set(chdrlifIO.getCownpfx());
		addrtrnrec.rldgcoy.set(chdrlifIO.getCowncoy());
		addrtrnrec.rldgacct.set(chdrlifIO.getCownnum());
		addrtrnrec.postyear.set(batcdorrec.actyear);
		addrtrnrec.postmonth.set(batcdorrec.actmonth);
		addrtrnrec.contot.set(0);
		addrtrnrec.trandesc.set(wsaaTrandesc);
		addrtrnrec.origccy.set(acblIO.getOrigcurr());
		addrtrnrec.genlcur.set(acblIO.getOrigcurr());
		chkcurrrec.chkcurrRec.set(SPACES);
		chkcurrrec.function.set("RATE");
		chkcurrrec.currcode.set(addrtrnrec.origccy);
		chkcurrrec.company.set(addrtrnrec.batccoy);
		chkcurrrec.date_var.set(bsscIO.getEffectiveDate());
		callProgram(Chkcurr.class, chkcurrrec.chkcurrRec);
		if (isEQ(chkcurrrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(chkcurrrec.statuz);
			fatalError600();
		}
		addrtrnrec.currrate.set(chkcurrrec.rate);
		addrtrnrec.origamt.set(wsaaSuspenseInterest);
		compute(addrtrnrec.acctamt, 9).set(mult(addrtrnrec.origamt,addrtrnrec.currrate));
		addrtrnrec.effdate.set(bsscIO.getEffectiveDate());
		callProgram(Addrtrn.class, addrtrnrec.addrtrnRec);
		if (isNE(addrtrnrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(addrtrnrec.statuz);
			fatalError600();
		}
		addrtrnrec.addrtrnRec.set(SPACES);
		addrtrnrec.transeq.set("0001");
		addrtrnrec.function.set("PSTW");
		addrtrnrec.sacscode.set(t5645rec.sacscode03);
		addrtrnrec.sacstyp.set(t5645rec.sacstype03);
		getGenlkey3700();
		addrtrnrec.genlpfx.set(wsaaGenlGenlpfx);
		addrtrnrec.genlcoy.set(wsaaGenlGenlcoy);
		addrtrnrec.glcode.set(wsaaGenlGenlcde);
		addrtrnrec.batcpfx.set(batcdorrec.prefix);
		addrtrnrec.batccoy.set(batcdorrec.company);
		addrtrnrec.batcbrn.set(batcdorrec.branch);
		addrtrnrec.batcactyr.set(batcdorrec.actyear);
		addrtrnrec.batcactmn.set(batcdorrec.actmonth);
		addrtrnrec.batctrcde.set(batcdorrec.trcde);
		addrtrnrec.batcbatch.set(batcdorrec.batch);
		addrtrnrec.rdocpfx.set(chdrlifIO.getChdrpfx());
		addrtrnrec.rdoccoy.set(chdrlifIO.getChdrcoy());
		addrtrnrec.rdocnum.set(chdrlifIO.getChdrnum());
		addrtrnrec.rldgpfx.set(chdrlifIO.getChdrpfx());
		addrtrnrec.rldgcoy.set(chdrlifIO.getChdrcoy());
		addrtrnrec.rldgacct.set(chdrlifIO.getChdrnum());
		addrtrnrec.postyear.set(batcdorrec.actyear);
		addrtrnrec.postmonth.set(batcdorrec.actmonth);
		addrtrnrec.contot.set(0);
		addrtrnrec.trandesc.set(wsaaTrandesc);
		addrtrnrec.origccy.set(acblIO.getOrigcurr());
		addrtrnrec.genlcur.set(acblIO.getOrigcurr());
		addrtrnrec.origamt.set(wsaaSuspenseInterest);
		addrtrnrec.currrate.set(chkcurrrec.rate);
		compute(addrtrnrec.acctamt, 9).set(mult(addrtrnrec.origamt,addrtrnrec.currrate));
		addrtrnrec.effdate.set(bsscIO.getEffectiveDate());
		callProgram(Addrtrn.class, addrtrnrec.addrtrnRec);
		if (isNE(addrtrnrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(addrtrnrec.statuz);
			fatalError600();
		}
	}

protected void writrPtrn3400()
	{
		para3410();
	}

protected void para3410()
	{
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setBatcpfx(batcdorrec.prefix);
		ptrnIO.setBatccoy(batcdorrec.company);
		ptrnIO.setBatcbrn(batcdorrec.branch);
		ptrnIO.setBatcactyr(batcdorrec.actyear);
		ptrnIO.setBatcactmn(batcdorrec.actmonth);
		ptrnIO.setBatctrcde(batcdorrec.trcde);
		ptrnIO.setBatcbatch(batcdorrec.batch);
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setTransactionDate(bsscIO.getEffectiveDate());
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setPtrneff(bsscIO.getEffectiveDate());
		ptrnIO.setDatesub(bsscIO.getEffectiveDate());
		ptrnIO.setCrtuser(bsscIO.getUserName().toString());   //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			fatalError600();
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void getGenlkey3700()
	{
		para3700();
		endGlkey3700();
	}

protected void para3700()
	{
		glkeyrec.trancode.set(bprdIO.getAuthCode());
		glkeyrec.function.set("RETN");
		glkeyrec.company.set(batcdorrec.company);
		glkeyrec.sacscode.set(addrtrnrec.sacscode);
		glkeyrec.sacstyp.set(addrtrnrec.sacstyp);
		callProgram(Glkey.class, glkeyrec.glkeyRec);
		if (isNE(glkeyrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(glkeyrec.statuz);
			fatalError600();
		}
	}

protected void endGlkey3700()
	{
		wsaaGenlKey.set(glkeyrec.genlkey);
		addrtrnrec.sacscode.set(glkeyrec.sacscode);
		addrtrnrec.sacstyp.set(glkeyrec.sacstyp);
		addrtrnrec.glsign.set(glkeyrec.sign);
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void softlockContract5000()
	{
		para5010();
	}

protected void para5010()
	{
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(bsprIO.getCompany());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void releaseSoftlock5100()
	{
		para5110();
	}

protected void para5110()
	{
		sftlockrec.function.set("UNLK");
		sftlockrec.company.set(bsprIO.getCompany());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void repositionChdr5200()
	{
		/*PARA*/
		chdrlifIO.setFunction(varcom.begn);
		chdrIo2100();
		chdrlifIO.setFunction(varcom.nextr);
		/*EXIT*/
	}
}
