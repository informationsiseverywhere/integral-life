/*
 * File: P5614.java
 * Date: 30 August 2009 0:31:55
 * Author: Quipoz Limited
 * 
 * Class transformed from P5614.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.general.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceAll;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.life.contractservicing.tablestructures.T5541rec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.general.dataaccess.PovrTableDAM;
import com.csc.life.general.dataaccess.PovrgenTableDAM;
import com.csc.life.general.screens.S5614ScreenVars;
import com.csc.life.general.tablestructures.T5604rec;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*                  PREMIUM CALCULATION
*
*   This screen/program will show the parameters required to
*   call the SUM premium calculation routine PRMPM05.
*
*   It is a test rig for the routine which will become an
*   integrated part of the system.
*
*****************************************************************
* </pre>
*/
public class P5614 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5614");
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private String wsaaChdrEntered = "";
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaDateX = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaCentury = new ZonedDecimalData(2, 0).isAPartOf(wsaaDateX, 0).setUnsigned();
	private FixedLengthStringData wsaaDate = new FixedLengthStringData(6).isAPartOf(wsaaDateX, 2);
	private ZonedDecimalData wsaaYy = new ZonedDecimalData(2, 0).isAPartOf(wsaaDate, 0).setUnsigned();
	private ZonedDecimalData wsaaMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaDate, 2).setUnsigned();
	private ZonedDecimalData wsaaDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaDate, 4).setUnsigned();
	private PackedDecimalData wsaaS1 = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaS2 = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaFreqMultiple = new PackedDecimalData(5, 4);
	private FixedLengthStringData wsaaChdrChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
	private ZonedDecimalData wsaaAnnamntStore = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaSuminsStore = new ZonedDecimalData(17, 2);
	private String wsaaFirstTimeFlag = "";

	private FixedLengthStringData wsbbTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTranscd = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 0);
	private FixedLengthStringData wsbbCrtable = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 4);

	private FixedLengthStringData wsbbTranCurrency = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTran = new FixedLengthStringData(5).isAPartOf(wsbbTranCurrency, 0);
	private FixedLengthStringData wsbbCurrency = new FixedLengthStringData(3).isAPartOf(wsbbTranCurrency, 5);

	private FixedLengthStringData wsaaChdrsel = new FixedLengthStringData(10);
	private FixedLengthStringData[] wsaaChdrchar = FLSArrayPartOfStructure(10, 1, wsaaChdrsel, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(10).isAPartOf(wsaaChdrsel, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaChdrselLast8 = new FixedLengthStringData(8).isAPartOf(filler1, 2);
		/* TABLES */
	private static final String t5541 = "T5541";
	private static final String t5604 = "T5604";
	private static final String t5606 = "T5606";
	private static final String t5671 = "T5671";
	private static final String t5675 = "T5675";
	private static final String t5687 = "T5687";
	private static final String itemrec = "ITEMREC";
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String covrrec = "COVRREC";
	private static final String liferec = "LIFEREC";
	private static final String povrrec = "POVRREC";
	private static final String povrgenrec = "POVRGENREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private PovrTableDAM povrIO = new PovrTableDAM();
	private PovrgenTableDAM povrgenIO = new PovrgenTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Gensswrec gensswrec = new Gensswrec();
	private T5604rec t5604rec = new T5604rec();
	private T5606rec t5606rec = new T5606rec();
	private T5671rec t5671rec = new T5671rec();
	private T5675rec t5675rec = new T5675rec();
	private T5687rec t5687rec = new T5687rec();
	private T5541rec t5541rec = new T5541rec();
	private Premiumrec premiumrec = new Premiumrec();
	private S5614ScreenVars sv = ScreenProgram.getScreenVars( S5614ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private WsaaScrnfldStoresInner wsaaScrnfldStoresInner = new WsaaScrnfldStoresInner();
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		chdrenqNotFound1020, 
		covrNotFound1030, 
		lifeNotFound1040, 
		jlifeNotFound1050, 
		continue1060, 
		exit1090, 
		storeScreen5020, 
		continue5030
	}

	public P5614() {
		super();
		screenVars = sv;
		new ScreenModel("S5614", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start1010();
				}
				case chdrenqNotFound1020: {
					chdrenqNotFound1020();
				}
				case covrNotFound1030: {
					covrNotFound1030();
				}
				case lifeNotFound1040: {
					lifeNotFound1040();
				}
				case jlifeNotFound1050: {
					jlifeNotFound1050();
				}
				case continue1060: {
					continue1060();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		wsaaFirstTimeFlag = "Y";
		wsaaChdrChdrcoy.set("2");
		sv.dataArea.set(SPACES);
		wsaaPlanSuffix.set(ZERO);
		wsaaAnnamntStore.set(ZERO);
		wsaaSuminsStore.set(ZERO);
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaDate.set(getCobolDate());
		if (isGT(wsaaYy,40)) {
			wsaaCentury.set(19);
		}
		else {
			wsaaCentury.set(20);
		}
		sv.effdate.set(wsaaDateX);
		sv.language.set("D");
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrenqIO);
		chdrenqIO.setFunction("RLSE ");
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			wsaaChdrEntered = "N";
			sv.crtableOut[varcom.pr.toInt()].set("N");
			sv.coverageOut[varcom.pr.toInt()].set("N");
			sv.coverageOut[varcom.nd.toInt()].set("N");
			sv.riderOut[varcom.pr.toInt()].set("N");
			sv.riderOut[varcom.nd.toInt()].set("N");
			sv.chdrselOut[varcom.nd.toInt()].set("Y");
			sv.lifeOut[varcom.pr.toInt()].set("Y");
			sv.lifeOut[varcom.nd.toInt()].set("Y");
			sv.jlifeOut[varcom.pr.toInt()].set("Y");
			sv.jlifeOut[varcom.nd.toInt()].set("Y");
			goTo(GotoLabel.chdrenqNotFound1020);
		}
		wsaaChdrEntered = "Y";
		sv.chdrtypeOut[varcom.pr.toInt()].set("Y");
		sv.currcdOut[varcom.pr.toInt()].set("Y");
		sv.crtableOut[varcom.pr.toInt()].set("Y");
		sv.coverageOut[varcom.pr.toInt()].set("Y");
		sv.coverageOut[varcom.nd.toInt()].set("Y");
		sv.riderOut[varcom.pr.toInt()].set("Y");
		sv.riderOut[varcom.nd.toInt()].set("Y");
		sv.billfreqOut[varcom.pr.toInt()].set("Y");
		wsaaChdrChdrcoy.set(chdrenqIO.getChdrcoy());
		sv.chdrsel.set(chdrenqIO.getChdrnum());
		sv.chdrtype.set(chdrenqIO.getCnttype());
		sv.currcd.set(chdrenqIO.getCntcurr());
		sv.billfreq.set(chdrenqIO.getBillfreq());
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(chdrenqIO.getChdrcoy());
		covrIO.setChdrnum(chdrenqIO.getChdrnum());
		covrIO.setLife("01");
		covrIO.setPlanSuffix(ZERO);
		covrIO.setCoverage("01");
		covrIO.setRider("00");
		covrIO.setFormat(covrrec);
		covrIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(covrIO.getStatuz(),varcom.oK)
		&& isEQ(covrIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		&& isEQ(covrIO.getChdrnum(),chdrenqIO.getChdrnum())
		&& isEQ(covrIO.getLife(),"01")
		&& isEQ(covrIO.getCoverage(),"01")
		&& isEQ(covrIO.getRider(),"00")) {
			sv.coverage.set(covrIO.getCoverage());
			sv.rider.set(covrIO.getRider());
			sv.crtable.set(covrIO.getCrtable());
			sv.singp.set(covrIO.getSingp());
			sv.annamnt.set(ZERO);
			sv.sumins.set(covrIO.getSumins());
			sv.premterm.set(covrIO.getPremCessTerm());
			sv.riskCessTerm.set(covrIO.getRiskCessTerm());
			sv.benterm.set(covrIO.getBenCessTerm());
			if (isGT(sv.benterm,ZERO)
			&& isLT(sv.benterm,120)) {
				getBenfreq4600();
			}
			else {
				sv.freqann.set("00");
			}
		}
		else {
			goTo(GotoLabel.covrNotFound1030);
		}
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeIO.setLife("01");
		lifeIO.setJlife("00");
		lifeIO.setCurrfrom(varcom.maxdate);
		lifeIO.setFormat(liferec);
		lifeIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)
		&& isNE(lifeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifeIO.getStatuz(),varcom.oK)
		&& isEQ(lifeIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		&& isEQ(lifeIO.getChdrnum(),chdrenqIO.getChdrnum())
		&& isEQ(lifeIO.getLife(),"01")
		&& isEQ(lifeIO.getJlife(),"00")) {
			sv.life.set("01");
			sv.sex.set(lifeIO.getCltsex());
			sv.age.set(lifeIO.getAnbAtCcd());
		}
		else {
			goTo(GotoLabel.lifeNotFound1040);
		}
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeIO.setLife("01");
		lifeIO.setJlife("01");
		lifeIO.setCurrfrom(varcom.maxdate);
		lifeIO.setFormat(liferec);
		lifeIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)
		&& isNE(lifeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifeIO.getStatuz(),varcom.oK)
		&& isEQ(lifeIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		&& isEQ(lifeIO.getChdrnum(),chdrenqIO.getChdrnum())
		&& isEQ(lifeIO.getLife(),"01")
		&& isEQ(lifeIO.getJlife(),"01")) {
			sv.jlife.set("01");
			sv.jlsex.set(lifeIO.getCltsex());
			sv.ageprem.set(lifeIO.getAnbAtCcd());
			goTo(GotoLabel.continue1060);
		}
		else {
			goTo(GotoLabel.jlifeNotFound1050);
		}
	}

protected void chdrenqNotFound1020()
	{
		wsaaChdrChdrcoy.set("2");
		sv.chdrsel.set("0000000000");
		sv.chdrtype.set("KLV");
		sv.currcd.set("DM");
		sv.billfreq.set("12");
	}

protected void covrNotFound1030()
	{
		sv.coverage.set("01");
		sv.rider.set("00");
		sv.crtable.set("TF11");
		sv.premterm.set(20);
		sv.riskCessTerm.set(20);
		sv.sumins.set(100000);
		sv.singp.set(ZERO);
		sv.annamnt.set(ZERO);
		sv.benterm.set(999);
		sv.freqann.set("00");
	}

protected void lifeNotFound1040()
	{
		sv.life.set("01");
		sv.sex.set(SPACES);
		sv.age.set(ZERO);
	}

protected void jlifeNotFound1050()
	{
		sv.jlife.set("00");
		sv.jlsex.set(SPACES);
		sv.ageprem.set(999);
	}

protected void continue1060()
	{
		sv.pbind.set(SPACES);
		sv.pbindOut[varcom.nd.toInt()].set("Y");
		sv.pbindOut[varcom.pr.toInt()].set("Y");
		storeScrnfields5400();
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(wsaaFirstTimeFlag,"Y")) {
			wsaaFirstTimeFlag = "N";
		}
		else {
			checkPovrgen4500();
		}
		/* Display the screen.                                             */
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
			start2010();
		}

protected void start2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")) {
			return ;
		}
		wsspcomn.edterror.set("Y");
		if (isEQ(wsaaChdrEntered,"Y")
		&& (isNE(sv.coverage, wsaaScrnfldStoresInner.wsaaCoverageStore)
		|| isNE(sv.rider, wsaaScrnfldStoresInner.wsaaRiderStore)
		|| isNE(sv.crtable, wsaaScrnfldStoresInner.wsaaCrtableStore))) {
			checkCoverage5000();
			scrnparams.errorCode.set(errorsInner.d027);
			return ;
		}
		shuntChdrsel2200();
		if (isNE(wsaaChdrsel,"0000000000")) {
			if (isNE(wsaaChdrsel,NUMERIC)) {
				sv.chdrselErr.set(errorsInner.f200);
			}
		}
		if (isEQ(sv.chdrtype,SPACES)) {
			sv.chdrtypeErr.set(errorsInner.d020);
		}
		else {
			checkT56042400();
		}
		if (isEQ(sv.currcd,SPACES)) {
			sv.currcdErr.set(errorsInner.d020);
		}
		if (isEQ(sv.billfreq,SPACES)
		|| isNE(sv.billfreq,NUMERIC)) {
			sv.billfreqErr.set(errorsInner.d020);
		}
		if (isEQ(sv.coverage,SPACES)
		|| isNE(sv.coverage,NUMERIC)) {
			sv.coverageErr.set(errorsInner.d020);
		}
		if (isEQ(sv.rider,SPACES)
		|| isNE(sv.rider,NUMERIC)) {
			sv.riderErr.set(errorsInner.d020);
		}
		if (isEQ(sv.life,SPACES)
		|| isNE(sv.life,NUMERIC)) {
			sv.lifeErr.set(errorsInner.d020);
		}
		if (isEQ(sv.jlife,SPACES)
		|| isNE(sv.jlife,NUMERIC)) {
			sv.jlifeErr.set(errorsInner.d020);
		}
		if (isEQ(sv.crtable,SPACES)) {
			sv.crtableErr.set(errorsInner.d020);
		}
		/*    ELSE                                                         */
		/*       PERFORM 5200-CHECK-T5602.                                 */
		if (isEQ(sv.effdate,ZERO)
		|| isEQ(sv.effdate,varcom.maxdate)) {
			sv.effdateErr.set(errorsInner.d020);
		}
		if (isNE(sv.billfreq,NUMERIC)
		|| isEQ(sv.billfreq,ZERO)) {
			sv.billfreqErr.set(errorsInner.d020);
		}
		if (isLTE(sv.sumins,ZERO)
		&& isLTE(sv.annamnt,ZERO)) {
			sv.suminsErr.set(errorsInner.d020);
		}
		if ((isEQ(sv.annamnt,wsaaAnnamntStore)
		&& isEQ(sv.sumins,wsaaSuminsStore)
		&& (isEQ(sv.pbind,"X")
		|| isEQ(sv.pbind,"+")))) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isGT(sv.sumins,ZERO)
			&& isGT(sv.annamnt,ZERO)) {
				sv.suminsErr.set(errorsInner.d020);
			}
		}
		if (isEQ(sv.age,ZERO)
		|| isEQ(sv.age,999)
		|| isGT(sv.age,80)) {
			sv.ageErr.set(errorsInner.d034);
		}
		if (isNE(sv.jlsex,SPACES)
		&& (isEQ(sv.ageprem,ZERO)
		|| isEQ(sv.ageprem,999))) {
			sv.agepremErr.set(errorsInner.d020);
		}
		if (isNE(sv.ageprem,ZERO)
		&& isNE(sv.ageprem,999)) {
			if (isEQ(sv.jlsex,SPACES)) {
				sv.jlsexErr.set(errorsInner.d020);
			}
			if (isGT(sv.ageprem,80)) {
				sv.agepremErr.set(errorsInner.d034);
			}
		}
		if (isEQ(sv.sex,SPACES)) {
			sv.sexErr.set(errorsInner.d020);
		}
		/* IF     (S5614-PREMTERM       = ZERO OR 99)                   */
		if ((isEQ(sv.premterm,ZERO)
		|| isEQ(sv.premterm, 999))
		&& (isEQ(sv.riskCessTerm,ZERO)
		|| isEQ(sv.riskCessTerm,999))
		&& (isEQ(sv.benterm,ZERO)
		|| isEQ(sv.benterm,999))) {
			sv.premtermErr.set(errorsInner.d020);
			sv.riskCessTerm.set(errorsInner.d020);
			sv.bentermErr.set(errorsInner.d020);
		}
		if (isNE(sv.freqann,NUMERIC)) {
			sv.freqannErr.set(errorsInner.d020);
		}
		if (isNE(sv.pbind, " ")
		&& isNE(sv.pbind,"+")
		&& isNE(sv.pbind,"X")) {
			sv.pbindErr.set(errorsInner.g620);
		}
		/* Check for errors.*/
		if (isNE(sv.errorIndicators,SPACES)) {
			return ;
		}
		/* Check the coverage details entered as these may be*/
		/* different to the contract primary coverage........*/
		/* Check if any of the important fields have changed.*/
		/* Check if any of the important fields have changed.*/
		if (isNE(sv.life, wsaaScrnfldStoresInner.wsaaLifeStore)
		|| isNE(sv.age, wsaaScrnfldStoresInner.wsaaAgeStore)
		|| isNE(sv.sex, wsaaScrnfldStoresInner.wsaaSexStore)
		|| isNE(sv.jlife, wsaaScrnfldStoresInner.wsaaJlifeStore)
		|| isNE(sv.ageprem, wsaaScrnfldStoresInner.wsaaAgepremStore)
		|| isNE(sv.jlsex, wsaaScrnfldStoresInner.wsaaJlsexStore)
		|| isNE(sv.coverage, wsaaScrnfldStoresInner.wsaaCoverageStore)
		|| isNE(sv.rider, wsaaScrnfldStoresInner.wsaaRiderStore)
		|| isNE(sv.crtable, wsaaScrnfldStoresInner.wsaaCrtableStore)
		|| isNE(sv.premterm, wsaaScrnfldStoresInner.wsaaPremtermStore)
		|| isNE(sv.riskCessTerm, wsaaScrnfldStoresInner.wsaaRisktermStore)
		|| isNE(sv.benterm, wsaaScrnfldStoresInner.wsaaBentermStore)
		|| isNE(sv.freqann, wsaaScrnfldStoresInner.wsaaFreqannStore)
		|| isNE(sv.sumins,wsaaSuminsStore)) {
			if (isEQ(sv.pbind,"X")
			|| isEQ(sv.pbind,"+")) {
				scrnparams.errorCode.set(errorsInner.d027);
				sv.pbind.set(SPACES);
				sv.pbindOut[varcom.nd.toInt()].set("Y");
				sv.pbindOut[varcom.pr.toInt()].set("Y");
			}
		}
		if ((isEQ(sv.annamnt,wsaaAnnamntStore)
		&& isEQ(sv.sumins,wsaaSuminsStore)
		&& isEQ(sv.pbind,"X"))) {
			wsspcomn.edterror.set(varcom.oK);
			return ;
		}
		fillCopybook2100();
		storeScrnfields5400();
		getRoutine5100();
		rlsePovrgen5300();
		premiumrec.language.set(wsspcomn.language);
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/	
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End		*/	
		//****Ticket #ILIFE-2005 end
		if (isNE(premiumrec.statuz,varcom.oK)
		&& isNE(premiumrec.statuz,SPACES)) {
			sv.annamntErr.set(premiumrec.statuz);
		}
		else {
			sv.annamnt.set(premiumrec.calcPrem);
			wsaaAnnamntStore.set(premiumrec.calcPrem);
			sv.sumins.set(premiumrec.sumin);
			wsaaSuminsStore.set(premiumrec.sumin);
			if (isEQ(sv.pbind,"X")) {
				wsspcomn.edterror.set(varcom.oK);
			}
		}
	}

protected void fillCopybook2100()
	{
		start2110();
	}

protected void start2110()
	{
		premiumrec.premiumRec.set(SPACES);
		premiumrec.function.set("CALC");
		premiumrec.chdrChdrcoy.set(wsaaChdrChdrcoy);
		premiumrec.chdrChdrnum.set("99999999");
		if (isEQ(wsaaChdrselLast8,"00000000")) {
			premiumrec.effectdt.set(sv.effdate);
			premiumrec.ratingdate.set(sv.effdate);
			premiumrec.reRateDate.set(sv.effdate);
		}
		else {
			premiumrec.effectdt.set(covrIO.getCrrcd());
			premiumrec.ratingdate.set(chdrenqIO.getOccdate());
			premiumrec.reRateDate.set(chdrenqIO.getOccdate());
		}
		premiumrec.lifeLife.set("99");
		premiumrec.covrCoverage.set("99");
		premiumrec.covrRider.set("99");
		premiumrec.plnsfx.set(wsaaPlanSuffix);
		premiumrec.billfreq.set(sv.billfreq);
		premiumrec.mop.set(SPACES);
		premiumrec.lifeJlife.set("99");
		premiumrec.crtable.set(sv.crtable);
		premiumrec.mortcls.set(SPACES);
		premiumrec.currcode.set(sv.currcd);
		premiumrec.termdate.set(sv.premterm);
		premiumrec.duration.set(sv.premterm);
		premiumrec.sumin.set(sv.sumins);
		premiumrec.calcPrem.set(sv.annamnt);
		premiumrec.lage.set(sv.age);
		premiumrec.lsex.set(sv.sex);
		if (isGT(sv.ageprem,120)) {
			premiumrec.jlage.set(ZERO);
		}
		else {
			premiumrec.jlage.set(sv.ageprem);
		}
		premiumrec.jlsex.set(sv.jlsex);
		premiumrec.reasind.set(SPACES);
		premiumrec.freqann.set(sv.freqann);
		premiumrec.arrears.set(SPACES);
		premiumrec.advance.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.calcBasPrem.set(ZERO);
		premiumrec.calcLoaPrem.set(ZERO);
		premiumrec.dthperco.set(ZERO);
		premiumrec.riskCessTerm.set(sv.riskCessTerm);
		if (isEQ(sv.benterm,999)) {
			premiumrec.benCessTerm.set(ZERO);
		}
		else {
			premiumrec.benCessTerm.set(sv.benterm);
		}
		premiumrec.cnttype.set(sv.chdrtype);
	}

protected void shuntChdrsel2200()
	{
		/*START*/
		if (isEQ(sv.chdrsel,SPACES)) {
			wsaaChdrsel.set("0000000000");
			return ;
		}
		else {
			wsaaChdrsel.set(sv.chdrsel);
		}
		for (wsaaS1.set(10); !(isLT(wsaaS1,1)); wsaaS1.add(-1)){
			if (isEQ(wsaaChdrchar[wsaaS1.toInt()],SPACES)) {
				for (wsaaS2.set(wsaaS1); !(isLT(wsaaS2,2)); wsaaS2.add(-1)){
					compute(wsaaChdrchar[wsaaS2.toInt()], 0).set(wsaaChdrchar[sub(wsaaS2,1).toInt()]);
				}
				wsaaS1.set(11);
			}
		}
		wsaaChdrsel.set(inspectReplaceAll(wsaaChdrsel, SPACES, "0"));
	}

protected void checkT55412300()
	{
		start2310();
	}

protected void start2310()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemtabl(t5541);
		itdmIO.setItemcoy(wsaaChdrChdrcoy);
		itdmIO.setItemitem(t5604rec.xfreqAltBasis);
		itdmIO.setItmfrm(sv.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			if (isEQ(itdmIO.getStatuz(),varcom.endp)
			|| isNE(itdmIO.getItemcoy(),wsaaChdrChdrcoy)
			|| isNE(itdmIO.getItemtabl(),t5541)
			|| isNE(itdmIO.getItemitem(),t5604rec.xfreqAltBasis)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(errorsInner.t075);
				fatalError600();
			}
			else {
				t5541rec.t5541Rec.set(itdmIO.getGenarea());
			}
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub,12)); wsaaSub.add(1)){
			if (isEQ(t5541rec.freqcy[wsaaSub.toInt()],sv.billfreq)) {
				wsaaSub.set(13);
			}
			else {
				if (isGT(wsaaSub,11)) {
					syserrrec.params.set(itdmIO.getParams());
					syserrrec.statuz.set(errorsInner.t076);
					fatalError600();
				}
			}
		}
	}

protected void checkT56042400()
	{
		start2410();
	}

protected void start2410()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemtabl(t5604);
		itdmIO.setItemcoy(wsaaChdrChdrcoy);
		itdmIO.setItemitem(sv.chdrtype);
		itdmIO.setItmfrm(sv.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			if (isEQ(itdmIO.getStatuz(),varcom.endp)
			|| isNE(itdmIO.getItemcoy(),wsaaChdrChdrcoy)
			|| isNE(itdmIO.getItemtabl(),t5604)
			|| isNE(itdmIO.getItemitem(),sv.chdrtype)) {
				sv.chdrtypeErr.set(errorsInner.d035);
			}
			else {
				t5604rec.t5604Rec.set(itdmIO.getGenarea());
			}
		}
	}

protected void update3000()
	{
			start3010();
		}

protected void start3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")
		|| isEQ(sv.pbind,"X")) {
			return ;
		}
		if (isEQ(chdrenqIO.getStatuz(),varcom.oK)) {
			chdrenqIO.setFormat(chdrenqrec);
			chdrenqIO.setFunction("RLSE");
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrenqIO.getParams());
				syserrrec.statuz.set(chdrenqIO.getStatuz());
				fatalError600();
			}
		}
		rlsePovrgen5300();
	}

protected void whereNext4000()
	{
			start4010();
		}

protected void start4010()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		wsspcomn.nextprog.set(wsaaProg);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				saveProgram4100();
			}
		}
		if (isEQ(sv.pbind,"?")) {
			sv.pbind.set("+");
			wsspcomn.programPtr.add(1);
			povrIO.setFunction(varcom.rlse);
			povrIO.setFormat(povrrec);
			SmartFileCode.execute(appVars, povrIO);
			if (isNE(povrIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(povrIO.getParams());
				syserrrec.statuz.set(povrIO.getStatuz());
				fatalError600();
			}
		}
		else {
			if (isEQ(sv.pbind,"X")) {
				gensswrec.function.set("A");
				sv.pbind.set("?");
				callGenssw4300();
				return ;
			}
		}
		wsaaX.set(wsspcomn.programPtr);
		wsaaY.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			restoreProgram4200();
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

protected void saveProgram4100()
	{
		/*SAVE*/
		wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
		wsaaY.add(1);
		wsaaX.add(1);
		/*EXIT*/
	}

protected void restoreProgram4200()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
		wsaaY.add(1);
		wsaaX.add(1);
		/*EXIT*/
	}

protected void callGenssw4300()
	{
			callSubroutine4310();
		}

protected void callSubroutine4310()
	{
		povrIO.setParams(povrgenIO.getParams());
		povrIO.setFunction(varcom.keeps);
		povrIO.setFormat(povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(sv.chdrselOut[varcom.nd.toInt()],"Y")) {
			chdrenqIO.setParams(SPACES);
			chdrenqIO.setChdrcoy(wsaaChdrChdrcoy);
			chdrenqIO.setChdrnum("99999999");
			chdrenqIO.setBillfreq(sv.billfreq);
			chdrenqIO.setTranno(ZERO);
			chdrenqIO.setOccdate(ZERO);
			chdrenqIO.setBtdate(ZERO);
			chdrenqIO.setPtdate(ZERO);
			chdrenqIO.setPolinc(ZERO);
			chdrenqIO.setNxtsfx(ZERO);
			chdrenqIO.setSinstamts(ZERO);
			chdrenqIO.setInststamts(ZERO);
			chdrenqIO.setInstfrom(ZERO);
			chdrenqIO.setPolsum(ZERO);
			chdrenqIO.setBillcd(ZERO);
		}
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)
		&& isNE(gensswrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			return ;
		}
		compute(wsaaX, 0).set(add(1,wsspcomn.programPtr));
		wsaaY.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgram4400();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void loadProgram4400()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void checkPovrgen4500()
	{
		start4510();
	}

protected void start4510()
	{
		povrgenIO.setParams(SPACES);
		povrgenIO.setChdrcoy(wsaaChdrChdrcoy);
		povrgenIO.setChdrnum("99999999");
		povrgenIO.setLife("99");
		povrgenIO.setCoverage("99");
		povrgenIO.setRider("99");
		povrgenIO.setPlanSuffix(ZERO);
		povrgenIO.setFormat(povrgenrec);
		povrgenIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, povrgenIO);
		if (isNE(povrgenIO.getStatuz(),varcom.oK)
		&& isNE(povrgenIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(povrgenIO.getParams());
			syserrrec.statuz.set(povrgenIO.getStatuz());
			fatalError600();
		}
		if (isEQ(povrgenIO.getStatuz(),varcom.mrnf)
		|| isNE(povrgenIO.getChdrcoy(),wsaaChdrChdrcoy)
		|| isNE(povrgenIO.getChdrnum(),"99999999")
		|| isNE(povrgenIO.getLife(),"99")
		|| isNE(povrgenIO.getCoverage(),"99")
		|| isNE(povrgenIO.getRider(),"99")) {
			sv.pbind.set(SPACES);
			sv.pbindOut[varcom.nd.toInt()].set("Y");
			sv.pbindOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.pbind.set("+");
			sv.pbindOut[varcom.nd.toInt()].set("N");
			sv.pbindOut[varcom.pr.toInt()].set("N");
		}
	}

protected void getBenfreq4600()
	{
		start4610();
	}

protected void start4610()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsaaChdrChdrcoy);
		itemIO.setItemtabl(t5671);
		wsbbTranscd.set(wsaaBatckey.batcBatctrcde);
		wsbbCrtable.set(sv.crtable);
		itemIO.setItemitem(wsbbTranCrtable);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		itdmIO.setItemtabl(t5606);
		if (isEQ(t5671rec.pgm[1],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
			wsbbTran.set(t5671rec.edtitm[1]);
		}
		else {
			if (isEQ(t5671rec.pgm[2],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
				wsbbTran.set(t5671rec.edtitm[2]);
			}
			else {
				if (isEQ(t5671rec.pgm[3],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
					wsbbTran.set(t5671rec.edtitm[3]);
				}
				else {
					wsbbTran.set(t5671rec.edtitm[4]);
				}
			}
		}
		wsbbCurrency.set(sv.currcd);
		itdmIO.setItemitem(wsbbTranCurrency);
		itdmIO.setItmfrm(sv.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			if (isNE(itdmIO.getStatuz(),varcom.endp)
			&& isEQ(itdmIO.getItemtabl(),t5606)
			&& isEQ(itdmIO.getItemitem(),wsbbTranCurrency)) {
				t5606rec.t5606Rec.set(itdmIO.getGenarea());
				sv.freqann.set(t5606rec.benfreq);
			}
			else {
				sv.freqann.set("00");
			}
		}
	}

protected void checkCoverage5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start5010();
				}
				case storeScreen5020: {
					storeScreen5020();
				}
				case continue5030: {
					continue5030();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5010()
	{
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(chdrenqIO.getChdrcoy());
		covrIO.setChdrnum(chdrenqIO.getChdrnum());
		covrIO.setLife(sv.life);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setCoverage(sv.coverage);
		covrIO.setRider(sv.rider);
		covrIO.setFormat(covrrec);
		covrIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(covrIO.getStatuz(),varcom.endp)
		|| isNE(covrIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(),chdrenqIO.getChdrnum())
		|| isNE(covrIO.getLife(),sv.life)
		|| isNE(covrIO.getCoverage(),sv.coverage)
		|| isNE(covrIO.getRider(),sv.rider)) {
			sv.coverageErr.set(errorsInner.e348);
			sv.riderErr.set(errorsInner.e348);
			goTo(GotoLabel.continue5030);
		}
		else {
			sv.coverage.set(covrIO.getCoverage());
			sv.rider.set(covrIO.getRider());
			sv.crtable.set(covrIO.getCrtable());
			sv.singp.set(covrIO.getSingp());
			sv.annamnt.set(ZERO);
			sv.sumins.set(covrIO.getSumins());
			sv.premterm.set(covrIO.getPremCessTerm());
			sv.riskCessTerm.set(covrIO.getRiskCessTerm());
			sv.benterm.set(covrIO.getBenCessTerm());
			if (isGT(covrIO.getBenCessTerm(),ZERO)
			&& isLT(covrIO.getBenCessTerm(),120)) {
				getBenfreq4600();
			}
			else {
				sv.freqann.set("00");
			}
		}
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeIO.setLife(covrIO.getLife());
		lifeIO.setJlife("00");
		lifeIO.setCurrfrom(varcom.maxdate);
		lifeIO.setFormat(liferec);
		lifeIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)
		&& isNE(lifeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifeIO.getStatuz(),varcom.endp)
		|| isNE(lifeIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(lifeIO.getChdrnum(),chdrenqIO.getChdrnum())
		|| isNE(lifeIO.getLife(),covrIO.getLife())
		|| isNE(lifeIO.getJlife(),"00")) {
			sv.lifeErr.set(errorsInner.e355);
			goTo(GotoLabel.continue5030);
		}
		else {
			sv.life.set(covrIO.getLife());
			sv.sex.set(lifeIO.getCltsex());
			sv.age.set(lifeIO.getAnbAtCcd());
		}
		if (isEQ(covrIO.getJlife(),"00")
		|| isEQ(covrIO.getJlife(),SPACES)) {
			goTo(GotoLabel.storeScreen5020);
		}
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeIO.setLife(covrIO.getLife());
		lifeIO.setJlife(covrIO.getJlife());
		lifeIO.setCurrfrom(varcom.maxdate);
		lifeIO.setFormat(liferec);
		lifeIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)
		&& isNE(lifeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifeIO.getStatuz(),varcom.endp)
		|| isNE(lifeIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(lifeIO.getChdrnum(),chdrenqIO.getChdrnum())
		|| isNE(lifeIO.getLife(),covrIO.getLife())
		|| isNE(lifeIO.getJlife(),covrIO.getJlife())) {
			sv.jlifeErr.set(errorsInner.e350);
			goTo(GotoLabel.continue5030);
		}
		else {
			sv.jlife.set(covrIO.getJlife());
			sv.jlsex.set(lifeIO.getCltsex());
			sv.ageprem.set(lifeIO.getAnbAtCcd());
		}
	}

protected void storeScreen5020()
	{
		/*    PERFORM 5200-CHECK-T5602.                                    */
		if (isEQ(sv.errorIndicators, SPACES)) {
			storeScrnfields5400();
		}
	}

protected void continue5030()
	{
		sv.pbind.set(SPACES);
		sv.pbindOut[varcom.nd.toInt()].set("Y");
		sv.pbindOut[varcom.pr.toInt()].set("Y");
		/*EXIT*/
	}

protected void getRoutine5100()
	{
		start5110();
	}

protected void start5110()
	{
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemitem(premiumrec.crtable);
		itdmIO.setItmfrm(premiumrec.effectdt);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),premiumrec.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),premiumrec.crtable)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.e300);
			fatalError600();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(premiumrec.chdrChdrcoy);
		if (isGT(premiumrec.jlage,ZERO)
		&& isLTE(premiumrec.jlage,120)) {
			itemIO.setItemitem(t5687rec.jlPremMeth);
		}
		else {
			itemIO.setItemitem(t5687rec.premmeth);
		}
		itemIO.setItemtabl(t5675);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		else {
			if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(errorsInner.e694);
				fatalError600();
			}
			else {
				/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
				if(AppVars.getInstance().getAppConfig().isVpmsEnable())
				{
					premiumrec.premMethod.set(itemIO.getItemitem());
				}
				/* ILIFE-3142 End*/
				t5675rec.t5675Rec.set(itemIO.getGenarea());
			}
		}
	}

	/**
	* <pre>
	*5200-CHECK-T5602 SECTION.                                        
	*5210-START.                                                      
	* Check the coverage is within SUM.......
	*    MOVE SPACES                 TO ITEM-PARAMS.                  
	*    MOVE 'IT'                   TO ITEM-ITEMPFX.                 
	*    MOVE WSAA-CHDR-CHDRCOY      TO ITEM-ITEMCOY.                 
	*    MOVE T5602                  TO ITEM-ITEMTABL.                
	*    MOVE S5614-CRTABLE          TO ITEM-ITEMITEM.                
	*    MOVE ITEMREC                TO ITEM-FORMAT.                  
	*    MOVE READR                  TO ITEM-FUNCTION.                
	*    CALL 'ITEMIO' USING ITEM-PARAMS.                             
	*    IF ITEM-STATUZ NOT = O-K                                     
	*       MOVE D024                TO S5614-CRTABLE-ERR             
	*    ELSE                                                         
	*       MOVE ITEM-GENAREA        TO T5602-T5602-REC               
	*       IF T5602-INDIC-01 NOT = SPACE                             
	*          MOVE D026             TO S5614-CRTABLE-ERR.            
	*5290-EXIT.                                                       
	*    EXIT.                                                        
	* </pre>
	*/
protected void rlsePovrgen5300()
	{
		start5310();
	}

protected void start5310()
	{
		povrgenIO.setParams(SPACES);
		povrgenIO.setChdrcoy(wsaaChdrChdrcoy);
		povrgenIO.setChdrnum("99999999");
		povrgenIO.setLife("99");
		povrgenIO.setCoverage("99");
		povrgenIO.setRider("99");
		povrgenIO.setPlanSuffix(ZERO);
		povrgenIO.setFormat(povrgenrec);
		povrgenIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, povrgenIO);
		if (isNE(povrgenIO.getStatuz(),varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
	}

protected void storeScrnfields5400()
	{
		start5410();
	}

protected void start5410()
	{
		wsaaScrnfldStoresInner.wsaaLifeStore.set(sv.life);
		wsaaScrnfldStoresInner.wsaaAgeStore.set(sv.age);
		wsaaScrnfldStoresInner.wsaaSexStore.set(sv.sex);
		wsaaScrnfldStoresInner.wsaaJlifeStore.set(sv.jlife);
		wsaaScrnfldStoresInner.wsaaAgepremStore.set(sv.ageprem);
		wsaaScrnfldStoresInner.wsaaJlsexStore.set(sv.jlsex);
		wsaaScrnfldStoresInner.wsaaCoverageStore.set(sv.coverage);
		wsaaScrnfldStoresInner.wsaaRiderStore.set(sv.rider);
		wsaaScrnfldStoresInner.wsaaCrtableStore.set(sv.crtable);
		wsaaScrnfldStoresInner.wsaaPremtermStore.set(sv.premterm);
		wsaaScrnfldStoresInner.wsaaRisktermStore.set(sv.riskCessTerm);
		wsaaScrnfldStoresInner.wsaaBentermStore.set(sv.benterm);
		wsaaScrnfldStoresInner.wsaaFreqannStore.set(sv.freqann);
		wsaaSuminsStore.set(sv.sumins);
		wsaaAnnamntStore.set(sv.annamnt);
	}
/*
 * Class transformed  from Data Structure WSAA-SCRNFLD-STORES--INNER
 */
private static final class WsaaScrnfldStoresInner { 
		/* WSAA-SCRNFLD-STORES */
	private FixedLengthStringData wsaaLifeStore = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaAgeStore = new ZonedDecimalData(3, 0);
	private FixedLengthStringData wsaaSexStore = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaJlifeStore = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaAgepremStore = new ZonedDecimalData(3, 0);
	private FixedLengthStringData wsaaJlsexStore = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCoverageStore = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRiderStore = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCrtableStore = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaPremtermStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaRisktermStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaBentermStore = new ZonedDecimalData(3, 0);
	private FixedLengthStringData wsaaFreqannStore = new FixedLengthStringData(2);
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData f200 = new FixedLengthStringData(4).init("F200");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData t075 = new FixedLengthStringData(4).init("T075");
	private FixedLengthStringData t076 = new FixedLengthStringData(4).init("T076");
	private FixedLengthStringData d020 = new FixedLengthStringData(4).init("D020");
	private FixedLengthStringData d027 = new FixedLengthStringData(4).init("D027");
	private FixedLengthStringData d034 = new FixedLengthStringData(4).init("D034");
	private FixedLengthStringData d035 = new FixedLengthStringData(4).init("D035");
	private FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
	private FixedLengthStringData e300 = new FixedLengthStringData(4).init("E300");
	private FixedLengthStringData e348 = new FixedLengthStringData(4).init("E348");
	private FixedLengthStringData e350 = new FixedLengthStringData(4).init("E350");
	private FixedLengthStringData e355 = new FixedLengthStringData(4).init("E355");
	private FixedLengthStringData e694 = new FixedLengthStringData(4).init("E694");
}
}
