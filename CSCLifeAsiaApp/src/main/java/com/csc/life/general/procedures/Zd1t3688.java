package com.csc.life.general.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import com.csc.fsu.general.tablestructures.T3688rec;
import com.csc.fsu.general.recordstructures.Zsdmcde;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescpfDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.ItempfDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Sub-routine to read T3688 for SUN Dimension 1 Product Type code and return the code value 
 * to SUN Dimension main routine. This is applicable to Bank and Company suspense GL entry.
 * 
 * @author vhukumagrawa
 *
 */
public class Zd1t3688 extends COBOLConvCodeModel{

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaSubr = new FixedLengthStringData(8).init("ZD1T3688");
	
	private FixedLengthStringData wsaaT3688Array = new FixedLengthStringData(100400);
	private FixedLengthStringData[] wsaaT3688Rec = FLSArrayPartOfStructure(200, 502, wsaaT3688Array, 0);
	private FixedLengthStringData[] wsaaT3688Key = FLSDArrayPartOfArrayStructure(2, wsaaT3688Rec, 0);
	private FixedLengthStringData[] wsaaT3688BankCode = FLSDArrayPartOfArrayStructure(2, wsaaT3688Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT3688Data = FLSDArrayPartOfArrayStructure(500, wsaaT3688Rec, 2);
	private FixedLengthStringData[] wsaaT3688Genarea = FLSDArrayPartOfArrayStructure(500, wsaaT3688Data, 0);
	private int wsaaT3688Size = 200;
	private ZonedDecimalData wsaaT3688Ix = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	
	private Zsdmcde zsdmcde = new Zsdmcde();
	private T3688rec t3688rec = new T3688rec();
	
	private ItemTableDAM itemIO = new ItemTableDAM();
	
	private String t3688 = "T3688";
	List<Descpf> t3688List = null;
	
	
private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);	
private DescpfDAO descDAO =  DAOFactory.getDescpfDAO();
	private Map<String, List<Itempf>> t3688ListMap;
	private String strCompany;
	private String strEffDate;
	private static final String rp50 = "RP50";
	
	private Map<String,String> t3688Map = new LinkedHashMap<String,String>();
	private String tableName;
	private Itempf itempf;
	private List<Itempf> itemAl = new ArrayList<Itempf>();
	private ItempfDAO itempfDAO =  DAOFactory.getItempfDAO();
	boolean itemFound = false;
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit620
	}
	
	public void mainline(Object... parmArray)
	{
		zsdmcde.zsdmcdeRec = convertAndSetParam(zsdmcde.zsdmcdeRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}
	
	protected void startSubr010()
	{
		syserrrec.subrname.set(wsaaSubr);
		zsdmcde.statuz.set(varcom.oK);
		if(isEQ(wsaaFirstTime, SPACES)){
			loadTable1000();
			wsaaFirstTime.set("Y");
		}
		
		if(isEQ(zsdmcde.bankcode, SPACES)){
			/*EXIT*/
			exitProgram();
		}
		readT3688();
	//	for(wsaaT3688Ix.set(1); !isGT(wsaaT3688Ix, wsaaT3688Size); wsaaT3688Ix.add(1)){			
				
				//if(itemFound)
				//{
		
		String zdmsion = t3688Map.get(zsdmcde.bankcode.toString().trim());
		if(zdmsion != null && !"".equals(zdmsion.trim()))
				zsdmcde.rtzdmsion.set(zdmsion);
					
					//break;
				//}
		
			
			
		//}
		/*EXIT*/
		//exitProgram();
	}
	
	private void loadTable1000(){
		initialize(wsaaT3688Array);
		
		strCompany = zsdmcde.batccoy.toString();
		strEffDate = zsdmcde.beffdate.toString();	
		/*itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(zsdmcde.batccoy);
		itemIO.setItemtabl(t3688);
		itemIO.setItemitem(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setFunction(varcom.begn);
		//itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		wsaaT3688Ix.set(1);
		while(isNE(itemIO.getStatuz(), varcom.endp)){
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)
				&& isNE(itemIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(itemIO.getParams());
					fatalError600();
			}
			
			if(isEQ(itemIO.getStatuz(), varcom.endp)
				|| isNE(itemIO.getItempfx(), "IT")
				|| isNE(itemIO.getItemcoy(), zsdmcde.batccoy)
				|| isNE(itemIO.getItemtabl(), t3688)){
					itemIO.setStatuz(varcom.endp);
			}
			if(isEQ(itemIO.getStatuz(), varcom.oK)){
				
				wsaaT3688BankCode[wsaaT3688Ix.toInt()].set(itemIO.getItemitem());
				wsaaT3688Genarea[wsaaT3688Ix.toInt()].set(itemIO.getGenarea());
				Go to next item 
				wsaaT3688Ix.add(1);
				itemIO.setFunction(varcom.nextr);
			}
		}*/
		
		t3688ListMap = itemDAO.loadSmartTable("IT", strCompany, "T3688");
		//readT3688();

	}
	
	protected void readT3688(){
	
		tableName = "T3688";
		
		
		itempf = new Itempf();
		itemAl.clear();
		itempf.setItempfx("IT");
		itempf.setItemcoy(zsdmcde.batccoy.toString().trim());
		itempf.setItemtabl(tableName);
		itempf.setValidflag("1");
		//itempf.setItemitem(bankcode);
		itemAl = itempfDAO.findByExample(itempf);
		if(itemAl.isEmpty())
		{
			syserrrec.params.set(itempf.getItemitem());
			fatalError600();
		}	
		Iterator<Itempf> it = itemAl.iterator();
		while(it.hasNext())
		{
			itempf = it.next();
			t3688rec.t3688Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			t3688Map.put(itempf.getItemitem().trim(), t3688rec.zdmsion.toString().trim());
		}
	}
	
	protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error610();
				}
				case exit620: {
					exit620();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void error610()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit620);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

	protected void exit620()
	{
		zsdmcde.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
