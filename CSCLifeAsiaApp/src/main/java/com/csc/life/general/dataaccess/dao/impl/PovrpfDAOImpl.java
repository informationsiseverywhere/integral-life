package com.csc.life.general.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.general.dataaccess.dao.PovrpfDAO;
import com.csc.life.general.dataaccess.model.Povrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class PovrpfDAOImpl extends BaseDAOImpl<Povrpf> implements PovrpfDAO {
	
 private static final Logger LOGGER = LoggerFactory.getLogger(PovrpfDAOImpl.class);

 public Povrpf getPovrRecord(Povrpf povrpf){
	  StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER FROM POVR WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? ");
	  sb.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
	  PreparedStatement ps = getPrepareStatement(sb.toString());
      ResultSet rs = null;	
      
      Povrpf result = null;
      
      try {
    	  ps.setString(1, povrpf.getChdrcoy());
    	  ps.setString(2, povrpf.getChdrnum());
    	  ps.setString(3, povrpf.getLife());
    	  ps.setString(4, povrpf.getCoverage());
    	  ps.setString(5, povrpf.getRider());
    	  
    	  rs = ps.executeQuery();
    	  
    	  if(rs.next()) {
    		  result = new Povrpf();
    		  result.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
    	  }
      } catch (SQLException e) {
          LOGGER.error("getPovrRecord()", e);//IJTI-1561
          throw new SQLRuntimeException(e);
      } finally {
          close(ps, rs);
      }
      
     return result;
 }
}
