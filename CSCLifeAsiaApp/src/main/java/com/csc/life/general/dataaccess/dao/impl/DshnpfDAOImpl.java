package com.csc.life.general.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.general.dataaccess.dao.DshnpfDAO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.csc.smart400framework.dataaccess.model.Dshnpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class DshnpfDAOImpl extends BaseDAOImpl<Dshnpf> implements DshnpfDAO{

	private static final Logger LOGGER = LoggerFactory.getLogger(DshnpfDAOImpl.class);

	public void removeRecord(String chdrcoy, String chdrnum) {
			String sql = "DELETE FROM DSHNPF WHERE CHDRCOY=? AND CHDRNUM=? ";
			PreparedStatement ps = getPrepareStatement(sql);
			try {
					ps.setString(1, chdrcoy);
					ps.setString(2, chdrnum);				
				ps.execute();
			} catch (SQLException e) {
				LOGGER.error("deleteDSHNPFRecord()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);
			}
		
	}

	//Ticket #PINNACLE-2044 : AIA-265-Billing transaction didnt happen when L2POLRNWL run after 31days of 1st dishonour even after billing reversal for annual policy
	@Override
	public Integer getDishonouredRecordCountInPeriodOfDate(int date,String chdrnum) {
		String sql = " SELECT DSHNCOUNT FROM ( SELECT COUNT(*) AS DSHNCOUNT, INSTFROM, INSTTO FROM DSHNPF WHERE INSTFROM<=? AND INSTTO>=? AND CHDRNUM = ? "
				+ " GROUP BY INSTFROM, INSTTO ) MAIN ORDER BY MAIN.INSTTO DESC"; //PINNACLE-3156
		PreparedStatement ps = getPrepareStatement(sql);
		try {
				ps.setInt(1, date);
				ps.setInt(2, date);	
				ps.setString(3, chdrnum);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				return rs.getInt("DSHNCOUNT");
			}
		} catch (SQLException e) {
			LOGGER.error("getDishonouredRecordCountInPeriodOfDate()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
		return null;
	}
	//Ticket #PINNACLE-2044 : AIA-265-Billing transaction didnt happen when L2POLRNWL run after 31days of 1st dishonour even after billing reversal for annual policy ends

}
