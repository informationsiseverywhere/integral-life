package com.csc.life.general.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5615screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5615ScreenVars sv = (S5615ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5615screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5615ScreenVars screenVars = (S5615ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.trancde.setClassString("");
		screenVars.currcode.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.language.setClassString("");
		screenVars.singp.setClassString("");
		screenVars.chdrsel.setClassString("");
		screenVars.chdrtype.setClassString("");
		screenVars.pstatcode.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.annamnt.setClassString("");
		screenVars.sumins.setClassString("");
		screenVars.premterm.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.riskCessTerm.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.crrcdDisp.setClassString("");
		screenVars.jlsex.setClassString("");
		screenVars.ageprem.setClassString("");
		screenVars.life.setClassString("");
		screenVars.sex.setClassString("");
		screenVars.benterm.setClassString("");
		screenVars.age.setClassString("");
		screenVars.freqann.setClassString("");
		screenVars.statuz.setClassString("");
		screenVars.resamt.setClassString("");
	}

/**
 * Clear all the variables in S5615screen
 */
	public static void clear(VarModel pv) {
		S5615ScreenVars screenVars = (S5615ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.trancde.clear();
		screenVars.currcode.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.language.clear();
		screenVars.singp.clear();
		screenVars.chdrsel.clear();
		screenVars.chdrtype.clear();
		screenVars.pstatcode.clear();
		screenVars.currcd.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.billfreq.clear();
		screenVars.annamnt.clear();
		screenVars.sumins.clear();
		screenVars.premterm.clear();
		screenVars.coverage.clear();
		screenVars.riskCessTerm.clear();
		screenVars.rider.clear();
		screenVars.crtable.clear();
		screenVars.jlife.clear();
		screenVars.crrcdDisp.clear();
		screenVars.crrcd.clear();
		screenVars.jlsex.clear();
		screenVars.ageprem.clear();
		screenVars.life.clear();
		screenVars.sex.clear();
		screenVars.benterm.clear();
		screenVars.age.clear();
		screenVars.freqann.clear();
		screenVars.statuz.clear();
		screenVars.resamt.clear();
	}
}
