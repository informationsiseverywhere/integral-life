package com.csc.life.general.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:06
 * Description:
 * Copybook name: T5600REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5600rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5600Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData indics = new FixedLengthStringData(15).isAPartOf(t5600Rec, 0);
  	public FixedLengthStringData[] indic = FLSArrayPartOfStructure(15, 1, indics, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(15).isAPartOf(indics, 0, FILLER_REDEFINE);
  	public FixedLengthStringData indic01 = new FixedLengthStringData(1).isAPartOf(filler, 0);
  	public FixedLengthStringData indic02 = new FixedLengthStringData(1).isAPartOf(filler, 1);
  	public FixedLengthStringData indic03 = new FixedLengthStringData(1).isAPartOf(filler, 2);
  	public FixedLengthStringData indic04 = new FixedLengthStringData(1).isAPartOf(filler, 3);
  	public FixedLengthStringData indic05 = new FixedLengthStringData(1).isAPartOf(filler, 4);
  	public FixedLengthStringData indic06 = new FixedLengthStringData(1).isAPartOf(filler, 5);
  	public FixedLengthStringData indic07 = new FixedLengthStringData(1).isAPartOf(filler, 6);
  	public FixedLengthStringData indic08 = new FixedLengthStringData(1).isAPartOf(filler, 7);
  	public FixedLengthStringData indic09 = new FixedLengthStringData(1).isAPartOf(filler, 8);
  	public FixedLengthStringData indic10 = new FixedLengthStringData(1).isAPartOf(filler, 9);
  	public FixedLengthStringData indic11 = new FixedLengthStringData(1).isAPartOf(filler, 10);
  	public FixedLengthStringData indic12 = new FixedLengthStringData(1).isAPartOf(filler, 11);
  	public FixedLengthStringData indic13 = new FixedLengthStringData(1).isAPartOf(filler, 12);
  	public FixedLengthStringData indic14 = new FixedLengthStringData(1).isAPartOf(filler, 13);
  	public FixedLengthStringData indic15 = new FixedLengthStringData(1).isAPartOf(filler, 14);
  	public FixedLengthStringData inds = new FixedLengthStringData(15).isAPartOf(t5600Rec, 15);
  	public FixedLengthStringData[] ind = FLSArrayPartOfStructure(15, 1, inds, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(15).isAPartOf(inds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData ind01 = new FixedLengthStringData(1).isAPartOf(filler1, 0);
  	public FixedLengthStringData ind02 = new FixedLengthStringData(1).isAPartOf(filler1, 1);
  	public FixedLengthStringData ind03 = new FixedLengthStringData(1).isAPartOf(filler1, 2);
  	public FixedLengthStringData ind04 = new FixedLengthStringData(1).isAPartOf(filler1, 3);
  	public FixedLengthStringData ind05 = new FixedLengthStringData(1).isAPartOf(filler1, 4);
  	public FixedLengthStringData ind06 = new FixedLengthStringData(1).isAPartOf(filler1, 5);
  	public FixedLengthStringData ind07 = new FixedLengthStringData(1).isAPartOf(filler1, 6);
  	public FixedLengthStringData ind08 = new FixedLengthStringData(1).isAPartOf(filler1, 7);
  	public FixedLengthStringData ind09 = new FixedLengthStringData(1).isAPartOf(filler1, 8);
  	public FixedLengthStringData ind10 = new FixedLengthStringData(1).isAPartOf(filler1, 9);
  	public FixedLengthStringData ind11 = new FixedLengthStringData(1).isAPartOf(filler1, 10);
  	public FixedLengthStringData ind12 = new FixedLengthStringData(1).isAPartOf(filler1, 11);
  	public FixedLengthStringData ind13 = new FixedLengthStringData(1).isAPartOf(filler1, 12);
  	public FixedLengthStringData ind14 = new FixedLengthStringData(1).isAPartOf(filler1, 13);
  	public FixedLengthStringData ind15 = new FixedLengthStringData(1).isAPartOf(filler1, 14);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(470).isAPartOf(t5600Rec, 30, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5600Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5600Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}