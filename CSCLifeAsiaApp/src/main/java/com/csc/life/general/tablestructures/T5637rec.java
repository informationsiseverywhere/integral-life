package com.csc.life.general.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:27
 * Description:
 * Copybook name: T5637REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5637rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5637Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData datedue = new ZonedDecimalData(8, 0).isAPartOf(t5637Rec, 0);
  	public ZonedDecimalData depint = new ZonedDecimalData(6, 3).isAPartOf(t5637Rec, 8);
  	public FixedLengthStringData mop = new FixedLengthStringData(1).isAPartOf(t5637Rec, 14);
  	public FixedLengthStringData filler = new FixedLengthStringData(485).isAPartOf(t5637Rec, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5637Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5637Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}