package com.csc.life.general.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5161screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5161ScreenVars sv = (S5161ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5161screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5161ScreenVars screenVars = (S5161ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.instamnt04.setClassString("");
		screenVars.instamnt07.setClassString("");
		screenVars.instamnt10.setClassString("");
		screenVars.instamnt13.setClassString("");
		screenVars.instamnt16.setClassString("");
		screenVars.instamnt18.setClassString("");
		screenVars.instamnt19.setClassString("");
		screenVars.instamnt20.setClassString("");
		screenVars.instamnt21.setClassString("");
		screenVars.instamnt22.setClassString("");
		screenVars.instamnt23.setClassString("");
		screenVars.instamnt24.setClassString("");
		screenVars.annamnt04.setClassString("");
		screenVars.annamnt07.setClassString("");
		screenVars.annamnt10.setClassString("");
		screenVars.annamnt13.setClassString("");
		screenVars.annamnt16.setClassString("");
		screenVars.annamnt18.setClassString("");
		screenVars.annamnt19.setClassString("");
		screenVars.annamnt20.setClassString("");
		screenVars.annamnt21.setClassString("");
		screenVars.annamnt22.setClassString("");
		screenVars.annamnt23.setClassString("");
		screenVars.annamnt24.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.instamnt03.setClassString("");
		screenVars.instamnt05.setClassString("");
		screenVars.annamnt03.setClassString("");
		screenVars.annamnt05.setClassString("");
		screenVars.grossprem.setClassString("");
		screenVars.agrossprem.setClassString("");
		screenVars.instamnt06.setClassString("");
		screenVars.annamnt06.setClassString("");
		screenVars.instamnt08.setClassString("");
		screenVars.annamnt08.setClassString("");
		screenVars.instamnt09.setClassString("");
		screenVars.annamnt09.setClassString("");
		screenVars.instamnt11.setClassString("");
		screenVars.annamnt11.setClassString("");
		screenVars.instamnt12.setClassString("");
		screenVars.annamnt12.setClassString("");
		screenVars.instamnt14.setClassString("");
		screenVars.annamnt14.setClassString("");
		screenVars.instamnt15.setClassString("");
		screenVars.annamnt15.setClassString("");
		screenVars.instamnt17.setClassString("");
		screenVars.instamnt25.setClassString("");
		screenVars.annamnt17.setClassString("");
		screenVars.annamnt25.setClassString("");
		screenVars.instamnt01.setClassString("");
		screenVars.annamnt01.setClassString("");
		screenVars.instamnt02.setClassString("");
		screenVars.annamnt02.setClassString("");
	}

/**
 * Clear all the variables in S5161screen
 */
	public static void clear(VarModel pv) {
		S5161ScreenVars screenVars = (S5161ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.instamnt04.clear();
		screenVars.instamnt07.clear();
		screenVars.instamnt10.clear();
		screenVars.instamnt13.clear();
		screenVars.instamnt16.clear();
		screenVars.instamnt18.clear();
		screenVars.instamnt19.clear();
		screenVars.instamnt20.clear();
		screenVars.instamnt21.clear();
		screenVars.instamnt22.clear();
		screenVars.instamnt23.clear();
		screenVars.instamnt24.clear();
		screenVars.annamnt04.clear();
		screenVars.annamnt07.clear();
		screenVars.annamnt10.clear();
		screenVars.annamnt13.clear();
		screenVars.annamnt16.clear();
		screenVars.annamnt18.clear();
		screenVars.annamnt19.clear();
		screenVars.annamnt20.clear();
		screenVars.annamnt21.clear();
		screenVars.annamnt22.clear();
		screenVars.annamnt23.clear();
		screenVars.annamnt24.clear();
		screenVars.chdrnum.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.instamnt03.clear();
		screenVars.instamnt05.clear();
		screenVars.annamnt03.clear();
		screenVars.annamnt05.clear();
		screenVars.grossprem.clear();
		screenVars.agrossprem.clear();
		screenVars.instamnt06.clear();
		screenVars.annamnt06.clear();
		screenVars.instamnt08.clear();
		screenVars.annamnt08.clear();
		screenVars.instamnt09.clear();
		screenVars.annamnt09.clear();
		screenVars.instamnt11.clear();
		screenVars.annamnt11.clear();
		screenVars.instamnt12.clear();
		screenVars.annamnt12.clear();
		screenVars.instamnt14.clear();
		screenVars.annamnt14.clear();
		screenVars.instamnt15.clear();
		screenVars.annamnt15.clear();
		screenVars.instamnt17.clear();
		screenVars.instamnt25.clear();
		screenVars.annamnt17.clear();
		screenVars.annamnt25.clear();
		screenVars.instamnt01.clear();
		screenVars.annamnt01.clear();
		screenVars.instamnt02.clear();
		screenVars.annamnt02.clear();
	}
}
