/*
 * File: T5611pt.java
 * Date: 30 August 2009 2:24:10
 * Author: Quipoz Limited
 * 
 * Class transformed from T5611PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.general.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.general.tablestructures.T5611rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5611.
*
*
*****************************************************************
* </pre>
*/
public class T5611pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(51).isAPartOf(wsaaPrtLine001, 25, FILLER).init("SUM Surrender/Paid-Up/Bonus Parameters        S5611");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(67);
	private FixedLengthStringData filler9 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(60).isAPartOf(wsaaPrtLine004, 7, FILLER).init("Surrender Methods                   Surrender Charge Details");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(70);
	private FixedLengthStringData filler11 = new FixedLengthStringData(41).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine005, 41, FILLER).init("Max Client Age To Charge:");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 67).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(69);
	private FixedLengthStringData filler13 = new FixedLengthStringData(41).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine006, 41, FILLER).init("Min Risk Term To Charge :");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 67).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(74);
	private FixedLengthStringData filler15 = new FixedLengthStringData(42).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler16 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine007, 42, FILLER).init("% Base Amount      % Sum At Risk");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(72);
	private FixedLengthStringData filler17 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine008, 0, FILLER).init("    X            :");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 19);
	private FixedLengthStringData filler18 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine008, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(7, 3).isAPartOf(wsaaPrtLine008, 45).setPattern("ZZZZ.ZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine008, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(7, 3).isAPartOf(wsaaPrtLine008, 64).setPattern("ZZZZ.ZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(72);
	private FixedLengthStringData filler20 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine009, 0, FILLER).init("    X + 1        :");
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 19);
	private FixedLengthStringData filler21 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine009, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(7, 3).isAPartOf(wsaaPrtLine009, 45).setPattern("ZZZZ.ZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine009, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(7, 3).isAPartOf(wsaaPrtLine009, 64).setPattern("ZZZZ.ZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(72);
	private FixedLengthStringData filler23 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine010, 0, FILLER).init("    % Sum Assured:");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 19);
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(7, 3).isAPartOf(wsaaPrtLine010, 22).setPattern("ZZZZ.ZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine010, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(7, 3).isAPartOf(wsaaPrtLine010, 45).setPattern("ZZZZ.ZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine010, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(7, 3).isAPartOf(wsaaPrtLine010, 64).setPattern("ZZZZ.ZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(65);
	private FixedLengthStringData filler27 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler28 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine011, 21, FILLER).init("Use Lower Of Two Values:");
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 46);
	private FixedLengthStringData filler29 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine011, 47, FILLER).init("  (Default Higher)");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(47);
	private FixedLengthStringData filler30 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler31 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine012, 21, FILLER).init("Total The Two Values   :");
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 46);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(53);
	private FixedLengthStringData filler32 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler33 = new FixedLengthStringData(40).isAPartOf(wsaaPrtLine013, 6, FILLER).init("Premium Adjustment Calculation Routine:");
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine013, 46);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5611rec t5611rec = new T5611rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5611pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5611rec.t5611Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5611rec.agemax);
		fieldNo008.set(t5611rec.termMin);
		fieldNo009.set(t5611rec.indic01);
		fieldNo010.set(t5611rec.perbsamt01);
		fieldNo011.set(t5611rec.persrisk01);
		fieldNo012.set(t5611rec.indic02);
		fieldNo013.set(t5611rec.perbsamt02);
		fieldNo014.set(t5611rec.persrisk02);
		fieldNo015.set(t5611rec.indic03);
		fieldNo016.set(t5611rec.persassd);
		fieldNo017.set(t5611rec.perbsamt03);
		fieldNo018.set(t5611rec.persrisk03);
		fieldNo019.set(t5611rec.indic04);
		fieldNo020.set(t5611rec.indic05);
		fieldNo021.set(t5611rec.calcprog);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
