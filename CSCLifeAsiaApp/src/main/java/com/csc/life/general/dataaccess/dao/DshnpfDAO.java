package com.csc.life.general.dataaccess.dao;


import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.smart400framework.dataaccess.model.Dshnpf;

public interface DshnpfDAO extends BaseDAO<Dshnpf> {
public  void removeRecord(String chdrcoy, String chdrnum) ;
//Ticket #PINNACLE-2044 : AIA-265-Billing transaction didnt happen when L2POLRNWL run after 31days of 1st dishonour even after billing reversal for annual policy
public Integer getDishonouredRecordCountInPeriodOfDate(int date,String chdrnum);
}
