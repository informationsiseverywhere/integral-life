/*
 * File: T5601pt.java
 * Date: 30 August 2009 2:23:32
 * Author: Quipoz Limited
 * 
 * Class transformed from T5601PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.general.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.general.tablestructures.T5601rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5601.
*
*
*****************************************************************
* </pre>
*/
public class T5601pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(47).isAPartOf(wsaaPrtLine001, 29, FILLER).init("KOMPONENTE Premium Details                S5601");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(51);
	private FixedLengthStringData filler7 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine003, 31, FILLER).init("Slots To Be Occupied");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(70);
	private FixedLengthStringData filler9 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Frequency Loading   Slot  1 :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 31);
	private FixedLengthStringData filler10 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine004, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine004, 39, FILLER).init("Subst. Loading 1    Slot 14 :");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 69);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(70);
	private FixedLengthStringData filler12 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine005, 0, FILLER).init(" Net Premium 1       Slot  2 :");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 31);
	private FixedLengthStringData filler13 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine005, 39, FILLER).init("Subst. Loading 2    Slot 15 :");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 69);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(70);
	private FixedLengthStringData filler15 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine006, 0, FILLER).init(" Net Premium 2       Slot  3 :");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 31);
	private FixedLengthStringData filler16 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler17 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine006, 39, FILLER).init("Subst. Loading 3    Slot 16 :");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 69);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(70);
	private FixedLengthStringData filler18 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine007, 0, FILLER).init(" Net Premium 3       Slot  4 :");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 31);
	private FixedLengthStringData filler19 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler20 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine007, 39, FILLER).init("High Sum Rebate     Slot 17 :");
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 69);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(70);
	private FixedLengthStringData filler21 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine008, 0, FILLER).init(" Alpha Loading 1     Slot  5 :");
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 31);
	private FixedLengthStringData filler22 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine008, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler23 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine008, 39, FILLER).init("Staff Discount      Slot 18 :");
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 69);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(70);
	private FixedLengthStringData filler24 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine009, 0, FILLER).init(" Alpha Loading 2     Slot  6 :");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 31);
	private FixedLengthStringData filler25 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine009, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler26 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine009, 39, FILLER).init("Spare               Slot 19 :");
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 69);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(70);
	private FixedLengthStringData filler27 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine010, 0, FILLER).init(" Alpha Loading 3     Slot  7 :");
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 31);
	private FixedLengthStringData filler28 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine010, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler29 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine010, 39, FILLER).init("Spare               Slot 20 :");
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 69);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(70);
	private FixedLengthStringData filler30 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine011, 0, FILLER).init(" Beta Loading  1     Slot  8 :");
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 31);
	private FixedLengthStringData filler31 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine011, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler32 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine011, 39, FILLER).init("Spare               Slot 21 :");
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 69);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(70);
	private FixedLengthStringData filler33 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine012, 0, FILLER).init(" Beta Loading  2     Slot  9 :");
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 31);
	private FixedLengthStringData filler34 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine012, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler35 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine012, 39, FILLER).init("Spare               Slot 22 :");
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 69);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(70);
	private FixedLengthStringData filler36 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine013, 0, FILLER).init(" Beta Loading  3     Slot 10 :");
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 31);
	private FixedLengthStringData filler37 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine013, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler38 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine013, 39, FILLER).init("Spare               Slot 23 :");
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 69);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(70);
	private FixedLengthStringData filler39 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine014, 0, FILLER).init(" Gamma Loading 1     Slot 11 :");
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 31);
	private FixedLengthStringData filler40 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine014, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler41 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine014, 39, FILLER).init("Spare               Slot 24 :");
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 69);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(70);
	private FixedLengthStringData filler42 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine015, 0, FILLER).init(" Gamma Loading 2     Slot 12 :");
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 31);
	private FixedLengthStringData filler43 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine015, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler44 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine015, 39, FILLER).init("Reserve Injection   Slot 25 :");
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 69);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(32);
	private FixedLengthStringData filler45 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine016, 0, FILLER).init(" Gamma Loading 3     Slot 13 :");
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 31);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5601rec t5601rec = new T5601rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5601pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5601rec.t5601Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(t5601rec.indic01);
		fieldNo007.set(t5601rec.indic02);
		fieldNo009.set(t5601rec.indic03);
		fieldNo011.set(t5601rec.indic04);
		fieldNo013.set(t5601rec.indic05);
		fieldNo015.set(t5601rec.indic06);
		fieldNo017.set(t5601rec.indic07);
		fieldNo019.set(t5601rec.indic08);
		fieldNo021.set(t5601rec.indic09);
		fieldNo023.set(t5601rec.indic10);
		fieldNo025.set(t5601rec.indic11);
		fieldNo027.set(t5601rec.indic12);
		fieldNo029.set(t5601rec.indic13);
		fieldNo006.set(t5601rec.indic14);
		fieldNo008.set(t5601rec.indic15);
		fieldNo010.set(t5601rec.indic16);
		fieldNo012.set(t5601rec.indic17);
		fieldNo014.set(t5601rec.indic18);
		fieldNo016.set(t5601rec.indic19);
		fieldNo018.set(t5601rec.indic20);
		fieldNo020.set(t5601rec.indic21);
		fieldNo022.set(t5601rec.indic22);
		fieldNo024.set(t5601rec.indic23);
		fieldNo026.set(t5601rec.indic24);
		fieldNo028.set(t5601rec.indic25);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
