package com.csc.life.general.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:08
 * Description:
 * Copybook name: T5602REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5602rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5602Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData excluscd = new FixedLengthStringData(4).isAPartOf(t5602Rec, 0);
  	public FixedLengthStringData indics = new FixedLengthStringData(2).isAPartOf(t5602Rec, 4);
  	public FixedLengthStringData[] indic = FLSArrayPartOfStructure(2, 1, indics, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(indics, 0, FILLER_REDEFINE);
  	public FixedLengthStringData indic01 = new FixedLengthStringData(1).isAPartOf(filler, 0);
  	public FixedLengthStringData indic02 = new FixedLengthStringData(1).isAPartOf(filler, 1);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(494).isAPartOf(t5602Rec, 6, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5602Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5602Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}