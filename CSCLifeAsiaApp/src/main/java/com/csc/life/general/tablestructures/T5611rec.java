package com.csc.life.general.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:22
 * Description:
 * Copybook name: T5611REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5611rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5611Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData agemax = new ZonedDecimalData(3, 0).isAPartOf(t5611Rec, 0);
  	public FixedLengthStringData calcprog = new FixedLengthStringData(7).isAPartOf(t5611Rec, 3);
  	public FixedLengthStringData indics = new FixedLengthStringData(5).isAPartOf(t5611Rec, 10);
  	public FixedLengthStringData[] indic = FLSArrayPartOfStructure(5, 1, indics, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(5).isAPartOf(indics, 0, FILLER_REDEFINE);
  	public FixedLengthStringData indic01 = new FixedLengthStringData(1).isAPartOf(filler, 0);
  	public FixedLengthStringData indic02 = new FixedLengthStringData(1).isAPartOf(filler, 1);
  	public FixedLengthStringData indic03 = new FixedLengthStringData(1).isAPartOf(filler, 2);
  	public FixedLengthStringData indic04 = new FixedLengthStringData(1).isAPartOf(filler, 3);
  	public FixedLengthStringData indic05 = new FixedLengthStringData(1).isAPartOf(filler, 4);
  	public FixedLengthStringData perbsamts = new FixedLengthStringData(21).isAPartOf(t5611Rec, 15);
  	public ZonedDecimalData[] perbsamt = ZDArrayPartOfStructure(3, 7, 3, perbsamts, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(21).isAPartOf(perbsamts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData perbsamt01 = new ZonedDecimalData(7, 3).isAPartOf(filler1, 0);
  	public ZonedDecimalData perbsamt02 = new ZonedDecimalData(7, 3).isAPartOf(filler1, 7);
  	public ZonedDecimalData perbsamt03 = new ZonedDecimalData(7, 3).isAPartOf(filler1, 14);
  	public ZonedDecimalData persassd = new ZonedDecimalData(7, 3).isAPartOf(t5611Rec, 36);
  	public FixedLengthStringData persrisks = new FixedLengthStringData(21).isAPartOf(t5611Rec, 43);
  	public ZonedDecimalData[] persrisk = ZDArrayPartOfStructure(3, 7, 3, persrisks, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(21).isAPartOf(persrisks, 0, FILLER_REDEFINE);
  	public ZonedDecimalData persrisk01 = new ZonedDecimalData(7, 3).isAPartOf(filler2, 0);
  	public ZonedDecimalData persrisk02 = new ZonedDecimalData(7, 3).isAPartOf(filler2, 7);
  	public ZonedDecimalData persrisk03 = new ZonedDecimalData(7, 3).isAPartOf(filler2, 14);
  	public ZonedDecimalData termMin = new ZonedDecimalData(2, 0).isAPartOf(t5611Rec, 64);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(434).isAPartOf(t5611Rec, 66, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5611Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5611Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}