package com.csc.life.general.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5605screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 6, 2, 53}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5605ScreenVars sv = (S5605ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5605screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5605ScreenVars screenVars = (S5605ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.windowRow.setClassString("");
		screenVars.windowColumn.setClassString("");
		screenVars.premind.setClassString("");
		screenVars.sumasind.setClassString("");
		screenVars.indxflg.setClassString("");
		screenVars.autorate.setClassString("");
	}

/**
 * Clear all the variables in S5605screen
 */
	public static void clear(VarModel pv) {
		S5605ScreenVars screenVars = (S5605ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.windowRow.clear();
		screenVars.windowColumn.clear();
		screenVars.premind.clear();
		screenVars.sumasind.clear();
		screenVars.indxflg.clear();
		screenVars.autorate.clear();
	}
}
