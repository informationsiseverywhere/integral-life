package com.csc.life.general.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 *
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:33
 * Description:
 * Copybook name: T5656REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5656rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************

  	public FixedLengthStringData t5656Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData sumbfacs = new FixedLengthStringData(42).isAPartOf(t5656Rec, 0);
  	public ZonedDecimalData[] sumbfac = ZDArrayPartOfStructure(6, 7, 3, sumbfacs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(sumbfacs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData sumbfac01 = new ZonedDecimalData(7, 3).isAPartOf(filler, 0);
  	public ZonedDecimalData sumbfac02 = new ZonedDecimalData(7, 3).isAPartOf(filler, 7);
  	public ZonedDecimalData sumbfac03 = new ZonedDecimalData(7, 3).isAPartOf(filler, 14);
  	public ZonedDecimalData sumbfac04 = new ZonedDecimalData(7, 3).isAPartOf(filler, 21);
  	public ZonedDecimalData sumbfac05 = new ZonedDecimalData(7, 3).isAPartOf(filler, 28);
  	public ZonedDecimalData sumbfac06 = new ZonedDecimalData(7, 3).isAPartOf(filler, 35);
  	public ZonedDecimalData termMin = new ZonedDecimalData(3, 0).isAPartOf(t5656Rec, 42);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(456).isAPartOf(t5656Rec, 44, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5656Rec);
	}


	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5656Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}