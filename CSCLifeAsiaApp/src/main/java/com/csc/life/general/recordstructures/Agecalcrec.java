package com.csc.life.general.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:04
 * Description:
 * Copybook name: AGECALCREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agecalcrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData agecalcRec = new FixedLengthStringData(64);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(agecalcRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(agecalcRec, 5);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(agecalcRec, 9);
  	public FixedLengthStringData intDatex1 = new FixedLengthStringData(8).isAPartOf(agecalcRec, 12);
  	public ZonedDecimalData intDate1 = new ZonedDecimalData(8, 0).isAPartOf(intDatex1, 0).setUnsigned();
  	public FixedLengthStringData intDatex2 = new FixedLengthStringData(8).isAPartOf(agecalcRec, 20);
  	public ZonedDecimalData intDate2 = new ZonedDecimalData(8, 0).isAPartOf(intDatex2, 0).setUnsigned();
  	public ZonedDecimalData agerating = new ZonedDecimalData(3, 0).isAPartOf(agecalcRec, 28);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(agecalcRec, 31);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(agecalcRec, 32);
	public ZonedDecimalData year1 = new ZonedDecimalData(4, 0).isAPartOf(agecalcRec, 33); //MTL002
  	public ZonedDecimalData year2 = new ZonedDecimalData(4, 0).isAPartOf(agecalcRec, 37); //MTL002
  	public FixedLengthStringData filler = new FixedLengthStringData(23).isAPartOf(agecalcRec, 41, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agecalcRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agecalcRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}