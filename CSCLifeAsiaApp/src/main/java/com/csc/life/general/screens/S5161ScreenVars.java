package com.csc.life.general.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5161
 * @version 1.0 generated on 30/08/09 06:37
 * @author Quipoz
 */
public class S5161ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1794);
	public FixedLengthStringData dataFields = new FixedLengthStringData(898).isAPartOf(dataArea, 0);
	public ZonedDecimalData agrossprem = DD.agrossprem.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData annamnts = new FixedLengthStringData(425).isAPartOf(dataFields, 17);
	public ZonedDecimalData[] annamnt = ZDArrayPartOfStructure(25, 17, 2, annamnts, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(425).isAPartOf(annamnts, 0, FILLER_REDEFINE);
	public ZonedDecimalData annamnt01 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData annamnt02 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,17);
	public ZonedDecimalData annamnt03 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,34);
	public ZonedDecimalData annamnt04 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,51);
	public ZonedDecimalData annamnt05 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,68);
	public ZonedDecimalData annamnt06 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,85);
	public ZonedDecimalData annamnt07 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,102);
	public ZonedDecimalData annamnt08 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,119);
	public ZonedDecimalData annamnt09 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,136);
	public ZonedDecimalData annamnt10 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,153);
	public ZonedDecimalData annamnt11 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,170);
	public ZonedDecimalData annamnt12 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,187);
	public ZonedDecimalData annamnt13 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,204);
	public ZonedDecimalData annamnt14 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,221);
	public ZonedDecimalData annamnt15 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,238);
	public ZonedDecimalData annamnt16 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,255);
	public ZonedDecimalData annamnt17 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,272);
	public ZonedDecimalData annamnt18 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,289);
	public ZonedDecimalData annamnt19 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,306);
	public ZonedDecimalData annamnt20 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,323);
	public ZonedDecimalData annamnt21 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,340);
	public ZonedDecimalData annamnt22 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,357);
	public ZonedDecimalData annamnt23 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,374);
	public ZonedDecimalData annamnt24 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,391);
	public ZonedDecimalData annamnt25 = DD.annamnt.copyToZonedDecimal().isAPartOf(filler,408);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,442);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,450);
	public ZonedDecimalData grossprem = DD.grossprem.copyToZonedDecimal().isAPartOf(dataFields,452);
	public FixedLengthStringData instamnts = new FixedLengthStringData(425).isAPartOf(dataFields, 469);
	public ZonedDecimalData[] instamnt = ZDArrayPartOfStructure(25, 17, 2, instamnts, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(425).isAPartOf(instamnts, 0, FILLER_REDEFINE);
	public ZonedDecimalData instamnt01 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData instamnt02 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,17);
	public ZonedDecimalData instamnt03 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,34);
	public ZonedDecimalData instamnt04 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,51);
	public ZonedDecimalData instamnt05 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,68);
	public ZonedDecimalData instamnt06 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,85);
	public ZonedDecimalData instamnt07 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,102);
	public ZonedDecimalData instamnt08 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,119);
	public ZonedDecimalData instamnt09 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,136);
	public ZonedDecimalData instamnt10 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,153);
	public ZonedDecimalData instamnt11 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,170);
	public ZonedDecimalData instamnt12 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,187);
	public ZonedDecimalData instamnt13 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,204);
	public ZonedDecimalData instamnt14 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,221);
	public ZonedDecimalData instamnt15 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,238);
	public ZonedDecimalData instamnt16 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,255);
	public ZonedDecimalData instamnt17 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,272);
	public ZonedDecimalData instamnt18 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,289);
	public ZonedDecimalData instamnt19 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,306);
	public ZonedDecimalData instamnt20 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,323);
	public ZonedDecimalData instamnt21 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,340);
	public ZonedDecimalData instamnt22 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,357);
	public ZonedDecimalData instamnt23 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,374);
	public ZonedDecimalData instamnt24 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,391);
	public ZonedDecimalData instamnt25 = DD.instamnt.copyToZonedDecimal().isAPartOf(filler1,408);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,894);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,896);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(224).isAPartOf(dataArea, 898);
	public FixedLengthStringData agrosspremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData annamntsErr = new FixedLengthStringData(100).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] annamntErr = FLSArrayPartOfStructure(25, 4, annamntsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(100).isAPartOf(annamntsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData annamnt01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData annamnt02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData annamnt03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData annamnt04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData annamnt05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData annamnt06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData annamnt07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData annamnt08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData annamnt09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData annamnt10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData annamnt11Err = new FixedLengthStringData(4).isAPartOf(filler2, 40);
	public FixedLengthStringData annamnt12Err = new FixedLengthStringData(4).isAPartOf(filler2, 44);
	public FixedLengthStringData annamnt13Err = new FixedLengthStringData(4).isAPartOf(filler2, 48);
	public FixedLengthStringData annamnt14Err = new FixedLengthStringData(4).isAPartOf(filler2, 52);
	public FixedLengthStringData annamnt15Err = new FixedLengthStringData(4).isAPartOf(filler2, 56);
	public FixedLengthStringData annamnt16Err = new FixedLengthStringData(4).isAPartOf(filler2, 60);
	public FixedLengthStringData annamnt17Err = new FixedLengthStringData(4).isAPartOf(filler2, 64);
	public FixedLengthStringData annamnt18Err = new FixedLengthStringData(4).isAPartOf(filler2, 68);
	public FixedLengthStringData annamnt19Err = new FixedLengthStringData(4).isAPartOf(filler2, 72);
	public FixedLengthStringData annamnt20Err = new FixedLengthStringData(4).isAPartOf(filler2, 76);
	public FixedLengthStringData annamnt21Err = new FixedLengthStringData(4).isAPartOf(filler2, 80);
	public FixedLengthStringData annamnt22Err = new FixedLengthStringData(4).isAPartOf(filler2, 84);
	public FixedLengthStringData annamnt23Err = new FixedLengthStringData(4).isAPartOf(filler2, 88);
	public FixedLengthStringData annamnt24Err = new FixedLengthStringData(4).isAPartOf(filler2, 92);
	public FixedLengthStringData annamnt25Err = new FixedLengthStringData(4).isAPartOf(filler2, 96);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData grosspremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData instamntsErr = new FixedLengthStringData(100).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData[] instamntErr = FLSArrayPartOfStructure(25, 4, instamntsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(100).isAPartOf(instamntsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData instamnt01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData instamnt02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData instamnt03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData instamnt04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData instamnt05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData instamnt06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData instamnt07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData instamnt08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData instamnt09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData instamnt10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData instamnt11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData instamnt12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData instamnt13Err = new FixedLengthStringData(4).isAPartOf(filler3, 48);
	public FixedLengthStringData instamnt14Err = new FixedLengthStringData(4).isAPartOf(filler3, 52);
	public FixedLengthStringData instamnt15Err = new FixedLengthStringData(4).isAPartOf(filler3, 56);
	public FixedLengthStringData instamnt16Err = new FixedLengthStringData(4).isAPartOf(filler3, 60);
	public FixedLengthStringData instamnt17Err = new FixedLengthStringData(4).isAPartOf(filler3, 64);
	public FixedLengthStringData instamnt18Err = new FixedLengthStringData(4).isAPartOf(filler3, 68);
	public FixedLengthStringData instamnt19Err = new FixedLengthStringData(4).isAPartOf(filler3, 72);
	public FixedLengthStringData instamnt20Err = new FixedLengthStringData(4).isAPartOf(filler3, 76);
	public FixedLengthStringData instamnt21Err = new FixedLengthStringData(4).isAPartOf(filler3, 80);
	public FixedLengthStringData instamnt22Err = new FixedLengthStringData(4).isAPartOf(filler3, 84);
	public FixedLengthStringData instamnt23Err = new FixedLengthStringData(4).isAPartOf(filler3, 88);
	public FixedLengthStringData instamnt24Err = new FixedLengthStringData(4).isAPartOf(filler3, 92);
	public FixedLengthStringData instamnt25Err = new FixedLengthStringData(4).isAPartOf(filler3, 96);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 220);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(672).isAPartOf(dataArea, 1122);
	public FixedLengthStringData[] agrosspremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData annamntsOut = new FixedLengthStringData(300).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] annamntOut = FLSArrayPartOfStructure(25, 12, annamntsOut, 0);
	public FixedLengthStringData[][] annamntO = FLSDArrayPartOfArrayStructure(12, 1, annamntOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(300).isAPartOf(annamntsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] annamnt01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] annamnt02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] annamnt03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] annamnt04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] annamnt05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] annamnt06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] annamnt07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] annamnt08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] annamnt09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] annamnt10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData[] annamnt11Out = FLSArrayPartOfStructure(12, 1, filler4, 120);
	public FixedLengthStringData[] annamnt12Out = FLSArrayPartOfStructure(12, 1, filler4, 132);
	public FixedLengthStringData[] annamnt13Out = FLSArrayPartOfStructure(12, 1, filler4, 144);
	public FixedLengthStringData[] annamnt14Out = FLSArrayPartOfStructure(12, 1, filler4, 156);
	public FixedLengthStringData[] annamnt15Out = FLSArrayPartOfStructure(12, 1, filler4, 168);
	public FixedLengthStringData[] annamnt16Out = FLSArrayPartOfStructure(12, 1, filler4, 180);
	public FixedLengthStringData[] annamnt17Out = FLSArrayPartOfStructure(12, 1, filler4, 192);
	public FixedLengthStringData[] annamnt18Out = FLSArrayPartOfStructure(12, 1, filler4, 204);
	public FixedLengthStringData[] annamnt19Out = FLSArrayPartOfStructure(12, 1, filler4, 216);
	public FixedLengthStringData[] annamnt20Out = FLSArrayPartOfStructure(12, 1, filler4, 228);
	public FixedLengthStringData[] annamnt21Out = FLSArrayPartOfStructure(12, 1, filler4, 240);
	public FixedLengthStringData[] annamnt22Out = FLSArrayPartOfStructure(12, 1, filler4, 252);
	public FixedLengthStringData[] annamnt23Out = FLSArrayPartOfStructure(12, 1, filler4, 264);
	public FixedLengthStringData[] annamnt24Out = FLSArrayPartOfStructure(12, 1, filler4, 276);
	public FixedLengthStringData[] annamnt25Out = FLSArrayPartOfStructure(12, 1, filler4, 288);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] grosspremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData instamntsOut = new FixedLengthStringData(300).isAPartOf(outputIndicators, 348);
	public FixedLengthStringData[] instamntOut = FLSArrayPartOfStructure(25, 12, instamntsOut, 0);
	public FixedLengthStringData[][] instamntO = FLSDArrayPartOfArrayStructure(12, 1, instamntOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(300).isAPartOf(instamntsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] instamnt01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] instamnt02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] instamnt03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] instamnt04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] instamnt05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] instamnt06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] instamnt07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] instamnt08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] instamnt09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] instamnt10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] instamnt11Out = FLSArrayPartOfStructure(12, 1, filler5, 120);
	public FixedLengthStringData[] instamnt12Out = FLSArrayPartOfStructure(12, 1, filler5, 132);
	public FixedLengthStringData[] instamnt13Out = FLSArrayPartOfStructure(12, 1, filler5, 144);
	public FixedLengthStringData[] instamnt14Out = FLSArrayPartOfStructure(12, 1, filler5, 156);
	public FixedLengthStringData[] instamnt15Out = FLSArrayPartOfStructure(12, 1, filler5, 168);
	public FixedLengthStringData[] instamnt16Out = FLSArrayPartOfStructure(12, 1, filler5, 180);
	public FixedLengthStringData[] instamnt17Out = FLSArrayPartOfStructure(12, 1, filler5, 192);
	public FixedLengthStringData[] instamnt18Out = FLSArrayPartOfStructure(12, 1, filler5, 204);
	public FixedLengthStringData[] instamnt19Out = FLSArrayPartOfStructure(12, 1, filler5, 216);
	public FixedLengthStringData[] instamnt20Out = FLSArrayPartOfStructure(12, 1, filler5, 228);
	public FixedLengthStringData[] instamnt21Out = FLSArrayPartOfStructure(12, 1, filler5, 240);
	public FixedLengthStringData[] instamnt22Out = FLSArrayPartOfStructure(12, 1, filler5, 252);
	public FixedLengthStringData[] instamnt23Out = FLSArrayPartOfStructure(12, 1, filler5, 264);
	public FixedLengthStringData[] instamnt24Out = FLSArrayPartOfStructure(12, 1, filler5, 276);
	public FixedLengthStringData[] instamnt25Out = FLSArrayPartOfStructure(12, 1, filler5, 288);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5161screenWritten = new LongData(0);
	public LongData S5161protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5161ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {instamnt04, instamnt07, instamnt10, instamnt13, instamnt16, instamnt18, instamnt19, instamnt20, instamnt21, instamnt22, instamnt23, instamnt24, annamnt04, annamnt07, annamnt10, annamnt13, annamnt16, annamnt18, annamnt19, annamnt20, annamnt21, annamnt22, annamnt23, annamnt24, chdrnum, life, coverage, rider, instamnt03, instamnt05, annamnt03, annamnt05, grossprem, agrossprem, instamnt06, annamnt06, instamnt08, annamnt08, instamnt09, annamnt09, instamnt11, annamnt11, instamnt12, annamnt12, instamnt14, annamnt14, instamnt15, annamnt15, instamnt17, instamnt25, annamnt17, annamnt25, instamnt01, annamnt01, instamnt02, annamnt02};
		screenOutFields = new BaseData[][] {instamnt04Out, instamnt07Out, instamnt10Out, instamnt13Out, instamnt16Out, instamnt18Out, instamnt19Out, instamnt20Out, instamnt21Out, instamnt22Out, instamnt23Out, instamnt24Out, annamnt04Out, annamnt07Out, annamnt10Out, annamnt13Out, annamnt16Out, annamnt18Out, annamnt19Out, annamnt20Out, annamnt21Out, annamnt22Out, annamnt23Out, annamnt24Out, chdrnumOut, lifeOut, coverageOut, riderOut, instamnt03Out, instamnt05Out, annamnt03Out, annamnt05Out, grosspremOut, agrosspremOut, instamnt06Out, annamnt06Out, instamnt08Out, annamnt08Out, instamnt09Out, annamnt09Out, instamnt11Out, annamnt11Out, instamnt12Out, annamnt12Out, instamnt14Out, annamnt14Out, instamnt15Out, annamnt15Out, instamnt17Out, instamnt25Out, annamnt17Out, annamnt25Out, instamnt01Out, annamnt01Out, instamnt02Out, annamnt02Out};
		screenErrFields = new BaseData[] {instamnt04Err, instamnt07Err, instamnt10Err, instamnt13Err, instamnt16Err, instamnt18Err, instamnt19Err, instamnt20Err, instamnt21Err, instamnt22Err, instamnt23Err, instamnt24Err, annamnt04Err, annamnt07Err, annamnt10Err, annamnt13Err, annamnt16Err, annamnt18Err, annamnt19Err, annamnt20Err, annamnt21Err, annamnt22Err, annamnt23Err, annamnt24Err, chdrnumErr, lifeErr, coverageErr, riderErr, instamnt03Err, instamnt05Err, annamnt03Err, annamnt05Err, grosspremErr, agrosspremErr, instamnt06Err, annamnt06Err, instamnt08Err, annamnt08Err, instamnt09Err, annamnt09Err, instamnt11Err, annamnt11Err, instamnt12Err, annamnt12Err, instamnt14Err, annamnt14Err, instamnt15Err, annamnt15Err, instamnt17Err, instamnt25Err, annamnt17Err, annamnt25Err, instamnt01Err, annamnt01Err, instamnt02Err, annamnt02Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5161screen.class;
		protectRecord = S5161protect.class;
	}

}
