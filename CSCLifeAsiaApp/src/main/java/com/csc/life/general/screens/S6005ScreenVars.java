package com.csc.life.general.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6005
 * @version 1.0 generated on 30/08/09 06:50
 * @author Quipoz
 */
public class S6005ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(276);
	public FixedLengthStringData dataFields = new FixedLengthStringData(68).isAPartOf(dataArea, 0);
	public FixedLengthStringData bappmeths = new FixedLengthStringData(20).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] bappmeth = FLSArrayPartOfStructure(5, 4, bappmeths, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(bappmeths, 0, FILLER_REDEFINE);
	public FixedLengthStringData bappmeth01 = DD.bappmeth.copy().isAPartOf(filler,0);
	public FixedLengthStringData bappmeth02 = DD.bappmeth.copy().isAPartOf(filler,4);
	public FixedLengthStringData bappmeth03 = DD.bappmeth.copy().isAPartOf(filler,8);
	public FixedLengthStringData bappmeth04 = DD.bappmeth.copy().isAPartOf(filler,12);
	public FixedLengthStringData bappmeth05 = DD.bappmeth.copy().isAPartOf(filler,16);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,20);
	public FixedLengthStringData ind = DD.ind.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData indics = new FixedLengthStringData(3).isAPartOf(dataFields, 22);
	public FixedLengthStringData[] indic = FLSArrayPartOfStructure(3, 1, indics, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(3).isAPartOf(indics, 0, FILLER_REDEFINE);
	public FixedLengthStringData indic01 = DD.indic.copy().isAPartOf(filler1,0);
	public FixedLengthStringData indic02 = DD.indic.copy().isAPartOf(filler1,1);
	public FixedLengthStringData indic03 = DD.indic.copy().isAPartOf(filler1,2);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,33);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(52).isAPartOf(dataArea, 68);
	public FixedLengthStringData bappmethsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] bappmethErr = FLSArrayPartOfStructure(5, 4, bappmethsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(20).isAPartOf(bappmethsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData bappmeth01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData bappmeth02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData bappmeth03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData bappmeth04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData bappmeth05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData indErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData indicsErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData[] indicErr = FLSArrayPartOfStructure(3, 4, indicsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(12).isAPartOf(indicsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData indic01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData indic02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData indic03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(156).isAPartOf(dataArea, 120);
	public FixedLengthStringData bappmethsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] bappmethOut = FLSArrayPartOfStructure(5, 12, bappmethsOut, 0);
	public FixedLengthStringData[][] bappmethO = FLSDArrayPartOfArrayStructure(12, 1, bappmethOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(60).isAPartOf(bappmethsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] bappmeth01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] bappmeth02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] bappmeth03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] bappmeth04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] bappmeth05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] indOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData indicsOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 84);
	public FixedLengthStringData[] indicOut = FLSArrayPartOfStructure(3, 12, indicsOut, 0);
	public FixedLengthStringData[][] indicO = FLSDArrayPartOfArrayStructure(12, 1, indicOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(36).isAPartOf(indicsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] indic01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] indic02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] indic03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6005screenWritten = new LongData(0);
	public LongData S6005protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6005ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, bappmeth01, bappmeth02, bappmeth03, bappmeth04, bappmeth05, ind, indic01, indic02, indic03};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, bappmeth01Out, bappmeth02Out, bappmeth03Out, bappmeth04Out, bappmeth05Out, indOut, indic01Out, indic02Out, indic03Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, bappmeth01Err, bappmeth02Err, bappmeth03Err, bappmeth04Err, bappmeth05Err, indErr, indic01Err, indic02Err, indic03Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6005screen.class;
		protectRecord = S6005protect.class;
	}

}
