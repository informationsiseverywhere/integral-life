package com.csc.life.general.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:07
 * Description:
 * Copybook name: CLVD3BA
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Clvd3ba extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData record = new FixedLengthStringData(1385);
  	public FixedLengthStringData lvd3ba = new FixedLengthStringData(1385).isAPartOf(record, 0);
  	public ZonedDecimalData lvdbaVnr = new ZonedDecimalData(10, 0).isAPartOf(lvd3ba, 0).setUnsigned();
  	public FixedLengthStringData lvdbaBaustein = new FixedLengthStringData(8).isAPartOf(lvd3ba, 10);
  	public ZonedDecimalData lvdbaBaulfdnr = new ZonedDecimalData(2, 0).isAPartOf(lvd3ba, 18).setUnsigned();
  	public ZonedDecimalData lvdbaKalkzpkt = new ZonedDecimalData(8, 0).isAPartOf(lvd3ba, 20).setUnsigned();
  	public PackedDecimalData lvdbaDk = new PackedDecimalData(18, 8).isAPartOf(lvd3ba, 28);
  	public PackedDecimalData lvdbaDkRegr = new PackedDecimalData(10, 0).isAPartOf(lvdbaDk, 0, REDEFINE);
  	public FixedLengthStringData lvdbaDkX = new FixedLengthStringData(10).isAPartOf(lvdbaDkRegr, 0);
  	public FixedLengthStringData lvdbaStatusDk = new FixedLengthStringData(1).isAPartOf(lvd3ba, 38);
  	public FixedLengthStringData lvdbaEb = new FixedLengthStringData(37).isAPartOf(lvd3ba, 39);
  	public ZonedDecimalData lvdbaEbVnr = new ZonedDecimalData(10, 0).isAPartOf(lvdbaEb, 0).setUnsigned();
  	public FixedLengthStringData lvdbaEbBaustein = new FixedLengthStringData(8).isAPartOf(lvdbaEb, 10);
  	public ZonedDecimalData lvdbaEbBaulfdnr = new ZonedDecimalData(2, 0).isAPartOf(lvdbaEb, 18).setUnsigned();
  	public ZonedDecimalData lvdbaEbBaubeg = new ZonedDecimalData(8, 0).isAPartOf(lvdbaEb, 20).setUnsigned();
  	public FixedLengthStringData lvdbaEbRealzust = new FixedLengthStringData(1).isAPartOf(lvdbaEb, 28);
  	public ZonedDecimalData lvdbaEbRealzustbeg = new ZonedDecimalData(8, 0).isAPartOf(lvdbaEb, 29).setUnsigned();
  	public FixedLengthStringData lvdbaEbk = new FixedLengthStringData(705).isAPartOf(lvd3ba, 76);
  	public FixedLengthStringData[] lvdbaTabEbk = FLSArrayPartOfStructure(15, 47, lvdbaEbk, 0);
  	public ZonedDecimalData[] lvdbaEbkVnr = ZDArrayPartOfArrayStructure(10, 0, lvdbaTabEbk, 0, UNSIGNED_TRUE);
  	public FixedLengthStringData[] lvdbaEbkBaustein = FLSDArrayPartOfArrayStructure(8, lvdbaTabEbk, 10);
  	public ZonedDecimalData[] lvdbaEbkBaulfdnr = ZDArrayPartOfArrayStructure(2, 0, lvdbaTabEbk, 18, UNSIGNED_TRUE);
  	public FixedLengthStringData[] lvdbaEbkKomponente = FLSDArrayPartOfArrayStructure(4, lvdbaTabEbk, 20);
  	public PackedDecimalData[] lvdbaEbkGrundbetrag = PDArrayPartOfArrayStructure(18, 8, lvdbaTabEbk, 24);
  	public PackedDecimalData[] lvdbaEbkGrundbetragRegr = PDArrayPartOfArrayStructure(10, 0, lvdbaEbkGrundbetrag, 0, REDEFINE);
  	public FixedLengthStringData[] lvdbaEbkGrundbetragX = FLSDArrayPartOfArrayStructure(10, lvdbaEbkGrundbetragRegr, 0);
  	public FixedLengthStringData[] lvdbaEbkEpsilonAussen = FLSDArrayPartOfArrayStructure(1, lvdbaTabEbk, 34);
  	public FixedLengthStringData[] lvdbaEbkKoausschlussAussen = FLSDArrayPartOfArrayStructure(1, lvdbaTabEbk, 35);
  	public PackedDecimalData[] lvdbaEbkEinheitsbarwert = PDArrayPartOfArrayStructure(18, 15, lvdbaTabEbk, 36);
  	public FixedLengthStringData[] lvdbaEbkStatusGrb = FLSDArrayPartOfArrayStructure(1, lvdbaTabEbk, 46);
  	public FixedLengthStringData lvdbaEbkz = new FixedLengthStringData(480).isAPartOf(lvd3ba, 781);
  	public FixedLengthStringData[] lvdbaTabEbkz = FLSArrayPartOfStructure(15, 32, lvdbaEbkz, 0);
  	public ZonedDecimalData[] lvdbaEbkzVnr = ZDArrayPartOfArrayStructure(10, 0, lvdbaTabEbkz, 0, UNSIGNED_TRUE);
  	public FixedLengthStringData[] lvdbaEbkzBaustein = FLSDArrayPartOfArrayStructure(8, lvdbaTabEbkz, 10);
  	public ZonedDecimalData[] lvdbaEbkzBaulfdnr = ZDArrayPartOfArrayStructure(2, 0, lvdbaTabEbkz, 18, UNSIGNED_TRUE);
  	public FixedLengthStringData[] lvdbaEbkzKomponente = FLSDArrayPartOfArrayStructure(4, lvdbaTabEbkz, 20);
  	public FixedLengthStringData[] lvdbaEbkzZustand = FLSDArrayPartOfArrayStructure(1, lvdbaTabEbkz, 24);
  	public ZonedDecimalData[] lvdbaEbkzAufschub = ZDArrayPartOfArrayStructure(3, 0, lvdbaTabEbkz, 25, UNSIGNED_TRUE);
  	public ZonedDecimalData[] lvdbaEbkzDauer = ZDArrayPartOfArrayStructure(3, 0, lvdbaTabEbkz, 28, UNSIGNED_TRUE);
  	public FixedLengthStringData[] lvdbaEbkzOblrechles = FLSDArrayPartOfArrayStructure(1, lvdbaTabEbkz, 31);
  	public FixedLengthStringData lvdbaEbvp = new FixedLengthStringData(117).isAPartOf(lvd3ba, 1261);
  	public FixedLengthStringData[] lvdbaTabEbvp = FLSArrayPartOfStructure(3, 39, lvdbaEbvp, 0);
  	public ZonedDecimalData[] lvdbaEbvpVnr = ZDArrayPartOfArrayStructure(10, 0, lvdbaTabEbvp, 0, UNSIGNED_TRUE);
  	public FixedLengthStringData[] lvdbaEbvpBaustein = FLSDArrayPartOfArrayStructure(8, lvdbaTabEbvp, 10);
  	public ZonedDecimalData[] lvdbaEbvpBaulfdnr = ZDArrayPartOfArrayStructure(2, 0, lvdbaTabEbvp, 18, UNSIGNED_TRUE);
  	public FixedLengthStringData[] lvdbaEbvpBart = FLSDArrayPartOfArrayStructure(4, lvdbaTabEbvp, 20);
  	public ZonedDecimalData[] lvdbaEbvpReinalt = ZDArrayPartOfArrayStructure(3, 0, lvdbaTabEbvp, 24, UNSIGNED_TRUE);
  	public ZonedDecimalData[] lvdbaEbvpTeinalt = ZDArrayPartOfArrayStructure(3, 0, lvdbaTabEbvp, 27, UNSIGNED_TRUE);
  	public FixedLengthStringData[] lvdbaEbvpKalkperson = FLSDArrayPartOfArrayStructure(1, lvdbaTabEbvp, 30);
  	public FixedLengthStringData[] lvdbaEbvpPerseigkombi = FLSDArrayPartOfArrayStructure(8, lvdbaTabEbvp, 31);
  	public FixedLengthStringData lvdbaLeseart = new FixedLengthStringData(1).isAPartOf(lvd3ba, 1378);
  	public ZonedDecimalData lvdbaFehler = new ZonedDecimalData(4, 0).isAPartOf(lvd3ba, 1379).setUnsigned();
  	public FixedLengthStringData lvdbaFehlerherkunft = new FixedLengthStringData(2).isAPartOf(lvd3ba, 1383);


	public void initialize() {
		COBOLFunctions.initialize(record);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		record.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}