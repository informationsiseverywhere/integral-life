/*
 * File: Rvbsum1.java
 * Date: 30 August 2009 2:14:25
 * Author: Quipoz Limited
 *
 * Class transformed from RVBSUM1.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.general.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.regularprocessing.dataaccess.CovrbonTableDAM;
import com.csc.life.regularprocessing.recordstructures.Bonusrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  Reversionary Bonus Calculation subroutine using SUM.
*
* PROCESSING.
* ----------
*
* This program is based on a copy of RVBFLYR.
* To isolate the changes required to calculate bonus for SUM
* type contracts the initiation of SUM processing must be enacted
* through table subroutines. RB03 has been created as a revers-
* ionary bonus calculation method on T5672. This is turn will be
* used as part of the key to read T6639 which holds the rever-
* sionary bonus subroutine......in this case RVBSUM1.
* All processing except the actual calculation needs to be the
* same as the LIFE subroutine RVBFLYR and for this reason the
* SUM code has been kept completely seperate in subroutine
* RVBSUM. Note that this is hard-coded here as at 4/4/95.
*
* Note T6638 processing and the test for the number of years
* in force against T6638 is not required & has been removed.
*
* For comments on common coding refer to RVBFLYR.
*
*****************************************************************
* </pre>
*/
public class Rvbsum1 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "RVBSUM1";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSumassRate = new ZonedDecimalData(7, 2).setUnsigned();
	private ZonedDecimalData wsaaBonusRate = new ZonedDecimalData(7, 2).setUnsigned();
	private PackedDecimalData wsaaRvBonusSa = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRvBonusBon = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaBonusSum = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCurbal = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private String g578 = "G578";
		/* FORMATS */
	private String covrbonrec = "COVRBONREC";
	private String acblrec = "ACBLREC";
	private String itemrec = "ITEMREC";
		/* TABLES */
	private String t5645 = "T5645";
		/*Subsidiary account balance*/
	private AcblTableDAM acblIO = new AcblTableDAM();
	private Bonusrec bonusrec = new Bonusrec();
		/*Logical view on COVR file for Bonus Subr*/
	private CovrbonTableDAM covrbonIO = new CovrbonTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit390,
		exit870,
		exit970
	}

	public Rvbsum1() {
		super();
	}

public void mainline(Object... parmArray)
	{
		bonusrec.bonusRec = convertAndSetParam(bonusrec.bonusRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		initialise200();
		readBonusLinkage300();
		/*EXIT*/
		exitProgram();
	}

protected void initialise200()
	{
		/*INITIALISE*/
		bonusrec.statuz.set("****");
		wsaaBonusSum.set(ZERO);
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		syserrrec.subrname.set(wsaaSubr);
		/*EXIT*/
	}

protected void readBonusLinkage300()
	{
		try {
			beginReading310();
		}
		catch (GOTOException e){
		}
	}

protected void beginReading310()
	{
		if (isNE(bonusrec.function,"CALC")
		&& isNE(bonusrec.function,SPACES)) {
			syserrrec.params.set(bonusrec.bonusRec);
			syserrrec.statuz.set(g578);
			systemError900();
		}
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bonusrec.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError800();
		}
		else {
			t5645rec.t5645Rec.set(itemIO.getGenarea());
		}
		acblIO.setParams(SPACES);
		wsaaRldgChdrnum.set(bonusrec.chdrChdrnum);
		wsaaRldgLife.set(bonusrec.lifeLife);
		wsaaRldgCoverage.set(bonusrec.covrCoverage);
		wsaaRldgRider.set(bonusrec.covrRider);
		wsaaPlan.set(bonusrec.plnsfx);
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		acblIO.setRldgacct(wsaaRldgacct);
		acblIO.setRldgcoy(bonusrec.chdrChdrcoy);
		acblIO.setOrigcurr(bonusrec.cntcurr);
		acblIO.setSacscode(t5645rec.sacscode01);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			databaseError800();
		}
		if (isEQ(acblIO.getStatuz(),varcom.mrnf)) {
		}
		else {
		}
		bonusCalculations500();
		if (isEQ(bonusrec.function,"CALC")) {
			goTo(GotoLabel.exit390);
		}
		compute(wsaaBonusSum, 3).setRounded((add(bonusrec.rvBonusSa,bonusrec.rvBonusBon)));
		bonusrec.rvBalLy.set(wsaaBonusSum);
		lifacmvrec1.lifacmvRec.set(SPACES);
		lifacmvrec1.rdocnum.set(bonusrec.chdrChdrnum);
		lifacmvrec1.jrnseq.set(0);
		lifacmvrec1.rldgcoy.set(bonusrec.chdrChdrcoy);
		lifacmvrec1.genlcoy.set(bonusrec.chdrChdrcoy);
		lifacmvrec1.batckey.set(bonusrec.batckey);
		lifacmvrec1.origcurr.set(bonusrec.cntcurr);
		lifacmvrec1.origamt.set(wsaaBonusSum);
		lifacmvrec1.rcamt.set(0);
		lifacmvrec1.crate.set(0);
		lifacmvrec1.acctamt.set(0);
		lifacmvrec1.contot.set(0);
		lifacmvrec1.tranno.set(bonusrec.tranno);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.effdate.set(bonusrec.effectiveDate);
		lifacmvrec1.tranref.set(bonusrec.chdrChdrnum);
		lifacmvrec1.substituteCode[1].set(bonusrec.cnttype);
		lifacmvrec1.substituteCode[6].set(bonusrec.crtable);
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bonusrec.chdrChdrcoy);
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaSubr);
		descIO.setLanguage(bonusrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		lifacmvrec1.trandesc.set(descIO.getLongdesc());
		wsaaRldgChdrnum.set(bonusrec.chdrChdrnum);
		wsaaRldgLife.set(bonusrec.lifeLife);
		wsaaRldgCoverage.set(bonusrec.covrCoverage);
		wsaaRldgRider.set(bonusrec.covrRider);
		wsaaPlan.set(bonusrec.plnsfx);
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec1.rldgacct.set(wsaaRldgacct);
		lifacmvrec1.transactionDate.set(varcom.vrcmDate);
		lifacmvrec1.transactionTime.set(varcom.vrcmTime);
		lifacmvrec1.user.set(bonusrec.user);
		lifacmvrec1.sacscode.set(t5645rec.sacscode02);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec1.glcode.set(t5645rec.glmap02);
		lifacmvrec1.glsign.set(t5645rec.sign02);
		lifacmvrec1.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			databaseError800();
		}
		covrbonIO.setDataKey(SPACES);
		covrbonIO.setChdrcoy(bonusrec.chdrChdrcoy);
		covrbonIO.setChdrnum(bonusrec.chdrChdrnum);
		covrbonIO.setLife(bonusrec.lifeLife);
		covrbonIO.setCoverage(bonusrec.covrCoverage);
		covrbonIO.setRider(bonusrec.covrRider);
		covrbonIO.setPlanSuffix(bonusrec.plnsfx);
		covrbonIO.setFunction(varcom.readh);
		covrbonIO.setFormat(covrbonrec);
		SmartFileCode.execute(appVars, covrbonIO);
		if (isNE(covrbonIO.getStatuz(),varcom.oK)
		&& isNE(covrbonIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrbonIO.getParams());
			syserrrec.statuz.set(covrbonIO.getStatuz());
			databaseError800();
		}
		covrbonIO.setCurrto(bonusrec.effectiveDate);
		covrbonIO.setValidflag("2");
		covrbonIO.setFunction(varcom.rewrt);
		covrbonIO.setFormat(covrbonrec);
		SmartFileCode.execute(appVars, covrbonIO);
		if (isNE(covrbonIO.getStatuz(),varcom.oK)
		&& isNE(covrbonIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrbonIO.getParams());
			syserrrec.statuz.set(covrbonIO.getStatuz());
			databaseError800();
		}
		covrbonIO.setValidflag("1");
		covrbonIO.setTranno(bonusrec.tranno);
		covrbonIO.setCurrfrom(bonusrec.effectiveDate);
		covrbonIO.setUnitStatementDate(bonusrec.effectiveDate);
		covrbonIO.setCurrto(varcom.vrcmMaxDate);
		covrbonIO.setFunction(varcom.writr);
		covrbonIO.setFormat(covrbonrec);
		SmartFileCode.execute(appVars, covrbonIO);
		if (isNE(covrbonIO.getStatuz(),varcom.oK)
		&& isNE(covrbonIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrbonIO.getParams());
			syserrrec.statuz.set(covrbonIO.getStatuz());
			databaseError800();
		}
		if (isNE(bonusrec.lowCostRider,SPACES)
		&& isNE(bonusrec.lowCostSubr,SPACES)) {
			callProgram(bonusrec.lowCostSubr, bonusrec.bonusRec);
			if (isNE(bonusrec.statuz,varcom.oK)) {
				syserrrec.params.set(bonusrec.bonusRec);
				syserrrec.statuz.set(bonusrec.statuz);
				systemError900();
			}
		}
	}

protected void bonusCalculations500()
	{
		/*START*/
		callProgram(Rvbsum.class, bonusrec.bonusRec);
		if (isNE(bonusrec.statuz,varcom.oK)
		&& isNE(bonusrec.statuz,SPACES)) {
			syserrrec.params.set(bonusrec.bonusRec);
			syserrrec.statuz.set(bonusrec.statuz);
			systemError900();
		}
		/*EXIT*/
	}

protected void databaseError800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start810();
				}
				case exit870: {
					exit870();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start810()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit870);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit870()
	{
		bonusrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void systemError900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start910();
				}
				case exit970: {
					exit970();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start910()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit970);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit970()
	{
		bonusrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
