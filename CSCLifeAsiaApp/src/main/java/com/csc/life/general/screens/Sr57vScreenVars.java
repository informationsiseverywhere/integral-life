package com.csc.life.general.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen Variables for Sr57v
 * @version 1.0 Generated on Wed Sep 04 21:52:10 SGT 2013
 * @author CSC
 */
public class Sr57vScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(493);
	public FixedLengthStringData dataFields = new FixedLengthStringData(205).isAPartOf(dataArea, 0);
	//---------------data datafield contents
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0); 
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1); 
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9); 
	public FixedLengthStringData subrtns = new FixedLengthStringData(56).isAPartOf(dataFields, 39);
	public FixedLengthStringData[] subrtn = FLSArrayPartOfStructure(7, 8, subrtns, 0);
	public FixedLengthStringData subrtn01 = DD.subrtn.copy().isAPartOf(subrtns,0); 
	public FixedLengthStringData subrtn02 = DD.subrtn.copy().isAPartOf(subrtns,8); 
	public FixedLengthStringData subrtn03 = DD.subrtn.copy().isAPartOf(subrtns,16); 
	public FixedLengthStringData subrtn04 = DD.subrtn.copy().isAPartOf(subrtns,24); 
	public FixedLengthStringData subrtn05 = DD.subrtn.copy().isAPartOf(subrtns,32); 
	public FixedLengthStringData subrtn06 = DD.subrtn.copy().isAPartOf(subrtns,40); 
	public FixedLengthStringData subrtn07 = DD.subrtn.copy().isAPartOf(subrtns,48); 
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,95); 
	public FixedLengthStringData zdmsions = new FixedLengthStringData(105).isAPartOf(dataFields, 100);
	public FixedLengthStringData[] zdmsion = FLSArrayPartOfStructure(7, 15, zdmsions, 0);
	public FixedLengthStringData zdmsion01 = DD.zdmsion.copy().isAPartOf(zdmsions,0); 
	public FixedLengthStringData zdmsion02 = DD.zdmsion.copy().isAPartOf(zdmsions,15); 
	public FixedLengthStringData zdmsion03 = DD.zdmsion.copy().isAPartOf(zdmsions,30); 
	public FixedLengthStringData zdmsion04 = DD.zdmsion.copy().isAPartOf(zdmsions,45); 
	public FixedLengthStringData zdmsion05 = DD.zdmsion.copy().isAPartOf(zdmsions,60); 
	public FixedLengthStringData zdmsion06 = DD.zdmsion.copy().isAPartOf(zdmsions,75); 
	public FixedLengthStringData zdmsion07 = DD.zdmsion.copy().isAPartOf(zdmsions,90); 

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 205);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData subrtnsErr = new FixedLengthStringData(28).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] subrtnErr = FLSArrayPartOfStructure(7, 4, subrtnsErr, 0);
	public FixedLengthStringData subrtn01Err =new FixedLengthStringData(4).isAPartOf(subrtnsErr,0); 
	public FixedLengthStringData subrtn02Err =new FixedLengthStringData(4).isAPartOf(subrtnsErr,4); 
	public FixedLengthStringData subrtn03Err =new FixedLengthStringData(4).isAPartOf(subrtnsErr,8); 
	public FixedLengthStringData subrtn04Err =new FixedLengthStringData(4).isAPartOf(subrtnsErr,12); 
	public FixedLengthStringData subrtn05Err =new FixedLengthStringData(4).isAPartOf(subrtnsErr,16); 
	public FixedLengthStringData subrtn06Err =new FixedLengthStringData(4).isAPartOf(subrtnsErr,20); 
	public FixedLengthStringData subrtn07Err =new FixedLengthStringData(4).isAPartOf(subrtnsErr,24); 
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData zdmsionsErr = new FixedLengthStringData(28).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData[] zdmsionErr = FLSArrayPartOfStructure(7, 4, zdmsionsErr, 0);
	public FixedLengthStringData zdmsion01Err =new FixedLengthStringData(4).isAPartOf(zdmsionsErr,0); 
	public FixedLengthStringData zdmsion02Err =new FixedLengthStringData(4).isAPartOf(zdmsionsErr,4); 
	public FixedLengthStringData zdmsion03Err =new FixedLengthStringData(4).isAPartOf(zdmsionsErr,8); 
	public FixedLengthStringData zdmsion04Err =new FixedLengthStringData(4).isAPartOf(zdmsionsErr,12); 
	public FixedLengthStringData zdmsion05Err =new FixedLengthStringData(4).isAPartOf(zdmsionsErr,16); 
	public FixedLengthStringData zdmsion06Err =new FixedLengthStringData(4).isAPartOf(zdmsionsErr,20); 
	public FixedLengthStringData zdmsion07Err =new FixedLengthStringData(4).isAPartOf(zdmsionsErr,24); 

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(216).isAPartOf(dataArea,277);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData subrtnsOut = new FixedLengthStringData(84).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] subrtnOut = FLSArrayPartOfStructure(7, 12, subrtnsOut, 0);
	public FixedLengthStringData[][] subrtnO = FLSDArrayPartOfArrayStructure(12, 1, subrtnOut, 0);
	public FixedLengthStringData[] subrtn01Out = FLSArrayPartOfStructure(12, 1, subrtnsOut, 0);
	public FixedLengthStringData[] subrtn02Out = FLSArrayPartOfStructure(12, 1, subrtnsOut, 12);
	public FixedLengthStringData[] subrtn03Out = FLSArrayPartOfStructure(12, 1, subrtnsOut, 24);
	public FixedLengthStringData[] subrtn04Out = FLSArrayPartOfStructure(12, 1, subrtnsOut, 36);
	public FixedLengthStringData[] subrtn05Out = FLSArrayPartOfStructure(12, 1, subrtnsOut, 48);
	public FixedLengthStringData[] subrtn06Out = FLSArrayPartOfStructure(12, 1, subrtnsOut, 60);
	public FixedLengthStringData[] subrtn07Out = FLSArrayPartOfStructure(12, 1, subrtnsOut, 72);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData zdmsionsOut = new FixedLengthStringData(84).isAPartOf(outputIndicators, 132);
	public FixedLengthStringData[] zdmsionOut = FLSArrayPartOfStructure(7, 12, zdmsionsOut, 0);
	public FixedLengthStringData[][] zdmsionO = FLSDArrayPartOfArrayStructure(12, 1, zdmsionOut, 0);
	public FixedLengthStringData[] zdmsion01Out = FLSArrayPartOfStructure(12, 1, zdmsionsOut, 0);
	public FixedLengthStringData[] zdmsion02Out = FLSArrayPartOfStructure(12, 1, zdmsionsOut, 12);
	public FixedLengthStringData[] zdmsion03Out = FLSArrayPartOfStructure(12, 1, zdmsionsOut, 24);
	public FixedLengthStringData[] zdmsion04Out = FLSArrayPartOfStructure(12, 1, zdmsionsOut, 36);
	public FixedLengthStringData[] zdmsion05Out = FLSArrayPartOfStructure(12, 1, zdmsionsOut, 48);
	public FixedLengthStringData[] zdmsion06Out = FLSArrayPartOfStructure(12, 1, zdmsionsOut, 60);
	public FixedLengthStringData[] zdmsion07Out = FLSArrayPartOfStructure(12, 1, zdmsionsOut, 72);

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	
	
	public LongData Sr57vscreenctlWritten = new LongData(0);
	public LongData Sr57vscreensflWritten = new LongData(0);
	public LongData Sr57vscreenWritten = new LongData(0);
	public LongData Sr57vwindowWritten = new LongData(0);
	public LongData Sr57vhideWritten = new LongData(0);
	public LongData Sr57vprotectWritten = new LongData(0);

	public boolean hasSubfile() { 
		return false;
	}

	public Sr57vScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	protected void initialiseScreenVars() {
		fieldIndMap.put(companyOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(itemOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(longdescOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subrtn01Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subrtn02Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subrtn03Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subrtn04Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subrtn05Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subrtn06Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subrtn07Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tablOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdmsion01Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdmsion02Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdmsion03Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdmsion04Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdmsion05Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdmsion06Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdmsion07Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	
		screenFields = new BaseData[] { company,item,longdesc,subrtn01,subrtn02,subrtn03,subrtn04,subrtn05,subrtn06,subrtn07,tabl,zdmsion01,zdmsion02,zdmsion03,zdmsion04,zdmsion05,zdmsion06,zdmsion07 };
		screenOutFields = new BaseData[][] { companyOut,itemOut,longdescOut,subrtn01Out,subrtn02Out,subrtn03Out,subrtn04Out,subrtn05Out,subrtn06Out,subrtn07Out,tablOut,zdmsion01Out,zdmsion02Out,zdmsion03Out,zdmsion04Out,zdmsion05Out,zdmsion06Out,zdmsion07Out };
		screenErrFields = new BaseData[] {  companyErr,itemErr,longdescErr,subrtn01Err,subrtn02Err,subrtn03Err,subrtn04Err,subrtn05Err,subrtn06Err,subrtn07Err,tablErr,zdmsion01Err,zdmsion02Err,zdmsion03Err,zdmsion04Err,zdmsion05Err,zdmsion06Err,zdmsion07Err  };
		screenDateFields = new BaseData[] {   };
		screenDateErrFields = new BaseData[] {   };
		screenDateDispFields = new BaseData[] {    };
	
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr57vscreen.class;
		protectRecord = Sr57vprotect.class;
	}

 

}
