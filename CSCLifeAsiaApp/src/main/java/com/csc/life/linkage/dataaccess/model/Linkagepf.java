package com.csc.life.linkage.dataaccess.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * Created under ILIFE-6941
 */
@Entity
@Table(name = "LINKAGEPF")
public class Linkagepf {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "UNIQUE_NUMBER")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name = "LINKREF")
	private String linkRef;
	@Column(name = "LINKTYPE")
	private String linkType;
	@Column(name = "LINKINFO")
	private String linkInfo;
	@Column(name = "USERNAME")
	private String username;
	@Column(name = "DATIME")
	private Date datime;
	@Column(name = "UPDTUSER")
	private String updtuser;
	@Column(name = "UPDTIME")
	private Date updtime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLinkRef() {
		return linkRef;
	}

	public void setLinkRef(String linkRef) {
		this.linkRef = linkRef;
	}

	public String getLinkType() {
		return linkType;
	}

	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}


	public String getLinkInfo() {
		return linkInfo;
	}

	public void setLinkInfo(String linkInfo) {
		this.linkInfo = linkInfo;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getDatime() {
		return datime;
	}

	public void setDatime(Date datime) {
		this.datime = datime;
	}

	public String getUpdtuser() {
		return updtuser;
	}

	public void setUpdtuser(String updtuser) {
		this.updtuser = updtuser;
	}

	public Date getUpdtime() {
		return updtime;
	}

	public void setUpdtime(Date updtime) {
		this.updtime = updtime;
	}

}