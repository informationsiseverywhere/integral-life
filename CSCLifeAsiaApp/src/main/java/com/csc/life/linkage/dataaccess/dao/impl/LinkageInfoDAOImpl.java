package com.csc.life.linkage.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.linkage.dataaccess.dao.LinkageInfoDAO;
import com.csc.life.linkage.dataaccess.model.Linkagepf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;


/**
 * @author sagrawal35
 *
 * Created under ILIFE-6941
 * 
 * This class is the implementation class of LinkageInfoDAO
 * It performs all the database operations related with LINKAGEPF table
 */
public class LinkageInfoDAOImpl extends BaseDAOImpl<Linkagepf> implements LinkageInfoDAO {	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LinkageInfoDAOImpl.class);	
	
	@Override
	public Linkagepf fetchLinkageInfo(String linkageNum){
		
		StringBuilder sql = new StringBuilder();
        sql.append(
                "SELECT LINKINFO FROM LINKAGEPF WHERE LINKREF=? ORDER BY UNIQUE_NUMBER DESC ");
        PreparedStatement ps = getPrepareStatement(sql.toString());
        ResultSet rs = null; 
        Linkagepf linkagepf = null;
        try {
        	ps.setString(1, linkageNum);			
            rs = executeQuery(ps);

            while (rs.next()) {
            	linkagepf = new Linkagepf();
            	linkagepf.setLinkInfo(rs.getString(1));                               
            }
        }
        catch (SQLException e) {
            LOGGER.error("getLinkageInfo()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } 
        finally {
            close(ps, rs);
        }
        return linkagepf;		
	};	

}
