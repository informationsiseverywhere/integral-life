package com.csc.life.linkage.dataaccess.dao;

import com.csc.life.linkage.dataaccess.model.Linkagepf;


/**
 * @author sagrawal35
 *
 * Created under ILIFE-6941
 * 
 * Interface to perform all the database operations related with LINKAGEPF table
 */
public interface LinkageInfoDAO {	
	
	public Linkagepf fetchLinkageInfo(String linkageNum);

}