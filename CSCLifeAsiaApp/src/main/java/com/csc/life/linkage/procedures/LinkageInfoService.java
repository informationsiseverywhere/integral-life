package com.csc.life.linkage.procedures;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.linkage.dataaccess.dao.LinkageInfoDAO;
import com.csc.life.linkage.dataaccess.model.Linkagepf;
import com.csc.smart400framework.parent.SMARTCodeModel;

/**
 * @author sagrawal35
 *
 * This class reads LINKAGEPF table using the input parameter 'linkNum'
 * and return linkage cover info in JSON format 
 * 
 * Created under ILIFE-6941
 */
public class LinkageInfoService extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(LinkageInfoService.class);	
	
	private LinkageInfoDAO linkageInfoDAO = getApplicationContext().getBean("linkageInfoDAO", LinkageInfoDAO.class);	
	
	public String getLinkageInfo(String linkNum){	
		String linkInfo = null;
		// Code to read LINKAGEPF table based on passed 'Linkage Number' and return linkInfo value in JSON format			
		try {
			Linkagepf linkagepfObj = linkageInfoDAO.fetchLinkageInfo(linkNum);
			//ILIFE-6964 - Start
			if (linkagepfObj != null) {
				linkInfo = linkagepfObj.getLinkInfo();
			} 
			//ILIFE-6964 - End
		} catch (Exception e) {
			LOGGER.error("LinkageInfoService :: Exception found in getting Linked Coverage info :: ", e.getMessage());//IJTI-1561
		}
		return linkInfo;			
	}		
	
}