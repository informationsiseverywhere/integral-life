package com.csc.life.underwriting.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UndlTableDAM.java
 * Date: Sun, 30 Aug 2009 03:51:08
 * Class transformed from UNDL.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UndlTableDAM extends UndlpfTableDAM {

	public UndlTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("UNDL");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", JLIFE";
		
		QUALIFIEDCOLUMNS = 
		            "BMI, " +
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "JLIFE, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "HEIGHT, " +
		            "WEIGHT, " +
		            "BMIBASIS, " +
		            "BMIRULE, " +
		            "OVRRULE, " +
		            "CLNTNUM01, " +
		            "CLNTNUM02, " +
		            "CLNTNUM03, " +
		            "CLNTNUM04, " +
		            "CLNTNUM05, " +
		            "TRANNO, " +
		            "UNDWFLAG, " +
		            "VALIDFLAG, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "JLIFE ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "JLIFE DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               bmi,
                               chdrcoy,
                               chdrnum,
                               life,
                               jlife,
                               currfrom,
                               currto,
                               height,
                               weight,
                               bmibasis,
                               bmirule,
                               ovrrule,
                               clntnum01,
                               clntnum02,
                               clntnum03,
                               clntnum04,
                               clntnum05,
                               tranno,
                               undwflag,
                               validflag,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(243);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getJlife().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller40.setInternal(jlife.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(137);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getHeight().toInternal()
					+ getWeight().toInternal()
					+ getBmi().toInternal()
					+ getBmibasis().toInternal()
					+ getBmirule().toInternal()
					+ getOvrrule().toInternal()
					+ getClntnum01().toInternal()
					+ getClntnum02().toInternal()
					+ getClntnum03().toInternal()
					+ getClntnum04().toInternal()
					+ getClntnum05().toInternal()
					+ getTranno().toInternal()
					+ getUndwflag().toInternal()
					+ getValidflag().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, height);
			what = ExternalData.chop(what, weight);
			what = ExternalData.chop(what, bmi);
			what = ExternalData.chop(what, bmibasis);
			what = ExternalData.chop(what, bmirule);
			what = ExternalData.chop(what, ovrrule);
			what = ExternalData.chop(what, clntnum01);
			what = ExternalData.chop(what, clntnum02);
			what = ExternalData.chop(what, clntnum03);
			what = ExternalData.chop(what, clntnum04);
			what = ExternalData.chop(what, clntnum05);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, undwflag);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public PackedDecimalData getHeight() {
		return height;
	}
	public void setHeight(Object what) {
		setHeight(what, false);
	}
	public void setHeight(Object what, boolean rounded) {
		if (rounded)
			height.setRounded(what);
		else
			height.set(what);
	}	
	public PackedDecimalData getWeight() {
		return weight;
	}
	public void setWeight(Object what) {
		setWeight(what, false);
	}
	public void setWeight(Object what, boolean rounded) {
		if (rounded)
			weight.setRounded(what);
		else
			weight.set(what);
	}	
	public PackedDecimalData getBmi() {
		return bmi;
	}
	public void setBmi(Object what) {
		setBmi(what, false);
	}
	public void setBmi(Object what, boolean rounded) {
		if (rounded)
			bmi.setRounded(what);
		else
			bmi.set(what);
	}	
	public FixedLengthStringData getBmibasis() {
		return bmibasis;
	}
	public void setBmibasis(Object what) {
		bmibasis.set(what);
	}	
	public FixedLengthStringData getBmirule() {
		return bmirule;
	}
	public void setBmirule(Object what) {
		bmirule.set(what);
	}	
	public FixedLengthStringData getOvrrule() {
		return ovrrule;
	}
	public void setOvrrule(Object what) {
		ovrrule.set(what);
	}	
	public FixedLengthStringData getClntnum01() {
		return clntnum01;
	}
	public void setClntnum01(Object what) {
		clntnum01.set(what);
	}	
	public FixedLengthStringData getClntnum02() {
		return clntnum02;
	}
	public void setClntnum02(Object what) {
		clntnum02.set(what);
	}	
	public FixedLengthStringData getClntnum03() {
		return clntnum03;
	}
	public void setClntnum03(Object what) {
		clntnum03.set(what);
	}	
	public FixedLengthStringData getClntnum04() {
		return clntnum04;
	}
	public void setClntnum04(Object what) {
		clntnum04.set(what);
	}	
	public FixedLengthStringData getClntnum05() {
		return clntnum05;
	}
	public void setClntnum05(Object what) {
		clntnum05.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getUndwflag() {
		return undwflag;
	}
	public void setUndwflag(Object what) {
		undwflag.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getClntnums() {
		return new FixedLengthStringData(clntnum01.toInternal()
										+ clntnum02.toInternal()
										+ clntnum03.toInternal()
										+ clntnum04.toInternal()
										+ clntnum05.toInternal());
	}
	public void setClntnums(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getClntnums().getLength()).init(obj);
	
		what = ExternalData.chop(what, clntnum01);
		what = ExternalData.chop(what, clntnum02);
		what = ExternalData.chop(what, clntnum03);
		what = ExternalData.chop(what, clntnum04);
		what = ExternalData.chop(what, clntnum05);
	}
	public FixedLengthStringData getClntnum(BaseData indx) {
		return getClntnum(indx.toInt());
	}
	public FixedLengthStringData getClntnum(int indx) {

		switch (indx) {
			case 1 : return clntnum01;
			case 2 : return clntnum02;
			case 3 : return clntnum03;
			case 4 : return clntnum04;
			case 5 : return clntnum05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setClntnum(BaseData indx, Object what) {
		setClntnum(indx.toInt(), what);
	}
	public void setClntnum(int indx, Object what) {

		switch (indx) {
			case 1 : setClntnum01(what);
					 break;
			case 2 : setClntnum02(what);
					 break;
			case 3 : setClntnum03(what);
					 break;
			case 4 : setClntnum04(what);
					 break;
			case 5 : setClntnum05(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		jlife.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		currfrom.clear();
		currto.clear();
		height.clear();
		weight.clear();
		bmi.clear();
		bmibasis.clear();
		bmirule.clear();
		ovrrule.clear();
		clntnum01.clear();
		clntnum02.clear();
		clntnum03.clear();
		clntnum04.clear();
		clntnum05.clear();
		tranno.clear();
		undwflag.clear();
		validflag.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}