package com.csc.life.underwriting.dataaccess.model;

import java.util.Date;

public class Undqpf {
	private long unique_number;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private int currfrom;
	private int currto;
	private String questidf;
	private String answer;
	private String undwrule;
	private int tranno;
	private String validflag;
	private String usrprf;
	private String jobnm;
	private Date datime;
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getJlife() {
		return jlife;
	}
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	public int getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(int currfrom) {
		this.currfrom = currfrom;
	}
	public int getCurrto() {
		return currto;
	}
	public void setCurrto(int currto) {
		this.currto = currto;
	}
	public String getQuestidf() {
		return questidf;
	}
	public void setQuestidf(String questidf) {
		this.questidf = questidf;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getUndwrule() {
		return undwrule;
	}
	public void setUndwrule(String undwrule) {
		this.undwrule = undwrule;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Date getDatime() {
		return new Date(datime.getTime());//IJTI-316
	}
	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());//IJTI-314
	}

}
