package com.csc.life.underwriting.dataaccess.dao;

import java.util.List;
import com.csc.life.underwriting.dataaccess.model.Undqpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface UndqpfDAO extends BaseDAO<Undqpf> {
	public boolean  isExistundqpfByCoyAndNumAndLifeAndJLife(String chdrcoy, String chdrnum, String life, String jlife);
	public List<Undqpf> searchUndqpfRecord(String chdrcoy, String chdrnum); //ILIFE-8709
	public void updateUndqTranno(List<Undqpf> undqpfList); //ILIFE-8709
	int deleteUndqRecord(String chdrcoy, String chdrnum, String life, String jlife);
	public List<Undqpf>  searchUnqpf(String chdrcoy, String chdrnum, String life, String jlife);
	public List<Undqpf> searchUndqRecord(String chdrcoy, String chdrnum,String life, String jlife);
	public void deleteUndqpfRecords(List<Undqpf> undqpfDeleteList);
	public boolean updateUndqpfRecords(List<Undqpf> undqpfDeleteList);
	public boolean insertundqpf(List<Undqpf> undqpfList);
}
