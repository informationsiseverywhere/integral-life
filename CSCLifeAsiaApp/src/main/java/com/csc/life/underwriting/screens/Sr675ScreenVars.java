package com.csc.life.underwriting.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR675
 * @version 1.0 generated on 30/08/09 07:22
 * @author Quipoz
 */
public class Sr675ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData ages = new FixedLengthStringData(18).isAPartOf(dataFields, 0);
	public ZonedDecimalData[] age = ZDArrayPartOfStructure(6, 3, 0, ages, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(18).isAPartOf(ages, 0, FILLER_REDEFINE);
	public ZonedDecimalData age01 = DD.age.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData age02 = DD.age.copyToZonedDecimal().isAPartOf(filler,3);
	public ZonedDecimalData age03 = DD.age.copyToZonedDecimal().isAPartOf(filler,6);
	public ZonedDecimalData age04 = DD.age.copyToZonedDecimal().isAPartOf(filler,9);
	public ZonedDecimalData age05 = DD.age.copyToZonedDecimal().isAPartOf(filler,12);
	public ZonedDecimalData age06 = DD.age.copyToZonedDecimal().isAPartOf(filler,15);
	public FixedLengthStringData bmibasis = DD.bmibasis.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,23);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,24);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,32);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,40);
	public FixedLengthStringData language = DD.language.copy().isAPartOf(dataFields,48);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData questsets = new FixedLengthStringData(96).isAPartOf(dataFields, 79);
	public FixedLengthStringData[] questset = FLSArrayPartOfStructure(12, 8, questsets, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(96).isAPartOf(questsets, 0, FILLER_REDEFINE);
	public FixedLengthStringData questset01 = DD.questset.copy().isAPartOf(filler1,0);
	public FixedLengthStringData questset02 = DD.questset.copy().isAPartOf(filler1,8);
	public FixedLengthStringData questset03 = DD.questset.copy().isAPartOf(filler1,16);
	public FixedLengthStringData questset04 = DD.questset.copy().isAPartOf(filler1,24);
	public FixedLengthStringData questset05 = DD.questset.copy().isAPartOf(filler1,32);
	public FixedLengthStringData questset06 = DD.questset.copy().isAPartOf(filler1,40);
	public FixedLengthStringData questset07 = DD.questset.copy().isAPartOf(filler1,48);
	public FixedLengthStringData questset08 = DD.questset.copy().isAPartOf(filler1,56);
	public FixedLengthStringData questset09 = DD.questset.copy().isAPartOf(filler1,64);
	public FixedLengthStringData questset10 = DD.questset.copy().isAPartOf(filler1,72);
	public FixedLengthStringData questset11 = DD.questset.copy().isAPartOf(filler1,80);
	public FixedLengthStringData questset12 = DD.questset.copy().isAPartOf(filler1,88);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,175);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData agesErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] ageErr = FLSArrayPartOfStructure(6, 4, agesErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(agesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData age01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData age02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData age03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData age04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData age05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData age06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData bmibasisErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData languageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData questsetsErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData[] questsetErr = FLSArrayPartOfStructure(12, 4, questsetsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(48).isAPartOf(questsetsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData questset01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData questset02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData questset03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData questset04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData questset05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData questset06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData questset07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData questset08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData questset09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData questset10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData questset11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData questset12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize() + getErrorIndicatorSize());
	public FixedLengthStringData agesOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] ageOut = FLSArrayPartOfStructure(6, 12, agesOut, 0);
	public FixedLengthStringData[][] ageO = FLSDArrayPartOfArrayStructure(12, 1, ageOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(72).isAPartOf(agesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] age01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] age02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] age03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] age04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] age05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] age06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] bmibasisOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] languageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData questsetsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 156);
	public FixedLengthStringData[] questsetOut = FLSArrayPartOfStructure(12, 12, questsetsOut, 0);
	public FixedLengthStringData[][] questsetO = FLSDArrayPartOfArrayStructure(12, 1, questsetOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(144).isAPartOf(questsetsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] questset01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] questset02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] questset03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] questset04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] questset05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] questset06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] questset07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] questset08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] questset09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] questset10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] questset11Out = FLSArrayPartOfStructure(12, 1, filler5, 120);
	public FixedLengthStringData[] questset12Out = FLSArrayPartOfStructure(12, 1, filler5, 132);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sr675screenWritten = new LongData(0);
	public LongData Sr675protectWritten = new LongData(0);
	
	public static int[] screenPfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3};
	
	public boolean hasSubfile() {
		return false;
	}


	public Sr675ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr675screen.class;
		protectRecord = Sr675protect.class;
	}
	
	
	public int getDataAreaSize()
	{
		return 596 ;
	}
	

	public int getDataFieldsSize()
	{
		return 180 ;
	}
	

	public int getErrorIndicatorSize()
	{
		return 104;
	}
	
	public int getOutputFieldSize()
	{
		return 312;
	}
	

	public BaseData[] getscreenFields()
	{
		return new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, age01, questset01, questset02, age02, questset03, questset04, age03, questset05, questset06, age04, questset07, questset08, age05, questset09, questset10, age06, questset11, questset12, language, bmibasis};
	}
	
	public BaseData[][] getscreenOutFields()
	{
		return new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, age01Out, questset01Out, questset02Out, age02Out, questset03Out, questset04Out, age03Out, questset05Out, questset06Out, age04Out, questset07Out, questset08Out, age05Out, questset09Out, questset10Out, age06Out, questset11Out, questset12Out, languageOut, bmibasisOut};
	}
	
	public BaseData[] getscreenErrFields()
	{
		return new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, age01Err, questset01Err, questset02Err, age02Err, questset03Err, questset04Err, age03Err, questset05Err, questset06Err, age04Err, questset07Err, questset08Err, age05Err, questset09Err, questset10Err, age06Err, questset11Err, questset12Err, languageErr, bmibasisErr};
	}
	

	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {itmfrm, itmto};
	}
	
	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {itmfrmDisp, itmtoDisp};
	}
	
	public BaseData[] getscreenDateErrFields()
	{
		return  new BaseData[] {itmfrmErr, itmtoErr};
	}
	
	public static int[] getScreenPfInds()
	{
		return screenPfInds;
	}
	
	
}
