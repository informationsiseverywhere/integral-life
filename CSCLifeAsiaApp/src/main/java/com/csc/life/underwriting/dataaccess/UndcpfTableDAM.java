package com.csc.life.underwriting.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UndcpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:44
 * Class transformed from UNDCPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UndcpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 82;
	public FixedLengthStringData undcrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData undcpfRecord = undcrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(undcrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(undcrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(undcrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(undcrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(undcrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(undcrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(undcrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(undcrec);
	public FixedLengthStringData undwrule = DD.undwrule.copy().isAPartOf(undcrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(undcrec);
	public FixedLengthStringData specind = DD.specind.copy().isAPartOf(undcrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(undcrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(undcrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(undcrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(undcrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UndcpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UndcpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UndcpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UndcpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UndcpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UndcpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UndcpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("UNDCPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"CURRFROM, " +
							"CURRTO, " +
							"UNDWRULE, " +
							"TRANNO, " +
							"SPECIND, " +
							"VALIDFLAG, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     currfrom,
                                     currto,
                                     undwrule,
                                     tranno,
                                     specind,
                                     validflag,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		currfrom.clear();
  		currto.clear();
  		undwrule.clear();
  		tranno.clear();
  		specind.clear();
  		validflag.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUndcrec() {
  		return undcrec;
	}

	public FixedLengthStringData getUndcpfRecord() {
  		return undcpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUndcrec(what);
	}

	public void setUndcrec(Object what) {
  		this.undcrec.set(what);
	}

	public void setUndcpfRecord(Object what) {
  		this.undcpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(undcrec.getLength());
		result.set(undcrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}