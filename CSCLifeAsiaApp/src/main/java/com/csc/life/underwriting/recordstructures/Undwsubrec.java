package com.csc.life.underwriting.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:35
 * Description:
 * Copybook name: UNDWSUBREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Undwsubrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData undwsubRec = new FixedLengthStringData(77);
  	public FixedLengthStringData function = new FixedLengthStringData(4).isAPartOf(undwsubRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(undwsubRec, 4);
  	public FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(undwsubRec, 8);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(undwsubRec, 10);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(undwsubRec, 11);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(undwsubRec, 12);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(undwsubRec, 20);
  	public FixedLengthStringData jlife = new FixedLengthStringData(2).isAPartOf(undwsubRec, 22);
  	public FixedLengthStringData clntcoy = new FixedLengthStringData(1).isAPartOf(undwsubRec, 24);
  	public FixedLengthStringData clntnum = new FixedLengthStringData(8).isAPartOf(undwsubRec, 25);
  	public FixedLengthStringData doctor = new FixedLengthStringData(8).isAPartOf(undwsubRec, 33);
  	public FixedLengthStringData bmirule = new FixedLengthStringData(4).isAPartOf(undwsubRec, 41);
  	public FixedLengthStringData ovrrule = new FixedLengthStringData(4).isAPartOf(undwsubRec, 45);
  	public FixedLengthStringData currency = new FixedLengthStringData(3).isAPartOf(undwsubRec, 49);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(undwsubRec, 52);
  	public FixedLengthStringData errorCode = new FixedLengthStringData(4).isAPartOf(undwsubRec, 56);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(undwsubRec, 60);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(undwsubRec, 65);
  	public ZonedDecimalData user = new ZonedDecimalData(6, 0).isAPartOf(undwsubRec, 68).setUnsigned();
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(undwsubRec, 74);

	public void initialize() {
		COBOLFunctions.initialize(undwsubRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		undwsubRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}