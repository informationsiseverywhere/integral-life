package com.csc.life.underwriting.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class S6767screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static int maxRecords = 14;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 21, 2, 76}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6767ScreenVars sv = (S6767ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s6767screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s6767screensfl, 
			sv.S6767screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S6767ScreenVars sv = (S6767ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s6767screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S6767ScreenVars sv = (S6767ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s6767screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S6767screensflWritten.gt(0))
		{
			sv.s6767screensfl.setCurrentIndex(0);
			sv.S6767screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S6767ScreenVars sv = (S6767ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s6767screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6767ScreenVars screenVars = (S6767ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.language.setFieldName("language");
				screenVars.questidf.setFieldName("questidf");
				screenVars.questtyp.setFieldName("questtyp");
				screenVars.question.setFieldName("question");
				screenVars.answer.setFieldName("answer");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.language.set(dm.getField("language"));
			screenVars.questidf.set(dm.getField("questidf"));
			screenVars.questtyp.set(dm.getField("questtyp"));
			screenVars.question.set(dm.getField("question"));
			screenVars.answer.set(dm.getField("answer"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6767ScreenVars screenVars = (S6767ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.language.setFieldName("language");
				screenVars.questidf.setFieldName("questidf");
				screenVars.questtyp.setFieldName("questtyp");
				screenVars.question.setFieldName("question");
				screenVars.answer.setFieldName("answer");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("language").set(screenVars.language);
			dm.getField("questidf").set(screenVars.questidf);
			dm.getField("questtyp").set(screenVars.questtyp);
			dm.getField("question").set(screenVars.question);
			dm.getField("answer").set(screenVars.answer);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S6767screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S6767ScreenVars screenVars = (S6767ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.language.clearFormatting();
		screenVars.questidf.clearFormatting();
		screenVars.questtyp.clearFormatting();
		screenVars.question.clearFormatting();
		screenVars.answer.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S6767ScreenVars screenVars = (S6767ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.language.setClassString("");
		screenVars.questidf.setClassString("");
		screenVars.questtyp.setClassString("");
		screenVars.question.setClassString("");
		screenVars.answer.setClassString("");
	}

/**
 * Clear all the variables in S6767screensfl
 */
	public static void clear(VarModel pv) {
		S6767ScreenVars screenVars = (S6767ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.language.clear();
		screenVars.questidf.clear();
		screenVars.questtyp.clear();
		screenVars.question.clear();
		screenVars.answer.clear();
	}
}
