/*
 * File: Prpsprm.java
 * Date: 30 August 2009 1:59:31
 * Author: Quipoz Limited
 *
 * Class transformed from PRPSPRM.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.power;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.newbusiness.dataaccess.CovrtrbTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* PRPSPRM - Surrender value of a SP decreasing term Assurance
*
*   The program can calculate the value for either a single
*   component or for the whole-plan.
*   This is established by examination of SURC-PLAN-SUFFIX.
*   If this is zero then process the whole-plan otherwise,
*   process the single policy for the value passed.
*   For single component processing the policy may be broken
*   out or it may be NOTIONAL ie. part of the summarised record.
*   The program establishes this before reading the coverage
*   file.
*
*   A figure will be calculated for either the single
*   policy requested or for the whole plan by accumulation
*   of the figures for all the policy records pertaining to
*   this COVERAGE/RIDER (including the summary record).
*
* This  program  is  an  item  entry  on  T6598,  the  surrender
* claim subroutine:-
*
* PROCESSING.
* ----------
*   This is the routine to use for calculating the surrender
* value of a single premium decreasing term assurance, where
* the value is simply the proportionate refund of the original
* single premium, based on the curtate unexpired duration.
*
* For Whole-plan processing, figures are accumulated across
* all policies pertinent to this COVERAGE/RIDER and the routine
* loops round the coverage file until a new COVR/RID is read.
*
* For notional policies, the figure from the summary record
* is returned and this will be dealt with in the calling program
* by dividing by the number of policies summarised.
*
*                                / Curtate unexpired term \  2
* The calculation is       SP * |  ----------------------  |
*                                \     Original term      /
*
* Curtate unexpired term is calculated as:
*   Premium Cessation Date less Effective Date, rounded down
* to lower integer number of years.
*
* e.g. Prem-Cess-Date          01.01.2000
*      Orig-Comm-Date          01.01.1990
*      Effective Date          01.02.1992
*
*      Curtate unexpired term = 20000101 - 19920201
*                             = 7 years 11 months
*                             = 7 years
*
* Proportionate value         = SP x 49/100 (10 year term)
*
*****************************************************************
* </pre>
*/
public class Prpsprm extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
		/* ERRORS */
	private String h034 = "H034";
	private String g344 = "G344";
		/* TABLES */
	private String t5687 = "T5687";
	private String wsaaSubr = "PRPSPRM";
	private PackedDecimalData wsaaSurrenderVal = new PackedDecimalData(18, 9);
	private PackedDecimalData wsaaReserveTot = new PackedDecimalData(18, 3);

	private FixedLengthStringData wsaaProcessingType = new FixedLengthStringData(1);
	private Validator wholePlan = new Validator(wsaaProcessingType, "1");
	private Validator singleComponent = new Validator(wsaaProcessingType, "2");
	private Validator notionalSingleComponent = new Validator(wsaaProcessingType, "3");
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData wsaaOriginalTerm = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaUnexpiredTerm = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
		/*COVR layout for trad. reversionary bonus*/
	private CovrtrbTableDAM covrtrbIO = new CovrtrbTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit9020
	}

	public Prpsprm() {
		super();
	}

public void mainline(Object... parmArray)
	{
		srcalcpy.surrenderRec = convertAndSetParam(srcalcpy.surrenderRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
		/*START*/
		srcalcpy.status.set("****");
		syserrrec.subrname.set(wsaaSubr);
		wsaaReserveTot.set(ZERO);
		if (isEQ(srcalcpy.planSuffix,ZERO)) {
			wsaaProcessingType.set("1");
		}
		else {
			if (isLTE(srcalcpy.planSuffix,srcalcpy.polsum)) {
				wsaaProcessingType.set("3");
			}
			else {
				wsaaProcessingType.set("2");
			}
		}
		readCovrtrb100();
		getDescription200();
		while ( !(isEQ(covrtrbIO.getStatuz(),varcom.endp))) {
			getOriginalTerm300();
			getUnexpiredTerm400();
			setActualValue500();
			if (singleComponent.isTrue()
			|| notionalSingleComponent.isTrue()) {
				covrtrbIO.setStatuz(varcom.endp);
			}
			else {
				readCovr600();
			}
		}

		setExitValues700();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readCovrtrb100()
	{
		start100();
	}

protected void start100()
	{
		covrtrbIO.setDataArea(SPACES);
		covrtrbIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		covrtrbIO.setChdrnum(srcalcpy.chdrChdrnum);
		covrtrbIO.setLife(srcalcpy.lifeLife);
		covrtrbIO.setCoverage(srcalcpy.covrCoverage);
		covrtrbIO.setRider(srcalcpy.covrRider);
		covrtrbIO.setPlanSuffix(srcalcpy.planSuffix);
		if (notionalSingleComponent.isTrue()) {
			covrtrbIO.setPlanSuffix(0);
		}
		covrtrbIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, covrtrbIO);
		if (isNE(covrtrbIO.getStatuz(),"****")
		&& isNE(covrtrbIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(covrtrbIO.getParams());
			fatalError9000();
		}
		if (isNE(covrtrbIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(covrtrbIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(covrtrbIO.getLife(),srcalcpy.lifeLife)
		|| isNE(covrtrbIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(covrtrbIO.getRider(),srcalcpy.covrRider)
		|| isEQ(covrtrbIO.getStatuz(),varcom.endp)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(srcalcpy.chdrChdrcoy.toString());
			stringVariable1.append(srcalcpy.chdrChdrnum.toString());
			stringVariable1.append(srcalcpy.lifeLife.toString());
			stringVariable1.append(srcalcpy.covrCoverage.toString());
			stringVariable1.append(srcalcpy.covrRider.toString());
			syserrrec.params.setLeft(stringVariable1.toString());
			syserrrec.statuz.set(g344);
			fatalError9000();
		}
		if ((singleComponent.isTrue()
		&& isNE(covrtrbIO.getPlanSuffix(),srcalcpy.planSuffix))
		|| (notionalSingleComponent.isTrue()
		&& isNE(covrtrbIO.getPlanSuffix(),0))) {
			wsaaPlan.set(srcalcpy.planSuffix);
			StringBuilder stringVariable2 = new StringBuilder();
			stringVariable2.append(srcalcpy.chdrChdrcoy.toString());
			stringVariable2.append(srcalcpy.chdrChdrnum.toString());
			stringVariable2.append(srcalcpy.lifeLife.toString());
			stringVariable2.append(srcalcpy.covrCoverage.toString());
			stringVariable2.append(srcalcpy.covrRider.toString());
			stringVariable2.append(wsaaPlan.toString());
			syserrrec.params.setLeft(stringVariable2.toString());
			syserrrec.statuz.set(g344);
			fatalError9000();
		}
	}

protected void getDescription200()
	{
		start200();
	}

protected void start200()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(srcalcpy.chdrChdrcoy);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(srcalcpy.crtable);
		descIO.setLanguage(srcalcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(h034);
			fatalError9000();
		}
		if (isEQ(descIO.getShortdesc(),SPACES)) {
			srcalcpy.description.fill("?");
		}
		else {
			srcalcpy.description.set(descIO.getShortdesc());
		}
	}

protected void getOriginalTerm300()
	{
		/*START*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrtrbIO.getCrrcd());
		datcon3rec.intDate2.set(covrtrbIO.getPremCessDate());
		datcon3rec.function.set(SPACES);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,"****")) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError9000();
		}
		wsaaOriginalTerm.set(datcon3rec.freqFactor);
		/*EXIT*/
	}

protected void getUnexpiredTerm400()
	{
		/*START*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(srcalcpy.effdate);
		datcon3rec.intDate2.set(covrtrbIO.getPremCessDate());
		datcon3rec.function.set(SPACES);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,"****")) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError9000();
		}
		wsaaUnexpiredTerm.set(datcon3rec.freqFactor);
		/*EXIT*/
	}

protected void setActualValue500()
	{
		/*START*/
		compute(wsaaSurrenderVal, 9).set(mult(covrtrbIO.getSingp(),(power((div(wsaaUnexpiredTerm,wsaaOriginalTerm)),2))));
		wsaaReserveTot.add(wsaaSurrenderVal);
		/*EXIT*/
	}

protected void readCovr600()
	{
		/*START*/
		covrtrbIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrtrbIO);
		if (isNE(covrtrbIO.getStatuz(),varcom.oK)
		&& isNE(covrtrbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrtrbIO.getParams());
			syserrrec.statuz.set(covrtrbIO.getStatuz());
			fatalError9000();
		}
		if (isNE(covrtrbIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(covrtrbIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(covrtrbIO.getLife(),srcalcpy.lifeLife)
		|| isNE(covrtrbIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(covrtrbIO.getRider(),srcalcpy.covrRider)) {
			covrtrbIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void setExitValues700()
	{
		/*START*/
		compute(srcalcpy.actualVal, 4).setRounded(mult(wsaaReserveTot,1));
		srcalcpy.estimatedVal.set(0);
		srcalcpy.status.set(varcom.endp);
		srcalcpy.type.set("S");
		/*EXIT*/
		exitProgram();
	}

protected void fatalError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error9010();
				}
				case exit9020: {
					exit9020();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit9020);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		srcalcpy.status.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
