package com.csc.life.underwriting.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:34
 * Description:
 * Copybook name: UNDLAFIKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Undlafikey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData undlafiFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData undlafiKey = new FixedLengthStringData(256).isAPartOf(undlafiFileKey, 0, REDEFINE);
  	public FixedLengthStringData undlafiChdrcoy = new FixedLengthStringData(1).isAPartOf(undlafiKey, 0);
  	public FixedLengthStringData undlafiChdrnum = new FixedLengthStringData(8).isAPartOf(undlafiKey, 1);
  	public FixedLengthStringData undlafiLife = new FixedLengthStringData(2).isAPartOf(undlafiKey, 9);
  	public FixedLengthStringData undlafiJlife = new FixedLengthStringData(2).isAPartOf(undlafiKey, 11);
  	public PackedDecimalData undlafiTranno = new PackedDecimalData(5, 0).isAPartOf(undlafiKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(240).isAPartOf(undlafiKey, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(undlafiFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		undlafiFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}