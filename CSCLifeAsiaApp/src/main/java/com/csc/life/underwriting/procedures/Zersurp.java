
package com.csc.life.underwriting.procedures;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* This subroutine does not perform any calculation.
*/

public class Zersurp extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "ZERSURP";
	private Syserrrec syserrrec = new Syserrrec();
	private Srcalcpy srcalcpy = new Srcalcpy();
	
	
public Zersurp() {
		super();
	}


public void mainline(Object... parmArray)
	{
		srcalcpy.surrenderRec = convertAndSetParam(srcalcpy.surrenderRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline1000()
	{
		/*MAIN*/
		  initialise2000();
		
		  exitProgram();
	}

protected void initialise2000()
	{
	syserrrec.subrname.set(wsaaSubr);
	
	srcalcpy.status.set("ENDP");
	}

}
