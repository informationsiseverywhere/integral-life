/*
 * File: P6774.java
 * Date: 30 August 2009 0:57:28
 * Author: Quipoz Limited
 * 
 * Class transformed from P6774.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.underwriting.dataaccess.UndlTableDAM;
import com.csc.life.underwriting.screens.S6774ScreenVars;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* Underwriting Confirmation Window
* --------------------------------
* This program will let the user Confim that the Underwriting
* is complete. If YES the UNDL record will be updated.
*
***********************************************************************
* </pre>
*/
public class P6774 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6774");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaUndlExists = new FixedLengthStringData(1).init("N");
	private Validator noUnderwriting = new Validator(wsaaUndlExists, "N");
		/* ERRORS */
	private String f136 = "F136";
	private String undlrec = "UNDLREC";
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
		/*Underwriting Life Details*/
	private UndlTableDAM undlIO = new UndlTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S6774ScreenVars sv = ScreenProgram.getScreenVars( S6774ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit, 
		exit2090, 
		exit3090
	}

	public P6774() {
		super();
		screenVars = sv;
		new ScreenModel("S6774", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		wsaaBatckey.batcFileKey.set(wsspcomn.batchkey);
		lifelnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifelnbIO.getLifcnum());
		undlIO.setParams(SPACES);
		undlIO.setChdrcoy(lifelnbIO.getChdrcoy());
		undlIO.setChdrnum(lifelnbIO.getChdrnum());
		undlIO.setLife(lifelnbIO.getLife());
		undlIO.setJlife(lifelnbIO.getJlife());
		undlIO.setFormat(undlrec);
		undlIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, undlIO);
		if (isNE(undlIO.getStatuz(),varcom.oK)
		&& isNE(undlIO.getStatuz(),varcom.mrnf)) {
			undlIO.setParams(syserrrec.params);
			undlIO.setStatuz(syserrrec.statuz);
			fatalError600();
		}
		if (isEQ(undlIO.getStatuz(),varcom.oK)) {
			sv.undwflag.set(undlIO.getUndwflag());
			wsaaUndlExists.set("Y");
		}
		else {
			wsaaUndlExists.set("N");
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (noUnderwriting.isTrue()) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.undwflag,"Y")
		&& isNE(sv.undwflag,"N")) {
			sv.undwflagErr.set(f136);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		try {
			checkRequired3010();
		}
		catch (GOTOException e){
		}
	}

protected void checkRequired3010()
	{
		if (noUnderwriting.isTrue()) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
		if (isNE(sv.undwflag,undlIO.getUndwflag())) {
			undlIO.setUndwflag(sv.undwflag);
			undlIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, undlIO);
			if (isNE(undlIO.getStatuz(),varcom.oK)) {
				undlIO.setParams(syserrrec.params);
				undlIO.setStatuz(syserrrec.statuz);
				fatalError600();
			}
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
