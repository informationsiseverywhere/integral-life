package com.csc.life.underwriting.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class S6769screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 1, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6769ScreenVars sv = (S6769ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6769screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6769ScreenVars screenVars = (S6769ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.bmiclass01.setClassString("");
		screenVars.bmiclass02.setClassString("");
		screenVars.bmiclass03.setClassString("");
		screenVars.bmiclass04.setClassString("");
		screenVars.bmiclass05.setClassString("");
		screenVars.bmiclass06.setClassString("");
		screenVars.bmiclass07.setClassString("");
		screenVars.bmiclass08.setClassString("");
		screenVars.bmiclass09.setClassString("");
		screenVars.bmiclass10.setClassString("");
		screenVars.undwrule02.setClassString("");
		screenVars.undwrule03.setClassString("");
		screenVars.undwrule04.setClassString("");
		screenVars.undwrule05.setClassString("");
		screenVars.undwrule06.setClassString("");
		screenVars.undwrule07.setClassString("");
		screenVars.undwrule08.setClassString("");
		screenVars.undwrule09.setClassString("");
		screenVars.undwrule10.setClassString("");
		screenVars.bmifact.setClassString("");
		screenVars.bmi01.setClassString("");
		screenVars.bmi02.setClassString("");
		screenVars.bmi03.setClassString("");
		screenVars.bmi04.setClassString("");
		screenVars.bmi05.setClassString("");
		screenVars.bmi06.setClassString("");
		screenVars.bmi07.setClassString("");
		screenVars.bmi08.setClassString("");
		screenVars.bmi09.setClassString("");
		screenVars.bmi10.setClassString("");
		screenVars.undwrule01.setClassString("");
	}

/**
 * Clear all the variables in S6769screen
 */
	public static void clear(VarModel pv) {
		S6769ScreenVars screenVars = (S6769ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.bmiclass01.clear();
		screenVars.bmiclass02.clear();
		screenVars.bmiclass03.clear();
		screenVars.bmiclass04.clear();
		screenVars.bmiclass05.clear();
		screenVars.bmiclass06.clear();
		screenVars.bmiclass07.clear();
		screenVars.bmiclass08.clear();
		screenVars.bmiclass09.clear();
		screenVars.bmiclass10.clear();
		screenVars.undwrule02.clear();
		screenVars.undwrule03.clear();
		screenVars.undwrule04.clear();
		screenVars.undwrule05.clear();
		screenVars.undwrule06.clear();
		screenVars.undwrule07.clear();
		screenVars.undwrule08.clear();
		screenVars.undwrule09.clear();
		screenVars.undwrule10.clear();
		screenVars.bmifact.clear();
		screenVars.bmi01.clear();
		screenVars.bmi02.clear();
		screenVars.bmi03.clear();
		screenVars.bmi04.clear();
		screenVars.bmi05.clear();
		screenVars.bmi06.clear();
		screenVars.bmi07.clear();
		screenVars.bmi08.clear();
		screenVars.bmi09.clear();
		screenVars.bmi10.clear();
		screenVars.undwrule01.clear();
	}
}
