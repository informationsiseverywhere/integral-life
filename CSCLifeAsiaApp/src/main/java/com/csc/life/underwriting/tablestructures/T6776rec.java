package com.csc.life.underwriting.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:33
 * Description:
 * Copybook name: T6776REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6776rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6776Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData boolansws = new FixedLengthStringData(2).isAPartOf(t6776Rec, 0);
  	public FixedLengthStringData[] boolansw = FLSArrayPartOfStructure(2, 1, boolansws, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(boolansws, 0, FILLER_REDEFINE);
  	public FixedLengthStringData boolansw01 = new FixedLengthStringData(1).isAPartOf(filler, 0);
  	public FixedLengthStringData boolansw02 = new FixedLengthStringData(1).isAPartOf(filler, 1);
  	public FixedLengthStringData intgflag = new FixedLengthStringData(1).isAPartOf(t6776Rec, 2);
  	public FixedLengthStringData rulechcks = new FixedLengthStringData(2).isAPartOf(t6776Rec, 3);
  	public FixedLengthStringData[] rulechck = FLSArrayPartOfStructure(2, 1, rulechcks, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(2).isAPartOf(rulechcks, 0, FILLER_REDEFINE);
  	public FixedLengthStringData rulechck01 = new FixedLengthStringData(1).isAPartOf(filler1, 0);
  	public FixedLengthStringData rulechck02 = new FixedLengthStringData(1).isAPartOf(filler1, 1);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(495).isAPartOf(t6776Rec, 5, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6776Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6776Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}