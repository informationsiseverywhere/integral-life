/*
 * File: T6772pt.java
 * Date: 30 August 2009 2:32:45
 * Author: Quipoz Limited
 * 
 * Class transformed from T6772PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.underwriting.tablestructures.T6772rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6772.
*
*
*
***********************************************************************
* </pre>
*/
public class T6772pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(47).isAPartOf(wsaaPrtLine001, 29, FILLER).init("Underwriting Rules                        S6772");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(74);
	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 0, FILLER).init("Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 9);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 10, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 20);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 25, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 33);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 44);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(45);
	private FixedLengthStringData filler7 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Valid from:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 15);
	private FixedLengthStringData filler8 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 25, FILLER).init(SPACES);
	private FixedLengthStringData filler9 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 30, FILLER).init("To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 35);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(16);
	private FixedLengthStringData filler10 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine004, 0, FILLER).init("  Rule Type :");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 15);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(18);
	private FixedLengthStringData filler11 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine005, 0, FILLER).init("  Seq. No.  :");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 15).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(14);
	private FixedLengthStringData filler12 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine006, 0, FILLER).init("  Follow Ups :");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(61);
	private FixedLengthStringData filler13 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 3);
	private FixedLengthStringData filler14 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 7, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 9);
	private FixedLengthStringData filler15 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 15);
	private FixedLengthStringData filler16 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 21);
	private FixedLengthStringData filler17 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 27);
	private FixedLengthStringData filler18 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 33);
	private FixedLengthStringData filler19 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 39);
	private FixedLengthStringData filler20 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 45);
	private FixedLengthStringData filler21 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 51);
	private FixedLengthStringData filler22 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 57);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(17);
	private FixedLengthStringData filler23 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine008, 0, FILLER).init("  Special Terms :");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(59);
	private FixedLengthStringData filler24 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 3);
	private FixedLengthStringData filler25 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 5, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 9);
	private FixedLengthStringData filler26 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 11, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 15);
	private FixedLengthStringData filler27 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 17, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 21);
	private FixedLengthStringData filler28 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 27);
	private FixedLengthStringData filler29 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 33);
	private FixedLengthStringData filler30 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 39);
	private FixedLengthStringData filler31 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 45);
	private FixedLengthStringData filler32 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 51);
	private FixedLengthStringData filler33 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 57);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6772rec t6772rec = new T6772rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6772pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6772rec.t6772Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo009.set(t6772rec.fupcdes01);
		fieldNo010.set(t6772rec.fupcdes02);
		fieldNo011.set(t6772rec.fupcdes03);
		fieldNo012.set(t6772rec.fupcdes04);
		fieldNo013.set(t6772rec.fupcdes05);
		fieldNo014.set(t6772rec.fupcdes06);
		fieldNo015.set(t6772rec.fupcdes07);
		fieldNo016.set(t6772rec.fupcdes08);
		fieldNo017.set(t6772rec.fupcdes09);
		fieldNo018.set(t6772rec.fupcdes10);
		fieldNo019.set(t6772rec.opcda01);
		fieldNo020.set(t6772rec.opcda02);
		fieldNo021.set(t6772rec.opcda03);
		fieldNo022.set(t6772rec.opcda04);
		fieldNo023.set(t6772rec.opcda05);
		fieldNo024.set(t6772rec.opcda06);
		fieldNo025.set(t6772rec.opcda07);
		fieldNo026.set(t6772rec.opcda08);
		fieldNo027.set(t6772rec.opcda09);
		fieldNo028.set(t6772rec.opcda10);
		fieldNo007.set(t6772rec.ruletype);
		fieldNo008.set(t6772rec.seqnumb);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
