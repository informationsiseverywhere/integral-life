/******************************************************************************
 * File Name 		: UndcpfDAO.java
 * Author			: nloganathan5
 * Creation Date	: 27 October 2016
 * Project			: Integral Life
 * Description		: The DAO Interface for UNDCPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.underwriting.dataaccess.dao;

import java.util.List;

import com.csc.life.underwriting.dataaccess.model.Undcpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface UndcpfDAO extends BaseDAO<Undcpf> {

	public List<Undcpf> searchUndcpfRecord(String chdrcoy, String chdrnum) throws SQLRuntimeException;
	public void updateUndcTranno(List<Undcpf> undcpfList); //ILIFE-8709
	public List<Undcpf> getUndcpfRecord(String chdrcoy, String chdrnum, String life) throws SQLRuntimeException;
}
