/*
 * File: T6771pt.java
 * Date: 30 August 2009 2:32:25
 * Author: Quipoz Limited
 * 
 * Class transformed from T6771PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.underwriting.tablestructures.T6771rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6771.
*
*
*
***********************************************************************
* </pre>
*/
public class T6771pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(50).isAPartOf(wsaaPrtLine001, 26, FILLER).init("Underwriting Product Rules                   S6771");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(74);
	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 0, FILLER).init("Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 9);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 10, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 20);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 25, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 33);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 44);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(45);
	private FixedLengthStringData filler7 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Valid from:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 15);
	private FixedLengthStringData filler8 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 25, FILLER).init(SPACES);
	private FixedLengthStringData filler9 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 30, FILLER).init("To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 35);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(67);
	private FixedLengthStringData filler10 = new FixedLengthStringData(67).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Question Stat  Question Stat     Question Stat     Question Stat");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(66);
	private FixedLengthStringData filler11 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 3);
	private FixedLengthStringData filler12 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 12);
	private FixedLengthStringData filler13 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 20);
	private FixedLengthStringData filler14 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 29);
	private FixedLengthStringData filler15 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine005, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 38);
	private FixedLengthStringData filler16 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 47);
	private FixedLengthStringData filler17 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine005, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 56);
	private FixedLengthStringData filler18 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 65);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(66);
	private FixedLengthStringData filler19 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 3);
	private FixedLengthStringData filler20 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 12);
	private FixedLengthStringData filler21 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 20);
	private FixedLengthStringData filler22 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 29);
	private FixedLengthStringData filler23 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 38);
	private FixedLengthStringData filler24 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 47);
	private FixedLengthStringData filler25 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 56);
	private FixedLengthStringData filler26 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 65);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(66);
	private FixedLengthStringData filler27 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 3);
	private FixedLengthStringData filler28 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 12);
	private FixedLengthStringData filler29 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 20);
	private FixedLengthStringData filler30 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 29);
	private FixedLengthStringData filler31 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 38);
	private FixedLengthStringData filler32 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 47);
	private FixedLengthStringData filler33 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 56);
	private FixedLengthStringData filler34 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 65);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(66);
	private FixedLengthStringData filler35 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 3);
	private FixedLengthStringData filler36 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 12);
	private FixedLengthStringData filler37 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine008, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 20);
	private FixedLengthStringData filler38 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 29);
	private FixedLengthStringData filler39 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 38);
	private FixedLengthStringData filler40 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 47);
	private FixedLengthStringData filler41 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 56);
	private FixedLengthStringData filler42 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 65);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(66);
	private FixedLengthStringData filler43 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 3);
	private FixedLengthStringData filler44 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 12);
	private FixedLengthStringData filler45 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine009, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 20);
	private FixedLengthStringData filler46 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 29);
	private FixedLengthStringData filler47 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 38);
	private FixedLengthStringData filler48 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 47);
	private FixedLengthStringData filler49 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 56);
	private FixedLengthStringData filler50 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 65);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(66);
	private FixedLengthStringData filler51 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 3);
	private FixedLengthStringData filler52 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 12);
	private FixedLengthStringData filler53 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine010, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 20);
	private FixedLengthStringData filler54 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 29);
	private FixedLengthStringData filler55 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 38);
	private FixedLengthStringData filler56 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 47);
	private FixedLengthStringData filler57 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 56);
	private FixedLengthStringData filler58 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo054 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 65);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(66);
	private FixedLengthStringData filler59 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 3);
	private FixedLengthStringData filler60 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 12);
	private FixedLengthStringData filler61 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine011, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 20);
	private FixedLengthStringData filler62 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 29);
	private FixedLengthStringData filler63 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo059 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 38);
	private FixedLengthStringData filler64 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo060 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 47);
	private FixedLengthStringData filler65 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo061 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 56);
	private FixedLengthStringData filler66 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo062 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 65);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(66);
	private FixedLengthStringData filler67 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo063 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 3);
	private FixedLengthStringData filler68 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo064 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 12);
	private FixedLengthStringData filler69 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine012, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo065 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 20);
	private FixedLengthStringData filler70 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo066 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 29);
	private FixedLengthStringData filler71 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo067 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 38);
	private FixedLengthStringData filler72 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo068 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 47);
	private FixedLengthStringData filler73 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo069 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 56);
	private FixedLengthStringData filler74 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo070 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 65);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(66);
	private FixedLengthStringData filler75 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo071 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 3);
	private FixedLengthStringData filler76 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo072 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 12);
	private FixedLengthStringData filler77 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine013, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo073 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 20);
	private FixedLengthStringData filler78 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo074 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 29);
	private FixedLengthStringData filler79 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine013, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo075 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 38);
	private FixedLengthStringData filler80 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo076 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 47);
	private FixedLengthStringData filler81 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine013, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo077 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 56);
	private FixedLengthStringData filler82 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo078 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 65);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(66);
	private FixedLengthStringData filler83 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo079 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 3);
	private FixedLengthStringData filler84 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 8, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo080 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 12);
	private FixedLengthStringData filler85 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine014, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo081 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 20);
	private FixedLengthStringData filler86 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo082 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 29);
	private FixedLengthStringData filler87 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo083 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 38);
	private FixedLengthStringData filler88 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo084 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 47);
	private FixedLengthStringData filler89 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo085 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 56);
	private FixedLengthStringData filler90 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo086 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 65);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(38);
	private FixedLengthStringData filler91 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine015, 0, FILLER).init("   Underwriting subroutine:");
	private FixedLengthStringData fieldNo087 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine015, 28);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6771rec t6771rec = new T6771rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6771pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6771rec.t6771Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t6771rec.questidf01);
		fieldNo015.set(t6771rec.questidf05);
		fieldNo023.set(t6771rec.questidf09);
		fieldNo031.set(t6771rec.questidf13);
		fieldNo039.set(t6771rec.questidf17);
		fieldNo047.set(t6771rec.questidf21);
		fieldNo055.set(t6771rec.questidf25);
		fieldNo063.set(t6771rec.questidf29);
		fieldNo071.set(t6771rec.questidf33);
		fieldNo079.set(t6771rec.questidf37);
		fieldNo087.set(t6771rec.undwsubr);
		fieldNo008.set(t6771rec.questst01);
		fieldNo016.set(t6771rec.questst05);
		fieldNo024.set(t6771rec.questst09);
		fieldNo032.set(t6771rec.questst13);
		fieldNo040.set(t6771rec.questst17);
		fieldNo048.set(t6771rec.questst21);
		fieldNo056.set(t6771rec.questst25);
		fieldNo064.set(t6771rec.questst29);
		fieldNo072.set(t6771rec.questst33);
		fieldNo080.set(t6771rec.questst37);
		fieldNo009.set(t6771rec.questidf02);
		fieldNo017.set(t6771rec.questidf06);
		fieldNo025.set(t6771rec.questidf10);
		fieldNo033.set(t6771rec.questidf14);
		fieldNo041.set(t6771rec.questidf18);
		fieldNo049.set(t6771rec.questidf22);
		fieldNo057.set(t6771rec.questidf26);
		fieldNo065.set(t6771rec.questidf30);
		fieldNo073.set(t6771rec.questidf34);
		fieldNo081.set(t6771rec.questidf38);
		fieldNo011.set(t6771rec.questidf03);
		fieldNo019.set(t6771rec.questidf07);
		fieldNo027.set(t6771rec.questidf11);
		fieldNo035.set(t6771rec.questidf15);
		fieldNo043.set(t6771rec.questidf19);
		fieldNo051.set(t6771rec.questidf23);
		fieldNo059.set(t6771rec.questidf27);
		fieldNo067.set(t6771rec.questidf31);
		fieldNo075.set(t6771rec.questidf35);
		fieldNo083.set(t6771rec.questidf39);
		fieldNo013.set(t6771rec.questidf04);
		fieldNo021.set(t6771rec.questidf08);
		fieldNo029.set(t6771rec.questidf12);
		fieldNo037.set(t6771rec.questidf16);
		fieldNo045.set(t6771rec.questidf20);
		fieldNo053.set(t6771rec.questidf24);
		fieldNo061.set(t6771rec.questidf28);
		fieldNo069.set(t6771rec.questidf32);
		fieldNo077.set(t6771rec.questidf36);
		fieldNo085.set(t6771rec.questidf40);
		fieldNo010.set(t6771rec.questst02);
		fieldNo018.set(t6771rec.questst06);
		fieldNo026.set(t6771rec.questst10);
		fieldNo034.set(t6771rec.questst14);
		fieldNo042.set(t6771rec.questst18);
		fieldNo050.set(t6771rec.questst22);
		fieldNo058.set(t6771rec.questst26);
		fieldNo066.set(t6771rec.questst30);
		fieldNo074.set(t6771rec.questst34);
		fieldNo082.set(t6771rec.questst38);
		fieldNo012.set(t6771rec.questst03);
		fieldNo020.set(t6771rec.questst07);
		fieldNo028.set(t6771rec.questst11);
		fieldNo036.set(t6771rec.questst15);
		fieldNo044.set(t6771rec.questst19);
		fieldNo052.set(t6771rec.questst23);
		fieldNo060.set(t6771rec.questst27);
		fieldNo068.set(t6771rec.questst31);
		fieldNo076.set(t6771rec.questst35);
		fieldNo084.set(t6771rec.questst39);
		fieldNo014.set(t6771rec.questst04);
		fieldNo022.set(t6771rec.questst08);
		fieldNo030.set(t6771rec.questst12);
		fieldNo038.set(t6771rec.questst16);
		fieldNo046.set(t6771rec.questst20);
		fieldNo054.set(t6771rec.questst24);
		fieldNo062.set(t6771rec.questst28);
		fieldNo070.set(t6771rec.questst32);
		fieldNo078.set(t6771rec.questst36);
		fieldNo086.set(t6771rec.questst40);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
