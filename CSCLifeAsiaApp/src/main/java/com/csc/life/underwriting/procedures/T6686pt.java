/*
 * File: T6686pt.java
 * Date: 30 August 2009 2:29:49
 * Author: Quipoz Limited
 * 
 * Class transformed from T6686PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.underwriting.tablestructures.T6686rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6686.
*
*
*****************************************************************
* </pre>
*/
public class T6686pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6686rec t6686rec = new T6686rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6686pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6686rec.t6686Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		generalCopyLinesInner.fieldNo005.set(t6686rec.lxdp01);
		generalCopyLinesInner.fieldNo006.set(t6686rec.lxmort);
		generalCopyLinesInner.fieldNo007.set(t6686rec.lxmrt01);
		generalCopyLinesInner.fieldNo008.set(t6686rec.lxmrt02);
		generalCopyLinesInner.fieldNo009.set(t6686rec.lxmrt03);
		generalCopyLinesInner.fieldNo010.set(t6686rec.lxmrt04);
		generalCopyLinesInner.fieldNo011.set(t6686rec.lxmrt05);
		generalCopyLinesInner.fieldNo012.set(t6686rec.lxmrt06);
		generalCopyLinesInner.fieldNo014.set(t6686rec.lxmrt07);
		generalCopyLinesInner.fieldNo015.set(t6686rec.lxmrt08);
		generalCopyLinesInner.fieldNo016.set(t6686rec.lxmrt09);
		generalCopyLinesInner.fieldNo017.set(t6686rec.lxmrt10);
		generalCopyLinesInner.fieldNo018.set(t6686rec.lxmrt11);
		generalCopyLinesInner.fieldNo019.set(t6686rec.lxmrt12);
		generalCopyLinesInner.fieldNo020.set(t6686rec.lxmrt13);
		generalCopyLinesInner.fieldNo022.set(t6686rec.lxmrt14);
		generalCopyLinesInner.fieldNo023.set(t6686rec.lxmrt15);
		generalCopyLinesInner.fieldNo024.set(t6686rec.lxmrt16);
		generalCopyLinesInner.fieldNo025.set(t6686rec.lxmrt17);
		generalCopyLinesInner.fieldNo026.set(t6686rec.lxmrt18);
		generalCopyLinesInner.fieldNo027.set(t6686rec.lxmrt19);
		generalCopyLinesInner.fieldNo028.set(t6686rec.lxmrt20);
		generalCopyLinesInner.fieldNo030.set(t6686rec.lxmrt21);
		generalCopyLinesInner.fieldNo031.set(t6686rec.lxmrt22);
		generalCopyLinesInner.fieldNo032.set(t6686rec.lxmrt23);
		generalCopyLinesInner.fieldNo033.set(t6686rec.lxmrt24);
		generalCopyLinesInner.fieldNo034.set(t6686rec.lxmrt25);
		generalCopyLinesInner.fieldNo035.set(t6686rec.lxmrt26);
		generalCopyLinesInner.fieldNo036.set(t6686rec.lxmrt27);
		generalCopyLinesInner.fieldNo038.set(t6686rec.lxmrt28);
		generalCopyLinesInner.fieldNo039.set(t6686rec.lxmrt29);
		generalCopyLinesInner.fieldNo040.set(t6686rec.lxmrt30);
		generalCopyLinesInner.fieldNo041.set(t6686rec.lxmrt31);
		generalCopyLinesInner.fieldNo042.set(t6686rec.lxmrt32);
		generalCopyLinesInner.fieldNo043.set(t6686rec.lxmrt33);
		generalCopyLinesInner.fieldNo044.set(t6686rec.lxmrt34);
		generalCopyLinesInner.fieldNo046.set(t6686rec.lxmrt35);
		generalCopyLinesInner.fieldNo047.set(t6686rec.lxmrt36);
		generalCopyLinesInner.fieldNo048.set(t6686rec.lxmrt37);
		generalCopyLinesInner.fieldNo049.set(t6686rec.lxmrt38);
		generalCopyLinesInner.fieldNo050.set(t6686rec.lxmrt39);
		generalCopyLinesInner.fieldNo051.set(t6686rec.lxmrt40);
		generalCopyLinesInner.fieldNo052.set(t6686rec.lxmrt41);
		generalCopyLinesInner.fieldNo054.set(t6686rec.lxmrt42);
		generalCopyLinesInner.fieldNo055.set(t6686rec.lxmrt43);
		generalCopyLinesInner.fieldNo056.set(t6686rec.lxmrt44);
		generalCopyLinesInner.fieldNo057.set(t6686rec.lxmrt45);
		generalCopyLinesInner.fieldNo058.set(t6686rec.lxmrt46);
		generalCopyLinesInner.fieldNo059.set(t6686rec.lxmrt47);
		generalCopyLinesInner.fieldNo060.set(t6686rec.lxmrt48);
		generalCopyLinesInner.fieldNo062.set(t6686rec.lxmrt49);
		generalCopyLinesInner.fieldNo063.set(t6686rec.lxmrt50);
		generalCopyLinesInner.fieldNo064.set(t6686rec.lxmrt51);
		generalCopyLinesInner.fieldNo065.set(t6686rec.lxmrt52);
		generalCopyLinesInner.fieldNo066.set(t6686rec.lxmrt53);
		generalCopyLinesInner.fieldNo067.set(t6686rec.lxmrt54);
		generalCopyLinesInner.fieldNo068.set(t6686rec.lxmrt55);
		generalCopyLinesInner.fieldNo070.set(t6686rec.lxmrt56);
		generalCopyLinesInner.fieldNo071.set(t6686rec.lxmrt57);
		generalCopyLinesInner.fieldNo072.set(t6686rec.lxmrt58);
		generalCopyLinesInner.fieldNo073.set(t6686rec.lxmrt59);
		generalCopyLinesInner.fieldNo074.set(t6686rec.lxmrt60);
		generalCopyLinesInner.fieldNo075.set(t6686rec.lxmrt61);
		generalCopyLinesInner.fieldNo076.set(t6686rec.lxmrt62);
		generalCopyLinesInner.fieldNo078.set(t6686rec.lxmrt63);
		generalCopyLinesInner.fieldNo079.set(t6686rec.lxmrt64);
		generalCopyLinesInner.fieldNo080.set(t6686rec.lxmrt65);
		generalCopyLinesInner.fieldNo081.set(t6686rec.lxmrt66);
		generalCopyLinesInner.fieldNo082.set(t6686rec.lxmrt67);
		generalCopyLinesInner.fieldNo083.set(t6686rec.lxmrt68);
		generalCopyLinesInner.fieldNo084.set(t6686rec.lxmrt69);
		generalCopyLinesInner.fieldNo086.set(t6686rec.lxmrt70);
		generalCopyLinesInner.fieldNo087.set(t6686rec.lxmrt71);
		generalCopyLinesInner.fieldNo088.set(t6686rec.lxmrt72);
		generalCopyLinesInner.fieldNo089.set(t6686rec.lxmrt73);
		generalCopyLinesInner.fieldNo090.set(t6686rec.lxmrt74);
		generalCopyLinesInner.fieldNo091.set(t6686rec.lxmrt75);
		generalCopyLinesInner.fieldNo092.set(t6686rec.lxmrt76);
		generalCopyLinesInner.fieldNo094.set(t6686rec.lxmrt77);
		generalCopyLinesInner.fieldNo095.set(t6686rec.lxmrt78);
		generalCopyLinesInner.fieldNo096.set(t6686rec.lxmrt79);
		generalCopyLinesInner.fieldNo097.set(t6686rec.lxmrt80);
		generalCopyLinesInner.fieldNo098.set(t6686rec.lxmrt81);
		generalCopyLinesInner.fieldNo099.set(t6686rec.lxmrt82);
		generalCopyLinesInner.fieldNo100.set(t6686rec.lxmrt83);
		generalCopyLinesInner.fieldNo102.set(t6686rec.lxmrt84);
		generalCopyLinesInner.fieldNo103.set(t6686rec.lxmrt85);
		generalCopyLinesInner.fieldNo104.set(t6686rec.lxmrt86);
		generalCopyLinesInner.fieldNo105.set(t6686rec.lxmrt87);
		generalCopyLinesInner.fieldNo106.set(t6686rec.lxmrt88);
		generalCopyLinesInner.fieldNo107.set(t6686rec.lxmrt89);
		generalCopyLinesInner.fieldNo108.set(t6686rec.lxmrt90);
		generalCopyLinesInner.fieldNo110.set(t6686rec.lxmrt91);
		generalCopyLinesInner.fieldNo111.set(t6686rec.lxmrt92);
		generalCopyLinesInner.fieldNo112.set(t6686rec.lxmrt93);
		generalCopyLinesInner.fieldNo113.set(t6686rec.lxmrt94);
		generalCopyLinesInner.fieldNo114.set(t6686rec.lxmrt95);
		generalCopyLinesInner.fieldNo115.set(t6686rec.lxmrt96);
		generalCopyLinesInner.fieldNo116.set(t6686rec.lxmrt97);
		generalCopyLinesInner.fieldNo118.set(t6686rec.lxmrt98);
		generalCopyLinesInner.fieldNo119.set(t6686rec.lxmrt99);
		generalCopyLinesInner.fieldNo120.set(t6686rec.tlxmrt01);
		generalCopyLinesInner.fieldNo121.set(t6686rec.tlxmrt02);
		generalCopyLinesInner.fieldNo122.set(t6686rec.tlxmrt03);
		generalCopyLinesInner.fieldNo123.set(t6686rec.tlxmrt04);
		generalCopyLinesInner.fieldNo124.set(t6686rec.tlxmrt05);
		generalCopyLinesInner.fieldNo126.set(t6686rec.tlxmrt06);
		generalCopyLinesInner.fieldNo127.set(t6686rec.tlxmrt07);
		generalCopyLinesInner.fieldNo128.set(t6686rec.tlxmrt08);
		generalCopyLinesInner.fieldNo129.set(t6686rec.tlxmrt09);
		generalCopyLinesInner.fieldNo130.set(t6686rec.tlxmrt10);
		generalCopyLinesInner.fieldNo131.set(t6686rec.tlxmrt11);
		generalCopyLinesInner.fieldNo013.set(t6686rec.lxdp02);
		generalCopyLinesInner.fieldNo021.set(t6686rec.lxdp03);
		generalCopyLinesInner.fieldNo029.set(t6686rec.lxdp04);
		generalCopyLinesInner.fieldNo037.set(t6686rec.lxdp05);
		generalCopyLinesInner.fieldNo045.set(t6686rec.lxdp06);
		generalCopyLinesInner.fieldNo053.set(t6686rec.lxdp07);
		generalCopyLinesInner.fieldNo069.set(t6686rec.lxdp09);
		generalCopyLinesInner.fieldNo061.set(t6686rec.lxdp08);
		generalCopyLinesInner.fieldNo077.set(t6686rec.lxdp10);
		generalCopyLinesInner.fieldNo085.set(t6686rec.lxdp11);
		generalCopyLinesInner.fieldNo093.set(t6686rec.lxdp12);
		generalCopyLinesInner.fieldNo101.set(t6686rec.lxdp13);
		generalCopyLinesInner.fieldNo109.set(t6686rec.lxdp14);
		generalCopyLinesInner.fieldNo117.set(t6686rec.lxdp15);
		generalCopyLinesInner.fieldNo125.set(t6686rec.lxdp16);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine018);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine019);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(46).isAPartOf(wsaaPrtLine001, 30, FILLER).init("Mortality Table lx                       S6686");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(70);
	private FixedLengthStringData filler7 = new FixedLengthStringData(70).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  dp  Age:     0        1        2        3        4        5        6");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(74);
	private FixedLengthStringData filler8 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo005 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine004, 2).setPattern("Z");
	private FixedLengthStringData filler9 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine004, 3, FILLER).init("    0+");
	private ZonedDecimalData fieldNo006 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 12).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler10 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 21).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler11 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 30).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler12 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 39).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 48).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 57).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 66).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(74);
	private FixedLengthStringData filler16 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine005, 2).setPattern("Z");
	private FixedLengthStringData filler17 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine005, 3, FILLER).init("    7+");
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 12).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 21).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 30).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 39).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 48).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 57).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 66).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(74);
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine006, 2).setPattern("Z");
	private FixedLengthStringData filler25 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine006, 3, FILLER).init("   14+");
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 12).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 21).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 30).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 39).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 48).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 57).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 66).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(74);
	private FixedLengthStringData filler32 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine007, 2).setPattern("Z");
	private FixedLengthStringData filler33 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine007, 3, FILLER).init("   21+");
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 12).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 21).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 30).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 39).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 48).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 57).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 66).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(74);
	private FixedLengthStringData filler40 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine008, 2).setPattern("Z");
	private FixedLengthStringData filler41 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 3, FILLER).init("   28+");
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 12).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 21).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 30).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 39).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 48).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 57).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 66).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(74);
	private FixedLengthStringData filler48 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine009, 2).setPattern("Z");
	private FixedLengthStringData filler49 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 3, FILLER).init("   35+");
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 12).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 21).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 30).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 39).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 48).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 57).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 66).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(74);
	private FixedLengthStringData filler56 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine010, 2).setPattern("Z");
	private FixedLengthStringData filler57 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 3, FILLER).init("   42+");
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 12).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 21).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 30).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 39).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 48).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 57).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 66).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(74);
	private FixedLengthStringData filler64 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine011, 2).setPattern("Z");
	private FixedLengthStringData filler65 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine011, 3, FILLER).init("   49+");
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 12).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 21).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 30).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 39).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler69 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 48).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 57).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 66).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(74);
	private FixedLengthStringData filler72 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine012, 2).setPattern("Z");
	private FixedLengthStringData filler73 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine012, 3, FILLER).init("   56+");
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 12).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 21).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 30).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 39).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 48).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 57).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler79 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo076 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 66).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(74);
	private FixedLengthStringData filler80 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine013, 2).setPattern("Z");
	private FixedLengthStringData filler81 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine013, 3, FILLER).init("   63+");
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 12).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo079 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 21).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler83 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo080 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 30).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler84 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 39).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo082 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 48).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler86 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 57).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler87 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 66).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(74);
	private FixedLengthStringData filler88 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo085 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine014, 2).setPattern("Z");
	private FixedLengthStringData filler89 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine014, 3, FILLER).init("   70+");
	private ZonedDecimalData fieldNo086 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 12).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler90 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo087 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 21).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo088 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 30).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler92 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo089 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 39).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler93 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo090 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 48).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler94 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo091 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 57).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler95 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo092 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 66).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(74);
	private FixedLengthStringData filler96 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine015, 2).setPattern("Z");
	private FixedLengthStringData filler97 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine015, 3, FILLER).init("   77+");
	private ZonedDecimalData fieldNo094 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 12).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler98 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo095 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 21).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler99 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo096 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 30).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler100 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo097 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 39).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler101 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo098 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 48).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler102 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo099 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 57).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler103 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo100 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 66).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(74);
	private FixedLengthStringData filler104 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo101 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine016, 2).setPattern("Z");
	private FixedLengthStringData filler105 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine016, 3, FILLER).init("   84+");
	private ZonedDecimalData fieldNo102 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 12).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler106 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo103 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 21).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler107 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo104 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 30).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler108 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo105 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 39).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler109 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo106 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 48).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler110 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo107 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 57).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler111 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo108 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 66).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(74);
	private FixedLengthStringData filler112 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo109 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine017, 2).setPattern("Z");
	private FixedLengthStringData filler113 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine017, 3, FILLER).init("    91+");
	private ZonedDecimalData fieldNo110 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 12).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler114 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo111 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 21).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler115 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo112 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 30).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler116 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo113 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 39).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler117 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo114 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 48).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler118 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo115 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 57).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler119 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo116 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 66).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(74);
	private FixedLengthStringData filler120 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine018, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo117 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine018, 2).setPattern("Z");
	private FixedLengthStringData filler121 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine018, 3, FILLER).init("    98+");
	private ZonedDecimalData fieldNo118 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 12).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler122 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo119 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 21).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler123 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo120 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 30).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler124 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo121 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 39).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler125 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo122 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 48).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler126 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo123 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 57).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler127 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo124 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 66).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine019 = new FixedLengthStringData(65);
	private FixedLengthStringData filler128 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine019, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo125 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine019, 2).setPattern("Z");
	private FixedLengthStringData filler129 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine019, 3, FILLER).init("   105+");
	private ZonedDecimalData fieldNo126 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine019, 12).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler130 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine019, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo127 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine019, 21).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler131 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine019, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo128 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine019, 30).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler132 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine019, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo129 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine019, 39).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler133 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine019, 47, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo130 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine019, 48).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler134 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine019, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo131 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine019, 57).setPattern("ZZZZZZZZ");
	}
}
