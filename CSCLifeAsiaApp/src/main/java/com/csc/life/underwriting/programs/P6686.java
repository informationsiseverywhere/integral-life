/*
 * File: P6686.java
 * Date: 30 August 2009 0:53:01
 * Author: Quipoz Limited
 * 
 * Class transformed from P6686.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.underwriting.screens.S6686ScreenVars;
import com.csc.life.underwriting.tablestructures.T6686rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
* * * * * * * * * * * *  W A R N I N G  * * * * * * * * * * * *
*
* EXTRA DATA SCREEN CONTAINS MORE THAN 500 BYTES - ( MAX SIZE OF
* ITEM-GENAREA ). - THEREFORE FIELDS HAVE BEEN CHANGED TO COMP-4.
*
* THIS GENERATED PROGRAM HAS BEEN AMENDED TO MAKE INDIVIDUAL FIELD
* MOVES TO AVOID CONVERSION OF COMP-4 FIELDS WHICH WAS CAUSING
* DECIMAL DATA ERRORS.
*
*
* This table will hold the figures for the Mortality rates
* for people of various ages, between 11 and 109 years of
* age. All the fields have to be initialised to prevent any
* decimal data errors.
*  NOTE: ONLY RECOMPILE SCREEN AS *DSPF AND NOT *XDS!!!!!!!!!!
*
*****************************************************************
* </pre>
*/
public class P6686 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6686");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0);
	private static final String g581 = "G581";
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T6686rec t6686rec = new T6686rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6686ScreenVars sv = ScreenProgram.getScreenVars( S6686ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		other3080, 
		exit3090
	}

	public P6686() {
		super();
		screenVars = sv;
		new ScreenModel("S6686", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		readRecord1031();
		moveToScreen1040();
		generalArea1045();
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t6686rec.t6686Rec.set(ZERO);
		t6686rec.t6686Rec.set(itemIO.getGenarea());
		/*    IF THE EXTRA DATA AREA WAS NOT USED BEFORE THE NUMERIC*/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.*/
		if (isNE(itemIO.getGenarea(), SPACES)) {
			return ;
		}
		wsaaSub1.set(0);
		/*    PERFORM 1400-INIT-LXDP      15 TIMES.                        */
		for (int loopVar1 = 0; !(loopVar1 == 16); loopVar1 += 1){
			initLxdp1400();
		}
		wsaaSub1.set(0);
		for (int loopVar2 = 0; !(loopVar2 == 99); loopVar2 += 1){
			initLxmrt1500();
		}
		wsaaSub1.set(0);
		for (int loopVar3 = 0; !(loopVar3 == 11); loopVar3 += 1){
			initTlxmrt1800();
		}
		t6686rec.lxmort.set(ZERO);
	}

protected void generalArea1045()
	{
		wsaaSub1.set(0);
		/*    PERFORM 1600-MOVE-LXDP      15 TIMES.                        */
		for (int loopVar4 = 0; !(loopVar4 == 16); loopVar4 += 1){
			moveLxdp1600();
		}
		wsaaSub1.set(0);
		for (int loopVar5 = 0; !(loopVar5 == 99); loopVar5 += 1){
			moveLxmrt1700();
		}
		sv.lxmort.set(t6686rec.lxmort);
		wsaaSub1.set(0);
		for (int loopVar6 = 0; !(loopVar6 == 11); loopVar6 += 1){
			moveTlxmrt1900();
		}
		/*CONFIRMATION-FIELDS*/
		/*OTHER*/
		/*EXIT*/
	}

protected void initLxdp1400()
	{
		/*PARA*/
		wsaaSub1.add(1);
		t6686rec.lxdp[wsaaSub1.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void initLxmrt1500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		t6686rec.lxmrt[wsaaSub1.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void moveLxdp1600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		sv.lxdp[wsaaSub1.toInt()].set(t6686rec.lxdp[wsaaSub1.toInt()]);
		/*EXIT*/
	}

protected void moveLxmrt1700()
	{
		/*PARA*/
		wsaaSub1.add(1);
		sv.lxmrt[wsaaSub1.toInt()].set(t6686rec.lxmrt[wsaaSub1.toInt()]);
		/*EXIT*/
	}

protected void initTlxmrt1800()
	{
		/*PARA*/
		wsaaSub1.add(1);
		t6686rec.tlxmrt[wsaaSub1.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void moveTlxmrt1900()
	{
		/*PARA*/
		wsaaSub1.add(1);
		sv.tlxmrt[wsaaSub1.toInt()].set(t6686rec.tlxmrt[wsaaSub1.toInt()]);
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					other2080();
				case exit2090: 
					exit2090();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S6686IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S6686-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2090);
		}
	}

	/**
	* <pre>
	*    Enter screen validation here.
	* </pre>
	*/
protected void other2080()
	{
		if (isGT(sv.lxdp01, 8)) {
			sv.lxdp01Err.set(g581);
			goTo(GotoLabel.exit2090);
		}
		if (isGT(sv.lxdp02, 8)) {
			sv.lxdp02Err.set(g581);
			goTo(GotoLabel.exit2090);
		}
		if (isGT(sv.lxdp03, 8)) {
			sv.lxdp03Err.set(g581);
			goTo(GotoLabel.exit2090);
		}
		if (isGT(sv.lxdp04, 8)) {
			sv.lxdp04Err.set(g581);
			goTo(GotoLabel.exit2090);
		}
		if (isGT(sv.lxdp05, 8)) {
			sv.lxdp05Err.set(g581);
			goTo(GotoLabel.exit2090);
		}
		if (isGT(sv.lxdp06, 8)) {
			sv.lxdp06Err.set(g581);
			goTo(GotoLabel.exit2090);
		}
		if (isGT(sv.lxdp07, 8)) {
			sv.lxdp07Err.set(g581);
			goTo(GotoLabel.exit2090);
		}
		if (isGT(sv.lxdp08, 8)) {
			sv.lxdp08Err.set(g581);
			goTo(GotoLabel.exit2090);
		}
		if (isGT(sv.lxdp09, 8)) {
			sv.lxdp09Err.set(g581);
			goTo(GotoLabel.exit2090);
		}
		if (isGT(sv.lxdp10, 8)) {
			sv.lxdp10Err.set(g581);
			goTo(GotoLabel.exit2090);
		}
		if (isGT(sv.lxdp11, 8)) {
			sv.lxdp11Err.set(g581);
			goTo(GotoLabel.exit2090);
		}
		if (isGT(sv.lxdp12, 8)) {
			sv.lxdp12Err.set(g581);
			goTo(GotoLabel.exit2090);
		}
		if (isGT(sv.lxdp13, 8)) {
			sv.lxdp13Err.set(g581);
			goTo(GotoLabel.exit2090);
		}
		if (isGT(sv.lxdp14, 8)) {
			sv.lxdp14Err.set(g581);
			goTo(GotoLabel.exit2090);
		}
		if (isGT(sv.lxdp15, 8)) {
			sv.lxdp15Err.set(g581);
		}
		if (isGT(sv.lxdp16, 8)) {
			sv.lxdp16Err.set(g581);
		}
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080: 
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		wsaaUpdateFlag = "N";
		checkChanges3100();
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.other3080);
		}
		itemIO.setGenarea(t6686rec.t6686Rec);
		itemIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		/*CHECK*/
		wsaaSub1.set(0);
		/*    PERFORM 3400-UPDATE-LXDP    15 TIMES.                        */
		for (int loopVar7 = 0; !(loopVar7 == 16); loopVar7 += 1){
			updateLxdp3400();
		}
		if (isNE(sv.lxmort, t6686rec.lxmort)) {
			t6686rec.lxmort.set(sv.lxmort);
			wsaaUpdateFlag = "Y";
		}
		wsaaSub1.set(0);
		for (int loopVar8 = 0; !(loopVar8 == 99); loopVar8 += 1){
			updateLxmrt3500();
		}
		wsaaSub1.set(0);
		for (int loopVar9 = 0; !(loopVar9 == 11); loopVar9 += 1){
			updateTlxmrt3600();
		}
		/*EXIT*/
	}

protected void updateLxdp3400()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.lxdp[wsaaSub1.toInt()], t6686rec.lxdp[wsaaSub1.toInt()])) {
			t6686rec.lxdp[wsaaSub1.toInt()].set(sv.lxdp[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*EXIT*/
	}

protected void updateLxmrt3500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.lxmrt[wsaaSub1.toInt()], t6686rec.lxmrt[wsaaSub1.toInt()])) {
			t6686rec.lxmrt[wsaaSub1.toInt()].set(sv.lxmrt[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*EXIT*/
	}

protected void updateTlxmrt3600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.tlxmrt[wsaaSub1.toInt()], t6686rec.tlxmrt[wsaaSub1.toInt()])) {
			t6686rec.tlxmrt[wsaaSub1.toInt()].set(sv.tlxmrt[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
