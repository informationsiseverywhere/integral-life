/*
 * File: Tt562pt.java
 * Date: 30 August 2009 2:47:36
 * Author: Quipoz Limited
 * 
 * Class transformed from TT562PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.underwriting.tablestructures.Tt562rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TT562.
*
*
*****************************************************************
* </pre>
*/
public class Tt562pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(49).isAPartOf(wsaaPrtLine001, 27, FILLER).init("Monetary Function Nx                        ST562");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(72);
	private FixedLengthStringData filler7 = new FixedLengthStringData(72).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Dp  Age:       0        1        2        3        4  5        6");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(75);
	private FixedLengthStringData filler8 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo005 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine004, 2).setPattern("Z");
	private FixedLengthStringData filler9 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine004, 3, FILLER).init("    0+");
	private ZonedDecimalData fieldNo006 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler10 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler11 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler12 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(75);
	private FixedLengthStringData filler16 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine005, 2).setPattern("Z");
	private FixedLengthStringData filler17 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 3, FILLER).init("    7+");
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(75);
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine006, 2).setPattern("Z");
	private FixedLengthStringData filler25 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine006, 3, FILLER).init("   14+");
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(75);
	private FixedLengthStringData filler32 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine007, 2).setPattern("Z");
	private FixedLengthStringData filler33 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 3, FILLER).init("   21+");
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(75);
	private FixedLengthStringData filler40 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine008, 2).setPattern("Z");
	private FixedLengthStringData filler41 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine008, 3, FILLER).init("   28+");
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(75);
	private FixedLengthStringData filler48 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine009, 2).setPattern("Z");
	private FixedLengthStringData filler49 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 3, FILLER).init("   35+");
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(75);
	private FixedLengthStringData filler56 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine010, 2).setPattern("Z");
	private FixedLengthStringData filler57 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine010, 3, FILLER).init("   42+");
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(75);
	private FixedLengthStringData filler64 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine011, 2).setPattern("Z");
	private FixedLengthStringData filler65 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 3, FILLER).init("   49+");
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler69 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(75);
	private FixedLengthStringData filler72 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine012, 2).setPattern("Z");
	private FixedLengthStringData filler73 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 3, FILLER).init("   56+");
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler79 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo076 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(75);
	private FixedLengthStringData filler80 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine013, 2).setPattern("Z");
	private FixedLengthStringData filler81 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine013, 3, FILLER).init("   63+");
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo079 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler83 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo080 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler84 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo082 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler86 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler87 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(75);
	private FixedLengthStringData filler88 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo085 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine014, 2).setPattern("Z");
	private FixedLengthStringData filler89 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine014, 3, FILLER).init("   70+");
	private ZonedDecimalData fieldNo086 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler90 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo087 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo088 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler92 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo089 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler93 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo090 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler94 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo091 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler95 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo092 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(75);
	private FixedLengthStringData filler96 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine015, 2).setPattern("Z");
	private FixedLengthStringData filler97 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine015, 3, FILLER).init("   77+");
	private ZonedDecimalData fieldNo094 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler98 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo095 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler99 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo096 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler100 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo097 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler101 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo098 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler102 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo099 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler103 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo100 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(75);
	private FixedLengthStringData filler104 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo101 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine016, 2).setPattern("Z");
	private FixedLengthStringData filler105 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine016, 3, FILLER).init("   84+");
	private ZonedDecimalData fieldNo102 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler106 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo103 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler107 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo104 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler108 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo105 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler109 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo106 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler110 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo107 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler111 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo108 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(75);
	private FixedLengthStringData filler112 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo109 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine017, 2).setPattern("Z");
	private FixedLengthStringData filler113 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine017, 3, FILLER).init("   91+");
	private ZonedDecimalData fieldNo110 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler114 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo111 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler115 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo112 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler116 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo113 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler117 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo114 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler118 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo115 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler119 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo116 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(57);
	private FixedLengthStringData filler120 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine018, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo117 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine018, 2).setPattern("Z");
	private FixedLengthStringData filler121 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine018, 3, FILLER).init("   98+");
	private ZonedDecimalData fieldNo118 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler122 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo119 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler123 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo120 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler124 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo121 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler125 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo122 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 49).setPattern("ZZZZZZZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tt562rec tt562rec = new Tt562rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tt562pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tt562rec.tt562Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(tt562rec.nxdp01);
		fieldNo006.set(tt562rec.nxmoney);
		fieldNo013.set(tt562rec.nxdp02);
		fieldNo021.set(tt562rec.nxdp03);
		fieldNo029.set(tt562rec.nxdp04);
		fieldNo037.set(tt562rec.nxdp05);
		fieldNo045.set(tt562rec.nxdp06);
		fieldNo053.set(tt562rec.nxdp07);
		fieldNo061.set(tt562rec.nxdp08);
		fieldNo069.set(tt562rec.nxdp09);
		fieldNo077.set(tt562rec.nxdp10);
		fieldNo007.set(tt562rec.nxmon01);
		fieldNo008.set(tt562rec.nxmon02);
		fieldNo009.set(tt562rec.nxmon03);
		fieldNo010.set(tt562rec.nxmon04);
		fieldNo011.set(tt562rec.nxmon05);
		fieldNo012.set(tt562rec.nxmon06);
		fieldNo014.set(tt562rec.nxmon07);
		fieldNo015.set(tt562rec.nxmon08);
		fieldNo016.set(tt562rec.nxmon09);
		fieldNo017.set(tt562rec.nxmon10);
		fieldNo018.set(tt562rec.nxmon11);
		fieldNo019.set(tt562rec.nxmon12);
		fieldNo020.set(tt562rec.nxmon13);
		fieldNo022.set(tt562rec.nxmon14);
		fieldNo023.set(tt562rec.nxmon15);
		fieldNo024.set(tt562rec.nxmon16);
		fieldNo025.set(tt562rec.nxmon17);
		fieldNo026.set(tt562rec.nxmon18);
		fieldNo027.set(tt562rec.nxmon19);
		fieldNo028.set(tt562rec.nxmon20);
		fieldNo030.set(tt562rec.nxmon21);
		fieldNo031.set(tt562rec.nxmon22);
		fieldNo032.set(tt562rec.nxmon23);
		fieldNo033.set(tt562rec.nxmon24);
		fieldNo034.set(tt562rec.nxmon25);
		fieldNo035.set(tt562rec.nxmon26);
		fieldNo036.set(tt562rec.nxmon27);
		fieldNo038.set(tt562rec.nxmon28);
		fieldNo039.set(tt562rec.nxmon29);
		fieldNo040.set(tt562rec.nxmon30);
		fieldNo041.set(tt562rec.nxmon31);
		fieldNo042.set(tt562rec.nxmon32);
		fieldNo043.set(tt562rec.nxmon33);
		fieldNo044.set(tt562rec.nxmon34);
		fieldNo046.set(tt562rec.nxmon35);
		fieldNo047.set(tt562rec.nxmon36);
		fieldNo048.set(tt562rec.nxmon37);
		fieldNo049.set(tt562rec.nxmon38);
		fieldNo050.set(tt562rec.nxmon39);
		fieldNo051.set(tt562rec.nxmon40);
		fieldNo052.set(tt562rec.nxmon41);
		fieldNo054.set(tt562rec.nxmon42);
		fieldNo055.set(tt562rec.nxmon43);
		fieldNo056.set(tt562rec.nxmon44);
		fieldNo057.set(tt562rec.nxmon45);
		fieldNo058.set(tt562rec.nxmon46);
		fieldNo059.set(tt562rec.nxmon47);
		fieldNo060.set(tt562rec.nxmon48);
		fieldNo062.set(tt562rec.nxmon49);
		fieldNo063.set(tt562rec.nxmon50);
		fieldNo064.set(tt562rec.nxmon51);
		fieldNo065.set(tt562rec.nxmon52);
		fieldNo066.set(tt562rec.nxmon53);
		fieldNo067.set(tt562rec.nxmon54);
		fieldNo068.set(tt562rec.nxmon55);
		fieldNo070.set(tt562rec.nxmon56);
		fieldNo071.set(tt562rec.nxmon57);
		fieldNo072.set(tt562rec.nxmon58);
		fieldNo073.set(tt562rec.nxmon59);
		fieldNo074.set(tt562rec.nxmon60);
		fieldNo075.set(tt562rec.nxmon61);
		fieldNo076.set(tt562rec.nxmon62);
		fieldNo078.set(tt562rec.nxmon63);
		fieldNo079.set(tt562rec.nxmon64);
		fieldNo080.set(tt562rec.nxmon65);
		fieldNo081.set(tt562rec.nxmon66);
		fieldNo082.set(tt562rec.nxmon67);
		fieldNo083.set(tt562rec.nxmon68);
		fieldNo084.set(tt562rec.nxmon69);
		fieldNo086.set(tt562rec.nxmon70);
		fieldNo087.set(tt562rec.nxmon71);
		fieldNo088.set(tt562rec.nxmon72);
		fieldNo089.set(tt562rec.nxmon73);
		fieldNo090.set(tt562rec.nxmon74);
		fieldNo091.set(tt562rec.nxmon75);
		fieldNo092.set(tt562rec.nxmon76);
		fieldNo094.set(tt562rec.nxmon77);
		fieldNo095.set(tt562rec.nxmon78);
		fieldNo096.set(tt562rec.nxmon79);
		fieldNo097.set(tt562rec.nxmon80);
		fieldNo098.set(tt562rec.nxmon81);
		fieldNo099.set(tt562rec.nxmon82);
		fieldNo100.set(tt562rec.nxmon83);
		fieldNo102.set(tt562rec.nxmon84);
		fieldNo103.set(tt562rec.nxmon85);
		fieldNo104.set(tt562rec.nxmon86);
		fieldNo105.set(tt562rec.nxmon87);
		fieldNo106.set(tt562rec.nxmon88);
		fieldNo107.set(tt562rec.nxmon89);
		fieldNo108.set(tt562rec.nxmon90);
		fieldNo110.set(tt562rec.nxmon91);
		fieldNo111.set(tt562rec.nxmon92);
		fieldNo112.set(tt562rec.nxmon93);
		fieldNo113.set(tt562rec.nxmon94);
		fieldNo114.set(tt562rec.nxmon95);
		fieldNo115.set(tt562rec.nxmon96);
		fieldNo116.set(tt562rec.nxmon97);
		fieldNo118.set(tt562rec.nxmon98);
		fieldNo119.set(tt562rec.nxmon99);
		fieldNo085.set(tt562rec.nxdp11);
		fieldNo093.set(tt562rec.nxdp12);
		fieldNo101.set(tt562rec.nxdp13);
		fieldNo109.set(tt562rec.nxdp14);
		fieldNo117.set(tt562rec.nxdp15);
		fieldNo120.set(tt562rec.tnxmon01);
		fieldNo121.set(tt562rec.tnxmon02);
		fieldNo122.set(tt562rec.tnxmon03);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
