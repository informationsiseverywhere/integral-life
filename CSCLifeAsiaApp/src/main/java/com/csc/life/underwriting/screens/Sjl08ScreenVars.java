package com.csc.life.underwriting.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sjl08ScreenVars  extends SmartVarModel{

	public FixedLengthStringData dataArea = new FixedLengthStringData(231);
	public FixedLengthStringData dataFields = new FixedLengthStringData(71).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,14);
	public ZonedDecimalData sumAssuredFactor = DD.sumAssuredFactor.copyToZonedDecimal().isAPartOf(dataFields,44);
	public FixedLengthStringData riskClass01 = DD.lrkcls.copy().isAPartOf(dataFields,51);
	public FixedLengthStringData riskClass02 = DD.lrkcls.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData riskClass03 = DD.lrkcls.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData riskClass04 = DD.lrkcls.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData riskClass05 =  DD.lrkcls.copy().isAPartOf(dataFields,67);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 71);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData sumAssuredFactorErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData riskClass01Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData riskClass02Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData riskClass03Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData riskClass04Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData riskClass05Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 111);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] sumAssuredFactorOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] riskClass01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] riskClass02Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] riskClass03Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] riskClass04Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] riskClass05Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);

	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public LongData Sjl08screenWritten = new LongData(0);
	public LongData Sjl08protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sjl08ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(sumAssuredFactorOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, sumAssuredFactor, riskClass01 , riskClass02, riskClass03, riskClass04, riskClass05};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, sumAssuredFactorOut, riskClass01Out, riskClass02Out, riskClass03Out, riskClass04Out, riskClass05Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr,  sumAssuredFactorErr, riskClass01Err, riskClass02Err, riskClass02Err, riskClass03Err, riskClass05Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sjl08screen.class;
		protectRecord = Sjl08protect.class;
	}
}
