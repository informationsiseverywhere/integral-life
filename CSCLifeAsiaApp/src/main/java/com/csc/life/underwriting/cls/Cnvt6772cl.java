/*
 * File: Cnvt6772cl.java
 * Date: 30 August 2009 2:59:03
 * Author: $Id$
 * 
 * Class transformed from CNVT6772CL.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.cls;

import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.underwriting.procedures.Cnvt6772;
import com.csc.life.underwriting.procedures.Cnvt6772a;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class Cnvt6772cl extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData dtalib = new FixedLengthStringData(10);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData statuz = new FixedLengthStringData(4);

	public Cnvt6772cl() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		company = convertAndSetParam(company, parmArray, 1);
		dtalib = convertAndSetParam(dtalib, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			try {
				switch (qState) {
				case QS_START: {
					/* CPYT6772*/
					appVars.startCommitControl();
					callProgram(Cnvt6772a.class, new Object[] {company, statuz});
					if (isNE(statuz,"****")) {
						appVars.sendMessageToQueue("AN ERROR OCCURED IN CNVT6772A ERROR STATUS IS "+statuz+".", "*");
						rollback();
						qState = returnVar;
						break;
					}
					callProgram(Cnvt6772.class, new Object[] {company, statuz});
					if (isNE(statuz,"****")) {
						appVars.sendMessageToQueue("AN ERROR OCCURED IN CNVT6772 ERROR STATUS IS "+statuz+".", "*");
						rollback();
						qState = returnVar;
						break;
					}
					appVars.endCommitControl();
				}
				case returnVar: {
					return ;
				}
				case error: {
					appVars.sendMessageToQueue("UNEXPECTED ERRORS IN CNVT6772CL", "*");
				}
				default:{
					qState = QS_END;
				}
				}
			}
			catch (ExtMsgException ex){
				if (ex.messageMatches("CPF0000")
				|| ex.messageMatches("LBE0000")) {
					qState = error;
				}
				else {
					throw ex;
				}
			}
		}
		
	}
}
