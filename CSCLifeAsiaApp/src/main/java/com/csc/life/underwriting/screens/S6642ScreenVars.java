package com.csc.life.underwriting.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6642
 * @version 1.0 generated on 30/08/09 06:55
 * @author Quipoz
 */
public class S6642ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(242);
	public FixedLengthStringData dataFields = new FixedLengthStringData(82).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData fmageadj = DD.fmageadj.copyToZonedDecimal().isAPartOf(dataFields,1);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData jleqage = DD.jleqage.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData jlrevbns = DD.jlrevbns.copy().isAPartOf(dataFields,15);
	public FixedLengthStringData jlsumass = DD.jlsumass.copy().isAPartOf(dataFields,23);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,31);
	public FixedLengthStringData slrevbns = DD.slrevbns.copy().isAPartOf(dataFields,61);
	public FixedLengthStringData slsumass = DD.slsumass.copy().isAPartOf(dataFields,69);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,77);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 82);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData fmageadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData jleqageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData jlrevbnsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData jlsumassErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData slrevbnsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData slsumassErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 122);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] fmageadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] jleqageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] jlrevbnsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] jlsumassOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] slrevbnsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] slsumassOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6642screenWritten = new LongData(0);
	public LongData S6642protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6642ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, slsumass, slrevbns, jlsumass, jlrevbns, jleqage, fmageadj};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, slsumassOut, slrevbnsOut, jlsumassOut, jlrevbnsOut, jleqageOut, fmageadjOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, slsumassErr, slrevbnsErr, jlsumassErr, jlrevbnsErr, jleqageErr, fmageadjErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6642screen.class;
		protectRecord = S6642protect.class;
	}

}
