package com.csc.life.underwriting.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:31
 * Description:
 * Copybook name: T6775REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6775rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6775Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData cont = new FixedLengthStringData(8).isAPartOf(t6775Rec, 0);
  	public FixedLengthStringData intgfroms = new FixedLengthStringData(96).isAPartOf(t6775Rec, 8);
  	public ZonedDecimalData[] intgfrom = ZDArrayPartOfStructure(24, 4, 0, intgfroms, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(96).isAPartOf(intgfroms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData intgfrom01 = new ZonedDecimalData(4, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData intgfrom02 = new ZonedDecimalData(4, 0).isAPartOf(filler, 4);
  	public ZonedDecimalData intgfrom03 = new ZonedDecimalData(4, 0).isAPartOf(filler, 8);
  	public ZonedDecimalData intgfrom04 = new ZonedDecimalData(4, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData intgfrom05 = new ZonedDecimalData(4, 0).isAPartOf(filler, 16);
  	public ZonedDecimalData intgfrom06 = new ZonedDecimalData(4, 0).isAPartOf(filler, 20);
  	public ZonedDecimalData intgfrom07 = new ZonedDecimalData(4, 0).isAPartOf(filler, 24);
  	public ZonedDecimalData intgfrom08 = new ZonedDecimalData(4, 0).isAPartOf(filler, 28);
  	public ZonedDecimalData intgfrom09 = new ZonedDecimalData(4, 0).isAPartOf(filler, 32);
  	public ZonedDecimalData intgfrom10 = new ZonedDecimalData(4, 0).isAPartOf(filler, 36);
  	public ZonedDecimalData intgfrom11 = new ZonedDecimalData(4, 0).isAPartOf(filler, 40);
  	public ZonedDecimalData intgfrom12 = new ZonedDecimalData(4, 0).isAPartOf(filler, 44);
  	public ZonedDecimalData intgfrom13 = new ZonedDecimalData(4, 0).isAPartOf(filler, 48);
  	public ZonedDecimalData intgfrom14 = new ZonedDecimalData(4, 0).isAPartOf(filler, 52);
  	public ZonedDecimalData intgfrom15 = new ZonedDecimalData(4, 0).isAPartOf(filler, 56);
  	public ZonedDecimalData intgfrom16 = new ZonedDecimalData(4, 0).isAPartOf(filler, 60);
  	public ZonedDecimalData intgfrom17 = new ZonedDecimalData(4, 0).isAPartOf(filler, 64);
  	public ZonedDecimalData intgfrom18 = new ZonedDecimalData(4, 0).isAPartOf(filler, 68);
  	public ZonedDecimalData intgfrom19 = new ZonedDecimalData(4, 0).isAPartOf(filler, 72);
  	public ZonedDecimalData intgfrom20 = new ZonedDecimalData(4, 0).isAPartOf(filler, 76);
  	public ZonedDecimalData intgfrom21 = new ZonedDecimalData(4, 0).isAPartOf(filler, 80);
  	public ZonedDecimalData intgfrom22 = new ZonedDecimalData(4, 0).isAPartOf(filler, 84);
  	public ZonedDecimalData intgfrom23 = new ZonedDecimalData(4, 0).isAPartOf(filler, 88);
  	public ZonedDecimalData intgfrom24 = new ZonedDecimalData(4, 0).isAPartOf(filler, 92);
  	public FixedLengthStringData intgtos = new FixedLengthStringData(96).isAPartOf(t6775Rec, 104);
  	public ZonedDecimalData[] intgto = ZDArrayPartOfStructure(24, 4, 0, intgtos, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(96).isAPartOf(intgtos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData intgto01 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData intgto02 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 4);
  	public ZonedDecimalData intgto03 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 8);
  	public ZonedDecimalData intgto04 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData intgto05 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 16);
  	public ZonedDecimalData intgto06 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 20);
  	public ZonedDecimalData intgto07 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 24);
  	public ZonedDecimalData intgto08 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 28);
  	public ZonedDecimalData intgto09 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 32);
  	public ZonedDecimalData intgto10 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 36);
  	public ZonedDecimalData intgto11 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 40);
  	public ZonedDecimalData intgto12 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 44);
  	public ZonedDecimalData intgto13 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 48);
  	public ZonedDecimalData intgto14 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 52);
  	public ZonedDecimalData intgto15 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 56);
  	public ZonedDecimalData intgto16 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 60);
  	public ZonedDecimalData intgto17 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 64);
  	public ZonedDecimalData intgto18 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 68);
  	public ZonedDecimalData intgto19 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 72);
  	public ZonedDecimalData intgto20 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 76);
  	public ZonedDecimalData intgto21 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 80);
  	public ZonedDecimalData intgto22 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 84);
  	public ZonedDecimalData intgto23 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 88);
  	public ZonedDecimalData intgto24 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 92);
  	public FixedLengthStringData undwrules = new FixedLengthStringData(96).isAPartOf(t6775Rec, 200);
  	public FixedLengthStringData[] undwrule = FLSArrayPartOfStructure(24, 4, undwrules, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(96).isAPartOf(undwrules, 0, FILLER_REDEFINE);
  	public FixedLengthStringData undwrule01 = new FixedLengthStringData(4).isAPartOf(filler2, 0);
  	public FixedLengthStringData undwrule02 = new FixedLengthStringData(4).isAPartOf(filler2, 4);
  	public FixedLengthStringData undwrule03 = new FixedLengthStringData(4).isAPartOf(filler2, 8);
  	public FixedLengthStringData undwrule04 = new FixedLengthStringData(4).isAPartOf(filler2, 12);
  	public FixedLengthStringData undwrule05 = new FixedLengthStringData(4).isAPartOf(filler2, 16);
  	public FixedLengthStringData undwrule06 = new FixedLengthStringData(4).isAPartOf(filler2, 20);
  	public FixedLengthStringData undwrule07 = new FixedLengthStringData(4).isAPartOf(filler2, 24);
  	public FixedLengthStringData undwrule08 = new FixedLengthStringData(4).isAPartOf(filler2, 28);
  	public FixedLengthStringData undwrule09 = new FixedLengthStringData(4).isAPartOf(filler2, 32);
  	public FixedLengthStringData undwrule10 = new FixedLengthStringData(4).isAPartOf(filler2, 36);
  	public FixedLengthStringData undwrule11 = new FixedLengthStringData(4).isAPartOf(filler2, 40);
  	public FixedLengthStringData undwrule12 = new FixedLengthStringData(4).isAPartOf(filler2, 44);
  	public FixedLengthStringData undwrule13 = new FixedLengthStringData(4).isAPartOf(filler2, 48);
  	public FixedLengthStringData undwrule14 = new FixedLengthStringData(4).isAPartOf(filler2, 52);
  	public FixedLengthStringData undwrule15 = new FixedLengthStringData(4).isAPartOf(filler2, 56);
  	public FixedLengthStringData undwrule16 = new FixedLengthStringData(4).isAPartOf(filler2, 60);
  	public FixedLengthStringData undwrule17 = new FixedLengthStringData(4).isAPartOf(filler2, 64);
  	public FixedLengthStringData undwrule18 = new FixedLengthStringData(4).isAPartOf(filler2, 68);
  	public FixedLengthStringData undwrule19 = new FixedLengthStringData(4).isAPartOf(filler2, 72);
  	public FixedLengthStringData undwrule20 = new FixedLengthStringData(4).isAPartOf(filler2, 76);
  	public FixedLengthStringData undwrule21 = new FixedLengthStringData(4).isAPartOf(filler2, 80);
  	public FixedLengthStringData undwrule22 = new FixedLengthStringData(4).isAPartOf(filler2, 84);
  	public FixedLengthStringData undwrule23 = new FixedLengthStringData(4).isAPartOf(filler2, 88);
  	public FixedLengthStringData undwrule24 = new FixedLengthStringData(4).isAPartOf(filler2, 92);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(204).isAPartOf(t6775Rec, 296, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6775Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6775Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}