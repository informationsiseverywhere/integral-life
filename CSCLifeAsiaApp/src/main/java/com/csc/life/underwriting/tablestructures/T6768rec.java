package com.csc.life.underwriting.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:22
 * Description:
 * Copybook name: T6768REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6768rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6768Rec = new FixedLengthStringData(505);
  	public FixedLengthStringData agecont = new FixedLengthStringData(8).isAPartOf(t6768Rec, 0);
  	public FixedLengthStringData sacont = new FixedLengthStringData(8).isAPartOf(t6768Rec, 8);
  	public FixedLengthStringData undages = new FixedLengthStringData(30).isAPartOf(t6768Rec, 16);
  	public ZonedDecimalData[] undage = ZDArrayPartOfStructure(10, 3, 0, undages, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(undages, 0, FILLER_REDEFINE);
  	public ZonedDecimalData undage01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData undage02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData undage03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData undage04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public ZonedDecimalData undage05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData undage06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData undage07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData undage08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
  	public ZonedDecimalData undage09 = new ZonedDecimalData(3, 0).isAPartOf(filler, 24);
  	public ZonedDecimalData undage10 = new ZonedDecimalData(3, 0).isAPartOf(filler, 27);
  	public FixedLengthStringData undsas = new FixedLengthStringData(60).isAPartOf(t6768Rec, 46);
  	public ZonedDecimalData[] undsa = ZDArrayPartOfStructure(5, 12, 0, undsas, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(60).isAPartOf(undsas, 0, FILLER_REDEFINE);
  	public ZonedDecimalData undsa01 = new ZonedDecimalData(12, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData undsa02 = new ZonedDecimalData(12, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData undsa03 = new ZonedDecimalData(12, 0).isAPartOf(filler1, 24);
  	public ZonedDecimalData undsa04 = new ZonedDecimalData(12, 0).isAPartOf(filler1, 36);
  	public ZonedDecimalData undsa05 = new ZonedDecimalData(12, 0).isAPartOf(filler1, 48);
  	public FixedLengthStringData undwrules = new FixedLengthStringData(200).isAPartOf(t6768Rec, 106);
  	public FixedLengthStringData[] undwrule = FLSArrayPartOfStructure(50, 4, undwrules, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(200).isAPartOf(undwrules, 0, FILLER_REDEFINE);
  	public FixedLengthStringData undwrule01 = new FixedLengthStringData(4).isAPartOf(filler2, 0);
  	public FixedLengthStringData undwrule02 = new FixedLengthStringData(4).isAPartOf(filler2, 4);
  	public FixedLengthStringData undwrule03 = new FixedLengthStringData(4).isAPartOf(filler2, 8);
  	public FixedLengthStringData undwrule04 = new FixedLengthStringData(4).isAPartOf(filler2, 12);
  	public FixedLengthStringData undwrule05 = new FixedLengthStringData(4).isAPartOf(filler2, 16);
  	public FixedLengthStringData undwrule06 = new FixedLengthStringData(4).isAPartOf(filler2, 20);
  	public FixedLengthStringData undwrule07 = new FixedLengthStringData(4).isAPartOf(filler2, 24);
  	public FixedLengthStringData undwrule08 = new FixedLengthStringData(4).isAPartOf(filler2, 28);
  	public FixedLengthStringData undwrule09 = new FixedLengthStringData(4).isAPartOf(filler2, 32);
  	public FixedLengthStringData undwrule10 = new FixedLengthStringData(4).isAPartOf(filler2, 36);
  	public FixedLengthStringData undwrule11 = new FixedLengthStringData(4).isAPartOf(filler2, 40);
  	public FixedLengthStringData undwrule12 = new FixedLengthStringData(4).isAPartOf(filler2, 44);
  	public FixedLengthStringData undwrule13 = new FixedLengthStringData(4).isAPartOf(filler2, 48);
  	public FixedLengthStringData undwrule14 = new FixedLengthStringData(4).isAPartOf(filler2, 52);
  	public FixedLengthStringData undwrule15 = new FixedLengthStringData(4).isAPartOf(filler2, 56);
  	public FixedLengthStringData undwrule16 = new FixedLengthStringData(4).isAPartOf(filler2, 60);
  	public FixedLengthStringData undwrule17 = new FixedLengthStringData(4).isAPartOf(filler2, 64);
  	public FixedLengthStringData undwrule18 = new FixedLengthStringData(4).isAPartOf(filler2, 68);
  	public FixedLengthStringData undwrule19 = new FixedLengthStringData(4).isAPartOf(filler2, 72);
  	public FixedLengthStringData undwrule20 = new FixedLengthStringData(4).isAPartOf(filler2, 76);
  	public FixedLengthStringData undwrule21 = new FixedLengthStringData(4).isAPartOf(filler2, 80);
  	public FixedLengthStringData undwrule22 = new FixedLengthStringData(4).isAPartOf(filler2, 84);
  	public FixedLengthStringData undwrule23 = new FixedLengthStringData(4).isAPartOf(filler2, 88);
  	public FixedLengthStringData undwrule24 = new FixedLengthStringData(4).isAPartOf(filler2, 92);
  	public FixedLengthStringData undwrule25 = new FixedLengthStringData(4).isAPartOf(filler2, 96);
  	public FixedLengthStringData undwrule26 = new FixedLengthStringData(4).isAPartOf(filler2, 100);
  	public FixedLengthStringData undwrule27 = new FixedLengthStringData(4).isAPartOf(filler2, 104);
  	public FixedLengthStringData undwrule28 = new FixedLengthStringData(4).isAPartOf(filler2, 108);
  	public FixedLengthStringData undwrule29 = new FixedLengthStringData(4).isAPartOf(filler2, 112);
  	public FixedLengthStringData undwrule30 = new FixedLengthStringData(4).isAPartOf(filler2, 116);
  	public FixedLengthStringData undwrule31 = new FixedLengthStringData(4).isAPartOf(filler2, 120);
  	public FixedLengthStringData undwrule32 = new FixedLengthStringData(4).isAPartOf(filler2, 124);
  	public FixedLengthStringData undwrule33 = new FixedLengthStringData(4).isAPartOf(filler2, 128);
  	public FixedLengthStringData undwrule34 = new FixedLengthStringData(4).isAPartOf(filler2, 132);
  	public FixedLengthStringData undwrule35 = new FixedLengthStringData(4).isAPartOf(filler2, 136);
  	public FixedLengthStringData undwrule36 = new FixedLengthStringData(4).isAPartOf(filler2, 140);
  	public FixedLengthStringData undwrule37 = new FixedLengthStringData(4).isAPartOf(filler2, 144);
  	public FixedLengthStringData undwrule38 = new FixedLengthStringData(4).isAPartOf(filler2, 148);
  	public FixedLengthStringData undwrule39 = new FixedLengthStringData(4).isAPartOf(filler2, 152);
  	public FixedLengthStringData undwrule40 = new FixedLengthStringData(4).isAPartOf(filler2, 156);
  	public FixedLengthStringData undwrule41 = new FixedLengthStringData(4).isAPartOf(filler2, 160);
  	public FixedLengthStringData undwrule42 = new FixedLengthStringData(4).isAPartOf(filler2, 164);
  	public FixedLengthStringData undwrule43 = new FixedLengthStringData(4).isAPartOf(filler2, 168);
  	public FixedLengthStringData undwrule44 = new FixedLengthStringData(4).isAPartOf(filler2, 172);
  	public FixedLengthStringData undwrule45 = new FixedLengthStringData(4).isAPartOf(filler2, 176);
  	public FixedLengthStringData undwrule46 = new FixedLengthStringData(4).isAPartOf(filler2, 180);
  	public FixedLengthStringData undwrule47 = new FixedLengthStringData(4).isAPartOf(filler2, 184);
  	public FixedLengthStringData undwrule48 = new FixedLengthStringData(4).isAPartOf(filler2, 188);
  	public FixedLengthStringData undwrule49 = new FixedLengthStringData(4).isAPartOf(filler2, 192);
  	public FixedLengthStringData undwrule50 = new FixedLengthStringData(4).isAPartOf(filler2, 196);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(199).isAPartOf(t6768Rec, 306, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6768Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6768Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}