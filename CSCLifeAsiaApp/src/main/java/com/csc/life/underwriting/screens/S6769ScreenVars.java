package com.csc.life.underwriting.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6769
 * @version 1.0 generated on 30/08/09 07:00
 * @author Quipoz
 */
public class S6769ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(940);
	public FixedLengthStringData dataFields = new FixedLengthStringData(348).isAPartOf(dataArea, 0);
	public FixedLengthStringData bmiclasss = new FixedLengthStringData(200).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] bmiclass = FLSArrayPartOfStructure(10, 20, bmiclasss, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(200).isAPartOf(bmiclasss, 0, FILLER_REDEFINE);
	public FixedLengthStringData bmiclass01 = DD.bmiclass.copy().isAPartOf(filler,0);
	public FixedLengthStringData bmiclass02 = DD.bmiclass.copy().isAPartOf(filler,20);
	public FixedLengthStringData bmiclass03 = DD.bmiclass.copy().isAPartOf(filler,40);
	public FixedLengthStringData bmiclass04 = DD.bmiclass.copy().isAPartOf(filler,60);
	public FixedLengthStringData bmiclass05 = DD.bmiclass.copy().isAPartOf(filler,80);
	public FixedLengthStringData bmiclass06 = DD.bmiclass.copy().isAPartOf(filler,100);
	public FixedLengthStringData bmiclass07 = DD.bmiclass.copy().isAPartOf(filler,120);
	public FixedLengthStringData bmiclass08 = DD.bmiclass.copy().isAPartOf(filler,140);
	public FixedLengthStringData bmiclass09 = DD.bmiclass.copy().isAPartOf(filler,160);
	public FixedLengthStringData bmiclass10 = DD.bmiclass.copy().isAPartOf(filler,180);
	public ZonedDecimalData bmifact = DD.bmifact.copyToZonedDecimal().isAPartOf(dataFields,200);
	public FixedLengthStringData bmis = new FixedLengthStringData(40).isAPartOf(dataFields, 208);
	public ZonedDecimalData[] bmi = ZDArrayPartOfStructure(10, 4, 1, bmis, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(40).isAPartOf(bmis, 0, FILLER_REDEFINE);
	public ZonedDecimalData bmi01 = DD.bmi.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData bmi02 = DD.bmi.copyToZonedDecimal().isAPartOf(filler1,4);
	public ZonedDecimalData bmi03 = DD.bmi.copyToZonedDecimal().isAPartOf(filler1,8);
	public ZonedDecimalData bmi04 = DD.bmi.copyToZonedDecimal().isAPartOf(filler1,12);
	public ZonedDecimalData bmi05 = DD.bmi.copyToZonedDecimal().isAPartOf(filler1,16);
	public ZonedDecimalData bmi06 = DD.bmi.copyToZonedDecimal().isAPartOf(filler1,20);
	public ZonedDecimalData bmi07 = DD.bmi.copyToZonedDecimal().isAPartOf(filler1,24);
	public ZonedDecimalData bmi08 = DD.bmi.copyToZonedDecimal().isAPartOf(filler1,28);
	public ZonedDecimalData bmi09 = DD.bmi.copyToZonedDecimal().isAPartOf(filler1,32);
	public ZonedDecimalData bmi10 = DD.bmi.copyToZonedDecimal().isAPartOf(filler1,36);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,248);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,249);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,257);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,265);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,273);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,303);
	public FixedLengthStringData undwrules = new FixedLengthStringData(40).isAPartOf(dataFields, 308);
	public FixedLengthStringData[] undwrule = FLSArrayPartOfStructure(10, 4, undwrules, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(undwrules, 0, FILLER_REDEFINE);
	public FixedLengthStringData undwrule01 = DD.undwrule.copy().isAPartOf(filler2,0);
	public FixedLengthStringData undwrule02 = DD.undwrule.copy().isAPartOf(filler2,4);
	public FixedLengthStringData undwrule03 = DD.undwrule.copy().isAPartOf(filler2,8);
	public FixedLengthStringData undwrule04 = DD.undwrule.copy().isAPartOf(filler2,12);
	public FixedLengthStringData undwrule05 = DD.undwrule.copy().isAPartOf(filler2,16);
	public FixedLengthStringData undwrule06 = DD.undwrule.copy().isAPartOf(filler2,20);
	public FixedLengthStringData undwrule07 = DD.undwrule.copy().isAPartOf(filler2,24);
	public FixedLengthStringData undwrule08 = DD.undwrule.copy().isAPartOf(filler2,28);
	public FixedLengthStringData undwrule09 = DD.undwrule.copy().isAPartOf(filler2,32);
	public FixedLengthStringData undwrule10 = DD.undwrule.copy().isAPartOf(filler2,36);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(148).isAPartOf(dataArea, 348);
	public FixedLengthStringData bmiclasssErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] bmiclassErr = FLSArrayPartOfStructure(10, 4, bmiclasssErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(bmiclasssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData bmiclass01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData bmiclass02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData bmiclass03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData bmiclass04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData bmiclass05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData bmiclass06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData bmiclass07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData bmiclass08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData bmiclass09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData bmiclass10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData bmifactErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData bmisErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData[] bmiErr = FLSArrayPartOfStructure(10, 4, bmisErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(40).isAPartOf(bmisErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData bmi01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData bmi02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData bmi03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData bmi04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData bmi05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData bmi06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData bmi07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData bmi08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData bmi09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData bmi10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData undwrulesErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData[] undwruleErr = FLSArrayPartOfStructure(10, 4, undwrulesErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(undwrulesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData undwrule01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData undwrule02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData undwrule03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData undwrule04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData undwrule05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData undwrule06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData undwrule07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData undwrule08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData undwrule09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData undwrule10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(444).isAPartOf(dataArea, 496);
	public FixedLengthStringData bmiclasssOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] bmiclassOut = FLSArrayPartOfStructure(10, 12, bmiclasssOut, 0);
	public FixedLengthStringData[][] bmiclassO = FLSDArrayPartOfArrayStructure(12, 1, bmiclassOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(120).isAPartOf(bmiclasssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] bmiclass01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] bmiclass02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] bmiclass03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] bmiclass04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] bmiclass05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] bmiclass06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] bmiclass07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);
	public FixedLengthStringData[] bmiclass08Out = FLSArrayPartOfStructure(12, 1, filler6, 84);
	public FixedLengthStringData[] bmiclass09Out = FLSArrayPartOfStructure(12, 1, filler6, 96);
	public FixedLengthStringData[] bmiclass10Out = FLSArrayPartOfStructure(12, 1, filler6, 108);
	public FixedLengthStringData[] bmifactOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData bmisOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 132);
	public FixedLengthStringData[] bmiOut = FLSArrayPartOfStructure(10, 12, bmisOut, 0);
	public FixedLengthStringData[][] bmiO = FLSDArrayPartOfArrayStructure(12, 1, bmiOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(120).isAPartOf(bmisOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] bmi01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] bmi02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] bmi03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] bmi04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] bmi05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] bmi06Out = FLSArrayPartOfStructure(12, 1, filler7, 60);
	public FixedLengthStringData[] bmi07Out = FLSArrayPartOfStructure(12, 1, filler7, 72);
	public FixedLengthStringData[] bmi08Out = FLSArrayPartOfStructure(12, 1, filler7, 84);
	public FixedLengthStringData[] bmi09Out = FLSArrayPartOfStructure(12, 1, filler7, 96);
	public FixedLengthStringData[] bmi10Out = FLSArrayPartOfStructure(12, 1, filler7, 108);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData undwrulesOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 324);
	public FixedLengthStringData[] undwruleOut = FLSArrayPartOfStructure(10, 12, undwrulesOut, 0);
	public FixedLengthStringData[][] undwruleO = FLSDArrayPartOfArrayStructure(12, 1, undwruleOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(120).isAPartOf(undwrulesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] undwrule01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] undwrule02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] undwrule03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] undwrule04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] undwrule05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] undwrule06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] undwrule07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] undwrule08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] undwrule09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] undwrule10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6769screenWritten = new LongData(0);
	public LongData S6769protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6769ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, bmiclass01, bmiclass02, bmiclass03, bmiclass04, bmiclass05, bmiclass06, bmiclass07, bmiclass08, bmiclass09, bmiclass10, undwrule02, undwrule03, undwrule04, undwrule05, undwrule06, undwrule07, undwrule08, undwrule09, undwrule10, bmifact, bmi01, bmi02, bmi03, bmi04, bmi05, bmi06, bmi07, bmi08, bmi09, bmi10, undwrule01};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, bmiclass01Out, bmiclass02Out, bmiclass03Out, bmiclass04Out, bmiclass05Out, bmiclass06Out, bmiclass07Out, bmiclass08Out, bmiclass09Out, bmiclass10Out, undwrule02Out, undwrule03Out, undwrule04Out, undwrule05Out, undwrule06Out, undwrule07Out, undwrule08Out, undwrule09Out, undwrule10Out, bmifactOut, bmi01Out, bmi02Out, bmi03Out, bmi04Out, bmi05Out, bmi06Out, bmi07Out, bmi08Out, bmi09Out, bmi10Out, undwrule01Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, bmiclass01Err, bmiclass02Err, bmiclass03Err, bmiclass04Err, bmiclass05Err, bmiclass06Err, bmiclass07Err, bmiclass08Err, bmiclass09Err, bmiclass10Err, undwrule02Err, undwrule03Err, undwrule04Err, undwrule05Err, undwrule06Err, undwrule07Err, undwrule08Err, undwrule09Err, undwrule10Err, bmifactErr, bmi01Err, bmi02Err, bmi03Err, bmi04Err, bmi05Err, bmi06Err, bmi07Err, bmi08Err, bmi09Err, bmi10Err, undwrule01Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6769screen.class;
		protectRecord = S6769protect.class;
	}

}
