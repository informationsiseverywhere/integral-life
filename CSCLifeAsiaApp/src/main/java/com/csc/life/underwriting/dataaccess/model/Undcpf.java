/******************************************************************************
 * File Name 		: Undcpf.java
 * Author			: nloganathan5
 * Creation Date	: 27 October 2016
 * Project			: Integral Life
 * Description		: The Model Class for UNDCPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.underwriting.dataaccess.model;

import java.io.Serializable;
import java.util.Date;

public class Undcpf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	// Constant for database table name
	public static final String TABLE_NAME = "UNDCPF";

	//member variables for columns
	private long uniqueNumber;
	private Character chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer currfrom;
	private Integer currto;
	private String undwrule;
	private Integer tranno;
	private Character specind;
	private Character validflag;
	private String usrprf;
	private String jobnm;
	private Date datime;

	// Constructor
	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public Character getChdrcoy(){
		return this.chdrcoy;
	}
	public String getChdrnum(){
		return this.chdrnum;
	}
	public String getLife(){
		return this.life;
	}
	public String getJlife(){
		return this.jlife;
	}
	public String getCoverage(){
		return this.coverage;
	}
	public String getRider(){
		return this.rider;
	}
	public Integer getCurrfrom(){
		return this.currfrom;
	}
	public Integer getCurrto(){
		return this.currto;
	}
	public String getUndwrule(){
		return this.undwrule;
	}
	public Integer getTranno(){
		return this.tranno;
	}
	public Character getSpecind(){
		return this.specind;
	}
	public Character getValidflag(){
		return this.validflag;
	}
	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public Date getDatime(){
		return new Date(this.datime.getTime());//IJTI-316
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setChdrcoy( Character chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setChdrnum( String chdrnum ){
		 this.chdrnum = chdrnum;
	}
	public void setLife( String life ){
		 this.life = life;
	}
	public void setJlife( String jlife ){
		 this.jlife = jlife;
	}
	public void setCoverage( String coverage ){
		 this.coverage = coverage;
	}
	public void setRider( String rider ){
		 this.rider = rider;
	}
	public void setCurrfrom( Integer currfrom ){
		 this.currfrom = currfrom;
	}
	public void setCurrto( Integer currto ){
		 this.currto = currto;
	}
	public void setUndwrule( String undwrule ){
		 this.undwrule = undwrule;
	}
	public void setTranno( Integer tranno ){
		 this.tranno = tranno;
	}
	public void setSpecind( Character specind ){
		 this.specind = specind;
	}
	public void setValidflag( Character validflag ){
		 this.validflag = validflag;
	}
	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime( Date datime ){
		this.datime = new Date(datime.getTime());//IJTI-314
	}

	// ToString method
	public String toString(){

		StringBuilder output = new StringBuilder();

		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("CHDRCOY:		");
		output.append(getChdrcoy());
		output.append("\r\n");
		output.append("CHDRNUM:		");
		output.append(getChdrnum());
		output.append("\r\n");
		output.append("LIFE:		");
		output.append(getLife());
		output.append("\r\n");
		output.append("JLIFE:		");
		output.append(getJlife());
		output.append("\r\n");
		output.append("COVERAGE:		");
		output.append(getCoverage());
		output.append("\r\n");
		output.append("RIDER:		");
		output.append(getRider());
		output.append("\r\n");
		output.append("CURRFROM:		");
		output.append(getCurrfrom());
		output.append("\r\n");
		output.append("CURRTO:		");
		output.append(getCurrto());
		output.append("\r\n");
		output.append("UNDWRULE:		");
		output.append(getUndwrule());
		output.append("\r\n");
		output.append("TRANNO:		");
		output.append(getTranno());
		output.append("\r\n");
		output.append("SPECIND:		");
		output.append(getSpecind());
		output.append("\r\n");
		output.append("VALIDFLAG:		");
		output.append(getValidflag());
		output.append("\r\n");
		output.append("USRPRF:		");
		output.append(getUsrprf());
		output.append("\r\n");
		output.append("JOBNM:		");
		output.append(getJobnm());
		output.append("\r\n");
		output.append("DATIME:		");
		output.append(getDatime());
		output.append("\r\n");

		return output.toString();
	}

}