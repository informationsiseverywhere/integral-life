package com.csc.life.underwriting.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:34
 * Description:
 * Copybook name: UNDLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Undlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData undlFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData undlKey = new FixedLengthStringData(256).isAPartOf(undlFileKey, 0, REDEFINE);
  	public FixedLengthStringData undlChdrcoy = new FixedLengthStringData(1).isAPartOf(undlKey, 0);
  	public FixedLengthStringData undlChdrnum = new FixedLengthStringData(8).isAPartOf(undlKey, 1);
  	public FixedLengthStringData undlLife = new FixedLengthStringData(2).isAPartOf(undlKey, 9);
  	public FixedLengthStringData undlJlife = new FixedLengthStringData(2).isAPartOf(undlKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(undlKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(undlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		undlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}