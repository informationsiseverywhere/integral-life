/*
 * File: Undwsubr.java
 * Date: 30 August 2009 2:49:10
 * Author: Quipoz Limited
 * 
 * Class transformed from UNDWSUBR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.FupeTableDAM;
import com.csc.life.contractservicing.procedures.UwQuestionnaireUtil;
import com.csc.life.enquiries.dataaccess.dao.ItdmpfDAO;
import com.csc.life.enquiries.dataaccess.model.Itdmpf;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.FluplnbTableDAM;
import com.csc.life.newbusiness.dataaccess.FlupundTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.tablestructures.T5608rec;
import com.csc.life.newbusiness.tablestructures.T5657rec;
import com.csc.life.productdefinition.recordstructures.UWQuestionnaireRec;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.life.underwriting.dataaccess.UndcTableDAM;
import com.csc.life.underwriting.dataaccess.UndqTableDAM;
import com.csc.life.underwriting.dataaccess.dao.UndcpfDAO;
import com.csc.life.underwriting.dataaccess.dao.UndqpfDAO;
import com.csc.life.underwriting.dataaccess.model.Undcpf;
import com.csc.life.underwriting.dataaccess.model.Undqpf;
import com.csc.life.underwriting.recordstructures.Undwsubrec;
import com.csc.life.underwriting.tablestructures.T6772rec;
import com.csc.life.underwriting.tablestructures.T6779rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* This underwriting subroutine is called from pre-issue
* validation for each life assured.
*
* First of all it will determine the OVERALL GENERIC underwriting
* rule for the life. It will do this by gathering all the rules
* associated with the life assured so far.
*
* There are 3 kinds of underwriting rule :
*
* - A BMI u/w rule is passed through linkage (UNDW-BMIRULE).
* - An u/w rule for each component (stored on UNDCPF).
* - An u/w rule for each lifestyle question (stored on UNDQPF).
*
* An u/w rule may be either GENERIC or SPECIFIC. This is
* determined by T6772 which stores all u/w rules.
*
* Once all the rules have been gathered then they are stored in
* two working storage tables. The first table holds all GENERIC
* u/w rule details. the second holds all SPECIFIC u/w rule
* details. (Read T6772 to retrieve the rule type. This will
* either be G (generic) or S (specific).
*
* When determining the OVERALL GENERIC rule, all SPECIFIC rules
* may be disregarded.
*
* GENERIC rules
* -------------
*
* Firstly the generic working storage list is processed.
* again from T6772 retrieve the sequence no associated with each
* rule. Work out which rule has the LOWEST sequence number.
* This is the OVERALL GENERIC UNDERWRITING RULE.
*             ---------------------------------
*
* The next step is to create any automatic follow ups, special
* terms or letter request records associated with the overall
* generic u/w rule. Again this is determined by T6772.
*
* SPECIFIC rules
* --------------
*
* Next the specific u/w rules in the working storage table are
* processed. For EACH specific rule any associated follow ups,
* special terms and letter request records must be created.
*
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Undwsubr extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "UNDWSUBR";

	private FixedLengthStringData wsaaSpecialTermFlag = new FixedLengthStringData(1);
	private Validator specialTermAllowed = new Validator(wsaaSpecialTermFlag, "Y");

	private FixedLengthStringData wsaaLetterFlag = new FixedLengthStringData(1);
	private Validator letterFound = new Validator(wsaaLetterFlag, "Y");
	private Validator letterNotFound = new Validator(wsaaLetterFlag, "N");

	private FixedLengthStringData wsaaLextFlag = new FixedLengthStringData(1);
	private Validator lextFound = new Validator(wsaaLextFlag, "Y");
	private Validator lextNotFound = new Validator(wsaaLextFlag, "N");
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData iy = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaDocseq = new ZonedDecimalData(4, 0).setUnsigned();

		/* WSAA-COVTLNB-ARRAY */
	private FixedLengthStringData[] wsaaComponents = FLSInittedArray (30, 10);
	private FixedLengthStringData[] wsaaCovtCovr = FLSDArrayPartOfArrayStructure(2, wsaaComponents, 0);
	private FixedLengthStringData[] wsaaCovtRidr = FLSDArrayPartOfArrayStructure(2, wsaaComponents, 2);
	private PackedDecimalData[] wsaaCovtSeqnbr = PDArrayPartOfArrayStructure(3, 0, wsaaComponents, 4);
	private FixedLengthStringData[] wsaaCovtCrtable = FLSDArrayPartOfArrayStructure(4, wsaaComponents, 6);

		/* WSAA-LEXT-ARRAY */
	private FixedLengthStringData[] wsaaSpecTerms = FLSInittedArray (99, 12);
	private FixedLengthStringData[] wsaaLextLife = FLSDArrayPartOfArrayStructure(2, wsaaSpecTerms, 0);
	private FixedLengthStringData[] wsaaLextJlife = FLSDArrayPartOfArrayStructure(2, wsaaSpecTerms, 2);
	private FixedLengthStringData[] wsaaLextCovr = FLSDArrayPartOfArrayStructure(2, wsaaSpecTerms, 4);
	private FixedLengthStringData[] wsaaLextRidr = FLSDArrayPartOfArrayStructure(2, wsaaSpecTerms, 6);
	private FixedLengthStringData[] wsaaLextOpcda = FLSDArrayPartOfArrayStructure(2, wsaaSpecTerms, 8);
	private PackedDecimalData[] wsaaLextSeqnbr = PDArrayPartOfArrayStructure(3, 0, wsaaSpecTerms, 10);

		/* WSAA-TABLE-KEYS */
	private FixedLengthStringData wsaaItemKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaValidItem = new FixedLengthStringData(5).isAPartOf(wsaaItemKey, 0);
	private FixedLengthStringData wsaaCurrency = new FixedLengthStringData(3).isAPartOf(wsaaItemKey, 5);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrancd = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4);

	private FixedLengthStringData wsaaT5657Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaComponent = new FixedLengthStringData(4).isAPartOf(wsaaT5657Key, 0);
	private FixedLengthStringData wsaaSpTerm = new FixedLengthStringData(2).isAPartOf(wsaaT5657Key, 4);
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemtabl = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaOverallRule = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaUwRule = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2);

		/* WSAA-SPECIFIC-RULE-DTLS */
	private FixedLengthStringData[] wsaaSpecificRules = FLSInittedArray (20, 67);
	private FixedLengthStringData[] wsaaSRule = FLSDArrayPartOfArrayStructure(4, wsaaSpecificRules, 0);
	private ZonedDecimalData[] wsaaSSeqnumb = ZDArrayPartOfArrayStructure(3, 0, wsaaSpecificRules, 4, UNSIGNED_TRUE);
	private FixedLengthStringData[] wsaaSFollowUps = FLSDArrayPartOfArrayStructure(30, wsaaSpecificRules, 7);
	private FixedLengthStringData[][] wsaaSFups = FLSDArrayPartOfArrayStructure(10, 3, wsaaSFollowUps, 0);
	private FixedLengthStringData[][] wsaaSFupcode = FLSDArrayPartOfArrayStructure(3, wsaaSFups, 0);
	private FixedLengthStringData[] wsaaSSpecTerms = FLSDArrayPartOfArrayStructure(30, wsaaSpecificRules, 37);
	private FixedLengthStringData[][] wsaaSTerms = FLSDArrayPartOfArrayStructure(10, 3, wsaaSSpecTerms, 0);
	private FixedLengthStringData[][] wsaaSOpcda = FLSDArrayPartOfArrayStructure(2, wsaaSTerms, 0);
	private FixedLengthStringData[][] wsaaSLextflag = FLSDArrayPartOfArrayStructure(1, wsaaSTerms, 2);

		/* WSAA-GENERIC-RULE-DTLS */
	private FixedLengthStringData[] wsaaGenericRules = FLSInittedArray (20, 67);
	private FixedLengthStringData[] wsaaGRule = FLSDArrayPartOfArrayStructure(4, wsaaGenericRules, 0);
	private ZonedDecimalData[] wsaaGSeqnumb = ZDArrayPartOfArrayStructure(3, 0, wsaaGenericRules, 4, UNSIGNED_TRUE);
	private FixedLengthStringData[] wsaaGFollowUps = FLSDArrayPartOfArrayStructure(30, wsaaGenericRules, 7);
	private FixedLengthStringData[][] wsaaGFups = FLSDArrayPartOfArrayStructure(10, 3, wsaaGFollowUps, 0);
	private FixedLengthStringData[][] wsaaGFupcode = FLSDArrayPartOfArrayStructure(3, wsaaGFups, 0);
	private FixedLengthStringData[] wsaaGSpecTerms = FLSDArrayPartOfArrayStructure(30, wsaaGenericRules, 37);
	private FixedLengthStringData[][] wsaaGTerms = FLSDArrayPartOfArrayStructure(10, 3, wsaaGSpecTerms, 0);
	private FixedLengthStringData[][] wsaaGOpcda = FLSDArrayPartOfArrayStructure(2, wsaaGTerms, 0);
	private FixedLengthStringData[][] wsaaGLextflag = FLSDArrayPartOfArrayStructure(1, wsaaGTerms, 2);

		/* WSAA-COMMON-RULE-DTLS */
	private FixedLengthStringData[] wsaaCommonRules = FLSInittedArray (20, 67);
	private FixedLengthStringData[] wsaaCRule = FLSDArrayPartOfArrayStructure(4, wsaaCommonRules, 0);
	private ZonedDecimalData[] wsaaCSeqnumb = ZDArrayPartOfArrayStructure(3, 0, wsaaCommonRules, 4, UNSIGNED_TRUE);
	private FixedLengthStringData[] wsaaCFollowUps = FLSDArrayPartOfArrayStructure(30, wsaaCommonRules, 7);
	private FixedLengthStringData[][] wsaaCFups = FLSDArrayPartOfArrayStructure(10, 3, wsaaCFollowUps, 0);
	private FixedLengthStringData[][] wsaaCFupcode = FLSDArrayPartOfArrayStructure(3, wsaaCFups, 0);
	private FixedLengthStringData[] wsaaCSpecTerms = FLSDArrayPartOfArrayStructure(30, wsaaCommonRules, 37);
	private FixedLengthStringData[][] wsaaCTerms = FLSDArrayPartOfArrayStructure(10, 3, wsaaCSpecTerms, 0);
	private FixedLengthStringData[][] wsaaCOpcda = FLSDArrayPartOfArrayStructure(2, wsaaCTerms, 0);
	private FixedLengthStringData[][] wsaaCLextflag = FLSDArrayPartOfArrayStructure(1, wsaaCTerms, 2);

		/*        05  WSAA-C-LETTER-TYPES.                                 
		            07  WSAA-C-LETTYPES               OCCURS 5.          
		 Constants.*/
	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
		/* TABLES */
	private static final String t5551 = "T5551";
	private static final String t5606 = "T5606";
	private static final String t5608 = "T5608";
	private static final String t5657 = "T5657";
	private static final String t5661 = "T5661";
	private static final String t5671 = "T5671";
	private static final String t6772 = "T6772";
	private static final String t6779 = "T6779";
		/* ERRORS */
	private static final String e557 = "E557";
		/* FORMATS */
	private static final String covtlnbrec = "COVTLNBREC";
	private static final String descrec = "DESCREC";
	private static final String fluplnbrec = "FLUPLNBREC";
	private static final String flupundrec = "FLUPUNDREC";
	private static final String fuperec = "FUPEREC";
	private static final String itemrec = "ITEMREC";
	private static final String lextrec = "LEXTREC";
	private static final String undcrec = "UNDCREC";
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FluplnbTableDAM fluplnbIO = new FluplnbTableDAM();
	private FlupundTableDAM flupundIO = new FlupundTableDAM();
	private FupeTableDAM fupeIO = new FupeTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	private UndcTableDAM undcIO = new UndcTableDAM();
	private UndqTableDAM undqIO = new UndqTableDAM();
	private T5551rec t5551rec = new T5551rec();
	private T5606rec t5606rec = new T5606rec();
	private T5608rec t5608rec = new T5608rec();
	private T5657rec t5657rec = new T5657rec();
	private T5661rec t5661rec = new T5661rec();
	private T5671rec t5671rec = new T5671rec();
	private T6772rec t6772rec = new T6772rec();
	private T6779rec t6779rec = new T6779rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Undwsubrec undwsubrec = new Undwsubrec();
	private WsaaNumericsInner wsaaNumericsInner = new WsaaNumericsInner();
	private boolean undwjpnFlag = false;
	private static final String  Undwjpn_FEATURE_ID = "UNWRT002";
	private boolean uwFlag = false;
	private UwQuestionnaireUtil uwQuestionnaireUtil = getApplicationContext().getBean("uwQuestionnaireUtil", UwQuestionnaireUtil.class);
	private UWQuestionnaireRec uwQuestionnaireRec = null;
	private UWQuestionnaireRec uwQuestionnaireRecReturn =null;
	//IBPLIFE-569
	private UndcpfDAO undcpfDAO = getApplicationContext().getBean("undcpfDAO", UndcpfDAO.class);
	private UndqpfDAO undqpfDAO = getApplicationContext().getBean("undqpfDAO", UndqpfDAO.class);
	private Undqpf undqpf = new Undqpf();
	private Undcpf undcpf = new Undcpf();

	private List<Itdmpf> itdmlist= new LinkedList<Itdmpf>();
	private ItdmpfDAO itdmdao=getApplicationContext().getBean("itdmdao", ItdmpfDAO.class);
	private Itdmpf itdmpf = new Itdmpf();
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit769, 
	}

	public Undwsubr() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		undwsubrec.undwsubRec = convertAndSetParam(undwsubrec.undwsubRec, parmArray, 0);
		try {
			main100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main100()
	{
		init110();
		exit190();
	}

protected void init110()
	{
		undwsubrec.statuz.set(varcom.oK);
		wsaaNumericsInner.subg.set(ZERO);
		wsaaNumericsInner.subs.set(ZERO);
		undwjpnFlag = FeaConfg.isFeatureExist(undwsubrec.chdrcoy.toString(), Undwjpn_FEATURE_ID, appVars, "IT");
		/* Initialise Working Storage arrays.*/
		for (wsaaNumericsInner.subx.set(1); !(isGT(wsaaNumericsInner.subx, 20)); wsaaNumericsInner.subx.add(1)){
			wsaaGRule[wsaaNumericsInner.subx.toInt()].set(SPACES);
			wsaaGSeqnumb[wsaaNumericsInner.subx.toInt()].set(ZERO);
			wsaaSRule[wsaaNumericsInner.subx.toInt()].set(SPACES);
			for (wsaaNumericsInner.sub.set(1); !(isGT(wsaaNumericsInner.sub, 10)); wsaaNumericsInner.sub.add(1)){
				wsaaCFupcode[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()].set(SPACES);
				wsaaCOpcda[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()].set(SPACES);
				wsaaGFupcode[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()].set(SPACES);
				wsaaGOpcda[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()].set(SPACES);
				wsaaGLextflag[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()].set(SPACES);
				wsaaSFupcode[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()].set(SPACES);
				wsaaSOpcda[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()].set(SPACES);
			}
			/**       PERFORM VARYING SUB FROM 1 BY 1 UNTIL SUB > 5             */
			/**          MOVE SPACES           TO WSAA-C-LTYPE(SUBX, SUB)       */
			/**          MOVE SPACES           TO WSAA-C-CROLE(SUBX, SUB)       */
			/**          MOVE SPACES           TO WSAA-G-LTYPE(SUBX, SUB)       */
			/**          MOVE SPACES           TO WSAA-G-CROLE(SUBX, SUB)       */
			/**          MOVE SPACES           TO WSAA-S-LTYPE(SUBX, SUB)       */
			/**          MOVE SPACES           TO WSAA-S-CROLE(SUBX, SUB)       */
			/**       END-PERFORM                                               */
		}
		wsaaNumericsInner.wsaaTime.set(getCobolTime());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError6000();
		}
		syserrrec.subrname.set(wsaaSubrname);
		if (isNE(undwsubrec.bmirule,SPACES)) {
			retrieveBmiRule200();
		}
		uwFlag = FeaConfg.isFeatureExist(undwsubrec.chdrcoy.toString().trim(), "NBPRP123", appVars, smtpfxcpy.item.toString());
		componentRules300();
		questionRules400();
		determineOverallRule500();
		processSpecificRules900();
	}

protected void exit190()
	{
		goBack();
	}

	/**
	* <pre>
	* The BMI rule if there is one will be passed through linkage.
	* Read T6772 to retrieve the details relating to this rule i.e.
	* any follow up, special term or letter request records which
	* must be triggered as a result of this rule being applied.
	* If the rule is generic then move the details to the GENERIC W-S
	* array, if specific then move them to the SPECIFIC W-S array.
	* </pre>
	*/
protected void retrieveBmiRule200()
	{
		init210();
	}

protected void init210()
	{
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		wsaaItemitem.set(undwsubrec.bmirule);
		wsaaItemtabl.set(t6772);
		x200ReadItdm();
		if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
			t6772rec.t6772Rec.set(itdmIO.getGenarea());
			wsaaItemtabl.set(t6779);
			wsaaItemitem.set(t6772rec.ruletype);
			x100ReadItem();
			t6779rec.t6779Rec.set(itemIO.getGenarea());
			wsaaUwRule.set(undwsubrec.bmirule);
			if (isNE(t6779rec.gencrule,SPACES)) {
				z100MoveGenericDtls();
			}
			else {
				z200MoveSpecificDtls();
			}
		}
	}

	/**
	* <pre>
	* Retrieve any component level underwriting details from UNDCPF.
	* </pre>
	*/
protected void componentRules300()
	{
		/*INIT*/
		//IBPLIFE-569
		/*undcIO.setRecKeyData(SPACES);
		undcIO.setRecNonKeyData(SPACES);
		initialize(undcIO.getStatuz());
		undcIO.setChdrcoy(undwsubrec.chdrcoy);
		undcIO.setChdrnum(undwsubrec.chdrnum);
		undcIO.setLife(undwsubrec.life);
		undcIO.setCoverage("00");
		undcIO.setRider("00");
		undcIO.setFormat(undcrec);
		undcIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		undcIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		undcIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");
		y100Undcio();
		while ( !(isEQ(undcIO.getStatuz(),varcom.endp))) {
			callUndcio350();
		}
*/		
	undcpf.setChdrcoy(undwsubrec.chdrcoy.toString().charAt(0));
	undcpf.setChdrnum(undwsubrec.chdrnum.toString());
	undcpf.setLife(undwsubrec.life.toString());
	undcpf.setCoverage("00");
	undcpf.setRider("00");
	List <Undcpf> undcpfList;
	undcpfList = undcpfDAO.getUndcpfRecord(undcpf.getChdrcoy().toString(),undcpf.getChdrnum().toString(), undcpf.getLife());
	if(undcpfList == null ){
		fatalError6000();  
	}
	if(undcpfList.size() > 0) {
		for (Undcpf undc : undcpfList) {	
			wsaaItemitem.set(undc.getUndwrule());
			wsaaItemtabl.set(t6772);
			x200ReadItdm();
			if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
				t6772rec.t6772Rec.set(itdmIO.getGenarea());
				wsaaUwRule.set(undc.getUndwrule());
				wsaaItemtabl.set(t6779);
				wsaaItemitem.set(t6772rec.ruletype);
				x100ReadItem();
				t6779rec.t6779Rec.set(itemIO.getGenarea());
				if (isNE(t6779rec.gencrule,SPACES)) {
					z100MoveGenericDtls();
				}
				else {
					z200MoveSpecificDtls();
				}
			}
			
		}
	 }
	}

	/**
	* <pre>
	* For every underwriting component record found, read T6772 to
	* retrieve the related underwriting rules. If the rule is generic
	* move the details to the GENERIC W-S array, if specific move
	* them to the SPECIFIC W-S array.
	* </pre>
	*/
/*protected void callUndcio350()
	{
		init351();
	}

protected void init351()
	{
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		initialize(itemIO.getStatuz());
		wsaaItemitem.set(undcIO.getUndwrule());
		wsaaItemtabl.set(t6772);
		x200ReadItdm();
		if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
			t6772rec.t6772Rec.set(itdmIO.getGenarea());
			wsaaUwRule.set(undcIO.getUndwrule());
			wsaaItemtabl.set(t6779);
			wsaaItemitem.set(t6772rec.ruletype);
			x100ReadItem();
			t6779rec.t6779Rec.set(itemIO.getGenarea());
			if (isNE(t6779rec.gencrule,SPACES)) {
				z100MoveGenericDtls();
			}
			else {
				z200MoveSpecificDtls();
			}
		}
		// Read the next underwriting component record.
		undcIO.setFunction(varcom.nextr);
		y100Undcio();
	}
	*/

	/**
	* <pre>
	* Read through every u/w question record (UNDQPF) relating to the
	* relevant life.
	* </pre>
	*/
protected void questionRules400()
	{
		/*INIT*/
	/*/*IBPLIFE-569
		  undqIO.setRecKeyData(SPACES);
		undqIO.setRecNonKeyData(SPACES);
		initialize(undqIO.getStatuz());
		undqIO.setChdrcoy(undwsubrec.chdrcoy);
		undqIO.setChdrnum(undwsubrec.chdrnum);
		undqIO.setLife(undwsubrec.life);
		undqIO.setJlife(undwsubrec.jlife);
		undqIO.setFormat(undqrec);
		undqIO.setFunction(varcom.begn);
		*/	
		
	    undqpf.setChdrcoy(undwsubrec.chdrcoy.toString());
		undqpf.setChdrnum(undwsubrec.chdrnum.toString());
		undqpf.setLife(undwsubrec.life.toString());
		undqpf.setJlife(undwsubrec.jlife.toString());
		List<Undqpf> undqpfList = undqpfDAO.searchUndqRecord(undqpf.getChdrcoy(),undqpf.getChdrnum(),undqpf.getLife(),undqpf.getJlife());
		if(undqpfList == null ){
			fatalError6000();  
		}
		if(undqpfList.size() > 0) {
			for (Undqpf undq : undqpfList) {				
				//initialize(itemIO.getRecKeyData());
				//initialize(itemIO.getRecNonKeyData());
				wsaaItemitem.set(undq.getUndwrule());
				wsaaItemtabl.set(t6772);
				x200ReadItdm();
				if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
					t6772rec.t6772Rec.set(itdmIO.getGenarea());
					wsaaUwRule.set(undq.getUndwrule());
					wsaaItemtabl.set(t6779);
					wsaaItemitem.set(t6772rec.ruletype);
					x100ReadItem();
					t6779rec.t6779Rec.set(itemIO.getGenarea());
					if (isNE(t6779rec.gencrule,SPACES)) {
						z100MoveGenericDtls();
					}
					else {
						z200MoveSpecificDtls();
					}
				}
			}

		} 
		/*EXIT*/
	}

	/**
	* <pre>
	* For each question there MAY be an associated u/w rule. For each
	* rule found, read T6772 to retrieve the corresponding rule
	* details. If the rule is generic move the details to the GENERIC
	* WS array, if specific move them to the SPECIFIC W-S array.
	* </pre>
	*/
/* protected void callUndqio450()
	{
		init451();
	}

protected void init451()
	{
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		wsaaItemitem.set(undqIO.getUndwrule());
		wsaaItemtabl.set(t6772);
		x200ReadItdm();
		if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
			t6772rec.t6772Rec.set(itdmIO.getGenarea());
			wsaaUwRule.set(undqIO.getUndwrule());
			wsaaItemtabl.set(t6779);
			wsaaItemitem.set(t6772rec.ruletype);
			x100ReadItem();
			t6779rec.t6779Rec.set(itemIO.getGenarea());
			if (isNE(t6779rec.gencrule,SPACES)) {
				z100MoveGenericDtls();
			}
			else {
				z200MoveSpecificDtls();
			}
		}

		// Read the next underwriting question record.
		undqIO.setFunction(varcom.nextr);
		y200Undqio();
	}
*/

	/**
	* <pre>
	* This section processes the GENERIC W-S array to work out the
	* OVERALL generic rule.
	* How to work out the OVERALL generic rule :
	* Each rule has a sequence number which is stored on T6772 and
	* also in the WS array.
	* The rule which has the LOWEST sequence number is the OVERALL
	* generic rule.
	* </pre>
	*/
protected void determineOverallRule500()
	{
		init510();
	}

protected void init510()
	{
		wsaaOverallRule.set(SPACES);
		wsaaNumericsInner.wsaaOverallSeqnumb.set(ZERO);
		for (wsaaNumericsInner.subx.set(1); !(isGT(wsaaNumericsInner.subx, 40)
		|| isEQ(wsaaGRule[wsaaNumericsInner.subx.toInt()], SPACES)); wsaaNumericsInner.subx.add(1)){
			if (isEQ(wsaaNumericsInner.subx, 1)) {
				wsaaOverallRule.set(wsaaGRule[wsaaNumericsInner.subx.toInt()]);
				wsaaNumericsInner.wsaaOverallSeqnumb.set(wsaaGSeqnumb[wsaaNumericsInner.subx.toInt()]);
			}
			else {
				if (isLT(wsaaGSeqnumb[wsaaNumericsInner.subx.toInt()], wsaaNumericsInner.wsaaOverallSeqnumb)) {
					wsaaOverallRule.set(wsaaGRule[wsaaNumericsInner.subx.toInt()]);
					wsaaNumericsInner.wsaaOverallSeqnumb.set(wsaaGSeqnumb[wsaaNumericsInner.subx.toInt()]);
				}
			}
		}
		undwsubrec.ovrrule.set(wsaaOverallRule);
		/* Once the overall generic rule has been identified create any*/
		/* automatic follow ups, special terms and letter requests which*/
		/* are applicable to the overall rule.*/
		for (wsaaNumericsInner.subx.set(1); !(isGT(wsaaNumericsInner.subx, 20)); wsaaNumericsInner.subx.add(1)){
			if (isEQ(wsaaOverallRule, wsaaGRule[wsaaNumericsInner.subx.toInt()])) {
				for (wsaaNumericsInner.sub.set(1); !(isGT(wsaaNumericsInner.sub, 10)); wsaaNumericsInner.sub.add(1)){
					wsaaCFupcode[1][wsaaNumericsInner.sub.toInt()].set(wsaaGFupcode[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()]);
					wsaaCOpcda[1][wsaaNumericsInner.sub.toInt()].set(wsaaGOpcda[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()]);
			}
				/*          PERFORM VARYING SUB FROM 1 BY 1 UNTIL SUB > 5          */
				/*             MOVE WSAA-G-LTYPE(SUBX, SUB)                        */
				/*                                TO WSAA-C-LTYPE(1, SUB)          */
				/*             MOVE WSAA-G-CROLE(SUBX, SUB)                        */
				/*                                TO WSAA-C-CROLE(1, SUB)          */
				/*          END-PERFORM                                            */
				/*       PERFORM VARYING SUB FROM 1 BY 1 UNTIL SUB > 10         */
				/*          IF WSAA-C-FUPCODE(SUBX, SUB) NOT = SPACES           */
				/*             PERFORM 600-READ-FLUP                            */
				/*          END-IF                                              */
				/*       END-PERFORM                                            */
				/*       IF WSAA-C-OPCDA(SUBX, 1) NOT = SPACES                  */
				/*          PERFORM 700-CHECK-FOR-SPECIAL-TERMS                 */
				/*       END-IF                                                 */
				/*       PERFORM 800-CREATE-LETTERS                             */
				wsaaNumericsInner.subx.set(21);
			}
		}
		/* There is only ONE generic rule therefore the FUPS, LEXTS and    */
		/* LETC's have been moved to the FIRST line of the WSAA-C array.   */
		/* This is also why we are moving 1 to SUBX here.                  */
		/* (WSAA-C array is a common array used for both generic and       */
		/* specific processing).                                           */
		wsaaNumericsInner.subx.set(1);
		for (wsaaNumericsInner.sub.set(1); !(isGT(wsaaNumericsInner.sub, 10)); wsaaNumericsInner.sub.add(1)){
			if (isNE(wsaaCFupcode[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()], SPACES)) {
				readFlup600();
			}
		}
		if (isNE(wsaaCOpcda[wsaaNumericsInner.subx.toInt()][1], SPACES)) {
			checkForSpecialTerms700();
		}
	}

	/**
	* <pre>
	* Only write a follow up record if one does not already exist.
	* I.e. this could be proposal modify.
	* If a follow up record does not exist then read the FLUP file
	* again this time with a function of ENDR to pick up the most
	* recent record (with the highest follow up no.). When writing a
	* new follow up record increment the sequence no. by 1.
	* </pre>
	*/
protected void readFlup600()
	{
		init610();
	}

protected void init610()
	{
		flupundIO.setRecKeyData(SPACES);
		flupundIO.setRecNonKeyData(SPACES);
		initialize(flupundIO.getStatuz());
		flupundIO.setChdrcoy(undwsubrec.chdrcoy);
		flupundIO.setChdrnum(undwsubrec.chdrnum);
		flupundIO.setLife(undwsubrec.life);
		flupundIO.setJlife(undwsubrec.jlife);
		flupundIO.setFupcode(wsaaCFupcode[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()]);
		flupundIO.setFormat(flupundrec);
		flupundIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, flupundIO);
		if (isNE(flupundIO.getStatuz(),varcom.oK)
		&& isNE(flupundIO.getStatuz(),varcom.mrnf)) {
			/*        MOVE SYSR-STATUZ        TO FLUPUND-STATUZ                */
			/*        MOVE SYSR-PARAMS        TO FLUPUND-PARAMS                */
			syserrrec.statuz.set(flupundIO.getStatuz());
			syserrrec.params.set(flupundIO.getParams());
			fatalError6000();
		}
		if (isEQ(flupundIO.getStatuz(),varcom.mrnf)) {
			getLastFupno650();
			//ILJ-108 
			if(undwjpnFlag){
				//skip adding default followups
			}
			else{
			writeFlup660();
		 }
		}
	}

protected void getLastFupno650()
	{
		init651();
	}

protected void init651()
	{
		fluplnbIO.setRecKeyData(SPACES);
		fluplnbIO.setRecNonKeyData(SPACES);
		initialize(fluplnbIO.getStatuz());
		fluplnbIO.setChdrcoy(undwsubrec.chdrcoy);
		fluplnbIO.setChdrnum(undwsubrec.chdrnum);
		fluplnbIO.setFupno(99);
		fluplnbIO.setFormat(fluplnbrec);
		fluplnbIO.setFunction(varcom.endr);
		SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(),varcom.oK)
		&& isNE(fluplnbIO.getStatuz(),varcom.endp)) {
			/*        MOVE SYSR-STATUZ        TO FLUPUND-STATUZ                */
			/*        MOVE SYSR-PARAMS        TO FLUPUND-PARAMS                */
			syserrrec.statuz.set(flupundIO.getStatuz());
			syserrrec.params.set(flupundIO.getParams());
			fatalError6000();
		}
		if (isNE(fluplnbIO.getChdrcoy(),undwsubrec.chdrcoy)
		|| isNE(fluplnbIO.getChdrnum(),undwsubrec.chdrnum)
		|| isEQ(fluplnbIO.getStatuz(),varcom.endp)) {
			fluplnbIO.setStatuz(varcom.endp);
			wsaaNumericsInner.wsaaFupno.set(ZERO);
		}
		else {
			wsaaNumericsInner.wsaaFupno.set(fluplnbIO.getFupno());
		}
	}

	/**
	* <pre>
	* Write an underwriting follow up record.
	* </pre>
	*/
protected void writeFlup660()
	{
		init661();
	}

protected void init661()
	{
		/* Read T5661 to get the corresponding status and elapsed days     */
		/* before action, if any.                                          */
		itemIO.setStatuz(varcom.oK);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(undwsubrec.chdrcoy);
		itemIO.setItemtabl(t5661);
		/*    MOVE WSAA-C-FUPCODE(SUBX, SUB)                       <V72L11>*/
		/*                                TO ITEM-ITEMITEM.        <V72L11>*/
		wsaaT5661Lang.set(undwsubrec.language);
		wsaaT5661Fupcode.set(wsaaCFupcode[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()]);
		itemIO.setItemitem(wsaaT5661Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError6000();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setStatuz(e557);
			syserrrec.statuz.set(e557);
			syserrrec.params.set(itemIO.getParams());
			fatalError6000();
		}
		t5661rec.t5661Rec.set(itemIO.getGenarea());
		flupundIO.setRecKeyData(SPACES);
		flupundIO.setRecNonKeyData(SPACES);
		initialize(flupundIO.getStatuz());
		flupundIO.setChdrcoy(undwsubrec.chdrcoy);
		flupundIO.setChdrnum(undwsubrec.chdrnum);
		flupundIO.setTranno(undwsubrec.tranno);
		flupundIO.setFuptype("U");
		flupundIO.setLife(undwsubrec.life);
		flupundIO.setJlife(undwsubrec.jlife);
		flupundIO.setFupcode(wsaaCFupcode[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()]);
		/*    MOVE 'O'                    TO FLUPUND-FUPSTAT.              */
		/*    MOVE UNDW-EFFDATE           TO FLUPUND-FUPREMDT.             */
		flupundIO.setFupstat(t5661rec.fupstat);
		flupundIO.setUser(undwsubrec.user);
		flupundIO.setZautoind("Y");
		flupundIO.setEffdate(undwsubrec.effdate);
		/* When present, advance todays' date by T5661-Reminder-Days.      */
		if (isNE(t5661rec.zelpdays,ZERO)) {
			/*    MOVE UNDW-EFFDATE        TO DTC2-INT-DATE-1       <LA4977>*/
			datcon2rec.intDate1.set(datcon1rec.intDate);
			datcon2rec.frequency.set("DY");
			datcon2rec.freqFactor.set(t5661rec.zelpdays);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			flupundIO.setFupremdt(datcon2rec.intDate2);
		}
		else {
			flupundIO.setFupremdt(undwsubrec.effdate);
		}
		descIO.setParams(SPACES);
		wsaaItemtabl.set(t5661);
		/*    MOVE FLUPUND-FUPCODE        TO WSAA-ITEMITEM.                */
		wsaaT5661Lang.set(undwsubrec.language);
		wsaaT5661Fupcode.set(flupundIO.getFupcode());
		wsaaItemitem.set(wsaaT5661Key);
		x300ReadDesc();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			flupundIO.setFupremk(descIO.getLongdesc());
		}
		else {
			flupundIO.setFupremk(SPACES);
		}
		flupundIO.setClamnum(SPACES);
		compute(wsaaNumericsInner.wsaaFupno, 0).set(add(wsaaNumericsInner.wsaaFupno, 1));
		flupundIO.setFupno(wsaaNumericsInner.wsaaFupno);
		flupundIO.setTransactionDate(datcon1rec.intDate);
		flupundIO.setTransactionTime(wsaaNumericsInner.wsaaTime);
		/*    MOVE ZEROES                 TO FLUPUND-USER.*/
		flupundIO.setCrtdate(datcon1rec.intDate);
		flupundIO.setFuprcvd(varcom.vrcmMaxDate);
		flupundIO.setExprdate(varcom.vrcmMaxDate);
		flupundIO.setZlstupdt(varcom.vrcmMaxDate);
		flupundIO.setFormat(flupundrec);
		flupundIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, flupundIO);
		if (isNE(flupundIO.getStatuz(),varcom.oK)) {
			flupundIO.setStatuz(syserrrec.statuz);
			flupundIO.setParams(syserrrec.params);
			fatalError6000();
		}
		/*  Write the Default Extend Texts if exists.                      */
		z300WriteFupe();
	}

	/**
	* <pre>
	* Step 1 - identify the coverages & riders attached to this life
	* and store them in WS to avoid numerous IO calls.
	* Step 2 - read through all the LEXT records for the contract
	* and store them in WS. Again this will avoid numerouse IO calls.
	* The reason for this is to avoid writing duplicate LEXT records.
	* </pre>
	*/
protected void checkForSpecialTerms700()
	{
		init710();
	}

protected void init710()
	{
		/* This means that we haven't retrieved the COVT details yet.*/
		/* Only do this once to avoid numerous IO calls.*/
		if (isEQ(wsaaCovtCovr[1],SPACES)) {
			readCovt740();
		}
		if (isEQ(wsaaLextCovr[1],SPACES)) {
			readLext730();
		}
		/* Process the components in the WS array.*/
		/* For each component check whether a LEXT record for the special*/
		/* term codes attached to the u/w rule being processed exist.*/
		/* (WSAA-OPCDA(SUBX, SUB) is compared to the OPCDA stored in the*/
		/* WSAA-LEXT array).*/
		/* If not then create one (if it is allowed for the component).*/
		for (wsaaNumericsInner.covno.set(1); !(isGT(wsaaNumericsInner.covno, 30)
		|| isEQ(wsaaCovtCovr[wsaaNumericsInner.covno.toInt()], SPACES)); wsaaNumericsInner.covno.add(1)){
			for (wsaaNumericsInner.sub.set(1); !(isGT(wsaaNumericsInner.sub, 10)
			|| isEQ(wsaaCOpcda[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()], SPACES)); wsaaNumericsInner.sub.add(1)){
				wsaaLextFlag.set("N");
				for (wsaaNumericsInner.wsaaLno.set(1); !(isGT(wsaaNumericsInner.wsaaLno, 10)
				|| isEQ(wsaaLextCovr[wsaaNumericsInner.wsaaLno.toInt()], SPACES)); wsaaNumericsInner.wsaaLno.add(1)){
					if (isEQ(wsaaCOpcda[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()], wsaaLextOpcda[wsaaNumericsInner.wsaaLno.toInt()])
					&& isEQ(undwsubrec.life, wsaaLextLife[wsaaNumericsInner.wsaaLno.toInt()])
					&& isEQ(undwsubrec.jlife, wsaaLextJlife[wsaaNumericsInner.wsaaLno.toInt()])
					&& isEQ(wsaaCovtCovr[wsaaNumericsInner.covno.toInt()], wsaaLextCovr[wsaaNumericsInner.wsaaLno.toInt()])
					&& isEQ(wsaaCovtRidr[wsaaNumericsInner.covno.toInt()], wsaaLextRidr[wsaaNumericsInner.wsaaLno.toInt()])) {
						lextFound.setTrue();
					}
				}
				wsaaSpecialTermFlag.set("N");
				if (lextNotFound.isTrue()) {
					checkLextAllowed760();
				}
				if (specialTermAllowed.isTrue()) {
					writeLext780();
				}
			}
		}
	}

protected void readNextLext720()
	{
		init721();
	}

protected void init721()
	{
		if (isEQ(lextIO.getJlife(),SPACES)){
		}
		else if (isEQ(lextIO.getJlife(),"00")){
		}
		else if (isEQ(lextIO.getJlife(),"01")){
		}
		/* Move any lext records found for the component to WS.*/
		wsaaNumericsInner.wsaaLext.add(1);
		wsaaLextLife[wsaaNumericsInner.wsaaLext.toInt()].set(lextIO.getLife());
		wsaaLextJlife[wsaaNumericsInner.wsaaLext.toInt()].set(lextIO.getJlife());
		wsaaLextCovr[wsaaNumericsInner.wsaaLext.toInt()].set(lextIO.getCoverage());
		wsaaLextRidr[wsaaNumericsInner.wsaaLext.toInt()].set(lextIO.getRider());
		wsaaLextOpcda[wsaaNumericsInner.wsaaLext.toInt()].set(lextIO.getOpcda());
		wsaaLextSeqnbr[wsaaNumericsInner.wsaaLext.toInt()].set(lextIO.getSeqnbr());
		lextIO.setFunction(varcom.nextp);
		y300Lextio();
	}

protected void readLext730()
	{
		init731();
	}

protected void init731()
	{
		wsaaNumericsInner.wsaaLext.set(ZERO);
		for (wsaaNumericsInner.wsaaLno.set(1); !(isGT(wsaaNumericsInner.wsaaLno, 99)); wsaaNumericsInner.wsaaLno.add(1)){
			wsaaLextLife[wsaaNumericsInner.wsaaLno.toInt()].set(SPACES);
			wsaaLextJlife[wsaaNumericsInner.wsaaLno.toInt()].set(SPACES);
			wsaaLextCovr[wsaaNumericsInner.wsaaLno.toInt()].set(SPACES);
			wsaaLextRidr[wsaaNumericsInner.wsaaLno.toInt()].set(SPACES);
			wsaaLextOpcda[wsaaNumericsInner.wsaaLno.toInt()].set(SPACES);
			wsaaLextSeqnbr[wsaaNumericsInner.wsaaLno.toInt()].set(ZERO);
		}
		lextIO.setRecKeyData(SPACES);
		lextIO.setRecNonKeyData(SPACES);
		initialize(lextIO.getStatuz());
		lextIO.setChdrcoy(undwsubrec.chdrcoy);
		lextIO.setChdrnum(undwsubrec.chdrnum);
		lextIO.setLife(undwsubrec.life);
		lextIO.setCoverage("99");
		lextIO.setRider("99");
		lextIO.setSeqnbr(999);
		lextIO.setFormat(lextrec);
		lextIO.setFunction(varcom.endr);
		y300Lextio();
		wsaaNumericsInner.wsaaLextno.set(ZERO);
		while ( !(isEQ(lextIO.getStatuz(),varcom.endp))) {
			readNextLext720();
		}
		
	}

protected void readCovt740()
	{
		/*INIT*/
		wsaaNumericsInner.covno.set(ZERO);
		covtlnbIO.setRecKeyData(SPACES);
		covtlnbIO.setRecNonKeyData(SPACES);
		initialize(covtlnbIO.getStatuz());
		covtlnbIO.setChdrcoy(undwsubrec.chdrcoy);
		covtlnbIO.setChdrnum(undwsubrec.chdrnum);
		covtlnbIO.setLife(undwsubrec.life);
		covtlnbIO.setCoverage(ZERO);
		covtlnbIO.setRider(ZERO);
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setFormat(covtlnbrec);
		covtlnbIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		covtlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtlnbIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");
		
		y400Covtio();
		while ( !(isEQ(covtlnbIO.getStatuz(),varcom.endp))) {
			processCovt750();
		}
		
		/*EXIT*/
	}

	/**
	* <pre>
	* Once all components have been identified for the life then
	* store them in W-S to avoid numerous unnecessary IO calls.
	* </pre>
	*/
protected void processCovt750()
	{
		/*INIT*/
		wsaaNumericsInner.covno.add(1);
		/* Store the component details for future processing.*/
		wsaaCovtCovr[wsaaNumericsInner.covno.toInt()].set(covtlnbIO.getCoverage());
		wsaaCovtRidr[wsaaNumericsInner.covno.toInt()].set(covtlnbIO.getRider());
		wsaaCovtCrtable[wsaaNumericsInner.covno.toInt()].set(covtlnbIO.getCrtable());
		covtlnbIO.setFunction(varcom.nextr);
		y400Covtio();
		/*EXIT*/
	}

	/**
	* <pre>
	* Before writing a LEXT record check to see if special terms are
	* allowed for the component. This is determined from either T5551,
	* T5606 or T5608 depending upon whether the component is unit
	* linked, traditional or a regular benefit etc......
	* Only write a LEXT record if special terms are allowed !
	* </pre>
	*/
protected void checkLextAllowed760()
	{
		try {
			init761();
			readT5606765();
			readT5608766();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void init761()
	{
		/* Before writing the LEXT record, read T5671 to retrieve the*/
		/* validation key.*/
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		initialize(itemIO.getStatuz());
		wsaaItemtabl.set(t5671);
		wsaaTrancd.set(undwsubrec.batctrcde);
		wsaaCrtable.set(wsaaCovtCrtable[wsaaNumericsInner.covno.toInt()]);
		wsaaItemitem.set(wsaaT5671Key);
		x100ReadItem();
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		wsaaValidItem.set(t5671rec.edtitm[1]);
		/* We don't know what type of component we are dealing with so we*/
		/* will need to read T5551, T5606 and T5608 until we find an item.*/
		/* Once an item is found then check whether special terms are*/
		/* allowed for this component.*/
		/*    Read T5551 to obtain the Unit Linked Edit Rules.*/
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		wsaaItemtabl.set(t5551);
		wsaaCurrency.set(undwsubrec.currency);
		wsaaItemitem.set(wsaaItemKey);
		x200ReadItdm();
		if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
			t5551rec.t5551Rec.set(itdmIO.getGenarea());
			if (isEQ(t5551rec.specind,"Y")) {
				specialTermAllowed.setTrue();
			}
			goTo(GotoLabel.exit769);
		}
	}

protected void readT5606765()
	{
		wsaaValidItem.set(t5671rec.edtitm[1]);
		wsaaCurrency.set(undwsubrec.currency);
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		wsaaItemtabl.set(t5606);
		itdmIO.setItemitem(wsaaItemKey);
		x200ReadItdm();
		if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
			t5606rec.t5606Rec.set(itdmIO.getGenarea());
			if (isEQ(t5606rec.specind,"Y")) {
				specialTermAllowed.setTrue();
			}
			goTo(GotoLabel.exit769);
		}
	}

protected void readT5608766()
	{
		wsaaValidItem.set(t5671rec.edtitm[1]);
		wsaaCurrency.set(undwsubrec.currency);
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		wsaaItemtabl.set(t5608);
		itdmIO.setItemitem(wsaaItemKey);
		x200ReadItdm();
		if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
			t5608rec.t5608Rec.set(itdmIO.getGenarea());
			if (isEQ(t5608rec.specind,"Y")) {
				specialTermAllowed.setTrue();
			}
		}
	}
private void callVPMS(){
	if(uwFlag){
		uwQuestionnaireRec = new UWQuestionnaireRec();
		uwQuestionnaireRec.setTransEffdate(datcon1rec.intDate.getbigdata());
		uwQuestionnaireRec.setContractType(undwsubrec.cnttype.toString());
		uwQuestionnaireRecReturn = uwQuestionnaireUtil.getUWMessage(uwQuestionnaireRec,"E", 
				undwsubrec.chdrcoy.toString().trim(),"", wsaaComponent.toString().trim(),wsaaSpTerm.toString().trim());
		lextIO.setInsprm(uwQuestionnaireRecReturn.getOutputSplTermRateAdj().get(0));
		lextIO.setAgerate(uwQuestionnaireRecReturn.getOutputSplTermAge().get(0));
		lextIO.setOppc(uwQuestionnaireRecReturn.getOutputSplTermLoadPer().get(0));
		lextIO.setZmortpct(uwQuestionnaireRecReturn.getOutputMortPerc().get(0));
		lextIO.setZnadjperc(uwQuestionnaireRecReturn.getOutputSplTermSAPer().get(0));
		lextIO.setUwoverwrite("Y");
	}
}
	/**
	* <pre>
	* Read T5657 to retrieve default values for writing the LEXT.
	* </pre>
	*/
protected void writeLext780(){
		wsaaComponent.set(wsaaCovtCrtable[wsaaNumericsInner.covno.toInt()]);
		wsaaSpTerm.set(wsaaCOpcda[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()]);
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		wsaaItemtabl.set(t5657);
		wsaaItemitem.set(wsaaT5657Key);
		x100ReadItem();
		t5657rec.t5657Rec.set(itemIO.getGenarea());
		//--------------IBPLIFE-1491 start------------
		/* Determine the correct sequence no. before writing the LEXT*/
		/* record.*/
		wsaaNumericsInner.wsaaLastSeqnbr.set(ZERO);
		for (wsaaNumericsInner.wsaaLno.set(1); !(isGT(wsaaNumericsInner.wsaaLno, 10)
		|| isEQ(wsaaLextCovr[wsaaNumericsInner.wsaaLno.toInt()], SPACES)); wsaaNumericsInner.wsaaLno.add(1)){
			if (isEQ(wsaaLextLife[wsaaNumericsInner.wsaaLno.toInt()], undwsubrec.life)
			&& isEQ(wsaaLextCovr[wsaaNumericsInner.wsaaLno.toInt()], wsaaCovtCovr[wsaaNumericsInner.covno.toInt()])
			&& isEQ(wsaaLextRidr[wsaaNumericsInner.wsaaLno.toInt()], wsaaCovtRidr[wsaaNumericsInner.covno.toInt()])) {
				if (isGT(wsaaLextSeqnbr[wsaaNumericsInner.wsaaLno.toInt()], wsaaNumericsInner.wsaaLastSeqnbr)) {
					wsaaNumericsInner.wsaaLastSeqnbr.set(wsaaLextSeqnbr[wsaaNumericsInner.wsaaLno.toInt()]);
				}
			}
		}
		lextIO.setRecKeyData(SPACES);
		lextIO.setRecNonKeyData(SPACES);
		initialize(lextIO.getStatuz());
		lextIO.setChdrcoy(undwsubrec.chdrcoy);
		lextIO.setChdrnum(undwsubrec.chdrnum);
		lextIO.setLife(undwsubrec.life);
		lextIO.setCoverage(wsaaCovtCovr[wsaaNumericsInner.covno.toInt()]);
		lextIO.setRider(wsaaCovtRidr[wsaaNumericsInner.covno.toInt()]);
		compute(wsaaNumericsInner.wsaaLextno, 0).set(add(1, wsaaNumericsInner.wsaaLastSeqnbr));
		lextIO.setSeqnbr(wsaaNumericsInner.wsaaLextno);
		lextIO.setValidflag("1");
		lextIO.setTranno(undwsubrec.tranno);
		lextIO.setCurrfrom(undwsubrec.effdate);
		lextIO.setExtCommDate(undwsubrec.effdate);
		lextIO.setCurrto(varcom.vrcmMaxDate);
		lextIO.setExtCessDate(varcom.vrcmMaxDate);
		lextIO.setExtCessTerm(ZERO);
		lextIO.setOpcda(wsaaCOpcda[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()]);
		lextIO.setInsprm(t5657rec.insprm);
		lextIO.setAgerate(t5657rec.agerate);
		lextIO.setOppc(t5657rec.oppc);
		lextIO.setJlife(undwsubrec.jlife);
		lextIO.setSbstdl(t5657rec.sbstdl);
		lextIO.setTermid(varcom.vrcmTermid);
		lextIO.setTransactionDate(datcon1rec.intDate);
		lextIO.setTransactionTime(wsaaNumericsInner.wsaaTime);
		lextIO.setUser(ZERO);
		/* MOVE SPACE                  TO LEXT-REASIND.                 */
		lextIO.setReasind("1");
		callVPMS();
		lextIO.setFormat(lextrec);
		lextIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lextIO.getParams());
			syserrrec.statuz.set(lextIO.getStatuz());
			fatalError6000();
		}
		/* Move details of LEXT just written to WS so that another LEXT*/
		/* does not get written for the same special term code.*/
		wsaaNumericsInner.wsaaLext.add(1);
		wsaaLextLife[wsaaNumericsInner.wsaaLext.toInt()].set(lextIO.getLife());
		wsaaLextJlife[wsaaNumericsInner.wsaaLext.toInt()].set(lextIO.getJlife());
		wsaaLextCovr[wsaaNumericsInner.wsaaLext.toInt()].set(lextIO.getCoverage());
		wsaaLextRidr[wsaaNumericsInner.wsaaLext.toInt()].set(lextIO.getRider());
		wsaaLextOpcda[wsaaNumericsInner.wsaaLext.toInt()].set(lextIO.getOpcda());
		wsaaLextSeqnbr[wsaaNumericsInner.wsaaLext.toInt()].set(lextIO.getSeqnbr());
		undcIO.setRecKeyData(SPACES);
		undcIO.setRecNonKeyData(SPACES);
		initialize(undcIO.getStatuz());
		undcIO.setChdrcoy(lextIO.getChdrcoy());
		undcIO.setChdrnum(lextIO.getChdrnum());
		undcIO.setLife(lextIO.getLife());
		undcIO.setJlife(lextIO.getJlife());
		undcIO.setCoverage(lextIO.getCoverage());
		undcIO.setRider(lextIO.getRider());
		undcIO.setFormat(undcrec);
		undcIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, undcIO);
		if (isNE(undcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(undcIO.getParams());
			syserrrec.statuz.set(undcIO.getStatuz());
			fatalError6000();
		}
		undcIO.setSpecind("X");
		undcIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, undcIO);
		if (isNE(undcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(undcIO.getParams());
			syserrrec.statuz.set(undcIO.getStatuz());
			fatalError6000();
		}
	}

	/**
	* <pre>
	*800-CREATE-LETTERS SECTION.                                      
	*810-INIT.                                                        
	* Find letter types associated with generic overall underwriting  
	* rule.                                                           
	*    PERFORM VARYING SUB FROM 1 BY 1 UNTIL SUB > 5                
	*       IF WSAA-C-LTYPE(SUBX, SUB) NOT = SPACES                   
	*          MOVE SPACES           TO WSAA-CLNTNUM                  
	*                                   WSAA-OTHER-KEYS               
	*          EVALUATE WSAA-C-CROLE(SUBX, SUB)                       
	*             WHEN WSCC-DOCTOR                                    
	*                MOVE UNDW-LIFE    TO WSAA-OTHER-KEYS(1:2)        
	*                MOVE UNDW-JLIFE   TO WSAA-OTHER-KEYS(3:2)        
	*                MOVE UNDW-DOCTOR  TO WSAA-CLNTNUM                
	*                IF WSAA-CLNTNUM    = SPACES                      
	*                   MOVE E592      TO UNDW-ERROR-CODE             
	*                   GO TO 890-EXIT                                
	*                END-IF                                           
	*             WHEN OTHER                                          
	*                MOVE UNDW-CLNTNUM TO WSAA-CLNTNUM                
	*                MOVE UNDW-LIFE    TO WSAA-OTHER-KEYS(1:2)        
	*                MOVE UNDW-JLIFE   TO WSAA-OTHER-KEYS(3:2)        
	*          END-EVALUATE                                           
	*          INITIALIZE               LETCUTIREC-KEY-DATA           
	*                                   LETCUTIREC-NON-KEY-DATA       
	*                                   LETCUTI-STATUZ                
	*          MOVE PRFX-CHDR        TO LETCUTI-RDOCPFX               
	*          MOVE UNDW-CHDRCOY     TO LETCUTI-RDOCCOY               
	*          MOVE UNDW-CHDRNUM     TO LETCUTI-RDOCNUM               
	*          MOVE UNDW-TRANNO      TO LETCUTI-TRANNO                
	*          MOVE WSAA-C-LTYPE(SUBX, SUB) TO LETCUTI-LETTER-TYPE    
	*          MOVE WSAA-CLNTNUM     TO LETCUTI-CLNTNUM               
	*          MOVE 9999999          TO LETCUTI-LETTER-SEQNO          
	*          MOVE LETCUTIREC       TO LETCUTI-FORMAT                
	*          MOVE BEGN             TO LETCUTI-FUNCTION              
	*          MOVE 'N'              TO WSAA-LETTER-FLAG              
	*          PERFORM 840-CHECK-FOR-LETTER                           
	*             UNTIL LETCUTI-STATUZ = ENDP                         
	*          IF  LETTER-NOT-FOUND                                   
	*              PERFORM 850-WRITE-LETCUTI                          
	*          END-IF                                                 
	*       END-IF                                                    
	*    END-PERFORM.                                                 
	*890-EXIT.                                                        
	*    EXIT.                                                        
	*840-CHECK-FOR-LETTER SECTION.                                    
	*841-INIT.                                                        
	*    CALL 'LETCUTIIO'         USING LETCUTI-PARAMS.               
	*    IF  LETCUTI-STATUZ       NOT = O-K                           
	*    AND LETCUTI-STATUZ       NOT = ENDP                          
	*        MOVE SYSR-STATUZ        TO LETCUTI-STATUZ                
	*        MOVE SYSR-PARAMS        TO LETCUTI-PARAMS                
	*        PERFORM 6000-FATAL-ERROR                                 
	*    END-IF.                                                      
	*    IF  LETCUTI-RDOCPFX      NOT = PRFX-CHDR                     
	*    OR  LETCUTI-RDOCCOY      NOT = UNDW-CHDRCOY                  
	*    OR  LETCUTI-RDOCNUM      NOT = UNDW-CHDRNUM                  
	*    OR  LETCUTI-TRANNO       NOT = UNDW-TRANNO                   
	*    OR  LETCUTI-LETTER-TYPE  NOT = WSAA-C-LTYPE(SUBX, SUB)       
	*    OR  LETCUTI-CLNTNUM      NOT = WSAA-CLNTNUM                  
	*        MOVE ENDP               TO LETCUTI-STATUZ                
	*    END-IF.                                                      
	*    IF LETCUTI-STATUZ             = O-K                          
	*       IF WSAA-C-CROLE(SUBX, SUB) = WSCC-DOCTOR                  
	*          IF WSAA-OTHER-KEYS      = LETCUTI-OTHER-KEYS           
	*             SET LETTER-FOUND    TO TRUE                         
	*          END-IF                                                 
	*       ELSE                                                      
	*          SET LETTER-FOUND      TO TRUE                          
	*       END-IF                                                    
	*    END-IF                                                       
	*    MOVE NEXTR                  TO LETCUTI-FUNCTION.             
	*849-EXIT.                                                        
	*    EXIT.                                                        
	* Write LETC record via LETRQST.                                  
	*850-WRITE-LETCUTI SECTION.                                       
	*851-INIT.                                                        
	*    MOVE SPACES                 TO LETRQST-STATUZ.               
	*    MOVE UNDW-CHDRCOY           TO LETRQST-REQUEST-COMPANY.      
	*    MOVE WSAA-C-LTYPE(SUBX, SUB) TO LETRQST-LETTER-TYPES.        
	*    MOVE UNDW-EFFDATE           TO LETRQST-LETTER-REQUEST-DATE.  
	*    MOVE UNDW-CLNTCOY           TO LETRQST-CLNTCOY.              
	*    MOVE SPACES                 TO LETRQST-OTHER-KEYS.           
	*    EVALUATE WSAA-C-CROLE(SUBX, SUB)                             
	*       WHEN WSCC-DOCTOR                                          
	*          MOVE UNDW-LIFE        TO LETRQST-OTHER-KEYS(1:2)       
	*          MOVE UNDW-JLIFE       TO LETRQST-OTHER-KEYS(3:2)       
	*          MOVE UNDW-DOCTOR      TO LETRQST-CLNTNUM               
	*       WHEN OTHER                                                
	*          MOVE UNDW-CLNTNUM     TO LETRQST-CLNTNUM               
	*          MOVE UNDW-LIFE        TO LETRQST-OTHER-KEYS(1:2)       
	*          MOVE UNDW-JLIFE       TO LETRQST-OTHER-KEYS(3:2)       
	*    END-EVALUATE                                                 
	*    MOVE PRFX-CHDR              TO LETRQST-RDOCPFX.              
	*    MOVE UNDW-CHDRCOY           TO LETRQST-RDOCCOY.              
	*    MOVE UNDW-CHDRNUM           TO LETRQST-RDOCNUM.              
	*    MOVE UNDW-BRANCH            TO LETRQST-BRANCH.               
	*    MOVE UNDW-TRANNO            TO LETRQST-TRANNO                
	*    MOVE SPACES                 TO LETRQST-DESPNUM.              
	*    MOVE 'ADD'                  TO LETRQST-FUNCTION.             
	*    CALL 'LETRQST'           USING LETRQST-PARAMS.               
	*    IF  LETRQST-STATUZ       NOT = O-K                           
	*        MOVE LETRQST-PARAMS     TO SYSR-PARAMS                   
	*        MOVE LETRQST-STATUZ     TO SYSR-STATUZ                   
	*        PERFORM 6000-FATAL-ERROR                                 
	*    END-IF.                                                      
	*859-EXIT.                                                        
	*    EXIT.                                                        
	* Go through the working storage array for specific underwriting
	* rules and create the necessary follow ups, special term and
	* letter records.
	* </pre>
	*/
protected void processSpecificRules900()
	{
		/*INIT*/
		for (wsaaNumericsInner.subx.set(1); !(isGT(wsaaNumericsInner.subx, 20)); wsaaNumericsInner.subx.add(1)){
			checkFups950();
			if (isNE(wsaaCOpcda[wsaaNumericsInner.subx.toInt()][1], SPACES)) {
				checkForSpecialTerms700();
			}
			/**       PERFORM 800-CREATE-LETTERS                                */
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* In this section move all the specific rule details to a common
	* W-S table, then create the relevant follow ups, special terms
	* and letter requests in the same way as for generic rules.
	* </pre>
	*/
protected void checkFups950()
	{
		/*INIT*/
		for (wsaaNumericsInner.sub.set(1); !(isGT(wsaaNumericsInner.sub, 10)); wsaaNumericsInner.sub.add(1)){
			wsaaCFupcode[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()].set(wsaaSFupcode[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()]);
			wsaaCOpcda[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()].set(wsaaSOpcda[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()]);
		}
		for (wsaaNumericsInner.sub.set(1); !(isGT(wsaaNumericsInner.sub, 10)); wsaaNumericsInner.sub.add(1)){
			if (isNE(wsaaSFupcode[wsaaNumericsInner.subx.toInt()][wsaaNumericsInner.sub.toInt()], SPACES)) {
				readFlup600();
			}
		}
		/*EXIT*/
	}

protected void fatalError6000()
	{
		/*ERROR*/
		syserrrec.subrname.set(wsaaSubrname);
		undwsubrec.statuz.set(syserrrec.statuz);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		goBack();
	}

	/**
	* <pre>
	* This section performs a generic read of the ITEM logical file.
	* </pre>
	*/
protected void x100ReadItem()
	{
		x110Init();
	}

protected void x110Init()
	{
		itemIO.setItemcoy(undwsubrec.chdrcoy);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemtabl(wsaaItemtabl);
		itemIO.setItemitem(wsaaItemitem);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			itemIO.setParams(syserrrec.params);
			itemIO.setStatuz(syserrrec.statuz);
			fatalError6000();
		}
	}

	/**
	* <pre>
	* This section performs a generic read of the ITDM logical file.
	* </pre>
	*/
protected void x200ReadItdm()
	{
		x210Init();
	}

protected void x210Init()
	{
	/*
		itdmIO.setItemcoy(undwsubrec.chdrcoy);
		itdmIO.setItemtabl(wsaaItemtabl);
		itdmIO.setItemitem(wsaaItemitem);
		itdmIO.setItmfrm(undwsubrec.effdate);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setParams(syserrrec.params);
			itdmIO.setStatuz(syserrrec.statuz);
			fatalError6000();
		}
		if (isNE(itdmIO.getItemcoy(),undwsubrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),wsaaItemtabl)
		|| isNE(itdmIO.getItemitem(),wsaaItemitem)) {
			itdmIO.setStatuz(varcom.endp);
		}
	*/
		//IBPLIFE-8670
		itdmlist=itdmdao.readItdmGenarea(undwsubrec.chdrcoy.toString(), wsaaItemtabl.toString(), wsaaItemitem.toString().trim(), undwsubrec.effdate.toInt()); 
		
		if (itdmlist.isEmpty()) {
			itdmIO.setParams(syserrrec.params);
			itdmIO.setStatuz(syserrrec.statuz);
			fatalError6000(); 
		}
		if (!itdmlist.isEmpty()) { 
			itdmpf = itdmlist.get(0);
			itdmIO.setGenarea(StringUtil.rawToString(itdmpf.getGenarea()));
			itdmIO.setStatuz(varcom.oK);
			if (isNE(itdmpf.getItemcoy(),undwsubrec.chdrcoy)
					|| isNE(itdmpf.getItemtabl(),wsaaItemtabl)
					|| isNE(itdmpf.getItemitem(),wsaaItemitem)) {
				itdmIO.setStatuz(varcom.endp);
			}
		}  
		//IBPLIFE-8670
	}

protected void x300ReadDesc()
	{
		x310Init();
	}

protected void x310Init()
	{
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(undwsubrec.chdrcoy);
		descIO.setDesctabl(wsaaItemtabl);
		descIO.setLanguage(undwsubrec.language);
		descIO.setDescitem(wsaaItemitem);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setParams(syserrrec.params);
			descIO.setStatuz(syserrrec.statuz);
			fatalError6000();
		}
	}

/*protected void y100Undcio()
	{
		//Y110-INIT
		SmartFileCode.execute(appVars, undcIO);
		if (isNE(undcIO.getStatuz(),varcom.oK)
		&& isNE(undcIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(undcIO.getParams());
			syserrrec.statuz.set(undcIO.getStatuz());
			fatalError6000();
		}
		if (isNE(undcIO.getChdrcoy(),undwsubrec.chdrcoy)
		|| isNE(undcIO.getChdrnum(),undwsubrec.chdrnum)
		|| isNE(undcIO.getLife(),undwsubrec.life)) {
			undcIO.setStatuz(varcom.endp);
		}
		//Y190-EXIT
	}
 protected void y200Undqio()
	{
		//Y210-INIT
		SmartFileCode.execute(appVars, undqIO);
		if (isNE(undqIO.getStatuz(),varcom.oK)
		&& isNE(undqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(undqIO.getParams());
			syserrrec.statuz.set(undqIO.getStatuz());
			fatalError6000();
		}
		if (isNE(undqIO.getChdrcoy(),undwsubrec.chdrcoy)
		|| isNE(undqIO.getChdrnum(),undwsubrec.chdrnum)
		|| isNE(undqIO.getLife(),undwsubrec.life)
		|| isNE(undqIO.getJlife(),undwsubrec.jlife)) {
			undqIO.setStatuz(varcom.endp);
		}
		//Y290-EXIT
	}
*/
protected void y300Lextio()
	{
		/*Y310-INIT*/
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
		&& isNE(lextIO.getStatuz(),varcom.endp)) {
			lextIO.setStatuz(syserrrec.statuz);
			lextIO.setParams(syserrrec.params);
			fatalError6000();
		}
		if (isNE(lextIO.getChdrcoy(),undwsubrec.chdrcoy)
		|| isNE(lextIO.getChdrnum(),undwsubrec.chdrnum)
		|| isNE(lextIO.getLife(),undwsubrec.life)
		|| isEQ(lextIO.getStatuz(),varcom.endp)) {
			lextIO.setStatuz(varcom.endp);
			wsaaNumericsInner.wsaaLextno.set(ZERO);
		}
		else {
			if (isEQ(lextIO.getFunction(),varcom.endr)) {
				wsaaNumericsInner.wsaaLextno.set(lextIO.getSeqnbr());
			}
		}
		/*Y390-EXIT*/
	}

protected void y400Covtio()
	{
		/*Y410-INIT*/
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)
		&& isNE(covtlnbIO.getStatuz(),varcom.endp)) {
			covtlnbIO.setStatuz(syserrrec.statuz);
			covtlnbIO.setParams(syserrrec.params);
			fatalError6000();
		}
		if (isNE(covtlnbIO.getChdrcoy(),undwsubrec.chdrcoy)
		|| isNE(covtlnbIO.getChdrnum(),undwsubrec.chdrnum)
		|| isNE(covtlnbIO.getLife(),undwsubrec.life)) {
			covtlnbIO.setStatuz(varcom.endp);
		}
		/*Y490-EXIT*/
	}

	/**
	* <pre>
	* This section moves the generic u/w rule details to the GENERIC
	* W-S array.
	* </pre>
	*/
protected void z100MoveGenericDtls()
	{
		/*Z110-INIT*/
		wsaaNumericsInner.subg.add(1);
		wsaaGSeqnumb[wsaaNumericsInner.subg.toInt()].set(t6772rec.seqnumb);
		wsaaGRule[wsaaNumericsInner.subg.toInt()].set(wsaaUwRule);
		for (wsaaNumericsInner.sub.set(1); !(isGT(wsaaNumericsInner.sub, 10)); wsaaNumericsInner.sub.add(1)){
			wsaaGFupcode[wsaaNumericsInner.subg.toInt()][wsaaNumericsInner.sub.toInt()].set(t6772rec.fupcdes[wsaaNumericsInner.sub.toInt()]);
			wsaaGOpcda[wsaaNumericsInner.subg.toInt()][wsaaNumericsInner.sub.toInt()].set(t6772rec.opcda[wsaaNumericsInner.sub.toInt()]);
		}
		/*Z190-EXIT*/
	}

	/**
	* <pre>
	* This section moves the specific u/w rule details to the
	* SPECIFIC W-S array.
	* </pre>
	*/
protected void z200MoveSpecificDtls()
	{
		/*Z210-INIT*/
		wsaaNumericsInner.subs.add(1);
		wsaaSRule[wsaaNumericsInner.subs.toInt()].set(wsaaUwRule);
		for (wsaaNumericsInner.sub.set(1); !(isGT(wsaaNumericsInner.sub, 10)); wsaaNumericsInner.sub.add(1)){
			wsaaSFupcode[wsaaNumericsInner.subs.toInt()][wsaaNumericsInner.sub.toInt()].set(t6772rec.fupcdes[wsaaNumericsInner.sub.toInt()]);
			wsaaSOpcda[wsaaNumericsInner.subs.toInt()][wsaaNumericsInner.sub.toInt()].set(t6772rec.opcda[wsaaNumericsInner.sub.toInt()]);
		}
		/*Z290-EXIT*/
	}

protected void z300WriteFupe()
	{
		z310Start();
		z330NextExtendedText();
	}

protected void z310Start()
	{
		wsaaDocseq.set(ZERO);
		iy.set(ZERO);
		for (ix.set(5); !(isEQ(ix, ZERO)
		|| isNE(iy, ZERO)); ix.add(-1)){
			if (isNE(t5661rec.message[ix.toInt()], SPACES)) {
				iy.set(ix);
			}
		}
		ix.set(ZERO);
	}

protected void z330NextExtendedText()
	{
		ix.add(1);
		if (isGT(ix, 5)
		|| isGT(ix, iy)) {
			return ;
		}
		wsaaDocseq.add(1);
		fupeIO.setParams(SPACES);
		fupeIO.setStatuz(varcom.oK);
		fupeIO.setChdrcoy(flupundIO.getChdrcoy());
		fupeIO.setChdrnum(flupundIO.getChdrnum());
		fupeIO.setFupno(flupundIO.getFupno());
		fupeIO.setClamnum(SPACES);
		fupeIO.setTranno(flupundIO.getTranno());
		fupeIO.setDocseq(wsaaDocseq);
		fupeIO.setMessage(t5661rec.message[ix.toInt()]);
		fupeIO.setFunction(varcom.writr);
		fupeIO.setFormat(fuperec);
		SmartFileCode.execute(appVars, fupeIO);
		if (isNE(fupeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fupeIO.getParams());
			syserrrec.statuz.set(fupeIO.getStatuz());
			fatalError6000();
		}
		z330NextExtendedText();
		return ;
	}
/*
 * Class transformed  from Data Structure WSAA-NUMERICS--INNER
 */
private static final class WsaaNumericsInner { 
		/* WSAA-NUMERICS */
	private ZonedDecimalData covno = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData sub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData subg = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData subs = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData subx = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaFupno = new ZonedDecimalData(2, 0);
	private ZonedDecimalData wsaaLastSeqnbr = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaLext = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaLextno = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaLno = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(6, 0);
	private ZonedDecimalData wsaaOverallSeqnumb = new ZonedDecimalData(3, 0).setUnsigned();
}
}