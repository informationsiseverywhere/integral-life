/*
 * File: Treserv.java
 * Date: 30 August 2009 2:45:30
 * Author: Quipoz Limited
 * 
 * Class transformed from TRESERV.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.procedures.Anpyintcal;
import com.csc.life.contractservicing.recordstructures.Annypyintcalcrec;
import com.csc.life.diary.dataaccess.dao.ZraepfDAO;
import com.csc.life.diary.dataaccess.model.Zraepf;
import com.csc.life.newbusiness.dataaccess.CovrtrbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.tablestructures.T5585rec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.underwriting.recordstructures.Actcalcrec;
import com.csc.life.underwriting.tablestructures.T6642rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*   TRADITIONAL LIFE NET PREMIUM ACTUARIAL RESERVE CALCULATION
*                 FOR TEMPORARY ASSURANCE.
*
*   This subroutine is used as the calculation routine for
*   traditional type temporary assurance component surrender
*   values.
*
*   It will determine the Net Premium Actuarial Reserve for a
*   coverage or rider using actuarial functions calculated in
*   "ACTCALC".
*
*   The program can calculate the value for either a single
*   component or for the whole-plan.
*   This is established by examination of SURC-PLAN-SUFFIX.
*   If this is zero, then process the whole-plan, otherwise
*   process the single policy for the value passed.
*   For single component processing, the policy may be broken
*   out or it may be NOTIONAL, i.e. part of the summarised
*   record. The program establishes this before reading the
*   coverage file.
*
*   A figure will be calculated for either the single
*   policy requested or for the whole plan by accumulation
*   of the figures for all the policy records pertaining to
*   this COVERAGE/RIDER (including the summary record).
*
*   The SUM ASSURED is lifted directly from the component
*   coverage and the REVERSIONARY BONUS.
*   The factor returned from the actuarial calculations is
*   then applied to this figure to give the RESERVE.
*   Where a reserve is required for a single NOTIONAL policy,
*   the figure for the summary record is passed and this will
*   be divided by the number of policies summarised subsequently.
*
*   Processing.
*   ----------
*
*   - BEGN on the COVR file.
*
*   - Determine if single or joint life and sexes of life(s)
*     assured.
*
*   - Read T6642 to establish:
*         - Age adjustment for females.
*         - Joint life equivalent age basis (for access to T5585)
*         - Single or joint life calculation basis as
*           appropriate. This is used as the Table Item Key to
*           T6641. Read the Sum Assured basis.
*
*   - Calculate duration in force of the component.
*
*   - Calculate, as appropriate, joint life equivalent equal
*     age next birthday at component commencement, and the
*     adjusted equivalent male age for any female life.
*
*   MAIN PROCESSING LOOP.
*   --------------------
*
*   PERFORM UNTIL COVRTRB-STATUZ = ENDP
*
*   - Calculate the net premium for the component by calling
*     the subroutine "ACTCALC" with function of TNETP. The
*     information to pass to ACTCALC is:
*     Initial age = the age next birthday at commencement
*     (or equivalent equal age)
*     Final age (1) = term plus initial age
*     Final age (2) = premium cessation term plus initial age
*
*   - If duration is not integral number of years, calculate
*     the lower and higher integral years, T and T + 1.
*          e.g. Duration = 3 years 4 months, T = 3 years and
*               T + 1 = 4 years.
*     Calculate TBIGA by calling ACTCALC with relevant function,
*     and using initial age equal to age next birthday at
*     commencement plus T years. Final age(1) will be age next
*     birthday at commencement plus term. Repeat for initial
*     age equal to ANB at commencement plus T + 1 years.
*     Interpolate for the exact duration in force, between
*     BIGA at T and T + 1 years.
*
*   - In similar fashion to the above, call ACTCALC again to
*     calculate TADUE. Final age(2) will be ANB at commencement
*     plus premium term. Interpolate between values T and T + 1
*     as above. Multiply the net premium TNETP by the value of
*     TADUE. Subtract the resultant product from the value of
*     TBIGA. Multiply the result by the Sum Assured from the
*     component to give the reserve value.
*
*   - Add this to the total so far.
*
*   - If this is SINGLE COMPONENT, set COVRTRB-STATUZ to ENDP
*     and finish
*   - If this is WHOLE-PLAN, get the next coverage.
*     If this is a new COVERAGE/RIDER, set COVRTRB-STATUZ to ENDP
*     and finish otherwise return to top of MAIN PROCESSING LOOP.
*
*   END OF MAIN PROCESSING LOOP.
*   ---------------------------
*
*   SET EXIT VARIABLES.
*   ------------------
*   Set linkage variables as follows:
*
*   The accumulated reserve value is returned in
*   SURC-ACTUAL-VALUE.
*
*   To indicate the progress of processing:
*
*   - STATUS -  to    'ENDP'
*
*   - TYPE -    to    'S'
*
*****************************************************************
* </pre>
*/
public class Treserv extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "TRESERV";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaLifeInd = new FixedLengthStringData(1);
	private Validator singleLife = new Validator(wsaaLifeInd, "1");
	private Validator jointLife = new Validator(wsaaLifeInd, "2");

	private FixedLengthStringData wsaaProcessingType = new FixedLengthStringData(1);
	private Validator wholePlan = new Validator(wsaaProcessingType, "1");
	private Validator singleComponent = new Validator(wsaaProcessingType, "2");
	private Validator notionalSingleComponent = new Validator(wsaaProcessingType, "3");
	private PackedDecimalData wsaaReserveTot = new PackedDecimalData(17, 2);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();
		/* WSAA-LIFE-DETAILS */
	private FixedLengthStringData wsaaLifeCnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLifeSex = new FixedLengthStringData(1);
	private PackedDecimalData wsaaLifeDob = new PackedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaaLifeAnbCcd = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaLifeAge = new PackedDecimalData(3, 0).init(0);
		/* WSAA-JLIFE-DETAILS */
	private FixedLengthStringData wsaaJlifeCnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaJlifeSex = new FixedLengthStringData(1);
	private PackedDecimalData wsaaJlifeDob = new PackedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaaJlifeAnbCcd = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaJlifeAge = new PackedDecimalData(3, 0).init(0);
	private ZonedDecimalData wsaaAgeDifference = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaAdjustedAge = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0);
	private ZonedDecimalData wsaaDuration = new ZonedDecimalData(11, 5).init(ZERO);
	private ZonedDecimalData wsaaPcessTerm = new ZonedDecimalData(11, 5).init(ZERO);
	private ZonedDecimalData wsaaRcessTerm = new ZonedDecimalData(11, 5).init(ZERO);
	private ZonedDecimalData wsaaT = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaT1 = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaTnetp = new ZonedDecimalData(17, 9).init(ZERO);
	private ZonedDecimalData wsaaTbiga = new ZonedDecimalData(17, 9).init(ZERO);
	private ZonedDecimalData wsaaTbiga1 = new ZonedDecimalData(17, 9).init(ZERO);
	private ZonedDecimalData wsaaTadue = new ZonedDecimalData(17, 9).init(ZERO);
	private ZonedDecimalData wsaaTadue1 = new ZonedDecimalData(17, 9).init(ZERO);
	private ZonedDecimalData wsaaCalculate = new ZonedDecimalData(18, 9).init(ZERO);
	private ZonedDecimalData wsaaFraction = new ZonedDecimalData(6, 5).setUnsigned();
		/* ERRORS */
	private String g344 = "G344";
	private String g579 = "G579";
	private String g580 = "G580";
	private String h034 = "H034";
		/* TABLES */
	private String t6642 = "T6642";
	private String t5585 = "T5585";
	private String t5687 = "T5687";
	private Actcalcrec actcalcrec = new Actcalcrec();
		/*Contract Header - Surrenders*/
	private ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
		/*Client logical file*/
	private ClntTableDAM clntIO = new ClntTableDAM();
		/*COVR layout for trad. reversionary bonus*/
	private CovrtrbTableDAM covrtrbIO = new CovrtrbTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private T5585rec t5585rec = new T5585rec();
	private T6642rec t6642rec = new T6642rec();
	private Varcom varcom = new Varcom();
	private FixedLengthStringData wsaaProcessStatus = new FixedLengthStringData(1).init("1");
	private Validator firstTimeThrough = new Validator(wsaaProcessStatus, "1");
	private Validator secondTimeThrough = new Validator(wsaaProcessStatus, "2");
	private boolean endowFlag = false;
	private static final String h155 = "H155";
	private PackedDecimalData wsaaTotInt = new PackedDecimalData(18, 3);
	private List<Acblpf> acblenqListLPAE = null;
	private List<Acblpf> acblenqListLPAS = null;
	private BigDecimal accAmtLPAE = new BigDecimal(0);
	private BigDecimal accAmtLPAS = new BigDecimal(0);
	private ZraepfDAO zraepfDAO =  getApplicationContext().getBean("zraepfDAO", ZraepfDAO.class);
	private Zraepf zraepf = new Zraepf();
	private Chdrpf chdrpf;
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private Batckey wsaaBatckey = new Batckey();
	public PackedDecimalData interestAmount = new PackedDecimalData(17, 2);
	private Annypyintcalcrec annypyintcalcrec = new Annypyintcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	int counter=0;
	private String t5645 = "T5645";
	private static final String t3695 = "T3695";
	private static final String PRPRO001 = "PRPRO001"; 
	private List<Zraepf> zraepfList;
	private T5645rec t5645rec = new T5645rec();
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	private Itempf itempf = null;
	private Descpf descpf = null;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2199, 
		checkT55852250, 
		exit2299, 
		exit4299, 
		exit4399, 
		exit9090
	}

	public Treserv() {
		super();
	}

public void mainline(Object... parmArray)
	{
		srcalcpy.surrenderRec = convertAndSetParam(srcalcpy.surrenderRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

private void readZrae() {
	zraepf=zraepfDAO.getItemByContractNum(srcalcpy.chdrChdrnum.toString());
    List<String> chdrnumList = new ArrayList<String>();
    chdrnumList.add(srcalcpy.chdrChdrnum.toString());
    zraepfList = zraepfDAO.searchZraepfRecord(chdrnumList, srcalcpy.chdrChdrcoy.toString());

}

protected void mainline1000()
	{
		/*MAIN*/
		initialisation2000();
		if(firstTimeThrough.isTrue()){
			while ( !(isEQ(covrtrbIO.getStatuz(),varcom.endp))) {
				obtainInitVals3000();
				calcSurrenderValue4000();
			}
		}else {
			if (secondTimeThrough.isTrue()) {	
				if(endowFlag){
					srcalcpy.actualVal.set(ZERO);	
					readZrae();
					if(zraepfList.size() > 0)
						calcEndowment();
				}
			} else {
					syserrrec.statuz.set(h155);
					fatalError9000();
			}			
		}
		
		setExitVariables5000();
		/*EXIT*/
		exitProgram();
	}

protected void initialisation2000()
	{
		/*START*/
		endowFlag = FeaConfg.isFeatureExist("2", PRPRO001, appVars, "IT");
		syserrrec.subrname.set(wsaaSubr);
		wsaaReserveTot.set(ZERO);
		srcalcpy.status.set(varcom.oK);
		if (isEQ(srcalcpy.planSuffix,ZERO)) {
			wsaaProcessingType.set("1");
		}
		else {
			if (isLTE(srcalcpy.planSuffix,srcalcpy.polsum)) {
				wsaaProcessingType.set("3");
			}
			else {
				wsaaProcessingType.set("2");
			}
		}
		readFiles2100();
		readTablesProcess2200();
		
		/*EXIT*/
	}

private void calcEndowment() {
	chdrpf = new Chdrpf();
	chdrpf = chdrpfDAO.getChdrpf(srcalcpy.chdrChdrcoy.toString().trim(), srcalcpy.chdrChdrnum.toString());
	itempf = itempfDAO.findItemByItdm(srcalcpy.chdrChdrcoy.toString(), t5645, wsaaSubr);/* IJTI-1523 */
	if (itempf == null) {
		return;
	}
	t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));

	/*  Read T3695 description to get SACSTYPE description.*/
	
	descpf = descDAO.getdescData("IT", t3695, t5645rec.sacstype01.toString(),/* IJTI-1523 */
			srcalcpy.chdrChdrcoy.toString(), srcalcpy.language.toString());
	if (descpf == null) {
		srcalcpy.description.fill("?");
	} else {
		srcalcpy.description.set(descpf.getShortdesc());
	}
	
	acblenqListLPAE = acblpfDAO.getAcblenqRecord(srcalcpy.chdrChdrcoy.toString(), StringUtils.rightPad(srcalcpy.chdrChdrnum.toString(), 16),t5645rec.sacscode[1].toString(),t5645rec.sacstype[1].toString());
	acblenqListLPAS =acblpfDAO.getAcblenqRecord(srcalcpy.chdrChdrcoy.toString(), StringUtils.rightPad(srcalcpy.chdrChdrnum.toString(), 16),t5645rec.sacscode[2].toString(),t5645rec.sacstype[2].toString());	
	if (acblenqListLPAE != null && !acblenqListLPAE.isEmpty()) {
		accAmtLPAE = acblenqListLPAE.get(0).getSacscurbal();
		accAmtLPAE = new BigDecimal(ZERO.toString()).subtract(accAmtLPAE);	
	}
	if (acblenqListLPAS != null && !acblenqListLPAS.isEmpty()) {
		accAmtLPAS =  acblenqListLPAS.get(0).getSacscurbal();
		accAmtLPAS = new BigDecimal(ZERO.toString()).subtract(accAmtLPAS);
		}	
	
	calculateInterest();
	displayEndowment();
}

protected void readFiles2100()
	{
		try {
			start2100();
			readLifeFile2120();
		}
		catch (GOTOException e){
		}
	}

protected void start2100()
	{
		covrtrbIO.setDataArea(SPACES);
		covrtrbIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		covrtrbIO.setChdrnum(srcalcpy.chdrChdrnum);
		covrtrbIO.setLife(srcalcpy.lifeLife);
		covrtrbIO.setCoverage(srcalcpy.covrCoverage);
		covrtrbIO.setRider(srcalcpy.covrRider);
		covrtrbIO.setPlanSuffix(srcalcpy.planSuffix);
		if (notionalSingleComponent.isTrue()) {
			covrtrbIO.setPlanSuffix(0);
		}
		covrtrbIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		covrtrbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrtrbIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
	
		SmartFileCode.execute(appVars, covrtrbIO);
		if (isNE(covrtrbIO.getStatuz(),varcom.oK)
		&& isNE(covrtrbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrtrbIO.getParams());
			syserrrec.statuz.set(covrtrbIO.getStatuz());
			fatalError9000();
		}
		if (isNE(covrtrbIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(covrtrbIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(covrtrbIO.getLife(),srcalcpy.lifeLife)
		|| isNE(covrtrbIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(covrtrbIO.getRider(),srcalcpy.covrRider)
		|| isEQ(covrtrbIO.getStatuz(),varcom.endp)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(srcalcpy.chdrChdrcoy.toString());
			stringVariable1.append(srcalcpy.chdrChdrnum.toString());
			stringVariable1.append(srcalcpy.lifeLife.toString());
			stringVariable1.append(srcalcpy.covrCoverage.toString());
			stringVariable1.append(srcalcpy.covrRider.toString());
			syserrrec.params.setLeft(stringVariable1.toString());
			syserrrec.statuz.set(g344);
			fatalError9000();
		}
		if ((singleComponent.isTrue()
		&& isNE(covrtrbIO.getPlanSuffix(),srcalcpy.planSuffix))
		|| (notionalSingleComponent.isTrue()
		&& isNE(covrtrbIO.getPlanSuffix(),0))) {
			wsaaPlan.set(srcalcpy.planSuffix);
			StringBuilder stringVariable2 = new StringBuilder();
			stringVariable2.append(srcalcpy.chdrChdrcoy.toString());
			stringVariable2.append(srcalcpy.chdrChdrnum.toString());
			stringVariable2.append(srcalcpy.lifeLife.toString());
			stringVariable2.append(srcalcpy.covrCoverage.toString());
			stringVariable2.append(srcalcpy.covrRider.toString());
			stringVariable2.append(wsaaPlan.toString());
			syserrrec.params.setLeft(stringVariable2.toString());
			syserrrec.statuz.set(g344);
			fatalError9000();
		}
	}

protected void readLifeFile2120()
	{
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		lifelnbIO.setChdrnum(srcalcpy.chdrChdrnum);
		lifelnbIO.setLife(srcalcpy.lifeLife);
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),"****")) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError9000();
		}
		wsaaLifeSex.set(lifelnbIO.getCltsex());
		wsaaLifeAnbCcd.set(lifelnbIO.getAnbAtCcd());
		wsaaLifeAge.set(lifelnbIO.getAnbAtCcd());
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		lifelnbIO.setChdrnum(srcalcpy.chdrChdrnum);
		lifelnbIO.setLife(srcalcpy.lifeLife);
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),"****")
		&& isNE(lifelnbIO.getStatuz(),"MRNF")) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(lifelnbIO.getStatuz(),"MRNF")) {
			wsaaLifeInd.set("1");
			wsaaJlifeAnbCcd.set(ZERO);
			goTo(GotoLabel.exit2199);
		}
		wsaaJlifeSex.set(lifelnbIO.getCltsex());
		wsaaJlifeAnbCcd.set(lifelnbIO.getAnbAtCcd());
		wsaaJlifeAge.set(lifelnbIO.getAnbAtCcd());
		wsaaLifeInd.set("2");
	}

protected void readTablesProcess2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readT66422200();
					adjustAgeForFemales2210();
					getDescription2220();
					readT55852230();
					getAgeDifference2240();
				}
				case checkT55852250: {
					checkT55852250();
				}
				case exit2299: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readT66422200()
	{
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t6642);
		itdmIO.setItemitem(srcalcpy.crtable);
		itdmIO.setItmfrm(srcalcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t6642)
		|| isNE(itdmIO.getItemitem(),srcalcpy.crtable)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(srcalcpy.crtable);
			syserrrec.statuz.set(g579);
			fatalError9000();
		}
		else {
			t6642rec.t6642Rec.set(itdmIO.getGenarea());
		}
	}

protected void adjustAgeForFemales2210()
	{
		if (isEQ(wsaaLifeSex,"F")) {
			compute(wsaaLifeAge, 0).set(add(t6642rec.fmageadj,wsaaLifeAnbCcd));
		}
		if (jointLife.isTrue()
		&& isEQ(wsaaJlifeSex,"F")) {
			compute(wsaaJlifeAge, 0).set(add(t6642rec.fmageadj,wsaaJlifeAnbCcd));
		}
		if (singleLife.isTrue()) {
			if (isNE(t6642rec.slsumass,SPACES)) {
				actcalcrec.basis.set(t6642rec.slsumass);
			}
		}
		else {
			if (isNE(t6642rec.jlsumass,SPACES)) {
				actcalcrec.basis.set(t6642rec.jlsumass);
			}
		}
	}

protected void getDescription2220()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(srcalcpy.chdrChdrcoy);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(srcalcpy.crtable);
		descIO.setLanguage(srcalcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(h034);
			fatalError9000();
		}
		if (isEQ(descIO.getShortdesc(),SPACES)) {
			srcalcpy.description.fill("?");
		}
		else {
			srcalcpy.description.set(descIO.getShortdesc());
		}
		if (singleLife.isTrue()) {
			goTo(GotoLabel.exit2299);
		}
	}

protected void readT55852230()
	{
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5585);
		itdmIO.setItemitem(t6642rec.jleqage);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5585)
		|| isNE(itdmIO.getItemitem(),t6642rec.jleqage)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(srcalcpy.crtable);
			syserrrec.statuz.set(g580);
			fatalError9000();
		}
		else {
			t5585rec.t5585Rec.set(itdmIO.getGenarea());
		}
	}

protected void getAgeDifference2240()
	{
		if (isGT(wsaaLifeAge,wsaaJlifeAge)) {
			compute(wsaaAgeDifference, 0).set(sub(wsaaLifeAge,wsaaJlifeAge));
		}
		else {
			compute(wsaaAgeDifference, 0).set(sub(wsaaJlifeAge,wsaaLifeAge));
		}
	}

protected void checkT55852250()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,18)) {
			goTo(GotoLabel.exit2299);
		}
		if (isGT(wsaaAgeDifference,t5585rec.agedif[wsaaSub.toInt()])) {
			goTo(GotoLabel.checkT55852250);
		}
		if (isEQ(t5585rec.hghlowage,"H")) {
			if (isGT(wsaaLifeAge,wsaaJlifeAge)) {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()],wsaaLifeAge));
			}
			else {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()],wsaaJlifeAge));
			}
		}
		if (isEQ(t5585rec.hghlowage,"L")) {
			if (isLTE(wsaaLifeAge,wsaaJlifeAge)) {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()],wsaaLifeAge));
			}
			else {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()],wsaaJlifeAge));
			}
		}
	}

protected void obtainInitVals3000()
	{
		/*START*/
		calculateTerms3100();
		/*EXIT*/
	}

protected void calculateTerms3100()
	{
		getDuration3100();
		getPcessTerm3110();
		getRcessTerm3120();
	}

protected void getDuration3100()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrtrbIO.getCrrcd());
		datcon3rec.intDate2.set(srcalcpy.ptdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,"****")) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError9000();
		}
		wsaaDuration.set(datcon3rec.freqFactor);
		wsaaT.set(wsaaDuration);
		if ((setPrecision(0, 5)
		&& isNE(sub(wsaaDuration,wsaaT),0))) {
			compute(wsaaT1, 0).set(add(wsaaT,1));
		}
		else {
			wsaaT1.set(ZERO);
		}
	}

protected void getPcessTerm3110()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrtrbIO.getCrrcd());
		datcon3rec.intDate2.set(covrtrbIO.getPremCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,"****")) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError9000();
		}
		wsaaPcessTerm.set(datcon3rec.freqFactor);
	}

protected void getRcessTerm3120()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrtrbIO.getCrrcd());
		datcon3rec.intDate2.set(covrtrbIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,"****")) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError9000();
		}
		wsaaRcessTerm.set(datcon3rec.freqFactor);
		/*EXIT*/
	}

protected void calcSurrenderValue4000()
	{
		/*START*/
		getTnetp4100();
		getTbiga4200();
		getTadue4300();
		calcValue4400();
		if (singleComponent.isTrue()
		|| notionalSingleComponent.isTrue()) {
			covrtrbIO.setStatuz(varcom.endp);
		}
		else {
			readCovr6000();
		}
		/*EXIT*/
	}

protected void getTnetp4100()
	{
		start4100();
	}

protected void start4100()
	{
		if (singleLife.isTrue()) {
			actcalcrec.initialAge.set(wsaaLifeAge);
		}
		else {
			actcalcrec.initialAge.set(wsaaAdjustedAge);
		}
		compute(actcalcrec.finalAge1, 5).set(add(wsaaRcessTerm,actcalcrec.initialAge));
		compute(actcalcrec.finalAge2, 5).set(add(wsaaPcessTerm,actcalcrec.initialAge));
		actcalcrec.company.set(srcalcpy.chdrChdrcoy);
		actcalcrec.value.set(ZERO);
		actcalcrec.statuz.set(SPACES);
		actcalcrec.function.set("TNETP");
		callProgram(Actcalc.class, actcalcrec.actcalcRec);
		if (isNE(actcalcrec.statuz,"****")) {
			syserrrec.params.set(actcalcrec.actcalcRec);
			syserrrec.statuz.set(actcalcrec.statuz);
			fatalError9000();
		}
		wsaaTnetp.set(actcalcrec.value);
	}

protected void getTbiga4200()
	{
		try {
			start4200();
		}
		catch (GOTOException e){
		}
	}

protected void start4200()
	{
		if (singleLife.isTrue()) {
			compute(actcalcrec.initialAge, 0).set(add(wsaaLifeAge,wsaaT));
			compute(actcalcrec.finalAge1, 5).set(add(wsaaLifeAge,wsaaRcessTerm));
		}
		else {
			compute(actcalcrec.initialAge, 0).set(add(wsaaAdjustedAge,wsaaT));
			compute(actcalcrec.finalAge1, 5).set(add(wsaaAdjustedAge,wsaaRcessTerm));
		}
		actcalcrec.company.set(srcalcpy.chdrChdrcoy);
		actcalcrec.value.set(ZERO);
		actcalcrec.finalAge2.set(ZERO);
		actcalcrec.statuz.set(SPACES);
		actcalcrec.function.set("TBIGA");
		callProgram(Actcalc.class, actcalcrec.actcalcRec);
		if (isNE(actcalcrec.statuz,"****")) {
			syserrrec.params.set(actcalcrec.actcalcRec);
			syserrrec.statuz.set(actcalcrec.statuz);
			fatalError9000();
		}
		wsaaTbiga.set(actcalcrec.value);
		if (isEQ(wsaaT1,ZERO)) {
			goTo(GotoLabel.exit4299);
		}
		compute(wsaaFraction, 5).set(sub(wsaaDuration,wsaaT));
		if (singleLife.isTrue()) {
			compute(actcalcrec.initialAge, 0).set(add(wsaaLifeAge,wsaaT1));
			compute(actcalcrec.finalAge1, 5).set(add(wsaaLifeAge,wsaaRcessTerm));
		}
		else {
			compute(actcalcrec.initialAge, 0).set(add(wsaaAdjustedAge,wsaaT1));
			compute(actcalcrec.finalAge1, 5).set(add(wsaaAdjustedAge,wsaaRcessTerm));
		}
		actcalcrec.company.set(srcalcpy.chdrChdrcoy);
		actcalcrec.value.set(ZERO);
		actcalcrec.finalAge2.set(ZERO);
		actcalcrec.statuz.set(SPACES);
		actcalcrec.function.set("TBIGA");
		callProgram(Actcalc.class, actcalcrec.actcalcRec);
		if (isNE(actcalcrec.statuz,"****")) {
			syserrrec.params.set(actcalcrec.actcalcRec);
			syserrrec.statuz.set(actcalcrec.statuz);
			fatalError9000();
		}
		wsaaTbiga1.set(actcalcrec.value);
		compute(wsaaTbiga, 10).setRounded(add(mult(wsaaFraction,(sub(wsaaTbiga1,wsaaTbiga))),wsaaTbiga));
	}

protected void getTadue4300()
	{
		try {
			start4300();
		}
		catch (GOTOException e){
		}
	}

protected void start4300()
	{
		if (singleLife.isTrue()) {
			compute(actcalcrec.initialAge, 0).set(add(wsaaLifeAge,wsaaT));
			compute(actcalcrec.finalAge2, 5).set(add(wsaaLifeAge,wsaaPcessTerm));
		}
		else {
			compute(actcalcrec.initialAge, 0).set(add(wsaaAdjustedAge,wsaaT));
			compute(actcalcrec.finalAge2, 5).set(add(wsaaAdjustedAge,wsaaPcessTerm));
		}
		actcalcrec.company.set(srcalcpy.chdrChdrcoy);
		actcalcrec.value.set(ZERO);
		actcalcrec.finalAge1.set(ZERO);
		actcalcrec.statuz.set(SPACES);
		actcalcrec.function.set("TADUE");
		callProgram(Actcalc.class, actcalcrec.actcalcRec);
		if (isNE(actcalcrec.statuz,"****")) {
			syserrrec.params.set(actcalcrec.actcalcRec);
			syserrrec.statuz.set(actcalcrec.statuz);
			fatalError9000();
		}
		wsaaTadue.set(actcalcrec.value);
		if (isEQ(wsaaT1,ZERO)) {
			goTo(GotoLabel.exit4399);
		}
		if (singleLife.isTrue()) {
			compute(actcalcrec.initialAge, 0).set(add(wsaaLifeAge,wsaaT1));
			compute(actcalcrec.finalAge2, 5).set(add(wsaaLifeAge,wsaaPcessTerm));
		}
		else {
			compute(actcalcrec.initialAge, 0).set(add(wsaaAdjustedAge,wsaaT1));
			compute(actcalcrec.finalAge2, 5).set(add(wsaaAdjustedAge,wsaaPcessTerm));
		}
		actcalcrec.company.set(srcalcpy.chdrChdrcoy);
		actcalcrec.value.set(ZERO);
		actcalcrec.finalAge1.set(ZERO);
		actcalcrec.statuz.set(SPACES);
		actcalcrec.function.set("TADUE");
		callProgram(Actcalc.class, actcalcrec.actcalcRec);
		if (isNE(actcalcrec.statuz,"****")) {
			syserrrec.params.set(actcalcrec.actcalcRec);
			syserrrec.statuz.set(actcalcrec.statuz);
			fatalError9000();
		}
		wsaaTadue1.set(actcalcrec.value);
		compute(wsaaTadue, 10).setRounded(add(mult(wsaaFraction,(sub(wsaaTadue1,wsaaTadue))),wsaaTadue));
	}

protected void calcValue4400()
	{
		/*START*/
		compute(wsaaCalculate, 9).set(mult(wsaaTnetp,wsaaTadue));
		compute(wsaaCalculate, 9).set(sub(wsaaTbiga,wsaaCalculate));
		compute(wsaaCalculate, 9).set(mult(covrtrbIO.getSumins(),wsaaCalculate));
		wsaaReserveTot.add(wsaaCalculate);
		/*EXIT*/
	}

protected void setExitVariables5000()
	{
		
		if (firstTimeThrough.isTrue()
		|| isEQ(srcalcpy.endf,SPACES)) {
			basicCashValue1000();
			wsaaProcessStatus.set("2");
			srcalcpy.endf.set(varcom.oK);
		}
		else {
			if (secondTimeThrough.isTrue()) {		
				srcalcpy.status.set(varcom.endp);
			}
			
		}
	}

private void displayEndowment() {
	srcalcpy.actualVal.set(ZERO);	
	srcalcpy.estimatedVal.set(wsaaTotInt);
	wsaaTotInt.set(ZERO);	
	wsaaTotInt.add(accAmtLPAE.doubleValue());
	wsaaTotInt.add(accAmtLPAS.doubleValue());
	wsaaTotInt.add(interestAmount);
	srcalcpy.actualVal.set(wsaaTotInt);
	if (isLT(srcalcpy.actualVal,ZERO)) {
		srcalcpy.actualVal.set(ZERO);
	}
	srcalcpy.type.set("E");
	
	


}

private void calculateInterest() {
	annypyintcalcrec.intcalcRec.set(SPACES);
	annypyintcalcrec.chdrcoy.set(zraepf.getChdrcoy());
	annypyintcalcrec.chdrnum.set(zraepf.getChdrnum());
	annypyintcalcrec.cnttype.set(chdrpf.getCnttype());
	annypyintcalcrec.interestTo.set(srcalcpy.effdate);
	annypyintcalcrec.interestFrom.set(zraepf.getAplstintbdte());
	annypyintcalcrec.annPaymt.set(zraepf.getApcaplamt());
	annypyintcalcrec.lastCaplsnDate.set(zraepf.getAplstcapdate());		
	annypyintcalcrec.interestAmount.set(ZERO);
	annypyintcalcrec.currency.set(zraepf.getPaycurr());
	annypyintcalcrec.transaction.set("ENDOWMENT");
	
	callProgram(Anpyintcal.class, annypyintcalcrec.intcalcRec);
	if (isNE(annypyintcalcrec.statuz, varcom.oK)) {
		syserrrec.params.set(annypyintcalcrec.intcalcRec);
		syserrrec.statuz.set(annypyintcalcrec.statuz);
		fatalError9000();	
	}
	
	/* MOVE INTC-INTEREST-AMOUNT    TO ZRDP-AMOUNT-IN.              */
	/* PERFORM 5000-CALL-ROUNDING.                                  */
	/* MOVE ZRDP-AMOUNT-OUT         TO INTC-INTEREST-AMOUNT.        */
	if (isNE(annypyintcalcrec.interestAmount, 0)) {
		zrdecplrec.amountIn.set(annypyintcalcrec.interestAmount);
		callRounding5100();
		annypyintcalcrec.interestAmount.set(zrdecplrec.amountOut);
		interestAmount.add(annypyintcalcrec.interestAmount);
	}
	
}

private void callRounding5100() {
	/*CALL*/
	zrdecplrec.function.set(SPACES);
	zrdecplrec.company.set(chdrpf.getChdrcoy());
	zrdecplrec.statuz.set(varcom.oK);
	zrdecplrec.currency.set(zraepf.getPaycurr());
	zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
	callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
	if (isNE(zrdecplrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(zrdecplrec.statuz);
		fatalError9000();	
	}
	/*EXIT*/
}

private void basicCashValue1000() {
	/*START*/
	srcalcpy.actualVal.set(wsaaReserveTot);
	srcalcpy.estimatedVal.set(0);
	srcalcpy.type.set("S");
	
	/*EXIT*/
	
}

protected void readCovr6000()
	{
		/*START*/
		covrtrbIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrtrbIO);
		if (isNE(covrtrbIO.getStatuz(),varcom.oK)
		&& isNE(covrtrbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrtrbIO.getParams());
			syserrrec.statuz.set(covrtrbIO.getStatuz());
			fatalError9000();
		}
		if (isNE(covrtrbIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(covrtrbIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(covrtrbIO.getLife(),srcalcpy.lifeLife)
		|| isNE(covrtrbIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(covrtrbIO.getRider(),srcalcpy.covrRider)) {
			covrtrbIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void fatalError9000()
	{
		try {
			error9010();
		}
		catch (GOTOException e){
		}
		finally{
			exit9090();
		}
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		srcalcpy.status.set("BOMB");
	}

protected void exit9090()
	{
		exitProgram();
	}
}
