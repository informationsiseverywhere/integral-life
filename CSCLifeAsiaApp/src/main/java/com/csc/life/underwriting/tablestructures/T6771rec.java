package com.csc.life.underwriting.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:26
 * Description:
 * Copybook name: T6771REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6771rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6771Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(t6771Rec, 0);
  	public FixedLengthStringData questidfs = new FixedLengthStringData(200).isAPartOf(t6771Rec, 1);
  	public FixedLengthStringData[] questidf = FLSArrayPartOfStructure(40, 5, questidfs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(200).isAPartOf(questidfs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData questidf01 = new FixedLengthStringData(5).isAPartOf(filler, 0);
  	public FixedLengthStringData questidf02 = new FixedLengthStringData(5).isAPartOf(filler, 5);
  	public FixedLengthStringData questidf03 = new FixedLengthStringData(5).isAPartOf(filler, 10);
  	public FixedLengthStringData questidf04 = new FixedLengthStringData(5).isAPartOf(filler, 15);
  	public FixedLengthStringData questidf05 = new FixedLengthStringData(5).isAPartOf(filler, 20);
  	public FixedLengthStringData questidf06 = new FixedLengthStringData(5).isAPartOf(filler, 25);
  	public FixedLengthStringData questidf07 = new FixedLengthStringData(5).isAPartOf(filler, 30);
  	public FixedLengthStringData questidf08 = new FixedLengthStringData(5).isAPartOf(filler, 35);
  	public FixedLengthStringData questidf09 = new FixedLengthStringData(5).isAPartOf(filler, 40);
  	public FixedLengthStringData questidf10 = new FixedLengthStringData(5).isAPartOf(filler, 45);
  	public FixedLengthStringData questidf11 = new FixedLengthStringData(5).isAPartOf(filler, 50);
  	public FixedLengthStringData questidf12 = new FixedLengthStringData(5).isAPartOf(filler, 55);
  	public FixedLengthStringData questidf13 = new FixedLengthStringData(5).isAPartOf(filler, 60);
  	public FixedLengthStringData questidf14 = new FixedLengthStringData(5).isAPartOf(filler, 65);
  	public FixedLengthStringData questidf15 = new FixedLengthStringData(5).isAPartOf(filler, 70);
  	public FixedLengthStringData questidf16 = new FixedLengthStringData(5).isAPartOf(filler, 75);
  	public FixedLengthStringData questidf17 = new FixedLengthStringData(5).isAPartOf(filler, 80);
  	public FixedLengthStringData questidf18 = new FixedLengthStringData(5).isAPartOf(filler, 85);
  	public FixedLengthStringData questidf19 = new FixedLengthStringData(5).isAPartOf(filler, 90);
  	public FixedLengthStringData questidf20 = new FixedLengthStringData(5).isAPartOf(filler, 95);
  	public FixedLengthStringData questidf21 = new FixedLengthStringData(5).isAPartOf(filler, 100);
  	public FixedLengthStringData questidf22 = new FixedLengthStringData(5).isAPartOf(filler, 105);
  	public FixedLengthStringData questidf23 = new FixedLengthStringData(5).isAPartOf(filler, 110);
  	public FixedLengthStringData questidf24 = new FixedLengthStringData(5).isAPartOf(filler, 115);
  	public FixedLengthStringData questidf25 = new FixedLengthStringData(5).isAPartOf(filler, 120);
  	public FixedLengthStringData questidf26 = new FixedLengthStringData(5).isAPartOf(filler, 125);
  	public FixedLengthStringData questidf27 = new FixedLengthStringData(5).isAPartOf(filler, 130);
  	public FixedLengthStringData questidf28 = new FixedLengthStringData(5).isAPartOf(filler, 135);
  	public FixedLengthStringData questidf29 = new FixedLengthStringData(5).isAPartOf(filler, 140);
  	public FixedLengthStringData questidf30 = new FixedLengthStringData(5).isAPartOf(filler, 145);
  	public FixedLengthStringData questidf31 = new FixedLengthStringData(5).isAPartOf(filler, 150);
  	public FixedLengthStringData questidf32 = new FixedLengthStringData(5).isAPartOf(filler, 155);
  	public FixedLengthStringData questidf33 = new FixedLengthStringData(5).isAPartOf(filler, 160);
  	public FixedLengthStringData questidf34 = new FixedLengthStringData(5).isAPartOf(filler, 165);
  	public FixedLengthStringData questidf35 = new FixedLengthStringData(5).isAPartOf(filler, 170);
  	public FixedLengthStringData questidf36 = new FixedLengthStringData(5).isAPartOf(filler, 175);
  	public FixedLengthStringData questidf37 = new FixedLengthStringData(5).isAPartOf(filler, 180);
  	public FixedLengthStringData questidf38 = new FixedLengthStringData(5).isAPartOf(filler, 185);
  	public FixedLengthStringData questidf39 = new FixedLengthStringData(5).isAPartOf(filler, 190);
  	public FixedLengthStringData questidf40 = new FixedLengthStringData(5).isAPartOf(filler, 195);
  	public FixedLengthStringData queststs = new FixedLengthStringData(40).isAPartOf(t6771Rec, 201);
  	public FixedLengthStringData[] questst = FLSArrayPartOfStructure(40, 1, queststs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(40).isAPartOf(queststs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData questst01 = new FixedLengthStringData(1).isAPartOf(filler1, 0);
  	public FixedLengthStringData questst02 = new FixedLengthStringData(1).isAPartOf(filler1, 1);
  	public FixedLengthStringData questst03 = new FixedLengthStringData(1).isAPartOf(filler1, 2);
  	public FixedLengthStringData questst04 = new FixedLengthStringData(1).isAPartOf(filler1, 3);
  	public FixedLengthStringData questst05 = new FixedLengthStringData(1).isAPartOf(filler1, 4);
  	public FixedLengthStringData questst06 = new FixedLengthStringData(1).isAPartOf(filler1, 5);
  	public FixedLengthStringData questst07 = new FixedLengthStringData(1).isAPartOf(filler1, 6);
  	public FixedLengthStringData questst08 = new FixedLengthStringData(1).isAPartOf(filler1, 7);
  	public FixedLengthStringData questst09 = new FixedLengthStringData(1).isAPartOf(filler1, 8);
  	public FixedLengthStringData questst10 = new FixedLengthStringData(1).isAPartOf(filler1, 9);
  	public FixedLengthStringData questst11 = new FixedLengthStringData(1).isAPartOf(filler1, 10);
  	public FixedLengthStringData questst12 = new FixedLengthStringData(1).isAPartOf(filler1, 11);
  	public FixedLengthStringData questst13 = new FixedLengthStringData(1).isAPartOf(filler1, 12);
  	public FixedLengthStringData questst14 = new FixedLengthStringData(1).isAPartOf(filler1, 13);
  	public FixedLengthStringData questst15 = new FixedLengthStringData(1).isAPartOf(filler1, 14);
  	public FixedLengthStringData questst16 = new FixedLengthStringData(1).isAPartOf(filler1, 15);
  	public FixedLengthStringData questst17 = new FixedLengthStringData(1).isAPartOf(filler1, 16);
  	public FixedLengthStringData questst18 = new FixedLengthStringData(1).isAPartOf(filler1, 17);
  	public FixedLengthStringData questst19 = new FixedLengthStringData(1).isAPartOf(filler1, 18);
  	public FixedLengthStringData questst20 = new FixedLengthStringData(1).isAPartOf(filler1, 19);
  	public FixedLengthStringData questst21 = new FixedLengthStringData(1).isAPartOf(filler1, 20);
  	public FixedLengthStringData questst22 = new FixedLengthStringData(1).isAPartOf(filler1, 21);
  	public FixedLengthStringData questst23 = new FixedLengthStringData(1).isAPartOf(filler1, 22);
  	public FixedLengthStringData questst24 = new FixedLengthStringData(1).isAPartOf(filler1, 23);
  	public FixedLengthStringData questst25 = new FixedLengthStringData(1).isAPartOf(filler1, 24);
  	public FixedLengthStringData questst26 = new FixedLengthStringData(1).isAPartOf(filler1, 25);
  	public FixedLengthStringData questst27 = new FixedLengthStringData(1).isAPartOf(filler1, 26);
  	public FixedLengthStringData questst28 = new FixedLengthStringData(1).isAPartOf(filler1, 27);
  	public FixedLengthStringData questst29 = new FixedLengthStringData(1).isAPartOf(filler1, 28);
  	public FixedLengthStringData questst30 = new FixedLengthStringData(1).isAPartOf(filler1, 29);
  	public FixedLengthStringData questst31 = new FixedLengthStringData(1).isAPartOf(filler1, 30);
  	public FixedLengthStringData questst32 = new FixedLengthStringData(1).isAPartOf(filler1, 31);
  	public FixedLengthStringData questst33 = new FixedLengthStringData(1).isAPartOf(filler1, 32);
  	public FixedLengthStringData questst34 = new FixedLengthStringData(1).isAPartOf(filler1, 33);
  	public FixedLengthStringData questst35 = new FixedLengthStringData(1).isAPartOf(filler1, 34);
  	public FixedLengthStringData questst36 = new FixedLengthStringData(1).isAPartOf(filler1, 35);
  	public FixedLengthStringData questst37 = new FixedLengthStringData(1).isAPartOf(filler1, 36);
  	public FixedLengthStringData questst38 = new FixedLengthStringData(1).isAPartOf(filler1, 37);
  	public FixedLengthStringData questst39 = new FixedLengthStringData(1).isAPartOf(filler1, 38);
  	public FixedLengthStringData questst40 = new FixedLengthStringData(1).isAPartOf(filler1, 39);
  	public FixedLengthStringData undwsubr = new FixedLengthStringData(10).isAPartOf(t6771Rec, 241);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(249).isAPartOf(t6771Rec, 251, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6771Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6771Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}