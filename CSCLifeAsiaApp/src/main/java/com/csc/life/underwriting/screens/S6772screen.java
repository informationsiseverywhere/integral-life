package com.csc.life.underwriting.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class S6772screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 16, 1, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6772ScreenVars sv = (S6772ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6772screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6772ScreenVars screenVars = (S6772ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.language.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.fupcdes01.setClassString("");
		screenVars.fupcdes02.setClassString("");
		screenVars.fupcdes03.setClassString("");
		screenVars.fupcdes04.setClassString("");
		screenVars.fupcdes05.setClassString("");
		screenVars.fupcdes06.setClassString("");
		screenVars.fupcdes07.setClassString("");
		screenVars.fupcdes08.setClassString("");
		screenVars.fupcdes09.setClassString("");
		screenVars.fupcdes10.setClassString("");
		screenVars.opcda01.setClassString("");
		screenVars.opcda02.setClassString("");
		screenVars.opcda03.setClassString("");
		screenVars.opcda04.setClassString("");
		screenVars.opcda05.setClassString("");
		screenVars.opcda06.setClassString("");
		screenVars.opcda07.setClassString("");
		screenVars.opcda08.setClassString("");
		screenVars.opcda09.setClassString("");
		screenVars.opcda10.setClassString("");
		screenVars.ruletype.setClassString("");
		screenVars.seqnumb.setClassString("");
	}

/**
 * Clear all the variables in S6772screen
 */
	public static void clear(VarModel pv) {
		S6772ScreenVars screenVars = (S6772ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.language.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.fupcdes01.clear();
		screenVars.fupcdes02.clear();
		screenVars.fupcdes03.clear();
		screenVars.fupcdes04.clear();
		screenVars.fupcdes05.clear();
		screenVars.fupcdes06.clear();
		screenVars.fupcdes07.clear();
		screenVars.fupcdes08.clear();
		screenVars.fupcdes09.clear();
		screenVars.fupcdes10.clear();
		screenVars.opcda01.clear();
		screenVars.opcda02.clear();
		screenVars.opcda03.clear();
		screenVars.opcda04.clear();
		screenVars.opcda05.clear();
		screenVars.opcda06.clear();
		screenVars.opcda07.clear();
		screenVars.opcda08.clear();
		screenVars.opcda09.clear();
		screenVars.opcda10.clear();
		screenVars.ruletype.clear();
		screenVars.seqnumb.clear();
	}
}
