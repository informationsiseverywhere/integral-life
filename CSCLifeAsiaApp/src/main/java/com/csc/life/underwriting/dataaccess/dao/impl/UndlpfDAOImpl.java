package com.csc.life.underwriting.dataaccess.dao.impl; 

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.underwriting.dataaccess.dao.UndlpfDAO;
import com.csc.life.underwriting.dataaccess.model.Undlpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class UndlpfDAOImpl extends BaseDAOImpl<Undlpf> implements UndlpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(UndlpfDAOImpl.class);

	@Override
	public List<Undlpf> searchUndlpfRecords(Undlpf undlpfRec) {
		StringBuilder sqlUndlpfSelect1 = new StringBuilder(
                "SELECT CHDRCOY, CHDRNUM, LIFE, JLIFE, CURRFROM, CURRTO, HEIGHT, WEIGHT, BMI, BMIBASIS, BMIRULE, OVRRULE, CLNTNUM01, CLNTNUM02, CLNTNUM03, CLNTNUM04, CLNTNUM05, TRANNO, UNDWFLAG, VALIDFLAG,UNIQUE_NUMBER");
        sqlUndlpfSelect1
                .append(" FROM VM1DTA.UNDLPF WHERE CHDRCOY= ? AND CHDRNUM = ? AND LIFE= ?");

        sqlUndlpfSelect1
                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, UNIQUE_NUMBER DESC ");

        PreparedStatement psUndlpfSelect = getPrepareStatement(sqlUndlpfSelect1.toString());
        ResultSet sqlUndlpf1rs = null;
        List<Undlpf> undlpfList = new ArrayList<Undlpf>();
        try {
        	psUndlpfSelect.setString(1, undlpfRec.getChdrcoy());
        	psUndlpfSelect.setString(2, undlpfRec.getChdrnum());
        	psUndlpfSelect.setString(3, undlpfRec.getLife());
            sqlUndlpf1rs = executeQuery(psUndlpfSelect);
            while (sqlUndlpf1rs.next()) {
            	Undlpf undlpf = new Undlpf();
            	undlpf.setChdrcoy(sqlUndlpf1rs.getString(1));
            	undlpf.setChdrnum(sqlUndlpf1rs.getString(2));
            	undlpf.setLife(sqlUndlpf1rs.getString(3));
            	undlpf.setJlife(sqlUndlpf1rs.getString(4));
            	undlpf.setCurrfrom(sqlUndlpf1rs.getInt(5));
            	undlpf.setCurrto(sqlUndlpf1rs.getInt(6));
            	undlpf.setHeight(sqlUndlpf1rs.getInt(7));//IBPLIFE-8671
            	undlpf.setWeight(sqlUndlpf1rs.getInt(8));//IBPLIFE-8671
            	undlpf.setBmi(sqlUndlpf1rs.getBigDecimal(9));
            	undlpf.setBmibasis(sqlUndlpf1rs.getString(10));
            	undlpf.setBmirule(sqlUndlpf1rs.getString(11));
            	undlpf.setOvrrule(sqlUndlpf1rs.getString(12));
            	undlpf.setClntnum01(sqlUndlpf1rs.getString(13));
            	undlpf.setClntnum02(sqlUndlpf1rs.getString(14));
            	undlpf.setClntnum03(sqlUndlpf1rs.getString(15));
            	undlpf.setClntnum04(sqlUndlpf1rs.getString(16));
            	undlpf.setClntnum05(sqlUndlpf1rs.getString(17));
            	undlpf.setTranno(sqlUndlpf1rs.getInt(18));//  IBPLIFE-8671
            	undlpf.setUndwflag(sqlUndlpf1rs.getString(19));
            	undlpf.setValidflag(sqlUndlpf1rs.getString(20));
            	undlpfList.add(undlpf);
                }
            } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(psUndlpfSelect, sqlUndlpf1rs);
        }
        return undlpfList;

	}
	
	
	@Override
	public List<? extends Undlpf> checkUndlpf(String chdrcoy ,String chdrnum) {
		
		StringBuilder sqlUndlpf = new StringBuilder("SELECT UNIQUE_NUMBER, UNDWFLAG, LIFE, JLIFE, VALIDFLAG, TRANNO"); //ILIFE-8709
  		sqlUndlpf.append(" FROM UNDL ");
  		sqlUndlpf.append(" WHERE CHDRCOY = ? AND CHDRNUM =? ");
        PreparedStatement psUndlpf = getPrepareStatement(sqlUndlpf.toString());
        ResultSet undlpf1rs = null;
        List<Undlpf> undlpfList = new ArrayList<Undlpf>();
      
        try {
        	psUndlpf.setString(1,chdrcoy);
        	psUndlpf.setString(2,chdrnum);
        	
        	undlpf1rs = psUndlpf.executeQuery();
            while (undlpf1rs.next()) {
            	Undlpf undlpf= new Undlpf();
            	
            	undlpf.setUniqueNumber(undlpf1rs.getLong("UNIQUE_NUMBER")); //ILIFE-8709
            	undlpf.setUndwflag(undlpf1rs.getString("UNDWFLAG"));
            	undlpf.setLife(undlpf1rs.getString("LIFE"));
            	undlpf.setJlife(undlpf1rs.getString("JLIFE"));
            	undlpf.setValidflag(undlpf1rs.getString("VALIDFLAG")); //ILIFE-8709
            	undlpf.setTranno(undlpf1rs.getInt("TRANNO")); //ILIFE-8709 IBPLIFE-8671
            	undlpfList.add(undlpf);
         	}

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(psUndlpf, undlpf1rs);
        }
        return undlpfList;
	}
	public List<Undlpf> searchUndlpfList(String chdrcoy ,String chdrnum,String life, String jlife) {
		StringBuilder sqlUndlpfSelect1 = new StringBuilder(
                "SELECT CHDRCOY, CHDRNUM, LIFE, JLIFE, CURRFROM, CURRTO, HEIGHT, WEIGHT, BMI, BMIBASIS, BMIRULE, OVRRULE, CLNTNUM01, CLNTNUM02, CLNTNUM03, CLNTNUM04, CLNTNUM05, TRANNO, UNDWFLAG, VALIDFLAG");
        sqlUndlpfSelect1.append(" FROM VM1DTA.UNDLPF WHERE CHDRCOY= ? AND CHDRNUM =? AND LIFE= ? AND JLIFE=?");

        sqlUndlpfSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, UNIQUE_NUMBER DESC ");

        PreparedStatement psUndlpfSelect = getPrepareStatement(sqlUndlpfSelect1.toString());
        ResultSet sqlUndlpf1rs = null;
        Undlpf undlpf = null;
        List<Undlpf> undlpfList = new ArrayList<Undlpf>();
        try {
        	psUndlpfSelect.setString(1,chdrcoy);
        	psUndlpfSelect.setString(2,chdrnum);
        	psUndlpfSelect.setString(3,life);
        	psUndlpfSelect.setString(4,jlife);
            sqlUndlpf1rs = executeQuery(psUndlpfSelect);
            while (sqlUndlpf1rs.next()) {
            	undlpf = new Undlpf();
            	undlpf.setChdrcoy(sqlUndlpf1rs.getString(1));
            	undlpf.setChdrnum(sqlUndlpf1rs.getString(2));
            	undlpf.setLife(sqlUndlpf1rs.getString(3));
            	undlpf.setJlife(sqlUndlpf1rs.getString(4));
            	undlpf.setCurrfrom(sqlUndlpf1rs.getInt(5));
            	undlpf.setCurrto(sqlUndlpf1rs.getInt(6));
            	undlpf.setHeight(sqlUndlpf1rs.getInt(7));//IBPLIFE-8671
            	undlpf.setWeight(sqlUndlpf1rs.getInt(8));//IBPLIFE-8671
            	undlpf.setBmi(sqlUndlpf1rs.getBigDecimal(9));
            	undlpf.setBmibasis(sqlUndlpf1rs.getString(10));
            	undlpf.setBmirule(sqlUndlpf1rs.getString(11));
            	undlpf.setOvrrule(sqlUndlpf1rs.getString(12));
            	undlpf.setClntnum01(sqlUndlpf1rs.getString(13));
            	undlpf.setClntnum02(sqlUndlpf1rs.getString(14));
            	undlpf.setClntnum03(sqlUndlpf1rs.getString(15));
            	undlpf.setClntnum04(sqlUndlpf1rs.getString(16));
            	undlpf.setClntnum05(sqlUndlpf1rs.getString(17));
            	undlpf.setTranno(sqlUndlpf1rs.getInt(18));//IBPLIFE-8671
            	undlpf.setUndwflag(sqlUndlpf1rs.getString(19));
            	undlpf.setValidflag(sqlUndlpf1rs.getString(20));
            	undlpfList.add(undlpf);
                }
            } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(psUndlpfSelect, sqlUndlpf1rs);
        }
        return undlpfList;

	}

	@Override
	public void updateUndlTranno(List<? extends Undlpf> undlList) {
		 if (undlList == null || undlList.isEmpty()) {
	            return;
	        }
	        String sql = " UPDATE UNDLPF SET VALIDFLAG = ?, TRANNO = ?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER = ? ";
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try {
	            ps = getPrepareStatement(sql);
	            int ctr;
	            for (Undlpf undl : undlList) {
	                ctr = 0;
	                ps.setString(++ctr, undl.getValidflag());
	                ps.setInt(++ctr, undl.getTranno());//IBPLIFE-8671
	                ps.setString(++ctr, getJobnm());
	                ps.setString(++ctr, getUsrprf());
	                ps.setTimestamp(++ctr, getDatime());
	                ps.setLong(++ctr, undl.getUniqueNumber());
	                ps.addBatch();
	            }
	            ps.executeBatch();
	        } catch (SQLException e) {
	            LOGGER.error("updateUndlTranno()" , e);
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, rs);
	        }
	    }

	@Override
	public void deleteUndlRecord(String chdrcoy, String chdrnum, String life, String jlife) {
		String sql = "DELETE FROM VM1DTA.UNDLPF WHERE CHDRCOY= ? AND CHDRNUM =? AND LIFE= ? AND JLIFE=?";
		PreparedStatement ps = getPrepareStatement(sql);
		try {
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, life);
			ps.setString(4, jlife);
			executeUpdate(ps);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	} 
}
