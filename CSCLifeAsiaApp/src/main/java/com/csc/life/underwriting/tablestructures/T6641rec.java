package com.csc.life.underwriting.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:43
 * Description:
 * Copybook name: T6641REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6641rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6641Rec = new FixedLengthStringData(476);
  	public FixedLengthStringData nxdps = new FixedLengthStringData(32).isAPartOf(t6641Rec, 0);
  		/*                                           OCCURS 15 .*/
  	public BinaryData[] nxdp = BDArrayPartOfStructure(16, 1, 0, nxdps, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(32).isAPartOf(nxdps, 0, FILLER_REDEFINE);
  	public BinaryData nxdp01 = new BinaryData(1, 0).isAPartOf(filler, 0);
  	public BinaryData nxdp02 = new BinaryData(1, 0).isAPartOf(filler, 2);
  	public BinaryData nxdp03 = new BinaryData(1, 0).isAPartOf(filler, 4);
  	public BinaryData nxdp04 = new BinaryData(1, 0).isAPartOf(filler, 6);
  	public BinaryData nxdp05 = new BinaryData(1, 0).isAPartOf(filler, 8);
  	public BinaryData nxdp06 = new BinaryData(1, 0).isAPartOf(filler, 10);
  	public BinaryData nxdp07 = new BinaryData(1, 0).isAPartOf(filler, 12);
  	public BinaryData nxdp08 = new BinaryData(1, 0).isAPartOf(filler, 14);
  	public BinaryData nxdp09 = new BinaryData(1, 0).isAPartOf(filler, 16);
  	public BinaryData nxdp10 = new BinaryData(1, 0).isAPartOf(filler, 18);
  	public BinaryData nxdp11 = new BinaryData(1, 0).isAPartOf(filler, 20);
  	public BinaryData nxdp12 = new BinaryData(1, 0).isAPartOf(filler, 22);
  	public BinaryData nxdp13 = new BinaryData(1, 0).isAPartOf(filler, 24);
  	public BinaryData nxdp14 = new BinaryData(1, 0).isAPartOf(filler, 26);
  	public BinaryData nxdp15 = new BinaryData(1, 0).isAPartOf(filler, 28);
  	public BinaryData nxdp16 = new BinaryData(1, 0).isAPartOf(filler, 30);
  	public BinaryData nxmoney = new BinaryData(8, 0).isAPartOf(t6641Rec, 32);
  	public FixedLengthStringData nxmons = new FixedLengthStringData(396).isAPartOf(t6641Rec, 36);
  	public BinaryData[] nxmon = BDArrayPartOfStructure(99, 8, 0, nxmons, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(396).isAPartOf(nxmons, 0, FILLER_REDEFINE);
  	public BinaryData nxmon01 = new BinaryData(8, 0).isAPartOf(filler1, 0);
  	public BinaryData nxmon02 = new BinaryData(8, 0).isAPartOf(filler1, 4);
  	public BinaryData nxmon03 = new BinaryData(8, 0).isAPartOf(filler1, 8);
  	public BinaryData nxmon04 = new BinaryData(8, 0).isAPartOf(filler1, 12);
  	public BinaryData nxmon05 = new BinaryData(8, 0).isAPartOf(filler1, 16);
  	public BinaryData nxmon06 = new BinaryData(8, 0).isAPartOf(filler1, 20);
  	public BinaryData nxmon07 = new BinaryData(8, 0).isAPartOf(filler1, 24);
  	public BinaryData nxmon08 = new BinaryData(8, 0).isAPartOf(filler1, 28);
  	public BinaryData nxmon09 = new BinaryData(8, 0).isAPartOf(filler1, 32);
  	public BinaryData nxmon10 = new BinaryData(8, 0).isAPartOf(filler1, 36);
  	public BinaryData nxmon11 = new BinaryData(8, 0).isAPartOf(filler1, 40);
  	public BinaryData nxmon12 = new BinaryData(8, 0).isAPartOf(filler1, 44);
  	public BinaryData nxmon13 = new BinaryData(8, 0).isAPartOf(filler1, 48);
  	public BinaryData nxmon14 = new BinaryData(8, 0).isAPartOf(filler1, 52);
  	public BinaryData nxmon15 = new BinaryData(8, 0).isAPartOf(filler1, 56);
  	public BinaryData nxmon16 = new BinaryData(8, 0).isAPartOf(filler1, 60);
  	public BinaryData nxmon17 = new BinaryData(8, 0).isAPartOf(filler1, 64);
  	public BinaryData nxmon18 = new BinaryData(8, 0).isAPartOf(filler1, 68);
  	public BinaryData nxmon19 = new BinaryData(8, 0).isAPartOf(filler1, 72);
  	public BinaryData nxmon20 = new BinaryData(8, 0).isAPartOf(filler1, 76);
  	public BinaryData nxmon21 = new BinaryData(8, 0).isAPartOf(filler1, 80);
  	public BinaryData nxmon22 = new BinaryData(8, 0).isAPartOf(filler1, 84);
  	public BinaryData nxmon23 = new BinaryData(8, 0).isAPartOf(filler1, 88);
  	public BinaryData nxmon24 = new BinaryData(8, 0).isAPartOf(filler1, 92);
  	public BinaryData nxmon25 = new BinaryData(8, 0).isAPartOf(filler1, 96);
  	public BinaryData nxmon26 = new BinaryData(8, 0).isAPartOf(filler1, 100);
  	public BinaryData nxmon27 = new BinaryData(8, 0).isAPartOf(filler1, 104);
  	public BinaryData nxmon28 = new BinaryData(8, 0).isAPartOf(filler1, 108);
  	public BinaryData nxmon29 = new BinaryData(8, 0).isAPartOf(filler1, 112);
  	public BinaryData nxmon30 = new BinaryData(8, 0).isAPartOf(filler1, 116);
  	public BinaryData nxmon31 = new BinaryData(8, 0).isAPartOf(filler1, 120);
  	public BinaryData nxmon32 = new BinaryData(8, 0).isAPartOf(filler1, 124);
  	public BinaryData nxmon33 = new BinaryData(8, 0).isAPartOf(filler1, 128);
  	public BinaryData nxmon34 = new BinaryData(8, 0).isAPartOf(filler1, 132);
  	public BinaryData nxmon35 = new BinaryData(8, 0).isAPartOf(filler1, 136);
  	public BinaryData nxmon36 = new BinaryData(8, 0).isAPartOf(filler1, 140);
  	public BinaryData nxmon37 = new BinaryData(8, 0).isAPartOf(filler1, 144);
  	public BinaryData nxmon38 = new BinaryData(8, 0).isAPartOf(filler1, 148);
  	public BinaryData nxmon39 = new BinaryData(8, 0).isAPartOf(filler1, 152);
  	public BinaryData nxmon40 = new BinaryData(8, 0).isAPartOf(filler1, 156);
  	public BinaryData nxmon41 = new BinaryData(8, 0).isAPartOf(filler1, 160);
  	public BinaryData nxmon42 = new BinaryData(8, 0).isAPartOf(filler1, 164);
  	public BinaryData nxmon43 = new BinaryData(8, 0).isAPartOf(filler1, 168);
  	public BinaryData nxmon44 = new BinaryData(8, 0).isAPartOf(filler1, 172);
  	public BinaryData nxmon45 = new BinaryData(8, 0).isAPartOf(filler1, 176);
  	public BinaryData nxmon46 = new BinaryData(8, 0).isAPartOf(filler1, 180);
  	public BinaryData nxmon47 = new BinaryData(8, 0).isAPartOf(filler1, 184);
  	public BinaryData nxmon48 = new BinaryData(8, 0).isAPartOf(filler1, 188);
  	public BinaryData nxmon49 = new BinaryData(8, 0).isAPartOf(filler1, 192);
  	public BinaryData nxmon50 = new BinaryData(8, 0).isAPartOf(filler1, 196);
  	public BinaryData nxmon51 = new BinaryData(8, 0).isAPartOf(filler1, 200);
  	public BinaryData nxmon52 = new BinaryData(8, 0).isAPartOf(filler1, 204);
  	public BinaryData nxmon53 = new BinaryData(8, 0).isAPartOf(filler1, 208);
  	public BinaryData nxmon54 = new BinaryData(8, 0).isAPartOf(filler1, 212);
  	public BinaryData nxmon55 = new BinaryData(8, 0).isAPartOf(filler1, 216);
  	public BinaryData nxmon56 = new BinaryData(8, 0).isAPartOf(filler1, 220);
  	public BinaryData nxmon57 = new BinaryData(8, 0).isAPartOf(filler1, 224);
  	public BinaryData nxmon58 = new BinaryData(8, 0).isAPartOf(filler1, 228);
  	public BinaryData nxmon59 = new BinaryData(8, 0).isAPartOf(filler1, 232);
  	public BinaryData nxmon60 = new BinaryData(8, 0).isAPartOf(filler1, 236);
  	public BinaryData nxmon61 = new BinaryData(8, 0).isAPartOf(filler1, 240);
  	public BinaryData nxmon62 = new BinaryData(8, 0).isAPartOf(filler1, 244);
  	public BinaryData nxmon63 = new BinaryData(8, 0).isAPartOf(filler1, 248);
  	public BinaryData nxmon64 = new BinaryData(8, 0).isAPartOf(filler1, 252);
  	public BinaryData nxmon65 = new BinaryData(8, 0).isAPartOf(filler1, 256);
  	public BinaryData nxmon66 = new BinaryData(8, 0).isAPartOf(filler1, 260);
  	public BinaryData nxmon67 = new BinaryData(8, 0).isAPartOf(filler1, 264);
  	public BinaryData nxmon68 = new BinaryData(8, 0).isAPartOf(filler1, 268);
  	public BinaryData nxmon69 = new BinaryData(8, 0).isAPartOf(filler1, 272);
  	public BinaryData nxmon70 = new BinaryData(8, 0).isAPartOf(filler1, 276);
  	public BinaryData nxmon71 = new BinaryData(8, 0).isAPartOf(filler1, 280);
  	public BinaryData nxmon72 = new BinaryData(8, 0).isAPartOf(filler1, 284);
  	public BinaryData nxmon73 = new BinaryData(8, 0).isAPartOf(filler1, 288);
  	public BinaryData nxmon74 = new BinaryData(8, 0).isAPartOf(filler1, 292);
  	public BinaryData nxmon75 = new BinaryData(8, 0).isAPartOf(filler1, 296);
  	public BinaryData nxmon76 = new BinaryData(8, 0).isAPartOf(filler1, 300);
  	public BinaryData nxmon77 = new BinaryData(8, 0).isAPartOf(filler1, 304);
  	public BinaryData nxmon78 = new BinaryData(8, 0).isAPartOf(filler1, 308);
  	public BinaryData nxmon79 = new BinaryData(8, 0).isAPartOf(filler1, 312);
  	public BinaryData nxmon80 = new BinaryData(8, 0).isAPartOf(filler1, 316);
  	public BinaryData nxmon81 = new BinaryData(8, 0).isAPartOf(filler1, 320);
  	public BinaryData nxmon82 = new BinaryData(8, 0).isAPartOf(filler1, 324);
  	public BinaryData nxmon83 = new BinaryData(8, 0).isAPartOf(filler1, 328);
  	public BinaryData nxmon84 = new BinaryData(8, 0).isAPartOf(filler1, 332);
  	public BinaryData nxmon85 = new BinaryData(8, 0).isAPartOf(filler1, 336);
  	public BinaryData nxmon86 = new BinaryData(8, 0).isAPartOf(filler1, 340);
  	public BinaryData nxmon87 = new BinaryData(8, 0).isAPartOf(filler1, 344);
  	public BinaryData nxmon88 = new BinaryData(8, 0).isAPartOf(filler1, 348);
  	public BinaryData nxmon89 = new BinaryData(8, 0).isAPartOf(filler1, 352);
  	public BinaryData nxmon90 = new BinaryData(8, 0).isAPartOf(filler1, 356);
  	public BinaryData nxmon91 = new BinaryData(8, 0).isAPartOf(filler1, 360);
  	public BinaryData nxmon92 = new BinaryData(8, 0).isAPartOf(filler1, 364);
  	public BinaryData nxmon93 = new BinaryData(8, 0).isAPartOf(filler1, 368);
  	public BinaryData nxmon94 = new BinaryData(8, 0).isAPartOf(filler1, 372);
  	public BinaryData nxmon95 = new BinaryData(8, 0).isAPartOf(filler1, 376);
  	public BinaryData nxmon96 = new BinaryData(8, 0).isAPartOf(filler1, 380);
  	public BinaryData nxmon97 = new BinaryData(8, 0).isAPartOf(filler1, 384);
  	public BinaryData nxmon98 = new BinaryData(8, 0).isAPartOf(filler1, 388);
  	public BinaryData nxmon99 = new BinaryData(8, 0).isAPartOf(filler1, 392);
  	public FixedLengthStringData tnxmons = new FixedLengthStringData(44).isAPartOf(t6641Rec, 432);
  	public BinaryData[] tnxmon = BDArrayPartOfStructure(11, 8, 0, tnxmons, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(44).isAPartOf(tnxmons, 0, FILLER_REDEFINE);
  	public BinaryData tnxmon01 = new BinaryData(8, 0).isAPartOf(filler2, 0);
  	public BinaryData tnxmon02 = new BinaryData(8, 0).isAPartOf(filler2, 4);
  	public BinaryData tnxmon03 = new BinaryData(8, 0).isAPartOf(filler2, 8);
  	public BinaryData tnxmon04 = new BinaryData(8, 0).isAPartOf(filler2, 12);
  	public BinaryData tnxmon05 = new BinaryData(8, 0).isAPartOf(filler2, 16);
  	public BinaryData tnxmon06 = new BinaryData(8, 0).isAPartOf(filler2, 20);
  	public BinaryData tnxmon07 = new BinaryData(8, 0).isAPartOf(filler2, 24);
  	public BinaryData tnxmon08 = new BinaryData(8, 0).isAPartOf(filler2, 28);
  	public BinaryData tnxmon09 = new BinaryData(8, 0).isAPartOf(filler2, 32);
  	public BinaryData tnxmon10 = new BinaryData(8, 0).isAPartOf(filler2, 36);
  	public BinaryData tnxmon11 = new BinaryData(8, 0).isAPartOf(filler2, 40);


	public void initialize() {
		COBOLFunctions.initialize(t6641Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6641Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}