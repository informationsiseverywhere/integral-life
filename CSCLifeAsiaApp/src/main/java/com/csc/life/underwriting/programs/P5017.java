/*
 * File: P5017.java
 * Date: 29 August 2009 23:55:40
 * Author: Quipoz Limited
 * 
 * Class transformed from P5017.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.life.underwriting.recordstructures.P5017par;
import com.csc.life.underwriting.screens.S5017ScreenVars;
import com.csc.life.underwriting.tablestructures.T6641rec;
import com.csc.life.underwriting.tablestructures.T6686rec;
import com.csc.smart.dataaccess.BparTableDAM;
import com.csc.smart.dataaccess.BppdTableDAM;
import com.csc.smart.dataaccess.BscdTableDAM;
import com.csc.smart.dataaccess.BsscTableDAM;
import com.csc.smart.dataaccess.BupaTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*            P5017 MAINLINE.
*
*   Parameter prompt program
*
*   This program requires the name of an item on T6686 along
*   with the interest percentage, to make up an 8 character
*   field. This field is validated against T6686 to make sure
*   it exists, and against T6641 to ensure it doesn't exist on
*   that table. This field is then passed to B5017 in order to
*   calculate the item values.
*
*
*****************************************************************
* </pre>
*/
public class P5017 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5017");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private FixedLengthStringData wsaaItemFormat = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItem = new FixedLengthStringData(5).isAPartOf(wsaaItemFormat, 0).init(SPACES);
	private ZonedDecimalData wsaaItemPerc = new ZonedDecimalData(3, 0).isAPartOf(wsaaItemFormat, 5).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaItemCheck = new FixedLengthStringData(8);
	private FixedLengthStringData filler = new FixedLengthStringData(5).isAPartOf(wsaaItemCheck, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaItemPercChk1 = new FixedLengthStringData(1).isAPartOf(wsaaItemCheck, 5).init(ZERO);
	private FixedLengthStringData wsaaItemPercChk2 = new FixedLengthStringData(1).isAPartOf(wsaaItemCheck, 6).init(ZERO);
	private FixedLengthStringData wsaaItemPercChk3 = new FixedLengthStringData(1).isAPartOf(wsaaItemCheck, 7).init(ZERO);
		/* ERRORS */
	private static final String a123 = "A123";
	private static final String g561 = "G561";
	private static final String g573 = "G573";
	private static final String e726 = "E726";
	private static final String h926 = "H926";
		/* TABLES */
	private static final String t6641 = "T6641";
	private static final String t6686 = "T6686";
		/* FORMATS */
	private static final String bscdrec = "BSCDREC";
	private static final String bsscrec = "BSSCREC";
	private static final String buparec = "BUPAREC";
	private static final String bppdrec = "BPPDREC";
	private static final String bparrec = "BPARREC";
	private BparTableDAM bparIO = new BparTableDAM();
	private BppdTableDAM bppdIO = new BppdTableDAM();
	private BscdTableDAM bscdIO = new BscdTableDAM();
	private BsscTableDAM bsscIO = new BsscTableDAM();
	private BupaTableDAM bupaIO = new BupaTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private P5017par p5017par = new P5017par();
	private T6686rec t6686rec = new T6686rec();
	private T6641rec t6641rec = new T6641rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5017ScreenVars sv = ScreenProgram.getScreenVars( S5017ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		error2080, 
		exit2090
	}

	public P5017() {
		super();
		screenVars = sv;
		new ScreenModel("S5017", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/* Retrieve Schedule.*/
		bsscIO.setFormat(bsscrec);
		bsscIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bsscIO);
		if (isNE(bsscIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bsscIO.getParams());
			fatalError600();
		}
		/* Retrieve Schedule Definition.*/
		bscdIO.setFormat(bscdrec);
		bscdIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bscdIO);
		if (isNE(bscdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bscdIO.getParams());
			fatalError600();
		}
		/* Retrieve Parameter Prompt Definition.*/
		bppdIO.setFormat(bppdrec);
		bppdIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bppdIO);
		if (isNE(bppdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bppdIO.getParams());
			fatalError600();
		}
		sv.dataArea.set(SPACES);
		sv.scheduleName.set(bsscIO.getScheduleName());
		sv.scheduleNumber.set(bsscIO.getScheduleNumber());
		sv.effdate.set(bsscIO.getEffectiveDate());
		sv.acctmonth.set(bsscIO.getAcctMonth());
		sv.acctyear.set(bsscIO.getAcctYear());
		sv.jobq.set(bscdIO.getJobq());
		sv.bcompany.set(bppdIO.getCompany());
		sv.bbranch.set(bsscIO.getInitBranch());
		if (isNE(wsspcomn.flag, "I")
		&& isNE(wsspcomn.flag, "M")) {
			return ;
		}
		bparIO.setParams(SPACES);
		bparIO.setScheduleName(bsscIO.getScheduleName());
		bparIO.setCompany(bppdIO.getCompany());
		bparIO.setParmPromptProg(wsaaProg);
		bparIO.setBruntype(subString(wsspcomn.inqkey, 1, 8));
		bparIO.setBrunoccur(subString(wsspcomn.inqkey, 9, 3));
		bparIO.setEffectiveDate(bsscIO.getEffectiveDate());
		bparIO.setAcctmonth(bsscIO.getAcctMonth());
		bparIO.setAcctyear(bsscIO.getAcctYear());
		bparIO.setFunction(varcom.readr);
		bparIO.setFormat(bparrec);
		SmartFileCode.execute(appVars, bparIO);
		if (isNE(bparIO.getStatuz(), varcom.oK)
		&& isNE(bparIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(bparIO.getStatuz());
			syserrrec.params.set(bparIO.getParams());
			fatalError600();
		}
		if (isEQ(bparIO.getStatuz(), varcom.mrnf)) {
			scrnparams.errorCode.set(a123);
			return ;
	}
		p5017par.parmRecord.set(bparIO.getParmarea());
		sv.itemToCreate.set(p5017par.itemToCreate);
		sv.longdesc.set(p5017par.longdesc);
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					checkForErrors2080();
				case error2080: 
					error2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2090);
		}
		/*VALIDATE*/
		/**    Validate fields*/
	}

protected void checkForErrors2080()
	{
		if (isEQ(sv.itemToCreate,SPACES)) {
			sv.itemcrtErr.set(h926);
			goTo(GotoLabel.error2080);
		}
		wsaaItemCheck.set(sv.itemToCreate);
		if (isEQ(wsaaItemPercChk1,SPACES)
		|| isEQ(wsaaItemPercChk2,SPACES)
		|| isEQ(wsaaItemPercChk3,SPACES)) {
			sv.itemcrtErr.set(e726);
			goTo(GotoLabel.error2080);
		}
		wsaaItemFormat.set(sv.itemToCreate);
		itemIO.setDataArea(SPACES);
		itemIO.setItemcoy(bppdIO.getCompany());
		itemIO.setItemtabl(t6686);
		itemIO.setItemitem(wsaaItem);
		itemIO.setItempfx("IT");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			sv.itemcrtErr.set(g561);
		}
		t6686rec.t6686Rec.set(itemIO.getGenarea());
		itemIO.setDataArea(SPACES);
		itemIO.setItemcoy(bppdIO.getCompany());
		itemIO.setItemtabl(t6641);
		itemIO.setItemitem(sv.itemToCreate);
		itemIO.setItempfx("IT");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.mrnf)
		&& isNE(itemIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			sv.itemcrtErr.set(g573);
		}
		t6641rec.t6641Rec.set(ZERO);
	}

protected void error2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*LOAD-FIELDS*/
		moveParameters3100();
		writeParamRecord3200();
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATING SECTIONS
	* </pre>
	*/
protected void moveParameters3100()
	{
		/*MOVE-TO-PARM-RECORD*/
		p5017par.itemToCreate.set(sv.itemToCreate);
		p5017par.longdesc.set(sv.longdesc);
		/*EXIT*/
	}

protected void writeParamRecord3200()
	{
		writBupa3210();
	}

protected void writBupa3210()
	{
		bupaIO.setParams(SPACES);
		bupaIO.setScheduleName(sv.scheduleName);
		bupaIO.setScheduleNumber(sv.scheduleNumber);
		bupaIO.setEffectiveDate(sv.effdate);
		bupaIO.setAcctMonth(sv.acctmonth);
		bupaIO.setAcctYear(sv.acctyear);
		bupaIO.setCompany(sv.bcompany);
		bupaIO.setBranch(sv.bbranch);
		bupaIO.setParmarea(p5017par.parmRecord);
		bupaIO.setParmPromptProg(bppdIO.getParmPromptProg());
		bupaIO.setFormat(buparec);
		bupaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, bupaIO);
		if (isNE(bupaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bupaIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
