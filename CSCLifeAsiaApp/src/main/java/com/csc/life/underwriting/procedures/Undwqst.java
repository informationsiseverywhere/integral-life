/*
 * File: Undwqst.java
 * Date: 30 August 2009 2:49:05
 * Author: Quipoz Limited
 * 
 * Class transformed from UNDWQST.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.underwriting.dataaccess.UndqTableDAM;
import com.csc.life.underwriting.dataaccess.dao.UndqpfDAO;
import com.csc.life.underwriting.dataaccess.model.Undqpf;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 *
 *  EXISTENCE CHECK SUBR FOR UNDERWRITING QUESTIONS
 *  -----------------------------------------------
 *  This  subroutine  is  called  from  OPTSWCH Table T1661. It
 *  determines whether any UNDQ records (Underwriting Questions)
 *  exist for the given Life Assured being enquired upon in
 *  P5005.
 ***********************************************************************
 *                                                                     *
 * </pre>
 */
public class Undwqst extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("UNDWQST");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData optsDteeff = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaChkpStatuz = new FixedLengthStringData(4);
	/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	/*Underwriting Question Details*/
	//private UndqTableDAM undqIO = new UndqTableDAM();
	private Varcom varcom = new Varcom();
	private List<Undqpf> undqList =new ArrayList<Undqpf>(); 

	private UndqpfDAO undqpfDAO = getApplicationContext().getBean("undqpfDAO", UndqpfDAO.class);


	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		errorProg610
	}

	public Undwqst() {
		super();
	}

	public void mainline(Object... parmArray)
	{
		wsaaChkpStatuz = convertAndSetParam(wsaaChkpStatuz, parmArray, 1);
		optsDteeff = convertAndSetParam(optsDteeff, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

	protected void mainline0000()
	{
		try {
			performs0010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

	protected void performs0010()
	{
		lifelnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}

		boolean isExist = false;

		isExist  =undqpfDAO.isExistundqpfByCoyAndNumAndLifeAndJLife(lifelnbIO.getChdrcoy().toString(),lifelnbIO.getChdrnum().toString(),lifelnbIO.getLife().toString(),lifelnbIO.getJlife().toString());

		if(isExist){
			wsaaChkpStatuz.set("DFND");
		}
		else {
			wsaaChkpStatuz.set("NFND");
			goTo(GotoLabel.exit090);
		}

	}

	protected void exit090()
	{
		exitProgram();
	}

	protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

	protected void errorProg610()
	{
		wsaaChkpStatuz.set(varcom.bomb);
		exitProgram();
	}

	protected void exit690()
	{
		exitProgram();
	}
}
