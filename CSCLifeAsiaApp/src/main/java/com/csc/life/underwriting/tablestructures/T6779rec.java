package com.csc.life.underwriting.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:33
 * Description:
 * Copybook name: T6779REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6779rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6779Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData gencrule = new FixedLengthStringData(1).isAPartOf(t6779Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(499).isAPartOf(t6779Rec, 1, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6779Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6779Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}