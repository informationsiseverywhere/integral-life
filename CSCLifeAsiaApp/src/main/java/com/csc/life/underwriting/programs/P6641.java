/*
 * File: P6641.java
 * Date: 30 August 2009 0:48:00
 * Author: Quipoz Limited
 * 
 * Class transformed from P6641.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.underwriting.screens.S6641ScreenVars;
import com.csc.life.underwriting.tablestructures.T6641rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* * * * * * * * * * * *  W A R N I N G  * * * * * * * * * * * *
*
* EXTRA DATA SCREEN CONTAINS MORE THAN 500 BYTES - ( MAX SIZE OF
* ITEM-GENAREA ). - THEREFORE FIELDS HAVE BEEN CHANGED TO COMP-4.
*
* THIS GENERATED PROGRAM HAS BEEN AMENDED TO MAKE INDIVIDUAL FIELD
* MOVES TO AVOID CONVERSION OF COMP-4 FIELDS WHICH WAS CAUSING
* DECIMAL DATA ERRORS.
*
*
*
*****************************************************************
* </pre>
*/
public class P6641 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6641");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T6641rec t6641rec = new T6641rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6641ScreenVars sv = ScreenProgram.getScreenVars( S6641ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		other3080, 
		exit3090
	}

	public P6641() {
		super();
		screenVars = sv;
		new ScreenModel("S6641", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
					initialise1010();
					readRecord1031();
					moveToScreen1040();
					generalArea1045();
				}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t6641rec.t6641Rec.set(ZERO);
		t6641rec.t6641Rec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(),SPACES)) {
			return ;
		}
		wsaaSub1.set(0);
		/*    PERFORM 1400-INIT-NXDP      15 TIMES.                        */
		for (int loopVar1 = 0; !(loopVar1 == 16); loopVar1 += 1){
			initNxdp1400();
		}
		wsaaSub1.set(0);
		for (int loopVar2 = 0; !(loopVar2 == 99); loopVar2 += 1){
			initNxmon1500();
		}
		wsaaSub1.set(0);
		for (int loopVar3 = 0; !(loopVar3 == 11); loopVar3 += 1){
			initTnxmon1800();
		}
		t6641rec.nxmoney.set(ZERO);
	}

protected void generalArea1045()
	{
		wsaaSub1.set(0);
		/*    PERFORM 1600-MOVE-NXDP      15 TIMES.                        */
		for (int loopVar4 = 0; !(loopVar4 == 16); loopVar4 += 1){
			moveNxdp1600();
		}
		wsaaSub1.set(0);
		for (int loopVar5 = 0; !(loopVar5 == 99); loopVar5 += 1){
			moveNxmon1700();
		}
		sv.nxmoney.set(t6641rec.nxmoney);
		wsaaSub1.set(0);
		for (int loopVar6 = 0; !(loopVar6 == 11); loopVar6 += 1){
			moveTnxmon1900();
		}
		/*CONFIRMATION-FIELDS*/
		/*OTHER*/
		/*EXIT*/
	}

protected void initNxdp1400()
	{
		/*PARA*/
		wsaaSub1.add(1);
		t6641rec.nxdp[wsaaSub1.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void initNxmon1500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		t6641rec.nxmon[wsaaSub1.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void moveNxdp1600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		sv.nxdp[wsaaSub1.toInt()].set(t6641rec.nxdp[wsaaSub1.toInt()]);
		/*EXIT*/
	}

protected void moveNxmon1700()
	{
		/*PARA*/
		wsaaSub1.add(1);
		sv.nxmon[wsaaSub1.toInt()].set(t6641rec.nxmon[wsaaSub1.toInt()]);
		/*EXIT*/
	}

protected void initTnxmon1800()
	{
		/*PARA*/
		wsaaSub1.add(1);
		t6641rec.tnxmon[wsaaSub1.toInt()].set(ZERO);
		/*EXIT*/
		}

protected void moveTnxmon1900()
	{
		/*PARA*/
		wsaaSub1.add(1);
		sv.tnxmon[wsaaSub1.toInt()].set(t6641rec.tnxmon[wsaaSub1.toInt()]);
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
					screenIo2010();
					exit2090();
				}

protected void screenIo2010()
	{
		/*    CALL 'S6641IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S6641-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			return ;
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080: 
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		wsaaUpdateFlag = "N";
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
		itemIO.setGenarea(t6641rec.t6641Rec);
		itemIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		/*CHECK*/
		wsaaSub1.set(0);
		for (int loopVar7 = 0; !(loopVar7 == 16); loopVar7 += 1){
			updateNxdp3400();
		}
		if (isNE(sv.nxmoney,t6641rec.nxmoney)) {
			t6641rec.nxmoney.set(sv.nxmoney);
			wsaaUpdateFlag = "Y";
		}
		wsaaSub1.set(0);
		for (int loopVar8 = 0; !(loopVar8 == 99); loopVar8 += 1){
			updateNxmon3500();
		}
		wsaaSub1.set(0);
		for (int loopVar9 = 0; !(loopVar9 == 11); loopVar9 += 1){
			updateTnxmon3600();
		}
		/*EXIT*/
	}

protected void updateNxdp3400()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.nxdp[wsaaSub1.toInt()],t6641rec.nxdp[wsaaSub1.toInt()])) {
			t6641rec.nxdp[wsaaSub1.toInt()].set(sv.nxdp[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*EXIT*/
	}

protected void updateNxmon3500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.nxmon[wsaaSub1.toInt()],t6641rec.nxmon[wsaaSub1.toInt()])) {
			t6641rec.nxmon[wsaaSub1.toInt()].set(sv.nxmon[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*EXIT*/
	}

protected void updateTnxmon3600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.tnxmon[wsaaSub1.toInt()], t6641rec.tnxmon[wsaaSub1.toInt()])) {
			t6641rec.tnxmon[wsaaSub1.toInt()].set(sv.tnxmon[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
