package com.csc.life.underwriting.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:20
 * Description:
 * Copybook name: TR675REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr675rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr675Rec = new FixedLengthStringData(getRecSize());
  	public FixedLengthStringData ages = new FixedLengthStringData(18).isAPartOf(tr675Rec, 0);
  	public ZonedDecimalData[] age = ZDArrayPartOfStructure(6, 3, 0, ages, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(18).isAPartOf(ages, 0, FILLER_REDEFINE);
  	public ZonedDecimalData age01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData age02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData age03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData age04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public ZonedDecimalData age05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData age06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
  	public FixedLengthStringData bmibasis = new FixedLengthStringData(5).isAPartOf(tr675Rec, 18);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(tr675Rec, 23);
  	public FixedLengthStringData questsets = new FixedLengthStringData(96).isAPartOf(tr675Rec, 24);
  	public FixedLengthStringData[] questset = FLSArrayPartOfStructure(12, 8, questsets, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(96).isAPartOf(questsets, 0, FILLER_REDEFINE);
  	public FixedLengthStringData questset01 = new FixedLengthStringData(8).isAPartOf(filler1, 0);
  	public FixedLengthStringData questset02 = new FixedLengthStringData(8).isAPartOf(filler1, 8);
  	public FixedLengthStringData questset03 = new FixedLengthStringData(8).isAPartOf(filler1, 16);
  	public FixedLengthStringData questset04 = new FixedLengthStringData(8).isAPartOf(filler1, 24);
  	public FixedLengthStringData questset05 = new FixedLengthStringData(8).isAPartOf(filler1, 32);
  	public FixedLengthStringData questset06 = new FixedLengthStringData(8).isAPartOf(filler1, 40);
  	public FixedLengthStringData questset07 = new FixedLengthStringData(8).isAPartOf(filler1, 48);
  	public FixedLengthStringData questset08 = new FixedLengthStringData(8).isAPartOf(filler1, 56);
  	public FixedLengthStringData questset09 = new FixedLengthStringData(8).isAPartOf(filler1, 64);
  	public FixedLengthStringData questset10 = new FixedLengthStringData(8).isAPartOf(filler1, 72);
  	public FixedLengthStringData questset11 = new FixedLengthStringData(8).isAPartOf(filler1, 80);
  	public FixedLengthStringData questset12 = new FixedLengthStringData(8).isAPartOf(filler1, 88);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(380).isAPartOf(tr675Rec, 120, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr675Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr675Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	
	public int getRecSize() {
		return 500;
	}
	


}