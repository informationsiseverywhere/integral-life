package com.csc.life.underwriting.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

public class Sd5g5ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(128);
	public FixedLengthStringData dataFields = new FixedLengthStringData(68).isAPartOf(dataArea, 0);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,8);
	public ZonedDecimalData totRiskamt = DD.ttotamt.copyToZonedDecimal().isAPartOf(dataFields, 55);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 68);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData totRiskamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 80);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] totRiskamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	
		
	public FixedLengthStringData subfileArea = new FixedLengthStringData(157);	
	public FixedLengthStringData subfileFields = new FixedLengthStringData(52).isAPartOf(subfileArea, 0);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData riskClass = DD.riskclass.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData riskClassDesc = DD.riskclsdesc.copy().isAPartOf(subfileFields,5);
	public ZonedDecimalData riskAmt = DD.riskamt.copyToZonedDecimal().isAPartOf(subfileFields,35);	
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(16).isAPartOf(subfileArea, 90);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData riskClassErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData riskClassDescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData riskAmtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(48).isAPartOf(subfileArea, 106);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] riskClassOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] riskClassDescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] riskAmtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 154);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData Sd5g5screensflWritten = new LongData(0);
	public LongData Sd5g5screenctlWritten = new LongData(0);
	public LongData Sd5g5screenWritten = new LongData(0);
	public LongData Sd5g5protectWritten = new LongData(0);
	public GeneralTable sd5g5screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sd5g5screensfl;
	}

	public Sd5g5ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		//fieldIndMap.put(answerOut,new String[] {"03","01","-03","01",null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {select, riskClass, riskClassDesc, riskAmt};
		screenSflOutFields = new BaseData[][] {selectOut, riskClassOut, riskClassDescOut, riskAmtOut};
		screenSflErrFields = new BaseData[] {selectErr, riskClassErr, riskClassDescErr, riskAmtErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {lifenum, lifename,totRiskamt};
		screenOutFields = new BaseData[][] {lifenumOut, lifenameOut,totRiskamtOut};
		screenErrFields = new BaseData[] {lifenumErr, lifenameErr,totRiskamtErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sd5g5screen.class;
		screenSflRecord = Sd5g5screensfl.class;
		screenCtlRecord = Sd5g5screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sd5g5protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sd5g5screenctl.lrec.pageSubfile);
	}
}
