/*
 * File: Cnvt6772.java
 * Date: 29 August 2009 22:41:55
 * Author: Quipoz Limited
 * 
 * Class transformed from CNVT6772.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.underwriting.tablestructures.T6772rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table T6772, field has changed from FUPCODE
*   to FUPCDES.
*
***********************************************************************
* </pre>
*/
public class Cnvt6772 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVT6772");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String t6772 = "T6772";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData t6772OldRec = new FixedLengthStringData(500);
	private FixedLengthStringData t6772OldFupcodes = new FixedLengthStringData(30).isAPartOf(t6772OldRec, 0);
	private FixedLengthStringData[] t6772OldFupcode = FLSArrayPartOfStructure(10, 3, t6772OldFupcodes, 0);
	private FixedLengthStringData t6772OldOpcdas = new FixedLengthStringData(20).isAPartOf(t6772OldRec, 30);
	private FixedLengthStringData[] t6772OldOpcda = FLSArrayPartOfStructure(10, 2, t6772OldOpcdas, 0);
	private FixedLengthStringData t6772OldRuletype = new FixedLengthStringData(1).isAPartOf(t6772OldRec, 50);
	private ZonedDecimalData t6772OldSeqnumb = new ZonedDecimalData(3, 0).isAPartOf(t6772OldRec, 51);
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private T6772rec t6772rec = new T6772rec();
	private Varcom varcom = new Varcom();

	public Cnvt6772() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITDM records an convert...*/
		wsaaCount.set(ZERO);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(lsaaCompany);
		itdmIO.setItemtabl(t6772);
		itdmIO.setItemitem(SPACES);
		itdmIO.setItmfrm(99999999);
		itdmIO.setItemseq(SPACES);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT T6772 ERROR - "+itdmIO.getFunction().toString() +"-STATUS-"+itdmIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itdmIO.getItemitem());
			return ;
		}
		if (isNE(itdmIO.getItemtabl(), t6772)) {
			getAppVars().addDiagnostic("Table T6772 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itdmIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT T6772 ERROR - "+itdmIO.getFunction().toString() +"-STATUS-"+itdmIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteT67722080();
	}

protected void update2000()
	{
		t6772OldRec.set(itdmIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		t6772rec.ruletype.set(t6772OldRuletype);
		t6772rec.seqnumb.set(t6772OldSeqnumb);
		for (ix.set(1); !(isGT(ix, 10)); ix.add(1)){
			populateItem3000();
		}
		itdmIO.setGenarea(SPACES);
		itdmIO.setGenarea(t6772rec.t6772Rec);
	}

protected void rewriteT67722080()
	{
		wsaaCount.add(1);
		itdmIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itdmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getItemtabl(), t6772)) {
			itdmIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for T6772");
		}
	}

protected void populateItem3000()
	{
		/*POPULATE*/
		t6772rec.fupcdes[ix.toInt()].set(t6772OldFupcode[ix.toInt()]);
		t6772rec.opcda[ix.toInt()].set(t6772OldOpcda[ix.toInt()]);
		/*EXIT*/
	}
}
