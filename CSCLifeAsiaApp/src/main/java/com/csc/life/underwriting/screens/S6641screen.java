package com.csc.life.underwriting.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6641screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6641ScreenVars sv = (S6641ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6641screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6641ScreenVars screenVars = (S6641ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.nxdp01.setClassString("");
		screenVars.nxmoney.setClassString("");
		screenVars.nxdp02.setClassString("");
		screenVars.nxdp03.setClassString("");
		screenVars.nxdp04.setClassString("");
		screenVars.nxdp05.setClassString("");
		screenVars.nxdp06.setClassString("");
		screenVars.nxdp07.setClassString("");
		screenVars.nxdp08.setClassString("");
		screenVars.nxdp09.setClassString("");
		screenVars.nxdp10.setClassString("");
		screenVars.nxmon01.setClassString("");
		screenVars.nxmon02.setClassString("");
		screenVars.nxmon03.setClassString("");
		screenVars.nxmon04.setClassString("");
		screenVars.nxmon05.setClassString("");
		screenVars.nxmon06.setClassString("");
		screenVars.nxmon07.setClassString("");
		screenVars.nxmon08.setClassString("");
		screenVars.nxmon09.setClassString("");
		screenVars.nxmon10.setClassString("");
		screenVars.nxmon11.setClassString("");
		screenVars.nxmon12.setClassString("");
		screenVars.nxmon13.setClassString("");
		screenVars.nxmon14.setClassString("");
		screenVars.nxmon15.setClassString("");
		screenVars.nxmon16.setClassString("");
		screenVars.nxmon17.setClassString("");
		screenVars.nxmon18.setClassString("");
		screenVars.nxmon19.setClassString("");
		screenVars.nxmon20.setClassString("");
		screenVars.nxmon21.setClassString("");
		screenVars.nxmon22.setClassString("");
		screenVars.nxmon23.setClassString("");
		screenVars.nxmon24.setClassString("");
		screenVars.nxmon25.setClassString("");
		screenVars.nxmon26.setClassString("");
		screenVars.nxmon27.setClassString("");
		screenVars.nxmon28.setClassString("");
		screenVars.nxmon29.setClassString("");
		screenVars.nxmon30.setClassString("");
		screenVars.nxmon31.setClassString("");
		screenVars.nxmon32.setClassString("");
		screenVars.nxmon33.setClassString("");
		screenVars.nxmon34.setClassString("");
		screenVars.nxmon35.setClassString("");
		screenVars.nxmon36.setClassString("");
		screenVars.nxmon37.setClassString("");
		screenVars.nxmon38.setClassString("");
		screenVars.nxmon39.setClassString("");
		screenVars.nxmon40.setClassString("");
		screenVars.nxmon41.setClassString("");
		screenVars.nxmon42.setClassString("");
		screenVars.nxmon43.setClassString("");
		screenVars.nxmon44.setClassString("");
		screenVars.nxmon45.setClassString("");
		screenVars.nxmon46.setClassString("");
		screenVars.nxmon47.setClassString("");
		screenVars.nxmon48.setClassString("");
		screenVars.nxmon49.setClassString("");
		screenVars.nxmon50.setClassString("");
		screenVars.nxmon51.setClassString("");
		screenVars.nxmon52.setClassString("");
		screenVars.nxmon53.setClassString("");
		screenVars.nxmon54.setClassString("");
		screenVars.nxmon55.setClassString("");
		screenVars.nxmon56.setClassString("");
		screenVars.nxmon57.setClassString("");
		screenVars.nxmon58.setClassString("");
		screenVars.nxmon59.setClassString("");
		screenVars.nxmon60.setClassString("");
		screenVars.nxmon61.setClassString("");
		screenVars.nxmon62.setClassString("");
		screenVars.nxmon63.setClassString("");
		screenVars.nxmon64.setClassString("");
		screenVars.nxmon65.setClassString("");
		screenVars.nxmon66.setClassString("");
		screenVars.nxmon67.setClassString("");
		screenVars.nxmon68.setClassString("");
		screenVars.nxmon69.setClassString("");
		screenVars.nxmon70.setClassString("");
		screenVars.nxmon71.setClassString("");
		screenVars.nxmon72.setClassString("");
		screenVars.nxmon73.setClassString("");
		screenVars.nxmon74.setClassString("");
		screenVars.nxmon75.setClassString("");
		screenVars.nxmon76.setClassString("");
		screenVars.nxmon77.setClassString("");
		screenVars.nxmon78.setClassString("");
		screenVars.nxmon79.setClassString("");
		screenVars.nxmon80.setClassString("");
		screenVars.nxmon81.setClassString("");
		screenVars.nxmon82.setClassString("");
		screenVars.nxmon83.setClassString("");
		screenVars.nxmon84.setClassString("");
		screenVars.nxmon85.setClassString("");
		screenVars.nxmon86.setClassString("");
		screenVars.nxmon87.setClassString("");
		screenVars.nxmon88.setClassString("");
		screenVars.nxmon89.setClassString("");
		screenVars.nxmon90.setClassString("");
		screenVars.nxmon91.setClassString("");
		screenVars.nxmon92.setClassString("");
		screenVars.nxmon93.setClassString("");
		screenVars.nxmon94.setClassString("");
		screenVars.nxmon95.setClassString("");
		screenVars.nxmon96.setClassString("");
		screenVars.nxmon97.setClassString("");
		screenVars.nxmon98.setClassString("");
		screenVars.nxmon99.setClassString("");
		screenVars.tnxmon01.setClassString("");
		screenVars.tnxmon02.setClassString("");
		screenVars.tnxmon03.setClassString("");
		screenVars.tnxmon04.setClassString("");
		screenVars.tnxmon05.setClassString("");
		screenVars.tnxmon06.setClassString("");
		screenVars.tnxmon07.setClassString("");
		screenVars.tnxmon08.setClassString("");
		screenVars.tnxmon09.setClassString("");
		screenVars.tnxmon10.setClassString("");
		screenVars.tnxmon11.setClassString("");
		screenVars.nxdp11.setClassString("");
		screenVars.nxdp12.setClassString("");
		screenVars.nxdp13.setClassString("");
		screenVars.nxdp14.setClassString("");
		screenVars.nxdp15.setClassString("");
		screenVars.nxdp16.setClassString("");
	}

/**
 * Clear all the variables in S6641screen
 */
	public static void clear(VarModel pv) {
		S6641ScreenVars screenVars = (S6641ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.nxdp01.clear();
		screenVars.nxmoney.clear();
		screenVars.nxdp02.clear();
		screenVars.nxdp03.clear();
		screenVars.nxdp04.clear();
		screenVars.nxdp05.clear();
		screenVars.nxdp06.clear();
		screenVars.nxdp07.clear();
		screenVars.nxdp08.clear();
		screenVars.nxdp09.clear();
		screenVars.nxdp10.clear();
		screenVars.nxmon01.clear();
		screenVars.nxmon02.clear();
		screenVars.nxmon03.clear();
		screenVars.nxmon04.clear();
		screenVars.nxmon05.clear();
		screenVars.nxmon06.clear();
		screenVars.nxmon07.clear();
		screenVars.nxmon08.clear();
		screenVars.nxmon09.clear();
		screenVars.nxmon10.clear();
		screenVars.nxmon11.clear();
		screenVars.nxmon12.clear();
		screenVars.nxmon13.clear();
		screenVars.nxmon14.clear();
		screenVars.nxmon15.clear();
		screenVars.nxmon16.clear();
		screenVars.nxmon17.clear();
		screenVars.nxmon18.clear();
		screenVars.nxmon19.clear();
		screenVars.nxmon20.clear();
		screenVars.nxmon21.clear();
		screenVars.nxmon22.clear();
		screenVars.nxmon23.clear();
		screenVars.nxmon24.clear();
		screenVars.nxmon25.clear();
		screenVars.nxmon26.clear();
		screenVars.nxmon27.clear();
		screenVars.nxmon28.clear();
		screenVars.nxmon29.clear();
		screenVars.nxmon30.clear();
		screenVars.nxmon31.clear();
		screenVars.nxmon32.clear();
		screenVars.nxmon33.clear();
		screenVars.nxmon34.clear();
		screenVars.nxmon35.clear();
		screenVars.nxmon36.clear();
		screenVars.nxmon37.clear();
		screenVars.nxmon38.clear();
		screenVars.nxmon39.clear();
		screenVars.nxmon40.clear();
		screenVars.nxmon41.clear();
		screenVars.nxmon42.clear();
		screenVars.nxmon43.clear();
		screenVars.nxmon44.clear();
		screenVars.nxmon45.clear();
		screenVars.nxmon46.clear();
		screenVars.nxmon47.clear();
		screenVars.nxmon48.clear();
		screenVars.nxmon49.clear();
		screenVars.nxmon50.clear();
		screenVars.nxmon51.clear();
		screenVars.nxmon52.clear();
		screenVars.nxmon53.clear();
		screenVars.nxmon54.clear();
		screenVars.nxmon55.clear();
		screenVars.nxmon56.clear();
		screenVars.nxmon57.clear();
		screenVars.nxmon58.clear();
		screenVars.nxmon59.clear();
		screenVars.nxmon60.clear();
		screenVars.nxmon61.clear();
		screenVars.nxmon62.clear();
		screenVars.nxmon63.clear();
		screenVars.nxmon64.clear();
		screenVars.nxmon65.clear();
		screenVars.nxmon66.clear();
		screenVars.nxmon67.clear();
		screenVars.nxmon68.clear();
		screenVars.nxmon69.clear();
		screenVars.nxmon70.clear();
		screenVars.nxmon71.clear();
		screenVars.nxmon72.clear();
		screenVars.nxmon73.clear();
		screenVars.nxmon74.clear();
		screenVars.nxmon75.clear();
		screenVars.nxmon76.clear();
		screenVars.nxmon77.clear();
		screenVars.nxmon78.clear();
		screenVars.nxmon79.clear();
		screenVars.nxmon80.clear();
		screenVars.nxmon81.clear();
		screenVars.nxmon82.clear();
		screenVars.nxmon83.clear();
		screenVars.nxmon84.clear();
		screenVars.nxmon85.clear();
		screenVars.nxmon86.clear();
		screenVars.nxmon87.clear();
		screenVars.nxmon88.clear();
		screenVars.nxmon89.clear();
		screenVars.nxmon90.clear();
		screenVars.nxmon91.clear();
		screenVars.nxmon92.clear();
		screenVars.nxmon93.clear();
		screenVars.nxmon94.clear();
		screenVars.nxmon95.clear();
		screenVars.nxmon96.clear();
		screenVars.nxmon97.clear();
		screenVars.nxmon98.clear();
		screenVars.nxmon99.clear();
		screenVars.tnxmon01.clear();
		screenVars.tnxmon02.clear();
		screenVars.tnxmon03.clear();
		screenVars.tnxmon04.clear();
		screenVars.tnxmon05.clear();
		screenVars.tnxmon06.clear();
		screenVars.tnxmon07.clear();
		screenVars.tnxmon08.clear();
		screenVars.tnxmon09.clear();
		screenVars.tnxmon10.clear();
		screenVars.tnxmon11.clear();
		screenVars.nxdp11.clear();
		screenVars.nxdp12.clear();
		screenVars.nxdp13.clear();
		screenVars.nxdp14.clear();
		screenVars.nxdp15.clear();
		screenVars.nxdp16.clear();
	}
}
