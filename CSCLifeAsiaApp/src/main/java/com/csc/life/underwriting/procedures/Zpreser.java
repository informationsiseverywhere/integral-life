/*
 * File: Zpreser.java
 * Date: 12 Oct 2018 1:57:40
 * Author: Kevin Han
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.tablestructures.Td5horec;
import com.csc.life.terminationclaims.tablestructures.Td5hprec;
import com.csc.life.terminationclaims.tablestructures.Td5hqrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* *
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.


*   New calculation method for Non-cash value component, 
*   defined in part surrender method code SC07
*   
*   Initialise
*     - retrieve and set up standard report headings.
*
*   Read
*     - read first primary file record
*
*   Perform     Until End of File
*
*      Edit
*       - Check if the primary file record is required
*       - Softlock it if the record is to be updated
*
*      Update
*       - update database files
*       - write details to report while not primary file EOF
*       - look up referred to records for output details
*       - if new page, write headings
*       - write details
*
*      Read next primary file record
*
*    End Perform
*
*   Control totals:
*     01  -  Number of pages printed
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*   These remarks must be replaced by what the program actually
*     does.
*
***********************************************************************
* </pre>
*/
public class Zpreser extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "ZPRESER";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private ZonedDecimalData wsaaIx = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaRate = new ZonedDecimalData(6, 4);
	
	private PackedDecimalData wsaaReserveFactor = new PackedDecimalData(18, 9);
	
	private PackedDecimalData wsaaReserveTot = new PackedDecimalData(17, 2);
	
	private FixedLengthStringData wsaaTd5hoKey = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTd5hoCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTd5hoKey, 0);
	private FixedLengthStringData wsaaTd5hoFreq = new FixedLengthStringData(2).isAPartOf(wsaaTd5hoKey, 4);
	private FixedLengthStringData wsaaTd5hpKey = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTd5hpCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTd5hpKey, 0);
	private FixedLengthStringData wsaaTd5hpFreq = new FixedLengthStringData(2).isAPartOf(wsaaTd5hpKey, 4);
	
		/* TABLES */
	private static final String td5ho = "TD5HO";
	private static final String td5hp = "TD5HP";
	private static final String td5hq = "TD5HQ";
	
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private ErrorsInner errorsInner = new ErrorsInner();
	private Td5horec td5horec = new Td5horec();
	private Td5hprec td5hprec = new Td5hprec();
	private Td5hqrec td5hqrec = new Td5hqrec();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Itempf itempf = null;
    private Covrpf covrpf = null;
    private List<Covrpf> covrpfList = null;
    private Iterator<Covrpf> covrpfIter;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		continue2160, 
		exit3199, 
		seExit9090, 
		dbExit9190
	}

	public Zpreser() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		srcalcpy.surrenderRec = convertAndSetParam(srcalcpy.surrenderRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline1000()
	{
		/*MAIN*/
		srcalcpy.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaReserveTot.set(ZERO);
		
		if (isNE(srcalcpy.endf,"X")) {
			readTd5hq2000();
			if (isEQ(td5hqrec.calBasis,"M")) {
				readTd5ho3000();
			}

			if (isEQ(td5hqrec.calBasis,"D")) {
				readTd5hp4000();
			}
		}
		setExitVariables5000();
	}

protected void readTd5hq2000()
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(srcalcpy.chdrChdrcoy.toString());
		itempf.setItemtabl(td5hq);
		itempf.setItemitem(srcalcpy.crtable.toString());
		itempf = itemDAO.getItempfRecord(itempf);
		if (itempf== null ) {
			syserrrec.params.set(itempf);
			dbError9100();
		}
		else {
			td5hqrec.td5hqRec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}

protected void readTd5ho3000()
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(srcalcpy.chdrChdrcoy.toString());
		itempf.setItemtabl(td5ho);
		wsaaTd5hoCrtable.set(srcalcpy.crtable.toString());
		wsaaTd5hoFreq.set(srcalcpy.billfreq.toString());
		itempf.setItemitem(wsaaTd5hoKey.toString());
		itempf = itemDAO.getItempfRecord(itempf);
		if (itempf== null ) {
			syserrrec.params.set(itempf);
			dbError9100();
		}
		else {
			td5horec.td5hoRec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	
		datcon2rec.intDate1.set(srcalcpy.effdate);
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			dbError9100();
		}
	
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(datcon2rec.intDate2);
		datcon3rec.intDate2.set(srcalcpy.ptdate);
		datcon3rec.frequency.set("12");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		
		wsaaIx.set(datcon3rec.freqFactor);
		
		for (wsaaSub.set(1); !(isGT(wsaaSub,20)); wsaaSub.add(1)){
			if (isGTE(wsaaIx,td5horec.remamon[wsaaSub.toInt()])) {
				wsaaRate.set(td5horec.rate[wsaaSub.toInt()]);
				wsaaSub.set(21);
			}
		}
	}

protected void readTd5hp4000()
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(srcalcpy.chdrChdrcoy.toString());
		itempf.setItemtabl(td5hp);
		wsaaTd5hpCrtable.set(srcalcpy.crtable.toString());
		wsaaTd5hpFreq.set(srcalcpy.billfreq.toString());
		itempf.setItemitem(wsaaTd5hpKey.toString());
		itempf = itemDAO.getItempfRecord(itempf);
		if (itempf== null ) {
			syserrrec.params.set(itempf);
			dbError9100();
		}
		else {
			td5hprec.td5hpRec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		
		datcon2rec.intDate1.set(srcalcpy.effdate);
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			dbError9100();
		}
	
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(datcon2rec.intDate2);
		datcon3rec.intDate2.set(srcalcpy.ptdate);
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		
		wsaaIx.set(datcon3rec.freqFactor);
		
		for (wsaaSub.set(1); !(isGT(wsaaSub,20)); wsaaSub.add(1)){
			if (isGTE(wsaaIx,td5hprec.remaday[wsaaSub.toInt()])) {
				wsaaRate.set(td5hprec.rate[wsaaSub.toInt()]);
				wsaaSub.set(21);
			}
		}
	}

protected void setExitVariables5000()
	{
		covrpfList = covrpfDAO.getcovrtrbRecord(srcalcpy.chdrChdrcoy.toString(), srcalcpy.chdrChdrnum.toString(), srcalcpy.lifeLife.toString(), srcalcpy.covrCoverage.toString(), srcalcpy.covrRider.toString());
		if (covrpfList == null || covrpfList.size() == 0) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(srcalcpy.chdrChdrcoy);
			stringVariable1.addExpression(srcalcpy.chdrChdrnum);
			stringVariable1.addExpression(srcalcpy.lifeLife);
			stringVariable1.addExpression(srcalcpy.covrCoverage);
			stringVariable1.addExpression(srcalcpy.covrRider);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(errorsInner.g344);
			dbError9100();
		}
		
		covrpfIter = covrpfList.iterator();
		if(covrpfIter.hasNext()){   
		covrpf = covrpfIter.next();
		}       
		compute(srcalcpy.actualVal, 11).setRounded(div(mult(covrpf.getInstprem(),wsaaRate), 100));
	    srcalcpy.estimatedVal.set(ZERO);
	    srcalcpy.status.set(varcom.endp);
	    srcalcpy.type.set("S");
	    srcalcpy.endf.set("N");
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9000();
				case seExit9090: 
					seExit9090();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		srcalcpy.status.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9100();
				case dbExit9190: 
					dbExit9190();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		srcalcpy.status.set(varcom.bomb);
		exitProgram();
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData g344 = new FixedLengthStringData(4).init("G344");
	}
}
