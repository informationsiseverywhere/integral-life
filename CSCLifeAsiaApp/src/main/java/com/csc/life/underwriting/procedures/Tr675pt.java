/*
 * File: Tr675pt.java
 * Date: 30 August 2009 2:44:06
 * Author: Quipoz Limited
 * 
 * Class transformed from TR675PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.underwriting.tablestructures.Tr675rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR675.
*
*
*
***********************************************************************
* </pre>
*/
public class Tr675pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(50).isAPartOf(wsaaPrtLine001, 26, FILLER).init("Product Health Questionaire                  SR675");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(52);
	private FixedLengthStringData filler9 = new FixedLengthStringData(36).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine004, 36, FILLER).init("Questionaire Set");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(53);
	private FixedLengthStringData filler11 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine005, 15, FILLER).init("Set#      Age       Male        Female");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(54);
	private FixedLengthStringData filler13 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine006, 16, FILLER).init("1");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 25).setPattern("ZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 28, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 33);
	private FixedLengthStringData filler16 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 46);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(54);
	private FixedLengthStringData filler17 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler18 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine007, 16, FILLER).init("2");
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 25).setPattern("ZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 28, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 33);
	private FixedLengthStringData filler20 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 46);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(54);
	private FixedLengthStringData filler21 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler22 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 16, FILLER).init("3");
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 25).setPattern("ZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 28, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 33);
	private FixedLengthStringData filler24 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 46);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(54);
	private FixedLengthStringData filler25 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler26 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 16, FILLER).init("4");
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 25).setPattern("ZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 28, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 33);
	private FixedLengthStringData filler28 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 46);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(54);
	private FixedLengthStringData filler29 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler30 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 16, FILLER).init("5");
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 25).setPattern("ZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 28, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 33);
	private FixedLengthStringData filler32 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 46);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(54);
	private FixedLengthStringData filler33 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler34 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine011, 16, FILLER).init("6");
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 25).setPattern("ZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 28, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 33);
	private FixedLengthStringData filler36 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 46);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(62);
	private FixedLengthStringData filler37 = new FixedLengthStringData(46).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler38 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine012, 46, FILLER).init("BMI Basis:");
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 57);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(28);
	private FixedLengthStringData filler39 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine013, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tr675rec tr675rec = new Tr675rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr675pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr675rec.tr675Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(tr675rec.age01);
		fieldNo008.set(tr675rec.questset01);
		fieldNo009.set(tr675rec.questset02);
		fieldNo010.set(tr675rec.age02);
		fieldNo011.set(tr675rec.questset03);
		fieldNo012.set(tr675rec.questset04);
		fieldNo013.set(tr675rec.age03);
		fieldNo014.set(tr675rec.questset05);
		fieldNo015.set(tr675rec.questset06);
		fieldNo016.set(tr675rec.age04);
		fieldNo017.set(tr675rec.questset07);
		fieldNo018.set(tr675rec.questset08);
		fieldNo019.set(tr675rec.age05);
		fieldNo020.set(tr675rec.questset09);
		fieldNo021.set(tr675rec.questset10);
		fieldNo022.set(tr675rec.age06);
		fieldNo023.set(tr675rec.questset11);
		fieldNo024.set(tr675rec.questset12);
		fieldNo025.set(tr675rec.bmibasis);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
