package com.csc.life.underwriting.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6776
 * @version 1.0 generated on 30/08/09 07:00
 * @author Quipoz
 */
public class S6776ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(193);
	public FixedLengthStringData dataFields = new FixedLengthStringData(49).isAPartOf(dataArea, 0);
	public FixedLengthStringData boolansws = new FixedLengthStringData(2).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] boolansw = FLSArrayPartOfStructure(2, 1, boolansws, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(boolansws, 0, FILLER_REDEFINE);
	public FixedLengthStringData boolansw01 = DD.boolansw.copy().isAPartOf(filler,0);
	public FixedLengthStringData boolansw02 = DD.boolansw.copy().isAPartOf(filler,1);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData intgflag = DD.intgflag.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,4);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData rulechcks = new FixedLengthStringData(2).isAPartOf(dataFields, 42);
	public FixedLengthStringData[] rulechck = FLSArrayPartOfStructure(2, 1, rulechcks, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(2).isAPartOf(rulechcks, 0, FILLER_REDEFINE);
	public FixedLengthStringData rulechck01 = DD.rulechck.copy().isAPartOf(filler1,0);
	public FixedLengthStringData rulechck02 = DD.rulechck.copy().isAPartOf(filler1,1);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 49);
	public FixedLengthStringData boolanswsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] boolanswErr = FLSArrayPartOfStructure(2, 4, boolanswsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(boolanswsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData boolansw01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData boolansw02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData intgflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData rulechcksErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] rulechckErr = FLSArrayPartOfStructure(2, 4, rulechcksErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(rulechcksErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData rulechck01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData rulechck02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 85);
	public FixedLengthStringData boolanswsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] boolanswOut = FLSArrayPartOfStructure(2, 12, boolanswsOut, 0);
	public FixedLengthStringData[][] boolanswO = FLSDArrayPartOfArrayStructure(12, 1, boolanswOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(boolanswsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] boolansw01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] boolansw02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] intgflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData rulechcksOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] rulechckOut = FLSArrayPartOfStructure(2, 12, rulechcksOut, 0);
	public FixedLengthStringData[][] rulechckO = FLSDArrayPartOfArrayStructure(12, 1, rulechckOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(rulechcksOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] rulechck01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] rulechck02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6776screenWritten = new LongData(0);
	public LongData S6776protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6776ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, intgflag, boolansw01, boolansw02, rulechck01, rulechck02};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, intgflagOut, boolansw01Out, boolansw02Out, rulechck01Out, rulechck02Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, intgflagErr, boolansw01Err, boolansw02Err, rulechck01Err, rulechck02Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6776screen.class;
		protectRecord = S6776protect.class;
	}

}
