package com.csc.life.underwriting.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class S6775screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 17, 22, 4, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 1, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6775ScreenVars sv = (S6775ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6775screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6775ScreenVars screenVars = (S6775ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.undwrule01.setClassString("");
		screenVars.undwrule13.setClassString("");
		screenVars.undwrule02.setClassString("");
		screenVars.undwrule03.setClassString("");
		screenVars.undwrule04.setClassString("");
		screenVars.undwrule05.setClassString("");
		screenVars.undwrule06.setClassString("");
		screenVars.undwrule07.setClassString("");
		screenVars.undwrule08.setClassString("");
		screenVars.undwrule09.setClassString("");
		screenVars.undwrule10.setClassString("");
		screenVars.undwrule11.setClassString("");
		screenVars.undwrule12.setClassString("");
		screenVars.undwrule14.setClassString("");
		screenVars.undwrule15.setClassString("");
		screenVars.undwrule16.setClassString("");
		screenVars.undwrule17.setClassString("");
		screenVars.undwrule18.setClassString("");
		screenVars.undwrule19.setClassString("");
		screenVars.undwrule20.setClassString("");
		screenVars.undwrule21.setClassString("");
		screenVars.undwrule22.setClassString("");
		screenVars.undwrule23.setClassString("");
		screenVars.undwrule24.setClassString("");
		screenVars.cont.setClassString("");
		screenVars.intgfrom01.setClassString("");
		screenVars.intgto01.setClassString("");
		screenVars.intgfrom02.setClassString("");
		screenVars.intgto02.setClassString("");
		screenVars.intgfrom03.setClassString("");
		screenVars.intgto03.setClassString("");
		screenVars.intgfrom04.setClassString("");
		screenVars.intgto04.setClassString("");
		screenVars.intgto05.setClassString("");
		screenVars.intgto06.setClassString("");
		screenVars.intgto07.setClassString("");
		screenVars.intgto08.setClassString("");
		screenVars.intgto09.setClassString("");
		screenVars.intgto10.setClassString("");
		screenVars.intgto11.setClassString("");
		screenVars.intgto12.setClassString("");
		screenVars.intgfrom05.setClassString("");
		screenVars.intgfrom06.setClassString("");
		screenVars.intgfrom07.setClassString("");
		screenVars.intgfrom08.setClassString("");
		screenVars.intgfrom09.setClassString("");
		screenVars.intgfrom11.setClassString("");
		screenVars.intgfrom10.setClassString("");
		screenVars.intgfrom12.setClassString("");
		screenVars.intgfrom13.setClassString("");
		screenVars.intgfrom14.setClassString("");
		screenVars.intgfrom15.setClassString("");
		screenVars.intgfrom16.setClassString("");
		screenVars.intgfrom17.setClassString("");
		screenVars.intgfrom18.setClassString("");
		screenVars.intgfrom19.setClassString("");
		screenVars.intgfrom20.setClassString("");
		screenVars.intgfrom21.setClassString("");
		screenVars.intgfrom22.setClassString("");
		screenVars.intgfrom23.setClassString("");
		screenVars.intgfrom24.setClassString("");
		screenVars.intgto13.setClassString("");
		screenVars.intgto14.setClassString("");
		screenVars.intgto15.setClassString("");
		screenVars.intgto16.setClassString("");
		screenVars.intgto17.setClassString("");
		screenVars.intgto18.setClassString("");
		screenVars.intgto19.setClassString("");
		screenVars.intgto20.setClassString("");
		screenVars.intgto21.setClassString("");
		screenVars.intgto22.setClassString("");
		screenVars.intgto23.setClassString("");
		screenVars.intgto24.setClassString("");
	}

/**
 * Clear all the variables in S6775screen
 */
	public static void clear(VarModel pv) {
		S6775ScreenVars screenVars = (S6775ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.undwrule01.clear();
		screenVars.undwrule13.clear();
		screenVars.undwrule02.clear();
		screenVars.undwrule03.clear();
		screenVars.undwrule04.clear();
		screenVars.undwrule05.clear();
		screenVars.undwrule06.clear();
		screenVars.undwrule07.clear();
		screenVars.undwrule08.clear();
		screenVars.undwrule09.clear();
		screenVars.undwrule10.clear();
		screenVars.undwrule11.clear();
		screenVars.undwrule12.clear();
		screenVars.undwrule14.clear();
		screenVars.undwrule15.clear();
		screenVars.undwrule16.clear();
		screenVars.undwrule17.clear();
		screenVars.undwrule18.clear();
		screenVars.undwrule19.clear();
		screenVars.undwrule20.clear();
		screenVars.undwrule21.clear();
		screenVars.undwrule22.clear();
		screenVars.undwrule23.clear();
		screenVars.undwrule24.clear();
		screenVars.cont.clear();
		screenVars.intgfrom01.clear();
		screenVars.intgto01.clear();
		screenVars.intgfrom02.clear();
		screenVars.intgto02.clear();
		screenVars.intgfrom03.clear();
		screenVars.intgto03.clear();
		screenVars.intgfrom04.clear();
		screenVars.intgto04.clear();
		screenVars.intgto05.clear();
		screenVars.intgto06.clear();
		screenVars.intgto07.clear();
		screenVars.intgto08.clear();
		screenVars.intgto09.clear();
		screenVars.intgto10.clear();
		screenVars.intgto11.clear();
		screenVars.intgto12.clear();
		screenVars.intgfrom05.clear();
		screenVars.intgfrom06.clear();
		screenVars.intgfrom07.clear();
		screenVars.intgfrom08.clear();
		screenVars.intgfrom09.clear();
		screenVars.intgfrom11.clear();
		screenVars.intgfrom10.clear();
		screenVars.intgfrom12.clear();
		screenVars.intgfrom13.clear();
		screenVars.intgfrom14.clear();
		screenVars.intgfrom15.clear();
		screenVars.intgfrom16.clear();
		screenVars.intgfrom17.clear();
		screenVars.intgfrom18.clear();
		screenVars.intgfrom19.clear();
		screenVars.intgfrom20.clear();
		screenVars.intgfrom21.clear();
		screenVars.intgfrom22.clear();
		screenVars.intgfrom23.clear();
		screenVars.intgfrom24.clear();
		screenVars.intgto13.clear();
		screenVars.intgto14.clear();
		screenVars.intgto15.clear();
		screenVars.intgto16.clear();
		screenVars.intgto17.clear();
		screenVars.intgto18.clear();
		screenVars.intgto19.clear();
		screenVars.intgto20.clear();
		screenVars.intgto21.clear();
		screenVars.intgto22.clear();
		screenVars.intgto23.clear();
		screenVars.intgto24.clear();
	}
}
