package com.csc.life.underwriting.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6774
 * @version 1.0 generated on 30/08/09 07:00
 * @author Quipoz
 */
public class S6774ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(41);
	public FixedLengthStringData dataFields = new FixedLengthStringData(9).isAPartOf(dataArea, 0);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData undwflag = DD.undwflag.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(8).isAPartOf(dataArea, 9);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData undwflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 17);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] undwflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public FixedLengthStringData select = DD.select.copy();


	public LongData S6774screenWritten = new LongData(0);
	public LongData S6774windowWritten = new LongData(0);
	public LongData S6774protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6774ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {lifcnum, undwflag};
		screenOutFields = new BaseData[][] {lifcnumOut, undwflagOut};
		screenErrFields = new BaseData[] {lifcnumErr, undwflagErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6774screen.class;
		protectRecord = S6774protect.class;
	}

}
