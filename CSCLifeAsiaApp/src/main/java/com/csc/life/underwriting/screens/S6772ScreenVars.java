package com.csc.life.underwriting.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6772
 * @version 1.0 generated on 30/08/09 07:00
 * @author Quipoz
 */
public class S6772ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(589);
	public FixedLengthStringData dataFields = new FixedLengthStringData(125).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData fupcdess = new FixedLengthStringData(40).isAPartOf(dataFields, 1);
	public FixedLengthStringData[] fupcdes = FLSArrayPartOfStructure(10, 4, fupcdess, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(fupcdess, 0, FILLER_REDEFINE);
	public FixedLengthStringData fupcdes01 = DD.fupcdes.copy().isAPartOf(filler,0);
	public FixedLengthStringData fupcdes02 = DD.fupcdes.copy().isAPartOf(filler,4);
	public FixedLengthStringData fupcdes03 = DD.fupcdes.copy().isAPartOf(filler,8);
	public FixedLengthStringData fupcdes04 = DD.fupcdes.copy().isAPartOf(filler,12);
	public FixedLengthStringData fupcdes05 = DD.fupcdes.copy().isAPartOf(filler,16);
	public FixedLengthStringData fupcdes06 = DD.fupcdes.copy().isAPartOf(filler,20);
	public FixedLengthStringData fupcdes07 = DD.fupcdes.copy().isAPartOf(filler,24);
	public FixedLengthStringData fupcdes08 = DD.fupcdes.copy().isAPartOf(filler,28);
	public FixedLengthStringData fupcdes09 = DD.fupcdes.copy().isAPartOf(filler,32);
	public FixedLengthStringData fupcdes10 = DD.fupcdes.copy().isAPartOf(filler,36);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,41);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,49);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,57);
	public FixedLengthStringData language = DD.language.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,66);
	public FixedLengthStringData opcdas = new FixedLengthStringData(20).isAPartOf(dataFields, 96);
	public FixedLengthStringData[] opcda = FLSArrayPartOfStructure(10, 2, opcdas, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(opcdas, 0, FILLER_REDEFINE);
	public FixedLengthStringData opcda01 = DD.opcda.copy().isAPartOf(filler1,0);
	public FixedLengthStringData opcda02 = DD.opcda.copy().isAPartOf(filler1,2);
	public FixedLengthStringData opcda03 = DD.opcda.copy().isAPartOf(filler1,4);
	public FixedLengthStringData opcda04 = DD.opcda.copy().isAPartOf(filler1,6);
	public FixedLengthStringData opcda05 = DD.opcda.copy().isAPartOf(filler1,8);
	public FixedLengthStringData opcda06 = DD.opcda.copy().isAPartOf(filler1,10);
	public FixedLengthStringData opcda07 = DD.opcda.copy().isAPartOf(filler1,12);
	public FixedLengthStringData opcda08 = DD.opcda.copy().isAPartOf(filler1,14);
	public FixedLengthStringData opcda09 = DD.opcda.copy().isAPartOf(filler1,16);
	public FixedLengthStringData opcda10 = DD.opcda.copy().isAPartOf(filler1,18);
	public FixedLengthStringData ruletype = DD.ruletype.copy().isAPartOf(dataFields,116);
	public ZonedDecimalData seqnumb = DD.seqnumb.copyToZonedDecimal().isAPartOf(dataFields,117);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,120);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(116).isAPartOf(dataArea, 125);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData fupcdessErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] fupcdesErr = FLSArrayPartOfStructure(10, 4, fupcdessErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(fupcdessErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData fupcdes01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData fupcdes02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData fupcdes03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData fupcdes04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData fupcdes05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData fupcdes06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData fupcdes07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData fupcdes08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData fupcdes09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData fupcdes10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData languageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData opcdasErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData[] opcdaErr = FLSArrayPartOfStructure(10, 4, opcdasErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(opcdasErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData opcda01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData opcda02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData opcda03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData opcda04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData opcda05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData opcda06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData opcda07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData opcda08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData opcda09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData opcda10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData ruletypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData seqnumbErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(348).isAPartOf(dataArea, 241);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData fupcdessOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] fupcdesOut = FLSArrayPartOfStructure(10, 12, fupcdessOut, 0);
	public FixedLengthStringData[][] fupcdesO = FLSDArrayPartOfArrayStructure(12, 1, fupcdesOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(120).isAPartOf(fupcdessOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] fupcdes01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] fupcdes02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] fupcdes03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] fupcdes04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] fupcdes05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] fupcdes06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] fupcdes07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] fupcdes08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] fupcdes09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] fupcdes10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] languageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData opcdasOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 192);
	public FixedLengthStringData[] opcdaOut = FLSArrayPartOfStructure(10, 12, opcdasOut, 0);
	public FixedLengthStringData[][] opcdaO = FLSDArrayPartOfArrayStructure(12, 1, opcdaOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(120).isAPartOf(opcdasOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] opcda01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] opcda02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] opcda03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] opcda04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] opcda05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] opcda06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] opcda07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] opcda08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] opcda09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] opcda10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] ruletypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] seqnumbOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6772screenWritten = new LongData(0);
	public LongData S6772protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6772ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {language, company, tabl, item, longdesc, itmfrm, itmto, fupcdes01, fupcdes02, fupcdes03, fupcdes04, fupcdes05, fupcdes06, fupcdes07, fupcdes08, fupcdes09, fupcdes10, opcda01, opcda02, opcda03, opcda04, opcda05, opcda06, opcda07, opcda08, opcda09, opcda10, ruletype, seqnumb};
		screenOutFields = new BaseData[][] {languageOut, companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, fupcdes01Out, fupcdes02Out, fupcdes03Out, fupcdes04Out, fupcdes05Out, fupcdes06Out, fupcdes07Out, fupcdes08Out, fupcdes09Out, fupcdes10Out, opcda01Out, opcda02Out, opcda03Out, opcda04Out, opcda05Out, opcda06Out, opcda07Out, opcda08Out, opcda09Out, opcda10Out, ruletypeOut, seqnumbOut};
		screenErrFields = new BaseData[] {languageErr, companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, fupcdes01Err, fupcdes02Err, fupcdes03Err, fupcdes04Err, fupcdes05Err, fupcdes06Err, fupcdes07Err, fupcdes08Err, fupcdes09Err, fupcdes10Err, opcda01Err, opcda02Err, opcda03Err, opcda04Err, opcda05Err, opcda06Err, opcda07Err, opcda08Err, opcda09Err, opcda10Err, ruletypeErr, seqnumbErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6772screen.class;
		protectRecord = S6772protect.class;
	}

}
