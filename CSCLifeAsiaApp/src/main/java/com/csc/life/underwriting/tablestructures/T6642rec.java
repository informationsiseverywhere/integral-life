package com.csc.life.underwriting.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:48
 * Description:
 * Copybook name: T6642REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6642rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6642Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData fmageadj = new ZonedDecimalData(2, 0).isAPartOf(t6642Rec, 0);
  	public FixedLengthStringData jleqage = new FixedLengthStringData(4).isAPartOf(t6642Rec, 2);
  	public FixedLengthStringData jlrevbns = new FixedLengthStringData(8).isAPartOf(t6642Rec, 6);
  	public FixedLengthStringData jlsumass = new FixedLengthStringData(8).isAPartOf(t6642Rec, 14);
  	public FixedLengthStringData slrevbns = new FixedLengthStringData(8).isAPartOf(t6642Rec, 22);
  	public FixedLengthStringData slsumass = new FixedLengthStringData(8).isAPartOf(t6642Rec, 30);
  	public FixedLengthStringData filler = new FixedLengthStringData(462).isAPartOf(t6642Rec, 38, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6642Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6642Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}