package com.csc.life.underwriting.dataaccess.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "UNDLPF", schema = "VM1DTA")
public class Undlpf implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2830219787250717845L;

	@Id
	@Column(name = "UNIQUE_NUMBER")
	private Long unique_number;
	public String chdrcoy;
	public String chdrnum;
	public String life;
	public String jlife;
	public Integer currfrom;//IBPLIFE-8671
	public Integer currto;//IBPLIFE-8671
	public Integer height;//IBPLIFE-8671
	public Integer weight;//IBPLIFE-8671
	public BigDecimal bmi;
	public String bmibasis;
	public String bmirule;
	public String ovrrule;
	public String clntnum01;
	public String clntnum02;
	public String clntnum03;
	public String clntnum04;
	public String clntnum05;
	public Integer tranno;//IBPLIFE-8671
	public String undwflag;
	public String validflag;
	private String usrprf;//IBPLIFE-8671
	private String jobnm;//IBPLIFE-8671
	@Transient
	private Long uniqueNumber;//IBPLIFE-8671
	@Transient
	public String userProfile;//IBPLIFE-8671
	@Transient
	public String jobName;
	
	@Transient
	public String datime; // IBPLIFE-8671
	
	public Undlpf() {
		chdrcoy = "";
		chdrnum = "";
		life = "";
		jlife = "";
		currfrom = 0;
		currto = 0;
		height = 0;
		weight = 0;
		bmi = BigDecimal.ZERO;
		bmibasis = "";
		bmirule = "";
		ovrrule = "";
		clntnum01 = "";
		clntnum02 = "";
		clntnum03 = "";
		clntnum04 = "";
		clntnum05 = "";
		tranno = 0;
		undwflag = "";
		validflag = "";
		usrprf="";
		jobnm="";
		jobName="";
		userProfile="";
		datime ="";
	}
	
	// IBPLIFE-8671 start
	public Undlpf(Undlpf temp) { 
		this.life=temp.life;	
		this.jlife=temp.jlife;
		this.chdrcoy=temp.chdrcoy;
		this.chdrnum=temp.chdrnum;
	}
	// IBPLIFE-8671 end

	public Long getUniqueNumber() {
		return unique_number;
	}
	public void setUniqueNumber(Long uniqueNumber) {
		this.unique_number = uniqueNumber;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getJlife() {
		return jlife;
	}
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	public Integer getCurrfrom() { 
		return currfrom;
	}
	public void setCurrfrom(Integer currfrom) {
		this.currfrom = currfrom;
	}
	public Integer getCurrto() {
		return currto;
	}
	public void setCurrto(int currto) {
		this.currto = currto;
	}
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	public BigDecimal getBmi() {
		return bmi;
	}
	public void setBmi(BigDecimal bmi) {
		this.bmi = bmi;
	}
	public String getBmibasis() {
		return bmibasis;
	}
	public void setBmibasis(String bmibasis) {
		this.bmibasis = bmibasis;
	}
	public String getBmirule() {
		return bmirule;
	}
	public void setBmirule(String bmirule) {
		this.bmirule = bmirule;
	}
	public String getOvrrule() {
		return ovrrule;
	}
	public void setOvrrule(String ovrrule) {
		this.ovrrule = ovrrule;
	}
	public String getClntnum01() {
		return clntnum01;
	}
	public void setClntnum01(String clntnum01) {
		this.clntnum01 = clntnum01;
	}
	public String getClntnum02() {
		return clntnum02;
	}
	public void setClntnum02(String clntnum02) {
		this.clntnum02 = clntnum02;
	}
	public String getClntnum03() {
		return clntnum03;
	}
	public void setClntnum03(String clntnum03) {
		this.clntnum03 = clntnum03;
	}
	public String getClntnum04() {
		return clntnum04;
	}
	public void setClntnum04(String clntnum04) {
		this.clntnum04 = clntnum04;
	}
	public String getClntnum05() {
		return clntnum05;
	}
	public void setClntnum05(String clntnum05) {
		this.clntnum05 = clntnum05;
	}
	public Integer getTranno() {
		return tranno;
	}
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}
	public String getUndwflag() {
		return undwflag;
	}
	public void setUndwflag(String undwflag) {
		this.undwflag = undwflag;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}

	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

}