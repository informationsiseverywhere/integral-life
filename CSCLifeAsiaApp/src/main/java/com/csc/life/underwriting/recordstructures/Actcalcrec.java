package com.csc.life.underwriting.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:48
 * Description:
 * Copybook name: ACTCALCREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Actcalcrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData actcalcRec = new FixedLengthStringData(50);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(actcalcRec, 0);
  	public FixedLengthStringData basis = new FixedLengthStringData(8).isAPartOf(actcalcRec, 5);
  	public ZonedDecimalData initialAge = new ZonedDecimalData(3, 0).isAPartOf(actcalcRec, 13).setUnsigned();
  	public ZonedDecimalData finalAge1 = new ZonedDecimalData(3, 0).isAPartOf(actcalcRec, 16).setUnsigned();
  	public ZonedDecimalData finalAge2 = new ZonedDecimalData(3, 0).isAPartOf(actcalcRec, 19).setUnsigned();
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(actcalcRec, 22);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(actcalcRec, 23);
  	public PackedDecimalData value = new PackedDecimalData(18, 9).isAPartOf(actcalcRec, 27);
  	public FixedLengthStringData filler = new FixedLengthStringData(13).isAPartOf(actcalcRec, 37, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(actcalcRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		actcalcRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}