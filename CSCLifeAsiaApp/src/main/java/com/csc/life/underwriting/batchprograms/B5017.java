/*
 * File: B5017.java
 * Date: 29 August 2009 20:51:10
 * Author: Quipoz Limited
 *
 * Class transformed from B5017.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.power;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.underwriting.recordstructures.P5017par;
import com.csc.life.underwriting.tablestructures.T6641rec;
import com.csc.life.underwriting.tablestructures.T6686rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* This program will take an item on T6686, and calculate the
* Monetary functions Nx, for T6641 and create an item entry
* on that table. The item key consists of 8 characters, the
* first five are the name of the item on T6686, and the last
* three are the percentage rate for that item. This takes the
* form of, for example; 4 percent is shown as 400.
* So an item key could be :- 6770S400.
*
* The calculation formula used in the program is as follows;
*
*     Nx = £Nx + 1] + £ Lx / £(1 + i) ** x] ].
*
*     Nx - Cash required now to cover lives in N years time
*          allowing for mortality.
*     Lx - Mortality figures for ages 11 - 109.
*     i  - interest rate.
*
*
*
*****************************************************************
* </pre>
*/
public class B5017 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5017");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSub3 = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSub4 = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaLxdp = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaNxdp = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaItemFormat = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItem = new FixedLengthStringData(5).isAPartOf(wsaaItemFormat, 0).init(SPACES);
	private ZonedDecimalData wsaaItemPerc = new ZonedDecimalData(3, 0).isAPartOf(wsaaItemFormat, 5).init(ZERO).setUnsigned();
		/* WSAA-CHECK-FIELD */
	private ZonedDecimalData wsaaCheck = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaCheckR = new FixedLengthStringData(8).isAPartOf(wsaaCheck, 0, REDEFINE);
	private ZonedDecimalData wsaaCk1 = new ZonedDecimalData(1, 0).isAPartOf(wsaaCheckR, 0).setUnsigned();
	private ZonedDecimalData wsaaCk2 = new ZonedDecimalData(1, 0).isAPartOf(wsaaCheckR, 1).setUnsigned();
	private ZonedDecimalData wsaaCk3 = new ZonedDecimalData(1, 0).isAPartOf(wsaaCheckR, 2).setUnsigned();
	private ZonedDecimalData wsaaCk4 = new ZonedDecimalData(1, 0).isAPartOf(wsaaCheckR, 3).setUnsigned();
	private ZonedDecimalData wsaaCk5 = new ZonedDecimalData(1, 0).isAPartOf(wsaaCheckR, 4).setUnsigned();
	private ZonedDecimalData wsaaCk6 = new ZonedDecimalData(1, 0).isAPartOf(wsaaCheckR, 5).setUnsigned();
	private ZonedDecimalData wsaaCk7 = new ZonedDecimalData(1, 0).isAPartOf(wsaaCheckR, 6).setUnsigned();
	private ZonedDecimalData wsaaCk8 = new ZonedDecimalData(1, 0).isAPartOf(wsaaCheckR, 7).setUnsigned();
	private ZonedDecimalData wsaaMultiply = new ZonedDecimalData(9, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaInterest = new ZonedDecimalData(16, 8).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaOrigInt = new ZonedDecimalData(16, 8).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaNxmon = new ZonedDecimalData(16, 8).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaLxmrt = new ZonedDecimalData(16, 8).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaCompute = new ZonedDecimalData(16, 8).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaStorage = new FixedLengthStringData(1776);
	private ZonedDecimalData[] wsaaStore = ZDArrayPartOfStructure(110, 16, 8, wsaaStorage, 0, UNSIGNED_TRUE);
	private ZonedDecimalData wsaaStored = new ZonedDecimalData(16, 8).isAPartOf(wsaaStorage, 1760).setUnsigned();

		/*  88 WSAA-OTHER-ROW             VALUE 07 14 21 28
		                                35 42 49 56 63
		                                70 77 84 91 98.
		  88 WSAA-FIRST-ROW             VALUE 01.                        */
	private ZonedDecimalData wsaaRowCheck = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private Validator wsaaOtherRow = new Validator(wsaaRowCheck, 7,14,21,28,35,42,49,56,63,70,77,84,91,98,105);
	private Validator wsaaFirstRow = new Validator(wsaaRowCheck, 1);
		/* FORMATS */
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private static final String t6641 = "T6641";
	private static final String t6686 = "T6686";
		/* ERRORS */
	private static final String g561 = "G561";
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Varcom varcom = new Varcom();
	private T6641rec t6641rec = new T6641rec();
	private T6686rec t6686rec = new T6686rec();
	private P5017par p5017par = new P5017par();
	private WsaaDecimalCheckInner wsaaDecimalCheckInner = new WsaaDecimalCheckInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextItem330,
		next520,
		continue550
	}

	public B5017() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		initialise200();
		readItemRecord300();
		/*EXIT*/
	}

protected void initialise200()
	{
		/*210-INITIALISE.*/
		p5017par.parmRecord.set(conjobrec.params);
		/*SET-UP-OTHER-FIELDS*/
		wsaaStorage.set(ZERO);
		t6641rec.t6641Rec.set(ZERO);
		/*    MOVE P5017-ITEM             TO WSAA-ITEM-FORMAT.             */
		wsaaItemFormat.set(p5017par.itemToCreate);
		/* Put interest rate into a valid form.*/
		compute(wsaaOrigInt, 8).set(div(wsaaItemPerc,10000));
		compute(wsaaOrigInt, 8).set(add(1,wsaaOrigInt));
		/*    MOVE 99                     TO WSAA-SUB.                     */
		/*    MOVE 100                    TO WSAA-SUB1.                    */
		/*    MOVE 110                    TO WSAA-SUB2.                    */
		wsaaSub.set(110);
		wsaaSub1.set(111);
		wsaaSub2.set(121);
		/*EXIT*/
	}

protected void readItemRecord300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					beginReading310();
				case nextItem330:
					nextItem330();
					continue350();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void beginReading310()
	{
		/*  Start READ of T6686 to get item values for calculation.*/
		/*    MOVE P5017-ITEM             TO WSAA-ITEM-FORMAT.             */
		wsaaItemFormat.set(p5017par.itemToCreate);
		itemIO.setDataArea(SPACES);
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(t6686);
		itemIO.setItemitem(wsaaItem);
		itemIO.setItempfx("IT");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(itemIO.getParams());
			conerrrec.statuz.set(g561);
			databaseError006();
		}
		t6686rec.t6686Rec.set(itemIO.getGenarea());
		wsaaStore[wsaaSub.toInt()].set(ZERO);
		/*    MOVE ZERO                   TO T6641-NXMON(WSAA-SUB).        */
		if (isLT(wsaaSub, 100)) {
		t6641rec.nxmon[wsaaSub.toInt()].set(ZERO);
		}
		else {
			t6641rec.tnxmon[sub(wsaaSub, 99).toInt()].set(ZERO);
		}
		wsaaSub3.set(6);
	}

protected void nextItem330()
	{
		/* Entries from T6686 have to be read backwards for the*/
		/* the calculation to work.*/
		wsaaSub.subtract(1);
		wsaaSub1.subtract(1);
		wsaaSub2.subtract(1);
		/* This equates to £( 1 + i ) ** x ].*/
		compute(wsaaInterest, 8).set((power(wsaaOrigInt,wsaaSub2)));
		/* To check the number of decimal places used on T6686.*/
		wsaaDecimalCheckInner.wsaaDecimalCheck.set(wsaaSub);
		if (wsaaDecimalCheckInner.wsaaRow16.isTrue()) {
			wsaaLxdp.set(t6686rec.lxdp[16]);
			moveLxmrt700();
		}
		if (wsaaDecimalCheckInner.wsaaRow15.isTrue()) {
			wsaaLxdp.set(t6686rec.lxdp[15]);
			moveLxmrt700();
		}
		if (wsaaDecimalCheckInner.wsaaRow14.isTrue()) {
			wsaaLxdp.set(t6686rec.lxdp[14]);
			moveLxmrt700();
		}
		if (wsaaDecimalCheckInner.wsaaRow13.isTrue()) {
			wsaaLxdp.set(t6686rec.lxdp[13]);
			moveLxmrt700();
		}
		if (wsaaDecimalCheckInner.wsaaRow12.isTrue()) {
			wsaaLxdp.set(t6686rec.lxdp[12]);
			moveLxmrt700();
		}
		if (wsaaDecimalCheckInner.wsaaRow11.isTrue()) {
			wsaaLxdp.set(t6686rec.lxdp[11]);
			moveLxmrt700();
		}
		if (wsaaDecimalCheckInner.wsaaRow10.isTrue()) {
			wsaaLxdp.set(t6686rec.lxdp[10]);
			moveLxmrt700();
		}
		if (wsaaDecimalCheckInner.wsaaRow9.isTrue()) {
			wsaaLxdp.set(t6686rec.lxdp[9]);
			moveLxmrt700();
		}
		if (wsaaDecimalCheckInner.wsaaRow8.isTrue()) {
			wsaaLxdp.set(t6686rec.lxdp[8]);
			moveLxmrt700();
		}
		if (wsaaDecimalCheckInner.wsaaRow7.isTrue()) {
			wsaaLxdp.set(t6686rec.lxdp[7]);
			moveLxmrt700();
		}
		if (wsaaDecimalCheckInner.wsaaRow6.isTrue()) {
			wsaaLxdp.set(t6686rec.lxdp[6]);
			moveLxmrt700();
		}
		if (wsaaDecimalCheckInner.wsaaRow5.isTrue()) {
			wsaaLxdp.set(t6686rec.lxdp[5]);
			moveLxmrt700();
		}
		if (wsaaDecimalCheckInner.wsaaRow4.isTrue()) {
			wsaaLxdp.set(t6686rec.lxdp[4]);
			moveLxmrt700();
		}
		if (wsaaDecimalCheckInner.wsaaRow3.isTrue()) {
			wsaaLxdp.set(t6686rec.lxdp[3]);
			moveLxmrt700();
		}
		if (wsaaDecimalCheckInner.wsaaRow2.isTrue()) {
			wsaaLxdp.set(t6686rec.lxdp[2]);
			moveLxmrt700();
		}
		if (wsaaDecimalCheckInner.wsaaRow1.isTrue()) {
			wsaaLxdp.set(t6686rec.lxdp[1]);
			moveLxmrt700();
		}
		/* This equates to £ Lx / £ ( 1 + i ) ** x ].*/
		compute(wsaaNxmon, 8).set(div(wsaaLxmrt,wsaaInterest));
		/* This equates to Nx = (Nx + 1) + £ Lx / £ (1 + i) ** x ].*/
		compute(wsaaCompute, 8).set((add(wsaaStore[wsaaSub1.toInt()],wsaaNxmon)));
		wsaaSub3.add(1);
		wsaaStore[wsaaSub.toInt()].set(wsaaCompute);
		wsaaRowCheck.set(wsaaSub);
		/* The values are stored for each row, and when a row is*/
		/* complete, the decimal places to be displayed on the screen*/
		/* are calculated.*/
		if (wsaaOtherRow.isTrue()
		&& isEQ(wsaaSub3,7)) {
			moveNxmon500();
			wsaaSub3.set(ZERO);
		}
		if (wsaaFirstRow.isTrue()
		&& isEQ(wsaaSub3,6)) {
			moveNxmon500();
			wsaaSub3.set(ZERO);
		}
		/* This is for the first field of the array of 100 fields.*/
		if (isGT(wsaaSub,1)) {
			goTo(GotoLabel.nextItem330);
		}
		wsaaSub1.subtract(1);
		/* This equates to £( 1 + i ) ** x ].*/
		compute(wsaaInterest, 8).set((power(wsaaOrigInt,11)));
		wsaaLxdp.set(t6686rec.lxdp[1]);
		moveLxmort800();
		/* This equates to £ Lx / £ ( 1 + i ) ** x ].*/
		compute(wsaaNxmon, 8).set(div(wsaaLxmrt,wsaaInterest));
		/* This equates to Nx = (Nx + 1) + £ Lx / £ (1 + i) ** x ].*/
		compute(wsaaCompute, 8).set((add(wsaaStore[wsaaSub1.toInt()],wsaaNxmon)));
		wsaaStored.set(wsaaCompute);
		moveNxmoney600();
	}

protected void continue350()
	{
		/* Writes the completed item to T6641.*/
		itemIO.setDataArea(SPACES);
		itemIO.setGenarea(t6641rec.t6641Rec);
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(t6641);
		itemIO.setValidflag("1");
		itemIO.setItmfrm(runparmrec.effdate);
		itemIO.setItmto(varcom.vrcmMaxDate);
		/*    MOVE P5017-ITEM             TO ITEM-ITEMITEM.                */
		itemIO.setItemitem(p5017par.itemToCreate);
		itemIO.setItempfx("IT");
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.statuz.set(itemIO.getStatuz());
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		/* Writes the item description to T6641.*/
		descIO.setDescpfx("IT");
		/*    MOVE P5017-ITEM             TO DESC-SHORTDESC.               */
		descIO.setShortdesc(p5017par.itemToCreate);
		descIO.setLongdesc(p5017par.longdesc);
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t6641);
		descIO.setLanguage(runparmrec.language);
		/*    MOVE P5017-ITEM             TO DESC-DESCITEM                 */
		descIO.setDescitem(p5017par.itemToCreate);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(descIO.getParams());
			conerrrec.statuz.set(descIO.getStatuz());
			databaseError006();
		}
	}

protected void moveNxmon500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start510();
				case next520:
					next520();
				case continue550:
					continue550();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*  Check to see how many decimal places are needed.
	* </pre>
	*/
protected void start510()
	{
		wsaaCheck.set(wsaaStore[wsaaSub.toInt()]);
		if (isGT(wsaaCk1,0)) {
			wsaaNxdp.set(ZERO);
			wsaaMultiply.set(1);
			goTo(GotoLabel.next520);
		}
		if (isGT(wsaaCk2,0)) {
			wsaaNxdp.set(1);
			wsaaMultiply.set(10);
			goTo(GotoLabel.next520);
		}
		if (isGT(wsaaCk3,0)) {
			wsaaNxdp.set(2);
			wsaaMultiply.set(100);
			goTo(GotoLabel.next520);
		}
		if (isGT(wsaaCk4,0)) {
			wsaaNxdp.set(3);
			wsaaMultiply.set(1000);
			goTo(GotoLabel.next520);
		}
		if (isGT(wsaaCk5,0)) {
			wsaaNxdp.set(4);
			wsaaMultiply.set(10000);
			goTo(GotoLabel.next520);
		}
		if (isGT(wsaaCk6,0)) {
			wsaaNxdp.set(5);
			wsaaMultiply.set(100000);
			goTo(GotoLabel.next520);
		}
		if (isGT(wsaaCk7,0)) {
			wsaaNxdp.set(6);
			wsaaMultiply.set(1000000);
			goTo(GotoLabel.next520);
		}
		if (isGT(wsaaCk8,0)) {
			wsaaNxdp.set(7);
			wsaaMultiply.set(10000000);
			goTo(GotoLabel.next520);
		}
		else {
			wsaaNxdp.set(8);
			wsaaMultiply.set(100000000);
		}
	}

protected void next520()
	{
		wsaaSub4.set(ZERO);
		if (isEQ(wsaaSub, 105)) {
			t6641rec.nxdp[16].set(wsaaNxdp);
			goTo(GotoLabel.continue550);
		}
		if (isEQ(wsaaSub,98)) {
			t6641rec.nxdp[15].set(wsaaNxdp);
			goTo(GotoLabel.continue550);
		}
		if (isEQ(wsaaSub,91)) {
			t6641rec.nxdp[14].set(wsaaNxdp);
			goTo(GotoLabel.continue550);
		}
		if (isEQ(wsaaSub,84)) {
			t6641rec.nxdp[13].set(wsaaNxdp);
			goTo(GotoLabel.continue550);
		}
		if (isEQ(wsaaSub,77)) {
			t6641rec.nxdp[12].set(wsaaNxdp);
			goTo(GotoLabel.continue550);
		}
		if (isEQ(wsaaSub,70)) {
			t6641rec.nxdp[11].set(wsaaNxdp);
			goTo(GotoLabel.continue550);
		}
		if (isEQ(wsaaSub,63)) {
			t6641rec.nxdp[10].set(wsaaNxdp);
			goTo(GotoLabel.continue550);
		}
		if (isEQ(wsaaSub,56)) {
			t6641rec.nxdp[9].set(wsaaNxdp);
			goTo(GotoLabel.continue550);
		}
		if (isEQ(wsaaSub,49)) {
			t6641rec.nxdp[8].set(wsaaNxdp);
			goTo(GotoLabel.continue550);
		}
		if (isEQ(wsaaSub,42)) {
			t6641rec.nxdp[7].set(wsaaNxdp);
			goTo(GotoLabel.continue550);
		}
		if (isEQ(wsaaSub,35)) {
			t6641rec.nxdp[6].set(wsaaNxdp);
			goTo(GotoLabel.continue550);
		}
		if (isEQ(wsaaSub,28)) {
			t6641rec.nxdp[5].set(wsaaNxdp);
			goTo(GotoLabel.continue550);
		}
		if (isEQ(wsaaSub,21)) {
			t6641rec.nxdp[4].set(wsaaNxdp);
			goTo(GotoLabel.continue550);
		}
		if (isEQ(wsaaSub,14)) {
			t6641rec.nxdp[3].set(wsaaNxdp);
			goTo(GotoLabel.continue550);
		}
		if (isEQ(wsaaSub,7)) {
			t6641rec.nxdp[2].set(wsaaNxdp);
			goTo(GotoLabel.continue550);
		}
		if (isLT(wsaaSub,7)) {
			t6641rec.nxdp[1].set(wsaaNxdp);
		}
	}

protected void continue550()
	{
		/* Multiply the stored field by the right amount to*/
		/* get the correct number of decimal places needed.*/
		/*    IF WSAA-SUB                  = 98                            */
		/*       ADD 5                     TO WSAA-SUB4.                   */
		if (isEQ(wsaaSub, 105)) {
			wsaaSub4.add(1);
		}
		wsaaSub4.add(1);
		/*    MOVE ZERO                    TO T6641-NXMON(WSAA-SUB).       */
		if (isLT(wsaaSub, 100)) {
		t6641rec.nxmon[wsaaSub.toInt()].set(ZERO);
		}
		else {
			t6641rec.tnxmon[sub(wsaaSub, 99).toInt()].set(ZERO);
		}
		/*    MULTIPLY WSAA-STORE(WSAA-SUB) BY WSAA-MULTIPLY               */
		/*     GIVING       T6641-NXMON(WSAA-SUB).                         */
		if (isLT(wsaaSub, 100)) {
		compute(t6641rec.nxmon[wsaaSub.toInt()], 8).set(mult(wsaaStore[wsaaSub.toInt()],wsaaMultiply));
		}
		else {
			compute(t6641rec.tnxmon[sub(wsaaSub, 99).toInt()], 8).set(mult(wsaaStore[wsaaSub.toInt()], wsaaMultiply));
		}
		if (isGT(wsaaSub,6)) {
			wsaaSub.add(1);
			if (isLT(wsaaSub4,7)) {
				goTo(GotoLabel.continue550);
			}
		}
		else {
			wsaaSub.add(1);
			if (isLT(wsaaSub4,6)) {
				goTo(GotoLabel.continue550);
			}
		}
		/*    IF WSAA-SUB                  = 100                           */
		/*       SUBTRACT 2                FROM WSAA-SUB                   */
		/*       GO                        TO 590-EXIT.                    */
		if (isEQ(wsaaSub, 110)) {
			wsaaSub.subtract(6);
			return ;
		}
		if (isGT(wsaaSub,6)) {
			wsaaSub.subtract(7);
		}
		else {
			wsaaSub.subtract(6);
		}
	}

protected void moveNxmoney600()
	{
					start610();
					next620();
				}

	/**
	* <pre>
	*  Check to see how many decimal places are needed.
	* </pre>
	*/
protected void start610()
	{
		wsaaCheck.set(wsaaStored);
		if (isGT(wsaaCk1,0)) {
			wsaaNxdp.set(ZERO);
			wsaaMultiply.set(1);
			return ;
		}
		if (isGT(wsaaCk2,0)) {
			wsaaNxdp.set(1);
			wsaaMultiply.set(10);
			return ;
		}
		if (isGT(wsaaCk3,0)) {
			wsaaNxdp.set(2);
			wsaaMultiply.set(100);
			return ;
		}
		if (isGT(wsaaCk4,0)) {
			wsaaNxdp.set(3);
			wsaaMultiply.set(1000);
			return ;
		}
		if (isGT(wsaaCk5,0)) {
			wsaaNxdp.set(4);
			wsaaMultiply.set(10000);
			return ;
		}
		if (isGT(wsaaCk6,0)) {
			wsaaNxdp.set(5);
			wsaaMultiply.set(100000);
			return ;
		}
		if (isGT(wsaaCk7,0)) {
			wsaaNxdp.set(6);
			wsaaMultiply.set(1000000);
			return ;
		}
		if (isGT(wsaaCk8,0)) {
			wsaaNxdp.set(7);
			wsaaMultiply.set(10000000);
			return ;
		}
		else {
			wsaaNxdp.set(8);
			wsaaMultiply.set(100000000);
		}
	}

protected void next620()
	{
		t6641rec.nxdp[1].set(wsaaNxdp);
		t6641rec.nxmoney.set(ZERO);
		/* Multiply the stored field by the right amount to*/
		/* get the correct number of decimal places needed.*/
		compute(t6641rec.nxmoney, 8).set(mult(wsaaStored,wsaaMultiply));
		/*EXIT*/
	}

protected void moveLxmrt700()
	{
		start710();
	}

	/**
	* <pre>
	*  Check to see how many decimal places are used.
	* </pre>
	*/
protected void start710()
	{
		if (isEQ(wsaaLxdp,0)) {
			/*       MOVE T6686-LXMRT(WSAA-SUB) TO WSAA-LXMRT.                 */
			if (isLT(wsaaSub, 100)) {
			wsaaLxmrt.set(t6686rec.lxmrt[wsaaSub.toInt()]);
		}
			else {
				compute(wsaaLxmrt, 8).set(t6686rec.tlxmrt[sub(wsaaSub, 99).toInt()]);
			}
		}
		if (isEQ(wsaaLxdp,1)) {
			/*       MOVE T6686-LXMRT(WSAA-SUB) TO WSAA-LXMRT                  */
			if (isLT(wsaaSub, 100)) {
			wsaaLxmrt.set(t6686rec.lxmrt[wsaaSub.toInt()]);
			}
			else {
				compute(wsaaLxmrt, 8).set(t6686rec.tlxmrt[sub(wsaaSub, 99).toInt()]);
			}
			compute(wsaaLxmrt, 8).set(div(wsaaLxmrt,10));
		}
		if (isEQ(wsaaLxdp,2)) {
			/*       MOVE T6686-LXMRT(WSAA-SUB) TO WSAA-LXMRT                  */
			if (isLT(wsaaSub, 100)) {
			wsaaLxmrt.set(t6686rec.lxmrt[wsaaSub.toInt()]);
			}
			else {
				compute(wsaaLxmrt, 8).set(t6686rec.tlxmrt[sub(wsaaSub, 99).toInt()]);
			}
			compute(wsaaLxmrt, 8).set(div(wsaaLxmrt,100));
		}
		if (isEQ(wsaaLxdp,3)) {
			/*       MOVE T6686-LXMRT(WSAA-SUB) TO WSAA-LXMRT                  */
			if (isLT(wsaaSub, 100)) {
			wsaaLxmrt.set(t6686rec.lxmrt[wsaaSub.toInt()]);
			}
			else {
				compute(wsaaLxmrt, 8).set(t6686rec.tlxmrt[sub(wsaaSub, 99).toInt()]);
			}
			compute(wsaaLxmrt, 8).set(div(wsaaLxmrt,1000));
		}
		if (isEQ(wsaaLxdp,4)) {
			/*       MOVE T6686-LXMRT(WSAA-SUB) TO WSAA-LXMRT                  */
			if (isLT(wsaaSub, 100)) {
			wsaaLxmrt.set(t6686rec.lxmrt[wsaaSub.toInt()]);
			}
			else {
				compute(wsaaLxmrt, 8).set(t6686rec.tlxmrt[sub(wsaaSub, 99).toInt()]);
			}
			compute(wsaaLxmrt, 8).set(div(wsaaLxmrt,10000));
		}
		if (isEQ(wsaaLxdp,5)) {
			/*       MOVE T6686-LXMRT(WSAA-SUB) TO WSAA-LXMRT                  */
			if (isLT(wsaaSub, 100)) {
			wsaaLxmrt.set(t6686rec.lxmrt[wsaaSub.toInt()]);
			}
			else {
				compute(wsaaLxmrt, 8).set(t6686rec.tlxmrt[sub(wsaaSub, 99).toInt()]);
			}
			compute(wsaaLxmrt, 8).set(div(wsaaLxmrt,100000));
		}
		if (isEQ(wsaaLxdp,6)) {
			/*       MOVE T6686-LXMRT(WSAA-SUB) TO WSAA-LXMRT                  */
			if (isLT(wsaaSub, 100)) {
			wsaaLxmrt.set(t6686rec.lxmrt[wsaaSub.toInt()]);
			}
			else {
				compute(wsaaLxmrt, 8).set(t6686rec.tlxmrt[sub(wsaaSub, 99).toInt()]);
			}
			compute(wsaaLxmrt, 8).set(div(wsaaLxmrt,1000000));
		}
		if (isEQ(wsaaLxdp,7)) {
			/*       MOVE T6686-LXMRT(WSAA-SUB) TO WSAA-LXMRT                  */
			if (isLT(wsaaSub, 100)) {
			wsaaLxmrt.set(t6686rec.lxmrt[wsaaSub.toInt()]);
			}
			else {
				compute(wsaaLxmrt, 8).set(t6686rec.tlxmrt[sub(wsaaSub, 99).toInt()]);
			}
			compute(wsaaLxmrt, 8).set(div(wsaaLxmrt,10000000));
		}
		if (isEQ(wsaaLxdp,8)) {
			/*       MOVE T6686-LXMRT(WSAA-SUB) TO WSAA-LXMRT                  */
			if (isLT(wsaaSub, 100)) {
			wsaaLxmrt.set(t6686rec.lxmrt[wsaaSub.toInt()]);
			}
			else {
				compute(wsaaLxmrt, 8).set(t6686rec.tlxmrt[sub(wsaaSub, 99).toInt()]);
			}
			compute(wsaaLxmrt, 8).set(div(wsaaLxmrt,100000000));
		}
	}

protected void moveLxmort800()
	{
		start810();
	}

	/**
	* <pre>
	*  Check to see how many decimal places are used.
	* </pre>
	*/
protected void start810()
	{
		if (isEQ(wsaaLxdp,0)) {
			wsaaLxmrt.set(t6686rec.lxmort);
		}
		if (isEQ(wsaaLxdp,1)) {
			wsaaLxmrt.set(t6686rec.lxmort);
			compute(wsaaLxmrt, 8).set(div(wsaaLxmrt,10));
		}
		if (isEQ(wsaaLxdp,2)) {
			wsaaLxmrt.set(t6686rec.lxmort);
			compute(wsaaLxmrt, 8).set(div(wsaaLxmrt,100));
		}
		if (isEQ(wsaaLxdp,3)) {
			wsaaLxmrt.set(t6686rec.lxmort);
			compute(wsaaLxmrt, 8).set(div(wsaaLxmrt,1000));
		}
		if (isEQ(wsaaLxdp,4)) {
			wsaaLxmrt.set(t6686rec.lxmort);
			compute(wsaaLxmrt, 8).set(div(wsaaLxmrt,10000));
		}
		if (isEQ(wsaaLxdp,5)) {
			wsaaLxmrt.set(t6686rec.lxmort);
			compute(wsaaLxmrt, 8).set(div(wsaaLxmrt,100000));
		}
		if (isEQ(wsaaLxdp,6)) {
			wsaaLxmrt.set(t6686rec.lxmort);
			compute(wsaaLxmrt, 8).set(div(wsaaLxmrt,1000000));
		}
		if (isEQ(wsaaLxdp,7)) {
			wsaaLxmrt.set(t6686rec.lxmort);
			compute(wsaaLxmrt, 8).set(div(wsaaLxmrt,10000000));
		}
		if (isEQ(wsaaLxdp,8)) {
			wsaaLxmrt.set(t6686rec.lxmort);
			compute(wsaaLxmrt, 8).set(div(wsaaLxmrt,100000000));
		}
	}
/*
 * Class transformed  from Data Structure WSAA-DECIMAL-CHECK--INNER
 */
private static final class WsaaDecimalCheckInner {

		/*  88 WSAA-ROW1                  VALUE 01 02 03 04
		                                05 06.
		  88 WSAA-ROW2                  VALUE 07 08 09 10
		                                11 12 13.
		  88 WSAA-ROW3                  VALUE 14 15 16 17
		                                18 19 20.
		  88 WSAA-ROW4                  VALUE 21 22 23 24
		                                25 26 27.
		  88 WSAA-ROW5                  VALUE 28 29 30 31
		                                32 33 34.
		  88 WSAA-ROW6                  VALUE 35 36 37 38
		                                39 40 41.
		  88 WSAA-ROW7                  VALUE 42 43 44 45
		                                46 47 48.
		  88 WSAA-ROW8                  VALUE 49 50 51 52
		                                53 54 55.
		  88 WSAA-ROW9                  VALUE 56 57 58 59
		                                60 61 62.
		  88 WSAA-ROW10                 VALUE 63 64 65 66
		                                67 68 69.
		  88 WSAA-ROW11                 VALUE 70 71 72 73
		                                74 75 76.
		  88 WSAA-ROW12                 VALUE 77 78 79 80
		                                81 82 83.
		  88 WSAA-ROW13                 VALUE 84 85 86 87
		                                88 89 90.
		  88 WSAA-ROW14                 VALUE 91 92 93 94
		                                95 96 97.
		  88 WSAA-ROW15                 VALUE 98 99.                     */
	private ZonedDecimalData wsaaDecimalCheck = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private Validator wsaaRow1 = new Validator(wsaaDecimalCheck, 0,1,2,3,4,5,6);
	private Validator wsaaRow2 = new Validator(wsaaDecimalCheck, 7,8,9,10,11,12,13);
	private Validator wsaaRow3 = new Validator(wsaaDecimalCheck, 14,15,16,17,18,19,20);
	private Validator wsaaRow4 = new Validator(wsaaDecimalCheck, 21,22,23,24,25,26,27);
	private Validator wsaaRow5 = new Validator(wsaaDecimalCheck, 28,29,30,31,32,33,34);
	private Validator wsaaRow6 = new Validator(wsaaDecimalCheck, 35,36,37,38,39,40,41);
	private Validator wsaaRow7 = new Validator(wsaaDecimalCheck, 42,43,44,45,46,47,48);
	private Validator wsaaRow8 = new Validator(wsaaDecimalCheck, 49,50,51,52,53,54,55);
	private Validator wsaaRow9 = new Validator(wsaaDecimalCheck, 56,57,58,59,60,61,62);
	private Validator wsaaRow10 = new Validator(wsaaDecimalCheck, 63,64,65,66,67,68,69);
	private Validator wsaaRow11 = new Validator(wsaaDecimalCheck, 70,71,72,73,74,75,76);
	private Validator wsaaRow12 = new Validator(wsaaDecimalCheck, 77,78,79,80,81,82,83);
	private Validator wsaaRow13 = new Validator(wsaaDecimalCheck, 84,85,86,87,88,89,90);
	private Validator wsaaRow14 = new Validator(wsaaDecimalCheck, 91,92,93,94,95,96,97);
	private Validator wsaaRow15 = new Validator(wsaaDecimalCheck, 98,99,100,101,102,103,104);
	private Validator wsaaRow16 = new Validator(wsaaDecimalCheck, 105,106,107,108,109,110);
}
}
