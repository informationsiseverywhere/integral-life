package com.csc.life.underwriting.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sd5g5screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {23, 23, 4, 4}); 
	}

	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5g5ScreenVars sv = (Sd5g5ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sd5g5screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sd5g5ScreenVars screenVars = (Sd5g5ScreenVars)pv;
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.totRiskamt.setClassString("");
	}

/**
 * Clear all the variables in Sd5g5screen
 */
	public static void clear(VarModel pv) {
		Sd5g5ScreenVars screenVars = (Sd5g5ScreenVars) pv;
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.totRiskamt.clear();
	}
}
