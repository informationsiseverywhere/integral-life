package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.contractservicing.dataaccess.CovrincTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.FluplnbTableDAM;
import com.csc.life.newbusiness.dataaccess.ZfupcdeTableDAM;
import com.csc.life.newbusiness.dataaccess.ZlifelcTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.newbusiness.tablestructures.Th552rec;
import com.csc.life.newbusiness.tablestructures.Th555rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5677rec;
import com.csc.life.underwriting.recordstructures.Jundwfuprec;
import com.csc.life.underwriting.tablestructures.Tjl08rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.dataaccess.dao.DescpfDAO;
import com.csc.smart.recordstructures.Conerrrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Jundwfup extends SMARTCodeModel{
	
		public static final String ROUTINE = QPUtilities.getThisClass();
		private static final String wsaaSubr = "JUNDWFUP";

		private FixedLengthStringData wsaaExitSubr = new FixedLengthStringData(1).init("Y");
		private Validator exitSubroutine = new Validator(wsaaExitSubr, "Y");

		protected FixedLengthStringData wsaaValidFupmeth = new FixedLengthStringData(1).init("N");
		protected Validator validFupmeth = new Validator(wsaaValidFupmeth, "Y");
		protected String wsaaMethFound = "N";
			/* WSAA-SUM-INSUREDS */
		private PackedDecimalData wsaaSi = new PackedDecimalData(17, 2);
		private PackedDecimalData wsaaTotalSi = new PackedDecimalData(17, 2);
		private static final int wsaaMaxSi = 99999999;
		protected ZonedDecimalData wsaaFupno = new ZonedDecimalData(2, 0).init(0).setUnsigned();
		protected String wsaaValidFlow = "N";
		protected ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(0).setUnsigned();
		protected ZonedDecimalData wsaaRow = new ZonedDecimalData(2, 0).init(0).setUnsigned();
		protected ZonedDecimalData wsaaCol = new ZonedDecimalData(2, 0).init(0).setUnsigned();
		protected ZonedDecimalData wsaaMethSub = new ZonedDecimalData(2, 0).init(0).setUnsigned();
		private FixedLengthStringData wsaaFupmeth = new FixedLengthStringData(4);
		protected FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
		protected ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).setUnsigned();

		private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
		private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
		private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
			/* ERRORS */
		private static final String e557 = "E557";
			/* TABLES */
		private static final String t5661 = "T5661";
		private static final String t5677 = "T5677";
		private static final String th552 = "TH552";
		protected CovrincTableDAM covrincIO = new CovrincTableDAM();
		protected CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
		protected DescTableDAM descIO = new DescTableDAM();
		private FluplnbTableDAM fluplnbIO = new FluplnbTableDAM();
		private ItemTableDAM itemIO = new ItemTableDAM();
		private ZfupcdeTableDAM zfupcdeIO = new ZfupcdeTableDAM();
		protected ZlifelcTableDAM zlifelcIO = new ZlifelcTableDAM();
		private Varcom varcom = new Varcom();
		protected Datcon1rec datcon1rec = new Datcon1rec();
		protected Datcon2rec datcon2rec = new Datcon2rec();
		private Zrdecplrec zrdecplrec = new Zrdecplrec();
		protected Syserrrec syserrrec = new Syserrrec();
		private T5661rec t5661rec = new T5661rec();
		private T5677rec t5677rec = new T5677rec();
		private Th552rec th552rec = new Th552rec();
		private Th555rec th555rec = new Th555rec();
		protected Jundwfuprec jundwfuprec =  new Jundwfuprec(); 
		private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);	
		private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);	
		private CovtpfDAO covtpfDAO = getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);	
		private Covrpf covr =  new Covrpf();
		private Itempf itempf;
		private Chdrpf chdrpf =  new Chdrpf();
		private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);	
		private List<String> riskClassList = new ArrayList<>();
		private Set<String> mainRiskList =  new HashSet<>();
		private HashMap<String,List<String>> crtableRiskClass  = new LinkedHashMap<>();
		private HashMap<String,Integer> crtableFactor  = new LinkedHashMap<>();
		private static final String tjl08 = "TJL08";
		private static final String tr386 = "TR386";
		private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
		private Tjl08rec tjl08rec = new Tjl08rec();
		private Tr386rec tr386rec = new Tr386rec();
		private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
		private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).setUnsigned();
		private ZonedDecimalData wsaaSub3 = new ZonedDecimalData(2, 0).setUnsigned();
		protected String wsaaRiskValid = "N";
		private boolean isContractCurrent = false;
		private List<String> statusCodes = new ArrayList<>();
		private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);	
		private List<Fluppf> fluppfList = new ArrayList<>();
		private List<Fluppf> deleteFluppfList = new ArrayList<>();		
		private Fluppf fluppfnew = new Fluppf();
		private Conlinkrec conlinkrec = new Conlinkrec();
		protected Conerrrec conerrrec = new Conerrrec();
		Descpf descpf = new Descpf();
		private DescpfDAO descpfDAO = getApplicationContext().getBean("iafDescDAO", DescpfDAO.class);
		List<String> covrlist = new ArrayList<>();
		List<BigDecimal> sumInsuredlist = new ArrayList<>();
		List<Integer> riskFactorlist = new ArrayList<>();
		
		List<BigDecimal> agrSumInsList = new ArrayList<>();
		List<Integer> agrRiskFactorList = new ArrayList<>();
		
		public Jundwfup() {
			super();
		}

		/**
		* The mainline method is the default entry point to the class
		*/
	public void mainline(Object... parmArray)
		{
			jundwfuprec.crtfupRec = convertAndSetParam(jundwfuprec.crtfupRec, parmArray, 0);
			try {
				mainline000();
			}
			catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
			}
		}

	protected void mainline000(){
		
		para010();
		exit090();
	}

	protected void para010(){
		
			syserrrec.subrname.set(wsaaSubr);
			jundwfuprec.statuz.set(varcom.oK);
			wsaaSi.set(ZERO);
			wsaaTotalSi.set(ZERO);
			deleteFollowUp1000();
			getLifepfData();
	}
	
	protected void getLifepfData(){
		
		getTr386Data();
		covrlist.clear();
		sumInsuredlist.clear();
		List<Lifepf> lifepfList = lifepfDAO.searchLifeRecordByLifcNum(jundwfuprec.chdrcoy.toString(), jundwfuprec.lifcnum.toString());
		fluppfList = fluppfDAO.getFluplnbRecordsForLife(jundwfuprec.chdrcoy.toString(), jundwfuprec.chdrnum.toString(), jundwfuprec.life.toString());
		if(!lifepfList.isEmpty()){
			for(Lifepf life :lifepfList){
				isContractCurrent = false;
				wsaaValidFlow = "N";
				
				chdrpf = chdrpfDAO.getchdrRecordData(jundwfuprec.chdrcoy.toString(), life.getChdrnum());
				if(isEQ(life.getChdrnum(),jundwfuprec.chdrnum)||statusCodes.contains(chdrpf.getStatcode())){
					if(isEQ(life.getChdrnum(),jundwfuprec.chdrnum)){
						isContractCurrent = true;
					}
					getCovtOrCovr(life.getValidflag(), life.getChdrnum());
				}
					
			}
			for(String iteratorRisk : mainRiskList){
				 BigDecimal wsaaOrigsum ;
				agrSumInsList.clear();
				agrRiskFactorList.clear();
				for(wsaaSub1.set(0);isLT(wsaaSub1, covrlist.size()); wsaaSub1.add(1)){
					if(crtableRiskClass.get(covrlist.get(wsaaSub1.toInt())).contains(iteratorRisk)){
						agrSumInsList.add(sumInsuredlist.get(wsaaSub1.toInt()));
						agrRiskFactorList.add(crtableFactor.get(covrlist.get(wsaaSub1.toInt())));
					}
				}
				
				if(agrSumInsList.isEmpty()){
					return;
				}
				wsaaOrigsum = getAggregatedSumInsured(agrSumInsList,agrRiskFactorList);				
				wsaaFupmeth.set(SPACES);
				followupMethod5000(wsaaOrigsum, iteratorRisk);
				if (isNE(wsaaFupmeth,SPACES)) {
					wsaaFupno.set(0);
					createFollowups6000();
				}	
				
			}
		}
	}

	
	private BigDecimal getAggregatedSumInsured( List<BigDecimal> agrSumInsList2,
			List<Integer> agrRiskFactorList2) {
		
		BigDecimal aggregtdSumIns = new BigDecimal(0);
		
	
		for(wsaaSub1.set(0);isLT(wsaaSub1, agrSumInsList2.size()); wsaaSub1.add(1)){
			aggregtdSumIns=aggregtdSumIns.add(mult(agrSumInsList2.get(wsaaSub1.toInt()),agrRiskFactorList2.get(wsaaSub1.toInt())).getbigdata());
		
	}
		return aggregtdSumIns;
	}

	protected void getCovtOrCovr( String validFlag, String chdrnum){
		
		if (isEQ(validFlag,"3")) {
			List<Covtpf> covtpfList = covtpfDAO.searchCovtRecordByCoyNumDescUniquNo(jundwfuprec.chdrcoy.toString(), chdrnum);
			if(!covtpfList.isEmpty()){
				for (Covtpf covt : covtpfList){
					wsaaRiskValid = "N";
					getTjl08Data(covt.getCrtable());
					if(isEQ(wsaaRiskValid,"Y")){
						BigDecimal convSumins = loopCustomerSpecific(covt.getSumins(),covt.getCntcurr());
						covrlist.add(covt.getCrtable());
						sumInsuredlist.add(convSumins);
					}
				}
			}
		}
		else{
			covr.setChdrcoy(jundwfuprec.chdrcoy.toString());
			covr.setChdrnum(chdrpf.getChdrnum());
			covr.setLife(jundwfuprec.life.toString());
			List<Covrpf> covrpfList = covrpfDAO.selectCovrRecordBegin(covr);
			if(!covrpfList.isEmpty()){
				for (Covrpf covrobj : covrpfList){
					wsaaRiskValid = "N";
					getTjl08Data(covrobj.getCrtable());
					if(isEQ(wsaaRiskValid,"Y")){
						BigDecimal convSumins = loopCustomerSpecific(covrobj.getSumins(),covrobj.getPremCurrency());
						covrlist.add(covrobj.getCrtable());
						sumInsuredlist.add(convSumins);
					}
				}
			}
		}
	}
	
	
	protected void getTr386Data(){
		
		itempf = new Itempf();

		itempf.setItempfx("IT");
		itempf.setItemcoy(jundwfuprec.chdrcoy.toString().trim());
		itempf.setItemtabl(tr386);
		itempf.setItemitem(wsaaSubr);
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			syserrrec.params.set("IT".concat(jundwfuprec.chdrcoy.toString()).concat(tr386));
			dbError8100();
		}
		else{
			tr386rec.tr386Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		wsaaSub2.set(0);
		wsaaSub3.set(2);
		for(wsaaSub1.set(01);!(isGT(wsaaSub1, 10)); wsaaSub1.add(1)){
			if(isNE(tr386rec.progdesc[wsaaSub1.toInt()].toString(),SPACES)){
				String statuslist = tr386rec.progdesc[wsaaSub1.toInt()].toString();
				while(isLT(wsaaSub2,statuslist.length())&&
						isNE(statuslist.substring(wsaaSub2.toInt(), wsaaSub3.toInt()),SPACES)){
					statusCodes.add(statuslist.substring(wsaaSub2.toInt(), wsaaSub3.toInt()));
					wsaaSub2.add(2);
					wsaaSub3.add(2);
				}
			}
		}
	}
	
protected void getTjl08Data(String crtable){
	
	itempf = new Itempf();

	itempf.setItempfx("IT");
	itempf.setItemcoy(jundwfuprec.chdrcoy.toString().trim());
	itempf.setItemtabl(tjl08);
	itempf.setItemitem(crtable);
	itempf = itemDAO.getItemRecordByItemkey(itempf);
	if (itempf == null) {
		return;
	}
	else{
		tjl08rec.tjl08Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	riskClassList.clear();
	addRiskClasses();
	
	if(!riskClassList.isEmpty()){
		List<String> risklist = new ArrayList<>(riskClassList);
		crtableRiskClass.put(crtable, risklist);
		crtableFactor.put(crtable, tjl08rec.sumAssuredFactor.toInt());
	}
}

protected void addRiskClasses(){
	
	for(wsaaSub1.set(01);!(isGT(wsaaSub1, 05)); wsaaSub1.add(1)){
		if(isNE(tjl08rec.riskClass[wsaaSub1.toInt()].toString(),SPACES)){
			getTh552Data(tjl08rec.riskClass[wsaaSub1.toInt()].toString());	
		}
	}	
}

protected void getTh552Data(String riskClass){
	
	itempf = new Itempf();

	itempf.setItempfx("IT");
	itempf.setItemcoy(jundwfuprec.chdrcoy.toString().trim());
	itempf.setItemtabl(th552);
	itempf.setItemitem(riskClass);
	itempf = itemDAO.getItemRecordByItemkey(itempf);
	if (itempf == null) {
		//take no action
	}
	else{
		th552rec.th552Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		if(isEQ(wsaaValidFlow,"N")){
			wsaaRiskValid = "Y";
			riskClassList.add(tjl08rec.riskClass[wsaaSub1.toInt()].toString());
			if(isContractCurrent)
				mainRiskList.add(tjl08rec.riskClass[wsaaSub1.toInt()].toString());
		}
	}	
}


	protected void exit090()
		{
			exitProgram();
		}

	protected void deleteFollowUp1000()
		{
			loopFollowUps1010();
		}

	protected void loopFollowUps1010()
		{
			
		fluppfList = fluppfDAO.getFluplnbRecordsForLife(jundwfuprec.chdrcoy.toString(), jundwfuprec.chdrnum.toString(), jundwfuprec.life.toString());
			if(fluppfList.isEmpty()){
				return;
			}
		for( Fluppf fluppf: fluppfList){
			
			if (isEQ(fluppf.getzAutoInd(),"Y")) {
				fupStatus6200(fluppf.getFupCde());
				
				if (isEQ(fluppf.getFupSts(),t5661rec.fupstat)&& isEQ(fluppf.getFupTyp(),"U")) {
					deleteFluppfList.add(fluppf);
				}			
			}
		}
		fluppfDAO.deleteFluppfRecord(deleteFluppfList);
		}


	protected BigDecimal loopCustomerSpecific(BigDecimal wsaaAmount, String curr){
		BigDecimal convSumins;
		if (isNE(curr,th552rec.currcode)) {
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.company.set(jundwfuprec.chdrcoy);
			conlinkrec.cashdate.set(jundwfuprec.effDate);
			conlinkrec.currIn.set(curr);
			conlinkrec.currOut.set(th552rec.currcode);
			conlinkrec.amountIn.set(wsaaAmount);
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.function.set("CVRT");
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isEQ(conlinkrec.statuz,"BOMB")) {
				conerrrec.statuz.set(conlinkrec.statuz);
				
			}
			convSumins = conlinkrec.amountOut.getbigdata();
			
		}else {
			convSumins= wsaaAmount;
		}
		
		return convSumins;
	}





	protected void followupMethod5000(BigDecimal value, String riskClass)
		{
			wsaaValidFupmeth.set("N");	
			wsaaValidFlow = "Y";
			getTh552Data(riskClass);
			loopTh5525010(value);
		}

	protected void loopTh5525010(BigDecimal value)
		{
			for (wsaaSub.set(1); !(isGT(wsaaSub,8)|| isEQ(wsaaValidFupmeth,"Y")); wsaaSub.add(1)){
				if (isEQ(th552rec.zsuminto[wsaaSub.toInt()],wsaaMaxSi)) {
					wsaaRow.set(wsaaSub);
					wsaaValidFupmeth.set("Y");
				}
				/*    IF WSAA-TOTAL-SI          NOT < TH522-ZSUMINFR (WSAA-SUB)  */
				/*    AND WSAA-TOTAL-SI         NOT > TH522-ZSUMINTO (WSAA-SUB)  */
				if (isGTE(value,th552rec.zsuminfr[wsaaSub.toInt()])
				&& isLTE(value,th552rec.zsuminto[wsaaSub.toInt()])) {
					wsaaRow.set(wsaaSub);
					wsaaValidFupmeth.set("Y");
				}
				/*    IF TH522-ZSUMINTO (WSAA-SUB) = 0                           */
				if (isEQ(th552rec.zsuminto[wsaaSub.toInt()],0)) {
					compute(wsaaRow, 0).set(sub(wsaaSub,1));
					wsaaValidFupmeth.set("Y");
				}
				if (validFupmeth.isTrue()) {
					wsaaMethFound = "N";
					for (wsaaCol.set(1); !(isGT(wsaaCol, 8)
					|| isEQ(wsaaMethFound,"Y")); wsaaCol.add(1)){
						if (isEQ(wsaaCol, 8)
						|| (isGTE(jundwfuprec.anbccd, th552rec.ageIssageFrm[wsaaCol.toInt()])
						&& isLTE(jundwfuprec.anbccd, th552rec.ageIssageTo[wsaaCol.toInt()]))) {
							/*               COMPUTE WSAA-METH-SUB = (WSAA-ROW - 1) * 9        */
							compute(wsaaMethSub, 0).set(add(mult((sub(wsaaRow, 1)), 8), wsaaCol));
							/*             MOVE TH522-DEF-FUP-METH (WSAA-METH-SUB)           */
							if(th552rec.defFupMeth[wsaaMethSub.toInt()] != null) {
								wsaaFupmeth.set(th552rec.defFupMeth[wsaaMethSub.toInt()]);
								wsaaMethFound = "Y";
							}
						}
					}
				}
			}
		}

	protected void createFollowups6000()
		{
			para6000();
		}

	protected void para6000()
		{
		itempf = new Itempf();

		itempf.setItempfx("IT");
		itempf.setItemcoy(jundwfuprec.chdrcoy.toString().trim());
		itempf.setItemtabl(t5677);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(jundwfuprec.batctrcde);
		stringVariable1.addExpression(wsaaFupmeth);
		itempf.setItemitem(stringVariable1.toString());
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			syserrrec.params.set("IT".concat(jundwfuprec.chdrcoy.toString()).concat(t5677));
			dbError8100();
		}
		else{
			t5677rec.t5677Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			for (wsaaSub.set(1); !(isGT(wsaaSub,16)); wsaaSub.add(1)){
				if (isNE(t5677rec.fupcdes[wsaaSub.toInt()],SPACES)) {
					fupUpdCrt6100();
				}
			}
		}
		
		
	}

	protected void fupUpdCrt6100()
		{
				para6100();
			}

	protected void para6100()
		{	
		fluppfList = fluppfDAO.getFluplnbRecordsForLife(jundwfuprec.chdrcoy.toString(), jundwfuprec.chdrnum.toString(), jundwfuprec.life.toString());
		if(!fluppfList.isEmpty()){
			for (Fluppf fluppf : fluppfList){
			
				if (isGT(fluppf.getFupNo(),wsaaFupno)) {
					wsaaFupno.set(fluppf.getFupNo());
							}
				if(isEQ(fluppf.getFupCde(),t5677rec.fupcdes[wsaaSub.toInt()].toString().trim())){
					return;
				}
			}
		}
		
		fupStatus6200(t5677rec.fupcdes[wsaaSub.toInt()].toString());
		
		fluppfnew.setFupSts(t5661rec.fupstat.toString().charAt(0));
		fluppfnew.setChdrcoy(jundwfuprec.chdrcoy.toString().charAt(0));
		fluppfnew.setChdrnum(jundwfuprec.chdrnum.toString());
		fluppfnew.setLife(jundwfuprec.life.toString());
		fluppfnew.setjLife(jundwfuprec.jlife.toString());
		wsaaFupno.add(1);
		fluppfnew.setFupNo(wsaaFupno.toInt());
		fluppfnew.setTranNo(jundwfuprec.tranno.toInt());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* When present, advance todays' date by T5661-Reminder-Days.*/
		if (isNE(t5661rec.zelpdays,ZERO)) {
			datcon2rec.intDate1.set(datcon1rec.intDate);
			datcon2rec.frequency.set("DY");
			datcon2rec.freqFactor.set(t5661rec.zelpdays);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			fluppfnew.setFupDt(datcon2rec.intDate2.toInt());
		}
		else {
			fluppfnew.setFupDt(datcon1rec.intDate.toInt());
		}
		/*    MOVE T5677-FUPCODE (WSAA-SUB) TO FLUPLNB-FUPCODE.            */
		fluppfnew.setFupCde(t5677rec.fupcdes[wsaaSub.toInt()].toString());
		fluppfnew.setFupTyp('U');
		if (null!=descpf) {
			fluppfnew.setFupRmk(descpf.getLongdesc());
		}
		else {
			fluppfnew.setFupRmk("????????????????????????????????????????");
		}
		fluppfnew.setTrdt(jundwfuprec.transactionDate.toInt());
		fluppfnew.setTrtm(jundwfuprec.transactionTime.toInt());
		fluppfnew.setUserT(jundwfuprec.user.toInt());
		fluppfnew.setzAutoInd('Y');
		fluppfnew.setEffDate(datcon1rec.intDate.toInt());
		fluppfnew.setCrtDate(datcon1rec.intDate.toInt());
		fluppfnew.setzLstUpDt(datcon1rec.intDate.toInt());
		fluppfnew.setFuprcvd(varcom.vrcmMaxDate.toInt());
		fluppfnew.setExprDate(varcom.vrcmMaxDate.toInt());
		
		fluppfDAO.insertFlupRecord(fluppfnew);
			
		}

	protected void fupStatus6200(String fupcode)
		{
			para6200(fupcode);
		}

	protected void para6200(String fupcode)
		{	

		
		itempf = new Itempf();

		itempf.setItempfx("IT");
		itempf.setItemcoy(jundwfuprec.chdrcoy.toString().trim());
		itempf.setItemtabl(t5661);
		wsaaT5661Lang.set(jundwfuprec.language);
		wsaaT5661Fupcode.set(fupcode);
		itempf.setItemitem(wsaaT5661Key.toString());
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			syserrrec.statuz.set(e557);
			syserrrec.params.set("IT".concat(jundwfuprec.chdrcoy.toString()).concat(t5661));
			dbError8100();
		}
		else{
			t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
				
		

		
		wsaaT5661Lang.set(jundwfuprec.language);
		wsaaT5661Fupcode.set(fupcode);
		String desctemp = descpfDAO.getLongDesc("IT",jundwfuprec.chdrcoy.toString().trim(), t5661, wsaaT5661Key.toString(),jundwfuprec.language.toString());
		if(null!=desctemp){
			descpf.setLongdesc(desctemp);	
		}
		
		
			
	
		}

	protected void dbError8100()
		{
				db8100();
				dbExit8190();
			}

	protected void db8100()
		{
			if (isEQ(syserrrec.statuz,varcom.bomb)) {
				return ;
			}
			syserrrec.syserrStatuz.set(syserrrec.statuz);
			if (isNE(syserrrec.syserrType,"2")) {
				syserrrec.syserrType.set("1");
			}
			callProgram(Syserr.class, syserrrec.syserrRec);
		}

	protected void dbExit8190()
		{
			jundwfuprec.statuz.set(varcom.bomb);
			exit090();
		}

	protected void a000CallRounding()
		{
			/*A100-CALL*/
			zrdecplrec.function.set(SPACES);
			zrdecplrec.company.set(jundwfuprec.chdrcoy);
			zrdecplrec.statuz.set(varcom.oK);
			zrdecplrec.currency.set(th552rec.currcode);
			zrdecplrec.batctrcde.set(jundwfuprec.batctrcde);
			callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
			if (isNE(zrdecplrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(zfupcdeIO.getStatuz());
				syserrrec.params.set(zfupcdeIO.getParams());
				dbError8100();
			}
			/*A900-EXIT*/
		}



	public String getWsaaSubr() {
		return wsaaSubr;
	}

	public PackedDecimalData getWsaaSi() {
		return wsaaSi;
	}

	public void setWsaaSi(PackedDecimalData wsaaSi) {
		this.wsaaSi = wsaaSi;
	}

	public PackedDecimalData getWsaaTotalSi() {
		return wsaaTotalSi;
	}

	public void setWsaaTotalSi(PackedDecimalData wsaaTotalSi) {
		this.wsaaTotalSi = wsaaTotalSi;
	}

	public Validator getExitSubroutine() {
		return exitSubroutine;
	}

	public void setExitSubroutine(Validator exitSubroutine) {
		this.exitSubroutine = exitSubroutine;
	}

	public FixedLengthStringData getWsaaFupmeth() {
		return wsaaFupmeth;
	}

	public void setWsaaFupmeth(FixedLengthStringData wsaaFupmeth) {
		this.wsaaFupmeth = wsaaFupmeth;
	}

	public Th552rec getTh552rec() {
		return th552rec;
	}

	public void setTh552rec(Th552rec th552rec) {
		this.th552rec = th552rec;
	}

	public FixedLengthStringData getWsaaExitSubr() {
		return wsaaExitSubr;
	}

	public void setWsaaExitSubr(FixedLengthStringData wsaaExitSubr) {
		this.wsaaExitSubr = wsaaExitSubr;
	}

	public static int getWsaamaxsi() {
		return wsaaMaxSi;
	}

	public FluplnbTableDAM getFluplnbIO() {
		return fluplnbIO;
	}

	public void setFluplnbIO(FluplnbTableDAM fluplnbIO) {
		this.fluplnbIO = fluplnbIO;
	}

	public ItemTableDAM getItemIO() {
		return itemIO;
	}

	public void setItemIO(ItemTableDAM itemIO) {
		this.itemIO = itemIO;
	}

	public FixedLengthStringData getWsaaT5661Lang() {
		return wsaaT5661Lang;
	}

	public void setWsaaT5661Lang(FixedLengthStringData wsaaT5661Lang) {
		this.wsaaT5661Lang = wsaaT5661Lang;
	}

	public FixedLengthStringData getWsaaT5661Fupcode() {
		return wsaaT5661Fupcode;
	}

	public void setWsaaT5661Fupcode(FixedLengthStringData wsaaT5661Fupcode) {
		this.wsaaT5661Fupcode = wsaaT5661Fupcode;
	}

	public FixedLengthStringData getWsaaT5661Key() {
		return wsaaT5661Key;
	}

	public void setWsaaT5661Key(FixedLengthStringData wsaaT5661Key) {
		this.wsaaT5661Key = wsaaT5661Key;
	}

	public T5661rec getT5661rec() {
		return t5661rec;
	}

	public void setT5661rec(T5661rec t5661rec) {
		this.t5661rec = t5661rec;
	}

	public Th555rec getTh555rec() {
		return th555rec;
	}

	public void setTh555rec(Th555rec th555rec) {
		this.th555rec = th555rec;
	}

	public Zrdecplrec getZrdecplrec() {
		return zrdecplrec;
	}

	public void setZrdecplrec(Zrdecplrec zrdecplrec) {
		this.zrdecplrec = zrdecplrec;
	}

	public T5677rec getT5677rec() {
		return t5677rec;
	}

	public void setT5677rec(T5677rec t5677rec) {
		this.t5677rec = t5677rec;
	}

}
