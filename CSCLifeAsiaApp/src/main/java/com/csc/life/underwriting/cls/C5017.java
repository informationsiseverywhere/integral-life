/*
 * File: C5017.java
 * Date: 30 August 2009 2:58:37
 * Author: $Id$
 * 
 * Class transformed from C5017.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.cls;

import com.csc.life.underwriting.batchprograms.B5017;
import com.csc.smart.procedures.Passparm;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class C5017 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData params = new FixedLengthStringData(342);
	private FixedLengthStringData conj = new FixedLengthStringData(860);

	public C5017() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		params = convertAndSetParam(params, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			try {
				switch (qState) {
				case QS_START: {
					callProgram(Passparm.class, new Object[] {params, conj});
					callProgram(B5017.class, new Object[] {params});
				}
				case returnVar: {
					return ;
				}
				case error: {
					appVars.sendMessageToQueue("Unexpected errors occurred", "*");
					params.setSub1String(1, 4, "ENDP");
					qState = returnVar;
					break;
				}
				default:{
					qState = QS_END;
				}
				}
			}
			catch (ExtMsgException ex){
				if (ex.messageMatches("CPF0000")
				|| ex.messageMatches("CBE0000")) {
					qState = error;
				}
				else {
					throw ex;
				}
			}
		}
		
	}
}
