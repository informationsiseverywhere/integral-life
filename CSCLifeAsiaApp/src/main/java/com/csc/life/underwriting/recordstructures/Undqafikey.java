package com.csc.life.underwriting.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:34
 * Description:
 * Copybook name: UNDQAFIKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Undqafikey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData undqafiFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData undqafiKey = new FixedLengthStringData(256).isAPartOf(undqafiFileKey, 0, REDEFINE);
  	public FixedLengthStringData undqafiChdrcoy = new FixedLengthStringData(1).isAPartOf(undqafiKey, 0);
  	public FixedLengthStringData undqafiChdrnum = new FixedLengthStringData(8).isAPartOf(undqafiKey, 1);
  	public FixedLengthStringData undqafiLife = new FixedLengthStringData(2).isAPartOf(undqafiKey, 9);
  	public FixedLengthStringData undqafiJlife = new FixedLengthStringData(2).isAPartOf(undqafiKey, 11);
  	public FixedLengthStringData undqafiQuestidf = new FixedLengthStringData(5).isAPartOf(undqafiKey, 13);
  	public PackedDecimalData undqafiTranno = new PackedDecimalData(5, 0).isAPartOf(undqafiKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(235).isAPartOf(undqafiKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(undqafiFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		undqafiFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}