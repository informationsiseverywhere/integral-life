/*
 * File: P6767.java
 * Date: 30 August 2009 0:56:59
 * Author: Quipoz Limited
 * 
 * Class transformed from P6767.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.underwriting.dataaccess.UndqTableDAM;
import com.csc.life.underwriting.screens.S6767ScreenVars;
import com.csc.life.underwriting.tablestructures.T6770rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* This is the underwriting lifestyle questionnaire enquiry
* screen. It will display all the questions and answers which
* were input at new business relating to the life or joint life.
*
***********************************************************************
* </pre>
*/
public class P6767 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6767");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaLinesOnPage = new ZonedDecimalData(3, 0);
	private PackedDecimalData wsaaSubfilePage = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaScrnLinesLeft = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaLineTest = new ZonedDecimalData(2, 0);
	private ZonedDecimalData wsaaRemainder = new ZonedDecimalData(2, 0).setUnsigned();

		/* WSAA-SUBFILE-AREA */
	private FixedLengthStringData wsaaSubfileFields = new FixedLengthStringData(81);
	private FixedLengthStringData wsaaAnswer = new FixedLengthStringData(4).isAPartOf(wsaaSubfileFields, 0);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaSubfileFields, 4);
	private FixedLengthStringData wsaaQuestidf = new FixedLengthStringData(5).isAPartOf(wsaaSubfileFields, 5);
	private FixedLengthStringData wsaaQuestion = new FixedLengthStringData(70).isAPartOf(wsaaSubfileFields, 10);
	private FixedLengthStringData wsaaQuesttyp = new FixedLengthStringData(1).isAPartOf(wsaaSubfileFields, 80);

	private FixedLengthStringData wsaaT6770Key = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaT6770Lang = new FixedLengthStringData(1).isAPartOf(wsaaT6770Key, 0);
	private FixedLengthStringData wsaaT6770Questidf = new FixedLengthStringData(4).isAPartOf(wsaaT6770Key, 1);
	private String h143 = "H143";
		/* TABLES */
	private String t6770 = "T6770";
		/* FORMATS */
	private String undqrec = "UNDQREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T6770rec t6770rec = new T6770rec();
		/*Underwriting Question Details*/
	private UndqTableDAM undqIO = new UndqTableDAM();
	private S6767ScreenVars sv = ScreenProgram.getScreenVars( S6767ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		readNext1285, 
		exit1289, 
		preExit
	}

	public P6767() {
		super();
		screenVars = sv;
		new ScreenModel("S6767", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		retrieveData1030();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6767", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}

protected void retrieveData1030()
	{
		lifelnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError600();
		}
		sv.chdrnum.set(lifelnbIO.getChdrnum());
		sv.life.set(lifelnbIO.getLife());
		sv.jlife.set(lifelnbIO.getJlife());
		sv.lifenum.set(lifelnbIO.getLifcnum());
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsio1900();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
			sv.lifename.set(SPACES);
		}
		else {
			plainname();
			sv.lifename.set(wsspcomn.longconfname);
		}
		
		

		undqIO.setParams(SPACES);
		undqIO.setChdrcoy(lifelnbIO.getChdrcoy());
		undqIO.setChdrnum(lifelnbIO.getChdrnum());
		undqIO.setLife(lifelnbIO.getLife());
		undqIO.setJlife(lifelnbIO.getJlife());
		undqIO.setFormat(undqrec);
		undqIO.setFunction(varcom.begn);
		//Perfromance phase-2 atiwari23
		undqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		undqIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","JLIFE");
		SmartFileCode.execute(appVars, undqIO);
		if (isNE(undqIO.getStatuz(),varcom.oK)
		&& isNE(undqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(undqIO.getParams());
			syserrrec.statuz.set(undqIO.getStatuz());
			fatalError600();
		}
		if (isNE(undqIO.getChdrcoy(),lifelnbIO.getChdrcoy())
		|| isNE(undqIO.getChdrnum(),lifelnbIO.getChdrnum())
		|| isNE(undqIO.getLife(),lifelnbIO.getLife())
		|| isNE(undqIO.getJlife(),lifelnbIO.getJlife())) {
			undqIO.setStatuz(varcom.endp);
		}
		wsaaLinesOnPage.set(sv.subfilePage);
		while ( !(isEQ(undqIO.getStatuz(),varcom.endp))) {
			loadSubfile1200();
		}
		
	}

protected void loadSubfile1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start1200();
				}
				case readNext1285: {
					readNext1285();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1200()
	{
		sv.subfileFields.set(SPACES);
		sv.questidf.set(undqIO.getQuestidf());
		sv.language.set(wsspcomn.language);
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6770);
		wsaaT6770Lang.set(wsspcomn.language);
		wsaaT6770Questidf.set(undqIO.getQuestidf());
		itemIO.setItemitem(wsaaT6770Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6770rec.t6770Rec.set(itemIO.getGenarea());
		if (isGT(scrnparams.subfileRrn,wsaaSubfilePage)) {
			wsaaSubfilePage.add(wsaaLinesOnPage);
		}
		compute(wsaaScrnLinesLeft, 0).set(sub(wsaaSubfilePage,scrnparams.subfileRrn));
		if (isNE(sv.questidf,SPACES)
		&& isEQ(wsaaScrnLinesLeft,3)
		&& isNE(t6770rec.question04,SPACES)) {
			wsaaSubfileFields.set(sv.subfileFields);
			sv.subfileFields.set(SPACES);
			for (int loopVar1 = 0; !(loopVar1 == 3); loopVar1 += 1){
				addBlankLine1280();
			}
			sv.subfileFields.set(wsaaSubfileFields);
		}
		if (isNE(sv.questidf,SPACES)
		&& isEQ(wsaaScrnLinesLeft,2)
		&& isNE(t6770rec.question03,SPACES)) {
			wsaaSubfileFields.set(sv.subfileFields);
			sv.subfileFields.set(SPACES);
			for (int loopVar2 = 0; !(loopVar2 == 2); loopVar2 += 1){
				addBlankLine1280();
			}
			sv.subfileFields.set(wsaaSubfileFields);
		}
		if (isNE(sv.questidf,SPACES)
		&& isEQ(wsaaScrnLinesLeft,1)
		&& isNE(t6770rec.question02,SPACES)) {
			wsaaSubfileFields.set(sv.subfileFields);
			sv.subfileFields.set(SPACES);
			addBlankLine1280();
			sv.subfileFields.set(wsaaSubfileFields);
		}
		if (isNE(t6770rec.question02,SPACES)) {
			sv.answer.set(undqIO.getAnswer());
			sv.question.set(t6770rec.question01);
			addSubfileRecord1250();
		}
		else {
			sv.question.set(t6770rec.question01);
			sv.answer.set(undqIO.getAnswer());
			sv.answerOut[varcom.nd.toInt()].set("N");
			addSubfileRecord1250();
			addBlankLine1280();
			goTo(GotoLabel.readNext1285);
		}
		if (isNE(t6770rec.question03,SPACES)) {
			sv.answer.set(SPACES);
			sv.answerOut[varcom.nd.toInt()].set("Y");
			sv.question.set(t6770rec.question02);
			addSubfileRecord1250();
		}
		else {
			sv.answerOut[varcom.nd.toInt()].set("N");
			sv.questtyp.set(t6770rec.questtyp);
			sv.answer.set(SPACES);
			sv.questidf.set(SPACES);
			sv.question.set(t6770rec.question02);
			addSubfileRecord1250();
			addBlankLine1280();
			goTo(GotoLabel.readNext1285);
		}
		if (isNE(t6770rec.question04,SPACES)) {
			sv.answer.set(SPACES);
			sv.answerOut[varcom.nd.toInt()].set("Y");
			sv.question.set(t6770rec.question03);
			addSubfileRecord1250();
			sv.answerOut[varcom.nd.toInt()].set("N");
			sv.questtyp.set(t6770rec.questtyp);
			sv.answer.set(SPACES);
			sv.questidf.set(SPACES);
			sv.question.set(t6770rec.question04);
			addSubfileRecord1250();
			addBlankLine1280();
			goTo(GotoLabel.readNext1285);
		}
		else {
			sv.answerOut[varcom.nd.toInt()].set("N");
			sv.questtyp.set(t6770rec.questtyp);
			sv.answer.set(SPACES);
			sv.questidf.set(SPACES);
			sv.question.set(t6770rec.question03);
			addSubfileRecord1250();
			addBlankLine1280();
		}
	}

protected void readNext1285()
	{
		undqIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, undqIO);
		if (isNE(undqIO.getStatuz(),varcom.oK)
		&& isNE(undqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(undqIO.getParams());
			fatalError600();
		}
		if (isNE(undqIO.getChdrcoy(),lifelnbIO.getChdrcoy())
		|| isNE(undqIO.getChdrnum(),lifelnbIO.getChdrnum())
		|| isNE(undqIO.getLife(),lifelnbIO.getLife())
		|| isNE(undqIO.getJlife(),lifelnbIO.getJlife())) {
			undqIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void addSubfileRecord1250()
	{
		/*INIT*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S6767", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void addBlankLine1280()
	{
		try {
			start1281();
		}
		catch (GOTOException e){
		}
	}

protected void start1281()
	{
		compute(wsaaLineTest, 0).setDivide(scrnparams.subfileRrn, (wsaaSubfilePage));
		wsaaRemainder.setRemainder(wsaaLineTest);
		if (isEQ(wsaaRemainder,ZERO)) {
			goTo(GotoLabel.exit1289);
		}
		sv.questidf.set(SPACES);
		sv.questtyp.set(SPACES);
		sv.question.set(SPACES);
		sv.answer.set(SPACES);
		sv.answerOut[varcom.nd.toInt()].set("Y");
	}

protected void cltsio1900()
	{
		/*CLTSIO*/
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
			sv.lifenumErr.set(h143);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE-SCREEN*/
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.srnch);
		processScreen("S6767", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		validation2610();
		readNextModifiedRecord2680();
	}

protected void validation2610()
	{
		/*UPDATE-ERROR-INDICATORS*/
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6767", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S6767", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
