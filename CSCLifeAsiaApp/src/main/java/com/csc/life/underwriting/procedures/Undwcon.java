/*
 * File: Undwcon.java
 * Date: 30 August 2009 2:49:01
 * Author: Quipoz Limited
 * 
 * Class transformed from UNDWCON.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.underwriting.dataaccess.UndlTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*  UNDERWRITING EXISTENCE CHECK SUBROUTINE
*  ---------------------------------------
*  This  subroutine  is  called  from  OPTSWCH Table T1661. It
*  determines whether the underwriting completed flag has been
*  set on UNDLPF.
***********************************************************************
*                                                                     *
* </pre>
*/
public class Undwcon extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("UNDWCON");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData optsDteeff = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaChkpStatuz = new FixedLengthStringData(4);
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
		/*Underwriting Life Details*/
	private UndlTableDAM undlIO = new UndlTableDAM();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		errorProg610
	}

	public Undwcon() {
		super();
	}

public void mainline(Object... parmArray)
	{
		wsaaChkpStatuz = convertAndSetParam(wsaaChkpStatuz, parmArray, 1);
		optsDteeff = convertAndSetParam(optsDteeff, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		performs0010();
		exit090();
	}

protected void performs0010()
	{
		lifelnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		undlIO.setChdrcoy(lifelnbIO.getChdrcoy());
		undlIO.setChdrnum(lifelnbIO.getChdrnum());
		undlIO.setLife(lifelnbIO.getLife());
		undlIO.setJlife(lifelnbIO.getJlife());
		undlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, undlIO);
		if (isNE(undlIO.getStatuz(),varcom.oK)
		&& isNE(undlIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(undlIO.getParams());
			syserrrec.statuz.set(undlIO.getStatuz());
			fatalError600();
		}
		if (isEQ(undlIO.getStatuz(),varcom.oK)
		&& isEQ(undlIO.getUndwflag(),"Y")) {
			wsaaChkpStatuz.set("DFND");
		}
		else {
			wsaaChkpStatuz.set("NFND");
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		wsaaChkpStatuz.set(varcom.bomb);
		exitProgram();
	}

protected void exit690()
	{
		exitProgram();
	}
}
