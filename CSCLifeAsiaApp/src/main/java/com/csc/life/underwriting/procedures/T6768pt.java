/*
 * File: T6768pt.java
 * Date: 30 August 2009 2:31:49
 * Author: Quipoz Limited
 * 
 * Class transformed from T6768PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.underwriting.tablestructures.T6768rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6768.
*
*
*
***********************************************************************
* </pre>
*/
public class T6768pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(55).isAPartOf(wsaaPrtLine001, 21, FILLER).init("Age & SA based underwriting rules                 S6768");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(70);
	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 0, FILLER).init("Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 9);
	private FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 10, FILLER).init(" Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 18);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 23, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 31);
	private FixedLengthStringData filler6 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 40);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(45);
	private FixedLengthStringData filler7 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Valid from:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 15);
	private FixedLengthStringData filler8 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 25, FILLER).init(SPACES);
	private FixedLengthStringData filler9 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 30, FILLER).init("To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 35);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(49);
	private FixedLengthStringData filler10 = new FixedLengthStringData(49).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Up to                   Up to Sum Assured Amount");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(4);
	private FixedLengthStringData filler11 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 0, FILLER).init(" Age");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(70);
	private FixedLengthStringData filler12 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(11, 0).isAPartOf(wsaaPrtLine006, 6).setPattern("ZZZZZZZZZZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(11, 0).isAPartOf(wsaaPrtLine006, 19).setPattern("ZZZZZZZZZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(11, 0).isAPartOf(wsaaPrtLine006, 32).setPattern("ZZZZZZZZZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(11, 0).isAPartOf(wsaaPrtLine006, 45).setPattern("ZZZZZZZZZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(12, 0).isAPartOf(wsaaPrtLine006, 58).setPattern("ZZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(68);
	private FixedLengthStringData filler17 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 1).setPattern("ZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 12);
	private FixedLengthStringData filler19 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine007, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 25);
	private FixedLengthStringData filler20 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine007, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 38);
	private FixedLengthStringData filler21 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine007, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 51);
	private FixedLengthStringData filler22 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine007, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 64);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(68);
	private FixedLengthStringData filler23 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 1).setPattern("ZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 12);
	private FixedLengthStringData filler25 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 25);
	private FixedLengthStringData filler26 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 38);
	private FixedLengthStringData filler27 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 51);
	private FixedLengthStringData filler28 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 64);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(68);
	private FixedLengthStringData filler29 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 1).setPattern("ZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 12);
	private FixedLengthStringData filler31 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 25);
	private FixedLengthStringData filler32 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 38);
	private FixedLengthStringData filler33 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 51);
	private FixedLengthStringData filler34 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 64);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(68);
	private FixedLengthStringData filler35 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 1).setPattern("ZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 12);
	private FixedLengthStringData filler37 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 25);
	private FixedLengthStringData filler38 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 38);
	private FixedLengthStringData filler39 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 51);
	private FixedLengthStringData filler40 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 64);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(68);
	private FixedLengthStringData filler41 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 1).setPattern("ZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 12);
	private FixedLengthStringData filler43 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine011, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 25);
	private FixedLengthStringData filler44 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine011, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 38);
	private FixedLengthStringData filler45 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine011, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 51);
	private FixedLengthStringData filler46 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine011, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 64);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(68);
	private FixedLengthStringData filler47 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 1).setPattern("ZZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 12);
	private FixedLengthStringData filler49 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine012, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 25);
	private FixedLengthStringData filler50 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine012, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 38);
	private FixedLengthStringData filler51 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine012, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 51);
	private FixedLengthStringData filler52 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine012, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 64);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(68);
	private FixedLengthStringData filler53 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 1).setPattern("ZZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine013, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 12);
	private FixedLengthStringData filler55 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine013, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 25);
	private FixedLengthStringData filler56 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine013, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 38);
	private FixedLengthStringData filler57 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine013, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 51);
	private FixedLengthStringData filler58 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine013, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 64);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(68);
	private FixedLengthStringData filler59 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 1).setPattern("ZZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 12);
	private FixedLengthStringData filler61 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine014, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 25);
	private FixedLengthStringData filler62 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine014, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 38);
	private FixedLengthStringData filler63 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine014, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 51);
	private FixedLengthStringData filler64 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine014, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo059 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 64);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(68);
	private FixedLengthStringData filler65 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 1).setPattern("ZZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine015, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo061 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 12);
	private FixedLengthStringData filler67 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine015, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo062 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 25);
	private FixedLengthStringData filler68 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine015, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo063 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 38);
	private FixedLengthStringData filler69 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine015, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo064 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 51);
	private FixedLengthStringData filler70 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine015, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo065 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 64);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(68);
	private FixedLengthStringData filler71 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 1).setPattern("ZZZ");
	private FixedLengthStringData filler72 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine016, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo067 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 12);
	private FixedLengthStringData filler73 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine016, 16, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo068 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 25);
	private FixedLengthStringData filler74 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine016, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo069 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 38);
	private FixedLengthStringData filler75 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine016, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo070 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 51);
	private FixedLengthStringData filler76 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine016, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo071 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 64);

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(65);
	private FixedLengthStringData filler77 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine017, 0, FILLER).init(" Age Continuation:");
	private FixedLengthStringData fieldNo072 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine017, 19);
	private FixedLengthStringData filler78 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine017, 27, FILLER).init("   Sum Assured Continuation:");
	private FixedLengthStringData fieldNo073 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine017, 57);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6768rec t6768rec = new T6768rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6768pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6768rec.t6768Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo013.set(t6768rec.undwrule01);
		fieldNo014.set(t6768rec.undwrule02);
		fieldNo015.set(t6768rec.undwrule03);
		fieldNo016.set(t6768rec.undwrule04);
		fieldNo019.set(t6768rec.undwrule06);
		fieldNo020.set(t6768rec.undwrule07);
		fieldNo021.set(t6768rec.undwrule08);
		fieldNo022.set(t6768rec.undwrule09);
		fieldNo025.set(t6768rec.undwrule11);
		fieldNo026.set(t6768rec.undwrule12);
		fieldNo027.set(t6768rec.undwrule13);
		fieldNo028.set(t6768rec.undwrule14);
		fieldNo031.set(t6768rec.undwrule16);
		fieldNo032.set(t6768rec.undwrule17);
		fieldNo033.set(t6768rec.undwrule18);
		fieldNo034.set(t6768rec.undwrule19);
		fieldNo037.set(t6768rec.undwrule21);
		fieldNo038.set(t6768rec.undwrule22);
		fieldNo039.set(t6768rec.undwrule23);
		fieldNo040.set(t6768rec.undwrule24);
		fieldNo043.set(t6768rec.undwrule26);
		fieldNo044.set(t6768rec.undwrule27);
		fieldNo045.set(t6768rec.undwrule28);
		fieldNo046.set(t6768rec.undwrule29);
		fieldNo049.set(t6768rec.undwrule31);
		fieldNo050.set(t6768rec.undwrule32);
		fieldNo051.set(t6768rec.undwrule33);
		fieldNo052.set(t6768rec.undwrule34);
		fieldNo055.set(t6768rec.undwrule36);
		fieldNo056.set(t6768rec.undwrule37);
		fieldNo057.set(t6768rec.undwrule38);
		fieldNo058.set(t6768rec.undwrule39);
		fieldNo061.set(t6768rec.undwrule41);
		fieldNo062.set(t6768rec.undwrule42);
		fieldNo063.set(t6768rec.undwrule43);
		fieldNo064.set(t6768rec.undwrule44);
		fieldNo067.set(t6768rec.undwrule46);
		fieldNo068.set(t6768rec.undwrule47);
		fieldNo069.set(t6768rec.undwrule48);
		fieldNo070.set(t6768rec.undwrule49);
		fieldNo072.set(t6768rec.agecont);
		fieldNo073.set(t6768rec.sacont);
		fieldNo012.set(t6768rec.undage01);
		fieldNo007.set(t6768rec.undsa01);
		fieldNo008.set(t6768rec.undsa02);
		fieldNo009.set(t6768rec.undsa03);
		fieldNo010.set(t6768rec.undsa04);
		fieldNo011.set(t6768rec.undsa05);
		fieldNo017.set(t6768rec.undwrule05);
		fieldNo023.set(t6768rec.undwrule10);
		fieldNo029.set(t6768rec.undwrule15);
		fieldNo035.set(t6768rec.undwrule20);
		fieldNo041.set(t6768rec.undwrule25);
		fieldNo047.set(t6768rec.undwrule30);
		fieldNo053.set(t6768rec.undwrule35);
		fieldNo059.set(t6768rec.undwrule40);
		fieldNo065.set(t6768rec.undwrule45);
		fieldNo071.set(t6768rec.undwrule50);
		fieldNo018.set(t6768rec.undage02);
		fieldNo024.set(t6768rec.undage03);
		fieldNo030.set(t6768rec.undage04);
		fieldNo036.set(t6768rec.undage05);
		fieldNo042.set(t6768rec.undage06);
		fieldNo048.set(t6768rec.undage07);
		fieldNo054.set(t6768rec.undage08);
		fieldNo060.set(t6768rec.undage09);
		fieldNo066.set(t6768rec.undage10);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
