package com.csc.life.underwriting.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UndqpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:44
 * Class transformed from UNDQPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UndqpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 86;
	public FixedLengthStringData undqrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData undqpfRecord = undqrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(undqrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(undqrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(undqrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(undqrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(undqrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(undqrec);
	public FixedLengthStringData questidf = DD.questidf.copy().isAPartOf(undqrec);
	public FixedLengthStringData answer = DD.answer.copy().isAPartOf(undqrec);
	public FixedLengthStringData undwrule = DD.undwrule.copy().isAPartOf(undqrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(undqrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(undqrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(undqrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(undqrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(undqrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UndqpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UndqpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UndqpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UndqpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UndqpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UndqpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UndqpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("UNDQPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"CURRFROM, " +
							"CURRTO, " +
							"QUESTIDF, " +
							"ANSWER, " +
							"UNDWRULE, " +
							"TRANNO, " +
							"VALIDFLAG, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     currfrom,
                                     currto,
                                     questidf,
                                     answer,
                                     undwrule,
                                     tranno,
                                     validflag,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		currfrom.clear();
  		currto.clear();
  		questidf.clear();
  		answer.clear();
  		undwrule.clear();
  		tranno.clear();
  		validflag.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUndqrec() {
  		return undqrec;
	}

	public FixedLengthStringData getUndqpfRecord() {
  		return undqpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUndqrec(what);
	}

	public void setUndqrec(Object what) {
  		this.undqrec.set(what);
	}

	public void setUndqpfRecord(Object what) {
  		this.undqpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(undqrec.getLength());
		result.set(undqrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}