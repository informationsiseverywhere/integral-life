package com.csc.life.underwriting.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:29
 * Description:
 * Copybook name: P5017PAR
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class P5017par extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData parmRecord = new FixedLengthStringData(38);
  	public FixedLengthStringData itemToCreate = new FixedLengthStringData(8).isAPartOf(parmRecord, 0);
  	public FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(parmRecord, 8);


	public void initialize() {
		COBOLFunctions.initialize(parmRecord);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRecord.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}