package com.csc.life.underwriting.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:32
 * Description:
 * Copybook name: UNDCAFIKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Undcafikey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData undcafiFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData undcafiKey = new FixedLengthStringData(256).isAPartOf(undcafiFileKey, 0, REDEFINE);
  	public FixedLengthStringData undcafiChdrcoy = new FixedLengthStringData(1).isAPartOf(undcafiKey, 0);
  	public FixedLengthStringData undcafiChdrnum = new FixedLengthStringData(8).isAPartOf(undcafiKey, 1);
  	public FixedLengthStringData undcafiLife = new FixedLengthStringData(2).isAPartOf(undcafiKey, 9);
  	public FixedLengthStringData undcafiJlife = new FixedLengthStringData(2).isAPartOf(undcafiKey, 11);
  	public FixedLengthStringData undcafiCoverage = new FixedLengthStringData(2).isAPartOf(undcafiKey, 13);
  	public FixedLengthStringData undcafiRider = new FixedLengthStringData(2).isAPartOf(undcafiKey, 15);
  	public PackedDecimalData undcafiTranno = new PackedDecimalData(5, 0).isAPartOf(undcafiKey, 17);
  	public FixedLengthStringData filler = new FixedLengthStringData(236).isAPartOf(undcafiKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(undcafiFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		undcafiFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}