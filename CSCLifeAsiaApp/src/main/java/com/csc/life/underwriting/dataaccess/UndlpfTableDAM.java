package com.csc.life.underwriting.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UndlpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:44
 * Class transformed from UNDLPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UndlpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 137;
	public FixedLengthStringData undlrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData undlpfRecord = undlrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(undlrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(undlrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(undlrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(undlrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(undlrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(undlrec);
	public PackedDecimalData height = DD.height.copy().isAPartOf(undlrec);
	public PackedDecimalData weight = DD.weight.copy().isAPartOf(undlrec);
	public PackedDecimalData bmi = DD.bmi.copy().isAPartOf(undlrec);
	public FixedLengthStringData bmibasis = DD.bmibasis.copy().isAPartOf(undlrec);
	public FixedLengthStringData bmirule = DD.bmirule.copy().isAPartOf(undlrec);
	public FixedLengthStringData ovrrule = DD.ovrrule.copy().isAPartOf(undlrec);
	public FixedLengthStringData clntnum01 = DD.clntnum.copy().isAPartOf(undlrec);
	public FixedLengthStringData clntnum02 = DD.clntnum.copy().isAPartOf(undlrec);
	public FixedLengthStringData clntnum03 = DD.clntnum.copy().isAPartOf(undlrec);
	public FixedLengthStringData clntnum04 = DD.clntnum.copy().isAPartOf(undlrec);
	public FixedLengthStringData clntnum05 = DD.clntnum.copy().isAPartOf(undlrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(undlrec);
	public FixedLengthStringData undwflag = DD.undwflag.copy().isAPartOf(undlrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(undlrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(undlrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(undlrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(undlrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UndlpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UndlpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UndlpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UndlpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UndlpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UndlpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UndlpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("UNDLPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"CURRFROM, " +
							"CURRTO, " +
							"HEIGHT, " +
							"WEIGHT, " +
							"BMI, " +
							"BMIBASIS, " +
							"BMIRULE, " +
							"OVRRULE, " +
							"CLNTNUM01, " +
							"CLNTNUM02, " +
							"CLNTNUM03, " +
							"CLNTNUM04, " +
							"CLNTNUM05, " +
							"TRANNO, " +
							"UNDWFLAG, " +
							"VALIDFLAG, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     currfrom,
                                     currto,
                                     height,
                                     weight,
                                     bmi,
                                     bmibasis,
                                     bmirule,
                                     ovrrule,
                                     clntnum01,
                                     clntnum02,
                                     clntnum03,
                                     clntnum04,
                                     clntnum05,
                                     tranno,
                                     undwflag,
                                     validflag,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		currfrom.clear();
  		currto.clear();
  		height.clear();
  		weight.clear();
  		bmi.clear();
  		bmibasis.clear();
  		bmirule.clear();
  		ovrrule.clear();
  		clntnum01.clear();
  		clntnum02.clear();
  		clntnum03.clear();
  		clntnum04.clear();
  		clntnum05.clear();
  		tranno.clear();
  		undwflag.clear();
  		validflag.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUndlrec() {
  		return undlrec;
	}

	public FixedLengthStringData getUndlpfRecord() {
  		return undlpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUndlrec(what);
	}

	public void setUndlrec(Object what) {
  		this.undlrec.set(what);
	}

	public void setUndlpfRecord(Object what) {
  		this.undlpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(undlrec.getLength());
		result.set(undlrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}