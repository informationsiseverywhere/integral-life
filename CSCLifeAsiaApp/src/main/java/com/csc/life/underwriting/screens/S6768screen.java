package com.csc.life.underwriting.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class S6768screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 17, 22, 4, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 1, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6768ScreenVars sv = (S6768ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6768screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6768ScreenVars screenVars = (S6768ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.undwrule01.setClassString("");
		screenVars.undwrule02.setClassString("");
		screenVars.undwrule03.setClassString("");
		screenVars.undwrule04.setClassString("");
		screenVars.undwrule06.setClassString("");
		screenVars.undwrule07.setClassString("");
		screenVars.undwrule08.setClassString("");
		screenVars.undwrule09.setClassString("");
		screenVars.undwrule11.setClassString("");
		screenVars.undwrule12.setClassString("");
		screenVars.undwrule13.setClassString("");
		screenVars.undwrule14.setClassString("");
		screenVars.undwrule16.setClassString("");
		screenVars.undwrule17.setClassString("");
		screenVars.undwrule18.setClassString("");
		screenVars.undwrule19.setClassString("");
		screenVars.undwrule21.setClassString("");
		screenVars.undwrule22.setClassString("");
		screenVars.undwrule23.setClassString("");
		screenVars.undwrule24.setClassString("");
		screenVars.undwrule26.setClassString("");
		screenVars.undwrule27.setClassString("");
		screenVars.undwrule28.setClassString("");
		screenVars.undwrule29.setClassString("");
		screenVars.undwrule31.setClassString("");
		screenVars.undwrule32.setClassString("");
		screenVars.undwrule33.setClassString("");
		screenVars.undwrule34.setClassString("");
		screenVars.undwrule36.setClassString("");
		screenVars.undwrule37.setClassString("");
		screenVars.undwrule38.setClassString("");
		screenVars.undwrule39.setClassString("");
		screenVars.undwrule41.setClassString("");
		screenVars.undwrule42.setClassString("");
		screenVars.undwrule43.setClassString("");
		screenVars.undwrule44.setClassString("");
		screenVars.undwrule46.setClassString("");
		screenVars.undwrule47.setClassString("");
		screenVars.undwrule48.setClassString("");
		screenVars.undwrule49.setClassString("");
		screenVars.agecont.setClassString("");
		screenVars.sacont.setClassString("");
		screenVars.undage01.setClassString("");
		screenVars.undsa01.setClassString("");
		screenVars.undsa02.setClassString("");
		screenVars.undsa03.setClassString("");
		screenVars.undsa04.setClassString("");
		screenVars.undsa05.setClassString("");
		screenVars.undwrule05.setClassString("");
		screenVars.undwrule10.setClassString("");
		screenVars.undwrule15.setClassString("");
		screenVars.undwrule20.setClassString("");
		screenVars.undwrule25.setClassString("");
		screenVars.undwrule30.setClassString("");
		screenVars.undwrule35.setClassString("");
		screenVars.undwrule40.setClassString("");
		screenVars.undwrule45.setClassString("");
		screenVars.undwrule50.setClassString("");
		screenVars.undage02.setClassString("");
		screenVars.undage03.setClassString("");
		screenVars.undage04.setClassString("");
		screenVars.undage05.setClassString("");
		screenVars.undage06.setClassString("");
		screenVars.undage07.setClassString("");
		screenVars.undage08.setClassString("");
		screenVars.undage09.setClassString("");
		screenVars.undage10.setClassString("");
	}

/**
 * Clear all the variables in S6768screen
 */
	public static void clear(VarModel pv) {
		S6768ScreenVars screenVars = (S6768ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.undwrule01.clear();
		screenVars.undwrule02.clear();
		screenVars.undwrule03.clear();
		screenVars.undwrule04.clear();
		screenVars.undwrule06.clear();
		screenVars.undwrule07.clear();
		screenVars.undwrule08.clear();
		screenVars.undwrule09.clear();
		screenVars.undwrule11.clear();
		screenVars.undwrule12.clear();
		screenVars.undwrule13.clear();
		screenVars.undwrule14.clear();
		screenVars.undwrule16.clear();
		screenVars.undwrule17.clear();
		screenVars.undwrule18.clear();
		screenVars.undwrule19.clear();
		screenVars.undwrule21.clear();
		screenVars.undwrule22.clear();
		screenVars.undwrule23.clear();
		screenVars.undwrule24.clear();
		screenVars.undwrule26.clear();
		screenVars.undwrule27.clear();
		screenVars.undwrule28.clear();
		screenVars.undwrule29.clear();
		screenVars.undwrule31.clear();
		screenVars.undwrule32.clear();
		screenVars.undwrule33.clear();
		screenVars.undwrule34.clear();
		screenVars.undwrule36.clear();
		screenVars.undwrule37.clear();
		screenVars.undwrule38.clear();
		screenVars.undwrule39.clear();
		screenVars.undwrule41.clear();
		screenVars.undwrule42.clear();
		screenVars.undwrule43.clear();
		screenVars.undwrule44.clear();
		screenVars.undwrule46.clear();
		screenVars.undwrule47.clear();
		screenVars.undwrule48.clear();
		screenVars.undwrule49.clear();
		screenVars.agecont.clear();
		screenVars.sacont.clear();
		screenVars.undage01.clear();
		screenVars.undsa01.clear();
		screenVars.undsa02.clear();
		screenVars.undsa03.clear();
		screenVars.undsa04.clear();
		screenVars.undsa05.clear();
		screenVars.undwrule05.clear();
		screenVars.undwrule10.clear();
		screenVars.undwrule15.clear();
		screenVars.undwrule20.clear();
		screenVars.undwrule25.clear();
		screenVars.undwrule30.clear();
		screenVars.undwrule35.clear();
		screenVars.undwrule40.clear();
		screenVars.undwrule45.clear();
		screenVars.undwrule50.clear();
		screenVars.undage02.clear();
		screenVars.undage03.clear();
		screenVars.undage04.clear();
		screenVars.undage05.clear();
		screenVars.undage06.clear();
		screenVars.undage07.clear();
		screenVars.undage08.clear();
		screenVars.undage09.clear();
		screenVars.undage10.clear();
	}
}
