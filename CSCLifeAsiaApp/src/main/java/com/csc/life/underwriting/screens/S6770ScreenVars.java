package com.csc.life.underwriting.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6770
 * @version 1.0 generated on 30/08/09 07:00
 * @author Quipoz
 */
public class S6770ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData answband = DD.answband.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,4);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,5);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,13);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,21);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,29);
	public FixedLengthStringData questions = new FixedLengthStringData(280).isAPartOf(dataFields, 59);
	public FixedLengthStringData[] question = FLSArrayPartOfStructure(4, 70, questions, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(280).isAPartOf(questions, 0, FILLER_REDEFINE);
	public FixedLengthStringData question01 = DD.question.copy().isAPartOf(filler,0);
	public FixedLengthStringData question02 = DD.question.copy().isAPartOf(filler,70);
	public FixedLengthStringData question03 = DD.question.copy().isAPartOf(filler,140);
	public FixedLengthStringData question04 = DD.question.copy().isAPartOf(filler,210);
	public FixedLengthStringData questtyp = DD.questtyp.copy().isAPartOf(dataFields,339);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,340);
	public FixedLengthStringData undwrule = DD.undwrule.copy().isAPartOf(dataFields,345);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData answbandErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData questionsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] questionErr = FLSArrayPartOfStructure(4, 4, questionsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(16).isAPartOf(questionsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData question01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData question02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData question03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData question04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData questtypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData undwruleErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] answbandOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData questionsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] questionOut = FLSArrayPartOfStructure(4, 12, questionsOut, 0);
	public FixedLengthStringData[][] questionO = FLSDArrayPartOfArrayStructure(12, 1, questionOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(questionsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] question01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] question02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] question03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] question04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] questtypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] undwruleOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6770screenWritten = new LongData(0);
	public LongData S6770protectWritten = new LongData(0);
	public static int[] screenSflPfInds = new int[] {8, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 1, 2, 3, 21}; 
	public static int[] screenSflAffectedInds = new int[] {6, 7, 4, 5, 1, 2, 3};

	public boolean hasSubfile() {
		return false;
	}


	public S6770ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(questtypOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(undwruleOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(answbandOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6770screen.class;
		protectRecord = S6770protect.class;
	}
	public int getDataAreaSize() {
		return 557;
	}
	public int getDataFieldsSize(){
		return 349;  
	}
	
	public int getErrorIndicatorSize(){
		return 52; 
	}
	public int getOutputFieldSize(){
		return 156; 
	}
	public BaseData[] getscreenFields(){
		return new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, question01, question02, question03, question04, questtyp, undwrule, answband};
	}
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][]  {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, question01Out, question02Out, question03Out, question04Out, questtypOut, undwruleOut, answbandOut};
	}
	
	public BaseData[] getscreenErrFields(){
		return new BaseData[]  {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, question01Err, question02Err, question03Err, question04Err, questtypErr, undwruleErr, answbandErr};
		
	}
	
	public BaseData[] getscreenDateFields(){
		return new BaseData[] {itmfrm, itmto};
	}
	

	public BaseData[] getscreenDateDispFields(){
		return new BaseData[] {itmfrmDisp, itmtoDisp};
	}
	
	
	public BaseData[] getscreenDateErrFields(){
		return new BaseData[] {itmfrmErr, itmtoErr};

	}
	
	public static int[] getScreenSflPfInds(){
		return screenSflPfInds;
	}
	public static int[] getScreenSflAffectedInds()
	{
		return screenSflAffectedInds;
	}


}
