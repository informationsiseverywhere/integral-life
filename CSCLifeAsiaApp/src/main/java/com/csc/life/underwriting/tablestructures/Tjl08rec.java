package com.csc.life.underwriting.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Tjl08rec extends ExternalData{

 	public FixedLengthStringData tjl08Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData sumAssuredFactor = new ZonedDecimalData(7, 2).isAPartOf(tjl08Rec, 0);
  	public FixedLengthStringData riskClasses = new FixedLengthStringData(20).isAPartOf(tjl08Rec, 7);
  	public FixedLengthStringData[] riskClass = FLSArrayPartOfStructure(5, 4, riskClasses, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(riskClasses, 0, FILLER_REDEFINE);
  	public FixedLengthStringData riskClass01 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
  	public FixedLengthStringData riskClass02 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
  	public FixedLengthStringData riskClass03 = new FixedLengthStringData(4).isAPartOf(filler1, 8);
  	public FixedLengthStringData riskClass04 = new FixedLengthStringData(4).isAPartOf(filler1, 12);
  	public FixedLengthStringData riskClass05 = new FixedLengthStringData(4).isAPartOf(filler1, 16);
  	public FixedLengthStringData filler = new FixedLengthStringData(473).isAPartOf(tjl08Rec, 27, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tjl08Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			tjl08Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}

}
