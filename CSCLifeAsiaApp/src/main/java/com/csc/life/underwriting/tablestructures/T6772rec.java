package com.csc.life.underwriting.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:29
 * Description:
 * Copybook name: T6772REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6772rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6772Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData fupcdess = new FixedLengthStringData(40).isAPartOf(t6772Rec, 0);
  	public FixedLengthStringData[] fupcdes = FLSArrayPartOfStructure(10, 4, fupcdess, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(fupcdess, 0, FILLER_REDEFINE);
  	public FixedLengthStringData fupcdes01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData fupcdes02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData fupcdes03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData fupcdes04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData fupcdes05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData fupcdes06 = new FixedLengthStringData(4).isAPartOf(filler, 20);
  	public FixedLengthStringData fupcdes07 = new FixedLengthStringData(4).isAPartOf(filler, 24);
  	public FixedLengthStringData fupcdes08 = new FixedLengthStringData(4).isAPartOf(filler, 28);
  	public FixedLengthStringData fupcdes09 = new FixedLengthStringData(4).isAPartOf(filler, 32);
  	public FixedLengthStringData fupcdes10 = new FixedLengthStringData(4).isAPartOf(filler, 36);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(t6772Rec, 40);
  	public FixedLengthStringData opcdas = new FixedLengthStringData(20).isAPartOf(t6772Rec, 41);
  	public FixedLengthStringData[] opcda = FLSArrayPartOfStructure(10, 2, opcdas, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(opcdas, 0, FILLER_REDEFINE);
  	public FixedLengthStringData opcda01 = new FixedLengthStringData(2).isAPartOf(filler1, 0);
  	public FixedLengthStringData opcda02 = new FixedLengthStringData(2).isAPartOf(filler1, 2);
  	public FixedLengthStringData opcda03 = new FixedLengthStringData(2).isAPartOf(filler1, 4);
  	public FixedLengthStringData opcda04 = new FixedLengthStringData(2).isAPartOf(filler1, 6);
  	public FixedLengthStringData opcda05 = new FixedLengthStringData(2).isAPartOf(filler1, 8);
  	public FixedLengthStringData opcda06 = new FixedLengthStringData(2).isAPartOf(filler1, 10);
  	public FixedLengthStringData opcda07 = new FixedLengthStringData(2).isAPartOf(filler1, 12);
  	public FixedLengthStringData opcda08 = new FixedLengthStringData(2).isAPartOf(filler1, 14);
  	public FixedLengthStringData opcda09 = new FixedLengthStringData(2).isAPartOf(filler1, 16);
  	public FixedLengthStringData opcda10 = new FixedLengthStringData(2).isAPartOf(filler1, 18);
  	public FixedLengthStringData ruletype = new FixedLengthStringData(1).isAPartOf(t6772Rec, 61);
  	public ZonedDecimalData seqnumb = new ZonedDecimalData(3, 0).isAPartOf(t6772Rec, 62);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(435).isAPartOf(t6772Rec, 65, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6772Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6772Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}