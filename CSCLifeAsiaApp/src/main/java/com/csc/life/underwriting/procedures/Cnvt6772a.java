/*
 * File: Cnvt6772a.java
 * Date: 29 August 2009 22:41:59
 * Author: Quipoz Limited
 *
 * Class transformed from CNVT6772A.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItempfTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Delete the existing records in ITEMPF with ITEM TABLE = T6772.
*   Copy the backup data of T6772 into ITEMPF.
*
*   Logic :- Loop T6772 and delete the record.
*            Loop T6772OLD (back-up data) and write to ITEMPF.
*
***********************************************************************
* </pre>
*/
public class Cnvt6772a extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private DiskFileDAM itemold = new DiskFileDAM("T6772OLD");
	private ItempfTableDAM itemoldRec = new ItempfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("T6772");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/* FORMATS */
	private String itdmrec = "ITEMREC";
		/* TABLES */
	private String t6772 = "T6772";
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		beginDel2000,
		exit2000,
		readItemold3020,
		checkError3030
	}

	public Cnvt6772a() {
		super();
	}

public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void control1000()
	{
		/*START*/
		wsspEdterror.set(varcom.oK);
		initialize1100();
		deleteExistingT67722000();
		if (isEQ(wsspEdterror,varcom.oK)
		|| isEQ(wsspEdterror,varcom.endp)) {
			copyOldT67723000();
		}
		/*EXIT*/
		stopRun();
	}

protected void initialize1100()
	{
		/*BEGIN*/
		itdmIO.setItemrecKeyData(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(lsaaCompany);
		itdmIO.setItemtabl(t6772);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		/*EXIT*/
	}

protected void deleteExistingT67722000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case beginDel2000: {
					beginDel2000();
				}
				case exit2000: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void beginDel2000()
	{
		callItdmio2200();
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItempfx(),"IT")
		|| isNE(itdmIO.getItemcoy(),lsaaCompany)
		|| isNE(itdmIO.getItemtabl(),t6772)) {
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit2000);
		}
		itdmIO.setFunction(varcom.deltd);
		callItdmio2200();
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit2000);
		}
		itdmIO.setFunction(varcom.nextr);
		goTo(GotoLabel.beginDel2000);
	}

protected void copyOldT67723000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					copy3010();
				}
				case readItemold3020: {
					readItemold3020();
				}
				case checkError3030: {
					checkError3030();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void copy3010()
	{
		wsspEdterror.set(varcom.oK);
		itemold.openInput();
	}

protected void readItemold3020()
	{
		itemold.readNext(itemoldRec);
		if (itemold.isAtEnd()) {
			goTo(GotoLabel.checkError3030);
		}
		if (isNE(itemoldRec.itempfx,"IT")) {
			goTo(GotoLabel.readItemold3020);
		}
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx(itemoldRec.itempfx);
		itdmIO.setItemcoy(itemoldRec.itemcoy);
		itdmIO.setItemtabl(itemoldRec.itemtabl);
		itdmIO.setItemitem(itemoldRec.itemitem);
		itdmIO.setItemseq(itemoldRec.itemseq);
		itdmIO.setTranid(itemoldRec.tranid);
		itdmIO.setTableprog(itemoldRec.tableprog);
		itdmIO.setValidflag(itemoldRec.validflag);
		itdmIO.setItmfrm(itemoldRec.itmfrm);
		itdmIO.setItmto(itemoldRec.itmto);
		itdmIO.setGenarea(itemoldRec.genarea);
		itdmIO.setUserProfile(itemoldRec.userProfile);
		itdmIO.setJobName(itemoldRec.jobName);
		itdmIO.setDatime(itemoldRec.datime);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.writr);
		callItdmio2200();
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			wsspEdterror.set(itdmIO.getStatuz());
			goTo(GotoLabel.checkError3030);
		}
		goTo(GotoLabel.readItemold3020);
	}

protected void checkError3030()
	{
		if (isEQ(wsspEdterror,varcom.oK)) {
			lsaaStatuz.set(varcom.oK);
			appVars.commit();
		}
		else {
			lsaaStatuz.set(SPACES);
		}
		itemold.close();
		/*EXIT*/
	}

protected void callItdmio2200()
	{
		/*START*/
		itdmIO.setFormat(itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)
		&& isNE(itdmIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void fatalError9000()
	{
		/*START*/
		rollback();
		lsaaStatuz.set("BOMB");
		syserrrec.syserrType.set("1");
		syserrrec.subrname.set(wsaaProg);
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		stopRun();
	}
}
