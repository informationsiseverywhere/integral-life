package com.csc.life.underwriting.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6767
 * @version 1.0 generated on 30/08/09 06:59
 * @author Quipoz
 */
public class S6767ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(147);
	public FixedLengthStringData dataFields = new FixedLengthStringData(67).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 67);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 87);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(163);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(81).isAPartOf(subfileArea, 0);
	public FixedLengthStringData answer = DD.answer.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData language = DD.language.copy().isAPartOf(subfileFields,4);
	public FixedLengthStringData questidf = DD.questidf.copy().isAPartOf(subfileFields,5);
	public FixedLengthStringData question = DD.question.copy().isAPartOf(subfileFields,10);
	public FixedLengthStringData questtyp = DD.questtyp.copy().isAPartOf(subfileFields,80);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(20).isAPartOf(subfileArea, 81);
	public FixedLengthStringData answerErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData languageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData questidfErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData questionErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData questtypErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(60).isAPartOf(subfileArea, 101);
	public FixedLengthStringData[] answerOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] languageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] questidfOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] questionOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] questtypOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 161);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S6767screensflWritten = new LongData(0);
	public LongData S6767screenctlWritten = new LongData(0);
	public LongData S6767screenWritten = new LongData(0);
	public LongData S6767protectWritten = new LongData(0);
	public GeneralTable s6767screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6767screensfl;
	}

	public S6767ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {language, questidf, questtyp, question, answer};
		screenSflOutFields = new BaseData[][] {languageOut, questidfOut, questtypOut, questionOut, answerOut};
		screenSflErrFields = new BaseData[] {languageErr, questidfErr, questtypErr, questionErr, answerErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, life, jlife, lifenum, lifename};
		screenOutFields = new BaseData[][] {chdrnumOut, lifeOut, jlifeOut, lifenumOut, lifenameOut};
		screenErrFields = new BaseData[] {chdrnumErr, lifeErr, jlifeErr, lifenumErr, lifenameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6767screen.class;
		screenSflRecord = S6767screensfl.class;
		screenCtlRecord = S6767screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6767protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6767screenctl.lrec.pageSubfile);
	}
}
