/*
 * File: Pt562.java
 * Date: 30 August 2009 2:02:06
 * Author: Quipoz Limited
 *
 * Class transformed from PT562.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.underwriting.procedures.Tt562pt;
import com.csc.life.underwriting.screens.St562ScreenVars;
import com.csc.life.underwriting.tablestructures.Tt562rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
* * * * * * * * * * * *  W A R N I N G  * * * * * * * * * * * *
*
* EXTRA DATA SCREEN CONTAINS MORE THAN 500 BYTES - ( MAX SIZE OF
* ITEM-GENAREA ). - THEREFORE FIELDS HAVE BEEN CHANGED TO COMP-4.
*
* THIS GENERATED PROGRAM HAS BEEN AMENDED TO MAKE INDIVIDUAL FILED
* MOVES TO AVOID CONVERSION OF COMP-4 FIELDS WHICH WAS CAUSING
* DECIMAL DATA ERRORS.
*
*
*
*
*
*****************************************************************
* </pre>
*/
public class Pt562 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PT562");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Tt562rec tt562rec = new Tt562rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private St562ScreenVars sv = ScreenProgram.getScreenVars( St562ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		generalArea1045,
		other3080,
		exit3090
	}

	public Pt562() {
		super();
		screenVars = sv;
		new ScreenModel("St562", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				case generalArea1045:
					generalArea1045();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itemIO.setParams(SPACES);
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		tt562rec.tt562Rec.set(itemIO.getGenarea());
		/*    IF THE EXTRA DATA AREA WAS NOT USED BEFORE THE NUMERIC     **/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.*/
		if (isNE(itemIO.getGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		wsaaSub1.set(ZERO);
		/*    PERFORM 1400-INIT-NXDP      15 TIMES.                        */
		for (int loopVar1 = 0; !(loopVar1 == 16); loopVar1 += 1){
			initNxdp1400();
		}
		wsaaSub1.set(ZERO);
		for (int loopVar2 = 0; !(loopVar2 == 99); loopVar2 += 1){
			initNxmon1500();
		}
		tt562rec.nxmoney.set(ZERO);
		tt562rec.tnxmon01.set(ZERO);
		tt562rec.tnxmon02.set(ZERO);
		tt562rec.tnxmon03.set(ZERO);
		tt562rec.tnxmon04.set(ZERO);
		tt562rec.tnxmon05.set(ZERO);
		tt562rec.tnxmon06.set(ZERO);
		tt562rec.tnxmon07.set(ZERO);
		tt562rec.tnxmon08.set(ZERO);
		tt562rec.tnxmon09.set(ZERO);
		tt562rec.tnxmon10.set(ZERO);
		tt562rec.tnxmon11.set(ZERO);
	}

protected void generalArea1045()
	{
		wsaaSub1.set(0);
		/*    PERFORM 1600-MOVE-NXDP      15 TIMES.                        */
		for (int loopVar3 = 0; !(loopVar3 == 16); loopVar3 += 1){
			moveNxdp1600();
		}
		wsaaSub1.set(0);
		for (int loopVar4 = 0; !(loopVar4 == 99); loopVar4 += 1){
			moveNxmon1700();
		}
		sv.nxmoney.set(tt562rec.nxmoney);
		sv.tnxmon01.set(tt562rec.tnxmon01);
		sv.tnxmon02.set(tt562rec.tnxmon02);
		sv.tnxmon03.set(tt562rec.tnxmon03);
		sv.tnxmon04.set(tt562rec.tnxmon04);
		sv.tnxmon05.set(tt562rec.tnxmon05);
		sv.tnxmon06.set(tt562rec.tnxmon06);
		sv.tnxmon07.set(tt562rec.tnxmon07);
		sv.tnxmon08.set(tt562rec.tnxmon08);
		sv.tnxmon09.set(tt562rec.tnxmon09);
		sv.tnxmon10.set(tt562rec.tnxmon10);
		sv.tnxmon11.set(tt562rec.tnxmon11);
	}

protected void initNxdp1400()
	{
		/*PARA*/
		wsaaSub1.add(1);
		tt562rec.nxdp[wsaaSub1.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void initNxmon1500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		tt562rec.nxmon[wsaaSub1.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void moveNxdp1600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		sv.nxdp[wsaaSub1.toInt()].set(tt562rec.nxdp[wsaaSub1.toInt()]);
		/*EXIT*/
	}

protected void moveNxmon1700()
	{
		/*PARA*/
		wsaaSub1.add(1);
		sv.nxmon[wsaaSub1.toInt()].set(tt562rec.nxmon[wsaaSub1.toInt()]);
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
					screenIo2010();
					exit2090();
				}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			return ;
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080:
				case exit3090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itemIO.setTableprog(wsaaProg);
		itemIO.setGenarea(tt562rec.tt562Rec);
		itemIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		/*CHECK*/
		wsaaSub1.set(0);
		/*    PERFORM 3400-UPDATE-NXDP    15 TIMES.                        */
		for (int loopVar5 = 0; !(loopVar5 == 16); loopVar5 += 1){
			updateNxdp3400();
		}
		if (isNE(sv.nxmoney,tt562rec.nxmoney)) {
			tt562rec.nxmoney.set(sv.nxmoney);
			wsaaUpdateFlag = "Y";
		}
		wsaaSub1.set(0);
		for (int loopVar6 = 0; !(loopVar6 == 99); loopVar6 += 1){
			updateNxmon3500();
		}
		/*    IF ST562-TNXMON-01               NOT =                       */
		/*       TT562-TNXMON-01                                           */
		/*        MOVE ST562-TNXMON-01                                     */
		/*          TO TT562-TNXMON-01                                     */
		/*        MOVE 'Y' TO WSAA-UPDATE-FLAG.                            */
		/*    IF ST562-TNXMON-02               NOT =                       */
		/*       TT562-TNXMON-02                                           */
		/*        MOVE ST562-TNXMON-02                                     */
		/*          TO TT562-TNXMON-02                                     */
		/*        MOVE 'Y' TO WSAA-UPDATE-FLAG.                            */
		/*    IF ST562-TNXMON-03               NOT =                       */
		/*       TT562-TNXMON-03                                           */
		/*        MOVE ST562-TNXMON-03                                     */
		/*          TO TT562-TNXMON-03                                     */
		/*        MOVE 'Y' TO WSAA-UPDATE-FLAG.                            */
		wsaaSub1.set(0);
		for (int loopVar7 = 0; !(loopVar7 == 11); loopVar7 += 1){
			updateTnxmon3600();
		}
		/*EXIT*/
	}

protected void updateNxdp3400()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.nxdp[wsaaSub1.toInt()],tt562rec.nxdp[wsaaSub1.toInt()])) {
			tt562rec.nxdp[wsaaSub1.toInt()].set(sv.nxdp[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*EXIT*/
	}

protected void updateNxmon3500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.nxmon[wsaaSub1.toInt()],tt562rec.nxmon[wsaaSub1.toInt()])) {
			tt562rec.nxmon[wsaaSub1.toInt()].set(sv.nxmon[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*EXIT*/
	}

protected void updateTnxmon3600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.tnxmon[wsaaSub1.toInt()], tt562rec.tnxmon[wsaaSub1.toInt()])) {
			tt562rec.tnxmon[wsaaSub1.toInt()].set(sv.tnxmon[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DUMMY CALL TO GENERATED PRINT PROGRAM TO ENSURE THAT
	*      IT IS TRANSFERED, TA/TR, ALONG WITH REST OF SUITE.
	* </pre>
	*/
protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Tt562pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
