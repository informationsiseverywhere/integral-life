/*
 * File: Actcalc.java
 * Date: 29 August 2009 20:11:45
 * Author: Quipoz Limited
 * 
 * Class transformed from ACTCALC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.underwriting.recordstructures.Actcalcrec;
import com.csc.life.underwriting.tablestructures.T6641rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*   THIS SUB-ROUTINE IS PART OF THE TRADITIONAL BUSINESS
*    SUITE OF PROGRAMS.
*
* This subroutine will calculate different Actuarial factors
* according to the function passed.
* Initially the following functions will be incorporated:
*         EBIGA
*         TBIGA
*         WBIGA
*         EADUE
*         TADUE
*         WADUE
*         ENETP
*         TNETP
*         WNETP
*
* (i)     Function EBIGA = Calculate variable EBIGA by the
*         formula:
*                          i    Nx-Nx+n
*         EBIGA = 1 -  ---- * ------
*                        (1+i)  Nx-Nx+1
*         x is the initial age passed,
*         x+n is the final age passed - final age (1)
*         i is the last 3 characters of the Table item key
*         divided by 10,000
*         Nx is a data item on the extra data screen of the
*         Table item, for age x.
*
*         eg.    Table Tnnn2 item key = 6770u001a400, initial age 40,
*                                    final age 60
*
*             400/10000       N40-N60       .04   (132001-35841)
* EBIGA = 1 - ------------- * ------- = 1 - ---- * --------------
*             (1+400/10000)   N40-N41       1.04  (132001-125015)
*
*                           = 0.47059
*
* (ii)    Function WBIGA: Calculate the variable WBIGA by the
*         formula:-
*                        i      Nx
*         WBIGA = 1 - --- * -------
*                       1+i    Nx-Nx+1
*
*          Using the same information as EBIGA above, WBIGA would
*         be:
*             .04      132001
*         1 - ---- * -------------
*             1.04   132001-125015
*                                  =  0.27327
*
* (iii)   Function EADUE: Calculate variable EADUE by formula:
*
*                 Nx-Nx+n
*         EADUE = -------, using final age(2) for x+n
*                 Nx-Nx+1
*
*         again using same example:
*
*                 132001-35841
*         EADUE = ------------ = 13.765
*                 132001-125015
*
* (iv)    Function WADUE: Calculate variable WADUE by formula:
*         if final age (2) not defined:
*                  Nx
*        WADUE = ------
*                Nx-Nx+1
*
*         else use same formula as EADUE using final age (2).
*         Again using same example:
*
*                   132001
*        WADUE = ------------- = 18.895
*                132001-125015
*
* (v)     Function ENETP: Calculate variable ENETP by formula:
*
*                 EBIGA
*         ENETP = -----
*                 EADUE
*
* (vi)    Function WNETP: Calculate variable WNETP by formula:-
*
*                 WBIGA
*         WNETP = -----
*                 WADUE
*
*                                            (Nx+n-Nx+n+1)
* (vii)   Function TBIGA: Calculate EBIGA -  -------------
*                                             (Nx-Nx+1)
*         x+n is final age (1)
*
* (viii)  Function TADUE: Calculate identically to EADUE
*
*                                       TBIGA
* (ix)    Function TNETP:  Calculate as -----
*                                       TADUE
*
*
*   The Linkage ( ACTCALCREC ) will pass the following .....
*
*        Function
*        Basis  ( Table item key into T6641 )
*        Initial Age
*        Final Age (1)
*        Final Age (2)
*        Company
*
*    And, return the following .....
*
*        Statuz
*        Value
*
*****************************************************************
* </pre>
*/
public class Actcalc extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "ACTCALC";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaValueFromT6641 = new FixedLengthStringData(45);
	private PackedDecimalData wsaaZeroDp = new PackedDecimalData(8, 0).isAPartOf(wsaaValueFromT6641, 0);
	private PackedDecimalData wsaaOneDp = new PackedDecimalData(8, 1).isAPartOf(wsaaValueFromT6641, 5);
	private PackedDecimalData wsaaTwoDp = new PackedDecimalData(8, 2).isAPartOf(wsaaValueFromT6641, 10);
	private PackedDecimalData wsaaThreeDp = new PackedDecimalData(8, 3).isAPartOf(wsaaValueFromT6641, 15);
	private PackedDecimalData wsaaFourDp = new PackedDecimalData(8, 4).isAPartOf(wsaaValueFromT6641, 20);
	private PackedDecimalData wsaaFiveDp = new PackedDecimalData(8, 5).isAPartOf(wsaaValueFromT6641, 25);
	private PackedDecimalData wsaaSixDp = new PackedDecimalData(8, 6).isAPartOf(wsaaValueFromT6641, 30);
	private PackedDecimalData wsaaSevenDp = new PackedDecimalData(8, 7).isAPartOf(wsaaValueFromT6641, 35);
	private PackedDecimalData wsaaEightDp = new PackedDecimalData(9, 8).isAPartOf(wsaaValueFromT6641, 40);
		/* WSAA-AGE-LIMITS */
	private ZonedDecimalData wsaaMinimumAge = new ZonedDecimalData(3, 0).setUnsigned();
	private int wsaaMaximumAge = 0;

	private FixedLengthStringData wsaaSplitBasis = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPassedBasis = new FixedLengthStringData(8).isAPartOf(wsaaSplitBasis, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaPassedBasis, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaPassedPercent = new ZonedDecimalData(3, 0).isAPartOf(filler, 5).setUnsigned();
	private ZonedDecimalData wsaaHoldFinalAge1 = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaHoldFinalAge2 = new ZonedDecimalData(3, 0).setUnsigned();
		/* ERRORS */
	private static final String e530 = "E530";
	private static final String g575 = "G575";
	private static final String g576 = "G576";
	private static final String g578 = "G578";
		/* TABLES */
	private static final String t6641 = "T6641";
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T6641rec t6641rec = new T6641rec();
	private Actcalcrec actcalcrec = new Actcalcrec();
	private WsaaAgeRowPointerInner wsaaAgeRowPointerInner = new WsaaAgeRowPointerInner();
	private WsaaInternalsInner wsaaInternalsInner = new WsaaInternalsInner();

	public Actcalc() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		actcalcrec.actcalcRec = convertAndSetParam(actcalcrec.actcalcRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		main110();
		exit190();
	}

protected void main110()
	{
		initialise300();
		checkPassedValues400();
		/*  If either FINAL-AGE-1 or FINAL-AGE-2 exceeds the MAXIMUM-AGE   */
		/*  set it to the MAXIMUM-AGE. Store the original FINAL-AGES       */
		/*  first.                                                         */
		if (isGT(actcalcrec.finalAge1, wsaaMaximumAge)) {
			wsaaHoldFinalAge1.set(actcalcrec.finalAge1);
			actcalcrec.finalAge1.set(wsaaMaximumAge);
		}
		if (isGT(actcalcrec.finalAge2, wsaaMaximumAge)) {
			wsaaHoldFinalAge2.set(actcalcrec.finalAge2);
			actcalcrec.finalAge2.set(wsaaMaximumAge);
		}
		/* Read T6641 to get all data required*/
		readTableT6641600();
		/* Decide which function we are going to process and*/
		/*  act accordingly.*/
		if ((isEQ(actcalcrec.function, "EBIGA"))) {
			calculateEbiga1000();
		}
		else {
			if ((isEQ(actcalcrec.function, "WBIGA"))) {
				calculateWbiga2000();
			}
			else {
				if ((isEQ(actcalcrec.function, "EADUE"))) {
					calculateEadue3000();
				}
				else {
					if ((isEQ(actcalcrec.function, "WADUE"))) {
						calculateWadue4000();
					}
					else {
						if ((isEQ(actcalcrec.function, "ENETP"))) {
							calculateEnetp5000();
						}
						else {
							if ((isEQ(actcalcrec.function, "WNETP"))) {
								calculateWnetp6000();
							}
							else {
								if ((isEQ(actcalcrec.function, "TBIGA"))) {
									calculateTbiga7000();
								}
								else {
									if ((isEQ(actcalcrec.function, "TADUE"))) {
										calculateTadue8000();
									}
									else {
										if ((isEQ(actcalcrec.function, "TNETP"))) {
											calculateTnetp9000();
										}
										else {
											syserrrec.params.set(actcalcrec.function);
											syserrrec.statuz.set(g578);
											systemError99000();
											return ;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		actcalcrec.value.set(wsaaInternalsInner.wsaaCalcValue);
		/*  Move back the original stored FINAL-AGES where necessary.      */
		if (isGT(wsaaHoldFinalAge1, 0)) {
			actcalcrec.finalAge1.set(wsaaHoldFinalAge1);
		}
		if (isGT(wsaaHoldFinalAge2, 0)) {
			actcalcrec.finalAge2.set(wsaaHoldFinalAge2);
		}
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialise300()
	{
		/*START*/
		syserrrec.subrname.set(wsaaSubr);
		actcalcrec.statuz.set(varcom.oK);
		wsaaValueFromT6641.set(ZERO);
		wsaaInternalsInner.wsaaInternals.set(ZERO);
		wsaaSplitBasis.set(ZERO);
		/*    MOVE 011                TO WSAA-MINIMUM-AGE.                 */
		/*    MOVE 109                TO WSAA-MAXIMUM-AGE.                 */
		wsaaMinimumAge.set(0);
		/*    MOVE 99                 TO WSAA-MAXIMUM-AGE.         <V73L03>*/
		wsaaMaximumAge = 110;
		wsaaHoldFinalAge1.set(0);
		wsaaHoldFinalAge2.set(0);
		/*EXIT*/
	}

protected void checkPassedValues400()
	{
		start400();
	}

protected void start400()
	{
		if ((isEQ(actcalcrec.basis, SPACES)
		|| isEQ(actcalcrec.basis, ZERO))) {
			syserrrec.statuz.set(g576);
			systemError99000();
		}
		/* IF ( ACTC-INITIAL-AGE       = ZEROS )                        */
		/*     MOVE G577               TO SYSR-STATUZ                   */
		/*     PERFORM 99000-SYSTEM-ERROR.                              */
		if ((isEQ(actcalcrec.finalAge1, ZERO)
		&& isEQ(actcalcrec.finalAge2, ZERO))) {
			if ((isEQ(actcalcrec.function, "EBIGA")
			|| isEQ(actcalcrec.function, "EADUE")
			|| isEQ(actcalcrec.function, "ENETP")
			|| isEQ(actcalcrec.function, "TBIGA")
			|| isEQ(actcalcrec.function, "TADUE")
			|| isEQ(actcalcrec.function, "TNETP"))) {
				syserrrec.statuz.set(g575);
				systemError99000();
			}
		}
		if ((isLT(actcalcrec.initialAge, wsaaMinimumAge)
		|| isGT(actcalcrec.initialAge, wsaaMaximumAge))) {
			syserrrec.statuz.set(e530);
			systemError99000();
		}
		if ((isEQ(actcalcrec.function, "EBIGA")
		|| isEQ(actcalcrec.function, "ENETP")
		|| isEQ(actcalcrec.function, "TBIGA")
		|| isEQ(actcalcrec.function, "TNETP"))) {
			/*        IF ( ACTC-FINAL-AGE-1 < WSAA-MINIMUM-AGE OR              */
			/*             ACTC-FINAL-AGE-1 > WSAA-MAXIMUM-AGE )               */
			if (isLT(actcalcrec.finalAge1, wsaaMinimumAge)) {
				syserrrec.statuz.set(e530);
				systemError99000();
			}
		}
		if ((isEQ(actcalcrec.function, "EADUE")
		|| isEQ(actcalcrec.function, "ENETP")
		|| isEQ(actcalcrec.function, "TADUE")
		|| isEQ(actcalcrec.function, "TNETP"))) {
			/*        IF ( ACTC-FINAL-AGE-2 < WSAA-MINIMUM-AGE OR              */
			/*             ACTC-FINAL-AGE-2 > WSAA-MAXIMUM-AGE )               */
			if (isLT(actcalcrec.finalAge2, wsaaMinimumAge)) {
				syserrrec.statuz.set(e530);
				systemError99000();
			}
		}
		if ((isEQ(actcalcrec.function, "WADUE")
		|| isEQ(actcalcrec.function, "WNETP"))) {
			/*        IF ( ACTC-FINAL-AGE-2 < WSAA-MINIMUM-AGE  OR             */
			/*             ACTC-FINAL-AGE-2 > WSAA-MAXIMUM-AGE )  AND          */
			/*    IF ( ACTC-FINAL-AGE-2 < WSAA-MINIMUM-AGE )  OR      <001>*/
			/* ( ACTC-FINAL-AGE-2 > ZERO )                           */
			/*       ( ACTC-FINAL-AGE-1 < WSAA-MAXIMUM-AGE )          <002>*/
			if (isLT(actcalcrec.finalAge2, wsaaMinimumAge)) {
				syserrrec.statuz.set(e530);
				systemError99000();
			}
		}
	}

protected void readTableT6641600()
	{
		start600();
	}

protected void start600()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItemcoy(actcalcrec.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t6641);
		itemIO.setItemitem(actcalcrec.basis);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		t6641rec.t6641Rec.set(itemIO.getGenarea());
	}

protected void calculateEbiga1000()
	{
		start1000();
	}

protected void start1000()
	{
		/* EBIGA calculation is as follows...*/
		/*                     i        N(x)  - N(x+n)*/
		/*     EBIGA =  1 -  -----  *  ----------------*/
		/*                   1 + i      N(x)  - N(x+1)*/
		/*  where.....*/
		/*   i is the last 3 characters of ACTC-BASIS divided by 10,000*/
		/*   x     is ACTC-INITIAL-AGE,*/
		/*   x + n is ACTC-FINAL-AGE-1,*/
		/*   N(x)  is a data item on the extra data screen of the Table*/
		/*         item ( ACTC-BASIS ) in table T6641 for age x*/
		wsaaPassedBasis.set(actcalcrec.basis);
		compute(wsaaInternalsInner.wsaaVal1, 0).set(div(wsaaPassedPercent, 10000));
		compute(wsaaInternalsInner.wsaaVal2, 0).set(add(1, wsaaInternalsInner.wsaaVal1));
		compute(wsaaInternalsInner.wsaaResult1, 0).set(div(wsaaInternalsInner.wsaaVal1, wsaaInternalsInner.wsaaVal2));
		wsaaInternalsInner.wsaaVal1.set(ZERO);
		wsaaInternalsInner.wsaaVal2.set(ZERO);
		/* Now start getting T6641 values.......*/
		/* Find T6641 value for initial age*/
		wsaaInternalsInner.wsaaFindAge.set(actcalcrec.initialAge);
		findAgeValue10000();
		/* Store initial age T6641 value for later use*/
		wsaaInternalsInner.wsaaInitAgeVal.set(wsaaInternalsInner.wsaaReadValue);
		/* Find T6641 value for ( initial age + 1 )*/
		compute(wsaaInternalsInner.wsaaFindAge, 0).set(add(1, actcalcrec.initialAge));
		findAgeValue10000();
		wsaaInternalsInner.wsaaAgeVal.set(wsaaInternalsInner.wsaaReadValue);
		/* Find T6641 value for final age (1)*/
		wsaaInternalsInner.wsaaFindAge.set(actcalcrec.finalAge1);
		findAgeValue10000();
		wsaaInternalsInner.wsaaFinAge1Val.set(wsaaInternalsInner.wsaaReadValue);
		compute(wsaaInternalsInner.wsaaVal1, 0).set(sub(wsaaInternalsInner.wsaaInitAgeVal, wsaaInternalsInner.wsaaFinAge1Val));
		compute(wsaaInternalsInner.wsaaVal2, 0).set(sub(wsaaInternalsInner.wsaaInitAgeVal, wsaaInternalsInner.wsaaAgeVal));
		compute(wsaaInternalsInner.wsaaResult2, 0).set(div(wsaaInternalsInner.wsaaVal1, wsaaInternalsInner.wsaaVal2));
		compute(wsaaInternalsInner.wsaaFinResult, 0).set(mult(wsaaInternalsInner.wsaaResult1, wsaaInternalsInner.wsaaResult2));
		compute(wsaaInternalsInner.wsaaCalcValue, 1).setRounded(sub(1, wsaaInternalsInner.wsaaFinResult));
	}

protected void calculateWbiga2000()
	{
		start2000();
	}

protected void start2000()
	{
		/* WBIGA calculation is as follows...*/
		/*                     i             N(x)*/
		/*     WBIGA =  1 -  -----  *  ---------------*/
		/*                   1 + i      N(x)  - N(x+1)*/
		/*  where.....*/
		/*   i is the last 3 characters of ACTC-BASIS divided by 10,000*/
		/*   x     is ACTC-INITIAL-AGE,*/
		/*   N(x)  is a data item on the extra data screen of the Table*/
		/*         item ( ACTC-BASIS ) in table T6641 for age x*/
		wsaaPassedBasis.set(actcalcrec.basis);
		compute(wsaaInternalsInner.wsaaVal1, 0).set(div(wsaaPassedPercent, 10000));
		compute(wsaaInternalsInner.wsaaVal2, 0).set(add(1, wsaaInternalsInner.wsaaVal1));
		compute(wsaaInternalsInner.wsaaResult1, 0).set(div(wsaaInternalsInner.wsaaVal1, wsaaInternalsInner.wsaaVal2));
		wsaaInternalsInner.wsaaVal1.set(ZERO);
		wsaaInternalsInner.wsaaVal2.set(ZERO);
		/* Now start getting T6641 values.....*/
		/* Find T6641 value for initial age*/
		wsaaInternalsInner.wsaaFindAge.set(actcalcrec.initialAge);
		findAgeValue10000();
		/* Store initial age T6641 value for later use*/
		wsaaInternalsInner.wsaaInitAgeVal.set(wsaaInternalsInner.wsaaReadValue);
		/* Find T6641 value for ( initial age + 1 )*/
		compute(wsaaInternalsInner.wsaaFindAge, 0).set(add(1, actcalcrec.initialAge));
		findAgeValue10000();
		wsaaInternalsInner.wsaaAgeVal.set(wsaaInternalsInner.wsaaReadValue);
		compute(wsaaInternalsInner.wsaaVal2, 0).set(sub(wsaaInternalsInner.wsaaInitAgeVal, wsaaInternalsInner.wsaaAgeVal));
		compute(wsaaInternalsInner.wsaaResult2, 0).set(div(wsaaInternalsInner.wsaaInitAgeVal, wsaaInternalsInner.wsaaVal2));
		compute(wsaaInternalsInner.wsaaFinResult, 0).set(mult(wsaaInternalsInner.wsaaResult1, wsaaInternalsInner.wsaaResult2));
		compute(wsaaInternalsInner.wsaaCalcValue, 1).setRounded(sub(1, wsaaInternalsInner.wsaaFinResult));
	}

protected void calculateEadue3000()
	{
		start3000();
	}

protected void start3000()
	{
		/* EADUE calculation is as follows...*/
		/*              N(x)  - N(x+n)*/
		/*     EADUE =  --------------*/
		/*              N(x)  - N(x+1)*/
		/*  where.....*/
		/*   x     is ACTC-INITIAL-AGE,*/
		/*   x + n is ACTC-FINAL-AGE-2,*/
		/*   N(x)  is a data item on the extra data screen of the Table*/
		/*         item ( ACTC-BASIS ) in table T6641 for age x*/
		/* Find T6641 value for initial age*/
		wsaaInternalsInner.wsaaFindAge.set(actcalcrec.initialAge);
		findAgeValue10000();
		/* Store initial age T6641 value for later use*/
		wsaaInternalsInner.wsaaInitAgeVal.set(wsaaInternalsInner.wsaaReadValue);
		/* Find T6641 value for ( initial age + 1 )*/
		compute(wsaaInternalsInner.wsaaFindAge, 0).set(add(1, actcalcrec.initialAge));
		findAgeValue10000();
		wsaaInternalsInner.wsaaAgeVal.set(wsaaInternalsInner.wsaaReadValue);
		/* Find T6641 value for final age (2)*/
		wsaaInternalsInner.wsaaFindAge.set(actcalcrec.finalAge2);
		findAgeValue10000();
		wsaaInternalsInner.wsaaFinAge2Val.set(wsaaInternalsInner.wsaaReadValue);
		compute(wsaaInternalsInner.wsaaVal1, 0).set(sub(wsaaInternalsInner.wsaaInitAgeVal, wsaaInternalsInner.wsaaFinAge2Val));
		compute(wsaaInternalsInner.wsaaVal2, 0).set(sub(wsaaInternalsInner.wsaaInitAgeVal, wsaaInternalsInner.wsaaAgeVal));
		compute(wsaaInternalsInner.wsaaCalcValue, 1).setRounded(div(wsaaInternalsInner.wsaaVal1, wsaaInternalsInner.wsaaVal2));
	}

protected void calculateWadue4000()
	{
		start4000();
	}

protected void start4000()
	{
		/* IF ACTC-FINAL-AGE-2 is passed and is NON-ZERO, then*/
		/*  calculate WADUE by using the EADUE formula as in*/
		/*  the section 3000-CALCULATE-EADUE.*/
		/* IF ACTC-FINAL-AGE-2 is passed as 000 then calculate*/
		/*  WADUE as follows...*/
		/*                   N(x)*/
		/*     WADUE =  --------------*/
		/*              N(x)  - N(x+1)*/
		/*  where.....*/
		/*   x     is ACTC-INITIAL-AGE,*/
		/*   N(x)  is a data item on the extra data screen of the Table*/
		/*         item ( ACTC-BASIS ) in table T6641 for age x*/
		if ((isNE(actcalcrec.finalAge2, ZERO))) {
			calculateEadue3000();
			return ;
		}
		/* Find T6641 value for initial age*/
		wsaaInternalsInner.wsaaFindAge.set(actcalcrec.initialAge);
		findAgeValue10000();
		/* Store initial age T6641 value for later use*/
		wsaaInternalsInner.wsaaInitAgeVal.set(wsaaInternalsInner.wsaaReadValue);
		/* Find T6641 value for ( initial age + 1 )*/
		compute(wsaaInternalsInner.wsaaFindAge, 0).set(add(1, actcalcrec.initialAge));
		findAgeValue10000();
		wsaaInternalsInner.wsaaAgeVal.set(wsaaInternalsInner.wsaaReadValue);
		compute(wsaaInternalsInner.wsaaVal2, 0).set(sub(wsaaInternalsInner.wsaaInitAgeVal, wsaaInternalsInner.wsaaAgeVal));
		compute(wsaaInternalsInner.wsaaCalcValue, 1).setRounded(div(wsaaInternalsInner.wsaaInitAgeVal, wsaaInternalsInner.wsaaVal2));
	}

protected void calculateEnetp5000()
	{
		/*START*/
		/* ENETP calculation is as follows...*/
		/*              EBIGA*/
		/*     ENETP =  -----*/
		/*              EADUE*/
		calculateEbiga1000();
		wsaaInternalsInner.wsaaEnetpVal1.set(wsaaInternalsInner.wsaaCalcValue);
		calculateEadue3000();
		wsaaInternalsInner.wsaaEnetpVal2.set(wsaaInternalsInner.wsaaCalcValue);
		compute(wsaaInternalsInner.wsaaCalcValue, 0).set(div(wsaaInternalsInner.wsaaEnetpVal1, wsaaInternalsInner.wsaaEnetpVal2));
		/*EXIT*/
	}

protected void calculateWnetp6000()
	{
		/*START*/
		/* WNETP calculation is as follows...*/
		/*              WBIGA*/
		/*     WNETP =  -----*/
		/*              WADUE*/
		calculateWbiga2000();
		wsaaInternalsInner.wsaaWnetpVal1.set(wsaaInternalsInner.wsaaCalcValue);
		calculateWadue4000();
		wsaaInternalsInner.wsaaWnetpVal2.set(wsaaInternalsInner.wsaaCalcValue);
		compute(wsaaInternalsInner.wsaaCalcValue, 0).set(div(wsaaInternalsInner.wsaaWnetpVal1, wsaaInternalsInner.wsaaWnetpVal2));
		/*EXIT*/
	}

protected void calculateTbiga7000()
	{
		start7000();
	}

protected void start7000()
	{
		/* TBIGA calculation is as follows...*/
		/*                        N(x+n)  -  N(x+n+1)*/
		/*     TBIGA =  EBIGA -  --------------------*/
		/*                          N(x)  -  N(x+1)*/
		/*   x     is ACTC-INITIAL-AGE,*/
		/*   x + n is ACTC-FINAL-AGE-1,*/
		/*   N(x)  is a data item on the extra data screen of the Table*/
		/*         item ( ACTC-BASIS ) in table T6641 for age x*/
		calculateEbiga1000();
		wsaaInternalsInner.wsaaTbigaVal1.set(wsaaInternalsInner.wsaaCalcValue);
		wsaaInternalsInner.wsaaCalcValue.set(ZERO);
		wsaaInternalsInner.wsaaVal1.set(ZERO);
		wsaaInternalsInner.wsaaVal2.set(ZERO);
		wsaaInternalsInner.wsaaResult1.set(ZERO);
		/* Now, for the 2nd part of our calculation, we need to find*/
		/*  out N(x), N(x+1), N(x+n) and N(x+n+1).*/
		/* BUT, from calling the 1000-CALCULATE-EBIGA section, we*/
		/*  already know what the values of N(x), N(x+1) and N(x+n) are...*/
		/* N(x)   is WSAA-INIT-AGE-VAL*/
		/* N(x+1) is WSAA-AGE-VAL*/
		/* N(x+n) is WSAA-FIN-AGE-VAL-1*/
		/*  so we only need to calculate the value N(x+n+1) .....*/
		compute(wsaaInternalsInner.wsaaFindAge, 0).set(add(1, actcalcrec.finalAge1));
		findAgeValue10000();
		wsaaInternalsInner.wsaaTbigaVal2.set(wsaaInternalsInner.wsaaReadValue);
		compute(wsaaInternalsInner.wsaaVal2, 0).set(sub(wsaaInternalsInner.wsaaInitAgeVal, wsaaInternalsInner.wsaaAgeVal));
		compute(wsaaInternalsInner.wsaaVal1, 0).set(sub(wsaaInternalsInner.wsaaFinAge1Val, wsaaInternalsInner.wsaaTbigaVal2));
		compute(wsaaInternalsInner.wsaaResult1, 1).setRounded(div(wsaaInternalsInner.wsaaVal1, wsaaInternalsInner.wsaaVal2));
		compute(wsaaInternalsInner.wsaaCalcValue, 0).set(sub(wsaaInternalsInner.wsaaTbigaVal1, wsaaInternalsInner.wsaaResult1));
	}

protected void calculateTadue8000()
	{
		/*START*/
		/* TADUE value is calculated exactly the same as EADUE*/
		calculateEadue3000();
		/*EXIT*/
	}

protected void calculateTnetp9000()
	{
		/*START*/
		/* TNETP calculation is as follows...*/
		/*              TBIGA*/
		/*     TNETP =  -----*/
		/*              TADUE*/
		calculateTbiga7000();
		wsaaInternalsInner.wsaaTnetpVal1.set(wsaaInternalsInner.wsaaCalcValue);
		wsaaInternalsInner.wsaaVal1.set(ZERO);
		wsaaInternalsInner.wsaaVal2.set(ZERO);
		wsaaInternalsInner.wsaaAgeVal.set(ZERO);
		wsaaInternalsInner.wsaaInitAgeVal.set(ZERO);
		wsaaInternalsInner.wsaaFinAge2Val.set(ZERO);
		calculateTadue8000();
		wsaaInternalsInner.wsaaTnetpVal2.set(wsaaInternalsInner.wsaaCalcValue);
		compute(wsaaInternalsInner.wsaaCalcValue, 0).set(div(wsaaInternalsInner.wsaaTnetpVal1, wsaaInternalsInner.wsaaTnetpVal2));
		/*EXIT*/
	}

protected void findAgeValue10000()
	{
		start10000();
	}

protected void start10000()
	{
		/* Get Age value from record*/
		/* Table T6641 has 100 fields, the age ranges from 11 to 109...*/
		/*  the data is stored on T6641 in a record which has a field*/
		/*  T6641-NXMONEY to hold the value for age 11, AND an array*/
		/*  T6641-NXMON occurs 99 which holds the values for ages 12 to*/
		/*  109.*/
		/*  so, field number  1  corresponds to age 11,*/
		/*      array element 1  --------"--------- 12,*/
		/*      ------"------ 10 --------"--------- 21,*/
		/*      ------"------ 66 --------"--------- 77,*/
		/*      ------"------ 99 --------"--------- 109*/
		/* Note : The above is no longer true. (LFA1117)*/
		/* Table T6641 has 100 fields, the age ranges from 0  to 98 ...*/
		/*  the data is stored on T6641 in a record which has a field*/
		/*  T6641-NXMONEY to hold the value for age 0 , AND an array*/
		/*  T6641-NXMON occurs 99 which holds the values for ages 1  to*/
		/*  98 .*/
		/*  so, field number  1  corresponds to age 0 ,*/
		/*      array element 1  --------"--------- 1 ,*/
		/*      ------"------ 10 --------"--------- 10,*/
		/*      ------"------ 66 --------"--------- 66,*/
		/*      ------"------ 98 --------"--------- 98*/
		wsaaInternalsInner.wsaaT6641RawVal.set(ZERO);
		/* calculate index into the T6641 field array to pick the T6641*/
		/*  value corresponding to the initial age passed to us...*/
		if ((isLT(wsaaInternalsInner.wsaaFindAge, wsaaMinimumAge)
		|| isGT(wsaaInternalsInner.wsaaFindAge, wsaaMaximumAge))) {
			wsaaInternalsInner.wsaaReadValue.set(ZERO);
			return ;
		}
		if ((isEQ(wsaaInternalsInner.wsaaFindAge, wsaaMinimumAge))) {
			wsaaInternalsInner.wsaaT6641RawVal.set(t6641rec.nxmoney);
		}
		else {
			if ((isGT(wsaaInternalsInner.wsaaFindAge, 99))) {
				compute(wsaaInternalsInner.wsaaIndex, 0).set(sub(wsaaInternalsInner.wsaaFindAge, wsaaMinimumAge));
				compute(wsaaInternalsInner.wsaaIndex, 0).set(sub(wsaaInternalsInner.wsaaIndex, 99));
				wsaaInternalsInner.wsaaT6641RawVal.set(t6641rec.tnxmon[wsaaInternalsInner.wsaaIndex.toInt()]);
			}
			else {
				compute(wsaaInternalsInner.wsaaIndex, 0).set(sub(wsaaInternalsInner.wsaaFindAge, wsaaMinimumAge));
				wsaaInternalsInner.wsaaT6641RawVal.set(t6641rec.nxmon[wsaaInternalsInner.wsaaIndex.toInt()]);
			}
		}
		/* Find out what row the age value required is on*/
		wsaaAgeRowPointerInner.wsaaAgeRowPointer.set(wsaaInternalsInner.wsaaFindAge);
		if (wsaaAgeRowPointerInner.wsaaAgeOnRow1.isTrue()) {
			wsaaInternalsInner.wsaaAgeRow.set(1);
		}
		else {
			if (wsaaAgeRowPointerInner.wsaaAgeOnRow2.isTrue()) {
				wsaaInternalsInner.wsaaAgeRow.set(2);
			}
			else {
				if (wsaaAgeRowPointerInner.wsaaAgeOnRow3.isTrue()) {
					wsaaInternalsInner.wsaaAgeRow.set(3);
				}
				else {
					if (wsaaAgeRowPointerInner.wsaaAgeOnRow4.isTrue()) {
						wsaaInternalsInner.wsaaAgeRow.set(4);
					}
					else {
						if (wsaaAgeRowPointerInner.wsaaAgeOnRow5.isTrue()) {
							wsaaInternalsInner.wsaaAgeRow.set(5);
						}
						else {
							if (wsaaAgeRowPointerInner.wsaaAgeOnRow6.isTrue()) {
								wsaaInternalsInner.wsaaAgeRow.set(6);
							}
							else {
								if (wsaaAgeRowPointerInner.wsaaAgeOnRow7.isTrue()) {
									wsaaInternalsInner.wsaaAgeRow.set(7);
								}
								else {
									if (wsaaAgeRowPointerInner.wsaaAgeOnRow8.isTrue()) {
										wsaaInternalsInner.wsaaAgeRow.set(8);
									}
									else {
										if (wsaaAgeRowPointerInner.wsaaAgeOnRow9.isTrue()) {
											wsaaInternalsInner.wsaaAgeRow.set(9);
										}
										else {
											if (wsaaAgeRowPointerInner.wsaaAgeOnRow10.isTrue()) {
												wsaaInternalsInner.wsaaAgeRow.set(10);
											}
											else {
												if (wsaaAgeRowPointerInner.wsaaAgeOnRow11.isTrue()) {
													wsaaInternalsInner.wsaaAgeRow.set(11);
												}
												else {
													if (wsaaAgeRowPointerInner.wsaaAgeOnRow12.isTrue()) {
														wsaaInternalsInner.wsaaAgeRow.set(12);
													}
													else {
														if (wsaaAgeRowPointerInner.wsaaAgeOnRow13.isTrue()) {
															wsaaInternalsInner.wsaaAgeRow.set(13);
														}
														else {
															if (wsaaAgeRowPointerInner.wsaaAgeOnRow14.isTrue()) {
																wsaaInternalsInner.wsaaAgeRow.set(14);
															}
															else {
																if (wsaaAgeRowPointerInner.wsaaAgeOnRow15.isTrue()) {
																	wsaaInternalsInner.wsaaAgeRow.set(15);
																}
																else {
																	if (wsaaAgeRowPointerInner.wsaaAgeOnRow16.isTrue()) {
																		wsaaInternalsInner.wsaaAgeRow.set(16);
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		wsaaInternalsInner.wsaaReadValue.set(ZERO);
		/* Depending on the number of decimal places ( from T6641 )*/
		/* get T6641 value into working storage.*/
		if ((isEQ(t6641rec.nxdp[wsaaInternalsInner.wsaaAgeRow.toInt()], ZERO))) {
			wsaaZeroDp.set(wsaaInternalsInner.wsaaT6641RawVal);
			wsaaInternalsInner.wsaaReadValue.set(wsaaZeroDp);
		}
		else {
			if ((isEQ(t6641rec.nxdp[wsaaInternalsInner.wsaaAgeRow.toInt()], 1))) {
				compute(wsaaOneDp, 1).set(div(wsaaInternalsInner.wsaaT6641RawVal, 10));
				wsaaInternalsInner.wsaaReadValue.set(wsaaOneDp);
			}
			else {
				if ((isEQ(t6641rec.nxdp[wsaaInternalsInner.wsaaAgeRow.toInt()], 2))) {
					compute(wsaaTwoDp, 2).set(div(wsaaInternalsInner.wsaaT6641RawVal, 100));
					wsaaInternalsInner.wsaaReadValue.set(wsaaTwoDp);
				}
				else {
					if ((isEQ(t6641rec.nxdp[wsaaInternalsInner.wsaaAgeRow.toInt()], 3))) {
						compute(wsaaThreeDp, 3).set(div(wsaaInternalsInner.wsaaT6641RawVal, 1000));
						wsaaInternalsInner.wsaaReadValue.set(wsaaThreeDp);
					}
					else {
						if ((isEQ(t6641rec.nxdp[wsaaInternalsInner.wsaaAgeRow.toInt()], 4))) {
							compute(wsaaFourDp, 4).set(div(wsaaInternalsInner.wsaaT6641RawVal, 10000));
							wsaaInternalsInner.wsaaReadValue.set(wsaaFourDp);
						}
						else {
							if ((isEQ(t6641rec.nxdp[wsaaInternalsInner.wsaaAgeRow.toInt()], 5))) {
								compute(wsaaFiveDp, 5).set(div(wsaaInternalsInner.wsaaT6641RawVal, 100000));
								wsaaInternalsInner.wsaaReadValue.set(wsaaFiveDp);
							}
							else {
								if ((isEQ(t6641rec.nxdp[wsaaInternalsInner.wsaaAgeRow.toInt()], 6))) {
									compute(wsaaSixDp, 6).set(div(wsaaInternalsInner.wsaaT6641RawVal, 1000000));
									wsaaInternalsInner.wsaaReadValue.set(wsaaSixDp);
								}
								else {
									if ((isEQ(t6641rec.nxdp[wsaaInternalsInner.wsaaAgeRow.toInt()], 7))) {
										compute(wsaaSevenDp, 7).set(div(wsaaInternalsInner.wsaaT6641RawVal, 10000000));
										wsaaInternalsInner.wsaaReadValue.set(wsaaSevenDp);
									}
									else {
										if ((isEQ(t6641rec.nxdp[wsaaInternalsInner.wsaaAgeRow.toInt()], 8))) {
											compute(wsaaEightDp, 8).set(div(wsaaInternalsInner.wsaaT6641RawVal, 100000000));
											wsaaInternalsInner.wsaaReadValue.set(wsaaEightDp);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

protected void systemError99000()
	{
		start99000();
		exit99490();
	}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		actcalcrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError99500()
	{
		start99500();
		exit99590();
	}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99590()
	{
		actcalcrec.statuz.set(varcom.bomb);
		exit190();
	}
/*
 * Class transformed  from Data Structure WSAA-INTERNALS--INNER
 */
private static final class WsaaInternalsInner { 

	private FixedLengthStringData wsaaInternals = new FixedLengthStringData(203);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(3, 0).isAPartOf(wsaaInternals, 0).setUnsigned();
	private PackedDecimalData wsaaReadValue = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 3);
	private PackedDecimalData wsaaT6641RawVal = new PackedDecimalData(8, 0).isAPartOf(wsaaInternals, 13);
	private PackedDecimalData wsaaInitAgeVal = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 18);
	private PackedDecimalData wsaaAgeVal = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 28);
	private PackedDecimalData wsaaFinAge1Val = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 38);
	private PackedDecimalData wsaaFinAge2Val = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 48);
	private PackedDecimalData wsaaVal1 = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 58);
	private PackedDecimalData wsaaVal2 = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 68);
	private PackedDecimalData wsaaResult1 = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 78);
	private PackedDecimalData wsaaResult2 = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 88);
	private PackedDecimalData wsaaFinResult = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 98);
	private PackedDecimalData wsaaCalcValue = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 108);
	private PackedDecimalData wsaaEnetpVal1 = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 118);
	private PackedDecimalData wsaaEnetpVal2 = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 128);
	private PackedDecimalData wsaaWnetpVal1 = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 138);
	private PackedDecimalData wsaaWnetpVal2 = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 148);
	private PackedDecimalData wsaaTbigaVal1 = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 158);
	private PackedDecimalData wsaaTbigaVal2 = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 168);
	private PackedDecimalData wsaaTnetpVal1 = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 178);
	private PackedDecimalData wsaaTnetpVal2 = new PackedDecimalData(18, 9).isAPartOf(wsaaInternals, 188);
	private ZonedDecimalData wsaaAgeRow = new ZonedDecimalData(2, 0).isAPartOf(wsaaInternals, 198).setUnsigned();
	private ZonedDecimalData wsaaFindAge = new ZonedDecimalData(3, 0).isAPartOf(wsaaInternals, 200).setUnsigned();
}
/*
 * Class transformed  from Data Structure WSAA-AGE-ROW-POINTER--INNER
 */
private static final class WsaaAgeRowPointerInner { 

	private ZonedDecimalData wsaaAgeRowPointer = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
		/*  88  WSAA-AGE-ON-ROW-1       VALUE 011 012 013 014              
		                                    015 016 017.                 
		  88  WSAA-AGE-ON-ROW-2       VALUE 018 019 020 021              
		                                    022 023 024.                 
		  88  WSAA-AGE-ON-ROW-3       VALUE 025 026 027 028              
		                                    029 030 031.                 
		  88  WSAA-AGE-ON-ROW-4       VALUE 032 033 034 035              
		                                    036 037 038.                 
		  88  WSAA-AGE-ON-ROW-5       VALUE 039 040 041 042              
		                                    043 044 045.                 
		  88  WSAA-AGE-ON-ROW-6       VALUE 046 047 048 049              
		                                    050 051 052.                 
		  88  WSAA-AGE-ON-ROW-7       VALUE 053 054 055 056              
		                                    057 058 059.                 
		  88  WSAA-AGE-ON-ROW-8       VALUE 060 061 062 063              
		                                    064 065 066.                 
		  88  WSAA-AGE-ON-ROW-9       VALUE 067 068 069 070              
		                                    071 072 073.                 
		  88  WSAA-AGE-ON-ROW-10      VALUE 074 075 076 077              
		                                    078 079 080.                 
		  88  WSAA-AGE-ON-ROW-11      VALUE 081 082 083 084              
		                                    085 086 087.                 
		  88  WSAA-AGE-ON-ROW-12      VALUE 088 089 090 091              
		                                    092 093 094.                 
		  88  WSAA-AGE-ON-ROW-13      VALUE 095 096 097 098              
		                                    099 100 101.                 
		  88  WSAA-AGE-ON-ROW-14      VALUE 102 103 104 105              
		                                    106 107 108.                 
		  88  WSAA-AGE-ON-ROW-15      VALUE 109.                         */
	private Validator wsaaAgeOnRow1 = new Validator(wsaaAgeRowPointer, 0,1,2,3,4,5,6);
	private Validator wsaaAgeOnRow2 = new Validator(wsaaAgeRowPointer, 7,8,9,10,11,12,13);
	private Validator wsaaAgeOnRow3 = new Validator(wsaaAgeRowPointer, 14,15,16,17,18,19,20);
	private Validator wsaaAgeOnRow4 = new Validator(wsaaAgeRowPointer, 21,22,23,24,25,26,27);
	private Validator wsaaAgeOnRow5 = new Validator(wsaaAgeRowPointer, 28,29,30,31,32,33,34);
	private Validator wsaaAgeOnRow6 = new Validator(wsaaAgeRowPointer, 35,36,37,38,39,40,41);
	private Validator wsaaAgeOnRow7 = new Validator(wsaaAgeRowPointer, 42,43,44,45,46,47,48);
	private Validator wsaaAgeOnRow8 = new Validator(wsaaAgeRowPointer, 49,50,51,52,53,54,55);
	private Validator wsaaAgeOnRow9 = new Validator(wsaaAgeRowPointer, 56,57,58,59,60,61,62);
	private Validator wsaaAgeOnRow10 = new Validator(wsaaAgeRowPointer, 63,64,65,66,67,68,69);
	private Validator wsaaAgeOnRow11 = new Validator(wsaaAgeRowPointer, 70,71,72,73,74,75,76);
	private Validator wsaaAgeOnRow12 = new Validator(wsaaAgeRowPointer, 77,78,79,80,81,82,83);
	private Validator wsaaAgeOnRow13 = new Validator(wsaaAgeRowPointer, 84,85,86,87,88,89,90);
	private Validator wsaaAgeOnRow14 = new Validator(wsaaAgeRowPointer, 91,92,93,94,95,96,97);
		/*  88  WSAA-AGE-ON-ROW-15      VALUE 098.                  LFA1117*/
	private Validator wsaaAgeOnRow15 = new Validator(wsaaAgeRowPointer, 98,99,100,101,102,103,104);
	private Validator wsaaAgeOnRow16 = new Validator(wsaaAgeRowPointer, 105,106,107,108,109,110);
}
}
