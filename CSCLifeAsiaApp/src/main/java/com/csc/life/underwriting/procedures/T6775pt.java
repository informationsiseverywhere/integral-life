/*
 * File: T6775pt.java
 * Date: 30 August 2009 2:32:55
 * Author: Quipoz Limited
 * 
 * Class transformed from T6775PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.underwriting.tablestructures.T6775rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6775.
*
*
*
***********************************************************************
* </pre>
*/
public class T6775pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(55).isAPartOf(wsaaPrtLine001, 21, FILLER).init("Underwriting Answer Bands                         S6775");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(70);
	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 0, FILLER).init("Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 9);
	private FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 10, FILLER).init(" Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 18);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 23, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 31);
	private FixedLengthStringData filler6 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 40);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(45);
	private FixedLengthStringData filler7 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Valid from:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 15);
	private FixedLengthStringData filler8 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 25, FILLER).init(SPACES);
	private FixedLengthStringData filler9 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 30, FILLER).init("To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 35);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(60);
	private FixedLengthStringData filler10 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(53).isAPartOf(wsaaPrtLine004, 7, FILLER).init("From     To      Rule           From     To      Rule");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(60);
	private FixedLengthStringData filler12 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine005, 7).setPattern("ZZZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine005, 16).setPattern("ZZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 24);
	private FixedLengthStringData filler15 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine005, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine005, 39).setPattern("ZZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine005, 48).setPattern("ZZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 56);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(60);
	private FixedLengthStringData filler18 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine006, 7).setPattern("ZZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine006, 16).setPattern("ZZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 24);
	private FixedLengthStringData filler21 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine006, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine006, 39).setPattern("ZZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine006, 48).setPattern("ZZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 56);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(60);
	private FixedLengthStringData filler24 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine007, 7).setPattern("ZZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine007, 16).setPattern("ZZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 24);
	private FixedLengthStringData filler27 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine007, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine007, 39).setPattern("ZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine007, 48).setPattern("ZZZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 56);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(60);
	private FixedLengthStringData filler30 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine008, 7).setPattern("ZZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine008, 16).setPattern("ZZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 24);
	private FixedLengthStringData filler33 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine008, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine008, 39).setPattern("ZZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine008, 48).setPattern("ZZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 56);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(60);
	private FixedLengthStringData filler36 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine009, 7).setPattern("ZZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine009, 16).setPattern("ZZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 24);
	private FixedLengthStringData filler39 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine009, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine009, 39).setPattern("ZZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine009, 48).setPattern("ZZZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 56);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(60);
	private FixedLengthStringData filler42 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine010, 7).setPattern("ZZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine010, 16).setPattern("ZZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 24);
	private FixedLengthStringData filler45 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine010, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine010, 39).setPattern("ZZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine010, 48).setPattern("ZZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 56);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(60);
	private FixedLengthStringData filler48 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine011, 7).setPattern("ZZZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine011, 16).setPattern("ZZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 24);
	private FixedLengthStringData filler51 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine011, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine011, 39).setPattern("ZZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine011, 48).setPattern("ZZZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 56);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(60);
	private FixedLengthStringData filler54 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine012, 7).setPattern("ZZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine012, 16).setPattern("ZZZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 24);
	private FixedLengthStringData filler57 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine012, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine012, 39).setPattern("ZZZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine012, 48).setPattern("ZZZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo054 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 56);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(60);
	private FixedLengthStringData filler60 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine013, 7).setPattern("ZZZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine013, 16).setPattern("ZZZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 24);
	private FixedLengthStringData filler63 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine013, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine013, 39).setPattern("ZZZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine013, 48).setPattern("ZZZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo060 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 56);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(60);
	private FixedLengthStringData filler66 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine014, 7).setPattern("ZZZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine014, 16).setPattern("ZZZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo063 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 24);
	private FixedLengthStringData filler69 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine014, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine014, 39).setPattern("ZZZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine014, 48).setPattern("ZZZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo066 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 56);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(60);
	private FixedLengthStringData filler72 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine015, 7).setPattern("ZZZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine015, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine015, 16).setPattern("ZZZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo069 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 24);
	private FixedLengthStringData filler75 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine015, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine015, 39).setPattern("ZZZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine015, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine015, 48).setPattern("ZZZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo072 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 56);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(60);
	private FixedLengthStringData filler78 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine016, 7).setPattern("ZZZZ");
	private FixedLengthStringData filler79 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine016, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine016, 16).setPattern("ZZZZ");
	private FixedLengthStringData filler80 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo075 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 24);
	private FixedLengthStringData filler81 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine016, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo076 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine016, 39).setPattern("ZZZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine016, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine016, 48).setPattern("ZZZZ");
	private FixedLengthStringData filler83 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 52, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo078 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 56);

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(40);
	private FixedLengthStringData filler84 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler85 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine017, 9, FILLER).init("Continuation Item :");
	private FixedLengthStringData fieldNo079 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine017, 32);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6775rec t6775rec = new T6775rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6775pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6775rec.t6775Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo009.set(t6775rec.undwrule01);
		fieldNo012.set(t6775rec.undwrule13);
		fieldNo015.set(t6775rec.undwrule02);
		fieldNo021.set(t6775rec.undwrule03);
		fieldNo027.set(t6775rec.undwrule04);
		fieldNo033.set(t6775rec.undwrule05);
		fieldNo039.set(t6775rec.undwrule06);
		fieldNo045.set(t6775rec.undwrule07);
		fieldNo051.set(t6775rec.undwrule08);
		fieldNo057.set(t6775rec.undwrule09);
		fieldNo063.set(t6775rec.undwrule10);
		fieldNo069.set(t6775rec.undwrule11);
		fieldNo075.set(t6775rec.undwrule12);
		fieldNo018.set(t6775rec.undwrule14);
		fieldNo024.set(t6775rec.undwrule15);
		fieldNo030.set(t6775rec.undwrule16);
		fieldNo036.set(t6775rec.undwrule17);
		fieldNo042.set(t6775rec.undwrule18);
		fieldNo048.set(t6775rec.undwrule19);
		fieldNo054.set(t6775rec.undwrule20);
		fieldNo060.set(t6775rec.undwrule21);
		fieldNo066.set(t6775rec.undwrule22);
		fieldNo072.set(t6775rec.undwrule23);
		fieldNo078.set(t6775rec.undwrule24);
		fieldNo079.set(t6775rec.cont);
		fieldNo007.set(t6775rec.intgfrom01);
		fieldNo008.set(t6775rec.intgto01);
		fieldNo013.set(t6775rec.intgfrom02);
		fieldNo014.set(t6775rec.intgto02);
		fieldNo019.set(t6775rec.intgfrom03);
		fieldNo020.set(t6775rec.intgto03);
		fieldNo025.set(t6775rec.intgfrom04);
		fieldNo026.set(t6775rec.intgto04);
		fieldNo032.set(t6775rec.intgto05);
		fieldNo038.set(t6775rec.intgto06);
		fieldNo044.set(t6775rec.intgto07);
		fieldNo050.set(t6775rec.intgto08);
		fieldNo056.set(t6775rec.intgto09);
		fieldNo062.set(t6775rec.intgto10);
		fieldNo068.set(t6775rec.intgto11);
		fieldNo074.set(t6775rec.intgto12);
		fieldNo031.set(t6775rec.intgfrom05);
		fieldNo037.set(t6775rec.intgfrom06);
		fieldNo043.set(t6775rec.intgfrom07);
		fieldNo049.set(t6775rec.intgfrom08);
		fieldNo055.set(t6775rec.intgfrom09);
		fieldNo067.set(t6775rec.intgfrom11);
		fieldNo061.set(t6775rec.intgfrom10);
		fieldNo073.set(t6775rec.intgfrom12);
		fieldNo010.set(t6775rec.intgfrom13);
		fieldNo016.set(t6775rec.intgfrom14);
		fieldNo022.set(t6775rec.intgfrom15);
		fieldNo028.set(t6775rec.intgfrom16);
		fieldNo034.set(t6775rec.intgfrom17);
		fieldNo040.set(t6775rec.intgfrom18);
		fieldNo046.set(t6775rec.intgfrom19);
		fieldNo052.set(t6775rec.intgfrom20);
		fieldNo058.set(t6775rec.intgfrom21);
		fieldNo064.set(t6775rec.intgfrom22);
		fieldNo070.set(t6775rec.intgfrom23);
		fieldNo076.set(t6775rec.intgfrom24);
		fieldNo011.set(t6775rec.intgto13);
		fieldNo017.set(t6775rec.intgto14);
		fieldNo023.set(t6775rec.intgto15);
		fieldNo029.set(t6775rec.intgto16);
		fieldNo035.set(t6775rec.intgto17);
		fieldNo041.set(t6775rec.intgto18);
		fieldNo047.set(t6775rec.intgto19);
		fieldNo053.set(t6775rec.intgto20);
		fieldNo059.set(t6775rec.intgto21);
		fieldNo065.set(t6775rec.intgto22);
		fieldNo071.set(t6775rec.intgto23);
		fieldNo077.set(t6775rec.intgto24);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
