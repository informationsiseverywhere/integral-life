package com.csc.life.underwriting.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:09
 * Description:
 * Copybook name: T6686REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6686rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6686Rec = new FixedLengthStringData(476);
  	public FixedLengthStringData lxdps = new FixedLengthStringData(32).isAPartOf(t6686Rec, 0);
  		/*                                           OCCURS 15 .*/
  	public BinaryData[] lxdp = BDArrayPartOfStructure(16, 1, 0, lxdps, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(32).isAPartOf(lxdps, 0, FILLER_REDEFINE);
  	public BinaryData lxdp01 = new BinaryData(1, 0).isAPartOf(filler, 0);
  	public BinaryData lxdp02 = new BinaryData(1, 0).isAPartOf(filler, 2);
  	public BinaryData lxdp03 = new BinaryData(1, 0).isAPartOf(filler, 4);
  	public BinaryData lxdp04 = new BinaryData(1, 0).isAPartOf(filler, 6);
  	public BinaryData lxdp05 = new BinaryData(1, 0).isAPartOf(filler, 8);
  	public BinaryData lxdp06 = new BinaryData(1, 0).isAPartOf(filler, 10);
  	public BinaryData lxdp07 = new BinaryData(1, 0).isAPartOf(filler, 12);
  	public BinaryData lxdp08 = new BinaryData(1, 0).isAPartOf(filler, 14);
  	public BinaryData lxdp09 = new BinaryData(1, 0).isAPartOf(filler, 16);
  	public BinaryData lxdp10 = new BinaryData(1, 0).isAPartOf(filler, 18);
  	public BinaryData lxdp11 = new BinaryData(1, 0).isAPartOf(filler, 20);
  	public BinaryData lxdp12 = new BinaryData(1, 0).isAPartOf(filler, 22);
  	public BinaryData lxdp13 = new BinaryData(1, 0).isAPartOf(filler, 24);
  	public BinaryData lxdp14 = new BinaryData(1, 0).isAPartOf(filler, 26);
  	public BinaryData lxdp15 = new BinaryData(1, 0).isAPartOf(filler, 28);
  	public BinaryData lxdp16 = new BinaryData(1, 0).isAPartOf(filler, 30);
  	public BinaryData lxmort = new BinaryData(8, 0).isAPartOf(t6686Rec, 32);
  	public FixedLengthStringData lxmrts = new FixedLengthStringData(396).isAPartOf(t6686Rec, 36);
  	public BinaryData[] lxmrt = BDArrayPartOfStructure(99, 8, 0, lxmrts, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(396).isAPartOf(lxmrts, 0, FILLER_REDEFINE);
  	public BinaryData lxmrt01 = new BinaryData(8, 0).isAPartOf(filler1, 0);
  	public BinaryData lxmrt02 = new BinaryData(8, 0).isAPartOf(filler1, 4);
  	public BinaryData lxmrt03 = new BinaryData(8, 0).isAPartOf(filler1, 8);
  	public BinaryData lxmrt04 = new BinaryData(8, 0).isAPartOf(filler1, 12);
  	public BinaryData lxmrt05 = new BinaryData(8, 0).isAPartOf(filler1, 16);
  	public BinaryData lxmrt06 = new BinaryData(8, 0).isAPartOf(filler1, 20);
  	public BinaryData lxmrt07 = new BinaryData(8, 0).isAPartOf(filler1, 24);
  	public BinaryData lxmrt08 = new BinaryData(8, 0).isAPartOf(filler1, 28);
  	public BinaryData lxmrt09 = new BinaryData(8, 0).isAPartOf(filler1, 32);
  	public BinaryData lxmrt10 = new BinaryData(8, 0).isAPartOf(filler1, 36);
  	public BinaryData lxmrt11 = new BinaryData(8, 0).isAPartOf(filler1, 40);
  	public BinaryData lxmrt12 = new BinaryData(8, 0).isAPartOf(filler1, 44);
  	public BinaryData lxmrt13 = new BinaryData(8, 0).isAPartOf(filler1, 48);
  	public BinaryData lxmrt14 = new BinaryData(8, 0).isAPartOf(filler1, 52);
  	public BinaryData lxmrt15 = new BinaryData(8, 0).isAPartOf(filler1, 56);
  	public BinaryData lxmrt16 = new BinaryData(8, 0).isAPartOf(filler1, 60);
  	public BinaryData lxmrt17 = new BinaryData(8, 0).isAPartOf(filler1, 64);
  	public BinaryData lxmrt18 = new BinaryData(8, 0).isAPartOf(filler1, 68);
  	public BinaryData lxmrt19 = new BinaryData(8, 0).isAPartOf(filler1, 72);
  	public BinaryData lxmrt20 = new BinaryData(8, 0).isAPartOf(filler1, 76);
  	public BinaryData lxmrt21 = new BinaryData(8, 0).isAPartOf(filler1, 80);
  	public BinaryData lxmrt22 = new BinaryData(8, 0).isAPartOf(filler1, 84);
  	public BinaryData lxmrt23 = new BinaryData(8, 0).isAPartOf(filler1, 88);
  	public BinaryData lxmrt24 = new BinaryData(8, 0).isAPartOf(filler1, 92);
  	public BinaryData lxmrt25 = new BinaryData(8, 0).isAPartOf(filler1, 96);
  	public BinaryData lxmrt26 = new BinaryData(8, 0).isAPartOf(filler1, 100);
  	public BinaryData lxmrt27 = new BinaryData(8, 0).isAPartOf(filler1, 104);
  	public BinaryData lxmrt28 = new BinaryData(8, 0).isAPartOf(filler1, 108);
  	public BinaryData lxmrt29 = new BinaryData(8, 0).isAPartOf(filler1, 112);
  	public BinaryData lxmrt30 = new BinaryData(8, 0).isAPartOf(filler1, 116);
  	public BinaryData lxmrt31 = new BinaryData(8, 0).isAPartOf(filler1, 120);
  	public BinaryData lxmrt32 = new BinaryData(8, 0).isAPartOf(filler1, 124);
  	public BinaryData lxmrt33 = new BinaryData(8, 0).isAPartOf(filler1, 128);
  	public BinaryData lxmrt34 = new BinaryData(8, 0).isAPartOf(filler1, 132);
  	public BinaryData lxmrt35 = new BinaryData(8, 0).isAPartOf(filler1, 136);
  	public BinaryData lxmrt36 = new BinaryData(8, 0).isAPartOf(filler1, 140);
  	public BinaryData lxmrt37 = new BinaryData(8, 0).isAPartOf(filler1, 144);
  	public BinaryData lxmrt38 = new BinaryData(8, 0).isAPartOf(filler1, 148);
  	public BinaryData lxmrt39 = new BinaryData(8, 0).isAPartOf(filler1, 152);
  	public BinaryData lxmrt40 = new BinaryData(8, 0).isAPartOf(filler1, 156);
  	public BinaryData lxmrt41 = new BinaryData(8, 0).isAPartOf(filler1, 160);
  	public BinaryData lxmrt42 = new BinaryData(8, 0).isAPartOf(filler1, 164);
  	public BinaryData lxmrt43 = new BinaryData(8, 0).isAPartOf(filler1, 168);
  	public BinaryData lxmrt44 = new BinaryData(8, 0).isAPartOf(filler1, 172);
  	public BinaryData lxmrt45 = new BinaryData(8, 0).isAPartOf(filler1, 176);
  	public BinaryData lxmrt46 = new BinaryData(8, 0).isAPartOf(filler1, 180);
  	public BinaryData lxmrt47 = new BinaryData(8, 0).isAPartOf(filler1, 184);
  	public BinaryData lxmrt48 = new BinaryData(8, 0).isAPartOf(filler1, 188);
  	public BinaryData lxmrt49 = new BinaryData(8, 0).isAPartOf(filler1, 192);
  	public BinaryData lxmrt50 = new BinaryData(8, 0).isAPartOf(filler1, 196);
  	public BinaryData lxmrt51 = new BinaryData(8, 0).isAPartOf(filler1, 200);
  	public BinaryData lxmrt52 = new BinaryData(8, 0).isAPartOf(filler1, 204);
  	public BinaryData lxmrt53 = new BinaryData(8, 0).isAPartOf(filler1, 208);
  	public BinaryData lxmrt54 = new BinaryData(8, 0).isAPartOf(filler1, 212);
  	public BinaryData lxmrt55 = new BinaryData(8, 0).isAPartOf(filler1, 216);
  	public BinaryData lxmrt56 = new BinaryData(8, 0).isAPartOf(filler1, 220);
  	public BinaryData lxmrt57 = new BinaryData(8, 0).isAPartOf(filler1, 224);
  	public BinaryData lxmrt58 = new BinaryData(8, 0).isAPartOf(filler1, 228);
  	public BinaryData lxmrt59 = new BinaryData(8, 0).isAPartOf(filler1, 232);
  	public BinaryData lxmrt60 = new BinaryData(8, 0).isAPartOf(filler1, 236);
  	public BinaryData lxmrt61 = new BinaryData(8, 0).isAPartOf(filler1, 240);
  	public BinaryData lxmrt62 = new BinaryData(8, 0).isAPartOf(filler1, 244);
  	public BinaryData lxmrt63 = new BinaryData(8, 0).isAPartOf(filler1, 248);
  	public BinaryData lxmrt64 = new BinaryData(8, 0).isAPartOf(filler1, 252);
  	public BinaryData lxmrt65 = new BinaryData(8, 0).isAPartOf(filler1, 256);
  	public BinaryData lxmrt66 = new BinaryData(8, 0).isAPartOf(filler1, 260);
  	public BinaryData lxmrt67 = new BinaryData(8, 0).isAPartOf(filler1, 264);
  	public BinaryData lxmrt68 = new BinaryData(8, 0).isAPartOf(filler1, 268);
  	public BinaryData lxmrt69 = new BinaryData(8, 0).isAPartOf(filler1, 272);
  	public BinaryData lxmrt70 = new BinaryData(8, 0).isAPartOf(filler1, 276);
  	public BinaryData lxmrt71 = new BinaryData(8, 0).isAPartOf(filler1, 280);
  	public BinaryData lxmrt72 = new BinaryData(8, 0).isAPartOf(filler1, 284);
  	public BinaryData lxmrt73 = new BinaryData(8, 0).isAPartOf(filler1, 288);
  	public BinaryData lxmrt74 = new BinaryData(8, 0).isAPartOf(filler1, 292);
  	public BinaryData lxmrt75 = new BinaryData(8, 0).isAPartOf(filler1, 296);
  	public BinaryData lxmrt76 = new BinaryData(8, 0).isAPartOf(filler1, 300);
  	public BinaryData lxmrt77 = new BinaryData(8, 0).isAPartOf(filler1, 304);
  	public BinaryData lxmrt78 = new BinaryData(8, 0).isAPartOf(filler1, 308);
  	public BinaryData lxmrt79 = new BinaryData(8, 0).isAPartOf(filler1, 312);
  	public BinaryData lxmrt80 = new BinaryData(8, 0).isAPartOf(filler1, 316);
  	public BinaryData lxmrt81 = new BinaryData(8, 0).isAPartOf(filler1, 320);
  	public BinaryData lxmrt82 = new BinaryData(8, 0).isAPartOf(filler1, 324);
  	public BinaryData lxmrt83 = new BinaryData(8, 0).isAPartOf(filler1, 328);
  	public BinaryData lxmrt84 = new BinaryData(8, 0).isAPartOf(filler1, 332);
  	public BinaryData lxmrt85 = new BinaryData(8, 0).isAPartOf(filler1, 336);
  	public BinaryData lxmrt86 = new BinaryData(8, 0).isAPartOf(filler1, 340);
  	public BinaryData lxmrt87 = new BinaryData(8, 0).isAPartOf(filler1, 344);
  	public BinaryData lxmrt88 = new BinaryData(8, 0).isAPartOf(filler1, 348);
  	public BinaryData lxmrt89 = new BinaryData(8, 0).isAPartOf(filler1, 352);
  	public BinaryData lxmrt90 = new BinaryData(8, 0).isAPartOf(filler1, 356);
  	public BinaryData lxmrt91 = new BinaryData(8, 0).isAPartOf(filler1, 360);
  	public BinaryData lxmrt92 = new BinaryData(8, 0).isAPartOf(filler1, 364);
  	public BinaryData lxmrt93 = new BinaryData(8, 0).isAPartOf(filler1, 368);
  	public BinaryData lxmrt94 = new BinaryData(8, 0).isAPartOf(filler1, 372);
  	public BinaryData lxmrt95 = new BinaryData(8, 0).isAPartOf(filler1, 376);
  	public BinaryData lxmrt96 = new BinaryData(8, 0).isAPartOf(filler1, 380);
  	public BinaryData lxmrt97 = new BinaryData(8, 0).isAPartOf(filler1, 384);
  	public BinaryData lxmrt98 = new BinaryData(8, 0).isAPartOf(filler1, 388);
  	public BinaryData lxmrt99 = new BinaryData(8, 0).isAPartOf(filler1, 392);
  	public FixedLengthStringData tlxmrts = new FixedLengthStringData(44).isAPartOf(t6686Rec, 432);
  	public BinaryData[] tlxmrt = BDArrayPartOfStructure(11, 8, 0, tlxmrts, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(44).isAPartOf(tlxmrts, 0, FILLER_REDEFINE);
  	public BinaryData tlxmrt01 = new BinaryData(8, 0).isAPartOf(filler2, 0);
  	public BinaryData tlxmrt02 = new BinaryData(8, 0).isAPartOf(filler2, 4);
  	public BinaryData tlxmrt03 = new BinaryData(8, 0).isAPartOf(filler2, 8);
  	public BinaryData tlxmrt04 = new BinaryData(8, 0).isAPartOf(filler2, 12);
  	public BinaryData tlxmrt05 = new BinaryData(8, 0).isAPartOf(filler2, 16);
  	public BinaryData tlxmrt06 = new BinaryData(8, 0).isAPartOf(filler2, 20);
  	public BinaryData tlxmrt07 = new BinaryData(8, 0).isAPartOf(filler2, 24);
  	public BinaryData tlxmrt08 = new BinaryData(8, 0).isAPartOf(filler2, 28);
  	public BinaryData tlxmrt09 = new BinaryData(8, 0).isAPartOf(filler2, 32);
  	public BinaryData tlxmrt10 = new BinaryData(8, 0).isAPartOf(filler2, 36);
  	public BinaryData tlxmrt11 = new BinaryData(8, 0).isAPartOf(filler2, 40);


	public void initialize() {
		COBOLFunctions.initialize(t6686Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6686Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}