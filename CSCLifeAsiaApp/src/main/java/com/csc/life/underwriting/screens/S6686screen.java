package com.csc.life.underwriting.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6686screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6686ScreenVars sv = (S6686ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6686screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6686ScreenVars screenVars = (S6686ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.lxdp01.setClassString("");
		screenVars.lxmort.setClassString("");
		screenVars.lxmrt01.setClassString("");
		screenVars.lxmrt02.setClassString("");
		screenVars.lxmrt03.setClassString("");
		screenVars.lxmrt04.setClassString("");
		screenVars.lxmrt05.setClassString("");
		screenVars.lxmrt06.setClassString("");
		screenVars.lxmrt07.setClassString("");
		screenVars.lxmrt08.setClassString("");
		screenVars.lxmrt09.setClassString("");
		screenVars.lxmrt10.setClassString("");
		screenVars.lxmrt11.setClassString("");
		screenVars.lxmrt12.setClassString("");
		screenVars.lxmrt13.setClassString("");
		screenVars.lxmrt14.setClassString("");
		screenVars.lxmrt15.setClassString("");
		screenVars.lxmrt16.setClassString("");
		screenVars.lxmrt17.setClassString("");
		screenVars.lxmrt18.setClassString("");
		screenVars.lxmrt19.setClassString("");
		screenVars.lxmrt20.setClassString("");
		screenVars.lxmrt21.setClassString("");
		screenVars.lxmrt22.setClassString("");
		screenVars.lxmrt23.setClassString("");
		screenVars.lxmrt24.setClassString("");
		screenVars.lxmrt25.setClassString("");
		screenVars.lxmrt26.setClassString("");
		screenVars.lxmrt27.setClassString("");
		screenVars.lxmrt28.setClassString("");
		screenVars.lxmrt29.setClassString("");
		screenVars.lxmrt30.setClassString("");
		screenVars.lxmrt31.setClassString("");
		screenVars.lxmrt32.setClassString("");
		screenVars.lxmrt33.setClassString("");
		screenVars.lxmrt34.setClassString("");
		screenVars.lxmrt35.setClassString("");
		screenVars.lxmrt36.setClassString("");
		screenVars.lxmrt37.setClassString("");
		screenVars.lxmrt38.setClassString("");
		screenVars.lxmrt39.setClassString("");
		screenVars.lxmrt40.setClassString("");
		screenVars.lxmrt41.setClassString("");
		screenVars.lxmrt42.setClassString("");
		screenVars.lxmrt43.setClassString("");
		screenVars.lxmrt44.setClassString("");
		screenVars.lxmrt45.setClassString("");
		screenVars.lxmrt46.setClassString("");
		screenVars.lxmrt47.setClassString("");
		screenVars.lxmrt48.setClassString("");
		screenVars.lxmrt49.setClassString("");
		screenVars.lxmrt50.setClassString("");
		screenVars.lxmrt51.setClassString("");
		screenVars.lxmrt52.setClassString("");
		screenVars.lxmrt53.setClassString("");
		screenVars.lxmrt54.setClassString("");
		screenVars.lxmrt55.setClassString("");
		screenVars.lxmrt56.setClassString("");
		screenVars.lxmrt57.setClassString("");
		screenVars.lxmrt58.setClassString("");
		screenVars.lxmrt59.setClassString("");
		screenVars.lxmrt60.setClassString("");
		screenVars.lxmrt61.setClassString("");
		screenVars.lxmrt62.setClassString("");
		screenVars.lxmrt63.setClassString("");
		screenVars.lxmrt64.setClassString("");
		screenVars.lxmrt65.setClassString("");
		screenVars.lxmrt66.setClassString("");
		screenVars.lxmrt67.setClassString("");
		screenVars.lxmrt68.setClassString("");
		screenVars.lxmrt69.setClassString("");
		screenVars.lxmrt70.setClassString("");
		screenVars.lxmrt71.setClassString("");
		screenVars.lxmrt72.setClassString("");
		screenVars.lxmrt73.setClassString("");
		screenVars.lxmrt74.setClassString("");
		screenVars.lxmrt75.setClassString("");
		screenVars.lxmrt76.setClassString("");
		screenVars.lxmrt77.setClassString("");
		screenVars.lxmrt78.setClassString("");
		screenVars.lxmrt79.setClassString("");
		screenVars.lxmrt80.setClassString("");
		screenVars.lxmrt81.setClassString("");
		screenVars.lxmrt82.setClassString("");
		screenVars.lxmrt83.setClassString("");
		screenVars.lxmrt84.setClassString("");
		screenVars.lxmrt85.setClassString("");
		screenVars.lxmrt86.setClassString("");
		screenVars.lxmrt87.setClassString("");
		screenVars.lxmrt88.setClassString("");
		screenVars.lxmrt89.setClassString("");
		screenVars.lxmrt90.setClassString("");
		screenVars.lxmrt91.setClassString("");
		screenVars.lxmrt92.setClassString("");
		screenVars.lxmrt93.setClassString("");
		screenVars.lxmrt94.setClassString("");
		screenVars.lxmrt95.setClassString("");
		screenVars.lxmrt96.setClassString("");
		screenVars.lxmrt97.setClassString("");
		screenVars.lxmrt98.setClassString("");
		screenVars.lxmrt99.setClassString("");
		screenVars.tlxmrt01.setClassString("");
		screenVars.tlxmrt02.setClassString("");
		screenVars.tlxmrt03.setClassString("");
		screenVars.tlxmrt04.setClassString("");
		screenVars.tlxmrt05.setClassString("");
		screenVars.tlxmrt06.setClassString("");
		screenVars.tlxmrt07.setClassString("");
		screenVars.tlxmrt08.setClassString("");
		screenVars.tlxmrt09.setClassString("");
		screenVars.tlxmrt10.setClassString("");
		screenVars.tlxmrt11.setClassString("");
		screenVars.lxdp02.setClassString("");
		screenVars.lxdp03.setClassString("");
		screenVars.lxdp04.setClassString("");
		screenVars.lxdp05.setClassString("");
		screenVars.lxdp06.setClassString("");
		screenVars.lxdp07.setClassString("");
		screenVars.lxdp09.setClassString("");
		screenVars.lxdp08.setClassString("");
		screenVars.lxdp10.setClassString("");
		screenVars.lxdp11.setClassString("");
		screenVars.lxdp12.setClassString("");
		screenVars.lxdp13.setClassString("");
		screenVars.lxdp14.setClassString("");
		screenVars.lxdp15.setClassString("");
		screenVars.lxdp16.setClassString("");
	}

/**
 * Clear all the variables in S6686screen
 */
	public static void clear(VarModel pv) {
		S6686ScreenVars screenVars = (S6686ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.lxdp01.clear();
		screenVars.lxmort.clear();
		screenVars.lxmrt01.clear();
		screenVars.lxmrt02.clear();
		screenVars.lxmrt03.clear();
		screenVars.lxmrt04.clear();
		screenVars.lxmrt05.clear();
		screenVars.lxmrt06.clear();
		screenVars.lxmrt07.clear();
		screenVars.lxmrt08.clear();
		screenVars.lxmrt09.clear();
		screenVars.lxmrt10.clear();
		screenVars.lxmrt11.clear();
		screenVars.lxmrt12.clear();
		screenVars.lxmrt13.clear();
		screenVars.lxmrt14.clear();
		screenVars.lxmrt15.clear();
		screenVars.lxmrt16.clear();
		screenVars.lxmrt17.clear();
		screenVars.lxmrt18.clear();
		screenVars.lxmrt19.clear();
		screenVars.lxmrt20.clear();
		screenVars.lxmrt21.clear();
		screenVars.lxmrt22.clear();
		screenVars.lxmrt23.clear();
		screenVars.lxmrt24.clear();
		screenVars.lxmrt25.clear();
		screenVars.lxmrt26.clear();
		screenVars.lxmrt27.clear();
		screenVars.lxmrt28.clear();
		screenVars.lxmrt29.clear();
		screenVars.lxmrt30.clear();
		screenVars.lxmrt31.clear();
		screenVars.lxmrt32.clear();
		screenVars.lxmrt33.clear();
		screenVars.lxmrt34.clear();
		screenVars.lxmrt35.clear();
		screenVars.lxmrt36.clear();
		screenVars.lxmrt37.clear();
		screenVars.lxmrt38.clear();
		screenVars.lxmrt39.clear();
		screenVars.lxmrt40.clear();
		screenVars.lxmrt41.clear();
		screenVars.lxmrt42.clear();
		screenVars.lxmrt43.clear();
		screenVars.lxmrt44.clear();
		screenVars.lxmrt45.clear();
		screenVars.lxmrt46.clear();
		screenVars.lxmrt47.clear();
		screenVars.lxmrt48.clear();
		screenVars.lxmrt49.clear();
		screenVars.lxmrt50.clear();
		screenVars.lxmrt51.clear();
		screenVars.lxmrt52.clear();
		screenVars.lxmrt53.clear();
		screenVars.lxmrt54.clear();
		screenVars.lxmrt55.clear();
		screenVars.lxmrt56.clear();
		screenVars.lxmrt57.clear();
		screenVars.lxmrt58.clear();
		screenVars.lxmrt59.clear();
		screenVars.lxmrt60.clear();
		screenVars.lxmrt61.clear();
		screenVars.lxmrt62.clear();
		screenVars.lxmrt63.clear();
		screenVars.lxmrt64.clear();
		screenVars.lxmrt65.clear();
		screenVars.lxmrt66.clear();
		screenVars.lxmrt67.clear();
		screenVars.lxmrt68.clear();
		screenVars.lxmrt69.clear();
		screenVars.lxmrt70.clear();
		screenVars.lxmrt71.clear();
		screenVars.lxmrt72.clear();
		screenVars.lxmrt73.clear();
		screenVars.lxmrt74.clear();
		screenVars.lxmrt75.clear();
		screenVars.lxmrt76.clear();
		screenVars.lxmrt77.clear();
		screenVars.lxmrt78.clear();
		screenVars.lxmrt79.clear();
		screenVars.lxmrt80.clear();
		screenVars.lxmrt81.clear();
		screenVars.lxmrt82.clear();
		screenVars.lxmrt83.clear();
		screenVars.lxmrt84.clear();
		screenVars.lxmrt85.clear();
		screenVars.lxmrt86.clear();
		screenVars.lxmrt87.clear();
		screenVars.lxmrt88.clear();
		screenVars.lxmrt89.clear();
		screenVars.lxmrt90.clear();
		screenVars.lxmrt91.clear();
		screenVars.lxmrt92.clear();
		screenVars.lxmrt93.clear();
		screenVars.lxmrt94.clear();
		screenVars.lxmrt95.clear();
		screenVars.lxmrt96.clear();
		screenVars.lxmrt97.clear();
		screenVars.lxmrt98.clear();
		screenVars.lxmrt99.clear();
		screenVars.tlxmrt01.clear();
		screenVars.tlxmrt02.clear();
		screenVars.tlxmrt03.clear();
		screenVars.tlxmrt04.clear();
		screenVars.tlxmrt05.clear();
		screenVars.tlxmrt06.clear();
		screenVars.tlxmrt07.clear();
		screenVars.tlxmrt08.clear();
		screenVars.tlxmrt09.clear();
		screenVars.tlxmrt10.clear();
		screenVars.tlxmrt11.clear();
		screenVars.lxdp02.clear();
		screenVars.lxdp03.clear();
		screenVars.lxdp04.clear();
		screenVars.lxdp05.clear();
		screenVars.lxdp06.clear();
		screenVars.lxdp07.clear();
		screenVars.lxdp09.clear();
		screenVars.lxdp08.clear();
		screenVars.lxdp10.clear();
		screenVars.lxdp11.clear();
		screenVars.lxdp12.clear();
		screenVars.lxdp13.clear();
		screenVars.lxdp14.clear();
		screenVars.lxdp15.clear();
		screenVars.lxdp16.clear();
	}
}
