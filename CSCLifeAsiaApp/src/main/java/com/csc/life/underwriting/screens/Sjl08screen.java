package com.csc.life.underwriting.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.TableModel.ScreenRecord.RecInfo;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl08screen  extends ScreenRecord {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl08ScreenVars sv = (Sjl08ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sjl08screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl08ScreenVars screenVars = (Sjl08ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.sumAssuredFactor.setClassString("");
		screenVars.riskClass01.setClassString("");
		screenVars.riskClass02.setClassString("");
		screenVars.riskClass03.setClassString("");
		screenVars.riskClass04.setClassString("");
		screenVars.riskClass05.setClassString("");
	}


	public static void clear(VarModel pv) {
		Sjl08ScreenVars screenVars = (Sjl08ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.sumAssuredFactor.clear();
		screenVars.riskClass01.clear();
		screenVars.riskClass02.clear();
		screenVars.riskClass03.clear();
		screenVars.riskClass04.clear();
		screenVars.riskClass05.clear();
	}
}
