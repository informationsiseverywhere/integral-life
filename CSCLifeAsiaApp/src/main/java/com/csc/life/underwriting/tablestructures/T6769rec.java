package com.csc.life.underwriting.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:24
 * Description:
 * Copybook name: T6769REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6769rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6769Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData bmiclasss = new FixedLengthStringData(200).isAPartOf(t6769Rec, 0);
  	public FixedLengthStringData[] bmiclass = FLSArrayPartOfStructure(10, 20, bmiclasss, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(200).isAPartOf(bmiclasss, 0, FILLER_REDEFINE);
  	public FixedLengthStringData bmiclass01 = new FixedLengthStringData(20).isAPartOf(filler, 0);
  	public FixedLengthStringData bmiclass02 = new FixedLengthStringData(20).isAPartOf(filler, 20);
  	public FixedLengthStringData bmiclass03 = new FixedLengthStringData(20).isAPartOf(filler, 40);
  	public FixedLengthStringData bmiclass04 = new FixedLengthStringData(20).isAPartOf(filler, 60);
  	public FixedLengthStringData bmiclass05 = new FixedLengthStringData(20).isAPartOf(filler, 80);
  	public FixedLengthStringData bmiclass06 = new FixedLengthStringData(20).isAPartOf(filler, 100);
  	public FixedLengthStringData bmiclass07 = new FixedLengthStringData(20).isAPartOf(filler, 120);
  	public FixedLengthStringData bmiclass08 = new FixedLengthStringData(20).isAPartOf(filler, 140);
  	public FixedLengthStringData bmiclass09 = new FixedLengthStringData(20).isAPartOf(filler, 160);
  	public FixedLengthStringData bmiclass10 = new FixedLengthStringData(20).isAPartOf(filler, 180);
  	public ZonedDecimalData bmifact = new ZonedDecimalData(8, 0).isAPartOf(t6769Rec, 200);
  	public FixedLengthStringData bmis = new FixedLengthStringData(40).isAPartOf(t6769Rec, 208);
  	public ZonedDecimalData[] bmi = ZDArrayPartOfStructure(10, 4, 1, bmis, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(40).isAPartOf(bmis, 0, FILLER_REDEFINE);
  	public ZonedDecimalData bmi01 = new ZonedDecimalData(4, 1).isAPartOf(filler1, 0);
  	public ZonedDecimalData bmi02 = new ZonedDecimalData(4, 1).isAPartOf(filler1, 4);
  	public ZonedDecimalData bmi03 = new ZonedDecimalData(4, 1).isAPartOf(filler1, 8);
  	public ZonedDecimalData bmi04 = new ZonedDecimalData(4, 1).isAPartOf(filler1, 12);
  	public ZonedDecimalData bmi05 = new ZonedDecimalData(4, 1).isAPartOf(filler1, 16);
  	public ZonedDecimalData bmi06 = new ZonedDecimalData(4, 1).isAPartOf(filler1, 20);
  	public ZonedDecimalData bmi07 = new ZonedDecimalData(4, 1).isAPartOf(filler1, 24);
  	public ZonedDecimalData bmi08 = new ZonedDecimalData(4, 1).isAPartOf(filler1, 28);
  	public ZonedDecimalData bmi09 = new ZonedDecimalData(4, 1).isAPartOf(filler1, 32);
  	public ZonedDecimalData bmi10 = new ZonedDecimalData(4, 1).isAPartOf(filler1, 36);
  	public FixedLengthStringData undwrules = new FixedLengthStringData(40).isAPartOf(t6769Rec, 248);
  	public FixedLengthStringData[] undwrule = FLSArrayPartOfStructure(10, 4, undwrules, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(undwrules, 0, FILLER_REDEFINE);
  	public FixedLengthStringData undwrule01 = new FixedLengthStringData(4).isAPartOf(filler2, 0);
  	public FixedLengthStringData undwrule02 = new FixedLengthStringData(4).isAPartOf(filler2, 4);
  	public FixedLengthStringData undwrule03 = new FixedLengthStringData(4).isAPartOf(filler2, 8);
  	public FixedLengthStringData undwrule04 = new FixedLengthStringData(4).isAPartOf(filler2, 12);
  	public FixedLengthStringData undwrule05 = new FixedLengthStringData(4).isAPartOf(filler2, 16);
  	public FixedLengthStringData undwrule06 = new FixedLengthStringData(4).isAPartOf(filler2, 20);
  	public FixedLengthStringData undwrule07 = new FixedLengthStringData(4).isAPartOf(filler2, 24);
  	public FixedLengthStringData undwrule08 = new FixedLengthStringData(4).isAPartOf(filler2, 28);
  	public FixedLengthStringData undwrule09 = new FixedLengthStringData(4).isAPartOf(filler2, 32);
  	public FixedLengthStringData undwrule10 = new FixedLengthStringData(4).isAPartOf(filler2, 36);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(212).isAPartOf(t6769Rec, 288, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6769Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6769Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}