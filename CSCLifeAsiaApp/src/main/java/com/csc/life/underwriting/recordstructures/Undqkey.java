package com.csc.life.underwriting.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:34
 * Description:
 * Copybook name: UNDQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Undqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData undqFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData undqKey = new FixedLengthStringData(256).isAPartOf(undqFileKey, 0, REDEFINE);
  	public FixedLengthStringData undqChdrcoy = new FixedLengthStringData(1).isAPartOf(undqKey, 0);
  	public FixedLengthStringData undqChdrnum = new FixedLengthStringData(8).isAPartOf(undqKey, 1);
  	public FixedLengthStringData undqLife = new FixedLengthStringData(2).isAPartOf(undqKey, 9);
  	public FixedLengthStringData undqJlife = new FixedLengthStringData(2).isAPartOf(undqKey, 11);
  	public FixedLengthStringData undqQuestidf = new FixedLengthStringData(5).isAPartOf(undqKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(238).isAPartOf(undqKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(undqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		undqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}