/*
 * File: P6765.java
 * Date: 30 August 2009 0:56:46
 * Author: Quipoz Limited
 * 
 * Class transformed from P6765.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceLeading;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/* ILIFE-5536(LIFE VPMS Externalization - Code promotion for Life Underwriting calculations) start */
import com.csc.dip.jvpms.runtime.base.IVpmsBaseSession;
import com.csc.dip.jvpms.runtime.base.IVpmsBaseSessionFactory;
import com.csc.dip.jvpms.runtime.base.VpmsComputeResult;
import com.csc.dip.jvpms.runtime.base.VpmsJniSessionFactory;
import com.csc.dip.jvpms.runtime.base.VpmsLoadFailedException;
import com.csc.dip.jvpms.runtime.base.VpmsTcpSessionFactory;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.procedures.UwQuestionnaireUtil;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.tablestructures.T5657rec;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.recordstructures.UWQuestionnaireRec;
import com.csc.life.underwriting.dataaccess.UndqTableDAM;
import com.csc.life.underwriting.dataaccess.dao.UndqpfDAO;
import com.csc.life.underwriting.dataaccess.model.Undqpf;
import com.csc.life.underwriting.screens.S6765ScreenVars;
import com.csc.life.underwriting.tablestructures.T6770rec;
import com.csc.life.underwriting.tablestructures.T6771rec;
import com.csc.life.underwriting.tablestructures.T6775rec;
import com.csc.life.underwriting.tablestructures.T6776rec;
import com.csc.life.underwriting.tablestructures.T6778rec;
import com.csc.life.underwriting.tablestructures.Tr675rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.ThreadLocalStore;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* This program will display all the underwriting questions
* which are relevant to a particular product type.
*
* The underwriting questions relating to each product are stored
* on T6771. This table also specifies whether a question is
* optional or mandatory.
*
* 1000 Section
* ============
*
* In the 1000- section all of the product specific questions are
* loaded into the subfile. Some questions will spill over on to
* more than 1 line, e.g. a question may consist of up to 4
* subfile lines. The ANSWER will always be displayed on the first
* line of the question.
*
* A blank line should separate each question.
*
* Some hidden fields are also displayed on the screen per
* question i.e. answer type (I=integer and B=boolean), question
* status (M=mandatory, O=optional) the question identifier and
* the associated underwriting rule.
*
* Question details (answer type & u/w rule are stored on
* T6770).
*
* Example of how screen could look......
*
* Ques  Question                         Ques  Answ  Answer  Rule
* Id                                     Stat  Type
*
* Q07   Is the client currently taking     M     B     N     UW07
*       medication?
*
* Q11   Has the client had cancer within   M     B     Y     UW11
*       the last 5 years?
*
*
* 2000 Section
* ============
* In this section validate that the answer provided corresponds
* to the answer type. I.e. if the question is 'Number of cigars
* smoken daily' then the answer type is 'I' (integer) and the
* answer should be an integer. The other answer type could be
* boolean i.e. 'Y' or 'N'.
*
* 3000 Section
* ============
* In this section write an UNDQ record for each question
* answered. If the answer to a question is either zero or 'N'
* then the underwriting rule should not be updated to the file.
*
*
***********************************************************************
* </pre>
*/
public class P6765 extends ScreenProgCS {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(P6765.class);

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6765");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	protected FixedLengthStringData wsaaCheckFlag = new FixedLengthStringData(1);
	private Validator numericFound = new Validator(wsaaCheckFlag, "Y");
	protected Validator nonNumericFound = new Validator(wsaaCheckFlag, "N");

	protected FixedLengthStringData wsaaAnswerFlag = new FixedLengthStringData(1);
	protected Validator answRangeFound = new Validator(wsaaAnswerFlag, "Y");

	private FixedLengthStringData wsaaQuesttypFlag = new FixedLengthStringData(1);
	private Validator questtypFound = new Validator(wsaaQuesttypFlag, "Y");

	protected FixedLengthStringData wsaaIntBoolFlag = new FixedLengthStringData(1);
	protected Validator integerType = new Validator(wsaaIntBoolFlag, "Y");
	protected Validator booleanType = new Validator(wsaaIntBoolFlag, "N");

	private FixedLengthStringData wsaaAnswerValFlag = new FixedLengthStringData(1);
	private Validator invalidAnswer = new Validator(wsaaAnswerValFlag, "Y");

	private FixedLengthStringData wsaaUndwRuleFlag = new FixedLengthStringData(1);
	private Validator applyUndwRule = new Validator(wsaaUndwRuleFlag, "Y");
	protected FixedLengthStringData wsaaAnswerField = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaAns = new FixedLengthStringData(4).isAPartOf(wsaaAnswerField, 0, REDEFINE);
	protected FixedLengthStringData[] wsaaAnswerI = FLSArrayPartOfStructure(4, 1, wsaaAns, 0);
	private FixedLengthStringData wsaaAnswerRev = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaAnsR = new FixedLengthStringData(4).isAPartOf(wsaaAnswerRev, 0, REDEFINE);
	private FixedLengthStringData[] wsaaAnswerR = FLSArrayPartOfStructure(4, 1, wsaaAnsR, 0);
	protected ZonedDecimalData wsaaAnswerNum = new ZonedDecimalData(4, 0).init(ZERO).setUnsigned();
	
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaAnswerNum, 0, FILLER_REDEFINE);
	protected ZonedDecimalData[] wsaaAnswerN = ZDArrayPartOfStructure(4, 1, 0, filler, 0, UNSIGNED_TRUE);

	private FixedLengthStringData wsaaAnswerOut = new FixedLengthStringData(4);
	private FixedLengthStringData[] wsaaAnswerOuti = FLSArrayPartOfStructure(4, 1, wsaaAnswerOut, 0);

	private FixedLengthStringData wsaaNumerics = new FixedLengthStringData(28);
	private ZonedDecimalData qtyp = new ZonedDecimalData(3, 0).isAPartOf(wsaaNumerics, 0).setUnsigned();
	protected ZonedDecimalData sub = new ZonedDecimalData(3, 0).isAPartOf(wsaaNumerics, 3).setUnsigned();
	protected ZonedDecimalData sub1 = new ZonedDecimalData(3, 0).isAPartOf(wsaaNumerics, 6).setUnsigned();
	private ZonedDecimalData wsaaLineTest = new ZonedDecimalData(2, 0).isAPartOf(wsaaNumerics, 9);
	private ZonedDecimalData wsaaScrnLinesLeft = new ZonedDecimalData(2, 0).isAPartOf(wsaaNumerics, 11).setUnsigned();
	private ZonedDecimalData wsaaRemainder = new ZonedDecimalData(2, 0).isAPartOf(wsaaNumerics, 13).setUnsigned();
	protected ZonedDecimalData wsaaLinesOnPage = new ZonedDecimalData(3, 0).isAPartOf(wsaaNumerics, 15);
	private ZonedDecimalData wsaaSubfilePage = new ZonedDecimalData(3, 0).isAPartOf(wsaaNumerics, 18);
	private BinaryData wsaaRrn = new BinaryData(9, 0).isAPartOf(wsaaNumerics, 21);
	protected ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).isAPartOf(wsaaNumerics, 25).setUnsigned();
	protected ZonedDecimalData wsaaNumSub = new ZonedDecimalData(1, 0).isAPartOf(wsaaNumerics, 27).setUnsigned();

	private FixedLengthStringData wsaaAlphanumerics = new FixedLengthStringData(13);
	protected FixedLengthStringData wsaaItemtabl = new FixedLengthStringData(5).isAPartOf(wsaaAlphanumerics, 0).init(SPACES);
	protected FixedLengthStringData wsaaItemitem = new FixedLengthStringData(8).isAPartOf(wsaaAlphanumerics, 5).init(SPACES);
		/* WSAA-SUBFILE-AREA */
	private FixedLengthStringData wsaaSubfileFields = new FixedLengthStringData(90);
	private FixedLengthStringData wsaaAnswband = new FixedLengthStringData(4).isAPartOf(wsaaSubfileFields, 0);
	private FixedLengthStringData wsaaAnswer = new FixedLengthStringData(4).isAPartOf(wsaaSubfileFields, 4);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaSubfileFields, 8);
	private FixedLengthStringData wsaaMandques = new FixedLengthStringData(1).isAPartOf(wsaaSubfileFields, 9);
	private FixedLengthStringData wsaaQuestidf = new FixedLengthStringData(4).isAPartOf(wsaaSubfileFields, 10);
	private FixedLengthStringData wsaaQuestion = new FixedLengthStringData(70).isAPartOf(wsaaSubfileFields, 14);
	private FixedLengthStringData wsaaQuestst = new FixedLengthStringData(1).isAPartOf(wsaaSubfileFields, 84);
	private FixedLengthStringData wsaaQuesttyp = new FixedLengthStringData(1).isAPartOf(wsaaSubfileFields, 85);
	protected FixedLengthStringData wsaaUndwrule = new FixedLengthStringData(4).isAPartOf(wsaaSubfileFields, 86);
	
	private FixedLengthStringData wsaaT6776Array = new FixedLengthStringData(60);
	private FixedLengthStringData[] wsaaT6776Item = FLSArrayPartOfStructure(10, 6, wsaaT6776Array, 0);
	protected FixedLengthStringData[] wsaaQuesttype = FLSDArrayPartOfArrayStructure(1, wsaaT6776Item, 0);
	protected FixedLengthStringData[] wsaaIntgflag = FLSDArrayPartOfArrayStructure(1, wsaaT6776Item, 1);
	private FixedLengthStringData[] wsaaBoolansw01 = FLSDArrayPartOfArrayStructure(1, wsaaT6776Item, 2);
	private FixedLengthStringData[] wsaaBoolansw02 = FLSDArrayPartOfArrayStructure(1, wsaaT6776Item, 3);
	private FixedLengthStringData[] wsaaRulechck01 = FLSDArrayPartOfArrayStructure(1, wsaaT6776Item, 4);
	private FixedLengthStringData[] wsaaRulechck02 = FLSDArrayPartOfArrayStructure(1, wsaaT6776Item, 5);
	protected FixedLengthStringData wsaaQuestset = new FixedLengthStringData(8);

	protected FixedLengthStringData wsaaT6775Key = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaT6775Questidf = new FixedLengthStringData(4).isAPartOf(wsaaT6775Key, 0);
	private FixedLengthStringData wsaaT6775Band = new FixedLengthStringData(5).isAPartOf(wsaaT6775Key, 4);
	private FixedLengthStringData wsaaT6775Answband = new FixedLengthStringData(4).isAPartOf(wsaaT6775Band, 0);
	private FixedLengthStringData wsaaT6775C = new FixedLengthStringData(1).isAPartOf(wsaaT6775Band, 4);

	private FixedLengthStringData wsaaT6770Key = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaT6770Lang = new FixedLengthStringData(1).isAPartOf(wsaaT6770Key, 0);
	private FixedLengthStringData wsaaT6770Questidf = new FixedLengthStringData(4).isAPartOf(wsaaT6770Key, 1);
		/* ERRORS */
//	private String e595 = "E595";
	private static final String E960 = "E960";
	private static final String E962 = "E962";
	protected static final String E963 = "E963";
		/* TABLES */
	private static final String T6770 = "T6770";
	protected static final String T6771 = "T6771";
	protected static final String T6775 = "T6775";
	private static final String T6776 = "T6776";
	private static final String T6778 = "T6778";
	private static final String TR675 = "TR675";
//	private String itemrec = "ITEMREC";
	private static final String UNDQREC = "UNDQREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Contract header - life new business*/
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Client logical file with new fields*/
//	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Clntpf cltsIO = new Clntpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Table items, date - maintenance view*/
	//private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
//	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life and joint life details - new busine*/
	protected LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
//	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	
	protected T6770rec t6770rec =  getT6770rec();
	protected T6771rec t6771rec = new T6771rec();
	private T6775rec t6775rec = new T6775rec();
	private T6776rec t6776rec = new T6776rec();
	private T6778rec t6778rec = new T6778rec();
	private Tr675rec tr675rec = new Tr675rec();
		/*Underwriting Question Details*/
	private UndqTableDAM undqIO = new UndqTableDAM();
	protected S6765ScreenVars sv = ScreenProgram.getScreenVars( S6765ScreenVars.class);
	//ILIFE-5536
	private int questionCount=0;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<String> questions = null;
	public static final String UWQUESTIONNAIRECACHE= "UWQUESTIONNAIRE";
	private boolean uwFlag = false;
	private LextpfDAO lextpfDAO = getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
	protected LextTableDAM lextIO = new LextTableDAM();
	private CovtpfDAO covtpfDAO= getApplicationContext().getBean("covtpfDAO",CovtpfDAO.class);
	private UwQuestionnaireUtil uwQuestionnaireUtil = getApplicationContext().getBean("uwQuestionnaireUtil", UwQuestionnaireUtil.class);
	protected ItemTableDAM itemIO = new ItemTableDAM();
	protected FixedLengthStringData wsaaT5657key = new FixedLengthStringData(6);
	protected FixedLengthStringData wsaaT5657key1 = new FixedLengthStringData(4).isAPartOf(wsaaT5657key, 0).init(SPACES);
	protected FixedLengthStringData wsaaT5657key2 = new FixedLengthStringData(2).isAPartOf(wsaaT5657key, 4).init(SPACES);
	protected T5657rec t5657rec = new T5657rec();
	private List<Undqpf> undqList = null;
	private List<String> undqResultList = null;
	private UndqpfDAO undqpfDAO = getApplicationContext().getBean("undqpfDAO", UndqpfDAO.class);
	
	public enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		readT67711070, 
		exit1290, 
		exit1289, 
		preExit, 
		exit2090, 
		exit2690, 
		exit3090, 
		exit3290, 
		exit3559
	}

	public P6765() {
		super();
		screenVars = sv;
		new ScreenModel("S6765", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
				}
				case readT67711070: {
					readT67711070();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		wsaaNumerics.set(ZERO);
		wsaaSubfileFields.set(SPACES);
		wsaaAlphanumerics.set(SPACES);
		wsaaT6776Array.set(SPACES);
		qtyp.set(ZERO);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6765", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		lifelnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError600();
		}
		sv.chdrnum.set(lifelnbIO.getChdrnum());
		sv.life.set(lifelnbIO.getLife());
		sv.jlife.set(lifelnbIO.getJlife());
		sv.lifenum.set(lifelnbIO.getLifcnum());
		/*cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifelnbIO.getLifcnum());*/
		cltsio1900();
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		uwFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP123", appVars, "IT");
		/* ILIFE-5536(LIFE VPMS Externalization - Code promotion for Life Underwriting calculations) start */
		boolean extValidation = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBEXTV01", appVars, "IT");
		if(AppVars.getInstance().getAppConfig().isVpmsEnable() && extValidation && (isEQ(chdrlnbIO.cnttype,"RUL") || isEQ(chdrlnbIO.cnttype,"TEN") || isEQ(chdrlnbIO.cnttype,"ULP")))
		{
			try {
					getExternalUW();
			}//IJTI-851-Overly Broad Catch
			catch (VpmsLoadFailedException e) {
				LOGGER.error("Exception occurred", e);
			}
		}
		else
		{		
		/*itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tr675);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}*/
		List<Itempf> items = itemDAO.getItdmByFrmdate(wsspcomn.company.toString(), TR675, chdrlnbIO.getCnttype().toString(), chdrlnbIO.getOccdate().toInt());
		if (items.isEmpty()) {
			fatalError600();
		}
		tr675rec.tr675Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
		wsaaQuestset.set(SPACES);
		if (isEQ(cltsIO.getCltsex(),"M")) {
			if (isLTE(lifelnbIO.getAnbAtCcd(),tr675rec.age01)) {
				wsaaQuestset.set(tr675rec.questset01);
				goTo(GotoLabel.readT67711070);
			}
			if (isGT(lifelnbIO.getAnbAtCcd(),tr675rec.age01)
			&& isLTE(lifelnbIO.getAnbAtCcd(),tr675rec.age02)) {
				wsaaQuestset.set(tr675rec.questset03);
				goTo(GotoLabel.readT67711070);
			}
			if (isGT(lifelnbIO.getAnbAtCcd(),tr675rec.age02)
			&& isLTE(lifelnbIO.getAnbAtCcd(),tr675rec.age03)) {
				wsaaQuestset.set(tr675rec.questset05);
				goTo(GotoLabel.readT67711070);
			}
			if (isGT(lifelnbIO.getAnbAtCcd(),tr675rec.age03)
			&& isLTE(lifelnbIO.getAnbAtCcd(),tr675rec.age04)) {
				wsaaQuestset.set(tr675rec.questset07);
				goTo(GotoLabel.readT67711070);
			}
			if (isGT(lifelnbIO.getAnbAtCcd(),tr675rec.age04)
			&& isLTE(lifelnbIO.getAnbAtCcd(),tr675rec.age05)) {
				wsaaQuestset.set(tr675rec.questset09);
				goTo(GotoLabel.readT67711070);
			}
			if (isGT(lifelnbIO.getAnbAtCcd(),tr675rec.age05)
			&& isLTE(lifelnbIO.getAnbAtCcd(),tr675rec.age06)) {
				wsaaQuestset.set(tr675rec.questset11);
				goTo(GotoLabel.readT67711070);
			}
		}
		else {
			if (isLTE(lifelnbIO.getAnbAtCcd(),tr675rec.age01)) {
				wsaaQuestset.set(tr675rec.questset02);
				goTo(GotoLabel.readT67711070);
			}
			if (isGT(lifelnbIO.getAnbAtCcd(),tr675rec.age01)
			&& isLTE(lifelnbIO.getAnbAtCcd(),tr675rec.age02)) {
				wsaaQuestset.set(tr675rec.questset04);
				goTo(GotoLabel.readT67711070);
			}
			if (isGT(lifelnbIO.getAnbAtCcd(),tr675rec.age02)
			&& isLTE(lifelnbIO.getAnbAtCcd(),tr675rec.age03)) {
				wsaaQuestset.set(tr675rec.questset06);
				goTo(GotoLabel.readT67711070);
			}
			if (isGT(lifelnbIO.getAnbAtCcd(),tr675rec.age03)
			&& isLTE(lifelnbIO.getAnbAtCcd(),tr675rec.age04)) {
				wsaaQuestset.set(tr675rec.questset08);
				goTo(GotoLabel.readT67711070);
			}
			if (isGT(lifelnbIO.getAnbAtCcd(),tr675rec.age04)
			&& isLTE(lifelnbIO.getAnbAtCcd(),tr675rec.age05)) {
				wsaaQuestset.set(tr675rec.questset10);
				goTo(GotoLabel.readT67711070);
			}
			if (isGT(lifelnbIO.getAnbAtCcd(),tr675rec.age05)
			&& isLTE(lifelnbIO.getAnbAtCcd(),tr675rec.age06)) {
				wsaaQuestset.set(tr675rec.questset12);
				goTo(GotoLabel.readT67711070);
			}
		}
		
		}
		
	}

protected void getExternalUW() throws VpmsLoadFailedException{
	
	String businessdate;
	String messageCount; 
	int count;
	String lang="E";
	boolean langflag=false;

	IVpmsBaseSession session = null;
	VpmsComputeResult result;
	IVpmsBaseSessionFactory factory = new VpmsJniSessionFactory();
	
	if(AppConfig.getVpmsXeServer().equalsIgnoreCase("true")){
		factory = new VpmsTcpSessionFactory(AppConfig.getVpmsXeServerIp(), AppConfig.getVpmsXeServerPort());
	}
	session= factory.create(AppConfig.getVpmsModelDirectory()+"EXTUW/INTLIFE.VPM");
	
	if(AppVars.getInstance().getBusinessdate()!=null && !AppVars.getInstance().getBusinessdate().equals("")){
		String[] aList = AppVars.getInstance().getBusinessdate().split("/");
		businessdate=aList[2]+aList[1]+aList[0];
	}
	else
	{
		businessdate=AppConfig.getVpmsTransEffDate();
	} 
	
	sv.answerOut[varcom.nd.toInt()].set("N");
	sv.language.set(wsspcomn.language);
	sv.subfileFields.set(SPACES);
	wsaaLinesOnPage.set(13); //IBPLIFE-10049
	
	session.setAttribute("Input TransEffDate", businessdate);
	session.setAttribute("Input Calling System", "");
	session.setAttribute("Input Calling Environment", "");
	session.setAttribute("Input Region", "");
	session.setAttribute("Input Locale", "");

	session.setAttribute("Input Contract Type", chdrlnbIO.getCnttype().toString());
	session.setAttribute("Input Sex", cltsIO.getCltsex().toString());
	session.setAttribute("Input Age", lifelnbIO.anbAtCcd.toString());
	//ILIFE-6873 start
	langflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBEXTV02", appVars, "IT");
	if(langflag){
		lang="F";
	}
	session.setAttribute("Input Language", lang);
	//ILIFE-6873 end
	session.setAttribute("Input Coverage Code", chdrlnbIO.getCnttype().toString().trim()+"1");
	
	
	result = session.computeUsingDefaults("Output Question Count"); 
	
	messageCount=session.compute("Output Question Count").getResult(); 
	count=Integer.parseInt(messageCount);
	
	for(int i=0;i<count;i++){
		result = session.computeUsingDefaults("Output Question ID("+i+")");
		
		VpmsComputeResult result1 = session.computeUsingDefaults("Output Question text line 1("+i+")");
		
		VpmsComputeResult result2 = session.computeUsingDefaults("Output Question text line 2("+i+")");
			
		VpmsComputeResult result3 = session.computeUsingDefaults("Output Question text line 3("+i+")");
		
		VpmsComputeResult result4 = session.computeUsingDefaults("Output Question text line 4("+i+")"); 
		
		VpmsComputeResult result5 = session.computeUsingDefaults("Output Question Status("+i+")"); 
		
		VpmsComputeResult result6 = session.computeUsingDefaults("Output Question ID("+i+1+")");
		
		VpmsComputeResult result7 = session.computeUsingDefaults("Output Question Type("+i+")");
	
		sv.questidf.set(result.getResult());
		sv.questtyp.set(result7.getResult()); 

		//ILIFE-5659 starts
		undqIO.setParams(SPACES);
		undqIO.setChdrcoy(lifelnbIO.getChdrcoy());
		undqIO.setChdrnum(lifelnbIO.getChdrnum());
		undqIO.setLife(lifelnbIO.getLife());
		undqIO.setJlife(lifelnbIO.getJlife());
		undqIO.setQuestidf(sv.questidf); 
		undqIO.setFormat(UNDQREC);
		undqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, undqIO);
		if (isNE(undqIO.getStatuz(),varcom.oK)
		&& isNE(undqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(undqIO.getParams());
			syserrrec.statuz.set(undqIO.getStatuz());
			fatalError600();
		}
		if (isEQ(undqIO.getStatuz(),varcom.oK)) {
			sv.answer.set(undqIO.getAnswer());
		}
		else {
			sv.answer.set(SPACES);
		}
		//ILIFE-5659 ends

		if (isEQ(result5.getResult(),"M")) {
			sv.mandques.set("Y");
		}		
		
		if (isGT(scrnparams.subfileRrn,wsaaSubfilePage)) {
			wsaaSubfilePage.add(wsaaLinesOnPage);
		}
		compute(wsaaScrnLinesLeft, 0).set(sub(wsaaSubfilePage,scrnparams.subfileRrn));
		if (isNE(sv.questidf.toString().trim(),"")
		&& isEQ(wsaaScrnLinesLeft,3)
		&& isNE(result4.getResult().trim(),"")) {
			wsaaSubfileFields.set(sv.subfileFields);
			for (int loopVar1 = 0; !(loopVar1 == 3); loopVar1 += 1){
				addBlankLine1280();
			}
			sv.subfileFields.set(wsaaSubfileFields);
		}
		if (isNE(sv.questidf.toString().trim(),"")
		&& isEQ(wsaaScrnLinesLeft,2)
		&& isNE(result3.getResult().trim(),"")) {
			wsaaSubfileFields.set(sv.subfileFields);
			for (int loopVar2 = 0; !(loopVar2 == 2); loopVar2 += 1){
				addBlankLine1280();
			}
			sv.subfileFields.set(wsaaSubfileFields);
		}
		if (isNE(sv.questidf.toString().trim(),"")
		&& isEQ(wsaaScrnLinesLeft,1)
		&& isNE(result2.getResult().trim(),"")) {
			wsaaSubfileFields.set(sv.subfileFields);
			addBlankLine1280();
			sv.subfileFields.set(wsaaSubfileFields);
		}
		sv.question.set(result1.getResult());
		sv.answerOut[varcom.nd.toInt()].set("N");
		addSubfileRecord1250();
		if (isEQ(result2.getResult().trim(),"")) {
			addBlankLine1280();
			continue;
		}
		sv.subfileFields.set(SPACES);
		sv.question.set(result2.getResult());
		sv.answerOut[varcom.nd.toInt()].set("Y");
		addSubfileRecord1250();
		if (isEQ(result3.getResult().trim(),"")) {
			addBlankLine1280();
			continue;
		}
		sv.subfileFields.set(SPACES);
		sv.answerOut[varcom.nd.toInt()].set("Y");
		sv.question.set(result3.getResult());
		addSubfileRecord1250();
		if (isEQ(result4.getResult().trim(),"")) {
			if (isEQ(result6.getResult().trim(),"")) {
				continue;
			}
			else {
				addBlankLine1280();
				continue;
			}
		}
		sv.subfileFields.set(SPACES);
		sv.question.set(result4.getResult());
		sv.answerOut[varcom.nd.toInt()].set("Y");
		addSubfileRecord1250();
		if (isEQ(result6.getResult().trim(),"")) {
			continue;
		}
		else {
			addBlankLine1280();
			continue;
		}

	}
}


protected void readT67711070()
	{
	
	boolean extValidation = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBEXTV01", appVars, "IT");
	if(AppVars.getInstance().getAppConfig().isVpmsEnable() && extValidation && (isEQ(chdrlnbIO.cnttype,"RUL") || isEQ(chdrlnbIO.cnttype,"TEN") || isEQ(chdrlnbIO.cnttype,"ULP")))
	{
		scrnparams.subfileRrn.set(1);
	}
	else
	{
			/*itdmIO.setParams(SPACES);
			itdmIO.setItempfx(smtpfxcpy.item);
			itdmIO.setItemcoy(wsspcomn.company);
			itdmIO.setItemtabl(t6771);
			itdmIO.setItemitem(wsaaQuestset);
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				fatalError600();
			}*/
			List<Itempf> items = itemDAO.getItdmByFrmdate(wsspcomn.company.toString(), T6771, wsaaQuestset.toString(), chdrlnbIO.getOccdate().toInt());
			if (items.isEmpty()) {
				fatalError600();
			}
			t6771rec.t6771Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
			sv.subfileFields.set(SPACES);
			wsaaLinesOnPage.set(13); //IBPLIFE-10049
			for (sub1.set(1); !(isGT(sub1,40)
			|| isEQ(t6771rec.questidf[sub1.toInt()],SPACES)); sub1.add(1)){
				loadSubfile1200();
			}
			scrnparams.subfileRrn.set(1);
		}
	}
    /* ILIFE-5536(LIFE VPMS Externalization - Code promotion for Life Underwriting calculations) end */

protected void loadSubfile1200()
	{
		try {
			start1210();
		}
		catch (GOTOException e){
		}
	}

protected void start1210()
	{
		sv.answerOut[varcom.nd.toInt()].set("N");
		sv.language.set(wsspcomn.language);
		sv.questidf.set(t6771rec.questidf[sub1.toInt()]);
		/*itdmIO.setParams(SPACES);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6770);*/
		wsaaT6770Lang.set(wsspcomn.language);
		wsaaT6770Questidf.set(t6771rec.questidf[sub1.toInt()]);
/*		itdmIO.setItemitem(wsaaT6770Key);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t6770)
		|| isNE(itdmIO.getItemitem(),wsaaT6770Key)) {
			scrnparams.errorCode.set(e595);
		}
		else {
			t6770rec.t6770Rec.set(itdmIO.getGenarea());
		}*/
		List<Itempf> items = itemDAO.getItdmByFrmdate(wsspcomn.company.toString(), T6770, wsaaT6770Key.toString(), chdrlnbIO.getOccdate().toInt());
		if (items.isEmpty()) {
			fatalError600();
		}
		t6770rec.t6770Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
		fillQuesMap();
		sv.undwrule.set(t6770rec.undwrule);
		sv.answband.set(t6770rec.answband);
		wsaaQuesttypFlag.set("N");
		if (isNE(t6770rec.questtyp,SPACES)) {
			for (sub.set(1); !(isGT(sub,10)
			|| isEQ(wsaaQuesttype[sub.toInt()],SPACES)); sub.add(1)){
				if (isEQ(t6770rec.questtyp,wsaaQuesttype[sub.toInt()])) {
					questtypFound.setTrue();
					sv.questtyp.set(t6770rec.questtyp);
				}
			}
		}
		if (isNE(t6770rec.questtyp,SPACES)
		&& !questtypFound.isTrue()) {
/*			itemIO.setParams(SPACES);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(t6776);
			itemIO.setItemitem(t6770rec.questtyp);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
			t6776rec.t6776Rec.set(itemIO.getGenarea());*/
			Itempf item = itemDAO.findItemByItem(wsspcomn.company.toString(), T6776, t6770rec.questtyp.toString());
			if(item == null) {
				fatalError600();
			}
			t6776rec.t6776Rec.set(StringUtil.rawToString(item.getGenarea()));
			qtyp.add(1);
			wsaaQuesttype[qtyp.toInt()].set(t6770rec.questtyp);
			wsaaIntgflag[qtyp.toInt()].set(t6776rec.intgflag);
			wsaaBoolansw01[qtyp.toInt()].set(t6776rec.boolansw01);
			wsaaBoolansw02[qtyp.toInt()].set(t6776rec.boolansw02);
			wsaaRulechck01[qtyp.toInt()].set(t6776rec.rulechck01);
			wsaaRulechck02[qtyp.toInt()].set(t6776rec.rulechck02);
			sv.questtyp.set(t6770rec.questtyp);
		}
		undqIO.setParams(SPACES);
		undqIO.setChdrcoy(lifelnbIO.getChdrcoy());
		undqIO.setChdrnum(lifelnbIO.getChdrnum());
		undqIO.setLife(lifelnbIO.getLife());
		undqIO.setJlife(lifelnbIO.getJlife());
		undqIO.setQuestidf(t6771rec.questidf[sub1.toInt()]);
		undqIO.setFormat(UNDQREC);
		undqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, undqIO);
		if (isNE(undqIO.getStatuz(),varcom.oK)
		&& isNE(undqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(undqIO.getParams());
			syserrrec.statuz.set(undqIO.getStatuz());
			fatalError600();
		}
		if (isEQ(undqIO.getStatuz(),varcom.oK)) {
			sv.answer.set(undqIO.getAnswer());
		}
		else {
			sv.answer.set(SPACES);
		}
		if (isNE(t6771rec.questst[sub1.toInt()],SPACES)) {
/*			itemIO.setParams(SPACES);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(t6778);
			itemIO.setItemitem(t6771rec.questst[sub1.toInt()]);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
			t6778rec.t6778Rec.set(itemIO.getGenarea());*/
			Itempf item = itemDAO.findItemByItem(wsspcomn.company.toString(), T6778, t6771rec.questst[sub1.toInt()].toString());
			if(item == null) {
				fatalError600();
			}
			t6778rec.t6778Rec.set(StringUtil.rawToString(item.getGenarea()));
			if (isNE(t6778rec.mandques,SPACES)) {
				sv.mandques.set("Y");
			}
		}
		if (isGT(scrnparams.subfileRrn,wsaaSubfilePage)) {
			wsaaSubfilePage.add(wsaaLinesOnPage);
		}
		compute(wsaaScrnLinesLeft, 0).set(sub(wsaaSubfilePage,scrnparams.subfileRrn));
		if (isNE(sv.questidf,SPACES)
		&& isEQ(wsaaScrnLinesLeft,3)
		&& isNE(t6770rec.question04,SPACES)) {
			wsaaSubfileFields.set(sv.subfileFields);
			for (int loopVar1 = 0; !(loopVar1 == 3); loopVar1 += 1){
				addBlankLine1280();
			}
			sv.subfileFields.set(wsaaSubfileFields);
		}
		if (isNE(sv.questidf,SPACES)
		&& isEQ(wsaaScrnLinesLeft,2)
		&& isNE(t6770rec.question03,SPACES)) {
			wsaaSubfileFields.set(sv.subfileFields);
			for (int loopVar2 = 0; !(loopVar2 == 2); loopVar2 += 1){
				addBlankLine1280();
			}
			sv.subfileFields.set(wsaaSubfileFields);
		}
		if (isNE(sv.questidf,SPACES)
		&& isEQ(wsaaScrnLinesLeft,1)
		&& isNE(t6770rec.question02,SPACES)) {
			wsaaSubfileFields.set(sv.subfileFields);
			addBlankLine1280();
			sv.subfileFields.set(wsaaSubfileFields);
		}
		sv.question.set(t6770rec.question01.trim());
		sv.answerOut[varcom.nd.toInt()].set("N");
		addSubfileRecord1250();
		if (isEQ(t6770rec.question02,SPACES)) {
			addBlankLine1280();
			goTo(GotoLabel.exit1290);
		}
		sv.subfileFields.set(SPACES);
		sv.question.set(t6770rec.question02.trim());
		sv.answerOut[varcom.nd.toInt()].set("Y");
		addSubfileRecord1250();
		if (isEQ(t6770rec.question03,SPACES)) {
			addBlankLine1280();
			goTo(GotoLabel.exit1290);
		}
		sv.subfileFields.set(SPACES);
		sv.answerOut[varcom.nd.toInt()].set("Y");
		sv.question.set(t6770rec.question03.trim());
		addSubfileRecord1250();
		if (isEQ(t6770rec.question04,SPACES)) {
			if (isEQ(t6771rec.questidf[add(sub1,1).toInt()],SPACES)) {
				goTo(GotoLabel.exit1290);
			}
			else {
				addBlankLine1280();
				goTo(GotoLabel.exit1290);
			}
		}
		sv.subfileFields.set(SPACES);
		sv.question.set(t6770rec.question04);
		sv.answerOut[varcom.nd.toInt()].set("Y");
		addSubfileRecord1250();
		if (isEQ(t6771rec.questidf[add(sub1,1).toInt()],SPACES)) {
			goTo(GotoLabel.exit1290);
		}
		else {
			addBlankLine1280();
			goTo(GotoLabel.exit1290);
		}
	}

protected void fillQuesMap() {
	// TODO Auto-generated method stub
	
}

protected void addSubfileRecord1250()
	{
		/*INIT*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S6765", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(sub1,1)
		&& isEQ(sv.answerOut[varcom.nd.toInt()],"N")) {
			wsaaRrn.set(scrnparams.subfileRrn);
		}
		/*EXIT*/
	}

protected void addBlankLine1280()
	{
		try {
			start1281();
		}
		catch (GOTOException e){
		}
	}

protected void start1281()
	{
		compute(wsaaLineTest, 0).setDivide(scrnparams.subfileRrn, (wsaaSubfilePage));
		wsaaRemainder.setRemainder(wsaaLineTest);
		if (isEQ(wsaaRemainder,ZERO)) {
			goTo(GotoLabel.exit1289);
		}
		sv.subfileFields.set(SPACES);
		sv.answerOut[varcom.nd.toInt()].set("Y");
		addSubfileRecord1250();
	}

protected void cltsio1900()
	{
		/*CLTSIO*/
		/*cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}*/
		/*EXIT*/
		cltsIO = clntpfDAO.getClntpf(fsupfxcpy.clnt.toString(), wsspcomn.fsuco.toString(), lifelnbIO.getLifcnum().toString());
		if (cltsIO == null) {
			fatalError600();
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		if (isEQ(wsaaRrn,ZERO)) {
			scrnparams.subfileRrn.set(1);
		}
		else {
			scrnparams.subfileRrn.set(wsaaRrn);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateSubfile2060();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE-SCREEN*/
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2060()
	{
		scrnparams.statuz.set(SPACES);
		questionCount=0;
		scrnparams.function.set(varcom.sstrt);
		vldt2060CustomerSpecific();
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			/* ILIFE-5536 (LIFE VPMS Externalization - Code promotion for Life Underwriting calculations)*/	
			boolean extValidation = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBEXTV01", appVars, "IT");
			if(AppVars.getInstance().getAppConfig().isVpmsEnable() && extValidation && (isEQ(chdrlnbIO.cnttype,"RUL") || isEQ(chdrlnbIO.cnttype,"TEN") || isEQ(chdrlnbIO.cnttype,"ULP")))
			{
				try {
					getValidationUW();
					updateErrorIndicators2670();
					processScreen("S6765", sv);
					if (isNE(scrnparams.statuz,varcom.oK)
					&& isNE(scrnparams.statuz,varcom.endp)) {
						syserrrec.statuz.set(scrnparams.statuz);
						fatalError600();
					}
					if (isEQ(scrnparams.statuz,varcom.endp)) {
						goTo(GotoLabel.exit2690);
					}
				}//IJTI-851-Overly Broad Catch 
				catch (VpmsLoadFailedException e) {
					LOGGER.error("Exception occurred", e);
				}
			}
			else
			{		
				validateSubfile2600();
			}
		}
		
	}

protected void vldt2060CustomerSpecific(){
	
}

protected void getValidationUW() throws VpmsLoadFailedException
{
	String businessdate;
	IVpmsBaseSession session = null;
	VpmsComputeResult result;
	IVpmsBaseSessionFactory factory = new VpmsJniSessionFactory();
	String lang="E";
	boolean langflag=false;

	processScreen("S6765", sv);
	if (isNE(scrnparams.statuz,varcom.oK)
	&& isNE(scrnparams.statuz,varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	sv.errorSubfile.set(SPACES);
	sv.errorIndicators.set(SPACES);
	if (isEQ(scrnparams.statuz,varcom.endp)) {
		goTo(GotoLabel.exit2690);
	}
	if (isEQ(sv.answer,SPACES)
	&& isEQ(sv.mandques,"Y")) {
		sv.answerErr.set(E962);
		return;
	}
	
	if(AppConfig.getVpmsXeServer().equalsIgnoreCase("true")){
		factory = new VpmsTcpSessionFactory(AppConfig.getVpmsXeServerIp(), AppConfig.getVpmsXeServerPort());
	}
	session= factory.create(AppConfig.getVpmsModelDirectory()+"EXTUW/INTLIFE.VPM");
	
	if(AppVars.getInstance().getBusinessdate()!=null && !AppVars.getInstance().getBusinessdate().equals("")){
		String[] aList = AppVars.getInstance().getBusinessdate().split("/");
		businessdate=aList[2]+aList[1]+aList[0];
	}
	else
	{
		businessdate=AppConfig.getVpmsTransEffDate();
	} 
	
	session.setAttribute("Input TransEffDate", businessdate);
	session.setAttribute("Input Calling System", "");
	session.setAttribute("Input Calling Environment", "");
	session.setAttribute("Input Region", "");
	session.setAttribute("Input Locale", "");

	session.setAttribute("Input Contract Type", chdrlnbIO.getCnttype().toString());
	session.setAttribute("Input Sex", cltsIO.getCltsex().toString());
	session.setAttribute("Input Age", lifelnbIO.anbAtCcd.toString());
	//ILIFE-6873 start
	langflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBEXTV02", appVars, "IT");
	if(langflag){
		lang="F";
	}
	session.setAttribute("Input Language", lang);
	//ILIFE-6873 end
	session.setAttribute("Input Coverage Code", chdrlnbIO.getCnttype().toString().trim()+"1");
	session.setAttribute("Input Question Answer["+questionCount+"]", sv.answer.toString().trim());
	

	result = session.computeUsingDefaults("Output Question Error Message("+questionCount+")");
	
	if(!(isEQ(sv.mandques.toString().trim(),"") && isEQ(result.getResult(),E962)))
		sv.answerErr.set(result.getResult());
	questionCount++;	
	
}
     /* ILIFE-5536(LIFE VPMS Externalization - Code promotion for Life Underwriting calculations) end */	

protected void validateSubfile2600()
	{
		try {
			validation2610();
			updateErrorIndicators2670();
		}
		catch (GOTOException e){
		}
	}

protected void validation2610()
	{
		processScreen("S6765", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit2690);
		}
		if (isEQ(sv.answer,SPACES)
		&& isEQ(sv.mandques,"Y")) {
			sv.answerErr.set(E962);
		}
		vldt2610CustomerSpecific();
		wsaaCheckFlag.set(SPACES);
		wsaaIntBoolFlag.set(SPACES);
		for (sub.set(1); !(isGT(sub,10)
		|| isEQ(wsaaQuesttype[sub.toInt()],SPACES)); sub.add(1)){
			if (isEQ(sv.questtyp,wsaaQuesttype[sub.toInt()])
			&& isNE(wsaaIntgflag[sub.toInt()],SPACES)) {
				integerType.setTrue();
			}
			if (isEQ(sv.questtyp,wsaaQuesttype[sub.toInt()])
			&& isEQ(wsaaIntgflag[sub.toInt()],SPACES)) {
				booleanType.setTrue();
			}
		}
		if (integerType.isTrue()
		&& isNE(sv.answer,SPACES)) {
			wsaaAnswerField.set(sv.answer);
			validateInteger2700();
			if (nonNumericFound.isTrue()) {
				sv.answerErr.set(E963);
			}
		}
		if (booleanType.isTrue()
		&& isNE(sv.answer,SPACES)) {
			validateBoolean2800();
		}
	}

protected void vldt2610CustomerSpecific(){
	
}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6765", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*READ-NEXT-MODIFIED-RECORD*/
		scrnparams.function.set(varcom.srdn);
	}

protected void validateInteger2700()
	{
		/*INIT*/
		wsaaAnswerField.set(inspectReplaceLeading(wsaaAnswerField, SPACES, ZERO));
		wsaaAnswerR[4].set(wsaaAnswerI[1]);
		wsaaAnswerR[3].set(wsaaAnswerI[2]);
		wsaaAnswerR[2].set(wsaaAnswerI[3]);
		wsaaAnswerR[1].set(wsaaAnswerI[4]);
		wsaaAnswerRev.set(inspectReplaceLeading(wsaaAnswerRev, SPACES, ZERO));
		if (isNE(wsaaAnswerRev,NUMERIC)) {
			nonNumericFound.setTrue();
		}
		/*EXIT*/
	}

protected void validateBoolean2800()
	{
		/*INIT*/
		wsaaAnswerValFlag.set(SPACES);
		for (sub.set(1); !(isGT(sub,10)
		|| isEQ(wsaaQuesttype[sub.toInt()],SPACES)); sub.add(1)){
			if (isEQ(sv.questtyp,wsaaQuesttype[sub.toInt()])) {
				if (isNE(sv.answer,wsaaBoolansw01[sub.toInt()])
				&& isNE(sv.answer,wsaaBoolansw02[sub.toInt()])) {
					invalidAnswer.setTrue();
				}
			}
		}
		if (invalidAnswer.isTrue()) {
			sv.answerErr.set(E960);
		}
		/*EXIT*/
	}

protected void update3000(){
	try {
		if(uwFlag){
			ThreadLocalStore.put(UWQUESTIONNAIRECACHE, null);
			questions = new ArrayList<String>();
			undqList = undqpfDAO.searchUnqpf(lifelnbIO.getChdrcoy().toString().trim(), lifelnbIO.getChdrnum().toString().trim(), 
					lifelnbIO.getLife().toString().trim(), "00");
			filterUnqqList();
		}
		updateDatabase3010();
		if(uwFlag){
			if(!questions.isEmpty()){
				ThreadLocalStore.put(UWQUESTIONNAIRECACHE, questions);
			}
		}
	}
	catch (GOTOException e){
	}
}
private void filterUnqqList(){
	undqResultList = new ArrayList<String>();
	for(Undqpf u : undqList){
		if("Y".equals(u.getAnswer().trim())){
			undqResultList.add(u.getQuestidf());
		}
	}
}
protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
		scrnparams.function.set(varcom.sstrt);
		processScreen("S6765", sv);
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			processSubfile3100();
		}
		
		updateUndq3200();
		if(uwFlag && (isEQ(wsspcomn.flag,"M") || isEQ(wsspcomn.flag,"C"))){
			undqResultList.sort(new Comparator<String>() {
				@Override
				public int compare(String o1, String o2) {
					return o1.compareTo(o2);
				}
			});
			questions.sort(new Comparator<String>() {
				@Override
				public int compare(String o1, String o2) {
					return o1.compareTo(o2);
				}
			});
			if(undqResultList.size()!=questions.size() || !undqResultList.containsAll(questions)) {
				lextpfDAO.deleteByAutoRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString(),"Q");
				updateLext();
			}
		}
	}

private void updateLext(){
	List<Covtpf> covtpfList=covtpfDAO.searchCovtRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
	UWQuestionnaireRec uwQuestionnaireRecReturn = null;
	UWQuestionnaireRec uwQuestionnaireRec = new UWQuestionnaireRec();
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	uwQuestionnaireRec.setTransEffdate(datcon1rec.intDate.getbigdata());
	uwQuestionnaireRec.setContractType(chdrlnbIO.getCnttype().toString());
	int index = lextpfDAO.getMaxSeqnbr(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
	for(Covtpf covtpf : covtpfList){
		for (String question : questions) {
			uwQuestionnaireRecReturn = uwQuestionnaireUtil.getUWMessage(uwQuestionnaireRec, wsspcomn.language.toString(), 
					chdrlnbIO.getChdrcoy().toString(),question, covtpf.getCrtable(),"");
			if(!uwQuestionnaireRecReturn.getOutputUWDec().isEmpty() && !uwQuestionnaireRecReturn.getOutputUWDec().contains("Accepted")){
				lextIO.setParams(SPACES);

				lextIO.setChdrcoy(covtpf.getChdrcoy());
				lextIO.setChdrnum(covtpf.getChdrnum());
				lextIO.setLife(covtpf.getLife());
				lextIO.setCoverage(covtpf.getCoverage());
				lextIO.setRider(covtpf.getRider());
				lextIO.setFormat("LEXTREC");
				lextIO.setSeqnbr(index+1);
				lextIO.setValidflag("1");
				lextIO.setCurrfrom(chdrlnbIO.getOccdate());
				lextIO.setCurrto(varcom.vrcmMaxDate);
				lextIO.setUwoverwrite("Y");
				lextIO.setAgerate(uwQuestionnaireRecReturn.getOutputSplTermAge().get(0));
				lextIO.setInsprm(uwQuestionnaireRecReturn.getOutputSplTermRateAdj().get(0));
				lextIO.setOppc(uwQuestionnaireRecReturn.getOutputSplTermLoadPer().get(0));
				lextIO.setOpcda(uwQuestionnaireRecReturn.getOutputSplTermCode().get(0));
				lextIO.setZnadjperc(uwQuestionnaireRecReturn.getOutputSplTermSAPer().get(0));
				lextIO.setZmortpct(uwQuestionnaireRecReturn.getOutputMortPerc().get(0));
				lextIO.setExtCessTerm(SPACE);
				lextIO.setPremadj(SPACE);
				lextIO.setReasind(SPACE);
				lextIO.setJlife("00");
				itemIO.setItempfx("IT");
				itemIO.setItemcoy(covtpf.getChdrcoy());
				itemIO.setItemtabl("T5657");
				wsaaT5657key1.set(covtpf.getCrtable());
				wsaaT5657key2.set(uwQuestionnaireRecReturn.getOutputSplTermCode().get(0));
				itemIO.setItemitem(wsaaT5657key);
				itemIO.setFunction(varcom.readr);
				itemIO.setFormat(itemrec);
				SmartFileCode.execute(appVars, itemIO);
//				if (isNE(itemIO.getStatuz(),varcom.oK)) {
//					syserrrec.params.set(itemIO.getParams());
//					fatalError600();
//				}
				fatalException(itemIO);
				t5657rec.t5657Rec.set(itemIO.getGenarea());
				lextIO.setSbstdl(t5657rec.sbstdl);
				lextIO.setTermid(varcom.vrcmTermid);
				lextIO.setTransactionDate(varcom.vrcmDate);
				lextIO.setTransactionTime(varcom.vrcmTime);
				lextIO.setUser(varcom.vrcmUser);
				lextIO.setFunction(varcom.updat);
				SmartFileCode.execute(appVars, lextIO);
//				if (isNE(lextIO.getStatuz(),varcom.oK)) {
//					syserrrec.params.set(lextIO.getParams());
//					fatalError600();
//				}
				fatalException(lextIO);
				index += 1;
			}

		}
	}
}
private void fatalException(PFAdapterDAM tableDAM){
	if (isNE(tableDAM.getStatuz(),varcom.oK)) {
		syserrrec.params.set(tableDAM.getParams());
		fatalError600();
	}
}
protected void processSubfile3100()
	{
		/*UPDATE-UNDQ*/
		if (isNE(sv.questidf,SPACES)) {
			updateUndq3200();
		}
		scrnparams.function.set(varcom.srdn);
		processScreen("S6765", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void updateUndq3200()
	{
		try {
			writeUndq3210();
			callUndqio3280();
		}
		catch (GOTOException e){
		}
	}

protected void writeUndq3210()
	{
		undqIO.setParams(SPACES);
		undqIO.setChdrcoy(lifelnbIO.getChdrcoy());
		undqIO.setChdrnum(lifelnbIO.getChdrnum());
		undqIO.setLife(lifelnbIO.getLife());
		undqIO.setJlife(lifelnbIO.getJlife());
		undqIO.setQuestidf(sv.questidf);
		undqIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, undqIO);
		if (isNE(undqIO.getStatuz(),varcom.oK)
		&& isNE(undqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(undqIO.getParams());
			fatalError600();
		}
		wsaaIntBoolFlag.set(SPACES);
		wsaaUndwRuleFlag.set("N");
		for (sub.set(1); !(isGT(sub,10)
		|| isEQ(wsaaQuesttype[sub.toInt()],SPACES)); sub.add(1)){
			if (isEQ(sv.questtyp,wsaaQuesttype[sub.toInt()])) {
				if (isNE(wsaaIntgflag[sub.toInt()],SPACES)) {
					integerType.setTrue();
				}
				else {
					booleanType.setTrue();
				}
				if (booleanType.isTrue()) {
					if (isEQ(sv.answer,wsaaBoolansw01[sub.toInt()])
					&& isNE(wsaaRulechck01[sub.toInt()],SPACES)) {
						applyUndwRule.setTrue();
					}
					if (isEQ(sv.answer,wsaaBoolansw02[sub.toInt()])
					&& isNE(wsaaRulechck02[sub.toInt()],SPACES)) {
						applyUndwRule.setTrue();
					}
				}
			}
		}
		if (isEQ(undqIO.getStatuz(),varcom.oK)) {
			if (isEQ(undqIO.getAnswer(),sv.answer)) {
				if(isEQ(wsspcomn.flag,"M") && isEQ(sv.answer,"Y")){
					questions.add(sv.questidf.toString());
				}
				goTo(GotoLabel.exit3290);
			}
			else {
				if (isEQ(sv.answer,SPACES)
				&& isNE(sv.mandques,"Y")) {
					undqIO.setFormat(UNDQREC);
					undqIO.setFunction(varcom.delet);
				}
				else {
					wsaaAnswerField.set(sv.answer);
					formatAnswer3700();
					undqIO.setAnswer(wsaaAnswerOut);
					if (integerType.isTrue()) {
						if (isNE(wsaaAnswerField,"0")) {
							checkAnswer3400();
						}
						else {
							undqIO.setUndwrule(SPACES);
						}
					}
					if (booleanType.isTrue()) {
						if (applyUndwRule.isTrue()) {
							checkAnswer3400();
						}
						else {
							undqIO.setUndwrule(SPACES);
						}
					}
					undqIO.setFunction(varcom.rewrt);
				}
			}
		}
		if (isEQ(undqIO.getStatuz(),varcom.mrnf)) {
			if (isEQ(sv.answer,SPACES)
			&& isNE(sv.mandques,"Y")) {
				goTo(GotoLabel.exit3290);
			}
			else {
				undqIO.setChdrcoy(lifelnbIO.getChdrcoy());
				undqIO.setChdrnum(lifelnbIO.getChdrnum());
				undqIO.setLife(sv.life);
				undqIO.setJlife(sv.jlife);
				undqIO.setQuestidf(sv.questidf);
				undqIO.setCurrfrom(lifelnbIO.getCurrfrom());
				undqIO.setCurrto(lifelnbIO.getCurrto());
				undqIO.setAnswer(sv.answer);
				undqIO.setUndwrule(sv.undwrule);
				undqIO.setQuestidf(sv.questidf);
				undqIO.setAnswer(sv.answer);
				if (isNE(sv.answer,"0")
				&& isNE(sv.answer,"N")) {
					wsaaAnswerField.set(sv.answer);
					formatAnswer3700();
					undqIO.setAnswer(wsaaAnswerOut);
					checkAnswer3400();
				}
				else {
					undqIO.setUndwrule(SPACES);
				}
				undqIO.setTranno(lifelnbIO.getTranno());
				undqIO.setValidflag(lifelnbIO.getValidflag());
				undqIO.setFunction(varcom.writr);			
			}
		}		
	}

protected void callUndqio3280()
	{
		undqIO.setFormat(UNDQREC);
		SmartFileCode.execute(appVars, undqIO);
		if (isNE(undqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(undqIO.getParams());
			syserrrec.statuz.set(undqIO.getStatuz());
			fatalError600();
		}
	}

protected void checkAnswer3400()
	{
		/*INIT*/
		for (sub.set(1); !(isGT(sub,10)
		|| isEQ(wsaaQuesttype[sub.toInt()],SPACES)); sub.add(1)){
			if (isEQ(sv.questtyp,wsaaQuesttype[sub.toInt()])) {
				if (isNE(wsaaIntgflag[sub.toInt()],SPACES)
				&& isNE(sv.answband,SPACES)) {
					wsaaT6775Questidf.set(sv.questidf);
					wsaaT6775Band.set(sv.answband);
					readT67753500();
				}
			}
		}
		undqIO.setUndwrule(sv.undwrule);
		/*EXIT*/
		if(uwFlag){
			questions.add(sv.questidf.toString());
		}
	}

protected void readT67753500()
	{
		/*INIT*/
//		itdmIO.setParams(SPACES);
		wsaaItemtabl.set(T6775);
		wsaaItemitem.set(wsaaT6775Key);
		wsaaUndwrule.set(SPACES);
		wsaaX.set(1);
		sortoutNumber3600();
		wsaaAnswerFlag.set("N");
		while ( !(answRangeFound.isTrue())) {
			determineRule3550();
		}
		
		/*EXIT*/
	}

protected void determineRule3550()
	{
		try {
			init3551();
		}
		catch (GOTOException e){
		}
	}

protected void init3551()
	{
		x100ReadItdm();
		check_additional_condn();
		for (wsaaX.set(1); !(isGT(wsaaX,24)); wsaaX.add(1)){
			if (isGTE(wsaaAnswerNum,t6775rec.intgfrom[wsaaX.toInt()])
			&& isLTE(wsaaAnswerNum,t6775rec.intgto[wsaaX.toInt()])) {
				sv.undwrule.set(t6775rec.undwrule[wsaaX.toInt()]);
				answRangeFound.setTrue();
				wsaaX.set(25);
			}
		}
		if (answRangeFound.isTrue()) {
			goTo(GotoLabel.exit3559);
		}
		if (isNE(t6775rec.cont,SPACES)) {
			wsaaItemtabl.set(T6775);
			wsaaItemitem.set(t6775rec.cont);
		}
		else {
			//syserrrec.params.set(itdmIO.getDataArea());
			syserrrec.statuz.set("MRNF");
			fatalError600();
		}
	}

protected void check_additional_condn(){
	
}

protected void sortoutNumber3600()
	{
		init3610();
	}

protected void init3610()
	{
		wsaaAnswerField.set(inspectReplaceLeading(wsaaAnswerField, SPACES, ZERO));
		wsaaNumSub.set(4);
		wsaaAnswerNum.set(ZERO);
		if (isNE(wsaaAnswerI[4],SPACES)) {
			wsaaAnswerN[wsaaNumSub.toInt()].set(wsaaAnswerI[4]);
			wsaaNumSub.subtract(1);
		}
		if (isNE(wsaaAnswerI[3],SPACES)) {
			wsaaAnswerN[wsaaNumSub.toInt()].set(wsaaAnswerI[3]);
			wsaaNumSub.subtract(1);
		}
		if (isNE(wsaaAnswerI[2],SPACES)) {
			wsaaAnswerN[wsaaNumSub.toInt()].set(wsaaAnswerI[2]);
			wsaaNumSub.subtract(1);
		}
		if (isNE(wsaaAnswerI[1],SPACES)) {
			wsaaAnswerN[wsaaNumSub.toInt()].set(wsaaAnswerI[1]);
			wsaaNumSub.subtract(1);
		}
	}

protected void formatAnswer3700()
	{
		init3710();
	}

protected void init3710()
	{
		wsaaAnswerOut.set(SPACES);
		wsaaNumSub.set(1);
		if (isNE(wsaaAnswerI[1],SPACES)) {
			wsaaAnswerOuti[wsaaNumSub.toInt()].set(wsaaAnswerI[1]);
			wsaaNumSub.add(1);
		}
		if (isNE(wsaaAnswerI[2],SPACES)) {
			wsaaAnswerOuti[wsaaNumSub.toInt()].set(wsaaAnswerI[2]);
			wsaaNumSub.add(1);
		}
		if (isNE(wsaaAnswerI[3],SPACES)) {
			wsaaAnswerOuti[wsaaNumSub.toInt()].set(wsaaAnswerI[3]);
			wsaaNumSub.add(1);
		}
		if (isNE(wsaaAnswerI[4],SPACES)) {
			wsaaAnswerOuti[wsaaNumSub.toInt()].set(wsaaAnswerI[4]);
			wsaaNumSub.add(1);
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void x100ReadItdm()
	{
		x110Init();
	}

protected void x110Init()
	{
/*		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(wsaaItemtabl);
		itdmIO.setItemitem(wsaaItemitem);
		itdmIO.setItmfrm(lifelnbIO.getCurrfrom());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			itdmIO.setParams(syserrrec.params);
			itdmIO.setStatuz(syserrrec.statuz);
			fatalError600();
		}*/
		List<Itempf> items = itemDAO.getItdmByFrmdate(wsspcomn.company.toString(), wsaaItemtabl.toString(), wsaaItemitem.toString(), lifelnbIO.getCurrfrom().toInt());
		if (items.isEmpty()) {
			if(isNE(wsaaT6775Band,SPACE) ){
				changeT6775Item();
				wsaaItemitem.set(wsaaT6775Key);
				items = itemDAO.getItdmByFrmdate(wsspcomn.company.toString(), wsaaItemtabl.toString(), wsaaItemitem.toString(), lifelnbIO.getCurrfrom().toInt());
				if (items.isEmpty()) {
					fatalError600();
				}
			}else{
				fatalError600();
			}
		}
		t6775rec.t6775Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
	}
private void changeT6775Item(){
	for(int i=0; i<4;i++){
		if(wsaaT6775Answband.toString().trim().length() <4){
			wsaaT6775Answband.set("0"+wsaaT6775Answband.toString().trim());
		}
	}
}
public T6770rec getT6770rec() {
	return new T6770rec();
}

public void setT6770rec(T6770rec t6770rec) {
	this.t6770rec = t6770rec;
}
}
