package com.csc.life.underwriting.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:32
 * Description:
 * Copybook name: UNDCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Undckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData undcFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData undcKey = new FixedLengthStringData(256).isAPartOf(undcFileKey, 0, REDEFINE);
  	public FixedLengthStringData undcChdrcoy = new FixedLengthStringData(1).isAPartOf(undcKey, 0);
  	public FixedLengthStringData undcChdrnum = new FixedLengthStringData(8).isAPartOf(undcKey, 1);
  	public FixedLengthStringData undcLife = new FixedLengthStringData(2).isAPartOf(undcKey, 9);
  	public FixedLengthStringData undcCoverage = new FixedLengthStringData(2).isAPartOf(undcKey, 11);
  	public FixedLengthStringData undcRider = new FixedLengthStringData(2).isAPartOf(undcKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(241).isAPartOf(undcKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(undcFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		undcFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}