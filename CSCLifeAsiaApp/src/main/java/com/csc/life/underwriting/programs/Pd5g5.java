package com.csc.life.underwriting.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.recordstructures.Rskamtrec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.Td5g4rec;
import com.csc.life.productdefinition.tablestructures.Td5gnrec;
import com.csc.life.reassurance.tablestructures.T5446rec;
import com.csc.life.underwriting.screens.Sd5g5ScreenVars;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
/******
 * 
 * 
	Life Insured Accumulated Risk Amount
	
	Screen is created for calculation of Risk amount based on the Table configuration provided in TD5G4.

 *
 *
 ******/

public class Pd5g5 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final Logger LOGGER = LoggerFactory.getLogger(Pd5g5.class);
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4);
	private BinaryData wsaaSflct = new BinaryData(5, 0);
	private FixedLengthStringData wsaaPresSeq = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPrevSeq = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5G5");
	private FixedLengthStringData wsaatranCode = new FixedLengthStringData(5).init("TAFG");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");	
	private Batckey wsaaBatckey = new Batckey();
	private FixedLengthStringData wsaaValid = new FixedLengthStringData(1);
	private Double wsaaRiskAmtTemp=0.0;
	private ZonedDecimalData wsaaTotRiskamt = new ZonedDecimalData(17,2);
	private ZonedDecimalData wsaaCalcRiskamt = new ZonedDecimalData(17,2);
	private String t5679 = "T5679";
	private String td5g4 = "TD5G4";
	private String td5gn = "TD5GN";
	private String t5446 = "T5446";
	private T5679rec t5679rec = new T5679rec();
	private Td5g4rec td5g4rec = new Td5g4rec();
	private T5446rec t5446rec = new T5446rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
    private FixedLengthStringData filler = new FixedLengthStringData(767).isAPartOf(wsspsmart.userArea, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsspLrkcls = new FixedLengthStringData(4).isAPartOf(filler, 3);
	private PackedDecimalData wsspTotsi = new PackedDecimalData(17, 2).isAPartOf(filler, 7);
	private Optswchrec optswchrec = new Optswchrec();
	private Sd5g5ScreenVars sv = ScreenProgram.getScreenVars( Sd5g5ScreenVars.class);
	private Clntpf clntpf = new Clntpf();
	private Chdrpf chdrpf = new Chdrpf();
	private List<Covrpf> covrpfList = null;
	private List<Covtpf> covtpfList = null;
	private Descpf descpf =null;
	private List<Itempf> itemList= new ArrayList<>();
	private List<Itempf> itemT5446= new ArrayList<>();
	private List<Lifepf> lifepfList= new ArrayList<>();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO",ClntpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO",LifepfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private CovtpfDAO covtpfDAO = getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	private DescDAO descDao = getApplicationContext().getBean("descDAO", DescDAO.class); 
	private Rskamtrec riskAmtRec= new Rskamtrec();
	private Td5gnrec td5gnRec = new Td5gnrec();
	private Map<String,Double> riskAmtMap= new HashMap<>();
	private Map<String,String> riskFormulaMap= new HashMap<>();
	private String g615 = "G615";
	public Pd5g5() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5g5", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
		}
	
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
		}
	@Override
	public void mainline(Object... parmArray)
		{
			sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
			sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
			scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
			wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
			wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
			try {
				super.mainline();
			}
			catch (COBOLExitProgramException e) {
				LOGGER.error(e.toString());
			}
		}
	@Override
	protected void initialise1000()
		{
			initialise1010();
			
		}
	
	protected void initialise1010()
		{
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
				wsspcomn.sectionno.set("4000");
				return;
			}
			wsaaPresSeq.set(LOVALUES);
			wsaaBatckey.set(wsspcomn.batchkey);
			
			sv.dataArea.set(SPACES);
			sv.subfileArea.set(SPACES);
			scrnparams.function.set(varcom.sclr);
			processScreen("SD5G5", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			scrnparams.subfileRrn.set(1);
			initialize(sv.dataFields);
			itemList= itemDAO.getAllItemitem("IT", "2", t5679, wsaatranCode.toString());
			for(Itempf itempf : itemList)
			{
				t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
			moveClntnam1020();
			loadSubfile1030();
			optswchrec.optsFunction.set(varcom.init);
			optswchrec.optsCallingProg.set(wsaaProg);
			optswchrec.optsDteeff.set(ZERO);
			optswchrec.optsCompany.set(wsspcomn.company);
			optswchrec.optsLanguage.set(wsspcomn.language);
			varcom.vrcmTranid.set(wsspcomn.tranid);
			optswchrec.optsUser.set(varcom.vrcmUser);
			callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
			if (isNE(optswchrec.optsStatuz,varcom.oK)) {
				syserrrec.function.set(optswchrec.optsFunction);
				syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
				syserrrec.statuz.set(optswchrec.optsStatuz);
				syserrrec.iomod.set("OPTSWCH");
				fatalError600();
			}
		}
	
	protected void loadSubfile1030()
		{
			wsaaSflct.set(ZERO);
			lifepfList = lifepfDAO.searchLifeRecordByLifcNum(wsspcomn.company.toString(),wsspsmart.flddkey.toString());
			if(lifepfList.isEmpty()) {
				return;
			}
			storeFormula1031();
			while (isLT(wsaaSflct,lifepfList.size())){
				ctrl1032();
			}
			
			Set<Map.Entry<String,Double>> hset = riskAmtMap.entrySet();
			
			for(Map.Entry<String,Double> entry : hset)
			{
					descpf = new Descpf();
					initialize(sv.subfileFields);
					sv.riskClass.set(entry.getKey());		
					
					sv.riskAmt.set(entry.getValue());
					descpf = descDao.getdescData("IT", t5446, sv.riskClass.toString(),wsspcomn.company.toString(),wsspcomn.language.toString());
					if(descpf!=null)
					{
						sv.riskClassDesc.set(descpf.getLongdesc());
					}
					
					scrnparams.function.set(varcom.sadd);
					processScreen("SD5G5", sv);
					wsaaTotRiskamt.add(sv.riskAmt);		
				
			}
			sv.totRiskamt.set(wsaaTotRiskamt);
			
		}
	
	protected void ctrl1032()
	{
		wsaaPrevSeq.set(wsaaPresSeq);
		wsaaValid.set("N");
		
		checkValidContract1033();
		if(isEQ(wsaaValid,"Y"))
		{
			getValuesByriskClass1036();
			wsaaSflct.add(1);
		}
	
	}
	protected void checkValidContract1033()
	{
		chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), lifepfList.get(wsaaSflct.toInt()).getChdrnum());
		if(chdrpf!=null) {
			for(int i =1; i<=12 || isNE(wsaaValid,"Y"); i++) {
				if (i>=13) {
					wsaaSflct.add(1);
					return;
				}
				if(isEQ(t5679rec.cnRiskStat[i],chdrpf.getStatcode())) {
					checkPremStatus1034();
				}
			}		
		}
	}
	protected void checkPremStatus1034()
	{

		for(int j =1; j<=12 || isNE(wsaaValid,"Y"); j++) {
			if (j>=13) {
				wsaaSflct.add(1);
				return;
			}
			if(isEQ(t5679rec.cnPremStat[j],chdrpf.getPstcde())) {
				wsaaValid.set("Y");
			}
		}
	
	}
	
	
	protected void getValuesByriskClass1036()
	{			
		covtpfList = covtpfDAO.getCovtlnbRecord(chdrpf.getChdrcoy().toString().trim(), chdrpf.getChdrnum().trim(),lifepfList.get(wsaaSflct.toInt()).getLife().trim());
		
		if(covtpfList.size() > 0) {
			for (Covtpf covtpf :covtpfList){
				covtAmountMap(covtpf);
			}
		}
		else {
			covrpfList = covrpfDAO.getCovrincrRecord(chdrpf.getChdrcoy().toString().trim(), chdrpf.getChdrnum().trim(),lifepfList.get(wsaaSflct.toInt()).getLife().trim());
			for (Covrpf covrpf :covrpfList){
				covrAmountMap(covrpf);
			}
		}		
		
	}
	
	protected void covtAmountMap(Covtpf covtpf) {
		
		itemList = itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), td5g4, covtpf.getCrtable());
		if(!itemList.isEmpty())
		{	
			int i=0;
			td5g4rec.td5g4Rec.set(StringUtil.rawToString(itemList.get(i).getGenarea()));
	
				for(int a=1;a<td5g4rec.riskclass.length;a++)
				{ 
					
						if(isEQ(td5g4rec.riskclass[a],SPACES) || isEQ(td5g4rec.formula[a],SPACES))
							continue;
						boolean isYes=checkT5446(td5g4rec.riskclass[a]);
						if(isYes) {
							riskAmtRec.lrkcls.set(td5g4rec.riskclass[a]);
							riskAmtRec.compcode.set(covtpf.getCrtable());
							riskAmtRec.sumin.set(covtpf.getSumins());
							riskAmtRec.premCessTerm.set(covtpf.getPcestrm());
							riskAmtRec.riskCessTerm.set(covtpf.getRcestrm());
							riskAmtRec.modeprem.set(covtpf.getInstprem());	
							riskAmtRec.riskcommDate.set(new BigDecimal(chdrpf.getOccdate()));
							riskAmtRec.billingfreq.set(chdrpf.getBillfreq());
							String formula= td5g4rec.formula[a].toString();
							getFormula1038(formula);	
							if(!riskAmtMap.containsKey(td5g4rec.riskclass[a].toString())) {
								riskAmtMap.put(td5g4rec.riskclass[a].toString(), wsaaCalcRiskamt.toDouble());	
							}
							else {
								wsaaRiskAmtTemp = riskAmtMap.get(td5g4rec.riskclass[a].toString());
								wsaaRiskAmtTemp = wsaaRiskAmtTemp + wsaaCalcRiskamt.toDouble();
								riskAmtMap.put(td5g4rec.riskclass[a].toString(), wsaaRiskAmtTemp);
							}
						}
				}
		}
	}
	
	protected void covrAmountMap(Covrpf covrpf) {
		
		itemList = itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), td5g4, covrpf.getCrtable());
		if(!itemList.isEmpty())
		{	
			int i=0;
			td5g4rec.td5g4Rec.set(StringUtil.rawToString(itemList.get(i).getGenarea()));
			for(int a=1;a<td5g4rec.riskclass.length;a++)
			{ 
				
					if(isEQ(td5g4rec.riskclass[a],SPACES) || isEQ(td5g4rec.formula[a],SPACES))
						continue;
					boolean isYes=checkT5446(td5g4rec.riskclass[a]);
					if(isYes) {
						riskAmtRec.lrkcls.set(td5g4rec.riskclass[a]);
						riskAmtRec.compcode.set(covrpf.getCrtable());
						riskAmtRec.sumin.set(covrpf.getSumins());
						riskAmtRec.premCessTerm.set(covrpf.getPremCessTerm());
						riskAmtRec.riskCessTerm.set(covrpf.getRiskCessTerm());
						riskAmtRec.modeprem.set(covrpf.getInstprem());	
						riskAmtRec.riskcommDate.set(new BigDecimal(chdrpf.getOccdate()));
						riskAmtRec.billingfreq.set(chdrpf.getBillfreq());
						String formula= td5g4rec.formula[a].toString();
						getFormula1038(formula);	
						if(!riskAmtMap.containsKey(td5g4rec.riskclass[a].toString())) {
							riskAmtMap.put(td5g4rec.riskclass[a].toString(), wsaaCalcRiskamt.toDouble());	
						}
						else {
							wsaaRiskAmtTemp = riskAmtMap.get(td5g4rec.riskclass[a].toString());
							wsaaRiskAmtTemp = wsaaRiskAmtTemp + wsaaCalcRiskamt.toDouble();
							riskAmtMap.put(td5g4rec.riskclass[a].toString(), wsaaRiskAmtTemp);
						}
				}
			}
		}
	}
	protected boolean checkT5446(FixedLengthStringData riskClass) {
		
		itemT5446=itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), t5446, riskClass.toString());
		for(Itempf item: itemT5446) {
			t5446rec.t5446Rec.set(StringUtil.rawToString(item.getGenarea()));
		}
		if(isEQ(t5446rec.rskcldisp,"Y"))
		{
			return true;
		}
		return false;
	}
	protected void storeFormula1031()
	{
		itemList = itemDAO.getAllitems("IT", wsspcomn.company.toString(), td5gn);
		for(Itempf item : itemList)
		{
			td5gnRec.td5gnRec.set(StringUtil.rawToString(item.getGenarea()));
			if(!riskFormulaMap.containsKey(item.getItemitem()))
			{
				riskFormulaMap.put(item.getItemitem(), td5gnRec.risksubr.toString());
			}
		}
		
	}
	protected void getFormula1038(String formula)
	{
		Set<Map.Entry<String,String>> riskCalc= riskFormulaMap.entrySet();
		String callSubroutine="";
		for(Map.Entry<String,String> entry: riskCalc)
		{
			if(entry.getKey().trim().equals(formula.trim()))
			{
				callSubroutine=entry.getValue().trim();break;
			}
		}
		
		calcRiskAmt1037(callSubroutine);
	}
	protected void calcRiskAmt1037(String callSubroutine)
	{	
		riskAmtRec.chdrcoy.set(2);
		callProgram(callSubroutine,riskAmtRec.rskamtRec);
		wsaaCalcRiskamt.set(riskAmtRec.riskamnt);
	}
	@Override
	protected void screenEdit2000()
	{
		wsaaScrnStatuz.set(scrnparams.statuz);
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		validateSubfile2060();
			
	}
	protected void validateSubfile2060()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("SD5G5", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		if (isNE(wsaaScrnStatuz,varcom.rolu)) {
			return;
		}
		
		loadSubfile1030();
		
		wsspcomn.edterror.set("Y");
		return;
	}
	protected void validateSubfile2600()
	{
		validation2610();
		readNextModifiedRecord2680();
	}

protected void validation2610()
	{
		if (!(isEQ(sv.select,SPACES)
		|| isEQ(sv.select,"1"))) {
			sv.selectErr.set(g615);
		}
		/*UPDATE-ERROR-INDICATORS*/
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SD5G5", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("SD5G5", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	@Override
	protected void update3000()
		{
			/*UPDATE-DATABASE*/
			/*EXIT*/
		}
	@Override
	protected void whereNext4000()
		{
			nextProgram4010();	
			nextSflLine4020();			
		}
	
	protected void nextProgram4010()
		{
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
				scrnparams.function.set(varcom.sstrt);
			}
			else {
				scrnparams.function.set(varcom.srdn);
			}
		}
	
	protected void nextSflLine4020()
		{
			processScreen("SD5G5", sv);
			if (isNE(scrnparams.statuz,varcom.oK)
			&& isNE(scrnparams.statuz,varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			if (isEQ(scrnparams.statuz,varcom.endp)) {
				optswchrec.optsSelOptno.set(ZERO);
				optswchrec.optsSelType.set(SPACES);
				switch4030();
				return;
			}
			if (isEQ(sv.select,SPACES)) {
				scrnparams.function.set(varcom.srdn);
				nextSflLine4020();
			}
			if (isNE(sv.select,SPACES)) {
				wsspLrkcls.set(sv.riskClass);
				wsspTotsi.set(sv.riskAmt);
				optswchrec.optsSelOptno.set(sv.select);
				optswchrec.optsSelType.set("L");
				sv.select.set(SPACES);
				scrnparams.function.set(varcom.supd);
				processScreen("SD5G5", sv);
				if (isNE(scrnparams.statuz,varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
				switch4030();
			}
		}
	
	protected void switch4030()
		{
			optswchrec.optsFunction.set("STCK");
			callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
			if (isNE(optswchrec.optsStatuz,varcom.oK)
			&& isNE(optswchrec.optsStatuz,varcom.endp)) {
				syserrrec.function.set(optswchrec.optsFunction);
				syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
				syserrrec.statuz.set(optswchrec.optsStatuz);
				syserrrec.iomod.set("OPTSWCH");
				fatalError600();
			}
			if (isEQ(optswchrec.optsStatuz,varcom.endp)) {
				wsspcomn.nextprog.set(scrnparams.scrname);
			}
			else {
				wsspcomn.programPtr.add(1);
			}
		}
	
	protected void moveClntnam1020()
		{
			if(isEQ(wsspcomn.flag,'M'))
			{
				clntpf = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), wsspcomn.clntkey.toString());	
				sv.lifenum.set(wsspcomn.clntkey);
				wsspsmart.flddkey.set(sv.lifenum);
			}
			if(isEQ(wsspcomn.flag,"I"))
			{
				chdrpf = chdrpfDAO.getChdrenqRecord(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString());
				if(chdrpf !=null) {
					clntpf = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(),chdrpf.getCownnum());
					wsspsmart.flddkey.set(chdrpf.getCownnum());
					sv.lifenum.set(wsspsmart.flddkey);
				}
			}
			else {
				clntpf = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), wsspsmart.flddkey.toString());	
				sv.lifenum.set(wsspsmart.flddkey);
			}
			if(clntpf!=null) {
				StringBuilder stringVariable1 = new StringBuilder();
				stringVariable1.append(delimitedExp(clntpf.getSurname(), "  "));
				stringVariable1.append(" ");
				stringVariable1.append(delimitedExp(clntpf.getGivname(), "  "));
				sv.lifename.setLeft(stringVariable1.toString());
			}
		}
	protected void exit1090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
				wsspcomn.edterror.set("Y");
		}
	}
	@Override
	protected void preScreenEdit() {
		//PreScreenEdit
	}
}
