package com.csc.life.underwriting.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:25
 * Description:
 * Copybook name: T6770REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6770rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  

  	public FixedLengthStringData t6770Rec = new FixedLengthStringData(getRecSize());

  	public FixedLengthStringData answband = new FixedLengthStringData(4).isAPartOf(t6770Rec, 0);
  	public FixedLengthStringData questions = new FixedLengthStringData(280).isAPartOf(t6770Rec, 4);
  	public FixedLengthStringData[] question = FLSArrayPartOfStructure(4, 70, questions, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(280).isAPartOf(questions, 0, FILLER_REDEFINE);
  	public FixedLengthStringData question01 = new FixedLengthStringData(70).isAPartOf(filler, 0);
  	public FixedLengthStringData question02 = new FixedLengthStringData(70).isAPartOf(filler, 70);
  	public FixedLengthStringData question03 = new FixedLengthStringData(70).isAPartOf(filler, 140);
  	public FixedLengthStringData question04 = new FixedLengthStringData(70).isAPartOf(filler, 210);
  	public FixedLengthStringData questtyp = new FixedLengthStringData(1).isAPartOf(t6770Rec, 284);
  	public FixedLengthStringData undwrule = new FixedLengthStringData(4).isAPartOf(t6770Rec, 285);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(211).isAPartOf(t6770Rec, 289, FILLER);



	public void initialize() {
		COBOLFunctions.initialize(t6770Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6770Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	
public int getRecSize() {
		
		return 500 ;
	}


}
