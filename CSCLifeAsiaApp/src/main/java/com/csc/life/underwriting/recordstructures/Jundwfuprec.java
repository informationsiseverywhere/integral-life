package com.csc.life.underwriting.recordstructures;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Jundwfuprec extends ExternalData {

	public FixedLengthStringData crtfupRec = new FixedLengthStringData(getCrtfupRecSize());
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(crtfupRec, 0);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(crtfupRec, 4);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(crtfupRec, 5);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(crtfupRec, 13);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(crtfupRec, 16);
  	public FixedLengthStringData lifcnum = new FixedLengthStringData(8).isAPartOf(crtfupRec, 18);
  	public PackedDecimalData anbccd = new PackedDecimalData(3, 0).isAPartOf(crtfupRec, 26);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(crtfupRec, 28);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(crtfupRec, 31);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(crtfupRec, 35);
  	public FixedLengthStringData fsuco = new FixedLengthStringData(1).isAPartOf(crtfupRec, 36);
  	public PackedDecimalData user = new PackedDecimalData(6, 0).isAPartOf(crtfupRec, 37);
  	public PackedDecimalData transactionTime = new PackedDecimalData(6, 0).isAPartOf(crtfupRec, 41);
  	public PackedDecimalData transactionDate = new PackedDecimalData(6, 0).isAPartOf(crtfupRec, 45);
  	public FixedLengthStringData firstLife = new FixedLengthStringData(1).isAPartOf(crtfupRec, 49);
  	public FixedLengthStringData jlife = new FixedLengthStringData(2).isAPartOf(crtfupRec, 50);
  	public PackedDecimalData totalSumins = new PackedDecimalData(17, 2).isAPartOf(crtfupRec, 52);
  	public FixedLengthStringData function = new FixedLengthStringData(4).isAPartOf(crtfupRec, 61);
  	public FixedLengthStringData lrkcls = new FixedLengthStringData(4).isAPartOf(crtfupRec, 65);
	public ZonedDecimalData effDate = new ZonedDecimalData(8, 0).isAPartOf(crtfupRec, 69);
	
	private List<String> agrCovrList = new ArrayList<>();
	private List<BigDecimal> agrSumInsList = new ArrayList<>();
	private List<Integer> agrRiskFactorList = new ArrayList<>();
	
	public BigDecimal aggregtedSumIns = new BigDecimal(0);
	

	public void initialize() {
		COBOLFunctions.initialize(crtfupRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		crtfupRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	
	public int getCrtfupRecSize()
	{
		return 77;
	}
	
	public List<String> getAgrCovrList() {
		return agrCovrList;
	}


	public void setAgrCovrList(List<String> agrCovrList) {
		this.agrCovrList = agrCovrList;
	}


	public List<BigDecimal> getAgrSumInsList() {
		return agrSumInsList;
	}


	public void setAgrSumInsList(List<BigDecimal> agrSumInsList) {
		this.agrSumInsList = agrSumInsList;
	}


	public List<Integer> getAgrRiskFactorList() {
		return agrRiskFactorList;
	}


	public void setAgrRiskFactorList(List<Integer> agrRiskFactorList) {
		this.agrRiskFactorList = agrRiskFactorList;
	}


	public BigDecimal getAggregtedSumIns() {
		return aggregtedSumIns;
	}


	public void setAggregtedSumIns(BigDecimal aggregtedSumIns) {
		this.aggregtedSumIns = aggregtedSumIns;
	}
	
}
