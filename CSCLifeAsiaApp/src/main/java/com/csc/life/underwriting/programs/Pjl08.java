package com.csc.life.underwriting.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;

import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.life.underwriting.screens.Sjl08ScreenVars;
import com.csc.life.underwriting.tablestructures.Tjl08rec;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Itmdkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pjl08 extends ScreenProgCS {

		public static final String ROUTINE = QPUtilities.getThisClass();
		private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL08");
		private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		private String wsaaUpdateFlag = "N";
		private Tjl08rec tjl08rec = new Tjl08rec();
		private Wsspsmart wsspsmart = new Wsspsmart();

		private Sjl08ScreenVars sv = ScreenProgram.getScreenVars(Sjl08ScreenVars.class);
		private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
		private final ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);	
		private Desckey wsaaDesckey = new Desckey();
		private Itemkey wsaaItemkey = new Itemkey();
		private Itmdkey wsaaItmdkey = new Itmdkey();
		private Itempf itempf = new Itempf();
		Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
		
		public Pjl08() {
			super();
			screenVars = sv;
			new ScreenModel("Sjl08", AppVars.getInstance(), sv);
		}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
		}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
		}

	public void mainline(Object... parmArray)
		{
			sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
			scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
			wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
			wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
			try {
				super.mainline();
			}
			catch (COBOLExitProgramException e) {
			}
		}

	protected void initialise1000()
				{		
						initialise1010();
				}

	protected void initialise1010()
		{
			/*INITIALISE-SCREEN*/
			sv.dataArea.set(SPACES);
			wsaaItemkey.set(wsspsmart.itemkey);
			wsaaDesckey.descDescpfx.set(wsaaItemkey.itemItempfx);
			wsaaDesckey.descDesccoy.set(wsaaItemkey.itemItemcoy);
			wsaaDesckey.descDesctabl.set(wsaaItemkey.itemItemtabl);
			wsaaDesckey.descDescitem.set(wsaaItemkey.itemItemitem);
			wsaaDesckey.descItemseq.set(wsaaItemkey.itemItemseq);
			wsaaDesckey.descLanguage.set(wsspcomn.language);
			sv.company.set(wsaaItemkey.itemItemcoy);
			sv.tabl.set(wsaaItemkey.itemItemtabl);
			sv.item.set(wsaaItemkey.itemItemitem);
			wsaaItmdkey.set(wsspsmart.itmdkey);
			Descpf descpf = descDAO.getdescData("IT", wsaaItemkey.itemItemtabl.toString(), wsaaItemkey.itemItemitem.toString(), wsaaItemkey.itemItemcoy.toString(), wsspcomn.language.toString());
			if (descpf==null) {
				fatalError600();
			}
			sv.longdesc.set(descpf.getLongdesc());

			itempf = itempfDAO.findItemByItdm(wsaaItemkey.itemItemcoy.toString(), wsaaItemkey.itemItemtabl.toString(),wsaaItemkey.itemItemitem.toString());

			if (itempf == null ) {
				fatalError600();
			}

			tjl08rec.tjl08Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			if (isNE(tjl08rec.tjl08Rec, SPACES)) {
				generalArea1045();
			}
			
		}


	protected void generalArea1045()
		{
			sv.sumAssuredFactor.set(tjl08rec.sumAssuredFactor);
			sv.riskClass01.set(tjl08rec.riskClass01);
			sv.riskClass02.set(tjl08rec.riskClass02);
			sv.riskClass03.set(tjl08rec.riskClass03);
			sv.riskClass04.set(tjl08rec.riskClass04);
			sv.riskClass05.set(tjl08rec.riskClass05);
			/*EXIT*/
		}

	protected void preScreenEdit()
		{
				preStart();
		}

	protected void preStart()
		{
			if (isEQ(wsspcomn.flag,"I")) {
				scrnparams.function.set(varcom.prot);
			}
		}

	protected void screenEdit2000()
		{
			screenIo2010();
			exit2090();
					
		}

	protected void screenIo2010()
		{
			wsspcomn.edterror.set(varcom.oK);
			/*VALIDATE*/
			if(isEQ(sv.sumAssuredFactor,ZERO)){
				sv.sumAssuredFactorErr.set("L001");
			}
			if (isEQ(wsspcomn.flag,"I")) {
				exit2090();
			}
			/*OTHER*/
		}

	protected void exit2090()
		{
			if (isNE(sv.errorIndicators,SPACES)) {
				wsspcomn.edterror.set("Y");
			}
			/*EXIT*/
		}

	protected void update3000()
		{
			preparation3010();
			updatePrimaryRecord3050();
			updateRecord3055();
					
		}

	protected void preparation3010()
		{
			if (isEQ(wsspcomn.flag,"I")) {
			}
		}

	protected void updatePrimaryRecord3050()
		{
			varcom.vrcmTranid.set(wsspcomn.tranid);
			varcom.vrcmCompTermid.set(varcom.vrcmTermid);
			varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
			itempf.setTranid(varcom.vrcmCompTranid.trim());
		}

	protected void updateRecord3055()
		{
			wsaaUpdateFlag = "N";
			checkChanges3100();
			if (isNE(wsaaUpdateFlag,"Y")) {
				return;
			}
			itempf.setGenarea(tjl08rec.tjl08Rec.toString().getBytes());
			itempf.setItempfx(smtpfxcpy.item.toString());
			itempf.setItemtabl(wsaaItemkey.itemItemtabl.toString());
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemitem(wsaaItemkey.itemItemitem.toString());
			itempfDAO.updateByKey(itempf, "ITEMPFX, ITEMCOY, ITEMTABL, ITEMITEM");
		}

	protected void checkChanges3100()
		{
			/*CHECK*/
			if (isNE(sv.sumAssuredFactor,tjl08rec.sumAssuredFactor)) {
				tjl08rec.sumAssuredFactor.set(sv.sumAssuredFactor);
				wsaaUpdateFlag = "Y";
			}
			if (isNE(sv.riskClass01,tjl08rec.riskClass01)) {
				tjl08rec.riskClass01.set(sv.riskClass01);
				wsaaUpdateFlag = "Y";
			}
			if (isNE(sv.riskClass02,tjl08rec.riskClass02)) {
				tjl08rec.riskClass02.set(sv.riskClass02);
				wsaaUpdateFlag = "Y";
			}
			if (isNE(sv.riskClass03,tjl08rec.riskClass03)) {
				tjl08rec.riskClass03.set(sv.riskClass03);
				wsaaUpdateFlag = "Y";
			}
			if (isNE(sv.riskClass04,tjl08rec.riskClass04)) {
				tjl08rec.riskClass04.set(sv.riskClass04);
				wsaaUpdateFlag = "Y";
			}
			if (isNE(sv.riskClass05,tjl08rec.riskClass05)) {
				tjl08rec.riskClass05.set(sv.riskClass05);
				wsaaUpdateFlag = "Y";
			}
			
			/*EXIT*/
		}

	protected void whereNext4000()
		{
			/*NEXT-PROGRAM*/
			wsspcomn.programPtr.add(1);
			/*EXIT*/
		}

}
