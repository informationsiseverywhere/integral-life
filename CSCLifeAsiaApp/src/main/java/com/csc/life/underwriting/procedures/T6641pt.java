/*
 * File: T6641pt.java
 * Date: 30 August 2009 2:28:26
 * Author: Quipoz Limited
 * 
 * Class transformed from T6641PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.underwriting.tablestructures.T6641rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6641.
*
*
*****************************************************************
* </pre>
*/
public class T6641pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6641rec t6641rec = new T6641rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6641pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6641rec.t6641Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		generalCopyLinesInner.fieldNo005.set(t6641rec.nxdp01);
		generalCopyLinesInner.fieldNo006.set(t6641rec.nxmoney);
		generalCopyLinesInner.fieldNo013.set(t6641rec.nxdp02);
		generalCopyLinesInner.fieldNo021.set(t6641rec.nxdp03);
		generalCopyLinesInner.fieldNo029.set(t6641rec.nxdp04);
		generalCopyLinesInner.fieldNo037.set(t6641rec.nxdp05);
		generalCopyLinesInner.fieldNo045.set(t6641rec.nxdp06);
		generalCopyLinesInner.fieldNo053.set(t6641rec.nxdp07);
		generalCopyLinesInner.fieldNo061.set(t6641rec.nxdp08);
		generalCopyLinesInner.fieldNo069.set(t6641rec.nxdp09);
		generalCopyLinesInner.fieldNo077.set(t6641rec.nxdp10);
		generalCopyLinesInner.fieldNo007.set(t6641rec.nxmon01);
		generalCopyLinesInner.fieldNo008.set(t6641rec.nxmon02);
		generalCopyLinesInner.fieldNo009.set(t6641rec.nxmon03);
		generalCopyLinesInner.fieldNo010.set(t6641rec.nxmon04);
		generalCopyLinesInner.fieldNo011.set(t6641rec.nxmon05);
		generalCopyLinesInner.fieldNo012.set(t6641rec.nxmon06);
		generalCopyLinesInner.fieldNo014.set(t6641rec.nxmon07);
		generalCopyLinesInner.fieldNo015.set(t6641rec.nxmon08);
		generalCopyLinesInner.fieldNo016.set(t6641rec.nxmon09);
		generalCopyLinesInner.fieldNo017.set(t6641rec.nxmon10);
		generalCopyLinesInner.fieldNo018.set(t6641rec.nxmon11);
		generalCopyLinesInner.fieldNo019.set(t6641rec.nxmon12);
		generalCopyLinesInner.fieldNo020.set(t6641rec.nxmon13);
		generalCopyLinesInner.fieldNo022.set(t6641rec.nxmon14);
		generalCopyLinesInner.fieldNo023.set(t6641rec.nxmon15);
		generalCopyLinesInner.fieldNo024.set(t6641rec.nxmon16);
		generalCopyLinesInner.fieldNo025.set(t6641rec.nxmon17);
		generalCopyLinesInner.fieldNo026.set(t6641rec.nxmon18);
		generalCopyLinesInner.fieldNo027.set(t6641rec.nxmon19);
		generalCopyLinesInner.fieldNo028.set(t6641rec.nxmon20);
		generalCopyLinesInner.fieldNo030.set(t6641rec.nxmon21);
		generalCopyLinesInner.fieldNo031.set(t6641rec.nxmon22);
		generalCopyLinesInner.fieldNo032.set(t6641rec.nxmon23);
		generalCopyLinesInner.fieldNo033.set(t6641rec.nxmon24);
		generalCopyLinesInner.fieldNo034.set(t6641rec.nxmon25);
		generalCopyLinesInner.fieldNo035.set(t6641rec.nxmon26);
		generalCopyLinesInner.fieldNo036.set(t6641rec.nxmon27);
		generalCopyLinesInner.fieldNo038.set(t6641rec.nxmon28);
		generalCopyLinesInner.fieldNo039.set(t6641rec.nxmon29);
		generalCopyLinesInner.fieldNo040.set(t6641rec.nxmon30);
		generalCopyLinesInner.fieldNo041.set(t6641rec.nxmon31);
		generalCopyLinesInner.fieldNo042.set(t6641rec.nxmon32);
		generalCopyLinesInner.fieldNo043.set(t6641rec.nxmon33);
		generalCopyLinesInner.fieldNo044.set(t6641rec.nxmon34);
		generalCopyLinesInner.fieldNo046.set(t6641rec.nxmon35);
		generalCopyLinesInner.fieldNo047.set(t6641rec.nxmon36);
		generalCopyLinesInner.fieldNo048.set(t6641rec.nxmon37);
		generalCopyLinesInner.fieldNo049.set(t6641rec.nxmon38);
		generalCopyLinesInner.fieldNo050.set(t6641rec.nxmon39);
		generalCopyLinesInner.fieldNo051.set(t6641rec.nxmon40);
		generalCopyLinesInner.fieldNo052.set(t6641rec.nxmon41);
		generalCopyLinesInner.fieldNo054.set(t6641rec.nxmon42);
		generalCopyLinesInner.fieldNo055.set(t6641rec.nxmon43);
		generalCopyLinesInner.fieldNo056.set(t6641rec.nxmon44);
		generalCopyLinesInner.fieldNo057.set(t6641rec.nxmon45);
		generalCopyLinesInner.fieldNo058.set(t6641rec.nxmon46);
		generalCopyLinesInner.fieldNo059.set(t6641rec.nxmon47);
		generalCopyLinesInner.fieldNo060.set(t6641rec.nxmon48);
		generalCopyLinesInner.fieldNo062.set(t6641rec.nxmon49);
		generalCopyLinesInner.fieldNo063.set(t6641rec.nxmon50);
		generalCopyLinesInner.fieldNo064.set(t6641rec.nxmon51);
		generalCopyLinesInner.fieldNo065.set(t6641rec.nxmon52);
		generalCopyLinesInner.fieldNo066.set(t6641rec.nxmon53);
		generalCopyLinesInner.fieldNo067.set(t6641rec.nxmon54);
		generalCopyLinesInner.fieldNo068.set(t6641rec.nxmon55);
		generalCopyLinesInner.fieldNo070.set(t6641rec.nxmon56);
		generalCopyLinesInner.fieldNo071.set(t6641rec.nxmon57);
		generalCopyLinesInner.fieldNo072.set(t6641rec.nxmon58);
		generalCopyLinesInner.fieldNo073.set(t6641rec.nxmon59);
		generalCopyLinesInner.fieldNo074.set(t6641rec.nxmon60);
		generalCopyLinesInner.fieldNo075.set(t6641rec.nxmon61);
		generalCopyLinesInner.fieldNo076.set(t6641rec.nxmon62);
		generalCopyLinesInner.fieldNo078.set(t6641rec.nxmon63);
		generalCopyLinesInner.fieldNo079.set(t6641rec.nxmon64);
		generalCopyLinesInner.fieldNo080.set(t6641rec.nxmon65);
		generalCopyLinesInner.fieldNo081.set(t6641rec.nxmon66);
		generalCopyLinesInner.fieldNo082.set(t6641rec.nxmon67);
		generalCopyLinesInner.fieldNo083.set(t6641rec.nxmon68);
		generalCopyLinesInner.fieldNo084.set(t6641rec.nxmon69);
		generalCopyLinesInner.fieldNo086.set(t6641rec.nxmon70);
		generalCopyLinesInner.fieldNo087.set(t6641rec.nxmon71);
		generalCopyLinesInner.fieldNo088.set(t6641rec.nxmon72);
		generalCopyLinesInner.fieldNo089.set(t6641rec.nxmon73);
		generalCopyLinesInner.fieldNo090.set(t6641rec.nxmon74);
		generalCopyLinesInner.fieldNo091.set(t6641rec.nxmon75);
		generalCopyLinesInner.fieldNo092.set(t6641rec.nxmon76);
		generalCopyLinesInner.fieldNo094.set(t6641rec.nxmon77);
		generalCopyLinesInner.fieldNo095.set(t6641rec.nxmon78);
		generalCopyLinesInner.fieldNo096.set(t6641rec.nxmon79);
		generalCopyLinesInner.fieldNo097.set(t6641rec.nxmon80);
		generalCopyLinesInner.fieldNo098.set(t6641rec.nxmon81);
		generalCopyLinesInner.fieldNo099.set(t6641rec.nxmon82);
		generalCopyLinesInner.fieldNo100.set(t6641rec.nxmon83);
		generalCopyLinesInner.fieldNo102.set(t6641rec.nxmon84);
		generalCopyLinesInner.fieldNo103.set(t6641rec.nxmon85);
		generalCopyLinesInner.fieldNo104.set(t6641rec.nxmon86);
		generalCopyLinesInner.fieldNo105.set(t6641rec.nxmon87);
		generalCopyLinesInner.fieldNo106.set(t6641rec.nxmon88);
		generalCopyLinesInner.fieldNo107.set(t6641rec.nxmon89);
		generalCopyLinesInner.fieldNo108.set(t6641rec.nxmon90);
		generalCopyLinesInner.fieldNo110.set(t6641rec.nxmon91);
		generalCopyLinesInner.fieldNo111.set(t6641rec.nxmon92);
		generalCopyLinesInner.fieldNo112.set(t6641rec.nxmon93);
		generalCopyLinesInner.fieldNo113.set(t6641rec.nxmon94);
		generalCopyLinesInner.fieldNo114.set(t6641rec.nxmon95);
		generalCopyLinesInner.fieldNo115.set(t6641rec.nxmon96);
		generalCopyLinesInner.fieldNo116.set(t6641rec.nxmon97);
		generalCopyLinesInner.fieldNo118.set(t6641rec.nxmon98);
		generalCopyLinesInner.fieldNo119.set(t6641rec.nxmon99);
		generalCopyLinesInner.fieldNo120.set(t6641rec.tnxmon01);
		generalCopyLinesInner.fieldNo121.set(t6641rec.tnxmon02);
		generalCopyLinesInner.fieldNo122.set(t6641rec.tnxmon03);
		generalCopyLinesInner.fieldNo123.set(t6641rec.tnxmon04);
		generalCopyLinesInner.fieldNo124.set(t6641rec.tnxmon05);
		generalCopyLinesInner.fieldNo126.set(t6641rec.tnxmon06);
		generalCopyLinesInner.fieldNo127.set(t6641rec.tnxmon07);
		generalCopyLinesInner.fieldNo128.set(t6641rec.tnxmon08);
		generalCopyLinesInner.fieldNo129.set(t6641rec.tnxmon09);
		generalCopyLinesInner.fieldNo130.set(t6641rec.tnxmon10);
		generalCopyLinesInner.fieldNo131.set(t6641rec.tnxmon11);
		generalCopyLinesInner.fieldNo085.set(t6641rec.nxdp11);
		generalCopyLinesInner.fieldNo093.set(t6641rec.nxdp12);
		generalCopyLinesInner.fieldNo101.set(t6641rec.nxdp13);
		generalCopyLinesInner.fieldNo109.set(t6641rec.nxdp14);
		generalCopyLinesInner.fieldNo117.set(t6641rec.nxdp15);
		generalCopyLinesInner.fieldNo125.set(t6641rec.nxdp16);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine018);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine019);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(49).isAPartOf(wsaaPrtLine001, 27, FILLER).init("Monetary Function Nx                        S6641");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(72);
	private FixedLengthStringData filler7 = new FixedLengthStringData(72).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Dp  Age:       0        1        2        3        4  5        6");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(75);
	private FixedLengthStringData filler8 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo005 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine004, 2).setPattern("Z");
	private FixedLengthStringData filler9 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine004, 3, FILLER).init("    0+");
	private ZonedDecimalData fieldNo006 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler10 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler11 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler12 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine004, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(75);
	private FixedLengthStringData filler16 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine005, 2).setPattern("Z");
	private FixedLengthStringData filler17 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 3, FILLER).init("    7+");
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine005, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(75);
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine006, 2).setPattern("Z");
	private FixedLengthStringData filler25 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine006, 3, FILLER).init("   14+");
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine006, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(75);
	private FixedLengthStringData filler32 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine007, 2).setPattern("Z");
	private FixedLengthStringData filler33 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 3, FILLER).init("   21+");
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(75);
	private FixedLengthStringData filler40 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine008, 2).setPattern("Z");
	private FixedLengthStringData filler41 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine008, 3, FILLER).init("   28+");
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(75);
	private FixedLengthStringData filler48 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine009, 2).setPattern("Z");
	private FixedLengthStringData filler49 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 3, FILLER).init("   35+");
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(75);
	private FixedLengthStringData filler56 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine010, 2).setPattern("Z");
	private FixedLengthStringData filler57 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine010, 3, FILLER).init("   42+");
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(75);
	private FixedLengthStringData filler64 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine011, 2).setPattern("Z");
	private FixedLengthStringData filler65 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 3, FILLER).init("   49+");
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler69 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(75);
	private FixedLengthStringData filler72 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine012, 2).setPattern("Z");
	private FixedLengthStringData filler73 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 3, FILLER).init("   56+");
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler79 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo076 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(75);
	private FixedLengthStringData filler80 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine013, 2).setPattern("Z");
	private FixedLengthStringData filler81 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine013, 3, FILLER).init("   63+");
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo079 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler83 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo080 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler84 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo082 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler86 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler87 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(75);
	private FixedLengthStringData filler88 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo085 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine014, 2).setPattern("Z");
	private FixedLengthStringData filler89 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine014, 3, FILLER).init("   70+");
	private ZonedDecimalData fieldNo086 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler90 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo087 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo088 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler92 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo089 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler93 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo090 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler94 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo091 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler95 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo092 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(75);
	private FixedLengthStringData filler96 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine015, 2).setPattern("Z");
	private FixedLengthStringData filler97 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine015, 3, FILLER).init("   77+");
	private ZonedDecimalData fieldNo094 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler98 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo095 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler99 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo096 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler100 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo097 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler101 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo098 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler102 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo099 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler103 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo100 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine015, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(75);
	private FixedLengthStringData filler104 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo101 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine016, 2).setPattern("Z");
	private FixedLengthStringData filler105 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine016, 3, FILLER).init("   84+");
	private ZonedDecimalData fieldNo102 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler106 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo103 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler107 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo104 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler108 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo105 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler109 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo106 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler110 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo107 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler111 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo108 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine016, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(75);
	private FixedLengthStringData filler112 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo109 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine017, 2).setPattern("Z");
	private FixedLengthStringData filler113 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine017, 3, FILLER).init("   91+");
	private ZonedDecimalData fieldNo110 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler114 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo111 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler115 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo112 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler116 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo113 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler117 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo114 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler118 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo115 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler119 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo116 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine017, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(75);
	private FixedLengthStringData filler120 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine018, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo117 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine018, 2).setPattern("Z");
	private FixedLengthStringData filler121 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine018, 3, FILLER).init("   98+");
	private ZonedDecimalData fieldNo118 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler122 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo119 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler123 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo120 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler124 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo121 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler125 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo122 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler126 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo123 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 58).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler127 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 66, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo124 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine018, 67).setPattern("ZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine019 = new FixedLengthStringData(66);
	private FixedLengthStringData filler128 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine019, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo125 = new ZonedDecimalData(1, 0).isAPartOf(wsaaPrtLine019, 2).setPattern("Z");
	private FixedLengthStringData filler129 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine019, 3, FILLER).init("   105+");
	private ZonedDecimalData fieldNo126 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine019, 13).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler130 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine019, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo127 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine019, 22).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler131 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine019, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo128 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine019, 31).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler132 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine019, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo129 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine019, 40).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler133 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine019, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo130 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine019, 49).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler134 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine019, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo131 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine019, 58).setPattern("ZZZZZZZZ");
}
}
