package com.csc.life.underwriting.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class S6771screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 1, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6771ScreenVars sv = (S6771ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6771screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6771ScreenVars screenVars = (S6771ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.language.setClassString("");
		screenVars.questidf01.setClassString("");
		screenVars.questidf05.setClassString("");
		screenVars.questidf09.setClassString("");
		screenVars.questidf13.setClassString("");
		screenVars.questidf17.setClassString("");
		screenVars.questidf21.setClassString("");
		screenVars.questidf25.setClassString("");
		screenVars.questidf29.setClassString("");
		screenVars.questidf33.setClassString("");
		screenVars.questidf37.setClassString("");
		screenVars.undwsubr.setClassString("");
		screenVars.questst01.setClassString("");
		screenVars.questst05.setClassString("");
		screenVars.questst09.setClassString("");
		screenVars.questst13.setClassString("");
		screenVars.questst17.setClassString("");
		screenVars.questst21.setClassString("");
		screenVars.questst25.setClassString("");
		screenVars.questst29.setClassString("");
		screenVars.questst33.setClassString("");
		screenVars.questst37.setClassString("");
		screenVars.questidf02.setClassString("");
		screenVars.questidf06.setClassString("");
		screenVars.questidf10.setClassString("");
		screenVars.questidf14.setClassString("");
		screenVars.questidf18.setClassString("");
		screenVars.questidf22.setClassString("");
		screenVars.questidf26.setClassString("");
		screenVars.questidf30.setClassString("");
		screenVars.questidf34.setClassString("");
		screenVars.questidf38.setClassString("");
		screenVars.questidf03.setClassString("");
		screenVars.questidf07.setClassString("");
		screenVars.questidf11.setClassString("");
		screenVars.questidf15.setClassString("");
		screenVars.questidf19.setClassString("");
		screenVars.questidf23.setClassString("");
		screenVars.questidf27.setClassString("");
		screenVars.questidf31.setClassString("");
		screenVars.questidf35.setClassString("");
		screenVars.questidf39.setClassString("");
		screenVars.questidf04.setClassString("");
		screenVars.questidf08.setClassString("");
		screenVars.questidf12.setClassString("");
		screenVars.questidf16.setClassString("");
		screenVars.questidf20.setClassString("");
		screenVars.questidf24.setClassString("");
		screenVars.questidf28.setClassString("");
		screenVars.questidf32.setClassString("");
		screenVars.questidf36.setClassString("");
		screenVars.questidf40.setClassString("");
		screenVars.questst02.setClassString("");
		screenVars.questst06.setClassString("");
		screenVars.questst10.setClassString("");
		screenVars.questst14.setClassString("");
		screenVars.questst18.setClassString("");
		screenVars.questst22.setClassString("");
		screenVars.questst26.setClassString("");
		screenVars.questst30.setClassString("");
		screenVars.questst34.setClassString("");
		screenVars.questst38.setClassString("");
		screenVars.questst03.setClassString("");
		screenVars.questst07.setClassString("");
		screenVars.questst11.setClassString("");
		screenVars.questst15.setClassString("");
		screenVars.questst19.setClassString("");
		screenVars.questst23.setClassString("");
		screenVars.questst27.setClassString("");
		screenVars.questst31.setClassString("");
		screenVars.questst35.setClassString("");
		screenVars.questst39.setClassString("");
		screenVars.questst04.setClassString("");
		screenVars.questst08.setClassString("");
		screenVars.questst12.setClassString("");
		screenVars.questst16.setClassString("");
		screenVars.questst20.setClassString("");
		screenVars.questst24.setClassString("");
		screenVars.questst28.setClassString("");
		screenVars.questst32.setClassString("");
		screenVars.questst36.setClassString("");
		screenVars.questst40.setClassString("");
	}

/**
 * Clear all the variables in S6771screen
 */
	public static void clear(VarModel pv) {
		S6771ScreenVars screenVars = (S6771ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.language.clear();
		screenVars.questidf01.clear();
		screenVars.questidf05.clear();
		screenVars.questidf09.clear();
		screenVars.questidf13.clear();
		screenVars.questidf17.clear();
		screenVars.questidf21.clear();
		screenVars.questidf25.clear();
		screenVars.questidf29.clear();
		screenVars.questidf33.clear();
		screenVars.questidf37.clear();
		screenVars.undwsubr.clear();
		screenVars.questst01.clear();
		screenVars.questst05.clear();
		screenVars.questst09.clear();
		screenVars.questst13.clear();
		screenVars.questst17.clear();
		screenVars.questst21.clear();
		screenVars.questst25.clear();
		screenVars.questst29.clear();
		screenVars.questst33.clear();
		screenVars.questst37.clear();
		screenVars.questidf02.clear();
		screenVars.questidf06.clear();
		screenVars.questidf10.clear();
		screenVars.questidf14.clear();
		screenVars.questidf18.clear();
		screenVars.questidf22.clear();
		screenVars.questidf26.clear();
		screenVars.questidf30.clear();
		screenVars.questidf34.clear();
		screenVars.questidf38.clear();
		screenVars.questidf03.clear();
		screenVars.questidf07.clear();
		screenVars.questidf11.clear();
		screenVars.questidf15.clear();
		screenVars.questidf19.clear();
		screenVars.questidf23.clear();
		screenVars.questidf27.clear();
		screenVars.questidf31.clear();
		screenVars.questidf35.clear();
		screenVars.questidf39.clear();
		screenVars.questidf04.clear();
		screenVars.questidf08.clear();
		screenVars.questidf12.clear();
		screenVars.questidf16.clear();
		screenVars.questidf20.clear();
		screenVars.questidf24.clear();
		screenVars.questidf28.clear();
		screenVars.questidf32.clear();
		screenVars.questidf36.clear();
		screenVars.questidf40.clear();
		screenVars.questst02.clear();
		screenVars.questst06.clear();
		screenVars.questst10.clear();
		screenVars.questst14.clear();
		screenVars.questst18.clear();
		screenVars.questst22.clear();
		screenVars.questst26.clear();
		screenVars.questst30.clear();
		screenVars.questst34.clear();
		screenVars.questst38.clear();
		screenVars.questst03.clear();
		screenVars.questst07.clear();
		screenVars.questst11.clear();
		screenVars.questst15.clear();
		screenVars.questst19.clear();
		screenVars.questst23.clear();
		screenVars.questst27.clear();
		screenVars.questst31.clear();
		screenVars.questst35.clear();
		screenVars.questst39.clear();
		screenVars.questst04.clear();
		screenVars.questst08.clear();
		screenVars.questst12.clear();
		screenVars.questst16.clear();
		screenVars.questst20.clear();
		screenVars.questst24.clear();
		screenVars.questst28.clear();
		screenVars.questst32.clear();
		screenVars.questst36.clear();
		screenVars.questst40.clear();
	}
}
