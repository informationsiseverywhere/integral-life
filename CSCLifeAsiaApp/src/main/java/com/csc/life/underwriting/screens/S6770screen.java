package com.csc.life.underwriting.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class S6770screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 1, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6770ScreenVars sv = (S6770ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6770screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6770ScreenVars screenVars = (S6770ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.question01.setClassString("");
		screenVars.question02.setClassString("");
		screenVars.question03.setClassString("");
		screenVars.question04.setClassString("");
		screenVars.questtyp.setClassString("");
		screenVars.undwrule.setClassString("");
		screenVars.answband.setClassString("");
	}

/**
 * Clear all the variables in S6770screen
 */
	public static void clear(VarModel pv) {
		S6770ScreenVars screenVars = (S6770ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.question01.clear();
		screenVars.question02.clear();
		screenVars.question03.clear();
		screenVars.question04.clear();
		screenVars.questtyp.clear();
		screenVars.undwrule.clear();
		screenVars.answband.clear();
	}
}
