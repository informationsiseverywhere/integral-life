package com.csc.life.underwriting.dataaccess.dao;

import java.util.List;

import com.csc.life.underwriting.dataaccess.model.Undlpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface UndlpfDAO extends BaseDAO<Undlpf> {

public List<Undlpf> searchUndlpfRecords(Undlpf undlpfRec);
public List<? extends Undlpf> checkUndlpf(String chdrcoy ,String chdrnum);
//ILIFE-6587
public List<Undlpf> searchUndlpfList(String chdrcoy ,String chdrnum,String life, String jlife);	
public void updateUndlTranno(List<? extends Undlpf> undlpfList); //ILIFE-8709
void deleteUndlRecord(String chdrcoy ,String chdrnum,String life, String jlife);
}