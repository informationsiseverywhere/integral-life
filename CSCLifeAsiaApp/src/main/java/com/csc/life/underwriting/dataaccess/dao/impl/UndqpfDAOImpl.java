package com.csc.life.underwriting.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList; //ILIFE-8709
import java.util.List; //ILIFE-8709

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.underwriting.dataaccess.dao.UndqpfDAO;
import com.csc.life.underwriting.dataaccess.model.Undqpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class UndqpfDAOImpl extends BaseDAOImpl<Undqpf> implements UndqpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(UndqpfDAOImpl.class);
	
	public boolean  isExistundqpfByCoyAndNumAndLifeAndJLife(String chdrcoy, String chdrnum, String life, String jlife){
	
		StringBuilder sql = new StringBuilder("SELECT * FROM UNDQPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND JLIFE=? ");
	    sql.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, JLIFE ASC, QUESTIDF ASC, UNIQUE_NUMBER ASC");

        PreparedStatement ps = getPrepareStatement(sql.toString());
        ResultSet rs = null;
        
        boolean result = false;
    try {
    	ps.setString(1, chdrcoy);
    	ps.setString(2, chdrnum);
    	ps.setString(3, life);
    	ps.setString(4, jlife);
        rs = ps.executeQuery();
        while (rs.next()) {
        	result = true;
            }
        } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps, rs);
    }
    return result;

 }
 //ILIFE-8709 start
	@Override
	public List<Undqpf> searchUndqpfRecord(String chdrcoy, String chdrnum){
		StringBuilder sqlUndqpf = new StringBuilder("SELECT UNIQUE_NUMBER, LIFE, JLIFE, VALIDFLAG, TRANNO");
  		sqlUndqpf.append(" FROM UNDQ ");
  		sqlUndqpf.append(" WHERE CHDRCOY = ? AND CHDRNUM =? ");
        PreparedStatement psUndqpf = getPrepareStatement(sqlUndqpf.toString());
        
        
        ResultSet undqpf1rs = null;
        List<Undqpf> undqpfList = new ArrayList<Undqpf>();
      
        try {
        	psUndqpf.setString(1,chdrcoy);
        	psUndqpf.setString(2,chdrnum);
        	
        	undqpf1rs = psUndqpf.executeQuery();
            while (undqpf1rs.next()) {
            	Undqpf undqpf= new Undqpf();
            	
            	undqpf.setUnique_number(undqpf1rs.getLong("UNIQUE_NUMBER"));
            	undqpf.setLife(undqpf1rs.getString("LIFE"));
            	undqpf.setJlife(undqpf1rs.getString("JLIFE"));
            	undqpf.setValidflag(undqpf1rs.getString("VALIDFLAG"));
            	undqpf.setTranno(undqpf1rs.getInt("TRANNO"));
            	undqpfList.add(undqpf);
         	}

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(psUndqpf, undqpf1rs);
        }
        return undqpfList;
	}

	@Override
	public void updateUndqTranno(List<Undqpf> undqpfList) {

		 if (undqpfList == null || undqpfList.isEmpty()) {
	            return;
	        }
	        String sql = " UPDATE UNDQPF SET VALIDFLAG = ?, TRANNO = ?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER = ? ";
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try {
	            ps = getPrepareStatement(sql);
	            int ctr;
	            for (Undqpf undq : undqpfList) {
	                ctr = 0;
	                ps.setString(++ctr, undq.getValidflag());
	                ps.setInt(++ctr, undq.getTranno());
	                ps.setString(++ctr, getJobnm());
	                ps.setString(++ctr, getUsrprf());
	                ps.setTimestamp(++ctr, getDatime());
	                ps.setLong(++ctr, undq.getUnique_number());
	                ps.addBatch();
	            }
	            ps.executeBatch();
	        } catch (SQLException e) {
	            LOGGER.error("updateUndqTranno()" , e);
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, rs);
	        }
	}
 //ILIFE-8709 end
	@Override
	public int deleteUndqRecord(String chdrcoy, String chdrnum, String life, String jlife) {
		String sql = "DELETE FROM UNDQPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND JLIFE=?";
		PreparedStatement ps = getPrepareStatement(sql);
		try {
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, life);
			ps.setString(4, jlife);
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

	@Override
	public List<Undqpf> searchUnqpf(String chdrcoy, String chdrnum,String life, String jlife) {
		StringBuilder sql = new StringBuilder("SELECT * FROM UNDQPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND JLIFE=? ");
	    sql.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, JLIFE ASC, QUESTIDF ASC, UNIQUE_NUMBER ASC");
        PreparedStatement ps = getPrepareStatement(sql.toString());
        
        
        ResultSet undqpf1rs = null;
        List<Undqpf> undqpfList = new ArrayList<Undqpf>();
      
        try {
        	ps.setString(1, chdrcoy);
        	ps.setString(2, chdrnum);
        	ps.setString(3, life);
        	ps.setString(4, jlife);
        	
        	undqpf1rs = ps.executeQuery();
            while (undqpf1rs.next()) {
            	Undqpf undqpf= new Undqpf();
            	
            	undqpf.setUnique_number(undqpf1rs.getLong("UNIQUE_NUMBER"));
            	undqpf.setLife(undqpf1rs.getString("LIFE"));
            	undqpf.setJlife(undqpf1rs.getString("JLIFE"));
            	undqpf.setValidflag(undqpf1rs.getString("VALIDFLAG"));
            	undqpf.setTranno(undqpf1rs.getInt("TRANNO"));
            	undqpf.setAnswer(undqpf1rs.getString("ANSWER"));
            	undqpf.setQuestidf(undqpf1rs.getString("QUESTIDF"));
            	undqpf.setChdrcoy(undqpf1rs.getString("CHDRCOY"));
            	undqpf.setChdrnum(undqpf1rs.getString("CHDRNUM"));
            	undqpf.setUndwrule(undqpf1rs.getString("UNDWRULE"));
            	undqpfList.add(undqpf);
         	}

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, undqpf1rs);
        }
        return undqpfList;
	}
            	
	public List<Undqpf> searchUndqRecord(String chdrcoy, String chdrnum,String life, String jlife){
		StringBuilder sqlUndqpf = new StringBuilder("SELECT UNIQUE_NUMBER, LIFE, JLIFE, VALIDFLAG, TRANNO, QUESTIDF, ANSWER, CHDRNUM ");
  		sqlUndqpf.append(" FROM UNDQ ");
  		sqlUndqpf.append(" WHERE CHDRCOY = ? AND CHDRNUM =?  AND LIFE = ? AND JLIFE = ? ");
        PreparedStatement psUndqpf = getPrepareStatement(sqlUndqpf.toString());
        ResultSet undqpf1rs = null;
        List<Undqpf> undqpfList = new ArrayList<Undqpf>();
        try {
        	psUndqpf.setString(1,chdrcoy);
        	psUndqpf.setString(2,chdrnum);
        	psUndqpf.setString(3, life);
        	psUndqpf.setString(4, jlife);
        	undqpf1rs = psUndqpf.executeQuery();
            while (undqpf1rs.next()) {
            	Undqpf undqpf= new Undqpf();
            	undqpf.setUnique_number(undqpf1rs.getLong("UNIQUE_NUMBER"));
            	undqpf.setLife(undqpf1rs.getString("LIFE"));
            	undqpf.setJlife(undqpf1rs.getString("JLIFE"));
            	undqpf.setValidflag(undqpf1rs.getString("VALIDFLAG"));
            	undqpf.setTranno(undqpf1rs.getInt("TRANNO"));
            	undqpf.setQuestidf(undqpf1rs.getString("QUESTIDF"));
            	undqpf.setAnswer(undqpf1rs.getString("ANSWER"));
            	undqpf.setChdrnum(undqpf1rs.getString("CHDRNUM"));
            	undqpfList.add(undqpf);
         	}
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(psUndqpf, undqpf1rs);
        }
        return undqpfList;
	}

	public boolean updateUndqpfRecords(List<Undqpf> undqpfUpdateList){
		boolean isUpdated = false;
		if (undqpfUpdateList != null && !undqpfUpdateList.isEmpty()) {
			String sql = "UPDATE UNDQPF SET ANSWER=?,UNDWRULE=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";          
			PreparedStatement psUndqUpdate = getPrepareStatement(sql);
			try {
				for (Undqpf u : undqpfUpdateList) {
					psUndqUpdate.setString(1,u.getAnswer());
					psUndqUpdate.setString(2, u.getUndwrule());	                    
					psUndqUpdate.setString(3, getJobnm());
					psUndqUpdate.setString(4, getUsrprf());
					psUndqUpdate.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
					psUndqUpdate.setLong(6, u.getUnique_number());					
					psUndqUpdate.addBatch();
					isUpdated = true;
				}
				psUndqUpdate.executeBatch();
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(psUndqUpdate, null);
			}
		}
		return isUpdated;
	}
	public void deleteUndqpfRecords(List<Undqpf> undqpfDeleteList){
		if (undqpfDeleteList != null && !undqpfDeleteList.isEmpty()) {
			String sql = "DELETE FROM UNDQPF WHERE UNIQUE_NUMBER=? ";
			PreparedStatement ps = getPrepareStatement(sql);
			try {
				for(Undqpf undqpf:undqpfDeleteList){
					ps.setLong(1, undqpf.getUnique_number());
					ps.addBatch();
				}
				ps.executeBatch();
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);
			}
		}
	}
	public boolean insertundqpf(List<Undqpf> undqpfList){
		boolean isInsertSuccessful = true;
		if (undqpfList != null && !undqpfList.isEmpty()) {
			StringBuilder sb = new StringBuilder("");
			sb.append("INSERT INTO UNDQPF (CHDRCOY,CHDRNUM,LIFE,JLIFE,CURRFROM,CURRTO,QUESTIDF,ANSWER,UNDWRULE,TRANNO,VALIDFLAG,JOBNM,USRPRF,DATIME) "); 
			sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) "); 
			PreparedStatement ps = null;
			ResultSet rs = null;
			try{
				ps = getPrepareStatement(sb.toString());
				for (Undqpf undq : undqpfList) {
					ps.setString(1, undq.getChdrcoy());
					ps.setString(2, undq.getChdrnum());
					ps.setString(3, undq.getLife());
					ps.setString(4, undq.getJlife());
					ps.setInt(5, undq.getCurrfrom());			    
					ps.setInt(6, undq.getCurrto());
					ps.setString(7, undq.getQuestidf());
					ps.setString(8, undq.getAnswer()); 			    
					ps.setString(9, undq.getUndwrule());		
					ps.setInt(10, undq.getTranno());
					ps.setString(11, undq.getValidflag());
					ps.setString(12, this.getUsrprf());
					ps.setString(13, this.getJobnm());
					ps.setTimestamp(14, new Timestamp(System.currentTimeMillis()));		    		    			    		    		    
					ps.addBatch();
				}		
				ps.executeBatch();
			}catch (SQLException e) {
				isInsertSuccessful = false;
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}			
		}	
		return isInsertSuccessful;
	}	
}
