/******************************************************************************
 * File Name 		: UndcpfDAOImpl.java
 * Author			: nloganathan5
 * Creation Date	: 27 October 2016
 * Project			: Integral Life
 * Description		: The DAO Implementation Class for UNDCPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.underwriting.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.underwriting.dataaccess.dao.UndcpfDAO;
import com.csc.life.underwriting.dataaccess.model.Undcpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class UndcpfDAOImpl extends BaseDAOImpl<Undcpf> implements UndcpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(UndcpfDAOImpl.class);

	public List<Undcpf> searchUndcpfRecord(String chdrcoy, String chdrnum) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlSelect.append("RIDER, CURRFROM, CURRTO, UNDWRULE, TRANNO, ");
		sqlSelect.append("SPECIND, VALIDFLAG, USRPRF, JOBNM, DATIME ");
		sqlSelect.append("FROM UNDCPF WHERE CHDRCOY = ? AND CHDRNUM = ? ");
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, UNIQUE_NUMBER ASC");


		PreparedStatement psUndcpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlUndcpfRs = null;
		List<Undcpf> outputList = new ArrayList<Undcpf>();

		try {

			psUndcpfSelect.setString(1, chdrcoy);
			psUndcpfSelect.setString(2, chdrnum);

			sqlUndcpfRs = executeQuery(psUndcpfSelect);

			while (sqlUndcpfRs.next()) {

				Undcpf undcpf = new Undcpf();

				undcpf.setUniqueNumber(sqlUndcpfRs.getInt("UNIQUE_NUMBER"));
				undcpf.setChdrcoy(sqlUndcpfRs.getString("CHDRCOY").charAt(0));
				undcpf.setChdrnum(sqlUndcpfRs.getString("CHDRNUM"));
				undcpf.setLife(sqlUndcpfRs.getString("LIFE"));
				undcpf.setJlife(sqlUndcpfRs.getString("JLIFE"));
				undcpf.setCoverage(sqlUndcpfRs.getString("COVERAGE"));
				undcpf.setRider(sqlUndcpfRs.getString("RIDER"));
				undcpf.setCurrfrom(sqlUndcpfRs.getInt("CURRFROM"));
				undcpf.setCurrto(sqlUndcpfRs.getInt("CURRTO"));
				undcpf.setUndwrule(sqlUndcpfRs.getString("UNDWRULE"));
				undcpf.setTranno(sqlUndcpfRs.getInt("TRANNO"));
				undcpf.setSpecind(getCharFromString(sqlUndcpfRs,"SPECIND"));
				undcpf.setValidflag(getCharFromString(sqlUndcpfRs,"VALIDFLAG"));
				undcpf.setUsrprf(sqlUndcpfRs.getString("USRPRF"));
				undcpf.setJobnm(sqlUndcpfRs.getString("JOBNM"));
				undcpf.setDatime(sqlUndcpfRs.getDate("DATIME"));

				outputList.add(undcpf);
			}

		} catch (SQLException e) {
				LOGGER.error("searchUndcpfRecord()", e);
				throw new SQLRuntimeException(e);
		} finally {
			close(psUndcpfSelect, sqlUndcpfRs);
		}

		return outputList;
	}
	
	private Character getCharFromString(ResultSet sqlpCovt1Rs , String columnName) throws SQLException{
		
		Character characterOutput = null;
		
		String stringOutput = sqlpCovt1Rs.getString(columnName);
		
		if(null != stringOutput){
			characterOutput = stringOutput.charAt(0);
		}
		
		return characterOutput;
	}
 //ILIFE-8709 start
	@Override
	public void updateUndcTranno(List<Undcpf> undcpfList) {
		 if (undcpfList == null || undcpfList.isEmpty()) {
	            return;
	        }
	        String sql = " UPDATE UNDCPF SET VALIDFLAG = ?, TRANNO = ?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER = ? ";
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try {
	            ps = getPrepareStatement(sql);
	            int ctr;
	            for (Undcpf undc : undcpfList) {
	                ctr = 0;
	                ps.setString(++ctr, undc.getValidflag().toString());
	                ps.setInt(++ctr, undc.getTranno());
	                ps.setString(++ctr, getJobnm());
	                ps.setString(++ctr, getUsrprf());
	                ps.setTimestamp(++ctr, getDatime());
	                ps.setLong(++ctr, undc.getUniqueNumber());
	                ps.addBatch();
	            }
	            ps.executeBatch();
	        } catch (SQLException e) {
	         	
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, rs);
	        }
	}
 //ILIFE-8709 end
 
 	//IBPLIFE-569
	public List<Undcpf> getUndcpfRecord(String chdrcoy, String chdrnum, String life) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlSelect.append("RIDER, CURRFROM, CURRTO, UNDWRULE, TRANNO, ");
		sqlSelect.append("SPECIND, VALIDFLAG, USRPRF, JOBNM, DATIME ");
		sqlSelect.append("FROM UNDCPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE= ? ");
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, UNIQUE_NUMBER ASC");


		PreparedStatement psUndcpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlUndcpfRs = null;
		List<Undcpf> outputList = new ArrayList<Undcpf>();

		try {

			psUndcpfSelect.setString(1, chdrcoy);
			psUndcpfSelect.setString(2, chdrnum);
			psUndcpfSelect.setString(3, life);

			sqlUndcpfRs = executeQuery(psUndcpfSelect);

			while (sqlUndcpfRs.next()) {

				Undcpf undcpf = new Undcpf();

				undcpf.setUniqueNumber(sqlUndcpfRs.getInt("UNIQUE_NUMBER"));
				undcpf.setChdrcoy(sqlUndcpfRs.getString("CHDRCOY").charAt(0));
				undcpf.setChdrnum(sqlUndcpfRs.getString("CHDRNUM"));
				undcpf.setLife(sqlUndcpfRs.getString("LIFE"));
				undcpf.setJlife(sqlUndcpfRs.getString("JLIFE"));
				undcpf.setCoverage(sqlUndcpfRs.getString("COVERAGE"));
				undcpf.setRider(sqlUndcpfRs.getString("RIDER"));
				undcpf.setCurrfrom(sqlUndcpfRs.getInt("CURRFROM"));
				undcpf.setCurrto(sqlUndcpfRs.getInt("CURRTO"));
				undcpf.setUndwrule(sqlUndcpfRs.getString("UNDWRULE"));
				undcpf.setTranno(sqlUndcpfRs.getInt("TRANNO"));
				undcpf.setSpecind(getCharFromString(sqlUndcpfRs,"SPECIND"));
				undcpf.setValidflag(getCharFromString(sqlUndcpfRs,"VALIDFLAG"));
				undcpf.setUsrprf(sqlUndcpfRs.getString("USRPRF"));
				undcpf.setJobnm(sqlUndcpfRs.getString("JOBNM"));
				undcpf.setDatime(sqlUndcpfRs.getDate("DATIME"));

				outputList.add(undcpf);
			}

		} catch (SQLException e) {
				LOGGER.error("getUndcpfRecord()", e);
				throw new SQLRuntimeException(e);
		} finally {
			close(psUndcpfSelect, sqlUndcpfRs);
		}

		return outputList;
	}

}
