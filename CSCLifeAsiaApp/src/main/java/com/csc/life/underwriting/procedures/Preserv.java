/*
 * File: Preserv.java
 * Date: 30 August 2009 1:57:40
 * Author: Quipoz Limited
 * 
 * Class transformed from PRESERV.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.underwriting.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;	//ILIFE-5601
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.procedures.Anpyintcal;
import com.csc.life.contractservicing.recordstructures.Annypyintcalcrec;
import com.csc.life.diary.dataaccess.dao.ZraepfDAO;
import com.csc.life.diary.dataaccess.model.Zraepf;
import com.csc.life.newbusiness.dataaccess.CovrtrbTableDAM;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5585rec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.underwriting.recordstructures.Actcalcrec;
import com.csc.life.underwriting.tablestructures.T6642rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*   TRADITIONAL LIFE NET PREMIUM ACTUARIAL RESERVE CALCULATION
*                FOR PERMANENT ASSURANCE
*
*   This subroutine is used as the calculation routine for
*   traditional type permanent assurance component surrender
*   values.
*
*   It will determine the Net Premium Actuarial Reserve for a
*   coverage or rider using actuarial functions calculated in
*   "ACTCALC".
*
*   A reserve value is calculated for both the Sum Assured and
*   the Reversionary Bonus and, as such, the program processes
*   on a FIRST-TIME-THROUGH/SECOND-TIME-THROUGH basis.
*   For the FIRST-TIME-THROUGH, the SUM ASSURED reserve is
*   calculated and for the SECOND-TIME-THROUGH the REVERSIONARY
*   BONUS reserve is calculated.
*
*   The program may be called to process either both of these
*   or the BONUS reserve only, i.e. to only do the SECOND-TIME-
*   THROUGH processing.
*
*   To establish which 'pass' the program is currently doing,
*   the program uses SURC-ENDF. If this is 'B', then this is
*   the SECOND-TIME-THROUGH, if spaces this is the FIRST-TIME-
*   THROUGH, otherwise this is an error.
*
*   To do the BONUS reserve only, the program must be called
*   with the SURC-STATUS = 'BONS'. If this is received by the
*   program, it immediately sets SURC-ENDF to 'B' to indicate
*   this is the SECOND-TIME-THROUGH.
*
*   The program can calculate these values for either a single
*   component or for the whole-plan.
*   This is established by examination of SURC-PLAN-SUFFIX.
*   If this is zero, then process the whole-plan, otherwise
*   process the single policy for the value passed.
*   For single component processing, the policy may be broken
*   out or it may be NOTIONAL, i.e. part of the summarised
*   record. The program establishes this before reading the
*   coverage file.
*
*   If this is a FULL SURRENDER, then a figure for the SUM
*   ASSURED reserve will be calculated for either the single
*   policy requested or for the whole plan by accumulation
*   of the figures for all the policy records pertaining to
*   this COVERAGE/RIDER (including the summary record).
*   This will then be repeated for the BONUS reserve value.
*
*   The SUM ASSURED is lifted directly from the component
*   coverage and the REVERSIONARY BONUS from the ACCOUNT BALANCE
*   file. The factor returned from the actuarial calculations is
*   then applied to this figure to give the RESERVE.
*   Where a reserve is required for a single NOTIONAL policy,
*   the figure for the summary record is passed and this will
*   be divided by the number of policies summarised subsequently.
*
*   The actuarial table to be used in each of these calculations
*   is read in from T6642.
*   If either of these is blank for either of the program passes,
*   then a zero reserve is assumed and passed back to the calling
*   program.
*
*   When calculating the Bonus Reserve value both the reserve and
*   the actual bonus are returned to the calling program.
*
*   FILE READS.
*   ----------
*
*   Coverage file for:
*   - determining assurance type (whole life or endowment)
*   - risk and premium cessation ages and terms.
*
*   Life file for:
*   - determining if single or joint life
*   - sexes of life (lives) assured
*   - age next birthdays at commencement date of lives assured
*
*   Account Balance File for:
*   - the total amount of bonus which has been applied to this
*     component.
*
*   PROCESSING.
*   ----------
*
*   INITIAL VALUES.
*   --------------
*
*   - Check value of SURC-STATUS.
*         SURC-STATUS = 'BONS' - Bonus Reserve Only.
*
*   - Set run type either WHOLE-PLAN, SINGLE-COMPONENT or
*     NOTIONAL-SINGLE-COMPONENT.
*
*   - BEGN on the COVR file.
*
*   - Determine if single or joint life and sexes of life(s)
*     assured.
*
*   - Read T6642 to establish:
*         - Age adjustment for females.
*         - Joint life equivalent age basis (for access to T5585)
*         - Single or joint life calculation basis as
*           appropriate.
*           Bases are read for Sum Assured and Rev Bonus and the
*           relevant one is set for use by "ACTCALC".
*
*   - Adjust ages if necessary and find joint equivalent age
*     for joint life cases.
*     - A single figure for Age Next Birthday at Contract
*       commencement date is required.
*     - Age adjustment is applied to any female life using the
*       figure read from T6642.
*     - Joint equivalent age is used in joint life cases and uses
*       the method read from T6642. The two ages are first
*       adjusted for females and then passed to T5585 to get a
*       single age equivalent.
*
*   MAIN PROCESSING LOOP.
*   --------------------
*
*   PERFORM UNTIL COVRTRB-STATUZ = ENDP
*
*   - Calculate duration in force of the component.
*     - A different duration figure is held for both BIG A and
*       A DUE calculation purposes.
*     - For both durations, this is calculated initially as the
*       difference between the paid to date and the contract
*       commencment date, i.e. the PREMIUM duration.
*     - If the current contract is such that the
*       paid to date has already expired, the duration for
*       BIG A is recalculated as the difference between
*       today's date and the contract commencment date, i.e.
*       the RISK duration.
*
*   - Determine assurance type (whole life or endowment).
*     - If age next birthday at risk cessation date is 99 then
*       this is, by definition, a whole of life. Otherwise, it
*       is an endowment.
*
*   - Get the transaction description and BONUS FIGURE if
*     appropriate.
*
*     Either:
*     -  the component description from T5687 (for Sum Assured)
*     Or:
*     -  the account type description from T3695 after reading
*        a dummy entry on T5645 and the ACBL file (for Rev Bon).
*
*   CALCULATE RESERVE FACTOR.
*
*   - Do the following calculations except where:
*     - Calculation basis is SPACES (from T6642) or
*       This is SECOND-TIME-THROUGH and the bonus figure is 0.
*     - For SECOND-TIME-THROUGH, only BIG A is required.
*
*   - Calculate the net premium for the component by calling
*     the subroutine "ACTCALC" with function WNETP or ENETP
*     according to whether whole life or endowment.
*
*   - Obtain value for "BIG A" at exact age of the life for this
*     component by calling "ACTCALC" with WBIGA or EBIGA, as
*     appropriate - for integral values either side of the age
*     required - and interpolating between them.
*
*   - Obtain value for "A DUE" at exact age of the life for this
*     component by calling "ACTCALC" with WADUE or EADUE, as
*     appropriate - for integral values either side of the age
*     required - and interpolating between them.
*
*   - Calculate surrender value factor as:
*
*          (BIG A - (Net Premium * A DUE)) * Sum Assured
*
*   - Apply this to either Sum Assured for FIRST-TIME-THROUGH
*                       or Rev Bonus for SECOND-TIME-THROUGH
*   - If this is second time through, then Net Premium and
*     A DUE will be zero.
*
*   - Add this to the total so far.
*
*   - If this is SINGLE COMPONENT, set COVRTRB-STATUZ to ENDP
*     and finish
*   - If this is WHOLE-PLAN, get the next coverage.
*     If this is a new COVERAGE/RIDER, set COVRTRB-STATUZ to ENDP
*     and finish otherwise return to top of MAIN PROCESSING LOOP.
*
*   END OF MAIN PROCESSING LOOP.
*   ---------------------------
*
*   SET EXIT VARIABLES.
*   ------------------
*   Set linkage variables as follows:
*
*   The accumulated reserve value is returned in
*   SURC-ACTUAL-VALUE for both SUM ASSURED reserve and BONUS
*   reserve.
*   For BONUS reserve only, the amount from the ACBL file is
*   also returned in the field SURC-ESTIMATED-VAL.
*
*   To indicate the progress of processing:
*
*   - STATUS -        '****' - FIRST-TIME-THROUGH
*                     'ENDP' - SECOND-TIME-THROUGH
*
*   To indicate the current pass:
*
*   - ENDF -          'B'    - FIRST-TIME-THROUGH
*                     ' '    - SECOND-TIME-THROUGH
*
*   To indicate the type of the current calculation:
*
*   - TYPE -          'S'    - FIRST-TIME-THROUGH
*                     'B'    - SECOND-TIME-THROUGH
*
*****************************************************************
* </pre>
*/
public class Preserv extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "PRESERV";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaLifeInd = new FixedLengthStringData(1);
	private Validator singleLife = new Validator(wsaaLifeInd, "1");
	private Validator jointLife = new Validator(wsaaLifeInd, "2");
		/* WSAA-LIFE-SEXES */
	private FixedLengthStringData wsaaLife1Sex = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaLife2Sex = new FixedLengthStringData(1);
		/* WSAA-LIFE-AGES */
	private ZonedDecimalData wsaaLife1AnbAtCcd = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaLife2AnbAtCcd = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaAnbAtCcd = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaAnbAtRcessDte = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaAnbAtPcessDte = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaRcessTerm = new ZonedDecimalData(11, 5);
	private ZonedDecimalData wsaaPcessTerm = new ZonedDecimalData(11, 5);
	private ZonedDecimalData wsaaAgeDifference = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaAnbDiff = new ZonedDecimalData(3, 0);

	private FixedLengthStringData wsaaAssType = new FixedLengthStringData(1);
	private Validator wholeLife = new Validator(wsaaAssType, "W");
	private Validator endowment = new Validator(wsaaAssType, "E");
	private PackedDecimalData wsaaSurrenderVal = new PackedDecimalData(18, 9);
	private PackedDecimalData wsaaReserveFactor = new PackedDecimalData(18, 9);
	private FixedLengthStringData wsaaSurrFlag = new FixedLengthStringData(1).init(SPACES);
		/* WSAA-EXACT-VALUES */
	private PackedDecimalData wsaaBiga = new PackedDecimalData(18, 10);
	private PackedDecimalData wsaaAdue = new PackedDecimalData(18, 10);
	private PackedDecimalData wsaaNetPremium = new PackedDecimalData(18, 10);
		/* WSAA-T-VALUES */
	private ZonedDecimalData wsaaIntLowTBiga = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaIntHighTBiga = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaIntLowTAdue = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaIntHighTAdue = new ZonedDecimalData(3, 0).setUnsigned();
		/* WSAA-FRACTIONS-DURATIONS */
	private ZonedDecimalData wsaaFractionBiga = new ZonedDecimalData(6, 5);
	private ZonedDecimalData wsaaDurationBiga = new ZonedDecimalData(11, 5);
	private ZonedDecimalData wsaaFractionAdue = new ZonedDecimalData(6, 5);
	private ZonedDecimalData wsaaDurationAdue = new ZonedDecimalData(11, 5);
		/* WSAA-BIGA-ADUE-VALUES */
	private PackedDecimalData wsaaLowValue = new PackedDecimalData(18, 10);
	private PackedDecimalData wsaaHighValue = new PackedDecimalData(18, 10);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaRunIndicator = new FixedLengthStringData(1);
	private Validator firstTimeThrough = new Validator(wsaaRunIndicator, " ");
	private Validator secondTimeThrough = new Validator(wsaaRunIndicator, "B");
	private Validator thirdTimeThrough = new Validator(wsaaRunIndicator, "E");

	private FixedLengthStringData wsaaAcblKey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaAcblChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaAcblKey, 0);
	private FixedLengthStringData wsaaAcblLife = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 8);
	private FixedLengthStringData wsaaAcblCoverage = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 10);
	private FixedLengthStringData wsaaAcblRider = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 12);
	private FixedLengthStringData wsaaAcblPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 14);
	private PackedDecimalData wsaaAcblCurrentBalance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaReserveTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaBonusTot = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaProcessingType = new FixedLengthStringData(1);
	private Validator wholePlan = new Validator(wsaaProcessingType, "1");
	private Validator singleComponent = new Validator(wsaaProcessingType, "2");
	private Validator notionalSingleComponent = new Validator(wsaaProcessingType, "3");
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
		/* FORMATS */
	private static final String acblrec = "ACBLREC";
	private static final String itemrec = "ITEMREC";
		/* TABLES */
	private static final String t6642 = "T6642";
	private static final String t5585 = "T5585";
	private static final String t5645 = "T5645";
	private static final String t5687 = "T5687";
	private static final String t3695 = "T3695";
	private static final String th506 = "TH506";
	private AcblTableDAM acblIO = new AcblTableDAM();
		/*COVR layout for trad. reversionary bonus*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life and joint life details - new busine*/
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5585rec t5585rec = new T5585rec();
	private T5645rec t5645rec = new T5645rec();
	private T6642rec t6642rec = new T6642rec();
	private Th506rec th506rec = new Th506rec();
	private Actcalcrec actcalcrec = new Actcalcrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private ErrorsInner errorsInner = new ErrorsInner();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Chdrpf chdrsur = null;
	private Itempf itempf = null;
	private Lifepf lifepf = null;
    private Covrpf covrpf = null;
    private List<Covrpf> covrpfList = null;
    private Iterator<Covrpf> covrpfIter;
    //ILIFE-7396 start
    private ExternalisedRules er = new ExternalisedRules();
    protected Wsspcomn wsspcomn = new Wsspcomn();
    private T5687rec t5687rec = new T5687rec();
   //ILIFE-7396 end
    private boolean endowFlag = false;
    private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
    private List<Acblpf> acblenqListLPAE = null;
	private List<Acblpf> acblenqListLPAS = null;
	private BigDecimal accAmtLPAE = new BigDecimal(0);
	private BigDecimal accAmtLPAS = new BigDecimal(0);
	private ZraepfDAO zraepfDAO =  getApplicationContext().getBean("zraepfDAO", ZraepfDAO.class);
	private Zraepf zraepf = new Zraepf();
	private List<Zraepf> zraepfList;
	private Annypyintcalcrec annypyintcalcrec = new Annypyintcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Batckey wsaaBatckey = new Batckey();
	public PackedDecimalData interestAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotInt = new PackedDecimalData(18, 3);
	private static final String PRPRO001 = "PRPRO001";   
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		continue2160, 
		exit3199, 
		seExit9090, 
		dbExit9190
	}

	public Preserv() {
		super();
	}

	

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		srcalcpy.surrenderRec = convertAndSetParam(srcalcpy.surrenderRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline1000()
	{
		/*MAIN*/
		initialise2000();
		//ILIFE-7396 start
		Vpxsurcrec vpxsurcrec=new Vpxsurcrec();
		int count=0;
		if(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VPMPRESERV") && er.isExternalized(chdrsur.getCnttype(), srcalcpy.crtable.toString())){
			vpxsurcrec.cnttype.set(chdrsur.getCnttype());
			vpxsurcrec.lage.set(wsaaLife1AnbAtCcd);
			vpxsurcrec.lsex.set(wsaaLife1Sex);
			vpxsurcrec.jlage.set(wsaaLife2AnbAtCcd);
			vpxsurcrec.jlsex.set(wsaaLife2Sex);
			vpxsurcrec.lifeInd.set(wsaaLifeInd);
			//readMethod();
			//vpxsurcrec.chargeCalc.set(t5687rec.svMethod);
			vpxsurcrec.chargeCalc.set("SC01");
		}
		if(covrpfList != null && covrpfList.size()>0){ //LIFE-5779
		 covrpfIter = covrpfList.iterator();
		  while (covrpfIter.hasNext()) {
			obtainInitVals3000();
			readAcbl7000();
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VPMPRESERV") && er.isExternalized(vpxsurcrec.cnttype.toString(), srcalcpy.crtable.toString()))){ 
				if(!thirdTimeThrough.isTrue())
				calcSurrenderValue4000();
			}
			else{
				switch(count){
				case 0:
					vpxsurcrec.covrrcomdate00.set(covrpf.getCrrcd());
					vpxsurcrec.covrrcesdate00.set(covrpf.getRiskCessDate());
					vpxsurcrec.covrpcesdate00.set(covrpf.getPremCessDate());
					vpxsurcrec.covrsumins00.set(covrpf.getSumins());
					vpxsurcrec.acblsacscurbal00.set(wsaaAcblCurrentBalance);
					break;
				case 1:
					vpxsurcrec.covrrcomdate01.set(covrpf.getCrrcd());
					vpxsurcrec.covrrcesdate01.set(covrpf.getRiskCessDate());
					vpxsurcrec.covrpcesdate01.set(covrpf.getPremCessDate());
					vpxsurcrec.covrsumins01.set(covrpf.getSumins());
					vpxsurcrec.acblsacscurbal01.set(wsaaAcblCurrentBalance);
					break;
				case 2:
					vpxsurcrec.covrrcomdate02.set(covrpf.getCrrcd());
					vpxsurcrec.covrrcesdate02.set(covrpf.getRiskCessDate());
					vpxsurcrec.covrpcesdate02.set(covrpf.getPremCessDate());
					vpxsurcrec.covrsumins02.set(covrpf.getSumins());
					vpxsurcrec.acblsacscurbal02.set(wsaaAcblCurrentBalance);
					break;
				case 3:
					vpxsurcrec.covrrcomdate03.set(covrpf.getCrrcd());
					vpxsurcrec.covrrcesdate03.set(covrpf.getRiskCessDate());
					vpxsurcrec.covrpcesdate03.set(covrpf.getPremCessDate());
					vpxsurcrec.covrsumins03.set(covrpf.getSumins());
					vpxsurcrec.acblsacscurbal03.set(wsaaAcblCurrentBalance);
					break;
				case 4:
					vpxsurcrec.covrrcomdate04.set(covrpf.getCrrcd());
					vpxsurcrec.covrrcesdate04.set(covrpf.getRiskCessDate());
					vpxsurcrec.covrpcesdate04.set(covrpf.getPremCessDate());
					vpxsurcrec.covrsumins04.set(covrpf.getSumins());
					vpxsurcrec.acblsacscurbal04.set(wsaaAcblCurrentBalance);
					break;
				case 5:
					vpxsurcrec.covrrcomdate05.set(covrpf.getCrrcd());
					vpxsurcrec.covrrcesdate05.set(covrpf.getRiskCessDate());
					vpxsurcrec.covrpcesdate05.set(covrpf.getPremCessDate());
					vpxsurcrec.covrsumins05.set(covrpf.getSumins());
					vpxsurcrec.acblsacscurbal05.set(wsaaAcblCurrentBalance);
					break;
				case 6:
					vpxsurcrec.covrrcomdate06.set(covrpf.getCrrcd());
					vpxsurcrec.covrrcesdate06.set(covrpf.getRiskCessDate());
					vpxsurcrec.covrpcesdate06.set(covrpf.getPremCessDate());
					vpxsurcrec.covrsumins06.set(covrpf.getSumins());
					vpxsurcrec.acblsacscurbal06.set(wsaaAcblCurrentBalance);
					break;
				case 7:
					vpxsurcrec.covrrcomdate07.set(covrpf.getCrrcd());
					vpxsurcrec.covrrcesdate07.set(covrpf.getRiskCessDate());
					vpxsurcrec.covrpcesdate07.set(covrpf.getPremCessDate());
					vpxsurcrec.covrsumins07.set(covrpf.getSumins());
					vpxsurcrec.acblsacscurbal07.set(wsaaAcblCurrentBalance);
					break;
				case 8:
					vpxsurcrec.covrrcomdate08.set(covrpf.getCrrcd());
					vpxsurcrec.covrrcesdate08.set(covrpf.getRiskCessDate());
					vpxsurcrec.covrpcesdate08.set(covrpf.getPremCessDate());
					vpxsurcrec.covrsumins08.set(covrpf.getSumins());
					vpxsurcrec.acblsacscurbal08.set(wsaaAcblCurrentBalance);
					break;
				case 9:
					vpxsurcrec.covrrcomdate09.set(covrpf.getCrrcd());
					vpxsurcrec.covrrcesdate09.set(covrpf.getRiskCessDate());
					vpxsurcrec.covrpcesdate09.set(covrpf.getPremCessDate());
					vpxsurcrec.covrsumins09.set(covrpf.getSumins());
					vpxsurcrec.acblsacscurbal09.set(wsaaAcblCurrentBalance);
					break;
				}	
				count++;
			}
			if (singleComponent.isTrue()
			|| notionalSingleComponent.isTrue()) {
			  break;
			}
			else {
				covrpf = covrpfIter.next();
		 	}
		  }
		}                                         //LIFE-5779
		  
		if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VPMPRESERV") && er.isExternalized(vpxsurcrec.cnttype.toString(), srcalcpy.crtable.toString()))){
			vpxsurcrec.covrcnt.set(count);
			if (secondTimeThrough.isTrue() && isEQ(wsaaAcblCurrentBalance,0)) {
				wsaaReserveTot.set(0);
				wsaaBonusTot.set(0);
			}
			else{
			callProgram("VPMPRESERV", srcalcpy.surrenderRec, vpxsurcrec);
			wsaaReserveTot.set(srcalcpy.actualVal);
			wsaaBonusTot.set(srcalcpy.estimatedVal);
			}
		}

		setExitVariables5000();
		  /*RETURN*/
		  exitProgram();
	}

protected void readMethod()
{
	itdmIO.setDataKey(SPACES);
	itdmIO.setItemcoy(wsspcomn.company);
	itdmIO.setItemtabl(t5687);
	itdmIO.setItemitem(srcalcpy.crtable);
	itdmIO.setFunction(varcom.begn);
	itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.getStatuz(), varcom.oK)
	&& isNE(itdmIO.getStatuz(), varcom.endp)) {
		syserrrec.params.set(itdmIO.getParams());
		dbError9100();
	}
	if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
	|| isNE(itdmIO.getItemtabl(), t5687)
	|| isNE(itdmIO.getItemitem(), srcalcpy.crtable)
	|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
		syserrrec.params.set(itdmIO.getParams());
		dbError9100();
	}
	else {
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
	}
}
//ILIFE-7396 end
protected void initialise2000()
	{
	    endowFlag = FeaConfg.isFeatureExist("2", PRPRO001, appVars, "IT");
		syserrrec.subrname.set(wsaaSubr);
		wsaaSurrFlag.set(SPACES);
		/*  Is this a BONUS SURRENDER ONLY run ?*/
		if (isEQ(srcalcpy.status,"BONS")) {
			wsaaRunIndicator.set("B");
		}
		else {
			wsaaRunIndicator.set(srcalcpy.endf);
		}
		if (!firstTimeThrough.isTrue()
		&& !secondTimeThrough.isTrue()
		&& !thirdTimeThrough.isTrue()) {
			
			syserrrec.params.set(srcalcpy.endf);
			syserrrec.statuz.set(errorsInner.h155);
			systemError9000();
		}
		wsaaReserveTot.set(ZERO);
		wsaaBonusTot.set(ZERO);
		if (isEQ(srcalcpy.planSuffix,ZERO)) {
			wsaaProcessingType.set("1");
		}
		else {
			if (isLTE(srcalcpy.planSuffix,srcalcpy.polsum)
			&& isNE(srcalcpy.polsum,1)) {
				wsaaProcessingType.set("3");
			}
			else {
				wsaaProcessingType.set("2");
			}
		}
		if (firstTimeThrough.isTrue()) {
			getSaDescription2400();
			
		}
		else {
			getRbDescAndAmt2500();
		}
		if(!thirdTimeThrough.isTrue()) 
		{
			readFiles2100();
			readT6642SetBasis2200();
			setAnbAtCcd2300();
		} else {
			readZrae();
			if(zraepfList.size() > 0)
				calcEndowment();
		}		
	}

private void displayEndowment() {		
	wsaaTotInt.set(ZERO);	
	wsaaTotInt.add(accAmtLPAE.doubleValue());
	wsaaTotInt.add(accAmtLPAS.doubleValue());
	wsaaTotInt.add(interestAmount);
	srcalcpy.actualVal.set(wsaaTotInt);
	if (isLT(srcalcpy.actualVal,ZERO)) {
		srcalcpy.actualVal.set(ZERO);
	}
	srcalcpy.type.set("E");
	
}

protected void readFiles2100()
	{
		/* Read CHDRSUR for the no. of years in force.                     */
	    List list = chdrpfDAO.getChdrpfItempfByConAndNumAndServunit(srcalcpy.chdrChdrcoy.toString(), srcalcpy.chdrChdrnum.toString(), "IT", th506);
	    String gen = "";//IJTI-320
		if (list == null || list.size() == 0) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(srcalcpy.chdrChdrcoy);
			stringVariable1.addExpression(srcalcpy.chdrChdrnum);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(errorsInner.e544);
			dbError9100();
		} else {//IJTI-320 START
			for(int i=0; list!=null && i<list.size(); i++){
				
				  if(list.get(i) instanceof Chdrpf){
					  chdrsur = new Chdrpf();
					  chdrsur = (Chdrpf)list.get(i);
					  continue;
				  }
				  
				  if(list.get(i) instanceof String){
					  gen=(String)list.get(i);
					  continue;
				  }
				  
			}	
		}
		wsaaSurrFlag.set("N");
		//IJTI-320 END
		if(gen.equals("")){
			catchAll2150();
			continue2160();
		}else{
			th506rec.th506Rec.set(gen);
			continue2160();
		}
	
	}

protected void catchAll2150()
	{
	    itempf = new Itempf();
	    itempf.setItempfx("IT");
	    itempf.setItemcoy(chdrsur.getChdrcoy().toString());
	    itempf.setItemtabl(th506);
	    itempf.setItemitem("***");
	    itempf = itemDAO.getItempfRecord(itempf);
		/*                                                         <V4LAQR>*/
		if (itempf == null) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(errorsInner.hl62);
			dbError9100();
		}
		th506rec.th506Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

protected void continue2160()
	{
		
		if (isNE(th506rec.yearInforce,ZERO)) {
			datcon2rec.intDate1.set(chdrsur.getOccdate());
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(th506rec.yearInforce);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				dbError9100();
			}
			if (isGT(chdrsur.getPtdate(),datcon2rec.intDate2)) {
				wsaaSurrFlag.set("Y");
			}
		}
		else {
			wsaaSurrFlag.set("Y");
		}
		/*  Read Coverage File.*/
		covrpfList = covrpfDAO.getcovrtrbRecord(srcalcpy.chdrChdrcoy.toString(), srcalcpy.chdrChdrnum.toString(), srcalcpy.lifeLife.toString(), srcalcpy.covrCoverage.toString(), srcalcpy.covrRider.toString());

	
		if (covrpfList == null || covrpfList.size() == 0) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(srcalcpy.chdrChdrcoy);
			stringVariable1.addExpression(srcalcpy.chdrChdrnum);
			stringVariable1.addExpression(srcalcpy.lifeLife);
			stringVariable1.addExpression(srcalcpy.covrCoverage);
			stringVariable1.addExpression(srcalcpy.covrRider);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(errorsInner.g344);
			dbError9100();
		}
		
		covrpfIter = covrpfList.iterator();
		if(covrpfIter.hasNext()){    //LIFE-5779
		covrpf = covrpfIter.next();
		}                            //LIFE-5779
		if ((singleComponent.isTrue()
		&& isNE(covrpf.getPlanSuffix(),srcalcpy.planSuffix))
		|| (notionalSingleComponent.isTrue()
		&& isNE(covrpf.getPlanSuffix(),0))) {
			wsaaPlan.set(srcalcpy.planSuffix);
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(srcalcpy.chdrChdrcoy);
			stringVariable2.addExpression(srcalcpy.chdrChdrnum);
			stringVariable2.addExpression(srcalcpy.lifeLife);
			stringVariable2.addExpression(srcalcpy.covrCoverage);
			stringVariable2.addExpression(srcalcpy.covrRider);
			stringVariable2.addExpression(wsaaPlan);
			stringVariable2.setStringInto(syserrrec.params);
			syserrrec.statuz.set(errorsInner.g344);
			dbError9100();
		}
		/*  Read Life file for first life.*/
		
		List<Lifepf> lifepfList = lifepfDAO.getLifelnb(srcalcpy.chdrChdrcoy.toString(), srcalcpy.chdrChdrnum.toString(), srcalcpy.lifeLife.toString(), new String[]{"00","01"});
		
		if(lifepfList == null || lifepfList.size() == 0){
			syserrrec.params.set(srcalcpy.chdrChdrnum.toString() + srcalcpy.lifeLife.toString());
			syserrrec.statuz.set("MRNF");
			dbError9100();
		}
		boolean flagJlife0 = false;
		boolean flagJlife1 = false;
		for(int i=0; lifepfList!=null && i<lifepfList.size(); i++){//IJTI-320
			lifepf = lifepfList.get(i);
			if(lifepf.getJlife().trim().equals("00")){
				wsaaLife1Sex.set(lifepf.getCltsex());
				wsaaLife1AnbAtCcd.set(lifepf.getAnbAtCcd());
				flagJlife0 = true;
				continue;
			}
			if(lifepf.getJlife().trim().equals("01")){
				wsaaLife2Sex.set(lifepf.getCltsex());
				wsaaLife2AnbAtCcd.set(lifepf.getAnbAtCcd());
				wsaaLifeInd.set("2");
				flagJlife1 = true;
				continue;
			}
		}
		
		if(!flagJlife0){
			syserrrec.params.set(srcalcpy.chdrChdrnum.toString() + srcalcpy.lifeLife.toString()+"00");
			syserrrec.statuz.set("MRNF");
			dbError9100();
		}
		if(!flagJlife1){
			wsaaLife2Sex.set(SPACES);
			wsaaLifeInd.set("1");
		}
		
	}

protected void readT6642SetBasis2200()
	{
		
		/*  Read T6642 to get age adjustment for females, joint equivalent*/
		/*  age basis, calculation basis.*/
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t6642);
		itdmIO.setItemitem(srcalcpy.crtable);
		itdmIO.setItmfrm(srcalcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError9100();
		}
		if (isNE(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t6642)
		|| isNE(itdmIO.getItemitem(),srcalcpy.crtable)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(srcalcpy.crtable);
			syserrrec.statuz.set(errorsInner.g579);
			dbError9100();
		}
		else {
			t6642rec.t6642Rec.set(itdmIO.getGenarea());
		}
		/*  Set the calculation basis according to type of run and*/
		/*  number of lives assured.*/
		/*  If this is the first time through then calculate the Sum*/
		/*  Assured reserve and set Run Indicator accordingly. If this is*/
		/*  the second time through then calculate the Reversionary Bonus*/
		/*  reserve.*/
		/*  Use joint or single life basis as appropriate.*/
		if (firstTimeThrough.isTrue()) {
			if (singleLife.isTrue()) {
				actcalcrec.basis.set(t6642rec.slsumass);
			}
			else {
				actcalcrec.basis.set(t6642rec.jlsumass);
			}
		}
		else {
			if (singleLife.isTrue()) {
				actcalcrec.basis.set(t6642rec.slrevbns);
			}
			else {
				actcalcrec.basis.set(t6642rec.jlrevbns);
			}
			srcalcpy.type.set("B");
		}
		/*  If either life is female, check that a valid female adjustment*/
		/*  has been read from T6642.*/
		if (isEQ(wsaaLife1Sex,"F")
		|| isEQ(wsaaLife2Sex,"F")) {
			if (isEQ(t6642rec.fmageadj,SPACES)) {
				syserrrec.params.set(srcalcpy.crtable);
				syserrrec.statuz.set(errorsInner.g584);
				dbError9100();
			}
		}
		/*  If joint life, check that a joint life equivalent age basis*/
		/*  is present on T6642.*/
		/*IF T6642-JLEQAGE = SPACES                                    */
		if (jointLife.isTrue()) {
			if (isEQ(t6642rec.jleqage,SPACES)) {
				syserrrec.params.set(srcalcpy.crtable);
				syserrrec.statuz.set(errorsInner.g585);
				dbError9100();
			}
		}
	}

protected void setAnbAtCcd2300()
	{
		wsaaAnbDiff.set(ZERO);
		/*  Set Age Next Birthday at Contract Commencement Date*/
		/*  for Life 1. First, adjust for female if necessary.*/
		if (isEQ(wsaaLife1Sex,"F")) {
			wsaaAnbDiff.add(t6642rec.fmageadj);
			wsaaLife1AnbAtCcd.add(t6642rec.fmageadj);
		}
		/*  Is this Single or Joint Life ie. for ANB-AT-CCD, is further*/
		/*  processing necessary.*/
		if (singleLife.isTrue()) {
			wsaaAnbAtCcd.set(wsaaLife1AnbAtCcd);
			return ;
		}
		/*  Set Age Next Birthday at Contract Commencement Date*/
		/*  for Life 2. First, adjust for female if necessary.*/
		if (isEQ(wsaaLife2Sex,"F")) {
			wsaaAnbDiff.add(t6642rec.fmageadj);
			wsaaLife2AnbAtCcd.add(t6642rec.fmageadj);
		}
		/*    Read T5585 to find single age equivalent for joint life*/
		/*    case.*/
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5585);
		itdmIO.setItemitem(t6642rec.jleqage);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isNE(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5585)
		|| isNE(itdmIO.getItemitem(),t6642rec.jleqage)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(srcalcpy.crtable);
			syserrrec.statuz.set(errorsInner.g580);
			dbError9100();
		}
		else {
			t5585rec.t5585Rec.set(itdmIO.getGenarea());
		}
		if (isGT(wsaaLife1AnbAtCcd,wsaaLife2AnbAtCcd)) {
			compute(wsaaAgeDifference, 0).set(sub(wsaaLife1AnbAtCcd,wsaaLife2AnbAtCcd));
		}
		else {
			compute(wsaaAgeDifference, 0).set(sub(wsaaLife2AnbAtCcd,wsaaLife1AnbAtCcd));
		}
		/*  Read table data to find appropriate age difference.*/
		/*  First increment subscript to correct table entry.*/
		wsaaSub.set(1);
		while ( !((isGT(wsaaSub,18))
		|| (isLTE(wsaaAgeDifference,t5585rec.agedif[wsaaSub.toInt()])))) {
			wsaaSub.add(1);
		}
		
		/*  If subscript > 18 then the appropriate table entry not found.*/
		if (isGT(wsaaSub,18)) {
			syserrrec.statuz.set(errorsInner.f262);
			systemError9000();
		}
		if (isEQ(t5585rec.hghlowage,"H")) {
			if (isGT(wsaaLife1AnbAtCcd,wsaaLife2AnbAtCcd)) {
				wsaaAnbDiff.add(t5585rec.addage[wsaaSub.toInt()]);
				compute(wsaaAnbAtCcd, 0).set(add(t5585rec.addage[wsaaSub.toInt()],wsaaLife1AnbAtCcd));
			}
			else {
				wsaaAnbDiff.add(t5585rec.addage[wsaaSub.toInt()]);
				compute(wsaaAnbAtCcd, 0).set(add(t5585rec.addage[wsaaSub.toInt()],wsaaLife2AnbAtCcd));
			}
		}
		/*  Which age to adjust - highest or lowest ?*/
		/*  Which age is highest or lowest ?*/
		if (isEQ(t5585rec.hghlowage,"L")) {
			if (isGT(wsaaLife1AnbAtCcd,wsaaLife2AnbAtCcd)) {
				wsaaAnbDiff.add(t5585rec.addage[wsaaSub.toInt()]);
				compute(wsaaAnbAtCcd, 0).set(add(t5585rec.addage[wsaaSub.toInt()],wsaaLife2AnbAtCcd));
			}
			else {
				wsaaAnbDiff.add(t5585rec.addage[wsaaSub.toInt()]);
				compute(wsaaAnbAtCcd, 0).set(add(t5585rec.addage[wsaaSub.toInt()],wsaaLife1AnbAtCcd));
			}
		}
	}

protected void getSaDescription2400()
	{
		start2400();
	}

	/**
	* <pre>
	*  This section is used when calculation sum assured reserve
	*  ie. first time through.
	*  It reads the T5687 using DESCIO to get the component
	*  description.
	* </pre>
	*/
protected void start2400()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(srcalcpy.chdrChdrcoy);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(srcalcpy.crtable);
		descIO.setLanguage(srcalcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			systemError9000();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(errorsInner.h053);
			syserrrec.params.set(descIO.getParams());
			systemError9000();
		}
		if (isEQ(descIO.getShortdesc(),SPACES)) {
			srcalcpy.description.fill("?");
		}
		else {
			srcalcpy.description.set(descIO.getShortdesc());
		}
	}

protected void getRbDescAndAmt2500()
	{
		start2500();
	}

	/**
	* <pre>
	*  This section is used when calculating Reversionary Bonus
	*  reserve ie. second time through. Table T5645 is read to get
	*  sub-account code and type to be used as key information
	*  to read the account balance file. This file is read to get
	*  a figure for the reversionary bonus reserve for the
	*  component currently being processed. The SACSTYPE from T5645
	*  is then also used as key information to T3695 to get the
	*  sub-account type description.
	* </pre>
	*/
protected void start2500()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError9000();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(errorsInner.h134);
			systemError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/*  Read T3695 description to get SACSTYPE description.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(srcalcpy.chdrChdrcoy);
		descIO.setDesctabl(t3695);
		if(!thirdTimeThrough.isTrue()){
		descIO.setDescitem(t5645rec.sacstype01);
		}else{
			descIO.setDescitem(t5645rec.sacstype03);	
		}
		descIO.setLanguage(srcalcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			systemError9000();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(errorsInner.h135);
			systemError9000();
		}
		if (isEQ(descIO.getShortdesc(),SPACES)) {
			srcalcpy.description.fill("?");
		}
		else {
			srcalcpy.description.set(descIO.getShortdesc());
		}
	}

protected void obtainInitVals3000()
	{
		/*START*/
		setDurationAndTVals3100();
		setAnbAtCessation3200();
		setAssType3300();
		/*EXIT*/
	}

protected void setDurationAndTVals3100()
	{
		try {
			premiumDuration3100();
			premiumTValues3110();
			premiumPaidUp3120();
			riskTValues3130();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	*  Call DATCON3 to find premium duration and if required, risk
	*  duration. Risk duration will be required if the policy is
	*  paid up (whether by PUP or by completion of the premium
	*  paying period) ie. the EFFECTIVE DATE is later than the
	*  PREMIUM CESSATION DATE.
	*  Risk duration is calculated as the difference between the
	*  effective date and the contract commencement date. Premium
	*  duration is calculated as the difference between paid to date
	*  and contract commencement date.
	*  The PREMIUM DURATION is initially moved to both BIG A and A
	*  DUE durations, however, if the policy is paid up, DURATION
	*  is overwritten with the RISK DURATION for BIG A.
	* </pre>
	*/
protected void premiumDuration3100()
	{
		/*  First calculate premium duration.*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(srcalcpy.crrcd);
		datcon3rec.intDate2.set(srcalcpy.ptdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		wsaaDurationBiga.set(datcon3rec.freqFactor);
	}

protected void premiumTValues3110()
	{
		/*  Set integral T values either side of the premium duration.*/
		/*  First set lower integral T value.*/
		wsaaIntLowTBiga.set(wsaaDurationBiga);
		wsaaIntLowTAdue.set(wsaaDurationBiga);
		/*  Set higher integral T value.*/
		compute(wsaaIntHighTBiga, 0).set(add(1,wsaaIntLowTBiga));
		compute(wsaaIntHighTAdue, 0).set(add(1,wsaaIntLowTBiga));
		/*  Get fractional part of premium duration.*/
		compute(wsaaFractionBiga, 5).set(sub(wsaaDurationBiga,wsaaIntLowTBiga));
		compute(wsaaFractionAdue, 5).set(sub(wsaaDurationBiga,wsaaIntLowTBiga));
	}

protected void premiumPaidUp3120()
	{
		/*  This is only required if the EFFECTIVE DATE is later than the*/
		/*  PREMIUM CESSATION DATE ie. the policy is paid in full.*/
		/*  It is only ever used in the calculation of BIG A and so the*/
		/*  high and low T values for BIG A must be set accordingly.*/
		if (isLTE(srcalcpy.effdate,covrpf.getPremCessDate())) {
			goTo(GotoLabel.exit3199);
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(srcalcpy.crrcd);
		datcon3rec.intDate2.set(srcalcpy.effdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		wsaaDurationBiga.set(datcon3rec.freqFactor);
	}

protected void riskTValues3130()
	{
		/*  Set integral T values either side of the risk duration.*/
		/*  First set lower integral T value.*/
		wsaaIntLowTBiga.set(wsaaDurationBiga);
		/*  Set higher integral T value.*/
		compute(wsaaIntHighTBiga, 0).set(add(1,wsaaIntLowTBiga));
		/*  Get fractional part of premium duration.*/
		compute(wsaaFractionBiga, 5).set(sub(wsaaDurationBiga,wsaaIntLowTBiga));
	}

protected void setAnbAtCessation3200()
	{
		start3200();
	}

protected void start3200()
	{
		/*  Calculate the Age Next Birthday at both Risk Cessation Date*/
		/*  and Premium Cessation Date by first calculating the Terms as*/
		/*  appropriate and adding these to the Age Next Birthday at*/
		/*  contract commencement date.*/
		/*  First calculate ANB at Risk Cessation Date.*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrpf.getCrrcd());
		datcon3rec.intDate2.set(covrpf.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		wsaaRcessTerm.set(datcon3rec.freqFactor);
		compute(wsaaAnbAtRcessDte, 5).set(add(wsaaRcessTerm,wsaaAnbAtCcd));
		/*  Second calculate ANB at Premium Cessation Date.*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrpf.getCrrcd());
		datcon3rec.intDate2.set(covrpf.getPremCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		wsaaPcessTerm.set(datcon3rec.freqFactor);
		compute(wsaaAnbAtPcessDte, 5).set(add(wsaaPcessTerm,wsaaAnbAtCcd));
	}

protected void setAssType3300()
	{
		/*START*/
		/*  Set WSAA-ASS-TYPE to 1 or 2 depending on whether whole life*/
		/*  or endowment. IF Age Next Birthday at Risk Cessation Date is*/
		/*  99 then this must be whole life, else it must be endowment.*/
		/*    IF WSAA-ANB-AT-RCESS-DTE = 99                                */
		/*IF (WSAA-LIFE1-ANB-AT-CCD + WSAA-RCESS-TERM) = 99       <001>*/
		if ((setPrecision(99, 5)
		&& isGTE(sub((add(wsaaLife1AnbAtCcd,wsaaRcessTerm)),wsaaAnbDiff),99))) {
			wsaaAssType.set("W");
		}
		else {
			wsaaAssType.set("E");
		}
		/*EXIT*/
	}

protected void calcSurrenderValue4000()
	{
		/*START*/
		actcalcrec.company.set(srcalcpy.chdrChdrcoy);
		wsaaSurrenderVal.set(0);
		wsaaBiga.set(0);
		wsaaNetPremium.set(0);
		wsaaAdue.set(0);
		/*  Return a zero value if either no basis is found or this*/
		/*  is SECOND-TIME-THROUGH and there has been no bonus.*/
		if ((isEQ(actcalcrec.basis,SPACES))
		|| (secondTimeThrough.isTrue()
		&& isEQ(wsaaAcblCurrentBalance,0))) {
			return ;
		}
		if (firstTimeThrough.isTrue()) {
			calculateNetPremium4100();
		}
		calculateBigA4200();
		if (firstTimeThrough.isTrue()) {
			calculateADue4300();
		}
		calculateNparValue4400();
		
		/*EXIT*/
	}

private void calculateInterest() {
	annypyintcalcrec.intcalcRec.set(SPACES);
	annypyintcalcrec.chdrcoy.set(zraepf.getChdrcoy());
	annypyintcalcrec.chdrnum.set(zraepf.getChdrnum());
	annypyintcalcrec.cnttype.set(chdrsur.getCnttype());
	annypyintcalcrec.interestTo.set(srcalcpy.effdate);
	annypyintcalcrec.interestFrom.set(zraepf.getAplstintbdte());
	annypyintcalcrec.annPaymt.set(zraepf.getApcaplamt());
	annypyintcalcrec.lastCaplsnDate.set(zraepf.getAplstcapdate());		
	annypyintcalcrec.interestAmount.set(ZERO);
	annypyintcalcrec.currency.set(zraepf.getPaycurr());
	annypyintcalcrec.transaction.set("ENDOWMENT");
	
	callProgram(Anpyintcal.class, annypyintcalcrec.intcalcRec);
	if (isNE(annypyintcalcrec.statuz, varcom.oK)) {
		syserrrec.params.set(annypyintcalcrec.intcalcRec);
		syserrrec.statuz.set(annypyintcalcrec.statuz);
		systemError9000();	
	}
	
	/* MOVE INTC-INTEREST-AMOUNT    TO ZRDP-AMOUNT-IN.              */
	/* PERFORM 5000-CALL-ROUNDING.                                  */
	/* MOVE ZRDP-AMOUNT-OUT         TO INTC-INTEREST-AMOUNT.        */
	if (isNE(annypyintcalcrec.interestAmount, 0)) {
		zrdecplrec.amountIn.set(annypyintcalcrec.interestAmount);
		callRounding5100();
		annypyintcalcrec.interestAmount.set(zrdecplrec.amountOut);
		interestAmount.add(annypyintcalcrec.interestAmount);
	}
	
}
private void callRounding5100() {

	/*CALL*/
	zrdecplrec.function.set(SPACES);
	zrdecplrec.company.set(2);
	zrdecplrec.statuz.set(varcom.oK);
	zrdecplrec.currency.set(zraepf.getPaycurr());
	zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
	callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
	if (isNE(zrdecplrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(zrdecplrec.statuz);
		syserrrec.params.set(zrdecplrec.zrdecplRec);
		systemError9000();
	}
	/*EXIT*/
	
}

protected void calculateNetPremium4100()
	{
		start4100();
	}

protected void start4100()
	{
		if (wholeLife.isTrue()) {
			actcalcrec.function.set("WNETP");
		}
		else {
			actcalcrec.function.set("ENETP");
		}
		actcalcrec.initialAge.set(wsaaAnbAtCcd);
		actcalcrec.finalAge1.set(wsaaAnbAtRcessDte);
		actcalcrec.finalAge2.set(wsaaAnbAtPcessDte);
		callProgram(Actcalc.class, actcalcrec.actcalcRec);
		if (isNE(actcalcrec.statuz,varcom.oK)) {
			syserrrec.params.set(actcalcrec.actcalcRec);
			syserrrec.statuz.set(actcalcrec.statuz);
			systemError9000();
		}
		wsaaNetPremium.set(actcalcrec.value);
	}

protected void calculateBigA4200()
	{
		start4200();
	}

protected void start4200()
	{
		/*  Call ACTCALC to get BIG A value at lower integral T value.*/
		actcalcrec.finalAge2.set(ZERO);
		if (wholeLife.isTrue()) {
			actcalcrec.function.set("WBIGA");
		}
		else {
			actcalcrec.function.set("EBIGA");
		}
		actcalcrec.finalAge1.set(wsaaAnbAtRcessDte);
		compute(actcalcrec.initialAge, 0).set(add(wsaaIntLowTBiga,wsaaAnbAtCcd));
		callProgram(Actcalc.class, actcalcrec.actcalcRec);
		if (isNE(actcalcrec.statuz,varcom.oK)) {
			syserrrec.params.set(actcalcrec.actcalcRec);
			syserrrec.statuz.set(actcalcrec.statuz);
			systemError9000();
		}
		wsaaLowValue.set(actcalcrec.value);
		/*  Call ACTCALC to get BIG A value at higher integral T value.*/
		compute(actcalcrec.initialAge, 0).set(add(wsaaIntHighTBiga,wsaaAnbAtCcd));
		callProgram(Actcalc.class, actcalcrec.actcalcRec);
		if (isNE(actcalcrec.statuz,varcom.oK)) {
			syserrrec.params.set(actcalcrec.actcalcRec);
			syserrrec.statuz.set(actcalcrec.statuz);
			systemError9000();
		}
		wsaaHighValue.set(actcalcrec.value);
		/*  Interpolate between lower and higher T values to find BIG A*/
		/*  at the actual current duration of contract.*/
		compute(wsaaBiga, 11).setRounded(add(mult(wsaaFractionBiga,(sub(wsaaHighValue,wsaaLowValue))),wsaaLowValue));
	}

protected void calculateADue4300()
	{
		start4300();
	}

protected void start4300()
	{
		actcalcrec.finalAge1.set(ZERO);
		/*  Call ACTCALC to get A DUE value at lower integral T value.*/
		if (wholeLife.isTrue()) {
			actcalcrec.function.set("WADUE");
		}
		else {
			actcalcrec.function.set("EADUE");
		}
		actcalcrec.finalAge2.set(wsaaAnbAtPcessDte);
		compute(actcalcrec.initialAge, 0).set(add(wsaaIntLowTAdue,wsaaAnbAtCcd));
		callProgram(Actcalc.class, actcalcrec.actcalcRec);
		if (isNE(actcalcrec.statuz,varcom.oK)) {
			syserrrec.params.set(actcalcrec.actcalcRec);
			syserrrec.statuz.set(actcalcrec.statuz);
			systemError9000();
		}
		wsaaLowValue.set(actcalcrec.value);
		/*  Call ACTCALC to get A DUE value at higher integral T value.*/
		compute(actcalcrec.initialAge, 0).set(add(wsaaIntHighTAdue,wsaaAnbAtCcd));
		callProgram(Actcalc.class, actcalcrec.actcalcRec);
		if (isNE(actcalcrec.statuz,varcom.oK)) {
			syserrrec.params.set(actcalcrec.actcalcRec);
			syserrrec.statuz.set(actcalcrec.statuz);
			systemError9000();
		}
		wsaaHighValue.set(actcalcrec.value);
		/*  Interpolate between lower and higher T values to find A DUE*/
		/*  at the actual current duration of contract.*/
		/*    COMPUTE WSAA-ADUE =                                          */
		compute(wsaaAdue, 11).setRounded(add(mult(wsaaFractionAdue,(sub(wsaaHighValue,wsaaLowValue))),wsaaLowValue));
	}

protected void calculateNparValue4400()
	{
		/*START*/
		/*  Compute actual surrender value using figures already obtained*/
		/*  for NET PREMIUM, BIG A and A DUE.*/
		/*    COMPUTE WSAA-RESERVE-FACTOR =                                */
		compute(wsaaReserveFactor, 11).setRounded(sub(wsaaBiga,(mult(wsaaNetPremium,wsaaAdue))));
		if (firstTimeThrough.isTrue()) {
			compute(wsaaSurrenderVal, 9).set(mult(wsaaReserveFactor,covrpf.getSumins()));
		}
		else {
			compute(wsaaSurrenderVal, 9).set(mult(wsaaReserveFactor,wsaaAcblCurrentBalance));
		}
		wsaaReserveTot.add(wsaaSurrenderVal);
		/*EXIT*/
	}

protected void calcEndowment(){
	
	acblenqListLPAE = acblpfDAO.getAcblenqRecord(srcalcpy.chdrChdrcoy.toString(), StringUtils.rightPad(srcalcpy.chdrChdrnum.toString(), 16),t5645rec.sacscode[2].toString(),t5645rec.sacstype[2].toString());

	acblenqListLPAS =acblpfDAO.getAcblenqRecord(srcalcpy.chdrChdrcoy.toString(), StringUtils.rightPad(srcalcpy.chdrChdrnum.toString(), 16),t5645rec.sacscode[3].toString(),t5645rec.sacstype[3].toString());
	
	if (acblenqListLPAE != null && !acblenqListLPAE.isEmpty()) {
		accAmtLPAE = acblenqListLPAE.get(0).getSacscurbal();
		accAmtLPAE = new BigDecimal(ZERO.toString()).subtract(accAmtLPAE);	
	}
	if (acblenqListLPAS != null && !acblenqListLPAS.isEmpty()) {
		accAmtLPAS =  acblenqListLPAS.get(0).getSacscurbal();
		accAmtLPAS = new BigDecimal(ZERO.toString()).subtract(accAmtLPAS);
		}
	
	calculateInterest();			
}
protected void readZrae()
{
	zraepf=zraepfDAO.getItemByContractNum(srcalcpy.chdrChdrnum.toString());
    List<String> chdrnumList = new ArrayList<String>();
    chdrnumList.add(srcalcpy.chdrChdrnum.toString());
    zraepfList = zraepfDAO.searchZraepfRecord(chdrnumList, srcalcpy.chdrChdrcoy.toString());

}

protected void setExitVariables5000()
	{
		start5000();
	}

protected void start5000()
	{
		if (isEQ(wsaaSurrFlag,"N")) {
			wsaaReserveTot.set(ZERO);
			wsaaBonusTot.set(ZERO);
		}
		srcalcpy.actualVal.set(wsaaReserveTot);
		srcalcpy.estimatedVal.set(0);
		if (firstTimeThrough.isTrue()) {
			srcalcpy.status.set("****");
			srcalcpy.endf.set("B");
			srcalcpy.type.set("S");
		}
		else {
			if (isEQ(srcalcpy.status,"BONS")) {
				srcalcpy.estimatedVal.set(wsaaBonusTot);
				srcalcpy.type.set("B");
			}		
			
			if(endowFlag && secondTimeThrough.isTrue()){		
				srcalcpy.status.set("****");
				srcalcpy.endf.set("E");
			} else {
				srcalcpy.status.set("ENDP");
				srcalcpy.endf.set(" ");
			}
			if(thirdTimeThrough.isTrue()){
				srcalcpy.actualVal.set(ZERO);
				if(zraepfList.size() > 0)
					displayEndowment();
				srcalcpy.status.set("ENDP");
				srcalcpy.endf.set(" ");
			}
		
			
		}
	}



protected void readAcbl7000()
	{
		start7000();
	}

protected void start7000()
	{
		/*  Read Account Balance file.*/
		acblIO.setParams(SPACES);
		acblIO.setRldgcoy(srcalcpy.chdrChdrcoy);
		acblIO.setSacscode(t5645rec.sacscode01);
		wsaaAcblChdrnum.set(srcalcpy.chdrChdrnum);
		wsaaAcblLife.set(srcalcpy.lifeLife);
		wsaaAcblCoverage.set(srcalcpy.covrCoverage);
		wsaaAcblRider.set(srcalcpy.covrRider);
		wsaaPlan.set(covrpf.getPlanSuffix());
		wsaaAcblPlanSuffix.set(wsaaPlansuff);
		acblIO.setRldgacct(wsaaAcblKey);
		acblIO.setOrigcurr(srcalcpy.currcode);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if ((isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			dbError9100();
		}
		if (isEQ(acblIO.getStatuz(),varcom.mrnf)) {
			wsaaAcblCurrentBalance.set(0);
		}
		else {
			wsaaAcblCurrentBalance.set(acblIO.getSacscurbal());
		}
		/*  ACBL balance will be held as a negative number.*/
		/*  Comment above incorrect, ACBL balance changed to be assumed to*/
		/*  be held as POSITIVE.*/
		/*    MULTIPLY WSAA-ACBL-CURRENT-BALANCE BY -1 GIVING              */
		/*                                     WSAA-ACBL-CURRENT-BALANCE.  */
		wsaaBonusTot.add(wsaaAcblCurrentBalance);
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9000();
				case seExit9090: 
					seExit9090();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		srcalcpy.status.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9100();
				case dbExit9190: 
					dbExit9190();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		srcalcpy.status.set(varcom.bomb);
		exitProgram();
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData g344 = new FixedLengthStringData(4).init("G344");
	private FixedLengthStringData g579 = new FixedLengthStringData(4).init("G579");
	private FixedLengthStringData g580 = new FixedLengthStringData(4).init("G580");
	private FixedLengthStringData g584 = new FixedLengthStringData(4).init("G584");
	private FixedLengthStringData g585 = new FixedLengthStringData(4).init("G585");
	private FixedLengthStringData f262 = new FixedLengthStringData(4).init("F262");
	private FixedLengthStringData h134 = new FixedLengthStringData(4).init("H134");
	private FixedLengthStringData h135 = new FixedLengthStringData(4).init("H135");
	private FixedLengthStringData h053 = new FixedLengthStringData(4).init("H053");
	private FixedLengthStringData h155 = new FixedLengthStringData(4).init("H155");
	private FixedLengthStringData e544 = new FixedLengthStringData(4).init("E544");
	private FixedLengthStringData hl62 = new FixedLengthStringData(4).init("HL62");
}
}
