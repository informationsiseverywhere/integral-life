/*
 * File: Cnvt5539.java
 * Date: December 3, 2013 2:17:11 AM ICT
 * Author: CSC
 * 
 * Class transformed from CNVT5539.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.catchall.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.unitlinkedprocessing.tablestructures.T5539rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table T5539, add new fields FACT01 to
*   FACT11.
*
*
****************************************************************** ****
* </pre>
*/
public class Cnvt5539 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVT5539");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String t5539 = "T5539";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5539rec t5539rec = new T5539rec();
	private Varcom varcom = new Varcom();
	private T5539OldRecInner t5539OldRecInner = new T5539OldRecInner();

	public Cnvt5539() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(t5539);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT T5539 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(), t5539)) {
			getAppVars().addDiagnostic("Table T5539 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itemIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT T5539 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteT55392080();
	}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		t5539OldRecInner.t5539OldRec.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		for (ix.set(1); !(isGT(ix, 99)); ix.add(1)){
			t5539rec.dfact[ix.toInt()].set(t5539OldRecInner.t5539OldDfact[ix.toInt()]);
		}
		for (ix.set(1); !(isGT(ix, 11)); ix.add(1)){
			t5539rec.fact[ix.toInt()].set(ZERO);
		}
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(t5539rec.t5539Rec);
	}

protected void rewriteT55392080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(), t5539)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for T5539");
		}
	}
/*
 * Class transformed  from Data Structure T5539-OLD-REC--INNER
 */
private static final class T5539OldRecInner { 

	private FixedLengthStringData t5539OldRec = new FixedLengthStringData(500);
	private FixedLengthStringData t5539OldDfacts = new FixedLengthStringData(495).isAPartOf(t5539OldRec, 0);
	private ZonedDecimalData[] t5539OldDfact = ZDArrayPartOfStructure(99, 5, 0, t5539OldDfacts, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(495).isAPartOf(t5539OldDfacts, 0, FILLER_REDEFINE);
	private ZonedDecimalData t5539OldDfact01 = new ZonedDecimalData(5, 0).isAPartOf(filler, 0);
	private ZonedDecimalData t5539OldDfact02 = new ZonedDecimalData(5, 0).isAPartOf(filler, 5);
	private ZonedDecimalData t5539OldDfact03 = new ZonedDecimalData(5, 0).isAPartOf(filler, 10);
	private ZonedDecimalData t5539OldDfact04 = new ZonedDecimalData(5, 0).isAPartOf(filler, 15);
	private ZonedDecimalData t5539OldDfact05 = new ZonedDecimalData(5, 0).isAPartOf(filler, 20);
	private ZonedDecimalData t5539OldDfact06 = new ZonedDecimalData(5, 0).isAPartOf(filler, 25);
	private ZonedDecimalData t5539OldDfact07 = new ZonedDecimalData(5, 0).isAPartOf(filler, 30);
	private ZonedDecimalData t5539OldDfact08 = new ZonedDecimalData(5, 0).isAPartOf(filler, 35);
	private ZonedDecimalData t5539OldDfact09 = new ZonedDecimalData(5, 0).isAPartOf(filler, 40);
	private ZonedDecimalData t5539OldDfact10 = new ZonedDecimalData(5, 0).isAPartOf(filler, 45);
	private ZonedDecimalData t5539OldDfact11 = new ZonedDecimalData(5, 0).isAPartOf(filler, 50);
	private ZonedDecimalData t5539OldDfact12 = new ZonedDecimalData(5, 0).isAPartOf(filler, 55);
	private ZonedDecimalData t5539OldDfact13 = new ZonedDecimalData(5, 0).isAPartOf(filler, 60);
	private ZonedDecimalData t5539OldDfact14 = new ZonedDecimalData(5, 0).isAPartOf(filler, 65);
	private ZonedDecimalData t5539OldDfact15 = new ZonedDecimalData(5, 0).isAPartOf(filler, 70);
	private ZonedDecimalData t5539OldDfact16 = new ZonedDecimalData(5, 0).isAPartOf(filler, 75);
	private ZonedDecimalData t5539OldDfact17 = new ZonedDecimalData(5, 0).isAPartOf(filler, 80);
	private ZonedDecimalData t5539OldDfact18 = new ZonedDecimalData(5, 0).isAPartOf(filler, 85);
	private ZonedDecimalData t5539OldDfact19 = new ZonedDecimalData(5, 0).isAPartOf(filler, 90);
	private ZonedDecimalData t5539OldDfact20 = new ZonedDecimalData(5, 0).isAPartOf(filler, 95);
	private ZonedDecimalData t5539OldDfact21 = new ZonedDecimalData(5, 0).isAPartOf(filler, 100);
	private ZonedDecimalData t5539OldDfact22 = new ZonedDecimalData(5, 0).isAPartOf(filler, 105);
	private ZonedDecimalData t5539OldDfact23 = new ZonedDecimalData(5, 0).isAPartOf(filler, 110);
	private ZonedDecimalData t5539OldDfact24 = new ZonedDecimalData(5, 0).isAPartOf(filler, 115);
	private ZonedDecimalData t5539OldDfact25 = new ZonedDecimalData(5, 0).isAPartOf(filler, 120);
	private ZonedDecimalData t5539OldDfact26 = new ZonedDecimalData(5, 0).isAPartOf(filler, 125);
	private ZonedDecimalData t5539OldDfact27 = new ZonedDecimalData(5, 0).isAPartOf(filler, 130);
	private ZonedDecimalData t5539OldDfact28 = new ZonedDecimalData(5, 0).isAPartOf(filler, 135);
	private ZonedDecimalData t5539OldDfact29 = new ZonedDecimalData(5, 0).isAPartOf(filler, 140);
	private ZonedDecimalData t5539OldDfact30 = new ZonedDecimalData(5, 0).isAPartOf(filler, 145);
	private ZonedDecimalData t5539OldDfact31 = new ZonedDecimalData(5, 0).isAPartOf(filler, 150);
	private ZonedDecimalData t5539OldDfact32 = new ZonedDecimalData(5, 0).isAPartOf(filler, 155);
	private ZonedDecimalData t5539OldDfact33 = new ZonedDecimalData(5, 0).isAPartOf(filler, 160);
	private ZonedDecimalData t5539OldDfact34 = new ZonedDecimalData(5, 0).isAPartOf(filler, 165);
	private ZonedDecimalData t5539OldDfact35 = new ZonedDecimalData(5, 0).isAPartOf(filler, 170);
	private ZonedDecimalData t5539OldDfact36 = new ZonedDecimalData(5, 0).isAPartOf(filler, 175);
	private ZonedDecimalData t5539OldDfact37 = new ZonedDecimalData(5, 0).isAPartOf(filler, 180);
	private ZonedDecimalData t5539OldDfact38 = new ZonedDecimalData(5, 0).isAPartOf(filler, 185);
	private ZonedDecimalData t5539OldDfact39 = new ZonedDecimalData(5, 0).isAPartOf(filler, 190);
	private ZonedDecimalData t5539OldDfact40 = new ZonedDecimalData(5, 0).isAPartOf(filler, 195);
	private ZonedDecimalData t5539OldDfact41 = new ZonedDecimalData(5, 0).isAPartOf(filler, 200);
	private ZonedDecimalData t5539OldDfact42 = new ZonedDecimalData(5, 0).isAPartOf(filler, 205);
	private ZonedDecimalData t5539OldDfact43 = new ZonedDecimalData(5, 0).isAPartOf(filler, 210);
	private ZonedDecimalData t5539OldDfact44 = new ZonedDecimalData(5, 0).isAPartOf(filler, 215);
	private ZonedDecimalData t5539OldDfact45 = new ZonedDecimalData(5, 0).isAPartOf(filler, 220);
	private ZonedDecimalData t5539OldDfact46 = new ZonedDecimalData(5, 0).isAPartOf(filler, 225);
	private ZonedDecimalData t5539OldDfact47 = new ZonedDecimalData(5, 0).isAPartOf(filler, 230);
	private ZonedDecimalData t5539OldDfact48 = new ZonedDecimalData(5, 0).isAPartOf(filler, 235);
	private ZonedDecimalData t5539OldDfact49 = new ZonedDecimalData(5, 0).isAPartOf(filler, 240);
	private ZonedDecimalData t5539OldDfact50 = new ZonedDecimalData(5, 0).isAPartOf(filler, 245);
	private ZonedDecimalData t5539OldDfact51 = new ZonedDecimalData(5, 0).isAPartOf(filler, 250);
	private ZonedDecimalData t5539OldDfact52 = new ZonedDecimalData(5, 0).isAPartOf(filler, 255);
	private ZonedDecimalData t5539OldDfact53 = new ZonedDecimalData(5, 0).isAPartOf(filler, 260);
	private ZonedDecimalData t5539OldDfact54 = new ZonedDecimalData(5, 0).isAPartOf(filler, 265);
	private ZonedDecimalData t5539OldDfact55 = new ZonedDecimalData(5, 0).isAPartOf(filler, 270);
	private ZonedDecimalData t5539OldDfact56 = new ZonedDecimalData(5, 0).isAPartOf(filler, 275);
	private ZonedDecimalData t5539OldDfact57 = new ZonedDecimalData(5, 0).isAPartOf(filler, 280);
	private ZonedDecimalData t5539OldDfact58 = new ZonedDecimalData(5, 0).isAPartOf(filler, 285);
	private ZonedDecimalData t5539OldDfact59 = new ZonedDecimalData(5, 0).isAPartOf(filler, 290);
	private ZonedDecimalData t5539OldDfact60 = new ZonedDecimalData(5, 0).isAPartOf(filler, 295);
	private ZonedDecimalData t5539OldDfact61 = new ZonedDecimalData(5, 0).isAPartOf(filler, 300);
	private ZonedDecimalData t5539OldDfact62 = new ZonedDecimalData(5, 0).isAPartOf(filler, 305);
	private ZonedDecimalData t5539OldDfact63 = new ZonedDecimalData(5, 0).isAPartOf(filler, 310);
	private ZonedDecimalData t5539OldDfact64 = new ZonedDecimalData(5, 0).isAPartOf(filler, 315);
	private ZonedDecimalData t5539OldDfact65 = new ZonedDecimalData(5, 0).isAPartOf(filler, 320);
	private ZonedDecimalData t5539OldDfact66 = new ZonedDecimalData(5, 0).isAPartOf(filler, 325);
	private ZonedDecimalData t5539OldDfact67 = new ZonedDecimalData(5, 0).isAPartOf(filler, 330);
	private ZonedDecimalData t5539OldDfact68 = new ZonedDecimalData(5, 0).isAPartOf(filler, 335);
	private ZonedDecimalData t5539OldDfact69 = new ZonedDecimalData(5, 0).isAPartOf(filler, 340);
	private ZonedDecimalData t5539OldDfact70 = new ZonedDecimalData(5, 0).isAPartOf(filler, 345);
	private ZonedDecimalData t5539OldDfact71 = new ZonedDecimalData(5, 0).isAPartOf(filler, 350);
	private ZonedDecimalData t5539OldDfact72 = new ZonedDecimalData(5, 0).isAPartOf(filler, 355);
	private ZonedDecimalData t5539OldDfact73 = new ZonedDecimalData(5, 0).isAPartOf(filler, 360);
	private ZonedDecimalData t5539OldDfact74 = new ZonedDecimalData(5, 0).isAPartOf(filler, 365);
	private ZonedDecimalData t5539OldDfact75 = new ZonedDecimalData(5, 0).isAPartOf(filler, 370);
	private ZonedDecimalData t5539OldDfact76 = new ZonedDecimalData(5, 0).isAPartOf(filler, 375);
	private ZonedDecimalData t5539OldDfact77 = new ZonedDecimalData(5, 0).isAPartOf(filler, 380);
	private ZonedDecimalData t5539OldDfact78 = new ZonedDecimalData(5, 0).isAPartOf(filler, 385);
	private ZonedDecimalData t5539OldDfact79 = new ZonedDecimalData(5, 0).isAPartOf(filler, 390);
	private ZonedDecimalData t5539OldDfact80 = new ZonedDecimalData(5, 0).isAPartOf(filler, 395);
	private ZonedDecimalData t5539OldDfact81 = new ZonedDecimalData(5, 0).isAPartOf(filler, 400);
	private ZonedDecimalData t5539OldDfact82 = new ZonedDecimalData(5, 0).isAPartOf(filler, 405);
	private ZonedDecimalData t5539OldDfact83 = new ZonedDecimalData(5, 0).isAPartOf(filler, 410);
	private ZonedDecimalData t5539OldDfact84 = new ZonedDecimalData(5, 0).isAPartOf(filler, 415);
	private ZonedDecimalData t5539OldDfact85 = new ZonedDecimalData(5, 0).isAPartOf(filler, 420);
	private ZonedDecimalData t5539OldDfact86 = new ZonedDecimalData(5, 0).isAPartOf(filler, 425);
	private ZonedDecimalData t5539OldDfact87 = new ZonedDecimalData(5, 0).isAPartOf(filler, 430);
	private ZonedDecimalData t5539OldDfact88 = new ZonedDecimalData(5, 0).isAPartOf(filler, 435);
	private ZonedDecimalData t5539OldDfact89 = new ZonedDecimalData(5, 0).isAPartOf(filler, 440);
	private ZonedDecimalData t5539OldDfact90 = new ZonedDecimalData(5, 0).isAPartOf(filler, 445);
	private ZonedDecimalData t5539OldDfact91 = new ZonedDecimalData(5, 0).isAPartOf(filler, 450);
	private ZonedDecimalData t5539OldDfact92 = new ZonedDecimalData(5, 0).isAPartOf(filler, 455);
	private ZonedDecimalData t5539OldDfact93 = new ZonedDecimalData(5, 0).isAPartOf(filler, 460);
	private ZonedDecimalData t5539OldDfact94 = new ZonedDecimalData(5, 0).isAPartOf(filler, 465);
	private ZonedDecimalData t5539OldDfact95 = new ZonedDecimalData(5, 0).isAPartOf(filler, 470);
	private ZonedDecimalData t5539OldDfact96 = new ZonedDecimalData(5, 0).isAPartOf(filler, 475);
	private ZonedDecimalData t5539OldDfact97 = new ZonedDecimalData(5, 0).isAPartOf(filler, 480);
	private ZonedDecimalData t5539OldDfact98 = new ZonedDecimalData(5, 0).isAPartOf(filler, 485);
	private ZonedDecimalData t5539OldDfact99 = new ZonedDecimalData(5, 0).isAPartOf(filler, 490);
}
}
