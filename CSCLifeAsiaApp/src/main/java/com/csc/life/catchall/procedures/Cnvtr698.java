/*
 * File: Cnvtr698.java
 * Date: December 3, 2013 2:17:50 AM ICT
 * Author: CSC
 * 
 * Class transformed from CNVTR698.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.catchall.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.tablestructures.Tr698rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table TR698, Replace INSTPR with INSTPR 1,
*   add new fields INSTPR02 to INSTPR11.
*
*
****************************************************************** ****
* </pre>
*/
public class Cnvtr698 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVTR698");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String tr698 = "TR698";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Tr698rec tr698rec = new Tr698rec();
	private Varcom varcom = new Varcom();
	private Tr698OldRecInner tr698OldRecInner = new Tr698OldRecInner();

	public Cnvtr698() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(tr698);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT TR698 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(), tr698)) {
			getAppVars().addDiagnostic("Table TR698 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itemIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT TR698 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteTr6982080();
	}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		tr698OldRecInner.tr698OldRec.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		tr698rec.disccntmeth.set(tr698OldRecInner.tr698OldDisccntmeth);
		tr698rec.insprms.set(tr698OldRecInner.tr698OldInsprms);
		tr698rec.mfacthm.set(tr698OldRecInner.tr698OldMfacthm);
		tr698rec.mfacthy.set(tr698OldRecInner.tr698OldMfacthy);
		tr698rec.mfactm.set(tr698OldRecInner.tr698OldMfactm);
		tr698rec.mfactq.set(tr698OldRecInner.tr698OldMfactq);
		tr698rec.mfactw.set(tr698OldRecInner.tr698OldMfactw);
		tr698rec.mfact2w.set(tr698OldRecInner.tr698OldMfact2w);
		tr698rec.mfact4w.set(tr698OldRecInner.tr698OldMfact4w);
		tr698rec.mfacty.set(tr698OldRecInner.tr698OldMfacty);
		tr698rec.premUnit.set(tr698OldRecInner.tr698OldPremUnit);
		tr698rec.unit.set(tr698OldRecInner.tr698OldUnit);
		tr698rec.insprem.set(tr698OldRecInner.tr698OldInsprem);
		tr698rec.instpr01.set(tr698OldRecInner.tr698OldInstpr);
		for (ix.set(2); !(isGT(ix, 11)); ix.add(1)){
			tr698rec.instpr[ix.toInt()].set(ZERO);
		}
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(tr698rec.tr698Rec);
	}

protected void rewriteTr6982080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(), tr698)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for TR698");
		}
	}
/*
 * Class transformed  from Data Structure TR698-OLD-REC--INNER
 */
private static final class Tr698OldRecInner { 

	private FixedLengthStringData tr698OldRec = new FixedLengthStringData(439);
	private FixedLengthStringData tr698OldDisccntmeth = new FixedLengthStringData(4).isAPartOf(tr698OldRec, 0);
	private FixedLengthStringData tr698OldInsprms = new FixedLengthStringData(396).isAPartOf(tr698OldRec, 4);
	private PackedDecimalData[] tr698OldInsprm = PDArrayPartOfStructure(99, 6, 0, tr698OldInsprms, 0);
	private PackedDecimalData tr698OldInstpr = new PackedDecimalData(6, 0).isAPartOf(tr698OldRec, 400);
	private PackedDecimalData tr698OldMfacthm = new PackedDecimalData(5, 4).isAPartOf(tr698OldRec, 404);
	private PackedDecimalData tr698OldMfacthy = new PackedDecimalData(5, 4).isAPartOf(tr698OldRec, 407);
	private PackedDecimalData tr698OldMfactm = new PackedDecimalData(5, 4).isAPartOf(tr698OldRec, 410);
	private PackedDecimalData tr698OldMfactq = new PackedDecimalData(5, 4).isAPartOf(tr698OldRec, 413);
	private PackedDecimalData tr698OldMfactw = new PackedDecimalData(5, 4).isAPartOf(tr698OldRec, 416);
	private PackedDecimalData tr698OldMfact2w = new PackedDecimalData(5, 4).isAPartOf(tr698OldRec, 419);
	private PackedDecimalData tr698OldMfact4w = new PackedDecimalData(5, 4).isAPartOf(tr698OldRec, 422);
	private PackedDecimalData tr698OldMfacty = new PackedDecimalData(5, 4).isAPartOf(tr698OldRec, 425);
	private PackedDecimalData tr698OldPremUnit = new PackedDecimalData(3, 0).isAPartOf(tr698OldRec, 428);
	private PackedDecimalData tr698OldUnit = new PackedDecimalData(9, 0).isAPartOf(tr698OldRec, 430);
	private PackedDecimalData tr698OldInsprem = new PackedDecimalData(6, 0).isAPartOf(tr698OldRec, 435);
}
}
