/*
 * File: Cnvth617.java
 * Date: December 3, 2013 2:17:47 AM ICT
 * Author: CSC
 * 
 * Class transformed from CNVTH617.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.catchall.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.tablestructures.Th617rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table TH617, Replace INSTPR with INSTPR 1,
*   add new fields INSTPR02 to INSTPR11.
*
*
****************************************************************** ****
* </pre>
*/
public class Cnvth617 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVTH617");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String th617 = "TH617";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData th617OldRec = new FixedLengthStringData(424);
	private FixedLengthStringData th617OldDisccntmeth = new FixedLengthStringData(4).isAPartOf(th617OldRec, 0);
	private FixedLengthStringData th617OldInsprms = new FixedLengthStringData(396).isAPartOf(th617OldRec, 4);
	private PackedDecimalData[] th617OldInsprm = PDArrayPartOfStructure(99, 6, 0, th617OldInsprms, 0);
	private ZonedDecimalData th617OldInstpr = new ZonedDecimalData(6, 0).isAPartOf(th617OldRec, 400);
	private ZonedDecimalData th617OldPremUnit = new ZonedDecimalData(3, 0).isAPartOf(th617OldRec, 406);
	private ZonedDecimalData th617OldUnit = new ZonedDecimalData(9, 0).isAPartOf(th617OldRec, 409);
	private ZonedDecimalData th617OldInsprem = new ZonedDecimalData(6, 0).isAPartOf(th617OldRec, 418);
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Th617rec th617rec = new Th617rec();
	private Varcom varcom = new Varcom();

	public Cnvth617() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(th617);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT TH617 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(), th617)) {
			getAppVars().addDiagnostic("Table TH617 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itemIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT TH617 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteTh6172080();
	}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		th617OldRec.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		th617rec.disccntmeth.set(th617OldDisccntmeth);
		th617rec.premUnit.set(th617OldPremUnit);
		th617rec.unit.set(th617OldUnit);
		th617rec.insprem.set(th617OldInsprem);
		th617rec.instpr01.set(th617OldInstpr);
		for (ix.set(1); !(isGT(ix, 99)); ix.add(1)){
			th617rec.insprm[ix.toInt()].set(th617OldInsprm[ix.toInt()]);
		}
		for (ix.set(2); !(isGT(ix, 11)); ix.add(1)){
			th617rec.instpr[ix.toInt()].set(ZERO);
		}
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(th617rec.th617Rec);
	}

protected void rewriteTh6172080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(), th617)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for TH617");
		}
	}
}
