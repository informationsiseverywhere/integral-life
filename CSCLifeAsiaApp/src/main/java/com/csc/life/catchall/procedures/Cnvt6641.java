/*
 * File: Cnvt6641.java
 * Date: December 3, 2013 2:17:21 AM ICT
 * Author: CSC
 * 
 * Class transformed from CNVT6641.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.catchall.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.underwriting.tablestructures.T6641rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table T6641, add new fields NXDP16,
*   TNXMON01 to TNXMON11.
*
*
****************************************************************** ****
* </pre>
*/
public class Cnvt6641 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVT6641");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String t6641 = "T6641";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private PackedDecimalData ix = new PackedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T6641rec t6641rec = new T6641rec();
	private Varcom varcom = new Varcom();
	private T6641OldRecInner t6641OldRecInner = new T6641OldRecInner();

	public Cnvt6641() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(t6641);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT T6641 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(), t6641)) {
			getAppVars().addDiagnostic("Table T6641 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itemIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT T6641 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteT66412080();
	}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		t6641OldRecInner.t6641OldRec.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		t6641rec.nxmoney.set(t6641OldRecInner.t6641OldNxmoney);
		for (ix.set(1); !(isGT(ix, 15)); ix.add(1)){
			t6641rec.nxdp[ix.toInt()].set(t6641OldRecInner.t6641OldNxdp[ix.toInt()]);
		}
		t6641rec.nxdp16.set(ZERO);
		for (ix.set(1); !(isGT(ix, 99)); ix.add(1)){
			t6641rec.nxmon[ix.toInt()].set(t6641OldRecInner.t6641OldNxmon[ix.toInt()]);
		}
		for (ix.set(1); !(isGT(ix, 11)); ix.add(1)){
			t6641rec.tnxmon[ix.toInt()].set(ZERO);
		}
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(t6641rec.t6641Rec);
	}

protected void rewriteT66412080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(), t6641)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for T6641");
		}
	}
/*
 * Class transformed  from Data Structure T6641-OLD-REC--INNER
 */
private static final class T6641OldRecInner { 

	private FixedLengthStringData t6641OldRec = new FixedLengthStringData(430);
	private FixedLengthStringData t6641OldNxdps = new FixedLengthStringData(30).isAPartOf(t6641OldRec, 0);
	private BinaryData[] t6641OldNxdp = BDArrayPartOfStructure(15, 1, 0, t6641OldNxdps, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(t6641OldNxdps, 0, FILLER_REDEFINE);
	private BinaryData t6641OldNxdp01 = new BinaryData(1, 0).isAPartOf(filler, 0);
	private BinaryData t6641OldNxdp02 = new BinaryData(1, 0).isAPartOf(filler, 2);
	private BinaryData t6641OldNxdp03 = new BinaryData(1, 0).isAPartOf(filler, 4);
	private BinaryData t6641OldNxdp04 = new BinaryData(1, 0).isAPartOf(filler, 6);
	private BinaryData t6641OldNxdp05 = new BinaryData(1, 0).isAPartOf(filler, 8);
	private BinaryData t6641OldNxdp06 = new BinaryData(1, 0).isAPartOf(filler, 10);
	private BinaryData t6641OldNxdp07 = new BinaryData(1, 0).isAPartOf(filler, 12);
	private BinaryData t6641OldNxdp08 = new BinaryData(1, 0).isAPartOf(filler, 14);
	private BinaryData t6641OldNxdp09 = new BinaryData(1, 0).isAPartOf(filler, 16);
	private BinaryData t6641OldNxdp10 = new BinaryData(1, 0).isAPartOf(filler, 18);
	private BinaryData t6641OldNxdp11 = new BinaryData(1, 0).isAPartOf(filler, 20);
	private BinaryData t6641OldNxdp12 = new BinaryData(1, 0).isAPartOf(filler, 22);
	private BinaryData t6641OldNxdp13 = new BinaryData(1, 0).isAPartOf(filler, 24);
	private BinaryData t6641OldNxdp14 = new BinaryData(1, 0).isAPartOf(filler, 26);
	private BinaryData t6641OldNxdp15 = new BinaryData(1, 0).isAPartOf(filler, 28);
	private BinaryData t6641OldNxmoney = new BinaryData(8, 0).isAPartOf(t6641OldRec, 30);
	private FixedLengthStringData t6641OldNxmons = new FixedLengthStringData(396).isAPartOf(t6641OldRec, 34);
	private BinaryData[] t6641OldNxmon = BDArrayPartOfStructure(99, 8, 0, t6641OldNxmons, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(396).isAPartOf(t6641OldNxmons, 0, FILLER_REDEFINE);
	private BinaryData t6641OldNxmon01 = new BinaryData(8, 0).isAPartOf(filler1, 0);
	private BinaryData t6641OldNxmon02 = new BinaryData(8, 0).isAPartOf(filler1, 4);
	private BinaryData t6641OldNxmon03 = new BinaryData(8, 0).isAPartOf(filler1, 8);
	private BinaryData t6641OldNxmon04 = new BinaryData(8, 0).isAPartOf(filler1, 12);
	private BinaryData t6641OldNxmon05 = new BinaryData(8, 0).isAPartOf(filler1, 16);
	private BinaryData t6641OldNxmon06 = new BinaryData(8, 0).isAPartOf(filler1, 20);
	private BinaryData t6641OldNxmon07 = new BinaryData(8, 0).isAPartOf(filler1, 24);
	private BinaryData t6641OldNxmon08 = new BinaryData(8, 0).isAPartOf(filler1, 28);
	private BinaryData t6641OldNxmon09 = new BinaryData(8, 0).isAPartOf(filler1, 32);
	private BinaryData t6641OldNxmon10 = new BinaryData(8, 0).isAPartOf(filler1, 36);
	private BinaryData t6641OldNxmon11 = new BinaryData(8, 0).isAPartOf(filler1, 40);
	private BinaryData t6641OldNxmon12 = new BinaryData(8, 0).isAPartOf(filler1, 44);
	private BinaryData t6641OldNxmon13 = new BinaryData(8, 0).isAPartOf(filler1, 48);
	private BinaryData t6641OldNxmon14 = new BinaryData(8, 0).isAPartOf(filler1, 52);
	private BinaryData t6641OldNxmon15 = new BinaryData(8, 0).isAPartOf(filler1, 56);
	private BinaryData t6641OldNxmon16 = new BinaryData(8, 0).isAPartOf(filler1, 60);
	private BinaryData t6641OldNxmon17 = new BinaryData(8, 0).isAPartOf(filler1, 64);
	private BinaryData t6641OldNxmon18 = new BinaryData(8, 0).isAPartOf(filler1, 68);
	private BinaryData t6641OldNxmon19 = new BinaryData(8, 0).isAPartOf(filler1, 72);
	private BinaryData t6641OldNxmon20 = new BinaryData(8, 0).isAPartOf(filler1, 76);
	private BinaryData t6641OldNxmon21 = new BinaryData(8, 0).isAPartOf(filler1, 80);
	private BinaryData t6641OldNxmon22 = new BinaryData(8, 0).isAPartOf(filler1, 84);
	private BinaryData t6641OldNxmon23 = new BinaryData(8, 0).isAPartOf(filler1, 88);
	private BinaryData t6641OldNxmon24 = new BinaryData(8, 0).isAPartOf(filler1, 92);
	private BinaryData t6641OldNxmon25 = new BinaryData(8, 0).isAPartOf(filler1, 96);
	private BinaryData t6641OldNxmon26 = new BinaryData(8, 0).isAPartOf(filler1, 100);
	private BinaryData t6641OldNxmon27 = new BinaryData(8, 0).isAPartOf(filler1, 104);
	private BinaryData t6641OldNxmon28 = new BinaryData(8, 0).isAPartOf(filler1, 108);
	private BinaryData t6641OldNxmon29 = new BinaryData(8, 0).isAPartOf(filler1, 112);
	private BinaryData t6641OldNxmon30 = new BinaryData(8, 0).isAPartOf(filler1, 116);
	private BinaryData t6641OldNxmon31 = new BinaryData(8, 0).isAPartOf(filler1, 120);
	private BinaryData t6641OldNxmon32 = new BinaryData(8, 0).isAPartOf(filler1, 124);
	private BinaryData t6641OldNxmon33 = new BinaryData(8, 0).isAPartOf(filler1, 128);
	private BinaryData t6641OldNxmon34 = new BinaryData(8, 0).isAPartOf(filler1, 132);
	private BinaryData t6641OldNxmon35 = new BinaryData(8, 0).isAPartOf(filler1, 136);
	private BinaryData t6641OldNxmon36 = new BinaryData(8, 0).isAPartOf(filler1, 140);
	private BinaryData t6641OldNxmon37 = new BinaryData(8, 0).isAPartOf(filler1, 144);
	private BinaryData t6641OldNxmon38 = new BinaryData(8, 0).isAPartOf(filler1, 148);
	private BinaryData t6641OldNxmon39 = new BinaryData(8, 0).isAPartOf(filler1, 152);
	private BinaryData t6641OldNxmon40 = new BinaryData(8, 0).isAPartOf(filler1, 156);
	private BinaryData t6641OldNxmon41 = new BinaryData(8, 0).isAPartOf(filler1, 160);
	private BinaryData t6641OldNxmon42 = new BinaryData(8, 0).isAPartOf(filler1, 164);
	private BinaryData t6641OldNxmon43 = new BinaryData(8, 0).isAPartOf(filler1, 168);
	private BinaryData t6641OldNxmon44 = new BinaryData(8, 0).isAPartOf(filler1, 172);
	private BinaryData t6641OldNxmon45 = new BinaryData(8, 0).isAPartOf(filler1, 176);
	private BinaryData t6641OldNxmon46 = new BinaryData(8, 0).isAPartOf(filler1, 180);
	private BinaryData t6641OldNxmon47 = new BinaryData(8, 0).isAPartOf(filler1, 184);
	private BinaryData t6641OldNxmon48 = new BinaryData(8, 0).isAPartOf(filler1, 188);
	private BinaryData t6641OldNxmon49 = new BinaryData(8, 0).isAPartOf(filler1, 192);
	private BinaryData t6641OldNxmon50 = new BinaryData(8, 0).isAPartOf(filler1, 196);
	private BinaryData t6641OldNxmon51 = new BinaryData(8, 0).isAPartOf(filler1, 200);
	private BinaryData t6641OldNxmon52 = new BinaryData(8, 0).isAPartOf(filler1, 204);
	private BinaryData t6641OldNxmon53 = new BinaryData(8, 0).isAPartOf(filler1, 208);
	private BinaryData t6641OldNxmon54 = new BinaryData(8, 0).isAPartOf(filler1, 212);
	private BinaryData t6641OldNxmon55 = new BinaryData(8, 0).isAPartOf(filler1, 216);
	private BinaryData t6641OldNxmon56 = new BinaryData(8, 0).isAPartOf(filler1, 220);
	private BinaryData t6641OldNxmon57 = new BinaryData(8, 0).isAPartOf(filler1, 224);
	private BinaryData t6641OldNxmon58 = new BinaryData(8, 0).isAPartOf(filler1, 228);
	private BinaryData t6641OldNxmon59 = new BinaryData(8, 0).isAPartOf(filler1, 232);
	private BinaryData t6641OldNxmon60 = new BinaryData(8, 0).isAPartOf(filler1, 236);
	private BinaryData t6641OldNxmon61 = new BinaryData(8, 0).isAPartOf(filler1, 240);
	private BinaryData t6641OldNxmon62 = new BinaryData(8, 0).isAPartOf(filler1, 244);
	private BinaryData t6641OldNxmon63 = new BinaryData(8, 0).isAPartOf(filler1, 248);
	private BinaryData t6641OldNxmon64 = new BinaryData(8, 0).isAPartOf(filler1, 252);
	private BinaryData t6641OldNxmon65 = new BinaryData(8, 0).isAPartOf(filler1, 256);
	private BinaryData t6641OldNxmon66 = new BinaryData(8, 0).isAPartOf(filler1, 260);
	private BinaryData t6641OldNxmon67 = new BinaryData(8, 0).isAPartOf(filler1, 264);
	private BinaryData t6641OldNxmon68 = new BinaryData(8, 0).isAPartOf(filler1, 268);
	private BinaryData t6641OldNxmon69 = new BinaryData(8, 0).isAPartOf(filler1, 272);
	private BinaryData t6641OldNxmon70 = new BinaryData(8, 0).isAPartOf(filler1, 276);
	private BinaryData t6641OldNxmon71 = new BinaryData(8, 0).isAPartOf(filler1, 280);
	private BinaryData t6641OldNxmon72 = new BinaryData(8, 0).isAPartOf(filler1, 284);
	private BinaryData t6641OldNxmon73 = new BinaryData(8, 0).isAPartOf(filler1, 288);
	private BinaryData t6641OldNxmon74 = new BinaryData(8, 0).isAPartOf(filler1, 292);
	private BinaryData t6641OldNxmon75 = new BinaryData(8, 0).isAPartOf(filler1, 296);
	private BinaryData t6641OldNxmon76 = new BinaryData(8, 0).isAPartOf(filler1, 300);
	private BinaryData t6641OldNxmon77 = new BinaryData(8, 0).isAPartOf(filler1, 304);
	private BinaryData t6641OldNxmon78 = new BinaryData(8, 0).isAPartOf(filler1, 308);
	private BinaryData t6641OldNxmon79 = new BinaryData(8, 0).isAPartOf(filler1, 312);
	private BinaryData t6641OldNxmon80 = new BinaryData(8, 0).isAPartOf(filler1, 316);
	private BinaryData t6641OldNxmon81 = new BinaryData(8, 0).isAPartOf(filler1, 320);
	private BinaryData t6641OldNxmon82 = new BinaryData(8, 0).isAPartOf(filler1, 324);
	private BinaryData t6641OldNxmon83 = new BinaryData(8, 0).isAPartOf(filler1, 328);
	private BinaryData t6641OldNxmon84 = new BinaryData(8, 0).isAPartOf(filler1, 332);
	private BinaryData t6641OldNxmon85 = new BinaryData(8, 0).isAPartOf(filler1, 336);
	private BinaryData t6641OldNxmon86 = new BinaryData(8, 0).isAPartOf(filler1, 340);
	private BinaryData t6641OldNxmon87 = new BinaryData(8, 0).isAPartOf(filler1, 344);
	private BinaryData t6641OldNxmon88 = new BinaryData(8, 0).isAPartOf(filler1, 348);
	private BinaryData t6641OldNxmon89 = new BinaryData(8, 0).isAPartOf(filler1, 352);
	private BinaryData t6641OldNxmon90 = new BinaryData(8, 0).isAPartOf(filler1, 356);
	private BinaryData t6641OldNxmon91 = new BinaryData(8, 0).isAPartOf(filler1, 360);
	private BinaryData t6641OldNxmon92 = new BinaryData(8, 0).isAPartOf(filler1, 364);
	private BinaryData t6641OldNxmon93 = new BinaryData(8, 0).isAPartOf(filler1, 368);
	private BinaryData t6641OldNxmon94 = new BinaryData(8, 0).isAPartOf(filler1, 372);
	private BinaryData t6641OldNxmon95 = new BinaryData(8, 0).isAPartOf(filler1, 376);
	private BinaryData t6641OldNxmon96 = new BinaryData(8, 0).isAPartOf(filler1, 380);
	private BinaryData t6641OldNxmon97 = new BinaryData(8, 0).isAPartOf(filler1, 384);
	private BinaryData t6641OldNxmon98 = new BinaryData(8, 0).isAPartOf(filler1, 388);
	private BinaryData t6641OldNxmon99 = new BinaryData(8, 0).isAPartOf(filler1, 392);
}
}
