package com.csc.life.catchall.dataaccess.dao;

import java.util.List;

import com.csc.life.catchall.dataaccess.model.Avyxpf;
import com.csc.life.productdefinition.dataaccess.model.Br523DTO;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface AvyxpfDAO extends BaseDAO<Avyxpf> {
	public List<Avyxpf> searchAvyxpfResult(String tableId, String memName, int batchExtractSize, int batchID);
}
