package com.csc.life.catchall.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.catchall.dataaccess.dao.AvyxpfDAO;
import com.csc.life.catchall.dataaccess.model.Avyxpf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.util.StringUtil; //IBPLIFE-2497
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AvyxpfDAOImpl extends BaseDAOImpl<Avyxpf> implements AvyxpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(AvyxpfDAOImpl.class);

	public List<Avyxpf> searchAvyxpfResult(String tableId, String memName,int batchExtractSize, int batchID) {
		StringBuilder sql = new StringBuilder();
		//IJTI-306 START
		sql.append(" SELECT b.* FROM (")
				.append(" select FLOOR((ROW_NUMBER()OVER( ORDER BY aa.UNIQUE_NUMBER ASC)-1)/")
				.append(batchExtractSize)
				.append(") batchnum ,aa.UNIQUE_NUMBER,aa.CHDRCOY,aa.CHDRNUM,aa.LIFE,aa.COVERAGE,aa.RIDER,aa.PLNSFX")
				.append(",aa.CRTABLE,aa.CRRCD,aa.STATCODE,cl.cbcvin,cl.currfrom,cl.instprem,")
				.append("cl.jlife,cl.pcestrm,cl.prmcur,cl.pstatcode,cl.rrtdat,cl.rcestrm,cl.singp,cl.sumins,")
				.append("cl.unique_number as clunique_number,cl.validflag,cl.zlinstprem,cl.rcesdte,cl.cbanpr from ")
				.append(tableId)
				.append(" aa join covrrnl cl on cl.chdrcoy=aa.chdrcoy and cl.chdrnum=aa.chdrnum and cl.life=aa.life ")
				.append("and cl.coverage=aa.coverage and cl.rider=aa.rider and cl.plnsfx = aa.plnsfx")
				.append(" where 1=1 ");
		if (memName != null) {
			sql.append(" and  aa.MEMBER_NAME= ?");
		}
		sql.append(" ) b where b.batchnum= ? ");// + batchID
		List<Avyxpf> avyxList = new ArrayList<Avyxpf>();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			 stmt = getPrepareStatement(sql.toString()); 
			 if (memName != null) {
				 stmt.setString(1, StringUtil.fillSpace(memName, 10));//IBPLIFE-2497
				 stmt.setInt(2, batchID);
			 } else {
				 stmt.setInt(1, batchID);
			 }
			 rs = stmt.executeQuery();
			//IJTI-306 END
			while (rs.next()) {
				Avyxpf avyxpf = new Avyxpf();
				Covrpf covrpf = new Covrpf();
				avyxpf.setChdrcoy(rs.getString(3));
				avyxpf.setChdrnum(rs.getString(4));
				avyxpf.setLife(rs.getString(5));
				avyxpf.setCoverage(rs.getString(6));
				avyxpf.setRider(rs.getString(7));
				avyxpf.setPlnsfx(rs.getInt(8));
				avyxpf.setCrtable(rs.getString(9));
				avyxpf.setCrrcd(rs.getInt(10));
				avyxpf.setStatcode(rs.getString(11));
				
				
				covrpf.setChdrcoy(rs.getString(3));
				covrpf.setChdrnum(rs.getString(4));
				covrpf.setLife(rs.getString(5));
				covrpf.setCoverage(rs.getString(6));
				covrpf.setRider(rs.getString(7));
				covrpf.setPlanSuffix(rs.getInt(8));
				covrpf.setCrtable(rs.getString(9));
				covrpf.setCrrcd(rs.getInt(10));
				covrpf.setStatcode(rs.getString(11));
				covrpf.setConvertInitialUnits(rs.getInt(12));
				covrpf.setCurrfrom(rs.getInt(13));
				covrpf.setInstprem(rs.getBigDecimal(14));
				covrpf.setJlife(rs.getString(15));
				covrpf.setPremCessTerm(rs.getInt(16));
				covrpf.setPremCurrency(rs.getString(17));
				covrpf.setPstatcode(rs.getString(18));
				covrpf.setRerateDate(rs.getInt(19));
				covrpf.setRiskCessTerm(rs.getInt(20));
				covrpf.setSingp(rs.getBigDecimal(21));
				covrpf.setSumins(rs.getBigDecimal(22));
				covrpf.setUniqueNumber(rs.getLong(23));
				covrpf.setValidflag(rs.getString(24));
				covrpf.setZlinstprem(rs.getBigDecimal(25));
				covrpf.setRiskCessDate(rs.getInt(26));
				covrpf.setAnnivProcDate(rs.getInt(27));
				
				avyxpf.setCovrpf(covrpf);
				avyxList.add(avyxpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchAvyxpfResult()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(stmt, rs);
		}
		return avyxList;
	}
}
