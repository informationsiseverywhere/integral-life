/*
 * File: Cnvtt505.java
 * Date: December 3, 2013 2:17:54 AM ICT
 * Author: CSC
 * 
 * Class transformed from CNVTT505.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.catchall.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.tablestructures.Tt505rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table TT505, Replace MODFAC with MODFAC 1,
*   add new fields MODFAC02 to MODFAC11.
*
*
****************************************************************** ****
* </pre>
*/
public class Cnvtt505 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVTT505");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String tt505 = "TT505";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData tt505OldRec = new FixedLengthStringData(313);
	private FixedLengthStringData tt505OldLfacts = new FixedLengthStringData(297).isAPartOf(tt505OldRec, 0);
	private PackedDecimalData[] tt505OldLfact = PDArrayPartOfStructure(99, 5, 4, tt505OldLfacts, 0);
	private PackedDecimalData tt505OldModfac = new PackedDecimalData(5, 4).isAPartOf(tt505OldRec, 297);
	private ZonedDecimalData tt505OldRiskunit = new ZonedDecimalData(6, 0).isAPartOf(tt505OldRec, 300);
	private FixedLengthStringData tt505OldDisccntmeth = new FixedLengthStringData(4).isAPartOf(tt505OldRec, 306);
	private PackedDecimalData tt505OldLfactor = new PackedDecimalData(5, 4).isAPartOf(tt505OldRec, 310);
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Tt505rec tt505rec = new Tt505rec();
	private Varcom varcom = new Varcom();

	public Cnvtt505() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(tt505);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT TT505 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(), tt505)) {
			getAppVars().addDiagnostic("Table TT505 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itemIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT TT505 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteTt5052080();
	}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		tt505OldRec.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		tt505rec.riskunit.set(tt505OldRiskunit);
		tt505rec.disccntmeth.set(tt505OldDisccntmeth);
		tt505rec.lfactor.set(tt505OldLfactor);
		tt505rec.modfac01.set(tt505OldModfac);
		for (ix.set(1); !(isGT(ix, 99)); ix.add(1)){
			tt505rec.lfact[ix.toInt()].set(tt505OldLfact[ix.toInt()]);
		}
		for (ix.set(2); !(isGT(ix, 11)); ix.add(1)){
			tt505rec.modfac[ix.toInt()].set(ZERO);
		}
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(tt505rec.tt505Rec);
	}

protected void rewriteTt5052080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(), tt505)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for TT505");
		}
	}
}
