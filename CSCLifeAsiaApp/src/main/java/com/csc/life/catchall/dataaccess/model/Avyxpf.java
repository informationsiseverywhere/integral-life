package com.csc.life.catchall.dataaccess.model;

import com.csc.life.productdefinition.dataaccess.model.Covrpf;


public class Avyxpf {
	private long uniquenumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int plnsfx;
	private String crtable;
	private int crrcd;
	private String statcode;
	private Covrpf covrpf;
	public long getUniquenumber() {
		return uniquenumber;
	}
	public void setUniquenumber(long uniquenumber) {
		this.uniquenumber = uniquenumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(int plnsfx) {
		this.plnsfx = plnsfx;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public int getCrrcd() {
		return crrcd;
	}
	public void setCrrcd(int crrcd) {
		this.crrcd = crrcd;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public Covrpf getCovrpf() {
		return covrpf;
	}
	public void setCovrpf(Covrpf covrpf) {
		this.covrpf = covrpf;
	}
}
