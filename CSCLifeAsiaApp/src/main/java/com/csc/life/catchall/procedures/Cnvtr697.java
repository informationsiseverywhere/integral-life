/*
 * File: Cnvtr697.java
 * Date: December 3, 2013 2:17:49 AM ICT
 * Author: CSC
 * 
 * Class transformed from CNVTR697.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.catchall.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.tablestructures.Tr697rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table TR697, Replace INSTPR with INSTPR 1,
*   add new fields INSTPR02 to INSTPR11.
*
*
****************************************************************** ****
* </pre>
*/
public class Cnvtr697 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVTR697");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String tr697 = "TR697";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Tr697rec tr697rec = new Tr697rec();
	private Varcom varcom = new Varcom();
	private Tr697OldRecInner tr697OldRecInner = new Tr697OldRecInner();

	public Cnvtr697() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(tr697);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT TR697 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(), tr697)) {
			getAppVars().addDiagnostic("Table TR697 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itemIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT TR697 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteTr6972080();
	}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		tr697OldRecInner.tr697OldRec.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		tr697rec.disccntmeth.set(tr697OldRecInner.tr697OldDisccntmeth);
		tr697rec.insprms.set(tr697OldRecInner.tr697OldInsprms);
		tr697rec.mfacthm.set(tr697OldRecInner.tr697OldMfacthm);
		tr697rec.mfacthy.set(tr697OldRecInner.tr697OldMfacthy);
		tr697rec.mfactm.set(tr697OldRecInner.tr697OldMfactm);
		tr697rec.mfactq.set(tr697OldRecInner.tr697OldMfactq);
		tr697rec.mfactw.set(tr697OldRecInner.tr697OldMfactw);
		tr697rec.mfact2w.set(tr697OldRecInner.tr697OldMfact2w);
		tr697rec.mfact4w.set(tr697OldRecInner.tr697OldMfact4w);
		tr697rec.mfacty.set(tr697OldRecInner.tr697OldMfacty);
		tr697rec.premUnit.set(tr697OldRecInner.tr697OldPremUnit);
		tr697rec.unit.set(tr697OldRecInner.tr697OldUnit);
		tr697rec.insprem.set(tr697OldRecInner.tr697OldInsprem);
		tr697rec.instpr01.set(tr697OldRecInner.tr697OldInstpr);
		for (ix.set(2); !(isGT(ix, 11)); ix.add(1)){
			tr697rec.instpr[ix.toInt()].set(ZERO);
		}
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(tr697rec.tr697Rec);
	}

protected void rewriteTr6972080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(), tr697)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for TR697");
		}
	}
/*
 * Class transformed  from Data Structure TR697-OLD-REC--INNER
 */
private static final class Tr697OldRecInner { 

	private FixedLengthStringData tr697OldRec = new FixedLengthStringData(439);
	private FixedLengthStringData tr697OldDisccntmeth = new FixedLengthStringData(4).isAPartOf(tr697OldRec, 0);
	private FixedLengthStringData tr697OldInsprms = new FixedLengthStringData(396).isAPartOf(tr697OldRec, 4);
	private PackedDecimalData[] tr697OldInsprm = PDArrayPartOfStructure(99, 6, 0, tr697OldInsprms, 0);
	private PackedDecimalData tr697OldInstpr = new PackedDecimalData(6, 0).isAPartOf(tr697OldRec, 400);
	private PackedDecimalData tr697OldMfacthm = new PackedDecimalData(5, 4).isAPartOf(tr697OldRec, 404);
	private PackedDecimalData tr697OldMfacthy = new PackedDecimalData(5, 4).isAPartOf(tr697OldRec, 407);
	private PackedDecimalData tr697OldMfactm = new PackedDecimalData(5, 4).isAPartOf(tr697OldRec, 410);
	private PackedDecimalData tr697OldMfactq = new PackedDecimalData(5, 4).isAPartOf(tr697OldRec, 413);
	private PackedDecimalData tr697OldMfactw = new PackedDecimalData(5, 4).isAPartOf(tr697OldRec, 416);
	private PackedDecimalData tr697OldMfact2w = new PackedDecimalData(5, 4).isAPartOf(tr697OldRec, 419);
	private PackedDecimalData tr697OldMfact4w = new PackedDecimalData(5, 4).isAPartOf(tr697OldRec, 422);
	private PackedDecimalData tr697OldMfacty = new PackedDecimalData(5, 4).isAPartOf(tr697OldRec, 425);
	private PackedDecimalData tr697OldPremUnit = new PackedDecimalData(3, 0).isAPartOf(tr697OldRec, 428);
	private PackedDecimalData tr697OldUnit = new PackedDecimalData(9, 0).isAPartOf(tr697OldRec, 430);
	private PackedDecimalData tr697OldInsprem = new PackedDecimalData(6, 0).isAPartOf(tr697OldRec, 435);
}
}
