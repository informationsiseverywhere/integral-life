/*
 * File: Cnvvflagl.java
 * Date: December 3, 2013 2:18:05 AM ICT
 * Author: CSC
 * 
 * Class transformed from CNVVFLAGL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.catchall.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.agents.dataaccess.AglfpfTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   This program will sequentially read thru AGLFPF - FIFE Agent H ader
*
*   Previously the above mentioned file does not contain the field
* </pre>
*/
public class Cnvvflagl extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private AglfpfTableDAM aglfpfFile = new AglfpfTableDAM();
	private AglfpfTableDAM aglfpfRec = new AglfpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("CNVVFLAGL");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaStatus = "";

	private FixedLengthStringData wsaaCounters = new FixedLengthStringData(16);
		/* WSAA-AGLFPF-CTR */
	private ZonedDecimalData wsaaAglfpfRead = new ZonedDecimalData(8, 0).isAPartOf(wsaaCounters, 0).setUnsigned();
	private ZonedDecimalData wsaaAglfpfUpd = new ZonedDecimalData(8, 0).isAPartOf(wsaaCounters, 8).setUnsigned();
	private FixedLengthStringData cnvvflaglStatuz = new FixedLengthStringData(4);
	private Varcom varcom = new Varcom();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readAglfpf220, 
		eof280
	}

	public Cnvvflagl() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		cnvvflaglStatuz = convertAndSetParam(cnvvflaglStatuz, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		cnvvflaglStatuz.set(varcom.oK);
		wsaaCounters.set(ZERO);
		processAglfpf200();
		displayCounters300();
		/*EXIT*/
		stopRun();
	}

protected void processAglfpf200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					openAglfpf210();
				case readAglfpf220: 
					readAglfpf220();
				case eof280: 
					eof280();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void openAglfpf210()
	{
		wsaaStatus = aglfpfFile.openIO();
		if (isNE(wsaaStatus, SPACES)
		&& isNE(wsaaStatus, "00")) {
			getAppVars().addDiagnostic("OPEN AGLFPF FAILED. STATUS = "+wsaaStatus);
			cnvvflaglStatuz.set(wsaaStatus);
			stopRun();
		}
	}

protected void readAglfpf220()
	{
		wsaaStatus = aglfpfFile.read(aglfpfRec);
		if (aglfpfFile.isAtEnd()) {
			goTo(GotoLabel.eof280);
		}
		wsaaAglfpfRead.add(1);
		if (isEQ(aglfpfRec.validflag, " ")) {
			aglfpfRec.validflag.set(1);
			wsaaStatus = aglfpfFile.rewrite(aglfpfRec);
			wsaaAglfpfUpd.add(1);
			if (isNE(wsaaStatus, SPACES)
			&& isNE(wsaaStatus, "00")) {
				getAppVars().addDiagnostic("REWRITE AGLF FAILED. STATUS = "+wsaaStatus+" DATA = "+aglfpfRec);
				cnvvflaglStatuz.set(wsaaStatus);
				stopRun();
			}
		}
		goTo(GotoLabel.readAglfpf220);
	}

protected void eof280()
	{
		wsaaStatus = aglfpfFile.close();
		/*EXIT*/
	}

protected void displayCounters300()
	{
		/*BEGIN*/
		getAppVars().addDiagnostic("NO. OF AGLFPF READ    : "+wsaaAglfpfRead);
		getAppVars().addDiagnostic("NO. OF AGLFPF UPDATED : "+wsaaAglfpfUpd);
		/*EXIT*/
	}
}
