/*
 * File: Cnvtt507.java
 * Date: December 3, 2013 2:17:57 AM ICT
 * Author: CSC
 * 
 * Class transformed from CNVTT507.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.catchall.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.tablestructures.Tt507rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table TT507, Replace INSTPR with INSTPR 1,
*   add new fields INSTPR02 to INSTPR11.
*
*
****************************************************************** ****
* </pre>
*/
public class Cnvtt507 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVTT507");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String tt507 = "TT507";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData tt507OldRec = new FixedLengthStringData(415);
	private FixedLengthStringData tt507OldDisccntmeth = new FixedLengthStringData(4).isAPartOf(tt507OldRec, 0);
	private FixedLengthStringData tt507OldInsprms = new FixedLengthStringData(396).isAPartOf(tt507OldRec, 4);
	private PackedDecimalData[] tt507OldInsprm = PDArrayPartOfStructure(99, 6, 0, tt507OldInsprms, 0);
	private PackedDecimalData tt507OldInstpr = new PackedDecimalData(6, 0).isAPartOf(tt507OldRec, 400);
	private PackedDecimalData tt507OldPremUnit = new PackedDecimalData(3, 0).isAPartOf(tt507OldRec, 404);
	private PackedDecimalData tt507OldUnit = new PackedDecimalData(9, 0).isAPartOf(tt507OldRec, 406);
	private PackedDecimalData tt507OldInsprem = new PackedDecimalData(6, 0).isAPartOf(tt507OldRec, 411);
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Tt507rec tt507rec = new Tt507rec();
	private Varcom varcom = new Varcom();

	public Cnvtt507() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(tt507);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT TT507 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(), tt507)) {
			getAppVars().addDiagnostic("Table TT507 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itemIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT TT507 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteTt5072080();
	}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		tt507OldRec.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		tt507rec.disccntmeth.set(tt507OldDisccntmeth);
		tt507rec.premUnit.set(tt507OldPremUnit);
		tt507rec.unit.set(tt507OldUnit);
		tt507rec.insprem.set(tt507OldInsprem);
		tt507rec.instpr01.set(tt507OldInstpr);
		for (ix.set(1); !(isGT(ix, 99)); ix.add(1)){
			tt507rec.insprm[ix.toInt()].set(tt507OldInsprm[ix.toInt()]);
		}
		for (ix.set(2); !(isGT(ix, 11)); ix.add(1)){
			tt507rec.instpr[ix.toInt()].set(ZERO);
		}
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(tt507rec.tt507Rec);
	}

protected void rewriteTt5072080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(), tt507)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for TT507");
		}
	}
}
