/*
 * File: Cnvt5664.java
 * Date: December 3, 2013 2:17:18 AM ICT
 * Author: CSC
 * 
 * Class transformed from CNVT5664.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.catchall.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.tablestructures.T5664rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table T5664, Replace INSTPR with INSTPR 1,
*   add new fields INSTPR02 to INSTPR11.
*
*
****************************************************************** ****
* </pre>
*/
public class Cnvt5664 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVT5664");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String t5664 = "T5664";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5664rec t5664rec = new T5664rec();
	private Varcom varcom = new Varcom();
	private T5664OldRecInner t5664OldRecInner = new T5664OldRecInner();

	public Cnvt5664() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(t5664);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT T5664 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(), t5664)) {
			getAppVars().addDiagnostic("Table T5664 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itemIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT T5664 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteT56642080();
	}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		t5664OldRecInner.t5664OldRec.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		t5664rec.disccntmeth.set(t5664OldRecInner.t5664OldDisccntmeth);
		t5664rec.premUnit.set(t5664OldRecInner.t5664OldPremUnit);
		t5664rec.unit.set(t5664OldRecInner.t5664OldUnit);
		t5664rec.insprem.set(t5664OldRecInner.t5664OldInsprem);
		t5664rec.mfacthm.set(t5664OldRecInner.t5664OldMfacthm);
		t5664rec.mfacthy.set(t5664OldRecInner.t5664OldMfacthy);
		t5664rec.mfactm.set(t5664OldRecInner.t5664OldMfactm);
		t5664rec.mfactq.set(t5664OldRecInner.t5664OldMfactq);
		t5664rec.mfactw.set(t5664OldRecInner.t5664OldMfactw);
		t5664rec.mfact2w.set(t5664OldRecInner.t5664OldMfact2w);
		t5664rec.mfact4w.set(t5664OldRecInner.t5664OldMfact4w);
		t5664rec.mfacty.set(t5664OldRecInner.t5664OldMfacty);
		t5664rec.instpr01.set(t5664OldRecInner.t5664OldInstpr);
		for (ix.set(1); !(isGT(ix, 99)); ix.add(1)){
			t5664rec.insprm[ix.toInt()].set(t5664OldRecInner.t5664OldInsprm[ix.toInt()]);
		}
		for (ix.set(2); !(isGT(ix, 11)); ix.add(1)){
			t5664rec.instpr[ix.toInt()].set(ZERO);
		}
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(t5664rec.t5664Rec);
	}

protected void rewriteT56642080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(), t5664)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for T5664");
		}
	}
/*
 * Class transformed  from Data Structure T5664-OLD-REC--INNER
 */
private static final class T5664OldRecInner { 

	private FixedLengthStringData t5664OldRec = new FixedLengthStringData(439);
	private FixedLengthStringData t5664OldDisccntmeth = new FixedLengthStringData(4).isAPartOf(t5664OldRec, 0);
	private FixedLengthStringData t5664OldInsprms = new FixedLengthStringData(396).isAPartOf(t5664OldRec, 4);
	private PackedDecimalData[] t5664OldInsprm = PDArrayPartOfStructure(99, 6, 0, t5664OldInsprms, 0);
	private PackedDecimalData t5664OldInstpr = new PackedDecimalData(6, 0).isAPartOf(t5664OldRec, 400);
	private PackedDecimalData t5664OldMfacthm = new PackedDecimalData(5, 4).isAPartOf(t5664OldRec, 404);
	private PackedDecimalData t5664OldMfacthy = new PackedDecimalData(5, 4).isAPartOf(t5664OldRec, 407);
	private PackedDecimalData t5664OldMfactm = new PackedDecimalData(5, 4).isAPartOf(t5664OldRec, 410);
	private PackedDecimalData t5664OldMfactq = new PackedDecimalData(5, 4).isAPartOf(t5664OldRec, 413);
	private PackedDecimalData t5664OldMfactw = new PackedDecimalData(5, 4).isAPartOf(t5664OldRec, 416);
	private PackedDecimalData t5664OldMfact2w = new PackedDecimalData(5, 4).isAPartOf(t5664OldRec, 419);
	private PackedDecimalData t5664OldMfact4w = new PackedDecimalData(5, 4).isAPartOf(t5664OldRec, 422);
	private PackedDecimalData t5664OldMfacty = new PackedDecimalData(5, 4).isAPartOf(t5664OldRec, 425);
	private PackedDecimalData t5664OldPremUnit = new PackedDecimalData(3, 0).isAPartOf(t5664OldRec, 428);
	private PackedDecimalData t5664OldUnit = new PackedDecimalData(9, 0).isAPartOf(t5664OldRec, 430);
	private PackedDecimalData t5664OldInsprem = new PackedDecimalData(6, 0).isAPartOf(t5664OldRec, 435);
}
}
