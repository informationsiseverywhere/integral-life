/*
 * File: Cnvtt562.java
 * Date: December 3, 2013 2:18:01 AM ICT
 * Author: CSC
 * 
 * Class transformed from CNVTT562.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.catchall.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.underwriting.tablestructures.Tt562rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table TT562, add new fields NXDP16,
*   TNXMON04 to TNXMON11.
*
*
****************************************************************** ****
* </pre>
*/
public class Cnvtt562 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVTT562");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String tt562 = "TT562";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private PackedDecimalData ix = new PackedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Tt562rec tt562rec = new Tt562rec();
	private Varcom varcom = new Varcom();
	private Tt562OldRecInner tt562OldRecInner = new Tt562OldRecInner();

	public Cnvtt562() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(tt562);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT TT562 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(), tt562)) {
			getAppVars().addDiagnostic("Table TT562 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itemIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT TT562 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteTt5622080();
	}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		tt562OldRecInner.tt562OldRec.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		tt562rec.nxmoney.set(tt562OldRecInner.tt562OldNxmoney);
		tt562rec.tnxmon01.set(tt562OldRecInner.tt562OldTnxmon01);
		tt562rec.tnxmon02.set(tt562OldRecInner.tt562OldTnxmon02);
		tt562rec.tnxmon03.set(tt562OldRecInner.tt562OldTnxmon03);
		for (ix.set(1); !(isGT(ix, 15)); ix.add(1)){
			tt562rec.nxdp[ix.toInt()].set(tt562OldRecInner.tt562OldNxdp[ix.toInt()]);
		}
		tt562rec.nxdp16.set(ZERO);
		for (ix.set(1); !(isGT(ix, 99)); ix.add(1)){
			tt562rec.nxmon[ix.toInt()].set(tt562OldRecInner.tt562OldNxmon[ix.toInt()]);
		}
		for (ix.set(4); !(isGT(ix, 11)); ix.add(1)){
			tt562rec.tnxmon[ix.toInt()].set(ZERO);
		}
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(tt562rec.tt562Rec);
	}

protected void rewriteTt5622080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(), tt562)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for TT562");
		}
	}
/*
 * Class transformed  from Data Structure TT562-OLD-REC--INNER
 */
private static final class Tt562OldRecInner { 

	private FixedLengthStringData tt562OldRec = new FixedLengthStringData(442);
	private FixedLengthStringData tt562OldNxdps = new FixedLengthStringData(30).isAPartOf(tt562OldRec, 0);
	private BinaryData[] tt562OldNxdp = BDArrayPartOfStructure(15, 1, 0, tt562OldNxdps, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(tt562OldNxdps, 0, FILLER_REDEFINE);
	private BinaryData tt562OldNxdp01 = new BinaryData(1, 0).isAPartOf(filler, 0);
	private BinaryData tt562OldNxdp02 = new BinaryData(1, 0).isAPartOf(filler, 2);
	private BinaryData tt562OldNxdp03 = new BinaryData(1, 0).isAPartOf(filler, 4);
	private BinaryData tt562OldNxdp04 = new BinaryData(1, 0).isAPartOf(filler, 6);
	private BinaryData tt562OldNxdp05 = new BinaryData(1, 0).isAPartOf(filler, 8);
	private BinaryData tt562OldNxdp06 = new BinaryData(1, 0).isAPartOf(filler, 10);
	private BinaryData tt562OldNxdp07 = new BinaryData(1, 0).isAPartOf(filler, 12);
	private BinaryData tt562OldNxdp08 = new BinaryData(1, 0).isAPartOf(filler, 14);
	private BinaryData tt562OldNxdp09 = new BinaryData(1, 0).isAPartOf(filler, 16);
	private BinaryData tt562OldNxdp10 = new BinaryData(1, 0).isAPartOf(filler, 18);
	private BinaryData tt562OldNxdp11 = new BinaryData(1, 0).isAPartOf(filler, 20);
	private BinaryData tt562OldNxdp12 = new BinaryData(1, 0).isAPartOf(filler, 22);
	private BinaryData tt562OldNxdp13 = new BinaryData(1, 0).isAPartOf(filler, 24);
	private BinaryData tt562OldNxdp14 = new BinaryData(1, 0).isAPartOf(filler, 26);
	private BinaryData tt562OldNxdp15 = new BinaryData(1, 0).isAPartOf(filler, 28);
	private BinaryData tt562OldNxmoney = new BinaryData(8, 0).isAPartOf(tt562OldRec, 30);
	private FixedLengthStringData tt562OldNxmons = new FixedLengthStringData(396).isAPartOf(tt562OldRec, 34);
	private BinaryData[] tt562OldNxmon = BDArrayPartOfStructure(99, 8, 0, tt562OldNxmons, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(396).isAPartOf(tt562OldNxmons, 0, FILLER_REDEFINE);
	private BinaryData tt562OldNxmon01 = new BinaryData(8, 0).isAPartOf(filler1, 0);
	private BinaryData tt562OldNxmon02 = new BinaryData(8, 0).isAPartOf(filler1, 4);
	private BinaryData tt562OldNxmon03 = new BinaryData(8, 0).isAPartOf(filler1, 8);
	private BinaryData tt562OldNxmon04 = new BinaryData(8, 0).isAPartOf(filler1, 12);
	private BinaryData tt562OldNxmon05 = new BinaryData(8, 0).isAPartOf(filler1, 16);
	private BinaryData tt562OldNxmon06 = new BinaryData(8, 0).isAPartOf(filler1, 20);
	private BinaryData tt562OldNxmon07 = new BinaryData(8, 0).isAPartOf(filler1, 24);
	private BinaryData tt562OldNxmon08 = new BinaryData(8, 0).isAPartOf(filler1, 28);
	private BinaryData tt562OldNxmon09 = new BinaryData(8, 0).isAPartOf(filler1, 32);
	private BinaryData tt562OldNxmon10 = new BinaryData(8, 0).isAPartOf(filler1, 36);
	private BinaryData tt562OldNxmon11 = new BinaryData(8, 0).isAPartOf(filler1, 40);
	private BinaryData tt562OldNxmon12 = new BinaryData(8, 0).isAPartOf(filler1, 44);
	private BinaryData tt562OldNxmon13 = new BinaryData(8, 0).isAPartOf(filler1, 48);
	private BinaryData tt562OldNxmon14 = new BinaryData(8, 0).isAPartOf(filler1, 52);
	private BinaryData tt562OldNxmon15 = new BinaryData(8, 0).isAPartOf(filler1, 56);
	private BinaryData tt562OldNxmon16 = new BinaryData(8, 0).isAPartOf(filler1, 60);
	private BinaryData tt562OldNxmon17 = new BinaryData(8, 0).isAPartOf(filler1, 64);
	private BinaryData tt562OldNxmon18 = new BinaryData(8, 0).isAPartOf(filler1, 68);
	private BinaryData tt562OldNxmon19 = new BinaryData(8, 0).isAPartOf(filler1, 72);
	private BinaryData tt562OldNxmon20 = new BinaryData(8, 0).isAPartOf(filler1, 76);
	private BinaryData tt562OldNxmon21 = new BinaryData(8, 0).isAPartOf(filler1, 80);
	private BinaryData tt562OldNxmon22 = new BinaryData(8, 0).isAPartOf(filler1, 84);
	private BinaryData tt562OldNxmon23 = new BinaryData(8, 0).isAPartOf(filler1, 88);
	private BinaryData tt562OldNxmon24 = new BinaryData(8, 0).isAPartOf(filler1, 92);
	private BinaryData tt562OldNxmon25 = new BinaryData(8, 0).isAPartOf(filler1, 96);
	private BinaryData tt562OldNxmon26 = new BinaryData(8, 0).isAPartOf(filler1, 100);
	private BinaryData tt562OldNxmon27 = new BinaryData(8, 0).isAPartOf(filler1, 104);
	private BinaryData tt562OldNxmon28 = new BinaryData(8, 0).isAPartOf(filler1, 108);
	private BinaryData tt562OldNxmon29 = new BinaryData(8, 0).isAPartOf(filler1, 112);
	private BinaryData tt562OldNxmon30 = new BinaryData(8, 0).isAPartOf(filler1, 116);
	private BinaryData tt562OldNxmon31 = new BinaryData(8, 0).isAPartOf(filler1, 120);
	private BinaryData tt562OldNxmon32 = new BinaryData(8, 0).isAPartOf(filler1, 124);
	private BinaryData tt562OldNxmon33 = new BinaryData(8, 0).isAPartOf(filler1, 128);
	private BinaryData tt562OldNxmon34 = new BinaryData(8, 0).isAPartOf(filler1, 132);
	private BinaryData tt562OldNxmon35 = new BinaryData(8, 0).isAPartOf(filler1, 136);
	private BinaryData tt562OldNxmon36 = new BinaryData(8, 0).isAPartOf(filler1, 140);
	private BinaryData tt562OldNxmon37 = new BinaryData(8, 0).isAPartOf(filler1, 144);
	private BinaryData tt562OldNxmon38 = new BinaryData(8, 0).isAPartOf(filler1, 148);
	private BinaryData tt562OldNxmon39 = new BinaryData(8, 0).isAPartOf(filler1, 152);
	private BinaryData tt562OldNxmon40 = new BinaryData(8, 0).isAPartOf(filler1, 156);
	private BinaryData tt562OldNxmon41 = new BinaryData(8, 0).isAPartOf(filler1, 160);
	private BinaryData tt562OldNxmon42 = new BinaryData(8, 0).isAPartOf(filler1, 164);
	private BinaryData tt562OldNxmon43 = new BinaryData(8, 0).isAPartOf(filler1, 168);
	private BinaryData tt562OldNxmon44 = new BinaryData(8, 0).isAPartOf(filler1, 172);
	private BinaryData tt562OldNxmon45 = new BinaryData(8, 0).isAPartOf(filler1, 176);
	private BinaryData tt562OldNxmon46 = new BinaryData(8, 0).isAPartOf(filler1, 180);
	private BinaryData tt562OldNxmon47 = new BinaryData(8, 0).isAPartOf(filler1, 184);
	private BinaryData tt562OldNxmon48 = new BinaryData(8, 0).isAPartOf(filler1, 188);
	private BinaryData tt562OldNxmon49 = new BinaryData(8, 0).isAPartOf(filler1, 192);
	private BinaryData tt562OldNxmon50 = new BinaryData(8, 0).isAPartOf(filler1, 196);
	private BinaryData tt562OldNxmon51 = new BinaryData(8, 0).isAPartOf(filler1, 200);
	private BinaryData tt562OldNxmon52 = new BinaryData(8, 0).isAPartOf(filler1, 204);
	private BinaryData tt562OldNxmon53 = new BinaryData(8, 0).isAPartOf(filler1, 208);
	private BinaryData tt562OldNxmon54 = new BinaryData(8, 0).isAPartOf(filler1, 212);
	private BinaryData tt562OldNxmon55 = new BinaryData(8, 0).isAPartOf(filler1, 216);
	private BinaryData tt562OldNxmon56 = new BinaryData(8, 0).isAPartOf(filler1, 220);
	private BinaryData tt562OldNxmon57 = new BinaryData(8, 0).isAPartOf(filler1, 224);
	private BinaryData tt562OldNxmon58 = new BinaryData(8, 0).isAPartOf(filler1, 228);
	private BinaryData tt562OldNxmon59 = new BinaryData(8, 0).isAPartOf(filler1, 232);
	private BinaryData tt562OldNxmon60 = new BinaryData(8, 0).isAPartOf(filler1, 236);
	private BinaryData tt562OldNxmon61 = new BinaryData(8, 0).isAPartOf(filler1, 240);
	private BinaryData tt562OldNxmon62 = new BinaryData(8, 0).isAPartOf(filler1, 244);
	private BinaryData tt562OldNxmon63 = new BinaryData(8, 0).isAPartOf(filler1, 248);
	private BinaryData tt562OldNxmon64 = new BinaryData(8, 0).isAPartOf(filler1, 252);
	private BinaryData tt562OldNxmon65 = new BinaryData(8, 0).isAPartOf(filler1, 256);
	private BinaryData tt562OldNxmon66 = new BinaryData(8, 0).isAPartOf(filler1, 260);
	private BinaryData tt562OldNxmon67 = new BinaryData(8, 0).isAPartOf(filler1, 264);
	private BinaryData tt562OldNxmon68 = new BinaryData(8, 0).isAPartOf(filler1, 268);
	private BinaryData tt562OldNxmon69 = new BinaryData(8, 0).isAPartOf(filler1, 272);
	private BinaryData tt562OldNxmon70 = new BinaryData(8, 0).isAPartOf(filler1, 276);
	private BinaryData tt562OldNxmon71 = new BinaryData(8, 0).isAPartOf(filler1, 280);
	private BinaryData tt562OldNxmon72 = new BinaryData(8, 0).isAPartOf(filler1, 284);
	private BinaryData tt562OldNxmon73 = new BinaryData(8, 0).isAPartOf(filler1, 288);
	private BinaryData tt562OldNxmon74 = new BinaryData(8, 0).isAPartOf(filler1, 292);
	private BinaryData tt562OldNxmon75 = new BinaryData(8, 0).isAPartOf(filler1, 296);
	private BinaryData tt562OldNxmon76 = new BinaryData(8, 0).isAPartOf(filler1, 300);
	private BinaryData tt562OldNxmon77 = new BinaryData(8, 0).isAPartOf(filler1, 304);
	private BinaryData tt562OldNxmon78 = new BinaryData(8, 0).isAPartOf(filler1, 308);
	private BinaryData tt562OldNxmon79 = new BinaryData(8, 0).isAPartOf(filler1, 312);
	private BinaryData tt562OldNxmon80 = new BinaryData(8, 0).isAPartOf(filler1, 316);
	private BinaryData tt562OldNxmon81 = new BinaryData(8, 0).isAPartOf(filler1, 320);
	private BinaryData tt562OldNxmon82 = new BinaryData(8, 0).isAPartOf(filler1, 324);
	private BinaryData tt562OldNxmon83 = new BinaryData(8, 0).isAPartOf(filler1, 328);
	private BinaryData tt562OldNxmon84 = new BinaryData(8, 0).isAPartOf(filler1, 332);
	private BinaryData tt562OldNxmon85 = new BinaryData(8, 0).isAPartOf(filler1, 336);
	private BinaryData tt562OldNxmon86 = new BinaryData(8, 0).isAPartOf(filler1, 340);
	private BinaryData tt562OldNxmon87 = new BinaryData(8, 0).isAPartOf(filler1, 344);
	private BinaryData tt562OldNxmon88 = new BinaryData(8, 0).isAPartOf(filler1, 348);
	private BinaryData tt562OldNxmon89 = new BinaryData(8, 0).isAPartOf(filler1, 352);
	private BinaryData tt562OldNxmon90 = new BinaryData(8, 0).isAPartOf(filler1, 356);
	private BinaryData tt562OldNxmon91 = new BinaryData(8, 0).isAPartOf(filler1, 360);
	private BinaryData tt562OldNxmon92 = new BinaryData(8, 0).isAPartOf(filler1, 364);
	private BinaryData tt562OldNxmon93 = new BinaryData(8, 0).isAPartOf(filler1, 368);
	private BinaryData tt562OldNxmon94 = new BinaryData(8, 0).isAPartOf(filler1, 372);
	private BinaryData tt562OldNxmon95 = new BinaryData(8, 0).isAPartOf(filler1, 376);
	private BinaryData tt562OldNxmon96 = new BinaryData(8, 0).isAPartOf(filler1, 380);
	private BinaryData tt562OldNxmon97 = new BinaryData(8, 0).isAPartOf(filler1, 384);
	private BinaryData tt562OldNxmon98 = new BinaryData(8, 0).isAPartOf(filler1, 388);
	private BinaryData tt562OldNxmon99 = new BinaryData(8, 0).isAPartOf(filler1, 392);
	private FixedLengthStringData tt562OldTnxmons = new FixedLengthStringData(12).isAPartOf(tt562OldRec, 430);
	private BinaryData[] tt562OldTnxmon = BDArrayPartOfStructure(3, 8, 0, tt562OldTnxmons, 0);
	private FixedLengthStringData filler2 = new FixedLengthStringData(12).isAPartOf(tt562OldTnxmons, 0, FILLER_REDEFINE);
	private BinaryData tt562OldTnxmon01 = new BinaryData(8, 0).isAPartOf(filler2, 0);
	private BinaryData tt562OldTnxmon02 = new BinaryData(8, 0).isAPartOf(filler2, 4);
	private BinaryData tt562OldTnxmon03 = new BinaryData(8, 0).isAPartOf(filler2, 8);
}
}
