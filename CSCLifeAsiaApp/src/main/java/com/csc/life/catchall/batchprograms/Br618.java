/*
 * File: Br618.java
 * Date: 29 August 2009 22:27:29
 * Author: Quipoz Limited
 *
 * Class transformed from BR618.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.catchall.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.life.regularprocessing.dataaccess.AvyxpfTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 2005.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*  SPLITTER PROGRAM (For Anniversary batch processing)
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* This program is to be run before the Anniversary Batch
* Program, BR619.
* It is run in single-thread, and writes the selected CHDR
* records to multiple files . Each of the members will be read
* by a copy of BR619 run in multi-thread mode.
*
* Embedded SQL statement is used to access the COVRPF & CHDRPF.
* The splitter program will extract COVR records that meet the
* following criteria:
*
* 1) Anniv. Processing Date   <=  Batch Effective Date
* 2) Anniv. Processing Date   >=  Current From Date
* 3) Anniv. Processing Date   >=  Current From Date
* 4) Anniv. Processing Date    <  Current To Date
* 5) Anniv. Processing Date    <  Benefit Cessation Date
* 6) Validflag of both files   =  '1'
* 7) Billing Channel of CHDR  <>  'N'
* 8) Contract Number between  P6671-chdrnumfrom
*                        and  p6671-chdrnumto
*    order by chdrcoy, chdrnum, life, coverage, rider, plnsfx
*
* Control totals used in this program:
*    01  -  No. of thread members
*    02  -  No. of extracted records
*
***********************************************************************
* </pre>
*/
public class Br618 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlavyxCursorrs = null;
	private java.sql.PreparedStatement sqlavyxCursorps = null;
	private java.sql.Connection sqlavyxCursorconn = null;
	private String sqlavyxCursor = "";
	private int avyxCursorLoopIndex = 0;
	private DiskFileDAM avyx01 = new DiskFileDAM("AVYX01");
	private DiskFileDAM avyx02 = new DiskFileDAM("AVYX02");
	private DiskFileDAM avyx03 = new DiskFileDAM("AVYX03");
	private DiskFileDAM avyx04 = new DiskFileDAM("AVYX04");
	private DiskFileDAM avyx05 = new DiskFileDAM("AVYX05");
	private DiskFileDAM avyx06 = new DiskFileDAM("AVYX06");
	private DiskFileDAM avyx07 = new DiskFileDAM("AVYX07");
	private DiskFileDAM avyx08 = new DiskFileDAM("AVYX08");
	private DiskFileDAM avyx09 = new DiskFileDAM("AVYX09");
	private DiskFileDAM avyx10 = new DiskFileDAM("AVYX10");
	private DiskFileDAM avyx11 = new DiskFileDAM("AVYX11");
	private DiskFileDAM avyx12 = new DiskFileDAM("AVYX12");
	private DiskFileDAM avyx13 = new DiskFileDAM("AVYX13");
	private DiskFileDAM avyx14 = new DiskFileDAM("AVYX14");
	private DiskFileDAM avyx15 = new DiskFileDAM("AVYX15");
	private DiskFileDAM avyx16 = new DiskFileDAM("AVYX16");
	private DiskFileDAM avyx17 = new DiskFileDAM("AVYX17");
	private DiskFileDAM avyx18 = new DiskFileDAM("AVYX18");
	private DiskFileDAM avyx19 = new DiskFileDAM("AVYX19");
	private DiskFileDAM avyx20 = new DiskFileDAM("AVYX20");
	private AvyxpfTableDAM avyxpf = new AvyxpfTableDAM();
	private AvyxpfTableDAM avyxpfData = new AvyxpfTableDAM();
		/* Change the record length to that of the temporary file. This
		 can be found by doing a DSPFD of the file being duplicated
		 by the CRTTMPF process.*/
	private FixedLengthStringData avyx01Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx02Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx03Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx04Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx05Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx06Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx07Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx08Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx09Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx10Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx11Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx12Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx13Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx14Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx15Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx16Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx17Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx18Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx19Rec = new FixedLengthStringData(29);
	private FixedLengthStringData avyx20Rec = new FixedLengthStringData(29);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR618");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

	private FixedLengthStringData wsaaAvyxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaAvyxFn, 0, FILLER).init("AVYX");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaAvyxFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaAvyxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThread = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThread, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThread, 6).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaThread, 9, FILLER).init(SPACES);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaAvyxData = FLSInittedArray (1000, 29);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaAvyxData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaAvyxData, 1);
	private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaAvyxData, 9);
	private FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaAvyxData, 11);
	private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaAvyxData, 13);
	private PackedDecimalData[] wsaaPlnsfx = PDArrayPartOfArrayStructure(4, 0, wsaaAvyxData, 15);
	private FixedLengthStringData[] wsaaCrtable = FLSDArrayPartOfArrayStructure(4, wsaaAvyxData, 18);
	private PackedDecimalData[] wsaaCrrcd = PDArrayPartOfArrayStructure(8, 0, wsaaAvyxData, 22);
	private FixedLengthStringData[] wsaaStatcode = FLSDArrayPartOfArrayStructure(2, wsaaAvyxData, 27);

		/* WSAA-IND-ARRAY */
	private FixedLengthStringData[] wsaaAvyxInd = FLSInittedArray (1000, 8);
	private BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(4, 4, 0, wsaaAvyxInd, 0);
	private FixedLengthStringData wsaa1 = new FixedLengthStringData(1).init("1");
	private PackedDecimalData wsaaZeros = new PackedDecimalData(8, 0).init(ZERO);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).init(ZERO);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
		/* ERRORS */
	private static final String ivrm = "IVRM";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
	private DescTableDAM descIO = new DescTableDAM();
	private P6671par p6671par = new P6671par();

	public Br618() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** This section is invoked in restart mode. Note section 1100 must*/
		/** clear each temporary file member as the members are not under*/
		/** commitment control. Note also that Splitter programs must have*/
		/** a Restart Method of 1 (re-run). Thus no updating should occur*/
		/** which would result in different records being returned from the*/
		/** primary file in a restart!*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/*    Check the restart method is compatible with the program.*/
		/*    This program is restartable but will always re-run from*/
		/*    the beginning. This is because the temporary file members*/
		/*    are not under commitment control.*/
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*    Do a ovrdbf for each temporary file member. (Note we have*/
		/*    allowed for a maximum of 20.)*/
		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		/*    Since this program runs from a parameter screen move the*/
		/*    internal parameter area to the original parameter copybook*/
		/*    to retrieve the contract range*/
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES)
		&& isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		/*    The next thing we must do is construct the name of the*/
		/*    temporary file which we will be working with.*/
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		/*    Do a ovrdbf for each temporary file member. (Note we have*/
		/*    allowed for a maximum of 20.)*/
		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		/*    Log the number of threads being used*/
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		wsaaCompany.set(bsprIO.getCompany());
		wsaaEffdate.set(bsscIO.getEffectiveDate());
		sqlavyxCursor = " SELECT  A.CHDRCOY, A.CHDRNUM, A.LIFE, A.COVERAGE, A.RIDER, A.PLNSFX, A.CRTABLE, A.CRRCD, A.STATCODE" +
" FROM   " + getAppVars().getTableNameOverriden("COVRPF") + "  A" +
" WHERE A.CHDRCOY = ?" +
" AND A.CHDRNUM BETWEEN ? AND ?" +
" AND A.VALIDFLAG = ?" +
" AND A.CBANPR <> ?" +
" AND A.CBANPR <= ?" +
" AND A.CBANPR >= A.CURRFROM" +
" AND A.CBANPR < A.CURRTO" +
" AND A.CBANPR < A.BCESDTE" +
" ORDER BY A.CHDRCOY, A.CHDRNUM, A.LIFE, A.COVERAGE, A.RIDER, A.PLNSFX";
		/*    Initialise the array which will hold all the records*/
		/*    returned from each fetch. (INITIALIZE will not work as the*/
		/*    the array is indexed.*/
		iy.set(1);
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialize(wsaaAvyxData[wsaaInd.toInt()]);
		}
		wsaaInd.set(1);
		sqlerrorflag = false;
		try {
			sqlavyxCursorconn = getAppVars().getDBConnectionForTable(new com.csc.life.newbusiness.dataaccess.CovrpfTableDAM());
			sqlavyxCursorps = getAppVars().prepareStatementEmbeded(sqlavyxCursorconn, sqlavyxCursor, "COVRPF");
			getAppVars().setDBString(sqlavyxCursorps, 1, wsaaCompany);
			getAppVars().setDBString(sqlavyxCursorps, 2, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlavyxCursorps, 3, wsaaChdrnumTo);
			getAppVars().setDBString(sqlavyxCursorps, 4, wsaa1);
			getAppVars().setDBNumber(sqlavyxCursorps, 5, wsaaZeros);
			getAppVars().setDBNumber(sqlavyxCursorps, 6, wsaaEffdate);
			sqlavyxCursorrs = getAppVars().executeQuery(sqlavyxCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
	}

protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

protected void openThreadMember1110()
	{
		/*    Clear the member for this thread in case we are in restart*/
		/*    mode in which case it might already contain data.*/
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(), 1), iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaAvyxFn, wsaaThread, wsaaStatuz);
		if (isNE(wsaaStatuz, varcom.oK)) {
			syserrrec.syserrType.set("2");
			syserrrec.subrname.set("CLRTMPF");
			syserrrec.statuz.set(wsaaStatuz);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(AVYX");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaAvyxFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThread);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*    Open the file.*/
		if (isEQ(iz, 1)) {
			avyx01.openOutput();
		}
		if (isEQ(iz, 2)) {
			avyx02.openOutput();
		}
		if (isEQ(iz, 3)) {
			avyx03.openOutput();
		}
		if (isEQ(iz, 4)) {
			avyx04.openOutput();
		}
		if (isEQ(iz, 5)) {
			avyx05.openOutput();
		}
		if (isEQ(iz, 6)) {
			avyx06.openOutput();
		}
		if (isEQ(iz, 7)) {
			avyx07.openOutput();
		}
		if (isEQ(iz, 8)) {
			avyx08.openOutput();
		}
		if (isEQ(iz, 9)) {
			avyx09.openOutput();
		}
		if (isEQ(iz, 10)) {
			avyx10.openOutput();
		}
		if (isEQ(iz, 11)) {
			avyx11.openOutput();
		}
		if (isEQ(iz, 12)) {
			avyx12.openOutput();
		}
		if (isEQ(iz, 13)) {
			avyx13.openOutput();
		}
		if (isEQ(iz, 14)) {
			avyx14.openOutput();
		}
		if (isEQ(iz, 15)) {
			avyx15.openOutput();
		}
		if (isEQ(iz, 16)) {
			avyx16.openOutput();
		}
		if (isEQ(iz, 17)) {
			avyx17.openOutput();
		}
		if (isEQ(iz, 18)) {
			avyx18.openOutput();
		}
		if (isEQ(iz, 19)) {
			avyx19.openOutput();
		}
		if (isEQ(iz, 20)) {
			avyx20.openOutput();
		}
	}

protected void readFile2000()
	{
		readFiles2010();
	}

protected void readFiles2010()
	{
		/*  Now a block of records is fetched into the array.*/
		/*  Also on the first entry into the program we must set up the*/
		/*  WSAA-PREV-CHDRNUM = the present chdrnum.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd, 1)) {
			return ;
		}
		sqlerrorflag = false;
		try {
			for (avyxCursorLoopIndex = 1; isLTE(avyxCursorLoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlavyxCursorrs); avyxCursorLoopIndex++ ){
				getAppVars().getDBObject(sqlavyxCursorrs, 1, wsaaChdrcoy[avyxCursorLoopIndex], wsaaNullInd[avyxCursorLoopIndex][1]);
				getAppVars().getDBObject(sqlavyxCursorrs, 2, wsaaChdrnum[avyxCursorLoopIndex], wsaaNullInd[avyxCursorLoopIndex][2]);
				getAppVars().getDBObject(sqlavyxCursorrs, 3, wsaaLife[avyxCursorLoopIndex], wsaaNullInd[avyxCursorLoopIndex][3]);
				getAppVars().getDBObject(sqlavyxCursorrs, 4, wsaaCoverage[avyxCursorLoopIndex], wsaaNullInd[avyxCursorLoopIndex][4]);
				getAppVars().getDBObject(sqlavyxCursorrs, 5, wsaaRider[avyxCursorLoopIndex]);
				getAppVars().getDBObject(sqlavyxCursorrs, 6, wsaaPlnsfx[avyxCursorLoopIndex]);
				getAppVars().getDBObject(sqlavyxCursorrs, 7, wsaaCrtable[avyxCursorLoopIndex]);
				getAppVars().getDBObject(sqlavyxCursorrs, 8, wsaaCrrcd[avyxCursorLoopIndex]);
				getAppVars().getDBObject(sqlavyxCursorrs, 9, wsaaStatcode[avyxCursorLoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		/*  We must detect :-*/
		/*      a) no rows returned on the first fetch*/
		/*      b) the last row returned (in the block)*/
		/*  IF Either of the above cases occur, then an*/
		/*      SQLCODE = +100 is returned.*/
		/*  The 3000 section is continued for case (b).*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(varcom.endp);
		}
		//tom chi modified, bug 1030
		else if(isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)){
			wsspEdterror.set(varcom.endp);
		}
		//end
		else {
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}
	}

protected void edit2500()
	{
		/*READ*/
		/*  Check record is required for processsing but do not do any othe*/
		/*  reads to any other file. The exception to this rule are reads t*/
		/*  ITEM and these should be kept to a mimimum. Do not do any soft*/
		/*  locking in Splitter programs. The soft lock should be done by*/
		/*  the subsequent program which reads the temporary file.*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/* In the update section we write a record to the temporary file*/
		/* member identified by the value of IY. If it is possible to*/
		/* to include the RRN from the primary file we should pass*/
		/* this data as the subsequent program can then use it to do*/
		/* a direct read which is faster than a normal read.*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}

		/*  Re-initialise the block for next fetch and point to first row.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialize(wsaaAvyxData[wsaaInd.toInt()]);
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*  If the the CHDRNUM being processed is not equal to the*/
		/*  to the previous we should move to the next output file member.*/
		/*  The condition is here to allow for checking the last chdrnum*/
		/*  of the old block with the first of the new and to move to the*/
		/*  next PAYX member to write to if they have changed.*/
		if (isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)) {
			wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
			iy.add(1);
		}
		/*  Load from storage all AVYX data for the same contract until*/
		/*  the CHDRNUM on AVYX has changed,*/
		/*    OR*/
		/*  The end of an incomplete block is reached.*/
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}

		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		start3200();
	}

protected void start3200()
	{
		avyxpfData.chdrcoy.set(wsaaChdrcoy[wsaaInd.toInt()]);
		avyxpfData.chdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
		avyxpfData.life.set(wsaaLife[wsaaInd.toInt()]);
		avyxpfData.coverage.set(wsaaCoverage[wsaaInd.toInt()]);
		avyxpfData.rider.set(wsaaRider[wsaaInd.toInt()]);
		avyxpfData.planSuffix.set(wsaaPlnsfx[wsaaInd.toInt()]);
		avyxpfData.crtable.set(wsaaCrtable[wsaaInd.toInt()]);
		avyxpfData.crrcd.set(wsaaCrrcd[wsaaInd.toInt()]);
		avyxpfData.statcode.set(wsaaStatcode[wsaaInd.toInt()]);
		if (isGT(iy, bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		/*    Write records to their corresponding temporary file*/
		/*    members, determined by the working storage count, IY.*/
		/*    This spreads the transaction members evenly across*/
		/*    the thread members.*/
		if (isEQ(iy, 1)) {
			avyx01.write(avyxpfData);
		}
		if (isEQ(iy, 2)) {
			avyx02.write(avyxpfData);
		}
		if (isEQ(iy, 3)) {
			avyx03.write(avyxpfData);
		}
		if (isEQ(iy, 4)) {
			avyx04.write(avyxpfData);
		}
		if (isEQ(iy, 5)) {
			avyx05.write(avyxpfData);
		}
		if (isEQ(iy, 6)) {
			avyx06.write(avyxpfData);
		}
		if (isEQ(iy, 7)) {
			avyx07.write(avyxpfData);
		}
		if (isEQ(iy, 8)) {
			avyx08.write(avyxpfData);
		}
		if (isEQ(iy, 9)) {
			avyx09.write(avyxpfData);
		}
		if (isEQ(iy, 10)) {
			avyx10.write(avyxpfData);
		}
		if (isEQ(iy, 11)) {
			avyx11.write(avyxpfData);
		}
		if (isEQ(iy, 12)) {
			avyx12.write(avyxpfData);
		}
		if (isEQ(iy, 13)) {
			avyx13.write(avyxpfData);
		}
		if (isEQ(iy, 14)) {
			avyx14.write(avyxpfData);
		}
		if (isEQ(iy, 15)) {
			avyx15.write(avyxpfData);
		}
		if (isEQ(iy, 16)) {
			avyx16.write(avyxpfData);
		}
		if (isEQ(iy, 17)) {
			avyx17.write(avyxpfData);
		}
		if (isEQ(iy, 18)) {
			avyx18.write(avyxpfData);
		}
		if (isEQ(iy, 19)) {
			avyx19.write(avyxpfData);
		}
		if (isEQ(iy, 20)) {
			avyx20.write(avyxpfData);
		}
		/*    Log the number of extacted records.*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
		/*  Set up the array for the next block of records.*/
		wsaaInd.add(1);
		/*  Check for an incomplete block retrieved.*/
		if (isLTE(wsaaInd,wsaaRowsInBlock) && isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)) {
			wsaaEofInBlock.set("Y");
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/**    There is no pre-commit processing to be done*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close the open files and remove the overrides.*/
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		close4110();
	}

protected void close4110()
	{
		if (isEQ(iz, 1)) {
			avyx01.close();
		}
		if (isEQ(iz, 2)) {
			avyx02.close();
		}
		if (isEQ(iz, 3)) {
			avyx03.close();
		}
		if (isEQ(iz, 4)) {
			avyx04.close();
		}
		if (isEQ(iz, 5)) {
			avyx05.close();
		}
		if (isEQ(iz, 6)) {
			avyx06.close();
		}
		if (isEQ(iz, 7)) {
			avyx07.close();
		}
		if (isEQ(iz, 8)) {
			avyx08.close();
		}
		if (isEQ(iz, 9)) {
			avyx09.close();
		}
		if (isEQ(iz, 10)) {
			avyx10.close();
		}
		if (isEQ(iz, 11)) {
			avyx11.close();
		}
		if (isEQ(iz, 12)) {
			avyx12.close();
		}
		if (isEQ(iz, 13)) {
			avyx13.close();
		}
		if (isEQ(iz, 14)) {
			avyx14.close();
		}
		if (isEQ(iz, 15)) {
			avyx15.close();
		}
		if (isEQ(iz, 16)) {
			avyx16.close();
		}
		if (isEQ(iz, 17)) {
			avyx17.close();
		}
		if (isEQ(iz, 18)) {
			avyx18.close();
		}
		if (isEQ(iz, 19)) {
			avyx19.close();
		}
		if (isEQ(iz, 20)) {
			avyx20.close();
		}
	}
}
