/*
 * File: Cnvvflglcl.java
 * Date: December 3, 2013 4:08:22 AM ICT
 * Author: $Id$
 * 
 * Class transformed from CNVVFLGLCL.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.catchall.cls;

import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.catchall.procedures.Cnvvflagl;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class Cnvvflglcl extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData statuz = new FixedLengthStringData(4);

	public Cnvvflglcl() {
		super();
	}




public void mainline()
		throws ExtMsgException
	{
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			try {
				try {
					switch (qState) {
					case QS_START: 
						appVars.startCommitControl();
					default:{
						qState = QS_END;
					}
					}
				}
				catch (ExtMsgException ex1){
					if (ex1.messageMatches("CPF8351")) {
					}
					else {
						throw ex1;
					}
				}
				callProgram(Cnvvflagl.class, new Object[] {statuz});
				if (isNE(statuz, "****")) {
					appVars.sendMessageToQueue("An error occured in CNVVFLAGL Error Status is "+statuz+".", "*");
					rollback();
					qState = returnVar;
					break;
				}
				appVars.commit();
				 returnVar: ;
				try {
					appVars.endCommitControl();
				}
				catch (ExtMsgException ex1){
					if (ex1.messageMatches("CPF8350")) {
					}
					else {
						throw ex1;
					}
				}
				return ;
				/* UNREACHABLE CODE
				 error: ;
				appVars.sendMessageToQueue("Unexpected errors occurred", "*");
				statuz.set("BOMB");
				rollback();
				qState = returnVar;
				break;
				 */
			}
			catch (ExtMsgException ex){
				if (ex.messageMatches("CPF0000")
				|| ex.messageMatches("LBE0000")) {
					qState = error;
				}
				else {
					throw ex;
				}
			}
		}
		
	}
}
