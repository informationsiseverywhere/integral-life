/*
 * File: Cnvth528.java
 * Date: December 3, 2013 2:17:34 AM ICT
 * Author: CSC
 * 
 * Class transformed from CNVTH528.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.catchall.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.cashdividends.tablestructures.Th528rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table TH528, Replace INSTPR with INSTPR 1,
*   add new fields INSTPR02 to INSTPR11.
*
*
****************************************************************** ****
* </pre>
*/
public class Cnvth528 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVTH528");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String th528 = "TH528";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Th528rec th528rec = new Th528rec();
	private Varcom varcom = new Varcom();
	private Th528OldRecInner th528OldRecInner = new Th528OldRecInner();

	public Cnvth528() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(th528);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT TH528 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(), th528)) {
			getAppVars().addDiagnostic("Table TH528 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itemIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT TH528 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteTh5282080();
	}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		th528OldRecInner.th528OldRec.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		th528rec.insprms.set(th528OldRecInner.th528OldInsprms);
		th528rec.mfacthm.set(th528OldRecInner.th528OldMfacthm);
		th528rec.mfacthy.set(th528OldRecInner.th528OldMfacthy);
		th528rec.mfactm.set(th528OldRecInner.th528OldMfactm);
		th528rec.mfactq.set(th528OldRecInner.th528OldMfactq);
		th528rec.mfactw.set(th528OldRecInner.th528OldMfactw);
		th528rec.mfact2w.set(th528OldRecInner.th528OldMfact2w);
		th528rec.mfact4w.set(th528OldRecInner.th528OldMfact4w);
		th528rec.mfacty.set(th528OldRecInner.th528OldMfacty);
		th528rec.premUnit.set(th528OldRecInner.th528OldPremUnit);
		th528rec.unit.set(th528OldRecInner.th528OldUnit);
		th528rec.instpr01.set(th528OldRecInner.th528OldInstpr);
		for (ix.set(2); !(isGT(ix, 11)); ix.add(1)){
			th528rec.instpr[ix.toInt()].set(ZERO);
		}
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(th528rec.th528Rec);
	}

protected void rewriteTh5282080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(), th528)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for TH528");
		}
	}
/*
 * Class transformed  from Data Structure TH528-OLD-REC--INNER
 */
private static final class Th528OldRecInner { 

	private FixedLengthStringData th528OldRec = new FixedLengthStringData(431);
	private FixedLengthStringData th528OldInsprms = new FixedLengthStringData(396).isAPartOf(th528OldRec, 0);
	private PackedDecimalData[] th528OldInsprm = PDArrayPartOfStructure(99, 6, 0, th528OldInsprms, 0);
	private PackedDecimalData th528OldInstpr = new PackedDecimalData(6, 0).isAPartOf(th528OldRec, 396);
	private PackedDecimalData th528OldMfacthm = new PackedDecimalData(5, 4).isAPartOf(th528OldRec, 400);
	private PackedDecimalData th528OldMfacthy = new PackedDecimalData(5, 4).isAPartOf(th528OldRec, 403);
	private PackedDecimalData th528OldMfactm = new PackedDecimalData(5, 4).isAPartOf(th528OldRec, 406);
	private PackedDecimalData th528OldMfactq = new PackedDecimalData(5, 4).isAPartOf(th528OldRec, 409);
	private PackedDecimalData th528OldMfactw = new PackedDecimalData(5, 4).isAPartOf(th528OldRec, 412);
	private PackedDecimalData th528OldMfact2w = new PackedDecimalData(5, 4).isAPartOf(th528OldRec, 415);
	private PackedDecimalData th528OldMfact4w = new PackedDecimalData(5, 4).isAPartOf(th528OldRec, 418);
	private PackedDecimalData th528OldMfacty = new PackedDecimalData(5, 4).isAPartOf(th528OldRec, 421);
	private PackedDecimalData th528OldPremUnit = new PackedDecimalData(3, 0).isAPartOf(th528OldRec, 424);
	private PackedDecimalData th528OldUnit = new PackedDecimalData(9, 0).isAPartOf(th528OldRec, 426);
}
}
