/*
 * File: Cnvth606.java
 * Date: December 3, 2013 2:17:44 AM ICT
 * Author: CSC
 * 
 * Class transformed from CNVTH606.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.catchall.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.tablestructures.Th606rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table TH606, Replace INSTPR with INSTPR 1,
*   add new fields INSTPR02 to INSTPR11.
*
*
****************************************************************** ****
* </pre>
*/
public class Cnvth606 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVTH606");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String th606 = "TH606";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Th606rec th606rec = new Th606rec();
	private Varcom varcom = new Varcom();
	private Th606OldRecInner th606OldRecInner = new Th606OldRecInner();

	public Cnvth606() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(th606);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT TH606 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(), th606)) {
			getAppVars().addDiagnostic("Table TH606 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itemIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT TH606 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteTh6062080();
	}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		th606OldRecInner.th606OldRec.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		th606rec.disccntmeth.set(th606OldRecInner.th606OldDisccntmeth);
		th606rec.premUnit.set(th606OldRecInner.th606OldPremUnit);
		th606rec.unit.set(th606OldRecInner.th606OldUnit);
		th606rec.insprem.set(th606OldRecInner.th606OldInsprem);
		th606rec.mfacthm.set(th606OldRecInner.th606OldMfacthm);
		th606rec.mfacthy.set(th606OldRecInner.th606OldMfacthy);
		th606rec.mfactm.set(th606OldRecInner.th606OldMfactm);
		th606rec.mfactq.set(th606OldRecInner.th606OldMfactq);
		th606rec.mfactw.set(th606OldRecInner.th606OldMfactw);
		th606rec.mfact2w.set(th606OldRecInner.th606OldMfact2w);
		th606rec.mfact4w.set(th606OldRecInner.th606OldMfact4w);
		th606rec.mfacty.set(th606OldRecInner.th606OldMfacty);
		th606rec.instpr01.set(th606OldRecInner.th606OldInstpr);
		for (ix.set(1); !(isGT(ix, 99)); ix.add(1)){
			th606rec.insprm[ix.toInt()].set(th606OldRecInner.th606OldInsprm[ix.toInt()]);
		}
		for (ix.set(2); !(isGT(ix, 11)); ix.add(1)){
			th606rec.instpr[ix.toInt()].set(ZERO);
		}
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(th606rec.th606Rec);
	}

protected void rewriteTh6062080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(), th606)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for TH606");
		}
	}
/*
 * Class transformed  from Data Structure TH606-OLD-REC--INNER
 */
private static final class Th606OldRecInner { 

	private FixedLengthStringData th606OldRec = new FixedLengthStringData(439);
	private FixedLengthStringData th606OldDisccntmeth = new FixedLengthStringData(4).isAPartOf(th606OldRec, 0);
	private FixedLengthStringData th606OldInsprms = new FixedLengthStringData(396).isAPartOf(th606OldRec, 4);
	private PackedDecimalData[] th606OldInsprm = PDArrayPartOfStructure(99, 6, 0, th606OldInsprms, 0);
	private PackedDecimalData th606OldInstpr = new PackedDecimalData(6, 0).isAPartOf(th606OldRec, 400);
	private PackedDecimalData th606OldMfacthm = new PackedDecimalData(5, 4).isAPartOf(th606OldRec, 404);
	private PackedDecimalData th606OldMfacthy = new PackedDecimalData(5, 4).isAPartOf(th606OldRec, 407);
	private PackedDecimalData th606OldMfactm = new PackedDecimalData(5, 4).isAPartOf(th606OldRec, 410);
	private PackedDecimalData th606OldMfactq = new PackedDecimalData(5, 4).isAPartOf(th606OldRec, 413);
	private PackedDecimalData th606OldMfactw = new PackedDecimalData(5, 4).isAPartOf(th606OldRec, 416);
	private PackedDecimalData th606OldMfact2w = new PackedDecimalData(5, 4).isAPartOf(th606OldRec, 419);
	private PackedDecimalData th606OldMfact4w = new PackedDecimalData(5, 4).isAPartOf(th606OldRec, 422);
	private PackedDecimalData th606OldMfacty = new PackedDecimalData(5, 4).isAPartOf(th606OldRec, 425);
	private PackedDecimalData th606OldPremUnit = new PackedDecimalData(3, 0).isAPartOf(th606OldRec, 428);
	private PackedDecimalData th606OldUnit = new PackedDecimalData(9, 0).isAPartOf(th606OldRec, 430);
	private PackedDecimalData th606OldInsprem = new PackedDecimalData(6, 0).isAPartOf(th606OldRec, 435);
}
}
