/*
 * File: Cnvt6686.java
 * Date: December 3, 2013 2:17:27 AM ICT
 * Author: CSC
 * 
 * Class transformed from CNVT6686.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.catchall.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.underwriting.tablestructures.T6686rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table T6686, add new fields LXDP16,
*   TLXMRT01 to TLXMRT11.
*
*
****************************************************************** ****
* </pre>
*/
public class Cnvt6686 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVT6686");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String t6686 = "T6686";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private PackedDecimalData ix = new PackedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T6686rec t6686rec = new T6686rec();
	private Varcom varcom = new Varcom();
	private T6686OldRecInner t6686OldRecInner = new T6686OldRecInner();

	public Cnvt6686() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(t6686);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT T6686 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(), t6686)) {
			getAppVars().addDiagnostic("Table T6686 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itemIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT T6686 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteT66862080();
	}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		t6686OldRecInner.t6686OldRec.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		t6686rec.lxmort.set(t6686OldRecInner.t6686OldLxmort);
		for (ix.set(1); !(isGT(ix, 15)); ix.add(1)){
			t6686rec.lxdp[ix.toInt()].set(t6686OldRecInner.t6686OldLxdp[ix.toInt()]);
		}
		t6686rec.lxdp16.set(ZERO);
		for (ix.set(1); !(isGT(ix, 99)); ix.add(1)){
			t6686rec.lxmrt[ix.toInt()].set(t6686OldRecInner.t6686OldLxmrt[ix.toInt()]);
		}
		for (ix.set(1); !(isGT(ix, 11)); ix.add(1)){
			t6686rec.tlxmrt[ix.toInt()].set(ZERO);
		}
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(t6686rec.t6686Rec);
	}

protected void rewriteT66862080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(), t6686)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for T6686");
		}
	}
/*
 * Class transformed  from Data Structure T6686-OLD-REC--INNER
 */
private static final class T6686OldRecInner { 

	private FixedLengthStringData t6686OldRec = new FixedLengthStringData(430);
	private FixedLengthStringData t6686OldLxdps = new FixedLengthStringData(30).isAPartOf(t6686OldRec, 0);
	private BinaryData[] t6686OldLxdp = BDArrayPartOfStructure(15, 1, 0, t6686OldLxdps, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(t6686OldLxdps, 0, FILLER_REDEFINE);
	private BinaryData t6686OldLxdp01 = new BinaryData(1, 0).isAPartOf(filler, 0);
	private BinaryData t6686OldLxdp02 = new BinaryData(1, 0).isAPartOf(filler, 2);
	private BinaryData t6686OldLxdp03 = new BinaryData(1, 0).isAPartOf(filler, 4);
	private BinaryData t6686OldLxdp04 = new BinaryData(1, 0).isAPartOf(filler, 6);
	private BinaryData t6686OldLxdp05 = new BinaryData(1, 0).isAPartOf(filler, 8);
	private BinaryData t6686OldLxdp06 = new BinaryData(1, 0).isAPartOf(filler, 10);
	private BinaryData t6686OldLxdp07 = new BinaryData(1, 0).isAPartOf(filler, 12);
	private BinaryData t6686OldLxdp08 = new BinaryData(1, 0).isAPartOf(filler, 14);
	private BinaryData t6686OldLxdp09 = new BinaryData(1, 0).isAPartOf(filler, 16);
	private BinaryData t6686OldLxdp10 = new BinaryData(1, 0).isAPartOf(filler, 18);
	private BinaryData t6686OldLxdp11 = new BinaryData(1, 0).isAPartOf(filler, 20);
	private BinaryData t6686OldLxdp12 = new BinaryData(1, 0).isAPartOf(filler, 22);
	private BinaryData t6686OldLxdp13 = new BinaryData(1, 0).isAPartOf(filler, 24);
	private BinaryData t6686OldLxdp14 = new BinaryData(1, 0).isAPartOf(filler, 26);
	private BinaryData t6686OldLxdp15 = new BinaryData(1, 0).isAPartOf(filler, 28);
	private BinaryData t6686OldLxmort = new BinaryData(8, 0).isAPartOf(t6686OldRec, 30);
	private FixedLengthStringData t6686OldLxmrts = new FixedLengthStringData(396).isAPartOf(t6686OldRec, 34);
	private BinaryData[] t6686OldLxmrt = BDArrayPartOfStructure(99, 8, 0, t6686OldLxmrts, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(396).isAPartOf(t6686OldLxmrts, 0, FILLER_REDEFINE);
	private BinaryData t6686OldLxmrt01 = new BinaryData(8, 0).isAPartOf(filler1, 0);
	private BinaryData t6686OldLxmrt02 = new BinaryData(8, 0).isAPartOf(filler1, 4);
	private BinaryData t6686OldLxmrt03 = new BinaryData(8, 0).isAPartOf(filler1, 8);
	private BinaryData t6686OldLxmrt04 = new BinaryData(8, 0).isAPartOf(filler1, 12);
	private BinaryData t6686OldLxmrt05 = new BinaryData(8, 0).isAPartOf(filler1, 16);
	private BinaryData t6686OldLxmrt06 = new BinaryData(8, 0).isAPartOf(filler1, 20);
	private BinaryData t6686OldLxmrt07 = new BinaryData(8, 0).isAPartOf(filler1, 24);
	private BinaryData t6686OldLxmrt08 = new BinaryData(8, 0).isAPartOf(filler1, 28);
	private BinaryData t6686OldLxmrt09 = new BinaryData(8, 0).isAPartOf(filler1, 32);
	private BinaryData t6686OldLxmrt10 = new BinaryData(8, 0).isAPartOf(filler1, 36);
	private BinaryData t6686OldLxmrt11 = new BinaryData(8, 0).isAPartOf(filler1, 40);
	private BinaryData t6686OldLxmrt12 = new BinaryData(8, 0).isAPartOf(filler1, 44);
	private BinaryData t6686OldLxmrt13 = new BinaryData(8, 0).isAPartOf(filler1, 48);
	private BinaryData t6686OldLxmrt14 = new BinaryData(8, 0).isAPartOf(filler1, 52);
	private BinaryData t6686OldLxmrt15 = new BinaryData(8, 0).isAPartOf(filler1, 56);
	private BinaryData t6686OldLxmrt16 = new BinaryData(8, 0).isAPartOf(filler1, 60);
	private BinaryData t6686OldLxmrt17 = new BinaryData(8, 0).isAPartOf(filler1, 64);
	private BinaryData t6686OldLxmrt18 = new BinaryData(8, 0).isAPartOf(filler1, 68);
	private BinaryData t6686OldLxmrt19 = new BinaryData(8, 0).isAPartOf(filler1, 72);
	private BinaryData t6686OldLxmrt20 = new BinaryData(8, 0).isAPartOf(filler1, 76);
	private BinaryData t6686OldLxmrt21 = new BinaryData(8, 0).isAPartOf(filler1, 80);
	private BinaryData t6686OldLxmrt22 = new BinaryData(8, 0).isAPartOf(filler1, 84);
	private BinaryData t6686OldLxmrt23 = new BinaryData(8, 0).isAPartOf(filler1, 88);
	private BinaryData t6686OldLxmrt24 = new BinaryData(8, 0).isAPartOf(filler1, 92);
	private BinaryData t6686OldLxmrt25 = new BinaryData(8, 0).isAPartOf(filler1, 96);
	private BinaryData t6686OldLxmrt26 = new BinaryData(8, 0).isAPartOf(filler1, 100);
	private BinaryData t6686OldLxmrt27 = new BinaryData(8, 0).isAPartOf(filler1, 104);
	private BinaryData t6686OldLxmrt28 = new BinaryData(8, 0).isAPartOf(filler1, 108);
	private BinaryData t6686OldLxmrt29 = new BinaryData(8, 0).isAPartOf(filler1, 112);
	private BinaryData t6686OldLxmrt30 = new BinaryData(8, 0).isAPartOf(filler1, 116);
	private BinaryData t6686OldLxmrt31 = new BinaryData(8, 0).isAPartOf(filler1, 120);
	private BinaryData t6686OldLxmrt32 = new BinaryData(8, 0).isAPartOf(filler1, 124);
	private BinaryData t6686OldLxmrt33 = new BinaryData(8, 0).isAPartOf(filler1, 128);
	private BinaryData t6686OldLxmrt34 = new BinaryData(8, 0).isAPartOf(filler1, 132);
	private BinaryData t6686OldLxmrt35 = new BinaryData(8, 0).isAPartOf(filler1, 136);
	private BinaryData t6686OldLxmrt36 = new BinaryData(8, 0).isAPartOf(filler1, 140);
	private BinaryData t6686OldLxmrt37 = new BinaryData(8, 0).isAPartOf(filler1, 144);
	private BinaryData t6686OldLxmrt38 = new BinaryData(8, 0).isAPartOf(filler1, 148);
	private BinaryData t6686OldLxmrt39 = new BinaryData(8, 0).isAPartOf(filler1, 152);
	private BinaryData t6686OldLxmrt40 = new BinaryData(8, 0).isAPartOf(filler1, 156);
	private BinaryData t6686OldLxmrt41 = new BinaryData(8, 0).isAPartOf(filler1, 160);
	private BinaryData t6686OldLxmrt42 = new BinaryData(8, 0).isAPartOf(filler1, 164);
	private BinaryData t6686OldLxmrt43 = new BinaryData(8, 0).isAPartOf(filler1, 168);
	private BinaryData t6686OldLxmrt44 = new BinaryData(8, 0).isAPartOf(filler1, 172);
	private BinaryData t6686OldLxmrt45 = new BinaryData(8, 0).isAPartOf(filler1, 176);
	private BinaryData t6686OldLxmrt46 = new BinaryData(8, 0).isAPartOf(filler1, 180);
	private BinaryData t6686OldLxmrt47 = new BinaryData(8, 0).isAPartOf(filler1, 184);
	private BinaryData t6686OldLxmrt48 = new BinaryData(8, 0).isAPartOf(filler1, 188);
	private BinaryData t6686OldLxmrt49 = new BinaryData(8, 0).isAPartOf(filler1, 192);
	private BinaryData t6686OldLxmrt50 = new BinaryData(8, 0).isAPartOf(filler1, 196);
	private BinaryData t6686OldLxmrt51 = new BinaryData(8, 0).isAPartOf(filler1, 200);
	private BinaryData t6686OldLxmrt52 = new BinaryData(8, 0).isAPartOf(filler1, 204);
	private BinaryData t6686OldLxmrt53 = new BinaryData(8, 0).isAPartOf(filler1, 208);
	private BinaryData t6686OldLxmrt54 = new BinaryData(8, 0).isAPartOf(filler1, 212);
	private BinaryData t6686OldLxmrt55 = new BinaryData(8, 0).isAPartOf(filler1, 216);
	private BinaryData t6686OldLxmrt56 = new BinaryData(8, 0).isAPartOf(filler1, 220);
	private BinaryData t6686OldLxmrt57 = new BinaryData(8, 0).isAPartOf(filler1, 224);
	private BinaryData t6686OldLxmrt58 = new BinaryData(8, 0).isAPartOf(filler1, 228);
	private BinaryData t6686OldLxmrt59 = new BinaryData(8, 0).isAPartOf(filler1, 232);
	private BinaryData t6686OldLxmrt60 = new BinaryData(8, 0).isAPartOf(filler1, 236);
	private BinaryData t6686OldLxmrt61 = new BinaryData(8, 0).isAPartOf(filler1, 240);
	private BinaryData t6686OldLxmrt62 = new BinaryData(8, 0).isAPartOf(filler1, 244);
	private BinaryData t6686OldLxmrt63 = new BinaryData(8, 0).isAPartOf(filler1, 248);
	private BinaryData t6686OldLxmrt64 = new BinaryData(8, 0).isAPartOf(filler1, 252);
	private BinaryData t6686OldLxmrt65 = new BinaryData(8, 0).isAPartOf(filler1, 256);
	private BinaryData t6686OldLxmrt66 = new BinaryData(8, 0).isAPartOf(filler1, 260);
	private BinaryData t6686OldLxmrt67 = new BinaryData(8, 0).isAPartOf(filler1, 264);
	private BinaryData t6686OldLxmrt68 = new BinaryData(8, 0).isAPartOf(filler1, 268);
	private BinaryData t6686OldLxmrt69 = new BinaryData(8, 0).isAPartOf(filler1, 272);
	private BinaryData t6686OldLxmrt70 = new BinaryData(8, 0).isAPartOf(filler1, 276);
	private BinaryData t6686OldLxmrt71 = new BinaryData(8, 0).isAPartOf(filler1, 280);
	private BinaryData t6686OldLxmrt72 = new BinaryData(8, 0).isAPartOf(filler1, 284);
	private BinaryData t6686OldLxmrt73 = new BinaryData(8, 0).isAPartOf(filler1, 288);
	private BinaryData t6686OldLxmrt74 = new BinaryData(8, 0).isAPartOf(filler1, 292);
	private BinaryData t6686OldLxmrt75 = new BinaryData(8, 0).isAPartOf(filler1, 296);
	private BinaryData t6686OldLxmrt76 = new BinaryData(8, 0).isAPartOf(filler1, 300);
	private BinaryData t6686OldLxmrt77 = new BinaryData(8, 0).isAPartOf(filler1, 304);
	private BinaryData t6686OldLxmrt78 = new BinaryData(8, 0).isAPartOf(filler1, 308);
	private BinaryData t6686OldLxmrt79 = new BinaryData(8, 0).isAPartOf(filler1, 312);
	private BinaryData t6686OldLxmrt80 = new BinaryData(8, 0).isAPartOf(filler1, 316);
	private BinaryData t6686OldLxmrt81 = new BinaryData(8, 0).isAPartOf(filler1, 320);
	private BinaryData t6686OldLxmrt82 = new BinaryData(8, 0).isAPartOf(filler1, 324);
	private BinaryData t6686OldLxmrt83 = new BinaryData(8, 0).isAPartOf(filler1, 328);
	private BinaryData t6686OldLxmrt84 = new BinaryData(8, 0).isAPartOf(filler1, 332);
	private BinaryData t6686OldLxmrt85 = new BinaryData(8, 0).isAPartOf(filler1, 336);
	private BinaryData t6686OldLxmrt86 = new BinaryData(8, 0).isAPartOf(filler1, 340);
	private BinaryData t6686OldLxmrt87 = new BinaryData(8, 0).isAPartOf(filler1, 344);
	private BinaryData t6686OldLxmrt88 = new BinaryData(8, 0).isAPartOf(filler1, 348);
	private BinaryData t6686OldLxmrt89 = new BinaryData(8, 0).isAPartOf(filler1, 352);
	private BinaryData t6686OldLxmrt90 = new BinaryData(8, 0).isAPartOf(filler1, 356);
	private BinaryData t6686OldLxmrt91 = new BinaryData(8, 0).isAPartOf(filler1, 360);
	private BinaryData t6686OldLxmrt92 = new BinaryData(8, 0).isAPartOf(filler1, 364);
	private BinaryData t6686OldLxmrt93 = new BinaryData(8, 0).isAPartOf(filler1, 368);
	private BinaryData t6686OldLxmrt94 = new BinaryData(8, 0).isAPartOf(filler1, 372);
	private BinaryData t6686OldLxmrt95 = new BinaryData(8, 0).isAPartOf(filler1, 376);
	private BinaryData t6686OldLxmrt96 = new BinaryData(8, 0).isAPartOf(filler1, 380);
	private BinaryData t6686OldLxmrt97 = new BinaryData(8, 0).isAPartOf(filler1, 384);
	private BinaryData t6686OldLxmrt98 = new BinaryData(8, 0).isAPartOf(filler1, 388);
	private BinaryData t6686OldLxmrt99 = new BinaryData(8, 0).isAPartOf(filler1, 392);
}
}
