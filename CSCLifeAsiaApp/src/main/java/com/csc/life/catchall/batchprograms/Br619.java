/*
 * File: Br619.java
 * Date: 29 August 2009 22:27:41
 * Author: Quipoz Limited
 *
 * Class transformed from BR619.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.catchall.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.catchall.dataaccess.dao.AvyxpfDAO;
import com.csc.life.catchall.dataaccess.model.Avyxpf;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.impl.CovrpfDAOImpl;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5519rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 2005.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*                 Regular Processing - Anniversary
*            -------------------------------------------
*            Substitute program for Old-structured B5094
*
*
***********************************************************************
* </pre>
*/
public class Br619 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	//private AvyxpfTableDAM avyxpfRec = new AvyxpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR619");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		  be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaDateFound = new FixedLengthStringData(1).init("N");
	private Validator dateFound = new Validator(wsaaDateFound, "Y");


		/*  Storage for T5687 table items.*/
	//private static final int wsaaT5687Size = 500;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5687Size = 1000;

		/* WSAA-T5687-ARRAY */
	private FixedLengthStringData[] wsaaT5687Rec = FLSInittedArray (1000, 13);
	private FixedLengthStringData[] wsaaT5687Key = FLSDArrayPartOfArrayStructure(4, wsaaT5687Rec, 0);
	private FixedLengthStringData[] wsaaT5687Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5687Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5687Data = FLSDArrayPartOfArrayStructure(9, wsaaT5687Rec, 4);
	private PackedDecimalData[] wsaaT5687Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5687Data, 0);
	private FixedLengthStringData[] wsaaT5687Annvry = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 5);
		/*  Storage for T6658 table items.*/
	//private static final int wsaaT6658Size = 50;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT6658Size = 1000;

		/* WSAA-T6658-ARRAY */
	private FixedLengthStringData[] wsaaT6658Rec = FLSInittedArray (1000, 19);
	private FixedLengthStringData[] wsaaT6658Key = FLSDArrayPartOfArrayStructure(4, wsaaT6658Rec, 0);
	private FixedLengthStringData[] wsaaT6658Annvry = FLSDArrayPartOfArrayStructure(4, wsaaT6658Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT6658Data = FLSDArrayPartOfArrayStructure(15, wsaaT6658Rec, 4);
	private PackedDecimalData[] wsaaT6658Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT6658Data, 0);
	private FixedLengthStringData[] wsaaT6658Subprog = FLSDArrayPartOfArrayStructure(10, wsaaT6658Data, 5);
		/*  Storage for T5540 table items.*/
	//private static final int wsaaT5540Size = 300;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5540Size = 1000;

		/* WSAA-T5540-ARRAY */
	private FixedLengthStringData[] wsaaT5540Rec = FLSInittedArray (1000, 13);
	private FixedLengthStringData[] wsaaT5540Key = FLSDArrayPartOfArrayStructure(4, wsaaT5540Rec, 0);
	private FixedLengthStringData[] wsaaT5540Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5540Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5540Data = FLSDArrayPartOfArrayStructure(9, wsaaT5540Rec, 4);
	private PackedDecimalData[] wsaaT5540Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5540Data, 0);
	private FixedLengthStringData[] wsaaT5540Iudiscbas = FLSDArrayPartOfArrayStructure(4, wsaaT5540Data, 5);
		/*  Storage for T5519 table items.*/
	//private static final int wsaaT5519Size = 20;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5519Size = 1000;

		/* WSAA-T5519-ARRAY */
	private FixedLengthStringData[] wsaaT5519Rec = FLSInittedArray (1000, 28);
	private FixedLengthStringData[] wsaaT5519Key = FLSDArrayPartOfArrayStructure(4, wsaaT5519Rec, 0);
	private FixedLengthStringData[] wsaaT5519Iudiscbas = FLSDArrayPartOfArrayStructure(4, wsaaT5519Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5519Data = FLSDArrayPartOfArrayStructure(24, wsaaT5519Rec, 4);
	private PackedDecimalData[] wsaaT5519Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5519Data, 0);
	private FixedLengthStringData[] wsaaT5519Iufreq = FLSDArrayPartOfArrayStructure(2, wsaaT5519Data, 5);
	private ZonedDecimalData[] wsaaT5519Annchg = ZDArrayPartOfArrayStructure(17, 2, wsaaT5519Data, 7);
		/* WSAA-VARIABLES */
	private ZonedDecimalData wsaaT5679Sub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaUpdateCovrOnly = new FixedLengthStringData(1).init(SPACES);
	private Validator updateCovrOnly = new Validator(wsaaUpdateCovrOnly, "Y");

	private FixedLengthStringData wsaaT5540Found = new FixedLengthStringData(1).init(SPACES);
	private Validator t5540Found = new Validator(wsaaT5540Found, "Y");
	private FixedLengthStringData wsaaAnnvry = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaSubprog = new FixedLengthStringData(10).init(SPACES);
	private FixedLengthStringData wsaaIudiscbas = new FixedLengthStringData(4).init(SPACES);

		/* ERRORS */
	private static final String h791 = "H791";
	private static final String ivrm = "IVRM";
		/* TABLES */
	private static final String t5679 = "T5679";
	private FixedLengthStringData t5687 = new FixedLengthStringData(6).init("T5687");
	private static final String t6658 = "T6658";
	private static final String t5540 = "T5540";
	private FixedLengthStringData t5519 = new FixedLengthStringData(6).init("T5519");
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct04 = 4;
	private static final int ct05 = 5;

	private FixedLengthStringData wsaaValidComp = new FixedLengthStringData(1).init("N");
	private Validator validComp = new Validator(wsaaValidComp, "Y");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private int wsaaT5687Ix;
	private int wsaaT6658Ix;
	private int wsaaT5540Ix;
	private int wsaaT5519Ix;
	private CovrTableDAM covrIO = new CovrTableDAM();

	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6658rec t6658rec = new T6658rec();
	private T5540rec t5540rec = new T5540rec();
	private T5519rec t5519rec = new T5519rec();
	private Annprocrec annprocrec = new Annprocrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private WsysSystemErrorParamsInner wsysSystemErrorParamsInner = new WsysSystemErrorParamsInner();
	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Map<String, List<Itempf>> t5687ListMap;
	private Map<String, List<Itempf>> t5519ListMap;
	private Map<String, List<Itempf>> t6658ListMap;
	private Map<String, List<Itempf>> t5540ListMap;
	private Map<String, List<Itempf>> t5679ListMap;
	private List<Itempf> t5679List;
	

	
	private FixedLengthStringData wsaaAvyxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaAvyxFn, 0, FILLER).init("AVYX");
	private FixedLengthStringData wsaaAvyxRunid = new FixedLengthStringData(2).isAPartOf(wsaaAvyxFn, 4);
	private ZonedDecimalData wsaaAvyxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaAvyxFn, 6).setUnsigned();

	
	private static String filler1 = "THREAD";
	private String wsaaThreadMember  = "";
	private String wsaaThreadNumber = "";
	
    private int intBatchID = 0;
    private int intBatchExtractSize;
    private Iterator<Avyxpf> iterator;
    private Avyxpf avyxpf;
	private AvyxpfDAO avyxDAO = getApplicationContext().getBean("avyxDAO", AvyxpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAOImpl.class);
	
	private List<Avyxpf> avyxList = null;
	private List<Covrpf> covrpfInsertList = new ArrayList<Covrpf>();
	private Covrpf covrpf = null;
	
	private int ctrCT01; 
	private int ctrCT02;
	private int ctrCT04; 
	private int ctrCT05;

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		readT56871120,
		readT66581220,
		readT55401320,
		readT55191420,
		callSoftlock2570,
		exit2590,
		validateRider2515,
		exit2519,
		updateCovr3050
	}

	public Br619() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Restarting of this program is handled by MAINB,*/
		/** using a restart method of '3'.*/
		/*EXIT*/
	}

	protected void initialise1000() {
		initialise1010();
		loadT5679();
		loadT5687();
		loadT6658();
		loadT5540();
		loadT5519();
	}

	

	protected void initialise1010() {
		if (isNE(bprdIO.getRestartMethod(), "3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/* Point to correct member of AVYXPF. */
		varcom.vrcmTranid.set(batcdorrec.tranid);
		wsaaAvyxRunid.set(bprdIO.getSystemParam04());		
		wsaaAvyxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber = this.formatThreadName(bsprIO.getProcessOccNum().toInt());

		wsaaThreadMember = filler1.trim() + wsaaThreadNumber.trim();
		 
		String company = bsprIO.getCompany().toString();
		t5687ListMap = itemDAO.loadSmartTable("IT", company, t5687.toString().trim());
		t6658ListMap = itemDAO.loadSmartTable("IT", company, t6658);
		t5540ListMap = itemDAO.loadSmartTable("IT", company, t5540);
		t5519ListMap = itemDAO.loadSmartTable("IT", company, t5519.toString().trim());
		t5679ListMap = itemDAO.loadSmartTable("IT", company, t5679);
		t5679List = t5679ListMap.get(bprdIO.getAuthCode().toString());
		
        if (bprdIO.systemParam01.isNumeric()){
            if (bprdIO.systemParam01.toInt() > 0){
                intBatchExtractSize = bprdIO.systemParam01.toInt();
            }else{
                intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
            }
        }else{
            intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();  
        }
        readChunkRecord();
        ctrCT01 = 0;
        ctrCT02 = 0;
        ctrCT04 = 0;
        ctrCT05 = 0;
	}

	private void readChunkRecord(){
			avyxList = avyxDAO.searchAvyxpfResult(wsaaAvyxFn.toString(), wsaaThreadMember, intBatchExtractSize, intBatchID);
		    if(avyxList != null && avyxList.size()>0){
		    	iterator = avyxList.iterator();    	   	
		    }else{
		    	wsspEdterror.set(varcom.endp);
		    	return;
		    }
	}
	protected void loadT5679() {
		if (t5679List == null || t5679List.size() == 0) {
			syserrrec.params.set("T5679" + bprdIO.getAuthCode());
			fatalError600();
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(t5679List.get(0).getGenarea()));
	}

	protected void loadT5687() {
		if (t5687ListMap == null || t5687ListMap.size() == 0) {
			syserrrec.params.set("TH510");
			fatalError600();
		}
		if (t5687ListMap.size() > wsaaT5687Size) {
			syserrrec.params.set(t5687);
			syserrrec.statuz.set(h791);
			fatalError600();
		}
		wsaaT5687Ix = 1;
		for (List<Itempf> items : t5687ListMap.values()) {
			for (Itempf item : items) {
				if (item.getItmfrm().compareTo(varcom.vrcmMaxDate.getbigdata()) <= 0) {
					t5687rec.t5687Rec.set(StringUtil.rawToString(item.getGenarea()));
					wsaaT5687Itmfrm[wsaaT5687Ix].set(item.getItmfrm());
					wsaaT5687Crtable[wsaaT5687Ix].set(item.getItemitem());
					wsaaT5687Annvry[wsaaT5687Ix].set(t5687rec.anniversaryMethod);
					wsaaT5687Ix++;
				}

			}

		}

	}

	protected void loadT6658() {
		if (t6658ListMap == null || t6658ListMap.size() == 0) {
			syserrrec.params.set(t6658);
			fatalError600();
		}
		if (t6658ListMap.size() > wsaaT6658Size) {
			syserrrec.params.set(t6658);
			syserrrec.statuz.set(h791);
			fatalError600();
		}
		wsaaT6658Ix = 1;
		for (List<Itempf> items : t5687ListMap.values()) {
			for (Itempf item : items) {
				if (item.getItmfrm().compareTo(varcom.vrcmMaxDate.getbigdata()) <= 0) {
					t6658rec.t6658Rec.set(StringUtil.rawToString(item.getGenarea()));
					wsaaT6658Itmfrm[wsaaT6658Ix].set(item.getItmfrm());
					wsaaT6658Annvry[wsaaT6658Ix].set(item.getItemitem());
					wsaaT6658Subprog[wsaaT6658Ix].set(t6658rec.subprog);
					wsaaT6658Ix++;
				}
			}

		}
	}

	protected void loadT5540() {
		if (t5540ListMap == null || t5540ListMap.size() == 0) {
			syserrrec.params.set(t5540);
			fatalError600();
		}
		if (t5540ListMap.size() > wsaaT5540Size) {
			syserrrec.params.set(t5540);
			syserrrec.statuz.set(h791);
			fatalError600();
		}
		wsaaT5540Ix = 1;
		for (List<Itempf> items : t5540ListMap.values()) {
			for (Itempf item : items) {
				if (item.getItmfrm().compareTo(varcom.vrcmMaxDate.getbigdata()) <= 0) {
					t5540rec.t5540Rec.set(StringUtil.rawToString(item
							.getGenarea()));
					wsaaT5540Itmfrm[wsaaT5540Ix].set(item.getItmfrm());
					wsaaT5540Crtable[wsaaT5540Ix].set(item.getItemitem());
					wsaaT5540Iudiscbas[wsaaT5540Ix].set(t5540rec.iuDiscBasis);
					wsaaT5540Ix++;
				}
			}
		}
	}

	protected void loadT5519() {
		wsaaT5519Ix = 1;
		if (t5519ListMap == null || t5519ListMap.size() == 0) {
			syserrrec.params.set(t5519);
			fatalError600();
		}
		if (t5519ListMap.size() > wsaaT5519Size) {
			syserrrec.params.set(t5519);
			syserrrec.statuz.set(h791);
			fatalError600();
		}
		for (List<Itempf> items : t5519ListMap.values()) {
			for (Itempf item : items) {
				if (item.getItmfrm().compareTo(varcom.vrcmMaxDate.getbigdata()) <= 0) {
					t5519rec.t5519Rec.set(StringUtil.rawToString(item.getGenarea()));
					wsaaT5519Itmfrm[wsaaT5519Ix].set(item.getItmfrm());
					wsaaT5519Iudiscbas[wsaaT5519Ix].set(item.getItemitem());
					wsaaT5519Iufreq[wsaaT5519Ix].set(t5519rec.initUnitChargeFreq);
					wsaaT5519Annchg[wsaaT5519Ix].set(t5519rec.annchg);
					wsaaT5519Ix++;
				}
			}
		}
	}

protected void readFile2000()
	{
		readFile2010();
	}

	protected void readFile2010() {

		if (iterator.hasNext()) {
			avyxpf = iterator.next();
		} else {
			wsspEdterror.set(varcom.endp);
			return;
		}
		/* No of COVR records read */

		ctrCT01++;
		/* Set up the key for the SYSR- copybook, should a system error */
		/* for this record occur. */
		wsysSystemErrorParamsInner.wsysChdrcoy.set(avyxpf.getChdrcoy());
		wsysSystemErrorParamsInner.wsysChdrnum.set(avyxpf.getChdrnum());
		wsysSystemErrorParamsInner.wsysLife.set(avyxpf.getLife());
		wsysSystemErrorParamsInner.wsysCoverage.set(avyxpf.getCoverage());
		wsysSystemErrorParamsInner.wsysRider.set(avyxpf.getRider());
		wsysSystemErrorParamsInner.wsysPlnsfx.set(avyxpf.getPlnsfx());
		wsysSystemErrorParamsInner.wsysCrtable.set(avyxpf.getCrtable());
		wsysSystemErrorParamsInner.wsysCrrcd.set(avyxpf.getCrrcd());
		wsysSystemErrorParamsInner.wsysStatcode.set(avyxpf.getStatcode());
	}

protected void edit2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					read2510();
				case callSoftlock2570:
					callSoftlock2570();
				case exit2590:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read2510()
	{
		wsspEdterror.set(varcom.oK);
		validateComp2510();
		if (!validComp.isTrue()) {
			/*  No of COVRs with invalid status*/

			ctrCT02++;
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit2590);
		}
		searchT56872520();
		if (updateCovrOnly.isTrue()) {
			goTo(GotoLabel.callSoftlock2570);
		}
		searchT66582530();
		if (updateCovrOnly.isTrue()) {
			goTo(GotoLabel.callSoftlock2570);
		}
		searchT55402540();
		if (!t5540Found.isTrue()) {
			goTo(GotoLabel.callSoftlock2570);
		}
		searchT55192550();
	}

protected void callSoftlock2570()
	{
		softlock2580();
		/* No of COVRs locked*/
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			ctrCT04++;
			wsspEdterror.set(SPACES);
		}
	}

protected void validateComp2510()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					para2511();
				case validateRider2515:
					validateRider2515();
				case exit2519:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2511()
	{
		/* Validate Coverage/Rider status against T5679.*/
		wsaaValidComp.set("N");
		if (isNE(avyxpf.getRider(), "00")
		&& isNE(avyxpf.getRider(), SPACES)) {
			goTo(GotoLabel.validateRider2515);
		}
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)
		|| validComp.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], avyxpf.getStatcode())) {
				wsaaValidComp.set("Y");
			}
		}
		goTo(GotoLabel.exit2519);
	}

protected void validateRider2515()
	{
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)
		|| validComp.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.ridRiskStat[wsaaT5679Sub.toInt()], avyxpf.getStatcode())) {
				wsaaValidComp.set("Y");
			}
		}
	}

	protected void searchT56872520() {
		wsaaUpdateCovrOnly.set(SPACES);
		wsaaT5687Ix = 1;
		searchlabel1: {
			for (; isLT(wsaaT5687Ix, wsaaT5687Rec.length); wsaaT5687Ix++) {
				if (isEQ(wsaaT5687Key[wsaaT5687Ix], avyxpf.getCrtable())) {
					break searchlabel1;
				}
			}

			fatalError600();
		}
		wsaaDateFound.set("N");
		while (!(isNE(avyxpf.getCrtable(), wsaaT5687Crtable[wsaaT5687Ix])
				|| isGT(wsaaT5687Ix, wsaaT5687Size) || dateFound.isTrue())) {
			if (isGTE(bsscIO.getEffectiveDate(), wsaaT5687Itmfrm[wsaaT5687Ix])) {
				wsaaDateFound.set("Y");
				wsaaAnnvry.set(wsaaT5687Annvry[wsaaT5687Ix]);
			} else {
				wsaaT5687Ix++;
			}
		}

		if (!dateFound.isTrue()) {

			fatalError600();
		}
		if (isEQ(wsaaAnnvry, SPACES)) {
			wsaaUpdateCovrOnly.set("Y");
		}
	}

	protected void searchT66582530() {
		wsaaUpdateCovrOnly.set(SPACES);
		wsaaT6658Ix = 1;
		searchlabel1: {
			for (; isLT(wsaaT6658Ix, wsaaT6658Rec.length); wsaaT6658Ix++) {
				if (isEQ(wsaaT6658Key[wsaaT6658Ix], wsaaAnnvry)) {
					break searchlabel1;
				}
			}
			wsaaUpdateCovrOnly.set("Y");
			return;
		}
		wsaaDateFound.set("N");
		while (!(isNE(wsaaAnnvry, wsaaT6658Annvry[wsaaT6658Ix])
				|| isGT(wsaaT6658Ix, wsaaT6658Size) || dateFound.isTrue())) {
			if (isGTE(bsscIO.getEffectiveDate(), wsaaT6658Itmfrm[wsaaT6658Ix])) {
				wsaaDateFound.set("Y");
				wsaaSubprog.set(wsaaT6658Subprog[wsaaT6658Ix]);
			} else {
				wsaaT6658Ix++;
			}
		}

		if (!dateFound.isTrue() || isEQ(wsaaSubprog, SPACES)) {
			wsaaUpdateCovrOnly.set("Y");
		}
	}

	protected void searchT55402540() {
		wsaaT5540Found.set(SPACES);
		wsaaT5540Ix = 1;
		searchlabel1: {
			for (; isLT(wsaaT5540Ix, wsaaT5540Rec.length); wsaaT5540Ix++) {
				if (isEQ(wsaaT5540Key[wsaaT5540Ix], avyxpf.getCrtable())) {
					break searchlabel1;
				}
			}
			return;
		}
		wsaaDateFound.set("N");
		while (!(isNE(avyxpf.getCrtable(), wsaaT5540Crtable[wsaaT5540Ix])
				|| isGT(wsaaT5540Ix, wsaaT5540Size) || dateFound.isTrue())) {
			if (isGTE(bsscIO.getEffectiveDate(), wsaaT5540Itmfrm[wsaaT5540Ix])) {
				wsaaDateFound.set("Y");
				wsaaT5540Found.set("Y");
				wsaaIudiscbas.set(wsaaT5540Iudiscbas[wsaaT5540Ix]);
			} else {
				wsaaT5540Ix++;
			}
		}

	}

	protected void searchT55192550() {
		wsaaT5519Ix = 1;
		searchlabel1: {
			for (; isLT(wsaaT5519Ix, wsaaT5519Rec.length); wsaaT5519Ix++) {
				if (isEQ(wsaaT5519Key[wsaaT5519Ix], wsaaIudiscbas)) {
					break searchlabel1;
				}
			}

			fatalError600();
		}
		wsaaDateFound.set("N");
		while (!(isNE(wsaaIudiscbas, wsaaT5519Iudiscbas[wsaaT5519Ix])|| isGT(wsaaT5519Ix, wsaaT5519Size) || dateFound.isTrue())) {
			if (isGTE(bsscIO.getEffectiveDate(), wsaaT5519Itmfrm[wsaaT5519Ix])) {
				wsaaDateFound.set("Y");
			} else {
				wsaaT5519Ix++;
			}
		}

		if (!dateFound.isTrue()) {
			fatalError600();
		}
	}

	protected void softlock2580() {
		/* Soft lock the contract, if it is to be processed. */
		sftlockrec.function.set("LOCK");
		sftlockrec.enttyp.set("CH");
		sftlockrec.company.set(avyxpf.getChdrcoy());
		sftlockrec.entity.set(avyxpf.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
				&& isNE(sftlockrec.statuz, "LOCK")) {
			wsysSystemErrorParamsInner.wsysSysparams.set(sftlockrec.sftlockRec);
			syserrrec.params
					.set(wsysSystemErrorParamsInner.wsysSystemErrorParams);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	protected void update3000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					update3010();
				case updateCovr3050:
					updateCovr3050();
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void update3010() {
		covrpf = avyxpf.getCovrpf();
		if(covrpf == null){
			fatalError600();
		}
		if (updateCovrOnly.isTrue()) {
			goTo(GotoLabel.updateCovr3050);
		}
		initialize(annprocrec.annpllRec);
		annprocrec.company.set(avyxpf.getChdrcoy());
		annprocrec.chdrnum.set(avyxpf.getChdrnum());
		annprocrec.life.set(avyxpf.getLife());
		annprocrec.coverage.set(avyxpf.getCoverage());
		annprocrec.rider.set(avyxpf.getRider());
		annprocrec.planSuffix.set(avyxpf.getPlnsfx());
		annprocrec.effdate.set(bsscIO.getEffectiveDate());
		annprocrec.batctrcde.set(bprdIO.getAuthCode());
		annprocrec.crdate.set(avyxpf.getCrrcd());
		annprocrec.crtable.set(avyxpf.getCrtable());
		annprocrec.user.set(varcom.vrcmUser);
		annprocrec.batcactyr.set(batcdorrec.actyear);
		annprocrec.batcactmn.set(batcdorrec.actmonth);
		annprocrec.batccoy.set(batcdorrec.company);
		annprocrec.batcbrn.set(batcdorrec.branch);
		annprocrec.batcbatch.set(batcdorrec.batch);
		annprocrec.batcpfx.set(batcdorrec.prefix);
		callProgram(wsaaSubprog, annprocrec.annpllRec);
		if (isNE(annprocrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(annprocrec.statuz);
			syserrrec.params.set(annprocrec.annpllRec);
			fatalError600();
		}
		/* Statictic *********************** */
		initialize(lifsttrrec.lifsttrRec);
		lifsttrrec.batccoy.set(batcdorrec.company);
		lifsttrrec.batcbrn.set(batcdorrec.branch);
		lifsttrrec.batcactyr.set(batcdorrec.actyear);
		lifsttrrec.batcactmn.set(batcdorrec.actmonth);
		lifsttrrec.batctrcde.set(bprdIO.getAuthCode());
		lifsttrrec.batcbatch.set(batcdorrec.batch);
		lifsttrrec.chdrcoy.set(avyxpf.getChdrcoy());
		lifsttrrec.chdrnum.set(avyxpf.getChdrnum());
		lifsttrrec.tranno.set(covrpf.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			fatalError600();
		}
	}
protected void updateCovr3050()
	{
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		datcon2rec.intDate1.set(covrpf.getAnnivProcDate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}

		covrpf.setAnnivProcDate(datcon2rec.intDate2.toInt());
		covrpfInsertList.add(covrpf);

		/* Undone soft lock.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(avyxpf.getChdrcoy());
		sftlockrec.entity.set(avyxpf.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
	}

	protected void commit3500() {
		if (covrpfInsertList != null && covrpfInsertList.size() > 0) {
			covrpfDAO.updateChunkRecordToCovr(covrpfInsertList);
			contotrec.totno.set(ct05);
			contotrec.totval.set(covrpfInsertList.size());
			callContot001();
			covrpfInsertList.clear();
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(ctrCT01);
		callContot001();
		ctrCT01 = 0;
		
		contotrec.totno.set(ct02);
		contotrec.totval.set(ctrCT02);
		callContot001();
		ctrCT02 = 0;
		
		contotrec.totno.set(ct04);
		contotrec.totval.set(ctrCT04);
		callContot001();
		ctrCT04 = 0;
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

	protected void close4000() {

		lsaaStatuz.set(varcom.oK);
		if (t5679List != null) {
			t5679List.clear();
		}
		t5679List = null;
		
		if (avyxList != null) {
			avyxList.clear();
		}
		avyxList = null;
		
		if (t5687ListMap != null) {
			t5687ListMap.clear();
		}
		t5687ListMap = null;
		
		if (t6658ListMap != null) {
			t6658ListMap.clear();
		}
		t6658ListMap  = null;
		
		if (t5540ListMap != null) {
			t5540ListMap.clear();
		}
		t5540ListMap = null;
		
		if (t5519ListMap != null) {
			t5519ListMap.clear();
		}
		t5519ListMap = null;
		
		if (t5687ListMap != null) {
			t5687ListMap.clear();
		}
		t5687ListMap  = null;
		/* EXIT */
	}
	protected String formatThreadName(int threadNo){
		String threadName = "";
		if(threadNo >= 10 && threadNo < 100 ){
			threadName =  "0" + String.valueOf(threadNo);
		}else if(threadNo >= 0 && threadNo < 10 ){
			threadName =  "00" + String.valueOf(threadNo);
		}else{
			threadName = String.valueOf(threadNo);
		}
		return threadName;
	}
/*
 * Class transformed  from Data Structure WSYS-SYSTEM-ERROR-PARAMS--INNER
 */
private static final class WsysSystemErrorParamsInner {

	private FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(100);
	private FixedLengthStringData wsysSysparams = new FixedLengthStringData(100).isAPartOf(wsysSystemErrorParams, 0);
	private FixedLengthStringData wsysAvyxData = new FixedLengthStringData(33).isAPartOf(wsysSysparams, 0, REDEFINE);
	private FixedLengthStringData wsysChdrcoy = new FixedLengthStringData(1).isAPartOf(wsysAvyxData, 0);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysAvyxData, 1);
	private FixedLengthStringData wsysLife = new FixedLengthStringData(2).isAPartOf(wsysAvyxData, 9);
	private FixedLengthStringData wsysCoverage = new FixedLengthStringData(2).isAPartOf(wsysAvyxData, 11);
	private FixedLengthStringData wsysRider = new FixedLengthStringData(2).isAPartOf(wsysAvyxData, 13);
	private ZonedDecimalData wsysPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsysAvyxData, 15).setUnsigned();
	private FixedLengthStringData wsysCrtable = new FixedLengthStringData(4).isAPartOf(wsysAvyxData, 19);
	private ZonedDecimalData wsysCrrcd = new ZonedDecimalData(8, 0).isAPartOf(wsysAvyxData, 23).setUnsigned();
	private FixedLengthStringData wsysStatcode = new FixedLengthStringData(2).isAPartOf(wsysAvyxData, 31);
}
}
