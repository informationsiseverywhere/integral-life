package com.csc.life.regularprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: TrwppfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:37
 * Class transformed from TRWPPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class TrwppfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 88;
	public FixedLengthStringData trwprec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData trwppfRecord = trwprec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(trwprec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(trwprec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(trwprec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(trwprec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(trwprec);
	public FixedLengthStringData rdocpfx = DD.rdocpfx.copy().isAPartOf(trwprec);
	public FixedLengthStringData rdoccoy = DD.rdoccoy.copy().isAPartOf(trwprec);
	public FixedLengthStringData rdocnum = DD.rdocnum.copy().isAPartOf(trwprec);
	public FixedLengthStringData origcurr = DD.origcurr.copy().isAPartOf(trwprec);
	public PackedDecimalData origamt = DD.origamt.copy().isAPartOf(trwprec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(trwprec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(trwprec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(trwprec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public TrwppfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for TrwppfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public TrwppfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for TrwppfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public TrwppfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for TrwppfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public TrwppfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("TRWPPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"EFFDATE, " +
							"TRANNO, " +
							"VALIDFLAG, " +
							"RDOCPFX, " +
							"RDOCCOY, " +
							"RDOCNUM, " +
							"ORIGCURR, " +
							"ORIGAMT, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     effdate,
                                     tranno,
                                     validflag,
                                     rdocpfx,
                                     rdoccoy,
                                     rdocnum,
                                     origcurr,
                                     origamt,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		effdate.clear();
  		tranno.clear();
  		validflag.clear();
  		rdocpfx.clear();
  		rdoccoy.clear();
  		rdocnum.clear();
  		origcurr.clear();
  		origamt.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getTrwprec() {
  		return trwprec;
	}

	public FixedLengthStringData getTrwppfRecord() {
  		return trwppfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setTrwprec(what);
	}

	public void setTrwprec(Object what) {
  		this.trwprec.set(what);
	}

	public void setTrwppfRecord(Object what) {
  		this.trwppfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(trwprec.getLength());
		result.set(trwprec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}