/* ********************  	*/
/*Author  :tsaxena3			*/
/*Purpose :Model for Crtloan*/
/*Date    :2018.10.04	 	*/
package com.csc.life.regularprocessing.procedures;


public class CrtloanPojo {
	
	private String function;
	private String statuz;
	private String chdrcoy;
	private String chdrnum;
	private int tranno;
	private int loanno;
	private String cnttype;
	private String cntcurr;
	private String billcurr;
	private int effdate;
	private int occdate;
	private String authCode;
	private String language;
	private int outstamt;
	private int cbillamt;
	private String batchkey;
	private String contkey;
	private String prefix;
	private String company;
	private String branch;
	private int actyear;
	private int actmonth;
	private String trcde;
	private String batch;
	private String filler;
	private String sacscode01;
	private String sacscode02;
	private String sacscode03;
	private String sacscode04;
	private String sacstyp01;
	private String sacstyp02;
	private String sacstyp03;
	private String sacstyp04;
	private String glcode01;
	private String glcode02;
	private String glcode03;
	private String glcode04;
	private String glsign01;
	private String glsign02;
	private String glsign03;
	private String glsign04;
	private String longdesc;
	private String loantype;
	
	
	public CrtloanPojo() {
		function = "";
		statuz = "";
		chdrcoy = "";
		chdrnum = "";
		tranno = 0;
		loanno = 0;
		cnttype = "";
		cntcurr = "";
		billcurr = "";
		effdate = 0;
		occdate = 0;
		authCode = "";
		language = "";
		outstamt = 0;
		cbillamt = 0;
		batchkey = "";
		contkey = "";
		prefix = "";
		company = "";
		branch = "";
		actyear = 0;
		actmonth = 0;
		trcde = "";
		batch = "";
		filler = "";
		sacscode01 = "";
		sacscode02 = "";
		sacscode03 = "";
		sacscode04 = "";
		sacstyp01 = "";
		sacstyp02 = "";
		sacstyp03 = "";
		sacstyp04 = "";
		glcode01 = "";
		glcode02 = "";
		glcode03 = "";
		glcode04 = "";
		glsign01 = "";
		glsign02 = "";
		glsign03 = "";
		glsign04 = "";
		longdesc = "";
		loantype = "";
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	
	public String getStatuz(){
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz=statuz;
	}
	
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy=chdrcoy;
	}
	
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum=chdrnum;
	}
	
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno=tranno;
	}
	
	public int getLoanno() {
		return loanno;
	}
	public void setLoanno(int loanno) {
		this.loanno=loanno;
	}
	
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype=cnttype;
	}
	
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr=cntcurr;
	}
	
	public String getBillcurr() {
		return billcurr;
	}
	public void setBillcurr(String billcurr) {
		this.billcurr=billcurr;
	}
	
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate=effdate;
	}
	
	public int getOccdate() {
		return occdate;
	}
	public void setOccdate(int occdate) {
		this.occdate=occdate;
	}
	
	public String getAuthCode() {
		return authCode;
	} 
	public void setAuthCode(String authCode) {
		this.authCode=authCode;
	}
	
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language=language;
	}
	
	public int getOutstamt() {
		return outstamt;
	}
	public void setOutstamt(int outstamt) {
		this.outstamt=outstamt;
	}
	
	public int getCbillamt() {
		return cbillamt;
	}
	public void setCbillamt(int cbillamt) {
		this.cbillamt=cbillamt;
	}
	
	public String getBatchkey() {
		return batchkey;
	}
	public void setBatchkey(String batchkey) {
		this.batchkey=batchkey;
	}
	
	public String getContkey() {
		return contkey;
	}
	public void setContkey(String contkey) {
		this.contkey=contkey;
	}
	
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix=prefix;
	}
	
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company=company;
	}
	
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch=branch;
	}
	
	public int getActyear() {
		return actyear;
	}
	public void setActyear(int actyear) {
		this.actyear=actyear;
	}
	
	public int getActmonth() {
		return actmonth;
	}
	public void setActmonth(int actmonth) {
		this.actmonth=actmonth;
	}
	
	public String getTrcde() {
		return trcde;
	}
	public void setTrcde(String trcde) {
		this.trcde=trcde;
	}
	
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch=batch;
	}
	
	public String getFiller() {
		return filler;
	}
	public void setFiller(String filler) {
		this.filler=filler;
	}
	
	public String getSacscode01() {
		return sacscode01;
	}
	public void setSacscode01(String sacscode01) {
		this.sacscode01=sacscode01;
	}
	
	public String getSacscode02() {
		return sacscode02;
	}
	public void setSacscode02(String sacscode02) {
		this.sacscode02=sacscode02;
	}
	
	public String getSacscode03() {
		return sacscode03;
	}
	public void setSacscode03(String sacscode03) {
		this.sacscode03=sacscode03;
	}
	
	public String getSacscode04() {
		return sacscode04;
	}
	public void setSacscode04(String sacscode04) {
		this.sacscode04=sacscode04;
	}
	
	public String getSacstyp01() {
		return sacstyp01;
	}
	public void setSacstyp01(String sacstyp01) {
		this.sacstyp01=sacstyp01;
	}
	
	public String getSacstyp02() {
		return sacstyp02;
	}
	public void setSacstyp02(String sacstyp02) {
		this.sacstyp02=sacstyp02;
	}
	
	public String getSacstyp03() {
		return sacstyp03;
	}
	public void setSacstyp03(String sacstyp03) {
		this.sacstyp03=sacstyp03;
	}
	
	public String getSacstyp04() {
		return sacstyp04;
	}
	public void setSacstyp04(String sacstyp04) {
		this.sacstyp04=sacstyp04;
	}
	
	public String getGlcode01() {
		return glcode01;
	}
	public void setGlcode01(String glcode01) {
		this.glcode01=glcode01;
	}
	
	public String getGlcode02() {
		return glcode02;
	}
	public void setGlcode02(String glcode02) {
		this.glcode02=glcode02;
	}
	
	public String getGlcode03() {
		return glcode03;
	}
	public void setGlcode03(String glcode03) {
		this.glcode03=glcode03;
	}
	
	public String getGlcode04() {
		return glcode04;
	}
	public void setGlcode04(String glcode04) {
		this.glcode04=glcode04;
	}
	
	public String getGlsign01() {
		return glsign01;
	}
	public void setGlsign01(String glsign01) {
		this.glsign01=glsign01;
	}
	
	public String getGlsign02() {
		return glsign02;
	}
	public void setGlsign02(String glsign02) {
		this.glsign02=glsign02;
	}
	
	public String getGlsign03() {
		return glsign03;
	}
	public void setGlsign03(String glsign03) {
		this.glsign03=glsign03;
	}
	
	public String getGlsign04() {
		return glsign04;
	}
	public void setGlsign04(String glsign04) {
		this.glsign04=glsign04;
	}
	
	public String getLongdesc() {
		return longdesc;
	}
	public void setLongdesc(String longdesc) {
		this.longdesc=longdesc;
	}
	
	public String getLoantype() {
		return loantype;
	}
	public void setLoantype(String loantype) {
		this.loantype=loantype;
	}
	
	@Override
	public String toString() {
		return "CrtloanPojo [function=" + function + ", statuz=" + statuz + ", chdrcoy=" + chdrcoy + ", chdrnum=" + chdrnum + ", tranno=" + tranno + ", loanno=" + loanno + ", cnttype=" + cnttype + ", cntcurr=" + cntcurr + ", billcurr=" + billcurr + ", effdate=" + effdate  + ", occdate=" + occdate + ", authCode=" + authCode + ", language=" + language + ", outstamt=" + outstamt + ", cbillamt=" + cbillamt + ", batchkey=" + batchkey + ", contkey=" + contkey + ", prefix=" + prefix + ", company=" + company  + ", branch=" + branch + ", actyear=" + actyear + ", actmonth=" + actmonth + ", trcde=" + trcde + ", batch=" + batch + ", filler=" + filler + ", sacscode01=" + sacscode01 + ", sacscode02=" + sacscode02 + ", sacscode03=" + sacscode03  + ", sacscode04=" + sacscode04 + ", sacstyp01=" + sacstyp01 + ", sacstyp02=" + sacstyp02 + ", sacstyp03=" + sacstyp03 + ", sacstyp04=" + sacstyp04 + ", glcode01=" + glcode01 + ", glcode02=" + glcode02 + ", glcode03=" + glcode03 + ", glcode04=" + glcode04 + ", glsign01=" + glsign01 + ", glsign02=" + glsign02 + ", glsign03=" + glsign03 + ", glsign04=" + glsign04 + ", longdesc=" + longdesc + ", loantype=" + loantype + "]";
	}
}