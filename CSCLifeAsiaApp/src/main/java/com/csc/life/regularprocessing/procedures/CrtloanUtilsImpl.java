/* ********************  */
/*Author  :tsaxena3				  		*/
/*Purpose :Instead of subroutine Crtloan*/
/*Date    :2018.10.04				*/
package com.csc.life.regularprocessing.procedures;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.csc.fsu.general.procedures.Datcon2Pojo;
import com.csc.fsu.general.procedures.Datcon2Utils;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.ZrdecplcPojo;
import com.csc.fsu.general.procedures.ZrdecplcUtils;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.integral.context.IntegralApplicationContext;
import com.csc.life.anticipatedendowment.dataaccess.dao.LoanpfDAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Loanpf;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.productdefinition.procedures.LifacmvPojo;
import com.csc.life.productdefinition.procedures.LifacmvUtils;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class CrtloanUtilsImpl implements CrtloanUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CrtloanUtilsImpl.class);
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("CRTLOAN");
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaT6633Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6633Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6633Key, 0);
	private FixedLengthStringData wsaaT6633Type = new FixedLengthStringData(1).isAPartOf(wsaaT6633Key, 3);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaRldgLoanno = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
		/* WSAA-C-DATE */
	private ZonedDecimalData wsaaContractDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaCtrtDate = new FixedLengthStringData(8).isAPartOf(wsaaContractDate, 0, REDEFINE);
	private FixedLengthStringData wsaaContractMd = new FixedLengthStringData(4).isAPartOf(wsaaCtrtDate, 4);
		/* WSAA-L-DATE */
	private ZonedDecimalData wsaaLoanDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaLndt = new FixedLengthStringData(8).isAPartOf(wsaaLoanDate, 0, REDEFINE);
	private ZonedDecimalData wsaaLoanYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaLndt, 0).setUnsigned();
	private FixedLengthStringData wsaaLoanMd = new FixedLengthStringData(4).isAPartOf(wsaaLndt, 4);
	private ZonedDecimalData wsaaLoanMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 0).setUnsigned();
	private ZonedDecimalData wsaaLoanDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 2).setUnsigned();
		/* WSAA-N-DATE */
	private ZonedDecimalData wsaaNewDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNdate = new FixedLengthStringData(8).isAPartOf(wsaaNewDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNewYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNdate, 0).setUnsigned();
	private FixedLengthStringData wsaaNewMd = new FixedLengthStringData(4).isAPartOf(wsaaNdate, 4);
	private ZonedDecimalData wsaaNewMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 0).setUnsigned();
	private ZonedDecimalData wsaaNewDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 2).setUnsigned();

	private ZonedDecimalData wsaaDayCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator daysLessThan28 = new Validator(wsaaDayCheck, new ValueRange("00","28"));
	private Validator daysInJan = new Validator(wsaaDayCheck, "31");
	//private Validator daysInFeb = new Validator(wsaaDayCheck, "28");
//	private Validator daysInApr = new Validator(wsaaDayCheck, "30");

	private PackedDecimalData wsaaResult = new PackedDecimalData(5, 0);
	private String wsaaLeapYear = "";
	private PackedDecimalData wsaaIntYear = new PackedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData wsaaCenturys = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaCenturyx = new FixedLengthStringData(4).isAPartOf(wsaaCenturys, 0, REDEFINE);
	private ZonedDecimalData wsaaCenturyYear = new ZonedDecimalData(2, 0).isAPartOf(wsaaCenturyx, 2).setUnsigned();
	private static final int wsaaLeapFebruaryDays = 29;
	private static final int wsaaFebruaryDays = 28;
	private static final int wsaaAprilDays = 30;
	
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private LoanpfDAO loanpfDao = getApplicationContext().getBean("loanpfDAO",LoanpfDAO.class);

	private T6633rec t6633rec = new T6633rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Syserr syserr = new Syserr();
	private Datcon2Utils datcon2Utils  = getApplicationContext().getBean("datcon2Utils", Datcon2Utils.class);
	private Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
	private Varcom varcom = new Varcom();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Xcvrt xcvrt = new Xcvrt();
	private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
	private ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
	private CrtloanPojo crtloanPojo = new CrtloanPojo();
	Loanpf loanpf =new Loanpf();
	private WsaaMonthCheckInner wsaaMonthCheckInner = new WsaaMonthCheckInner();
	private LifacmvUtils lifacmvUtils = getApplicationContext().getBean("lifacmvUtils", LifacmvUtils.class);
	private LifacmvPojo lifacmvPojo = new LifacmvPojo();
	private List<Loanpf> loanpfList = new ArrayList<Loanpf>();
	
	public void calCrtloan(CrtloanPojo crtloanPojo) {
		
		crtloanPojo.setStatuz("****");
		wsaaTime.set(getCobolTime());
		/*    Read LOAN file to see if there are any existing LOANS for*/
		/*    this contract. If so, find last one and add 1 to its*/
		/*    loan number (LOANENQ-LOAN-NUMBER) to get the new loan*/
		/*    number (LOAN-LOAN-NUMBER). If not, set the new loan number*/
		/*    to 1.*/
		Loanpf loanpf1 = loanpfDao.getLoanpfRecord(crtloanPojo.getChdrcoy(), crtloanPojo.getChdrnum(), 99);
		if(loanpf1 == null){
			loanpf.setLoannumber(1);
		}else{
			loanpf.setLoannumber(add(1, loanpf1.getLoannumber()).toInt());
		}
		crtloanPojo.setLoanno(loanpf.getLoannumber());
	
		/*    Read T6633 to see whether there are any loan rules.*/
		wsaaT6633Cnttype.set(crtloanPojo.getCnttype());
		wsaaT6633Type.set(crtloanPojo.getLoantype());

		List<Itempf> itemList = itempfDAO.getItdmByFrmdate(crtloanPojo.getChdrcoy(),"T6633",wsaaT6633Key.toString(),crtloanPojo.getEffdate()); 
		if (itemList == null || itemList.isEmpty()){
			syserrrec.params.set(itemList.get(0).getItemitem());
			syserr570(crtloanPojo,"1");
		}
		t6633rec.t6633Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
		/*    We need to set the next interest billing date (NXTINTBDTE)*/
		/*    and the next capitalisation date (NXTCAPDATE).*/
		/*    Set these dates depending on what is set in the T6633 table.*/
		/*    Check the capitalisation details on T6633 in the following*/
		/*    order: i) Capitalise on Loan anniv ... Y/N*/
		/*          ii) Capitalise on Policy anniv.. Y/N*/
		/*         iii) Check frequency and fixed date.*/
		wsaaLoanDate.set(crtloanPojo.getEffdate());
		wsaaContractDate.set(crtloanPojo.getOccdate());
		
		/*    Check if loan anniversary flag is set.*/
		if (isEQ(t6633rec.annloan, "Y")) {
			/*    Call Datcon2 to add 1 year on to loan start date.*/
			datcon2Pojo.setIntDate1(String.valueOf(crtloanPojo.getEffdate()));
			datcon2Pojo.setFrequency("01");
			datcon2Pojo.setFreqFactor(1);
			callDatcon27000();
			loanpf.setNxtcapdate(Integer.valueOf(datcon2Pojo.getIntDate1()));
			return ;
		}
		/*    Check if contract anniversary flag is set.*/
		if (isEQ(t6633rec.annpoly, "Y")
		&& isEQ(wsaaContractDate, wsaaLoanDate)) {
			/*    Contract start date = Loan start date*/
			/*    so add 1 year on to loan date.*/
			
			datcon2Pojo.setIntDate1(String.valueOf(crtloanPojo.getEffdate()));
			datcon2Pojo.setFrequency("01");
			datcon2Pojo.setFreqFactor(1);
			callDatcon27000();
			loanpf.setNxtcapdate(Integer.valueOf(datcon2Pojo.getIntDate2()));
			return ;

		}
		/*    Contract start date must be before loan date - cannot have*/
		/*    a loan starting before a contract !!!!!*/
		if (isEQ(t6633rec.annpoly, "Y")
		&& isGT(wsaaContractMd, wsaaLoanMd)) {
			/*    The month & day on the contract are later in the year*/
			/*    than the month & day on the loan therefore the first*/
			/*    capitalisation calculation date is on the contract*/
			/*    anniversary later the same year.*/
			/*    Example...*/
			/*        Contract start date..................... 19910303*/
			/*        Loan start date......................... 19920107*/
			/*        Next capitalisation calculation date ... 19920303*/
			wsaaNewMd.set(wsaaContractMd);
			wsaaNewYear.set(wsaaLoanYear);
			loanpf.setNxtcapdate(wsaaNewDate.toInt());
			return ;
		}
		if (isEQ(t6633rec.annpoly, "Y")
		&& isLTE(wsaaContractMd, wsaaLoanMd)) {
			/*    The month & day on the contract are earlier in the year*/
			/*    than the month & day on the loan, therefore the first*/
			/*    capitalisation calculation date is on the contract*/
			/*    anniversary in the following year.*/
			/*    Example...*/
			/*        Contract start date.................... 19910303*/
			/*        Loan start date........................ 19920406*/
			/*        Next capitalisation calculation date... 19930303*/
			wsaaNewMd.set(wsaaContractMd);
			wsaaNewYear.set(wsaaLoanYear);
			
			datcon2Pojo.setIntDate1(String.valueOf(crtloanPojo.getEffdate()));
			datcon2Pojo.setFrequency("01");
			datcon2Pojo.setFreqFactor(1);
			callDatcon27000();
			loanpf.setNxtcapdate(Integer.valueOf(datcon2Pojo.getIntDate2()));
			return ;
		}
		fixedCapitalisation3100();
		
		// third function ends
		
		
		//4th function starts
		
		/*    Check the interest  details on T6633 in the following*/
		/*    order: i) Calculate interest on Loan anniv ... Y/N*/
		/*          ii) Calculate interest on Policy anniv.. Y/N*/
		/*         iii) Check frequency and fixed date.*/
		/*    Check if loan anniversary flag is set.*/
		if (isEQ(t6633rec.loanAnnivInterest, "Y")) {
			/*    Call datcon2 to add 1 year on to loan start date*/
			
			datcon2Pojo.setIntDate1(String.valueOf(crtloanPojo.getEffdate()));
			datcon2Pojo.setFrequency("01");
			datcon2Pojo.setFreqFactor(1);
			callDatcon27000();
			loanpf.setNxtintbdte(Integer.valueOf(datcon2Pojo.getIntDate2()));
			return ;

		}
		/*    Check if contract anniversary flag is set.*/
		if (isEQ(t6633rec.policyAnnivInterest, "Y")
		&& isEQ(wsaaContractDate, wsaaLoanDate)) {
			/*    Contract start date = Loan start date*/
			/*    so add 1 year on to loan date.*/
			
			datcon2Pojo.setIntDate1(String.valueOf(crtloanPojo.getEffdate()));
			datcon2Pojo.setFrequency("01");
			datcon2Pojo.setFreqFactor(1);
			callDatcon27000();
			loanpf.setNxtintbdte(Integer.valueOf(datcon2Pojo.getIntDate2()));
			return ;
			
		}
		/*    Contract start date must be before loan date - cannot have*/
		/*    a loan starting before a contract !!!!!*/
		if (isEQ(t6633rec.policyAnnivInterest, "Y")
		&& isGT(wsaaContractMd, wsaaLoanMd)) {
			/*    The month & day on the contract are later in the year than*/
			/*    the month & day on the loan therefore the first interest*/
			/*    calculation date is on the contract anniversary later in*/
			/*    the same year.*/
			/*    Example...*/
			/*              Contract start date............... 19910303*/
			/*              Loan start date................... 19920107*/
			/*              Next interest calculation date ... 19920303*/
			wsaaNewMd.set(wsaaContractMd);
			wsaaNewYear.set(wsaaLoanYear);
			loanpf.setNxtintbdte(wsaaNewDate.toInt());
			return ;
		}
		if (isEQ(t6633rec.policyAnnivInterest, "Y")
		&& isLTE(wsaaContractMd, wsaaLoanMd)) {
			/*    The month & day on the contract are earlier in the year than*/
			/*    the month & day on the loan therefore the first interest*/
			/*    calculation date is on the contract anniversary in the*/
			/*    following year.*/
			/*    Example...*/
			/*              Contract start date.............. 19910303*/
			/*              Loan start date.................. 19920406*/
			/*              Next interest calculation date... 19930303*/
			wsaaNewMd.set(wsaaContractMd);
			wsaaNewYear.set(wsaaLoanYear);
			
			datcon2Pojo.setIntDate1(wsaaNewDate.toString());
			datcon2Pojo.setFrequency("01");
			datcon2Pojo.setFreqFactor(1);
			callDatcon27000();
			loanpf.setNxtintbdte(Integer.valueOf(datcon2Pojo.getIntDate2()));
			return ;

		}
		fixedInterest4100();

		loanpf.setTplstmdty((double)0);
		loanpf.setChdrcoy(crtloanPojo.getChdrcoy());
		loanpf.setChdrnum(crtloanPojo.getChdrnum());
		loanpf.setFtranno(crtloanPojo.getTranno());
		loanpf.setLtranno(Long.parseLong(ZERO.toString()));
		/* MOVE 'A'                    TO LOAN-LOAN-TYPE.               */
		loanpf.setLoantype(crtloanPojo.getLoantype());
		loanpf.setLoancurr(crtloanPojo.getCntcurr());
		loanpf.setLoanorigam(crtloanPojo.getOutstamt());
		loanpf.setLstcaplamt(crtloanPojo.getOutstamt());
		loanpf.setLoansdate(crtloanPojo.getEffdate());
		loanpf.setLstcapdate(crtloanPojo.getEffdate());
		loanpf.setLstintbdte(crtloanPojo.getEffdate());
		loanpf.setTrdt(crtloanPojo.getEffdate());
		loanpf.setTermid(varcom.vrcmTermid.toString());
		loanpf.setTrtm(wsaaTime.toInt());
		loanpf.setUser_t(0);
		loanpf.setValidflag("1");
		loanpfList.add(loanpf);
		loanpfDao.insertLoanRecords(loanpfList);
	
		if (!crtloanPojo.getFunction().equals("NPSTW")) {
			start6010(crtloanPojo);
			chkPstw6020(crtloanPojo);
		}
		
	}
	
	protected void start6010(CrtloanPojo crtloanPojo)
	{
		lifacmvPojo.setRdocnum(crtloanPojo.getChdrnum());
		lifacmvPojo.setJrnseq(0);
		lifacmvPojo.setBatccoy(crtloanPojo.getChdrcoy());
		lifacmvPojo.setRldgcoy(crtloanPojo.getChdrcoy());
		lifacmvPojo.setGenlcoy(crtloanPojo.getChdrcoy());
		lifacmvPojo.setBatckey(crtloanPojo.getBatchkey());
		/*    Set correct Transaction code on ACMV records*/
		lifacmvPojo.setBatctrcde(crtloanPojo.getAuthCode());
		lifacmvPojo.setGenlcur("");
		/* MOVE CRTL-CNTCURR           TO LIFA-ORIGCURR.                */
		/* MOVE CRTL-OUTSTAMT          TO LIFA-ORIGAMT.                 */
		lifacmvPojo.setRcamt(0);
		lifacmvPojo.setCrate(BigDecimal.ZERO);
		lifacmvPojo.setAcctamt(BigDecimal.ZERO);
		lifacmvPojo.setContot(0);
		lifacmvPojo.setTranno(crtloanPojo.getTranno());
		/* MOVE MAXDATE                TO LIFA-FRCDATE.                 */
		lifacmvPojo.setFrcdate(varcom.vrcmMaxDate.toInt());
		lifacmvPojo.setEffdate(crtloanPojo.getEffdate());
		lifacmvPojo.setTranref(crtloanPojo.getChdrnum());
		String[] substituteCodes = new String[6];
        substituteCodes[0] = crtloanPojo.getCnttype();
        lifacmvPojo.setSubstituteCodes(substituteCodes);
		lifacmvPojo.setTrandesc(crtloanPojo.getLongdesc());
		wsaaRldgacct.set(SPACES);
		wsaaRldgChdrnum.set(crtloanPojo.getChdrnum());
		/* For non APL case, we want identified the Loan/Deposits by    */
		/* supplying the Loan number in the Ledger account.             */
		/* For Prem deposit, loan suspense account is the premium suspense */
		/* account. Loan no. not required.                                 */
		if (isNE(loanpf.getLoantype(), "A")
		&& isNE(loanpf.getLoantype(), "D")) {
			wsaaRldgLoanno.set(loanpf.getLoannumber());
		}
		lifacmvPojo.setRldgacct(wsaaRldgacct.toString());
		lifacmvPojo.setTransactionDate(crtloanPojo.getEffdate());
		lifacmvPojo.setTransactionTime(wsaaTime.toInt());
		lifacmvPojo.setUser(0);
		lifacmvPojo.setTermid(varcom.vrcmTermid.toString());
		/* Convert Loan amount to Billing Currency if not the same         */
		/* as Loan Currency.                                               */
		if (isNE(crtloanPojo.getCntcurr(), crtloanPojo.getBillcurr())) {
			convertLoan6100(crtloanPojo);
			lifacmvPojo.setOrigcurr(crtloanPojo.getBillcurr());
			lifacmvPojo.setOrigamt(new BigDecimal(conlinkrec.amountOut.toString()));
		}
		else {
			lifacmvPojo.setOrigcurr(crtloanPojo.getCntcurr());
			lifacmvPojo.setOrigamt(new BigDecimal(String.valueOf(crtloanPojo.getOutstamt())));
		}
	}

	
	protected void convertLoan6100(CrtloanPojo crtloanPojo)
	{
		
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.company.set(crtloanPojo.getChdrcoy());
		conlinkrec.cashdate.set(crtloanPojo.getEffdate());
		conlinkrec.currIn.set(crtloanPojo.getCntcurr());
		conlinkrec.currOut.set(crtloanPojo.getBillcurr());
		conlinkrec.amountIn.set(crtloanPojo.getOutstamt());
		conlinkrec.function.set("SURR");
		xcvrt.mainline(new Object[] { conlinkrec.clnk002Rec });
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			syserr570(crtloanPojo,"2");
		}

		zrdecplcPojo.setAmountIn(conlinkrec.amountOut.getbigdata());
		a000CallRounding(crtloanPojo);
		conlinkrec.amountOut.set(zrdecplcPojo.getAmountOut());
	}
	
	protected void a000CallRounding(CrtloanPojo crtloanPojo)
	{
		/*A100-CALL*/
		zrdecplcPojo.setFunction(SPACES.toString());
		zrdecplcPojo.setCompany(crtloanPojo.getChdrcoy());
		zrdecplcPojo.setStatuz(varcom.oK.toString());
		zrdecplcPojo.setCurrency(crtloanPojo.getBillcurr());
		zrdecplcPojo.setBatctrcde(crtloanPojo.getAuthCode());
		
		zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
		
		if(!varcom.oK.toString().equals(zrdecplcPojo.getStatuz())) {
			syserrrec.statuz.set(zrdecplcPojo.getStatuz());
			syserrrec.params.set(zrdecplcPojo.toString());
			syserr570(crtloanPojo,"2");
		}	
		/*A900-EXIT*/
	}

protected void chkPstw6020(CrtloanPojo crtloanPojo)
	{
		if (isEQ(crtloanPojo.getFunction(), "PSTW")) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(crtloanPojo.getFunction(), "PSLN")) {
				postPrincipal6030(crtloanPojo);
			}
		}
		/*    Post to suspense the loan amount*/
		lifacmvPojo.setSacscode(crtloanPojo.getSacscode01());
		lifacmvPojo.setSacstyp(crtloanPojo.getSacstyp01());
		lifacmvPojo.setGlcode(crtloanPojo.getGlcode01());
		lifacmvPojo.setGlsign(crtloanPojo.getGlsign01());
		lifacmvPojo.setFunction("PSTW");
		lifacmvUtils.calcLifacmv(lifacmvPojo);
		if (isNE(lifacmvPojo.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifacmvPojo);
			syserrrec.statuz.set(lifacmvPojo.getStatuz());
			syserr570(crtloanPojo,"2");
		}
	}

protected void postPrincipal6030(CrtloanPojo crtloanPojo)
{
	wsaaRldgacct.set(SPACES);
	wsaaRldgChdrnum.set(crtloanPojo.getChdrnum());
	wsaaRldgLoanno.set(loanpf.getLoannumber());
	lifacmvPojo.setRldgacct(wsaaRldgacct.toString());
	lifacmvPojo.setSacscode(crtloanPojo.getSacscode02());
	lifacmvPojo.setSacstyp(crtloanPojo.getSacstyp02());
	lifacmvPojo.setGlcode(crtloanPojo.getGlcode02());
	lifacmvPojo.setGlsign(crtloanPojo.getGlsign02());
	lifacmvPojo.setOrigcurr(crtloanPojo.getCntcurr());
	lifacmvPojo.setOrigamt(new BigDecimal(String.valueOf(crtloanPojo.getOutstamt())));
	lifacmvPojo.setFunction("PSTW");
	lifacmvUtils.calcLifacmv(lifacmvPojo);
	if (isNE(lifacmvPojo.getStatuz(), varcom.oK)) {
		syserrrec.params.set(lifacmvPojo);
		syserrrec.statuz.set(lifacmvPojo.getStatuz());
		syserr570(crtloanPojo,"2");
	}
	/* Post to urrency conversion accounts if currencies differ.      */
	if (isEQ(crtloanPojo.getCntcurr(), crtloanPojo.getBillcurr())) {
		return ;
	}
	wsaaRldgacct.set(SPACES);
	wsaaRldgChdrnum.set(crtloanPojo.getChdrnum());
	lifacmvPojo.setRldgacct(wsaaRldgacct.toString());
	lifacmvPojo.setOrigcurr(crtloanPojo.getCntcurr());
	lifacmvPojo.setOrigamt(new BigDecimal(String.valueOf(crtloanPojo.getOutstamt())));
	lifacmvPojo.setGlcode(crtloanPojo.getGlcode03());
	lifacmvPojo.setSacscode(crtloanPojo.getSacscode03());
	lifacmvPojo.setSacstyp(crtloanPojo.getSacstyp03());
	lifacmvPojo.setGlsign(crtloanPojo.getGlsign03());
	lifacmvPojo.setJrnseq(lifacmvPojo.getJrnseq() + 1);
	lifacmvUtils.calcLifacmv(lifacmvPojo);
	if (isNE(lifacmvPojo.getStatuz(), varcom.oK)) {
		syserrrec.params.set(lifacmvPojo);
		syserrrec.statuz.set(lifacmvPojo.getStatuz());
		syserr570(crtloanPojo,"2");
	}
	lifacmvPojo.setOrigcurr(crtloanPojo.getBillcurr());
	lifacmvPojo.setOrigamt(new BigDecimal(conlinkrec.amountOut.toString()));
	lifacmvPojo.setGlcode(crtloanPojo.getGlcode04());
	lifacmvPojo.setSacscode(crtloanPojo.getSacscode04());
	lifacmvPojo.setSacstyp(crtloanPojo.getSacstyp04());
	lifacmvPojo.setGlsign(crtloanPojo.getGlsign04());
	lifacmvPojo.setJrnseq(lifacmvPojo.getJrnseq() + 1);
	lifacmvUtils.calcLifacmv(lifacmvPojo);
	if (isNE(lifacmvPojo.getStatuz(), varcom.oK)) {
		syserrrec.params.set(lifacmvPojo);
		syserrrec.statuz.set(lifacmvPojo.getStatuz());
		syserr570(crtloanPojo,"2");
	}
}

	
	protected void fixedInterest4100() {
		
		
		/*    Get here so the next interest calculation date is not*/
		/*    based on loan or contract anniversarys.*/
		wsaaNewDate.set(ZERO);
		/*    Check for a fixed date on T6633. If none is specified,*/
		/*    use the Loan day and month.*/
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaLoanMonth);
		wsaaIntYear.set(wsaaNewYear);
		if (isNE(t6633rec.interestDay, 0)) {
			wsaaDayCheck.set(t6633rec.interestDay);
			if (!daysLessThan28.isTrue()) {
				dateSet8000();
			}
			else {
				/*     END-IF                                                   */
				wsaaNewDay.set(t6633rec.interestDay);
			}
		}
		else {
			wsaaNewDay.set(wsaaLoanDay);
		}
		
		if (isEQ(wsaaNewDay,ZERO)) //MTL-350
		{
			wsaaNewDay.set(wsaaLoanDay);
		}
		
		wsaaNewYear.set(wsaaLoanYear);
		wsaaNewMonth.set(wsaaLoanMonth);
		
		datcon2Pojo.setIntDate1(wsaaNewDate.toString());
		datcon2Pojo.setFreqFactor(1);

		/*    Check for a fixed frequency on T6633 for interest*/
		/*    calculations. If none is specified, use 1 year as*/
		/*    the default value.*/
		if (isEQ(t6633rec.interestFrequency, SPACES)) {
			datcon2Pojo.setFrequency("01");
		}
		else {
			datcon2Pojo.setFrequency((t6633rec.interestFrequency).toString());
	
		}
		callDatcon27000();
		wsaaNewDate.set(datcon2Pojo.getIntDate2());
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaNewMonth);
		wsaaIntYear.set(wsaaNewYear);
		if (!daysLessThan28.isTrue()) {
			dateSet8000();
			loanpf.setNxtintbdte(wsaaNewDate.toInt());
		}
		else {
			loanpf.setNxtintbdte(Integer.valueOf(datcon2Pojo.getIntDate2()));
		}
	}
	
	protected void callDatcon27000()
	{
		/*START*/
		
		datcon2Utils.calDatcon2(datcon2Pojo);

		if(!varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
			syserrrec.statuz.set(datcon2Pojo.getStatuz());
			syserrrec.params.set(datcon2Pojo.toString());
			syserr570(crtloanPojo,"2");
		}	
		
		/*EXIT*/
	}
	
	protected void fixedCapitalisation3100() {
		
		/*    Get here so the next capitalisation date isn't based on loan*/
		/*    or contract anniversarys.*/
		wsaaNewDate.set(ZERO);
		/*    Check for a fixed date on T6633. If none is specified,*/
		/*    use the Loan day and month.*/
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaLoanMonth);
		wsaaIntYear.set(wsaaLoanYear);
		if (isNE(t6633rec.day, 0)) {
			wsaaDayCheck.set(t6633rec.day);
			if (!daysLessThan28.isTrue()) {
				dateSet8000();
			}
			else {
				/*     END-IF                                                   */
				wsaaNewDay.set(t6633rec.day);
			}
		}
		else {
			wsaaNewDay.set(wsaaLoanDay);
		}
		
		if (isEQ(wsaaNewDay,ZERO))  //MTL-350
		{
			wsaaNewDay.set(wsaaLoanDay);
		}
		

		wsaaNewYear.set(wsaaLoanYear);
		wsaaNewMonth.set(wsaaLoanMonth);
		
		datcon2Pojo.setIntDate1(wsaaNewDate.toString());
		datcon2Pojo.setFreqFactor(1);
		

		/*    Check for a fixed frequency on T6633 for capitalisation*/
		/*    calculations. If none is specified, use 1 year as the*/
		/*    default value.*/
		if (isEQ(t6633rec.compfreq, SPACES)) {
			datcon2Pojo.setFrequency("01");
		}
		else {
			datcon2Pojo.setFrequency(t6633rec.compfreq.toString());
		}
		callDatcon27000();
		wsaaNewDate.set(datcon2Pojo.getIntDate2());
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaNewMonth);
		wsaaIntYear.set(wsaaNewYear);
		if (!daysLessThan28.isTrue()) {
			dateSet8000();
			loanpf.setNxtcapdate(wsaaNewDate.toInt());
		}
		else {
			loanpf.setNxtcapdate(Integer.valueOf(datcon2Pojo.getIntDate2()));
		}
	}
	
	protected void dateSet8000() {
		
		/*    We have to check that the date we are going to call Datcon2*/
		/*    with is a valid from-date. If the interest/capn day in*/
		/*    T6633 is greater than the number of days in the from-date*/
		/*    month i.e. day = 30 and month = February, we want to set it*/
		/*    to the maximum day corresponding to that month i.e. for*/
		/*    February, use 28 days; for June, use 30 days.*/
		if (wsaaMonthCheckInner.january.isTrue()
		|| wsaaMonthCheckInner.march.isTrue()
		|| wsaaMonthCheckInner.may.isTrue()
		|| wsaaMonthCheckInner.july.isTrue()
		|| wsaaMonthCheckInner.august.isTrue()
		|| wsaaMonthCheckInner.october.isTrue()
		|| wsaaMonthCheckInner.december.isTrue()) {
			wsaaNewDay.set(wsaaDayCheck);
			return ;
		}
		if (wsaaMonthCheckInner.april.isTrue()
		|| wsaaMonthCheckInner.june.isTrue()
		|| wsaaMonthCheckInner.september.isTrue()
		|| wsaaMonthCheckInner.november.isTrue()) {
			if (daysInJan.isTrue()) {
				wsaaNewDay.set(wsaaAprilDays);
			}
			else {
				wsaaNewDay.set(wsaaDayCheck);
			}
			return ;
		}
		if (wsaaMonthCheckInner.february.isTrue()) {
			checkLeapYear5000();
			if (isEQ(wsaaLeapYear, "Y")) {
				wsaaNewDay.set(wsaaLeapFebruaryDays);
			}
			else {
				wsaaNewDay.set(wsaaFebruaryDays);
			}
		}
	}
	
	protected void checkLeapYear5000()
	{
		
		wsaaCenturys.set(wsaaIntYear);
		if (isEQ(wsaaCenturyYear, ZERO)) {
			compute(wsaaResult, 0).set(div(wsaaIntYear, 400));
			wsaaResult.multiply(400);
			return ;
		}
		compute(wsaaResult, 0).set(div(wsaaIntYear, 4));
		wsaaResult.multiply(4);
		
		if (isEQ(wsaaResult, wsaaIntYear)) {
			wsaaLeapYear = "Y";
		}
		else {
			wsaaLeapYear = "N";
		}
		/*EXIT1*/
	}
	
	
	protected void syserr570(CrtloanPojo crtloanPojo,String errtype){
		/*PARA*/
		syserrrec.subrname.set(wsaaProg);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set(errtype);
		syserr.mainline(new Object[] { syserrrec.syserrRec });
		/*EXIT*/
		crtloanPojo.setStatuz("BOMB");

		
		LOGGER.error("COBOLConvCodeModel.callProgram exception", new COBOLExitProgramException());
		throw new RuntimeException(new COBOLExitProgramException());
	}

	public ApplicationContext getApplicationContext() {
		return IntegralApplicationContext.getApplicationContext();
	}

	private static final class WsaaMonthCheckInner {

		private ZonedDecimalData wsaaMonthCheck = new ZonedDecimalData(2, 0).setUnsigned();
		private Validator january = new Validator(wsaaMonthCheck, "01");
		private Validator february = new Validator(wsaaMonthCheck, "02");
		private Validator march = new Validator(wsaaMonthCheck, "03");
		private Validator april = new Validator(wsaaMonthCheck, "04");
		private Validator may = new Validator(wsaaMonthCheck, "05");
		private Validator june = new Validator(wsaaMonthCheck, "06");
		private Validator july = new Validator(wsaaMonthCheck, "07");
		private Validator august = new Validator(wsaaMonthCheck, "08");
		private Validator september = new Validator(wsaaMonthCheck, "09");
		private Validator october = new Validator(wsaaMonthCheck, "10");
		private Validator november = new Validator(wsaaMonthCheck, "11");
		private Validator december = new Validator(wsaaMonthCheck, "12");
	}
}
