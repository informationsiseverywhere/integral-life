package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.CovxpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Covxpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class CovxpfDAOImpl extends BaseDAOImpl<Covxpf> implements CovxpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(CovxpfDAOImpl.class);
	public List<Covxpf> searchCovxRecord(String tableId, String memName, int batchExtractSize, int batchID) {

		StringBuilder sqlCovxSelect = new StringBuilder();
		sqlCovxSelect.append(" SELECT * FROM ( ");
		sqlCovxSelect.append(" SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,JLIFE,PRMCUR,BBLDAT,SUMINS,PCESDTE,CRTABLE,MORTCLS,SUBPROG,UFREQ,PREMMETH,JLPREMETH,SVMETH,ADFEEMTH ");
		sqlCovxSelect.append("   ,FLOOR((ROW_NUMBER()OVER( ORDER BY UNIQUE_NUMBER DESC)-1)/?) BATCHNUM ");
		sqlCovxSelect.append("  FROM ");
		sqlCovxSelect.append(tableId);
		sqlCovxSelect.append("   WHERE MEMBER_NAME = ? ");
		sqlCovxSelect.append(" ) MAIN WHERE BATCHNUM = ? ");

		PreparedStatement psCovxSelect = getPrepareStatement(sqlCovxSelect.toString());
		ResultSet sqlcovxpf1rs = null;
		List<Covxpf> covxpfList = new ArrayList<>();
		try {
			psCovxSelect.setInt(1, batchExtractSize);
			psCovxSelect.setString(2, memName);
			psCovxSelect.setInt(3, batchID);

			sqlcovxpf1rs = executeQuery(psCovxSelect);
			while (sqlcovxpf1rs.next()) {
				Covxpf covxpf = new Covxpf();
				covxpf.setChdrcoy(sqlcovxpf1rs.getString("chdrcoy"));
				covxpf.setChdrnum(sqlcovxpf1rs.getString("chdrnum"));
				covxpf.setLife(sqlcovxpf1rs.getString("life"));
				covxpf.setCoverage(sqlcovxpf1rs.getString("coverage"));
				covxpf.setRider(sqlcovxpf1rs.getString("rider"));
				covxpf.setPlanSuffix(sqlcovxpf1rs.getInt("plnsfx"));
				covxpf.setJlife(sqlcovxpf1rs.getString("jlife"));
				covxpf.setPremCurrency(sqlcovxpf1rs.getString("prmcur"));
				covxpf.setBenBillDate(sqlcovxpf1rs.getInt("bbldat"));
				covxpf.setSumins(sqlcovxpf1rs.getBigDecimal("sumins"));
				covxpf.setPremCessDate(sqlcovxpf1rs.getInt("pcesdte"));
				covxpf.setCrtable(sqlcovxpf1rs.getString("crtable"));
				covxpf.setMortcls(sqlcovxpf1rs.getString("mortcls"));
				covxpf.setSubprog(sqlcovxpf1rs.getString("subprog"));
				covxpf.setUnitFreq(sqlcovxpf1rs.getString("ufreq"));
				covxpf.setPremmeth(sqlcovxpf1rs.getString("premmeth"));
				covxpf.setJlPremMeth(sqlcovxpf1rs.getString("jlpremeth"));
				covxpf.setSvMethod(sqlcovxpf1rs.getString("svmeth"));
				covxpf.setAdfeemth(sqlcovxpf1rs.getString("adfeemth"));
				covxpf.setUniqueNumber(sqlcovxpf1rs.getLong("UNIQUE_NUMBER"));
				covxpfList.add(covxpf);
			}
		} catch (SQLException e) {
			LOGGER.error("searchCovxRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovxSelect, sqlcovxpf1rs);
		}
		return covxpfList;

	}
}