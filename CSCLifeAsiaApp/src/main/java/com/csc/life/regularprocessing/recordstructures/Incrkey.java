package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:59
 * Description:
 * Copybook name: INCRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Incrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData incrFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData incrKey = new FixedLengthStringData(64).isAPartOf(incrFileKey, 0, REDEFINE);
  	public FixedLengthStringData incrChdrcoy = new FixedLengthStringData(1).isAPartOf(incrKey, 0);
  	public FixedLengthStringData incrChdrnum = new FixedLengthStringData(8).isAPartOf(incrKey, 1);
  	public FixedLengthStringData incrLife = new FixedLengthStringData(2).isAPartOf(incrKey, 9);
  	public FixedLengthStringData incrCoverage = new FixedLengthStringData(2).isAPartOf(incrKey, 11);
  	public FixedLengthStringData incrRider = new FixedLengthStringData(2).isAPartOf(incrKey, 13);
  	public PackedDecimalData incrPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(incrKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(incrKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(incrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		incrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}