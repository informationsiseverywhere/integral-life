package com.csc.life.regularprocessing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R6282.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R6282Report extends SMARTReportLayout { 

	private FixedLengthStringData bankacckey = new FixedLengthStringData(20);
	private FixedLengthStringData bankcode = new FixedLengthStringData(2);
	private FixedLengthStringData bankkey = new FixedLengthStringData(10);
	private FixedLengthStringData billcd = new FixedLengthStringData(10);
	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData chdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData facthous = new FixedLengthStringData(2);
	private ZonedDecimalData instamtprt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData jobnofrom = new ZonedDecimalData(8, 0);
	private ZonedDecimalData jobnoto = new ZonedDecimalData(8, 0);
	private ZonedDecimalData jobnum = new ZonedDecimalData(8, 0);
	private FixedLengthStringData mandref = new FixedLengthStringData(5);
	private FixedLengthStringData mandstat = new FixedLengthStringData(2);
	private FixedLengthStringData occdate = new FixedLengthStringData(10);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData payrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData payrnum = new FixedLengthStringData(8);
	private FixedLengthStringData procaction = new FixedLengthStringData(1);
	private FixedLengthStringData repdate = new FixedLengthStringData(10);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private FixedLengthStringData surnameprt = new FixedLengthStringData(15);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R6282Report() {
		super();
	}


	/**
	 * Print the XML for R6282d01
	 */
	public void printR6282d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		payrcoy.setFieldName("payrcoy");
		payrcoy.setInternal(subString(recordData, 1, 1));
		payrnum.setFieldName("payrnum");
		payrnum.setInternal(subString(recordData, 2, 8));
		surnameprt.setFieldName("surnameprt");
		surnameprt.setInternal(subString(recordData, 10, 15));
		mandref.setFieldName("mandref");
		mandref.setInternal(subString(recordData, 25, 5));
		billcd.setFieldName("billcd");
		billcd.setInternal(subString(recordData, 30, 10));
		facthous.setFieldName("facthous");
		facthous.setInternal(subString(recordData, 40, 2));
		bankcode.setFieldName("bankcode");
		bankcode.setInternal(subString(recordData, 42, 2));
		bankkey.setFieldName("bankkey");
		bankkey.setInternal(subString(recordData, 44, 10));
		chdrcoy.setFieldName("chdrcoy");
		chdrcoy.setInternal(subString(recordData, 54, 1));
		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 55, 8));
		cnttype.setFieldName("cnttype");
		cnttype.setInternal(subString(recordData, 63, 3));
		occdate.setFieldName("occdate");
		occdate.setInternal(subString(recordData, 66, 10));
		instamtprt.setFieldName("instamtprt");
		instamtprt.setInternal(subString(recordData, 76, 17));
		jobnum.setFieldName("jobnum");
		jobnum.setInternal(subString(recordData, 93, 8));
		mandstat.setFieldName("mandstat");
		mandstat.setInternal(subString(recordData, 101, 2));
		procaction.setFieldName("procaction");
		procaction.setInternal(subString(recordData, 103, 1));
		bankacckey.setFieldName("bankacckey");
		bankacckey.setInternal(subString(recordData, 104, 20));
		printLayout("R6282d01",			// Record name
			new BaseData[]{			// Fields:
				payrcoy,
				payrnum,
				surnameprt,
				mandref,
				billcd,
				facthous,
				bankcode,
				bankkey,
				chdrcoy,
				chdrnum,
				cnttype,
				occdate,
				instamtprt,
				jobnum,
				mandstat,
				procaction,
				bankacckey
			}
		);

		currentPrintLine.add(2);
	}

	/**
	 * Print the XML for R6282d02
	 */
	public void printR6282d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(4);

		printLayout("R6282d02",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for R6282d03
	 */
	public void printR6282d03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("R6282d03",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for R6282h01
	 */
	public void printR6282h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 1, 10));
		jobnofrom.setFieldName("jobnofrom");
		jobnofrom.setInternal(subString(recordData, 11, 8));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		company.setFieldName("company");
		company.setInternal(subString(recordData, 19, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 20, 30));
		jobnoto.setFieldName("jobnoto");
		jobnoto.setInternal(subString(recordData, 50, 8));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 58, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 68, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 70, 30));
		time.setFieldName("time");
		time.set(getTime());
		printLayout("R6282h01",			// Record name
			new BaseData[]{			// Fields:
				repdate,
				jobnofrom,
				pagnbr,
				company,
				companynm,
				jobnoto,
				sdate,
				branch,
				branchnm,
				time
			}
		);

		currentPrintLine.set(14);
	}


}
