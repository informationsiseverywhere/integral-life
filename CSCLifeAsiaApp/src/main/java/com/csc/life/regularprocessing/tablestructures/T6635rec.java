package com.csc.life.regularprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:36
 * Description:
 * Copybook name: T6635REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6635rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6635Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData bonuss = new FixedLengthStringData(70).isAPartOf(t6635Rec, 0);
  	public ZonedDecimalData[] bonus = ZDArrayPartOfStructure(10, 7, 2, bonuss, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(70).isAPartOf(bonuss, 0, FILLER_REDEFINE);
  	public ZonedDecimalData bonus01 = new ZonedDecimalData(7, 2).isAPartOf(filler, 0);
  	public ZonedDecimalData bonus02 = new ZonedDecimalData(7, 2).isAPartOf(filler, 7);
  	public ZonedDecimalData bonus03 = new ZonedDecimalData(7, 2).isAPartOf(filler, 14);
  	public ZonedDecimalData bonus04 = new ZonedDecimalData(7, 2).isAPartOf(filler, 21);
  	public ZonedDecimalData bonus05 = new ZonedDecimalData(7, 2).isAPartOf(filler, 28);
  	public ZonedDecimalData bonus06 = new ZonedDecimalData(7, 2).isAPartOf(filler, 35);
  	public ZonedDecimalData bonus07 = new ZonedDecimalData(7, 2).isAPartOf(filler, 42);
  	public ZonedDecimalData bonus08 = new ZonedDecimalData(7, 2).isAPartOf(filler, 49);
  	public ZonedDecimalData bonus09 = new ZonedDecimalData(7, 2).isAPartOf(filler, 56);
  	public ZonedDecimalData bonus10 = new ZonedDecimalData(7, 2).isAPartOf(filler, 63);
  	public FixedLengthStringData offsets = new FixedLengthStringData(30).isAPartOf(t6635Rec, 70);
  	public ZonedDecimalData[] offset = ZDArrayPartOfStructure(10, 3, 0, offsets, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(offsets, 0, FILLER_REDEFINE);
  	public ZonedDecimalData offset01 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData offset02 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 3);
  	public ZonedDecimalData offset03 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 6);
  	public ZonedDecimalData offset04 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 9);
  	public ZonedDecimalData offset05 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData offset06 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 15);
  	public ZonedDecimalData offset07 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 18);
  	public ZonedDecimalData offset08 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 21);
  	public ZonedDecimalData offset09 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 24);
  	public ZonedDecimalData offset10 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 27);
  	public FixedLengthStringData riskunits = new FixedLengthStringData(12).isAPartOf(t6635Rec, 100);
  	public ZonedDecimalData[] riskunit = ZDArrayPartOfStructure(2, 6, 0, riskunits, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(12).isAPartOf(riskunits, 0, FILLER_REDEFINE);
  	public ZonedDecimalData riskunit01 = new ZonedDecimalData(6, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData riskunit02 = new ZonedDecimalData(6, 0).isAPartOf(filler2, 6);
  	public FixedLengthStringData sumasss = new FixedLengthStringData(70).isAPartOf(t6635Rec, 112);
  	public ZonedDecimalData[] sumass = ZDArrayPartOfStructure(10, 7, 2, sumasss, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(70).isAPartOf(sumasss, 0, FILLER_REDEFINE);
  	public ZonedDecimalData sumass01 = new ZonedDecimalData(7, 2).isAPartOf(filler3, 0);
  	public ZonedDecimalData sumass02 = new ZonedDecimalData(7, 2).isAPartOf(filler3, 7);
  	public ZonedDecimalData sumass03 = new ZonedDecimalData(7, 2).isAPartOf(filler3, 14);
  	public ZonedDecimalData sumass04 = new ZonedDecimalData(7, 2).isAPartOf(filler3, 21);
  	public ZonedDecimalData sumass05 = new ZonedDecimalData(7, 2).isAPartOf(filler3, 28);
  	public ZonedDecimalData sumass06 = new ZonedDecimalData(7, 2).isAPartOf(filler3, 35);
  	public ZonedDecimalData sumass07 = new ZonedDecimalData(7, 2).isAPartOf(filler3, 42);
  	public ZonedDecimalData sumass08 = new ZonedDecimalData(7, 2).isAPartOf(filler3, 49);
  	public ZonedDecimalData sumass09 = new ZonedDecimalData(7, 2).isAPartOf(filler3, 56);
  	public ZonedDecimalData sumass10 = new ZonedDecimalData(7, 2).isAPartOf(filler3, 63);
  	public FixedLengthStringData yrsinfs = new FixedLengthStringData(30).isAPartOf(t6635Rec, 182);
  	public ZonedDecimalData[] yrsinf = ZDArrayPartOfStructure(10, 3, 0, yrsinfs, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(30).isAPartOf(yrsinfs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData yrsinf01 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 0);
  	public ZonedDecimalData yrsinf02 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 3);
  	public ZonedDecimalData yrsinf03 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 6);
  	public ZonedDecimalData yrsinf04 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 9);
  	public ZonedDecimalData yrsinf05 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 12);
  	public ZonedDecimalData yrsinf06 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 15);
  	public ZonedDecimalData yrsinf07 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 18);
  	public ZonedDecimalData yrsinf08 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 21);
  	public ZonedDecimalData yrsinf09 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 24);
  	public ZonedDecimalData yrsinf10 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 27);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(288).isAPartOf(t6635Rec, 212, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6635Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6635Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}