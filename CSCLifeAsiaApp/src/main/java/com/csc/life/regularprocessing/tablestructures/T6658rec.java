package com.csc.life.regularprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:01
 * Description:
 * Copybook name: T6658REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6658rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6658Rec = new FixedLengthStringData(501);
  	public FixedLengthStringData addexist = new FixedLengthStringData(1).isAPartOf(t6658Rec, 0);
  	public FixedLengthStringData addnew = new FixedLengthStringData(1).isAPartOf(t6658Rec, 1);
  	public ZonedDecimalData agemax = new ZonedDecimalData(3, 0).isAPartOf(t6658Rec, 2);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(t6658Rec, 5);
  	public FixedLengthStringData comind = new FixedLengthStringData(1).isAPartOf(t6658Rec, 7);
  	public FixedLengthStringData compoundInd = new FixedLengthStringData(1).isAPartOf(t6658Rec, 8);
  	public ZonedDecimalData fixdtrm = new ZonedDecimalData(3, 0).isAPartOf(t6658Rec, 9);
  	public FixedLengthStringData manopt = new FixedLengthStringData(1).isAPartOf(t6658Rec, 12);
  	public ZonedDecimalData maxpcnt = new ZonedDecimalData(5, 2).isAPartOf(t6658Rec, 13);
  	public ZonedDecimalData maxRefusals = new ZonedDecimalData(2, 0).isAPartOf(t6658Rec, 18);
  	public ZonedDecimalData minctrm = new ZonedDecimalData(3, 0).isAPartOf(t6658Rec, 20);
  	public ZonedDecimalData minpcnt = new ZonedDecimalData(5, 2).isAPartOf(t6658Rec, 23);
  	public FixedLengthStringData nocommind = new FixedLengthStringData(1).isAPartOf(t6658Rec, 28);
  	public FixedLengthStringData nostatin = new FixedLengthStringData(1).isAPartOf(t6658Rec, 29);
  	public FixedLengthStringData optind = new FixedLengthStringData(1).isAPartOf(t6658Rec, 30);
  	public ZonedDecimalData refusalPeriod = new ZonedDecimalData(3, 0).isAPartOf(t6658Rec, 31);
  	public FixedLengthStringData simpleInd = new FixedLengthStringData(1).isAPartOf(t6658Rec, 34);
  	public FixedLengthStringData statind = new FixedLengthStringData(1).isAPartOf(t6658Rec, 35);
  	public FixedLengthStringData subprog = new FixedLengthStringData(10).isAPartOf(t6658Rec, 36);
  	public FixedLengthStringData premsubr = new FixedLengthStringData(8).isAPartOf(t6658Rec, 46);
  	public FixedLengthStringData trevsub = new FixedLengthStringData(10).isAPartOf(t6658Rec, 54);
  	public FixedLengthStringData incrFlg = new FixedLengthStringData(1).isAPartOf(t6658Rec, 64);
  	public FixedLengthStringData filler = new FixedLengthStringData(436).isAPartOf(t6658Rec, 65, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6658Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6658Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}