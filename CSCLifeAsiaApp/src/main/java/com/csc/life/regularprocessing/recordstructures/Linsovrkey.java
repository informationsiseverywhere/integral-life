package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:12
 * Description:
 * Copybook name: LINSOVRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Linsovrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData linsovrFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData linsovrKey = new FixedLengthStringData(64).isAPartOf(linsovrFileKey, 0, REDEFINE);
  	public FixedLengthStringData linsovrChdrcoy = new FixedLengthStringData(1).isAPartOf(linsovrKey, 0);
  	public FixedLengthStringData linsovrChdrnum = new FixedLengthStringData(8).isAPartOf(linsovrKey, 1);
  	public PackedDecimalData linsovrInstto = new PackedDecimalData(8, 0).isAPartOf(linsovrKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(linsovrKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(linsovrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		linsovrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}