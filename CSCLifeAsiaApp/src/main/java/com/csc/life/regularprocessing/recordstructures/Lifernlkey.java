package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:09
 * Description:
 * Copybook name: LIFERNLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lifernlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lifernlFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lifernlKey = new FixedLengthStringData(64).isAPartOf(lifernlFileKey, 0, REDEFINE);
  	public FixedLengthStringData lifernlChdrcoy = new FixedLengthStringData(1).isAPartOf(lifernlKey, 0);
  	public FixedLengthStringData lifernlChdrnum = new FixedLengthStringData(8).isAPartOf(lifernlKey, 1);
  	public FixedLengthStringData lifernlLife = new FixedLengthStringData(2).isAPartOf(lifernlKey, 9);
  	public FixedLengthStringData lifernlJlife = new FixedLengthStringData(2).isAPartOf(lifernlKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(51).isAPartOf(lifernlKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lifernlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lifernlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}