/*
 * File: B5349.java
 * Date: 29 August 2009 21:04:52
 * Author: Quipoz Limited
 *
 * Class transformed from B5349.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.DdsupfDAO;
import com.csc.fsu.general.dataaccess.dao.DshnpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ddsupf;
import com.csc.fsu.general.dataaccess.model.Dshnpf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.AgecalcPojo;
import com.csc.fsu.general.procedures.AgecalcUtils;
import com.csc.fsu.general.procedures.Billreq1;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Billreqrec;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3620rec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.general.tablestructures.Th5agrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.annuities.recordstructures.Prasrec;
import com.csc.life.annuities.tablestructures.T6687rec;
import com.csc.life.cashdividends.dataaccess.dao.HcsdpfDAO;
import com.csc.life.cashdividends.dataaccess.dao.HdispfDAO;
import com.csc.life.cashdividends.dataaccess.dao.HdivpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hcsdpf;
import com.csc.life.cashdividends.dataaccess.model.Hdispf;
import com.csc.life.cashdividends.dataaccess.model.Hdivpf;
import com.csc.life.contractservicing.dataaccess.dao.OverpfDAO;
import com.csc.life.contractservicing.dataaccess.dao.PrmhpfDAO;
import com.csc.life.contractservicing.dataaccess.dao.TaxdpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Overpf;
import com.csc.life.contractservicing.dataaccess.model.Prmhpf;
import com.csc.life.contractservicing.dataaccess.model.Taxdpf;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.flexiblepremium.dataaccess.dao.FpcopfDAO;
import com.csc.life.flexiblepremium.dataaccess.dao.FprmpfDAO;
import com.csc.life.flexiblepremium.dataaccess.model.Fpcopf;
import com.csc.life.flexiblepremium.dataaccess.model.Fprmpf;
import com.csc.life.linkage.procedures.LinkageInfoService;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.newbusiness.procedures.Nlgcalc;
import com.csc.life.newbusiness.recordstructures.Nlgcalcrec;
import com.csc.life.newbusiness.tablestructures.T3615rec;
import com.csc.life.productdefinition.dataaccess.dao.AnnypfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Annypf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Rlpdlon;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Rlpdlonrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.regularprocessing.dataaccess.dao.B5349DAO;
import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.RertpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.B5349DTO;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
import com.csc.life.regularprocessing.dataaccess.model.PayxTemppf;
import com.csc.life.regularprocessing.dataaccess.model.Rertpf;
import com.csc.life.terminationclaims.recordstructures.Calprpmrec;
import com.csc.life.terminationclaims.tablestructures.Td5j1rec;
import com.csc.life.terminationclaims.tablestructures.Td5j2rec;
import com.csc.smart.procedures.Contot;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.DateUtils;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*                   BILLING
*                   -------
* Overview
* ________
*
*   This program is part of the new 'Multi-Threading Batch
* Performance' suite. It runs directly after B5348, which 'splits'
* the PAYRPF according to the number of billing programs to run.
* All references to the PAYR are via PAYXPF - a temporary file
* holding all the PAYR records for this program to process.
*
* B5349 will perform all the processing for Billing that is
* applicable to LIFE/400. A new subroutine will be called from
* within the batch job to perform the Billing processing that
* is applicable to FSU.
*
* Billing processes only those PAYR records which have the
* BILLCD <= effective date (adjusted by the lead days on T6654)
*
* The following control totals are maintained within the program:
*
*       1 - PAYR records read
*       2 - PAYR records bill suppress'd
*       3 - PTRN records produced
*       4 - Total Amount Billed.
*       5 - LINS records created.
*       6 - Total amount on LINS records.
*       7 - CHDR records that have invalid statii.
*       8 - CHDR records that are 'locked'.
*       9 - Media records (BEXT) created. } Passed back from
*      10 - Total amount on BEXT records. }     BILLREQ1
*
* 1000-INITIALISE SECTION
* _______________________
*
*  -  Frequently-referenced tables are stored in working storage
*     arrays to minimize disk IO. These tables include
*     T3629 (Bank codes), T3620 (Billing channels), T6654
*     (Billing control) and T6687 (Tax relief methods).
*
*  -  Issue an override to read the correct PAYXPF for this run.
*     The CRTTMPF process has already created a file specific to
*     the run using the  PAYXPF fields and is identified  by
*     concatenating the following:-
*
*     'PAYX'
*      BPRD-SYSTEM-PARAM04
*      BSSC-SCHEDULE-NUMBER
*
*      eg PAYX2B0001,  for the first run
*         PAYX2B0002,  for the second etc.
*
*     The number of threads would have been created by the
*     CRTTMPF process given the parameters of the process
*     definition of the CRTTMPF.
*     To get the correct member of the above physical for B5349
*     to read from, concatenate :-
*
*     'THREAD'
*     BSPR-PROCESS-OCC-NUM
*
*  -  Initialise the static values of the LIFACMV copybook.
*
* 2000-READ SECTION
* _________________
*
* -  Read the PAYX records sequentially incrementing the control
*    total.
*
* -  If end of file move ENDP to WSSP-EDTERROR.
*
* 2500-EDIT SECTION
* _________________
*
* -  Move OK to WSSP-EDTERROR.
*
* -  Read and validate the CHDR risk status and premium status
*    against those obtained from T5679. If the status is invalid
*    add 1 to control total 6 and move SPACES to WSSP-EDTERROR.
*
* -  Obtain the number of lead days from table T6654 using the
*    contract type as the key.
*
* -  Use DATCON2 to increment the bill date by the number of lead
*    days.
*
* -  If the bill date is within the bill supprfrom and billsuppto
*    by-pass the contract and increment the control total.
*    Move SPACES to WSSP-EDTERROR to prevent 3000-update
*    processing.
*
* - 'Soft lock' the contract, if it is to be processed.
*    If the contract is already 'locked' increment control
*    total number 8 and  move SPACES to WSSP-EDTERROR.
*
*  3000-UPDATE SECTION
*  ___________________
*
*  - Read & hold the payer record using logical file PAYR.
*
*  - Read client roles (CLRF), role 'PY' to obtain the payer
*    number.
*
*  - Read the ACBL file using the sub-account code & type from
*    the T5645 entry to obtain the amount in contract suspense.
*    If there is enough in suspense then BEXT records do not have
*    to be produced later in program.
*
*  - Multiply the amount in the suspense account by the general
*    ledger sign that was obtained from T3695.
*
*    For PAYR records to be billed
*  - Calculate the premium
*
*  - Calculate any automatic increases
*
*  - Call DATCON4 to advance the billed to date by the billing
*    freq. Update this field on the PAYR & CHDR. (DATCON4
*    performs the same DATCON2, but takes into consideration the
*    original day and month  and therefore avoiding the problems of
*    that DATCON2 had with end months'.)
*
*  - Call DATCON4 to increment the billing renewal date by the
*    billing frequency. Update this field on the PAYR & CHDR.
*
*  - Using DATCON2 subtract the lead days from the billing
*    renewal date calculate the next billing extract date for the
*    PAYR record.
*
*  - Set up fields for the LINS record, converting the contract
*    total to the billing amount.  If the contract is not the
*    same as the billing currency, call XCVRT.
*
*  - Find the tax-relief method for this payer to be written to
*    the LINS.
*
*  - Write LINS record.
*
*  - If this is a direct debit payment obtain mandate details
*    & client bank account details (CLBA).  If there has been a
*    DD dishonour update the mandate status and the billdate
*    on the BEXT record.
*
*  - Calculate the tax relief on the billed premium. Convert this
*    net amount if the contract currency and billing currencies
*    are different by calling XCVRT.  If there is enough money
*    in suspense (calculated earlier) to cover instalment,  THERE
*    IS NO NEED TO PRODUCE A BEXT RECORD. Call BILLREQ1 and
*    update the relevant control total.
*
*  - Produce a PTRN record for this instalment.
*
*  - If the PAYR billing renewal date is still not greater
*    than the bill date produce another LINS.
*
*  - Rewrite CHDR & PAYR
*
*  - Unlock the contract.
*
* 4000-CLOSE SECTION
* __________________
*
*  - Close Files.
*
*  - Delete the Override function for the PAYXPF file
*
*   Error Processing:
*
*     Perform the 600-FATAL-ERROR section. The
*     SYSR-SYSERR-TYPE flag does not need to be set in this
*     program, because MAINB takes care of a system errors.
*
*          (BATD processing is handled in MAINB)
*
*****************************************************************
* </pre>
*/
public class B5349 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5349");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaT5679Sub = new ZonedDecimalData(2, 0).setUnsigned();

		/* WSAA-T3620-ARRAY */
	//private FixedLengthStringData[] wsaaT3620Rec = FLSInittedArray (10, 4);
	private FixedLengthStringData[] wsaaT3620Rec = FLSInittedArray (1000, 4);//ILIFE-1985
	private FixedLengthStringData[] wsaaT3620Key = FLSDArrayPartOfArrayStructure(2, wsaaT3620Rec, 0);
	private FixedLengthStringData[] wsaaT3620Billchnl = FLSDArrayPartOfArrayStructure(2, wsaaT3620Key, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT3620Data = FLSDArrayPartOfArrayStructure(2, wsaaT3620Rec, 2);
	private FixedLengthStringData[] wsaaT3620Ddind = FLSDArrayPartOfArrayStructure(1, wsaaT3620Data, 0);
	private FixedLengthStringData[] wsaaT3620Crcind = FLSDArrayPartOfArrayStructure(1, wsaaT3620Data, 1);

		/* WSAA-T3629-ARRAY */
	private ZonedDecimalData wsaaOldOutstamt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOverduePer = new ZonedDecimalData(11, 2);
	private ZonedDecimalData wsaaCovrInc = new ZonedDecimalData(17, 2);
	protected String wsaaFirstBill = "Y";
	private PackedDecimalData wsaaCashdate = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	protected PackedDecimalData wsaaTax = new PackedDecimalData(14, 2);
	private PackedDecimalData wsaaBillOutst = new PackedDecimalData(17, 2).init(ZERO);
	protected PackedDecimalData wsaaBillAmount = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaIncrBinstprem = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaIncrInstprem = new PackedDecimalData(17, 2).init(ZERO);
	private FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
	private ZonedDecimalData wsaaTr517Ix = new ZonedDecimalData(2, 0);
	private FixedLengthStringData wsaaTr517Item = new FixedLengthStringData(8);
	private String wsaaWaiveCovrCalcTax = "";

	private FixedLengthStringData wsaaWopFound = new FixedLengthStringData(1).init("N");
	private Validator wopFound = new Validator(wsaaWopFound, "Y");

	private FixedLengthStringData wsaaValidCovr = new FixedLengthStringData(1).init("N");
	private Validator validCovr = new Validator(wsaaValidCovr, "Y");
		/* ERRORS */
	private static final String ivrm = "IVRM";
	protected PackedDecimalData wsaaOldBtdate = new PackedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaaEffdatePlusCntlead = new PackedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaaSuspAvail = new PackedDecimalData(17, 2).init(0);
	protected ZonedDecimalData wsaaOldBillcd = new ZonedDecimalData(8, 0).setUnsigned();
	protected PackedDecimalData wsaaOldNextdate = new PackedDecimalData(8, 0).init(0);
	protected PackedDecimalData wsaaIncreaseDue = new PackedDecimalData(14, 2);
	private PackedDecimalData wsaaInstSub = new PackedDecimalData(3, 0).init(0);

	private FixedLengthStringData wsaaValidChdr = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidChdr, "Y");

	private FixedLengthStringData wsaaFlexPrem = new FixedLengthStringData(1);
	protected Validator flexiblePremiumContract = new Validator(wsaaFlexPrem, "Y");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");
	protected String wsaaGotPayrAtBtdate = "";

	protected FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(97);
	private FixedLengthStringData wsysPayrkey = new FixedLengthStringData(20).isAPartOf(wsysSystemErrorParams, 0);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysPayrkey, 0);
	protected ZonedDecimalData wsysBillcd = new ZonedDecimalData(8, 0).isAPartOf(wsysPayrkey, 9).setUnsigned();
	protected FixedLengthStringData wsysSysparams = new FixedLengthStringData(77).isAPartOf(wsysSystemErrorParams, 20);
	private PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaAllDvdTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRunDvdTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTfrAmt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaShortfall = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPremSusp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaDvdSusp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaIdx = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaNoOfHdis = new PackedDecimalData(3, 0);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	protected Billreqrec billreqrec = new Billreqrec();
	protected Conlinkrec conlinkrec = new Conlinkrec();
	protected Prasrec prasrec = new Prasrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Rlpdlonrec rlpdlonrec = new Rlpdlonrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	protected Txcalcrec txcalcrec = new Txcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T3620rec t3620rec = new T3620rec();
	private T3629rec t3629rec = new T3629rec();
	private T3695rec t3695rec = new T3695rec();
	protected T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private Tr384rec tr384rec = new Tr384rec();
	private T6654rec t6654rec = new T6654rec();
	private T6687rec t6687rec = new T6687rec();
	private Tr517rec tr517rec = new Tr517rec();
	protected Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private ControlTotalsInner controlTotalsInner = new ControlTotalsInner();
	private WsaaHdisArrayInner wsaaHdisArrayInner = new WsaaHdisArrayInner();
	
	/*Batch Upgrade Variables*/
	private String strEffDate;
	protected int intBatchID;
	private int intBatchStep;
	protected boolean noRecordFound;
	protected int intBatchExtractSize;
	
	/*PF Used / DAO Model*/
	protected PayxTemppf payxTemppf = new PayxTemppf();
	protected B5349DTO b5349DTO = new B5349DTO();
	protected Chdrpf chdrpf;
	private Ptrnpf ptrnpf;
	protected Payrpf payrpf;
	protected Payrpf currPayrLif;
	protected Linspf linspf;
	private Fpcopf fpcopf;
	private Fprmpf fprmpf;
	protected Taxdpf taxdpf;
	private Hdivpf hdivpf;
	
	/*Iterator, Maps and Lists*/
	private List<PayxTemppf> payxList;
	private Iterator<PayxTemppf> iteratorList;
	
	protected List<Chdrpf> chdrBulkUpdtList;
	protected List<Payrpf> payrBulkUpdList;
	protected List<Ptrnpf> ptrnBulkInsList;
	protected List<Linspf> linsBulkInsList;
	protected List<Fpcopf> fpcoBulkUpdList;
	protected List<Fprmpf> fprmBulkUpdList;
	protected List<Taxdpf> taxdBulkInsList;
	protected List<Hdivpf> hdivBulkInsList;
	protected List<Hpadpf> hpadBulkUpdtList;

	protected boolean isBulkProcessLinsPF;
	protected boolean isBulkProcessFpcoPF;
	protected boolean isBulkProcessTaxdPF;
	protected boolean isBulkProcessHdivPF;
	protected boolean isBulkProcessFprmPF;
	protected boolean isBulkProcessHpadPF;

	/* Smart Table Lists */
	protected Map<String, List<Itempf>> t5679ListMap;
	protected Map<String, List<Itempf>> t5645ListMap;
	protected Map<String, List<Itempf>> t3695ListMap;
	protected Map<String, List<Itempf>> t3620ListMap;
	protected Map<String, List<Itempf>> t3629ListMap;
	protected Map<String, List<Itempf>> t6654ListMap;
	protected Map<String, List<Itempf>> t6687ListMap;
	protected Map<String, List<Itempf>> t5729ListMap;
	protected Map<String, List<Itempf>> tr52dListMap;
	protected Map<String, List<Itempf>> tr384ListMap;
	protected Map<String, List<Itempf>> tr517ListMap;
	protected Map<String, List<Itempf>> tr52eListMap;
	protected Map<String, List<Itempf>> td5j2ListMap;
	protected Map<String, List<Itempf>> td5j1ListMap;
	protected Map<String, List<Itempf>> t5688ListMap;
	protected Map<String, List<Itempf>> t5674ListMap;

	protected Map<String, List<Covrpf>> covrListMap;
	private Map<String, List<Acblpf>> acblListMap;
	protected Map<String, List<Payrpf>> payrListMap;
	private Map<String, List<Incrpf>> incrListMap;
	private Map<String, List<Fpcopf>> fpcoListMap;
	private Map<String, List<Fprmpf>> fprmListMap;
	private Map<String, List<Ddsupf>> ddsuListMap;
	private Map<String, List<Hdispf>> hdisListMap;
	private Map<String, List<Hdivpf>> hdivcshListMap;
	private Map<String, List<Hcsdpf>> hcsdListMap;
	protected Map<String, List<Covtpf>> covtListMap;
	
	/*DAO Used*/
    protected DshnpfDAO dshnpfDao = getApplicationContext().getBean("dshnpfDao", DshnpfDAO.class);
	protected ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private B5349DAO b5349DAO =  getApplicationContext().getBean("b5349DAO", B5349DAO.class);
	private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private TaxdpfDAO taxdpfDAO =  getApplicationContext().getBean("taxdpfDAO", TaxdpfDAO.class);
	private LinspfDAO linspfDAO =  getApplicationContext().getBean("linspfDAO", LinspfDAO.class);
	private ChdrpfDAO chdrpfDAO =  getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private AcblpfDAO acblpfDAO =  getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private PayrpfDAO payrpfDAO =  getApplicationContext().getBean("payrDAO", PayrpfDAO.class);
	private IncrpfDAO incrpfDAO =  getApplicationContext().getBean("incrpfDAO", IncrpfDAO.class);
	private FpcopfDAO fpcopfDAO =  getApplicationContext().getBean("fpcopfDAO", FpcopfDAO.class);
	private FprmpfDAO fprmpfDAO =  getApplicationContext().getBean("fprmpfDAO", FprmpfDAO.class);
	private DdsupfDAO ddsupfDAO =  getApplicationContext().getBean("ddsupfDAO", DdsupfDAO.class);
	private HdispfDAO hdispfDAO =  getApplicationContext().getBean("hdispfDAO", HdispfDAO.class);
	private HdivpfDAO hdivpfDAO =  getApplicationContext().getBean("hdivpfDAO", HdivpfDAO.class);
	private HcsdpfDAO hcsdpfDAO =  getApplicationContext().getBean("hcsdpfDAO", HcsdpfDAO.class);
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);
	private CovtpfDAO covtpfDAO = getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);

	/*Error Codes Used*/
	private static final String h134 = "H134";
	private static final String h205 = "H205";  
	private static final String h420 = "H420";
	private static final String g437 = "G437";
	private static final String f921 = "F921";
	
	/*Control Totals*/
		
	protected long ctrCT01; 
	protected long ctrCT02;
	protected long ctrCT03; 
	protected BigDecimal ctrCT04;
	protected long ctrCT05; 
	protected BigDecimal ctrCT06;
	protected long ctrCT07; 
	protected long ctrCT08;
	protected long ctrCT09; 
	protected BigDecimal ctrCT10;
	protected long ctrCT11; 
	protected BigDecimal ctrCT12;
	
	protected int intT6654Leaddays;
	protected String strT3629BankCode;
	protected String strT6687TaxRelSub;
	protected BigDecimal TotalAmount;
	private boolean searchItemFound;
	
	protected static final Logger LOGGER = LoggerFactory.getLogger(B5349.class);
	//ILIFE-3997-STARTS
	private Nlgcalcrec nlgcalcrec = new Nlgcalcrec();
	
	private String t6654 = "T6654";
	private boolean isUpdate=false;
	private Map<String, Integer> trannoMap;
	//ILIFE-3997-ENDS
	protected StringBuilder threadNumber;
    private List<Covrpf> covrpflist = null;
    private List<Ptrnpf> ptrnrecords = null;
    private Ptrnpf ptrnpfReinstate = new Ptrnpf();
	private boolean reinstflag = false;
	public ZonedDecimalData lapsePTDate = new ZonedDecimalData(8, 0);
	public ZonedDecimalData nextPTDate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData proratePrem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaInstPrem = new ZonedDecimalData(17, 2);
	public ZonedDecimalData nextBTDate = new ZonedDecimalData(8, 0);
	private static final String td5j2 = "TD5J2";
	private static final String td5j1 = "TD5J1";
	private static final String ta85 = "TA85";
	private static final String t514 = "T514";
	private Td5j2rec td5j2rec = new Td5j2rec();
	private Td5j1rec td5j1rec = new Td5j1rec();
	private boolean isFoundPro = false;
	private boolean isShortTax = false;
	private ZonedDecimalData wsaaInstOutst = new ZonedDecimalData(2, 0).setUnsigned();
	protected PackedDecimalData wsaaOldBtdateBk = new PackedDecimalData(8, 0).init(0);
	private Map<Long,BigDecimal> reratedPremKeeps  = new LinkedHashMap<>();
	private Map<Integer, List<Incrpf>> incrDatedMap;
	private ZonedDecimalData increaseprem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData leaveprem = new ZonedDecimalData(17, 2);
	private Calprpmrec calprpmrec = new Calprpmrec();
	private ZonedDecimalData ptdlapse = new ZonedDecimalData(8, 0);
	private ZonedDecimalData lastpayrPTD = new ZonedDecimalData(8, 0);
	private boolean prmhldtrad = false;
	private Map<String, Itempf> ta524Map;
	private Prmhpf prmhpf;
	protected Payrpf phpayr;
	private PrmhpfDAO prmhpfDAO = getApplicationContext().getBean("prmhpfDAO", PrmhpfDAO.class);
	private boolean billWritten = false;
	private Boolean isBatchSkipFeatureEnabled;// IBPTE-74
	private Boolean japanBilling = false;
	private static final String BTPRO027 = "BTPRO027";
	protected Map<String, List<Itempf>> th5agListMap;
	protected Map<String, List<Itempf>> t3615ListMap;
	private T3615rec t3615rec = new T3615rec();
	private Th5agrec th5agrec = new Th5agrec();
	private T5688rec t5688rec = new T5688rec();
	private T5674rec t5674rec = new T5674rec();
	private FixedLengthStringData wsaaItemKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaYear = new FixedLengthStringData(4).isAPartOf(wsaaItemKey, 0);
	private FixedLengthStringData wsaaMop = new FixedLengthStringData(1).isAPartOf(wsaaItemKey, 4);
	private FixedLengthStringData wsaaFact = new FixedLengthStringData(2).isAPartOf(wsaaItemKey, 5);
	private ZonedDecimalData monthLastDate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData initFreqDate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData effDate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData sub1 = new ZonedDecimalData(4, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaTotalBill = new PackedDecimalData(17, 2).init(ZERO);
	protected ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	private boolean initFlag = false;
	private boolean linsAvailable = false;
	private Linspf linspfObj = null;
	private Clbapf clbapf = null;
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	protected Hpadpf hpadpf; 
	private Boolean chkFlag = false;
	private boolean billChgFlag = false;

	private static final String BTPRO028 = "BTPRO028";
	protected PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0);
	private OverpfDAO overpfDAO =  getApplicationContext().getBean("overpfDAO", OverpfDAO.class);
	private boolean BTPRO028Permission  = false;
	private PackedDecimalData tempSuspAvail = new PackedDecimalData(17, 2).init(0);
	private boolean isAiaAusDirectDebit ;
	private boolean isBilldayUpdated;
	private RertpfDAO rertpfDAO =  getApplicationContext().getBean("rertpfDAO", RertpfDAO.class);
	private Map<String, List<Rertpf>> rertDatedMap;	
	protected PackedDecimalData wsaaTempDiff = new PackedDecimalData(14, 2);	
	private ZonedDecimalData wsaaOccdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaOccdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaOccdateMm = new ZonedDecimalData(2, 0).isAPartOf(filler, 4).setUnsigned();
	private ZonedDecimalData wsaaOccdateDd = new ZonedDecimalData(2, 0).isAPartOf(filler, 6).setUnsigned();
	private ZonedDecimalData wsaaPtdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(wsaaPtdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaPtdateYy = new ZonedDecimalData(4, 0).isAPartOf(filler1, 0).setUnsigned();
	private ZonedDecimalData wsaaNextAnndate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(wsaaNextAnndate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaNextAnndateYy = new ZonedDecimalData(4, 0).isAPartOf(filler2, 0).setUnsigned();
	private ZonedDecimalData wsaaNextAnndateMm = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4).setUnsigned();
	private ZonedDecimalData wsaaNextAnndateDd = new ZonedDecimalData(2, 0).isAPartOf(filler2, 6).setUnsigned();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Freqcpy freqcpy = new Freqcpy();
	protected Premiumrec premiumrec = new Premiumrec();
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private int deffDays = 0;
	private int wsaaDeffDays = 0;
	protected T5687rec t5687rec = new T5687rec();
	private T5675rec t5675rec = new T5675rec();
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0);
	private AnnypfDAO annypfDAO = getApplicationContext().getBean("annypfDAO", AnnypfDAO.class);
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO", RcvdpfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private boolean stampDutyflag;
	private ZonedDecimalData catchupPrem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData catchupPremFee = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaactualPTD = new ZonedDecimalData(8, 0);
	private boolean BTPRO036Permission  = false;
	private static final String BTPRO036 = "BTPRO036";
	private boolean newPremiumFlag;
	private PackedDecimalData wsaaNewIncreaseDue = new PackedDecimalData(14, 2);
	
	private String wsaaWaiveTax = "";

	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1899,
		a3020ReadTr384,
		h190Exit
	}

	public B5349() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void  readTd5eeTable() {
	
}

protected void restart0900()
	{
		/*RESTART*/
		/** Restarting of this program is handled by MAINB,*/
		/** using a restart method of '3'.*/
		/*EXIT*/
	}

protected void initialise1000(){
	BTPRO028Permission  = FeaConfg.isFeatureExist("2", "BTPRO028", appVars, "IT");
	strEffDate = bsscIO.getEffectiveDate().toString();	
	intBatchID = 0;
	intBatchStep = 0;
	noRecordFound = false;
	payxList = new ArrayList<PayxTemppf>();
	covrListMap = new HashMap<String, List<Covrpf>>();
	acblListMap = new HashMap<String, List<Acblpf>>();
	payrListMap = new HashMap<String, List<Payrpf>>();
	incrListMap = new HashMap<String, List<Incrpf>>();
	fpcoListMap = new HashMap<String, List<Fpcopf>>();
	fprmListMap = new HashMap<String, List<Fprmpf>>();
	ddsuListMap = new HashMap<String, List<Ddsupf>>();
	hdisListMap = new HashMap<String, List<Hdispf>>();
	hdivcshListMap = new HashMap<String, List<Hdivpf>>();
	hcsdListMap = new HashMap<String, List<Hcsdpf>>();	
	covtListMap = new HashMap<String, List<Covtpf>>();
	rertDatedMap = new HashMap<String, List<Rertpf>>();
	
	if (isNE(bprdIO.getRestartMethod(), "3")) {
		syserrrec.statuz.set(ivrm);
		fatalError600();
	}
	
	//IBPTE-74 starts
	isBatchSkipFeatureEnabled = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "BTPRO025", appVars, "IT");
	if(isBatchSkipFeatureEnabled) {
		setSkippedEntities(getAlreadySkippedEntities("CH"));
	}
	//IBPTE-74 ends
    billChgFlag = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "CSBIL005", appVars, "IT");//IBPLIFE-4822
	
	if (bprdIO.systemParam01.isNumeric())
		if (bprdIO.systemParam01.toInt() > 0)
			intBatchExtractSize = bprdIO.systemParam01.toInt();
		else
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
	else
		intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();	
	
	StringBuilder payxTempTableName = new StringBuilder("");
	payxTempTableName.append("PAYX");
	payxTempTableName.append(bprdIO.getSystemParam04().toString().trim().toUpperCase());
	String scheduleNumber = bsscIO.getScheduleNumber().toString();
	scheduleNumber = scheduleNumber.substring(scheduleNumber.length()-4);
	payxTempTableName.append(String.format("%04d", Integer.parseInt(scheduleNumber.trim())));
	
	threadNumber = new StringBuilder("");
	threadNumber.append("THREAD");
	threadNumber.append(String.format("%03d", Integer.parseInt(bsprIO.getProcessOccNum().toString().trim())));
	japanBilling = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), BTPRO027, appVars, "IT");
	isAiaAusDirectDebit = FeaConfg.isFeatureExist(bsprIO.getCompany().trim(), "BTPRO029", appVars, "IT");		 		
	//int extractRows = b5349DAO.populateB5349Temp(intBatchExtractSize, payxTempTableName.toString(), threadNumber.toString());
	int extractRows = populateB5349Temp1000(payxTempTableName);
	if (extractRows > 0) {
		if (!performDataChunk()){
			wsspEdterror.set(SPACES);
			return;
		}
		reinstflag = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "CSLRI003", appVars, "IT");
		String prmhldtradItem = "CSOTH010";	//ILIFE-8179
		prmhldtrad = FeaConfg.isFeatureExist(batcdorrec.company.toString(), prmhldtradItem, appVars, "IT");
		if (!payxList.isEmpty()){
			initialise1010();
			readSmartTables(bsprIO.getCompany().toString().trim());
			readT5679();
			readT5645();
			readT3695(t5645rec.sacstype01.toString());
			readTd5eeTable();
		}			
	}
	else{
		LOGGER.info("No Medipay records retrieved for processing.");
		noRecordFound = true;
		return;
	}		
}

protected int populateB5349Temp1000(StringBuilder payxTempTableName){
	if(japanBilling){
		if (isEQ(bprdIO.getSystemParam05(), "JPN"))
		return b5349DAO.populateB5349TempJpn(intBatchExtractSize, payxTempTableName.toString(), threadNumber.toString());
		else
		return b5349DAO.getB5349DataCount(threadNumber.toString());
	}
	else{
		if (isEQ(bprdIO.getSystemParam05(), "JPN"))
			return 0;
		else
			return b5349DAO.populateB5349Temp(intBatchExtractSize, payxTempTableName.toString(), threadNumber.toString());
	}
}

protected void readSmartTables(String company){
	t5679ListMap = itemDAO.loadSmartTable("IT", company, "T5679");
	t5645ListMap = itemDAO.loadSmartTable("IT", company, "T5645");
	t3695ListMap = itemDAO.loadSmartTable("IT", company, "T3695");
	t3620ListMap = itemDAO.loadSmartTable("IT", company, "T3620");
	t3629ListMap = itemDAO.loadSmartTable("IT", company, "T3629");
	t6654ListMap = itemDAO.loadSmartTable("IT", company, "T6654");
	t6687ListMap = itemDAO.loadSmartTable("IT", company, "T6687");
	t5729ListMap = itemDAO.loadSmartTable("IT", company, "T5729");
	tr52dListMap = itemDAO.loadSmartTable("IT", company, "TR52D");
	tr384ListMap = itemDAO.loadSmartTable("IT", company, "TR384");
	tr517ListMap = itemDAO.loadSmartTable("IT", company, "TR517");	
	tr52eListMap = itemDAO.loadSmartTable("IT", company, "TR52E");	
	if(reinstflag){
	td5j2ListMap = itemDAO.loadSmartTable("IT", company, "TD5J2");	
	td5j1ListMap = itemDAO.loadSmartTable("IT", company, "TD5J1");	
	}
	if(prmhldtrad)	//ILIFE-8179
		ta524Map = itemDAO.getItemMap("IT", company, "TA524");
	if (japanBilling) {
			th5agListMap = itemDAO.loadSmartTable("IT", bsprIO.getFsuco().toString().trim(), "TH5AG");
			t3615ListMap = itemDAO.loadSmartTable("IT", company, "T3615");
			t5688ListMap = itemDAO.loadSmartTable("IT", company, "T5688");
			t5674ListMap = itemDAO.loadSmartTable("IT", company, "T5674");
		}
	BTPRO036Permission = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), BTPRO036, appVars, "IT");

	if(BTPRO036Permission){
		t5688ListMap = itemDAO.loadSmartTable("IT", company, "T5688");
		t5674ListMap = itemDAO.loadSmartTable("IT", company, "T5674");
	}
}

protected void readT5679(){
	String keyItemitem = bprdIO.getAuthCode().toString().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5679ListMap.containsKey(keyItemitem)){	
		itempfList = t5679ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(t5679rec.t5679Rec);
		syserrrec.statuz.set(h205);
		fatalError600();		
	}	
}

protected void readT5645(){
	String keyItemitem = wsaaProg.toString().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5645ListMap.containsKey(keyItemitem)){	
		itempfList = t5645ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(t5645rec.t5645Rec);
		syserrrec.statuz.set(h134);
		fatalError600();		
	}	
}

protected void readT3695(String sacsType){
	String keyItemitem = sacsType.trim();/* IJTI-1386 */
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t3695ListMap.containsKey(keyItemitem)){	
		itempfList = t3695ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t3695rec.t3695Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				t3695rec.t3695Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(t3695rec.t3695Rec);
		syserrrec.statuz.set(h420);
		fatalError600();		
	}	
}

protected void readT3620(){
	String keyItemitem = payxTemppf.getBillchnl().trim();/* IJTI-1386 */
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t3620ListMap.containsKey(keyItemitem)){	
		itempfList = t3620ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		int ctr = 0;
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t3620rec.t3620Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					ctr++;
					wsaaT3620Billchnl[ctr].set(itempf.getItemitem());
					wsaaT3620Ddind[ctr].set(t3620rec.ddind);
					wsaaT3620Crcind[ctr].set(t3620rec.crcind);	
					itemFound = true;
				}
			}else{
				t3620rec.t3620Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				ctr++;
				wsaaT3620Billchnl[ctr].set(itempf.getItemitem());
				wsaaT3620Ddind[ctr].set(t3620rec.ddind);
				wsaaT3620Crcind[ctr].set(t3620rec.crcind);	
				itemFound = true;					
			}				
		}		
	}	
	if (!itemFound) {
		syserrrec.params.set(t3620rec.t3620Rec);
		syserrrec.statuz.set(f921);
		fatalError600();		
	}
}

protected void readT3629(){
	String keyItemitem = b5349DTO.getPayrbillcurr().trim();/* IJTI-1386 */
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t3629ListMap.containsKey(keyItemitem)){	
		itempfList = t3629ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t3629rec.t3629Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					strT3629BankCode  = t3629rec.bankcode.toString();
					itemFound = true;
				}
			}else{
				t3629rec.t3629Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				// ILIFE-5629
				strT3629BankCode  = t3629rec.bankcode.toString();
				itemFound = true;					
			}				
		}		
	}	
	if (!itemFound) {
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("t3629");
		stringVariable1.addExpression(b5349DTO.getPayrbillcurr());
		stringVariable1.setStringInto(wsysSysparams);
		syserrrec.params.set(wsysSystemErrorParams);
		fatalError600();		
	}
}

protected void readT3615(String billingChnl) {

		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (t3615ListMap.containsKey(billingChnl)) {
			itempfList = t3615ListMap.get(billingChnl);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if (Integer.parseInt(itempf.getItmfrm().toString()) > 0) {
					if ((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()))
							&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())) {
						t3615rec.t3615Rec.set(StringUtil.rawToString(itempf.getGenarea()));
						itemFound = true;
					}
				} else {
					t3615rec.t3615Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;
				}
			}
		}
		if (!itemFound) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression("t3615");
			stringVariable1.addExpression(billingChnl);
			stringVariable1.setStringInto(wsysSysparams);
			syserrrec.params.set(wsysSystemErrorParams);
			fatalError600();
		}
	}

protected void readT6654(){
	BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
	if (BTPRO028Permission) {
		String keyItemitem	= payxTemppf.getBillchnl().trim().concat(b5349DTO.getCnttype().trim()).concat(b5349DTO.getBillfreq().trim());
	    if (!readT665401(keyItemitem)) {
	    	keyItemitem	= payxTemppf.getBillchnl().trim().concat(b5349DTO.getCnttype().trim()).concat("**");
	    	if (!readT665401(keyItemitem)) {
	    		keyItemitem	= payxTemppf.getBillchnl().trim().concat("*****");
	    		if (!readT665401(keyItemitem)) {
	    			syserrrec.params.set(t6654rec.t6654Rec);
					fatalError600();
	    		}
	    	}
	    }
	}
	else {
		String keyItemitem = payxTemppf.getBillchnl().trim().concat(b5349DTO.getCnttype().trim());
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (t6654ListMap.containsKey(keyItemitem)){	
			itempfList = t6654ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
							&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
						t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
						intT6654Leaddays = t6654rec.leadDays.toInt();
						itemFound = true;
					}
				}else{
					t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					intT6654Leaddays = t6654rec.leadDays.toInt();
					itemFound = true;					
				}				
			}		
		}
		if (!itemFound) {
			keyItemitem = payxTemppf.getBillchnl().trim().concat("***");/* IJTI-1386 */
			itempfList = new ArrayList<Itempf>();
			boolean itemFound2 = false;
			if (t6654ListMap.containsKey(keyItemitem)){	
				itempfList = t6654ListMap.get(keyItemitem);
				Iterator<Itempf> iterator = itempfList.iterator();
				while (iterator.hasNext()) {
					Itempf itempf = new Itempf();
					itempf = iterator.next();
					if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
						if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
								&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
							t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
							intT6654Leaddays = t6654rec.leadDays.toInt();
							itemFound2 = true;
						}
					}else{
						t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
						intT6654Leaddays = t6654rec.leadDays.toInt();
						itemFound2 = true;					
					}				
				}		
			}		
			if (!itemFound2) {
				syserrrec.params.set(t6654rec.t6654Rec);
				fatalError600();		
			}		
		}	
	}
}

protected boolean readT665401(String keyItemitem) {	
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t6654ListMap.containsKey(keyItemitem)){	
		itempfList = t6654ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
					intT6654Leaddays = t6654rec.leadDays.toInt();
					itemFound = true;
				}
			}else{
				t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				intT6654Leaddays = t6654rec.leadDays.toInt();
				itemFound = true;					
			}				
		}		
	}
	return itemFound;
}

protected void readT6687(){
	String keyItemitem = b5349DTO.getTaxrelmth().trim();/* IJTI-1386 */
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t6687ListMap.containsKey(keyItemitem)){	
		itempfList = t6687ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t6687rec.t6687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					strT6687TaxRelSub = t6687rec.taxrelsub.toString();

					itemFound = true;
				}
			}else{
				t6687rec.t6687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				strT6687TaxRelSub = t6687rec.taxrelsub.toString();
				itemFound = true;					
			}				
		}		
	}	
	if (!itemFound) {
		//readT5729();	
	}
}

private boolean searchItem(Map<String, List<Itempf>> listMap, String key, int date){
	String keyItemitem = key.trim();/* IJTI-1386 */
	List<Itempf> itempfList = new ArrayList<Itempf>();
	searchItemFound = false;
	if (listMap.containsKey(keyItemitem)){	
		itempfList = listMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (!searchItemFound && iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if (Integer.parseInt(itempf.getItmfrm().toString()) <= date 
					&& Integer.parseInt(itempf.getItmto().toString()) >= date){
					searchItemFound = true;
				}
			}			
		}		
	}
	return searchItemFound;
}

protected void readTr52d(){
	String keyItemitem = b5349DTO.getReg().trim();/* IJTI-1386 */
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (tr52dListMap.containsKey(keyItemitem)){	
		itempfList = tr52dListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		keyItemitem = "***".trim();/* IJTI-1386 */
		itempfList = new ArrayList<Itempf>();
		boolean itemFound2 = false;
		if (tr52dListMap.containsKey(keyItemitem)){	
			itempfList = tr52dListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
							&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
						tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));					
						itemFound2 = true;
					}
				}else{
					tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound2 = true;					
				}				
			}		
		}		
		if (!itemFound2) {
			syserrrec.params.set(tr52drec.tr52dRec);
			fatalError600();		
		}		
	}	
}

protected void readTr384(){
	String keyItemitem = b5349DTO.getCnttype().trim().concat(batcdorrec.trcde.toString().trim());/* IJTI-1386 */
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (tr384ListMap.containsKey(keyItemitem)){	
		itempfList = tr384ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					tr384rec.tr384Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				tr384rec.tr384Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		keyItemitem = "***".trim().concat(batcdorrec.trcde.toString().trim());/* IJTI-1386 */
		itempfList = new ArrayList<Itempf>();
		boolean itemFound2 = false;
		if (tr384ListMap.containsKey(keyItemitem)){	
			itempfList = tr384ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
							&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
						tr384rec.tr384Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
						itemFound2 = true;
					}
				}else{
					tr384rec.tr384Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound2 = true;					
				}				
			}		
		}		
		if (!itemFound2) {
			syserrrec.params.set(tr384rec.tr384Rec);
			syserrrec.statuz.set(g437);
			fatalError600();	
		}		
	}	
}

protected boolean readTr517(String itemKey, Integer crrCd){
	String keyItemitem = itemKey.trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (tr517ListMap.containsKey(keyItemitem)){	
		itempfList = tr517ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((crrCd >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& crrCd <= Integer.parseInt(itempf.getItmto().toString())){
					tr517rec.tr517Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
					wsaaTr517Item.set(itempf.getItemitem());
					itemFound = true;
					break;
				}
			}else{
				tr517rec.tr517Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				wsaaTr517Item.set(itempf.getItemitem());
				break;				
			}				
		}		
	}	
	return itemFound;	
}
protected boolean readTr52e(String itemKey, Integer effDate){
	String keyItemitem = itemKey.trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (tr52eListMap.containsKey(keyItemitem)){	
		itempfList = tr52eListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((effDate >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& effDate <= Integer.parseInt(itempf.getItmto().toString())){
					tr52erec.tr52eRec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
					break;
				}
			}else{
				tr52erec.tr52eRec.set(StringUtil.rawToString(itempf.getGenarea()));	
				itemFound = true;
				break;
			}				
		}		
	}
	return itemFound;	
}

protected List<PayxTemppf> loadDataByBatch(){
	return b5349DAO.loadDataByBatch(intBatchID, threadNumber.toString());
}

protected boolean performDataChunk(){
	boolean doPerform = true;
	intBatchID = intBatchStep;		
	if (payxList.size() > 0) payxList.clear();
	payxList = loadDataByBatch();
	if (payxList.isEmpty()) wsspEdterror.set(varcom.endp);
	else {
		iteratorList = payxList.iterator();
		
        Set<String> chdrnumSet = new HashSet<String>();
        Set<String> clntnumSet = new HashSet<String>();
        for(PayxTemppf payxTemp:payxList){
            chdrnumSet.add(payxTemp.getChdrnum());
            b5349DTO = new B5349DTO();
            b5349DTO = payxTemp.getB5349DTO();
            prfrnDataChnkCustomerSpecific(b5349DTO);
            if (!clntnumSet.contains(b5349DTO.getClntnum())) clntnumSet.add(b5349DTO.getClntnum());
        }
        
        if (chdrnumSet == null || chdrnumSet.isEmpty())  doPerform = false;

        List<String> chdrList = new ArrayList<String>(chdrnumSet);
        List<String> clntList = new ArrayList<String>(clntnumSet);
        covrListMap = covrpfDAO.searchCovrMap(bsprIO.getCompany().toString().trim(), chdrList);		
        acblListMap = acblpfDAO.searchAcblRecord(bsprIO.getCompany().toString().trim(), chdrList);
        payrListMap = payrpfDAO.getPayrLifMap(bsprIO.getCompany().toString().trim(), chdrList);
        incrListMap = incrpfDAO.getIncrgpMap(bsprIO.getCompany().toString().trim(), chdrList);
        fpcoListMap = fpcopfDAO.getFpcoMap(bsprIO.getCompany().toString().trim(), chdrList);
        fprmListMap = fprmpfDAO.getFprmMap(bsprIO.getCompany().toString().trim(), chdrList);
        ddsuListMap = ddsupfDAO.getDdsurnlMap(bsprIO.getFsuco().toString().trim(), clntList);
        hdisListMap = hdispfDAO.getHdisMap(bsprIO.getCompany().toString().trim(), chdrList);
        hdivcshListMap = hdivpfDAO.getHdivcshMap(bsprIO.getCompany().toString().trim(), chdrList);
        hcsdListMap = hcsdpfDAO.getHcsdMap(bsprIO.getCompany().toString().trim(), chdrList);
		covtListMap = covtpfDAO.searchCovt(bsprIO.getCompany().toString().trim(), chdrList);
		rertDatedMap = rertpfDAO.searchRertRecordByChdrnum(bsprIO.getCompany().toString().trim(), chdrList);
	
	}	
	return doPerform;
}

protected void prfrnDataChnkCustomerSpecific(B5349DTO b5349DTOtemp){
	
}

protected void initialise1010(){
	t5679ListMap = new HashMap<String, List<Itempf>>();
	t5645ListMap = new HashMap<String, List<Itempf>>();
	t3695ListMap = new HashMap<String, List<Itempf>>();
	t3620ListMap = new HashMap<String, List<Itempf>>();
	t3629ListMap = new HashMap<String, List<Itempf>>();
	t6654ListMap = new HashMap<String, List<Itempf>>();
	t6687ListMap = new HashMap<String, List<Itempf>>();
	t5729ListMap = new HashMap<String, List<Itempf>>();
	tr52dListMap = new HashMap<String, List<Itempf>>();
	tr384ListMap = new HashMap<String, List<Itempf>>();
	tr517ListMap = new HashMap<String, List<Itempf>>();
	tr52eListMap  = new HashMap<String, List<Itempf>>();
	
	chdrBulkUpdtList = new ArrayList<Chdrpf>();	
	payrBulkUpdList = new ArrayList<Payrpf>();	
	ptrnBulkInsList = new ArrayList<Ptrnpf>();
	linsBulkInsList = new ArrayList<Linspf>();
	fpcoBulkUpdList = new ArrayList<Fpcopf>();
	fprmBulkUpdList = new ArrayList<Fprmpf>();
	taxdBulkInsList = new ArrayList<Taxdpf>();
	hdivBulkInsList = new ArrayList<Hdivpf>();
	hpadBulkUpdtList = new ArrayList<Hpadpf>();
	
	isBulkProcessLinsPF = false;
	isBulkProcessFpcoPF = false;
	isBulkProcessTaxdPF = false;
	isBulkProcessHdivPF = false;
	isBulkProcessFprmPF = false;
	isBulkProcessHpadPF = false;
	
	currPayrLif = null;
	
	ctrCT01=0; 
	ctrCT02=0;
	ctrCT03=0; 
	ctrCT04 = BigDecimal.ZERO;
	ctrCT05=0; 
	ctrCT06 = BigDecimal.ZERO;
	ctrCT07=0; 
	ctrCT08=0;
	ctrCT09=0; 
	ctrCT10 = BigDecimal.ZERO;
	ctrCT11=0; 
	ctrCT12 = BigDecimal.ZERO;
	
	intT6654Leaddays=0;
	strT6687TaxRelSub="";
	TotalAmount = BigDecimal.ZERO;
}

protected void readFile2000(){
	if (!payxList.isEmpty()){
		if (!iteratorList.hasNext()) {
			intBatchStep++;	
			if (!performDataChunk()) {
				wsspEdterror.set(SPACES);
				return;
			}
			if (!payxList.isEmpty()){
				payxTemppf = new PayxTemppf();
				payxTemppf = iteratorList.next();
				b5349DTO = payxTemppf.getB5349DTO();
				rdFl2000CustomerSpecific();
				ctrCT01++;
			}
		}else {		
			payxTemppf = new PayxTemppf();
			payxTemppf = iteratorList.next();
			b5349DTO = payxTemppf.getB5349DTO();
			rdFl2000CustomerSpecific();
			ctrCT01++;
		}	
		/*  Set up the key for the SYSR- copybook, should a system error*/
		/*  for this instalment occur.*/
		wsysChdrnum.set(payxTemppf.getChdrnum());
		wsysBillcd.set(payxTemppf.getBillcd());			
	}else wsspEdterror.set(varcom.endp);		
}

protected void rdFl2000CustomerSpecific(){
	
}

protected void edit2500(){
	validateChdr2540();
	if (!validContract.isTrue()) {
		ctrCT07++;
		wsspEdterror.set(SPACES);
		return ;
	}	
	chdrpf = new Chdrpf();
	if (japanBilling) {
		initFlag = false;
		wsaaYear.set(batcdorrec.actyear);
		wsaaMop.set(b5349DTO.getChdrbillchnl());
		wsaaFact.set(b5349DTO.getFacthous());
		readTh5ag();
		monthLastDate.set(getMonthEnd(bsscIO.getEffectiveDate().toString()));
		sub1.set(bsscIO.getEffectiveDate().toString().substring(4, 6));
		if (isLT(bsscIO.getEffectiveDate(), th5agrec.prcbilldte[sub1.toInt()])
				|| !isLTE(b5349DTO.getPtdate(), monthLastDate)) {
			wsspEdterror.set(SPACES);
			return;
		}
		initFreqDate.set(getAdvanceDate(b5349DTO.getOccdate()));
		if (isEQ(b5349DTO.getPtdate(),ZERO) || isLTE(b5349DTO.getPtdate(),b5349DTO.getOccdate())) {
			if (isEQ(bprdIO.getSystemParam05(), "JPN")) {
				if(isNE(b5349DTO.getChdrbillchnl(),"D")){
					wsspEdterror.set(SPACES);
					return;
				}
				initFlag = true;
				readT3615(b5349DTO.getSrcebus());
				if(isEQ(b5349DTO.getChdrbillchnl(),"D") 
						&& isNE(t3615rec.onepcashless,"Y") 
						&& isNE(b5349DTO.getRcopt(),"Y")){
					wsspEdterror.set(SPACES);
					return;
				}
			} else {
				wsspEdterror.set(SPACES);
				return;
			}
		} else {
			if (isEQ(bprdIO.getSystemParam05(), "JPN")) {
				wsspEdterror.set(SPACES);
				return;
			}
		}
	} else {
		if (isEQ(bprdIO.getSystemParam05(), "JPN")) {
			wsspEdterror.set(SPACES);
			return;
		}
	}
	if(!japanBilling){
	if (searchItem(t5729ListMap, b5349DTO.getCnttype(), b5349DTO.getOccdate())) flexiblePremiumContract.setTrue();
	calcLeadDays();
	}
	else{
		if(!initFlag)
			wsaaEffdatePlusCntlead.set(monthLastDate);
	}
	/*    Once we have calculated the lead days for this contract      */
	/*    type we can compare it to the BILLCD date to see if the      */
	/*    contract requires billing.                                   */
		if (!initFlag) {
			if (isGT(payxTemppf.getBillcd(), wsaaEffdatePlusCntlead)) {
				wsspEdterror.set(SPACES);
				return;
			}
			//PINNACLE-2990 - Start
			if (isGT(payxTemppf.getBillcd(), bsscIO.getEffectiveDate())) {
				if (isEQ(b5349DTO.getOccdate(), b5349DTO.getBtdate())) {
					wsspEdterror.set(SPACES);
					return;
				}
			}//PINNACLE-2990 - End
		}
	/* If suppressed billing, check if the present bill date is within*/
	/* the suppressed period.*/
	if (isEQ(payxTemppf.getBillsupr(), "Y")) {
		/*     IF  BILLSPFROM     NOT > WSAA-EFFDATE-PLUS-CNTLEAD*/
		/*     AND BILLSPTO       NOT < WSAA-EFFDATE-PLUS-CNTLEAD*/
		if (isGTE(payxTemppf.getBillcd(), payxTemppf.getBillspfrom())
		&& isLT(payxTemppf.getBillcd(), payxTemppf.getBillspto())
		&& isLT(bsscIO.getEffectiveDate(), payxTemppf.getBillspto())) {
			ctrCT02++;
			wsspEdterror.set(SPACES);
			return ;
		}
	}	

	
	if (BTPRO028Permission) {
		Overpf overpf = overpfDAO.getOverpfRecord(payxTemppf.getChdrcoy(), payxTemppf.getChdrnum(), "1");
		Acblpf acblpf = acblpfDAO.loadDataByBatch(payxTemppf.getChdrcoy(),"LP",payxTemppf.getChdrnum(),b5349DTO.getBillcurr(), "S");
	    if (null!= acblpf) {
			if (isEQ(t3695rec.sign, "-")) {
				compute(tempSuspAvail, 2).set((sub(0, acblpf.getSacscurbal())));
			}
			else {
				tempSuspAvail.set(acblpf.getSacscurbal());
			}    	
	    }	
		if (isLT(tempSuspAvail, b5349DTO.getSinstamt06())) {
		    if(!isAiaAusDirectDebit) { //PINNACLE-2582
			if(null!= overpf && isEQ(overpf.getOverdueflag(),"O") && isEQ(overpf.getEnddate(),varcom.vrcmMaxDate )){
				wsspEdterror.set(SPACES);
				return ;
			}
		}
		}
	}
	

	readTr52d();
	softlock2580();
	/*  Log CHDRs locked*/
	checkSoftLock();// IBPTE-74
}

	// IBPTE-74 starts
	private void checkSoftLock() {
		boolean isAlreadyAddedSkippedEntity = false;
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			ctrCT08++;
			wsspEdterror.set(SPACES);
			// store skipped entity
			if (isBatchSkipFeatureEnabled) {
				addSkippedEntity("CH", payxTemppf.getChdrnum());
				isAlreadyAddedSkippedEntity = true;
			}
		}
	
		// If entity was skipped by previous batch due to soft lock
     	// and by the time this batch is run and entity was released from soft lock, 
     	// this batch should also skip this.
		if (isBatchSkipFeatureEnabled && isEntitySkippedBefore(payxTemppf.getChdrnum())) {
			wsspEdterror.set(SPACES);
			if (!isAlreadyAddedSkippedEntity) {
				addSkippedEntity("CH", payxTemppf.getChdrnum());
			}
		}
	
	}
	// IBPTE-74 ends

protected void validateChdr2540()
	{
		/*START*/
		/* Validate Contract status against T5679.*/
		wsaaValidChdr.set("N");
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)
		|| validContract.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Sub.toInt()], b5349DTO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)
				|| validContract.isTrue()); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.cnPremStat[wsaaT5679Sub.toInt()], b5349DTO.getPstcde())) {
						wsaaValidChdr.set("Y");
					}
				}
			}
		}
		/*EXIT1*/
	}

protected void calcLeadDays()
{
	readT6654();	
	/* Call 'DATCON2' to increment the effective date by the T6654*/
	/* LEAD DAYS.*/
	datcon2rec.freqFactor.set(intT6654Leaddays);
	datcon2rec.frequency.set("DY");
	datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon2rec.datcon2Rec);
		syserrrec.statuz.set(datcon2rec.statuz);
		fatalError600();
	}
	wsaaEffdatePlusCntlead.set(datcon2rec.intDate2);
}



protected void softlock2580()
	{
		start2580();
	}

protected void start2580()
	{
		/* Soft lock the contract, if it is to be processed.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(payxTemppf.getChdrcoy());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(payxTemppf.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			wsysSysparams.set(sftlockrec.sftlockRec);
			syserrrec.params.set(wsysSystemErrorParams);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void readTh5ag() {

		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (th5agListMap.containsKey(wsaaItemKey.toString().trim())) {
			itempfList = th5agListMap.get(wsaaItemKey.toString().trim());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				th5agrec.th5agRec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;
			}
		}
		if (!itemFound) {
			wsaaFact.set("**");
			if (th5agListMap.containsKey(wsaaItemKey)) {
				itempfList = th5agListMap.get(wsaaItemKey);
				Iterator<Itempf> iterator = itempfList.iterator();
				while (iterator.hasNext()) {
					Itempf itempf = new Itempf();
					itempf = iterator.next();
					th5agrec.th5agRec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;
				}
			}
		}
		if (!itemFound) {
			syserrrec.params.set("TH5AG".concat(wsaaItemKey.toString()));
			syserrrec.statuz.set(h134);
			fatalError600();
		}

	}

	protected String getMonthEnd(String date) {

		int lengthMonth = YearMonth
				.of(Integer.valueOf(date.substring(0, 4).trim()), Integer.valueOf(date.substring(4, 6).trim()))
				.lengthOfMonth();
		return date.substring(0, 6).concat(String.format("%d", lengthMonth));

	}

	protected int getAdvanceDate(int date) {
		readT6654();
		wsaaSub.set(1);
		if(!BTPRO028Permission) {
		for (wsaaSub.set(1); !(isGT(wsaaSub, 6)); wsaaSub.add(1)){
			if(isEQ(t6654rec.zrfreq[wsaaSub.toInt()], b5349DTO.getBillfreq())){
				break;
			}
		}
		}
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(date);
		datcon2rec.frequency.set(b5349DTO.getBillfreq());
		datcon2rec.freqFactor.set(t6654rec.zrincr[wsaaSub.toInt()]);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError600();
		}
		return datcon2rec.intDate2.toInt();
	}

protected void update3000(){
	readPayr();
	readAcbl();	
	if(prmhldtrad && ta524Map.get(b5349DTO.getCnttype()) != null) {//ILIFE-8509
		prmhpf = prmhpfDAO.getPrmhRecord(payxTemppf.getChdrcoy(), payxTemppf.getChdrnum());
		if(prmhpf != null && bsscIO.getEffectiveDate().toInt()<=prmhpf.getTodate() && !billWritten) {
			wsaaOldBtdate.set(wsaaOldBillcd);
			payrpf.setBtdate(getAnnptdt(wsaaOldBtdate.toInt()));
		}
	}
	if (b5349DTO.getSinstamt05().compareTo(BigDecimal.ZERO) != 0 
	&& isNE(tr52drec.txcode, SPACES)) {
		wsaaWopFound.set("N");
		wsaaTr517Item.set(SPACES);
		String chdrnum = payxTemppf.getChdrnum();
        if (covrListMap != null && covrListMap.containsKey(chdrnum)) {
            List<Covrpf> covrpfList = covrListMap.get(chdrnum);
            for (Iterator<Covrpf> iterator = covrpfList.iterator(); iterator.hasNext(); ){
            	Covrpf covr = iterator.next();
            		if (covr.getValidflag().equals("1") && checkWop(covr)) break;    				
                }        	
            }                
        }	

	wsaaFirstBill = "Y"; //:PErformance Need to check  uncomment this line as it will impact when more that one payrpf runs  in same time.value set for first
	   //payrpf will retain when this process second payrpf 
	boolean isbillday = FeaConfg.isFeatureExist(payxTemppf.getChdrcoy(), "NBPRP011", appVars, "IT");/* IJTI-1386 */
	boolean leaddayFeature = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "CSBIL006", appVars, "IT");

	if(isbillday && payrpf.getBillfreq() != null && isEQ(payrpf.getBillfreq(),"12")&&(isEQ(payrpf.getBillchnl(),"D")||isEQ(payrpf.getBillchnl(),"R")
			||isEQ(payrpf.getBillchnl(),"A")||isEQ(payrpf.getBillchnl(),"B"))){//ILIFE-8583
		if(!leaddayFeature) {
			wsaaEffdatePlusCntlead.set(bsscIO.getEffectiveDate());
			chkFlag= true;
		}else {
			chkFlag= true;
			datcon2rec.freqFactor.set(intT6654Leaddays);
			datcon2rec.frequency.set("DY");
			datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
			
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				fatalError600();
			}
			wsaaEffdatePlusCntlead.set(datcon2rec.intDate2);
		}
		
	}
	
	if(reinstflag){
    	ptrnrecords = ptrnpfDAO.searchPtrnenqRecord(payxTemppf.getChdrcoy(), payxTemppf.getChdrnum());
    	if(ptrnrecords != null){
    		for(Ptrnpf ptrn : ptrnrecords ){
    		if(isNE(ptrn.getBatctrcde(),ta85) && isNE(ptrn.getBatctrcde(),"B521")){
    			continue;
    		}
    		if(isEQ(ptrn.getBatctrcde(),"B521")){
    			break;
    		}
    		if(isEQ(ptrn.getBatctrcde(),ta85)){
    			 isFoundPro = true;
    			 covrpflist  = covrpfDAO.getCovrByComAndNum(bsprIO.getCompany().toString(), payxTemppf.getChdrnum());/* IJTI-1386 */
    			 incrDatedMap = new HashMap<Integer, List<Incrpf>>();
    			 incrDatedMap = incrpfDAO.getIncrDatedMap(bsprIO.getCompany().toString().trim(), payxTemppf.getChdrnum());/* IJTI-1386 */
    			 ptrnpfReinstate  = ptrnpfDAO.getPtrnData(payxTemppf.getChdrcoy(), payxTemppf.getChdrnum(), ta85);
    			 getLastPTD();
    			}
    		}
    	}
	}
	effDate.set(bsscIO.getEffectiveDate());
	if(initFlag){
		payxTemppf.setBillspfrom(b5349DTO.getOccdate());
		payxTemppf.setBillspto(initFreqDate.toInt());
		wsaaEffdatePlusCntlead.set(b5349DTO.getPayrbillcd());
		if(isEQ(b5349DTO.getPtdate(),ZERO)){
			effDate.set(b5349DTO.getOccdate());
		}
		else{
			effDate.set(initFreqDate.toInt());
		}
		linsAvailable = false;
		linspfObj = linspfDAO.getLinsRecordByCoyAndNumAndflag(payxTemppf.getChdrcoy(), payxTemppf.getChdrnum(), "1");
		if(null != linspfObj && isEQ(linspfObj.getInstfrom(),payxTemppf.getBillspfrom())){
			linsAvailable = true;
			return;
		}
	}	
	while (!(isGT(payrpf.getBillcd(), wsaaEffdatePlusCntlead)
				|| (isGTE(payrpf.getBillcd(), payxTemppf.getBillspfrom())
						&& isLT(payrpf.getBillcd(), payxTemppf.getBillspto())
						&& isLT(effDate, payxTemppf.getBillspto())))) {
		if (isbillday)
		{
			chkFlag =false;
		}
		//ILIFe-4688 Start
		if(trannoMap == null){
	        	trannoMap = new HashMap<>();
	        }
	        int curTranno = 0;
	        if(trannoMap.containsKey(payxTemppf.getChdrnum())){
	        	curTranno = trannoMap.get(payxTemppf.getChdrnum());
	        }else{
	        	curTranno = b5349DTO.getChdrtranno();
	        }
	    	curTranno++;
	    	trannoMap.put(payxTemppf.getChdrnum(), curTranno);
	    	chdrpf.setTranno(curTranno);
		
	//	chdrpf.setTranno(b5349DTO.getChdrtranno());
		payrpf.setTranno(curTranno);
		a1000GetCurrConvDate();
		calcPayrPrem();
		advanceBtdate();
		if (wsaaDeffDays>0 && wsaaDeffDays <= 2) {
			List<Covrpf> covrpfList = covrListMap.get(payxTemppf.getChdrnum());
			Covrpf c = new Covrpf();
			if (covrpfList.isEmpty()) {
				
			} else {
				c = covrpfList.get(0);
				catchupPrem.set(ZERO);
				catchupPremFee.set(ZERO);
				callPmexSubroutine(c);
				initialize(mgfeelrec.mgfeelRec);
				mgfeelrec.policyRCD.set(premiumrec.occdate);
				mgfeelrec.bilfrmdt.set(premiumrec.bilfrmdt);
				mgfeelrec.biltodt.set(premiumrec.biltodt);				
				calcMgfeel();
				catchupPremFee.set(mgfeelrec.mgfee);
			}
		}
		calcTax();
		automaticIncrease();
		writeLins();
		produceBextRecord();
		writePtrnRecord();
		wsaaOldBtdateBk.set(wsaaOldBtdate);
		/*     Advance the old billed to date for subsequent billing*/
		wsaaOldBtdate.set(payrpf.getBtdate());
		/*     Add increase due and tax to outstanding amount              */
		if(!initFlag){
		chdrpf.setOutstamt(chdrpf.getOutstamt().add(BigDecimal.valueOf(wsaaTax.toDouble())));
		payrpf.setOutstamt(payrpf.getOutstamt().add(BigDecimal.valueOf(wsaaTax.toDouble())));
		setPrecision(chdrpf.getOutstamt(), 2);
		}
	}
	
	if (isGTE(payrpf.getBillcd(), payxTemppf.getBillspfrom())
	&& isLT(payrpf.getBillcd(), payxTemppf.getBillspto())
	&& isLT(bsscIO.getEffectiveDate(), payxTemppf.getBillspto())) {
		ctrCT02++;
	}
	rewriteChdr();
	if(!(initFlag && isEQ(b5349DTO.getPtdate(),ZERO))){
		rewritePayr();
	}

	if (initFlag) 
		updateinit();
	writeLetter();
	sendBillingNoticeSmsNotification();
	releaseSoftlock4000();	
	
}

protected void readTd5j2(String crtable){
	/*String keyItemitem = payxTemppf.getBillchnl().trim().concat(crtable.trim());*/
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (td5j2ListMap.containsKey(crtable)){	
		itempfList = td5j2ListMap.get(crtable);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					td5j2rec.td5j2Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
					itemFound = true;
				}
			}else{
				td5j2rec.td5j2Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
				itemFound = true;					
			}				
		}		
	}
	else {
		td5j2rec.td5j2Rec.set(SPACES);
	}	
}

protected void readTd5j1(String cnttype){
	/*String keyItemitem = payxTemppf.getBillchnl().trim().concat(crtable.trim());*/
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (td5j1ListMap.containsKey(cnttype)){	
		itempfList = td5j1ListMap.get(cnttype);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					td5j1rec.td5j1Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
					itemFound = true;
				}
			}else{
				td5j1rec.td5j1Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
				itemFound = true;					
			}				
		}		
	}
	else {
		td5j1rec.td5j1Rec.set(SPACES);
	}	
}

protected void getLastPTD(){
	
	lastpayrPTD.set(payrpf.getBillcd());
	ptdlapse.set(payrpf.getBillcd());
	lapsePTDate.set(payrpf.getBillcd());
	while(isGT(ptrnpfReinstate.getDatesub(),ptdlapse)){
		datcon2rec.datcon2Rec.set(SPACES);
		lapsePTDate.set(ptdlapse);
		datcon2rec.frequency.set(payrpf.getBillfreq());
		datcon2rec.intDate1.set(ptdlapse);
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		else {
			ptdlapse.set(datcon2rec.intDate2);
		}
	}
}
protected void calcprorated(){
	readTd5j1(b5349DTO.getCnttype());
	if(isNE(td5j1rec.td5j1Rec,SPACES)){
		if(isEQ(td5j1rec.subroutine,SPACES))
			return;	
	}
	 increaseprem.set(ZERO);
	 leaveprem.set(ZERO);
	 proratePrem.set(ZERO);
	 wsaaInstPrem.set(ZERO);
	 if(isGTE(lapsePTDate,payrpf.getBtdate())){
	 for(Covrpf covrpf : covrpflist ){
		 readTd5j2(covrpf.getCrtable());/* IJTI-1386 */
		 readIncr(covrpf);	
	 }	}
}

protected void readIncr(Covrpf covrpf)
{
	if(isNE(td5j2rec.td5j2Rec,SPACES) &&
			isEQ(td5j2rec.shortTerm,"Y")){
		     compute(leaveprem, 2).add(covrpf.getInstprem().doubleValue());
			 if(isEQ(lapsePTDate,payrpf.getBtdate())){
				 callSubroutine(covrpf);
			 }
			
		return;
	}
	
	Incrpf currIncr = null;
	int date = payrpf.getBtdate();
	boolean incrFound = false;
	if (incrDatedMap != null && incrDatedMap.containsKey(date)) {
		List<Incrpf> incrpfList = incrDatedMap.get(date);
		for (Iterator<Incrpf> iterator = incrpfList.iterator(); iterator.hasNext(); ){
			Incrpf incr = iterator.next();
			if (incr.getChdrnum().equals(covrpf.getChdrnum())
			&&	incr.getLife().equals(covrpf.getLife())
    		&&  incr.getCoverage().equals(covrpf.getCoverage())
    		&& 	incr.getRider().equals(covrpf.getRider())
    		&& Integer.compare(incr.getPlnsfx(),covrpf.getPlanSuffix()) == 0) {
    			currIncr = incr;
    			incrFound = true;
    			break;		
			}
		}
	}
	
	if(incrFound){
		compute(increaseprem, 2).add(sub(covrpf.getInstprem(), currIncr.getNewinst()));
	}
}

protected void callSubroutine(Covrpf covrpf){
	
	 callDatcon4500();
	 calprpmrec.reinstDate.set(ptrnpfReinstate.getDatesub());
	 calprpmrec.lastPTDate.set(payrpf.getPtdate());
	 calprpmrec.nextPTDate.set(nextPTDate);
	 calprpmrec.transcode.set(batcdorrec.trcde);
	 calprpmrec.uniqueno.set(covrpf.getUniqueNumber());
	 calprpmrec.prem.set(covrpf.getInstprem());
	 calprpmrec.chdrcoy.set(bsprIO.getCompany());
	 calprpmrec.chdrnum.set(payxTemppf.getChdrnum());
	 callProgram(td5j1rec.subroutine, calprpmrec.calprpmRec,null);
	 if (isNE(calprpmrec.statuz, varcom.oK)) {
			syserrrec.params.set(calprpmrec.statuz);
			fatalError600();
	 }
	 reratedPremKeeps.put(covrpf.getUniqueNumber(), calprpmrec.prem.getbigdata());
	 proratePrem.add(calprpmrec.prem);
	 /*lapsePTDate.set(calprpmrec.ptdlapse);*/
}

protected void callDatcon4500()
{
	datcon3rec.datcon3Rec.set(SPACES);
	datcon3rec.intDate1.set(lastpayrPTD);
	datcon3rec.intDate2.set(ptrnpfReinstate.getDatesub());
	datcon3rec.frequency.set(payrpf.getBillfreq());
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz,"****")) {
		syserrrec.params.set(datcon3rec.datcon3Rec);
		fatalError600();
	}
	wsaaInstOutst.set(datcon3rec.freqFactor);
	wsaaInstOutst.add(1);
	datcon2rec.datcon2Rec.set(SPACES);
	datcon2rec.intDate1.set(payrpf.getPtdate());
	datcon2rec.freqFactor.set(wsaaInstOutst);
	datcon2rec.frequency.set(payrpf.getBillfreq());
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz,Varcom.oK)) {
		syserrrec.params.set(datcon2rec.datcon2Rec);
		fatalError600();
	}
	nextPTDate.set(datcon2rec.intDate2);
}

protected void commitControlTotals(){
	//ct01
	contotrec.totno.set(controlTotalsInner.ct01);
	contotrec.totval.set(ctrCT01);
	callContot001();
	ctrCT01 = 0;
	
	//ct02
	contotrec.totno.set(controlTotalsInner.ct02);
	contotrec.totval.set(ctrCT02);
	callContot001();	
	ctrCT02 = 0;	
	
	//ct03
	contotrec.totno.set(controlTotalsInner.ct03);
	contotrec.totval.set(ctrCT03);
	callContot001();
	ctrCT03 = 0;
	
	//ct04
	String totCT04;
	totCT04 = ZonedDecimalData.toStringRawStaticBigDecimal(ctrCT04, 17, 2);
	contotrec.totno.set(controlTotalsInner.ct04);
	contotrec.totval.set(totCT04);
	callContot001();		
	ctrCT04 = BigDecimal.ZERO;	
	
	//ct05
	contotrec.totno.set(controlTotalsInner.ct05);
	contotrec.totval.set(ctrCT05);
	callContot001();	
	ctrCT05 = 0;	
	
	//ct06
	String totCT06;
	totCT06 = ZonedDecimalData.toStringRawStaticBigDecimal(ctrCT06, 17, 2);	
	contotrec.totno.set(controlTotalsInner.ct06);
	contotrec.totval.set(totCT06);
	callProgram(Contot.class, contotrec.contotRec);		
	ctrCT06 = BigDecimal.ZERO;	
	
	//ct07
	contotrec.totno.set(controlTotalsInner.ct07);
	contotrec.totval.set(ctrCT07);
	callContot001();	
	ctrCT07 = 0;	
	
	//ct08
	contotrec.totno.set(controlTotalsInner.ct08);
	contotrec.totval.set(ctrCT08);
	callContot001();	
	ctrCT08 = 0;	
	
	//ct09
	contotrec.totno.set(controlTotalsInner.ct09);
	contotrec.totval.set(ctrCT09);
	callContot001();
	ctrCT09 = 0;	
	
	//ct10
	String totCT10;
	totCT10 = ZonedDecimalData.toStringRawStaticBigDecimal(ctrCT10, 17, 2);	
	contotrec.totno.set(controlTotalsInner.ct10);
	contotrec.totval.set(totCT10);
	callContot001();	
	ctrCT10 = BigDecimal.ZERO;
	
	//ct11
	contotrec.totno.set(controlTotalsInner.ct11);
	contotrec.totval.set(ctrCT11);
	callContot001();	
	ctrCT11 = 0;	
	
	//ct12
	String totCT12;
	totCT12 = ZonedDecimalData.toStringRawStaticBigDecimal(ctrCT12, 17, 2);		
	contotrec.totno.set(controlTotalsInner.ct12);
	contotrec.totval.set(totCT12);
	callContot001();	
	ctrCT12 = BigDecimal.ZERO;
	
}

		
//ILIF-3997-STARTS
protected boolean isUpdateNlgt(String item){
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t6654ListMap.containsKey(item)){	
		itempfList = t6654ListMap.get(item);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
					 if(( t6654rec.daexpy01.toInt() == 0) && 
					 	( t6654rec.daexpy02.toInt() == 0) &&
					 	 ( t6654rec.daexpy03.toInt() == 0)){
						itemFound = true;
					}
				}
			}else{
				t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				if(( t6654rec.daexpy01.toInt() == 0) && 
					 	( t6654rec.daexpy02.toInt() == 0) &&
					 	( t6654rec.daexpy03.toInt() == 0)){
							itemFound = true;					
					 	}
			}				
		}		
	}
		
	if(itemFound){
		return true;
	}else{
		return false;
	}
}
//ILIF-3997-ENDS

protected void readPayr(){
	payrpf = new Payrpf();
	wsaaOldBtdate.set(b5349DTO.getBtdate());
	wsaaOldBillcd.set(b5349DTO.getPayrbillcd());
	wsaaOldNextdate.set(b5349DTO.getNextdate());
	/* Save original amount as this is not to be updated for   <D9604>*/
	/* flexible premium contracts                              <D9604>*/
	wsaaOldOutstamt.set(b5349DTO.getOutstamt());
	payrpf.setBtdate(b5349DTO.getBtdate());
	payrpf.setNextdate(b5349DTO.getNextdate());
	payrpf.setBillcd(b5349DTO.getPayrbillcd());
	payrpf.setBillchnl(b5349DTO.getChdrbillchnl());
	payrpf.setBillfreq(b5349DTO.getBillfreq());
	payrpf.setEffdate(b5349DTO.getEffdate());
	payrpf.setSinstamt01(b5349DTO.getSinstamt01());
	payrpf.setPtdate(b5349DTO.getPtdate());
	payrpf.setOrgbillcd(b5349DTO.getOrgbillcd());
	chdrpf.setCnttype(b5349DTO.getCnttype());
	payrpf.setOutstamt(new BigDecimal(0)); //PINNACLE-2663
	chdrpf.setOutstamt(new BigDecimal(0)); //PINNACLE-2663
	
}


protected void readAcbl(){
	wsaaSuspAvail.set(0);
	Acblpf currAcbl = null;
	String chdrnum = payxTemppf.getChdrnum();
	boolean recordFound = false;
    if (acblListMap != null && acblListMap.containsKey(chdrnum)) {
        List<Acblpf> acblpfList = acblListMap.get(chdrnum);
        for (Iterator<Acblpf> iterator = acblpfList.iterator(); iterator.hasNext(); ){
        	Acblpf acbl = iterator.next();
			if(initFlag){
				if (acbl.getOrigcurr().equals(b5349DTO.getPayrbillcurr())
						&& acbl.getSacscode().equals(t5645rec.sacscode06.toString().trim())
						&& acbl.getSacstyp().trim().equals(t5645rec.sacstype06.toString().trim())) {
					recordFound = true;
					currAcbl = acbl;
					break;
				}
			}
			else{
				if (acbl.getOrigcurr().equals(b5349DTO.getPayrbillcurr())
						&& acbl.getSacscode().equals(t5645rec.sacscode01.toString().trim())
						&& acbl.getSacstyp().trim().equals(t5645rec.sacstype01.toString().trim())) {
            		recordFound = true;
            		currAcbl = acbl;
            		break;
            }  
			}		
        }
    }
    /* Multiply the balance by the sign from T3695.*/
    if (recordFound) {
		if (isEQ(t3695rec.sign, "-")) {
			compute(wsaaSuspAvail, 2).set((sub(0, currAcbl.getSacscurbal())));
		}
		else {
			wsaaSuspAvail.set(currAcbl.getSacscurbal());
		}    	
    }	
    includeApa();
}
protected void includeApa()
{
	initialize(rlpdlonrec.rec);
	rlpdlonrec.function.set(varcom.info);
	rlpdlonrec.chdrcoy.set(payxTemppf.getChdrcoy());
	rlpdlonrec.chdrnum.set(wsysChdrnum);
	rlpdlonrec.prmdepst.set(ZERO);
	rlpdlonrec.language.set(bsscIO.getLanguage());
	callProgram(Rlpdlon.class, rlpdlonrec.rec);
	if (isNE(rlpdlonrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(rlpdlonrec.statuz);
		syserrrec.params.set(rlpdlonrec.rec);
		fatalError600();
	}
	wsaaSuspAvail.add(rlpdlonrec.prmdepst);
	/*                                                         <LA2106>*/
	/* Store LP S amount just read to WSAA-PREM-SUSP           <LA2106>*/
	wsaaPremSusp.set(wsaaSuspAvail);
	/* MOVE WSAA-PREM-SUSP         TO ZRDP-AMOUNT-IN.               */
	/* MOVE PAYR-BILLCURR          TO ZRDP-CURRENCY.                */
	/* PERFORM 8000-CALL-ROUNDING.                                  */
	/* MOVE ZRDP-AMOUNT-OUT        TO WSAA-PREM-SUSP.               */
	if (isNE(wsaaPremSusp, 0)) {
		zrdecplrec.amountIn.set(wsaaPremSusp);
		zrdecplrec.currency.set(b5349DTO.getPayrbillcurr());
		callRounding8000();
		wsaaPremSusp.set(zrdecplrec.amountOut);
	}
	/*                                                         <LA2106>*/
	/* Read ACBL for LC DS                                     <LA2106>*/
	Acblpf currAcbl = null;
	String chdrnum = payxTemppf.getChdrnum();
	boolean recordFound = false;
    if (acblListMap != null && acblListMap.containsKey(chdrnum)) {
        List<Acblpf> acblpfList = acblListMap.get(chdrnum);
        for (Iterator<Acblpf> iterator = acblpfList.iterator(); iterator.hasNext(); ){
        	Acblpf acbl = iterator.next();
            if (acbl.getOrigcurr().equals(b5349DTO.getChdrcntcurr()) && acbl.getSacscode().equals(t5645rec.sacscode03.toString().trim())
                    && acbl.getSacstyp().equals(t5645rec.sacstype03.toString().trim())) {
            		recordFound = true;
            		currAcbl = acbl;
            		break;
            }        	
        }
    }	
    
    if (!recordFound) {
		wsaaDvdSusp.set(0);
		return ;  	
    }	    
	
	if (isEQ(currAcbl.getSacscurbal(), 0)) {
		wsaaDvdSusp.set(0);
		return ;
	}
	
	readT3695(t5645rec.sacstype03.toString());
	if (isEQ(t3695rec.sign, "-")) {
		setPrecision(currAcbl.getSacscurbal(), 2);
		currAcbl.setSacscurbal(currAcbl.getSacscurbal().multiply(new BigDecimal(-1)));
	}
	/*                                                         <LA2106>*/
	/* Convert if billing ccy and contract ccy differs.        <LA2106>*/
	if (isNE(b5349DTO.getBillcurr(), b5349DTO.getChdrcntcurr())) {
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(b5349DTO.getChdrcntcurr());
		conlinkrec.amountIn.set(currAcbl.getSacscurbal());
		conlinkrec.cashdate.set(varcom.vrcmMaxDate);
		conlinkrec.currOut.set(b5349DTO.getBillcurr());
		conlinkrec.company.set(payxTemppf.getChdrcoy());
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.function.set("REAL");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, "****")) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		wsaaDvdSusp.set(conlinkrec.amountOut);
		if (isNE(wsaaDvdSusp, 0)) {
			zrdecplrec.amountIn.set(wsaaDvdSusp);
			zrdecplrec.currency.set(b5349DTO.getBillcurr());
			callRounding8000();
			wsaaDvdSusp.set(zrdecplrec.amountOut);
		}
	}
	else {
		wsaaDvdSusp.set(currAcbl.getSacscurbal());
	}
	/*                                                         <LA2106>*/
	/* Consolidate total suspense                              <LA2106>*/
	wsaaSuspAvail.add(wsaaDvdSusp);
}


private boolean checkWop(Covrpf covrpf){
	/* Check to see if coverage is found in TR517                      */
	boolean itemFound = readTr517(covrpf.getCrtable(), covrpf.getCrrcd());
	
	if (!itemFound) tr517rec.tr517Rec.set(SPACES);
	else{
		wsaaWopFound.set("Y");
	}
	return itemFound;
}

protected void advanceBtdate(){
	/* Advance the 'Billed to date' by one Frequency.*/
	/* The current saved BTDATE is used, so that the INSTFROM and*/
	/* BILLdates can be updated.*/
	datcon4rec.freqFactor.set(1);
	datcon2rec.freqFactor.set(1);
	datcon4rec.frequency.set(b5349DTO.getBillfreq());
	datcon2rec.frequency.set(b5349DTO.getBillfreq());
	datcon4rec.intDate1.set(payrpf.getBtdate());
	datcon2rec.intDate1.set(payrpf.getBtdate());
	datcon4rec.billday.set(b5349DTO.getDuedd());
	datcon4rec.billmonth.set(b5349DTO.getDuemm());
	callProgram(Datcon4.class, datcon4rec.datcon4Rec);
	if (isNE(datcon4rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon4rec.datcon4Rec);
		syserrrec.statuz.set(datcon4rec.statuz);
		fatalError600();
	}
	wsaaOldBtdate.set(payrpf.getBtdate());
	wsaaactualPTD .set(datcon4rec.intDate2.toInt());
	payrpf.setBtdate(getAnnptdt(datcon4rec.intDate2.toInt()));
	if(!(initFlag && isEQ(b5349DTO.getPtdate(),ZERO))){
		chdrpf.setBtdate(payrpf.getBtdate());
	}
	else{
		chdrpf.setBtdate(getAnnptdt(b5349DTO.getBtdate()));
	}
	
	wsaaDeffDays = deffDays;
	wsaaOldBillcd.set(payrpf.getBillcd());
	wsaaOldNextdate.set(payrpf.getNextdate());
	/* Advance the BILLING COMMENCEMENT DATE by one frequency*/
	
    callDatcon4();
	
	if(!(initFlag && isEQ(b5349DTO.getPtdate(),ZERO))){
		chdrpf.setBillcd(payrpf.getBillcd());  //PINNACLE-2923
	}	
    else{
   		chdrpf.setBillcd(b5349DTO.getPayrbillcd());
    }
	/* Subtract the lead days from the billed to date to calculate*/
	/* the next billing extract date for the PAYR record.*/
	compute(datcon2rec.freqFactor, 0).set((sub(ZERO, intT6654Leaddays)));
	datcon2rec.frequency.set("DY");
	datcon2rec.intDate1.set(payrpf.getBillcd());
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon2rec.datcon2Rec);
		syserrrec.statuz.set(datcon2rec.statuz);
		fatalError600();
	}
	payrpf.setNextdate(datcon2rec.intDate2.toInt());
}

//PINNACLE-1862
protected int getAnnptdt(Integer currptdt) {
	deffDays = 0;
	if(BTPRO036Permission){
		wsaaOccdate.set(b5349DTO.getOccdate());
		wsaaPtdate.set(currptdt);
		wsaaNextAnndateYy.set(wsaaPtdateYy);
		wsaaNextAnndateMm.set(wsaaOccdateMm);
		wsaaNextAnndateDd.set(wsaaOccdateDd);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.statuz.set(SPACES);
		while ( !(isEQ(datcon1rec.statuz, varcom.oK))) {
			datcon1rec.intDate.set(wsaaNextAnndate);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz, varcom.oK)) {
				wsaaNextAnndateDd.subtract(1);
			}
		}
		if (isLT(wsaaNextAnndate, currptdt)) {
			datcon4rec.intDate1.set(wsaaNextAnndate);
			datcon4rec.billday.set(payrpf.getDuedd());
			datcon4rec.billmonth.set(payrpf.getDuemm());
			datcon4rec.freqFactor.set(1);
			datcon4rec.frequency.set("01");
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			wsaaNextAnndate.set(datcon4rec.intDate2);
		}
		deffDays = DateUtils.calDays(currptdt.toString(), wsaaNextAnndate.toString());
		if (isEQ(payrpf.getBillfreq(), freqcpy.fortnightly) && deffDays > 0 && deffDays <= 2 ) {
			return wsaaNextAnndate.toInt();
		}
	}
	return currptdt;
}

protected void callDatcon4() 
{
	datcon4rec.freqFactor.set(1);
	datcon4rec.frequency.set(b5349DTO.getBillfreq());
	if(billChgFlag && payrpf.getOrgbillcd()!=null 
			&& String.valueOf(payrpf.getOrgbillcd()).length()>=8 
			&& payrpf.getOrgbillcd().intValue() != payrpf.getBillcd()) {
		datcon4rec.intDate1.set(payrpf.getOrgbillcd().intValue());
		datcon4rec.billday.set(String.valueOf(payrpf.getOrgbillcd()).substring(6, 8));
		datcon4rec.billmonth.set(String.valueOf(payrpf.getOrgbillcd()).substring(4, 6));
		if(!String.valueOf(payrpf.getOrgbillcd()).substring(6, 8).equals(String.valueOf(payrpf.getBtdate()).substring(6, 8))) {
			payrpf.setBillday(String.valueOf(payrpf.getOrgbillcd()).substring(6, 8));
		} else {
			payrpf.setBillday("");
		}
		isBilldayUpdated = true;
	}else if(payrpf.getOrgbillcd()==1 || b5349DTO.getBillday()==null || b5349DTO.getBillday().trim().equals("")) { //PINNACLE-2605
		datcon4rec.intDate1.set(wsaaOldBtdate);
		datcon4rec.billday.set(String.valueOf(payrpf.getBtdate()).substring(6, 8));//PINNACLE-2579
		datcon4rec.billmonth.set(String.valueOf(payrpf.getBtdate()).substring(4, 6));//PINNACLE-2579
		payrpf.setBillday("");
		isBilldayUpdated = true;
	}
	else {//PINNACLE-2579
		datcon4rec.intDate1.set(payrpf.getBillcd());
		if(String.valueOf(b5349DTO.getBillday()).length()>0) {//PINNACLE-2579
			datcon4rec.billday.set(b5349DTO.getBillday());//PINNACLE-2579
			datcon4rec.billmonth.set(b5349DTO.getBillmonth());//PINNACLE-2579
		}
	}
	
	callProgram(Datcon4.class, datcon4rec.datcon4Rec);
	if (isNE(datcon4rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon4rec.datcon4Rec);
		syserrrec.statuz.set(datcon4rec.statuz);
		fatalError600();
	}
	payrpf.setBillcd(getAnnptdt(datcon4rec.intDate2.toInt()));
	payrpf.setOrgbillcd(0);
}

protected void calcTax(){
	wsaaTax.set(ZERO);
	if (flexiblePremiumContract.isTrue()
	|| isEQ(tr52drec.txcode, SPACES)) {
		return ;
	}	
	Covrpf currCovr = null;
	if(initFlag  && isEQ(b5349DTO.getPtdate(),ZERO)){
		wsaaTotalBill.set(ZERO);
		String chdrnum = payxTemppf.getChdrnum();
		if (covtListMap != null && covtListMap.containsKey(chdrnum)) {
			Covrpf covrpf = new Covrpf();
			List<Covtpf> covtpfList = covtListMap.get(chdrnum);
			for (Iterator<Covtpf> iterator = covtpfList.iterator(); iterator.hasNext();) {
				Covtpf covt = iterator.next();
				compute(wsaaTotalBill, 2).setRounded(add(wsaaTotalBill, covt.getInstprem()));
				currCovr = getCovrpf(covt,covrpf);
				processCovrTax(currCovr);
			}
		}
		initialize(mgfeelrec.mgfeelRec);
		calcMgfeel();
		b5349DTO.setSinstamt01(wsaaTotalBill.getbigdata());
		b5349DTO.setSinstamt02(mgfeelrec.mgfee.getbigdata());
		b5349DTO.setSinstamt06(wsaaTotalBill.getbigdata().add(wsaaTax.getbigdata()).add(mgfeelrec.mgfee.getbigdata()));
		b5349DTO.setOutstamt(wsaaTotalBill.getbigdata().add(wsaaTax.getbigdata()).add(mgfeelrec.mgfee.getbigdata()));
		processCtfeeTax(currCovr);
	}
	else{
	String chdrnum = payxTemppf.getChdrnum();
    if (covrListMap != null && covrListMap.containsKey(chdrnum)) {
        List<Covrpf> covrpfList = covrListMap.get(chdrnum);
        for (Iterator<Covrpf> iterator = covrpfList.iterator(); iterator.hasNext(); ){
        	Covrpf covr = iterator.next();
        	processCovrTax(covr);
        	currCovr = covr;
        }
    }	
	if (isGT(b5349DTO.getSinstamt02(), 0)) {
		processCtfeeTax(currCovr);
	}  
	}	
}	

protected void processCovrTax(Covrpf covr){
	/* Calculate tax on premiums.                                      */
	wsaaValidCovr.set("N");
	wsaaWaiveCovrCalcTax = "N";
	wsaaWaiveTax = "N";
	/* Check to see CURRFROM <= PAYR-BTDATE & CURRTO > PAYR-BTDATE     */
	if (!isFoundPro && !initFlag) {
	if ((isLT(covr.getCurrfrom(), payrpf.getBtdate())
	|| isEQ(covr.getCurrfrom(), payrpf.getBtdate()))
	&& isGT(covr.getCurrto(), payrpf.getBtdate())) {
		/*NEXT_SENTENCE*/
	}
	else return;
	}
	
	/* If installment premium = zero, exit                             */
	if (covr.getInstprem().compareTo(BigDecimal.ZERO) == 0 ) {
		return ;
	}
	if(reinstflag && isFoundPro){
	 isShortTax = false;
	 readTd5j2(covr.getCrtable());/* IJTI-1386 */
	 if(isNE(td5j2rec.td5j2Rec,SPACES)){
		 if(isEQ(td5j2rec.shortTerm,"Y")){
			 if(isEQ(lapsePTDate, wsaaOldBtdate)){
				 isShortTax = true;
			 }
			 else{	
				 return;
			}
		 }	
	 }
	}
	if(!initFlag){
	/* Check to see if coverage is of a valid status                   */
	if (isNE(covr.getValidflag(), "1")) {
		return ;
	}
	for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
		if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covr.getStatcode())) {
			for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
				if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], covr.getPstatcode())) {
					wsaaT5679Sub.set(13);
					wsaaValidCovr.set("Y");
				}
			}
		}
	}
	/*  If the coverage is not of a valid status read the next         */
	/*  record for the contract:                                       */
	if (!validCovr.isTrue()) {
		return ;
	}
	}
	/*  Read table TR52E.                                              */
	StringBuilder sbTr52eKey = new StringBuilder("");
	sbTr52eKey.append(tr52drec.txcode.toString().trim());
	sbTr52eKey.append(b5349DTO.getCnttype().trim());/* IJTI-1386 */
	sbTr52eKey.append(covr.getCrtable().trim());/* IJTI-1386 */
	boolean itemFound = readTr52e(sbTr52eKey.toString(), Integer.parseInt(wsaaOldBtdate.toString()));
	
	if (!itemFound){
		sbTr52eKey = new StringBuilder("");
		sbTr52eKey.append(tr52drec.txcode.toString().trim());
		sbTr52eKey.append(b5349DTO.getCnttype().trim());/* IJTI-1386 */
		sbTr52eKey.append("****");	
		itemFound = readTr52e(sbTr52eKey.toString(), Integer.parseInt(wsaaOldBtdate.toString()));
		if (!itemFound){
			sbTr52eKey = new StringBuilder("");
			sbTr52eKey.append(tr52drec.txcode.toString().trim());
			sbTr52eKey.append("***");
			sbTr52eKey.append("****");	
			itemFound = readTr52e(sbTr52eKey.toString(), Integer.parseInt(wsaaOldBtdate.toString()));
			if (!itemFound){
				syserrrec.params.set(sbTr52eKey);
				fatalError600();
			}
		}
	}
	
	/*  If TR52E tax indicator not = 'Y', do not calculate tax         */
	if (isNE(tr52erec.taxind01, "Y")) {
		return ;
	}
	/*  If PAYR-SINSTAMT05 not = zero, check to see if component       */
	/* exists in TR517                                                 */
	if (b5349DTO.getSinstamt05().compareTo(BigDecimal.ZERO) != 0  
	&& isEQ(covr.getCrtable(), wsaaTr517Item)
	&& isEQ(tr517rec.zrwvflg01, "Y")) {
		     // Waive the premium of the wop component.                   
		wsaaWaiveTax = "Y";
	}
	wsaaTr517Ix.set(ZERO);
	if (b5349DTO.getSinstamt05().compareTo(BigDecimal.ZERO) != 0 ) {
		for (wsaaTr517Ix.set(1); !(isGT(wsaaTr517Ix, 50)); wsaaTr517Ix.add(1)){
			if (isEQ(tr517rec.ctable[wsaaTr517Ix.toInt()], SPACES)) {
				wsaaTr517Ix.set(51);
			}
			else {
				if (isEQ(tr517rec.ctable[wsaaTr517Ix.toInt()], covr.getCrtable())) {
					wsaaWaiveCovrCalcTax = "Y";
					wsaaTr517Ix.set(51);
				}
			}
		}
	}
	
	
	/*  Check if component found in INCR where INCD-CRRCD < PAYR-BTDATE*/
	/*  and validflag = '1'                                            */
	Incrpf currIncr = null;
	String chdrnum = payxTemppf.getChdrnum();
	boolean incrFound = false;
    if (incrListMap != null && incrListMap.containsKey(chdrnum)) {
        List<Incrpf> incrpfList = incrListMap.get(chdrnum);
        for (Iterator<Incrpf> iterator = incrpfList.iterator(); iterator.hasNext(); ){
        	Incrpf incr = iterator.next();
        	if (incr.getLife().equals(covr.getLife())
        		&&  incr.getCoverage().equals(covr.getCoverage())
        		&& 	incr.getRider().equals(covr.getRider())
        		&& Integer.compare(incr.getPlnsfx(),covr.getPlanSuffix()) == 0 ) {
        			currIncr = incr;
        			incrFound = true;
        			break;
        			
        	}
        }
    }
    
    if (incrFound 
    	&& currIncr.getValidflag().equals("1")
    	&& Integer.compare(currIncr.getCrrcd(), payrpf.getBtdate()) < 0){
		compute(wsaaIncrBinstprem, 3).setRounded(sub(currIncr.getZbnewinst(), currIncr.getZblastinst()));
		compute(wsaaIncrInstprem, 3).setRounded(sub(currIncr.getNewinst(), currIncr.getLastInst()));

    }
    Incrpf currIncrPro = null;
	boolean incrFoundPro = false;
    if(isFoundPro){  
	int date = wsaaOldBtdate.toInt();
	if (incrDatedMap != null && incrDatedMap.containsKey(date)) {
		List<Incrpf> incrpfList = incrDatedMap.get(date);
		for (Iterator<Incrpf> iterator = incrpfList.iterator(); iterator.hasNext(); ){
			Incrpf incr = iterator.next();
			if (incr.getChdrnum().equals(covr.getChdrnum())
			&&	incr.getLife().equals(covr.getLife())
    		&&  incr.getCoverage().equals(covr.getCoverage())
    		&& 	incr.getRider().equals(covr.getRider())
    		&& Integer.compare(incr.getPlnsfx(),covr.getPlanSuffix()) == 0) {
				if(isGTE(lapsePTDate,payrpf.getBtdate())){
					currIncrPro = incr;
					incrFoundPro = true;
					break;		
			  }
			}
		}
	  }
    }
    boolean rertFound = false;
	if (!incrFound) {
		if (rertDatedMap != null && rertDatedMap.containsKey(chdrnum)) {
			List<Rertpf> rertpfList = rertDatedMap.get(chdrnum);
			for (Iterator<Rertpf> iterator = rertpfList.iterator(); iterator.hasNext();) {
				Rertpf rert = iterator.next();
				if (isEQ(rert.getEffdate(), wsaaOldBtdate) && rert.getLife().equals(covr.getLife()) && rert.getCoverage().equals(covr.getCoverage())
						&& rert.getRider().equals(covr.getRider())
						&& Integer.compare(rert.getPlnsfx(), covr.getPlanSuffix()) == 0) {
					rertFound = true;
					compute(wsaaIncrBinstprem, 3).setRounded(sub(rert.getZbnewinst(), covr.getZbinstprem()));
					compute(wsaaIncrInstprem, 3).setRounded(sub(rert.getNewinst(), covr.getInstprem()));
					break;
				}
			}
		}
	}
    
	/*  Set values to tax linkage and call tax subroutine              */
	initialize(txcalcrec.linkRec);
	txcalcrec.function.set("CALC");
	txcalcrec.statuz.set(varcom.oK);
	txcalcrec.transType.set("PREM");
	txcalcrec.chdrcoy.set(covr.getChdrcoy());
	txcalcrec.chdrnum.set(covr.getChdrnum());
	txcalcrec.taxrule.set(sbTr52eKey);
	wsaaRateItem.set(SPACES);
	txcalcrec.ccy.set(b5349DTO.getChdrcntcurr());
	wsaaCntCurr.set(b5349DTO.getChdrcntcurr());
	wsaaTxitem.set(tr52erec.txitem);
	txcalcrec.rateItem.set(wsaaRateItem);
	txcalcrec.txcode.set(tr52drec.txcode);
	txcalcrec.amountIn.set(ZERO);
	if(isFoundPro){
		txcalcrec.life.set(covr.getLife());
		txcalcrec.coverage.set(covr.getCoverage());
		txcalcrec.rider.set(covr.getRider());
		txcalcrec.planSuffix.set(covr.getPlanSuffix());
	}
	txcalcrec.crtable.set(covr.getCrtable());	//IBPLIFE-13475
	txcalcrec.cnttype.set(chdrpf.getCnttype());	//IBPLIFE-13475
	if (isEQ(tr52erec.zbastyp, "Y")) {
		if(isFoundPro && incrFoundPro){
			compute(txcalcrec.amountIn, 3).setRounded(add(currIncrPro.getZbnewinst(), wsaaIncrInstprem));
		}
		else{
		compute(txcalcrec.amountIn, 3).setRounded(add(covr.getZbinstprem(), wsaaIncrBinstprem));
		}
	}
	else {
		if(isShortTax){
		compute(txcalcrec.amountIn, 3).setRounded(add(reratedPremKeeps.get(covr.getUniqueNumber()), wsaaIncrInstprem));
		}
		else{
			if(isFoundPro && incrFoundPro){
				compute(txcalcrec.amountIn, 3).setRounded(add(currIncrPro.getNewinst(), wsaaIncrInstprem));
			}
			else{
				compute(txcalcrec.amountIn, 3).setRounded(add(covr.getInstprem(), wsaaIncrInstprem));
			}
		}
	}
	if(isShortTax){
	txcalcrec.effdate.set(ptrnpfReinstate.getDatesub());
	}
	else{
	txcalcrec.effdate.set(wsaaOldBtdate);
	}
	txcalcrec.taxType[1].set(SPACES);
	txcalcrec.taxType[2].set(SPACES);
	txcalcrec.taxAbsorb[1].set(SPACES);
	txcalcrec.taxAbsorb[2].set(SPACES);
	txcalcrec.taxAmt[1].set(ZERO);
	txcalcrec.taxAmt[2].set(ZERO);
	callProgram(tr52drec.txsubr, txcalcrec.linkRec);
	if (isNE(txcalcrec.statuz, varcom.oK)) {
		syserrrec.params.set(txcalcrec.linkRec);
		syserrrec.statuz.set(txcalcrec.statuz);
		fatalError600();
	}
	if (isGT(txcalcrec.taxAmt[1], ZERO)
	|| isGT(txcalcrec.taxAmt[2], ZERO)) {
		/*  If component found in TR517 or Waive itself is Y, do not add tax, so it will not add in outstanding bill amount and
		 * instamt06 of linspf            */
		if (isEQ(wsaaWaiveCovrCalcTax, "N") && isEQ(wsaaWaiveTax, "N")) { 
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}
	if(!initFlag){
	/*  Create TAXD record                                             */
	taxdpf = new Taxdpf();

	taxdpf.setChdrcoy(covr.getChdrcoy());
	taxdpf.setChdrnum(covr.getChdrnum());
	taxdpf.setTrantype("PREM");
	taxdpf.setLife(covr.getLife());
	taxdpf.setCoverage(covr.getCoverage());
	taxdpf.setRider(covr.getRider());
	taxdpf.setPlansfx(0);
	if(isShortTax){
		taxdpf.setEffdate(ptrnpfReinstate.getDatesub());
	}
	else{
	taxdpf.setEffdate(wsaaOldBtdate.toInt());
	}
	taxdpf.setInstfrom(wsaaOldBtdate.toInt());
	taxdpf.setInstto(payrpf.getBtdate());
	taxdpf.setBillcd(wsaaOldBillcd.toInt());
	wsaaTranref.set(SPACES);
	StringUtil stringVariable1 = new StringUtil();
	stringVariable1.addExpression(txcalcrec.taxrule);
	stringVariable1.addExpression(txcalcrec.rateItem);
	stringVariable1.setStringInto(wsaaTranref);
	taxdpf.setTranref(wsaaTranref.toString());
	taxdpf.setTranno(chdrpf.getTranno());
	taxdpf.setBaseamt(BigDecimal.valueOf(txcalcrec.amountIn.toDouble()));
	taxdpf.setTaxamt01(BigDecimal.valueOf(txcalcrec.taxAmt[1].toDouble()));
	taxdpf.setTaxamt02(BigDecimal.valueOf(txcalcrec.taxAmt[2].toDouble()));
	taxdpf.setTaxamt03(BigDecimal.ZERO);
	taxdpf.setTxabsind01(txcalcrec.taxAbsorb[1].toString());
	taxdpf.setTxabsind02(txcalcrec.taxAbsorb[2].toString());
	taxdpf.setTxabsind03(SPACES.toString());
	taxdpf.setTxtype01(txcalcrec.taxType[1].toString());
	taxdpf.setTxtype02(txcalcrec.taxType[2].toString());
	taxdpf.setTxtype03("");
	taxdpf.setPostflg(SPACES.toString());
	isBulkProcessTaxdPF = true;	
	taxdBulkInsList.add(taxdpf);
	}
}
protected void commitTaxdBulkInsert(){	
	isBulkProcessTaxdPF = false;
	boolean isInsertTaxdPF = taxdpfDAO.insertTaxdPF(taxdBulkInsList);	
	if (!isInsertTaxdPF) {
		LOGGER.error("Insert TaxdPF record failed.");
		fatalError600();
	}else taxdBulkInsList.clear();
}

protected void processCtfeeTax(Covrpf covr){
	/* Calculate tax on contract fee.                                  */
	/*  Read table TR52E.                                              */
	StringBuilder sbTr52eKey = new StringBuilder("");
	sbTr52eKey.append(tr52drec.txcode.toString().trim());
	sbTr52eKey.append(b5349DTO.getCnttype().trim());/* IJTI-1386 */
	sbTr52eKey.append(covr.getCrtable().trim());/* IJTI-1386 */
	boolean itemFound = readTr52e(sbTr52eKey.toString(), Integer.parseInt(wsaaOldBtdate.toString()));
	
	if (!itemFound){
		sbTr52eKey = new StringBuilder("");
		sbTr52eKey.append(tr52drec.txcode.toString().trim());
		sbTr52eKey.append(b5349DTO.getCnttype().trim());/* IJTI-1386 */
		sbTr52eKey.append("****");	
		itemFound = readTr52e(sbTr52eKey.toString(), Integer.parseInt(wsaaOldBtdate.toString()));
		if (!itemFound){
			sbTr52eKey = new StringBuilder("");
			sbTr52eKey.append(tr52drec.txcode.toString().trim());
			sbTr52eKey.append("***");
			sbTr52eKey.append("****");	
			itemFound = readTr52e(sbTr52eKey.toString(), Integer.parseInt(wsaaOldBtdate.toString()));
			if (!itemFound){
				syserrrec.params.set(sbTr52eKey);
				fatalError600();
			}
		}
	}
	
	/*  If TR52E tax indicator2 not 'Y', do not calculate tax          */
	if (isNE(tr52erec.taxind02, "Y")) return ;
	
	/*  Set values to tax linkage and call tax subroutine              */
	initialize(txcalcrec.linkRec);
	txcalcrec.function.set("CALC");
	txcalcrec.statuz.set(varcom.oK);
	txcalcrec.transType.set("CNTF");
	txcalcrec.chdrcoy.set(payxTemppf.getChdrcoy());
	txcalcrec.chdrnum.set(payxTemppf.getChdrnum());
	txcalcrec.taxrule.set(sbTr52eKey);
	wsaaRateItem.set(SPACES);
	txcalcrec.ccy.set(b5349DTO.getChdrcntcurr());
	wsaaCntCurr.set(b5349DTO.getChdrcntcurr());
	wsaaTxitem.set(tr52erec.txitem);
	txcalcrec.rateItem.set(wsaaRateItem);
	txcalcrec.effdate.set(wsaaOldBtdate);
	if(initFlag && isEQ(b5349DTO.getPtdate(),ZERO)){
		txcalcrec.amountIn.set(mgfeelrec.mgfee);
	}
	else{
	if (isEQ(wsaaGotPayrAtBtdate, "Y")) {
		txcalcrec.amountIn.set(currPayrLif.getSinstamt02());
	}
	else {
		txcalcrec.amountIn.set(b5349DTO.getSinstamt02());
	}
	}
	txcalcrec.crtable.set(covr.getCrtable());	//IBPLIFE-13475
	txcalcrec.cnttype.set(chdrpf.getCnttype());	//IBPLIFE-13475
	txcalcrec.taxType[1].set(SPACES);
	txcalcrec.taxType[2].set(SPACES);
	txcalcrec.taxAbsorb[1].set(SPACES);
	txcalcrec.taxAbsorb[2].set(SPACES);
	txcalcrec.taxAmt[1].set(ZERO);
	txcalcrec.taxAmt[2].set(ZERO);
	callProgram(tr52drec.txsubr, txcalcrec.linkRec);
	if (isNE(txcalcrec.statuz, varcom.oK)) {
		syserrrec.params.set(txcalcrec.linkRec);
		syserrrec.statuz.set(txcalcrec.statuz);
		fatalError600();
	}
	if (isGT(txcalcrec.taxAmt[1], ZERO)
	|| isGT(txcalcrec.taxAmt[2], ZERO)) {
		                                 
		if (b5349DTO.getSinstamt05().compareTo(BigDecimal.ZERO) == 0 
		|| isNE(tr517rec.zrwvflg03, "Y")) {
			
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}
	if(!initFlag){
	/*  Create TAXD record                                             */
	taxdpf = new Taxdpf();
	taxdpf.setChdrcoy(payxTemppf.getChdrcoy());
	taxdpf.setChdrnum(payxTemppf.getChdrnum());
	taxdpf.setTrantype("CNTF");
	taxdpf.setLife("");
	taxdpf.setCoverage("");
	taxdpf.setRider("");
	taxdpf.setPlansfx(0);
	taxdpf.setEffdate(wsaaOldBtdate.toInt());
	taxdpf.setInstfrom(wsaaOldBtdate.toInt());
	taxdpf.setInstto(payrpf.getBtdate());
	taxdpf.setBillcd(wsaaOldBillcd.toInt());
	wsaaTranref.set(SPACES);
	StringUtil stringVariable1 = new StringUtil();
	stringVariable1.addExpression(txcalcrec.taxrule);
	stringVariable1.addExpression(txcalcrec.rateItem);
	stringVariable1.setStringInto(wsaaTranref);
	taxdpf.setTranref(wsaaTranref.toString());
	taxdpf.setTranno(b5349DTO.getChdrtranno()+1);
	taxdpf.setBaseamt(BigDecimal.valueOf(txcalcrec.amountIn.toDouble()));
	taxdpf.setTaxamt01(BigDecimal.valueOf(txcalcrec.taxAmt[1].toDouble()));
	taxdpf.setTaxamt02(BigDecimal.valueOf(txcalcrec.taxAmt[2].toDouble()));
	taxdpf.setTaxamt03(BigDecimal.ZERO);
	taxdpf.setTxabsind01(txcalcrec.taxAbsorb[1].toString());
	taxdpf.setTxabsind02(txcalcrec.taxAbsorb[2].toString());
	taxdpf.setTxabsind03("");
	taxdpf.setTxtype01(txcalcrec.taxType[1].toString());
	taxdpf.setTxtype02(txcalcrec.taxType[2].toString());
	taxdpf.setTxtype03("");
	taxdpf.setPostflg("");
	taxdBulkInsList.add(taxdpf);	
	}
}


protected void automaticIncrease(){
	/* Look up any automatic increases.*/
	wsaaIncreaseDue.set(ZERO);
	
	String chdrnum = payxTemppf.getChdrnum();
    if (incrListMap != null && incrListMap.containsKey(chdrnum)) {
        List<Incrpf> incrpfList = incrListMap.get(chdrnum);
        for (Iterator<Incrpf> iterator = incrpfList.iterator(); iterator.hasNext(); ){
        	Incrpf incr = iterator.next();

        	if (isLT(incr.getCrrcd(), payrpf.getBtdate())) {
        		compute(wsaaIncreaseDue, 2).set(sub(add(wsaaIncreaseDue, incr.getNewinst()), incr.getLastInst()));
        	}
        }
    }	

    if(isEQ(wsaaIncreaseDue,ZERO)){
    	if (rertDatedMap != null && rertDatedMap.containsKey(chdrnum)) {
	      List<Rertpf> rertpfList = rertDatedMap.get(chdrnum);
	      for (Iterator<Rertpf> iterator = rertpfList.iterator(); iterator.hasNext(); ){
	    	 Rertpf rert = iterator.next();
	    	 if (isEQ(rert.getEffdate(), wsaaOldBtdate)) {
	    		compute(wsaaIncreaseDue, 2).set(sub(add(wsaaIncreaseDue, rert.getNewinst()), rert.getLastInst()));
	    		newPremiumFlag=true;
	    		wsaaNewIncreaseDue.set(wsaaIncreaseDue);
    	      } else if(newPremiumFlag) {
    	    	  wsaaIncreaseDue.set(wsaaNewIncreaseDue);
    	      }
    	   }
    	}		
    }
	/* Update Increase amount into PAYR OUTSTAMT                       */
    setPrecision(payrpf.getOutstamt(), 2);
	payrpf.setOutstamt(payrpf.getOutstamt().add(BigDecimal.valueOf(wsaaIncreaseDue.toDouble())));
	payrpf.setOutstamt(payrpf.getOutstamt().add(BigDecimal.valueOf(catchupPrem.toDouble())).add(BigDecimal.valueOf(catchupPremFee.toDouble())));
	chdrpf.setOutstamt(payrpf.getOutstamt());
	/* Log total amount billed.                                     */
	if (isEQ(wsaaGotPayrAtBtdate, "Y")) {
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(currPayrLif.getSinstamt06(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			compute(contotrec.totval, 2).set(add(wsaaInstPrem, wsaaIncreaseDue));
		}
		else{
		compute(contotrec.totval, 2).set(add(currPayrLif.getSinstamt06(), wsaaIncreaseDue));
		}
	}
	else {
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(b5349DTO.getSinstamt06(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			compute(contotrec.totval, 2).set(add(wsaaInstPrem, wsaaIncreaseDue));
		}
		else{
		compute(contotrec.totval, 2).set(add(b5349DTO.getSinstamt06(), wsaaIncreaseDue));
		}
	}
	
	contotrec.totval.add(wsaaTax);
	contotrec.totval.add(catchupPrem);
	contotrec.totval.add(catchupPremFee);
	ctrCT04 = ctrCT04.add(BigDecimal.valueOf(contotrec.totval.toDouble()));
	
	/*
	contotrec.totval.add(wsaaTax);
	contotrec.totno.set(controlTotalsInner.ct04);
	callContot001();
	*/	
}

protected void writeLins(){
	linspf = new Linspf();
	linspf.setInstjctl("");
	linspf.setDueflg(" ");	//ICIL-1498
	if (isEQ(wsaaGotPayrAtBtdate, "Y")) {
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(b5349DTO.getSinstamt01(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			linspf.setInstamt01(wsaaInstPrem.getbigdata());
		}
		else{
		linspf.setInstamt01(currPayrLif.getSinstamt01());
		}
		setPrecision(linspf.getInstamt01(), 2);
		linspf.setInstamt01(linspf.getInstamt01().add(BigDecimal.valueOf(wsaaIncreaseDue.toDouble())));
		
		linspf.setInstamt02(currPayrLif.getSinstamt02());
		linspf.setInstamt03(currPayrLif.getSinstamt03());
		linspf.setInstamt04(currPayrLif.getSinstamt04());
		linspf.setInstamt05(currPayrLif.getSinstamt05());
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(currPayrLif.getSinstamt06(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			linspf.setInstamt06(wsaaInstPrem.getbigdata());
		}
		else{
		linspf.setInstamt06(currPayrLif.getSinstamt06());
		}
		
		setPrecision(linspf.getInstamt06(), 2);
		linspf.setInstamt06(linspf.getInstamt06().add(BigDecimal.valueOf(wsaaIncreaseDue.toDouble())));		
		
	}
	else {
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(b5349DTO.getSinstamt01(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			linspf.setInstamt01(wsaaInstPrem.getbigdata());
		}
		else{
		linspf.setInstamt01(b5349DTO.getSinstamt01());
		}
		setPrecision(linspf.getInstamt01(), 2);
		linspf.setInstamt01(linspf.getInstamt01().add(BigDecimal.valueOf(wsaaIncreaseDue.toDouble())));
		linspf.setInstamt02(b5349DTO.getSinstamt02());		linspf.setInstamt03(b5349DTO.getSinstamt03());
		linspf.setInstamt04(b5349DTO.getSinstamt04());
		linspf.setInstamt05(b5349DTO.getSinstamt05());
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(b5349DTO.getSinstamt06(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			linspf.setInstamt06(wsaaInstPrem.getbigdata());
		}
		else{
		linspf.setInstamt06(b5349DTO.getSinstamt06());
		}
		setPrecision(linspf.getInstamt06(), 2);
		linspf.setInstamt06(linspf.getInstamt06().add(BigDecimal.valueOf(wsaaIncreaseDue.toDouble())));
	}
	linspf.setInstamt01(add(linspf.getInstamt01(), catchupPrem, catchupPremFee).getbigdata());
	linspf.setInstamt06(linspf.getInstamt06().add(BigDecimal.valueOf(catchupPrem.toDouble())).add(BigDecimal.valueOf(catchupPremFee.toDouble())));
	
	wsaaBillAmount.set(linspf.getInstamt06());
	setPrecision(linspf.getInstamt06(), 2);
	linspf.setInstamt06(linspf.getInstamt06().add(BigDecimal.valueOf(wsaaTax.toDouble())));
	/* Convert contract amount(total) to billing amount if contract*/
	/* currency is not the same as billing currency.*/
	if (isEQ(b5349DTO.getPayrcntcurr(), b5349DTO.getPayrbillcurr())
	|| linspf.getInstamt06().compareTo(BigDecimal.ZERO) == 0 ) {
		linspf.setCbillamt(linspf.getInstamt06());
	}
	else {
		conlinkrec.amountIn.set(linspf.getInstamt06());
		callXcvrt5000();
		//linspf.setCbillamt(BigDecimal.valueOf(conlinkrec.amountOut.toDouble()));
		linspf.setCbillamt(BigDecimal.valueOf(conlinkrec.amountOut.getbigdata().doubleValue()));  //ILIFE-5797  by dpuhawan
	}
	/* If this is a flexible premium contract then we do not   <D9604>*/
	/* want to write a LINS record. We have come this far      <D9604>*/
	/* however as we do need to know linsrnl-cbillamt value    <D9604>*/
	/* to update the FPRM file. Also we maintain CT11 which    <D9604>*/
	/* logs the number of flexible premium billings:           <D9604>*/
	if (flexiblePremiumContract.isTrue()) {
		writeFpco();
		ctrCT11++;
		return ;
	}
	linspf.setChdrnum(payxTemppf.getChdrnum());
	linspf.setChdrcoy(payxTemppf.getChdrcoy());
	linspf.setBillchnl(payxTemppf.getBillchnl());
	linspf.setPayrseqno(payxTemppf.getPayrseqno());
	linspf.setBranch(batcdorrec.branch.toString());
	linspf.setTranscode(bprdIO.getAuthCode().toString());
	linspf.setInstfreq(b5349DTO.getBillfreq());
	linspf.setCntcurr(b5349DTO.getPayrcntcurr());
	linspf.setBillcurr(b5349DTO.getBillcurr());
	if(initFlag && isEQ(b5349DTO.getPtdate(),ZERO)){
		linspf.setInstto(getAnnptdt(b5349DTO.getBtdate()));
	}
	else{
		linspf.setInstto(getAnnptdt(payrpf.getBtdate()));
	}
	linspf.setMandref(b5349DTO.getMandref());
	if(prmhldtrad && prmhpf != null && bsscIO.getEffectiveDate().toInt()<=prmhpf.getTodate() && !billWritten) {//ILIFE-8509
		linspf.setInstfrom(bsscIO.getEffectiveDate().toInt());
		linspf.setProrcntfee(currPayrLif != null ? currPayrLif.getProrcntfee() : BigDecimal.ZERO);
		linspf.setProramt(currPayrLif != null ? currPayrLif.getProramt() : BigDecimal.ZERO);
		billWritten = true;
	}
	else if(initFlag && isEQ(b5349DTO.getPtdate(),ZERO)){
			linspf.setInstfrom(effDate.toInt());
		}
		else{
			linspf.setInstfrom(wsaaOldBtdate.toInt());
		}
	linspf.setBillcd(wsaaOldBillcd.toInt());
	linspf.setAcctmeth(b5349DTO.getAcctmeth());
	linspf.setPayflag("O");
	linspf.setValidflag("1");
	/*  Update the tax relief method on the LINS record*/
	/*  if tax relief is applied.*/
	/* MOVE LINSRNL-INSTAMT06      TO PRAS-GROSSPREM.               */
	/*  Exclude WSAA-TAX in calculating Tax Relief                     */
	if (isEQ(b5349DTO.getPayrcntcurr(), b5349DTO.getPayrbillcurr())) {
		prasrec.grossprem.set(wsaaBillAmount);
	}
	else {
		conlinkrec.amountIn.set(wsaaBillAmount);
		callXcvrt5000();
		prasrec.grossprem.set(conlinkrec.amountOut);
	}
	calculateTaxRelief();  //done

	isBulkProcessLinsPF = true;
	linsBulkInsList.add(linspf);
	/*  Log number of LINS written*/
	ctrCT05++;
	/*  Log total of this instalement*/
	/* Do not add the increase amount into the Control Total since  */
	/* LINSRNL-INSTAMT06 already incorporates the increase.         */
	/* COMPUTE CONT-TOTVAL =                                        */
	/*                 WSAA-INCREASE-DUE + LINSRNL-INSTAMT06.       */
	/*
	contotrec.totval.set(linspf.getInstamt06());
	contotrec.totno.set(controlTotalsInner.ct06);
	callContot001();
	*/
	ctrCT06 = ctrCT06.add(linspf.getInstamt06());
	//ILIFE-5975
	if(isBulkProcessLinsPF) 
		commitLinsBulkInsert();
}

protected void commitLinsBulkInsert(){	 
	isBulkProcessLinsPF = false;
	boolean isInsertLinsPF = linspfDAO.insertLinsPF(linsBulkInsList);	
	if (!isInsertLinsPF) {
		LOGGER.error("InsertLinsPF record failed.");
		fatalError600();
	}else linsBulkInsList.clear();
}

protected void produceBextRecord(){
	wsaaBillOutst.set(ZERO);
	readT3620();
	/* Convert contract amount to billing amount if contract currency*/
	/* is not the same as billing currency.*/
	if (isEQ(b5349DTO.getPayrcntcurr(), b5349DTO.getPayrbillcurr())) {
		compute(prasrec.grossprem, 2).set(add(payrpf.getOutstamt(), wsaaIncreaseDue));
		calculateTaxRelief();
		compute(conlinkrec.amountOut, 2).set(add(sub(payrpf.getOutstamt(), prasrec.taxrelamt), wsaaIncreaseDue));
		compute(wsaaBillOutst, 3).setRounded(add(conlinkrec.amountOut, wsaaTax));
	}
	else {
		if (payrpf.getOutstamt().compareTo(BigDecimal.ZERO) != 0 ) {
			compute(conlinkrec.amountIn, 2).set((add(payrpf.getOutstamt(), wsaaIncreaseDue)));
			callXcvrt5000();
			prasrec.grossprem.set(conlinkrec.amountOut);
			calculateTaxRelief();
			conlinkrec.amountOut.subtract(prasrec.taxrelamt);
			wsaaBillOutst.set(conlinkrec.amountOut);
			conlinkrec.amountIn.set(wsaaTax);
			callXcvrt5000();
			wsaaBillOutst.add(conlinkrec.amountOut);
		}
	}
	if(initFlag && isEQ(b5349DTO.getPtdate(),ZERO)){
		wsaaBillOutst.set(b5349DTO.getSinstamt06());
	}
	/* Do not produce bext if there is enough money in the suspense*/
	/* account to cover the instalment amount.*/
	/* Additional checking for dividend suspense               <LA2106>*/
	/* IF WSAA-SUSP-AVAIL          >=  CLNK-AMOUNT-OUT              */
	if (isGTE(wsaaSuspAvail, wsaaBillOutst)) {
		//compute(wsaaSuspAvail, 3).setRounded(sub(wsaaSuspAvail, wsaaBillOutst)); //PINNACLE-2366 //Commented as part of PINNACLE-2847
		if (isEQ(wsaaDvdSusp, 0)) {
			return ;
		}
		/*                                                         <LA2106>*/
		/* Transfer LP DS to LP S                                  <LA2106>*/
		h100TfrDvdSusp();
		return ;
	}
	readDishonours();
	if(japanBilling && isNE(b5349DTO.getChdrbillchnl(),"D")){
		return;
	}
	callBillreq();
}

protected void readDishonours(){
	/*  Check if a previously dishonoured payment exists by reading*/
	/*  DDSURNL with a BEGN. If there is a key break move the*/
	/*  original MANDSTAT to BEXT-MANDSTAT, move the later of the*/
	/*  BSSC-EFFECTIVE-DATE or BILLCD to BILLDATE, else move DDSURNL-*/
	/*  MANDSTAT to BEXT-MANDSTAT and increment the BSSC-EFFECTIVE-*/
	/*  DATE by DDSURNL-LAPDAY to give BEXT-BILLDATE.*/

	
	Ddsupf currDdsu = null;
	String clntnum = b5349DTO.getClntnum();
	boolean isFound = false;
    if (ddsuListMap != null && ddsuListMap.containsKey(clntnum)) {
        List<Ddsupf> ddsupfList = ddsuListMap.get(clntnum);
        for (Iterator<Ddsupf> iterator = ddsupfList.iterator(); iterator.hasNext(); ){
        	Ddsupf ddsu = iterator.next();

        	if (ddsu.getMandref().equals(b5349DTO.getMandref())
        			&& Integer.compare(ddsu.getBillcd(),wsaaOldBillcd.toInt()) == 0
            		&& 	ddsu.getMandstat().equals("99")) {
        				currDdsu = ddsu;
        				isFound = true;
            			break;            	
            }
        }
    }
    
    if (!isFound){
		billreqrec.mandstat.set(b5349DTO.getMandstat());
		if (isGT(bsscIO.getEffectiveDate(), wsaaOldBillcd)) {
			billreqrec.billdate.set(bsscIO.getEffectiveDate());
		}
		else {
			billreqrec.billdate.set(wsaaOldBillcd);
		}     	
    }else{
    	billreqrec.mandstat.set(currDdsu.getMandstat());
    	datcon2rec.freqFactor.set(currDdsu.getLapday());
    	datcon2rec.frequency.set("DY");
    	datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
    	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
    	if (isNE(datcon2rec.statuz, varcom.oK)) {
    		syserrrec.params.set(datcon2rec.datcon2Rec);
    		syserrrec.statuz.set(datcon2rec.statuz);
    		fatalError600();
    	}
    	billreqrec.billdate.set(datcon2rec.intDate2);    	
    }
}
protected void callBillreq(){
	/* Search the T3629 array to obtain the required bank code.*/
	readT3629();
	billreqrec.bankcode.set(strT3629BankCode);
	billreqrec.user.set(0);
	billreqrec.contot01.set(0);
	billreqrec.contot02.set(0);
	billreqrec.instjctl.set(SPACES);
	billreqrec.payflag.set(SPACES);
	billreqrec.bilflag.set(SPACES);
	billreqrec.outflag.set(SPACES);
	billreqrec.supflag.set("N");
	billreqrec.company.set(bsprIO.getCompany());
	billreqrec.branch.set(batcdorrec.branch);
	billreqrec.language.set(bsscIO.getLanguage());
	billreqrec.effdate.set(bsscIO.getEffectiveDate());
	billreqrec.acctyear.set(batcdorrec.actyear);
	billreqrec.acctmonth.set(batcdorrec.actmonth);
	billreqrec.trancode.set(batcdorrec.trcde);
	billreqrec.batch.set(batcdorrec.batch);
	billreqrec.fsuco.set(bsprIO.getFsuco());
	billreqrec.modeInd.set("BATCH");
	billreqrec.termid.set(SPACES);
	billreqrec.time.set(ZERO);
	billreqrec.date_var.set(ZERO);
	billreqrec.tranno.set(chdrpf.getTranno());
	billreqrec.chdrpfx.set(b5349DTO.getChdrpfx());
	billreqrec.chdrcoy.set(payxTemppf.getChdrcoy());
	billreqrec.chdrnum.set(payxTemppf.getChdrnum());
	billreqrec.servunit.set(b5349DTO.getServunit());
	billreqrec.cnttype.set(b5349DTO.getCnttype());
	billreqrec.occdate.set(b5349DTO.getOccdate());
	billreqrec.ccdate.set(b5349DTO.getCcdate());
	billreqrec.instcchnl.set(b5349DTO.getCollchnl());
	billreqrec.cownpfx.set(b5349DTO.getCownpfx());
	billreqrec.cowncoy.set(b5349DTO.getCowncoy());
	billreqrec.cownnum.set(b5349DTO.getCownnum());
	billreqrec.cntbranch.set(b5349DTO.getCntbranch());
	billreqrec.agntpfx.set(b5349DTO.getAgntpfx());
	billreqrec.agntcoy.set(b5349DTO.getAgntcoy());
	billreqrec.agntnum.set(b5349DTO.getAgntnum());
	billreqrec.cntcurr.set(b5349DTO.getPayrcntcurr());
	billreqrec.billcurr.set(b5349DTO.getPayrbillcurr());
	billreqrec.ptdate.set(payrpf.getPtdate());
	if(initFlag && isEQ(b5349DTO.getPtdate(),ZERO)){
		billreqrec.instto.set(b5349DTO.getBtdate());
	}else{
		billreqrec.instto.set(payrpf.getBtdate());
	}
	billreqrec.instbchnl.set(b5349DTO.getChdrbillchnl());
	billreqrec.billchnl.set(b5349DTO.getChdrbillchnl());
	billreqrec.instfreq.set(b5349DTO.getBillfreq());
	billreqrec.grpscoy.set(b5349DTO.getGrupcoy());
	billreqrec.grpsnum.set(b5349DTO.getGrupnum());
	billreqrec.membsel.set(b5349DTO.getMembsel());
	billreqrec.mandref.set(b5349DTO.getMandref());
	billreqrec.nextdate.set(payrpf.getNextdate());
	billreqrec.payrpfx.set(b5349DTO.getClntpfx());
	billreqrec.payrcoy.set(b5349DTO.getClntcoy());
	billreqrec.payrnum.set(b5349DTO.getClntnum());
	billreqrec.facthous.set(b5349DTO.getFacthous());
	billreqrec.bankkey.set(b5349DTO.getBankkey());
	billreqrec.bankacckey.set(b5349DTO.getBankacckey());
	if(initFlag && isEQ(b5349DTO.getPtdate(),ZERO)){
		billreqrec.instfrom.set(effDate);
	}
	else{
		billreqrec.instfrom.set(wsaaOldBtdate);
	}
	billreqrec.btdate.set(wsaaOldBtdate);
	billreqrec.duedate.set(wsaaOldBtdate);
	billreqrec.billcd.set(wsaaOldBillcd);
	billreqrec.nextdate.set(wsaaOldNextdate);
	if(initFlag){
		billreqrec.sacscode01.set(t5645rec.sacscode06);
		billreqrec.sacstype01.set(t5645rec.sacstype06);
		billreqrec.glmap01.set(t5645rec.glmap06);
		billreqrec.glsign01.set(t5645rec.sign06);
	}
	else{
	billreqrec.sacscode01.set(t5645rec.sacscode01);
	billreqrec.sacstype01.set(t5645rec.sacstype01);
	billreqrec.glmap01.set(t5645rec.glmap01);
	billreqrec.glsign01.set(t5645rec.sign01);
	}
	billreqrec.sacscode02.set(t5645rec.sacscode02);
	billreqrec.sacstype02.set(t5645rec.sacstype02);
	billreqrec.glmap02.set(t5645rec.glmap02);
	billreqrec.glsign02.set(t5645rec.sign02);
	/* If BILLREQ1 needs to know the PAYER NAME then it will be looked*/
	/* up using the PAYRPFX*/
	billreqrec.payername.set(SPACES);
	if (isNE(b5349DTO.getPayrcntcurr(), b5349DTO.getPayrbillcurr())) {
		currNotEqualBillcurr();
	}
	else {
		if (isEQ(wsaaGotPayrAtBtdate, "Y")) {
			gotPayrAtBtdate();
		}
		else {
			payr();
		}
	}
	if(japanBilling){
		clbapf = clbapfDAO.searchClbapfRecordData(b5349DTO.getBankkey(), 
				b5349DTO.getBankacckey(), b5349DTO.getCowncoy(), 
				b5349DTO.getCownnum(),b5349DTO.getCownpfx());
		if(null != clbapf && isNE(clbapf.getBnkactyp(),SPACES)){
			billreqrec.bankactype.set(clbapf.getBnkactyp().trim());
		}
			
	}
	callProgram(Billreq1.class, billreqrec.billreqRec);
	if (isNE(billreqrec.statuz, varcom.oK)) {
		syserrrec.params.set(billreqrec.billreqRec);
		syserrrec.statuz.set(billreqrec.statuz);
		fatalError600();
	}
	/*  Log number of BEXT recs created.*/
	long billreq = billreqrec.contot01.toLong();
	ctrCT09 = ctrCT09 + billreq;
	/*  Log total amount for this BEXT rec.*/	
	/*
	contotrec.totno.set(controlTotalsInner.ct10);
	contotrec.totval.set(billreqrec.contot02);
	callContot001();	
	*/
	ctrCT10 = ctrCT10.add(BigDecimal.valueOf(billreqrec.contot02.toDouble()));
}

protected void currNotEqualBillcurr(){
	if (isEQ(wsaaGotPayrAtBtdate, "Y")) {
		for (wsaaInstSub.set(1); !(isGT(wsaaInstSub, 6)); wsaaInstSub.add(1)){
			try {
				convertOldInstamts3736();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				fatalError600();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				fatalError600();
			}
		}
	}
	else {
		for (wsaaInstSub.set(1); !(isGT(wsaaInstSub, 6)); wsaaInstSub.add(1)){
			convertInstamts(wsaaInstSub.toInt());
		}
	}
	prasrec.grossprem.set(billreqrec.instamt06);
	/* ADD  WSAA-INCREASE-DUE      TO  PRAS-GROSSPREM.              */
	conlinkrec.amountIn.set(wsaaTax);
	callXcvrt();
	prasrec.grossprem.subtract(conlinkrec.amountOut);
	calculateTaxRelief();
	compute(billreqrec.instamt06, 2).set(sub(billreqrec.instamt06, prasrec.taxrelamt));
	/*                             PRAS-TAXRELAMT +                 */
	/*                             WSAA-INCREASE-DUE.               */
	billreqrec.instamt07.set(ZERO);
	billreqrec.instamt08.set(ZERO);
	billreqrec.instamt09.set(ZERO);
	billreqrec.instamt10.set(ZERO);
	billreqrec.instamt11.set(ZERO);
	billreqrec.instamt12.set(ZERO);
	billreqrec.instamt13.set(ZERO);
	billreqrec.instamt14.set(ZERO);
	billreqrec.instamt15.set(ZERO);	
}

protected void convertInstamts(int ctr){
	switch (ctr){
	case 1:
		if (b5349DTO.getSinstamt01().compareTo(BigDecimal.ZERO) == 0) {
			billreqrec.instamt[ctr].set(ZERO);
		}
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(b5349DTO.getSinstamt01(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			conlinkrec.amountIn.set(wsaaInstPrem.getbigdata());
		}
		else{
			conlinkrec.amountIn.set(b5349DTO.getSinstamt01());
			}
		
	case 2:
		if (b5349DTO.getSinstamt02().compareTo(BigDecimal.ZERO) == 0 ) {
			billreqrec.instamt[ctr].set(ZERO);
		}
		conlinkrec.amountIn.set(b5349DTO.getSinstamt02());		
	case 3:
		if (b5349DTO.getSinstamt03().compareTo(BigDecimal.ZERO) == 0 ) {
			billreqrec.instamt[ctr].set(ZERO);
		}
		conlinkrec.amountIn.set(b5349DTO.getSinstamt03());		
	case 4:
		if (b5349DTO.getSinstamt04().compareTo(BigDecimal.ZERO) == 0 ) {
			billreqrec.instamt[ctr].set(ZERO);
		}
		conlinkrec.amountIn.set(b5349DTO.getSinstamt04());		
	case 5:
		if (b5349DTO.getSinstamt05().compareTo(BigDecimal.ZERO) == 0 ) {
			billreqrec.instamt[ctr].set(ZERO);
		}
		conlinkrec.amountIn.set(b5349DTO.getSinstamt05());	
	case 6:
		if (b5349DTO.getSinstamt06().compareTo(BigDecimal.ZERO) == 0 ) {
			billreqrec.instamt[ctr].set(ZERO);
		}
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(b5349DTO.getSinstamt06(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			conlinkrec.amountIn.set(wsaaInstPrem.getbigdata());
		}
		else{
		conlinkrec.amountIn.set(b5349DTO.getSinstamt06());	
		}
	}
	/* The increase is only added the first time.*/
	if (isEQ(ctr, 1)) {
		conlinkrec.amountIn.add(wsaaIncreaseDue);
	}	
	
	if (isEQ(ctr, 6)) {
		compute(conlinkrec.amountIn, 2).add(add(wsaaTax, wsaaIncreaseDue));
	}
	callXcvrt();
	billreqrec.instamt[ctr].set(conlinkrec.amountOut);
	/*EXIT*/
}

protected void gotPayrAtBtdate()
{   
	if(reinstflag && isFoundPro){
		compute(wsaaInstPrem, 2).set(add(currPayrLif.getSinstamt01(), proratePrem));
		compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
		compute(billreqrec.instamt01, 2).set((add(wsaaInstPrem, wsaaIncreaseDue)));
	}
	else{
		billreqrec.instamt01.set(currPayrLif.getSinstamt01());
	}
	billreqrec.instamt01.add(wsaaIncreaseDue);
	billreqrec.instamt01.add(catchupPrem);
	billreqrec.instamt02.set(currPayrLif.getSinstamt02());
	billreqrec.instamt02.add(catchupPremFee);
	billreqrec.instamt03.set(currPayrLif.getSinstamt03());
	billreqrec.instamt04.set(currPayrLif.getSinstamt04());
	billreqrec.instamt05.set(currPayrLif.getSinstamt05());
	if(reinstflag && isFoundPro){
		compute(wsaaInstPrem, 2).set(add(currPayrLif.getSinstamt06(), proratePrem));
		compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
		compute(prasrec.grossprem, 2).set((add(wsaaInstPrem, wsaaIncreaseDue)));
	}
	else{
	compute(prasrec.grossprem, 2).set((add(currPayrLif.getSinstamt06(), wsaaIncreaseDue)));
	}
	compute(prasrec.grossprem, 2).set((add(prasrec.grossprem, catchupPrem, catchupPremFee)));
	calculateTaxRelief();
	if(reinstflag && isFoundPro){
		compute(wsaaInstPrem, 2).set(add(currPayrLif.getSinstamt06(), proratePrem));
		compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
		compute(billreqrec.instamt06, 2).set(add(sub(currPayrLif.getSinstamt06(), prasrec.taxrelamt), wsaaIncreaseDue));
	}
	else{
	compute(billreqrec.instamt06, 2).set(add(sub(currPayrLif.getSinstamt06(), prasrec.taxrelamt), wsaaIncreaseDue));
	}
	compute(billreqrec.instamt06, 2).set((add(billreqrec.instamt06, catchupPrem,catchupPremFee)));
	billreqrec.instamt06.add(wsaaTax);
	billreqrec.instamt07.set(ZERO);
	billreqrec.instamt08.set(ZERO);
	billreqrec.instamt09.set(ZERO);
	billreqrec.instamt10.set(ZERO);
	billreqrec.instamt11.set(ZERO);
	billreqrec.instamt12.set(ZERO);
	billreqrec.instamt13.set(ZERO);
	billreqrec.instamt14.set(ZERO);
	billreqrec.instamt15.set(ZERO);
}

protected void payr()
{ 	
	if(reinstflag && isFoundPro){
		compute(wsaaInstPrem, 2).set(add(b5349DTO.getSinstamt01(), proratePrem));
		compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
		compute(billreqrec.instamt01, 2).set(add(sub(wsaaInstPrem, prasrec.taxrelamt), wsaaIncreaseDue));
	}
	else{
		billreqrec.instamt01.set(b5349DTO.getSinstamt01());
	}
	billreqrec.instamt01.add(wsaaIncreaseDue);
	billreqrec.instamt01.add(catchupPrem);
	billreqrec.instamt02.set(b5349DTO.getSinstamt02());
	billreqrec.instamt02.add(catchupPremFee);
	billreqrec.instamt03.set(b5349DTO.getSinstamt03());
	billreqrec.instamt04.set(b5349DTO.getSinstamt04());
	billreqrec.instamt05.set(b5349DTO.getSinstamt05());
	if(reinstflag && isFoundPro){
		compute(wsaaInstPrem, 2).set(add(b5349DTO.getSinstamt06(), proratePrem));
		compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
		compute(prasrec.grossprem, 2).set(add(sub(wsaaInstPrem, prasrec.taxrelamt), wsaaIncreaseDue));
	}
	else{
	compute(prasrec.grossprem, 2).set((add(b5349DTO.getSinstamt06(), wsaaIncreaseDue)));
	}
	compute(prasrec.grossprem, 2).set((add(prasrec.grossprem, catchupPrem, catchupPremFee)));
	calculateTaxRelief();
	if(reinstflag && isFoundPro){
		compute(wsaaInstPrem, 2).set(add(b5349DTO.getSinstamt06(), proratePrem));
		compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
		compute(billreqrec.instamt06, 2).set(add(sub(wsaaInstPrem, prasrec.taxrelamt), wsaaIncreaseDue));
	}
	else{
	compute(billreqrec.instamt06, 2).set(add(sub(b5349DTO.getSinstamt06(), prasrec.taxrelamt), wsaaIncreaseDue));
	}
	compute(billreqrec.instamt06, 2).set((add(billreqrec.instamt06, catchupPrem, catchupPremFee)));
	billreqrec.instamt06.add(wsaaTax);
	billreqrec.instamt07.set(ZERO);
	billreqrec.instamt08.set(ZERO);
	billreqrec.instamt09.set(ZERO);
	billreqrec.instamt10.set(ZERO);
	billreqrec.instamt11.set(ZERO);
	billreqrec.instamt12.set(ZERO);
	billreqrec.instamt13.set(ZERO);
	billreqrec.instamt14.set(ZERO);
	billreqrec.instamt15.set(ZERO);
}

protected void convertOldInstamts3736() throws IllegalArgumentException, IllegalAccessException
	{
		/*START*/
			Class  aClass = Payrpf.class;
			Field field;
			Object value=null;
			try {
				field = aClass.getField("sinsamt".concat(wsaaInstSub.toString()));
				Payrpf objectInstance = new Payrpf();
				value = field.get(objectInstance);
			} catch (NoSuchFieldException e) {
				// TODO Auto-generated catch block
				fatalError600();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				fatalError600();
			}

			if (value != null && value.equals(0)){//IJTI-320
				billreqrec.instamt[wsaaInstSub.toInt()].set(ZERO);
			}


			
			/*
		if (isEQ(currPayrLif.getSinstamt(wsaaInstSub.toInt()), ZERO)) {
			billreqrec.instamt[wsaaInstSub.toInt()].set(ZERO);
		}
		
		conlinkrec.amountIn.set(currPayrLif.getSinstamt(wsaaInstSub));
		*/
		/* The increase is only added the first time.*/
		if (isEQ(wsaaInstSub, 1)) {
			conlinkrec.amountIn.add(wsaaIncreaseDue);
		}
		if (isEQ(wsaaInstSub, 6)) {
			compute(conlinkrec.amountIn, 2).add(add(wsaaTax, wsaaIncreaseDue));
		}
		callXcvrt5000();
		billreqrec.instamt[wsaaInstSub.toInt()].set(conlinkrec.amountOut);
		/*EXIT*/
	}

protected void writePtrnRecord(){

	String wsaaItem;
	String wsaaItem1;

	ptrnpf = new Ptrnpf();
	ptrnpf.setChdrcoy(payxTemppf.getChdrcoy());
	ptrnpf.setChdrpfx(b5349DTO.getChdrpfx());
	ptrnpf.setChdrnum(payxTemppf.getChdrnum());
	ptrnpf.setTranno(chdrpf.getTranno());
	ptrnpf.setTrdt(Integer.parseInt(getCobolDate()));
	ptrnpf.setTrtm(varcom.vrcmTime.toInt());
	ptrnpf.setValidflag("1");
	if (flexiblePremiumContract.isTrue()) {
		ptrnpf.setPtrneff(wsaaOldBtdate.toInt());
	}
	else {
		ptrnpf.setPtrneff(linspf.getInstfrom());
	}
	ptrnpf.setUserT(0);
	ptrnpf.setBatccoy(batcdorrec.company.toString());
	ptrnpf.setBatcpfx(batcdorrec.prefix.toString());
	ptrnpf.setBatcbrn(batcdorrec.branch.toString());
	ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
	ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
	ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
	ptrnpf.setBatcbatch(batcdorrec.batch.toString());
	ptrnpf.setDatesub(bsscIO.getEffectiveDate().toInt());	
	ptrnBulkInsList.add(ptrnpf);

	//ILIF-3997-STARTS
	wsaaItem = payrpf.getBillchnl().trim();
	wsaaItem1 = wsaaItem.concat(chdrpf.getCnttype().trim());
	if(BTPRO028Permission) {                                          
		wsaaItem1 =	wsaaItem1.concat(payrpf.getBillfreq().trim());
	}
	//Chdrpf chdrpf = null;
	//chdrpf = chdrpfDAO.getchdrRecord(ptrnIO.getChdrcoy().toString(),ptrnIO.getChdrnum().toString());
	if((isUpdateNlgt(wsaaItem1))
		&& (chdrpf != null && chdrpf.getNlgflg() != null && chdrpf.getNlgflg() == 'Y')){
		writeNLGRec(ptrnpf);
	}
	//ILIF-3997-ENDS
	
	ctrCT03++;
}

protected void commitPtrnBulkInsert(){	 
	 boolean isInsertPtrnPF = ptrnpfDAO.insertPtrnPF(ptrnBulkInsList);	
	if (!isInsertPtrnPF) {
		LOGGER.error("Insert PtrnPF record failed.");
		fatalError600();
	}else ptrnBulkInsList.clear();
}
//ILIF-3997-STARTS
protected void writeNLGRec(Ptrnpf ptrnIO) {
	nlgcalcrec.chdrcoy.set (ptrnIO.getChdrcoy());
	nlgcalcrec.chdrnum.set(ptrnIO.getChdrnum());
	nlgcalcrec.tranno.set(ptrnIO.getTranno());
	nlgcalcrec.effdate.set(ptrnIO.getPtrneff());
	nlgcalcrec.batcactyr.set(ptrnIO.getBatcactyr());
	nlgcalcrec.batcactmn.set(ptrnIO.getBatcactmn());
	nlgcalcrec.batctrcde.set(ptrnIO.getBatctrcde());
	nlgcalcrec.cnttype.set(chdrpf.getCnttype());
	nlgcalcrec.language.set(bsscIO.getLanguage());
	nlgcalcrec.frmdate.set(chdrpf.getOccdate());
	nlgcalcrec.occdate.set(chdrpf.getOccdate());
	nlgcalcrec.todate.set(varcom.vrcmMaxDate);
	nlgcalcrec.ptdate.set(chdrpf.getPtdate());
	if (isLT(payrpf.getBtdate(), payrpf.getEffdate())) {
		nlgcalcrec.inputAmt.set(currPayrLif.getSinstamt01());
	}
	else
	{
		nlgcalcrec.inputAmt.set(payrpf.getSinstamt01());
	}
	nlgcalcrec.inputAmt.add(wsaaIncreaseDue);
	nlgcalcrec.inputAmt.add(catchupPrem);
	compute(nlgcalcrec.inputAmt, 0).set(mult(-1, nlgcalcrec.inputAmt));
	nlgcalcrec.function.set("OVDUE");
	callProgram(Nlgcalc.class, nlgcalcrec.nlgcalcRec) ;
	if (isNE(nlgcalcrec.status, varcom.oK)) {
		syserrrec.statuz.set(batcuprec.statuz);
		fatalError600();
	}
}
//ILIF-3997-ENDS
/*	{
		/*START*/
		/* Update the CHDR bill supression details*/
		/* MOVE 0                      TO CHDRLIF-BILLSPFROM            */
		/*                                CHDRLIF-BILLSPTO.             */
		/* MOVE 'N'                    TO CHDRLIF-BILLSUPR.             */
		/*                                                         <D9604>*/
		/* Restore outstanding amount if this is a flexible premium<D9604>*/
		/*if (flexiblePremiumContract.isTrue()) {
			chdrlifIO.setOutstamt(wsaaOldOutstamt);
		}
		/* Rewrite contract header*/
		/*chdrlifIO.setFunction(varcom.rewrt);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	//}

protected void rewriteChdr()
{
	if(chkFlag)
		return;
	if(initFlag && linsAvailable)
		return;
	/* Restore outstanding amount if this is a flexible premium<D9604>*/
	if (flexiblePremiumContract.isTrue()) {
		chdrpf.setOutstamt(BigDecimal.valueOf(wsaaOldOutstamt.toDouble()));
	}
	/* Rewrite contract header*/
	chdrpf.setUniqueNumber(b5349DTO.getChdruniqueno());
	chdrBulkUpdtList.add(chdrpf);
}

protected void commitChdrBulkUpdate(){	 
	if(japanBilling && isEQ(bprdIO.getSystemParam05(), "JPN") && chdrBulkUpdtList.isEmpty())
		return;
	boolean isUpdateChdrLif = chdrpfDAO.updateChdrLif(chdrBulkUpdtList);	
	if (!isUpdateChdrLif) {
		LOGGER.error("Update CHDRPF record failed.");
		fatalError600();
	}else chdrBulkUpdtList.clear();
}

protected void rewritePayr()
{
	if(chkFlag)
		return;
	
	/* Restore original outstanding amount                     <D9604>*/
	if (flexiblePremiumContract.isTrue()) {
		payrpf.setOutstamt(BigDecimal.valueOf(wsaaOldOutstamt.toDouble()));
	}
	/* Rewrite the PAYR record*/
	payrpf.setChdrcoy(payxTemppf.getChdrcoy());
	payrpf.setChdrnum(payxTemppf.getChdrnum());
	payrpf.setPayrseqno(payxTemppf.getPayrseqno());
	if (isEQ(wsaaGotPayrAtBtdate, "Y")){ 
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(currPayrLif.getSinstamt06(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			payrpf.setSinstamt06(wsaaInstPrem.getbigdata());
		}
		else{
		payrpf.setSinstamt06(currPayrLif.getSinstamt06());
		}
	}
	else{
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(b5349DTO.getSinstamt06(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			payrpf.setSinstamt06(wsaaInstPrem.getbigdata());
		}
		else{
		payrpf.setSinstamt06(b5349DTO.getSinstamt06());
		}
	}

	payrBulkUpdList.add(payrpf);
}


protected void commitPayrBulkUpdate(){	 
	boolean isUpdatePayrPF = payrpfDAO.updatePayrPF(payrBulkUpdList,isBilldayUpdated,false);	
	if (!isUpdatePayrPF) {
		LOGGER.error("Update PayrPF record failed.");
		fatalError600();
	}else payrBulkUpdList.clear();
}

protected void writeLetter(){
	readTr384();
	if (isEQ(tr384rec.letterType, SPACES)) {
		return ;
	}
	/*   Get set-up parameter for call 'HLETRQS'                   */
	letrqstrec.statuz.set(SPACES);
	letrqstrec.requestCompany.set(payxTemppf.getChdrcoy());
	/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
	letrqstrec.letterType.set(tr384rec.letterType);
	letrqstrec.letterRequestDate.set(bsscIO.getEffectiveDate());
	letrqstrec.rdocpfx.set(b5349DTO.getChdrpfx());	
	letrqstrec.rdoccoy.set(payxTemppf.getChdrcoy());
	letrqstrec.rdocnum.set(payxTemppf.getChdrnum());
	letrqstrec.otherKeys.set(bsscIO.getLanguage());
	letrqstrec.clntcoy.set(b5349DTO.getCowncoy());
	letrqstrec.clntnum.set(b5349DTO.getCownnum());
	letrqstrec.chdrcoy.set(payxTemppf.getChdrcoy());
	letrqstrec.chdrnum.set(payxTemppf.getChdrnum());
	letrqstrec.tranno.set(b5349DTO.getChdrtranno() + 1);
	letrqstrec.branch.set(b5349DTO.getCntbranch());
	letrqstrec.trcde.set(bprdIO.getAuthCode());
	letrqstrec.function.set("ADD");
	/* CALL 'HLETRQS' USING LETRQST-PARAMS.                 <PCPPRT>*/
	callProgram(Letrqst.class, letrqstrec.params);
	if (isNE(letrqstrec.statuz, varcom.oK)) {
		syserrrec.params.set(letrqstrec.params);
		syserrrec.statuz.set(letrqstrec.statuz);
		fatalError600();
	}		
}

	protected void sendBillingNoticeSmsNotification() {

	}

protected void releaseSoftlock4000()
	{
		start4010();
	}

protected void start4010()
	{
		/* Release the soft lock on the contract.*/
		sftlockrec.company.set(batcdorrec.company);
		sftlockrec.entity.set(payxTemppf.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(999999);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void commit3500(){
	if (!noRecordFound){
		commitControlTotals();
		if(isBulkProcessTaxdPF) commitTaxdBulkInsert();
		//if(isBulkProcessLinsPF) commitLinsBulkInsert();//ILIFE-5975
		commitPtrnBulkInsert();
		if(isBulkProcessFpcoPF) commitFpcoBulkUpdate();
		if(isBulkProcessFprmPF) commitFprmBulkUpdate();
		if(isBulkProcessHdivPF) commitHdivBulkInsert();
		commitChdrBulkUpdate();
		commitPayrBulkUpdate();
		if (japanBilling && isBulkProcessHpadPF){
			commitHpadBulkUpdate();
		}
	}
}

protected void rollback3600(){
	
}

protected void close4000(){
	if (!noRecordFound){	
		
		t5679ListMap.clear();
		t5679ListMap = null;
		
		t5645ListMap.clear();
		t5645ListMap = null;
		
		t3695ListMap.clear();
		t3695ListMap = null;
		
		t3620ListMap.clear();
		t3620ListMap = null;
		
		t3629ListMap.clear();
		t3629ListMap = null;
		
		t6654ListMap.clear();
		t6654ListMap = null;
		
		t6687ListMap.clear();
		t6687ListMap = null;
		
		t5729ListMap.clear();
		t5729ListMap = null;
		
		tr52dListMap.clear();
		tr52dListMap = null;
		
		tr384ListMap.clear();
		tr384ListMap = null;
		
		covrListMap.clear();
		covrListMap = null;
		
		acblListMap.clear();
		acblListMap = null;
		
		payrListMap.clear();
		payrListMap = null;
		
		incrListMap.clear();
		incrListMap = null;
		
		fpcoListMap.clear();
		fpcoListMap = null;
		
		fprmListMap.clear();
		fprmListMap = null;
		
		ddsuListMap.clear();
		ddsuListMap = null;
		
		hdisListMap.clear();
		hdisListMap = null;
		
		hdivcshListMap.clear();
		hdivcshListMap = null;
		
		hcsdListMap.clear();
		hcsdListMap = null;
				
		tr517ListMap.clear();
		tr517ListMap = null;
		
		tr52eListMap.clear();
		tr52eListMap = null;
		
		chdrBulkUpdtList.clear();
		chdrBulkUpdtList = null;
		
		payrBulkUpdList.clear();
		payrBulkUpdList = null;
		
		ptrnBulkInsList.clear();
		ptrnBulkInsList = null;
		
		linsBulkInsList.clear();
		linsBulkInsList = null;
		
		fpcoBulkUpdList.clear();
		fpcoBulkUpdList = null;
		
		fprmBulkUpdList.clear();
		fprmBulkUpdList = null;
		
		taxdBulkInsList.clear();
		taxdBulkInsList = null;
		
		hdivBulkInsList.clear();
		hdivBulkInsList = null;
		if(reinstflag){
			td5j2ListMap.clear();
			td5j2ListMap = null;
			
			td5j1ListMap.clear();
			td5j1ListMap = null;
		}
		if (japanBilling) {
			th5agListMap.clear();
			th5agListMap = null;
			t3615ListMap.clear();
			t3615ListMap = null;
			t5688ListMap.clear();
			t5688ListMap = null;
			t5674ListMap.clear();
			t5674ListMap = null;
		}
		if(BTPRO036Permission) {
			t5688ListMap.clear();
			t5688ListMap = null;
			t5674ListMap.clear();
			t5674ListMap = null;
		}
		lsaaStatuz.set(varcom.oK);
	}
}

protected void callXcvrt(){
	conlinkrec.currIn.set(b5349DTO.getPayrcntcurr());
	conlinkrec.currOut.set(b5349DTO.getPayrbillcurr());
	conlinkrec.rateUsed.set(0);
	conlinkrec.amountOut.set(0);
	/*  MOVE BSSC-EFFECTIVE-DATE    TO CLNK-CASHDATE.                */
	conlinkrec.cashdate.set(wsaaCashdate);
	conlinkrec.function.set("SURR");
	conlinkrec.company.set(batcdorrec.company);
	callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
	if (isNE(conlinkrec.statuz, varcom.oK)) {
		syserrrec.params.set(conlinkrec.clnk002Rec);
		syserrrec.statuz.set(conlinkrec.statuz);
		fatalError600();
	}

	if (isNE(conlinkrec.amountOut, 0)) {
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		zrdecplrec.currency.set(b5349DTO.getPayrbillcurr());
		callRounding8000();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}	
}
		
protected void callXcvrt5000()
	{
		start5000();
	}

protected void start5000()
	{
		conlinkrec.currIn.set(b5349DTO.getPayrcntcurr());
		conlinkrec.currOut.set(b5349DTO.getPayrbillcurr());
		conlinkrec.rateUsed.set(0);
		conlinkrec.amountOut.set(0);
		/*  MOVE BSSC-EFFECTIVE-DATE    TO CLNK-CASHDATE.                */
		conlinkrec.cashdate.set(wsaaCashdate);
		conlinkrec.function.set("SURR");
		conlinkrec.company.set(batcdorrec.company);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}

		if (isNE(conlinkrec.amountOut, 0)) {
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			zrdecplrec.currency.set(b5349DTO.getPayrbillcurr());
			callRounding8000();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
		}
	}

protected void calculateTaxRelief()
{
	linspf.setTaxrelmth("");
	prasrec.taxrelamt.set(ZERO);	
	if ("".equals(b5349DTO.getTaxrelmth().trim())) return;

	/* Look up the subroutine on T6687 Array in working storage.*/
	readT6687();

	if (isEQ(strT6687TaxRelSub, SPACES)) {
		return ;
	}
	/* If the tax relief method is not spaces calculate the tax*/
	/* relief amount and deduct it from the premium......*/
	prasrec.clntnum.set(b5349DTO.getClntnum());
	prasrec.clntcoy.set(b5349DTO.getClntcoy());
	prasrec.incomeSeqNo.set(b5349DTO.getIncseqno());
	prasrec.cnttype.set(b5349DTO.getCnttype());
	prasrec.taxrelmth.set(b5349DTO.getTaxrelmth());
	prasrec.effdate.set(payrpf.getBillcd());
	prasrec.company.set(payxTemppf.getChdrcoy());
	prasrec.statuz.set(varcom.oK);
	callProgram(strT6687TaxRelSub, prasrec.prascalcRec);
	if (isNE(prasrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(prasrec.statuz);
		syserrrec.subrname.set(strT6687TaxRelSub);
		wsysSysparams.set(prasrec.prascalcRec);
		syserrrec.params.set(wsysSystemErrorParams);
		fatalError600();
	}

	if (isNE(prasrec.taxrelamt, 0)) {
		zrdecplrec.amountIn.set(prasrec.taxrelamt);
		zrdecplrec.currency.set(b5349DTO.getBillcurr());
		callRounding8000();
		prasrec.taxrelamt.set(zrdecplrec.amountOut);
	}
	if (isNE(prasrec.taxrelamt, 0)) {
		linspf.setTaxrelmth(b5349DTO.getTaxrelmth());
	}
}

protected void writeFpco()
{
	fprmpf = new Fprmpf();
	TotalAmount = BigDecimal.ZERO;
	wsaaOverduePer.set(ZERO);
	
	String chdrnum = payxTemppf.getChdrnum();
    if (covrListMap != null && covrListMap.containsKey(chdrnum)) {
        List<Covrpf> covrpfList = covrListMap.get(chdrnum);
        for (Iterator<Covrpf> iterator = covrpfList.iterator(); iterator.hasNext(); ){
        	Covrpf covr = iterator.next();
        	readCovrlnb(covr);
        	iterator.remove();
        }
    }	

	int payrSeqNo = 0;
	if (isEQ(wsaaGotPayrAtBtdate, "Y")) {
		payrSeqNo = currPayrLif.getPayrseqno();
	}
	else {
		payrSeqNo = payxTemppf.getPayrseqno();
	} 
	
	/* Read the FPRM record:  */
	Fprmpf currFprm = null;
	chdrnum = payxTemppf.getChdrnum();
    if (fprmListMap != null && fprmListMap.containsKey(chdrnum)) {
        List<Fprmpf> fprmpfList = fprmListMap.get(chdrnum);
        for (Iterator<Fprmpf> iterator = fprmpfList.iterator(); iterator.hasNext(); ){
        	Fprmpf fprm = iterator.next();
        	if (Integer.compare(fprm.getPayrseqno(),payrSeqNo) == 0 ) {
        		currFprm = fprm;
        		break;
        	}
        }
    }    

	/*bug #ILIFE-1055 START*/
	//payrIO.sinstamt02 CONTRACT MANAGEMENT FEE FROM P5074AT
    TotalAmount = TotalAmount.add(b5349DTO.getSinstamt02());
	
	/*bug #ILIFE-1055 end*/
	/* Update total billed:                                    <D9604>*/
    //IJTI-320 START
    if(currFprm != null){
		setPrecision(currFprm.getTotbill(), 2);
		fprmpf.setUnique_number(currFprm.getUnique_number());
		fprmpf.setTotbill(currFprm.getTotbill().add(TotalAmount));
    }
    //IJTI-320 END
	/* Log total billed:                                       <D9604>*/
	
	/*
	String wsaaTotAmt;
	wsaaTotAmt = ZonedDecimalData.toStringRawStaticBigDecimal(TotalAmount, 17, 2);
	contotrec.totno.set(controlTotalsInner.ct12);
	contotrec.totval.set(wsaaTotAmt);
	callContot001();	
	*/
	 if ( isEQ(b5349DTO.getBillfreq(),"12") && ctrCT12!=BigDecimal.ZERO )
	    {	 
		 ctrCT12 =	ctrCT12.add(b5349DTO.getSinstamt01());
	    }
	 
	ctrCT12 = ctrCT12.add(TotalAmount);
	//IJTI-320 START
	 if(currFprm != null){
		setPrecision(currFprm.getMinreqd(), 3);
		fprmpf.setMinreqd(BigDecimal.valueOf(wsaaOverduePer.toDouble()).divide(BigDecimal.valueOf(100)).multiply(TotalAmount).add(currFprm.getMinreqd()));
	 
	
	/* MOVE FPRM-MIN-PRM-REQD      TO ZRDP-AMOUNT-IN.               */
	/* MOVE PAYR-BILLCURR          TO ZRDP-CURRENCY.                */
	/* PERFORM 8000-CALL-ROUNDING.                                  */
	/* MOVE ZRDP-AMOUNT-OUT        TO FPRM-MIN-PRM-REQD.            */
	if (isNE(currFprm.getMinreqd(), 0)) {
		zrdecplrec.amountIn.set(currFprm.getMinreqd());
		zrdecplrec.currency.set(b5349DTO.getPayrbillcurr());
		callRounding8000();
		fprmpf.setMinreqd(BigDecimal.valueOf(zrdecplrec.amountOut.toDouble()));
	}
	}
	//IJTI-320 END
	/* Write the record back to FPRM file.                     <D9604>*/
	isBulkProcessFprmPF = true;	
	fprmBulkUpdList.add(fprmpf);

}

protected void commitFprmBulkUpdate(){	 
	isBulkProcessFprmPF = false;
	boolean isUpdateFprmPF = fprmpfDAO.updateFprmPF(fprmBulkUpdList);	
	if (!isUpdateFprmPF) {
		LOGGER.error("Update FPRMPF record failed.");
		fatalError600();
	}else fprmBulkUpdList.clear();
}

protected void readCovrlnb(Covrpf covr)
{
	/* Check to see if coverage is of a valid status           <D9604>*/
	wsaaValidCoverage.set("N");
	if (isNE(covr.getValidflag(), "1")) {
		return ;
	}
	for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
		if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covr.getStatcode())) {
			for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
				if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], covr.getPstatcode())) {
					wsaaT5679Sub.set(13);
					wsaaValidCoverage.set("Y");
				}
			}
		}
	}
	/*  If the coverage is not of a valid status read the next <D9604>*/
	/*  record for the contract:                               <D9604>*/
	if (!validCoverage.isTrue()) return ;
	
	if (isEQ(covr.getInstprem(), 0)) return ;
	Incrpf currIncr = null;
	String chdrnum = payxTemppf.getChdrnum();
	boolean incrFound = false;
    if (incrListMap != null && incrListMap.containsKey(chdrnum)) {
        List<Incrpf> incrpfList = incrListMap.get(chdrnum);
        for (Iterator<Incrpf> iterator = incrpfList.iterator(); iterator.hasNext(); ){
        	Incrpf incr = iterator.next();

        	if (incr.getCoverage().equals(covr.getCoverage())
        		&& 	incr.getRider().equals(covr.getRider())
        		&& Integer.compare(incr.getPlnsfx(),covr.getPlanSuffix()) == 0 ) {
        			currIncr = incr;
        			incrFound = true;
        			break;
        			
        	}
        }
    }	
	if(incrFound && isLT(currIncr.getCrrcd(), b5349DTO.getBtdate())){
		compute(wsaaCovrInc, 2).set(sub(currIncr.getNewinst(), currIncr.getLastInst()));
	}
	else  wsaaCovrInc.set(ZERO);


	/* Select FPCO record which is active and has not reached  <D9604>*/
	/* Target Premium                                          <D9604>*/
	Fpcopf currFpco = null;
	chdrnum = payxTemppf.getChdrnum();
    if (fpcoListMap != null && fpcoListMap.containsKey(chdrnum)) {
        List<Fpcopf> fpcopfList = fpcoListMap.get(chdrnum);
        
        for (Iterator<Fpcopf> iterator = fpcopfList.iterator(); iterator.hasNext(); ){
        	Fpcopf fpco = iterator.next();
        	if (fpco.getLife().equals(covr.getLife())
        		&& fpco.getCoverage().equals(covr.getCoverage())
        		&& 	fpco.getRider().equals(covr.getRider())
        		&& Integer.compare(fpco.getPlnsfx(),covr.getPlanSuffix()) == 0 ) {
        			readFpco(fpco, covr);
        			currFpco = fpco;
        			iterator.remove();
        			break;	
            }        	
        }
    }
    
	/*bug #ILIFE-1055 start*/
	TotalAmount = TotalAmount.add(covr.getInstprem()).add(BigDecimal.valueOf(wsaaCovrInc.toDouble()));
	/*bug #ILIFE-1055 END*/
}

protected void readFpco(Fpcopf fpco, Covrpf covr){
	fpcopf = new Fpcopf();	
	if (isGT(fpco.getBilledp(), fpco.getPrmper())
	|| isEQ(fpco.getBilledp(), fpco.getPrmper())) return ;
	
	/* Update FPCO record with total of installment due and    <D9604>*/
	/* overdue minimum percentage. This should include any     <D9604>*/
	/* increases due                                           <D9604>*/
	setPrecision(fpco.getBilledp(), 2);
	fpcopf.setBilledp(fpco.getBilledp().add(covr.getInstprem()).add(BigDecimal.valueOf(wsaaCovrInc.toDouble())));
	
	/*bug #ILIFE-1055 start*/
	//compute(wsaaTotAmt, 2).set(add(add(wsaaTotAmt, covrlnbIO.getInstprem()), wsaaCovrInc));
	/*bug #ILIFE-1055 END*/
	wsaaOverduePer.set(fpco.getMinovrpro());
	setPrecision(fpco.getOvrminreq(), 3);
	fpcopf.setOvrminreq(BigDecimal.valueOf(wsaaOverduePer.toDouble()).divide(BigDecimal.valueOf(100)).multiply(covr.getInstprem().add(BigDecimal.valueOf(wsaaCovrInc.toDouble()))).add(fpco.getOvrminreq()));
	

	if (isNE(fpco.getOvrminreq(), 0)) {
		zrdecplrec.amountIn.set(fpco.getOvrminreq());
		zrdecplrec.currency.set(b5349DTO.getBillcurr());
		callRounding8000();
		fpcopf.setOvrminreq(BigDecimal.valueOf(zrdecplrec.amountOut.toDouble()));
	}
	/* Write the record back to FPCO file.                     <D9604>*/
	/* MOVE REWRT                  TO FPCO-FUNCTION.        <V65L19>*/
	isBulkProcessFpcoPF = true;
	fpcopf.setUniqueNumber(fpco.getUniqueNumber());
	fpcoBulkUpdList.add(fpcopf);
}

protected void commitFpcoBulkUpdate(){	 
	isBulkProcessFpcoPF = false;
	boolean isUpdateFpcoPF = fpcopfDAO.updateFpcoPF(fpcoBulkUpdList);	
	if (!isUpdateFpcoPF) {
		LOGGER.error("Update FPCOPF record failed.");
		fatalError600();
	}else fpcoBulkUpdList.clear();
}

protected void commitHpadBulkUpdate() {
	isBulkProcessHpadPF = false;
	boolean isUpdateHpadLif = hpadpfDAO.updateFirstprmrcpdateList(hpadBulkUpdtList);
	if (!isUpdateHpadLif) {
		LOGGER.error("Update HPADPF record failed.");
		fatalError600();
	} else
	 hpadBulkUpdtList.clear();
}
protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(batcdorrec.trcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void h100TfrDvdSusp()
	{
		try {
			h100Para();
			h120WriteHdiv();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	***************************                               <LA2106>
	* </pre>
	*/
protected void h100Para()
	{
		/*                                                         <LA2106>*/
		/* Determine the shortfall from the premium suspense for billing   */
		compute(wsaaShortfall, 2).set(sub(conlinkrec.amountOut, wsaaPremSusp));
		/*                                                         <LA2106>*/
		/* Shortfall < or = zero, no billing generated             <LA2106>*/
		if (isLTE(wsaaShortfall, 0)) {
			goTo(GotoLabel.h190Exit);
		}
		else {
			/*                                                         <LA2106>*/
			/* Shortfall > dividend suspense, transfer from dividend suspense  */
			/* Note that with tolerance limit, even the shortfall > dvd-susp,  */
			/* there is a chance no billing will be necessary.         <LA2106>*/
			if (isGT(wsaaShortfall, wsaaDvdSusp)) {
				wsaaTfrAmt.set(wsaaDvdSusp);
			}
			else {
				/*                                                         <LA2106>*/
				/* Dividend suspense > shortfall, transfer enough for dvd-susp     */
				/* no billing required.                                    <LA2106>*/
				wsaaTfrAmt.set(wsaaShortfall);
			}
		}
		/*                                                         <LA2106>*/
		/* Set up common fields in LIFA once only                  <LA2106>*/
		wsaaJrnseq.set(0);
		lifacmvrec.batccoy.set(batcdorrec.company);
		lifacmvrec.batcbrn.set(batcdorrec.branch);
		lifacmvrec.batcactyr.set(batcdorrec.actyear);
		lifacmvrec.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec.batctrcde.set(batcdorrec.trcde);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.rldgcoy.set(batcdorrec.company);
		lifacmvrec.genlcoy.set(batcdorrec.company);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.rdocnum.set(payxTemppf.getChdrnum());
		lifacmvrec.rldgacct.set(payxTemppf.getChdrnum());
		lifacmvrec.tranref.set(payxTemppf.getChdrnum());
		lifacmvrec.tranno.set(chdrpf.getTranno());
		lifacmvrec.effdate.set(payrpf.getBillcd());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.transactionDate.set(bsscIO.getEffectiveDate());
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.trandesc.set("Dividend Transfer");
		lifacmvrec.substituteCode[1].set(b5349DTO.getCnttype());
		/*  Compare contract currency to billing and post accordingly.     */
		if (isNE(b5349DTO.getChdrcntcurr(), b5349DTO.getBillcurr())) {
			conlinkrec.statuz.set(SPACES);
			conlinkrec.currIn.set(b5349DTO.getBillcurr());
			conlinkrec.amountIn.set(wsaaTfrAmt);
			conlinkrec.cashdate.set(varcom.vrcmMaxDate);
			conlinkrec.currOut.set(b5349DTO.getChdrcntcurr());
			conlinkrec.company.set(payxTemppf.getChdrcoy());
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.function.set("REAL");
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz, "****")) {
				syserrrec.params.set(conlinkrec.clnk002Rec);
				syserrrec.statuz.set(conlinkrec.statuz);
				fatalError600();
			}
			if (isNE(conlinkrec.amountOut, 0)) {
				zrdecplrec.amountIn.set(conlinkrec.amountOut);
				zrdecplrec.currency.set(b5349DTO.getChdrcntcurr());
				callRounding8000();
				conlinkrec.amountOut.set(zrdecplrec.amountOut);
			}
			/*                                                         <LA2106>*/
			/* Transfer out Dividend Suspense                          <LA2106>*/
			lifacmvrec.origamt.set(conlinkrec.amountOut);
			lifacmvrec.origcurr.set(b5349DTO.getChdrcntcurr());
			lifacmvrec.sacscode.set(t5645rec.sacscode03);
			lifacmvrec.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec.glcode.set(t5645rec.glmap03);
			lifacmvrec.glsign.set(t5645rec.sign03);
			lifacmvrec.contot.set(t5645rec.cnttot03);
			h200PostAcmvRecord();
			/*                                                         <LA2106>*/
			/* Transfer Dividend into Currency Exchange Account        <LA2106>*/
			lifacmvrec.sacscode.set(t5645rec.sacscode04);
			lifacmvrec.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec.glcode.set(t5645rec.glmap04);
			lifacmvrec.glsign.set(t5645rec.sign04);
			lifacmvrec.contot.set(t5645rec.cnttot04);
			h200PostAcmvRecord();
			/*                                                         <LA2106>*/
			/* Transfer out Dividend Suspense from Currency Exchange Account   */
			lifacmvrec.origamt.set(wsaaTfrAmt);
			lifacmvrec.origcurr.set(b5349DTO.getBillcurr());
			lifacmvrec.sacscode.set(t5645rec.sacscode05);
			lifacmvrec.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec.glcode.set(t5645rec.glmap05);
			lifacmvrec.glsign.set(t5645rec.sign05);
			lifacmvrec.contot.set(t5645rec.cnttot05);
			h200PostAcmvRecord();
			/*                                                         <LA2106>*/
			/* Transfer Dividend Suspense into Premium Suspense        <LA2106>*/
			if(initFlag){
			lifacmvrec.sacscode.set(t5645rec.sacscode06);
			lifacmvrec.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec.glcode.set(t5645rec.glmap06);
			lifacmvrec.glsign.set(t5645rec.sign06);
			lifacmvrec.contot.set(t5645rec.cnttot06);	
			}
			else{
			lifacmvrec.sacscode.set(t5645rec.sacscode01);
			lifacmvrec.sacstyp.set(t5645rec.sacstype01);
			lifacmvrec.glcode.set(t5645rec.glmap01);
			lifacmvrec.glsign.set(t5645rec.sign01);
			lifacmvrec.contot.set(t5645rec.cnttot01);
			}
			h200PostAcmvRecord();
			/*                                                         <LA2106>*/
			/* Set WSAA-TFR-AMT to converted amount in contract currency       */
			wsaaTfrAmt.set(conlinkrec.amountOut);
		}
		else {
			/*                                                         <LA2106>*/
			/* Transfer out Dividend Suspense                          <LA2106>*/
			lifacmvrec.origamt.set(wsaaTfrAmt);
			lifacmvrec.origcurr.set(b5349DTO.getChdrcntcurr());
			lifacmvrec.sacscode.set(t5645rec.sacscode03);
			lifacmvrec.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec.glcode.set(t5645rec.glmap03);
			lifacmvrec.glsign.set(t5645rec.sign03);
			lifacmvrec.contot.set(t5645rec.cnttot03);
			h200PostAcmvRecord();
			/*                                                         <LA2106>*/
			/* Transfer Dividend Suspense into Premium Suspense        <LA2106>*/
			if(initFlag){
			lifacmvrec.sacscode.set(t5645rec.sacscode06);
			lifacmvrec.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec.glcode.set(t5645rec.glmap06);
			lifacmvrec.glsign.set(t5645rec.sign06);
			lifacmvrec.contot.set(t5645rec.cnttot06);	
			}
			else{
			lifacmvrec.sacscode.set(t5645rec.sacscode01);
			lifacmvrec.sacstyp.set(t5645rec.sacstype01);
			lifacmvrec.glcode.set(t5645rec.glmap01);
			lifacmvrec.glsign.set(t5645rec.sign01);
			lifacmvrec.contot.set(t5645rec.cnttot01);
			}
			h200PostAcmvRecord();
		}
	}

protected void h120WriteHdiv()
	{
		/*                                                         <LA2106>*/
		/* Now write HDIV to denote the withdrawal of dividend at coverage */
		/* level.  So spread the amount across all the coverages according */
		/* to their share.                                         <LA2106>*/
		initialize(wsaaHdisArrayInner.wsaaHdisArray);		
		Hdispf currHdis = null;
		String chdrnum = payxTemppf.getChdrnum();
		int intCtr = 0;
	    if (hdisListMap != null && hdisListMap.containsKey(chdrnum)) {
	        List<Hdispf> hdispfList = hdisListMap.get(chdrnum);
	        for (Iterator<Hdispf> iterator = hdispfList.iterator(); iterator.hasNext(); ){
	        	Hdispf hdis = iterator.next();

	            if (Integer.compare(hdis.getPlnsfx(), 0)==0) {
	            		processHdis(hdis, intCtr++);
	            		currHdis = hdis;
	            		break;
	            }        	
	        }
	    }
	    
		wsaaAllDvdTot.set(ZERO);


		/*                                                         <LA2106>*/
		/* Set total no. of HDIS read                              <LA2106>*/
		wsaaNoOfHdis.set(intCtr);
		/*                                                         <LA2106>*/
		/* Then calculate share of each coverage on the dividend   <LA2106>*/
		wsaaIdx.set(ZERO);
		while ( !(isGTE(wsaaIdx, wsaaNoOfHdis))) {
			wsaaIdx.add(1);
			if (isEQ(wsaaIdx, wsaaNoOfHdis)) {
				compute(wsaaHdisArrayInner.wsaaDvdShare[wsaaIdx.toInt()], 2).set(sub(wsaaTfrAmt, wsaaRunDvdTot));
			}
			else {
				compute(wsaaHdisArrayInner.wsaaDvdShare[wsaaIdx.toInt()], 2).set(mult(wsaaTfrAmt, (div(wsaaHdisArrayInner.wsaaDvdTot[wsaaIdx.toInt()], wsaaAllDvdTot))));
				wsaaRunDvdTot.add(wsaaHdisArrayInner.wsaaDvdShare[wsaaIdx.toInt()]);
			}
			if (isNE(wsaaHdisArrayInner.wsaaDvdShare[wsaaIdx.toInt()], 0)) {
				zrdecplrec.amountIn.set(wsaaHdisArrayInner.wsaaDvdShare[wsaaIdx.toInt()]);
				zrdecplrec.currency.set(b5349DTO.getChdrcntcurr());
				callRounding8000();
				wsaaHdisArrayInner.wsaaDvdShare[wsaaIdx.toInt()].set(zrdecplrec.amountOut);
			}
		}

		/*                                                         <LA2106>*/
		/* Write withdrawn amount for each coverage                <LA2106>*/
		wsaaIdx.set(ZERO);
		while ( !(isGTE(wsaaIdx, wsaaNoOfHdis))) {
			wsaaIdx.add(1);
			
			hdivpf = new Hdivpf();

			hdivpf.setChdrcoy(payxTemppf.getChdrcoy());
			hdivpf.setChdrnum(payxTemppf.getChdrnum());
			hdivpf.setLife(wsaaHdisArrayInner.wsaaHdisLife[wsaaIdx.toInt()].toString());
			hdivpf.setJlife(wsaaHdisArrayInner.wsaaHdisJlife[wsaaIdx.toInt()].toString());
			hdivpf.setCoverage(wsaaHdisArrayInner.wsaaHdisCoverage[wsaaIdx.toInt()].toString());
			hdivpf.setRider(wsaaHdisArrayInner.wsaaHdisRider[wsaaIdx.toInt()].toString());
			hdivpf.setPlnsfx(wsaaHdisArrayInner.wsaaHdisPlnsfx[wsaaIdx.toInt()].toInt());
			hdivpf.setTranno(chdrpf.getTranno());
			/*  MOVE PAYR-BILLCD        TO HDIV-EFFDATE         <LFA1034>*/
			/*                            HDIV-DIVD-ALLOC-DATE <LFA1034>*/
			hdivpf.setEffdate(b5349DTO.getPtdate());
			hdivpf.setHdvaldt(b5349DTO.getPtdate());
			hdivpf.setHincapdt(wsaaHdisArrayInner.wsaaNextCapDate[wsaaIdx.toInt()].toInt());
			hdivpf.setCntcurr(b5349DTO.getChdrcntcurr());
			setPrecision(hdivpf.getHdvamt(), 0);
			hdivpf.setHdvamt(BigDecimal.valueOf(wsaaHdisArrayInner.wsaaDvdShare[wsaaIdx.toInt()].toDouble()).multiply(BigDecimal.valueOf(-1)));
			hdivpf.setHdvrate(BigDecimal.ZERO);			
			hdivpf.setHdveffdt(varcom.vrcmMaxDate.toInt());			
			hdivpf.setBatccoy(batcdorrec.company.toString());
			hdivpf.setBatcbrn(batcdorrec.branch.toString());
			hdivpf.setBatcactyr(batcdorrec.actyear.toInt());
			hdivpf.setBatcactmn(batcdorrec.actmonth.toInt());			
			hdivpf.setBatctrcde(batcdorrec.trcde.toString());
			hdivpf.setBatcbatch(batcdorrec.batch.toString());					
			hdivpf.setHdvtyp("C");
			hdivpf.setZdivopt(wsaaHdisArrayInner.wsaaZdivopt[wsaaIdx.toInt()].toString());
			hdivpf.setZcshdivmth(wsaaHdisArrayInner.wsaaZcshdivmth[wsaaIdx.toInt()].toString());
			hdivpf.setHdvopttx(0);
			hdivpf.setHdvcaptx(0);
			hdivpf.setHdvsmtno(0);
			hdivpf.setHpuanbr(0);
			isBulkProcessHdivPF = true;			
			hdivBulkInsList.add(hdivpf);
		}

	}

protected void commitHdivBulkInsert(){	 
	isBulkProcessHdivPF = false;
	boolean isInsertHdivPF = hdivpfDAO.insertHdivpf(hdivBulkInsList);	
	if (!isInsertHdivPF) {
		LOGGER.error("Insert HdivPF record failed.");
		fatalError600();
	}else hdivBulkInsList.clear();
}

	/**
	* <pre>
	*                                                         <LA2106>
	* </pre>
	*/
protected void h200PostAcmvRecord()
	{
		/*H210-POST*/
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			return ;
		}
		lifacmvrec.function.set("PSTW");
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
		/*H290-EXIT*/
	}

protected void processHdis(Hdispf hdis, int ctr)
{
	wsaaHdisArrayInner.wsaaHdisLife[ctr].set(hdis.getLife());
	wsaaHdisArrayInner.wsaaHdisCoverage[ctr].set(hdis.getCoverage());
	wsaaHdisArrayInner.wsaaHdisRider[ctr].set(hdis.getRider());
	wsaaHdisArrayInner.wsaaHdisPlnsfx[ctr].set(hdis.getPlnsfx());
	wsaaHdisArrayInner.wsaaHdisJlife[ctr].set(hdis.getJlife());
	wsaaHdisArrayInner.wsaaNextCapDate[ctr].set(hdis.getHcapndt());
	
	Hcsdpf currHcsd = null;
	String chdrnum = payxTemppf.getChdrnum();
    if (hcsdListMap != null && hcsdListMap.containsKey(chdrnum)) {
        List<Hcsdpf> hcsdpfList = hcsdListMap.get(chdrnum);
        
        for (Iterator<Hcsdpf> iterator = hcsdpfList.iterator(); iterator.hasNext(); ){
        	Hcsdpf hcsd = iterator.next();
        
        	if (hcsd.getLife().equals(hdis.getLife())
        		&&  hcsd.getCoverage().equals(hdis.getCoverage())
        		&& 	hcsd.getRider().equals(hdis.getRider())
        		&& Integer.compare(hcsd.getPlnsfx(),hdis.getPlnsfx()) == 0 ) {
        		currHcsd = hcsd;
        			break;
        			
        	}
        }
    }	
    
	wsaaHdisArrayInner.wsaaZdivopt[ctr].set(currHcsd.getZdivopt());
	wsaaHdisArrayInner.wsaaZcshdivmth[ctr].set(currHcsd.getZcshdivmth());
	wsaaHdisArrayInner.wsaaDvdTot[ctr].add(hdis.getHdvbalst().doubleValue());
	wsaaAllDvdTot.add(hdis.getHdvbalst().doubleValue());    
	
	Hdivpf currHdivcsh = null;
    if (hdivcshListMap != null && hdivcshListMap.containsKey(chdrnum)) {
        List<Hdivpf> hdivpfList = hdivcshListMap.get(chdrnum);
        for (Iterator<Hdivpf> iterator = hdivpfList.iterator(); iterator.hasNext(); ){
        	Hdivpf hdiv = iterator.next();

        	if (hdiv.getLife().equals(hdis.getLife())
        		&&  hdiv.getCoverage().equals(hdis.getCoverage())
        		&& 	hdiv.getRider().equals(hdis.getRider())
        		&& Integer.compare(hdiv.getPlnsfx(),hdis.getPlnsfx()) == 0 
        		&& (Integer.compare(hdiv.getHincapdt(),hdis.getHcapldt() + 1) == 0 
        		|| Integer.compare(hdiv.getHincapdt(),hdis.getHcapldt() + 1) < 0)) {
        			wsaaHdisArrayInner.wsaaDvdTot[ctr].add(hdiv.getHdvamt().doubleValue());
        			
        	}
        }
    }	
}

protected void a1000GetCurrConvDate(){
	/* If Contract and Billing currency is not the same :              */
	/* Subtract the lead days from the current BILLCD to get a date    */
	/* for currency conversion. This date will be used when calling    */
	/* XCVRT subroutine. For normal case this date = BSSC-EFF-DATE.    */
	wsaaCashdate.set(varcom.vrcmMaxDate);
	if (isEQ(b5349DTO.getPayrcntcurr(), b5349DTO.getPayrbillcurr())) {
		return ;
	}
	compute(datcon2rec.freqFactor, 0).set((sub(ZERO, intT6654Leaddays)));
	datcon2rec.frequency.set("DY");
	datcon2rec.intDate1.set(payrpf.getBillcd());
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon2rec.datcon2Rec);
		syserrrec.statuz.set(datcon2rec.statuz);
		fatalError600();
	}
	wsaaCashdate.set(datcon2rec.intDate2);
	if (isLT(wsaaCashdate, b5349DTO.getOccdate())) {
		wsaaCashdate.set(b5349DTO.getOccdate());
	}
}

protected void calcPayrPrem(){
	/* Check if the PAYR record has the correct premium for the*/
	/* instalment date, if not obtain it from history records*/
	wsaaOldBillcd.set(wsysBillcd);
	wsaaOldNextdate.set(b5349DTO.getNextdate());
	wsaaGotPayrAtBtdate = "N";
	String breakFlag = "N";
	if (isLT(b5349DTO.getBtdate(), b5349DTO.getEffdate())) {
		String chdrnum = payxTemppf.getChdrnum();
	    if (payrListMap != null && payrListMap.containsKey(chdrnum)) {
	        List<Payrpf> payrpfList = payrListMap.get(chdrnum);
	        for (Iterator<Payrpf> iterator = payrpfList.iterator(); iterator.hasNext(); ){
	        	Payrpf payr = iterator.next();

	        	if (isGTE(b5349DTO.getBtdate(), payr.getEffdate())) {
	        		wsaaGotPayrAtBtdate = "Y";
	        		currPayrLif = payr;
	        		break;
	        	}	        
	        }
	    }
	}
	
	if(reinstflag && isFoundPro){
		 calcprorated();
	}
	
	if(prmhldtrad && prmhpf != null && bsscIO.getEffectiveDate().toInt()<=prmhpf.getTodate() && !billWritten) {//ILIFE-8509
		String chdrnum = payxTemppf.getChdrnum();
	    if (payrListMap != null && payrListMap.containsKey(chdrnum)) {
	        List<Payrpf> payrpfList = payrListMap.get(chdrnum);
	        for (Payrpf payr: payrpfList){
	        	wsaaGotPayrAtBtdate = "Y";
	        	currPayrLif = payr;
	        	break;
	        }
	    }
	}
	
	if (isEQ(wsaaGotPayrAtBtdate, "Y")) {
		if (isEQ(wsaaFirstBill, "Y")) {		
			if(reinstflag && isFoundPro){
				compute(wsaaInstPrem, 2).set(add(currPayrLif.getSinstamt06(), proratePrem));
				compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
				compute(wsaaInstPrem, 2).set(add(currPayrLif.getOutstamt(), wsaaInstPrem));
				payrpf.setOutstamt(wsaaInstPrem.getbigdata());
			}
			else{
			payrpf.setOutstamt(currPayrLif.getOutstamt().add(currPayrLif.getSinstamt06()));
			}
			wsaaFirstBill = "N";
		}
		else {
			setPrecision(b5349DTO.getOutstamt(), 2);
			if(reinstflag && isFoundPro){
				compute(wsaaInstPrem, 2).set(add(currPayrLif.getSinstamt06(), proratePrem));
				compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
				compute(wsaaInstPrem, 2).set(add(payrpf.getOutstamt(), wsaaInstPrem)); //PINNACLE-2663
				payrpf.setOutstamt(wsaaInstPrem.getbigdata());
			}
			else{
			payrpf.setOutstamt(payrpf.getOutstamt().add(currPayrLif.getSinstamt06())); //PINNACLE-2663
			}
		}
		chdrpf.setOutstamt(payrpf.getOutstamt());
	}
	else {
		/*     COMPUTE CONT-TOTVAL     = PAYR-SINSTAMT06 +              */
		/*                               WSAA-INCREASE-DUE              */
		if (isEQ(wsaaFirstBill, "Y")) {	
			setPrecision(b5349DTO.getOutstamt(), 2);
			if(reinstflag && isFoundPro){
				compute(wsaaInstPrem, 2).set(add(b5349DTO.getSinstamt06(), proratePrem));
				compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
				compute(wsaaInstPrem, 2).set(add(b5349DTO.getOutstamt(), wsaaInstPrem)); //PINNACLE-2663
				payrpf.setOutstamt(wsaaInstPrem.getbigdata());
			}
			else{
				payrpf.setOutstamt(b5349DTO.getOutstamt().add(b5349DTO.getSinstamt06())); //PINNACLE-2663
			}
			wsaaFirstBill = "N";
		} else {
			if(reinstflag && isFoundPro){
				compute(wsaaInstPrem, 2).set(add(b5349DTO.getSinstamt06(), proratePrem));
				compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
				compute(wsaaInstPrem, 2).set(add(b5349DTO.getOutstamt(), wsaaInstPrem)); //PINNACLE-2663
				payrpf.setOutstamt(wsaaInstPrem.getbigdata());
			}
			else{
				payrpf.setOutstamt(payrpf.getOutstamt().add(b5349DTO.getSinstamt06())); //PINNACLE-2663
			}
		}
		/*                               PAYR-SINSTAMT06 +              */
		/*                               WSAA-INCREASE-DUE              */
		chdrpf.setOutstamt(payrpf.getOutstamt());
	}	
}
protected void callPmexSubroutine(Covrpf covrpf) {
	premiumrec.premiumRec.set(SPACES);
	boolean lnkgFlag = FeaConfg.isFeatureExist(payxTemppf.getChdrcoy().toString(), "NBPRP055", appVars, "IT");
	initialize(premiumrec.premiumRec);
	premiumrec.updateRequired.set("N");
	
	premiumrec.proratPremCalcFlag.set("Y");
	premiumrec.batcBatctrcde.set(bprdIO.getAuthCode());
	
	premiumrec.riskPrem.set(ZERO);
	premiumrec.language.set(bsscIO.getLanguage());
	premiumrec.billfreq.set(payrpf.getBillfreq());
	premiumrec.bilfrmdt.set(wsaaactualPTD);
	premiumrec.biltodt.set(payrpf.getBtdate());
	
	readTables(covrpf);
	if (AppVars.getInstance().getAppConfig().isVpmsEnable()) {
		premiumrec.premMethod.set(t5687rec.premmeth);
	}
	if (premiumrec.premMethod.toString().trim().equals("PMEX")) {
		premiumrec.setPmexCall.set("Y");
		premiumrec.cownnum.set(b5349DTO.getCownnum());
		premiumrec.occdate.set(b5349DTO.getOccdate());
	}
	premiumrec.chdrChdrcoy.set(payxTemppf.getChdrcoy());
	premiumrec.chdrChdrnum.set(payxTemppf.getChdrnum());
	premiumrec.cnttype.set(b5349DTO.getCnttype());
	premiumrec.effectdt.set(covrpf.getCrrcd());
	if (isNE(t5687rec.rtrnwfreq, ZERO)) {
		if (rertDatedMap != null && rertDatedMap.containsKey(payxTemppf.getChdrnum())) {
			List<Rertpf> rertpfList = rertDatedMap.get(payxTemppf.getChdrnum());
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.freqFactor.set(sub(ZERO, t5687rec.rtrnwfreq));
			datcon2rec.frequency.set("01");
			datcon2rec.intDate1.set(rertpfList.get(0).getEffdate());
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				fatalError600();
			}
			
			premiumrec.effectdt.set(datcon2rec.intDate2);
			premiumrec.ratingdate.set(datcon2rec.intDate2);
			premiumrec.reRateDate.set(datcon2rec.intDate2);
		}
	} else {
		premiumrec.ratingdate.set(covrpf.getCrrcd());
		premiumrec.reRateDate.set(covrpf.getCrrcd());
	}
	
	premiumrec.mop.set(payrpf.getBillchnl());
	premiumrec.commissionPrem.set(ZERO);
	premiumrec.zstpduty01.set(ZERO);
	premiumrec.zstpduty02.set(ZERO);
	premiumrec.crtable.set(covrpf.getCrtable());
	premiumrec.lifeLife.set(covrpf.getLife());
	if (("PMEX".equals(t5675rec.premsubr.toString().trim())) && (lnkgFlag == true)) {
		if (null == covrpf.getLnkgsubrefno() || covrpf.getLnkgsubrefno().trim().isEmpty()) {
			premiumrec.lnkgSubRefNo.set(SPACE);
		} else {
			premiumrec.lnkgSubRefNo.set(covrpf.getLnkgsubrefno().trim());
		}
		if (null == covrpf.getLnkgno() || covrpf.getLnkgno().trim().isEmpty()) {
			premiumrec.linkcov.set(SPACE);
		} else {
			LinkageInfoService linkgService = new LinkageInfoService();
			FixedLengthStringData linkgCov = new FixedLengthStringData(
					linkgService.getLinkageInfo(covrpf.getLnkgno().trim()));
			premiumrec.linkcov.set(linkgCov);
		}
	}
	premiumrec.lifeJlife.set(covrpf.getJlife());
	premiumrec.covrCoverage.set(covrpf.getCoverage());
	premiumrec.covrRider.set(covrpf.getRider());
	premiumrec.termdate.set(covrpf.getPremCessDate());
	Lifepf lifepf = lifepfDAO.getLifeEnqRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "00");
	premiumrec.lsex.set(lifepf.getCltsex());
	Lifepf jlife = lifepfDAO.getLifelnbRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "01");
	if (isNE(t5687rec.rtrnwfreq, ZERO)) {
		calculateAnb3260(lifepf);
		premiumrec.lage.set(wsaaAnb);
		if (jlife != null) {
			calculateAnb3260(jlife);
			premiumrec.jlage.set(wsaaAnb);
			premiumrec.jlsex.set(jlife.getCltsex());
		} else {
			premiumrec.jlsex.set(SPACES);
			premiumrec.jlage.set(ZERO);
		}
	} else {
		premiumrec.lage.set(covrpf.getAnbAtCcd());
		if (jlife != null) {
			premiumrec.jlsex.set(jlife.getCltsex());
			premiumrec.jlage.set(jlife.getAnbAtCcd());
		} else {
			premiumrec.jlsex.set(SPACES);
			premiumrec.jlage.set(ZERO);
		}
	}
	Clntpf clntpf = clntpfDAO.searchClntRecord("CN", bsprIO.getFsuco().toString(), lifepf.getLifcnum());
	stampDutyflag = FeaConfg.isFeatureExist(payxTemppf.getChdrcoy(), "NBPROP01", appVars, "IT");
	if (stampDutyflag) {
		if (clntpf.getClntStateCd() != null) {
			premiumrec.rstate01.set(clntpf.getClntStateCd().substring(3).trim());
		}
	}
	datcon3rec.intDate1.set(premiumrec.effectdt);
	datcon3rec.intDate2.set(premiumrec.termdate);
	datcon3rec.frequency.set("01");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, Varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		fatalError600();
	}
	datcon3rec.freqFactor.add(0.99999);
	premiumrec.duration.set(datcon3rec.freqFactor);
	datcon3rec.intDate1.set(premiumrec.effectdt);
	datcon3rec.intDate2.set(covrpf.getRiskCessDate());
	datcon3rec.frequency.set("01");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, Varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		fatalError600();
	}
	datcon3rec.freqFactor.add(0.99999);
	premiumrec.riskCessTerm.set(datcon3rec.freqFactor);
	premiumrec.currcode.set(covrpf.getPremCurrency());
	 Chdrpf chdrpf = chdrpfDAO.getChdrpfByConAndNumAndServunit(payxTemppf.getChdrcoy().toString(),
			payxTemppf.getChdrnum().toString());
	if (isEQ(covrpf.getPlanSuffix(), ZERO) && isNE(chdrpf.getPolinc(), 1)) {
		compute(premiumrec.sumin, 3).setRounded(div(covrpf.getSumins(), chdrpf.getPolsum()));
	} else {
		premiumrec.sumin.set(covrpf.getSumins());
	}
	compute(premiumrec.sumin, 3).setRounded(mult(premiumrec.sumin, chdrpf.getPolinc()));
	premiumrec.mortcls.set(covrpf.getMortcls());
	premiumrec.calcPrem.set(covrpf.getInstprem());
	premiumrec.calcBasPrem.set(covrpf.getZbinstprem());
	premiumrec.calcLoaPrem.set(covrpf.getZlinstprem());
	getAnny3280(covrpf);
	premiumrec.commTaxInd.set("Y");
	premiumrec.prevSumIns.set(ZERO);
	premiumrec.inputPrevPrem.set(ZERO);
	com.csc.smart400framework.dataaccess.dao.ClntpfDAO clntDao = DAOFactory.getClntpfDAO();
	com.csc.smart400framework.dataaccess.model.Clntpf clnt = clntDao.getClientByClntnum(lifepf.getLifcnum());
	if (null != clnt && null != clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()) {
		premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
	} else {
		premiumrec.stateAtIncep.set(clntpf.getClntStateCd());
	}
	premiumrec.validind.set("2");
	getRcvdpf(covrpf);
	callProgram(t5675rec.premsubr, premiumrec.premiumRec);
	if (isNE(premiumrec.statuz, Varcom.oK)) {
		syserrrec.statuz.set(premiumrec.statuz);
		fatalError600();
	}
	//wsaaOutstPrem.set(premiumrec.calcPrem);
	//wsaaOutstStpdty.set(premiumrec.zstpduty01);
	if (stampDutyflag) {
		compute(premiumrec.calcPrem, 3).set(add(premiumrec.calcPrem, premiumrec.zstpduty01));
	}
	catchupPrem.set(premiumrec.calcPrem);
	
}

protected void getRcvdpf(Covrpf covrpf) {
	boolean dialdownFlag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPRP012", appVars, "IT");
	boolean incomeProtectionflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP02", appVars, "IT");
	boolean premiumflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP03", appVars, "IT");
	if (incomeProtectionflag || premiumflag || dialdownFlag) {
		Rcvdpf rcvdPFObject = new Rcvdpf();
		rcvdPFObject.setChdrcoy(covrpf.getChdrcoy());
		rcvdPFObject.setChdrnum(covrpf.getChdrnum());
		rcvdPFObject.setLife(covrpf.getLife());
		rcvdPFObject.setCoverage(covrpf.getCoverage());
		rcvdPFObject.setRider(covrpf.getRider());
		rcvdPFObject.setCrtable(covrpf.getCrtable());
		rcvdPFObject = rcvdDAO.readRcvdpf(rcvdPFObject);
		premiumrec.bentrm.set(rcvdPFObject.getBentrm());
		if (rcvdPFObject.getPrmbasis() != null && isEQ("S", rcvdPFObject.getPrmbasis())) {
			premiumrec.prmbasis.set("Y");
		} else {
			premiumrec.prmbasis.set("");
		}
		premiumrec.poltyp.set(rcvdPFObject.getPoltyp());
		premiumrec.occpcode.set("");
		premiumrec.dialdownoption.set(rcvdPFObject.getDialdownoption());
		premiumrec.waitperiod.set(rcvdPFObject.getWaitperiod());
	}
}

protected void readTables(Covrpf c) {
	Itempf itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(payxTemppf.getChdrcoy());
	itempf.setItemtabl("T5687");
	itempf.setItemitem(c.getCrtable());
	itempf.setItmfrm(new BigDecimal(b5349DTO.getOccdate()));
	itempf.setItmto(new BigDecimal(b5349DTO.getOccdate()));
	List<Itempf> t5687ItemList = itempfDAO.findByItemDates(itempf);
	if (t5687ItemList.isEmpty()) {
		t5687rec.t5687Rec.set(SPACES);
		syserrrec.params.set("IT".concat(payxTemppf.getChdrcoy().toString()).concat("T5675")
				.concat(t5687rec.premmeth.toString()));
		fatalError600();
	} else {
		t5687rec.t5687Rec.set(StringUtil.rawToString(t5687ItemList.get(0).getGenarea()));
	}
	List<Itempf> t5675ItempfList = itempfDAO.getAllItemitem("IT", payxTemppf.getChdrcoy().toString(), "T5675",
			t5687rec.premmeth.toString());
	if (t5675ItempfList.isEmpty()) {
		t5675rec.t5675Rec.set(SPACES);
		syserrrec.params.set("IT".concat(payxTemppf.getChdrcoy().toString()).concat("T5675")
				.concat(t5687rec.premmeth.toString()));
		fatalError600();
	} else {
		t5675rec.t5675Rec.set(StringUtil.rawToString(t5675ItempfList.get(0).getGenarea()));
	}
}

protected void getAnny3280(Covrpf c) {
	Annypf annypf = annypfDAO.getAnnyRecordByAnnyKey(c.getChdrcoy(), c.getChdrnum(), c.getLife(), c.getCoverage(),
			c.getRider(), c.getPlanSuffix());
	if (annypf != null) {
		premiumrec.freqann.set(annypf.getFreqann());
		premiumrec.advance.set(annypf.getAdvance());
		premiumrec.arrears.set(annypf.getArrears());
		premiumrec.guarperd.set(annypf.getGuarperd());
		premiumrec.intanny.set(annypf.getIntanny());
		premiumrec.capcont.set(annypf.getCapcont());
		premiumrec.withprop.set(annypf.getWithprop());
		premiumrec.withoprop.set(annypf.getWithoprop());
		premiumrec.ppind.set(annypf.getPpind());
		premiumrec.nomlife.set(annypf.getNomlife());
		premiumrec.dthpercn.set(annypf.getDthpercn());
		premiumrec.dthperco.set(annypf.getDthperco());
	} else {
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
	}
}

protected void calculateAnb3260(Lifepf lifepf) {
	wsaaAnb.set(ZERO);
	AgecalcPojo agecalcPojo = new AgecalcPojo();
	AgecalcUtils agecalcUtils = getApplicationContext().getBean("agecalcUtils", AgecalcUtils.class);

	agecalcPojo.setFunction("CALCP");
	agecalcPojo.setLanguage(bsscIO.getLanguage().toString());
	agecalcPojo.setCnttype(b5349DTO.getCnttype());
	agecalcPojo.setIntDate1(Integer.toString(lifepf.getCltdob()));
	agecalcPojo.setIntDate2(premiumrec.effectdt.toString());
	agecalcPojo.setCompany(bsprIO.getFsuco().toString());
	agecalcUtils.calcAge(agecalcPojo);
	if (isNE(agecalcPojo.getStatuz(), Varcom.oK)) {
		syserrrec.params.set(agecalcPojo.toString());
		syserrrec.statuz.set(agecalcPojo.getStatuz());
		fatalError600();
	}
	wsaaAnb.set(agecalcPojo.getAgerating());
}


protected void updateinit(){
		hpadpf = new Hpadpf();
		if(isEQ(b5349DTO.getChdrbillchnl(),"D") && isEQ(t3615rec.onepcashless,"Y") && isNE(b5349DTO.getRcopt(),"Y")){
			hpadpf.setChdrcoy(payxTemppf.getChdrcoy());
			hpadpf.setChdrnum(payxTemppf.getChdrnum());
			hpadpf.setFirstprmrcpdate(th5agrec.bnktransfer[sub1.toInt()].toInt());
			isBulkProcessHpadPF = true;
			hpadBulkUpdtList.add(hpadpf);
		}
	}
	
	protected Covrpf getCovrpf(Covtpf covt, Covrpf covrpf){
		
		covrpf.setCrtable(covt.getCrtable());
		covrpf.setLife(covt.getLife());
		covrpf.setCoverage(covt.getCoverage());
		covrpf.setRider(covt.getRider());
		covrpf.setPlanSuffix(covt.getPlnsfx());
		covrpf.setChdrnum(covt.getChdrnum());
		covrpf.setChdrcoy(covt.getChdrcoy());
		covrpf.setZbinstprem(covt.getZbinstprem());
		covrpf.setInstprem(covt.getInstprem());
		return covrpf;
	}
	
	protected void calcMgfeel(){
		/* Look up the tax relief method on T5688.                         */
		readT5688(b5349DTO.getCnttype());
		/* Look up the subroutine on T6687.                                */
		if (isNE(t5688rec.feemeth, SPACES)) {
			readT5674(t5688rec.feemeth.toString());
			if(isNE(t5674rec.commsubr,SPACES)){
				
				mgfeelrec.effdate.set(ZERO);
				mgfeelrec.mgfee.set(ZERO);
				mgfeelrec.cnttype.set(b5349DTO.getCnttype());
				/* MOVE CHDRLNB-BILLFREQ       TO MGFL-BILLFREQ.                */
				mgfeelrec.billfreq.set(b5349DTO.getBillfreq());
				mgfeelrec.effdate.set(b5349DTO.getEffdate());
				mgfeelrec.cntcurr.set(b5349DTO.getChdrcntcurr());
				//mgfeelrec.riset(b5349DTO.getChdrcntcurr());
				mgfeelrec.policyRCD.set(b5349DTO.getOccdate());
				mgfeelrec.company.set(payxTemppf.getChdrcoy());
				callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
				if (isNE(mgfeelrec.statuz, Varcom.oK)
					&& isNE(mgfeelrec.statuz, Varcom.endp)) {
				syserrrec.params.set(mgfeelrec.mgfeelRec);
				fatalError600();
				}
				zrdecplrec.amountIn.set(mgfeelrec.mgfee);
				zrdecplrec.currency.set(b5349DTO.getChdrcntcurr());
				callRounding8000();
				mgfeelrec.mgfee.set(zrdecplrec.amountOut);
			}
		}
		else{
			mgfeelrec.mgfee.set(ZERO);
		}
	}	
	
protected void readT5688(String cnttype) {

	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5688ListMap.containsKey(cnttype)) {
		itempfList = t5688ListMap.get(cnttype);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if (Integer.parseInt(itempf.getItmfrm().toString()) > 0) {
				if (Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString())
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())) {
					t5688rec.t5688Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;
				}
			} else {
				t5688rec.t5688Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;
			}
		}
	}
	if (!itemFound) {
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("t5688");
		stringVariable1.addExpression(cnttype);
		stringVariable1.setStringInto(wsysSysparams);
		syserrrec.params.set(wsysSystemErrorParams);
		fatalError600();
	}
}

protected void readT5674(String feeMethod) {
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5674ListMap.containsKey(feeMethod)) {
		itempfList = t5674ListMap.get(feeMethod);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			t5674rec.t5674Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			itemFound = true;
		}
	}
	if (!itemFound) {
		syserrrec.params.set("T5674".concat(feeMethod.toString()));
		syserrrec.statuz.set(h134);
		fatalError600();
	}
}

private static final class ControlTotalsInner {
		/* CONTROL-TOTALS */
	private ZonedDecimalData ct01 = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private ZonedDecimalData ct02 = new ZonedDecimalData(2, 0).init(2).setUnsigned();
	private ZonedDecimalData ct03 = new ZonedDecimalData(2, 0).init(3).setUnsigned();
	private ZonedDecimalData ct04 = new ZonedDecimalData(2, 0).init(4).setUnsigned();
	private ZonedDecimalData ct05 = new ZonedDecimalData(2, 0).init(5).setUnsigned();
	private ZonedDecimalData ct06 = new ZonedDecimalData(2, 0).init(6).setUnsigned();
	private ZonedDecimalData ct07 = new ZonedDecimalData(2, 0).init(7).setUnsigned();
	private ZonedDecimalData ct08 = new ZonedDecimalData(2, 0).init(8).setUnsigned();
	private ZonedDecimalData ct09 = new ZonedDecimalData(2, 0).init(9).setUnsigned();
	private ZonedDecimalData ct10 = new ZonedDecimalData(2, 0).init(10).setUnsigned();
	private ZonedDecimalData ct11 = new ZonedDecimalData(2, 0).init(11).setUnsigned();
	private ZonedDecimalData ct12 = new ZonedDecimalData(2, 0).init(12).setUnsigned();
}
/*
 * Class transformed  from Data Structure WSAA-HDIS-ARRAY--INNER
 */
private static final class WsaaHdisArrayInner {

	private FixedLengthStringData wsaaHdisArray = new FixedLengthStringData(6138);
	private FixedLengthStringData[] wsaaHdisRec = FLSArrayPartOfStructure(99, 62, wsaaHdisArray, 0);
	private FixedLengthStringData[] wsaaHdisLife = FLSDArrayPartOfArrayStructure(2, wsaaHdisRec, 0);
	private FixedLengthStringData[] wsaaHdisCoverage = FLSDArrayPartOfArrayStructure(2, wsaaHdisRec, 2);
	private FixedLengthStringData[] wsaaHdisRider = FLSDArrayPartOfArrayStructure(2, wsaaHdisRec, 4);
	private FixedLengthStringData[] wsaaHdisJlife = FLSDArrayPartOfArrayStructure(2, wsaaHdisRec, 6);
	private ZonedDecimalData[] wsaaHdisPlnsfx = ZDArrayPartOfArrayStructure(4, 0, wsaaHdisRec, 8, UNSIGNED_TRUE);
	private FixedLengthStringData[] wsaaZdivopt = FLSDArrayPartOfArrayStructure(4, wsaaHdisRec, 12);
	private FixedLengthStringData[] wsaaZcshdivmth = FLSDArrayPartOfArrayStructure(4, wsaaHdisRec, 16);
	private ZonedDecimalData[] wsaaNextCapDate = ZDArrayPartOfArrayStructure(8, 0, wsaaHdisRec, 20);
	private ZonedDecimalData[] wsaaDvdTot = ZDArrayPartOfArrayStructure(17, 7, wsaaHdisRec, 28);
	private ZonedDecimalData[] wsaaDvdShare = ZDArrayPartOfArrayStructure(17, 7, wsaaHdisRec, 45);
}
}