package com.csc.life.regularprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6636screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6636ScreenVars sv = (S6636ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6636screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6636ScreenVars screenVars = (S6636ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.riskunit02.setClassString("");
		screenVars.yrsinf01.setClassString("");
		screenVars.yrsinf02.setClassString("");
		screenVars.yrsinf03.setClassString("");
		screenVars.yrsinf04.setClassString("");
		screenVars.yrsinf05.setClassString("");
		screenVars.yrsinf06.setClassString("");
		screenVars.yrsinf07.setClassString("");
		screenVars.yrsinf08.setClassString("");
		screenVars.yrsinf09.setClassString("");
		screenVars.yrsinf10.setClassString("");
		screenVars.riskunit01.setClassString("");
		screenVars.sumass01.setClassString("");
		screenVars.sumass02.setClassString("");
		screenVars.sumass03.setClassString("");
		screenVars.sumass04.setClassString("");
		screenVars.sumass05.setClassString("");
		screenVars.sumass06.setClassString("");
		screenVars.sumass07.setClassString("");
		screenVars.sumass08.setClassString("");
		screenVars.sumass09.setClassString("");
		screenVars.sumass10.setClassString("");
		screenVars.bonus01.setClassString("");
		screenVars.bonus02.setClassString("");
		screenVars.bonus03.setClassString("");
		screenVars.bonus04.setClassString("");
		screenVars.bonus05.setClassString("");
		screenVars.bonus06.setClassString("");
		screenVars.bonus07.setClassString("");
		screenVars.bonus08.setClassString("");
		screenVars.bonus09.setClassString("");
		screenVars.bonus10.setClassString("");
		screenVars.ztrmbpc.setClassString("");
		screenVars.ztrmdsc.setClassString("");
	}

/**
 * Clear all the variables in S6636screen
 */
	public static void clear(VarModel pv) {
		S6636ScreenVars screenVars = (S6636ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.riskunit02.clear();
		screenVars.yrsinf01.clear();
		screenVars.yrsinf02.clear();
		screenVars.yrsinf03.clear();
		screenVars.yrsinf04.clear();
		screenVars.yrsinf05.clear();
		screenVars.yrsinf06.clear();
		screenVars.yrsinf07.clear();
		screenVars.yrsinf08.clear();
		screenVars.yrsinf09.clear();
		screenVars.yrsinf10.clear();
		screenVars.riskunit01.clear();
		screenVars.sumass01.clear();
		screenVars.sumass02.clear();
		screenVars.sumass03.clear();
		screenVars.sumass04.clear();
		screenVars.sumass05.clear();
		screenVars.sumass06.clear();
		screenVars.sumass07.clear();
		screenVars.sumass08.clear();
		screenVars.sumass09.clear();
		screenVars.sumass10.clear();
		screenVars.bonus01.clear();
		screenVars.bonus02.clear();
		screenVars.bonus03.clear();
		screenVars.bonus04.clear();
		screenVars.bonus05.clear();
		screenVars.bonus06.clear();
		screenVars.bonus07.clear();
		screenVars.bonus08.clear();
		screenVars.bonus09.clear();
		screenVars.bonus10.clear();
		screenVars.ztrmbpc.clear();
		screenVars.ztrmdsc.clear();
	}
}
