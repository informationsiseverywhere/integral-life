package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:58
 * Description:
 * Copybook name: OVRDUEREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ovrduerec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData ovrdueRec = new FixedLengthStringData(getOvrdueRecSize());
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(ovrdueRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(ovrdueRec, 5);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(ovrdueRec, 9);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(ovrdueRec, 10);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(ovrdueRec, 11);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(ovrdueRec, 19);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(ovrdueRec, 21);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(ovrdueRec, 23);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(ovrdueRec, 25);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(ovrdueRec, 28);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(ovrdueRec, 31);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(ovrdueRec, 34).setUnsigned();
  	public ZonedDecimalData outstamt = new ZonedDecimalData(17, 2).isAPartOf(ovrdueRec, 42);
  	public ZonedDecimalData ptdate = new ZonedDecimalData(8, 0).isAPartOf(ovrdueRec, 59).setUnsigned();
  	public ZonedDecimalData btdate = new ZonedDecimalData(8, 0).isAPartOf(ovrdueRec, 67).setUnsigned();
  	public ZonedDecimalData ovrdueDays = new ZonedDecimalData(3, 0).isAPartOf(ovrdueRec, 75).setUnsigned();
  	public FixedLengthStringData agntnum = new FixedLengthStringData(8).isAPartOf(ovrdueRec, 78);
  	public FixedLengthStringData cownnum = new FixedLengthStringData(8).isAPartOf(ovrdueRec, 86);
  	public FixedLengthStringData statcode = new FixedLengthStringData(2).isAPartOf(ovrdueRec, 94);
  	public FixedLengthStringData pstatcode = new FixedLengthStringData(2).isAPartOf(ovrdueRec, 96);
  	public FixedLengthStringData trancode = new FixedLengthStringData(4).isAPartOf(ovrdueRec, 98);
  	public ZonedDecimalData acctyear = new ZonedDecimalData(4, 0).isAPartOf(ovrdueRec, 102).setUnsigned();
  	public ZonedDecimalData acctmonth = new ZonedDecimalData(2, 0).isAPartOf(ovrdueRec, 106).setUnsigned();
  	public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(ovrdueRec, 108);
  	public FixedLengthStringData batcbatch = new FixedLengthStringData(5).isAPartOf(ovrdueRec, 110);
  	public ZonedDecimalData user = new ZonedDecimalData(6, 0).isAPartOf(ovrdueRec, 115).setUnsigned();
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(ovrdueRec, 121);
  	public ZonedDecimalData tranDate = new ZonedDecimalData(6, 0).isAPartOf(ovrdueRec, 122).setUnsigned();
  	public ZonedDecimalData tranTime = new ZonedDecimalData(6, 0).isAPartOf(ovrdueRec, 128).setUnsigned();
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(ovrdueRec, 134);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(ovrdueRec, 138);
  	public FixedLengthStringData pumeth = new FixedLengthStringData(4).isAPartOf(ovrdueRec, 142);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(ovrdueRec, 146);
  	public PackedDecimalData instprem = new PackedDecimalData(17, 2).isAPartOf(ovrdueRec, 148);
  	public PackedDecimalData sumins = new PackedDecimalData(17, 2).isAPartOf(ovrdueRec, 157);
  	public PackedDecimalData crrcd = new PackedDecimalData(8, 0).isAPartOf(ovrdueRec, 166);
  	public PackedDecimalData premCessDate = new PackedDecimalData(8, 0).isAPartOf(ovrdueRec, 171);
  	public PackedDecimalData pupfee = new PackedDecimalData(17, 2).isAPartOf(ovrdueRec, 176);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(ovrdueRec, 185);
  	public PackedDecimalData occdate = new PackedDecimalData(8, 0).isAPartOf(ovrdueRec, 188);
  	public FixedLengthStringData aloind = new FixedLengthStringData(1).isAPartOf(ovrdueRec, 193);
  	public FixedLengthStringData efdcode = new FixedLengthStringData(2).isAPartOf(ovrdueRec, 194);
  	public ZonedDecimalData procSeqNo = new ZonedDecimalData(3, 0).isAPartOf(ovrdueRec, 196);
  	public PackedDecimalData newSumins = new PackedDecimalData(17, 2).isAPartOf(ovrdueRec, 199);
  	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(ovrdueRec, 208, FILLER);
  	public PackedDecimalData polsum = new PackedDecimalData(4, 0).isAPartOf(ovrdueRec, 232);
  	public PackedDecimalData actualVal = new PackedDecimalData(17, 2).isAPartOf(ovrdueRec, 235);
  	public FixedLengthStringData description = new FixedLengthStringData(30).isAPartOf(ovrdueRec, 244);
  	public PackedDecimalData riskCessDate = new PackedDecimalData(8, 0).isAPartOf(ovrdueRec, 274);
  	public PackedDecimalData newRiskCessDate = new PackedDecimalData(8, 0).isAPartOf(ovrdueRec, 279);
  	public PackedDecimalData newPremCessDate = new PackedDecimalData(8, 0).isAPartOf(ovrdueRec, 284);
  	public PackedDecimalData newRerateDate = new PackedDecimalData(8, 0).isAPartOf(ovrdueRec, 289);
  	public PackedDecimalData cvRefund = new PackedDecimalData(17, 2).isAPartOf(ovrdueRec, 294);
  	public PackedDecimalData surrenderValue = new PackedDecimalData(17, 2).isAPartOf(ovrdueRec, 303);
  	public ZonedDecimalData etiYears = new ZonedDecimalData(3, 0).isAPartOf(ovrdueRec, 312).setUnsigned();
  	public ZonedDecimalData etiDays = new ZonedDecimalData(3, 0).isAPartOf(ovrdueRec, 315).setUnsigned();
  	public PackedDecimalData newCrrcd = new PackedDecimalData(8, 0).isAPartOf(ovrdueRec, 318);
  	public PackedDecimalData newSingp = new PackedDecimalData(17, 2).isAPartOf(ovrdueRec, 323);
  	public PackedDecimalData newAnb = new PackedDecimalData(3, 0).isAPartOf(ovrdueRec, 332);
  	public PackedDecimalData newInstprem = new PackedDecimalData(17, 2).isAPartOf(ovrdueRec, 334);
  	public PackedDecimalData newPua = new PackedDecimalData(17, 2).isAPartOf(ovrdueRec, 343);
  	public PackedDecimalData newBonusDate = new PackedDecimalData(8, 0).isAPartOf(ovrdueRec, 352);
  	public PackedDecimalData runDate = new PackedDecimalData(8, 0).isAPartOf(ovrdueRec, 357);


	public void initialize() {
		COBOLFunctions.initialize(ovrdueRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ovrdueRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}

	public int getOvrdueRecSize()
	{
		return 362;
	}
	
}