package com.csc.life.regularprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: LinxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:44
 * Class transformed from LINXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LinxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 93;
	public FixedLengthStringData linxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData linxpfRecord = linxrec;
	
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(linxrec);
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(linxrec);
	public PackedDecimalData instfrom = DD.instfrom.copy().isAPartOf(linxrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(linxrec);
	public FixedLengthStringData billcurr = DD.billcurr.copy().isAPartOf(linxrec);
	public PackedDecimalData payrseqno = DD.payrseqno.copy().isAPartOf(linxrec);
	public PackedDecimalData cbillamt = DD.cbillamt.copy().isAPartOf(linxrec);
	public PackedDecimalData billcd = DD.billcd.copy().isAPartOf(linxrec);
	public FixedLengthStringData billchnl = DD.billchnl.copy().isAPartOf(linxrec);
	public PackedDecimalData instamt01 = DD.instamt.copy().isAPartOf(linxrec);
	public PackedDecimalData instamt02 = DD.instamt.copy().isAPartOf(linxrec);
	public PackedDecimalData instamt03 = DD.instamt.copy().isAPartOf(linxrec);
	public PackedDecimalData instamt04 = DD.instamt.copy().isAPartOf(linxrec);
	public PackedDecimalData instamt05 = DD.instamt.copy().isAPartOf(linxrec);
	public PackedDecimalData instamt06 = DD.instamt.copy().isAPartOf(linxrec);
	public FixedLengthStringData instfreq = DD.instfreq.copy().isAPartOf(linxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public LinxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for LinxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public LinxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for LinxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public LinxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for LinxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public LinxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("LINXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRNUM, " +
							"CHDRCOY, " +
							"INSTFROM, " +
							"CNTCURR, " +
							"BILLCURR, " +
							"PAYRSEQNO, " +
							"CBILLAMT, " +
							"BILLCD, " +
							"BILLCHNL, " +
							"INSTAMT01, " +
							"INSTAMT02, " +
							"INSTAMT03, " +
							"INSTAMT04, " +
							"INSTAMT05, " +
							"INSTAMT06, " +
							"INSTFREQ, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrnum,
                                     chdrcoy,
                                     instfrom,
                                     cntcurr,
                                     billcurr,
                                     payrseqno,
                                     cbillamt,
                                     billcd,
                                     billchnl,
                                     instamt01,
                                     instamt02,
                                     instamt03,
                                     instamt04,
                                     instamt05,
                                     instamt06,
                                     instfreq,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrnum.clear();
  		chdrcoy.clear();
  		instfrom.clear();
  		cntcurr.clear();
  		billcurr.clear();
  		payrseqno.clear();
  		cbillamt.clear();
  		billcd.clear();
  		billchnl.clear();
  		instamt01.clear();
  		instamt02.clear();
  		instamt03.clear();
  		instamt04.clear();
  		instamt05.clear();
  		instamt06.clear();
  		instfreq.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getLinxrec() {
  		return linxrec;
	}

	public FixedLengthStringData getLinxpfRecord() {
  		return linxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setLinxrec(what);
	}

	public void setLinxrec(Object what) {
  		this.linxrec.set(what);
	}

	public void setLinxpfRecord(Object what) {
  		this.linxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(linxrec.getLength());
		result.set(linxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}