package com.csc.life.regularprocessing.dataaccess.model;

import java.math.BigDecimal;

public class Aglfpf{
	private String agntcoy;
	private String agntnum;
	private String clntnum;
	private String termid;
	private int transactionDate;
	private int transactionTime;
	private int user;
	private int currfrom;
	private int currto;
	private int dteapp;
	private int dtetrm;
	private String trmcde;
	private int dteexp;
	private String pftflg;
	private String rasflg;
	private String bcmtab;
	private String rcmtab;
	private String scmtab;
	private String agentClass;
	private String ocmtab;
	private String reportag;
	private BigDecimal ovcpc;
	private String taxmeth;
	private String irdno;
	private String taxcde;
	private BigDecimal taxalw;
	private String sprschm;
	private BigDecimal sprprc;
	private String payclt;
	private String paymth;
	private String payfrq;
	private String facthous;
	private String bankkey;
	private String bankacckey;
	private int dtepay;
	private String currcode;
	private BigDecimal intcrd;
	private BigDecimal fixprc;
	private String bmaflg;
	private String exclAgmt;
	private String houseLoan;
	private String computerLoan;
	private String carLoan;
	private String officeRent;
	private String otherLoans;
	private String aracde;
	private BigDecimal minsta;
	private String zrorcode;
	private int effdate;
	private String tagsusind;
	private String tlaglicno;
	private int tlicexpdt;
	private BigDecimal tcolprct;
	private BigDecimal tcolmax;
	private String tsalesunt;
	private String prdagent;
	private String agccqind;
	private String zrecruit;
	private String validflag;
	private String userProfile;
	private String jobName;
	private String datime;
	private String agntbr; //ILIFE-8709
	
    public Aglfpf() {
	}
	public String getAgntcoy() {
        return agntcoy;
    }
    public String getAgntnum() {
        return agntnum;
    }
    public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	public String getTermid() {
        return termid;
    }
    public int getTransactionDate() {
        return transactionDate;
    }
    public int getTransactionTime() {
        return transactionTime;
    }
    public int getUser() {
        return user;
    }
    public int getCurrfrom() {
        return currfrom;
    }
    public int getCurrto() {
        return currto;
    }
    public int getDteapp() {
        return dteapp;
    }
    public int getDtetrm() {
        return dtetrm;
    }
    public String getTrmcde() {
        return trmcde;
    }
    public int getDteexp() {
        return dteexp;
    }
    public String getPftflg() {
        return pftflg;
    }
    public String getRasflg() {
        return rasflg;
    }
    public String getBcmtab() {
        return bcmtab;
    }
    public String getRcmtab() {
        return rcmtab;
    }
    public String getScmtab() {
        return scmtab;
    }
    public String getAgentClass() {
        return agentClass;
    }
    public String getOcmtab() {
        return ocmtab;
    }
    public String getReportag() {
        return reportag;
    }
    public BigDecimal getOvcpc() {
        return ovcpc;
    }
    public String getTaxmeth() {
        return taxmeth;
    }
    public String getIrdno() {
        return irdno;
    }
    public String getTaxcde() {
        return taxcde;
    }
    public BigDecimal getTaxalw() {
        return taxalw;
    }
    public String getSprschm() {
        return sprschm;
    }
    public BigDecimal getSprprc() {
        return sprprc;
    }
    public String getPayclt() {
        return payclt;
    }
    public String getPaymth() {
        return paymth;
    }
    public String getPayfrq() {
        return payfrq;
    }
    public String getFacthous() {
        return facthous;
    }
    public String getBankkey() {
        return bankkey;
    }
    public String getBankacckey() {
        return bankacckey;
    }
    public int getDtepay() {
        return dtepay;
    }
    public String getCurrcode() {
        return currcode;
    }
    public BigDecimal getIntcrd() {
        return intcrd;
    }
    public BigDecimal getFixprc() {
        return fixprc;
    }
    public String getBmaflg() {
        return bmaflg;
    }
    public String getExclAgmt() {
        return exclAgmt;
    }
    public String getHouseLoan() {
        return houseLoan;
    }
    public String getComputerLoan() {
        return computerLoan;
    }
    public String getCarLoan() {
        return carLoan;
    }
    public String getOfficeRent() {
        return officeRent;
    }
    public String getOtherLoans() {
        return otherLoans;
    }
    public String getAracde() {
        return aracde;
    }
    public BigDecimal getMinsta() {
        return minsta;
    }
    public String getZrorcode() {
        return zrorcode;
    }
    public int getEffdate() {
        return effdate;
    }
    public String getTagsusind() {
        return tagsusind;
    }
    public String getTlaglicno() {
        return tlaglicno;
    }
    public int getTlicexpdt() {
        return tlicexpdt;
    }
    public BigDecimal getTcolprct() {
        return tcolprct;
    }
    public BigDecimal getTcolmax() {
        return tcolmax;
    }
    public String getTsalesunt() {
        return tsalesunt;
    }
    public String getPrdagent() {
        return prdagent;
    }
    public String getAgccqind() {
        return agccqind;
    }
    public String getZrecruit() {
        return zrecruit;
    }
    public String getValidflag() {
        return validflag;
    }
    public String getUserProfile() {
        return userProfile;
    }
    public String getJobName() {
        return jobName;
    }
    public String getDatime() {
        return datime;
    }
    public void setAgntcoy(String agntcoy) {
        this.agntcoy = agntcoy;
    }
    public void setAgntnum(String agntnum) {
        this.agntnum = agntnum;
    }
    public void setTermid(String termid) {
        this.termid = termid;
    }
    public void setTransactionDate(int transactionDate) {
        this.transactionDate = transactionDate;
    }
    public void setTransactionTime(int transactionTime) {
        this.transactionTime = transactionTime;
    }
    public void setUser(int user) {
        this.user = user;
    }
    public void setCurrfrom(int currfrom) {
        this.currfrom = currfrom;
    }
    public void setCurrto(int currto) {
        this.currto = currto;
    }
    public void setDteapp(int dteapp) {
        this.dteapp = dteapp;
    }
    public void setDtetrm(int dtetrm) {
        this.dtetrm = dtetrm;
    }
    public void setTrmcde(String trmcde) {
        this.trmcde = trmcde;
    }
    public void setDteexp(int dteexp) {
        this.dteexp = dteexp;
    }
    public void setPftflg(String pftflg) {
        this.pftflg = pftflg;
    }
    public void setRasflg(String rasflg) {
        this.rasflg = rasflg;
    }
    public void setBcmtab(String bcmtab) {
        this.bcmtab = bcmtab;
    }
    public void setRcmtab(String rcmtab) {
        this.rcmtab = rcmtab;
    }
    public void setScmtab(String scmtab) {
        this.scmtab = scmtab;
    }
    public void setAgentClass(String agentClass) {
        this.agentClass = agentClass;
    }
    public void setOcmtab(String ocmtab) {
        this.ocmtab = ocmtab;
    }
    public void setReportag(String reportag) {
        this.reportag = reportag;
    }
    public void setOvcpc(BigDecimal ovcpc) {
        this.ovcpc = ovcpc;
    }
    public void setTaxmeth(String taxmeth) {
        this.taxmeth = taxmeth;
    }
    public void setIrdno(String irdno) {
        this.irdno = irdno;
    }
    public void setTaxcde(String taxcde) {
        this.taxcde = taxcde;
    }
    public void setTaxalw(BigDecimal taxalw) {
        this.taxalw = taxalw;
    }
    public void setSprschm(String sprschm) {
        this.sprschm = sprschm;
    }
    public void setSprprc(BigDecimal sprprc) {
        this.sprprc = sprprc;
    }
    public void setPayclt(String payclt) {
        this.payclt = payclt;
    }
    public void setPaymth(String paymth) {
        this.paymth = paymth;
    }
    public void setPayfrq(String payfrq) {
        this.payfrq = payfrq;
    }
    public void setFacthous(String facthous) {
        this.facthous = facthous;
    }
    public void setBankkey(String bankkey) {
        this.bankkey = bankkey;
    }
    public void setBankacckey(String bankacckey) {
        this.bankacckey = bankacckey;
    }
    public void setDtepay(int dtepay) {
        this.dtepay = dtepay;
    }
    public void setCurrcode(String currcode) {
        this.currcode = currcode;
    }
    public void setIntcrd(BigDecimal intcrd) {
        this.intcrd = intcrd;
    }
    public void setFixprc(BigDecimal fixprc) {
        this.fixprc = fixprc;
    }
    public void setBmaflg(String bmaflg) {
        this.bmaflg = bmaflg;
    }
    public void setExclAgmt(String exclAgmt) {
        this.exclAgmt = exclAgmt;
    }
    public void setHouseLoan(String houseLoan) {
        this.houseLoan = houseLoan;
    }
    public void setComputerLoan(String computerLoan) {
        this.computerLoan = computerLoan;
    }
    public void setCarLoan(String carLoan) {
        this.carLoan = carLoan;
    }
    public void setOfficeRent(String officeRent) {
        this.officeRent = officeRent;
    }
    public void setOtherLoans(String otherLoans) {
        this.otherLoans = otherLoans;
    }
    public void setAracde(String aracde) {
        this.aracde = aracde;
    }
    public void setMinsta(BigDecimal minsta) {
        this.minsta = minsta;
    }
    public void setZrorcode(String zrorcode) {
        this.zrorcode = zrorcode;
    }
    public void setEffdate(int effdate) {
        this.effdate = effdate;
    }
    public void setTagsusind(String tagsusind) {
        this.tagsusind = tagsusind;
    }
    public void setTlaglicno(String tlaglicno) {
        this.tlaglicno = tlaglicno;
    }
    public void setTlicexpdt(int tlicexpdt) {
        this.tlicexpdt = tlicexpdt;
    }
    public void setTcolprct(BigDecimal tcolprct) {
        this.tcolprct = tcolprct;
    }
    public void setTcolmax(BigDecimal tcolmax) {
        this.tcolmax = tcolmax;
    }
    public void setTsalesunt(String tsalesunt) {
        this.tsalesunt = tsalesunt;
    }
    public void setPrdagent(String prdagent) {
        this.prdagent = prdagent;
    }
    public void setAgccqind(String agccqind) {
        this.agccqind = agccqind;
    }
    public void setZrecruit(String zrecruit) {
        this.zrecruit = zrecruit;
    }
    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }
    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
    public void setDatime(String datime) {
        this.datime = datime;
    }
	public String getAgntbr() {
		return agntbr;
	}
	public void setAgntbr(String agntbr) {
		this.agntbr = agntbr;
	}
}