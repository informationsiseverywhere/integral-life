package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.B5023TempDAO;
import com.csc.life.regularprocessing.dataaccess.model.B5023DTO;
import com.csc.life.regularprocessing.dataaccess.model.T5679T5679Rec;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class B5023TempDAOImpl extends BaseDAOImpl<B5023DTO> implements B5023TempDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(B5023TempDAOImpl.class);

    private final static String TABLE_NAME = "B5023DATA"; //ILB-475

    private void deleteTempData() {
        String sqlStr = "DELETE FROM " + TABLE_NAME;
        PreparedStatement ps = getPrepareStatement(sqlStr);
        try {
            ps.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("deleteTempData()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
    }

    private void createTempTable(String wsaaParmCompany, String p5023ChdrnumFrm, String p5023ChdrnumTo,
            int wsaaSqlFrmdate, int wsaaSqlTodate, T5679T5679Rec t5679T5679RecInner) {
        StringBuilder sqlStr = new StringBuilder("INSERT INTO ");
        sqlStr.append(TABLE_NAME);
        sqlStr.append(" (COCHDRCOY,COCHDRNUM,COLIFE,COCOVERAGE,CORIDER,COPLNSFX,COCRTABLE,COPSTATCODE,COSTATCODE,COCRRCD,COCBUNST,CORCESDTE,COSUMINS,COVALIDFLAG,COCURRFROM,COCURRTO) ");
        sqlStr.append(" SELECT CO.CHDRCOY COCHDRCOY, CO.CHDRNUM COCHDRNUM, CO.LIFE COLIFE, CO.COVERAGE COCOVERAGE, CO.RIDER CORIDER, CO.PLNSFX COPLNSFX, CO.CRTABLE COCRTABLE, CO.PSTATCODE COPSTATCODE, CO.STATCODE COSTATCODE, CO.CRRCD COCRRCD, CO.CBUNST COCBUNST, CO.RCESDTE CORCESDTE, CO.SUMINS COSUMINS, CO.VALIDFLAG COVALIDFLAG, CO.CURRFROM COCURRFROM, CO.CURRTO COCURRTO ");
        //sqlStr.append(", CH.CURRFRM CHCURRFRM, CH.CURRTO CHCURRTO, CH.UNIQUE_NUMBER CHUNIQUE ");
        sqlStr.append("   FROM COVRPF CO, CHDRPF CH ");
        sqlStr.append("     WHERE CO.CHDRCOY = ? AND CO.BNUSIN = 'P' AND (CO.BAPPMETH IS NULL OR CO.BAPPMETH  = '    ') AND CO.VALIDFLAG = '1' AND CO.CHDRNUM BETWEEN ? AND ? AND (CO.CBUNST > ? OR CO.CBUNST = ?) AND (CO.CBUNST < ? OR CO.CBUNST = ?) AND CO.STATCODE IN (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) AND CO.PSTATCODE IN (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) AND (CH.CHDRCOY = CO.CHDRCOY AND CH.CHDRNUM = CO.CHDRNUM AND CH.VALIDFLAG = '1' AND CH.STATCODE IN (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) AND CH.PSTCDE IN (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)) ");
        sqlStr.append("       ORDER BY CO.CHDRCOY, CO.CHDRNUM, CO.COVERAGE, CO.LIFE, CO.RIDER, CO.PLNSFX, CH.CHDRCOY ASC, CH.CHDRNUM ASC, CH.UNIQUE_NUMBER DESC");

        PreparedStatement ps = getPrepareStatement(sqlStr.toString());
        try {
            int i = 1;
            ps.setString(i++, wsaaParmCompany);
            ps.setString(i++, p5023ChdrnumFrm);
            ps.setString(i++, p5023ChdrnumTo);
            ps.setInt(i++, wsaaSqlFrmdate);
            ps.setInt(i++, wsaaSqlFrmdate);
            ps.setInt(i++, wsaaSqlTodate);
            ps.setInt(i++, wsaaSqlTodate);
            ps.setString(i++, t5679T5679RecInner.getT5679CovRiskStat01());
            ps.setString(i++, t5679T5679RecInner.getT5679CovRiskStat02());
            ps.setString(i++, t5679T5679RecInner.getT5679CovRiskStat03());
            ps.setString(i++, t5679T5679RecInner.getT5679CovRiskStat04());
            ps.setString(i++, t5679T5679RecInner.getT5679CovRiskStat05());
            ps.setString(i++, t5679T5679RecInner.getT5679CovRiskStat06());
            ps.setString(i++, t5679T5679RecInner.getT5679CovRiskStat07());
            ps.setString(i++, t5679T5679RecInner.getT5679CovRiskStat08());
            ps.setString(i++, t5679T5679RecInner.getT5679CovRiskStat09());
            ps.setString(i++, t5679T5679RecInner.getT5679CovRiskStat10());
            ps.setString(i++, t5679T5679RecInner.getT5679CovRiskStat11());
            ps.setString(i++, t5679T5679RecInner.getT5679CovRiskStat12());
            ps.setString(i++, t5679T5679RecInner.getT5679CovPremStat01());
            ps.setString(i++, t5679T5679RecInner.getT5679CovPremStat02());
            ps.setString(i++, t5679T5679RecInner.getT5679CovPremStat03());
            ps.setString(i++, t5679T5679RecInner.getT5679CovPremStat04());
            ps.setString(i++, t5679T5679RecInner.getT5679CovPremStat05());
            ps.setString(i++, t5679T5679RecInner.getT5679CovPremStat06());
            ps.setString(i++, t5679T5679RecInner.getT5679CovPremStat07());
            ps.setString(i++, t5679T5679RecInner.getT5679CovPremStat08());
            ps.setString(i++, t5679T5679RecInner.getT5679CovPremStat09());
            ps.setString(i++, t5679T5679RecInner.getT5679CovPremStat10());
            ps.setString(i++, t5679T5679RecInner.getT5679CovPremStat11());
            ps.setString(i++, t5679T5679RecInner.getT5679CovPremStat12());
            ps.setString(i++, t5679T5679RecInner.getT5679CnRiskStat01());
            ps.setString(i++, t5679T5679RecInner.getT5679CnRiskStat02());
            ps.setString(i++, t5679T5679RecInner.getT5679CnRiskStat03());
            ps.setString(i++, t5679T5679RecInner.getT5679CnRiskStat04());
            ps.setString(i++, t5679T5679RecInner.getT5679CnRiskStat05());
            ps.setString(i++, t5679T5679RecInner.getT5679CnRiskStat06());
            ps.setString(i++, t5679T5679RecInner.getT5679CnRiskStat07());
            ps.setString(i++, t5679T5679RecInner.getT5679CnRiskStat08());
            ps.setString(i++, t5679T5679RecInner.getT5679CnRiskStat09());
            ps.setString(i++, t5679T5679RecInner.getT5679CnRiskStat10());
            ps.setString(i++, t5679T5679RecInner.getT5679CnRiskStat11());
            ps.setString(i++, t5679T5679RecInner.getT5679CnRiskStat12());
            ps.setString(i++, t5679T5679RecInner.getT5679CnPremStat01());
            ps.setString(i++, t5679T5679RecInner.getT5679CnPremStat02());
            ps.setString(i++, t5679T5679RecInner.getT5679CnPremStat03());
            ps.setString(i++, t5679T5679RecInner.getT5679CnPremStat04());
            ps.setString(i++, t5679T5679RecInner.getT5679CnPremStat05());
            ps.setString(i++, t5679T5679RecInner.getT5679CnPremStat06());
            ps.setString(i++, t5679T5679RecInner.getT5679CnPremStat07());
            ps.setString(i++, t5679T5679RecInner.getT5679CnPremStat08());
            ps.setString(i++, t5679T5679RecInner.getT5679CnPremStat09());
            ps.setString(i++, t5679T5679RecInner.getT5679CnPremStat10());
            ps.setString(i++, t5679T5679RecInner.getT5679CnPremStat11());
            ps.setString(i++, t5679T5679RecInner.getT5679CnPremStat12());
            ps.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("insertTempData()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }

    }

    @Override
    public void initTempTable(String wsaaParmCompany, String p5023ChdrnumFrm, String p5023ChdrnumTo,
            int wsaaSqlFrmdate, int wsaaSqlTodate, T5679T5679Rec t5679T5679RecInner) {
        deleteTempData();
        createTempTable(wsaaParmCompany, p5023ChdrnumFrm, p5023ChdrnumTo, wsaaSqlFrmdate, wsaaSqlTodate,
                t5679T5679RecInner);
    }

    @Override
    public List<B5023DTO> searchRacdResult(int batchExtractSize, int batchID) {
        StringBuilder sqlStr = new StringBuilder();
        sqlStr.append("SELECT * FROM (");
        sqlStr.append("SELECT COCHDRCOY,COCHDRNUM,COLIFE,COCOVERAGE,CORIDER,COPLNSFX,COCRTABLE,COPSTATCODE,COSTATCODE,COCRRCD,COCBUNST,CORCESDTE,COSUMINS,COVALIDFLAG,COCURRFROM,COCURRTO ");
        sqlStr.append(", FLOOR((ROW_NUMBER() OVER(ORDER BY COCHDRCOY, COCHDRNUM, COCOVERAGE, COLIFE, CORIDER, COPLNSFX)-1)/?) BATCHNUM ");
        sqlStr.append(" FROM ");
        sqlStr.append(TABLE_NAME);
        sqlStr.append(") MAIN WHERE batchnum = ? ");
        PreparedStatement ps = getPrepareStatement(sqlStr.toString());
        List<B5023DTO> dtoList = new LinkedList<B5023DTO>();
        ResultSet rs = null;
        try {
            ps.setInt(1, batchExtractSize);
            ps.setInt(2, batchID);

            rs = ps.executeQuery();

            while (rs.next()) {
                B5023DTO dto = new B5023DTO();
                dto.setChdrcoy(rs.getString(1));
                dto.setChdrnum(rs.getString(2));
                dto.setLife(rs.getString(3));
                dto.setCoverage(rs.getString(4));
                dto.setRider(rs.getString(5));
                dto.setPlnsfx(rs.getInt(6));
                dto.setCrtable(rs.getString(7));
                dto.setPstatcode(rs.getString(8));
                dto.setStatcode(rs.getString(9));
                dto.setCrrcd(rs.getInt(10));
                dto.setCbunst(rs.getInt(11));
                dto.setRcesdte(rs.getInt(12));
                dto.setSumins(rs.getBigDecimal(13));
                dto.setValidflag(rs.getString(14));
                dto.setCurrfrom(rs.getInt(15));
                dto.setCurrto(rs.getInt(16));
//                dto.setChcurrfrm(rs.getInt(17));
//                dto.setChcurrto(rs.getInt(18));
//                dto.setChunique(rs.getLong(19));
                dtoList.add(dto);
            }

        } catch (SQLException e) {
            LOGGER.error("searchRacdResult()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return dtoList;
    }

}