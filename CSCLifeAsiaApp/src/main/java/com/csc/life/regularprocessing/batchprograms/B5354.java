/*
 * File: B5354.java
 * Date: 29 August 2009 21:09:05
 * Author: Quipoz Limited
 * 
 * Class transformed from B5354.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsu.general.dataaccess.PayrpfTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.regularprocessing.dataaccess.PayzpfTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* B5354 - Overdue Splitter Program.
*
* Overview.
* =========
* This program is the Overdue Splitter program to be run
* before Overdue (B5355) in multi-thread.
*
* SQL will be used to access the PAYR physical file. The
* splitter program will extract PAYR records that meet the
* following criteria:
*
* i    btdate     not = 0
* ii   btdate     not = 999999999
* iii  btdate     >  ptdate
* iv   ptdate     <  <ws-overdue-date>
* v    validflag  =  '1'
* vi   chdrnum    >  <parm-contractfrom>
* vii  chdrnum    <  <parm-contractto>
* viii aplsupr    not = 'y' or
*      (aplsupr   = 'y' and
*      <run-date> not between aplsfrom and aplsto)
* ix   notssupr   not = 'y' or
*      (notssupr  = 'y' and
*      <run-date> not between notsspfrom and notsspto)
* x    chdroy     =  <run-company>
* xi   billchnl   <> 'n'
*      order by chdrcoy, chdrnum
*
* Control Totals.
* ===============
* 01 : Number of thread members
* 02 : Number of extracted records
*
* Notes on Splitter Programs.
* ===========================
* Splitter programs should be simple. They should only deal
* with a single file. If there is insufficient data on the
* primary file to completely identify a transaction then
* further editing should be done in the subsequent process.
* The objective of a splitter is to rapidily isolate potential
* transactions, not process the transaction. The primary concern
* for splitter program design is elasped time speed and this is
* achieved by minimising disk IO.
*
* Before the Splitter process is invoked the, the program CRTTMPF
* should be run. The CRTTMPF (Create Temporary File) will create
* a duplicate of a physical file (created under Smart and defined
* as a Query file) which will have as many members as are defined
* in that process's subsequent threads field. The temporary file
* will have the name XXXXDD9999 where XXXX is the first four
* characters of the file which was duplicated, DD is the first
* two characters of the 4th system parameter and 9999 is the
* schedule run number. Each member added to the temporary file
* will be called THREAD999 where 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read records from a primary file
* and for each record it selects it will write a record to a
* temporary file member. Exactly which temporary file member
* is written to is decided by a working storage count. The
* objective of the count is spread the transaction records
* evenly across the thread members.
*
* The Splitter program should always have a restart method of
* '1' indicating a re-run. In the event of a restart the Splitter
* program will always start with empty file members. Apart from
* writing records to the temporary file members Splitter programs
* should not be doing any further updates.
*
* There should be no non-standard CL commands in the CL Pgm which
* calls this program.
*
* It is important that the process which runs the splitter
* program has the same 'number of subsequent threads' as the
* following process has defined in 'number of threads this
* process.'
*
* The subsequent program need only point itself to one member
* based around the field BSPR-SCHEDULE-THREAD-NO which indicates
* in which thread the copy of the subsequent program is running.
*
*****************************************************************
* </pre>
*/
public class B5354 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlpayrCursorrs = null;
	private java.sql.PreparedStatement sqlpayrCursorps = null;
	private java.sql.Connection sqlpayrCursorconn = null;
	private String sqlpayrCursor = "";
	private int payrCursorLoopIndex = 0;
	private PayzpfTableDAM payzpf = new PayzpfTableDAM();
	private DiskFileDAM payz01 = new DiskFileDAM("PAYZ01");
	private DiskFileDAM payz02 = new DiskFileDAM("PAYZ02");
	private DiskFileDAM payz03 = new DiskFileDAM("PAYZ03");
	private DiskFileDAM payz04 = new DiskFileDAM("PAYZ04");
	private DiskFileDAM payz05 = new DiskFileDAM("PAYZ05");
	private DiskFileDAM payz06 = new DiskFileDAM("PAYZ06");
	private DiskFileDAM payz07 = new DiskFileDAM("PAYZ07");
	private DiskFileDAM payz08 = new DiskFileDAM("PAYZ08");
	private DiskFileDAM payz09 = new DiskFileDAM("PAYZ09");
	private DiskFileDAM payz10 = new DiskFileDAM("PAYZ10");
	private DiskFileDAM payz11 = new DiskFileDAM("PAYZ11");
	private DiskFileDAM payz12 = new DiskFileDAM("PAYZ12");
	private DiskFileDAM payz13 = new DiskFileDAM("PAYZ13");
	private DiskFileDAM payz14 = new DiskFileDAM("PAYZ14");
	private DiskFileDAM payz15 = new DiskFileDAM("PAYZ15");
	private DiskFileDAM payz16 = new DiskFileDAM("PAYZ16");
	private DiskFileDAM payz17 = new DiskFileDAM("PAYZ17");
	private DiskFileDAM payz18 = new DiskFileDAM("PAYZ18");
	private DiskFileDAM payz19 = new DiskFileDAM("PAYZ19");
	private DiskFileDAM payz20 = new DiskFileDAM("PAYZ20");
	private PayzpfTableDAM payzpfData = new PayzpfTableDAM();
		/*    Change the record length to that of the temporary file.*/
	private FixedLengthStringData payz01Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz02Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz03Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz04Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz05Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz06Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz07Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz08Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz09Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz10Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz11Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz12Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz13Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz14Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz15Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz16Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz17Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz18Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz19Rec = new FixedLengthStringData(22);
	private FixedLengthStringData payz20Rec = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5354");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

		/*    PAYZ member parameters.*/
	private FixedLengthStringData wsaaPayzFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaPayzFn, 0, FILLER).init("PAYZ");
	private FixedLengthStringData wsaaPayzRunid = new FixedLengthStringData(2).isAPartOf(wsaaPayzFn, 4);
	private ZonedDecimalData wsaaPayzJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaPayzFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
		/*01  PAYZPF-DATA.                                                 
		  COPY DDS-ALL-FORMATS OF PAYZPF.                              */
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
		/*    Pointers to point to the member to open (IZ) and write (IY).*/
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
		/*    Define the number of rows in the block to be fetched.*/
	private PackedDecimalData wsaaRowsInBlock = new PackedDecimalData(5, 0).init(2);

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaPayrData = FLSInittedArray (2, 24);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaPayrData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaPayrData, 1);
	private PackedDecimalData[] wsaaPayrseqno = PDArrayPartOfArrayStructure(1, 0, wsaaPayrData, 9);
	private PackedDecimalData[] wsaaBtdate = PDArrayPartOfArrayStructure(8, 0, wsaaPayrData, 10);
	private PackedDecimalData[] wsaaPtdate = PDArrayPartOfArrayStructure(8, 0, wsaaPayrData, 15);
	private FixedLengthStringData[] wsaaBillchnl = FLSDArrayPartOfArrayStructure(1, wsaaPayrData, 20);
	private FixedLengthStringData[] wsaaNfoOption = FLSDArrayPartOfArrayStructure(3, wsaaPayrData, 21);

		/* WSAA-IND-ARRAY */
	private FixedLengthStringData[] wsaaPayrInd = FLSInittedArray (2, 4);
	private BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(2, 4, 0, wsaaPayrInd, 0);
		/* WSAA-HOST-VARIABLES */
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).init(0);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private ZonedDecimalData wsaaOverdueDate = new ZonedDecimalData(8, 0).init(0);
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
	private PayrpfTableDAM payrpfData = new PayrpfTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private P6671par p6671par = new P6671par();

	public B5354() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/*    Check that the restart method from the process definition*/
		/*    is compatible with the program (of type '1'). In the event*/
		/*    of failure, the program will be re-run from the beginning.*/
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*     allowed for a maximum of 20.)*/
		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		/*    Since this program runs from a parameter screen move the*/
		/*    internal parameter area to the original parameter copybook*/
		/*    to retrieve the contract range.*/
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES)
		&& isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		/*    Construct the name of the temporary file that*/
		/*    we will be working with.*/
		wsaaPayzRunid.set(bprdIO.getSystemParam04());
		wsaaPayzJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		/*    Open the required number of temporary files, depending*/
		/*    on IZ and determined by the number of threads specified*/
		/*    for the subsequent process.*/
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		/*    Log the number of threads being used.*/
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		/*    Use DATCON2 to add 15 days onto the run-date to obtain*/
		/*    the WSAA-OVERDUE-DATE.*/
		datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(1);
		/*  MOVE -15                    TO DTC2-FREQ-FACTOR.     <LA2111>*/
		/* MOVE 15                     TO DTC2-FREQ-FACTOR.             */
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		wsaaOverdueDate.set(datcon2rec.intDate2);
		iy.set(1);
		wsaaEffdate.set(bsscIO.getEffectiveDate());
		wsaaCompany.set(bsprIO.getCompany());
		/*    Define the query required by declaring a cursor.*/
		sqlpayrCursor = " SELECT  PAYR.CHDRCOY, PAYR.CHDRNUM, PAYR.PAYRSEQNO, PAYR.BTDATE, PAYR.PTDATE, PAYR.BILLCHNL, HD.ZNFOPT" +
" FROM   " + getAppVars().getTableNameOverriden("PAYRPF") + " PAYR LEFT JOIN VM1DTA.HPADPF HD ON  HD.CHDRCOY = PAYR.CHDRCOY AND HD.CHDRNUM= PAYR.CHDRNUM AND HD.VALIDFLAG ='1' " +
" WHERE PAYR.BTDATE <> 0" +
" AND PAYR.BTDATE <> 99999999" +
" AND PAYR.BTDATE >= PTDATE" +
" AND PAYR.PTDATE <> 0" +
" AND PAYR.PTDATE <= ?" +
" AND PAYR.VALIDFLAG = '1'" +
" AND PAYR.CHDRNUM BETWEEN ? AND ?" +
" AND (PAYR.APLSUPR <> 'Y'" +
" OR (PAYR.APLSUPR = 'Y'" +
//ILIFE-4323 by dpuhawan
" AND ? > PAYR.APLSPFROM AND ? >=PAYR.APLSPTO))" +
//" AND ? NOT BETWEEN APLSPFROM AND APLSPTO))" +
" AND (PAYR.NOTSSUPR <> 'Y'" +
" OR (PAYR.NOTSSUPR = 'Y'" +
//ILIFE-4323 by dpuhawan
" AND ? >  PAYR.NOTSSPFROM AND ? >=PAYR.NOTSSPTO))" +
//" AND ? NOT BETWEEN NOTSSPFROM AND NOTSSPTO))" +
" AND PAYR.CHDRCOY = ?" +
" AND PAYR.BILLCHNL <> 'N'" +
" AND PAYR.PAYRSEQNO = 1" +
" ORDER BY PAYR.CHDRCOY, PAYR.CHDRNUM, PAYR.PAYRSEQNO";
		/*    Declare and initialise an indexed working storage array*/
		/*    to hold all the returned PAYR records, and an associated*/
		/*    null-indicator array for all character fields in the*/
		/*    first array.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		/*    Declare and open the cursor for this block.*/
		sqlerrorflag = false;
		try {
			sqlpayrCursorconn = getAppVars().getDBConnectionForTable(new com.csc.fsu.general.dataaccess.PayrpfTableDAM());
			sqlpayrCursorps = getAppVars().prepareStatementEmbeded(sqlpayrCursorconn, sqlpayrCursor, "PAYRPF");
			getAppVars().setDBNumber(sqlpayrCursorps, 1, wsaaOverdueDate);
			getAppVars().setDBString(sqlpayrCursorps, 2, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlpayrCursorps, 3, wsaaChdrnumTo);
			getAppVars().setDBNumber(sqlpayrCursorps, 4, wsaaEffdate);
			getAppVars().setDBNumber(sqlpayrCursorps, 5, wsaaEffdate);
			//ILIFE-4323 start by dpuhawan
			//getAppVars().setDBString(sqlpayrCursorps, 6, wsaaCompany);
			getAppVars().setDBNumber(sqlpayrCursorps, 6, wsaaEffdate);
			getAppVars().setDBNumber(sqlpayrCursorps, 7, wsaaEffdate);
			getAppVars().setDBString(sqlpayrCursorps, 8, wsaaCompany);
			//ILIFE-4323 end
			sqlpayrCursorrs = getAppVars().executeQuery(sqlpayrCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

protected void openThreadMember1110()
	{
		/*    Clear the temporary file.*/
		/* MOVE IZ                     TO WSAA-THREAD-NUMBER.           */
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(), 1), iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaPayzFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz, varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.params.set("CLRTMPF");
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(PAYZ");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaPayzFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*    Open the temporary output files.*/
		if (isEQ(iz, 1)) {
			payz01.openOutput();
		}
		if (isEQ(iz, 2)) {
			payz02.openOutput();
		}
		if (isEQ(iz, 3)) {
			payz03.openOutput();
		}
		if (isEQ(iz, 4)) {
			payz04.openOutput();
		}
		if (isEQ(iz, 5)) {
			payz05.openOutput();
		}
		if (isEQ(iz, 6)) {
			payz06.openOutput();
		}
		if (isEQ(iz, 7)) {
			payz07.openOutput();
		}
		if (isEQ(iz, 8)) {
			payz08.openOutput();
		}
		if (isEQ(iz, 9)) {
			payz09.openOutput();
		}
		if (isEQ(iz, 10)) {
			payz10.openOutput();
		}
		if (isEQ(iz, 11)) {
			payz11.openOutput();
		}
		if (isEQ(iz, 12)) {
			payz12.openOutput();
		}
		if (isEQ(iz, 13)) {
			payz13.openOutput();
		}
		if (isEQ(iz, 14)) {
			payz14.openOutput();
		}
		if (isEQ(iz, 15)) {
			payz15.openOutput();
		}
		if (isEQ(iz, 16)) {
			payz16.openOutput();
		}
		if (isEQ(iz, 17)) {
			payz17.openOutput();
		}
		if (isEQ(iz, 18)) {
			payz18.openOutput();
		}
		if (isEQ(iz, 19)) {
			payz19.openOutput();
		}
		if (isEQ(iz, 20)) {
			payz20.openOutput();
		}
	}

protected void initialiseArray1500()
	{
		/*START*/
		wsaaPayrseqno[wsaaInd.toInt()].set(ZERO);
		wsaaBtdate[wsaaInd.toInt()].set(ZERO);
		wsaaPtdate[wsaaInd.toInt()].set(ZERO);
		wsaaChdrcoy[wsaaInd.toInt()].set(SPACES);
		wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaBillchnl[wsaaInd.toInt()].set(SPACES);
		wsaaNfoOption[wsaaInd.toInt()].set(SPACES);
		/*EXIT*/
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		/*    Fetch the block of records into the array.*/
		/*    Also on the first entry into the program we must set up the*/
		/*    WSAA-PREV-CHDRNUM = the present chdrnum.*/
		if (isNE(wsaaInd, 1)) {
			return ;
		}
		sqlerrorflag = false;
		try {
			for (payrCursorLoopIndex = 1; isLT(payrCursorLoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlpayrCursorrs); payrCursorLoopIndex++ ){
				getAppVars().getDBObject(sqlpayrCursorrs, 1, wsaaChdrcoy[payrCursorLoopIndex], wsaaNullInd[payrCursorLoopIndex][1]);
				getAppVars().getDBObject(sqlpayrCursorrs, 2, wsaaChdrnum[payrCursorLoopIndex], wsaaNullInd[payrCursorLoopIndex][2]);
				getAppVars().getDBObject(sqlpayrCursorrs, 3, wsaaPayrseqno[payrCursorLoopIndex]);
				getAppVars().getDBObject(sqlpayrCursorrs, 4, wsaaBtdate[payrCursorLoopIndex]);
				getAppVars().getDBObject(sqlpayrCursorrs, 5, wsaaPtdate[payrCursorLoopIndex]);
				getAppVars().getDBObject(sqlpayrCursorrs, 6, wsaaBillchnl[payrCursorLoopIndex]);
				getAppVars().getDBObject(sqlpayrCursorrs, 7, wsaaNfoOption[payrCursorLoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*    An SQLCODE = +100 is returned when :*/
		/*    a) no rows are returned on the first fetch*/
		/*    b) the last row is returned within the block*/
		/*    We must continue processing the 3000 section for case b).*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(varcom.endp);
		}
		//tom chi modified, bug 1030
		else if(isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)){
			wsspEdterror.set(varcom.endp);
		}
		//end
		else {
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}
		/*EXIT*/
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES))) {
			/*    Re-initialise the block for next fetch and point to*/
			/*    first row.*/
			loadThreads3100();
		}
		
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*    If the CHDRNUM being processed is not equal to the*/
		/*    previous CHDRNUM we should move to the next output file*/
		/*    member. The condition is here to allow for checking the*/
		/*    last CHDRNUM of the old block with the first CHDRNUM of*/
		/*    the new block and to write to the next member if they*/
		/*    have changed.*/
		if (isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)) {
			iy.add(1);
			wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
		}
		/*    Load from storage all PAYZ data for the same contract*/
		/*    until the CHDRNUM on PAYZ has changed or the end*/
		/*    of an incomplete block is reached.*/
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum))) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		start3200();
	}

protected void start3200()
	{
		payzpfData.chdrcoy.set(wsaaChdrcoy[wsaaInd.toInt()]);
		payzpfData.chdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
		payzpfData.payrseqno.set(wsaaPayrseqno[wsaaInd.toInt()]);
		payzpfData.btdate.set(wsaaBtdate[wsaaInd.toInt()]);
		payzpfData.ptdate.set(wsaaPtdate[wsaaInd.toInt()]);
		payzpfData.billchnl.set(wsaaBillchnl[wsaaInd.toInt()]);
		payzpfData.znfopt.set(wsaaNfoOption[wsaaInd.toInt()]);
		if (isGT(iy, bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		/*    Write records to their corresponding temporary file*/
		/*    members, determined by the working storage count, IY.*/
		/*    This spreads the transaction members evenly across*/
		/*    the thread members.*/
		if (isEQ(iy, 1)) {
			payz01.write(payzpfData);
		}
		if (isEQ(iy, 2)) {
			payz02.write(payzpfData);
		}
		if (isEQ(iy, 3)) {
			payz03.write(payzpfData);
		}
		if (isEQ(iy, 4)) {
			payz04.write(payzpfData);
		}
		if (isEQ(iy, 5)) {
			payz05.write(payzpfData);
		}
		if (isEQ(iy, 6)) {
			payz06.write(payzpfData);
		}
		if (isEQ(iy, 7)) {
			payz07.write(payzpfData);
		}
		if (isEQ(iy, 8)) {
			payz08.write(payzpfData);
		}
		if (isEQ(iy, 9)) {
			payz09.write(payzpfData);
		}
		if (isEQ(iy, 10)) {
			payz10.write(payzpfData);
		}
		if (isEQ(iy, 11)) {
			payz11.write(payzpfData);
		}
		if (isEQ(iy, 12)) {
			payz12.write(payzpfData);
		}
		if (isEQ(iy, 13)) {
			payz13.write(payzpfData);
		}
		if (isEQ(iy, 14)) {
			payz14.write(payzpfData);
		}
		if (isEQ(iy, 15)) {
			payz15.write(payzpfData);
		}
		if (isEQ(iy, 16)) {
			payz16.write(payzpfData);
		}
		if (isEQ(iy, 17)) {
			payz17.write(payzpfData);
		}
		if (isEQ(iy, 18)) {
			payz18.write(payzpfData);
		}
		if (isEQ(iy, 19)) {
			payz19.write(payzpfData);
		}
		if (isEQ(iy, 20)) {
			payz20.write(payzpfData);
		}
		/*    Log the number of extacted records.*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
		/*    If the record written out above is the last in the array we*/
		/*    have already stored it to compare it with the first of the*/
		/*    next set of records to be fetched from the instalment file.*/
		/*    Set up the array for the next block of records.*/
		/*    Otherwise process the next record in the array.*/
		wsaaInd.add(1);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/**    There is no pre-commit processing to be done.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*    Close the cursor.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlpayrCursorconn, sqlpayrCursorps, sqlpayrCursorrs);
		/*    Close the open files.*/
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		/*    Remove the overrides.*/
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		close4110();
	}

protected void close4110()
	{
		if (isEQ(iz, 1)) {
			payz01.close();
		}
		if (isEQ(iz, 2)) {
			payz02.close();
		}
		if (isEQ(iz, 3)) {
			payz03.close();
		}
		if (isEQ(iz, 4)) {
			payz04.close();
		}
		if (isEQ(iz, 5)) {
			payz05.close();
		}
		if (isEQ(iz, 6)) {
			payz06.close();
		}
		if (isEQ(iz, 7)) {
			payz07.close();
		}
		if (isEQ(iz, 8)) {
			payz08.close();
		}
		if (isEQ(iz, 9)) {
			payz09.close();
		}
		if (isEQ(iz, 10)) {
			payz10.close();
		}
		if (isEQ(iz, 11)) {
			payz11.close();
		}
		if (isEQ(iz, 12)) {
			payz12.close();
		}
		if (isEQ(iz, 13)) {
			payz13.close();
		}
		if (isEQ(iz, 14)) {
			payz14.close();
		}
		if (isEQ(iz, 15)) {
			payz15.close();
		}
		if (isEQ(iz, 16)) {
			payz16.close();
		}
		if (isEQ(iz, 17)) {
			payz17.close();
		}
		if (isEQ(iz, 18)) {
			payz18.close();
		}
		if (isEQ(iz, 19)) {
			payz19.close();
		}
		if (isEQ(iz, 20)) {
			payz20.close();
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
