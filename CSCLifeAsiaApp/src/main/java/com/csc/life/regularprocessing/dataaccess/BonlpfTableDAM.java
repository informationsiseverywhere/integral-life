package com.csc.life.regularprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: BonlpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:05
 * Class transformed from BONLPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class BonlpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 834;
	public FixedLengthStringData bonlrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData bonlpfRecord = bonlrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(bonlrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(bonlrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(bonlrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(bonlrec);
	public FixedLengthStringData datetexc = DD.datetexc.copy().isAPartOf(bonlrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(bonlrec);
	public FixedLengthStringData longdesc01 = DD.longdesc.copy().isAPartOf(bonlrec);
	public FixedLengthStringData longdesc02 = DD.longdesc.copy().isAPartOf(bonlrec);
	public FixedLengthStringData longdesc03 = DD.longdesc.copy().isAPartOf(bonlrec);
	public FixedLengthStringData longdesc04 = DD.longdesc.copy().isAPartOf(bonlrec);
	public FixedLengthStringData longdesc05 = DD.longdesc.copy().isAPartOf(bonlrec);
	public FixedLengthStringData longdesc06 = DD.longdesc.copy().isAPartOf(bonlrec);
	public FixedLengthStringData longdesc07 = DD.longdesc.copy().isAPartOf(bonlrec);
	public FixedLengthStringData longdesc08 = DD.longdesc.copy().isAPartOf(bonlrec);
	public FixedLengthStringData longdesc09 = DD.longdesc.copy().isAPartOf(bonlrec);
	public FixedLengthStringData longdesc10 = DD.longdesc.copy().isAPartOf(bonlrec);
	public PackedDecimalData sumins01 = DD.sumins.copy().isAPartOf(bonlrec);
	public PackedDecimalData sumins02 = DD.sumins.copy().isAPartOf(bonlrec);
	public PackedDecimalData sumins03 = DD.sumins.copy().isAPartOf(bonlrec);
	public PackedDecimalData sumins04 = DD.sumins.copy().isAPartOf(bonlrec);
	public PackedDecimalData sumins05 = DD.sumins.copy().isAPartOf(bonlrec);
	public PackedDecimalData sumins06 = DD.sumins.copy().isAPartOf(bonlrec);
	public PackedDecimalData sumins07 = DD.sumins.copy().isAPartOf(bonlrec);
	public PackedDecimalData sumins08 = DD.sumins.copy().isAPartOf(bonlrec);
	public PackedDecimalData sumins09 = DD.sumins.copy().isAPartOf(bonlrec);
	public PackedDecimalData sumins10 = DD.sumins.copy().isAPartOf(bonlrec);
	public FixedLengthStringData bonusCalcMeth01 = DD.bcalmeth.copy().isAPartOf(bonlrec);
	public FixedLengthStringData bonusCalcMeth02 = DD.bcalmeth.copy().isAPartOf(bonlrec);
	public FixedLengthStringData bonusCalcMeth03 = DD.bcalmeth.copy().isAPartOf(bonlrec);
	public FixedLengthStringData bonusCalcMeth04 = DD.bcalmeth.copy().isAPartOf(bonlrec);
	public FixedLengthStringData bonusCalcMeth05 = DD.bcalmeth.copy().isAPartOf(bonlrec);
	public FixedLengthStringData bonusCalcMeth06 = DD.bcalmeth.copy().isAPartOf(bonlrec);
	public FixedLengthStringData bonusCalcMeth07 = DD.bcalmeth.copy().isAPartOf(bonlrec);
	public FixedLengthStringData bonusCalcMeth08 = DD.bcalmeth.copy().isAPartOf(bonlrec);
	public FixedLengthStringData bonusCalcMeth09 = DD.bcalmeth.copy().isAPartOf(bonlrec);
	public FixedLengthStringData bonusCalcMeth10 = DD.bcalmeth.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayThisYr01 = DD.bpayty.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayThisYr02 = DD.bpayty.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayThisYr03 = DD.bpayty.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayThisYr04 = DD.bpayty.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayThisYr05 = DD.bpayty.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayThisYr06 = DD.bpayty.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayThisYr07 = DD.bpayty.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayThisYr08 = DD.bpayty.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayThisYr09 = DD.bpayty.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayThisYr10 = DD.bpayty.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayLastYr01 = DD.bpayny.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayLastYr02 = DD.bpayny.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayLastYr03 = DD.bpayny.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayLastYr04 = DD.bpayny.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayLastYr05 = DD.bpayny.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayLastYr06 = DD.bpayny.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayLastYr07 = DD.bpayny.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayLastYr08 = DD.bpayny.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayLastYr09 = DD.bpayny.copy().isAPartOf(bonlrec);
	public PackedDecimalData bonPayLastYr10 = DD.bpayny.copy().isAPartOf(bonlrec);
	public PackedDecimalData totalBonus01 = DD.totbon.copy().isAPartOf(bonlrec);
	public PackedDecimalData totalBonus02 = DD.totbon.copy().isAPartOf(bonlrec);
	public PackedDecimalData totalBonus03 = DD.totbon.copy().isAPartOf(bonlrec);
	public PackedDecimalData totalBonus04 = DD.totbon.copy().isAPartOf(bonlrec);
	public PackedDecimalData totalBonus05 = DD.totbon.copy().isAPartOf(bonlrec);
	public PackedDecimalData totalBonus06 = DD.totbon.copy().isAPartOf(bonlrec);
	public PackedDecimalData totalBonus07 = DD.totbon.copy().isAPartOf(bonlrec);
	public PackedDecimalData totalBonus08 = DD.totbon.copy().isAPartOf(bonlrec);
	public PackedDecimalData totalBonus09 = DD.totbon.copy().isAPartOf(bonlrec);
	public PackedDecimalData totalBonus10 = DD.totbon.copy().isAPartOf(bonlrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(bonlrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(bonlrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(bonlrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(bonlrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(bonlrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(bonlrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(bonlrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public BonlpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for BonlpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public BonlpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for BonlpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public BonlpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for BonlpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public BonlpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("BONLPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"CURRFROM, " +
							"CURRTO, " +
							"DATETEXC, " +
							"VALIDFLAG, " +
							"LONGDESC01, " +
							"LONGDESC02, " +
							"LONGDESC03, " +
							"LONGDESC04, " +
							"LONGDESC05, " +
							"LONGDESC06, " +
							"LONGDESC07, " +
							"LONGDESC08, " +
							"LONGDESC09, " +
							"LONGDESC10, " +
							"SUMINS01, " +
							"SUMINS02, " +
							"SUMINS03, " +
							"SUMINS04, " +
							"SUMINS05, " +
							"SUMINS06, " +
							"SUMINS07, " +
							"SUMINS08, " +
							"SUMINS09, " +
							"SUMINS10, " +
							"BCALMETH01, " +
							"BCALMETH02, " +
							"BCALMETH03, " +
							"BCALMETH04, " +
							"BCALMETH05, " +
							"BCALMETH06, " +
							"BCALMETH07, " +
							"BCALMETH08, " +
							"BCALMETH09, " +
							"BCALMETH10, " +
							"BPAYTY01, " +
							"BPAYTY02, " +
							"BPAYTY03, " +
							"BPAYTY04, " +
							"BPAYTY05, " +
							"BPAYTY06, " +
							"BPAYTY07, " +
							"BPAYTY08, " +
							"BPAYTY09, " +
							"BPAYTY10, " +
							"BPAYNY01, " +
							"BPAYNY02, " +
							"BPAYNY03, " +
							"BPAYNY04, " +
							"BPAYNY05, " +
							"BPAYNY06, " +
							"BPAYNY07, " +
							"BPAYNY08, " +
							"BPAYNY09, " +
							"BPAYNY10, " +
							"TOTBON01, " +
							"TOTBON02, " +
							"TOTBON03, " +
							"TOTBON04, " +
							"TOTBON05, " +
							"TOTBON06, " +
							"TOTBON07, " +
							"TOTBON08, " +
							"TOTBON09, " +
							"TOTBON10, " +
							"TERMID, " +
							"USER_T, " +
							"TRDT, " +
							"TRTM, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     currfrom,
                                     currto,
                                     datetexc,
                                     validflag,
                                     longdesc01,
                                     longdesc02,
                                     longdesc03,
                                     longdesc04,
                                     longdesc05,
                                     longdesc06,
                                     longdesc07,
                                     longdesc08,
                                     longdesc09,
                                     longdesc10,
                                     sumins01,
                                     sumins02,
                                     sumins03,
                                     sumins04,
                                     sumins05,
                                     sumins06,
                                     sumins07,
                                     sumins08,
                                     sumins09,
                                     sumins10,
                                     bonusCalcMeth01,
                                     bonusCalcMeth02,
                                     bonusCalcMeth03,
                                     bonusCalcMeth04,
                                     bonusCalcMeth05,
                                     bonusCalcMeth06,
                                     bonusCalcMeth07,
                                     bonusCalcMeth08,
                                     bonusCalcMeth09,
                                     bonusCalcMeth10,
                                     bonPayThisYr01,
                                     bonPayThisYr02,
                                     bonPayThisYr03,
                                     bonPayThisYr04,
                                     bonPayThisYr05,
                                     bonPayThisYr06,
                                     bonPayThisYr07,
                                     bonPayThisYr08,
                                     bonPayThisYr09,
                                     bonPayThisYr10,
                                     bonPayLastYr01,
                                     bonPayLastYr02,
                                     bonPayLastYr03,
                                     bonPayLastYr04,
                                     bonPayLastYr05,
                                     bonPayLastYr06,
                                     bonPayLastYr07,
                                     bonPayLastYr08,
                                     bonPayLastYr09,
                                     bonPayLastYr10,
                                     totalBonus01,
                                     totalBonus02,
                                     totalBonus03,
                                     totalBonus04,
                                     totalBonus05,
                                     totalBonus06,
                                     totalBonus07,
                                     totalBonus08,
                                     totalBonus09,
                                     totalBonus10,
                                     termid,
                                     user,
                                     transactionDate,
                                     transactionTime,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		currfrom.clear();
  		currto.clear();
  		datetexc.clear();
  		validflag.clear();
  		longdesc01.clear();
  		longdesc02.clear();
  		longdesc03.clear();
  		longdesc04.clear();
  		longdesc05.clear();
  		longdesc06.clear();
  		longdesc07.clear();
  		longdesc08.clear();
  		longdesc09.clear();
  		longdesc10.clear();
  		sumins01.clear();
  		sumins02.clear();
  		sumins03.clear();
  		sumins04.clear();
  		sumins05.clear();
  		sumins06.clear();
  		sumins07.clear();
  		sumins08.clear();
  		sumins09.clear();
  		sumins10.clear();
  		bonusCalcMeth01.clear();
  		bonusCalcMeth02.clear();
  		bonusCalcMeth03.clear();
  		bonusCalcMeth04.clear();
  		bonusCalcMeth05.clear();
  		bonusCalcMeth06.clear();
  		bonusCalcMeth07.clear();
  		bonusCalcMeth08.clear();
  		bonusCalcMeth09.clear();
  		bonusCalcMeth10.clear();
  		bonPayThisYr01.clear();
  		bonPayThisYr02.clear();
  		bonPayThisYr03.clear();
  		bonPayThisYr04.clear();
  		bonPayThisYr05.clear();
  		bonPayThisYr06.clear();
  		bonPayThisYr07.clear();
  		bonPayThisYr08.clear();
  		bonPayThisYr09.clear();
  		bonPayThisYr10.clear();
  		bonPayLastYr01.clear();
  		bonPayLastYr02.clear();
  		bonPayLastYr03.clear();
  		bonPayLastYr04.clear();
  		bonPayLastYr05.clear();
  		bonPayLastYr06.clear();
  		bonPayLastYr07.clear();
  		bonPayLastYr08.clear();
  		bonPayLastYr09.clear();
  		bonPayLastYr10.clear();
  		totalBonus01.clear();
  		totalBonus02.clear();
  		totalBonus03.clear();
  		totalBonus04.clear();
  		totalBonus05.clear();
  		totalBonus06.clear();
  		totalBonus07.clear();
  		totalBonus08.clear();
  		totalBonus09.clear();
  		totalBonus10.clear();
  		termid.clear();
  		user.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getBonlrec() {
  		return bonlrec;
	}

	public FixedLengthStringData getBonlpfRecord() {
  		return bonlpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setBonlrec(what);
	}

	public void setBonlrec(Object what) {
  		this.bonlrec.set(what);
	}

	public void setBonlpfRecord(Object what) {
  		this.bonlpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(bonlrec.getLength());
		result.set(bonlrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}