package com.csc.life.regularprocessing.dataaccess.model;

import java.io.Serializable;
import java.math.BigDecimal;


public class Bd5lwDTO implements Serializable{


	private String rldgcoy;
	private String rldgacct;
	private String sacscode;
	private String sacstyp;
	private BigDecimal sacscurbal;
	private String chdrcoy;
	private String chdrnum;
	private String cntbranch;
	private String cnttype;
	private String statcode;
	private Integer occdate;
	private Integer ptdate;
	private Short polsum;
	private String cntcurr;

	public String getRldgcoy() {
		return rldgcoy;
	}

	public void setRldgcoy(String rldgcoy) {
		this.rldgcoy = rldgcoy;
	}

	public String getRldgacct() {
		return rldgacct;
	}

	public void setRldgacct(String rldgacct) {
		this.rldgacct = rldgacct;
	}

	public String getSacscode() {
		return sacscode;
	}

	public void setSacscode(String sacscode) {
		this.sacscode = sacscode;
	}

	public String getSacstyp() {
		return sacstyp;
	}

	public void setSacstyp(String sacstyp) {
		this.sacstyp = sacstyp;
	}

	public BigDecimal getSacscurbal() {
		return sacscurbal;
	}

	public void setSacscurbal(BigDecimal sacscurbal) {
		this.sacscurbal = sacscurbal;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getCntbranch() {
		return cntbranch;
	}

	public void setCntbranch(String cntbranch) {
		this.cntbranch = cntbranch;
	}

	public String getCnttype() {
		return cnttype;
	}

	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}

	public String getStatcode() {
		return statcode;
	}

	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	public Integer getOccdate() {
		return occdate;
	}

	public void setOccdate(Integer occdate) {
		this.occdate = occdate;
	}

	public Integer getPtdate() {
		return ptdate;
	}

	public void setPtdate(Integer ptdate) {
		this.ptdate = ptdate;
	}

	public Short getPolsum() {
		return polsum;
	}

	public void setPolsum(Short polsum) {
		this.polsum = polsum;
	}

	public String getCntcurr() {
		return cntcurr;
	}

	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}

}
