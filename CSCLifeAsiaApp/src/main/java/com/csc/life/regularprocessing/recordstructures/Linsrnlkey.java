package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:12
 * Description:
 * Copybook name: LINSRNLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Linsrnlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData linsrnlFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData linsrnlKey = new FixedLengthStringData(64).isAPartOf(linsrnlFileKey, 0, REDEFINE);
  	public FixedLengthStringData linsrnlChdrcoy = new FixedLengthStringData(1).isAPartOf(linsrnlKey, 0);
  	public FixedLengthStringData linsrnlChdrnum = new FixedLengthStringData(8).isAPartOf(linsrnlKey, 1);
  	public PackedDecimalData linsrnlInstfrom = new PackedDecimalData(8, 0).isAPartOf(linsrnlKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(linsrnlKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(linsrnlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		linsrnlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}