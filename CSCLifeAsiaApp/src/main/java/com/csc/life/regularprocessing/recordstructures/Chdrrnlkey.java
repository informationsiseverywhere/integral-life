package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:22
 * Description:
 * Copybook name: CHDRRNLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrrnlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrrnlFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData chdrrnlKey = new FixedLengthStringData(256).isAPartOf(chdrrnlFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrrnlChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrrnlKey, 0);
  	public FixedLengthStringData chdrrnlChdrnum = new FixedLengthStringData(8).isAPartOf(chdrrnlKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(chdrrnlKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrrnlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrrnlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}