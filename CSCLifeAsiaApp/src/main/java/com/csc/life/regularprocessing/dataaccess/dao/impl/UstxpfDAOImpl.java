package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.UstxpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Ustxpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class UstxpfDAOImpl extends BaseDAOImpl<Ustxpf> implements UstxpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(UstxpfDAOImpl.class);
	
	public List<Ustxpf> searchResult(String tableId, String memName, int batchExtractSize, int batchID){
		StringBuilder sqlStr = new StringBuilder();
		sqlStr.append(" SELECT * FROM ( ");
		sqlStr.append(" SELECT CHDRCOY,CHDRNUM,STMDTE,PTDATE,CNTTYPE,STATCODE,PSTCDE,OCCDATE,UNIQUE_NUMBER ");
		sqlStr.append("   ,FLOOR((ROW_NUMBER()OVER( ORDER BY UNIQUE_NUMBER DESC)-1)/?) BATCHNUM ");
		sqlStr.append("  FROM ");
		sqlStr.append(tableId);
		sqlStr.append("   WHERE MEMBER_NAME = ? ");
		sqlStr.append(" ) MAIN WHERE BATCHNUM = ? ");
		PreparedStatement ps = getPrepareStatement(sqlStr.toString());
		ResultSet rs = null;
		List<Ustxpf> ustxpfList = new ArrayList<>();
		try {
			ps.setInt(1, batchExtractSize);
			ps.setString(2, memName);
			ps.setInt(3, batchID);
			rs = executeQuery(ps);
			while (rs.next()) {
				Ustxpf ustxpf = new Ustxpf();
				ustxpf.setChdrcoy(rs.getString("CHDRCOY"));
				ustxpf.setChdrnum(rs.getString("CHDRNUM"));
				ustxpf.setStatementDate(rs.getInt("STMDTE"));
				ustxpf.setPtdate(rs.getInt("PTDATE"));
				ustxpf.setCnttype(rs.getString("CNTTYPE"));
				ustxpf.setStatcode(rs.getString("STATCODE"));
				ustxpf.setPstatcode(rs.getString("PSTCDE"));
				ustxpf.setOccdate(rs.getInt("OCCDATE"));
				ustxpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				ustxpfList.add(ustxpf);
			}
		} catch (SQLException e) {
			LOGGER.error("searchResult()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return ustxpfList;
	}
}
