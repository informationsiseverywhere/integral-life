/*
 * File: Rvbflyr.java
 * Date: 30 August 2009 2:14:02
 * Author: Quipoz Limited
 * 
 * Class transformed from RVBFLYR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.accounting.dataaccess.dao.impl.AcblpfDAOImpl;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.regularprocessing.recordstructures.Bonusrec;
import com.csc.life.regularprocessing.tablestructures.T6638rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsu.accounting.dataaccess.dao.impl.AcblpfDAOImpl;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
/**
* <pre>
*REMARKS.
*
* REVERSIONARY BONUS FULL YEAR
* ----------------------------
*
* RVBFLYR will be called from either B5018 (Company Anniversary)
* or   B5023  (Policy  Anniversary)   batch  jobs  to   allocate
* Reversionary  bonuses  for a full year.  ie.  no proportioning
* necessary.
*
* RVBFLYR will be  called from B5018/B5023 to  calculate the Sum
* Assured Reversionary Bonus due plus the Bonus on Bonus Due. It
* is called using  the linkage BONUSREC.
*
* This program will be called with a Function of CALC or spaces,
* if  SPACES, an ACMV will be  written.  Otherwise,  ACMV's  are
* skipped and the values just returned as calculated values.  If
* Function is neither then perform normal error handling.
*
*
* CALCULATE BONUS.
*
* Read T6638 - Reversionary Bonus Rates with a key of:-
*              BONS-BONUS-CALC-METH
*              BONS-PREM-STATUS
*
* Make sure the Risk Unit on the extra data screen is valid.
*
* Read  ACBL  record  to  get  the  current  Reversionary  Bonus
* allocated.
*
* Calculate the Term in Force for contract to be able to do  the
* Rate look up on T6638.
*
*   Compute Bonus S/A = Sum Assured / T6638-Risk Unit01
*                     * Sum Assured Rate * Bonus Period.
*
*   Compute Bonus Bon = ACBL-Curbal / T6638-Risk Unit02
*                     * Bonus Rate * Bonus Period.
*
*   Compute the Total Bonus = Bonus S/A + Bonus Bon.
*
* WRITE ACMV RECORD.
*
*       The  subroutine LIFACMV  will  be  called  to  add
*       this  ACMV record,  the following key  information
*       will be required.
*
*       LIFA-RDOCNUM           -  BONS-CHDR-CHDRNUM
*       LIFA-RLDGACCT          -  Full Entity Key:-
*                                 Chdrnum       (08).
*                                 Life          (02).
*                                 Coverage      (02).
*                                 Rider         (02).
*                                 Plan-Suffix   (02).
*
*       LIFA-BATCKEY           -  BONS-BATCKEY (Linkage)
*       LIFA-ORIGCURR          -  BONS-CNTCURR (Linkage)
*       LIFA-TRANNO            -  BONS-TRANNO (Linkage)
*       LIFA-JRNSEQ            -  0
*       LIFA-ORIGAMT           -  Total Bonus as Above.
*       LIFA-TRANREF           -  BONS-CHDRNUM
*       LIFA-SACSTYP           -  T5645 Sacstype-02
*       LIFA-SACSCODE          -  T5645 Sacscode-02
*       LIFA-GLCODE            -  T5645 Glcode-02
*       LIFA-GLSIGN            -  T5645 Glsign-02
*       LIFA-CNTTOT            -  T5645 Contot-02
*       LIFA-EFFDATE           -  BONS-EFFDATE (Linkage)
*
* UPDATE COVR RECORD.
*
* The  COVR is  read  and the  CURRFROM/CURRTO dates are updated
* and  the  record  is validflagged as  2.  The new COVR has the
* CURRFROM/CURRTO  dates  updated, as well as the unit statement
* date, and flagged as 1.
*
* UPDATE RETURNED LINKAGE.
*
* Move T6640 values,  Low cost rider/subroutine  to the BONUSREC
* values.  Call the  Bonus Routine  held here  which will reduce
* from the Sum Assured the Bonus Value Allocated.
*
*
*****************************************************************
* </pre>
*/
public class Rvbflyr extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "RVBFLYR";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSumassRate = new ZonedDecimalData(7, 2).setUnsigned();
	private ZonedDecimalData wsaaBonusRate = new ZonedDecimalData(7, 2).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaRvBonusSa = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRvBonusBon = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaBonusSum = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCurbal = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBonusMethod = new FixedLengthStringData(4).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaPremStatus = new FixedLengthStringData(2).isAPartOf(wsaaItem, 4);
		/* ERRORS */
	private static final String g407 = "G407";
	private static final String g550 = "G550";
	private static final String g578 = "G578";
	private static final String g814 = "G814";
		/* FORMATS */
	private static final String acblrec = "ACBLREC";
	private static final String itemrec = "ITEMREC";
	private static final String itdmrec = "ITEMREC";
		/* TABLES */
	private static final String t5645 = "T5645";
	private static final String t6638 = "T6638";
	private AcblTableDAM acblIO = new AcblTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private T5645rec t5645rec = new T5645rec();
	private T6638rec t6638rec = new T6638rec();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Bonusrec bonusrec = new Bonusrec();
	private Map<String, List<Itempf>> T6638ListMap;
	private Map<String, List<Itempf>> t5645ListMap;
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private List<Descpf>  t5645DescList;
	AcblpfDAOImpl objAcbl = new AcblpfDAOImpl();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		continue320, 
		exit390
	}

	public Rvbflyr() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		bonusrec.bonusRec = convertAndSetParam(bonusrec.bonusRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		initialise200();
		readBonusLinkage300();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialise200()
	{
		/*INITIALISE*/
		bonusrec.statuz.set("****");
		wsaaSumassRate.set(ZERO);
		wsaaBonusRate.set(ZERO);
		wsaaBonusSum.set(ZERO);
		wsaaCurbal.set(ZERO);
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		syserrrec.subrname.set(wsaaSubr);
		/*EXIT*/
	}

protected void readBonusLinkage300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					beginReading310();
				case continue320: 
					continue320();
				case exit390: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void beginReading310()
	{
		/*  Check BONS-FUNCTION for CALC or SPACES.*/
		if (isNE(bonusrec.function,"CALC")
		&& isNE(bonusrec.function,SPACES)) {
			syserrrec.params.set(bonusrec.bonusRec);
			syserrrec.statuz.set(g578);
			systemError900();
		}
		/*itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(bonusrec.chdrChdrcoy);
		itdmIO.setItmfrm(bonusrec.effectiveDate);
		itdmIO.setItemtabl(t6638);
		wsaaBonusMethod.set(bonusrec.bonusCalcMeth);
		wsaaPremStatus.set(bonusrec.premStatus);
		itdmIO.setItemitem(wsaaItem);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError800();
		}
		if ((isNE(itdmIO.getItemitem(),wsaaItem))
		|| (isNE(itdmIO.getItemcoy(),bonusrec.chdrChdrcoy))
		|| (isNE(itdmIO.getItemtabl(),t6638))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemitem(wsaaItem);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g407);
			systemError900();
		}
		t6638rec.t6638Rec.set(itdmIO.getGenarea());
		*/
		wsaaBonusMethod.set(bonusrec.bonusCalcMeth);
		wsaaPremStatus.set(bonusrec.premStatus);
		
		T6638ListMap = itemDAO.loadSmartTable("IT", bonusrec.chdrChdrcoy.toString(), t6638);
		String keyItemitem = wsaaItem.toString();
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (T6638ListMap.containsKey(keyItemitem.trim()))
		{	
			itempfList = T6638ListMap.get(keyItemitem.trim());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0)
				{					 
					t6638rec.t6638Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;					 
				}
				else
				{
					t6638rec.t6638Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;					
				}				
			}		
		}
		
		if (!itemFound) {
			itdmIO.setItemitem(wsaaItem);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g407);
			systemError900();
		}
		
		
		/* check table has been set up correctly - must have non-zero*/
		/*  risk units otherwise we will be dividing by zero !!*/
		if (isEQ(t6638rec.riskunit01,ZERO)
		|| isEQ(t6638rec.riskunit02,ZERO)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g550);
			databaseError800();
		}
		/* Check the Years in force for the component.*/
		wsaaSub1.set(1);
		checkYearsInForce400();
		//itemIO.setDataArea(SPACES);
		//itemIO.setItempfx("IT");
		//itemIO.setItemcoy(bonusrec.chdrChdrcoy);
		//itemIO.setItemtabl(t5645);
		//itemIO.setItemitem(wsaaSubr);
		//itemIO.setFormat(itemrec);
		//itemIO.setFunction(varcom.readr);
		//SmartFileCode.execute(appVars, itemIO);
		//if (isNE(itemIO.getStatuz(),varcom.oK)) {
			//syserrrec.params.set(itemIO.getParams());
			//syserrrec.statuz.set(itemIO.getStatuz());
			//databaseError800();
		//}
		//t5645rec.t5645Rec.set(itemIO.getGenarea());
		
		
		t5645ListMap = itemDAO.loadSmartTable("IT", bonusrec.chdrChdrcoy.toString(), t5645);
		keyItemitem = wsaaSubr;/* IJTI-1523 */
		List<Itempf> itempfLst = new ArrayList<Itempf>();
		itemFound = false;
		if (t5645ListMap.containsKey(keyItemitem.trim()))
		{	
			itempfLst = t5645ListMap.get(keyItemitem.trim());
			Iterator<Itempf> iterator = itempfLst.iterator();
			while (iterator.hasNext()) {
				
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0)
				{					 
					t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;					 
				}
				else
				{
					t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;					
				}				
			}		
		}
		
		if (!itemFound) 
		{
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError800();
		}
		
		
		/* Read ACBL for suspense balance.*/
		acblIO.setParams(SPACES);
		wsaaRldgChdrnum.set(bonusrec.chdrChdrnum);
		wsaaRldgLife.set(bonusrec.lifeLife);
		wsaaRldgCoverage.set(bonusrec.covrCoverage);
		wsaaRldgRider.set(bonusrec.covrRider);
		wsaaPlan.set(bonusrec.plnsfx);
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		
		//acblIO.setRldgacct(wsaaRldgacct);
		//acblIO.setRldgcoy(bonusrec.chdrChdrcoy);
		//acblIO.setOrigcurr(bonusrec.cntcurr);		
		//acblIO.setSacscode(t5645rec.sacscode01);
		//acblIO.setSacstyp(t5645rec.sacstype01);
		//acblIO.setFunction(varcom.readr);
		//acblIO.setFormat(acblrec);
		//SmartFileCode.execute(appVars, acblIO);
		//if ((isNE(acblIO.getStatuz(),varcom.oK))
		//&& (isNE(acblIO.getStatuz(),varcom.mrnf))) {
			//syserrrec.params.set(acblIO.getParams());
			//syserrrec.statuz.set(acblIO.getStatuz());
			//databaseError800();
		//}
		//if (isEQ(acblIO.getStatuz(),varcom.mrnf)) {
			//wsaaCurbal.set(ZERO);
		////}
		//else {
			//wsaaCurbal.set(acblIO.getSacscurbal());
		//}
		
		
		Acblpf acblpfData= new Acblpf();
		acblpfData.setRldgacct(wsaaRldgacct.toString().trim());
		acblpfData.setRldgcoy(bonusrec.chdrChdrcoy.toString().trim());
		acblpfData.setSacscode(t5645rec.sacscode01.toString().trim());
		acblpfData.setSacstyp(t5645rec.sacstype01.toString().trim());
		acblpfData.setOrigcurr(bonusrec.cntcurr.toString().trim());
		List<Acblpf> acbl=objAcbl.selectAcblData(acblpfData);
		wsaaCurbal.set(ZERO);
		if (acbl!=null && acbl.size()>0)
		{
			for (Acblpf objAc : acbl) 
			{
				wsaaCurbal.set(objAc.getSacscurbal());
				break;
			}
		}
		else
		{
			wsaaCurbal.set(ZERO);
		}
		
		
		/* Calculate the sum assured and bonus bonuses.*/
		bonusCalculations500();
		if (isEQ(bonusrec.function,"CALC")) {
			goTo(GotoLabel.exit390);
		}
		/* Add up both bonuses for lifa-origamt.*/
		compute(wsaaBonusSum, 3).setRounded((add(bonusrec.rvBonusSa,bonusrec.rvBonusBon)));
		bonusrec.rvBalLy.set(wsaaBonusSum);
		if (isEQ(wsaaBonusSum,ZERO)) {
			goTo(GotoLabel.continue320);
		}
		/* Set up lifacmv fields.*/
		lifacmvrec1.lifacmvRec.set(SPACES);
		lifacmvrec1.rdocnum.set(bonusrec.chdrChdrnum);
		lifacmvrec1.jrnseq.set(0);
		lifacmvrec1.rldgcoy.set(bonusrec.chdrChdrcoy);
		lifacmvrec1.genlcoy.set(bonusrec.chdrChdrcoy);
		lifacmvrec1.batckey.set(bonusrec.batckey);
		lifacmvrec1.origcurr.set(bonusrec.cntcurr);
		lifacmvrec1.origamt.set(wsaaBonusSum);
		lifacmvrec1.rcamt.set(0);
		lifacmvrec1.crate.set(0);
		lifacmvrec1.acctamt.set(0);
		lifacmvrec1.contot.set(0);
		lifacmvrec1.tranno.set(bonusrec.tranno);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.effdate.set(bonusrec.effectiveDate);
		lifacmvrec1.tranref.set(bonusrec.chdrChdrnum);
		lifacmvrec1.substituteCode[1].set(bonusrec.cnttype);
		lifacmvrec1.substituteCode[6].set(bonusrec.crtable);
		/* Get item description.*/
		//descIO.setParams(SPACES);
		//descIO.setDescpfx("IT");
		//descIO.setDesccoy(bonusrec.chdrChdrcoy);
		//descIO.setDesctabl(t5645);
		//descIO.setDescitem(wsaaSubr);
		//descIO.setLanguage(bonusrec.language);
		//descIO.setFunction(varcom.readr);
		//SmartFileCode.execute(appVars, descIO);
		//if (isNE(descIO.getStatuz(),varcom.oK)) {
			//descIO.setLongdesc(SPACES);
		//}
		
		t5645DescList = descDAO.getItemByDescItem("IT", bonusrec.chdrChdrcoy.toString(), t5645,wsaaSubr.trim(),bonusrec.language.toString());/* IJTI-1523 */		
		boolean DescitemFound = false;
		for (Descpf descItem : t5645DescList ) {
			if (descItem.getDescitem().trim().equals(wsaaSubr.trim())  )/* IJTI-1523 */
			{
				DescitemFound=true;
				lifacmvrec1.trandesc.set(descItem.getLongdesc());
				break;
			}
		}
		
		//lifacmvrec1.trandesc.set(descIO.getLongdesc());
		wsaaRldgChdrnum.set(bonusrec.chdrChdrnum);
		wsaaRldgLife.set(bonusrec.lifeLife);
		wsaaRldgCoverage.set(bonusrec.covrCoverage);
		wsaaRldgRider.set(bonusrec.covrRider);
		wsaaPlan.set(bonusrec.plnsfx);
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec1.rldgacct.set(wsaaRldgacct);
		lifacmvrec1.transactionDate.set(varcom.vrcmDate);
		lifacmvrec1.transactionTime.set(varcom.vrcmTime);
		lifacmvrec1.user.set(bonusrec.user);
		lifacmvrec1.sacscode.set(t5645rec.sacscode02);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec1.glcode.set(t5645rec.glmap02);
		lifacmvrec1.glsign.set(t5645rec.sign02);
		lifacmvrec1.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			databaseError800();
		}
	}

protected void continue320()
	{
		continue321();
		
		/* Set CURRTO date to a day behind today's date.*/
		/* This is no longer true                                       */
		/* CURRTO is now set to EFFECTIVE DATE                          */
		/* MOVE -1                     TO DTC2-FREQ-FACTOR.             */
		/* MOVE 'DY'                   TO DTC2-FREQUENCY.               */
		/* MOVE BONS-EFFECTIVE-DATE    TO DTC2-INT-DATE-1.              */
		/* CALL 'DATCON2'              USING DTC2-DATCON2-REC.          */
		/* IF DTC2-STATUZ              NOT = O-K                        */
		/*    MOVE DTC2-STATUZ         TO SYSR-STATUZ                   */
		/*    PERFORM 800-DATABASE-ERROR                                */
		/* ELSE                                                         */
		/*    MOVE DTC2-INT-DATE-2     TO COVRBON-CURRTO                */
		/* END-IF.                                                      */
		/* Rewrt old COVRBON record.*/
		
		/* If we are doing a Low Cost Endowment component, then see if*/
		/*   we need to call subroutine specified in BONS-LOW-COST-SUBR*/
		if (isEQ(bonusrec.lowCostRider,SPACES)
		|| isEQ(bonusrec.lowCostSubr,SPACES)) {
			return ;
		}
		if ((isNE(bonusrec.lowCostRider,SPACES)
		&& isNE(bonusrec.lowCostSubr,SPACES))) {
			callProgram(bonusrec.lowCostSubr, bonusrec.bonusRec);
		}
		if ((isNE(bonusrec.statuz,varcom.oK))) {
			syserrrec.params.set(bonusrec.bonusRec);
			syserrrec.statuz.set(bonusrec.statuz);
			systemError900();
		}
	}
private void continue321(){
		Covrpf covrpfbon = covrpfDAO.getCovrRecord(bonusrec.chdrChdrcoy.toString(), bonusrec.chdrChdrnum.toString(), 
				bonusrec.lifeLife.toString(), bonusrec.covrCoverage.toString(), bonusrec.covrRider.toString(), bonusrec.plnsfx.toInt(), "1");
		if (covrpfbon!=null) {
			if(covrpfbon.getUniqueNumber()<=0){
				syserrrec.params.set(bonusrec.chdrChdrnum.toString());
				databaseError800();
			}
			covrpfbon.setCurrto(bonusrec.effectiveDate.toInt());
			covrpfbon.setValidflag("2");
			boolean updateCov = covrpfDAO.updateCovrValidAndCurrtoFlag(covrpfbon);
			if(!updateCov){
				syserrrec.params.set(bonusrec.chdrChdrnum.toString());
				databaseError800();
			}
			
			/* Write new COVR record.*/
			covrpfbon.setValidflag("1");
			covrpfbon.setTranno(bonusrec.tranno.toInt());
			covrpfbon.setCurrfrom(bonusrec.effectiveDate.toInt());
			covrpfbon.setUnitStatementDate(bonusrec.effectiveDate.toInt());
			covrpfbon.setCurrto(varcom.vrcmMaxDate.toInt());
			covrpfDAO.insertCovrRecord(covrpfbon);
			
		}
		
}
protected void checkYearsInForce400()
	{
		/*START*/
		/* find out which row on the screen S6638 corresponds to*/
		/*  the BONS-TERM-IN-FORCE value.*/
		while ( !(isGT(wsaaSub1,10))) {
			if ((isGT(bonusrec.termInForce,t6638rec.yrsinf[wsaaSub1.toInt()]))) {
				wsaaSub1.add(1);
			}
			else {
				wsaaSumassRate.set(t6638rec.sumass[wsaaSub1.toInt()]);
				wsaaBonusRate.set(t6638rec.bonus[wsaaSub1.toInt()]);
				return ;
			}
		}
		
		/*CHECK*/
		syserrrec.params.set(bonusrec.bonusRec);
		syserrrec.statuz.set(g814);
		databaseError800();
	}

protected void bonusCalculations500()
	{
		/*START*/
		compute(bonusrec.rvBonusSa, 6).setRounded(mult(mult((div(bonusrec.sumin,t6638rec.riskunit01)),wsaaSumassRate),bonusrec.bonusPeriod));
		zrdecplrec.amountIn.set(bonusrec.rvBonusSa);
		a000CallRounding();
		bonusrec.rvBonusSa.set(zrdecplrec.amountOut);
		compute(bonusrec.rvBonusBon, 6).setRounded(mult(mult((div(wsaaCurbal,t6638rec.riskunit02)),wsaaBonusRate),bonusrec.bonusPeriod));
		zrdecplrec.amountIn.set(bonusrec.rvBonusBon);
		a000CallRounding();
		bonusrec.rvBonusBon.set(zrdecplrec.amountOut);
		/*EXIT*/
	}

protected void databaseError800()
	{
					start810();
					exit870();
				}

protected void start810()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit870()
	{
		bonusrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void systemError900()
	{
					start910();
					exit970();
				}

protected void start910()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit970()
	{
		bonusrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bonusrec.chdrChdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(bonusrec.cntcurr);
		zrdecplrec.batctrcde.set(bonusrec.transcd);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError900();
		}
		/*A900-EXIT*/
	}
}
