package com.csc.life.regularprocessing.dataaccess.model;

import java.math.BigDecimal;


public class B5018DTO {
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int plnsfx;
	private String crtable;
	private String pstatcode;
	private String statcode;
	private int crrcd;
	private int cbunst;
	private int rcesdte;
	private BigDecimal sumins;
	private String validflag;
	private int currfrom;
	private int currto;
	private String cntcurr;
	private String cnttype;
	private int tranno;
	private String cowncoy;
	private String cownnum;
	private String jobnm;
	private String usrprf; 
	private String cntbranch;
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(int plnsfx) {
		this.plnsfx = plnsfx;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public int getCrrcd() {
		return crrcd;
	}
	public void setCrrcd(int crrcd) {
		this.crrcd = crrcd;
	}
	public int getCbunst() {
		return cbunst;
	}
	public void setCbunst(int cbunst) {
		this.cbunst = cbunst;
	}
	public int getRcesdte() {
		return rcesdte;
	}
	public void setRcesdte(int rcesdte) {
		this.rcesdte = rcesdte;
	}
	public BigDecimal getSumins() {
		return sumins;
	}
	public void setSumins(BigDecimal sumins) {
		this.sumins = sumins;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public int getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(int currfrom) {
		this.currfrom = currfrom;
	}
	public int getCurrto() {
		return currto;
	}
	public void setCurrto(int currto) {
		this.currto = currto;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getCowncoy() {
		return cowncoy;
	}
	public void setCowncoy(String cowncoy) {
		this.cowncoy = cowncoy;
	}
	public String getCownnum() {
		return cownnum;
	}
	public void setCownnum(String cownnum) {
		this.cownnum = cownnum;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(String cntbranch) {
		this.cntbranch = cntbranch;
	}
	
	
}
