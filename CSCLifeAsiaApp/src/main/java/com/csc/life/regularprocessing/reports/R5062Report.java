package com.csc.life.regularprocessing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5062.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5062Report extends SMARTReportLayout { 

	private FixedLengthStringData batctrcde = new FixedLengthStringData(4);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData effdate = new FixedLengthStringData(10);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData repdate = new FixedLengthStringData(10);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();
	private FixedLengthStringData trandesc = new FixedLengthStringData(30);
	private ZonedDecimalData tranno = new ZonedDecimalData(5, 0);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5062Report() {
		super();
	}


	/**
	 * Print the XML for R5062d01
	 */
	public void printR5062d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		batctrcde.setFieldName("batctrcde");
		batctrcde.setInternal(subString(recordData, 9, 4));
		trandesc.setFieldName("trandesc");
		trandesc.setInternal(subString(recordData, 13, 30));
		tranno.setFieldName("tranno");
		tranno.setInternal(subString(recordData, 43, 5));
		effdate.setFieldName("effdate");
		effdate.setInternal(subString(recordData, 48, 10));
		printLayout("R5062d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				batctrcde,
				trandesc,
				tranno,
				effdate
			}
		);

	}

	/**
	 * Print the XML for R5062d02
	 */
	public void printR5062d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(4);

		printLayout("R5062d02",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for R5062h01
	 */
	public void printR5062h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 1, 10));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 11, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 12, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 42, 10));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("R5062h01",			// Record name
			new BaseData[]{			// Fields:
				repdate,
				company,
				companynm,
				sdate,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(11);
	}


}
