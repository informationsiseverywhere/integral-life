package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:18
 * Description:
 * Copybook name: UBBLALLPAR
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ubblallpar extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData ubblallRec = new FixedLengthStringData(547);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(ubblallRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(ubblallRec, 5);
  	public FixedLengthStringData ridrkey = new FixedLengthStringData(17).isAPartOf(ubblallRec, 9);
  	public FixedLengthStringData chdrChdrcoy = new FixedLengthStringData(1).isAPartOf(ridrkey, 0);
  	public FixedLengthStringData chdrChdrnum = new FixedLengthStringData(8).isAPartOf(ridrkey, 1);
  	public FixedLengthStringData lifeLife = new FixedLengthStringData(2).isAPartOf(ridrkey, 9);
  	public FixedLengthStringData lifeJlife = new FixedLengthStringData(2).isAPartOf(ridrkey, 11);
  	public FixedLengthStringData covrCoverage = new FixedLengthStringData(2).isAPartOf(ridrkey, 13);
  	public FixedLengthStringData covrRider = new FixedLengthStringData(2).isAPartOf(ridrkey, 15);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(ubblallRec, 26);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(ubblallRec, 29);
  	public ZonedDecimalData user = new ZonedDecimalData(6, 0).isAPartOf(ubblallRec, 30).setUnsigned();
  	public FixedLengthStringData batccoy = new FixedLengthStringData(1).isAPartOf(ubblallRec, 36);
  	public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(ubblallRec, 37);
  	public ZonedDecimalData batcactyr = new ZonedDecimalData(4, 0).isAPartOf(ubblallRec, 39).setUnsigned();
  	public ZonedDecimalData batcactmn = new ZonedDecimalData(2, 0).isAPartOf(ubblallRec, 43).setUnsigned();
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(ubblallRec, 45);
  	public FixedLengthStringData batch = new FixedLengthStringData(5).isAPartOf(ubblallRec, 49);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(ubblallRec, 54);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(ubblallRec, 59);
  	public ZonedDecimalData billfreq = new ZonedDecimalData(2, 0).isAPartOf(ubblallRec, 62).setUnsigned();
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(ubblallRec, 64);
  	public ZonedDecimalData sumins = new ZonedDecimalData(17, 2).isAPartOf(ubblallRec, 67);
  	public FixedLengthStringData premMeth = new FixedLengthStringData(10).isAPartOf(ubblallRec, 84);
  	public FixedLengthStringData jlifePremMeth = new FixedLengthStringData(10).isAPartOf(ubblallRec, 94);
  	public ZonedDecimalData premCessDate = new ZonedDecimalData(8, 0).isAPartOf(ubblallRec, 104).setUnsigned();
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(ubblallRec, 112);
  	public FixedLengthStringData billchnl = new FixedLengthStringData(2).isAPartOf(ubblallRec, 116);
  	public FixedLengthStringData mortcls = new FixedLengthStringData(1).isAPartOf(ubblallRec, 118);
  	public FixedLengthStringData svMethod = new FixedLengthStringData(4).isAPartOf(ubblallRec, 119);
  	public ZonedDecimalData tranno = new ZonedDecimalData(5, 0).isAPartOf(ubblallRec, 123).setUnsigned();
  	public FixedLengthStringData adfeemth = new FixedLengthStringData(4).isAPartOf(ubblallRec, 128);
  	public PackedDecimalData polsum = new PackedDecimalData(4, 0).isAPartOf(ubblallRec, 132);
  	public PackedDecimalData ptdate = new PackedDecimalData(8, 0).isAPartOf(ubblallRec, 135);
  	public PackedDecimalData polinc = new PackedDecimalData(4, 0).isAPartOf(ubblallRec, 140);
  	public PackedDecimalData singp = new PackedDecimalData(17, 2).isAPartOf(ubblallRec, 143);
  	public FixedLengthStringData filler = new FixedLengthStringData(3).isAPartOf(ubblallRec, 152, FILLER);
  	public FixedLengthStringData premsubr = new FixedLengthStringData(7).isAPartOf(ubblallRec, 155);
  	public FixedLengthStringData jpremsubr = new FixedLengthStringData(7).isAPartOf(ubblallRec, 162);
  	public FixedLengthStringData trandesc = new FixedLengthStringData(30).isAPartOf(ubblallRec, 169);
  	public FixedLengthStringData svCalcprog = new FixedLengthStringData(7).isAPartOf(ubblallRec, 199);
  	public FixedLengthStringData comlvlacc = new FixedLengthStringData(1).isAPartOf(ubblallRec, 206);
  	public ZonedDecimalData occdate = new ZonedDecimalData(8, 0).isAPartOf(ubblallRec, 207).setUnsigned();
  	public FixedLengthStringData occdtex = new FixedLengthStringData(8).isAPartOf(occdate, 0, REDEFINE);
  	//cluster support by vhukumagrawa
  	public PackedDecimalData threadNumber = new PackedDecimalData(3, 0).isAPartOf(ubblallRec, 215);
  	public FixedLengthStringData chdrRegister = new FixedLengthStringData(3).isAPartOf(ubblallRec, 217);
  	//ILIFE-7442
  	public FixedLengthStringData polcurr = new FixedLengthStringData(3).isAPartOf(ubblallRec, 220);
  	public FixedLengthStringData benfunc = new FixedLengthStringData(5).isAPartOf(ubblallRec, 223);
  	public ZonedDecimalData firstPrem = new ZonedDecimalData(17, 2).isAPartOf(ubblallRec, 228); //ALS-4706
  	public ZonedDecimalData curUnitBal = new ZonedDecimalData(17, 2).isAPartOf(ubblallRec, 245);//ALS-4706
  	public ZonedDecimalData unitBidPrice = new ZonedDecimalData(17, 2).isAPartOf(ubblallRec, 262);//ALS-4706
  	public FixedLengthStringData cntAdvisorFessReq = new  FixedLengthStringData(1).isAPartOf(ubblallRec, 279);
	public ZonedDecimalData nrFunds = new ZonedDecimalData(3).isAPartOf(ubblallRec, 280);
	public ZonedDecimalData sbillfreq = new ZonedDecimalData(2, 0).isAPartOf(ubblallRec, 283).setUnsigned();
  	
	public FixedLengthStringData curdunitbals = new FixedLengthStringData(160).isAPartOf(ubblallRec, 285);
  	public ZonedDecimalData[] curdunitbal = ZDArrayPartOfStructure(10, 16, 5, curdunitbals, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(160).isAPartOf(curdunitbals, 0, FILLER_REDEFINE);
  	public ZonedDecimalData curdunitbal01 = new ZonedDecimalData(16,5).isAPartOf(filler2, 0);
  	public ZonedDecimalData curdunitbal02 = new ZonedDecimalData(16,5).isAPartOf(filler2, 16);
  	public ZonedDecimalData curdunitbal03 = new ZonedDecimalData(16,5).isAPartOf(filler2, 32);
  	public ZonedDecimalData curdunitbal04 = new ZonedDecimalData(16,5).isAPartOf(filler2, 48);
  	public ZonedDecimalData curdunitbal05 = new ZonedDecimalData(16,5).isAPartOf(filler2, 64);
  	public ZonedDecimalData curdunitbal06 = new ZonedDecimalData(16,5).isAPartOf(filler2, 80);
  	public ZonedDecimalData curdunitbal07 = new ZonedDecimalData(16,5).isAPartOf(filler2, 96);
  	public ZonedDecimalData curdunitbal08 = new ZonedDecimalData(16,5).isAPartOf(filler2, 112);
  	public ZonedDecimalData curdunitbal09 = new ZonedDecimalData(16,5).isAPartOf(filler2, 128);
  	public ZonedDecimalData curdunitbal10 = new ZonedDecimalData(16,5).isAPartOf(filler2, 144);
  	
  	public FixedLengthStringData curdunitprices = new FixedLengthStringData(90).isAPartOf(ubblallRec, 445);
  	public ZonedDecimalData[] curdunitprice = ZDArrayPartOfStructure(10, 9, 5, curdunitprices, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(9).isAPartOf(curdunitprices, 0, FILLER_REDEFINE);
  	public ZonedDecimalData curdunitprice01 = new ZonedDecimalData(9,5).isAPartOf(filler1, 0);
  	public ZonedDecimalData curdunitprice02 = new ZonedDecimalData(9,5).isAPartOf(filler1, 9);
  	public ZonedDecimalData curdunitprice03 = new ZonedDecimalData(9,5).isAPartOf(filler1, 18);
  	public ZonedDecimalData curdunitprice04 = new ZonedDecimalData(9,5).isAPartOf(filler1, 27);
  	public ZonedDecimalData curdunitprice05 = new ZonedDecimalData(9,5).isAPartOf(filler1, 36);
  	public ZonedDecimalData curdunitprice06 = new ZonedDecimalData(9,5).isAPartOf(filler1, 45);
  	public ZonedDecimalData curdunitprice07 = new ZonedDecimalData(9,5).isAPartOf(filler1, 54);
  	public ZonedDecimalData curdunitprice08 = new ZonedDecimalData(9,5).isAPartOf(filler1, 63);
  	public ZonedDecimalData curdunitprice09 = new ZonedDecimalData(9,5).isAPartOf(filler1, 72);
  	public ZonedDecimalData curdunitprice10 = new ZonedDecimalData(9,5).isAPartOf(filler1, 81);
  	
  	public ZonedDecimalData znofdue = new ZonedDecimalData(4).isAPartOf(ubblallRec, 535);
  	public PackedDecimalData batchrundate = new PackedDecimalData(8, 0).isAPartOf(ubblallRec, 539);

	public void initialize() {
		COBOLFunctions.initialize(ubblallRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ubblallRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}