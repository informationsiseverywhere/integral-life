package com.csc.life.regularprocessing.dataaccess.model;

import java.math.BigDecimal;

public class Ainrpf{

    private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private String crtable;
	private BigDecimal rd01Oldsum;
	private BigDecimal newsumi;
	private BigDecimal oldinst;
	private BigDecimal newinst;
	private int planSuffix;
	private int cmdate;
	private String rasnum;
	private String rngmnt;
	private BigDecimal raAmount;
	private int riskCessDate;
	private String currency;
	private String aintype;
	private String userProfile;
	private String jobName;
	private String datime;
    public long getUniqueNumber() {
        return uniqueNumber;
    }
    public void setUniqueNumber(long uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }
    public String getChdrcoy() {
        return chdrcoy;
    }
    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }
    public String getChdrnum() {
        return chdrnum;
    }
    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }
    public String getLife() {
        return life;
    }
    public void setLife(String life) {
        this.life = life;
    }
    public String getCoverage() {
        return coverage;
    }
    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }
    public String getRider() {
        return rider;
    }
    public void setRider(String rider) {
        this.rider = rider;
    }
    public String getCrtable() {
        return crtable;
    }
    public void setCrtable(String crtable) {
        this.crtable = crtable;
    }
    public BigDecimal getRd01Oldsum() {
        return rd01Oldsum;
    }
    public void setRd01Oldsum(BigDecimal rd01Oldsum) {
        this.rd01Oldsum = rd01Oldsum;
    }
    public BigDecimal getNewsumi() {
        return newsumi;
    }
    public void setNewsumi(BigDecimal newsumi) {
        this.newsumi = newsumi;
    }
    public BigDecimal getOldinst() {
        return oldinst;
    }
    public void setOldinst(BigDecimal oldinst) {
        this.oldinst = oldinst;
    }
    public BigDecimal getNewinst() {
        return newinst;
    }
    public void setNewinst(BigDecimal newinst) {
        this.newinst = newinst;
    }
    public int getPlanSuffix() {
        return planSuffix;
    }
    public void setPlanSuffix(int planSuffix) {
        this.planSuffix = planSuffix;
    }
    public int getCmdate() {
        return cmdate;
    }
    public void setCmdate(int cmdate) {
        this.cmdate = cmdate;
    }
    public String getRasnum() {
        return rasnum;
    }
    public void setRasnum(String rasnum) {
        this.rasnum = rasnum;
    }
    public String getRngmnt() {
        return rngmnt;
    }
    public void setRngmnt(String rngmnt) {
        this.rngmnt = rngmnt;
    }
    public BigDecimal getRaAmount() {
        return raAmount;
    }
    public void setRaAmount(BigDecimal raAmount) {
        this.raAmount = raAmount;
    }
    public int getRiskCessDate() {
        return riskCessDate;
    }
    public void setRiskCessDate(int riskCessDate) {
        this.riskCessDate = riskCessDate;
    }
    public String getCurrency() {
        return currency;
    }
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    public String getAintype() {
        return aintype;
    }
    public void setAintype(String aintype) {
        this.aintype = aintype;
    }
    public String getUserProfile() {
        return userProfile;
    }
    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }
    public String getJobName() {
        return jobName;
    }
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
    public String getDatime() {
        return datime;
    }
    public void setDatime(String datime) {
        this.datime = datime;
    }
}