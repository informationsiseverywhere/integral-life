package com.csc.life.regularprocessing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: IncrrefTableDAM.java
 * Date: Sun, 30 Aug 2009 03:41:52
 * Class transformed from INCRREF.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class IncrrefTableDAM extends IncrpfTableDAM {

	public IncrrefTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("INCRREF");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", PLNSFX"
		             + ", CRRCD";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "JLIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "TRANNO, " +
		            "VALIDFLAG, " +
		            "STATCODE, " +
		            "PSTATCODE, " +
		            "CRRCD, " +
		            "CRTABLE, " +
		            "ORIGINST, " +
		            "LASTINST, " +
		            "NEWINST, " +
		            "ORIGSUM, " +
		            "LASTSUM, " +
		            "NEWSUM, " +
		            "ANNVRY, " +
		            "BASCMETH, " +
		            "BASCPY, " +
		            "PCTINC, " +
		            "REFFLAG, " +
		            "CEASEIND, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "PLNSFX ASC, " +
		            "CRRCD DESC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "PLNSFX DESC, " +
		            "CRRCD ASC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               jlife,
                               coverage,
                               rider,
                               planSuffix,
                               tranno,
                               validflag,
                               statcode,
                               pstatcode,
                               crrcd,
                               crtable,
                               origInst,
                               lastInst,
                               newinst,
                               origSum,
                               lastSum,
                               newsum,
                               anniversaryMethod,
                               basicCommMeth,
                               bascpy,
                               pctinc,
                               refusalFlag,
                               ceaseInd,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(41);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ getCrrcd().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, crrcd);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller7 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller12 = new FixedLengthStringData(5);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(life.toInternal());
	nonKeyFiller5.setInternal(coverage.toInternal());
	nonKeyFiller6.setInternal(rider.toInternal());
	nonKeyFiller7.setInternal(planSuffix.toInternal());
	nonKeyFiller12.setInternal(crrcd.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(167);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ getJlife().toInternal()
					+ nonKeyFiller5.toInternal()
					+ nonKeyFiller6.toInternal()
					+ nonKeyFiller7.toInternal()
					+ getTranno().toInternal()
					+ getValidflag().toInternal()
					+ getStatcode().toInternal()
					+ getPstatcode().toInternal()
					+ nonKeyFiller12.toInternal()
					+ getCrtable().toInternal()
					+ getOrigInst().toInternal()
					+ getLastInst().toInternal()
					+ getNewinst().toInternal()
					+ getOrigSum().toInternal()
					+ getLastSum().toInternal()
					+ getNewsum().toInternal()
					+ getAnniversaryMethod().toInternal()
					+ getBasicCommMeth().toInternal()
					+ getBascpy().toInternal()
					+ getPctinc().toInternal()
					+ getRefusalFlag().toInternal()
					+ getCeaseInd().toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, nonKeyFiller7);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, nonKeyFiller12);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, origInst);
			what = ExternalData.chop(what, lastInst);
			what = ExternalData.chop(what, newinst);
			what = ExternalData.chop(what, origSum);
			what = ExternalData.chop(what, lastSum);
			what = ExternalData.chop(what, newsum);
			what = ExternalData.chop(what, anniversaryMethod);
			what = ExternalData.chop(what, basicCommMeth);
			what = ExternalData.chop(what, bascpy);
			what = ExternalData.chop(what, pctinc);
			what = ExternalData.chop(what, refusalFlag);
			what = ExternalData.chop(what, ceaseInd);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	public PackedDecimalData getCrrcd() {
		return crrcd;
	}
	public void setCrrcd(Object what) {
		setCrrcd(what, false);
	}
	public void setCrrcd(Object what, boolean rounded) {
		if (rounded)
			crrcd.setRounded(what);
		else
			crrcd.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public PackedDecimalData getOrigInst() {
		return origInst;
	}
	public void setOrigInst(Object what) {
		setOrigInst(what, false);
	}
	public void setOrigInst(Object what, boolean rounded) {
		if (rounded)
			origInst.setRounded(what);
		else
			origInst.set(what);
	}	
	public PackedDecimalData getLastInst() {
		return lastInst;
	}
	public void setLastInst(Object what) {
		setLastInst(what, false);
	}
	public void setLastInst(Object what, boolean rounded) {
		if (rounded)
			lastInst.setRounded(what);
		else
			lastInst.set(what);
	}	
	public PackedDecimalData getNewinst() {
		return newinst;
	}
	public void setNewinst(Object what) {
		setNewinst(what, false);
	}
	public void setNewinst(Object what, boolean rounded) {
		if (rounded)
			newinst.setRounded(what);
		else
			newinst.set(what);
	}	
	public PackedDecimalData getOrigSum() {
		return origSum;
	}
	public void setOrigSum(Object what) {
		setOrigSum(what, false);
	}
	public void setOrigSum(Object what, boolean rounded) {
		if (rounded)
			origSum.setRounded(what);
		else
			origSum.set(what);
	}	
	public PackedDecimalData getLastSum() {
		return lastSum;
	}
	public void setLastSum(Object what) {
		setLastSum(what, false);
	}
	public void setLastSum(Object what, boolean rounded) {
		if (rounded)
			lastSum.setRounded(what);
		else
			lastSum.set(what);
	}	
	public PackedDecimalData getNewsum() {
		return newsum;
	}
	public void setNewsum(Object what) {
		setNewsum(what, false);
	}
	public void setNewsum(Object what, boolean rounded) {
		if (rounded)
			newsum.setRounded(what);
		else
			newsum.set(what);
	}	
	public FixedLengthStringData getAnniversaryMethod() {
		return anniversaryMethod;
	}
	public void setAnniversaryMethod(Object what) {
		anniversaryMethod.set(what);
	}	
	public FixedLengthStringData getBasicCommMeth() {
		return basicCommMeth;
	}
	public void setBasicCommMeth(Object what) {
		basicCommMeth.set(what);
	}	
	public FixedLengthStringData getBascpy() {
		return bascpy;
	}
	public void setBascpy(Object what) {
		bascpy.set(what);
	}	
	public PackedDecimalData getPctinc() {
		return pctinc;
	}
	public void setPctinc(Object what) {
		setPctinc(what, false);
	}
	public void setPctinc(Object what, boolean rounded) {
		if (rounded)
			pctinc.setRounded(what);
		else
			pctinc.set(what);
	}	
	public FixedLengthStringData getRefusalFlag() {
		return refusalFlag;
	}
	public void setRefusalFlag(Object what) {
		refusalFlag.set(what);
	}	
	public FixedLengthStringData getCeaseInd() {
		return ceaseInd;
	}
	public void setCeaseInd(Object what) {
		ceaseInd.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		crrcd.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		jlife.clear();
		nonKeyFiller5.clear();
		nonKeyFiller6.clear();
		nonKeyFiller7.clear();
		tranno.clear();
		validflag.clear();
		statcode.clear();
		pstatcode.clear();
		nonKeyFiller12.clear();
		crtable.clear();
		origInst.clear();
		lastInst.clear();
		newinst.clear();
		origSum.clear();
		lastSum.clear();
		newsum.clear();
		anniversaryMethod.clear();
		basicCommMeth.clear();
		bascpy.clear();
		pctinc.clear();
		refusalFlag.clear();
		ceaseInd.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}