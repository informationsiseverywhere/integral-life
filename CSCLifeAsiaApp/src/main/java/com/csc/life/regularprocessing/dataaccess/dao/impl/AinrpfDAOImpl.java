package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.AinrpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Ainrpf;
import com.csc.life.regularprocessing.dataaccess.model.Covipf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AinrpfDAOImpl extends BaseDAOImpl<Ainrpf> implements AinrpfDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AinrpfDAOImpl.class);

	@Override
	public void insertAinrpfList(List<Ainrpf> ainrpfList) {
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO AINRPF(CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, CRTABLE, OLDSUM,NEWSUMI,OLDINST,NEWINST,PLNSFX,CMDATE,RASNUM,RNGMNT,RAAMOUNT,RCESDTE,CURRENCY,AINTYPE, USRPRF, JOBNM, DATIME) "); 
		sb.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) "); 
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			int i = 1;
			for(Ainrpf ainrpf : ainrpfList) {
			    i = 1;
			    ps.setString(i++, ainrpf.getChdrcoy());
			    ps.setString(i++, ainrpf.getChdrnum());
			    ps.setString(i++, ainrpf.getLife());
			    ps.setString(i++, ainrpf.getCoverage());
			    ps.setString(i++, ainrpf.getRider());
			    ps.setString(i++, ainrpf.getCrtable());
			    ps.setBigDecimal(i++, ainrpf.getRd01Oldsum());
			    ps.setBigDecimal(i++, ainrpf.getNewsumi());
			    ps.setBigDecimal(i++, ainrpf.getOldinst());
			    ps.setBigDecimal(i++, ainrpf.getNewinst());
			    ps.setInt(i++, ainrpf.getPlanSuffix());
			    ps.setInt(i++, ainrpf.getCmdate());
			    ps.setString(i++, ainrpf.getRasnum());
			    ps.setString(i++, ainrpf.getRngmnt());
			    ps.setBigDecimal(i++, ainrpf.getRaAmount());
			    ps.setInt(i++, ainrpf.getRiskCessDate());
			    ps.setString(i++, ainrpf.getCurrency());
			    ps.setString(i++, ainrpf.getAintype().trim());
				ps.setString(i++, getUsrprf());
				ps.setString(i++, getJobnm());
				ps.setTimestamp(i++, getDatime());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertAinrpfList()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}	
	}
	
	  public List<Ainrpf> searchAinrpfRecord(String chdrcoy, String aintype, int batchExtractSize, int batchID) {
	        StringBuilder sqlAinrSelect = new StringBuilder();
	        sqlAinrSelect.append(" SELECT * FROM (");
	        sqlAinrSelect.append("  SELECT UNIQUE_NUMBER,CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, CRTABLE, OLDSUM,NEWSUMI,OLDINST,NEWINST,PLNSFX,CMDATE,RASNUM,RNGMNT,RAAMOUNT,RCESDTE,CURRENCY,AINTYPE ");
	        sqlAinrSelect.append("   ,FLOOR((ROW_NUMBER()OVER( ORDER BY AINTYPE ASC,CHDRCOY ASC,CHDRNUM ASC,LIFE ASC, COVERAGE ASC, RIDER ASC,PLNSFX ASC,UNIQUE_NUMBER DESC  )-1)/?) BATCHNUM FROM AINRPF ");
	        sqlAinrSelect.append("   WHERE CHDRCOY = ? AND AINTYPE=? ");
	        sqlAinrSelect.append(" ) MAIN WHERE BATCHNUM = ? ");
	        PreparedStatement psAinrSelect = getPrepareStatement(sqlAinrSelect.toString());
	        ResultSet sqlainrpf1rs = null;
	        List<Ainrpf> ainrpfList = new ArrayList<>();
	        try {
	            psAinrSelect.setInt(1, batchExtractSize);
	            psAinrSelect.setString(2, chdrcoy);
	            psAinrSelect.setString(3, aintype);
	            psAinrSelect.setInt(4, batchID);

	            sqlainrpf1rs = executeQuery(psAinrSelect);
	            while (sqlainrpf1rs.next()) {
	                Ainrpf ainrpf = new Ainrpf();
	                ainrpf.setUniqueNumber(sqlainrpf1rs.getLong("UNIQUE_NUMBER"));
	                ainrpf.setChdrcoy(sqlainrpf1rs.getString("Chdrcoy"));
	                ainrpf.setChdrnum(sqlainrpf1rs.getString("Chdrnum"));
	                ainrpf.setLife(sqlainrpf1rs.getString("Life"));
	                ainrpf.setCoverage(sqlainrpf1rs.getString("Coverage"));
	                ainrpf.setRider(sqlainrpf1rs.getString("Rider"));
	                ainrpf.setCrtable(sqlainrpf1rs.getString("Crtable"));
	                ainrpf.setRd01Oldsum(sqlainrpf1rs.getBigDecimal("Oldsum"));
	                ainrpf.setNewsumi(sqlainrpf1rs.getBigDecimal("Newsumi"));
	                ainrpf.setOldinst(sqlainrpf1rs.getBigDecimal("Oldinst"));
	                ainrpf.setNewinst(sqlainrpf1rs.getBigDecimal("Newinst"));
	                ainrpf.setPlanSuffix(sqlainrpf1rs.getInt("Plnsfx"));
	                ainrpf.setCmdate(sqlainrpf1rs.getInt("Cmdate"));
	                ainrpf.setRasnum(sqlainrpf1rs.getString("Rasnum"));
	                ainrpf.setRngmnt(sqlainrpf1rs.getString("Rngmnt"));
	                ainrpf.setRaAmount(sqlainrpf1rs.getBigDecimal("Raamount"));
	                ainrpf.setRiskCessDate(sqlainrpf1rs.getInt("Rcesdte"));
	                ainrpf.setCurrency(sqlainrpf1rs.getString("Currency"));
	                ainrpf.setAintype(sqlainrpf1rs.getString("Aintype"));
	                ainrpfList.add(ainrpf);
	            }
	        } catch (SQLException e) {
	            LOGGER.error("searchAinrRecord()", e);//IJTI-1561
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(psAinrSelect, sqlainrpf1rs);
	        }
	        return ainrpfList;
	    }
}
