/*
 * File: Overbill.java
 * Date: 29 August 2009 23:02:07
 * Author: Quipoz Limited
 * 
 * Class transformed from OVERBILL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.tablestructures.T3699rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.regularprocessing.dataaccess.LinsovrTableDAM;
import com.csc.life.regularprocessing.recordstructures.Ovrbillrec;
import com.csc.life.regularprocessing.reports.R5062Report;
import com.csc.life.regularprocessing.tablestructures.T6626rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;

/**
* <pre>
*REMARKS.
*
*
*        OVERDUE PROCESSING - BTDATE CORRECTION PROCESSING
*        -------------------------------------------------
*
* Overview
* ~~~~~~~~
*    This subroutine is called from Overdue processing. When a
*     contract is about to go Overdue and the Billed-to and
*     Paid-to dates don't match, then this subroutine is called
*     to Reverse all transactions that have occurred since the
*     last Paid-to date.
*     The theory is to reverse all PTRNs back to the last
*     'Collection-type' PTRN, ie. Collection or Paid-to-date
*     advance, so that the Billed-to and Paid-to dates will match
*     at the time the Contract is put through Non-forfeiture
*     processing.
*     The revsesal process will stop IF
*       a) A Collection-type PTRN is found (specified on T6626)
*       b) The Issue PTRN is reached
*       c) a 'Show-stopper' type PTRN is found. eg Full Surrender
*
*     This subroutine basically works in the same way as the
*      Full Contract Reversal AT module, REVGENAT.
*
* Initial Processing
* ~~~~~~~~~~~~~~~~~~
* Firstly read contract  header  record  using the logical view
* CHDRLIF. The details of minor alteration is then saved to
* working storage.
*
* The Payer Details record is then read and similarly saved to
* working storage.
*
* Get the PTRN records by reading the PTRNREV logical view.
* (PTRNREV logical view is used because it omits all valid
*  flag '2' records).
*
* For each PTRN record found - carry out the following
* processing :
* (Once  all  the  records  have  been processed go to update
*  contract header.)
*
*
* Generic Reversal processing
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~
* All  updating will be carried out by generic subroutines as
* set up  in table T6661. The key into this table is transaction
* code which we get from the relevant PTRNREV record.
* (If no entry  is  found on the table get the next record)
*
* The  copybook  to  be  set  up  for the calls will be REVERSEREC,
* the fields are to be set up as follows:
*
*     Component key with just the contract number and
*     company.
*     Effective date 1 as reverse to date.
*     TRANNO from the PTRN record
*     New TRANNO = Contract Header TRANNO + 1
*
* A detail line also has to be produced for the report using the
* following details:
*
*     Transaction number from the policy history record
*     Transaction long description from T1688
*     Effective date of transaction from policy history
*     record
*
* Move '2' to the valid flag on the policy history record and
* write back to the database.
*
* Go  back  and  process  next  record from PTRNREV logical
* view.
*
*
* Update Contract Header and Payor Details
*-----------------------------------------
* The Contract Header is read, the minor alteration details are
* restored from working storage, the new transaction number is
* stamped on the contract header and an update of the record is
* performed.
*
* Similarly, the Payer details is read, minor alteration details
* are restored from working storage and then the database record
* is updated.
*
*
*****************************************************************
* </pre>
*/
public class Overbill extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R5062Report printerFile = new R5062Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private String wsaaSubr = "OVERBILL";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaOverflow = "Y";
	protected String wsaaCollection = "";
	protected String wsaaIssue = "";
	protected FixedLengthStringData wsaaStop = new FixedLengthStringData(1);
	protected String wsaaBillTran = "N";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaNoLinsFound = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData r5062h01Record = new FixedLengthStringData(51);
	private FixedLengthStringData r5062h01O = new FixedLengthStringData(51).isAPartOf(r5062h01Record, 0);
	private FixedLengthStringData repdate = new FixedLengthStringData(10).isAPartOf(r5062h01O, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5062h01O, 10);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5062h01O, 11);
	private FixedLengthStringData sdate = new FixedLengthStringData(10).isAPartOf(r5062h01O, 41);

	private FixedLengthStringData r5062d01Record = new FixedLengthStringData(57);
	private FixedLengthStringData r5062d01O = new FixedLengthStringData(57).isAPartOf(r5062d01Record, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5062d01O, 0);
	private FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(r5062d01O, 8);
	private FixedLengthStringData trandesc = new FixedLengthStringData(30).isAPartOf(r5062d01O, 12);
	private ZonedDecimalData tranno = new ZonedDecimalData(5, 0).isAPartOf(r5062d01O, 42);
	private FixedLengthStringData effdate = new FixedLengthStringData(10).isAPartOf(r5062d01O, 47);

	private FixedLengthStringData r5062d02Record = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaT6654Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6654Billchnl = new FixedLengthStringData(1).isAPartOf(wsaaT6654Item, 0);
	private FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Item, 1);
	private FixedLengthStringData wsaaChdrAplsupr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrBillsupr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrCommsupr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrNotssupr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrRnwlsupr = new FixedLengthStringData(1);
	private PackedDecimalData wsaaChdrAplspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrBillspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrCommspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrNotsspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrRnwlspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrAplspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrBillspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrCommspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrNotsspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrRnwlspto = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaChdrCownpfx = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrCowncoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrCownnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrJownnum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaChdrPolsum = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaChdrDesppfx = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrDespcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrDespnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrAgntpfx = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrAgntcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrAgntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPayrAplsupr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPayrBillsupr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPayrNotssupr = new FixedLengthStringData(1);
	private PackedDecimalData wsaaPayrAplspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrBillspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrNotsspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrAplspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrBillspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrNotsspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrBillcd = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaThreeYears = new FixedLengthStringData(1116);
	private FixedLengthStringData wsaa3Year = new FixedLengthStringData(1116).isAPartOf(wsaaThreeYears, 0);
	private FixedLengthStringData wsaa3YearR = new FixedLengthStringData(1116).isAPartOf(wsaa3Year, 0, REDEFINE);
	private FixedLengthStringData[] wsaa3YearR1 = FLSArrayPartOfStructure(36, 31, wsaa3YearR, 0);
	private FixedLengthStringData[][] wsaa3Dd = FLSDArrayPartOfArrayStructure(31, 1, wsaa3YearR1, 0);

	private FixedLengthStringData wsaaThreeYearsR = new FixedLengthStringData(1116).isAPartOf(wsaaThreeYears, 0, REDEFINE);
	private FixedLengthStringData wsaaLastYy = new FixedLengthStringData(372).isAPartOf(wsaaThreeYearsR, 0);
	private FixedLengthStringData[] wsaaLastMm = FLSArrayPartOfStructure(12, 31, wsaaLastYy, 0);
	private FixedLengthStringData wsaaThisYy = new FixedLengthStringData(372).isAPartOf(wsaaThreeYearsR, 372);
	private FixedLengthStringData[] wsaaThisMm = FLSArrayPartOfStructure(12, 31, wsaaThisYy, 0);
	private FixedLengthStringData wsaaNextYy = new FixedLengthStringData(372).isAPartOf(wsaaThreeYearsR, 744);
	private FixedLengthStringData[] wsaaNextMm = FLSArrayPartOfStructure(12, 31, wsaaNextYy, 0);
	private ZonedDecimalData wsaaLastYear = new ZonedDecimalData(4, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaThisYear = new ZonedDecimalData(4, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaNextYear = new ZonedDecimalData(4, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData lastStart = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private int thisStart = 12;
	private int nextStart = 24;
	private ZonedDecimalData wsaaCountMm = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaCountDd = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaValyd = new FixedLengthStringData(1);
	private Validator valyd = new Validator(wsaaValyd, ".", "W", "E", "D", "X", "H");
	private ZonedDecimalData wsaaAddToDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(wsaaAddToDate, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaYyyy = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	private ZonedDecimalData wsaaYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaYyyy, 0).setUnsigned();
	private ZonedDecimalData wsaaMm = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4).setUnsigned();
	private ZonedDecimalData wsaaDd = new ZonedDecimalData(2, 0).isAPartOf(filler2, 6).setUnsigned();
		/* TABLES */
	private String t1688 = "T1688";
	private String t1693 = "T1693";
	private String t3699 = "T3699";
	private String t6626 = "T6626";
	private String t6654 = "T6654";
	private String t6661 = "T6661";
		/* ERRORS */
	private String h842 = "H842";
	private String t084 = "T084";
		/* FORMATS */
	private String chdrlifrec = "CHDRLIFREC";
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
	private String linsovrrec = "LINSOVRREC";
	private String payrrec = "PAYRREC";
	private String ptrnrevrec = "PTRNREVREC";
		/*Contract Header Life Fields*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
		/*Life Installment - Last Paid record view*/
	private LinsovrTableDAM linsovrIO = new LinsovrTableDAM();
	private Ovrbillrec ovrbillrec = new Ovrbillrec();
		/*Payor Details Logical File*/
	protected PayrTableDAM payrIO = new PayrTableDAM();
		/*Policy transaction history for reversals*/
	protected PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private Reverserec reverserec = new Reverserec();
	private Syserrrec syserrrec = new Syserrrec();
	private T3699rec t3699rec = new T3699rec();
	private T6626rec t6626rec = new T6626rec();
	private T6654rec t6654rec = new T6654rec();
	protected T6661rec t6661rec = new T6661rec();
	protected Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected Itempf itempf = null;
	
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1790, 
		exit2090, 
		exit2190, 
		exit2290, 
		detailLine2340, 
		continue3340, 
		startCheck3420, 
		setDate3480, 
		exit4190, 
		xxxxErrorBomb
	}

	public Overbill() {
		super();
	}

public void mainline(Object... parmArray)
	{
		ovrbillrec.overbillRec = convertAndSetParam(ovrbillrec.overbillRec, parmArray, 0);
		try {
			control100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void control100()
	{
		/*START*/
		initialise1000();
		while ( !(isEQ(ptrnrevIO.getStatuz(),varcom.endp))) {
			processPtrn2000();
		}
		
		processChdrPayr3000();
		housekeeping4000();
		a000Statistics();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		start1000();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		ovrbillrec.statuz.set(varcom.oK);
		ptrnrevIO.setParams(SPACES);
		wsaaNoLinsFound.set(SPACES);
		wsaaStop.set(SPACES);
		wsaaOverflow = "Y";
		wsaaBatckey.set(ovrbillrec.batchkey);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		printerFile.openOutput();
		readChdr1100();
		readPayr1200();
		readPtrn1300();
		readT66261600();
		getLins1700();
	}

protected void readChdr1100()
	{
		start1100();
	}

protected void start1100()
	{
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(ovrbillrec.company);
		chdrlifIO.setChdrnum(ovrbillrec.chdrnum);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		saveChdrMinorAlt1400();
	}

protected void readPayr1200()
	{
		start1200();
	}

protected void start1200()
	{
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrlifIO.getChdrcoy());
		payrIO.setChdrnum(chdrlifIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
		savePayrMinorAlt1500();
		wsaaPayrBillcd.set(payrIO.getBillcd());
	}

protected void readPtrn1300()
	{
		start1300();
	}

protected void start1300()
	{
		ptrnrevIO.setDataArea(SPACES);
		ptrnrevIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnrevIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnrevIO.setTranno(99999);
		ptrnrevIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		ptrnrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrnrevIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getChdrnum(),ptrnrevIO.getChdrnum())
		|| isNE(chdrlifIO.getChdrcoy(),ptrnrevIO.getChdrcoy())) {
			ptrnrevIO.setStatuz(varcom.endp);
		}
	}

protected void saveChdrMinorAlt1400()
	{
		start1400();
	}

protected void start1400()
	{
		wsaaChdrAplsupr.set(chdrlifIO.getAplsupr());
		wsaaChdrBillsupr.set(chdrlifIO.getBillsupr());
		wsaaChdrCommsupr.set(chdrlifIO.getCommsupr());
		wsaaChdrNotssupr.set(chdrlifIO.getNotssupr());
		wsaaChdrRnwlsupr.set(chdrlifIO.getRnwlsupr());
		wsaaChdrAplspfrom.set(chdrlifIO.getAplspfrom());
		wsaaChdrBillspfrom.set(chdrlifIO.getBillspfrom());
		wsaaChdrCommspfrom.set(chdrlifIO.getCommspfrom());
		wsaaChdrNotsspfrom.set(chdrlifIO.getNotsspfrom());
		wsaaChdrRnwlspfrom.set(chdrlifIO.getRnwlspfrom());
		wsaaChdrAplspto.set(chdrlifIO.getAplspto());
		wsaaChdrBillspto.set(chdrlifIO.getBillspto());
		wsaaChdrCommspto.set(chdrlifIO.getCommspto());
		wsaaChdrNotsspto.set(chdrlifIO.getNotsspto());
		wsaaChdrRnwlspto.set(chdrlifIO.getRnwlspto());
		wsaaChdrCownpfx.set(chdrlifIO.getCownpfx());
		wsaaChdrCowncoy.set(chdrlifIO.getCowncoy());
		wsaaChdrCownnum.set(chdrlifIO.getCownnum());
		wsaaChdrJownnum.set(chdrlifIO.getJownnum());
		wsaaChdrPolsum.set(chdrlifIO.getPolsum());
		wsaaChdrDesppfx.set(chdrlifIO.getDesppfx());
		wsaaChdrDespcoy.set(chdrlifIO.getDespcoy());
		wsaaChdrDespnum.set(chdrlifIO.getDespnum());
		wsaaChdrAgntpfx.set(chdrlifIO.getAgntpfx());
		wsaaChdrAgntcoy.set(chdrlifIO.getAgntcoy());
		wsaaChdrAgntnum.set(chdrlifIO.getAgntnum());
	}

protected void savePayrMinorAlt1500()
	{
		/*START*/
		wsaaPayrAplsupr.set(payrIO.getAplsupr());
		wsaaPayrBillsupr.set(payrIO.getBillsupr());
		wsaaPayrNotssupr.set(payrIO.getNotssupr());
		wsaaPayrAplspfrom.set(payrIO.getAplspfrom());
		wsaaPayrBillspfrom.set(payrIO.getBillspfrom());
		wsaaPayrNotsspfrom.set(payrIO.getNotsspfrom());
		wsaaPayrAplspto.set(payrIO.getAplspto());
		wsaaPayrBillspto.set(payrIO.getBillspto());
		wsaaPayrNotsspto.set(payrIO.getNotsspto());
		/*EXIT*/
	}

protected void readT66261600()
	{
		start1600();
	}

protected void start1600()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ovrbillrec.company);
		itemIO.setItemtabl(t6626);
		itemIO.setItemitem(ovrbillrec.batctrcde);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t6626rec.t6626Rec.set(itemIO.getGenarea());
	}

protected void getLins1700()
	{
		try {
			start1700();
		}
		catch (GOTOException e){
		}
	}

protected void start1700()
	{
		linsovrIO.setParams(SPACES);
		linsovrIO.setChdrcoy(ovrbillrec.company);
		linsovrIO.setChdrnum(ovrbillrec.chdrnum);
		linsovrIO.setInstto(99999999);
		linsovrIO.setFunction(varcom.begn);
		linsovrIO.setFormat(linsovrrec);
		//performance improvement --  atiwari23 
		linsovrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		linsovrIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		
		SmartFileCode.execute(appVars, linsovrIO);
		if (isNE(linsovrIO.getStatuz(),varcom.oK)
		&& isNE(linsovrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(linsovrIO.getParams());
			syserrrec.statuz.set(linsovrIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(ovrbillrec.company,linsovrIO.getChdrcoy())
		|| isNE(ovrbillrec.chdrnum,linsovrIO.getChdrnum())
		|| isEQ(linsovrIO.getStatuz(),varcom.endp)) {
			wsaaNoLinsFound.set("Y");
			goTo(GotoLabel.exit1790);
		}
		if (isNE(linsovrIO.getInstto(),payrIO.getPtdate())) {
			syserrrec.params.set(linsovrIO.getParams());
			syserrrec.statuz.set(t084);
			xxxxFatalError();
		}
	}

protected void processPtrn2000()
	{
		try {
			start2000();
			readNext2020();
		}
		catch (GOTOException e){
		}
	}

protected void start2000()
	{
		wsaaBillTran = "N";
		validateBatctrcde2200();
		if (isEQ(wsaaIssue,"Y")
		|| isEQ(wsaaCollection,"Y")) {
			wsaaStop.set("Y");
			ptrnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		callGenericProcessing2100();
		if (isEQ(wsaaStop,"Y")) {
			ptrnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		if (isNE(t6661rec.contRevFlag,"N")) {
			printDetailLine2300();
			rewritePtrn2500();
		}
		if (isEQ(wsaaBillTran,"Y")
		&& isLTE(ptrnrevIO.getPtrneff(),payrIO.getPtdate())) {
			ptrnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
	}

protected void readNext2020()
	{
		ptrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(ptrnrevIO.getChdrcoy(),chdrlifIO.getChdrcoy())
		|| isNE(ptrnrevIO.getChdrnum(),chdrlifIO.getChdrnum())) {
			ptrnrevIO.setStatuz(varcom.endp);
		}
	}

protected void callGenericProcessing2100()
	{
		try {
			start2110();
		}
		catch (GOTOException e){
		}
	}

protected void start2110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ovrbillrec.company);
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(ptrnrevIO.getBatctrcde());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		if (isEQ(t6661rec.contRevFlag,"N")) {
			goTo(GotoLabel.exit2190);
		}
		if (isEQ(t6661rec.contRevFlag,SPACES)) {
			wsaaStop.set("Y");
			goTo(GotoLabel.exit2190);
		}
		reverserec.chdrnum.set(chdrlifIO.getChdrnum());
		reverserec.company.set(chdrlifIO.getChdrcoy());
		reverserec.tranno.set(ptrnrevIO.getTranno());
		reverserec.ptrneff.set(ptrnrevIO.getPtrneff());
		reverserec.oldBatctrcde.set(ptrnrevIO.getBatctrcde());
		reverserec.language.set(ovrbillrec.language);
		reverserec.newTranno.set(ovrbillrec.newTranno);
		reverserec.effdate1.set(ptrnrevIO.getPtrneff());
		reverserec.effdate2.set(ZERO);
		reverserec.batchkey.set(ovrbillrec.batchkey);
		reverserec.transDate.set(ovrbillrec.tranDate);
		reverserec.transTime.set(ovrbillrec.tranTime);
		reverserec.user.set(ovrbillrec.user);
		reverserec.termid.set(ovrbillrec.termid);
		reverserec.planSuffix.set(ZERO);
		reverserec.ptrnBatcpfx.set(ptrnrevIO.getBatcpfx());
		reverserec.ptrnBatccoy.set(ptrnrevIO.getBatccoy());
		reverserec.ptrnBatcbrn.set(ptrnrevIO.getBatcbrn());
		reverserec.ptrnBatcactyr.set(ptrnrevIO.getBatcactyr());
		reverserec.ptrnBatcactmn.set(ptrnrevIO.getBatcactmn());
		reverserec.ptrnBatctrcde.set(ptrnrevIO.getBatctrcde());
		reverserec.ptrnBatcbatch.set(ptrnrevIO.getBatcbatch());
		reverserec.statuz.set(varcom.oK);
		if (isNE(t6661rec.subprog01,SPACES)) {
			callProgram(t6661rec.subprog01, reverserec.reverseRec);
			if (isNE(reverserec.statuz,varcom.oK)) {
				syserrrec.params.set(reverserec.reverseRec);
				syserrrec.statuz.set(reverserec.statuz);
				xxxxFatalError();
			}
		}
		reverserec.statuz.set(varcom.oK);
		if (isNE(t6661rec.subprog02,SPACES)) {
			callProgram(t6661rec.subprog02, reverserec.reverseRec);
			if (isNE(reverserec.statuz,varcom.oK)) {
				syserrrec.params.set(reverserec.reverseRec);
				syserrrec.statuz.set(reverserec.statuz);
				xxxxFatalError();
			}
		}
	}

protected void validateBatctrcde2200()
	{
		try {
			start2200();
		}
		catch (GOTOException e){
		}
	}

protected void start2200()
	{
		wsaaCollection = "N";
		wsaaIssue = "N";
		if (isEQ(ptrnrevIO.getBatctrcde(),t6626rec.trcode)) {
			wsaaBillTran = "Y";
		}
		if (isEQ(wsaaNoLinsFound,"Y")) {
			for (wsaaSub.set(6); !(isGT(wsaaSub,10)); wsaaSub.add(1)){
				if (isEQ(ptrnrevIO.getBatctrcde(),t6626rec.transcd[wsaaSub.toInt()])) {
					wsaaIssue = "Y";
					wsaaSub.set(11);
				}
			}
			goTo(GotoLabel.exit2290);
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub,5)); wsaaSub.add(1)){
			if (isEQ(ptrnrevIO.getBatctrcde(),t6626rec.transcd[wsaaSub.toInt()])) {
				wsaaCollection = "Y";
				wsaaSub.set(6);
			}
		}
	}

protected void printDetailLine2300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2310();
				}
				case detailLine2340: {
					detailLine2340();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2310()
	{
		printerRec.set(SPACES);
		if (isNE(wsaaOverflow,"Y")) {
			goTo(GotoLabel.detailLine2340);
		}
		printerRec.set(SPACES);
		company.set(chdrlifIO.getChdrcoy());
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(wsaaToday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			xxxxFatalError();
		}
		repdate.set(datcon1rec.extDate);
		sdate.set(datcon1rec.extDate);
		descIO.setParams(SPACES);
		descIO.setDescitem(ptrnrevIO.getChdrcoy());
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setLanguage(ovrbillrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			companynm.fill("?");
		}
		else {
			companynm.set(descIO.getLongdesc());
		}
		wsaaOverflow = "N";
		printerFile.printR5062h01(r5062h01Record);
	}

protected void detailLine2340()
	{
		printerRec.set(SPACES);
		chdrnum.set(chdrlifIO.getChdrnum());
		batctrcde.set(ptrnrevIO.getBatctrcde());
		descIO.setParams(SPACES);
		descIO.setDescitem(ptrnrevIO.getBatctrcde());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(ptrnrevIO.getChdrcoy());
		descIO.setDesctabl(t1688);
		descIO.setLanguage(ovrbillrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			trandesc.fill("?");
		}
		else {
			trandesc.set(descIO.getLongdesc());
		}
		tranno.set(ptrnrevIO.getTranno());
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(ptrnrevIO.getPtrneff());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			xxxxFatalError();
		}
		effdate.set(datcon1rec.extDate);
		wsaaOverflow = "N";
		printerFile.printR5062d01(r5062d01Record);
	}

protected void rewritePtrn2500()
	{
		start2510();
	}

protected void start2510()
	{
		ptrnrevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			xxxxFatalError();
		}
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnrevIO.setUserProfile(username); //IJS-523
		ptrnrevIO.setValidflag("2");
		ptrnrevIO.setFunction(varcom.rewrt);
		ptrnrevIO.setFormat(ptrnrevrec);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void processChdrPayr3000()
	{
		readhChdr3010();
		readhPayr3030();
		updateFields3040();
		rewrtChdr3050();
		rewrtPayr3070();
	}

protected void readhChdr3010()
	{
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		restoreChdrMinorAlt3100();
	}

protected void readhPayr3030()
	{
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrlifIO.getChdrcoy());
		payrIO.setChdrnum(chdrlifIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
		restorePayrMinorAlt3200();
	}

protected void updateFields3040()
	{
		if (isNE(payrIO.getBillcd(),wsaaPayrBillcd)) {
			updatePayrNextdate3300();
		}
	}

protected void rewrtChdr3050()
	{
		chdrlifIO.setTranno(ovrbillrec.newTranno);
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		varcom.vrcmDate.set(ovrbillrec.tranDate);
		varcom.vrcmTime.set(ovrbillrec.tranTime);
		varcom.vrcmUser.set(ovrbillrec.user);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		varcom.vrcmCompTermid.set(ovrbillrec.termid);
		chdrlifIO.setTranid(varcom.vrcmCompTranid);
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void rewrtPayr3070()
	{
		payrIO.setTranno(ovrbillrec.newTranno);
		payrIO.setTransactionDate(ovrbillrec.tranDate);
		payrIO.setTransactionTime(ovrbillrec.tranTime);
		payrIO.setTermid(ovrbillrec.termid);
		payrIO.setUser(ovrbillrec.user);
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
		ovrbillrec.btdate.set(payrIO.getBtdate());
		ovrbillrec.ptdate.set(payrIO.getPtdate());
	}

protected void restoreChdrMinorAlt3100()
	{
		start3101();
	}

protected void start3101()
	{
		chdrlifIO.setAplsupr(wsaaChdrAplsupr);
		chdrlifIO.setBillsupr(wsaaChdrBillsupr);
		chdrlifIO.setCommsupr(wsaaChdrCommsupr);
		chdrlifIO.setNotssupr(wsaaChdrNotssupr);
		chdrlifIO.setRnwlsupr(wsaaChdrRnwlsupr);
		chdrlifIO.setAplspfrom(wsaaChdrAplspfrom);
		chdrlifIO.setBillspfrom(wsaaChdrBillspfrom);
		chdrlifIO.setCommspfrom(wsaaChdrCommspfrom);
		chdrlifIO.setNotsspfrom(wsaaChdrNotsspfrom);
		chdrlifIO.setRnwlspfrom(wsaaChdrRnwlspfrom);
		chdrlifIO.setAplspto(wsaaChdrAplspto);
		chdrlifIO.setBillspto(wsaaChdrBillspto);
		chdrlifIO.setCommspto(wsaaChdrCommspto);
		chdrlifIO.setNotsspto(wsaaChdrNotsspto);
		chdrlifIO.setRnwlspto(wsaaChdrRnwlspto);
		chdrlifIO.setCownpfx(wsaaChdrCownpfx);
		chdrlifIO.setCowncoy(wsaaChdrCowncoy);
		chdrlifIO.setCownnum(wsaaChdrCownnum);
		chdrlifIO.setJownnum(wsaaChdrJownnum);
		chdrlifIO.setPolsum(wsaaChdrPolsum);
		chdrlifIO.setDesppfx(wsaaChdrDesppfx);
		chdrlifIO.setDespcoy(wsaaChdrDespcoy);
		chdrlifIO.setDespnum(wsaaChdrDespnum);
		chdrlifIO.setAgntpfx(wsaaChdrAgntpfx);
		chdrlifIO.setAgntcoy(wsaaChdrAgntcoy);
		chdrlifIO.setAgntnum(wsaaChdrAgntnum);
	}

protected void restorePayrMinorAlt3200()
	{
		/*START*/
		payrIO.setAplsupr(wsaaPayrAplsupr);
		payrIO.setBillsupr(wsaaPayrBillsupr);
		payrIO.setNotssupr(wsaaPayrNotssupr);
		payrIO.setAplspfrom(wsaaPayrAplspfrom);
		payrIO.setBillspfrom(wsaaPayrBillspfrom);
		payrIO.setNotsspfrom(wsaaPayrNotsspfrom);
		payrIO.setAplspto(wsaaPayrAplspto);
		payrIO.setBillspto(wsaaPayrBillspto);
		payrIO.setNotsspto(wsaaPayrNotsspto);
		/*EXIT*/
	}

protected void updatePayrNextdate3300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3301();
				}
				case continue3340: {
					continue3340();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3301()
	{
		
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		String key;
		if(BTPRO028Permission) {
			key = payrIO.getBillchnl().toString().trim() + chdrlifIO.getCnttype().toString().trim() + payrIO.getBillfreq().toString().trim();
			if(!readT6654(key)) {
				key = payrIO.getBillchnl().toString().trim().concat(chdrlifIO.getCnttype().toString().trim()).concat("**");
				if(!readT6654(key)) {
					key = payrIO.getBillchnl().toString().trim().concat("*****");
					if(!readT6654(key)) {
						syserrrec.params.set(itemIO.getParams());
						syserrrec.statuz.set(itemIO.getStatuz());
						xxxxFatalError();
					}
				}
			}
		}
		else {
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(payrIO.getChdrcoy());
		itemIO.setItemtabl(t6654);
		wsaaT6654Item.set(SPACES);
		wsaaT6654Billchnl.set(payrIO.getBillchnl());
		wsaaT6654Cnttype.set(chdrlifIO.getCnttype());
		itemIO.setItemitem(wsaaT6654Item);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			t6654rec.t6654Rec.set(itemIO.getGenarea());
			goTo(GotoLabel.continue3340);
		}
		wsaaT6654Cnttype.set("***");
		itemIO.setItemitem(wsaaT6654Item);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t6654rec.t6654Rec.set(itemIO.getGenarea());
		}
	}

protected boolean readT6654(String key) {
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(payrIO.getChdrcoy().toString());
	itempf.setItemtabl(t6654);
	itempf.setItemitem(key);
	itempf = itempfDAO.getItempfRecord(itempf);
	if (null != itempf) {
		t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		return true;
	}
	return false;
}

protected void continue3340()
	{
		wsaaAddToDate.set(payrIO.getBillcd());
		wsaaThisYear.set(wsaaYear);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlifIO.getCowncoy());
		itemIO.setItemtabl(t3699);
		itemIO.setItemitem(wsaaYyyy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t3699rec.t3699Rec.set(itemIO.getGenarea());
		wsaaThisYy.set(t3699rec.t3699Rec);
		wsaaAddToDate.set(payrIO.getBillcd());
		wsaaYear.subtract(1);
		wsaaLastYear.set(wsaaYear);
		itemIO.setItemitem(wsaaYyyy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t3699rec.t3699Rec.set(itemIO.getGenarea());
		wsaaLastYy.set(t3699rec.t3699Rec);
		wsaaAddToDate.set(payrIO.getBillcd());
		wsaaYear.add(1);
		wsaaNextYear.set(wsaaYear);
		itemIO.setItemitem(wsaaYyyy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t3699rec.t3699Rec.set(itemIO.getGenarea());
		wsaaNextYy.set(t3699rec.t3699Rec);
		wsaaAddToDate.set(payrIO.getBillcd());
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,t6654rec.leadDays)); wsaaCnt.add(1)){
			datcon2rec.intDate1.set(wsaaAddToDate);
			datcon2rec.frequency.set("DY");
			datcon2rec.freqFactor.set(-1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				xxxxFatalError();
			}
			wsaaAddToDate.set(datcon2rec.intDate2);
			checkHolidays3400();
		}
		payrIO.setNextdate(wsaaAddToDate);
	}

protected void checkHolidays3400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					setUp3410();
				}
				case startCheck3420: {
					startCheck3420();
				}
				case setDate3480: {
					setDate3480();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setUp3410()
	{
		if (isEQ(wsaaYear,wsaaLastYear)) {
			wsaaCountMm.set(lastStart);
		}
		if (isEQ(wsaaYear,wsaaThisYear)) {
			wsaaCountMm.set(thisStart);
		}
		if (isEQ(wsaaYear,wsaaNextYear)) {
			wsaaCountMm.set(nextStart);
		}
		wsaaCountMm.add(wsaaMm);
		wsaaCountDd.set(wsaaDd);
	}

protected void startCheck3420()
	{
		wsaaValyd.set(wsaa3Dd[wsaaCountMm.toInt()][wsaaCountDd.toInt()]);
		if (!valyd.isTrue()) {
			syserrrec.statuz.set(h842);
			syserrrec.params.set(t3699);
			xxxxFatalError();
		}
		if (isEQ(wsaa3Dd[wsaaCountMm.toInt()][wsaaCountDd.toInt()],".")
		|| isEQ(wsaa3Dd[wsaaCountMm.toInt()][wsaaCountDd.toInt()],"D")) {
			goTo(GotoLabel.setDate3480);
		}
		if (isEQ(wsaa3Dd[wsaaCountMm.toInt()][wsaaCountDd.toInt()],"W")) {
			wsaaCountDd.subtract(1);
			if (isLT(wsaaCountDd,1)) {
				wsaaCountDd.set(31);
				wsaaCountMm.subtract(1);
			}
		}
		if (isEQ(wsaa3Dd[wsaaCountMm.toInt()][wsaaCountDd.toInt()],"E")) {
			wsaaCountDd.subtract(1);
			if (isLT(wsaaCountDd,1)) {
				wsaaCountDd.set(31);
				wsaaCountMm.subtract(1);
			}
		}
		if (isEQ(wsaa3Dd[wsaaCountMm.toInt()][wsaaCountDd.toInt()],"H")) {
			wsaaCountDd.subtract(1);
			if (isLT(wsaaCountDd,1)) {
				wsaaCountDd.set(31);
				wsaaCountMm.subtract(1);
			}
		}
		if (isEQ(wsaa3Dd[wsaaCountMm.toInt()][wsaaCountDd.toInt()],"X")) {
			wsaaCountDd.subtract(1);
			if (isLT(wsaaCountDd,1)) {
				wsaaCountDd.set(31);
				wsaaCountMm.subtract(1);
			}
		}
		goTo(GotoLabel.startCheck3420);
	}

protected void setDate3480()
	{
		wsaaDd.set(wsaaCountDd);
		wsaaMm.set(wsaaCountMm);
		if (isLT(wsaaCountMm,13)) {
			wsaaMm.set(wsaaCountMm);
			wsaaYear.set(wsaaLastYear);
		}
		if (isGT(wsaaCountMm,12)
		&& isLT(wsaaCountMm,25)) {
			compute(wsaaMm, 0).set(sub(wsaaCountMm,12));
			wsaaYear.set(wsaaThisYear);
		}
		if (isGT(wsaaCountMm,24)) {
			compute(wsaaMm, 0).set(sub(wsaaCountMm,24));
			wsaaYear.set(wsaaNextYear);
		}
	}

protected void housekeeping4000()
	{
		/*START*/
		if (isNE(ovrbillrec.btdate,ovrbillrec.ptdate)) {
			errorLine4100();
		}
		printerFile.close();
		/*EXIT*/
	}

protected void errorLine4100()
	{
		try {
			start4100();
		}
		catch (GOTOException e){
		}
	}

protected void start4100()
	{
		if (isNE(wsaaOverflow,"Y")) {
			printerFile.printR5062d02(r5062d02Record);
			goTo(GotoLabel.exit4190);
		}
		printerRec.set(SPACES);
		company.set(chdrlifIO.getChdrcoy());
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(wsaaToday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			xxxxFatalError();
		}
		repdate.set(datcon1rec.extDate);
		descIO.setParams(SPACES);
		descIO.setDescitem(ptrnrevIO.getChdrcoy());
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setLanguage(ovrbillrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			companynm.fill("?");
		}
		else {
			companynm.set(descIO.getLongdesc());
		}
		wsaaOverflow = "N";
		printerFile.printR5062h01(r5062h01Record);
		printerFile.printR5062d02(r5062d02Record);
	}

protected void xxxxFatalError()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					xxxxFatalErrors();
				}
				case xxxxErrorBomb: {
					xxxxErrorBomb();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.xxxxErrorBomb);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorBomb()
	{
		ovrbillrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

protected void a000Statistics()
	{
		a010Start();
	}

protected void a010Start()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrlifIO.getChdrnum());
		lifsttrrec.tranno.set(ovrbillrec.newTranno);
		lifsttrrec.trannor.set(ptrnrevIO.getTranno());
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			xxxxFatalError();
		}
	}
}
