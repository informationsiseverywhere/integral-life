package com.csc.life.regularprocessing.dataaccess.model;


public class Linrpf {
	private long uniqueNumber = 0;
	private String chdrcoy = "";
	private String chdrnum = "";
	private int payrseqno = 0;
	private int instfrom = 0;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public int getPayrseqno() {
		return payrseqno;
	}
	public void setPayrseqno(int payrseqno) {
		this.payrseqno = payrseqno;
	}
	public int getInstfrom() {
		return instfrom;
	}
	public void setInstfrom(int instfrom) {
		this.instfrom = instfrom;
	}
}