package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.regularprocessing.dataaccess.model.Bonlpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface BonlpfDAO extends BaseDAO<Bonlpf> {
    public void insertBonlpf(List<Bonlpf> bonlpfList);
    public void updateBonlpf(List<Bonlpf> bonlpfList);
    public Map<String,List<Bonlpf>> searchBonlpf(String coy,List<String> chdrnum);
}