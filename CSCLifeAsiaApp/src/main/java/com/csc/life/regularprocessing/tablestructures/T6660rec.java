package com.csc.life.regularprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:03
 * Description:
 * Copybook name: T6660REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6660rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6660Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData acctccys = new FixedLengthStringData(270).isAPartOf(t6660Rec, 0);
  	public FixedLengthStringData[] acctccy = FLSArrayPartOfStructure(90, 3, acctccys, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(270).isAPartOf(acctccys, 0, FILLER_REDEFINE);
  	public FixedLengthStringData acctccy01 = new FixedLengthStringData(3).isAPartOf(filler, 0);
  	public FixedLengthStringData acctccy02 = new FixedLengthStringData(3).isAPartOf(filler, 3);
  	public FixedLengthStringData acctccy03 = new FixedLengthStringData(3).isAPartOf(filler, 6);
  	public FixedLengthStringData acctccy04 = new FixedLengthStringData(3).isAPartOf(filler, 9);
  	public FixedLengthStringData acctccy05 = new FixedLengthStringData(3).isAPartOf(filler, 12);
  	public FixedLengthStringData acctccy06 = new FixedLengthStringData(3).isAPartOf(filler, 15);
  	public FixedLengthStringData acctccy07 = new FixedLengthStringData(3).isAPartOf(filler, 18);
  	public FixedLengthStringData acctccy08 = new FixedLengthStringData(3).isAPartOf(filler, 21);
  	public FixedLengthStringData acctccy09 = new FixedLengthStringData(3).isAPartOf(filler, 24);
  	public FixedLengthStringData acctccy10 = new FixedLengthStringData(3).isAPartOf(filler, 27);
  	public FixedLengthStringData acctccy11 = new FixedLengthStringData(3).isAPartOf(filler, 30);
  	public FixedLengthStringData acctccy12 = new FixedLengthStringData(3).isAPartOf(filler, 33);
  	public FixedLengthStringData acctccy13 = new FixedLengthStringData(3).isAPartOf(filler, 36);
  	public FixedLengthStringData acctccy14 = new FixedLengthStringData(3).isAPartOf(filler, 39);
  	public FixedLengthStringData acctccy15 = new FixedLengthStringData(3).isAPartOf(filler, 42);
  	public FixedLengthStringData acctccy16 = new FixedLengthStringData(3).isAPartOf(filler, 45);
  	public FixedLengthStringData acctccy17 = new FixedLengthStringData(3).isAPartOf(filler, 48);
  	public FixedLengthStringData acctccy18 = new FixedLengthStringData(3).isAPartOf(filler, 51);
  	public FixedLengthStringData acctccy19 = new FixedLengthStringData(3).isAPartOf(filler, 54);
  	public FixedLengthStringData acctccy20 = new FixedLengthStringData(3).isAPartOf(filler, 57);
  	public FixedLengthStringData acctccy21 = new FixedLengthStringData(3).isAPartOf(filler, 60);
  	public FixedLengthStringData acctccy22 = new FixedLengthStringData(3).isAPartOf(filler, 63);
  	public FixedLengthStringData acctccy23 = new FixedLengthStringData(3).isAPartOf(filler, 66);
  	public FixedLengthStringData acctccy24 = new FixedLengthStringData(3).isAPartOf(filler, 69);
  	public FixedLengthStringData acctccy25 = new FixedLengthStringData(3).isAPartOf(filler, 72);
  	public FixedLengthStringData acctccy26 = new FixedLengthStringData(3).isAPartOf(filler, 75);
  	public FixedLengthStringData acctccy27 = new FixedLengthStringData(3).isAPartOf(filler, 78);
  	public FixedLengthStringData acctccy28 = new FixedLengthStringData(3).isAPartOf(filler, 81);
  	public FixedLengthStringData acctccy29 = new FixedLengthStringData(3).isAPartOf(filler, 84);
  	public FixedLengthStringData acctccy30 = new FixedLengthStringData(3).isAPartOf(filler, 87);
  	public FixedLengthStringData acctccy31 = new FixedLengthStringData(3).isAPartOf(filler, 90);
  	public FixedLengthStringData acctccy32 = new FixedLengthStringData(3).isAPartOf(filler, 93);
  	public FixedLengthStringData acctccy33 = new FixedLengthStringData(3).isAPartOf(filler, 96);
  	public FixedLengthStringData acctccy34 = new FixedLengthStringData(3).isAPartOf(filler, 99);
  	public FixedLengthStringData acctccy35 = new FixedLengthStringData(3).isAPartOf(filler, 102);
  	public FixedLengthStringData acctccy36 = new FixedLengthStringData(3).isAPartOf(filler, 105);
  	public FixedLengthStringData acctccy37 = new FixedLengthStringData(3).isAPartOf(filler, 108);
  	public FixedLengthStringData acctccy38 = new FixedLengthStringData(3).isAPartOf(filler, 111);
  	public FixedLengthStringData acctccy39 = new FixedLengthStringData(3).isAPartOf(filler, 114);
  	public FixedLengthStringData acctccy40 = new FixedLengthStringData(3).isAPartOf(filler, 117);
  	public FixedLengthStringData acctccy41 = new FixedLengthStringData(3).isAPartOf(filler, 120);
  	public FixedLengthStringData acctccy42 = new FixedLengthStringData(3).isAPartOf(filler, 123);
  	public FixedLengthStringData acctccy43 = new FixedLengthStringData(3).isAPartOf(filler, 126);
  	public FixedLengthStringData acctccy44 = new FixedLengthStringData(3).isAPartOf(filler, 129);
  	public FixedLengthStringData acctccy45 = new FixedLengthStringData(3).isAPartOf(filler, 132);
  	public FixedLengthStringData acctccy46 = new FixedLengthStringData(3).isAPartOf(filler, 135);
  	public FixedLengthStringData acctccy47 = new FixedLengthStringData(3).isAPartOf(filler, 138);
  	public FixedLengthStringData acctccy48 = new FixedLengthStringData(3).isAPartOf(filler, 141);
  	public FixedLengthStringData acctccy49 = new FixedLengthStringData(3).isAPartOf(filler, 144);
  	public FixedLengthStringData acctccy50 = new FixedLengthStringData(3).isAPartOf(filler, 147);
  	public FixedLengthStringData acctccy51 = new FixedLengthStringData(3).isAPartOf(filler, 150);
  	public FixedLengthStringData acctccy52 = new FixedLengthStringData(3).isAPartOf(filler, 153);
  	public FixedLengthStringData acctccy53 = new FixedLengthStringData(3).isAPartOf(filler, 156);
  	public FixedLengthStringData acctccy54 = new FixedLengthStringData(3).isAPartOf(filler, 159);
  	public FixedLengthStringData acctccy55 = new FixedLengthStringData(3).isAPartOf(filler, 162);
  	public FixedLengthStringData acctccy56 = new FixedLengthStringData(3).isAPartOf(filler, 165);
  	public FixedLengthStringData acctccy57 = new FixedLengthStringData(3).isAPartOf(filler, 168);
  	public FixedLengthStringData acctccy58 = new FixedLengthStringData(3).isAPartOf(filler, 171);
  	public FixedLengthStringData acctccy59 = new FixedLengthStringData(3).isAPartOf(filler, 174);
  	public FixedLengthStringData acctccy60 = new FixedLengthStringData(3).isAPartOf(filler, 177);
  	public FixedLengthStringData acctccy61 = new FixedLengthStringData(3).isAPartOf(filler, 180);
  	public FixedLengthStringData acctccy62 = new FixedLengthStringData(3).isAPartOf(filler, 183);
  	public FixedLengthStringData acctccy63 = new FixedLengthStringData(3).isAPartOf(filler, 186);
  	public FixedLengthStringData acctccy64 = new FixedLengthStringData(3).isAPartOf(filler, 189);
  	public FixedLengthStringData acctccy65 = new FixedLengthStringData(3).isAPartOf(filler, 192);
  	public FixedLengthStringData acctccy66 = new FixedLengthStringData(3).isAPartOf(filler, 195);
  	public FixedLengthStringData acctccy67 = new FixedLengthStringData(3).isAPartOf(filler, 198);
  	public FixedLengthStringData acctccy68 = new FixedLengthStringData(3).isAPartOf(filler, 201);
  	public FixedLengthStringData acctccy69 = new FixedLengthStringData(3).isAPartOf(filler, 204);
  	public FixedLengthStringData acctccy70 = new FixedLengthStringData(3).isAPartOf(filler, 207);
  	public FixedLengthStringData acctccy71 = new FixedLengthStringData(3).isAPartOf(filler, 210);
  	public FixedLengthStringData acctccy72 = new FixedLengthStringData(3).isAPartOf(filler, 213);
  	public FixedLengthStringData acctccy73 = new FixedLengthStringData(3).isAPartOf(filler, 216);
  	public FixedLengthStringData acctccy74 = new FixedLengthStringData(3).isAPartOf(filler, 219);
  	public FixedLengthStringData acctccy75 = new FixedLengthStringData(3).isAPartOf(filler, 222);
  	public FixedLengthStringData acctccy76 = new FixedLengthStringData(3).isAPartOf(filler, 225);
  	public FixedLengthStringData acctccy77 = new FixedLengthStringData(3).isAPartOf(filler, 228);
  	public FixedLengthStringData acctccy78 = new FixedLengthStringData(3).isAPartOf(filler, 231);
  	public FixedLengthStringData acctccy79 = new FixedLengthStringData(3).isAPartOf(filler, 234);
  	public FixedLengthStringData acctccy80 = new FixedLengthStringData(3).isAPartOf(filler, 237);
  	public FixedLengthStringData acctccy81 = new FixedLengthStringData(3).isAPartOf(filler, 240);
  	public FixedLengthStringData acctccy82 = new FixedLengthStringData(3).isAPartOf(filler, 243);
  	public FixedLengthStringData acctccy83 = new FixedLengthStringData(3).isAPartOf(filler, 246);
  	public FixedLengthStringData acctccy84 = new FixedLengthStringData(3).isAPartOf(filler, 249);
  	public FixedLengthStringData acctccy85 = new FixedLengthStringData(3).isAPartOf(filler, 252);
  	public FixedLengthStringData acctccy86 = new FixedLengthStringData(3).isAPartOf(filler, 255);
  	public FixedLengthStringData acctccy87 = new FixedLengthStringData(3).isAPartOf(filler, 258);
  	public FixedLengthStringData acctccy88 = new FixedLengthStringData(3).isAPartOf(filler, 261);
  	public FixedLengthStringData acctccy89 = new FixedLengthStringData(3).isAPartOf(filler, 264);
  	public FixedLengthStringData acctccy90 = new FixedLengthStringData(3).isAPartOf(filler, 267);
  	public FixedLengthStringData cntcurrs = new FixedLengthStringData(27).isAPartOf(t6660Rec, 270);
  	public FixedLengthStringData[] cntcurr = FLSArrayPartOfStructure(9, 3, cntcurrs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(27).isAPartOf(cntcurrs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData cntcurr01 = new FixedLengthStringData(3).isAPartOf(filler1, 0);
  	public FixedLengthStringData cntcurr02 = new FixedLengthStringData(3).isAPartOf(filler1, 3);
  	public FixedLengthStringData cntcurr03 = new FixedLengthStringData(3).isAPartOf(filler1, 6);
  	public FixedLengthStringData cntcurr04 = new FixedLengthStringData(3).isAPartOf(filler1, 9);
  	public FixedLengthStringData cntcurr05 = new FixedLengthStringData(3).isAPartOf(filler1, 12);
  	public FixedLengthStringData cntcurr06 = new FixedLengthStringData(3).isAPartOf(filler1, 15);
  	public FixedLengthStringData cntcurr07 = new FixedLengthStringData(3).isAPartOf(filler1, 18);
  	public FixedLengthStringData cntcurr08 = new FixedLengthStringData(3).isAPartOf(filler1, 21);
  	public FixedLengthStringData cntcurr09 = new FixedLengthStringData(3).isAPartOf(filler1, 24);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(203).isAPartOf(t6660Rec, 297, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6660Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6660Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}