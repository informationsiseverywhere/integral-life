/*
 * File: Br612.java
 * Date: 29 August 2009 22:24:24
 * Author: Quipoz Limited
 * 
 * Class transformed from BR612.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.newbusiness.dataaccess.CorepfTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.dataaccess.ChdrrnlTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.regularprocessing.tablestructures.T5655rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.impl.ItemDAOImpl;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*  SPLITTER PROGRAM (For Component Rerate batch processing)
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* This program is to be run before Re-rate Porgram, BR613.
* It is run in single-thread, and writes the selected COVR
* records to multiple members. Each of the members will be read
* by a copy of BR613 run in multi-thread mode.
*
* SQL will be used to access the COVR physical file. The
* splitter program will extract COVR records that meet the
* following criteria:
*
* i    Service Unit =  'LP'
* ii   Chdroy     =  <run-company>
* iii  Validflag  =  '1'
* iv   Billing Channel not = 'N'
* v    Chdrnum    between  <p6671-chdrnumfrom>
*                     and  <p6671-chdrnumto>
* vi   Rerate Date not = 0
* vii  Rerate Date < Batch Effective Date + Highest number of
*                    rerate lead days on T5655.
*
* Control totals used in this program:
*
*    01  -  No. of thread members
*    02  -  No. of extracted records
*
*
* A Splitter Program's purpose is to find a pending transactions from
* the database and create multiple extract files for processing
* by multiple copies of the subsequent program, each running in it's
* own thread. Each subsequent program therefore processes a descrete
* portion of the total transactions.
*
* Splitter programs should be simple. They should only deal with
* a single file. If there is insufficient data on the primary file
* to completely identify a transaction then the further editing
* should be done in the subsequent process. The objective of a
* Splitter is to rapidily isolate potential transactions, not
* to process the transaction. The primary concern for Splitter
* program design is elasped time speed and this is acheived by
* minimising disk IO.
*
* Before the Splitter process is invoked the, the program CRTTMPF
* should be run. The CRTTMPF, (Create Temporary File), will create
* a duplicate of a physical file, (created under Smart and defined
* as a Query file), which will have as many members as is defined
* in that process's 'subsequent threads' field. The temporary file
* will have the name XXXXDD9999 where XXXX is the first four
* characters of the file which was duplicated, DD is the first two
* characters of the 4th system parameter and 9999 is the schedule
* run number. Each member added to the temporary file will be
* called THREAD999 where 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read records from a primary file
* and for each record it selects it will write a record to a
* temporary file member. Exactly which temporary file member
* is written to is decided by a working storage count. The
* objective of the count is spread the transaction records evenly
* amoungst each thread member.
*
* The Splitter program should always have a restart method of
* '1' indicating a re-run. In the event of a restart the Splitter
* program will always start with empty file members. Apart from
* writing records to the temporary file members Splitter programs
* should not be doing any further updates.
*
* There should be no non-standard CL commands in the CL Pgm which
* calls this program.
*
* Notes for programs which use Splitter Program Temporary Files.
* --------------------------------------------------------------
*
* The MTBE will be able to submit multiple versions of the program
* which use the file members created from this program. It is
* important that the process which runs the splitter program has
* the same 'number of subsequent threads' as the following
* process has defined in 'number of threads this process'.
*
* The subsequent program need only point itself to one member
* based around the field BSPR-SCHEDULE-THREAD-NO which indicates
* in which thread the copy of the subsequent program is running.
*
***********************************************************************
* </pre>
*/
public class Br612 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlchdrpf1rs = null;
	private java.sql.PreparedStatement sqlchdrpf1ps = null;
	private java.sql.Connection sqlchdrpf1conn = null;
	private String sqlchdrpf1 = "";
	private int chdrpf1LoopIndex = 0;
	private CorepfTableDAM corepf = new CorepfTableDAM();
	private DiskFileDAM core01 = new DiskFileDAM("CORE01");
	private DiskFileDAM core02 = new DiskFileDAM("CORE02");
	private DiskFileDAM core03 = new DiskFileDAM("CORE03");
	private DiskFileDAM core04 = new DiskFileDAM("CORE04");
	private DiskFileDAM core05 = new DiskFileDAM("CORE05");
	private DiskFileDAM core06 = new DiskFileDAM("CORE06");
	private DiskFileDAM core07 = new DiskFileDAM("CORE07");
	private DiskFileDAM core08 = new DiskFileDAM("CORE08");
	private DiskFileDAM core09 = new DiskFileDAM("CORE09");
	private DiskFileDAM core10 = new DiskFileDAM("CORE10");
	private DiskFileDAM core11 = new DiskFileDAM("CORE11");
	private DiskFileDAM core12 = new DiskFileDAM("CORE12");
	private DiskFileDAM core13 = new DiskFileDAM("CORE13");
	private DiskFileDAM core14 = new DiskFileDAM("CORE14");
	private DiskFileDAM core15 = new DiskFileDAM("CORE15");
	private DiskFileDAM core16 = new DiskFileDAM("CORE16");
	private DiskFileDAM core17 = new DiskFileDAM("CORE17");
	private DiskFileDAM core18 = new DiskFileDAM("CORE18");
	private DiskFileDAM core19 = new DiskFileDAM("CORE19");
	private DiskFileDAM core20 = new DiskFileDAM("CORE20");
	private CorepfTableDAM corepfData = new CorepfTableDAM();
	private FixedLengthStringData core01Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core02Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core03Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core04Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core05Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core06Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core07Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core08Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core09Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core10Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core11Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core12Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core13Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core14Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core15Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core16Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core17Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core18Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core19Rec = new FixedLengthStringData(22);
	private FixedLengthStringData core20Rec = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR612");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaChdrnumfrm = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumto = new FixedLengthStringData(8);	
	private ZonedDecimalData wsaaMaxLeadDays = new ZonedDecimalData(3, 0);
	private FixedLengthStringData wsaaSqlCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSqlValidflag = new FixedLengthStringData(1).init("1");
	private ZonedDecimalData wsaaSqlDate = new ZonedDecimalData(8, 0);

	
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(100);

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaCovrData = FLSInittedArray (100, 22);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaCovrData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaCovrData, 1);
	private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaCovrData, 9);
	private FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaCovrData, 11);
	private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaCovrData, 13);
	private PackedDecimalData[] wsaaPlnsfx = PDArrayPartOfArrayStructure(4, 0, wsaaCovrData, 15);
	private FixedLengthStringData[] wsaaStatcode = FLSDArrayPartOfArrayStructure(2, wsaaCovrData, 18);
	private FixedLengthStringData[] wsaaPstatcode = FLSDArrayPartOfArrayStructure(2, wsaaCovrData, 20);

		/* WSAA-IND-ARRAY */
	private FixedLengthStringData[] wsaaCovrInd = FLSInittedArray (100, 4);
	private BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(2, 4, 0, wsaaCovrInd, 0);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

	private FixedLengthStringData wsaaCoreFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler4 = new FixedLengthStringData(4).isAPartOf(wsaaCoreFn, 0, FILLER).init("CORE");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaCoreFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaCoreFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler5 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaClrtmpfError = new FixedLengthStringData(97);
		/* WSAA-HOST-VARIABLES */
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");
		/*  Pointers to point to the member to read (IY) and write (IZ).*/
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private static final String chdrrnlrec = "CHDRRNLREC";
		/* ERRORS */
	private static final String esql = "ESQL";	
	private static final String ivrm = "IVRM";
		/* TABLES */
	private static final String t5679 = "T5679";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);	
	private IntegerData wsaaInd = new IntegerData();
	private ChdrrnlTableDAM chdrrnlIO = new ChdrrnlTableDAM();	
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private T5679rec t5679rec = new T5679rec();
	private T5655rec t5655rec = new T5655rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private P6671par p6671par = new P6671par();
	private ItemDAOImpl itemDAO = getApplicationContext().getBean("itemDao", ItemDAOImpl.class);

	public Br612() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct02);
		callContot001();
		p6671par.parmRecord.set(bupaIO.getParmarea());
		iy.set(ZERO);
		wsaaSqlDate.set(varcom.vrcmMaxDate);
		wsaaMaxLeadDays.set(ZERO);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		
		populateMaxLeadDays();
		
		if (isEQ(p6671par.chdrnum,SPACES)) {
			wsaaChdrnumfrm.set(ZERO);
		}
		else {
			wsaaChdrnumfrm.set(p6671par.chdrnum);
		}
		if (isEQ(p6671par.chdrnum1,SPACES)) {
			wsaaChdrnumto.set("99999999");
		}
		else {
			wsaaChdrnumto.set(p6671par.chdrnum1);
		}
		wsaaSqlCompany.set(batcdorrec.company);
		datcon2rec.freqFactor.set(wsaaMaxLeadDays);
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError600();
		}
		wsaaSqlDate.set(datcon2rec.intDate2);
		sqlchdrpf1 = " SELECT  CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX" +
" FROM   " + getAppVars().getTableNameOverriden("COVRPF") + " " +
" WHERE CHDRCOY = ?" +
" AND CHDRNUM BETWEEN ? AND ?" +
" AND VALIDFLAG = ?" +
" AND NOT RRTDAT = 0" +
" AND RRTDAT <= ?" +
" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX";
		iy.set(1);
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		sqlerrorflag = false;
		try {
			sqlchdrpf1conn = getAppVars().getDBConnectionForTable(new com.csc.life.newbusiness.dataaccess.CovrpfTableDAM());
			sqlchdrpf1ps = getAppVars().prepareStatementEmbeded(sqlchdrpf1conn, sqlchdrpf1, "COVRPF");
			getAppVars().setDBString(sqlchdrpf1ps, 1, wsaaSqlCompany);
			getAppVars().setDBString(sqlchdrpf1ps, 2, wsaaChdrnumfrm);
			getAppVars().setDBString(sqlchdrpf1ps, 3, wsaaChdrnumto);
			getAppVars().setDBString(sqlchdrpf1ps, 4, wsaaSqlValidflag);
			getAppVars().setDBNumber(sqlchdrpf1ps, 5, wsaaSqlDate);
			sqlchdrpf1rs = getAppVars().executeQuery(sqlchdrpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void populateMaxLeadDays()
{
	List<Itempf> data = itemDAO.getItemsbyItemcodeLikeOperator("IT", bsprIO.getCompany().toString(), "T5655",  bprdIO.getAuthCode().toString());
	if(data.isEmpty())
	{
		syserrrec.statuz.set(Varcom.mrnf);
		StringBuilder error = new StringBuilder("IT");
		error.append(bsprIO.getCompany());
		error.append("T5655");
		error.append(bprdIO.getAuthCode());
		error.append(varcom.vrcmMaxDate).append(1);			
		syserrrec.params.set(error.toString());
		fatalError600();
	}
	Iterator<Itempf> it = data.iterator();
	while(it.hasNext())
	{
		Itempf itempf  = it.next();		
		t5655rec.t5655Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		if (isGT(t5655rec.leadDays, wsaaMaxLeadDays)) {
			wsaaMaxLeadDays.set(t5655rec.leadDays);
		}				
	}
}

protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

protected void openThreadMember1110()
	{
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(),1),iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaCoreFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz,varcom.oK)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression("CLRTMPF", SPACES);
			stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
			stringVariable1.addExpression(SPACES);
			stringVariable1.addExpression(wsaaCoreFn);
			stringVariable1.addExpression(SPACES);
			stringVariable1.addExpression(wsaaThreadMember);
			stringVariable1.setStringInto(wsaaClrtmpfError);
			syserrrec.statuz.set(wsaaStatuz);
			wsaaClrtmpfError.set(wsaaClrtmpfError);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression("OVRDBF FILE(CORE");
		stringVariable2.addExpression(iz);
		stringVariable2.addExpression(") TOFILE(");
		stringVariable2.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable2.addExpression("/");
		stringVariable2.addExpression(wsaaCoreFn);
		stringVariable2.addExpression(") MBR(");
		stringVariable2.addExpression(wsaaThreadMember);
		stringVariable2.addExpression(")");
		stringVariable2.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		if (isEQ(iz,1)) {
			core01.openOutput();
		}
		if (isEQ(iz,2)) {
			core02.openOutput();
		}
		if (isEQ(iz,3)) {
			core03.openOutput();
		}
		if (isEQ(iz,4)) {
			core04.openOutput();
		}
		if (isEQ(iz,5)) {
			core05.openOutput();
		}
		if (isEQ(iz,6)) {
			core06.openOutput();
		}
		if (isEQ(iz,7)) {
			core07.openOutput();
		}
		if (isEQ(iz,8)) {
			core08.openOutput();
		}
		if (isEQ(iz,9)) {
			core09.openOutput();
		}
		if (isEQ(iz,10)) {
			core10.openOutput();
		}
		if (isEQ(iz,11)) {
			core11.openOutput();
		}
		if (isEQ(iz,12)) {
			core12.openOutput();
		}
		if (isEQ(iz,13)) {
			core13.openOutput();
		}
		if (isEQ(iz,14)) {
			core14.openOutput();
		}
		if (isEQ(iz,15)) {
			core15.openOutput();
		}
		if (isEQ(iz,16)) {
			core16.openOutput();
		}
		if (isEQ(iz,17)) {
			core17.openOutput();
		}
		if (isEQ(iz,18)) {
			core18.openOutput();
		}
		if (isEQ(iz,19)) {
			core19.openOutput();
		}
		if (isEQ(iz,20)) {
			core20.openOutput();
		}
	}

protected void initialiseArray1500()
	{
		/*START*/
		wsaaChdrcoy[wsaaInd.toInt()].set(SPACES);
		wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaLife[wsaaInd.toInt()].set(SPACES);
		wsaaCoverage[wsaaInd.toInt()].set(SPACES);
		wsaaRider[wsaaInd.toInt()].set(SPACES);
		wsaaStatcode[wsaaInd.toInt()].set(SPACES);
		wsaaPstatcode[wsaaInd.toInt()].set(SPACES);
		wsaaPlnsfx[wsaaInd.toInt()].set(0);
		/*EXIT*/
	}

protected void readFile2000()
	{
			readFile2010();
		}

protected void readFile2010()
	{
		/*  We only need to FETCH records when a full block has been*/
		/*  processed or if the end of file was retrieved in the last*/
		/*  block.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd,1)) {
			return ;
		}
		sqlerrorflag = false;
		try {
			for (chdrpf1LoopIndex = 1; isLT(chdrpf1LoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlchdrpf1rs); chdrpf1LoopIndex++ ){
				getAppVars().getDBObject(sqlchdrpf1rs, 1, wsaaChdrcoy[chdrpf1LoopIndex], wsaaNullInd[chdrpf1LoopIndex][1]);
				getAppVars().getDBObject(sqlchdrpf1rs, 2, wsaaChdrnum[chdrpf1LoopIndex], wsaaNullInd[chdrpf1LoopIndex][2]);
				getAppVars().getDBObject(sqlchdrpf1rs, 3, wsaaLife[chdrpf1LoopIndex]);
				getAppVars().getDBObject(sqlchdrpf1rs, 4, wsaaCoverage[chdrpf1LoopIndex]);
				getAppVars().getDBObject(sqlchdrpf1rs, 5, wsaaRider[chdrpf1LoopIndex]);
				getAppVars().getDBObject(sqlchdrpf1rs, 6, wsaaPlnsfx[chdrpf1LoopIndex]);
				getAppVars().getDBObject(sqlchdrpf1rs, 7, wsaaStatcode[chdrpf1LoopIndex]);
				getAppVars().getDBObject(sqlchdrpf1rs, 8, wsaaPstatcode[chdrpf1LoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*  An SQLCODE = +100 is returned when :-*/
		/*      a) no rows are returned on the first fetch*/
		/*      b) the last row of the cursor is either in the block or is*/
		/*         the last row of the block.*/
		/*  We must continue processing to the 3000- section for case b)*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)) {
			wsspEdterror.set(varcom.endp);
		}
		//tom chi modified, bug 1030
		else if(isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)){
			wsspEdterror.set(varcom.endp);
		}
		//end
		else {
			/*  On the first entry to the program we must set up the*/
			/*  WSAA-PREV-CHDRNUM = present chdrnum.*/
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}
	}

protected void edit2500()
	{
		/*EDIT*/
		/*  Check record is required for processing.*/
		/*  Softlock the record if it is to be updated.*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		wsaaInd.set(1);
		/*  Load threads until end of array or an incomplete block is*/
		/*  detected.*/
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		/*  Re-initialise the block for next fetch and point to first row.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*  If the the CHDRNUM being processed is not equal to the*/
		/*  to the previous we should move to the next output file member.*/
		/*  The condition is here to allow for checking the last chdrnum*/
		/*  of the old block with the first of the new and to move to the*/
		/*  next CORE member to write to if they have changed.*/
		if (isNE(wsaaChdrnum[wsaaInd.toInt()],wsaaPrevChdrnum)) {
			wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
			iy.add(1);
		}
		/*  Load from storage all CORE data for the same contract until*/
		/*  the CHDRNUM on CORE has changed.*/
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| isNE(wsaaChdrnum[wsaaInd.toInt()],wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		start3200();
		nextr3288();
	}

protected void start3200()
	{
		corepfData.chdrcoy.set(wsaaChdrcoy[wsaaInd.toInt()]);
		corepfData.chdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
		corepfData.life.set(wsaaLife[wsaaInd.toInt()]);
		corepfData.coverage.set(wsaaCoverage[wsaaInd.toInt()]);
		corepfData.rider.set(wsaaRider[wsaaInd.toInt()]);
		corepfData.planSuffix.set(wsaaPlnsfx[wsaaInd.toInt()]);
		chdrrnlIO.setParams(SPACES);
		chdrrnlIO.setChdrcoy(wsaaChdrcoy[wsaaInd.toInt()]);
		chdrrnlIO.setChdrnum(wsaaChdrnum[wsaaInd.toInt()]);
		chdrrnlIO.setFormat(chdrrnlrec);
		chdrrnlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrrnlIO);
		if (isNE(chdrrnlIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrrnlIO.getParams());
			syserrrec.statuz.set(chdrrnlIO.getStatuz());
			fatalError600();
		}
		corepfData.statcode.set(chdrrnlIO.getStatcode());
		corepfData.pstatcode.set(chdrrnlIO.getPstatcode());
		if (isGT(iy,bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		if (isEQ(iy,1)) {
			core01.write(corepfData);
		}
		if (isEQ(iy,2)) {
			core02.write(corepfData);
		}
		if (isEQ(iy,3)) {
			core03.write(corepfData);
		}
		if (isEQ(iy,4)) {
			core04.write(corepfData);
		}
		if (isEQ(iy,5)) {
			core05.write(corepfData);
		}
		if (isEQ(iy,6)) {
			core06.write(corepfData);
		}
		if (isEQ(iy,7)) {
			core07.write(corepfData);
		}
		if (isEQ(iy,8)) {
			core08.write(corepfData);
		}
		if (isEQ(iy,9)) {
			core09.write(corepfData);
		}
		if (isEQ(iy,10)) {
			core10.write(corepfData);
		}
		if (isEQ(iy,11)) {
			core11.write(corepfData);
		}
		if (isEQ(iy,12)) {
			core12.write(corepfData);
		}
		if (isEQ(iy,13)) {
			core13.write(corepfData);
		}
		if (isEQ(iy,14)) {
			core14.write(corepfData);
		}
		if (isEQ(iy,15)) {
			core15.write(corepfData);
		}
		if (isEQ(iy,16)) {
			core16.write(corepfData);
		}
		if (isEQ(iy,17)) {
			core17.write(corepfData);
		}
		if (isEQ(iy,18)) {
			core18.write(corepfData);
		}
		if (isEQ(iy,19)) {
			core19.write(corepfData);
		}
		if (isEQ(iy,20)) {
			core20.write(corepfData);
		}
		/*    Log the number of extacted records.*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct01);
		callContot001();
	}

protected void nextr3288()
	{
		/*  If the record written out above is the last in the array*/
		/*  we have already stored it to compare it with the first of the*/
		/*  next set of records to be fetched from the instalment file.*/
		/*  Set up the array for the next block of records.*/
		/*  Otherwise process the next record in the array.*/
		wsaaInd.add(1);
		/*  Check for an incomplete block retrieved.*/		
		if (isLTE(wsaaInd,wsaaRowsInBlock) && isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)) {
			wsaaEofInBlock.set("Y");
		}
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*  Close the open files and remove the overrides.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlchdrpf1conn, sqlchdrpf1ps, sqlchdrpf1rs);
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		close4110();
	}

protected void close4110()
	{
		if (isEQ(iz,1)) {
			core01.close();
		}
		if (isEQ(iz,2)) {
			core02.close();
		}
		if (isEQ(iz,3)) {
			core03.close();
		}
		if (isEQ(iz,4)) {
			core04.close();
		}
		if (isEQ(iz,5)) {
			core05.close();
		}
		if (isEQ(iz,6)) {
			core06.close();
		}
		if (isEQ(iz,7)) {
			core07.close();
		}
		if (isEQ(iz,8)) {
			core08.close();
		}
		if (isEQ(iz,9)) {
			core09.close();
		}
		if (isEQ(iz,10)) {
			core10.close();
		}
		if (isEQ(iz,11)) {
			core11.close();
		}
		if (isEQ(iz,12)) {
			core12.close();
		}
		if (isEQ(iz,13)) {
			core13.close();
		}
		if (isEQ(iz,14)) {
			core14.close();
		}
		if (isEQ(iz,15)) {
			core15.close();
		}
		if (isEQ(iz,16)) {
			core16.close();
		}
		if (isEQ(iz,17)) {
			core17.close();
		}
		if (isEQ(iz,18)) {
			core18.close();
		}
		if (isEQ(iz,19)) {
			core19.close();
		}
		if (isEQ(iz,20)) {
			core20.close();
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
