package com.csc.life.regularprocessing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:05:00
 * Description:
 * Copybook name: INCRSREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Incrsrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData increaseRec = new FixedLengthStringData(303);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(increaseRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(increaseRec, 5);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(increaseRec, 9);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(increaseRec, 10);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(increaseRec, 18);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(increaseRec, 20);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(increaseRec, 22);
  	public PackedDecimalData plnsfx = new PackedDecimalData(4, 0).isAPartOf(increaseRec, 24);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(increaseRec, 27);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(increaseRec, 32);
  	public FixedLengthStringData annvmeth = new FixedLengthStringData(4).isAPartOf(increaseRec, 36);
  	public PackedDecimalData origsum = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 40);
  	public PackedDecimalData lastsum = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 49);
  	public PackedDecimalData currsum = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 58);
  	public PackedDecimalData newsum = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 67);
  	public PackedDecimalData originst01 = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 76);
  	public PackedDecimalData lastinst01 = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 85);
  	public PackedDecimalData currinst01 = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 94);
  	public PackedDecimalData newinst01 = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 103);
  	public PackedDecimalData originst02 = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 112);
  	public PackedDecimalData lastinst02 = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 121);
  	public PackedDecimalData currinst02 = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 130);
  	public PackedDecimalData newinst02 = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 139);
  	public PackedDecimalData originst03 = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 148);
  	public PackedDecimalData lastinst03 = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 157);
  	public PackedDecimalData currinst03 = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 166);
  	public PackedDecimalData newinst03 = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 175);
  	public PackedDecimalData pctinc = new PackedDecimalData(5, 2).isAPartOf(increaseRec, 184);
  	public PackedDecimalData maxpcnt = new PackedDecimalData(5, 2).isAPartOf(increaseRec, 187);
  	public PackedDecimalData minpcnt = new PackedDecimalData(5, 2).isAPartOf(increaseRec, 190);
  	public ZonedDecimalData fixdtrm = new ZonedDecimalData(3, 0).isAPartOf(increaseRec, 193);
  	public PackedDecimalData rcesdte = new PackedDecimalData(8, 0).isAPartOf(increaseRec, 196);
  	public PackedDecimalData occdate = new PackedDecimalData(8, 0).isAPartOf(increaseRec, 201);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(increaseRec, 206);
  	public FixedLengthStringData mortcls = new FixedLengthStringData(1).isAPartOf(increaseRec, 209);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(increaseRec, 210);
  	public FixedLengthStringData mop = new FixedLengthStringData(2).isAPartOf(increaseRec, 212);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(increaseRec, 214);
  	public FixedLengthStringData autoincreaseindicator = new FixedLengthStringData(1).isAPartOf(increaseRec, 215);
  	//ILIFE-6569
  	public FixedLengthStringData simpleInd = new FixedLengthStringData(1).isAPartOf(increaseRec, 216);
  	public PackedDecimalData zstpduty01 = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 217);
  	public PackedDecimalData zstpduty02 = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 234);
  	public PackedDecimalData zstpduty03 = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 251);
	public FixedLengthStringData compoundInd =  new FixedLengthStringData(1).isAPartOf(increaseRec, 268);
	public PackedDecimalData calcprem = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 269);
	public PackedDecimalData inputPrevPrem = new PackedDecimalData(17, 2).isAPartOf(increaseRec, 286);
	public void initialize() {
		COBOLFunctions.initialize(increaseRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		increaseRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}