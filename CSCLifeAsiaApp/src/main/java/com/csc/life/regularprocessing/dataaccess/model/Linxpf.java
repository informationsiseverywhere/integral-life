package com.csc.life.regularprocessing.dataaccess.model;

import java.math.BigDecimal;



public class Linxpf {
    private String chdrnum = "";
    private String chdrcoy = "";
    private int instfrom = 0;
    private String cntcurr = "";
    private String billcurr = "";
    private int payrseqno = 0;
    private BigDecimal cbillamt = BigDecimal.ZERO;
    private int billcd = 0;
    private String billchnl ="";
    private BigDecimal instamt01 = BigDecimal.ZERO ;
    private BigDecimal instamt02 = BigDecimal.ZERO;
    private BigDecimal instamt03 = BigDecimal.ZERO;
    private BigDecimal instamt04 = BigDecimal.ZERO;
    private BigDecimal instamt05 = BigDecimal.ZERO;
    private BigDecimal instamt06 = BigDecimal.ZERO;
    private String instfreq = "";
    private String proraterec = "";
    
    private String chdrCnttype ="";
    private int chdrOccdate = 0;
    private String chdrStatcode = "";
    private String chdrPstatcode = "";
    private int chdrTranno = 0;
    private BigDecimal chdrInsttot01 = BigDecimal.ZERO;
    private BigDecimal chdrInsttot02 = BigDecimal.ZERO;
    private BigDecimal chdrInsttot03 = BigDecimal.ZERO;
    private BigDecimal chdrInsttot04 = BigDecimal.ZERO;
    private BigDecimal chdrInsttot05 = BigDecimal.ZERO;
    private BigDecimal chdrInsttot06 = BigDecimal.ZERO;
    private BigDecimal chdrOutstamt = BigDecimal.ZERO;
    private String chdrAgntnum = "";
    private String chdrCntcurr = "";
    private int chdrPtdate = 0;
    private String chdrRnwlsupr = "";
    private int chdrRnwlspfrom = 0;
    private int chdrRnwlspto = 0 ;
    private String chdrCowncoy = "";
    private String chdrCownnum = "";
    private String chdrCntbranch = "";
    private String chdrAgntcoy = "";
    private String chdrRegister = "";
    private String chdrchdrpfx = "";
    private String chdrnlgflg = "";
    
    private String payrTaxrelmth = "";
    private int payrIncomeseqno = 0;
    private int payrTranno = 0;
    private BigDecimal payrOutstamt = BigDecimal.ZERO;
    private String payrBillchnl = "";
    private String payrMandref = "";
    private String payrCntcurr = "";
    private String payrBillfreq = "";
    private int payrBillcd = 0;
    private int payrPtdate = 0;
    private int payrBtdate = 0;
    private int payrNextDate = 0;
    private int payOrgbillcd = 0;
    		
    private int linsInstto = 0;
    
    
    private long chdrUniqueNumber = 0l;
    private long linsUniqueNumber = 0l;
    private long payrUniqueNumber = 0l;
    
    private int aglfDtetrm;
    private int aglfDteexp;
    private int aglfDteapp;
    private String hcsdZdivopt;
    private String hcsdZcshdivmth;
    private int hdisHcapndt;
    private BigDecimal prorcntfee;//ILIFE-8509
    private BigDecimal proramt;
    
    public int getHdisHcapndt() {
		return hdisHcapndt;
	}
	public void setHdisHcapndt(int hdisHcapndt) {
		this.hdisHcapndt = hdisHcapndt;
	}
	public String getHcsdZdivopt() {
		return hcsdZdivopt;
	}
	public void setHcsdZdivopt(String hcsdZdivopt) {
		this.hcsdZdivopt = hcsdZdivopt;
	}
	public String getHcsdZcshdivmth() {
		return hcsdZcshdivmth;
	}
	public void setHcsdZcshdivmth(String hcsdZcshdivmth) {
		this.hcsdZcshdivmth = hcsdZcshdivmth;
	}
	public int getAglfDtetrm() {
		return aglfDtetrm;
	}
	public void setAglfDtetrm(int aglfDtetrm) {
		this.aglfDtetrm = aglfDtetrm;
	}
	public int getAglfDteexp() {
		return aglfDteexp;
	}
	public void setAglfDteexp(int aglfDteexp) {
		this.aglfDteexp = aglfDteexp;
	}
	public int getAglfDteapp() {
		return aglfDteapp;
	}
	public void setAglfDteapp(int aglfDteapp) {
		this.aglfDteapp = aglfDteapp;
	}
	public long getPayrUniqueNumber() {
        return payrUniqueNumber;
    }
    public void setPayrUniqueNumber(long payrUniqueNumber) {
        this.payrUniqueNumber = payrUniqueNumber;
    }
    public long getLinsUniqueNumber() {
        return linsUniqueNumber;
    }
    public void setLinsUniqueNumber(long linsUniqueNumber) {
        this.linsUniqueNumber = linsUniqueNumber;
    }
    public long getChdrUniqueNumber() {
        return chdrUniqueNumber;
    }
    public void setChdrUniqueNumber(long chdrUniqueNumber) {
        this.chdrUniqueNumber = chdrUniqueNumber;
    }
    public int getLinsInstto() {
        return linsInstto;
    }
    public void setLinsInstto(int linsInstto) {
        this.linsInstto = linsInstto;
    }
    public String getPayrTaxrelmth() {
        return payrTaxrelmth;
    }
    public int getPayrIncomeseqno() {
        return payrIncomeseqno;
    }
    public int getPayrTranno() {
        return payrTranno;
    }
    public BigDecimal getPayrOutstamt() {
        return payrOutstamt;
    }
    public String getPayrBillchnl() {
        return payrBillchnl;
    }
    public String getPayrMandref() {
        return payrMandref;
    }
    public String getPayrCntcurr() {
        return payrCntcurr;
    }
    public String getPayrBillfreq() {
        return payrBillfreq;
    }
    public int getPayrBillcd() {
        return payrBillcd;
    }
    public int getPayrPtdate() {
        return payrPtdate;
    }
    public void setPayrTaxrelmth(String payrTaxrelmth) {
        this.payrTaxrelmth = payrTaxrelmth;
    }
    public void setPayrIncomeseqno(int payrIncomeseqno) {
        this.payrIncomeseqno = payrIncomeseqno;
    }
    public void setPayrTranno(int payrTranno) {
        this.payrTranno = payrTranno;
    }
    public void setPayrOutstamt(BigDecimal payrOutstamt) {
        this.payrOutstamt = payrOutstamt;
    }
    public void setPayrBillchnl(String payrBillchnl) {
        this.payrBillchnl = payrBillchnl;
    }
    public void setPayrMandref(String payrMandref) {
        this.payrMandref = payrMandref;
    }
    public void setPayrCntcurr(String payrCntcurr) {
        this.payrCntcurr = payrCntcurr;
    }
    public void setPayrBillfreq(String payrBillfreq) {
        this.payrBillfreq = payrBillfreq;
    }
    public void setPayrBillcd(int payrBillcd) {
        this.payrBillcd = payrBillcd;
    }
    public void setPayrPtdate(int payrPtdate) {
        this.payrPtdate = payrPtdate;
    }
    public int getPayrBtdate() {
		return payrBtdate;
	}
	public void setPayrBtdate(int payrBtdate) {
		this.payrBtdate = payrBtdate;
	}
	public int getPayrNextDate() {
		return payrNextDate;
	}
	public void setPayrNextDate(int payrNextDate) {
		this.payrNextDate = payrNextDate;
	}
	public String getChdrchdrpfx() {
        return chdrchdrpfx;
    }
    public void setChdrchdrpfx(String chdrchdrpfx) {
        this.chdrchdrpfx = chdrchdrpfx;
    }
    public String getChdrCnttype() {
        return chdrCnttype;
    }
    public int getChdrOccdate() {
        return chdrOccdate;
    }
    public String getChdrStatcode() {
        return chdrStatcode;
    }
    public String getChdrPstatcode() {
        return chdrPstatcode;
    }
    public int getChdrTranno() {
        return chdrTranno;
    }
    public BigDecimal getChdrInsttot01() {
        return chdrInsttot01;
    }
    public BigDecimal getChdrInsttot02() {
        return chdrInsttot02;
    }
    public BigDecimal getChdrInsttot03() {
        return chdrInsttot03;
    }
    public BigDecimal getChdrInsttot04() {
        return chdrInsttot04;
    }
    public BigDecimal getChdrInsttot05() {
        return chdrInsttot05;
    }
    public BigDecimal getChdrInsttot06() {
        return chdrInsttot06;
    }
    public BigDecimal getChdrOutstamt() {
        return chdrOutstamt;
    }
    public String getChdrAgntnum() {
        return chdrAgntnum;
    }
    public String getChdrCntcurr() {
        return chdrCntcurr;
    }
    public int getChdrPtdate() {
        return chdrPtdate;
    }
    public String getChdrRnwlsupr() {
        return chdrRnwlsupr;
    }
    public int getChdrRnwlspfrom() {
        return chdrRnwlspfrom;
    }
    public int getChdrRnwlspto() {
        return chdrRnwlspto;
    }
    public String getChdrCowncoy() {
        return chdrCowncoy;
    }
    public String getChdrCownnum() {
        return chdrCownnum;
    }
    public String getChdrCntbranch() {
        return chdrCntbranch;
    }
    public String getChdrAgntcoy() {
        return chdrAgntcoy;
    }
    public String getChdrRegister() {
        return chdrRegister;
    }
    public void setChdrCnttype(String chdrCnttype) {
        this.chdrCnttype = chdrCnttype;
    }
    public void setChdrOccdate(int chdrOccdate) {
        this.chdrOccdate = chdrOccdate;
    }
    public void setChdrStatcode(String chdrStatcode) {
        this.chdrStatcode = chdrStatcode;
    }
    public void setChdrPstatcode(String chdrPstatcode) {
        this.chdrPstatcode = chdrPstatcode;
    }
    public void setChdrTranno(int chdrTranno) {
        this.chdrTranno = chdrTranno;
    }
    public void setChdrInsttot01(BigDecimal chdrInsttot01) {
        this.chdrInsttot01 = chdrInsttot01;
    }
    public void setChdrInsttot02(BigDecimal chdrInsttot02) {
        this.chdrInsttot02 = chdrInsttot02;
    }
    public void setChdrInsttot03(BigDecimal chdrInsttot03) {
        this.chdrInsttot03 = chdrInsttot03;
    }
    public void setChdrInsttot04(BigDecimal chdrInsttot04) {
        this.chdrInsttot04 = chdrInsttot04;
    }
    public void setChdrInsttot05(BigDecimal chdrInsttot05) {
        this.chdrInsttot05 = chdrInsttot05;
    }
    public void setChdrInsttot06(BigDecimal chdrInsttot06) {
        this.chdrInsttot06 = chdrInsttot06;
    }
    public void setChdrOutstamt(BigDecimal chdrOutstamt) {
        this.chdrOutstamt = chdrOutstamt;
    }
    public void setChdrAgntnum(String chdrAgntnum) {
        this.chdrAgntnum = chdrAgntnum;
    }
    public void setChdrCntcurr(String chdrCntcurr) {
        this.chdrCntcurr = chdrCntcurr;
    }
    public void setChdrPtdate(int chdrPtdate) {
        this.chdrPtdate = chdrPtdate;
    }
    public void setChdrRnwlsupr(String chdrRnwlsupr) {
        this.chdrRnwlsupr = chdrRnwlsupr;
    }
    public void setChdrRnwlspfrom(int chdrRnwlspfrom) {
        this.chdrRnwlspfrom = chdrRnwlspfrom;
    }
    public void setChdrRnwlspto(int chdrRnwlspto) {
        this.chdrRnwlspto = chdrRnwlspto;
    }
    public void setChdrCowncoy(String chdrCowncoy) {
        this.chdrCowncoy = chdrCowncoy;
    }
    public void setChdrCownnum(String chdrCownnum) {
        this.chdrCownnum = chdrCownnum;
    }
    public void setChdrCntbranch(String chdrCntbranch) {
        this.chdrCntbranch = chdrCntbranch;
    }
    public void setChdrAgntcoy(String chdrAgntcoy) {
        this.chdrAgntcoy = chdrAgntcoy;
    }
    public void setChdrRegister(String chdrRegister) {
        this.chdrRegister = chdrRegister;
    }
    public String getChdrnum() {
        return chdrnum;
    }
    public String getChdrcoy() {
        return chdrcoy;
    }
    public int getInstfrom() {
        return instfrom;
    }
    public String getCntcurr() {
        return cntcurr;
    }
    public String getBillcurr() {
        return billcurr;
    }
    public int getPayrseqno() {
        return payrseqno;
    }
    public BigDecimal getCbillamt() {
        return cbillamt;
    }
    public int getBillcd() {
        return billcd;
    }
    public String getBillchnl() {
        return billchnl;
    }
    public BigDecimal getInstamt01() {
        return instamt01;
    }
    public BigDecimal getInstamt02() {
        return instamt02;
    }
    public BigDecimal getInstamt03() {
        return instamt03;
    }
    public BigDecimal getInstamt04() {
        return instamt04;
    }
    public BigDecimal getInstamt05() {
        return instamt05;
    }
    public BigDecimal getInstamt06() {
        return instamt06;
    }
    public String getInstfreq() {
        return instfreq;
    }
    public String getProraterec() {
        return proraterec;
    }
    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }
    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }
    public void setInstfrom(int instfrom) {
        this.instfrom = instfrom;
    }
    public void setCntcurr(String cntcurr) {
        this.cntcurr = cntcurr;
    }
    public void setBillcurr(String billcurr) {
        this.billcurr = billcurr;
    }
    public void setPayrseqno(int payrseqno) {
        this.payrseqno = payrseqno;
    }
    public void setCbillamt(BigDecimal cbillamt) {
        this.cbillamt = cbillamt;
    }
    public void setBillcd(int billcd) {
        this.billcd = billcd;
    }
    public void setBillchnl(String billchnl) {
        this.billchnl = billchnl;
    }
    public void setInstamt01(BigDecimal instamt01) {
        this.instamt01 = instamt01;
    }
    public void setInstamt02(BigDecimal instamt02) {
        this.instamt02 = instamt02;
    }
    public void setInstamt03(BigDecimal instamt03) {
        this.instamt03 = instamt03;
    }
    public void setInstamt04(BigDecimal instamt04) {
        this.instamt04 = instamt04;
    }
    public void setInstamt05(BigDecimal instamt05) {
        this.instamt05 = instamt05;
    }
    public void setInstamt06(BigDecimal instamt06) {
        this.instamt06 = instamt06;
    }
    public void setInstfreq(String instfreq) {
        this.instfreq = instfreq;
    }
    public void setProraterec(String proraterec) {
        this.proraterec = proraterec;
    }
	public BigDecimal getProrcntfee() {//ILIFE-8509
		return prorcntfee;
	}
	public void setProrcntfee(BigDecimal prorcntfee) {
		this.prorcntfee = prorcntfee;
	}
	public BigDecimal getProramt() {
		return proramt;
	}
	public void setProramt(BigDecimal proramt) {
		this.proramt = proramt;
	}
	public String getChdrnlgflg() {
		return chdrnlgflg;
	}
	public void setChdrnlgflg(String chdrnlgflg) {
		this.chdrnlgflg = chdrnlgflg;
	}
	public int getPayOrgbillcd() {
		return payOrgbillcd;
	}
	public void setPayOrgbillcd(int payOrgbillcd) {
		this.payOrgbillcd = payOrgbillcd;
	}
	
}