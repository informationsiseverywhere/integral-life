package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.PayxTemppf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface B5349DAO extends BaseDAO<PayxTemppf> {
	public List<PayxTemppf> loadDataByBatch(int batchID, String threadNumber);
	public int populateB5349Temp(int batchExtractSize, String payxtempTable, String memberName);
	public void initializeB5349Temp();
	public int populateB5349TempJpn(int batchExtractSize, String payxtempTable, String memberName);
	public int getB5349DataCount(String memberName);
}
