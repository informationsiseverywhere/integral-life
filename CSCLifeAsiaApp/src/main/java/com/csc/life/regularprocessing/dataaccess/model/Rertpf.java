package com.csc.life.regularprocessing.dataaccess.model;

import java.math.BigDecimal;
import java.util.Date;

public class Rertpf {

	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	private int effdate;
	private String validflag;
	private BigDecimal lastInst;
	private BigDecimal newinst;
	private BigDecimal zblastinst;
	private BigDecimal zbnewinst;
	private BigDecimal zllastinst;
	private BigDecimal zlnewinst;
	private BigDecimal lastzstpduty;
	private BigDecimal newzstpduty;
	private String usrprf;
	private String jobnm;
	private Date datime;
	private Date created_at;
	private int tranno;
	private BigDecimal commprem;
	
	public Rertpf() {
		super();
	}
	public Rertpf(Rertpf rertpf) {
		this.uniqueNumber=rertpf.uniqueNumber;
		this.chdrcoy = rertpf.chdrcoy;
		this.chdrnum=rertpf.chdrnum;
		this.life=rertpf.life;
		this.jlife=rertpf.jlife;
		this.coverage=rertpf.coverage;
		this.rider=rertpf.rider;
		this.plnsfx=rertpf.plnsfx;
		this.effdate=rertpf.effdate;
		this.validflag=rertpf.validflag;
		this.lastInst=rertpf.lastInst;
		this.newinst=rertpf.newinst;
		this.zblastinst=rertpf.zblastinst;
		this.zbnewinst=rertpf.zbnewinst;
		this.zllastinst=rertpf.zllastinst;
		this.zlnewinst=rertpf.zlnewinst;
        this.lastzstpduty=rertpf.lastzstpduty;
        this.newzstpduty=rertpf.newzstpduty;
        this.usrprf=rertpf.usrprf;
        this.jobnm=rertpf.jobnm;
        this.created_at=rertpf.created_at;
        this.datime=rertpf.datime;
        this.tranno=rertpf.tranno;
        this.commprem=rertpf.commprem;
	}
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getJlife() {
		return jlife;
	}
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public Integer getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public BigDecimal getLastInst() {
		return lastInst;
	}
	public void setLastInst(BigDecimal lastInst) {
		this.lastInst = lastInst;
	}
	public BigDecimal getNewinst() {
		return newinst;
	}
	public void setNewinst(BigDecimal newinst) {
		this.newinst = newinst;
	}
	public BigDecimal getZblastinst() {
		return zblastinst;
	}
	public void setZblastinst(BigDecimal zblastinst) {
		this.zblastinst = zblastinst;
	}
	public BigDecimal getZbnewinst() {
		return zbnewinst;
	}
	public void setZbnewinst(BigDecimal zbnewinst) {
		this.zbnewinst = zbnewinst;
	}
	public BigDecimal getZllastinst() {
		return zllastinst;
	}
	public void setZllastinst(BigDecimal zllastinst) {
		this.zllastinst = zllastinst;
	}
	public BigDecimal getZlnewinst() {
		return zlnewinst;
	}
	public void setZlnewinst(BigDecimal zlnewinst) {
		this.zlnewinst = zlnewinst;
	}
	public BigDecimal getLastzstpduty() {
		return lastzstpduty;
	}
	public void setLastzstpduty(BigDecimal lastzstpduty) {
		this.lastzstpduty = lastzstpduty;
	}
	public BigDecimal getNewzstpduty() {
		return newzstpduty;
	}
	public void setNewzstpduty(BigDecimal newzstpduty) {
		this.newzstpduty = newzstpduty;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Date getDatime() {
		return datime;
	}
	public void setDatime(Date datime) {
		this.datime = datime;
	}
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public BigDecimal getCommprem() {
		return commprem;
	}
	public void setCommprem(BigDecimal commprem) {
		this.commprem = commprem;
	}
	

}
