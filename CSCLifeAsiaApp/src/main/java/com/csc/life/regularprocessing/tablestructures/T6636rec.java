package com.csc.life.regularprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:38
 * Description:
 * Copybook name: T6636REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6636rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6636Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData bonuss = new FixedLengthStringData(70).isAPartOf(t6636Rec, 0);
  	public ZonedDecimalData[] bonus = ZDArrayPartOfStructure(10, 7, 2, bonuss, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(70).isAPartOf(bonuss, 0, FILLER_REDEFINE);
  	public ZonedDecimalData bonus01 = new ZonedDecimalData(7, 2).isAPartOf(filler, 0);
  	public ZonedDecimalData bonus02 = new ZonedDecimalData(7, 2).isAPartOf(filler, 7);
  	public ZonedDecimalData bonus03 = new ZonedDecimalData(7, 2).isAPartOf(filler, 14);
  	public ZonedDecimalData bonus04 = new ZonedDecimalData(7, 2).isAPartOf(filler, 21);
  	public ZonedDecimalData bonus05 = new ZonedDecimalData(7, 2).isAPartOf(filler, 28);
  	public ZonedDecimalData bonus06 = new ZonedDecimalData(7, 2).isAPartOf(filler, 35);
  	public ZonedDecimalData bonus07 = new ZonedDecimalData(7, 2).isAPartOf(filler, 42);
  	public ZonedDecimalData bonus08 = new ZonedDecimalData(7, 2).isAPartOf(filler, 49);
  	public ZonedDecimalData bonus09 = new ZonedDecimalData(7, 2).isAPartOf(filler, 56);
  	public ZonedDecimalData bonus10 = new ZonedDecimalData(7, 2).isAPartOf(filler, 63);
  	public FixedLengthStringData riskunits = new FixedLengthStringData(12).isAPartOf(t6636Rec, 70);
  	public ZonedDecimalData[] riskunit = ZDArrayPartOfStructure(2, 6, 0, riskunits, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(12).isAPartOf(riskunits, 0, FILLER_REDEFINE);
  	public ZonedDecimalData riskunit01 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData riskunit02 = new ZonedDecimalData(6, 0).isAPartOf(filler1, 6);
  	public FixedLengthStringData sumasss = new FixedLengthStringData(70).isAPartOf(t6636Rec, 82);
  	public ZonedDecimalData[] sumass = ZDArrayPartOfStructure(10, 7, 2, sumasss, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(70).isAPartOf(sumasss, 0, FILLER_REDEFINE);
  	public ZonedDecimalData sumass01 = new ZonedDecimalData(7, 2).isAPartOf(filler2, 0);
  	public ZonedDecimalData sumass02 = new ZonedDecimalData(7, 2).isAPartOf(filler2, 7);
  	public ZonedDecimalData sumass03 = new ZonedDecimalData(7, 2).isAPartOf(filler2, 14);
  	public ZonedDecimalData sumass04 = new ZonedDecimalData(7, 2).isAPartOf(filler2, 21);
  	public ZonedDecimalData sumass05 = new ZonedDecimalData(7, 2).isAPartOf(filler2, 28);
  	public ZonedDecimalData sumass06 = new ZonedDecimalData(7, 2).isAPartOf(filler2, 35);
  	public ZonedDecimalData sumass07 = new ZonedDecimalData(7, 2).isAPartOf(filler2, 42);
  	public ZonedDecimalData sumass08 = new ZonedDecimalData(7, 2).isAPartOf(filler2, 49);
  	public ZonedDecimalData sumass09 = new ZonedDecimalData(7, 2).isAPartOf(filler2, 56);
  	public ZonedDecimalData sumass10 = new ZonedDecimalData(7, 2).isAPartOf(filler2, 63);
  	public FixedLengthStringData yrsinfs = new FixedLengthStringData(30).isAPartOf(t6636Rec, 152);
  	public ZonedDecimalData[] yrsinf = ZDArrayPartOfStructure(10, 3, 0, yrsinfs, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(30).isAPartOf(yrsinfs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData yrsinf01 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 0);
  	public ZonedDecimalData yrsinf02 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 3);
  	public ZonedDecimalData yrsinf03 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 6);
  	public ZonedDecimalData yrsinf04 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 9);
  	public ZonedDecimalData yrsinf05 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 12);
  	public ZonedDecimalData yrsinf06 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 15);
  	public ZonedDecimalData yrsinf07 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 18);
  	public ZonedDecimalData yrsinf08 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 21);
  	public ZonedDecimalData yrsinf09 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 24);
  	public ZonedDecimalData yrsinf10 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 27);
  	public ZonedDecimalData ztrmbpc = new ZonedDecimalData(5, 2).isAPartOf(t6636Rec, 182);
  	public ZonedDecimalData ztrmdsc = new ZonedDecimalData(5, 2).isAPartOf(t6636Rec, 187);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(308).isAPartOf(t6636Rec, 192, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6636Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6636Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}