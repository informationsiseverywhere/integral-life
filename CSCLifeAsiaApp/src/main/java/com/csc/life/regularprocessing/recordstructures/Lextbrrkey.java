package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:03
 * Description:
 * Copybook name: LEXTBRRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lextbrrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lextbrrFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lextbrrKey = new FixedLengthStringData(64).isAPartOf(lextbrrFileKey, 0, REDEFINE);
  	public FixedLengthStringData lextbrrChdrcoy = new FixedLengthStringData(1).isAPartOf(lextbrrKey, 0);
  	public FixedLengthStringData lextbrrChdrnum = new FixedLengthStringData(8).isAPartOf(lextbrrKey, 1);
  	public FixedLengthStringData lextbrrLife = new FixedLengthStringData(2).isAPartOf(lextbrrKey, 9);
  	public FixedLengthStringData lextbrrCoverage = new FixedLengthStringData(2).isAPartOf(lextbrrKey, 11);
  	public FixedLengthStringData lextbrrRider = new FixedLengthStringData(2).isAPartOf(lextbrrKey, 13);
  	public PackedDecimalData lextbrrExtCessDate = new PackedDecimalData(8, 0).isAPartOf(lextbrrKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(lextbrrKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lextbrrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lextbrrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}