package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:12
 * Description:
 * Copybook name: LINSRVBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Linsrvbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData linsrvbFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData linsrvbKey = new FixedLengthStringData(256).isAPartOf(linsrvbFileKey, 0, REDEFINE);
  	public FixedLengthStringData linsrvbChdrcoy = new FixedLengthStringData(1).isAPartOf(linsrvbKey, 0);
  	public FixedLengthStringData linsrvbChdrnum = new FixedLengthStringData(8).isAPartOf(linsrvbKey, 1);
  	public PackedDecimalData linsrvbInstfrom = new PackedDecimalData(8, 0).isAPartOf(linsrvbKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(242).isAPartOf(linsrvbKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(linsrvbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		linsrvbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}