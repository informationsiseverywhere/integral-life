package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.WopxpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Wopxpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class WopxpfDAOImpl extends BaseDAOImpl<Wopxpf> implements WopxpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(WopxpfDAOImpl.class);

	public List<Wopxpf> searchWopxRecord(String tableId, String memName, int batchExtractSize, int batchID) {
		StringBuilder sqlWopxSelect = new StringBuilder();
		sqlWopxSelect.append(" SELECT * FROM ( ");
		sqlWopxSelect.append(" SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX ");
		sqlWopxSelect.append("   ,FLOOR((ROW_NUMBER()OVER( ORDER BY UNIQUE_NUMBER DESC)-1)/?) BATCHNUM ");
		sqlWopxSelect.append("  FROM ");
		sqlWopxSelect.append(tableId);
		sqlWopxSelect.append("   WHERE MEMBER_NAME = ? ");
		sqlWopxSelect.append(" ) MAIN WHERE BATCHNUM = ? ");

		PreparedStatement psWopxSelect = getPrepareStatement(sqlWopxSelect.toString());
		ResultSet sqlwopxpf1rs = null;
		List<Wopxpf> wopxpfList = new ArrayList<>();
		try {
			psWopxSelect.setInt(1, batchExtractSize);
			psWopxSelect.setString(2, memName);
			psWopxSelect.setInt(3, batchID);

			sqlwopxpf1rs = executeQuery(psWopxSelect);
			while (sqlwopxpf1rs.next()) {
				Wopxpf wopxpf = new Wopxpf();
				wopxpf.setChdrcoy(sqlwopxpf1rs.getString("chdrcoy"));
				wopxpf.setChdrnum(sqlwopxpf1rs.getString("chdrnum"));
				wopxpf.setLife(sqlwopxpf1rs.getString("life"));
				wopxpf.setCoverage(sqlwopxpf1rs.getString("coverage"));
				wopxpf.setRider(sqlwopxpf1rs.getString("rider"));
				wopxpf.setPlanSuffix(sqlwopxpf1rs.getInt("plnsfx"));
				wopxpf.setUniqueNumber(sqlwopxpf1rs.getLong("UNIQUE_NUMBER"));
				wopxpfList.add(wopxpf);
			}
		} catch (SQLException e) {
			LOGGER.error("searchWopxRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psWopxSelect, sqlwopxpf1rs);
		}
		return wopxpfList;
	}
}
