/*
 * File: Br615.java
 * Date: 29 August 2009 22:25:54
 * Author: Quipoz Limited
 *
 * Class transformed from BR615.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.procedures.Crtundwrt;
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.flexiblepremium.dataaccess.dao.AgcmpfDAO;
import com.csc.life.flexiblepremium.dataaccess.model.Agcmpf;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.linkage.procedures.LinkageInfoService;
import com.csc.life.newbusiness.dataaccess.dao.PcddpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Pcddpf;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.productdefinition.dataaccess.dao.AnnypfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.productdefinition.dataaccess.model.Annypf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.WopxpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.regularprocessing.dataaccess.model.Wopxpf;
import com.csc.life.regularprocessing.tablestructures.T5399rec;
import com.csc.life.regularprocessing.tablestructures.T5534rec;
import com.csc.life.regularprocessing.tablestructures.T5655rec;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Contot;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;
/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 2005.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*                   Premium Waiver Re-rating process
*            ---------------------------------------------
*            Rewrite of BH592 with Multi-Thread capability
*
*    This is a program as one of the process in Renewals for
*    rerating WOP components of contracts. The contracts
*    fall in the criteria to re-rate, that is when re-rate date
*    is before the effective date of the run and maximum lead
*    days in T5655. New COVR records are written with the
*    re-rated premium and new re-rate dates if applicable.
*
*    The reason that this program is introduced is because
*    re-rating on individual component will affect the total sum
*    insured for WOP, also the automatic increase processes will
*    result in new installment premiums for components, therefore
*    this program is run after all relevant coverages have been
*    re-rated, to write new COVR records for WOP components.
*
*    While reading through COVR records, skip processing on non
*    WOP components. For WOP components, find the earliest
*    re-rate dates from all the related components. Also,
*    accumulate for the correct WOP, SI before calling the
*    premium calculation routine for new renewal premium.
*
***********************************************************************
* </pre>
*/
public class Br615 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR615");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaDateFound = new FixedLengthStringData(1).init("N");
	private Validator dateFound = new Validator(wsaaDateFound, "Y");

	private FixedLengthStringData wsaaWopxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaWopxFn, 0, FILLER).init("WOPX");
	private FixedLengthStringData wsaaWopxRunid = new FixedLengthStringData(2).isAPartOf(wsaaWopxFn, 4);
	private ZonedDecimalData wsaaWopxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaWopxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private PackedDecimalData ix = new PackedDecimalData(4, 0);

	private FixedLengthStringData wsaaCoverageStatusArray = new FixedLengthStringData(396);
	private FixedLengthStringData[] filler3 = FLSArrayPartOfStructure(99, 4, wsaaCoverageStatusArray, 0, FILLER);
	private FixedLengthStringData[] wsaaCovrStatcode = FLSDArrayPartOfArrayStructure(2, filler3, 0);
	private FixedLengthStringData[] wsaaCovrPstatcode = FLSDArrayPartOfArrayStructure(2, filler3, 2);

	private FixedLengthStringData wsaaT5399Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5399Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5399Key, 0);
	private FixedLengthStringData wsaaT5399Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT5399Key, 4);
		/*  Storage for T5655 table items.*/
	//private static final int wsaaT5655Size = 100;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5655Size = 1000;
	private PackedDecimalData wsaaT5655IxMax = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaT5655Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5655Item, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT5655Item, 4);

		/* WSAA-T5655-ARRAY */
	private FixedLengthStringData[] wsaaT5655Rec = FLSInittedArray (1000, 20);
	private FixedLengthStringData[] wsaaT5655Key = FLSDArrayPartOfArrayStructure(7, wsaaT5655Rec, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT5655Trcde = FLSDArrayPartOfArrayStructure(4, wsaaT5655Key, 0);
	private FixedLengthStringData[] wsaaT5655Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5655Key, 4);
	private FixedLengthStringData[] wsaaT5655Data = FLSDArrayPartOfArrayStructure(13, wsaaT5655Rec, 7);
	private PackedDecimalData[] wsaaT5655Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5655Data, 0);
	private PackedDecimalData[] wsaaT5655Itmto = PDArrayPartOfArrayStructure(8, 0, wsaaT5655Data, 5);
	private ZonedDecimalData[] wsaaT5655LeadDays = ZDArrayPartOfArrayStructure(3, 0, wsaaT5655Data, 10);
		/*  Storage for T5671 table items.*/
	//private static final int wsaaT5671Size = 500;
	private static final int wsaaT5671Size = 3000;//ILIFE-1985

		/* WSAA-T5671-ARRAY */
	//private FixedLengthStringData[] wsaaT5671Rec = FLSInittedArray (500, 48);
	private FixedLengthStringData[] wsaaT5671Rec = FLSInittedArray (3000, 48);//ILIFE-1985
	private FixedLengthStringData[] wsaaT5671Key = FLSDArrayPartOfArrayStructure(8, wsaaT5671Rec, 0);
	private FixedLengthStringData[] wsaaT5671Trcde = FLSDArrayPartOfArrayStructure(4, wsaaT5671Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5671Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5671Key, 4, SPACES);
	private FixedLengthStringData[] wsaaT5671Data = FLSDArrayPartOfArrayStructure(40, wsaaT5671Rec, 8);
	private FixedLengthStringData[] wsaaT5671Subprogs = FLSDArrayPartOfArrayStructure(40, wsaaT5671Data, 0);
	private FixedLengthStringData[][] wsaaT5671Subprog = FLSDArrayPartOfArrayStructure(4, 10, wsaaT5671Subprogs, 0);
		/*  Storage for T5674 table items.*/
	//private static final int wsaaT5674Size = 20;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5674Size = 1000;

		/* WSAA-T5674-ARRAY */
	private FixedLengthStringData[] wsaaT5674Rec = FLSInittedArray (1000, 11);
	private FixedLengthStringData[] wsaaT5674Key = FLSDArrayPartOfArrayStructure(4, wsaaT5674Rec, 0);
	private FixedLengthStringData[] wsaaT5674Feemeth = FLSDArrayPartOfArrayStructure(4, wsaaT5674Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5674Data = FLSDArrayPartOfArrayStructure(7, wsaaT5674Rec, 4);
	private FixedLengthStringData[] wsaaT5674Commsubr = FLSDArrayPartOfArrayStructure(7, wsaaT5674Data, 0);
		/*  Storage for T5675 table items.*/
	//private static final int wsaaT5675Size = 70;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5675Size = 1000;

		/* WSAA-T5675-ARRAY */
	private FixedLengthStringData[] wsaaT5675Rec = FLSInittedArray (1000, 11);
	private FixedLengthStringData[] wsaaT5675Key = FLSDArrayPartOfArrayStructure(4, wsaaT5675Rec, 0);
	private FixedLengthStringData[] wsaaT5675Premmeth = FLSDArrayPartOfArrayStructure(4, wsaaT5675Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5675Data = FLSDArrayPartOfArrayStructure(7, wsaaT5675Rec, 4);
	private FixedLengthStringData[] wsaaT5675Premsubr = FLSDArrayPartOfArrayStructure(7, wsaaT5675Data, 0);
		/*  Storage for T5687 table items.*/
	
	private static final int wsaaT5534Size = 1000;

	/* WSAA-T5675-ARRAY */
	private FixedLengthStringData[] wsaaT5534Rec = FLSInittedArray (1000, 11);
	private FixedLengthStringData[] wsaaT5534Key = FLSDArrayPartOfArrayStructure(4, wsaaT5534Rec, 0);
	private FixedLengthStringData[] wsaaT5534Premmeth = FLSDArrayPartOfArrayStructure(4, wsaaT5534Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5534Data = FLSDArrayPartOfArrayStructure(7, wsaaT5534Rec, 4);
	private FixedLengthStringData[] wsaaT5534Subprog = FLSDArrayPartOfArrayStructure(7, wsaaT5534Data, 0);
	/*  Storage for T5687 table items.*/
	
	
	//private static final int wsaaT5687Size = 500;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5687Size = 1000;
		/*  Storage for T5688 table items.*/
	//private static final int wsaaT5688Size = 100;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5688Size = 1000;

		/* WSAA-T5688-ARRAY */
	private FixedLengthStringData[] wsaaT5688Rec = FLSInittedArray (1000, 12);
	private FixedLengthStringData[] wsaaT5688Key = FLSDArrayPartOfArrayStructure(3, wsaaT5688Rec, 0);
	private FixedLengthStringData[] wsaaT5688Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5688Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5688Data = FLSDArrayPartOfArrayStructure(9, wsaaT5688Rec, 3);
	private PackedDecimalData[] wsaaT5688Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5688Data, 0);
	private FixedLengthStringData[] wsaaT5688Feemeth = FLSDArrayPartOfArrayStructure(4, wsaaT5688Data, 5);
		/*  Storage for TR517 table items.*/
	//private static final int wsaaTr517Size = 100;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaTr517Size = 1000;

		/* WSAA-TR517-ARRAY */
	private FixedLengthStringData[] wsaaTr517Search = FLSInittedArray (1000, 225);
	private FixedLengthStringData[] wsaaTr517Key = FLSDArrayPartOfArrayStructure(8, wsaaTr517Search, 0);
	private FixedLengthStringData[] wsaaTr517Crtable = FLSDArrayPartOfArrayStructure(8, wsaaTr517Key, 0, SPACES);
	private FixedLengthStringData[] wsaaTr517Data = FLSDArrayPartOfArrayStructure(217, wsaaTr517Search, 8);
	private PackedDecimalData[] wsaaTr517Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaTr517Data, 0);
	private FixedLengthStringData[] wsaaTr517Zrwvflgs = FLSDArrayPartOfArrayStructure(4, wsaaTr517Data, 5);
	private FixedLengthStringData[][] wsaaTr517Zrwvflg = FLSDArrayPartOfArrayStructure(4, 1, wsaaTr517Zrwvflgs, 0);
	private FixedLengthStringData[] wsaaTr517Ctables = FLSDArrayPartOfArrayStructure(200, wsaaTr517Data, 9);
	private FixedLengthStringData[][] wsaaTr517Ctable = FLSDArrayPartOfArrayStructure(50, 4, wsaaTr517Ctables, 0);
	private FixedLengthStringData[] wsaaTr517Contitem = FLSDArrayPartOfArrayStructure(8, wsaaTr517Data, 209);
		/*  Storage for T6658 table items.*/
	//private static final int wsaaT6658Size = 50;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT6658Size = 1000;
	

		/* WSAA-T6658-ARRAY */
	private FixedLengthStringData[] wsaaT6658Rec = FLSInittedArray (1000, 11);
	private FixedLengthStringData[] wsaaT6658Key = FLSDArrayPartOfArrayStructure(4, wsaaT6658Rec, 0);
	private FixedLengthStringData[] wsaaT6658Annvry = FLSDArrayPartOfArrayStructure(4, wsaaT6658Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT6658Data = FLSDArrayPartOfArrayStructure(7, wsaaT6658Rec, 4);
	private PackedDecimalData[] wsaaT6658Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT6658Data, 0);
	private FixedLengthStringData[] wsaaT6658Billfreq = FLSDArrayPartOfArrayStructure(2, wsaaT6658Data, 5);
	private BinaryData wsaaMainAge = new BinaryData(5, 0);
	private FixedLengthStringData wsaaMainLife = new FixedLengthStringData(2);
	private BinaryData wsaaJointAge = new BinaryData(5, 0);
	private FixedLengthStringData wsaaMainCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaMainCoverage = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaMainCessdate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaMainPcessdte = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaMainMortclass = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaRiskCessTerm = new ZonedDecimalData(3, 0).setUnsigned();
		/* WSAA-MAIN-LIFE-DETS */
	private PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1);
		/* WSBB-JOINT-LIFE-DETS */
	private FixedLengthStringData wsbbSex = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaPremStatuz = new FixedLengthStringData(1);
	private Validator premReqd = new Validator(wsaaPremStatuz, "Y");
	private Validator premNotReqd = new Validator(wsaaPremStatuz, "N");
	private Validator userPremEntered = new Validator(wsaaPremStatuz, "U");
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0);
	private PackedDecimalData wsaaRerateStore = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaRateFrom = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaNewTranno = new ZonedDecimalData(5, 0);

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1);
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private ZonedDecimalData wsaaFreqFactor = new ZonedDecimalData(5, 0);

	private FixedLengthStringData wsaaCrtableMatch = new FixedLengthStringData(1).init("N");
	private Validator crtableMatch = new Validator(wsaaCrtableMatch, "Y");
	private PackedDecimalData wsaaEarliestRerateDate = new PackedDecimalData(8, 0).setUnsigned();
		/* WSAA-TRANID */
	private static final String wsaaTermid = "";
	private static final int wsaaTransactionDate = 0;
	private static final int wsaaTransactionTime = 0;
	private static final int wsaaUser = 0;

	private FixedLengthStringData wsaaT5671Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemTranCode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 0);
	private FixedLengthStringData wsaaItemCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 4);
	private String wsaaFullyPaid = "";
	private FixedLengthStringData wsaaBillfreqx = new FixedLengthStringData(2);

	private FixedLengthStringData filler5 = new FixedLengthStringData(2).isAPartOf(wsaaBillfreqx, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaBillfreqn = new ZonedDecimalData(2, 0).isAPartOf(filler5, 0).setUnsigned();
	private ZonedDecimalData wsaaBillDate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaPremDiff = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaInstPrem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaZbinstprem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaZlinstprem = new ZonedDecimalData(17, 2);
	private String wsaaChdrValid = "";
	private String wsaaCovrValid = "";
	private FixedLengthStringData wsaaCompCovrValid = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaLeadDays = new ZonedDecimalData(3, 0).setUnsigned();
	private String wsaaCovrUpdated = "";
	private ZonedDecimalData wsaaFirstIncrDate = new ZonedDecimalData(8, 0).init(99999999);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaRerateType = new FixedLengthStringData(1);
	private Validator trueRerate = new Validator(wsaaRerateType, "1");
	private Validator lextRerate = new Validator(wsaaRerateType, "2");
	private ZonedDecimalData wsaaDurationInt = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaDurationRem = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaLastRrtDate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCedagent = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaAgntFound = new FixedLengthStringData(1);
	private Validator agentFound = new Validator(wsaaAgntFound, "Y");
	private String wsaaWaiveCont = "";
	//ILIFE-3472 Code Promotion for VPMS externalization of LIFE TRX product calculations to LIFE Target2 Environment
	private FixedLengthStringData wsaaPremMeth= new FixedLengthStringData(4);
	private FixedLengthStringData wsaaZrwvflgs = new FixedLengthStringData(3);
	private FixedLengthStringData[] wsaaZrwvflg = FLSArrayPartOfStructure(3, 1, wsaaZrwvflgs, 0);
	private FixedLengthStringData wsaaTr517Rec = new FixedLengthStringData(250);
	private static final String f294 = "F294";
	private static final String h036 = "H036";
	private static final String h791 = "H791";
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t5655 = "T5655";
	private static final String t5671 = "T5671";
	private static final String t5674 = "T5674";
	private FixedLengthStringData t5675 = new FixedLengthStringData(6).init("T5675");
	private FixedLengthStringData t5687 = new FixedLengthStringData(6).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(6).init("T5688");
	private FixedLengthStringData t5534 = new FixedLengthStringData(6).init("T5534");
	private static final String tr517 = "TR517";
	private FixedLengthStringData t6658 = new FixedLengthStringData(6).init("T6658");
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;
	private static final int ct07 = 7;
	private static final int ct08 = 8;

	private FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(100);
	private FixedLengthStringData wsysSysparams = new FixedLengthStringData(100).isAPartOf(wsysSystemErrorParams, 0);
	private FixedLengthStringData wsysWopxData = new FixedLengthStringData(19).isAPartOf(wsysSysparams, 0, REDEFINE);
	private FixedLengthStringData wsysChdrcoy = new FixedLengthStringData(1).isAPartOf(wsysWopxData, 0);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysWopxData, 1);
	private FixedLengthStringData wsysLife = new FixedLengthStringData(2).isAPartOf(wsysWopxData, 9);
	private FixedLengthStringData wsysCoverage = new FixedLengthStringData(2).isAPartOf(wsysWopxData, 11);
	private FixedLengthStringData wsysRider = new FixedLengthStringData(2).isAPartOf(wsysWopxData, 13);
	private ZonedDecimalData wsysPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsysWopxData, 15).setUnsigned();
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ZonedDecimalData computPaymt = new ZonedDecimalData(8, 0);
	

	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	
	private Freqcpy freqcpy = new Freqcpy();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Premiumrec premiumrec = new Premiumrec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Isuallrec isuallrec = new Isuallrec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	private Crtundwrec crtundwrec = new Crtundwrec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private T5655rec t5655rec = new T5655rec();
	private T5674rec t5674rec = new T5674rec();
	private T5675rec t5675rec = new T5675rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private Tr517rec tr517rec = new Tr517rec();
	private T6658rec t6658rec = new T6658rec();
	private T5399rec t5399rec = new T5399rec();
	private T5534rec t5534rec = new T5534rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaT5687ArrayInner wsaaT5687ArrayInner = new WsaaT5687ArrayInner();
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();
	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private WopxpfDAO wopxpfDAO = getApplicationContext().getBean("wopxpfDAO", WopxpfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private LextpfDAO lextpfDAO = getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private AnnypfDAO annypfDAO = getApplicationContext().getBean("annypfDAO", AnnypfDAO.class);
	private IncrpfDAO incrpfDAO = getApplicationContext().getBean("incrpfDAO", IncrpfDAO.class);
	private AgcmpfDAO agcmpfDAO = getApplicationContext().getBean("agcmpfDAO", AgcmpfDAO.class);
	private PcddpfDAO pcddpfDAO = getApplicationContext().getBean("pcddpfDAO", PcddpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private RegppfDAO regpDAO = getApplicationContext().getBean("regppfDAO", RegppfDAO.class);
	
	private Wopxpf wopxpfRec;
	private Chdrpf chdrlifIO;
	private Iterator<Wopxpf> iteratorList;
	private Map<String, List<Itempf>> t5655Map = null;
	private Map<String, List<Itempf>> t5671Map = null;
	private Map<String, List<Itempf>> t5674Map = null;
	private Map<String, List<Itempf>> t5675Map = null;
	private Map<String, List<Itempf>> t5687Map = null;
	private Map<String, List<Itempf>> t5688Map = null;
	private Map<String, List<Itempf>> tr517Map = null;
	private Map<String, List<Itempf>> t6658Map = null;
	private Map<String, List<Itempf>> t5399Map = null;
	private Map<String, List<Itempf>> t5534Map = null;
	private Map<String, List<Chdrpf>> chdrlifMap;
	private Map<String, List<Covrpf>> covrrnlMap;
	private Map<String, List<Covrpf>> covrlnbMap;
	private Map<String, List<Lextpf>> lextbrrMap;
	private Map<String, List<Lifepf>> lifelnbMap;
	private Map<String, List<Payrpf>> payrMap;
	private Map<String, List<Annypf>> annyMap;
	private Map<String, List<Incrpf>> incrmjaMap;
	private Map<String, List<Incrpf>> incrhstMap;
	private Map<String, List<Agcmpf>> agcmbchMap;
	private Map<String, List<Agcmpf>> agcmMap;
	private Map<String, List<Pcddpf>> pcddchgMap;
	private Map<String, List<Lifepf>> lifeenqMap;
	private Map<String, List<Covrpf>> covrbbrMap;
	private List<Regppf> regppfList;
	
	private List<Covrpf> updateCovrrnlList = null;
	private List<Covrpf> insertCovrList = null;
	private List<Incrpf> insertIncrpfList = null;
	private List<Agcmpf> updateAgcmpfList = null;
	private List<Agcmpf> insertAgcmbchList = null;
	private List<Ptrnpf> insertPtrnpfList = null;
	private List<Chdrpf> updateChdrlifList = null;
	private List<Chdrpf> insertChdrlifList = null;
	private List<Payrpf> updatePayrpfList = null;
	private List<Payrpf> insertPayrpfList = null;
	private List<Incrpf> updateIncrList =  null;
	private int wsaaT5655Ix = 1;
	private int wsaaT5671Ix = 1;
	private int wsaaT5674Ix = 1;
	private int wsaaT5675Ix = 1;
	private int wsaaT5687Ix = 1;
	private int wsaaT5688Ix = 1;
	private int wsaaTr517Ix = 1;
	private int wsaaT6658Ix = 1;
	private int wsaaT5534Ix = 1;
	
	private int ct01Value = 0;
	private int ct02Value = 0;
	private BigDecimal ct03Value = BigDecimal.ZERO;
	private int ct04Value = 0;
	private int ct05Value = 0;
	private int ct06Value = 0;
	private int ct07Value = 0;
	private int ct08Value = 0;
	private int intBatchID = 0;
	private int intBatchExtractSize;
	private boolean endFlag = false;
	private BigDecimal wsaaCovrSumins = BigDecimal.ZERO;
	private int covrlnbPlanSuffix = 0;
	
	private PackedDecimalData wsaaTax = new PackedDecimalData(17, 2).init(0);
	private Tr52drec tr52drec = new Tr52drec();
	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);
	private Tr52erec tr52erec = new Tr52erec();
	private String tr52e = "TR52E";	
	private String tr52d = "TR52D";//ILIFE-6658
	private Txcalcrec txcalcrec = new Txcalcrec();
	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private Regppf regppf = new Regppf();
	private PackedDecimalData wsaaIntermed = new PackedDecimalData(7, 5);
	private PackedDecimalData wsaaSumins2 = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaPrcnt = new PackedDecimalData(7, 4).setUnsigned();
	private PackedDecimalData wsaaTotPymtamt = new PackedDecimalData(17, 2).init(0);
	private boolean riskPremflag = false; //ILIFE-7845 
	private static final String  RISKPREM_FEATURE_ID="NBPRP094";//ILIFE-7845 
	private static final String  PRD005_FEA_ID="BTPRO012";
	boolean isFeatureConfig = false;
	private List<Ptrnpf> ptrnrecords = null;
	private boolean reinstflag = false;
	private static final String ta85 = "TA85";
	private boolean isFoundPro = false;
	private Ptrnpf ptrnpfReinstate = new Ptrnpf();
	private Incrpf incrpf = null;
	private boolean stampDutyflag = false;
	private PackedDecimalData wsaaCovrSuminInc = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaProCpiDate = new PackedDecimalData(8, 0);
	
	private boolean ptdateConfig = false;
	/*ILIFE-8248 start*/
  	private boolean lnkgFlag = false;
	/*ILIFE-8248 end*/
  	private Clntpf clntpf= new Clntpf();//ILIFE-8502
  	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);//ILIFE-8502
	private com.csc.smart400framework.dataaccess.model.Clntpf clnt;//ILIFE-8502
	private com.csc.smart400framework.dataaccess.dao.ClntpfDAO clntDao;	//ILIFE-8502
	private boolean prmhldtrad = false;//ILIFE-8509

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit3b29,
		next3a28,
		exit3a29,
		accSumins3a38,
		a210ByPass,
		a220ByPass,
		a200Exit,
		a020CallCovrbbrio,
		a080NextRecord,
		a090Exit
	}

	public Br615() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Restarting of this program is handled by MAINB,*/
		/** using a restart method of '3'.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		if (isNE(bprdIO.getRestartMethod(),"3")) {
			syserrrec.statuz.set("IVRM");
			fatalError600();
		}
		/*  Point to correct member of WOPXPF.*/
		varcom.vrcmTranid.set(batcdorrec.tranid);
		wsaaWopxRunid.set(bprdIO.getSystemParam04());
		wsaaWopxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				intBatchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
		readChunkRecord();
		
		wsaaAnb.set(ZERO);
		wsaaNewTranno.set(ZERO);
		wsaaIndex.set(ZERO);
		wsaaLeadDays.set(ZERO);
		wsaaRateFrom.set(varcom.vrcmMaxDate);
		wsaaBillDate.set(varcom.vrcmMaxDate);
		wsaaFirstIncrDate.set(varcom.vrcmMaxDate);
		wsaaEof.set("N");
		/* Read T5679 for valid statii.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());//BH65
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		
		String coy = bsprIO.getCompany().toString();
		
		
		t5674Map = itemDAO.loadSmartTable("IT", coy, "T5674");
		t5687Map = itemDAO.loadSmartTable("IT", coy, "T5687");
		t5688Map = itemDAO.loadSmartTable("IT", coy, "T5688");
		tr517Map = itemDAO.loadSmartTable("IT", coy, "Tr517");
		t6658Map = itemDAO.loadSmartTable("IT", coy, "T6658");
		
		wsaaT5655Ix = 1;
		t5655Map = itemDAO.loadSmartTable("IT", coy, "T5655");
		loadT56551100();
		
		wsaaT5671Ix = 1;
		t5671Map = itemDAO.loadSmartTable("IT", coy, "T5671");
		loadT56711200();
		
		wsaaT5674Ix = 1;
		t5674Map = itemDAO.loadSmartTable("IT", coy, "T5674");
		loadT56741300();
		
		wsaaT5675Ix = 1;
		t5675Map = itemDAO.loadSmartTable("IT", coy, "T5675");
		loadT56751400();
		
		wsaaT5687Ix = 1;
		t5687Map = itemDAO.loadSmartTable("IT", coy, "T5687");
		loadT56871500();
		
		wsaaT5688Ix = 1;
		t5688Map = itemDAO.loadSmartTable("IT", coy, "T5688");
		loadT56881600();
		
		wsaaTr517Ix = 1;
		tr517Map = itemDAO.loadSmartTable("IT", coy, "TR517");
		loadTr5171700();
		
		wsaaT6658Ix = 1;
		t6658Map = itemDAO.loadSmartTable("IT", coy, "T6658");
		loadT66581800();

		t5399Map = itemDAO.loadSmartTable("IT", coy, "T5399");
		
		wsaaT5534Ix = 1;
		t5534Map = itemDAO.loadSmartTable("IT", coy, "T5534");
		loadT55341900();
		
		String stampdutyItem = "NBPROP01";
		stampDutyflag = FeaConfg.isFeatureExist(coy, stampdutyItem, appVars, "IT");
		
		String ptdateItem = "CSOTH012";
		ptdateConfig = FeaConfg.isFeatureExist(coy, ptdateItem, appVars, "IT");

		/* ILIFE-8248 start */
		String linkageItem ="NBPRP055";
		lnkgFlag = FeaConfg.isFeatureExist(coy, linkageItem, appVars, "IT");
		/* ILIFE-8248 start */
		String prmhldtradItem = "CSOTH010";//ILIFE-8509
		prmhldtrad = FeaConfg.isFeatureExist(coy, prmhldtradItem, appVars, "IT");
	}

	private void readChunkRecord() {
		List<Wopxpf> wopxpfList = wopxpfDAO.searchWopxRecord(wsaaWopxFn.toString(), wsaaThreadMember.toString(),
				intBatchExtractSize, intBatchID);
		iteratorList = wopxpfList.iterator();
		if (!wopxpfList.isEmpty()) {
			List<String> chdrnumSet = new ArrayList<>();
			Map<String,List<String>> chdrMap = new HashMap<>();
			for (Wopxpf w : wopxpfList) {
				String coy = w.getChdrcoy();
				if(chdrMap.containsKey(coy)){
					chdrMap.get(coy).add(w.getChdrnum());
				}else{
					List<String> chdrnumList = new ArrayList<>();
					chdrnumList.add(w.getChdrnum());
					chdrMap.put(coy, chdrnumList);
				}
				chdrnumSet.add(w.getChdrnum());
			}
			//ILIFE-5011 start by dpuhawan
			//chdrlifMap = chdrpfDAO.searchChdrRecordByChdrnum(chdrnumSet);
			chdrlifMap = chdrpfDAO.searchChdrmjaByChdrnum(chdrnumSet);
			//ILIFE-5011 end by dpuhawan
			
			covrrnlMap = new HashMap<>();
			for(String coy:chdrMap.keySet()){
				covrrnlMap.putAll(covrpfDAO.searchCovrMap(coy, chdrnumSet));
			}
			covrlnbMap = covrrnlMap;
			lextbrrMap = lextpfDAO.searchLextbrrMap(chdrnumSet);
			lifelnbMap = lifepfDAO.searchLifelnbRecord(bsprIO.getCompany().trim(),chdrnumSet); //ILIFE-8901
			payrMap = payrpfDAO.searchPayrRecordByChdrnumChdrcoy(bsprIO.getCompany().trim(),chdrnumSet);
			annyMap = annypfDAO.searchAnnyRecordByChdrnum(chdrnumSet);
			
			incrmjaMap = new HashMap<>();
			incrhstMap = new HashMap<>();
			Map<String, List<Incrpf>> tempIncrMap = incrpfDAO.searchIncrRecordByChdrnum(chdrnumSet);
			if (tempIncrMap != null) {
				for (String key : tempIncrMap.keySet()) {
					List<Incrpf> ncrmjaList = new ArrayList<>();
					List<Incrpf> incrhstList = new ArrayList<>();
					for (Incrpf c : tempIncrMap.get(key)) {
						if (c.getValidflag().equals("1")) {
							ncrmjaList.add(c);
						}else if(c.getValidflag().equals("2")&&c.getRefusalFlag().trim().isEmpty()){
							incrhstList.add(c);
						}
					}
					incrmjaMap.put(key, ncrmjaList);
					incrhstMap.put(key, incrhstList);
				}
			}
	
			Map<String, List<Agcmpf>> tempAgcmMap = agcmpfDAO.searchAgcmRecordByChdrnum(chdrnumSet);
			agcmbchMap = new HashMap<>();
			agcmMap = new HashMap<>();
			if (tempAgcmMap != null) {
				for (String key : tempAgcmMap.keySet()) {
					List<Agcmpf> agcmbchList = new ArrayList<>();
					List<Agcmpf> agcmList = new ArrayList<>();
					for (Agcmpf l : tempAgcmMap.get(key)) {
						if ((l.getValidflag().equals("1")&&!l.getDormflag().equals("Y"))||(l.getValidflag().trim().isEmpty()&&!l.getDormflag().equals("Y"))) {
							agcmList.add(l);
						}
						if(l.getValidflag().equals("1") || l.getValidflag().trim().isEmpty()){
							agcmbchList.add(l);
						}
					}
					agcmMap.put(key, agcmList);
					agcmbchMap.put(key, agcmbchList);
				}
			}
			
			pcddchgMap = pcddpfDAO.searchPcddRecordByChdrnum(chdrnumSet);
			
			lifeenqMap = new HashMap<>();
			if (lextbrrMap != null) {
				for (String key : lifelnbMap.keySet()) {
					List<Lifepf> ll = new ArrayList<>();
					for (Lifepf l : lifelnbMap.get(key)) {
						if (l.getValidflag().equals("1")) {
							ll.add(l);
						}
					}
					lifeenqMap.put(key, ll);
				}
			}
			
			covrbbrMap = new HashMap<>();
			if (covrrnlMap != null) {
				for (String key : covrrnlMap.keySet()) {
					List<Covrpf> cl = new ArrayList<>();
					for (Covrpf c : covrrnlMap.get(key)) {						
							cl.add(c);
						
					}
					covrbbrMap.put(key, cl);
				}
			}
		}
	}

protected void loadT56551100()
 {
		if (t5655Map != null) {
			String authCode = bprdIO.getAuthCode().toString();
			for (List<Itempf> itemList : t5655Map.values()) {
				for (Itempf i : itemList) {
					if (i.getItemitem().startsWith(authCode)) {
						t5655rec.t5655Rec.set(StringUtil.rawToString(i.getGenarea()));
						wsaaT5655Key[wsaaT5655Ix].set(i.getItemitem());
						wsaaT5655Itmfrm[wsaaT5655Ix].set(i.getItmfrm());
						wsaaT5655Itmto[wsaaT5655Ix].set(i.getItmto());
						wsaaT5655LeadDays[wsaaT5655Ix].set(t5655rec.leadDays);
						wsaaT5655IxMax.set(wsaaT5655Ix);
						wsaaT5655Ix++;
					}
					if (isGT(wsaaT5655Ix, wsaaT5655Size)) {
						syserrrec.params.set(t5655);
						syserrrec.statuz.set(h791);
						fatalError600();
					}
				}
			}
		}
	}

protected void loadT56711200()
	{
		if (t5671Map != null) {
			String code = batcdorrec.trcde.toString();
			for (List<Itempf> itemList : t5671Map.values()) {
				for (Itempf i : itemList) {
					if (i.getItemitem().startsWith(code)) {
						t5671rec.t5671Rec.set(StringUtil.rawToString(i.getGenarea()));
						wsaaT5671Key[wsaaT5671Ix].set(i.getItemitem());
						wsaaT5671Subprogs[wsaaT5671Ix].set(t5671rec.subprogs);
						wsaaT5671Ix++;
					}
					if (isGT(wsaaT5671Ix, wsaaT5671Size)) {
						syserrrec.params.set(t5671);
						syserrrec.statuz.set(h791);
						fatalError600();
					}
				}
			}
		}
	}

protected void loadT56741300()
	{
		if (t5674Map != null) {
			for (List<Itempf> itemList : t5674Map.values()) {
				for (Itempf i : itemList) {
						t5674rec.t5674Rec.set(StringUtil.rawToString(i.getGenarea()));
						wsaaT5674Key[wsaaT5674Ix].set(i.getItemitem());
						wsaaT5674Commsubr[wsaaT5674Ix].set(t5674rec.commsubr);
						wsaaT5674Ix++;
					}
					if (isGT(wsaaT5674Ix, wsaaT5674Size)) {
						syserrrec.params.set(t5674);
						syserrrec.statuz.set(h791);
						fatalError600();
					}
				}
			}
	}

protected void loadT56751400()
 {
		if (t5675Map != null) {
			for (List<Itempf> itemList : t5675Map.values()) {
				for (Itempf i : itemList) {
					t5675rec.t5675Rec.set(StringUtil.rawToString(i.getGenarea()));
					wsaaT5675Key[wsaaT5675Ix].set(i.getItemitem());
					wsaaT5675Premsubr[wsaaT5675Ix].set(t5675rec.premsubr);
					wsaaT5675Ix++;
				}
				if (isGT(wsaaT5675Ix, wsaaT5675Size)) {
					syserrrec.params.set(t5675);
					syserrrec.statuz.set(h791);
					fatalError600();
				}
			}
		}
	}

protected void loadT56871500()
	{
		if (t5687Map != null) {
			for (List<Itempf> itemList : t5687Map.values()) {
				for (Itempf i : itemList) {
					t5687rec.t5687Rec.set(StringUtil.rawToString(i.getGenarea()));
					wsaaT5687ArrayInner.wsaaT5687Itmfrm[wsaaT5687Ix].set(i.getItmfrm());
					wsaaT5687ArrayInner.wsaaT5687Crtable[wsaaT5687Ix].set(i.getItemitem());
					wsaaT5687ArrayInner.wsaaT5687Annvry[wsaaT5687Ix].set(t5687rec.anniversaryMethod);
					wsaaT5687ArrayInner.wsaaT5687Premmeth[wsaaT5687Ix].set(t5687rec.premmeth);
					wsaaT5687ArrayInner.wsaaT5687Jlpremmeth[wsaaT5687Ix].set(t5687rec.jlPremMeth);
					wsaaT5687ArrayInner.wsaaT5687Pguarp[wsaaT5687Ix].set(t5687rec.premGuarPeriod);
					wsaaT5687ArrayInner.wsaaT5687Rtrnwfreq[wsaaT5687Ix].set(t5687rec.rtrnwfreq);
					wsaaT5687ArrayInner.wsaaT5687Zrrcombas[wsaaT5687Ix].set(t5687rec.zrrcombas);
					wsaaT5687ArrayInner.wsaaT5687Bbmeth[wsaaT5687Ix].set(t5687rec.bbmeth);
					wsaaT5687Ix++;
				}
				if (isGT(wsaaT5687Ix, wsaaT5687Size)) {
					syserrrec.params.set(t5687);
					syserrrec.statuz.set(h791);
					fatalError600();
				}
			}
		}
	}

protected void loadT56881600()
	{
		if (t5688Map != null) {
			for (List<Itempf> itemList : t5688Map.values()) {
				for (Itempf i : itemList) {
					t5688rec.t5688Rec.set(StringUtil.rawToString(i.getGenarea()));
					wsaaT5688Itmfrm[wsaaT5688Ix].set(i.getItmfrm());
					wsaaT5688Cnttype[wsaaT5688Ix].set(i.getItemitem());
					wsaaT5688Feemeth[wsaaT5688Ix].set(t5688rec.feemeth);
					wsaaT5688Ix++;
				}
				if (isGT(wsaaT5688Ix, wsaaT5688Size)) {
					syserrrec.params.set(t5688);
					syserrrec.statuz.set(h791);
					fatalError600();
				}
			}
		}
	}

protected void loadTr5171700()
	{
		if (tr517Map != null) {
			for (List<Itempf> itemList : tr517Map.values()) {
				for (Itempf i : itemList) {
					tr517rec.tr517Rec.set(StringUtil.rawToString(i.getGenarea()));
					//tr517rec.tr517Rec.set(i.getGenarea());
					wsaaTr517Itmfrm[wsaaTr517Ix].set(i.getItmfrm());
					wsaaTr517Crtable[wsaaTr517Ix].set(i.getItemitem());
					wsaaTr517Zrwvflgs[wsaaTr517Ix].set(tr517rec.zrwvflgs);
					wsaaTr517Ctables[wsaaTr517Ix].set(tr517rec.ctables);
					wsaaTr517Contitem[wsaaTr517Ix].set(tr517rec.contitem);
					wsaaTr517Ix++;
				}
				if (isGT(wsaaTr517Ix, wsaaTr517Size)) {
					syserrrec.params.set(tr517);
					syserrrec.statuz.set(h791);
					fatalError600();
				}
			}
		}
	}

protected void loadT66581800()
	{
		if (t6658Map != null) {
			for (List<Itempf> itemList : t6658Map.values()) {
				for (Itempf i : itemList) {
					t6658rec.t6658Rec.set(StringUtil.rawToString(i.getGenarea()));
					wsaaT6658Itmfrm[wsaaT6658Ix].set(i.getItmfrm());
					wsaaT6658Annvry[wsaaT6658Ix].set(i.getItemitem());
					wsaaT6658Billfreq[wsaaT6658Ix].set(t6658rec.billfreq);
					wsaaT6658Ix++;
				}
				if (isGT(wsaaT6658Ix, wsaaT6658Size)) {
					syserrrec.params.set(t6658);
					syserrrec.statuz.set(h791);
					fatalError600();
				}
			}
		}
	}

protected void loadT55341900()
{
		if (t5534Map != null) {
			for (List<Itempf> itemList : t5534Map.values()) {
				for (Itempf i : itemList) {
					t5534rec.t5534Rec.set(StringUtil.rawToString(i.getGenarea()));
					wsaaT5534Key[wsaaT5534Ix].set(i.getItemitem());
					wsaaT5534Subprog[wsaaT5534Ix].set(t5534rec.subprog);
					wsaaT5534Ix++;
				}
				if (isGT(wsaaT5534Ix, wsaaT5675Size)) {
					syserrrec.params.set(t5534);
					syserrrec.statuz.set(h791);
					fatalError600();
				}
			}
		}
	}

protected void readFile2000()
	{
		if (iteratorList != null && iteratorList.hasNext()) {
			wopxpfRec = iteratorList.next();
			ct01Value++;
			/* No of COVR records read*/
			/*  Set up the key for the SYSR- copybook, should a system error*/
			/*  for this record occur.*/
			wsysChdrcoy.set(wopxpfRec.getChdrcoy());
			wsysChdrnum.set(wopxpfRec.getChdrnum());
			wsysLife.set(wopxpfRec.getLife());
			wsysCoverage.set(wopxpfRec.getCoverage());
			wsysRider.set(wopxpfRec.getRider());
			wsysPlnsfx.set(wopxpfRec.getPlanSuffix());
		} else {
			intBatchID++;
			clearList2100();
			readChunkRecord();
			if (iteratorList.hasNext()) {
				wopxpfRec = iteratorList.next();			
				ct01Value++;
				wsysChdrcoy.set(wopxpfRec.getChdrcoy());
				wsysChdrnum.set(wopxpfRec.getChdrnum());
				wsysLife.set(wopxpfRec.getLife());
				wsysCoverage.set(wopxpfRec.getCoverage());
				wsysRider.set(wopxpfRec.getRider());
				wsysPlnsfx.set(wopxpfRec.getPlanSuffix());
			} else {
				wsspEdterror.set(varcom.endp);
				return ;
			}
		}
	}

	private void clearList2100() {
		if (chdrlifMap != null) {
			chdrlifMap.clear();
		}
		if (covrrnlMap != null) {
			covrrnlMap.clear();
		}
		if (covrlnbMap != null) {
			covrlnbMap.clear();
		}
		if (lextbrrMap != null) {
			lextbrrMap.clear();
		}
		if (lifelnbMap != null) {
			lifelnbMap.clear();
		}
		if (payrMap != null) {
			payrMap.clear();
		}
		if (annyMap != null) {
			annyMap.clear();
		}
		if (incrmjaMap != null) {
			incrmjaMap.clear();
		}
		if (incrhstMap != null) {
			incrhstMap.clear();
		}
		if (agcmbchMap != null) {
			agcmbchMap.clear();
		}
		if (agcmMap != null) {
			agcmMap.clear();
		}
		if (pcddchgMap != null) {
			pcddchgMap.clear();
		}
		if (lifeenqMap != null) {
			lifeenqMap.clear();
		}
		if (covrbbrMap != null) {
			covrbbrMap.clear();
		}
		iteratorList = null;
	}
protected void edit2500()
	{
		/*READ*/
		/* Check record is required for processing.*/
		wsaaChdrValid = "N";
		readChdr2600();
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
			validateChdrStatus2520(chdrlifIO.getStatcode(), chdrlifIO.getPstcde());
		}
		/* Check for increase records.  If a pending increase record*/
		/* exists (Validflag = 1), write a batch message to indicate*/
		/* this and do not process the contract.*/
		/*    PERFORM 2700-PENDING-INCREASE.                               */
		/* If validation on the contract header fails, moving SPACES to*/
		/* to WSSP-EDTERROR stops the program from entering the update*/
		/* section and returns to the read file section.*/
		if (isEQ(wsaaChdrValid,"N")) {
			wsspEdterror.set(SPACES);
			ct06Value++;
		}
		else {
			wsspEdterror.set(varcom.oK);
		}
		/*EXIT*/
	}

protected void validateChdrStatus2520(String statcode, String pstcode)
	{
		/*CHECK*/
		if (isEQ(t5679rec.cnRiskStat[wsaaIndex.toInt()], statcode)) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
				validateChdrPremStatus2550(pstcode);
			}
		}
		/*EXIT*/
	}

protected void validateChdrPremStatus2550(String pstcode)
	{
		/*PARA*/
		if (isEQ(t5679rec.cnPremStat[wsaaIndex.toInt()], pstcode)) {
			wsaaIndex.set(13);
			wsaaChdrValid = "Y";
		}
		/*EXIT*/
	}

protected void readChdr2600()
	{
		if(chdrlifMap!=null&&chdrlifMap.containsKey(wopxpfRec.getChdrnum())){
			for(Chdrpf c:chdrlifMap.get(wopxpfRec.getChdrnum())){
				if(c.getChdrcoy().toString().equals(wopxpfRec.getChdrcoy())){
					chdrlifIO = c;
					break; //ILIFE-5011 added by dpuhawan
				}
			}
		}
		if(chdrlifIO == null){
			syserrrec.params.set(wopxpfRec.getChdrnum());
			fatalError600();
		}
		for (wsaaT5655Ix = wsaaT5655IxMax.toInt(); !(isEQ(wsaaT5655Ix,0)
		|| (isEQ(chdrlifIO.getCnttype(),wsaaT5655Cnttype[wsaaT5655Ix])
		&& isGTE(chdrlifIO.getOccdate(),wsaaT5655Itmfrm[wsaaT5655Ix])
		&& isLTE(chdrlifIO.getOccdate(),wsaaT5655Itmto[wsaaT5655Ix]))); wsaaT5655Ix--)
	{
			/*CONTINUE_STMT*/
		}
		/* If the contract type specific entry is not found, use the*/
		/* default entry of the transaction code plus '***'.*/
		if (isEQ(wsaaT5655Ix,0)) {
			for (wsaaT5655Ix = wsaaT5655IxMax.toInt(); !(isEQ(wsaaT5655Ix,0)
			|| isEQ(wsaaT5655Cnttype[wsaaT5655Ix],"***")); wsaaT5655Ix--)
	{
				/*CONTINUE_STMT*/
			}
		}
		if (isEQ(wsaaT5655Ix,0)) {
			itdmIO.setItempfx("IT");
			itdmIO.setItemcoy(bsprIO.getCompany());
			itdmIO.setItemtabl(t5655);
			wsaaTrcde.set(bprdIO.getAuthCode());
			wsaaCnttype.set(chdrlifIO.getCnttype());
			itdmIO.setItemitem(wsaaT5655Item);
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.endp);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		/* Increment the effective date of the batch run by the lead*/
		/* days on T5655 for use in validating the coverage.*/
		wsaaLeadDays.set(wsaaT5655LeadDays[wsaaT5655Ix]);
		datcon2rec.freqFactor.set(wsaaLeadDays);
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError600();
		}
		wsaaBillDate.set(datcon2rec.intDate2);
	}

	/**
	* <pre>
	*2700-PENDING-INCREASE SECTION.
	*2700-BEGN.
	*    MOVE SPACES                 TO INCRMJA-PARAMS.
	*    MOVE CHDRCOY                TO INCRMJA-CHDRCOY.
	*    MOVE CHDRNUM                TO INCRMJA-CHDRNUM.
	*    MOVE ZEROES                 TO INCRMJA-LIFE
	*                                   INCRMJA-COVERAGE
	*                                   INCRMJA-RIDER
	*                                   INCRMJA-PLAN-SUFFIX.
	*    MOVE INCRMJAREC             TO INCRMJA-FORMAT.
	*    MOVE BEGN                   TO INCRMJA-FUNCTION.
	*    CALL 'INCRMJAIO'            USING INCRMJA-PARAMS.
	*    IF INCRMJA-STATUZ        NOT = O-K
	*       AND                   NOT = ENDP
	*        MOVE INCRMJA-PARAMS     TO SYSR-PARAMS
	*        MOVE INCRMJA-STATUZ     TO SYSR-STATUZ
	*        PERFORM 600-FATAL-ERROR
	*    END-IF.
	**** If the record retrieved is not for the correct contract,
	**** move ENDP to the status.
	*    IF INCRMJA-CHDRCOY       NOT = CHDRCOY
	*       OR INCRMJA-CHDRNUM    NOT = CHDRNUM
	*        MOVE ENDP               TO INCRMJA-STATUZ
	*    END-IF.
	**** If a pending INCR has been found, write out a message to
	**** indicate this and do no further processing on the contract.
	*    IF INCRMJA-STATUZ        NOT = ENDP
	*        MOVE F374               TO CONL-ERROR
	*        MOVE INCRMJA-DATA-KEY   TO CONL-PARAMS
	*        PERFORM 003-CALL-CONLOG
	*        MOVE 'N'                TO WSAA-CHDR-VALID
	*    END-IF.
	*2799-EXIT.
	*     EXIT.
	* </pre>
	*/
protected void update3000()
	{
	isFeatureConfig = FeaConfg.isFeatureExist(batcdorrec.company.toString(),PRD005_FEA_ID , appVars, "IT");
		/* Soft lock contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(batcdorrec.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		/*    MOVE ZEROES                 TO SFTL-USER.*/
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrlifIO.getChdrnum());
			stringVariable1.addExpression(" HEADER RECORD LOCKED");
			stringVariable1.setStringInto(conlogrec.message);
			conlogrec.error.set(sftlockrec.statuz);
			callConlog003();
			return ;
		}
		reinstflag = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "CSLRI003", appVars, "IT");
		if(reinstflag)
			markprorated();
		/* Perform a begin on the COVRRNL and then enter the COVR loop.*/
		/* The Wsaa-covrs-live indicator is set to N initially. This*/
		/* is set to Y if a valid cover/rider is found, on the contract*/
		/* which is alive. This indicator is then checked in section*/
		/* 4000 to determine whether the whole contract has matured or*/
		/* whether there are valid covers/riders existing.*/
		wsaaCovrUpdated = "N";
		wsaaPremDiff.set(ZERO);
		ix.set(ZERO);
		wsaaCoverageStatusArray.set(SPACES);
		/* Move CHDRLIF values to COVRRNL and begin read.*/
		if (covrrnlMap != null && covrrnlMap.containsKey(chdrlifIO.getChdrnum())) {
			for (Covrpf c : covrrnlMap.get(chdrlifIO.getChdrnum())) {
				if (c.getChdrcoy().equals(chdrlifIO.getChdrcoy().toString()) && c.getLife().equals(wopxpfRec.getLife())
						&& c.getCoverage().equals(wopxpfRec.getCoverage()) && c.getRider().equals(wopxpfRec.getRider())
						&& c.getPlanSuffix() == wopxpfRec.getPlanSuffix()) {
					validateCovr3100(c);
					if (isEQ(wsaaCovrValid, "Y")) {
						readTr52d();
						processWop3a10(c);						
						Covrpf covrIO = updateCovr3200(c);
						updateAgcm3700(c, covrIO);
						genericProcessing3900(c, covrIO);
						updateUndr4500(c);
					} else {
						ct07Value++;
					}
				}
			}
		}else{
			syserrrec.params.set(chdrlifIO.getChdrnum());
			fatalError600();
		}
	
		if (isEQ(wsaaCovrUpdated,"Y")) {
			updateChdrPtrn4100();
			updatePayr4200();
		}
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.function.set("UNLK");
		sftlockrec.company.set(batcdorrec.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(ZERO);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void markprorated(){
	
   	ptrnrecords = ptrnpfDAO.searchPtrnenqRecord(wopxpfRec.getChdrcoy(), wopxpfRec.getChdrnum());
   	if(ptrnrecords != null){
   		for(Ptrnpf ptrn : ptrnrecords ){
   		if(isNE(ptrn.getBatctrcde(),ta85) && isNE(ptrn.getBatctrcde(),"BH65")){
   			continue;
   		}
   		if(isEQ(ptrn.getBatctrcde(),"BH65")){
   			break;
   		}
   		if(isEQ(ptrn.getBatctrcde(),ta85)){
   			isFoundPro = true;
   			ptrnpfReinstate = ptrnpfDAO.getPtrnData(wopxpfRec.getChdrcoy(), wopxpfRec.getChdrnum(), ta85);
   			incrpf = incrpfDAO.getIncrByCrrcdDate(wopxpfRec.getChdrcoy(), wopxpfRec.getChdrnum(), wopxpfRec.getLife(), wopxpfRec.getCoverage(), wopxpfRec.getRider(), wopxpfRec.getPlanSuffix(), bsscIO.getEffectiveDate().toInt(),"2");//IJTI-1485
   			if(incrpf != null){
   				wsaaProCpiDate.set(incrpf.getCrrcd());	
   			    wsaaCovrSuminInc.set(sub(incrpf.getNewsum(),incrpf.getOrigSum()));
   			}
   		}
   	}
  }
}

protected void validateCovr3100(Covrpf covrrnlIO)
	{
		/* Check if COVR is the WOP component, If it is not, ignore*/
		/* it.*/
		/* If a valid cover/rider has a premium cessation date beyond*/
		/* the billing date then it is still 'live' and hence set*/
		/* set indicator accordingly.*/
		/* Validate the coverage risk and premium statii.*/
		/* Use Working Storage fields to avoid problems comparing*/
		/* dates in packed decimal.*/
		wsaaCovrValid = "N";
		/*if (isNE(covrrnlIO.getValidflag(), "1")) {
			return ;
		}*/
		if (isEQ(covrrnlIO.getRerateDate(), ZERO)
		|| isEQ(covrrnlIO.getRerateDate(), varcom.vrcmMaxDate)) {
			return ;
		}
		wsaaTr517Ix = 1;
		 searchlabel1:
		{
			for (; isLT(wsaaTr517Ix,wsaaTr517Search.length); wsaaTr517Ix++){
				if (isEQ(wsaaTr517Key[wsaaTr517Ix],covrrnlIO.getCrtable())) {
					break searchlabel1;
				}
			}
			return ;
		}
		wsaaDateFound.set("N");
		while ( !(isNE(covrrnlIO.getCrtable(),wsaaTr517Key[wsaaTr517Ix])
		|| isGT(wsaaTr517Ix,wsaaTr517Size)
		|| dateFound.isTrue())) {
			if (isGTE(covrrnlIO.getCrrcd(),wsaaTr517Itmfrm[wsaaTr517Ix])) {
				wsaaDateFound.set("Y");
				tr517rec.zrwvflgs.set(wsaaTr517Zrwvflgs[wsaaTr517Ix]);
				tr517rec.ctables.set(wsaaTr517Ctables[wsaaTr517Ix]);
				tr517rec.contitem.set(wsaaTr517Contitem[wsaaTr517Ix]);
			}
			else {
				wsaaTr517Ix++;
			}
		}
	
		if (!dateFound.isTrue()) {
			return ;
		}
		wsaaTr517Rec.set(tr517rec.tr517Rec);
		if (isNE(covrrnlIO.getRerateDate(),ZERO))
		//&& isEQ(covrrnlIO.getValidflag(),"1") 
		{
			if (isLTE(covrrnlIO.getRerateDate(),wsaaBillDate)) {
				for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
					validateCovrStatus3150(covrrnlIO);
				}
			}
			if (isEQ(wsaaCovrValid,"Y")) {
				ct05Value++;
			}
		}

		}

protected void validateCovrStatus3150(Covrpf covrrnlIO)
	{
		/*VALIDATE-RISK-STATUS*/
		if (isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()],covrrnlIO.getStatcode())) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
				validateCovrPremStatus3170(covrrnlIO);
			}
		}
		/*EXIT*/
	}

protected void validateCovrPremStatus3170(Covrpf covrrnlIO)
	{
		/*PARA*/
		if (isNE(covrrnlIO.getRider(),"00")) {
			if (isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()],covrrnlIO.getPstatcode())) {
				wsaaIndex.set(13);
				wsaaCovrValid = "Y";
			}
			return ;
		}
		if (isEQ(t5679rec.covPremStat[wsaaIndex.toInt()],covrrnlIO.getPstatcode())) {
			wsaaIndex.set(13);
			wsaaCovrValid = "Y";
		}
		/*EXIT*/
	}

protected void readTr517310c(Covrpf covrrnlIO)
	{
		wsaaWaiveCont = "N";
		wsaaZrwvflgs.set(tr517rec.zrwvflgs);
		wsaaTr517Ix = 1;
		 searchlabel1:
		{
			for (; isLT(wsaaTr517Ix,wsaaTr517Search.length); wsaaTr517Ix++){
				if (isEQ(wsaaTr517Key[wsaaTr517Ix],tr517rec.contitem)) {
					break searchlabel1;
				}
			}
			return ;
		}
		wsaaDateFound.set("N");
		while ( !(isNE(tr517rec.contitem,wsaaTr517Key[wsaaTr517Ix])
		|| isGT(wsaaTr517Ix,wsaaTr517Size)
		|| dateFound.isTrue())) {
			if (isGTE(covrrnlIO.getCrrcd(),wsaaTr517Itmfrm[wsaaTr517Ix])) {
				wsaaDateFound.set("Y");
				tr517rec.zrwvflgs.set(wsaaTr517Zrwvflgs[wsaaTr517Ix]);
				tr517rec.ctables.set(wsaaTr517Ctables[wsaaTr517Ix]);
				tr517rec.contitem.set(wsaaTr517Contitem[wsaaTr517Ix]);
			}
			else {
				wsaaTr517Ix++;
			}
		}
	
		if (!dateFound.isTrue()) {
			return ;
		}
		wsaaWaiveCont = "Y";
		tr517rec.zrwvflgs.set(wsaaZrwvflgs);
	}


protected void processWop3a10(Covrpf covrrnlIO)
	{
		/* Store the earliest rerate-date of COVRRNL-RERATE-DATE or*/
		/* COVRRNL-CPI-DATE.*/
		if (isGT(covrrnlIO.getCpiDate(),covrrnlIO.getRerateDate())) {
			wsaaEarliestRerateDate.set(covrrnlIO.getRerateDate());
		}
		else {
			wsaaEarliestRerateDate.set(covrrnlIO.getCpiDate());
		}
		wsaaCovrSumins = BigDecimal.ZERO;
		/* Determine if the WOP is life insured specific.*/
		if (covrlnbMap != null && covrlnbMap.containsKey(covrrnlIO.getChdrnum())) {
			List<Covrpf> covrList = covrlnbMap.get(covrrnlIO.getChdrnum());
			if (isNE(tr517rec.zrwvflg02, "Y")) {

				for (Covrpf c : covrList) {
					if (c.getChdrcoy().equals(covrrnlIO.getChdrcoy()) && c.getLife().equals(covrrnlIO.getLife())) {
						tr517rec.tr517Rec.set(wsaaTr517Rec);
						if (isNE(tr517rec.zrwvflg02, "Y") && isNE(c.getLife(), covrrnlIO.getLife())) {
							break;
						}
						wsaaCrtableMatch.set("N");
						check3b22(c);
						if (endFlag) {
							break;
						}
					}
				}
				endFlag = false;
				for (Covrpf c : covrList) {
					if (c.getChdrcoy().equals(covrrnlIO.getChdrcoy()) && c.getLife().equals(covrrnlIO.getLife())) {
						covrlnbPlanSuffix = c.getPlanSuffix();
						tr517rec.tr517Rec.set(wsaaTr517Rec);
						if (isNE(tr517rec.zrwvflg02, "Y") && isNE(c.getLife(), covrrnlIO.getLife())) {
							break;
						}
						/*if (isNE(c.getValidflag(), "1")) {
							continue;
						}
*/						wsaaCompCovrValid.set(SPACES);
						for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)); wsaaIndex.add(1)) {
							validateCovrStatus3a50(c);
						}
						if (isNE(wsaaCompCovrValid, "Y")) {
							continue;
						}
						wsaaCrtableMatch.set("N");
						check3a22(c, covrrnlIO.getLife());
					}
				}
			} else {
				for (Covrpf c : covrList) {
					if (c.getChdrcoy().equals(covrrnlIO.getChdrcoy())) {
						tr517rec.tr517Rec.set(wsaaTr517Rec);
						if (isNE(tr517rec.zrwvflg02, "Y") && isNE(c.getLife(), covrrnlIO.getLife())) {
							break;
						}
						wsaaCrtableMatch.set("N");
						check3b22(c);
						if (endFlag) {
							break;
						}
					}
				}
				endFlag = false;
				for (Covrpf c : covrList) {
					if (c.getChdrcoy().equals(covrrnlIO.getChdrcoy())) {
						covrlnbPlanSuffix = c.getPlanSuffix();
						tr517rec.tr517Rec.set(wsaaTr517Rec);
						if (isNE(tr517rec.zrwvflg02, "Y") && isNE(c.getLife(), covrrnlIO.getLife())) {
							break;
						}
						/*if (isNE(c.getValidflag(), "1")) {
							continue;
						}
*/						wsaaCompCovrValid.set(SPACES);
						for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)); wsaaIndex.add(1)) {
							validateCovrStatus3a50(c);
						}
						if (isNE(wsaaCompCovrValid, "Y")) {
							continue;
						}
						wsaaCrtableMatch.set("N");
						check3a22(c, covrrnlIO.getLife());
					}
				}

			}
		}
		if (isEQ(tr517rec.zrwvflg03,"Y")) {
			calcFee3b10(covrrnlIO.getCoverage());
		}

	}

protected void calcFee3b10(String coverage)
	{
		/* Read T5688 for fee method, then T5674 for the fee*/
		/* calculation subroutine.  Call the subroutine and add*/
		/* the management fees to the instalment premium.*/
		wsaaT5688Ix = 1;
		 searchlabel1:
		{
			for (; isLT(wsaaT5688Ix,wsaaT5688Rec.length); wsaaT5688Ix++){
				if (isEQ(wsaaT5688Key[wsaaT5688Ix],chdrlifIO.getCnttype())) {
					break searchlabel1;
				}
			}
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(t5688);
			stringVariable1.addExpression(" searching MRNF for : ");
			stringVariable1.addExpression(wsysSystemErrorParams);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(varcom.mrnf);
			fatalError600();
		}
		wsaaDateFound.set("N");
		while ( !(isNE(chdrlifIO.getCnttype(),wsaaT5688Key[wsaaT5688Ix])
		|| isGT(wsaaT5688Ix,wsaaT5688Size)
		|| dateFound.isTrue())) {
			if (isGTE(chdrlifIO.getOccdate(),wsaaT5688Itmfrm[wsaaT5688Ix])) {
				wsaaDateFound.set("Y");
				t5688rec.feemeth.set(wsaaT5688Feemeth[wsaaT5688Ix]);
			}
			else {
				wsaaT5688Ix++;
			}
		}
	
		if (!dateFound.isTrue()) {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(t5688);
			stringVariable2.addExpression(" searching MRNF for : ");
			stringVariable2.addExpression(wsysSystemErrorParams);
			stringVariable2.setStringInto(syserrrec.params);
			syserrrec.statuz.set(varcom.mrnf);
			fatalError600();
		}
		/* Exit section if no fee method is present.*/
		if (isEQ(t5688rec.feemeth,SPACES)) {
			return ;
		}
		wsaaT5674Ix = 1;
		 searchlabel2:
		{
			for (; isLT(wsaaT5674Ix,wsaaT5674Rec.length); wsaaT5674Ix++){
				if (isEQ(wsaaT5674Key[wsaaT5674Ix],t5688rec.feemeth)) {
					t5674rec.commsubr.set(wsaaT5674Commsubr[wsaaT5674Ix]);
					break searchlabel2;
				}
			}
			t5674rec.commsubr.set(SPACES);
		}
		/* Exit section if no fee subroutine exists.*/
		if (isEQ(t5674rec.commsubr,SPACES)) {
			return ;
		}
		/* Having got this far, management fees are applicable.*/
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(chdrlifIO.getCnttype());
		mgfeelrec.billfreq.set(chdrlifIO.getBillfreq());
		mgfeelrec.effdate.set(chdrlifIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrlifIO.getCntcurr());
		mgfeelrec.company.set(chdrlifIO.getChdrcoy().toString());
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz,varcom.oK)
		&& isNE(mgfeelrec.statuz,varcom.endp)) {
			syserrrec.statuz.set(mgfeelrec.statuz);
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			fatalError600();
		}
		if (isEQ(coverage, "01")
		&& isEQ(tr517rec.zrwvflg03,"Y")) {
			wsaaCovrSumins = wsaaCovrSumins.add(mgfeelrec.mgfee.getbigdata());
			if (isNE(mgfeelrec.mgfee, ZERO)) {
				checkCalcContTax7100();
			}
		}
	}


protected void check3b22(Covrpf covrlnbIO)
	{
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,50)
		|| crtableMatch.isTrue()); wsaaIndex.add(1)){
			if (isEQ(covrlnbIO.getCrtable(),tr517rec.ctable[wsaaIndex.toInt()])) {
				crtableMatch.setTrue();
			}
		}
		if (!crtableMatch.isTrue()
		&& isNE(tr517rec.contitem,SPACES)) {
			readTr517310c(covrlnbIO);
			if (isEQ(wsaaWaiveCont,"Y")) {
				check3b22(covrlnbIO);
			}
		}
		if (crtableMatch.isTrue()) {
		//&& isNE(covrlnbIO.getValidflag(),"1")
			wsaaEarliestRerateDate.set(varcom.vrcmMaxDate);
			endFlag = true;
			return;
		}
	}

protected void check3a22(Covrpf covrlnbIO, String covrrnlLife)
	{
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,50)
		|| crtableMatch.isTrue()); wsaaIndex.add(1)){
			if (isEQ(covrlnbIO.getCrtable(),tr517rec.ctable[wsaaIndex.toInt()])) {
				crtableMatch.setTrue();
			}
		}
		if (!crtableMatch.isTrue()
		&& isNE(tr517rec.contitem,SPACES)) {
			readTr517310c(covrlnbIO);
			if (isEQ(wsaaWaiveCont,"Y")) {
				check3a22(covrlnbIO, covrrnlLife);
				return ;
			}
		}
		if (crtableMatch.isTrue()
		&& (isEQ(tr517rec.zrwvflg02,"Y")
		|| isEQ(covrlnbIO.getLife(),covrrnlLife))) {
			if (isLT(covrlnbIO.getRerateDate(),wsaaEarliestRerateDate)
			&& isGT(covrlnbIO.getRerateDate(),0)) {
				wsaaEarliestRerateDate.set(covrlnbIO.getRerateDate());
			}
			if (isLT(covrlnbIO.getCpiDate(),wsaaEarliestRerateDate)
			&& isGT(covrlnbIO.getCpiDate(),0)) {
				wsaaEarliestRerateDate.set(covrlnbIO.getCpiDate());
			}
			readIncr3a30(covrlnbIO);
		}
	}

protected void readIncr3a30(Covrpf covrlnbIO)
	{
		Incrpf i = read3a31(covrlnbIO);
		accSumins3a38(covrlnbIO, i);
	}

protected Incrpf read3a31(Covrpf covrlnbIO)
	{
		/* Up to here in the renewal processing, the contract may have*/
		/* been automatic increased and pending increase records created*/
		/* in INCR, the subsequent billing process will apply the new*/
		/* premium in INCR, therefore it is also to accumulate for the*/
		/* sum insured of WOP, if exists.*/
		/* If INCR records exist for the contract, that is it has been*/
		/* processed by automatic increase, the CPI date will be*/
		/* incremented by 1 frequency factor in T6588. Therefore to*/
		/* locate the valid INCR records, based on the latest COVRLNB-*/
		/* CPI-DATE and reverse it by 1 frequency.*/
		wsaaT5687Ix = 1;
		 searchlabel1:
		{
			for (; isLT(wsaaT5687Ix, wsaaT5687ArrayInner.wsaaT5687Rec.length); wsaaT5687Ix++){
				if (isEQ(wsaaT5687ArrayInner.wsaaT5687Key[wsaaT5687Ix], covrlnbIO.getCrtable())) {
					break searchlabel1;
				}
			}
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(t5687);
			stringVariable1.addExpression(" searching MRNF for : ");
			stringVariable1.addExpression(wsysSystemErrorParams);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(f294);
			fatalError600();
		}
		wsaaDateFound.set("N");
		while ( !(isNE(covrlnbIO.getCrtable(), wsaaT5687ArrayInner.wsaaT5687Key[wsaaT5687Ix])
		|| isGT(wsaaT5687Ix,wsaaT5687Size)
		|| dateFound.isTrue())) {
			if (isGTE(covrlnbIO.getCrrcd(), wsaaT5687ArrayInner.wsaaT5687Itmfrm[wsaaT5687Ix])) {
				wsaaDateFound.set("Y");
				moveT56877000();
			}
			else {
				wsaaT5687Ix++;
			}
		}
		Incrpf incrmjaIO = null;
		if (!dateFound.isTrue()) {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(t5687);
			stringVariable2.addExpression(" searching MRNF for : ");
			stringVariable2.addExpression(wsysSystemErrorParams);
			stringVariable2.setStringInto(syserrrec.params);
			syserrrec.statuz.set(f294);
			fatalError600();
		}
		/* If no anniversary method exists on T5687 just accumulate*/
		/* the INSTPREM of this component for WOP sumins*/
		if (isEQ(t5687rec.anniversaryMethod,SPACES)) {
			/*        MOVE MRNF               TO INCRREF-STATUZ                */
			return incrmjaIO;
		}
		wsaaT6658Ix = 1;
		 searchlabel2:
		{
			for (; isLT(wsaaT6658Ix,wsaaT6658Rec.length); wsaaT6658Ix++){
				if (isEQ(wsaaT6658Key[wsaaT6658Ix],t5687rec.anniversaryMethod)) {
					break searchlabel2;
				}
			}
			StringUtil stringVariable3 = new StringUtil();
			stringVariable3.addExpression(t6658);
			stringVariable3.addExpression(" searching MRNF for : ");
			stringVariable3.addExpression(wsysSystemErrorParams);
			stringVariable3.setStringInto(syserrrec.params);
			syserrrec.statuz.set(h036);
			fatalError600();
		}
		wsaaDateFound.set("N");
		while ( !(isNE(t5687rec.anniversaryMethod,wsaaT6658Key[wsaaT6658Ix])
		|| isGT(wsaaT6658Ix,wsaaT6658Size)
		|| dateFound.isTrue())) {
			if (isGTE(covrlnbIO.getCrrcd(),wsaaT6658Itmfrm[wsaaT6658Ix])) {
				wsaaDateFound.set("Y");
				t6658rec.billfreq.set(wsaaT6658Billfreq[wsaaT6658Ix]);
			}
			else {
				wsaaT6658Ix++;
			}
		}

		if (!dateFound.isTrue()) {
			StringUtil stringVariable4 = new StringUtil();
			stringVariable4.addExpression(t6658);
			stringVariable4.addExpression(" searching MRNF for : ");
			stringVariable4.addExpression(wsysSystemErrorParams);
			stringVariable4.setStringInto(syserrrec.params);
			syserrrec.statuz.set(h036);
			fatalError600();
		}
		if (isEQ(t6658rec.billfreq,"00")
		|| isEQ(covrlnbIO.getCpiDate(),varcom.vrcmMaxDate)) {
			/*        MOVE MRNF               TO INCRREF-STATUZ                */
			return incrmjaIO;
		}
		/*    MOVE SPACES                 TO DTC2-DATCON2-REC.             */
		/*    MOVE T6658-BILLFREQ         TO WSAA-FREQ-FACTOR.             */
		/*    COMPUTE DTC2-FREQ-FACTOR     =  WSAA-FREQ-FACTOR * -1.       */
		/*    MOVE '01'                   TO DTC2-FREQUENCY.               */
		/*    MOVE COVRLNB-CPI-DATE       TO DTC2-INT-DATE-1.              */
		/*    CALL 'DATCON2'           USING DTC2-DATCON2-REC.             */
		/*    IF DTC2-STATUZ              NOT = O-K                        */
		/*        MOVE DTC2-DATCON2-REC   TO SYSR-PARAMS                   */
		/*        MOVE DTC2-STATUZ        TO SYSR-STATUZ                   */
		/*        PERFORM 600-FATAL-ERROR.                                 */
		/*    MOVE COVRLNB-CHDRCOY        TO INCRREF-CHDRCOY.              */
		/*    MOVE COVRLNB-CHDRNUM        TO INCRREF-CHDRNUM.              */
		/*    MOVE COVRLNB-LIFE           TO INCRREF-LIFE.                 */
		/*    MOVE COVRLNB-COVERAGE       TO INCRREF-COVERAGE.             */
		/*    MOVE COVRLNB-RIDER          TO INCRREF-RIDER.                */
		/*    MOVE COVRLNB-PLAN-SUFFIX    TO INCRREF-PLAN-SUFFIX.          */
		/*    MOVE DTC2-INT-DATE-2        TO INCRREF-CRRCD.                */
		/*    MOVE INCRREFREC             TO INCRREF-FORMAT.               */
		/*    MOVE READR                  TO INCRREF-FUNCTION.             */
		/*    CALL 'INCRREFIO'            USING INCRREF-PARAMS.            */
		/*    IF  INCRREF-STATUZ           NOT = O-K                       */
		/*    AND INCRREF-STATUZ           NOT = MRNF                      */
		/*       MOVE INCRREF-PARAMS      TO SYSR-PARAMS                   */
		/*       MOVE INCRREF-STATUZ      TO SYSR-STATUZ                   */
		/*       PERFORM 600-FATAL-ERROR.                                  */
		if(incrmjaMap!=null&&incrmjaMap.containsKey(covrlnbIO.getChdrnum())){
			for(Incrpf i:incrmjaMap.get(covrlnbIO.getChdrnum())){
				if(i.getChdrcoy().equals(covrlnbIO.getChdrcoy())&&i.getLife().equals(covrlnbIO.getLife())&&i.getCoverage().equals(covrlnbIO.getCoverage())&&i.getRider().equals(covrlnbIO.getRider())&&i.getPlnsfx()==covrlnbIO.getPlanSuffix()){
					incrmjaIO = i;
					break;
				}
			}
		}
		if (incrmjaIO!=null) {
		datcon2rec.datcon2Rec.set(SPACES);
		wsaaFreqFactor.set(t6658rec.billfreq);
			datcon2rec.freqFactor.set(wsaaFreqFactor);
		datcon2rec.frequency.set("01");
			datcon2rec.intDate1.set(incrmjaIO.getCrrcd());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
			if (isEQ(wsaaFirstIncrDate, varcom.vrcmMaxDate)) {
				wsaaEarliestRerateDate.set(datcon2rec.intDate2);
				wsaaFirstIncrDate.set(datcon2rec.intDate2);
			}
			else {
				if (isLT(datcon2rec.intDate2, wsaaFirstIncrDate)) {
					wsaaEarliestRerateDate.set(datcon2rec.intDate2);
					wsaaFirstIncrDate.set(datcon2rec.intDate2);
				}
				else {
					wsaaEarliestRerateDate.set(wsaaFirstIncrDate);
				}
			}
		}
		return incrmjaIO;
	}

protected void accSumins3a38(Covrpf covrlnbIO, Incrpf incrmjaIO)
	{
		/*    IF INCRREF-STATUZ               = MRNF*/
		/*       ADD COVRLNB-INSTPREM        TO WSAA-COVR-SUMINS*/
		/*    ELSE*/
		/*       ADD INCRREF-NEWINST         TO WSAA-COVR-SUMINS*/
		/*    END-IF.*/
		if (isEQ(tr517rec.zrwvflg04,"Y")) {
			if (isEQ(covrlnbIO.getRider(),"00")) {
				/*           IF INCRREF-STATUZ               = MRNF                */
				if (incrmjaIO == null) {
					wsaaCovrSumins = covrlnbIO.getSumins();
					wsaaMainCrtable.set(covrlnbIO.getCrtable());
					wsaaMainCoverage.set(covrlnbIO.getCoverage());
					wsaaMainCessdate.set(covrlnbIO.getRiskCessDate());
					wsaaMainPcessdte.set(covrlnbIO.getPremCessDate());
					wsaaMainMortclass.set(covrlnbIO.getMortcls());
					wsaaMainLife.set(covrlnbIO.getLife());
				}
				else {
					/*              ADD INCRREF-NEWINST     TO WSAA-COVR-SUMINS        */
					wsaaCovrSumins.add(incrmjaIO.getNewinst());
				}
			}
			else {
				/*           IF INCRREF-STATUZ               = MRNF                */
				if (incrmjaIO == null) {
					wsaaCovrSumins = wsaaCovrSumins.subtract(covrlnbIO.getSumins());
					a200CalcBenefitAmount(covrlnbIO);
				}
				else {
					wsaaCovrSumins = wsaaCovrSumins.subtract(incrmjaIO.getNewinst());
				}
			}
		}
		else {
			/*        IF INCRREF-STATUZ               = MRNF                   */
			if (incrmjaIO == null) {
				wsaaCovrSumins = wsaaCovrSumins.add(covrlnbIO.getInstprem());
				if (covrlnbIO.getInstprem().compareTo(BigDecimal.ZERO)>0)						
					checkCalcCompTax7000(covrlnbIO,incrmjaIO);
			}
			else {
				/*           ADD INCRREF-NEWINST         TO WSAA-COVR-SUMINS       */
				wsaaCovrSumins = wsaaCovrSumins.add(incrmjaIO.getNewinst());
				if (incrmjaIO.getNewinst().compareTo(BigDecimal.ZERO)>0)						
					checkCalcCompTax7000(covrlnbIO,incrmjaIO);
			}
		}
	}
protected void readTr52d()
{
	itemIO.setDataKey(SPACES);
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(chdrlifIO.getChdrcoy());
	itemIO.setItemtabl(tr52d);
	itemIO.setItemitem(chdrlifIO.getReg());
	itemIO.setFormat(formatsInner.itemrec);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(), varcom.oK)
	&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(itemIO.getParams());
		syserrrec.statuz.set(itemIO.getStatuz());
		fatalError600();
	}
	if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlifIO.getChdrcoy());
		itemIO.setItemtabl(tr52d);
		itemIO.setItemitem("***");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
	}
	tr52drec.tr52dRec.set(itemIO.getGenarea());
}
protected void checkCalcCompTax7000(Covrpf covrlnbIO,Incrpf incrmjaIO)
{
	wsaaTax.set(0);
	if (isEQ(tr52drec.txcode, SPACES)) {
		return ;
	}
	
	/* Read table TR52E                                                */
	wsaaTr52eKey.set(SPACES);
	wsaaTr52eTxcode.set(tr52drec.txcode);
	wsaaTr52eCnttype.set(chdrlifIO.getCnttype());
	wsaaTr52eCrtable.set(covrlnbIO.getCrtable());
	a500ReadTr52e();
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlifIO.getCnttype());
		wsaaTr52eCrtable.set("****");
		a500ReadTr52e();
	}
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set("***");
		wsaaTr52eCrtable.set("****");
		a500ReadTr52e();
	}
	/* Call TR52D tax subroutine                                       */
	if (isNE(tr52erec.taxind01, "Y")) {
		return ;
	}
	initialize(txcalcrec.linkRec);
	txcalcrec.function.set("CALC");
	txcalcrec.statuz.set(Varcom.oK);
	txcalcrec.chdrcoy.set(chdrlifIO.getChdrcoy());
	txcalcrec.chdrnum.set(chdrlifIO.getChdrnum());
	txcalcrec.life.set(covrlnbIO.getLife());
	txcalcrec.coverage.set(covrlnbIO.getCoverage());
	txcalcrec.rider.set(covrlnbIO.getRider());
	txcalcrec.planSuffix.set(ZERO);
	txcalcrec.crtable.set(covrlnbIO.getCrtable());
	txcalcrec.cnttype.set(chdrlifIO.getCnttype());
	txcalcrec.register.set(chdrlifIO.getReg());
	txcalcrec.taxrule.set(wsaaTr52eKey);
	wsaaRateItem.set(SPACES);
	txcalcrec.ccy.set(chdrlifIO.getCntcurr());
	wsaaCntCurr.set(chdrlifIO.getCntcurr());
	wsaaTxitem.set(tr52erec.txitem);
	txcalcrec.rateItem.set(wsaaRateItem);
	txcalcrec.effdate.set(chdrlifIO.getOccdate());
	txcalcrec.transType.set("PREM");
	txcalcrec.taxType[1].set(SPACES);
	txcalcrec.taxType[2].set(SPACES);
	txcalcrec.taxAmt[1].set(ZERO);
	txcalcrec.taxAmt[2].set(ZERO);
	txcalcrec.taxAbsorb[1].set(SPACES);
	txcalcrec.taxAbsorb[2].set(SPACES);
	
	if(ptdateConfig)
	{
		if ((incrmjaIO == null && (covrlnbIO.getInstprem().compareTo(BigDecimal.ZERO)>0))
				|| ((incrmjaIO == null || incrmjaIO.getNewinst().compareTo(BigDecimal.ZERO)>0))) { //check
			if (isEQ(tr52erec.zbastyp, "Y")) {
				if(incrmjaIO == null)
					txcalcrec.amountIn.set(covrlnbIO.getZbinstprem());
				else 
					txcalcrec.amountIn.set(incrmjaIO.getZbnewinst());
			}
			else {
				if(incrmjaIO == null)
					txcalcrec.amountIn.set(covrlnbIO.getInstprem());
				else
					txcalcrec.amountIn.set(incrmjaIO.getNewinst());
			}
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, Varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
			|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaTax.add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaTax.add(txcalcrec.taxAmt[2]);
				}
			}
		}
	}
	else {
	if ((incrmjaIO == null && (covrlnbIO.getInstprem().compareTo(BigDecimal.ZERO)>0))
			|| ((incrmjaIO.getNewinst().compareTo(BigDecimal.ZERO)>0) )) { //check
		if (isEQ(tr52erec.zbastyp, "Y")) {
			if(incrmjaIO == null)
				txcalcrec.amountIn.set(covrlnbIO.getZbinstprem());
			else 
				txcalcrec.amountIn.set(incrmjaIO.getZbnewinst());
		}
		else {
			if(incrmjaIO == null)
				txcalcrec.amountIn.set(covrlnbIO.getInstprem());
			else
				txcalcrec.amountIn.set(incrmjaIO.getNewinst());
		}
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, Varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[2]);
			}
		}
	
	}
	}	// changed by yy for ILIFE-3960
	
	wsaaCovrSumins = wsaaCovrSumins.add(wsaaTax.getbigdata());
}

protected void checkCalcContTax7100()
{	
	wsaaTax.set(0);	
	if (isEQ(tr52drec.txcode, SPACES)) {
		return ;
	}
	/* Read table TR52E                                                */
	wsaaTr52eKey.set(SPACES);
	wsaaTr52eTxcode.set(tr52drec.txcode);
	wsaaTr52eCnttype.set(chdrlifIO.getCnttype());
	wsaaTr52eCrtable.set("****");
	a500ReadTr52e();
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set("***");
		wsaaTr52eCrtable.set("****");
		a500ReadTr52e();
	}
	if (isNE(tr52erec.taxind02, "Y")) {
		return ;
	}
	/* Call TR52D tax subroutine                                       */
	initialize(txcalcrec.linkRec);
	txcalcrec.function.set("CALC");
	txcalcrec.statuz.set(Varcom.oK);
	txcalcrec.chdrcoy.set(chdrlifIO.getChdrcoy());
	txcalcrec.chdrnum.set(chdrlifIO.getChdrnum());
	txcalcrec.life.set(SPACES);
	txcalcrec.coverage.set(SPACES);
	txcalcrec.rider.set(SPACES);
	txcalcrec.crtable.set(SPACES);
	txcalcrec.planSuffix.set(ZERO);
	txcalcrec.cnttype.set(chdrlifIO.getCnttype());
	txcalcrec.register.set(chdrlifIO.getReg());
	txcalcrec.taxrule.set(wsaaTr52eKey);
	wsaaRateItem.set(SPACES);
	txcalcrec.ccy.set(chdrlifIO.getCntcurr());
	wsaaCntCurr.set(chdrlifIO.getCntcurr());
	wsaaTxitem.set(tr52erec.txitem);
	txcalcrec.rateItem.set(wsaaRateItem);
	txcalcrec.taxType[1].set(SPACES);
	txcalcrec.taxType[2].set(SPACES);
	txcalcrec.taxAmt[1].set(ZERO);
	txcalcrec.taxAmt[2].set(ZERO);
	txcalcrec.taxAbsorb[1].set(SPACES);
	txcalcrec.taxAbsorb[2].set(SPACES);
	txcalcrec.amountIn.set(mgfeelrec.mgfee);
	txcalcrec.effdate.set(chdrlifIO.getOccdate());
	txcalcrec.transType.set("CNTF");
	callProgram(tr52drec.txsubr, txcalcrec.linkRec);
	if (isNE(txcalcrec.statuz, Varcom.oK)) {
		syserrrec.params.set(txcalcrec.linkRec);
		syserrrec.statuz.set(txcalcrec.statuz);
		fatalError600();
	}
	if (isGT(txcalcrec.taxAmt[1], ZERO)
	|| isGT(txcalcrec.taxAmt[2], ZERO)) {
		if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaTax.add(txcalcrec.taxAmt[1]);
		}
		if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaTax.add(txcalcrec.taxAmt[2]);
		}
	}
	wsaaCovrSumins = wsaaCovrSumins.add(wsaaTax.getbigdata());
}

protected void a500ReadTr52e()
{
	tr52erec.tr52eRec.set(SPACES);
	Itempf itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(chdrlifIO.getChdrcoy().toString());
	itempf.setItmfrm(new BigDecimal(chdrlifIO.getOccdate().toString()));
	itempf.setItmto(new BigDecimal(chdrlifIO.getOccdate().toString()));
	itempf.setItemtabl(tr52e);
	itempf.setItemitem( wsaaTr52eKey.toString());
	itempf.setValidflag("1");
	List<Itempf> itempfList = itemDAO.findByItemDates(itempf);		 

	if ((itempfList == null || itempfList.size() == 0 )
			&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
		syserrrec.params.set(wsaaTr52eKey);
		syserrrec.statuz.set("MRNF");
		fatalError600();
	}
	if (itempfList != null && itempfList.size() != 0 ) {
		tr52erec.tr52eRec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
	}	
}

protected void validateCovrStatus3a50(Covrpf covrlnbIO)
	{
		/*A60-VALIDATE-RISK-STATUS*/
		if (isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()],covrlnbIO.getStatcode())) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
				validateCovrPremStatus3a70(covrlnbIO);
			}
		}
		/*A69-EXIT*/
	}

protected void validateCovrPremStatus3a70(Covrpf covrlnbIO)
	{
		/*A80-PARA*/
		if (isEQ(covrlnbIO.getRider(),"00")) {
			if (isEQ(t5679rec.covPremStat[wsaaIndex.toInt()],covrlnbIO.getPstatcode())) {
				wsaaIndex.set(13);
				wsaaCompCovrValid.set("Y");
			}
		}
		else {
			if (isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()],covrlnbIO.getPstatcode())) {
				wsaaIndex.set(13);
				wsaaCompCovrValid.set("Y");
			}
		}
		/*A99-EXIT*/
	}

protected Covrpf updateCovr3200(Covrpf covrrnlIO)
	{
		wsaaCovrUpdated = "Y";
		/*  Store the rerate date.*/
		if(isFoundPro &&incrpf != null){
			wsaaRerateStore.set(wsaaProCpiDate);
		}
		else{
		wsaaRerateStore.set(covrrnlIO.getRerateDate());
		}
		/*  Default to a TRUE rerate.*/
		wsaaRerateType.set("1");
		wsaaInstPrem.set(ZERO);
		wsaaZbinstprem.set(ZERO);
		wsaaZlinstprem.set(ZERO);
		/* Read the COVR record before rewriting the validflag '2'*/
		/* record using a different logical view to avoid picking up the*/
		/* new COVR record when reading the next record.*/
		
		Covrpf covrIO = new Covrpf(covrrnlIO);
		/* Recalculate premium if premium cessation date not = rerate*/
		/* date.*/
		if (isEQ(covrrnlIO.getPremCessDate(),covrrnlIO.getRerateDate())) {
			wsaaFullyPaid = "Y";
			pastCeaseDate3290(covrrnlIO, covrIO);
			return covrIO;
		}
		wsaaFullyPaid = "N";
		calculatePremium3250(covrrnlIO, covrIO);
		/* Rewrite Validflag '2' COVR with new current to date.  Set COV*/
		/* rerate and rerate from dates based on LEXTBBR cessation dates*/
		/* Store the COVR-RERATE-DATE which will be used to initialise*/
		/* the PAYR-EFFDATE*/
		covrrnlIO.setCurrto(wsaaRerateStore.toInt());
		covrrnlIO.setVarSumInsured(BigDecimal.ZERO);
		covrrnlIO.setValidflag("2");
		
		if(updateCovrrnlList == null){
			updateCovrrnlList = new ArrayList<>();
		}
		updateCovrrnlList.add(covrrnlIO);

		/* Read LEXTBRR for unexpired records.*/
		checkLexts3300(covrIO, covrrnlIO);
		statistics5000();
		return covrIO;
	}


protected void calculatePremium3250(Covrpf covrrnlIO, Covrpf covrIO)
	{
		/* Read T5687 for the premium calculation method and*/
		/* recalculation frequency.*/
		wsaaT5687Ix = 1;
		 searchlabel1:
		{
			for (; isLT(wsaaT5687Ix, wsaaT5687ArrayInner.wsaaT5687Rec.length); wsaaT5687Ix++){
				if (isEQ(wsaaT5687ArrayInner.wsaaT5687Key[wsaaT5687Ix], covrrnlIO.getCrtable())) {
					break searchlabel1;
				}
			}
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(t5687);
			stringVariable1.addExpression(" searching MRNF for : ");
			stringVariable1.addExpression(wsysSystemErrorParams);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(f294);
			fatalError600();
		}
		wsaaDateFound.set("N");
		while ( !(isNE(covrrnlIO.getCrtable(), wsaaT5687ArrayInner.wsaaT5687Key[wsaaT5687Ix])
		|| isGT(wsaaT5687Ix,wsaaT5687Size)
		|| dateFound.isTrue())) {
			if (isGTE(covrrnlIO.getCrrcd(), wsaaT5687ArrayInner.wsaaT5687Itmfrm[wsaaT5687Ix])) {
				wsaaDateFound.set("Y");
				moveT56877000();
			}
			else {
				wsaaT5687Ix++;
			}
		}
	
		if (!dateFound.isTrue()) {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(t5687);
			stringVariable2.addExpression(" searching MRNF for : ");
			stringVariable2.addExpression(wsysSystemErrorParams);
			stringVariable2.setStringInto(syserrrec.params);
			syserrrec.statuz.set(f294);
			fatalError600();
		}
		/* If no premium method exists on T5687 leave the section.*/
		
		if(ptdateConfig) 
		{	
			if (isEQ(t5687rec.premmeth,SPACES)) {
				if (isEQ(t5687rec.bbmeth,SPACES)) {
					return ;
				}
			}
		}
		else {
			if (isEQ(t5687rec.premmeth,SPACES)) {
				return ;
			}
		}
		/*  Determine whether this is TRUE or LEXT triggered re-rate*/
		/*  date*/
		setRerateType3410(covrrnlIO);
		/* Read LIFELNB to obtain sex & age for premium calculation.*/
		premiumrec.premiumRec.set(SPACES);
		Lifepf lifelnbIO = null;
		if(lifelnbMap!=null&&lifelnbMap.containsKey(covrIO.getChdrnum())){
			List<Lifepf> lifeList = lifelnbMap.get(covrIO.getChdrnum());
			for(Lifepf l:lifeList){
				if(l.getChdrcoy().equals(covrIO.getChdrcoy())&&l.getLife().equals(covrIO.getLife())&&l.getJlife().equals("00")){
					lifelnbIO = l;
					break;
				}
			}
		}
		if (lifelnbIO == null) {
			syserrrec.params.set(covrIO.getChdrnum());
			fatalError600();
		}
		premiumrec.lsex.set(lifelnbIO.getCltsex());
		calculateAnb3260(lifelnbIO.getCltdob());
		premiumrec.lage.set(wsaaAnb);
		/* Read T5675 for premium calculation subroutine call.*/
		/* Check if a joint life is present*/
		checkJointLife3270(covrIO);
		wsaaT5675Ix = 1;
		wsaaT5534Ix = 1;
		 searchlabel2:
			 
		if(ptdateConfig)	 
		{
			if(isNE(t5687rec.premmeth,SPACES)) {
				for (; isLT(wsaaT5675Ix,wsaaT5675Rec.length); wsaaT5675Ix++){
					if (isEQ(wsaaT5675Key[wsaaT5675Ix],itemIO.getItemitem())) {
						t5675rec.premsubr.set(wsaaT5675Premsubr[wsaaT5675Ix]);
						//ILIFE-3472 Code Promotion for VPMS externalization of LIFE TRX product calculations to LIFE Target2 Environment
						wsaaPremMeth.set(wsaaT5675Key[wsaaT5675Ix]);
						break searchlabel2;
					}
				}
				StringUtil stringVariable3 = new StringUtil();
				stringVariable3.addExpression(t5675);
				stringVariable3.addExpression(" searching MRNF for : ");
				stringVariable3.addExpression(wsysSystemErrorParams);
				stringVariable3.setStringInto(syserrrec.params);
				syserrrec.statuz.set(varcom.mrnf);
				fatalError600();
			}
			
			else {
				for (; isLT(wsaaT5534Ix,wsaaT5534Rec.length); wsaaT5534Ix++){
					if (isEQ(wsaaT5534Key[wsaaT5534Ix],itemIO.getItemitem())) {
						t5534rec.subprog.set(wsaaT5534Subprog[wsaaT5534Ix]);
						//ILIFE-3472 Code Promotion for VPMS externalization of LIFE TRX product calculations to LIFE Target2 Environment
						wsaaPremMeth.set(wsaaT5534Key[wsaaT5534Ix]);
						break searchlabel2;
					}
				}
				StringUtil stringVariable3 = new StringUtil();
				stringVariable3.addExpression(t5534);
				stringVariable3.addExpression(" searching MRNF for : ");
				stringVariable3.addExpression(wsysSystemErrorParams);
				stringVariable3.setStringInto(syserrrec.params);
				syserrrec.statuz.set(varcom.mrnf);
				fatalError600();
			}
		}
		else{	 
		
			for (; isLT(wsaaT5675Ix,wsaaT5675Rec.length); wsaaT5675Ix++){
				if (isEQ(wsaaT5675Key[wsaaT5675Ix],itemIO.getItemitem())) {
					t5675rec.premsubr.set(wsaaT5675Premsubr[wsaaT5675Ix]);
					//ILIFE-3472 Code Promotion for VPMS externalization of LIFE TRX product calculations to LIFE Target2 Environment
					wsaaPremMeth.set(wsaaT5675Key[wsaaT5675Ix]);
					break searchlabel2;
				}
			}
			StringUtil stringVariable3 = new StringUtil();
			stringVariable3.addExpression(t5675);
			stringVariable3.addExpression(" searching MRNF for : ");
			stringVariable3.addExpression(wsysSystemErrorParams);
			stringVariable3.setStringInto(syserrrec.params);
			syserrrec.statuz.set(varcom.mrnf);
			fatalError600();
		}
		/*  The PAYR file must be read to get the billing frequency.*/
		Payrpf payrIO = null;
		if(payrMap!=null&&payrMap.containsKey(chdrlifIO.getChdrnum())){
			for(Payrpf p:payrMap.get(chdrlifIO.getChdrnum())){
				if(p.getChdrcoy().equals(chdrlifIO.getChdrcoy().toString())&&p.getPayrseqno() == 1){
					payrIO = p;
					break;
				}
			}
		}
		if (payrIO == null) {
			syserrrec.params.set(chdrlifIO.getChdrnum());
			fatalError600();
		} else {//IJTI-320 START
			wsaaBillfreqx.set(payrIO.getBillfreq());
			premiumrec.mop.set(payrIO.getBillchnl());
			premiumrec.billfreq.set(payrIO.getBillfreq());
		}
		//IJTI-320 END
		premiumrec.calcTotPrem.set(BigDecimal.ZERO);
		premiumrec.zstpduty01.set(BigDecimal.ZERO);
		premiumrec.cnttype.set(chdrlifIO.getCnttype());
		premiumrec.crtable.set(covrIO.getCrtable());
		premiumrec.chdrChdrcoy.set(covrIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(covrIO.getChdrnum());
		premiumrec.lifeLife.set(covrIO.getLife());
		premiumrec.lifeJlife.set(covrIO.getJlife());
		premiumrec.covrRider.set(covrIO.getRider());
		premiumrec.covrCoverage.set(covrIO.getCoverage());
		premiumrec.effectdt.set(wsaaLastRrtDate);
		premiumrec.mortcls.set(covrIO.getMortcls());
		premiumrec.currcode.set(covrIO.getPremCurrency());
		premiumrec.termdate.set(covrIO.getPremCessDate());
		/*  Calculate the remaining term. This is re-rate type*/
		/*  dependent. For TRUE re-rates this is the re-rate date.*/
		/*  For LEXT re-rates this is the LAST TRUE RERATE DATE.*/
		/*  Either way the correct result is stored as*/
		/*  WSAA-LAST-RRT-DATE.*/
		datcon3rec.function.set(SPACES);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		compute(premiumrec.duration, 5).set(add(datcon3rec.freqFactor,.99999));
		premiumrec.ratingdate.set(covrIO.getRerateFromDate());
		premiumrec.reRateDate.set(covrIO.getRerateDate());
		premiumrec.calcPrem.set(covrIO.getInstprem());
		premiumrec.calcBasPrem.set(covrIO.getZbinstprem());
		premiumrec.calcLoaPrem.set(covrIO.getZlinstprem());
		/* Adjust installment for plan processing.*/
		if(isFoundPro){
			wsaaCovrSumins = wsaaCovrSumins.add(wsaaCovrSuminInc.getbigdata());
		}
		covrIO.setSumins(wsaaCovrSumins);
		if (isEQ(covrlnbPlanSuffix, ZERO)
		&& isNE(chdrlifIO.getPolinc(),1)) {
			compute(premiumrec.sumin, 3).setRounded(div(premiumrec.sumin,chdrlifIO.getPolsum()));
		}
		else {
			premiumrec.sumin.set(covrIO.getSumins());
		}
		compute(premiumrec.sumin, 3).setRounded(mult(premiumrec.sumin,chdrlifIO.getPolinc()));
		premiumrec.function.set("CALC");
		/* Get any Annuity values required for the calculation.  If the*/
		/* coverage is not an annuity, initialise these values.*/
		getAnny3280(covrIO);
		premiumrec.language.set(bsscIO.getLanguage());
		//ILIFE-8248
		if (("PMEX".equals(t5675rec.premsubr.toString().trim())) && (lnkgFlag == true)) {

			if (null == covrIO.getLnkgsubrefno() || covrIO.getLnkgsubrefno().trim().isEmpty()) {
				premiumrec.lnkgSubRefNo.set(SPACE);
			} else {
				premiumrec.lnkgSubRefNo.set(covrIO.getLnkgsubrefno().trim());//IJTI-1485
			}

			if (null == covrIO.getLnkgno() || covrIO.getLnkgno().trim().isEmpty()) {
				premiumrec.linkcov.set(SPACE);
			} else {
					LinkageInfoService linkgService = new LinkageInfoService();
					FixedLengthStringData linkgCov = new FixedLengthStringData(
							linkgService.getLinkageInfo(covrIO.getLnkgno().trim()));//IJTI-1485
					premiumrec.linkcov.set(linkgCov);
			}
		}
		// ILIFE-8248
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		//ILIFE-4795
		//ILIFE-8502
				premiumrec.commTaxInd.set("Y");
				List<Incrpf> incrpfList=incrpfDAO.getIncrpfList(covrIO.getChdrcoy(),covrIO.getChdrnum());
				if(incrpfList !=null && !incrpfList.isEmpty()) {
				if(incrpfList.get(0).getLastSum().intValue() !=0) {
				premiumrec.prevSumIns.set(incrpfList.get(0).getLastSum().intValue());
				}else {
					premiumrec.prevSumIns.set(ZERO);
				}
			}else {
				premiumrec.prevSumIns.set(ZERO);
			}
				clntpf = clntpfDAO.searchClntRecord("CN",bsprIO.getFsuco().toString(),lifelnbIO.getLifcnum());
				clntDao = DAOFactory.getClntpfDAO();
				clnt=clntDao.getClientByClntnum(lifelnbIO.getLifcnum());
				if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
					premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
				}else{
					premiumrec.stateAtIncep.set(clntpf.getClntStateCd());//IJTI-1485
				}
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && (er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX"))))	//ILIFE-7584
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else{
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			/* ILIFE-3472 Code Promotion for VPMS externalization of LIFE TRX product calculations to LIFE Target2 Environment start*/
			premiumrec.premMethod.set(wsaaPremMeth);
			premiumrec.cnttype.set(chdrlifIO.getCnttype());
			/* ILIFE-3472 end */
			//START OF ILIFE-7584
			premiumrec.cownnum.set(SPACES);
			premiumrec.occdate.set(ZERO);
			if(t5675rec.premsubr.toString().trim().equals("PMEX")){
				premiumrec.setPmexCall.set("Y");
				premiumrec.inputPrevPrem.set(ZERO);//ILIFE-8537
				premiumrec.updateRequired.set("N");
				premiumrec.validflag.set("Y");
				premiumrec.cownnum.set(chdrlifIO.getCownnum());
				premiumrec.occdate.set(chdrlifIO.getOccdate());
			}
			//END OF ILIFE-7584
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		//ILIFE-7845 
		premiumrec.riskPrem.set(BigDecimal.ZERO);
		riskPremflag = FeaConfg.isFeatureExist(covrIO.getChdrcoy().trim(), RISKPREM_FEATURE_ID, appVars, "IT");//IJTI-1485
		if(riskPremflag)
		{
			premiumrec.riskPrem.set(ZERO);
			premiumrec.cnttype.set(chdrlifIO.getCnttype());
			premiumrec.crtable.set(covrIO.getCrtable());
			premiumrec.calcTotPrem.set(add(premiumrec.calcPrem.getbigdata(), premiumrec.zstpduty01.getbigdata()));
			callProgram("RISKPREMIUM", premiumrec.premiumRec);
			
	}
		//ILIFE-7845  end
		/*Ticket #IVE-792 - End		*/
		/*Ticket #ILIFE-2005 - END	*/
		if (isNE(premiumrec.statuz,varcom.oK)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}

	}


protected void calculateAnb3260(int cltdob)
	{
		wsaaAnb.set(ZERO);
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(bsscIO.getLanguage());
		agecalcrec.cnttype.set(chdrlifIO.getCnttype());
		agecalcrec.intDate1.set(cltdob);
		agecalcrec.intDate2.set(wsaaLastRrtDate);
		agecalcrec.company.set(bsprIO.getFsuco());
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.params.set(agecalcrec.agecalcRec);
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
		wsaaAnb.set(agecalcrec.agerating);
	}

protected void checkJointLife3270(Covrpf covrIO)
 {
		Lifepf lifelnbIO = null;
		if (lifelnbMap != null && lifelnbMap.containsKey(covrIO.getChdrnum())) {
			List<Lifepf> lifeList = lifelnbMap.get(covrIO.getChdrnum());
			for (Lifepf l : lifeList) {
				if (l.getChdrcoy().equals(covrIO.getChdrcoy()) && l.getLife().equals(covrIO.getLife())
						&& l.getJlife().equals("01")) {
					lifelnbIO = l;
					break;
				}
			}
		}

		if (lifelnbIO != null) {
			itemIO.setItemitem(t5687rec.jlPremMeth);
			premiumrec.jlsex.set(lifelnbIO.getCltsex());
			calculateAnb3260(lifelnbIO.getCltdob());
			premiumrec.jlage.set(wsaaAnb);
		} else {
			if(ptdateConfig)
			{
				if (isNE(t5687rec.premmeth,SPACES)) {
					itemIO.setItemitem(t5687rec.premmeth);
				}
				else {
					itemIO.setItemitem(t5687rec.bbmeth);
				}
			}
			else
			{
				itemIO.setItemitem(t5687rec.premmeth);
			}
			premiumrec.jlsex.set(SPACES);
			premiumrec.jlage.set(ZERO);
		}
	}

protected void getAnny3280(Covrpf covrIO)
 {
		Annypf annyIO = null;
		if (annyMap != null && annyMap.containsKey(covrIO.getChdrnum())) {
			for (Annypf a : annyMap.get(covrIO.getChdrnum())) {
				if (a.getChdrcoy().equals(covrIO.getChdrcoy()) && a.getLife().equals(covrIO.getLife())
						&& a.getCoverage().equals(covrIO.getCoverage()) && a.getRider().equals(covrIO.getRider())
						&& a.getPlanSuffix() == covrIO.getPlanSuffix()) {
					annyIO = a;
					break;
				}
			}
		}
		if (annyIO != null) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		} else {
			premiumrec.advance.set(SPACES);
			premiumrec.arrears.set(SPACES);
			premiumrec.freqann.set(SPACES);
			premiumrec.withprop.set(SPACES);
			premiumrec.withoprop.set(SPACES);
			premiumrec.ppind.set(SPACES);
			premiumrec.nomlife.set(SPACES);
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
	}


protected void pastCeaseDate3290(Covrpf covrrnlIO,Covrpf covrIO)
	{
		/* Rewrite Validflag '2' COVR with new current to date.*/
		covrrnlIO.setCurrto(wsaaRerateStore.toInt());
		covrrnlIO.setVarSumInsured(BigDecimal.ZERO);
		covrrnlIO.setValidflag("2");
		if(updateCovrrnlList == null){
			updateCovrrnlList = new ArrayList<>();
		}
		updateCovrrnlList.add(covrrnlIO);
		
		/* Write a new COVR , set the rerate date to the earliest rerate*/
		/* date among the related components if it is earlier than the*/
		/* new rerate date and CPI date of the WOP component.*/
		if (isEQ(covrIO.getRider(),"00")
		|| isEQ(covrrnlIO.getRider(), "  ")) {
			if (isNE(t5679rec.setCovPremStat,SPACES)) {
				covrIO.setPstatcode(t5679rec.setCovPremStat.toString());
			}
		}
		else {
			if (isNE(t5679rec.setRidPremStat,SPACES)) {
				covrIO.setPstatcode(t5679rec.setRidPremStat.toString());
			}
		}
		compute(wsaaNewTranno, 0).set(add(chdrlifIO.getTranno(),1));
		covrIO.setTranno(wsaaNewTranno.toInt());
		covrIO.setCurrfrom(wsaaRerateStore.toInt());
		covrIO.setCurrto(varcom.vrcmMaxDate.toInt());
		covrIO.setRerateDate(wsaaEarliestRerateDate.toInt());
		if (isNE(covrIO.getCpiDate(),varcom.vrcmMaxDate)) {
			covrIO.setCpiDate(wsaaEarliestRerateDate.toInt());
		}
		compute(wsaaPremDiff, 3).setRounded(add(wsaaPremDiff,(mult(covrIO.getInstprem(),-1))));
		covrIO.setRiskprem(premiumrec.riskPrem.getbigdata()); //ILIFE-7845 
		if(prmhldtrad && "Y".equals(covrIO.getReinstated()))//ILIFE-8509
			covrIO.setReinstated(SPACE);
		if(insertCovrList == null){
			insertCovrList = new ArrayList<>();
		}
		insertCovrList.add(covrIO);
		ct04Value++;
	}

protected void checkLexts3300(Covrpf covrIO, Covrpf covrrnlIO)
	{
		/* The next re-rate date will be the earliest rerate date*/
		/* WSAA-EARLIEST-RERATE-DATE among the related components if*/
		/* it is earlier than the new rerate date and CPI date of the*/
		/* WOP components.*/
		wsaaRateFrom.set(wsaaEarliestRerateDate);
		/*  The next re-rate date held in WSAA-RATE-FROM. Now check to*/
		/*  see if any LEXT records expire before this is reached.*/
		/*  If so, set the next re-rate date to that cessation date.*/
		/*  Check also that this LEXT record has a cessation date past*/
		/*  the current re-rate date, since it could be the one which*/
		if (lextbrrMap != null && lextbrrMap.containsKey(covrIO.getChdrnum())) {
			for (Lextpf l : lextbrrMap.get(covrIO.getChdrnum())) {
				if (l.getChdrcoy().equals(covrIO.getChdrcoy()) && l.getLife().equals(covrIO.getLife())
						&& l.getCoverage().equals(covrIO.getCoverage()) && l.getRider().equals(covrIO.getRider())) {
					if (isLT(l.getExtCessDate(), wsaaRateFrom) && isGT(l.getExtCessDate(), covrIO.getRerateDate())) {
						wsaaRateFrom.set(l.getExtCessDate());
					}
				}
			}
		}
	
		/*  Now know the next re-rate date.*/
		/*  Default the 'rates to use' date to be the next re-rate*/
		/*  date and then check against the minimum guaranteed rates*/
		/*  period from T5687. If the re-rate date is still within*/
		/*  this then use the risk commencement date.*/
		covrIO.setRerateDate(wsaaRateFrom.toInt());
		covrIO.setRerateFromDate(covrIO.getRerateDate());
		checkMgp3360(covrIO);
		/* Update COVR with new details.*/
		/* Adjust calculated premiums for plan polices.*/
		/* If no premium method was present on T5687, skip the premium*/
		/* update.*/
		
		if (isNE(t5687rec.premmeth,SPACES)) {
			if(!stampDutyflag)
				compute(wsaaInstPrem, 3).setRounded(div(premiumrec.calcPrem,chdrlifIO.getPolinc()));
			else {
				compute(wsaaInstPrem, 3).setRounded(div(add(premiumrec.calcPrem, premiumrec.zstpduty01),chdrlifIO.getPolinc()));
				covrIO.setZstpduty01(premiumrec.zstpduty01.getbigdata());
				covrIO.setZclstate(covrIO.getZclstate());
			}
			compute(wsaaZbinstprem, 3).setRounded(div(premiumrec.calcBasPrem,chdrlifIO.getPolinc()));
			compute(wsaaZlinstprem, 3).setRounded(div(premiumrec.calcLoaPrem,chdrlifIO.getPolinc()));
			if (isEQ(covrIO.getPlanSuffix(),0)
			&& isNE(chdrlifIO.getPolinc(),1)) {
				compute(wsaaInstPrem, 3).setRounded(mult(wsaaInstPrem,chdrlifIO.getPolsum()));
				compute(wsaaZbinstprem, 3).setRounded(mult(wsaaZbinstprem,chdrlifIO.getPolsum()));    
				compute(wsaaZlinstprem, 3).setRounded(mult(wsaaZlinstprem,chdrlifIO.getPolsum()));
			}
			if (isNE(wsaaInstPrem, 0)) {
				zrdecplrec.amountIn.set(wsaaInstPrem);
				callRounding8000();
				wsaaInstPrem.set(zrdecplrec.amountOut);
			}
			if (isNE(wsaaZbinstprem, 0)) {
				zrdecplrec.amountIn.set(wsaaZbinstprem);
				callRounding8000();
				wsaaZbinstprem.set(zrdecplrec.amountOut);
			}
			if (isNE(wsaaZlinstprem, 0)) {
				zrdecplrec.amountIn.set(wsaaZlinstprem);
				callRounding8000();
				wsaaZlinstprem.set(zrdecplrec.amountOut);
			}
			compute(wsaaPremDiff, 3).setRounded(add(wsaaPremDiff,(sub(wsaaInstPrem,covrIO.getInstprem()))));
			covrIO.setInstprem(wsaaInstPrem.getbigdata());
			covrIO.setZbinstprem(wsaaZbinstprem.getbigdata());
			covrIO.setZlinstprem(wsaaZlinstprem.getbigdata());
		}
		
		/* If the component has an historical INCR record, a new*/
		/* INCR record must be written with the rerated premium.*/
		checkForIncrease3800(covrIO, covrrnlIO);
		/* Ensure that the re-rate dates do not exceed the premium*/
		/* cessation date for the component.*/
		if (isGT(covrIO.getRerateDate(),covrIO.getPremCessDate())) {
			covrIO.setRerateDate(covrIO.getPremCessDate());
		}
		if (isGT(covrIO.getRerateFromDate(),covrIO.getPremCessDate())) {
			covrIO.setRerateFromDate(covrIO.getPremCessDate());
		}
		if (isNE(covrIO.getCpiDate(),varcom.vrcmMaxDate)) {
			covrIO.setCpiDate(covrIO.getRerateDate());
		}
		covrIO.setValidflag("1");
		compute(wsaaNewTranno, 0).set(add(chdrlifIO.getTranno(),1));
		covrIO.setTranno(wsaaNewTranno.toInt());
		covrIO.setCurrfrom(wsaaRerateStore.toInt());
		covrIO.setCurrto(varcom.vrcmMaxDate.toInt());
		
		if(insertCovrList == null){
			insertCovrList = new ArrayList<>();
		}
		
		covrIO.setRiskprem(premiumrec.riskPrem.getbigdata()); //ILIFE-7845 
		insertCovrList.add(covrIO);
		
		if(isFeatureConfig){
			checkRerateOnExistingWaiverClaim(covrIO);
		}
		ct02Value++;
	}

/**
* <pre>
****  If the new re-rate date is still within the Minimum
****  Guaranteed rates period then use the Component Risk
****  Commencement date as the 'rates to use' date.
* </pre>
*/
protected void checkMgp3360(Covrpf covrIO)
	{
		if (isEQ(t5687rec.premGuarPeriod,0)) {
			covrIO.setRerateFromDate(covrIO.getRerateDate());
		}
		else {    
			datcon2rec.intDate1.set(covrIO.getCrrcd());
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(t5687rec.premGuarPeriod);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				fatalError600();
			}
			/*  Check whether the re-rate date is within the minimum*/
			/*  guarantee period. If it is then use the rates from the*/
			/*  Contract Commencement date. If not use the rates from the*/
			/*  re-rate date.*/
			if (isLT(datcon2rec.intDate2,covrIO.getRerateDate())) {
				covrIO.setRerateFromDate(covrIO.getRerateDate());
			}
			else {
				covrIO.setRerateFromDate(covrIO.getCrrcd());
			}
		}
	}



/**
* <pre>
****  Determine whether this re-rate was triggered by the actual
****  re-rate date being reached (re-rate date falls on a re-rate
****  frequency boundary) - a TRUE re-rate, or whether this
****  re-rate was originally triggered by the expiration of a
****  LEXT record - LEXT re-rate. Default is to a TRUE Re-rate.
****  Set the LAST TRUE re-rate date accordingly. If this is a
****  TRUE re-rate then this is the last true re-rate date.
****  If this is a LEXT re-rate then calculate the last re-rate
****  date by adding the number of whole years in the difference
****  calculated above to the Component Risk Commencement Date.
* </pre>
*/
protected void setRerateType3410(Covrpf covrrnlIO)
	{
		/* Calculate the difference between the re-rate date and the*/
		/* contract commencement date in years.*/
		wsaaDurationInt.set(ZERO);
		if (isNE(t5687rec.rtrnwfreq,NUMERIC)
		|| isEQ(t5687rec.rtrnwfreq,0)) {
			wsaaRerateType.set("2");
		}
		else {
			datcon3rec.function.set(SPACES);
			datcon3rec.intDate1.set(covrrnlIO.getCrrcd());
			/*        MOVE COVRRNL-RERATE-DATE    TO DTC3-INT-DATE-2*/
			datcon3rec.intDate2.set(wsaaEarliestRerateDate);
			datcon3rec.frequency.set("01");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				syserrrec.statuz.set(datcon3rec.statuz);
				syserrrec.params.set(datcon3rec.datcon3Rec);
				fatalError600();
			}
			/*  Divide the result by the re-rate frequency from T5687.*/
			/*  If the answer is a whole number then this is a TRUE re-rate*/
			/*  else this is a LEXT re-rate.*/
			/*  A LEXT re-rate can no longer be assumed as such and a check*/
			/*  for a LEXT with a cease date equal to the last re-rate date*/
			/*  must be made.*/
			compute(wsaaDurationInt, 5).setDivide(datcon3rec.freqFactor, (t5687rec.rtrnwfreq));
			wsaaDurationRem.setRemainder(wsaaDurationInt);
			if (isNE(wsaaDurationRem,0)) {
				checkLextRerate3420(covrrnlIO);
			}
		}
		/*  For a LEXT rerate, get the whole number of years within*/
		/*  the difference in the dates calculated above by multiplying*/
		/*  the re-rate frequency from T5687 by the integer result of*/
		/*  the division above.*/
		/*  Add this number of years to the Risk Commencement date of*/
		/*  this component using DATCON2.*/
		if (lextRerate.isTrue()) {
			compute(datcon2rec.freqFactor, 0).set((mult(t5687rec.rtrnwfreq,wsaaDurationInt)));
			datcon2rec.intDate1.set(wsaaEarliestRerateDate);
			datcon2rec.frequency.set("01");
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.statuz.set(datcon2rec.statuz);
				syserrrec.params.set(datcon2rec.datcon2Rec);
				fatalError600();
			}
			wsaaLastRrtDate.set(datcon2rec.intDate2);
		}
		else {
			wsaaLastRrtDate.set(wsaaEarliestRerateDate);
		}
	}

protected void checkLextRerate3420(Covrpf covrrnlIO)
	{
		/* Check for a LEXT for this component with a cessation date*/
		/* equal to the last re-rate date.  If one exists, this is a*/
		/* LEXT re-rate.*/
		if (lextbrrMap != null && lextbrrMap.containsKey(covrrnlIO.getChdrnum())) {
			for (Lextpf l : lextbrrMap.get(covrrnlIO.getChdrnum())) {
				if (l.getChdrcoy().equals(covrrnlIO.getChdrcoy()) && l.getLife().equals(covrrnlIO.getLife())
						&& l.getCoverage().equals(covrrnlIO.getCoverage()) && l.getRider().equals(covrrnlIO.getRider())
						&& l.getExtCessDate() == wsaaEarliestRerateDate.toInt()) {
					wsaaRerateType.set("2");
					break;
				}
			}
		}
	}


protected void commit3500()
	{
		commitControlTotals();
		if(updateCovrrnlList!=null){
			covrpfDAO.updateVarSumInsuredRecord(updateCovrrnlList);
			updateCovrrnlList.clear();
		}
		if(insertCovrList!=null){
			covrpfDAO.insertCovrBulk(insertCovrList);
			insertCovrList.clear();
		}
		if(insertIncrpfList!=null){
			incrpfDAO.insertIncrList(insertIncrpfList);
			insertIncrpfList.clear();
		}
		if(insertPtrnpfList!=null){
			ptrnpfDAO.insertPtrnPF(insertPtrnpfList);
			insertPtrnpfList.clear();
		}
		if(updatePayrpfList!=null){
			payrpfDAO.updatePayrRecord(updatePayrpfList);
			updatePayrpfList.clear();
		}
		if(insertPayrpfList!=null){
			payrpfDAO.insertPayrpfList(insertPayrpfList);
			insertPayrpfList.clear();
		}
		if(insertAgcmbchList!=null){
			agcmpfDAO.insertAgcmpfList(insertAgcmbchList);
			insertAgcmbchList.clear();
		}
		
		if(updateChdrlifList!=null){
			chdrpfDAO.updateInvalidChdrRecord(updateChdrlifList);
			updateChdrlifList.clear();
		}
		if(insertChdrlifList!=null){
			chdrpfDAO.insertChdrpfMatureList(insertChdrlifList, 0);
			insertChdrlifList.clear();
		}		
		if(updateAgcmpfList!=null){
			agcmpfDAO.updateInvalidRecord(updateAgcmpfList);
			updateAgcmpfList.clear();
		}
	}

private void commitControlTotals(){
	contotrec.totno.set(ct01);
	contotrec.totval.set(ct01Value);
	callProgram(Contot.class, contotrec.contotRec);		
	ct01Value = 0;

	contotrec.totno.set(ct02);
	contotrec.totval.set(ct02Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct02Value = 0;
	
	contotrec.totno.set(ct03);
	contotrec.totval.set(ct03Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct03Value = BigDecimal.ZERO;
	
	contotrec.totno.set(ct04);
	contotrec.totval.set(ct04Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct04Value = 0;
	
	contotrec.totno.set(ct05);
	contotrec.totval.set(ct05Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct05Value = 0;
	
	contotrec.totno.set(ct06);
	contotrec.totval.set(ct06Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct05Value = 0;
	
	contotrec.totno.set(ct07);
	contotrec.totval.set(ct07Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct05Value = 0;
	
	contotrec.totno.set(ct08);
	contotrec.totval.set(ct08Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct05Value = 0;
}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}


/**
* <pre>
****  When the coverage/rider has expired, it is necessary to
****  update the corresponding AGCMBCH Records. Loop round all the
****  AGCMBCH records for this component. For each, set the
****  record to validflag 2 and write a new validflag 1 record
****  with the new annualised premium.
* </pre>
*/
protected void updateAgcm3700(Covrpf covrrnlIO, Covrpf covrIO)
 {
		boolean skipFlag = false;
		if (agcmbchMap != null && agcmbchMap.containsKey(covrrnlIO.getChdrnum())) {
			for (Agcmpf a : agcmbchMap.get(covrrnlIO.getChdrnum())) {
				if (a.getChdrcoy().equals(covrrnlIO.getChdrcoy()) && a.getLife().equals(covrrnlIO.getLife())
						&& a.getRider().equals(covrrnlIO.getRider()) && a.getCoverage().equals(covrrnlIO.getCoverage())
						&& a.getPlnsfx() == covrrnlIO.getPlanSuffix()) {
					if (!skipFlag) {
						agcmUpdateLoop3710(a, covrrnlIO, covrIO);
					}
					if (!(((isNE(a.getTranno(), wsaaNewTranno)) && (isNE(a.getDormflag(), "Y"))))) {
						skipFlag = true;
					} else {
						skipFlag = false;
					}
				}
			}
		}
	}


protected void agcmUpdateLoop3710(Agcmpf agcmbchIO,Covrpf covrrnlIO, Covrpf covrIO)
	{
		/*  Process only records where the dormant flag not set to*/
		/*  'Y'. For these, set the valid flag to '2'.*/
	/*  Read the next AGCMBCH record. Read through the records to*/
	/*  find one with the previous tranno. This will avoid*/
	/*  picking up the record just written.*/
		if (isNE(agcmbchIO.getDormflag(),"Y")) {
			agcmbchIO.setCurrto(wsaaRerateStore.toInt());
			agcmbchIO.setValidflag("2");
			if(updateAgcmpfList==null){
				updateAgcmpfList = new ArrayList<>();
			}
			updateAgcmpfList.add(agcmbchIO);
			writeNewAgcm3720(agcmbchIO, covrrnlIO, covrIO);
		}
	}


protected void writeNewAgcm3720(Agcmpf oldagcmbchIO,Covrpf covrrnlIO, Covrpf covrIO)
	{
		Agcmpf agcmbchIO = new Agcmpf(oldagcmbchIO);
		agcmbchIO.setTranno(wsaaNewTranno.toInt());
		agcmbchIO.setCurrfrom(wsaaRerateStore.toInt());
		agcmbchIO.setCurrto(varcom.vrcmMaxDate.toInt());
		agcmbchIO.setValidflag("1");
		/*  If this component has ceased then the premium for this*/
		/*  is set to zero. Otherwise get the new annual premium for*/
		/*  this agent.*/
		if (isEQ(covrrnlIO.getPremCessDate(),covrrnlIO.getRerateDate())) {
			/*CONTINUE_STMT*/
		}
		else {
			agentAnnprem3730(agcmbchIO, covrrnlIO, covrIO);
		}
		if(insertAgcmbchList == null){
			insertAgcmbchList = new ArrayList<>();
		}
		insertAgcmbchList.add(agcmbchIO);
	}

/**
* <pre>
****  Calculate the revised annual premium for this Agent
****  Commission record. Get the Commission split (PCDD) for
****  this agent and apply it to the anualised, rerated premium.
* </pre>
*/
protected void agentAnnprem3730(Agcmpf agcmbchIO, Covrpf covrrnlIO, Covrpf covrIO)
 {
		wsaaAgntnum.set(agcmbchIO.getAgntnum());
		/* Need to find the 'BASIC' agent for an Override case. */
		if (isEQ(agcmbchIO.getOvrdcat(), "O")) {
			wsaaAgntFound.set("N");
			wsaaCedagent.set(agcmbchIO.getCedagent());
			while (!(agentFound.isTrue())) {
				findBasicAgent3740(agcmbchIO);
			}

		}
		Pcddpf pcddchgIO = null;
		if (pcddchgMap != null && pcddchgMap.containsKey(covrrnlIO.getChdrnum())) {
			for (Pcddpf p : pcddchgMap.get(covrrnlIO.getChdrnum())) {
				if (p.getChdrcoy().toString().equals(covrrnlIO.getChdrcoy()) && p.getAgntNum().equals(wsaaAgntnum.toString())) {
					pcddchgIO = p;
					break;
				}
			}
		}
		if (pcddchgIO == null) {
			syserrrec.params.set(covrrnlIO.getChdrnum());
			fatalError600();
		} else {//IJTI-320 START
		/* Compute the new annualised premium for this agent */
		/* commission record. */
		if (isNE(t5687rec.zrrcombas, SPACES)) {
			agcmbchIO.setAnnprem(mult((mult(covrIO.getZbinstprem(), wsaaBillfreqn)), (div(pcddchgIO.getSplitB(), 100)))
					.getbigdata());
		} else {
			agcmbchIO.setAnnprem(mult((mult(covrIO.getInstprem(), wsaaBillfreqn)), (div(pcddchgIO.getSplitB(), 100)))
					.getbigdata());
		}
		/* MOVE AGCMBCH-ANNPREM TO ZRDP-AMOUNT-IN. */
		/* PERFORM 8000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO AGCMBCH-ANNPREM. */
		if (isNE(agcmbchIO.getAnnprem(), 0)) {
			zrdecplrec.amountIn.set(agcmbchIO.getAnnprem());
			callRounding8000();
			agcmbchIO.setAnnprem(zrdecplrec.amountOut.getbigdata());
		}
		}
		//IJTI-320 END
	}

/**
* <pre>
****  This is an override agent. Loop through the AGCM records
****  on this contract to find the 'BASIC' commission agent.
* </pre>
*/
protected void findBasicAgent3740(Agcmpf agcmbchIO)
 {
		/* Read the Agent Commission record using the Ceding Agent */
		/* for this record. */
		Agcmpf agcmIO = null;
		if (agcmMap != null && agcmMap.containsKey(agcmbchIO.getChdrnum())) {
			for (Agcmpf a : agcmMap.get(agcmbchIO.getChdrnum())) {
				if (a.getChdrcoy().equals(agcmbchIO.getChdrcoy()) && a.getLife().equals(agcmbchIO.getLife())
						&& a.getCoverage().equals(agcmbchIO.getCoverage()) && a.getRider().equals(agcmbchIO.getRider())
						&& a.getPlnsfx() == agcmbchIO.getPlnsfx() && a.getAgntnum().equals(wsaaCedagent.toString())) {
					agcmIO = a;
					break;
				}
			}
		}
		if (agcmIO == null) {
			syserrrec.params.set(agcmbchIO.getChdrnum());
			fatalError600();
		} else {
		if (isEQ(agcmIO.getOvrdcat(), "B")) {
			wsaaAgntFound.set("Y");
			wsaaAgntnum.set(agcmIO.getAgntnum());
		} else {
			wsaaCedagent.set(agcmIO.getCedagent());
		}
		}
	}

protected void checkForIncrease3800(Covrpf covrIO, Covrpf covrrnlIO)
 {
		/* Check for an historical Increase record for this component */
		/* which has not been refused or reversed. If one exists, */
		/* write a new INCR record based on the old record. */
		Incrpf incrhstIO = null;
		if (incrhstMap != null && incrhstMap.containsKey(covrIO.getChdrnum())) {
			for (Incrpf i : incrhstMap.get(covrIO.getChdrnum())) {
				if (i.getChdrcoy().equals(covrIO.getChdrcoy()) && i.getLife().equals(covrIO.getLife())
						&& i.getCoverage().equals(covrIO.getCoverage()) && i.getRider().equals(covrIO.getRider())
						&& i.getPlnsfx() == covrIO.getPlanSuffix()) {
					if(isFoundPro){
                		if(isEQ(wsaaProCpiDate,i.getCrrcd())){
                			incrhstIO = i;
                			break;
                		}
                	}
                	else{
					incrhstIO = i;
					break;
                	}
				}
			}
		}

		if (incrhstIO == null) {
			return;
		}
		/* Write a new historical INCR record based on the details of */
		/* the last record. The main changes from the previous record */
		/* are that the LASTINST comes from the NEWINST on the previous */
		/* record and that the increase in premium caused by the */
		/* rerate is added to the NEWINST to give the 'new' NEWINST. */
		/* The TRANNO and date/time fields are also updated. */
		incrhstIO.setLastInst(incrhstIO.getNewinst());
		incrhstIO.setZblastinst(incrhstIO.getZbnewinst());
		incrhstIO.setZllastinst(incrhstIO.getZlnewinst());
		setPrecision(incrhstIO.getNewinst(), 2);
		incrhstIO.setNewinst(incrhstIO.getNewinst().add(covrIO.getInstprem().subtract(covrrnlIO.getInstprem())));
		setPrecision(incrhstIO.getZbnewinst(), 2);
		incrhstIO
				.setZbnewinst(incrhstIO.getZbnewinst().add(covrIO.getZbinstprem().subtract(covrrnlIO.getZbinstprem())));
		setPrecision(incrhstIO.getZlnewinst(), 2);
		incrhstIO
				.setZlnewinst(incrhstIO.getZlnewinst().add(covrIO.getZlinstprem().subtract(covrrnlIO.getZlinstprem())));
		setPrecision(incrhstIO.getTranno(), 0);
		incrhstIO.setTranno(chdrlifIO.getTranno() + 1);
		incrhstIO.setStatcode(covrIO.getStatcode());
		incrhstIO.setPstatcode(covrIO.getPstatcode());
		if(isFoundPro){
			incrhstIO.setCrrcd(wsaaProCpiDate.toInt());
		}
		else{
		incrhstIO.setCrrcd(bsscIO.getEffectiveDate().toInt());
		}
		incrhstIO.setTransactionDate(wsaaTransactionDate);
		incrhstIO.setTransactionTime(wsaaTransactionTime);
		incrhstIO.setTermid(wsaaTermid);
		incrhstIO.setUser(wsaaUser);
		setPrecision(incrhstIO.getZstpduty01(), 2);
		if(covrIO.getZstpduty01() != null){//ILIFE-8299
			incrhstIO.setZstpduty01(add(incrhstIO.getNewinst(),(sub(covrIO.getZstpduty01(),covrIO.getZstpduty01()))).getbigdata());
		}else{
			incrhstIO.setZstpduty01(incrhstIO.getNewinst());
		}
		if (insertIncrpfList == null) {
			insertIncrpfList = new ArrayList();
		}
		insertIncrpfList.add(incrhstIO);

	}


protected void genericProcessing3900(Covrpf covrrnlIO, Covrpf covrIO)
	{
		wsaaT5671Item.set(SPACES);
		wsaaItemTranCode.set(batcdorrec.trcde);
		wsaaItemCrtable.set(covrIO.getCrtable());
		wsaaT5671Ix = 1;
		 searchlabel1:
		{
			for (; isLT(wsaaT5671Ix,wsaaT5671Rec.length); wsaaT5671Ix++){
				if (isEQ(wsaaT5671Key[wsaaT5671Ix],wsaaT5671Item)) {
					t5671rec.subprogs.set(wsaaT5671Subprogs[wsaaT5671Ix]);
					break searchlabel1;
				}
			}
			t5671rec.subprogs.set(SPACES);
		}
		isuallrec.isuallRec.set(SPACES);
		isuallrec.function.set("ACTIN");
		isuallrec.company.set(covrIO.getChdrcoy());
		isuallrec.chdrnum.set(covrIO.getChdrnum());
		isuallrec.life.set(covrIO.getLife());
		isuallrec.coverage.set(covrIO.getCoverage());
		isuallrec.rider.set(covrIO.getRider());
		isuallrec.planSuffix.set(covrIO.getPlanSuffix());
		isuallrec.oldcovr.set(covrrnlIO.getCoverage());
		isuallrec.oldrider.set(covrrnlIO.getRider());
		if (isEQ(wsaaFullyPaid,"Y")) {
			isuallrec.freqFactor.set(0);
		}
		else {
			isuallrec.freqFactor.set(1);
		}
		isuallrec.batchkey.set(batcdorrec.batchkey);
		isuallrec.transactionDate.set(wsaaTransactionDate);
		isuallrec.transactionTime.set(wsaaTransactionTime);
		isuallrec.user.set(wsaaUser);
		isuallrec.termid.set(wsaaTermid);
		isuallrec.effdate.set(covrrnlIO.getCrrcd());
		isuallrec.newTranno.set(covrIO.getTranno());
		isuallrec.covrSingp.set(ZERO);
		isuallrec.covrInstprem.set(covrIO.getInstprem());
		isuallrec.language.set(bsscIO.getLanguage());
		if (isGT(chdrlifIO.getPolinc(),1)) {
			isuallrec.convertUnlt.set("Y");
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub,4)); wsaaSub.add(1)){
			subroutineCall3950();
		}

	}

protected void subroutineCall3950()
	{
		/*CALL*/
		isuallrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.subprog[wsaaSub.toInt()],SPACES)) {
			callProgram(t5671rec.subprog[wsaaSub.toInt()], isuallrec.isuallRec);
		}
		if (isNE(isuallrec.statuz,varcom.oK)) {
			syserrrec.params.set(isuallrec.isuallRec);
			syserrrec.statuz.set(isuallrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void updateChdrPtrn4100()
	{
		/* Write PTRN record.*/
		Ptrnpf ptrnIO = new Ptrnpf(); 
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy().toString());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTranno(wsaaNewTranno.toInt());
		ptrnIO.setPtrneff(bsscIO.getEffectiveDate().toInt());
		ptrnIO.setTrdt(wsaaTransactionDate);
		ptrnIO.setTrtm(wsaaTransactionTime);
		ptrnIO.setUserT(wsaaUser);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setBatcpfx(batcdorrec.prefix.toString());
		ptrnIO.setBatccoy(batcdorrec.company.toString());
		ptrnIO.setBatcbrn(batcdorrec.branch.toString());
		ptrnIO.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnIO.setBatctrcde(bprdIO.getAuthCode().toString());
		ptrnIO.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnIO.setBatcbatch(batcdorrec.batch.toString());
		ptrnIO.setDatesub(bsscIO.getEffectiveDate().toInt());
		ptrnIO.setValidflag("1"); // ILIFE-5114
		if(insertPtrnpfList == null){
			insertPtrnpfList = new ArrayList<>();
		}
		insertPtrnpfList.add(ptrnIO);
		
		a000LoadNonWopCovrStatus();
		a100ReadT5399();
		
		/* Valid flag 2 old Contract Header record.*/
		//ILIFE-5011 start by dpuhawan
		Chdrpf chdrUpdate = new Chdrpf(chdrlifIO);
		chdrUpdate.setValidflag('2');
		chdrUpdate.setCurrto(wsaaRerateStore.toInt());
		chdrUpdate.setUniqueNumber(chdrlifIO.getUniqueNumber());
		if(updateChdrlifList == null){
			updateChdrlifList = new ArrayList<>();
		}
		updateChdrlifList.add(chdrUpdate);
		//ILIFE-5011 end by dpuhawan
	
		/* Write a new CHDRLIF record with incremented tranno, new*/
		/* current from and current to dates, changed instalment amounts*/
		/* and an updated premium status code if all of the coverages*/
		/* attached to the contract header have expired.*/
		chdrlifIO.setValidflag('1');
		chdrlifIO.setCurrto(varcom.vrcmMaxDate.toInt());
		chdrlifIO.setCurrfrom(wsaaRerateStore.toInt());
		chdrlifIO.setTranno(wsaaNewTranno.toInt());
		chdrlifIO.setSinstamt01(add(chdrlifIO.getSinstamt01(),wsaaPremDiff).getbigdata());
		/*  IF WSAA-COVRS-LIVE           = 'N'                           */
		/*      PERFORM 4150-ALL-COVRS-EXPIRED                           */
		/*  END-IF.                                                      */
		/*  ADD WSAA-PREM-DIFF          TO CHDRLIF-SINSTAMT06.           */
		if ((setPrecision(0, 2)
		&& isEQ((add(wsaaPremDiff, chdrlifIO.getSinstamt01())), 0))) {
			chdrlifIO.setSinstamt02(BigDecimal.ZERO);
		}
		setPrecision(chdrlifIO.getSinstamt06(), 2);
		
		if(chdrlifIO.getSinstamt01()==null)		
			chdrlifIO.setSinstamt01(BigDecimal.ZERO);
		
		if(chdrlifIO.getSinstamt02()==null)
			chdrlifIO.setSinstamt02(BigDecimal.ZERO);
		
		if(chdrlifIO.getSinstamt03()==null)
			chdrlifIO.setSinstamt03(BigDecimal.ZERO);
		
		
		if(chdrlifIO.getSinstamt04()==null)
			chdrlifIO.setSinstamt04(BigDecimal.ZERO);
		
		
		if(chdrlifIO.getSinstamt05()==null)
			chdrlifIO.setSinstamt05(BigDecimal.ZERO);
		
		
		if(chdrlifIO.getSinstamt06()==null)
			chdrlifIO.setSinstamt06(BigDecimal.ZERO);
		
		
		
		chdrlifIO.setSinstamt06(chdrlifIO.getSinstamt01().add(chdrlifIO.getSinstamt02()).add(chdrlifIO.getSinstamt03()).add(chdrlifIO.getSinstamt04()));

		
		for (wsaaSub.set(1); !(isGT(wsaaSub, 32)); wsaaSub.add(1)){
			if (isEQ(t5399rec.covRiskStats, SPACES)) {
				wsaaSub.set(37);
			}
			else {
				for (ix.set(1); !(isGT(ix, 99)
				|| isEQ(wsaaCovrStatcode[ix.toInt()], SPACES)); ix.add(1)){
					if (isEQ(t5399rec.covRiskStat[wsaaSub.toInt()], wsaaCovrStatcode[ix.toInt()])) {
						chdrlifIO.setStatcode(t5399rec.setCnRiskStat[wsaaSub.toInt()].toString());
						ix.set(100);
						wsaaSub.set(37);
					}
				}
			}
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
			if (isEQ(t5399rec.covPremStats, SPACES)) {
				wsaaSub.set(37);
			}
			else {
				for (ix.set(1); !(isGT(ix, 99)
				|| isEQ(wsaaCovrPstatcode[ix.toInt()], SPACES)); ix.add(1)){
					if (isEQ(t5399rec.covPremStat[wsaaSub.toInt()], wsaaCovrPstatcode[ix.toInt()])) {
						chdrlifIO.setPstcde(t5399rec.setCnPremStat[wsaaSub.toInt()].toString());
						ix.set(100);
						wsaaSub.set(37);
					}
				}
			}
		}
		if(insertChdrlifList == null){
			insertChdrlifList = new ArrayList<>(); 
		}
		insertChdrlifList.add(chdrlifIO);
		ct03Value = ct03Value.add(wsaaPremDiff.getbigdata()); 
		ct08Value++;
	}

protected void allCovrsExpired4150()
	{
		/*START*/
		/* Since all of the coverages attached to the contract have*/
		/* expired, the contract fee (SINSTAMT02) is no longer payable.*/
		/* Also, the premium status on the contract header should be*/
		/* updated.*/
		compute(wsaaPremDiff, 2).set(sub(wsaaPremDiff,chdrlifIO.getSinstamt02()));
		chdrlifIO.setSinstamt02(BigDecimal.ZERO);
		if (isNE(t5679rec.setCnPremStat,SPACES)) {
			chdrlifIO.setPstcde(t5679rec.setCnPremStat.toString());
		}
		/*EXIT*/
	}

protected void updatePayr4200()
	{
		/* Rewrite a Validflag '2' PAYR record*/
		Payrpf payrIO = null;
		if(payrMap!=null&&payrMap.containsKey(chdrlifIO.getChdrnum())){
			for(Payrpf p:payrMap.get(chdrlifIO.getChdrnum())){
				if(p.getChdrcoy().equals(chdrlifIO.getChdrcoy().toString())&&p.getPayrseqno() == 1){
					payrIO = p;
					break;
				}
			}
		}
		if (payrIO == null) {
			syserrrec.params.set(chdrlifIO.getChdrnum());
			fatalError600();
		} else {//IJTI-320 START
		payrIO.setValidflag("2");
		if(updatePayrpfList == null){
			updatePayrpfList = new ArrayList<>();
		}
		updatePayrpfList.add(new Payrpf(payrIO));
		
		/* Write a new PAYR record with new dates, an incremented*/
		/* transaction number and new amounts.*/
		payrIO.setValidflag("1");
		payrIO.setEffdate(wsaaRerateStore.toInt());
		payrIO.setTranno(wsaaNewTranno.toInt());
		/* Re-rate amount - Calculate using premium difference.*/
		/* If all of the coverages linked to the contract have expired,*/
		/* the contract fee will have been removed from the contract.*/
		/* In this case, both SINSTAMT01 and SINSTAMT02 should equal*/
		/* zero.*/
		/*   COMPUTE PAYR-SINSTAMT06      = PAYR-SINSTAMT06               */
		/*                                  + WSAA-PREM-DIFF.             */
		/*   IF WSAA-COVRS-LIVE           = 'N'                           */
		/*      MOVE ZERO                 TO PAYR-SINSTAMT02              */
		/*                                   PAYR-SINSTAMT01              */
		/*   ELSE                                                         */
		/*       COMPUTE PAYR-SINSTAMT01  = PAYR-SINSTAMT01               */
		/*                                + WSAA-PREM-DIFF                */
		/*   END-IF.                                                      */
		payrIO.setSinstamt01(chdrlifIO.getSinstamt01());
		payrIO.setSinstamt02(chdrlifIO.getSinstamt02());
		payrIO.setSinstamt03(chdrlifIO.getSinstamt03());
		payrIO.setSinstamt04(chdrlifIO.getSinstamt04());
		payrIO.setSinstamt05(chdrlifIO.getSinstamt05());
		payrIO.setSinstamt06(chdrlifIO.getSinstamt06());
		payrIO.setPstatcode(chdrlifIO.getPstcde());
		datcon2rec.intDate1.set(chdrlifIO.getBillcd());
		datcon2rec.frequency.set("DY");
		compute(datcon2rec.freqFactor, 0).set(mult(wsaaLeadDays,-1));
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError600();
		}
		payrIO.setNextdate(datcon2rec.intDate2.toInt());
		if(insertPayrpfList == null){
			insertPayrpfList = new ArrayList<>();
		}
		insertPayrpfList.add(payrIO);
		}
		//IJTI-320 END
	}

protected void updateUndr4500(Covrpf covrrnlIO)
	{
		/* Remove the existing UNDR records*/
		initialize(crtundwrec.parmRec);
		Lifepf lifeenqIO = a300ReadLife(covrrnlIO);
		crtundwrec.clntnum.set(lifeenqIO.getLifcnum());
		crtundwrec.coy.set(covrrnlIO.getChdrcoy());
		crtundwrec.currcode.set(chdrlifIO.getCntcurr());
		crtundwrec.chdrnum.set(covrrnlIO.getChdrnum());
		crtundwrec.crtable.set(covrrnlIO.getCrtable());
		crtundwrec.cnttyp.set(chdrlifIO.getCnttype());
		crtundwrec.function.set("DEL");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status,varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			fatalError600();
		}
		/* Create the new UNDR records*/
		initialize(crtundwrec.parmRec);
		crtundwrec.clntnum.set(lifeenqIO.getLifcnum());
		crtundwrec.coy.set(covrrnlIO.getChdrcoy());
		crtundwrec.chdrnum.set(covrrnlIO.getChdrnum());
		crtundwrec.life.set(covrrnlIO.getLife());
		crtundwrec.crtable.set(covrrnlIO.getCrtable());
		crtundwrec.batctrcde.set(bprdIO.getAuthCode());
		crtundwrec.sumins.set(wsaaCovrSumins);
		crtundwrec.cnttyp.set(chdrlifIO.getCnttype());
		crtundwrec.currcode.set(chdrlifIO.getCntcurr());
		crtundwrec.function.set("ADD");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status,varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			fatalError600();
		}

	}


protected void statistics5000()
	{
		lifsttrrec.batccoy.set(batcdorrec.company);
		lifsttrrec.batcbrn.set(batcdorrec.branch);
		lifsttrrec.batcactyr.set(batcdorrec.actyear);
		lifsttrrec.batcactmn.set(batcdorrec.actmonth);
		lifsttrrec.batctrcde.set(bprdIO.getAuthCode());
		lifsttrrec.batcbatch.set(batcdorrec.batch);
		lifsttrrec.chdrcoy.set(chdrlifIO.getChdrcoy().toString());
		lifsttrrec.chdrnum.set(chdrlifIO.getChdrnum());
		lifsttrrec.tranno.set(wsaaNewTranno);
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			fatalError600();
		}
	}


protected Lifepf a300ReadLife(Covrpf covrrnlIO)
 {
		Lifepf lifeenqIO = null;
		if (lifeenqMap != null && lifeenqMap.containsKey(chdrlifIO.getChdrnum())) {
			for (Lifepf l : lifeenqMap.get(chdrlifIO.getChdrnum())) {
				if (l.getChdrcoy().equals(chdrlifIO.getChdrcoy().toString()) && l.getLife().equals(covrrnlIO.getLife())
						&& l.getJlife().equals("00")) {
					lifeenqIO = l;
					break;
				}
			}
		}
		if (lifeenqIO == null) {
			syserrrec.params.set(chdrlifIO.getChdrnum());
			fatalError600();
		}
		return lifeenqIO;
	}


protected void a200CalcBenefitAmount(Covrpf covrlnbIO)
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					a200Ctrl();
				case a210ByPass:
					a210ByPass(covrlnbIO);
				case a220ByPass:
					a220ByPass();
				case a200Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a200Ctrl()
	{
		/* To read T5687 2 times here, one for the main coverage the other*/
		/* read is to det it back to the current rider. Same with T5675.*/
		/*  This is the first read using the main component*/
		wsaaT5687Ix = 1;
		 searchlabel1:
		{
			for (; isLT(wsaaT5687Ix, wsaaT5687ArrayInner.wsaaT5687Rec.length); wsaaT5687Ix++){
				if (isEQ(wsaaT5687ArrayInner.wsaaT5687Key[wsaaT5687Ix], wsaaMainCrtable)) {
					break searchlabel1;
				}
			}
			initialize(t5687rec.t5687Rec);
			goTo(GotoLabel.a210ByPass);
		}
		wsaaDateFound.set("N");
		while ( !(isNE(wsaaMainCrtable, wsaaT5687ArrayInner.wsaaT5687Key[wsaaT5687Ix])
		|| isGT(wsaaT5687Ix,wsaaT5687Size)
		|| dateFound.isTrue())) {
			if (isGTE(chdrlifIO.getOccdate(), wsaaT5687ArrayInner.wsaaT5687Itmfrm[wsaaT5687Ix])) {
				wsaaDateFound.set("Y");
				moveT56877000();
			}
			else {
				wsaaT5687Ix++;
			}
		}

		if (!dateFound.isTrue()) {
			initialize(t5687rec.t5687Rec);
		}
	}

protected void a210ByPass(Covrpf covrlnbIO)
	{
		/*  Read table T5675*/
		wsaaT5675Ix = 1;
		 searchlabel1:
		{
			for (; isLT(wsaaT5675Ix,wsaaT5675Rec.length); wsaaT5675Ix++){
				if (isEQ(wsaaT5675Key[wsaaT5675Ix],t5687rec.premmeth)) {
					t5675rec.premsubr.set(wsaaT5675Premsubr[wsaaT5675Ix]);
					//ILIFE-3472 Code Promotion for VPMS externalization of LIFE TRX product calculations to LIFE Target2 Environment
					wsaaPremMeth.set(wsaaT5675Key[wsaaT5675Ix]);
					break searchlabel1;
				}
			}
			premReqd.setTrue();
			t5675rec.premsubr.set(SPACES);
			goTo(GotoLabel.a200Exit);
		}
		if (isEQ(t5675rec.premsubr,SPACES)) {
			wsaaPremStatuz.set("Y");
		}
		else {
			wsaaPremStatuz.set("N");
		}
		a300CallPremiumCalc();
		/*  This is the second call using the main component*/
		wsaaT5687Ix = 1;
		 searchlabel2:
		{
			for (; isLT(wsaaT5687Ix, wsaaT5687ArrayInner.wsaaT5687Rec.length); wsaaT5687Ix++){
				if (isEQ(wsaaT5687ArrayInner.wsaaT5687Key[wsaaT5687Ix], covrlnbIO.getCrtable())) {
					break searchlabel2;
				}
			}
			initialize(t5687rec.t5687Rec);
			goTo(GotoLabel.a220ByPass);
		}
		wsaaDateFound.set("N");
		while ( !(isNE(covrlnbIO.getCrtable(), wsaaT5687ArrayInner.wsaaT5687Key[wsaaT5687Ix])
		|| isGT(wsaaT5687Ix,wsaaT5687Size)
		|| dateFound.isTrue())) {
			if (isGTE(chdrlifIO.getOccdate(), wsaaT5687ArrayInner.wsaaT5687Itmfrm[wsaaT5687Ix])) {
				wsaaDateFound.set("Y");
				moveT56877000();
			}
			else {
				wsaaT5687Ix++;
			}
		}

		if (!dateFound.isTrue()) {
			initialize(t5687rec.t5687Rec);
		}
	}

protected void a220ByPass()
	{
		/*  Read table T5675  again*/
		wsaaT5675Ix = 1;
		 searchlabel1:
		{
			for (; isLT(wsaaT5675Ix,wsaaT5675Rec.length); wsaaT5675Ix++){
				if (isEQ(wsaaT5675Key[wsaaT5675Ix],t5687rec.premmeth)) {
					t5675rec.premsubr.set(wsaaT5675Premsubr[wsaaT5675Ix]);
					//ILIFE-3472 Code Promotion for VPMS externalization of LIFE TRX product calculations to LIFE Target2 Environment
					wsaaPremMeth.set(wsaaT5675Key[wsaaT5675Ix]);
					break searchlabel1;
				}
			}
			premReqd.setTrue();
			t5675rec.premsubr.set(SPACES);
			return ;
		}
	}

protected void a300CallPremiumCalc()
	{
		/*  Read Main Life file*/
		Lifepf  lifelnbIO = null;
		if(lifelnbMap!=null&&lifelnbMap.containsKey(chdrlifIO.getChdrnum())){
			for(Lifepf l:lifelnbMap.get(chdrlifIO.getChdrnum())){
				if(l.getChdrcoy().equals(bsprIO.getCompany().toString())&&l.getLife().equals(wsaaMainLife.toString())&&l.getJlife().equals("00")){
					lifelnbIO = l;
					break;
				}
			}
		}
		if (lifelnbIO == null) {
			syserrrec.params.set(chdrlifIO.getChdrnum());
			fatalError600();
		}
		wsaaMainAge.set(lifelnbIO.getAnbAtCcd());
		wsaaAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		wsaaSex.set(lifelnbIO.getCltsex());
		wsaaJointAge.set(ZERO);
		wsbbSex.set(SPACES);
		/*  Read Join Life file*/
		if(lifelnbMap!=null&&lifelnbMap.containsKey(chdrlifIO.getChdrnum())){
			for(Lifepf l:lifelnbMap.get(chdrlifIO.getChdrnum())){
				if(l.getChdrcoy().equals(bsprIO.getCompany().toString())&&l.getLife().equals(wsaaMainLife.toString())&&l.getJlife().equals("01")){
					lifelnbIO = l;
					break;
				}
			}
		}
		if (lifelnbIO != null) {
			wsaaJointAge.set(lifelnbIO.getAnbAtCcd());
			wsbbSex.set(lifelnbIO.getCltsex());
		}
		initialize(premiumrec.premiumRec);
		premiumrec.function.set("CALC");
		premiumrec.crtable.set(wsaaMainCrtable);
		premiumrec.chdrChdrcoy.set(chdrlifIO.getChdrcoy().toString());
		premiumrec.chdrChdrnum.set(wopxpfRec.getChdrnum());
		premiumrec.lifeLife.set(lifelnbIO.getLife());
		premiumrec.lifeJlife.set("00");
		premiumrec.covrCoverage.set(wsaaMainCoverage);
		premiumrec.covrRider.set("00");
		premiumrec.effectdt.set(chdrlifIO.getOccdate());
		premiumrec.termdate.set(wsaaMainPcessdte);
		premiumrec.currcode.set(chdrlifIO.getCntcurr());
		premiumrec.lsex.set(wsaaSex);
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.lage.set(wsaaMainAge);
		premiumrec.jlsex.set(wsbbSex);
		premiumrec.jlage.set(wsaaJointAge);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/*  (wsaa-sumin already adjusted for plan processing)*/
		premiumrec.cnttype.set(chdrlifIO.getCnttype());
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(wsaaMainCessdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		premiumrec.riskCessTerm.set(wsaaRiskCessTerm);
		/* MOVE ZEROES                 TO CPRM-BEN-CESS-TERM.*/
		premiumrec.sumin.set(wsaaCovrSumins);
		premiumrec.mortcls.set(wsaaMainMortclass);
		premiumrec.billfreq.set(chdrlifIO.getBillfreq());
		premiumrec.mop.set(chdrlifIO.getBillchnl());
		premiumrec.ratingdate.set(chdrlifIO.getOccdate());
		premiumrec.reRateDate.set(chdrlifIO.getOccdate());
		premiumrec.language.set(bsscIO.getLanguage());
		
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/* ILIFE-3472 Code Promotion for VPMS externalization of LIFE TRX product calculations to LIFE Target2 Environment Start */
	
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else{
		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
		vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
		Vpxlextrec vpxlextrec = new Vpxlextrec();
		vpxlextrec.function.set("INIT");
		callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
		
		Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
		vpxchdrrec.function.set("INIT");
		callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
		premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
		
		premiumrec.premMethod.set(wsaaPremMeth);
		callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxchdrrec);
		}
		/*Ticket ILIFE-3472  end */
		/*Ticket #ILIFE-2005 - End*/
	
		if (isNE(premiumrec.statuz,varcom.oK)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		wsaaCovrSumins = premiumrec.calcPrem.getbigdata();

	}

protected void moveT56877000()
	{
		/*PARA*/
		t5687rec.anniversaryMethod.set(wsaaT5687ArrayInner.wsaaT5687Annvry[wsaaT5687Ix]);
		t5687rec.premmeth.set(wsaaT5687ArrayInner.wsaaT5687Premmeth[wsaaT5687Ix]);
		t5687rec.jlPremMeth.set(wsaaT5687ArrayInner.wsaaT5687Jlpremmeth[wsaaT5687Ix]);
		t5687rec.premGuarPeriod.set(wsaaT5687ArrayInner.wsaaT5687Pguarp[wsaaT5687Ix]);
		t5687rec.rtrnwfreq.set(wsaaT5687ArrayInner.wsaaT5687Rtrnwfreq[wsaaT5687Ix]);
		t5687rec.zrrcombas.set(wsaaT5687ArrayInner.wsaaT5687Zrrcombas[wsaaT5687Ix]);
		t5687rec.bbmeth.set(wsaaT5687ArrayInner.wsaaT5687Bbmeth[wsaaT5687Ix]);
		/*EXIT*/
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.currency.set(chdrlifIO.getCntcurr());
		zrdecplrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void a000LoadNonWopCovrStatus()
	{
		if(covrbbrMap != null && covrbbrMap.containsKey(chdrlifIO.getChdrnum())){
			for(Covrpf covrbbrIO:covrbbrMap.get(chdrlifIO.getChdrnum())){
				if(covrbbrIO.getChdrcoy().equals(chdrlifIO.getChdrcoy().toString())){
					if (isEQ(covrbbrIO.getTranno(), wsaaNewTranno)) {
						continue;
					}
					ix.add(1);
					wsaaCovrStatcode[ix.toInt()].set(covrbbrIO.getStatcode());
					wsaaCovrPstatcode[ix.toInt()].set(covrbbrIO.getPstatcode());
				}
			}
		}
	}

protected void a100ReadT5399()
	{
		/* Read T5399 for update contract status                        */
		wsaaT5399Batctrcde.set(bprdIO.getAuthCode());
		wsaaT5399Cnttype.set(chdrlifIO.getCnttype());
		if(t5399Map!=null&&t5399Map.containsKey(wsaaT5399Key)){
			t5399rec.t5399Rec.set(StringUtil.rawToString(t5399Map.get(wsaaT5399Key).get(0).getGenarea()));
		}
	}
protected void getTr517rec(Covrpf covrIO){

	if (tr517Map != null) {
		for (List<Itempf> itemList : tr517Map.values()) {
			for (Itempf i : itemList) {

				if(covrIO.getCrtable().equals(i.getItemitem())){//IJTI-1485

					tr517rec.tr517Rec.set(StringUtil.rawToString(i.getGenarea()));

					return;
				}
			}

		}
	}
}

protected void checkRerateOnExistingWaiverClaim(Covrpf covrIO){

	getTr517rec(covrIO);
		if(isEQ(tr517rec.zrwvflg05 ,"Y")){

			regppfList = new ArrayList<>();

			List<Regppf>  rgpytypelist = regpDAO.readRegpByRgpytype(covrIO.getChdrnum(), covrIO.getCrtable());//IJTI-1485

			if(rgpytypelist !=null && !rgpytypelist.isEmpty()){

				Payrpf payrIO = null;
				if(payrMap!=null&&payrMap.containsKey(chdrlifIO.getChdrnum())){
					for(Payrpf p:payrMap.get(chdrlifIO.getChdrnum())){
						if(p.getChdrcoy().equals(chdrlifIO.getChdrcoy().toString())&&p.getPayrseqno() == 1){
							payrIO = p;
							break;
						}
					}
				}				
				wsaaTotPymtamt.set(ZERO);
				for(Regppf regppf : rgpytypelist){
					if(ptdateConfig){
						if(isNE(regppf.getValidflag(),'1')){
							continue;
						}
					}   
					
					regppf.setPymt(covrIO.getSumins());
					if (isEQ(tr517rec.zrwvflg01, "Y")) {
						computPaymt.set(regppf.getPymt());
						checkCalcCompTax7000(covrIO, null);
						zrdecplrec.amountIn.set(add(computPaymt, (div(mult(add(covrIO.getInstprem(),wsaaTax), payrIO.getBillfreq()), regppf.getRegpayfreq()))));
						callRounding8000();
						regppf.setPymt(zrdecplrec.amountOut.getbigdata());
						if(isNE(regppf.getAdjamt(),ZERO)){
							BigDecimal temp = regppf.getPymt().add(regppf.getAdjamt());
							regppf.setNetamt(temp);
						}else{
							regppf.setNetamt(regppf.getPymt());	
						}
						compute(wsaaIntermed, 6).setRounded(div(regppf.getPymt(), covrIO.getSumins()));
						compute(wsaaPrcnt, 6).setRounded(mult(wsaaIntermed, 100));
						regppf.setPrcnt(wsaaPrcnt.getbigdata());						
						wsaaTotPymtamt.add(zrdecplrec.amountOut);
					}

					regppf.setUser(varcom.vrcmUser.toInt());
					regppf.setTransactionDate(wsaaTransactionDate);
					regppf.setTransactionTime(wsaaTransactionTime);
					regppfList.add(regppf);					
					if(chdrlifIO.getSinstamt05()==null)
						chdrlifIO.setSinstamt05(BigDecimal.ZERO);
					else
						chdrlifIO.setSinstamt05(mult(wsaaTotPymtamt, -1).getbigdata());
				}

				regpDAO.updatePymtPrcnt(regppfList);
			}
		}
}

/*
 * Class transformed  from Data Structure WSAA-T5687-ARRAY--INNER
 */
private static final class WsaaT5687ArrayInner {

		/* WSAA-T5687-ARRAY */
	private FixedLengthStringData[] wsaaT5687Rec = FLSInittedArray (500, 31);
	private FixedLengthStringData[] wsaaT5687Key = FLSDArrayPartOfArrayStructure(4, wsaaT5687Rec, 0);
	private FixedLengthStringData[] wsaaT5687Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5687Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5687Data = FLSDArrayPartOfArrayStructure(27, wsaaT5687Rec, 4);
	private PackedDecimalData[] wsaaT5687Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5687Data, 0);
	private FixedLengthStringData[] wsaaT5687Annvry = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 5);
	private FixedLengthStringData[] wsaaT5687Premmeth = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 9);
	private FixedLengthStringData[] wsaaT5687Jlpremmeth = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 13);
	private ZonedDecimalData[] wsaaT5687Pguarp = ZDArrayPartOfArrayStructure(3, 0, wsaaT5687Data, 17);
	private ZonedDecimalData[] wsaaT5687Rtrnwfreq = ZDArrayPartOfArrayStructure(2, 0, wsaaT5687Data, 20);
	private FixedLengthStringData[] wsaaT5687Zrrcombas = FLSDArrayPartOfArrayStructure(1, wsaaT5687Data, 22);
	private FixedLengthStringData[] wsaaT5687Bbmeth = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 23);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData itemrec = new FixedLengthStringData(7).init("ITEMREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(7).init("COVRREC");
	private FixedLengthStringData covrrnlrec = new FixedLengthStringData(10).init("COVRRNLREC");
	private FixedLengthStringData covrlnbrec = new FixedLengthStringData(10).init("COVRLNBREC");
	private FixedLengthStringData lifelnbrec = new FixedLengthStringData(10).init("LIFELNBREC");
	private FixedLengthStringData lextbrrrec = new FixedLengthStringData(10).init("LEXTBRRREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(7).init("PTRNREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData annyrec = new FixedLengthStringData(10).init("ANNYREC");
	private FixedLengthStringData agcmbchrec = new FixedLengthStringData(10).init("AGCMBCHREC");
	private FixedLengthStringData agcmrec = new FixedLengthStringData(10).init("AGCMREC   ");
	private FixedLengthStringData pcddchgrec = new FixedLengthStringData(10).init("PCDDCHGREC");
	private FixedLengthStringData incrhstrec = new FixedLengthStringData(10).init("INCRHSTREC");
	private FixedLengthStringData incrmjarec = new FixedLengthStringData(10).init("INCRMJAREC");
}
}
