package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.B5018TempDAO;
import com.csc.life.regularprocessing.dataaccess.model.B5018DTO;
import com.csc.life.regularprocessing.dataaccess.model.T5679Rec;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class B5018TempDAOImpl extends BaseDAOImpl<B5018DTO> implements B5018TempDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(B5018TempDAOImpl.class);
	
	private void deleteB5018Temp() {
		String sql = "delete from B5018DATA"; //ILB-475
		PreparedStatement ps = getPrepareStatement(sql);
		try {
			ps.executeUpdate();
			getConnection().commit();
		} catch (SQLException e) {
			LOGGER.error("deleteTempCovr()" , e);
			try {
				getConnection().rollback();
			} catch (SQLException e1) {
				LOGGER.error("deleteB5018Temp()" , e);
			}
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	    
    public List<B5018DTO> searchB5018Temp( int batchID){
    	String sql = "  select  * from B5018DATA   where  BATCHID=? "; //ILB-475
    	PreparedStatement ps = getPrepareStatement(sql);
        ResultSet rs = null;
        List<B5018DTO> b5018DtoList = null;
    	 try {
 			 ps.setInt(1, batchID);
 			 
    		 rs = executeQuery(ps);
    		 
			 b5018DtoList = new ArrayList<B5018DTO>();
			 while(rs.next()){
				 B5018DTO covrSearchResultDto = new B5018DTO();
				 covrSearchResultDto.setChdrcoy(rs.getString(2));
				 covrSearchResultDto.setChdrnum(rs.getString(3));
				 covrSearchResultDto.setLife(rs.getString(4));
				 covrSearchResultDto.setCoverage(rs.getString(5));
				 covrSearchResultDto.setRider(rs.getString(6));
				 covrSearchResultDto.setPlnsfx(rs.getInt(7));
				 covrSearchResultDto.setPstatcode(rs.getString(8));
				 covrSearchResultDto.setStatcode(rs.getString(9));
				 covrSearchResultDto.setCrrcd(rs.getInt(10));
				 covrSearchResultDto.setCbunst(rs.getInt(11));
				 covrSearchResultDto.setRcesdte(rs.getInt(12));
				 covrSearchResultDto.setSumins(rs.getBigDecimal(13));
				 covrSearchResultDto.setValidflag(rs.getString(14));
				 covrSearchResultDto.setCurrfrom(rs.getInt(15));
				 covrSearchResultDto.setCurrto(rs.getInt(16));
				 covrSearchResultDto.setCrtable(rs.getString(17));
				 covrSearchResultDto.setCntcurr(rs.getString(18));
				 covrSearchResultDto.setCnttype(rs.getString(19));
				 covrSearchResultDto.setTranno(rs.getInt(20));
				 covrSearchResultDto.setCowncoy(rs.getString(21));
				 covrSearchResultDto.setCownnum(rs.getString(22));
				 covrSearchResultDto.setJobnm(rs.getString(23));
				 covrSearchResultDto.setUsrprf(rs.getString(24));
				 covrSearchResultDto.setCntbranch(rs.getString(25));
				 b5018DtoList.add(covrSearchResultDto);
			}
		} catch (SQLException e) {
            LOGGER.error("searchCovrByT5679FromB5018dtosTemp()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
    	 return b5018DtoList;
    }
    
	@Override
	public void initTempTable(String company, String chdrnumStart,
			String chdrnumEnd, String cbunst,int batchExtractSize, T5679Rec t579Rec) {
		deleteB5018Temp();
		insertToB5018Temp( company, chdrnumStart, chdrnumEnd, cbunst,batchExtractSize,t579Rec);
	}
	private void insertToB5018Temp(String company,String chdrnumStart, String chdrnumEnd, String cbunst,int batchExtractSize,T5679Rec t579Rec) {
		StringBuilder sql = new StringBuilder();
		 //ILB-475
		sql.append("insert into B5018DATA(chdrcoy, chdrnum, life, coverage, rider, plnsfx, crtable, pstatcode, statcode, crrcd, cbunst, rcesdte, sumins, validflag, currfrom, currto")
		.append(",cntcurr,cnttype,tranno,cowncoy,cownnum ,jobnm,usrprf,cntbranch,batchid)")
		.append(" select distinct co.chdrcoy, co.chdrnum, co.life, co.coverage, co.rider, co.plnsfx, co.crtable, co.pstatcode, co.statcode, co.crrcd, co.cbunst, co.rcesdte, co.sumins, co.validflag,")
		.append("co.currfrom, co.currto,ch.cntcurr,ch.cnttype,ch.tranno,ch.cowncoy,ch.cownnum ,ch.jobnm,ch.usrprf,cf.cntbranch,FLOOR((ROW_NUMBER() over(order by  co.chdrcoy,co.chdrnum,co.coverage, co.life,co.rider, co.plnsfx ASC)-1)/?) batchid from  covrpf co")
		.append(" left join chdrpf  ch on  ch.chdrcoy = co.chdrcoy and ch.chdrnum = co.chdrnum")
		.append( " left join chdrlif cf on  cf.chdrcoy=ch.chdrcoy and cf.chdrnum=ch.chdrnum" )
		.append(" where co.chdrcoy = ? and co.bnusin = 'C' and co.chdrnum between ? and ? and co.validflag = '1' and (co.cbunst < ? or co.cbunst = 99999999)  ")
		.append(" and co.statcode in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)  and co.pstatcode in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)  and (" )
		.append( "ch.validflag = '1' and ch.statcode in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) and ch.pstcde in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)) ")
		//.append("order by a.rldgacct, a.origcurr, a.effdate, a.sacstyp")
		.append("order by co.chdrcoy,co.chdrnum,co.coverage, co.life,co.rider, co.plnsfx ");
		 PreparedStatement ps = getPrepareStatement(sql.toString());
//		        ResultSet rs = null;
//		        List<B5018DTO> b5018DtoList = null;
		 try {
			 	ps.setInt(1, batchExtractSize);
				ps.setString(2, company);
				ps.setString(3, chdrnumStart);
				ps.setString(4, chdrnumEnd);
				ps.setString(5, cbunst);
				ps.setString(6, t579Rec.t5679CovRiskStat01.toString());
				ps.setString(7, t579Rec.t5679CovRiskStat02.toString());
				ps.setString(8, t579Rec.t5679CovRiskStat03.toString());
				ps.setString(9, t579Rec.t5679CovRiskStat04.toString());
				ps.setString(10, t579Rec.t5679CovRiskStat05.toString());
				ps.setString(11, t579Rec.t5679CovRiskStat06.toString());
				ps.setString(12, t579Rec.t5679CovRiskStat07.toString());
				ps.setString(13, t579Rec.t5679CovRiskStat08.toString());
				ps.setString(14, t579Rec.t5679CovRiskStat09.toString());
				ps.setString(15, t579Rec.t5679CovRiskStat10.toString());
				ps.setString(16, t579Rec.t5679CovRiskStat11.toString());
				ps.setString(17, t579Rec.t5679CovRiskStat12.toString());
				ps.setString(18, t579Rec.t5679CovPremStat01.toString());
				ps.setString(19, t579Rec.t5679CovPremStat02.toString());
				ps.setString(20, t579Rec.t5679CovPremStat03.toString());
				ps.setString(21, t579Rec.t5679CovPremStat04.toString());
				ps.setString(22, t579Rec.t5679CovPremStat05.toString());
				ps.setString(23, t579Rec.t5679CovPremStat06.toString());
				ps.setString(24, t579Rec.t5679CovPremStat07.toString());
				ps.setString(25, t579Rec.t5679CovPremStat08.toString());
				ps.setString(26, t579Rec.t5679CovPremStat09.toString());
				ps.setString(27, t579Rec.t5679CovPremStat10.toString());
				ps.setString(28, t579Rec.t5679CovPremStat11.toString());
				ps.setString(29, t579Rec.t5679CovPremStat12.toString());
				ps.setString(30, t579Rec.t5679CnRiskStat01.toString());
				ps.setString(31, t579Rec.t5679CnRiskStat02.toString());
				ps.setString(32, t579Rec.t5679CnRiskStat03.toString());
				ps.setString(33, t579Rec.t5679CnRiskStat04.toString());
				ps.setString(34, t579Rec.t5679CnRiskStat05.toString());
				ps.setString(35, t579Rec.t5679CnRiskStat06.toString());
				ps.setString(36, t579Rec.t5679CnRiskStat07.toString());
				ps.setString(37, t579Rec.t5679CnRiskStat08.toString());
				ps.setString(38, t579Rec.t5679CnRiskStat09.toString());
				ps.setString(39, t579Rec.t5679CnRiskStat10.toString());
				ps.setString(40, t579Rec.t5679CnRiskStat11.toString());
				ps.setString(41, t579Rec.t5679CnRiskStat12.toString());
				ps.setString(42, t579Rec.t5679CnPremStat01.toString());
				ps.setString(43, t579Rec.t5679CnPremStat02.toString());
				ps.setString(44, t579Rec.t5679CnPremStat03.toString());
				ps.setString(45, t579Rec.t5679CnPremStat04.toString());
				ps.setString(46, t579Rec.t5679CnPremStat05.toString());
				ps.setString(47, t579Rec.t5679CnPremStat06.toString());
				ps.setString(48, t579Rec.t5679CnPremStat07.toString());
				ps.setString(49, t579Rec.t5679CnPremStat08.toString());
				ps.setString(50, t579Rec.t5679CnPremStat09.toString());
				ps.setString(51, t579Rec.t5679CnPremStat10.toString());
				ps.setString(52, t579Rec.t5679CnPremStat11.toString());
				ps.setString(53, t579Rec.t5679CnPremStat12.toString());
				ps.executeUpdate();
				getConnection().commit();
			} catch (SQLException e) {
	            LOGGER.error("searchCovrByT5679()", e);//IJTI-1561
	            try {
					getConnection().rollback();
				} catch (SQLException e1) {
					LOGGER.error("insertToB5018Temp()", e);//IJTI-1561
				}
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, null);
	        }
			
	}

	@Override
	public List<B5018DTO> getALLB5018Temp() {
		String sql = "select  * from B5018DATA "; //ILB-475
    	
        ResultSet rs = null;
        List<B5018DTO> b5018DtoList = new ArrayList<B5018DTO>();
    	try (PreparedStatement ps = getPrepareStatement(sql)) {
 			 
    		 rs = executeQuery(ps);
    		 
			 while(rs.next()){
				 B5018DTO covrSearchResultDto = new B5018DTO();
				 covrSearchResultDto.setChdrcoy(rs.getString(2));
				 covrSearchResultDto.setChdrnum(rs.getString(3));
				 covrSearchResultDto.setLife(rs.getString(4));
				 covrSearchResultDto.setCoverage(rs.getString(5));
				 covrSearchResultDto.setRider(rs.getString(6));
				 covrSearchResultDto.setPlnsfx(rs.getInt(7));
				 covrSearchResultDto.setPstatcode(rs.getString(8));
				 covrSearchResultDto.setStatcode(rs.getString(9));
				 covrSearchResultDto.setCrrcd(rs.getInt(10));
				 covrSearchResultDto.setCbunst(rs.getInt(11));
				 covrSearchResultDto.setRcesdte(rs.getInt(12));
				 covrSearchResultDto.setSumins(rs.getBigDecimal(13));
				 covrSearchResultDto.setValidflag(rs.getString(14));
				 covrSearchResultDto.setCurrfrom(rs.getInt(15));
				 covrSearchResultDto.setCurrto(rs.getInt(16));
				 covrSearchResultDto.setCrtable(rs.getString(17));
				 covrSearchResultDto.setCntcurr(rs.getString(18));
				 covrSearchResultDto.setCnttype(rs.getString(19));
				 covrSearchResultDto.setTranno(rs.getInt(20));
				 covrSearchResultDto.setCowncoy(rs.getString(21));
				 covrSearchResultDto.setCownnum(rs.getString(22));
				 covrSearchResultDto.setJobnm(rs.getString(23));
				 covrSearchResultDto.setUsrprf(rs.getString(24));
				 covrSearchResultDto.setCntbranch(rs.getString(25));
				 b5018DtoList.add(covrSearchResultDto);
			}
		} catch (SQLException e) {
            LOGGER.error("searchCovrByT5679FromB5018dtosTemp()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        }
    	 return b5018DtoList;
	}}