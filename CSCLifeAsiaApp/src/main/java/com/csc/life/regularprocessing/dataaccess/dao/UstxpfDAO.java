package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.Ustxpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface UstxpfDAO extends BaseDAO<Ustxpf> {
	public List<Ustxpf> searchResult(String tableId, String memName, int batchExtractSize, int batchID);
}
