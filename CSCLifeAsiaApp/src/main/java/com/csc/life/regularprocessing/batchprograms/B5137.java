/*
 * File: B5137.java
 * Date: 29 August 2009 20:58:42
 * Author: Quipoz Limited
 *
 * Class transformed from B5137.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.PcdtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.PrmhpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Prmhpf;
import com.csc.life.contractservicing.procedures.Acomcalc;
import com.csc.life.contractservicing.recordstructures.Acomcalrec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.reassurance.dataaccess.dao.RacdpfDAO;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.life.reassurance.procedures.Actvres;
import com.csc.life.reassurance.procedures.Rasaltr;
import com.csc.life.reassurance.recordstructures.Actvresrec;
import com.csc.life.reassurance.recordstructures.Rasaltrrec;
import com.csc.life.regularprocessing.dataaccess.InczpfTableDAM;
import com.csc.life.regularprocessing.dataaccess.dao.AinrpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.InczpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Ainrpf;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.regularprocessing.dataaccess.model.Inczpf;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*              ACTUAL AUTOMATIC INCREASES
*              --------------------------
* Overview
* ________
*
*   This program is part of the Automatic Increases multi-thread
* batch suite. It runs directly after B5136 which 'splits' the
* INCRPF into a number of threads. References to INCR are via
* INCZPF - a temporary file holding all of the INCR records for
* this program to process.
*
* This program processes the Increase (INCRPF) records selected by
* the splitter program for processing.  The Increase records are
* updated to prevent them being reselected for processing.
* Depending on the increase method, a new coverage record is
* written for the increase or the existing coverage is updated.
* Any commission and reassurance changes are processed before
* calling any generic processing subroutines which may be required.
* Finally, per contract processed, the contract header is updated
* and a transaction record (PTRNPF) written.
*
***********************************************************************
* </pre>
*/
public class B5137 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private InczpfTableDAM inczpf = new InczpfTableDAM();
//	private InczpfTableDAM inczpfRec = new InczpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5137");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

		/* INCZ PARAMETERS.                                             */
	private FixedLengthStringData wsaaInczFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaInczFn, 0, FILLER).init("INCZ");
	private FixedLengthStringData wsaaInczRunid = new FixedLengthStringData(2).isAPartOf(wsaaInczFn, 4);
	private ZonedDecimalData wsaaInczJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaInczFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler2 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaOldCpiDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaTransDate = new PackedDecimalData(6, 0);
	private PackedDecimalData wsaaTransTime = new PackedDecimalData(6, 0);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaFixNoIncr = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaMinTrmToCess = new ZonedDecimalData(3, 0);
	private PackedDecimalData wsaaZbinstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaZlinstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaInstprem = new PackedDecimalData(13, 2).init(ZERO);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaBillfreq = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaKeyChange = "N";
	private String wsaaSubprogFound = "N";

	private FixedLengthStringData wsaaNextCovridno = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaNextCovno = new FixedLengthStringData(2).isAPartOf(wsaaNextCovridno, 0);
	private FixedLengthStringData wsaaNextRidno = new FixedLengthStringData(2).isAPartOf(wsaaNextCovridno, 2);

	private FixedLengthStringData filler4 = new FixedLengthStringData(4).isAPartOf(wsaaNextCovridno, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaNextCovnoR = new ZonedDecimalData(2, 0).isAPartOf(filler4, 0).setUnsigned();
	private ZonedDecimalData wsaaNextRidnoR = new ZonedDecimalData(2, 0).isAPartOf(filler4, 2).setUnsigned();
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaStoreRider = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaStorePlnsfx = new PackedDecimalData(4, 0).init(ZERO);
	private FixedLengthStringData wsaaL1Clntnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaL2Clntnum = new FixedLengthStringData(8).init(SPACES);
		/* Arrays to store regularly-referenced data, and reduce the   r
		 number accesses to the Itempf.
		 Array sizes are also declared so that an increase of table
		 size does not impact its processing.*/
	private ZonedDecimalData wsaaT5679Ix = new ZonedDecimalData(2, 0).setUnsigned();
	private static final int wsaaT6658Size = 200;
	
	private PackedDecimalData wsaaT6658IxMax = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaT5671IxMax = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaT5688IxMax = new PackedDecimalData(5, 0);
	//ILAE-63 Issue7 START by dpuhawwan
	//private static final int wsaaT5447Size = 30;
	//private static final int wsaaT5447Size = 90;
	//ILIFE-2628 fixed--Array size increased 
	private static final int wsaaT5447Size = 1000;
	//ILAE-63 Issue7 END

		/* WSAA-T5447-ARRAY */
	//ILIFE-1707 START by dpuhawan
	//private FixedLengthStringData[] wsaaT5447Rec = FLSInittedArray (30, 3);
	private FixedLengthStringData[] wsaaT5447Rec = FLSInittedArray (1000, 3);
	//ILIFE-1707 END
	private FixedLengthStringData[] wsaaT5447Key = FLSDArrayPartOfArrayStructure(3, wsaaT5447Rec, 0);
	private FixedLengthStringData[] wsaaT5447ChdrlifCnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5447Key, 0, HIVALUES);
	//private static final int wsaaT5688Size = 300;
	//ILIFE-2628 fixed--Array size increased 
	private static final int wsaaT5688Size = 1000;

		/* WSAA-T5688-ARRAY */
	private FixedLengthStringData[] wsaaT5688Rec = FLSInittedArray (1000, 14);
	private PackedDecimalData[] wsaaT5688Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5688Rec, 0);
	private PackedDecimalData[] wsaaT5688Itmto = PDArrayPartOfArrayStructure(8, 0, wsaaT5688Rec, 5);
	private FixedLengthStringData[] wsaaT5688Key = FLSDArrayPartOfArrayStructure(3, wsaaT5688Rec, 10);
	private FixedLengthStringData[] wsaaT5688Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5688Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT5688Data = FLSDArrayPartOfArrayStructure(1, wsaaT5688Rec, 13);
	private FixedLengthStringData[] wsaaT5688Comlvlacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 0);
		/* WSAA-T5671-ITEM */
	private FixedLengthStringData wsaaItemTranCode = new FixedLengthStringData(4);
	//private static final int wsaaT5671Size = 500;
	//ILIFE-2628 fixed--Array size increased 
	private static final int wsaaT5671Size = 1000;

		/* WSAA-T5671-ARRAY
		    03  WSAA-T5671-REC          OCCURS 50                        */
	private FixedLengthStringData[] wsaaT5671Rec = FLSInittedArray (1000, 48);
	private FixedLengthStringData[] wsaaT5671Key = FLSDArrayPartOfArrayStructure(8, wsaaT5671Rec, 0);
	private FixedLengthStringData[] wsaaT5671Authcode = FLSDArrayPartOfArrayStructure(4, wsaaT5671Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT5671Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5671Key, 4, HIVALUES);
	private FixedLengthStringData[] wsaaT5671Data = FLSDArrayPartOfArrayStructure(40, wsaaT5671Rec, 8);
	private FixedLengthStringData[] wsaaT5671Subprog1 = FLSDArrayPartOfArrayStructure(10, wsaaT5671Data, 0);
	private FixedLengthStringData[] wsaaT5671Subprog2 = FLSDArrayPartOfArrayStructure(10, wsaaT5671Data, 10);
	private FixedLengthStringData[] wsaaT5671Subprog3 = FLSDArrayPartOfArrayStructure(10, wsaaT5671Data, 20);
	private FixedLengthStringData[] wsaaT5671Subprog4 = FLSDArrayPartOfArrayStructure(10, wsaaT5671Data, 30);

	private FixedLengthStringData wsaaSubprogs = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsaaSubprog = FLSArrayPartOfStructure(4, 10, wsaaSubprogs, 0);

	private FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(wsaaSubprogs, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaSubprog1 = new FixedLengthStringData(10).isAPartOf(filler5, 0);
	private FixedLengthStringData wsaaSubprog2 = new FixedLengthStringData(10).isAPartOf(filler5, 10);
	private FixedLengthStringData wsaaSubprog3 = new FixedLengthStringData(10).isAPartOf(filler5, 20);
	private FixedLengthStringData wsaaSubprog4 = new FixedLengthStringData(10).isAPartOf(filler5, 30);

	private FixedLengthStringData wsaaValidComp = new FixedLengthStringData(1).init("N");
	private Validator validComp = new Validator(wsaaValidComp, "Y");
	private Validator invalidComp = new Validator(wsaaValidComp, "N");

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");
	private Validator invalidContract = new Validator(wsaaValidContract, "N");

	private FixedLengthStringData wsaaContractLock = new FixedLengthStringData(1).init("N");
	private Validator contractLocked = new Validator(wsaaContractLock, "Y");
	private Validator contractNotLocked = new Validator(wsaaContractLock, "N");

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");
	private Validator notFirstTime = new Validator(wsaaFirstTime, "N");

	private FixedLengthStringData wsaaBypassFlag = new FixedLengthStringData(1).init("N");
	private Validator processReassurance = new Validator(wsaaBypassFlag, "Y");
	private Validator bypassReassurance = new Validator(wsaaBypassFlag, "N");
		/* TABLES */
	private static final String t5688 = "T5688";
	private static final String t6658 = "T6658";
	private static final String t5671 = "T5671";
	private static final String t5687 = "T5687";
	private static final String t5447 = "T5447";
	private static final String t5679 = "T5679";
		/* ERRORS */
	private static final String e302 = "E302";
	private static final String e308 = "E308";
	private static final String e514 = "E514";
	private static final String f294 = "F294";
	private static final String h791 = "H791";
	private static final String h036 = "H036";
	private static final String ivrm = "IVRM";
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaT6658Ix = new IntegerData();
	private IntegerData wsaaT5447Ix = new IntegerData();
	private IntegerData wsaaT5688Ix = new IntegerData();
	private IntegerData wsaaT5671Ix = new IntegerData();
//	private AinrTableDAM ainrIO = new AinrTableDAM();
//	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
//	private CovrTableDAM covrIO = new CovrTableDAM();
//	private CovrrnlTableDAM covrrnlIO = new CovrrnlTableDAM();
//	private IncrTableDAM incrIO = new IncrTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
//	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
//	private LifergpTableDAM lifergpIO = new LifergpTableDAM();
//	private PayrTableDAM payrIO = new PayrTableDAM();
	private PcdtmjaTableDAM pcdtmjaIO = new PcdtmjaTableDAM();
//	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
//	private RacdTableDAM racdIO = new RacdTableDAM();
//	private RacdlnbTableDAM racdlnbIO = new RacdlnbTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private T6658rec t6658rec = new T6658rec();
	private Acomcalrec acomcalrec = new Acomcalrec();
	private Isuallrec isuallrec = new Isuallrec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Actvresrec actvresrec = new Actvresrec();
	private Rasaltrrec rasaltrrec = new Rasaltrrec();
	private T5687rec t5687rec = new T5687rec();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaT6658ArrayInner wsaaT6658ArrayInner = new WsaaT6658ArrayInner();

    private Iterator<Inczpf> iteratorList; 
    private int intBatchID = 0;
    private int intBatchExtractSize;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private InczpfDAO inczpfDAO = getApplicationContext().getBean("inczpfDAO", InczpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private IncrpfDAO incrpfDAO = getApplicationContext().getBean("incrpfDAO", IncrpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private RacdpfDAO racdpfDAO = getApplicationContext().getBean("racdpfDAO", RacdpfDAO.class);
	private AinrpfDAO ainrpfDAO = getApplicationContext().getBean("ainrpfDAO", AinrpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	
	private Inczpf inczpfRec = null;
	private Chdrpf chdrlifRec = null;
	private Map<String, List<Itempf>> t6658Map = null;
	private Map<String, List<Itempf>> t5688Map = null;
	private Map<String, List<Itempf>> t5671Map = null;
	private Map<String, List<Itempf>> t5447Map = null;
	private Map<String, List<Itempf>> t5687Map = null;
	private Map<String, List<Chdrpf>> chdrlifMap = null;
	private Map<String, List<Payrpf>> payrMap = null;
	private Map<String, List<Incrpf>> incrMap = null;
	private Map<String, List<Covrpf>> covrMap = null;
	private Map<String, List<Lifepf>> lifelnbMap = null;
	
    private int ct01Value = 0;
    private int ct02Value = 0;
    private int ct03Value = 0;
    private int ct04Value = 0;
    private int ct05Value = 0;
    private int ct06Value = 0;
    private int ct07Value = 0;
    private int ct08Value = 0;
    private int ct09Value = 0;
    private int ct10Value = 0;
    private int ct11Value = 0;
    
    private List<Chdrpf> updateChdrlifList;
    private List<Chdrpf> insertChdrlifList;
    private List<Payrpf> updatePayrList;
    private List<Payrpf> insertPayrList;
    private List<Ptrnpf> insertPtrnList;
    private List<Incrpf> updateIncrList;
    private List<Covrpf> updateCovrList;
    private List<Covrpf> insertCovrList;
    private List<Covrpf> insertNewCovrList;
    private List<Ainrpf> insertAinrList;
    private List<Racdpf> deleteRacdList;
    private boolean riskPremflag = false; //ILIFE-7845 
  	private static final String  RISKPREM_FEATURE_ID="NBPRP094";//ILIFE-7845 
  	private Premiumrec premiumrec=new Premiumrec(); //ILIFE-7845 
  	private boolean stampDutyflag = false;
	private boolean reinstflag = false;
  	private List<Ptrnpf> ptrnrecords = null;
	private static final String ta85 = "TA85";
	private boolean isFoundPro = false;
	private Ptrnpf ptrnpfReinstate = new Ptrnpf();
	private Incrpf incrpf = null;
	private ZonedDecimalData wsaaInstOutst = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaProCpiDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaLastCpiDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaNewCpiDate = new PackedDecimalData(8, 0);
	private PackedDecimalData chdrTranno = new PackedDecimalData(5, 0);
	private boolean prmhldtrad = false;//ILIFE-8509
	private boolean lapseReinstated = false;
	private Map<String, Itempf> ta524Map;
	private Prmhpf prmhpf;
	private PrmhpfDAO prmhpfDAO = getApplicationContext().getBean("prmhpfDAO", PrmhpfDAO.class);

	public B5137() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
        if (isNE(bprdIO.getRestartMethod(),"3")) {
            syserrrec.statuz.set(ivrm);
            fatalError600();
        }
        /* Point to correct member of INCZPF.                           */
        wsaaInczRunid.set(bprdIO.getSystemParam04());
        wsaaInczJobno.set(bsscIO.getScheduleNumber());
        wsaaThreadNumber.set(bsprIO.getProcessOccNum());
//        StringUtil stringVariable1 = new StringUtil();
//        stringVariable1.addExpression("OVRDBF FILE(INCZPF) TOFILE(");
//        stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
//        stringVariable1.addExpression("/");
//        stringVariable1.addExpression(wsaaInczFn);
//        stringVariable1.addExpression(") ");
//        stringVariable1.addExpression("MBR(");
//        stringVariable1.addExpression(wsaaThreadMember);
//        stringVariable1.addExpression(")");
//        stringVariable1.addExpression(" SEQONLY(*YES 1000)");
//        stringVariable1.setStringInto(wsaaQcmdexc);
//        com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
        wsaaTransDate.set(bsscIO.getEffectiveDate());
        varcom.vrcmTimex.set(getCobolTime());
        wsaaTransTime.set(varcom.vrcmTimen);
       // inczpf.openInput();
        
        if (bprdIO.systemParam01.isNumeric()){
            if (bprdIO.systemParam01.toInt() > 0){
                intBatchExtractSize = bprdIO.systemParam01.toInt();
            }else{
                intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
            }
        }else{
            intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();  
        }
        readChunkRecord();
        
        firstTime.setTrue();
        wsaaChdrnum.set(SPACES);
        wsspEdterror.set(varcom.oK);
        itemIO.setDataArea(SPACES);
        itemIO.setStatuz(varcom.oK);
        itemIO.setItempfx("IT");
        itemIO.setItemcoy(bsprIO.getCompany());
        itemIO.setItemtabl(t5679);
        itemIO.setItemitem(bprdIO.getAuthCode());
        itemIO.setFormat(formatsInner.itemrec);
        itemIO.setFunction(varcom.readr);
        SmartFileCode.execute(appVars, itemIO);
        if (isNE(itemIO.getStatuz(),varcom.oK)) {
            syserrrec.statuz.set(itemIO.getStatuz());
            syserrrec.params.set(itemIO.getParams());
            fatalError600();
        }
        t5679rec.t5679Rec.set(itemIO.getGenarea());
        
        String coy = bsprIO.getCompany().toString();
        t6658Map = itemDAO.loadSmartTable("IT", coy, "T6658");
        wsaaT6658Ix.set(1);
        loadT66581500();
    
        t5688Map = itemDAO.loadSmartTable("IT", coy, "T5688");
        wsaaT5688Ix.set(1);
        loadT56881600();
        
        t5671Map = itemDAO.loadSmartTable("IT", coy, "T5671");
        wsaaT5671Ix.set(1);
        loadT56711700();
        
        t5447Map = itemDAO.loadSmartTable("IT", coy, "T5447");
        wsaaT5447Ix.set(1);
        loadT54471800();
        
        t5687Map = itemDAO.loadSmartTable("IT", coy, "T5687");
        
        String stampdutyItem = "NBPROP01";
		stampDutyflag = FeaConfg.isFeatureExist(coy, stampdutyItem, appVars, "IT");
		String prmhldtradItem = "CSOTH010";//ILIFE-8509
		prmhldtrad = FeaConfg.isFeatureExist(coy, prmhldtradItem, appVars, "IT");
		if(prmhldtrad)	//ILIFE-8179
			ta524Map = itemDAO.getItemMap("IT", coy, "TA524");
	}

protected void loadT66581500()
 {
        if (t6658Map == null || t6658Map.size() == 0) {
            return;
        }
        /* If the number of items stored exceeds the working storage */
        /* array size, this is a fatal error. */
        if (t6658Map.size() > wsaaT6658Size) {
            syserrrec.statuz.set(h036);
            syserrrec.params.set("t6658");
            fatalError600();
        }
        for (List<Itempf> items : t6658Map.values()) {
            for (Itempf item : items) {
                t6658rec.t6658Rec.set(StringUtil.rawToString(item.getGenarea()));
                wsaaT6658ArrayInner.wsaaT6658Annvry[wsaaT6658Ix.toInt()].set(item.getItemitem());
                wsaaT6658ArrayInner.wsaaT6658Itmfrm[wsaaT6658Ix.toInt()].set(item.getItmfrm());
                wsaaT6658ArrayInner.wsaaT6658Itmto[wsaaT6658Ix.toInt()].set(item.getItmto());
                wsaaT6658ArrayInner.wsaaT6658Data[wsaaT6658Ix.toInt()].set(t6658rec.t6658Rec);
                wsaaT6658IxMax.set(wsaaT6658Ix);
                wsaaT6658Ix.add(1);
            }
        }
    }

protected void loadT56881600()
 {
        if (t5688Map == null || t5688Map.size() == 0) {
            return;
        }
        /* If the number of items stored exceeds the working storage */
        /* array size, this is a fatal error. */
        if (t5688Map.size() > wsaaT5688Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set("t5688");
            fatalError600();
        }
        for (List<Itempf> items : t5688Map.values()) {
            for (Itempf item : items) {
                t5688rec.t5688Rec.set(StringUtil.rawToString(item.getGenarea()));
                wsaaT5688Cnttype[wsaaT5688Ix.toInt()].set(item.getItemitem());
                wsaaT5688Itmfrm[wsaaT5688Ix.toInt()].set(item.getItmfrm());
                wsaaT5688Itmto[wsaaT5688Ix.toInt()].set(item.getItmto());
                wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()].set(t5688rec.comlvlacc);
                wsaaT5688IxMax.set(wsaaT5688Ix);
                wsaaT5688Ix.add(1);
            }
        }
    }

protected void loadT56711700()
 {
        if (t5671Map == null || t5671Map.size() == 0) {
            syserrrec.params.set("t5671:" + bprdIO.getAuthCode());
            fatalError600();
            return;
        }
        for (List<Itempf> items : t5671Map.values()) {
            for (Itempf item : items) {
                if (isEQ(subString(item.getItemitem(), 1, 4), bprdIO.getAuthCode())) {
                    /* If the number of items stored exceeds the working storage */
                    /* array size, this is a fatal error. */
                    if (isGT(wsaaT5671Ix,wsaaT5671Size)) {
                        syserrrec.statuz.set(h791);
                        syserrrec.params.set("t5671");
                        fatalError600();
                    }
                    t5671rec.t5671Rec.set(StringUtil.rawToString(item.getGenarea()));
                    wsaaT5671Key[wsaaT5671Ix.toInt()].set(item.getItemitem());
                    wsaaT5671Subprog1[wsaaT5671Ix.toInt()].set(t5671rec.subprog01);
                    wsaaT5671Subprog2[wsaaT5671Ix.toInt()].set(t5671rec.subprog02);
                    wsaaT5671Subprog3[wsaaT5671Ix.toInt()].set(t5671rec.subprog03);
                    wsaaT5671Subprog4[wsaaT5671Ix.toInt()].set(t5671rec.subprog04);
                    wsaaT5671IxMax.set(wsaaT5671Ix);
                    wsaaT5671Ix.add(1);
                }
            }
        }
        if(wsaaT5671Ix.toInt() == 1){
            syserrrec.params.set("t5671:" + bprdIO.getAuthCode());
            fatalError600();
            return;
        }
    }

protected void loadT54471800()
 {
        if (t5447Map == null || t5447Map.size() == 0) {
            return;
        }
        /* If the number of items stored exceeds the working storage */
        /* array size, this is a fatal error. */
        if (t5447Map.size() > wsaaT5447Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set("t5447");
            fatalError600();
        }
        for (List<Itempf> items : t5447Map.values()) {
            for (Itempf item : items) {
                wsaaT5447Key[wsaaT5447Ix.toInt()].set(item.getItemitem());
                wsaaT5447Ix.add(1);
            }
        }
    }

protected void readFile2000()
 {
        /* Read the temporary file INCZPF */
        /* Perform contract update processing only on change of cnt */
        /* or at end of file to pick up the last contract processed. */
        if (!iteratorList.hasNext()) {
            intBatchID++;
            readChunkRecord();
            if (!iteratorList.hasNext()) {
                wsspEdterror.set(varcom.endp);
                updateHeader2080();
                return;
            }
        }
        if (firstTime.isTrue()) {
            notFirstTime.setTrue();
        }
        inczpfRec = iteratorList.next();
        ct01Value++;
        /* GO TO 2090-EXIT. */
        updateHeader2080();
    }

    private void readChunkRecord() {
        List<Inczpf> inczpfList = inczpfDAO.searchInczRecord(wsaaInczFn.toString(), wsaaThreadMember.toString(),
                intBatchExtractSize, intBatchID);
        iteratorList = inczpfList.iterator();
        if (!inczpfList.isEmpty()) {
            Set<String> chdrnumSet = new LinkedHashSet<>();
            for (Inczpf h : inczpfList) {
                chdrnumSet.add(h.getChdrnum());
            }
            List<String> chdrList = new ArrayList<>(chdrnumSet);
            chdrlifMap = chdrpfDAO.searchChdrRecordByChdrnum(chdrList);
            payrMap = payrpfDAO.searchPayrRecordByChdrnumChdrcoy(bsprIO.getCompany().trim(),chdrList);
            incrMap = incrpfDAO.searchIncrRecordByChdrnum(chdrList);
            covrMap = covrpfDAO.searchValidCovrByChdrnum(chdrList);
            lifelnbMap = lifepfDAO.searchLifelnbRecord(bsprIO.getCompany().trim(),chdrList); //ILIFE-8901
            //racdlnbMap = racdpfDAO.searchRacdlnbRecord(chdrList);
        }
    }

protected void updateHeader2080()
	{
		/* Contract header and payor file should be updated only when      */
		/* there is a changed in contract number or end of file.           */
		if ((notFirstTime.isTrue()
		&& isNE(inczpfRec.getChdrnum(),wsaaChdrnum)
		&& isNE(wsaaChdrnum,SPACES))
		|| (notFirstTime.isTrue()
		&& isEQ(wsspEdterror,varcom.endp))) {
			
			Chdrpf c = chdrlifUpdate8000();
			if(isFoundPro){
				while(isLT(wsaaLastCpiDate,bsscIO.getEffectiveDate())){
					ptrnWrite8100(c);
					wsaaLastCpiDate.set(getChdrTranno(wsaaLastCpiDate.toInt()));
				}
			}
			else{
			ptrnWrite8100(c);
			}
			writeUnlock8200(c.getChdrnum());
			
		}
		/*EXIT*/
	}

protected void edit2500()
	{
        /* This section checks the contract risk and premium status*/
        /* and the component risk and premium status on T5679 and*/
        /* then softlocks the contract once valid status are returned*/
        /* from all checks.*/
        /* BUT before all of this, if the contract is locked then*/
        /* then do not allow this record to be processed any further.*/
        /* Do this check only if we are processing a record for the*/
        /* current contract.*/
        /* IF CHDRNUM                   = WSAA-CHDRNUM                  */
        /*     IF CONTRACT-LOCKED                                       */
        /*        OR INVALID-CONTRACT                                   */
        /*         MOVE SPACES         TO WSSP-EDTERROR                 */
        /*         GO TO 2599-EXIT                                      */
        /*     END-IF                                                   */
        /* END-IF.                                                      */
        wsspEdterror.set(varcom.oK);
        checkComponentStatuz2600();
        	
        if (invalidComp.isTrue()) {
            wsspEdterror.set(SPACES);
            ct10Value++;
            return ;
        }
        if (isEQ(inczpfRec.getChdrnum(),wsaaChdrnum)) {
            return ;
        }
        else
        	lapseReinstated = false;//ILIFE-8509
        chdrlifRec = readChdr2700();
        checkContractStatuz2750(chdrlifRec);
        if (invalidContract.isTrue()) {
            wsspEdterror.set(SPACES);
            ct11Value++;
            return ;
        }
        softlock2760();
	}

protected void checkComponentStatuz2600()
	{
        invalidComp.setTrue();
        for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix,12)
        || validComp.isTrue()); wsaaT5679Ix.add(1)){
            if (isEQ(inczpfRec.getStatcode(),t5679rec.covRiskStat[wsaaT5679Ix.toInt()])) {
                validComp.setTrue();
            }
        }
        if (!validComp.isTrue()) {
            wsspEdterror.set(SPACES);
            return ;
        }
        invalidComp.setTrue();
        for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix,12)
        || validComp.isTrue()); wsaaT5679Ix.add(1)){
            if (isEQ(inczpfRec.getPstatcode(),t5679rec.covPremStat[wsaaT5679Ix.toInt()])) {
                validComp.setTrue();
            }
        }
        if (!validComp.isTrue()) {
            wsspEdterror.set(SPACES);
            return ;
        }
	}

protected Chdrpf readChdr2700()
	{
        Chdrpf chdrlifIO = null;
        if (chdrlifMap != null && chdrlifMap.containsKey(inczpfRec.getChdrnum())) {
            for (Chdrpf c : chdrlifMap.get(inczpfRec.getChdrnum())) {
                if (c.getChdrcoy().toString().equals(inczpfRec.getChdrcoy())) {
                    chdrlifIO = c;
                    break;
                }
            }
        }
        if (chdrlifIO == null) {
            syserrrec.params.set(wsaaChdrnum.toString());
            fatalError600();
        } //IJTI-462 START
        else {
	        /* Set up next transaction number, last contract number and*/
	        /* initialise  required  fields, including  the  commission    n*/
	        /* calculation linkage.*/
	        compute(wsaaTranno, 0).set(add(chdrlifIO.getTranno(),1));
	        wsaaChdrnum.set(inczpfRec.getChdrnum());
	        wsaaNextCovnoR.set(ZERO);
	        wsaaNextRidnoR.set(ZERO);
	        wsaaInstprem.set(ZERO);
	        acomcalrec.acomcalRec.set(SPACES);
	        acomcalrec.chdrCurrfrom.set(ZERO);
	        acomcalrec.chdrOccdate.set(ZERO);
	        acomcalrec.covrAnnprem.set(ZERO);
	        acomcalrec.covrPlanSuffix.set(ZERO);
	        acomcalrec.covtAnnprem.set(ZERO);
	        acomcalrec.covtPlanSuffix.set(ZERO);
	        acomcalrec.fpcoCurrto.set(ZERO);
	        acomcalrec.fpcoTargPrem.set(ZERO);
	        acomcalrec.payrBtdate.set(ZERO);
	        acomcalrec.payrPtdate.set(ZERO);
	        acomcalrec.tranno.set(ZERO);
	        acomcalrec.transactionDate.set(ZERO);
	        acomcalrec.transactionTime.set(ZERO);
	        acomcalrec.user.set(ZERO);
        } //IJTI-462 END
        return chdrlifIO;
	}

protected void checkContractStatuz2750(Chdrpf chdrlifIO)
	{
        invalidContract.setTrue();
        for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix,12)
        || validContract.isTrue()); wsaaT5679Ix.add(1)){
            if (isEQ(chdrlifIO.getStatcode(),t5679rec.cnRiskStat[wsaaT5679Ix.toInt()])) {
                validContract.setTrue();
            }
        }
        if (!validContract.isTrue()) {
            wsspEdterror.set(SPACES);
            return ;
        }
        invalidContract.setTrue();
        for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix,12)
        || validContract.isTrue()); wsaaT5679Ix.add(1)){
            if (isEQ(chdrlifIO.getPstcde(),t5679rec.cnPremStat[wsaaT5679Ix.toInt()])) {
                validContract.setTrue();
            }
        }
        if(prmhldtrad && !validContract.isTrue()) {//ILIFE-8509
			validatePhContract(chdrlifIO);
        }
	}

protected void validatePhContract(Chdrpf chdrlifIO) {
	if(ta524Map.get(chdrlifIO.getCnttype()) != null) {
		prmhpf = prmhpfDAO.getPrmhRecord(chdrlifIO.getChdrcoy().toString(), chdrlifIO.getChdrnum());
		if(prmhpf != null && bsscIO.getEffectiveDate().toInt() >= prmhpf.getFrmdate()) {
			validContract.setTrue();
		}
	}
}

protected void softlock2760()
	{
        /* Soft lock the contract.*/
        sftlockrec.statuz.set(SPACES);
        sftlockrec.sftlockRec.set(SPACES);
        sftlockrec.function.set("LOCK");
        sftlockrec.company.set(bsprIO.getCompany());
        sftlockrec.enttyp.set("CH");
        sftlockrec.entity.set(inczpfRec.getChdrnum());
        sftlockrec.transaction.set(bprdIO.getAuthCode());
        sftlockrec.user.set(subString(bsscIO.getDatimeInit(), 21, 6));
        callProgram(Sftlock.class, sftlockrec.sftlockRec);
        if (isNE(sftlockrec.statuz,varcom.oK)
        && isNE(sftlockrec.statuz,"LOCK")) {
            syserrrec.params.set(sftlockrec.sftlockRec);
            syserrrec.statuz.set(sftlockrec.statuz);
            fatalError600();
        }
        /* Finally, set the contract-lock flag if the contract*/
        /* is locked. This flag is used to ensure that all input*/
        /* for this contract is NOT processed.*/
        if (isEQ(sftlockrec.statuz,"LOCK")) {
            ct05Value++;
            callContot001();
            conlogrec.error.set("LOCK");
            StringUtil stringVariable1 = new StringUtil();
            stringVariable1.addExpression(inczpfRec.getChdrcoy());
            stringVariable1.addExpression("|");
            stringVariable1.addExpression(inczpfRec.getChdrnum());
            stringVariable1.addExpression("|");
            stringVariable1.addExpression(inczpfRec.getLife());
            stringVariable1.addExpression("|");
            stringVariable1.addExpression(inczpfRec.getCoverage());
            stringVariable1.addExpression("|");
            stringVariable1.addExpression(inczpfRec.getRider());
            stringVariable1.addExpression("|");
            stringVariable1.addExpression(inczpfRec.getCrtable());
            stringVariable1.setStringInto(conlogrec.params);
            callConlog003();
        }
        else {
            contractNotLocked.setTrue();
        }
	}

protected void update3000()
	{
		/*START*/
		/* Increase the instalment premium by the difference between*/
		/* the new and the last instalments.*/
		reinstflag = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "CSLRI003", appVars, "IT");
		if(reinstflag){
			markprorated();
		}
        compute(wsaaInstprem, 2).set(sub(add(wsaaInstprem,inczpfRec.getNewinst()),inczpfRec.getLastInst()));
        compute(wsaaZbinstprem, 2).set(sub(add(wsaaZbinstprem,inczpfRec.getZbnewinst()),inczpfRec.getZblastinst()));
        compute(wsaaZlinstprem, 2).set(sub(add(wsaaZlinstprem,inczpfRec.getZlnewinst()),inczpfRec.getZllastinst()));
		Incrpf i = updateIncr3100();
		searchT66583150();
		if(isFoundPro && (isNE(wsaaProCpiDate,inczpfRec.getCrrcd()))){
			return;
		}
		else{
			if(isFoundPro){
			
			 while(isLT(wsaaNewCpiDate,bsscIO.getEffectiveDate())){
				 wsaaNewCpiDate.set(getChdrTranno(wsaaNewCpiDate.toInt()));
				 compute(wsaaTranno, 0).set(add(wsaaTranno,1));
			  }
		}
		Covrpf c = t6658Test3200(i);
		a100ReadT5687();
		commissionProcess3300(c);
		statistics3400();
		}
		/*EXIT*/
	}

protected void markprorated(){
	
   	ptrnrecords = ptrnpfDAO.searchPtrnenqRecord(inczpfRec.getChdrcoy(), inczpfRec.getChdrnum());
   	if(ptrnrecords != null){
   		for(Ptrnpf ptrn : ptrnrecords ){
   		if(isNE(ptrn.getBatctrcde(),ta85) && isNE(ptrn.getBatctrcde(),"B524")){
   			continue;
   		}
   		if(isEQ(ptrn.getBatctrcde(),"B524")){
   			break;
   		}
   		if(isEQ(ptrn.getBatctrcde(),ta85)){
   			isFoundPro = true;
   			ptrnpfReinstate = ptrnpfDAO.getPtrnData(inczpfRec.getChdrcoy(), inczpfRec.getChdrnum(), ta85);
   			calcProCpi();
   		}
   	}
  }
}

protected void calcProCpi(){
	
	incrpf = incrpfDAO.getIncrByCrrcdDate(inczpfRec.getChdrcoy(), inczpfRec.getChdrnum(), inczpfRec.getLife(), inczpfRec.getCoverage(), inczpfRec.getRider(), inczpfRec.getPlanSuffix(), bsscIO.getEffectiveDate().toInt(),"1");/* IJTI-1523 */
	if(incrpf != null){
		wsaaProCpiDate.set(incrpf.getCrrcd());	
	}
	if(isEQ(wsaaLastCpiDate,ZERO)){
		wsaaLastCpiDate.set(chdrlifRec.getBillcd());	
		wsaaNewCpiDate.set(chdrlifRec.getBillcd());	
		 chdrTranno.set(wsaaTranno);
	}
}

protected int getChdrTranno(int date){
	
	datcon2rec.datcon2Rec.set(SPACES);
	datcon2rec.intDate1.set(date); 
	datcon2rec.frequency.set("01");
	datcon2rec.freqFactor.set(wsaaT6658ArrayInner.wsaaT6658Billfreq[wsaaT6658Ix.toInt()]);
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz,varcom.oK)) {
		syserrrec.statuz.set(datcon2rec.statuz);
		syserrrec.params.set(datcon2rec.datcon2Rec);
		fatalError600();
	}
    return datcon2rec.intDate2.toInt();	
}

protected Incrpf updateIncr3100()
 {
        Incrpf incrIO = null;
        if (incrMap != null && incrMap.containsKey(inczpfRec.getChdrnum())) {
            for (Incrpf i : incrMap.get(inczpfRec.getChdrnum())) {
                if (inczpfRec.getChdrcoy().equals(i.getChdrcoy()) && inczpfRec.getLife().equals(i.getLife())
                        && inczpfRec.getCoverage().equals(i.getCoverage()) && inczpfRec.getRider().equals(i.getRider())
                        && inczpfRec.getPlanSuffix() == i.getPlnsfx()
                        && i.getValidflag().equals("1")) {
                	if(isFoundPro){
                		if(isEQ(inczpfRec.getCrrcd(),i.getCrrcd())){
                			incrIO = i;
                			break;
                		}
                	}
                	else{
                		incrIO = i;
                		break;
                	}
                }
            }
        }
        if (incrIO == null) {
            syserrrec.params.set(inczpfRec.getChdrnum());
            fatalError600();
        } //IJTI-462 START
        else {
	        if (updateIncrList == null) {
	            updateIncrList = new ArrayList<>();
	        }
	
	        incrIO.setValidflag("2");
	        updateIncrList.add(incrIO);
	        ct02Value++;
        } //IJTI-462 END
        return incrIO;
    }

protected void searchT66583150()
 {
        /* Check the from and to date on T6658 comply with the */
        /* contract risk commencement date. */
        for (wsaaT6658Ix.set(1); !(isGT(wsaaT6658Ix, wsaaT6658IxMax) || (isEQ(
                wsaaT6658ArrayInner.wsaaT6658Annvry[wsaaT6658Ix.toInt()], inczpfRec.getAnniversaryMethod())
                && isGTE(inczpfRec.getCrrcd(), wsaaT6658ArrayInner.wsaaT6658Itmfrm[wsaaT6658Ix.toInt()]) && isLTE(
                    inczpfRec.getCrrcd(), wsaaT6658ArrayInner.wsaaT6658Itmto[wsaaT6658Ix.toInt()]))); wsaaT6658Ix
                .add(1)) {
            /* CONTINUE_STMT */
        }
        if (isGT(wsaaT6658Ix, wsaaT6658IxMax)) {
            syserrrec.statuz.set(h036);
            itdmIO.setParams(SPACES);
            itdmIO.setFunction(varcom.begn);
            itdmIO.setStatuz(varcom.endp);
            itdmIO.setItemcoy(bsprIO.getCompany());
            itdmIO.setItemtabl(t6658);
            itdmIO.setItemitem(inczpfRec.getAnniversaryMethod());
            itdmIO.setItmfrm(inczpfRec.getCrrcd());
            syserrrec.params.set(itdmIO.getParams());
            fatalError600();
        }
    }

protected Covrpf t6658Test3200(Incrpf incrIO)
	{
		/*START*/
		/* Part of the updating in this section is performed for both*/
		/* updating existing components and adding new components.*/
        Covrpf insertCovr = null;
		if (isNE(wsaaT6658ArrayInner.wsaaT6658Addnew[wsaaT6658Ix.toInt()], SPACES)
		|| isNE(wsaaT6658ArrayInner.wsaaT6658Addexist[wsaaT6658Ix.toInt()], SPACES)) {
			insertCovr = commonProcessing3210(incrIO);
			if (isNE(wsaaT6658ArrayInner.wsaaT6658Addexist[wsaaT6658Ix.toInt()], SPACES)) {
				searchT54473800();
				checkReass3220(incrIO, insertCovr);
			}
			else {
			    insertCovr = addCovr3230(insertCovr, incrIO);
			}
			genericProcessing3700(insertCovr);
		}
		return insertCovr;
		/*EXIT*/
	}

protected Covrpf commonProcessing3210(Incrpf incrIO)
	{
        Covrpf covrIO = null;
        if(covrMap != null && covrMap.containsKey(inczpfRec.getChdrnum())){
            for(Covrpf i:covrMap.get(inczpfRec.getChdrnum())){
                if (inczpfRec.getChdrcoy().equals(i.getChdrcoy()) && inczpfRec.getLife().equals(i.getLife())
                        && inczpfRec.getCoverage().equals(i.getCoverage()) && inczpfRec.getRider().equals(i.getRider())
                        && inczpfRec.getPlanSuffix() == i.getPlanSuffix()) {
                	if(isFoundPro){	
                			covrIO = i;
                			break;
                	}
                	else{
                		covrIO = i;
                		break;
                	}
                }
            }
        }
        if (covrIO == null) {
            syserrrec.params.set(inczpfRec.getChdrnum());
            fatalError600();
        } //IJTI-462 START
        else {
	        /*  Update the COVR record with a valid flag of 2 and set*/
	        /*  'COVR-CURRTO' date to the effective date of the Increase.*/
	        if (isNE(wsaaT6658ArrayInner.wsaaT6658Addexist[wsaaT6658Ix.toInt()], SPACES)) {
	            rasaltrrec.oldSumins.set(covrIO.getSumins());
	        }
	        
	        if (updateCovrList == null) {
	            updateCovrList = new ArrayList<>();
	        }
	        
	        covrIO.setValidflag("2");
	        if(isFoundPro){	
	        	 covrIO.setCurrto(wsaaProCpiDate.toInt());	
	        }
	        else{
	        covrIO.setCurrto(covrIO.getCpiDate());
	        }
	        updateCovrList.add(covrIO);
	
	        Covrpf insertCovr= new Covrpf(covrIO);
	        insertCovr.setValidflag("1");
	        insertCovr.setTranno(wsaaTranno.toInt());//ILIFE-8472
	        if(isFoundPro){	
	        	insertCovr.setCurrfrom(wsaaProCpiDate.toInt());	
	        }
	        else{
	        insertCovr.setCurrfrom(covrIO.getCpiDate());
	        }
	        insertCovr.setCurrto(varcom.vrcmMaxDate.toInt());
	        insertCovr.setTransactionDate(wsaaTransDate.toInt());
	        insertCovr.setTransactionTime(wsaaTransTime.toInt());
	        insertCovr.setUser(subString(bsscIO.getDatimeInit(), 21, 6).toInt());
	        initialize(varcom.vrcmTranid);
	        insertCovr.setTermid(varcom.vrcmTermid.toString());
	        insertCovr.setIndexationInd(SPACES.toString());
	        if(isFoundPro){	
	        	wsaaOldCpiDate.set(wsaaProCpiDate.toInt());	
	        }
	        else{
	        wsaaOldCpiDate.set(insertCovr.getCpiDate());
	        }
	        calcNextCpiDate3211(insertCovr);
	        getLifergpDetails7000(insertCovr);
	        trmToCessDate3214(insertCovr);
	        calcIncrProcess3216(insertCovr);
	        if (isNE(wsaaT6658ArrayInner.wsaaT6658Addexist[wsaaT6658Ix.toInt()], SPACES)) {
	            insertCovr.setInstprem(incrIO.getNewinst());
	            insertCovr.setZbinstprem(incrIO.getZbnewinst());
	            insertCovr.setZlinstprem(incrIO.getZlnewinst());
	            insertCovr.setSumins(incrIO.getNewsum());
	        }
	        
	        
	      //ILIFE-7845 
	        premiumrec.riskPrem.set(BigDecimal.ZERO);
	        insertCovr.setRiskprem(BigDecimal.ZERO);
	        riskPremflag = FeaConfg.isFeatureExist(chdrlifRec.getChdrcoy().toString().trim(), RISKPREM_FEATURE_ID, appVars, "IT");
			if(riskPremflag)
			{
				premiumrec.riskPrem.set(ZERO);
				premiumrec.cnttype.set(chdrlifRec.getCnttype());
				premiumrec.crtable.set(covrIO.getCrtable());
				premiumrec.calcTotPrem.set(incrIO.getNewinst());
				callProgram("RISKPREMIUM", premiumrec.premiumRec);
				insertCovr.setRiskprem(premiumrec.riskPrem.getbigdata()); //ILIFE-7845
			}
			 
			//ILIFE-7845  end
			insertCovr.setZclstate(SPACE);
			insertCovr.setZstpduty01(BigDecimal.ZERO);
			if(stampDutyflag) {
				insertCovr.setZstpduty01(incrIO.getZstpduty01());
				insertCovr.setZclstate(covrIO.getZclstate());
			}
			if(prmhldtrad && "Y".equals(covrIO.getReinstated()))//ILIFE-8509
				insertCovr.setReinstated(SPACE);
	        if (insertCovrList == null) {
	            insertCovrList = new ArrayList<>();
	        }
	        insertCovrList.add(insertCovr);
	        if("Y".equals(covrIO.getReinstated()))//ILIFE-8509
	        	lapseReinstated = true;
	        return insertCovr;
        } //IJTI-462 END
        return null;
	}

protected void calcNextCpiDate3211(Covrpf insertCovr)
	{
		/*CALC*/
		initialize(datcon2rec.datcon2Rec);
		if(isFoundPro){
			datcon2rec.intDate1.set(wsaaProCpiDate);
		}
		else{
			datcon2rec.intDate1.set(insertCovr.getCpiDate());
		}
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(wsaaT6658ArrayInner.wsaaT6658Billfreq[wsaaT6658Ix.toInt()]);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		insertCovr.setCpiDate(datcon2rec.intDate2.toInt());
		/*EXIT*/
	}

protected void trmToCessDate3214(Covrpf insertCovr)
	{
        initialize(datcon3rec.datcon3Rec);
        datcon3rec.intDate1.set(insertCovr.getCpiDate());
        datcon3rec.intDate2.set(insertCovr.getRiskCessDate());
        datcon3rec.frequency.set("01");
        callProgram(Datcon3.class, datcon3rec.datcon3Rec);
        if (isNE(datcon3rec.statuz,varcom.oK)) {
            syserrrec.params.set(datcon3rec.datcon3Rec);
            syserrrec.statuz.set(datcon3rec.statuz);
            fatalError600();
        }
        datcon3rec.freqFactor.add(0.99999);
        wsaaMinTrmToCess.set(datcon3rec.freqFactor);
	}

protected void calcIncrProcess3216(Covrpf covrIO)
	{
        initialize(datcon3rec.datcon3Rec);
        datcon3rec.intDate1.set(covrIO.getCrrcd());
        datcon3rec.intDate2.set(covrIO.getCpiDate());
        datcon3rec.frequency.set("01");
        callProgram(Datcon3.class, datcon3rec.datcon3Rec);
        if (isNE(datcon3rec.statuz,varcom.oK)) {
            syserrrec.params.set(datcon3rec.datcon3Rec);
            syserrrec.statuz.set(datcon3rec.statuz);
            fatalError600();
        }
        datcon3rec.freqFactor.add(0.99999);
        wsaaFixNoIncr.set(datcon3rec.freqFactor);
        wsaaBillfreq.set(wsaaT6658ArrayInner.wsaaT6658Billfreq[wsaaT6658Ix.toInt()]);
        compute(wsaaFixNoIncr, 1).setRounded(mult(wsaaFixNoIncr,wsaaBillfreq));
        /* Set the maximum age and fixed number of increases to maximum*/
        /* values if they are not set on the table.*/
        if (isLTE(wsaaT6658ArrayInner.wsaaT6658MaxAge[wsaaT6658Ix.toInt()], 0)) {
            /*        MOVE 99                 TO WSAA-T6658-MAX-AGE            */
            wsaaT6658ArrayInner.wsaaT6658MaxAge[wsaaT6658Ix.toInt()].set(999);
        }
        if (isLTE(wsaaT6658ArrayInner.wsaaT6658Fixdtrm[wsaaT6658Ix.toInt()], 0)) {
            /*        MOVE 99                 TO WSAA-T6658-FIXDTRM            */
            wsaaT6658ArrayInner.wsaaT6658Fixdtrm[wsaaT6658Ix.toInt()].set(999);
        }
        /* If the component is no longer eligible for increases for the*/
        /* following reasons, according to the values on T6658:*/
        /* - it has less than the minimum term left to run*/
        /* - it has already processed the fixed number of increases*/
        /* - the life assured has passed the maximum age for increases*/
        /* write out a message to indicate this and set the next*/
        /* increase date to 9s to prevent it from being processed*/
        /* again.*/
        if (isGT(wsaaT6658ArrayInner.wsaaT6658Minctrm[wsaaT6658Ix.toInt()], wsaaMinTrmToCess)
        || isLT(wsaaT6658ArrayInner.wsaaT6658Fixdtrm[wsaaT6658Ix.toInt()], wsaaFixNoIncr)
        || isLT(wsaaT6658ArrayInner.wsaaT6658MaxAge[wsaaT6658Ix.toInt()], wsaaAnb)
        || isGT(covrIO.getCpiDate(),covrIO.getRiskCessDate())) {
            StringUtil stringVariable1 = new StringUtil();
            stringVariable1.addExpression(inczpfRec.getChdrnum());
            stringVariable1.addExpression("|");
            stringVariable1.addExpression(inczpfRec.getLife());
            stringVariable1.addExpression("|");
            stringVariable1.addExpression(inczpfRec.getCoverage());
            stringVariable1.addExpression("|");
            stringVariable1.addExpression(inczpfRec.getRider());
            stringVariable1.setStringInto(conlogrec.message);
            conlogrec.error.set(e514);
            callConlog003();
            covrIO.setCpiDate(99999999);
        }
	}

protected void checkReass3220(Incrpf incrIO, Covrpf covr)
	{
        if (processReassurance.isTrue()) {
            rasaltrrec.newSumins.set(incrIO.getNewsum());
            rasaltrrec.addNewCovr.set("N");
            readLife3250();
            processReassurance5000(covr);
        }
        acomcalrec.covtLife.set(inczpfRec.getLife());
        acomcalrec.covtCoverage.set(inczpfRec.getCoverage());
        acomcalrec.covtRider.set(inczpfRec.getRider());
        acomcalrec.covtPlanSuffix.set(inczpfRec.getPlanSuffix());
        ct06Value++;
	}

protected Covrpf addCovr3230(Covrpf origCovrIO, Incrpf incrIO)
	{
        Covrpf covrIO = new Covrpf(origCovrIO);
        covrIO.setValidflag("1");
        covrIO.setTranno(wsaaTranno.toInt());
        covrIO.setCurrfrom(wsaaOldCpiDate.toInt());
        covrIO.setCurrto(varcom.vrcmMaxDate.toInt());
        covrIO.setTransactionDate(wsaaTransDate.toInt());
        covrIO.setTransactionTime(wsaaTransTime.toInt());
        covrIO.setUser(subString(bsscIO.getDatimeInit(), 21, 6).toInt());
        initialize(varcom.vrcmTranid);
        covrIO.setTermid(varcom.vrcmTermid.toString());
        covrIO.setIndexationInd(SPACES.toString());
        covrIO.setCpiDate(99999999);
        covrIO.setCrrcd(inczpfRec.getCrrcd());
        covrIO.setInstprem(sub(incrIO.getNewinst(),incrIO.getLastInst()).getbigdata());
        covrIO.setZbinstprem(sub(incrIO.getZbnewinst(),incrIO.getZblastinst()).getbigdata());
        covrIO.setZlinstprem(sub(incrIO.getZlnewinst(),incrIO.getZllastinst()).getbigdata());
        covrIO.setSumins(sub(incrIO.getNewsum(),incrIO.getLastSum()).getbigdata());
        nextComponent6000();
        wsaaCount.set(ZERO);
        wsaaStoreRider.set(inczpfRec.getRider());
        wsaaStorePlnsfx.set(inczpfRec.getPlanSuffix());
        wsaaNextCovnoR.add(1);
        covrIO.setCoverage(wsaaNextCovno.toString());
        if (isNE(inczpfRec.getRider(),wsaaStoreRider)) {
            if (isEQ(inczpfRec.getPlanSuffix(),wsaaStorePlnsfx)) {
                wsaaCount.add(1);
            }
        }
        wsaaNextRidnoR.set(wsaaCount);
        covrIO.setRider(wsaaNextRidno.toString());
        covrIO.setAnbAtCcd(sub(wsaaAnb,1).toInt());
        
        if (insertNewCovrList == null) {
            insertNewCovrList = new ArrayList<>();
        }
        insertNewCovrList.add(covrIO);
        searchT54473800();
        if (processReassurance.isTrue()) {
            rasaltrrec.newSumins.set(incrIO.getNewsum());
            rasaltrrec.oldSumins.set(incrIO.getLastSum());
            rasaltrrec.addNewCovr.set("Y");
            readLife3250();
            processReassurance5000(covrIO);
        }
        acomcalrec.covtLife.set(inczpfRec.getLife());
        acomcalrec.covtCoverage.set(wsaaNextCovnoR);
        acomcalrec.covtRider.set(wsaaNextRidnoR);
        acomcalrec.covtPlanSuffix.set(inczpfRec.getPlanSuffix());
        ct07Value++;
        return covrIO;
	}

protected void readLife3250()
 {
        boolean foundFlag = false;
        if (lifelnbMap != null && lifelnbMap.containsKey(chdrlifRec.getChdrnum())) {
            for (Lifepf l : lifelnbMap.get(chdrlifRec.getChdrnum())) {
                if (l.getChdrcoy().equals(String.valueOf(chdrlifRec.getChdrcoy())) && l.getLife().equals(inczpfRec.getLife())//ILIFE-6658
                        && "00".equals(l.getJlife())) {
                    wsaaL1Clntnum.set(l.getLifcnum());
                    foundFlag = true;
                    break;
                }
            }
            if (!foundFlag) {
                syserrrec.params.set(chdrlifRec.getChdrnum());
                fatalError600();
            }
            wsaaL2Clntnum.set(SPACES);
            for (Lifepf l : lifelnbMap.get(chdrlifRec.getChdrnum())) {
                if (l.getChdrcoy().equals(String.valueOf(chdrlifRec.getChdrcoy())) && l.getLife().equals(inczpfRec.getLife())//ILIFE-6658
                        && "01".equals(l.getJlife())) {
                    wsaaL2Clntnum.set(l.getLifcnum());
                    break;
                }
            }
        }
    }

protected void commissionProcess3300(Covrpf covrIO)
 {
        /* If no commission is required, leave the section. */
        if (isEQ(wsaaT6658ArrayInner.wsaaT6658Comind[wsaaT6658Ix.toInt()], SPACES)) {
            return;
        }
        /* Read the PAYR details here as some fields are needed in this */
        /* section. */
        Payrpf payrIO = readPayrDetails3350();
        /* Check if contract or component level accounting is required. */
        for (wsaaT5688Ix.set(1); !(isGT(wsaaT5688Ix, wsaaT5688IxMax) || (isEQ(wsaaT5688Cnttype[wsaaT5688Ix.toInt()],
                chdrlifRec.getCnttype()) && isGTE(chdrlifRec.getOccdate(), wsaaT5688Itmfrm[wsaaT5688Ix.toInt()]) && isLTE(
                    chdrlifRec.getOccdate(), wsaaT5688Itmto[wsaaT5688Ix.toInt()]))); wsaaT5688Ix.add(1)) {
            /* CONTINUE_STMT */
        }
        if (isGT(wsaaT5688Ix, wsaaT5688IxMax)) {
            syserrrec.statuz.set(e308);
            itdmIO.setParams(SPACES);
            itdmIO.setFunction(varcom.begn);
            itdmIO.setStatuz(varcom.endp);
            itdmIO.setItemcoy(bsprIO.getCompany());
            itdmIO.setItemtabl(t5688);
            itdmIO.setItemitem(chdrlifRec.getCnttype());
            itdmIO.setItmfrm(chdrlifRec.getOccdate());
            syserrrec.params.set(itdmIO.getParams());
            fatalError600();
        }
        /* Create a PCDT record for the Servicing Agent Number on the , */
        /* CHDR and then call ACOMCALC subroutine to revalidate all */
        /* Commission records. The PCDT record will later be deleted. */
        pcdtmjaIO.setRecKeyData(SPACES);
        pcdtmjaIO.setRecNonKeyData(SPACES);
        pcdtmjaIO.setChdrcoy(inczpfRec.getChdrcoy());
        pcdtmjaIO.setChdrnum(inczpfRec.getChdrnum());
        pcdtmjaIO.setPlanSuffix(inczpfRec.getPlanSuffix());
        pcdtmjaIO.setLife(inczpfRec.getLife());
        pcdtmjaIO.setCoverage(acomcalrec.covtCoverage);
        pcdtmjaIO.setRider(acomcalrec.covtRider);
        pcdtmjaIO.setTranno(wsaaTranno);
        pcdtmjaIO.setAgntnum(1, chdrlifRec.getAgntnum());
        pcdtmjaIO.setSplitc(1, 100);
        pcdtmjaIO.setFunction(varcom.writr);
        pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
        SmartFileCode.execute(appVars, pcdtmjaIO);
        if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)) {
            syserrrec.statuz.set(pcdtmjaIO.getStatuz());
            syserrrec.params.set(pcdtmjaIO.getParams());
            fatalError600();
        }
        ct08Value++;
        acomcalrec.statuz.set(varcom.oK);
        acomcalrec.company.set(inczpfRec.getChdrcoy());
        acomcalrec.chdrnum.set(inczpfRec.getChdrnum());
        acomcalrec.covrLife.set(inczpfRec.getLife());
        acomcalrec.covrCoverage.set(inczpfRec.getCoverage());
        acomcalrec.covrRider.set(inczpfRec.getRider());
        acomcalrec.covrPlanSuffix.set(inczpfRec.getPlanSuffix());
        acomcalrec.tranno.set(wsaaTranno);
        acomcalrec.transactionDate.set(wsaaTransDate);
        acomcalrec.transactionTime.set(wsaaTransTime);
        acomcalrec.user.set(subString(bsscIO.getDatimeInit(), 21, 6));
        varcom.vrcmTranid.set(SPACES);
        acomcalrec.termid.set(varcom.vrcmTermid);
        acomcalrec.payrBtdate.set(payrIO.getBtdate());
        /* The PAYR-PTDATE is not passed in the linkage, instead the */
        /* CRRCD of the Increase records is passed, because this */
        /* date is used in ACOMCALC for the Effective Date of the AGCM. */
        acomcalrec.payrPtdate.set(inczpfRec.getCrrcd());
        acomcalrec.payrBillfreq.set(payrIO.getBillfreq());
        wsaaBillfreq.set(payrIO.getBillfreq());
        acomcalrec.payrCntcurr.set(payrIO.getCntcurr());
        acomcalrec.cnttype.set(chdrlifRec.getCnttype());
        acomcalrec.chdrOccdate.set(chdrlifRec.getOccdate());
        acomcalrec.chdrCurrfrom.set(chdrlifRec.getCurrfrom());
        acomcalrec.chdrAgntcoy.set(inczpfRec.getChdrcoy());
        acomcalrec.covrJlife.set(covrIO.getJlife());
        acomcalrec.covrCrtable.set(covrIO.getCrtable());
        acomcalrec.comlvlacc.set(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()]);
        acomcalrec.basicCommMeth.set(inczpfRec.getBasicCommMeth());
        acomcalrec.bascpy.set(inczpfRec.getBascpy());
        acomcalrec.rnwcpy.set(inczpfRec.getRnwcpy());
        acomcalrec.srvcpy.set(inczpfRec.getSrvcpy());
        /* COMPUTE ACOM-COVR-ANNPREM ROUNDED = LASTINST */
        /* * WSAA-BILLFREQ. */
        /* COMPUTE ACOM-COVT-ANNPREM ROUNDED = NEWINST */
        /* * WSAA-BILLFREQ. */
        if (isNE(t6658rec.addnew, SPACES)) {
            acomcalrec.covrAnnprem.set(ZERO);
        } else {
            if (isNE(t5687rec.zrrcombas, SPACES)) {
                compute(acomcalrec.covrAnnprem, 2).set(mult(inczpfRec.getZblastinst(), wsaaBillfreq));
            } else {
                compute(acomcalrec.covrAnnprem, 2).set(mult(inczpfRec.getLastInst(), wsaaBillfreq));
            }
        }
        if (isNE(t6658rec.addnew, SPACES)) {
            if (isNE(t5687rec.zrrcombas, SPACES)) {
                compute(acomcalrec.covtAnnprem, 2).set(
                        sub((mult(inczpfRec.getZbnewinst(), wsaaBillfreq)),
                                (mult(inczpfRec.getZblastinst(), wsaaBillfreq))));
            } else {
                compute(acomcalrec.covtAnnprem, 2)
                        .set(sub((mult(inczpfRec.getNewinst(), wsaaBillfreq)),
                                (mult(inczpfRec.getLastInst(), wsaaBillfreq))));
            }
        } else {
            if (isNE(t5687rec.zrrcombas, SPACES)) {
                compute(acomcalrec.covtAnnprem, 2).set(mult(inczpfRec.getZbnewinst(), wsaaBillfreq));
            } else {
                compute(acomcalrec.covtAnnprem, 2).set(mult(inczpfRec.getNewinst(), wsaaBillfreq));
            }
        }
        acomcalrec.batctrcde.set(batcdorrec.trcde);
        acomcalrec.atmdLanguage.set(bsscIO.getLanguage());
        acomcalrec.atmdBatchKey.set(batcdorrec.batchkey);
        acomcalrec.fpcoTargPrem.set(0);
        acomcalrec.fpcoCurrto.set(0);
        callProgram(Acomcalc.class, acomcalrec.acomcalRec);
        if (isNE(acomcalrec.statuz, varcom.oK)) {
            syserrrec.statuz.set(acomcalrec.statuz);
            syserrrec.params.set(acomcalrec.acomcalRec);
            fatalError600();
        }
        pcdtmjaIO.setFunction(varcom.delet);
        SmartFileCode.execute(appVars, pcdtmjaIO);
        if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)) {
            syserrrec.statuz.set(pcdtmjaIO.getStatuz());
            syserrrec.params.set(pcdtmjaIO.getParams());
            fatalError600();
        }
    }

protected Payrpf readPayrDetails3350()
	{
        Payrpf payrIO = null;
        if (payrMap != null && payrMap.containsKey(wsaaChdrnum.toString())) {
            for (Payrpf p : payrMap.get(inczpfRec.getChdrnum())) {
                if (inczpfRec.getChdrcoy().equals(p.getChdrcoy()) && 1 == p.getPayrseqno()) {
                    payrIO = p;
                    break;
                }
            }
        }
        if (payrIO == null) {
            syserrrec.params.set(inczpfRec.getChdrnum());
            fatalError600();
        }
        return payrIO;
	}

protected void statistics3400()
	{
        /* If no statistics are required, leave the section.*/
        if (isEQ(wsaaT6658ArrayInner.wsaaT6658Statind[wsaaT6658Ix.toInt()], SPACES)) {
            return ;
        }
        lifsttrrec.batccoy.set(batcdorrec.company);
        lifsttrrec.batcbrn.set(batcdorrec.branch);
        lifsttrrec.batcactyr.set(batcdorrec.actyear);
        lifsttrrec.batcactmn.set(batcdorrec.actmonth);
        lifsttrrec.batcbatch.set(batcdorrec.batch);
        lifsttrrec.batctrcde.set(batcdorrec.trcde);
        lifsttrrec.chdrcoy.set(inczpfRec.getChdrcoy());
        lifsttrrec.chdrnum.set(inczpfRec.getChdrnum());
        lifsttrrec.tranno.set(wsaaTranno);
        lifsttrrec.trannor.set(99999);
        lifsttrrec.agntnum.set(SPACES);
        lifsttrrec.oldAgntnum.set(SPACES);
        callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
        if (isNE(lifsttrrec.statuz,varcom.oK)) {
            syserrrec.statuz.set(lifsttrrec.statuz);
            syserrrec.params.set(lifsttrrec.lifsttrRec);
            fatalError600();
        }
	}

protected void commit3500()
 {
        if (updateIncrList != null && !updateIncrList.isEmpty()) {
            incrpfDAO.updateValidFlag(updateIncrList);
            updateIncrList.clear();
        }
        if (updateCovrList != null && !updateCovrList.isEmpty()) {
            covrpfDAO.updateCovrRecord(updateCovrList, 0);
            updateCovrList.clear();
        }
        List<Covrpf> insertAllCovrpfList = new ArrayList<>();
        if (insertCovrList != null && !insertCovrList.isEmpty()) {
            insertAllCovrpfList.addAll(insertCovrList);
            insertCovrList.clear();
        }
        /*      ILIFE-6796   start   if (insertNewCovrList != null && !insertNewCovrList.isEmpty()) {
            insertAllCovrpfList.addAll(insertNewCovrList);
            insertNewCovrList.clear();
        }   ILIFE-6796   end      */
        covrpfDAO.insertCovrBulk(insertAllCovrpfList);
        
        if (insertPtrnList != null && !insertPtrnList.isEmpty()) {
            ptrnpfDAO.insertPtrnPF(insertPtrnList);
            insertPtrnList.clear();
        }
        if (updatePayrList != null && !updatePayrList.isEmpty()) {
            payrpfDAO.updatePayrRecord(updatePayrList);
            updatePayrList.clear();
        }
        if (insertPayrList != null && !insertPayrList.isEmpty()) {
            payrpfDAO.insertPayrpfList(insertPayrList);
            insertPayrList.clear();
        }
        if (insertAinrList != null && !insertAinrList.isEmpty()) {
            ainrpfDAO.insertAinrpfList(insertAinrList);
            insertAinrList.clear();
        }
        if (deleteRacdList != null && !deleteRacdList.isEmpty()) {
            racdpfDAO.deleteRacdRcds(deleteRacdList);
            deleteRacdList.clear();
        }
        if (updateChdrlifList != null && !updateChdrlifList.isEmpty()) {
            chdrpfDAO.updateChdrRecord(updateChdrlifList, 0);
            updateChdrlifList.clear();
        }
        if (insertChdrlifList != null && !insertChdrlifList.isEmpty()) {
            chdrpfDAO.insertChdrAmt(insertChdrlifList);
            insertChdrlifList.clear();
        }
        commitControlTotals();
    }

private void commitControlTotals() {
    // ct01
    contotrec.totno.set(1);
    contotrec.totval.set(ct01Value);
    callContot001();
    ct01Value = 0;

    // ct02
    contotrec.totno.set(2);
    contotrec.totval.set(ct02Value);
    callContot001();
    ct02Value = 0;

    // ct03
    contotrec.totno.set(3);
    contotrec.totval.set(ct03Value);
    callContot001();
    ct03Value = 0;

    // ct04
    contotrec.totno.set(4);
    contotrec.totval.set(ct04Value);
    callContot001();
    ct04Value = 0;

    // ct05
    contotrec.totno.set(5);
    contotrec.totval.set(ct05Value);
    callContot001();
    ct05Value = 0;

    // ct06
    contotrec.totno.set(6);
    contotrec.totval.set(ct06Value);
    callContot001();
    ct06Value = 0;

    // ct07
    contotrec.totno.set(7);
    contotrec.totval.set(ct07Value);
    callContot001();
    ct07Value = 0;

    // ct08
    contotrec.totno.set(8);
    contotrec.totval.set(ct08Value);
    callContot001();
    ct08Value = 0;

    // ct09
    contotrec.totno.set(9);
    contotrec.totval.set(ct09Value);
    callContot001();
    ct09Value = 0;

    // ct10
    contotrec.totno.set(10);
    contotrec.totval.set(ct10Value);
    callContot001();
    ct10Value = 0;

    // ct11
    contotrec.totno.set(11);
    contotrec.totval.set(ct11Value);
    callContot001();
    ct11Value = 0;
   
}
protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void genericProcessing3700(Covrpf covrIO)
	{
        wsaaSubprogFound = "N";
        for (wsaaT5671Ix.set(1); !(isGT(wsaaT5671Ix,wsaaT5671IxMax)
        || isEQ(wsaaSubprogFound,"Y")); wsaaT5671Ix.add(1)){
            if (isEQ(wsaaT5671Crtable[wsaaT5671Ix.toInt()],inczpfRec.getCrtable())) {
                wsaaSubprog1.set(wsaaT5671Subprog1[wsaaT5671Ix.toInt()]);
                wsaaSubprog2.set(wsaaT5671Subprog2[wsaaT5671Ix.toInt()]);
                wsaaSubprog3.set(wsaaT5671Subprog3[wsaaT5671Ix.toInt()]);
                wsaaSubprog4.set(wsaaT5671Subprog4[wsaaT5671Ix.toInt()]);
                wsaaSubprogFound = "Y";
            }
        }
        if (isNE(wsaaSubprogFound,"Y")) {
            syserrrec.statuz.set(e302);
            itdmIO.setParams(SPACES);
            itdmIO.setFunction(varcom.readr);
            itdmIO.setStatuz(varcom.mrnf);
            itdmIO.setItemcoy(bsprIO.getCompany());
            itdmIO.setItemtabl(t5671);
            StringUtil stringVariable1 = new StringUtil();
            stringVariable1.addExpression(bprdIO.getAuthCode());
            stringVariable1.addExpression(inczpfRec.getCrtable());
            stringVariable1.setStringInto(itemIO.getItemitem());
            syserrrec.params.set(itdmIO.getParams());
            fatalError600();
        }
        /* Set function to non-spaces to prevent generic processing*/
        /* creating UTRNS for U/Linked components.*/
        isuallrec.function.set("ACTIN");
        isuallrec.company.set(chdrlifRec.getChdrcoy());
        isuallrec.chdrnum.set(chdrlifRec.getChdrnum());
        isuallrec.life.set(covrIO.getLife());
        isuallrec.coverage.set(covrIO.getCoverage());
        isuallrec.rider.set(covrIO.getRider());
        isuallrec.planSuffix.set(covrIO.getPlanSuffix());
        isuallrec.oldcovr.set(inczpfRec.getCoverage());
        isuallrec.oldrider.set(inczpfRec.getRider());
        isuallrec.freqFactor.set(1);
        isuallrec.batchkey.set(batcdorrec.batchkey);
        isuallrec.transactionDate.set(wsaaTransDate);
        isuallrec.transactionTime.set(wsaaTransTime);
        isuallrec.user.set(subString(bsscIO.getDatimeInit(), 21, 6));
        varcom.vrcmTranid.set(SPACES);
        isuallrec.termid.set(varcom.vrcmTermid);
        isuallrec.effdate.set(inczpfRec.getCrrcd());
        isuallrec.runDate.set(bsscIO.getEffectiveDate());
        isuallrec.newTranno.set(wsaaTranno);
        isuallrec.covrSingp.set(ZERO);
        isuallrec.covrInstprem.set(covrIO.getInstprem());
        isuallrec.convertUnlt.set(SPACES);
        isuallrec.language.set(bsscIO.getLanguage());
        if (isGT(chdrlifRec.getPolinc(),1)) {
            isuallrec.convertUnlt.set("Y");
        }
        for (wsaaSub1.set(1); !(isGT(wsaaSub1,4)); wsaaSub1.add(1)){
            subCall3750();
        }

	}

protected void subCall3750()
	{
		/*SUB-CALL*/
		isuallrec.statuz.set(varcom.oK);
		if (isNE(wsaaSubprog[wsaaSub1.toInt()],SPACES)) {
			callProgram(wsaaSubprog[wsaaSub1.toInt()], isuallrec.isuallRec);
		}
		if (isNE(isuallrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(isuallrec.statuz);
			syserrrec.params.set(isuallrec.isuallRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void searchT54473800()
	{
		/*START*/
		wsaaT5447Ix.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaT5447Ix,wsaaT5447Rec.length); wsaaT5447Ix.add(1)){
				if (isEQ(wsaaT5447ChdrlifCnttype[wsaaT5447Ix.toInt()],chdrlifRec.getCnttype())) {
					bypassReassurance.setTrue();
					break searchlabel1;
				}
			}
			processReassurance.setTrue();
		}
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		inczpf.close();
		wsaaQcmdexc.set("DLTOVR FILE(INCZPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void processReassurance5000(Covrpf covrIO)
 {
        rasaltrrec.statuz.set(varcom.oK);
        rasaltrrec.function.set("INCR");
        rasaltrrec.chdrcoy.set(chdrlifRec.getChdrcoy());
        rasaltrrec.chdrnum.set(chdrlifRec.getChdrnum());
        rasaltrrec.life.set(covrIO.getLife());
        rasaltrrec.jlife.set(covrIO.getJlife());
        rasaltrrec.coverage.set(covrIO.getCoverage());
        rasaltrrec.rider.set(covrIO.getRider());
        rasaltrrec.planSuffix.set(covrIO.getPlanSuffix());
        rasaltrrec.polsum.set(chdrlifRec.getPolsum());
        rasaltrrec.effdate.set(bsscIO.getEffectiveDate());
        rasaltrrec.batckey.set(batcdorrec.batchkey);
        rasaltrrec.tranno.set(wsaaTranno);
        rasaltrrec.l1Clntnum.set(wsaaL1Clntnum);
        rasaltrrec.l2Clntnum.set(wsaaL2Clntnum);
        rasaltrrec.fsuco.set(bsprIO.getFsuco());
        rasaltrrec.crtable.set(covrIO.getCrtable());
        rasaltrrec.cnttype.set(chdrlifRec.getCnttype());
        rasaltrrec.cntcurr.set(chdrlifRec.getCntcurr());
        rasaltrrec.language.set(bsscIO.getLanguage());
        rasaltrrec.crrcd.set(covrIO.getCrrcd());
        callProgram(Rasaltr.class, rasaltrrec.rasaltrRec);
        if (isNE(rasaltrrec.statuz, varcom.oK) && isNE(rasaltrrec.statuz, "FACL") && isNE(rasaltrrec.statuz, "TRET")) {
            syserrrec.statuz.set(rasaltrrec.statuz);
            syserrrec.params.set(rasaltrrec.rasaltrRec);
            fatalError600();
        }
        callActvres5460(covrIO);
        List<Racdpf> racdpfList = racdpfDAO.searchRacdstRecord(covrIO.getChdrcoy(), covrIO.getChdrnum(), covrIO.getLife(), covrIO.getCoverage(), covrIO.getRider(),
        		covrIO.getPlanSuffix(), "1");
        if (racdpfList != null && !racdpfList.isEmpty()) {
			for (Racdpf r : racdpfList) {
				if (isEQ(r.getRetype(), "F")
						// ILIFE-1296 Starts
						&& isNE(r.getRasnum(), SPACES) && isNE(r.getRngmnt(), SPACES)
				// ILIFE-1296 Ends
				) {
					writeAinr5440(r);
					if (deleteRacdList == null) {
						deleteRacdList = new ArrayList<>();
					}
					deleteRacdList.add(r);
				}
			}
        }
    }

protected void writeAinr5440(Racdpf racdlnbIO)
 {
        Ainrpf ainrIO = new Ainrpf();
        ainrIO.setAintype(bprdIO.getSystemParam03().toString().trim());
        ainrIO.setChdrcoy(racdlnbIO.getChdrcoy());
        ainrIO.setChdrnum(racdlnbIO.getChdrnum());
        ainrIO.setLife(racdlnbIO.getLife());
        ainrIO.setCoverage(racdlnbIO.getCoverage());
        ainrIO.setRider(racdlnbIO.getRider());
        ainrIO.setCrtable(rasaltrrec.crtable.toString());
        ainrIO.setPlanSuffix(racdlnbIO.getPlanSuffix());
        ainrIO.setCmdate(racdlnbIO.getCmdate());
        ainrIO.setRasnum(racdlnbIO.getRasnum());
        ainrIO.setRngmnt(racdlnbIO.getRngmnt());
        ainrIO.setRaAmount(racdlnbIO.getRaAmount());
        if (insertAinrList == null) {
            insertAinrList = new ArrayList<>();
        }
        insertAinrList.add(ainrIO);
        ct09Value++;

    }

protected void callActvres5460(Covrpf covrIO)
	{
        actvresrec.actvresRec.set(SPACES);
        actvresrec.chdrcoy.set(chdrlifRec.getChdrcoy());
        actvresrec.chdrnum.set(chdrlifRec.getChdrnum());
        actvresrec.cnttype.set(chdrlifRec.getCnttype());
        actvresrec.currency.set(chdrlifRec.getCntcurr());
        actvresrec.tranno.set(wsaaTranno);
        actvresrec.life.set(inczpfRec.getLife());
        actvresrec.coverage.set(inczpfRec.getCoverage());
        actvresrec.rider.set(inczpfRec.getRider());
        actvresrec.planSuffix.set(inczpfRec.getPlanSuffix());
        actvresrec.crtable.set(inczpfRec.getCrtable());
        actvresrec.effdate.set(bsscIO.getEffectiveDate());
        actvresrec.clntcoy.set(bsprIO.getFsuco());
        actvresrec.l1Clntnum.set(wsaaL1Clntnum);
        actvresrec.jlife.set(inczpfRec.getJlife());
        actvresrec.l2Clntnum.set(wsaaL2Clntnum);
        actvresrec.oldSumins.set(ZERO);
        actvresrec.newSumins.set(inczpfRec.getNewsum());
        actvresrec.crrcd.set(covrIO.getCrrcd());
        actvresrec.language.set(bsscIO.getLanguage());
        actvresrec.function.set("ACT8");
        actvresrec.batctrcde.set(bprdIO.getAuthCode());
        callProgram(Actvres.class, actvresrec.actvresRec);
        if (isNE(actvresrec.statuz,varcom.oK)) {
            syserrrec.statuz.set(actvresrec.statuz);
            syserrrec.params.set(actvresrec.actvresRec);
            fatalError600();
        }
	}

protected void nextComponent6000()
 {
        Covrpf covrIO = null;
        if (covrMap != null && covrMap.containsKey(inczpfRec.getChdrnum())) {
            for (Covrpf i : covrMap.get(inczpfRec.getChdrnum())) {
                if (inczpfRec.getChdrcoy().equals(i.getChdrcoy()) && inczpfRec.getLife().equals(i.getLife())
                        && "00".equals(i.getJlife()) && isGTE(i.getCoverage(), inczpfRec.getCoverage())
                        && "00".equals(i.getRider())) {
                    if (isGT(i.getCoverage(), wsaaNextCovno)) {
                        wsaaNextCovno.set(i.getCoverage());
                    }
                }
            }
        }
    }

protected void getLifergpDetails7000(Covrpf covrIO)
 {
        boolean foundFlag = true;
        if (lifelnbMap != null && lifelnbMap.containsKey(covrIO.getChdrnum())) {
            for (Lifepf l : lifelnbMap.get(covrIO.getChdrnum())) {
                if ("1".equals(l.getValidflag()) && l.getChdrcoy().equals(covrIO.getChdrcoy())
                        && l.getLife().equals(covrIO.getLife()) && "00".equals(l.getJlife())) {
                    calculateAnb7500(l, covrIO);
                    foundFlag = false;
                    break;
                }
            }
            if (foundFlag) {
                syserrrec.params.set(covrIO.getChdrnum());
                fatalError600();
            }
            for (Lifepf l : lifelnbMap.get(covrIO.getChdrnum())) {
                if ("1".equals(l.getValidflag()) && l.getChdrcoy().equals(covrIO.getChdrcoy())
                        && l.getLife().equals(covrIO.getLife()) && "01".equals(l.getJlife())) {
                    calculateAnb7500(l, covrIO);
                    break;
                }
            }
        }
    }

protected void calculateAnb7500(Lifepf lifergpIO, Covrpf covrIO)
	{
        if (isEQ(lifergpIO.getCltdob(),ZERO)) {
            covrIO.setCpiDate(0);
            return ;
        }
        initialize(agecalcrec.agecalcRec);
        agecalcrec.function.set("CALCB");
        agecalcrec.cnttype.set(chdrlifRec.getCnttype());
        agecalcrec.intDate1.set(lifergpIO.getCltdob());
        agecalcrec.intDate2.set(covrIO.getCpiDate());
        agecalcrec.company.set(bsprIO.getFsuco());
        agecalcrec.language.set(bsscIO.getLanguage());
        callProgram(Agecalc.class, agecalcrec.agecalcRec);
        if (isNE(agecalcrec.statuz,varcom.oK)) {
            syserrrec.statuz.set(agecalcrec.statuz);
            syserrrec.params.set(agecalcrec.agecalcRec);
            fatalError600();
        }
        wsaaAnb.set(agecalcrec.agerating);
	}


protected Chdrpf chdrlifUpdate8000()
 {
        Chdrpf chdrlifIO = null;
        if (chdrlifMap != null && chdrlifMap.containsKey(wsaaChdrnum.toString())) {
            for (Chdrpf c : chdrlifMap.get(wsaaChdrnum.toString())) {
                if (c.getChdrcoy().toString().equals(inczpfRec.getChdrcoy())) {
                    chdrlifIO = c;
                    break;
                }
            }
        }
        if (chdrlifIO == null) {
            syserrrec.params.set(wsaaChdrnum.toString());
            fatalError600();
        }

        if (updateChdrlifList == null) {
            updateChdrlifList = new ArrayList<>();
        }

        chdrlifIO.setValidflag('2');
        chdrlifIO.setCurrto(wsaaOldCpiDate.toInt());
        updateChdrlifList.add(chdrlifIO);

        if (insertChdrlifList == null) {
            insertChdrlifList = new ArrayList<>();
        }

        Chdrpf insertChdr = new Chdrpf(chdrlifIO);
        insertChdr.setUniqueNumber(chdrlifIO.getUniqueNumber());
        insertChdr.setTranno(wsaaTranno.toInt());
        insertChdr.setValidflag('1');
        insertChdr.setCurrfrom(wsaaOldCpiDate.toInt());
        insertChdr.setCurrto(varcom.vrcmMaxDate.toInt());
        if(!prmhldtrad || !lapseReinstated)
        	insertChdr.setSinstamt01(insertChdr.getSinstamt01().add(wsaaInstprem.getbigdata()));
		insertChdr.setSinstamt06(add(add(add(insertChdr.getSinstamt01(),insertChdr.getSinstamt02()),insertChdr.getSinstamt03()),insertChdr.getSinstamt04()).getbigdata());
        
        insertChdrlifList.add(insertChdr);

        Payrpf payrIO = null;
        if (payrMap != null && payrMap.containsKey(wsaaChdrnum.toString())) {
            for (Payrpf p : payrMap.get(wsaaChdrnum.toString())) {
                if (inczpfRec.getChdrcoy().equals(p.getChdrcoy()) && 1 == p.getPayrseqno()) {
                    payrIO = p;
                    break;
                }
            }
        }
        if (payrIO == null) {
            syserrrec.params.set(wsaaChdrnum);
            fatalError600();
        } //IJTI-462 START
        else {
	        payrIO.setValidflag("2");
	        if (updatePayrList == null) {
	            updatePayrList = new ArrayList<>();
	        }
	        updatePayrList.add(payrIO);
	        Payrpf payrInsert = new Payrpf(payrIO);
	        payrInsert.setTranno(chdrlifIO.getTranno());
	        payrInsert.setValidflag("1");
	        if(!prmhldtrad || !lapseReinstated) {//ILIFE-8509
	        	payrInsert.setSinstamt01(payrInsert.getSinstamt01().add(wsaaInstprem.getbigdata()));
	        	payrInsert.setTranno(wsaaTranno.toInt());
	        }
	       	payrInsert.setSinstamt06(add(
	        		add(add(payrInsert.getSinstamt01(),payrInsert.getSinstamt02()),payrInsert.getSinstamt03()),
	        			payrInsert.getSinstamt04()).getbigdata());
			payrInsert.setEffdate(wsaaOldCpiDate.toInt());
	        if (insertPayrList == null) {
	            insertPayrList = new ArrayList<>();
	        }
	        insertPayrList.add(payrInsert);
	        ct03Value++;
        } //IJTI-462 END
        return insertChdr;
    }

protected void ptrnWrite8100(Chdrpf chdrlifIO)
	{
        Ptrnpf ptrnIO = new Ptrnpf();
        ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy().toString());
        ptrnIO.setChdrpfx(chdrlifIO.getChdrpfx());
        ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
        if(isFoundPro){
        	 ptrnIO.setTranno(chdrTranno.toInt());	
        	 compute(chdrTranno, 0).set(add(chdrTranno,1));
        }
        else{
        ptrnIO.setTranno(chdrlifIO.getTranno());
        }
        ptrnIO.setTrdt(bsscIO.getEffectiveDate().toInt());
        ptrnIO.setTrtm(wsaaTransTime.toInt());
        if(isFoundPro){
        	ptrnIO.setPtrneff(wsaaLastCpiDate.toInt());
        }
        else{
        ptrnIO.setPtrneff(wsaaOldCpiDate.toInt());
        }
        ptrnIO.setUserT(subString(bsscIO.getDatimeInit(), 21, 6).toInt());
        varcom.vrcmTranid.set(SPACES);
        ptrnIO.setTermid(varcom.vrcmTermid.toString());
        
        ptrnIO.setBatcpfx(batcdorrec.prefix.toString());
        ptrnIO.setBatccoy(batcdorrec.company.toString());
        ptrnIO.setBatcbrn(batcdorrec.branch.toString());
        ptrnIO.setBatcactyr(batcdorrec.actyear.toInt());
        ptrnIO.setBatcactmn(batcdorrec.actmonth.toInt());
        ptrnIO.setBatctrcde(batcdorrec.trcde.toString());
        ptrnIO.setBatcbatch(batcdorrec.batch.toString());
        ptrnIO.setDatesub(bsscIO.getEffectiveDate().toInt());
        if(insertPtrnList == null){
            insertPtrnList = new ArrayList<>();
        }
        insertPtrnList.add(ptrnIO);
        ct04Value++;
	}

protected void writeUnlock8200(String chdrnum)
	{
        if (contractLocked.isTrue()) {
            contractNotLocked.setTrue();
            return ;
        }
        sftlockrec.sftlockRec.set(SPACES);
        sftlockrec.company.set(bsprIO.getCompany());
        sftlockrec.enttyp.set("CH");
        sftlockrec.entity.set(chdrnum);
        sftlockrec.user.set(subString(bsscIO.getDatimeInit(), 21, 6));
        sftlockrec.transaction.set(bprdIO.getAuthCode());
        sftlockrec.statuz.set(SPACES);
        sftlockrec.function.set("UNLK");
        callProgram(Sftlock.class, sftlockrec.sftlockRec);
        if (isNE(sftlockrec.statuz,varcom.oK)) {
            syserrrec.statuz.set(sftlockrec.statuz);
            syserrrec.params.set(sftlockrec.sftlockRec);
            fatalError600();
        }
        contractNotLocked.setTrue();
	}




protected void a100ReadT5687()
 {
        boolean foundFlag = false;
        if (t5687Map == null) {
            t5687Map = itemDAO.loadSmartTable("IT", bsprIO.getCompany().toString(), "T5687");
        }
        if (t5687Map != null && t5687Map.containsKey(inczpfRec.getCrtable())) {
            for (Itempf i : t5687Map.get(inczpfRec.getCrtable())) {
                if (i.getItmfrm().compareTo(bsscIO.getEffectiveDate().getbigdata()) <= 0) {
                    t5687rec.t5687Rec.set(StringUtil.rawToString(i.getGenarea()));
                    foundFlag = true;
                    break;
                }
            }
        }

        if (!foundFlag) {
            itdmIO.setItemitem(inczpfRec.getCrtable());
            syserrrec.params.set(itdmIO.getParams());
            syserrrec.statuz.set(f294);
            fatalError600();
        }
    }

/*
 * Class transformed  from Data Structure WSAA-T6658-ARRAY--INNER
 */
private static final class WsaaT6658ArrayInner {

		/* WSAA-T6658-ARRAY */
	private FixedLengthStringData[] wsaaT6658Rec = FLSInittedArray (200, 78);
	private PackedDecimalData[] wsaaT6658Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT6658Rec, 0);
	private PackedDecimalData[] wsaaT6658Itmto = PDArrayPartOfArrayStructure(8, 0, wsaaT6658Rec, 5);
	private FixedLengthStringData[] wsaaT6658Key = FLSDArrayPartOfArrayStructure(4, wsaaT6658Rec, 10);
	private FixedLengthStringData[] wsaaT6658Annvry = FLSDArrayPartOfArrayStructure(4, wsaaT6658Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT6658Data = FLSDArrayPartOfArrayStructure(64, wsaaT6658Rec, 14);
	private FixedLengthStringData[] wsaaT6658Addexist = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 0);
	private FixedLengthStringData[] wsaaT6658Addnew = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 1);
	private ZonedDecimalData[] wsaaT6658MaxAge = ZDArrayPartOfArrayStructure(3, 0, wsaaT6658Data, 2);
	private FixedLengthStringData[] wsaaT6658Billfreq = FLSDArrayPartOfArrayStructure(2, wsaaT6658Data, 5);
	private FixedLengthStringData[] wsaaT6658Comind = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 7);
	private FixedLengthStringData[] wsaaT6658CompoundInd = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 8);
	private ZonedDecimalData[] wsaaT6658Fixdtrm = ZDArrayPartOfArrayStructure(3, 0, wsaaT6658Data, 9);
	private FixedLengthStringData[] wsaaT6658Manopt = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 12);
	private ZonedDecimalData[] wsaaT6658MaxPcnt = ZDArrayPartOfArrayStructure(5, 2, wsaaT6658Data, 13);
	private ZonedDecimalData[] wsaaT6658MaxRefusals = ZDArrayPartOfArrayStructure(2, 0, wsaaT6658Data, 18);
	private ZonedDecimalData[] wsaaT6658Minctrm = ZDArrayPartOfArrayStructure(3, 0, wsaaT6658Data, 20);
	private ZonedDecimalData[] wsaaT6658Minpcnt = ZDArrayPartOfArrayStructure(5, 2, wsaaT6658Data, 23);
	private FixedLengthStringData[] wsaaT6658Nocommind = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 28);
	private FixedLengthStringData[] wsaaT6658Nostatin = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 29);
	private FixedLengthStringData[] wsaaT6658Optind = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 30);
	private ZonedDecimalData[] wsaaT6658RefusalPeriod = ZDArrayPartOfArrayStructure(3, 0, wsaaT6658Data, 31);
	private FixedLengthStringData[] wsaaT6658SimpleInd = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 34);
	private FixedLengthStringData[] wsaaT6658Statind = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 35);
	private FixedLengthStringData[] wsaaT6658Subprog = FLSDArrayPartOfArrayStructure(10, wsaaT6658Data, 36);
	private FixedLengthStringData[] wsaaT6658Premsubr = FLSDArrayPartOfArrayStructure(8, wsaaT6658Data, 46);
	private FixedLengthStringData[] wsaaT6658Trevsub = FLSDArrayPartOfArrayStructure(10, wsaaT6658Data, 54);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData itemrec = new FixedLengthStringData(7).init("ITEMREC");
	private FixedLengthStringData pcdtmjarec = new FixedLengthStringData(10).init("PCDTMJAREC");
}
}
