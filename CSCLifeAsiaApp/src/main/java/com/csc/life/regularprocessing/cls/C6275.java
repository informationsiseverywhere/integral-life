/*
 * File: C6275.java
 * Date: 30 August 2009 2:58:54
 * Author: $Id$
 * 
 * Class transformed from C6275.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.cls;

import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.life.regularprocessing.batchprograms.B6275;
import com.csc.smart.procedures.Passparm;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.query.Opnqryf;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class C6275 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData params = new FixedLengthStringData(300);
	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData coy = new FixedLengthStringData(1);
	private FixedLengthStringData parmdate = new FixedLengthStringData(8);
	private FixedLengthStringData conj = new FixedLengthStringData(860);
	private FixedLengthStringData chdrnumfrm = new FixedLengthStringData(8);
	private FixedLengthStringData chdrnumto = new FixedLengthStringData(8);
	private java.sql.ResultSet linspfQryfset = null;
	private java.sql.ResultSet linspfQryfset1 = null;

	public C6275() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		params = convertAndSetParam(params, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			try {
				switch (qState) {
				case QS_START: {
					callProgram(Passparm.class, new Object[] {params, conj});
					chdrnumfrm.set(subString(conj, 1, 8));
					chdrnumto.set(subString(conj, 9, 8));
					branch.set(subString(params, 143, 2));
					coy.set(subString(params, 6, 1));
					parmdate.set(subString(params, 113, 8));
					appVars.overrideTable("LINSPF", "LINSPF");
					appVars.overrideTableShare("LINSPF", true);
					if (isEQ(chdrnumfrm,"        ")
					&& isEQ(chdrnumto,"        ")) {
						java.sql.ResultSet linspfQryfset = null;
						Opnqryf.getInstance().execute(new Object[] {"FILE", "FORMAT", "QRYSLT", "KEYFLD", "MAPFLD"}, new Object[] {"LINSPF", "LINSPF", coy, new String[] {"CHDRCOY", "CHDRNUM", "INSTFROM"}, new String[] {"CHDRCOY 'LINSPF/CHDRCOY'", "CHDRNUM 'LINSPF/CHDRNUM'", "CNTCURR 'LINSPF/CNTCURR'", "VALIDFLAG 'LINSPF/VALIDFLAG'", "INSTFROM 'LINSPF/INSTFROM'", "INSTTO 'LINSPF/INSTTO'", "INSTFREQ 'LINSPF/INSTFREQ'", "INSTJCTL 'LINSPF/INSTJCTL'", "BILLCHNL 'LINSPF/BILLCHNL'", "PAYFLAG 'LINSPF/PAYFLAG'", "DUEFLG 'LINSPF/DUEFLG'", "BILLCURR 'LINSPF/BILLCURR'", "MANDREF 'LINSPF/MANDREF'", "ACCTMETH 'LINSPF/ACCTMETH'", "BILLCD 'LINSPF/BILLCD'"}});
					}
					else {
						java.sql.ResultSet linspfQryfset1 = null;
						Opnqryf.getInstance().execute(new Object[] {"FILE", "FORMAT", "QRYSLT", "KEYFLD", "MAPFLD"}, new Object[] {"LINSPF", "LINSPF", chdrnumfrm, new String[] {"CHDRCOY", "CHDRNUM", "INSTFROM"}, new String[] {"CHDRCOY 'LINSPF/CHDRCOY'", "CHDRNUM 'LINSPF/CHDRNUM'", "CNTCURR 'LINSPF/CNTCURR'", "VALIDFLAG 'LINSPF/VALIDFLAG'", "INSTFROM 'LINSPF/INSTFROM'", "INSTTO 'LINSPF/INSTTO'", "INSTFREQ 'LINSPF/INSTFREQ'", "INSTJCTL 'LINSPF/INSTJCTL'", "BILLCHNL 'LINSPF/BILLCHNL'", "PAYFLAG 'LINSPF/PAYFLAG'", "DUEFLG 'LINSPF/DUEFLG'", "BILLCURR 'LINSPF/BILLCURR'", "MANDREF 'LINSPF/MANDREF'", "ACCTMETH 'LINSPF/ACCTMETH'", "BILLCD 'LINSPF/BILLCD'"}});
					}
					callProgram(B6275.class, new Object[] {params});
					try {
						Opnqryf.getInstance().close("LINSPF");
					}
					catch (ExtMsgException ex1){
						if (ex1.messageMatches("CPF4520")) {
						}
						else {
							throw ex1;
						}
					}
					appVars.deleteOverride("LINSPF", ROUTINE);
				}
				case returnVar: {
					return ;
				}
				case error: {
					appVars.addExtMessage("Unexpected Errors Have Occured"+".");
					qState = returnVar;
					break;
				}
				default:{
					qState = QS_END;
				}
				}
			}
			catch (ExtMsgException ex){
				if (ex.messageMatches("CPF0000")) {
					qState = error;
				}
				else {
					throw ex;
				}
			}
		}
		
	}
}
