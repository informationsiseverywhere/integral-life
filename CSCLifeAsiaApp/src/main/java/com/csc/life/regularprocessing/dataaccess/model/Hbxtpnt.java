package com.csc.life.regularprocessing.dataaccess.model;

import java.sql.Date;

public class Hbxtpnt {

	private long unique_number;
    private String chdrpfx;
    private String chdrcoy;
    private String chdrnum;
    private String servunit;
    private String cnttype;
    private String cntcurr;
    private int occdate;
    private int ccdate;
    private int ptdate;
    private int btdate;
    private int billdate;
    private String billchnl;
    private String bankcode;
    private int instfrom;
    private int instto;
    private String instbchnl;
    private String instcchnl;
    private String instfreq;
    private double instamt01;
    private double instamt02;
    private double instamt03;
    private double instamt04;
    private double instamt05;
    private double instamt06;
    private double instamt07;
    private double instamt08;
    private double instamt09;
    private double instamt10;
    private double instamt11;
    private double instamt12;
    private double instamt13;
    private double instamt14;
    private double instamt15;
    private String instjctl;
    private String grupkey;
    private String membsel;
    private String facthous;
    private String bankkey;
    private String bankacckey;
    private String cownpfx;
    private String cowncoy;
    private String cownnum;
    private String payrpfx;
    private String payrcoy;
    private String payrnum;
    private String cntbranch;
    private String agntpfx;
    private String agntcoy;
    private String agntnum;
    private String payflag;
    private String bilflag;
    private String outflag;
    private String supflag;
    private String mandref;
    private int billcd;
    private String sacscode;
    private String sacstyp;
    private String glmap;
    private String mandstat;
    private String userProfile;
    private String jobName;
    private Date datime;
    private int nextdate;
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
	public String getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getServunit() {
		return servunit;
	}
	public void setServunit(String servunit) {
		this.servunit = servunit;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}
	public int getOccdate() {
		return occdate;
	}
	public void setOccdate(int occdate) {
		this.occdate = occdate;
	}
	public int getCcdate() {
		return ccdate;
	}
	public void setCcdate(int ccdate) {
		this.ccdate = ccdate;
	}
	public int getPtdate() {
		return ptdate;
	}
	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}
	public int getBtdate() {
		return btdate;
	}
	public void setBtdate(int btdate) {
		this.btdate = btdate;
	}
	public int getBilldate() {
		return billdate;
	}
	public void setBilldate(int billdate) {
		this.billdate = billdate;
	}
	public String getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(String billchnl) {
		this.billchnl = billchnl;
	}
	public String getBankcode() {
		return bankcode;
	}
	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}
	public int getInstfrom() {
		return instfrom;
	}
	public void setInstfrom(int instfrom) {
		this.instfrom = instfrom;
	}
	public int getInstto() {
		return instto;
	}
	public void setInstto(int instto) {
		this.instto = instto;
	}
	public String getInstbchnl() {
		return instbchnl;
	}
	public void setInstbchnl(String instbchnl) {
		this.instbchnl = instbchnl;
	}
	public String getInstcchnl() {
		return instcchnl;
	}
	public void setInstcchnl(String instcchnl) {
		this.instcchnl = instcchnl;
	}
	public String getInstfreq() {
		return instfreq;
	}
	public void setInstfreq(String instfreq) {
		this.instfreq = instfreq;
	}
	public double getInstamt01() {
		return instamt01;
	}
	public void setInstamt01(double instamt01) {
		this.instamt01 = instamt01;
	}
	public double getInstamt02() {
		return instamt02;
	}
	public void setInstamt02(double instamt02) {
		this.instamt02 = instamt02;
	}
	public double getInstamt03() {
		return instamt03;
	}
	public void setInstamt03(double instamt03) {
		this.instamt03 = instamt03;
	}
	public double getInstamt04() {
		return instamt04;
	}
	public void setInstamt04(double instamt04) {
		this.instamt04 = instamt04;
	}
	public double getInstamt05() {
		return instamt05;
	}
	public void setInstamt05(double instamt05) {
		this.instamt05 = instamt05;
	}
	public double getInstamt06() {
		return instamt06;
	}
	public void setInstamt06(double instamt06) {
		this.instamt06 = instamt06;
	}
	public double getInstamt07() {
		return instamt07;
	}
	public void setInstamt07(double instamt07) {
		this.instamt07 = instamt07;
	}
	public double getInstamt08() {
		return instamt08;
	}
	public void setInstamt08(double instamt08) {
		this.instamt08 = instamt08;
	}
	public double getInstamt09() {
		return instamt09;
	}
	public void setInstamt09(double instamt09) {
		this.instamt09 = instamt09;
	}
	public double getInstamt10() {
		return instamt10;
	}
	public void setInstamt10(double instamt10) {
		this.instamt10 = instamt10;
	}
	public double getInstamt11() {
		return instamt11;
	}
	public void setInstamt11(double instamt11) {
		this.instamt11 = instamt11;
	}
	public double getInstamt12() {
		return instamt12;
	}
	public void setInstamt12(double instamt12) {
		this.instamt12 = instamt12;
	}
	public double getInstamt13() {
		return instamt13;
	}
	public void setInstamt13(double instamt13) {
		this.instamt13 = instamt13;
	}
	public double getInstamt14() {
		return instamt14;
	}
	public void setInstamt14(double instamt14) {
		this.instamt14 = instamt14;
	}
	public double getInstamt15() {
		return instamt15;
	}
	public void setInstamt15(double instamt15) {
		this.instamt15 = instamt15;
	}
	public String getInstjctl() {
		return instjctl;
	}
	public void setInstjctl(String instjctl) {
		this.instjctl = instjctl;
	}
	public String getGrupkey() {
		return grupkey;
	}
	public void setGrupkey(String grupkey) {
		this.grupkey = grupkey;
	}
	public String getMembsel() {
		return membsel;
	}
	public void setMembsel(String membsel) {
		this.membsel = membsel;
	}
	public String getFacthous() {
		return facthous;
	}
	public void setFacthous(String facthous) {
		this.facthous = facthous;
	}
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	public String getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}
	public String getCownpfx() {
		return cownpfx;
	}
	public void setCownpfx(String cownpfx) {
		this.cownpfx = cownpfx;
	}
	public String getCowncoy() {
		return cowncoy;
	}
	public void setCowncoy(String cowncoy) {
		this.cowncoy = cowncoy;
	}
	public String getCownnum() {
		return cownnum;
	}
	public void setCownnum(String cownnum) {
		this.cownnum = cownnum;
	}
	public String getPayrpfx() {
		return payrpfx;
	}
	public void setPayrpfx(String payrpfx) {
		this.payrpfx = payrpfx;
	}
	public String getPayrcoy() {
		return payrcoy;
	}
	public void setPayrcoy(String payrcoy) {
		this.payrcoy = payrcoy;
	}
	public String getPayrnum() {
		return payrnum;
	}
	public void setPayrnum(String payrnum) {
		this.payrnum = payrnum;
	}
	public String getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(String cntbranch) {
		this.cntbranch = cntbranch;
	}
	public String getAgntpfx() {
		return agntpfx;
	}
	public void setAgntpfx(String agntpfx) {
		this.agntpfx = agntpfx;
	}
	public String getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public String getPayflag() {
		return payflag;
	}
	public void setPayflag(String payflag) {
		this.payflag = payflag;
	}
	public String getBilflag() {
		return bilflag;
	}
	public void setBilflag(String bilflag) {
		this.bilflag = bilflag;
	}
	public String getOutflag() {
		return outflag;
	}
	public void setOutflag(String outflag) {
		this.outflag = outflag;
	}
	public String getSupflag() {
		return supflag;
	}
	public void setSupflag(String supflag) {
		this.supflag = supflag;
	}
	public String getMandref() {
		return mandref;
	}
	public void setMandref(String mandref) {
		this.mandref = mandref;
	}
	public int getBillcd() {
		return billcd;
	}
	public void setBillcd(int billcd) {
		this.billcd = billcd;
	}
	public String getSacscode() {
		return sacscode;
	}
	public void setSacscode(String sacscode) {
		this.sacscode = sacscode;
	}
	public String getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(String sacstyp) {
		this.sacstyp = sacstyp;
	}
	public String getGlmap() {
		return glmap;
	}
	public void setGlmap(String glmap) {
		this.glmap = glmap;
	}
	public String getMandstat() {
		return mandstat;
	}
	public void setMandstat(String mandstat) {
		this.mandstat = mandstat;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public Date getDatime() {
		return new Date(datime.getTime());//IJTI-316
	}
	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());//IJTI-314
	}
	public int getNextdate() {
		return nextdate;
	}
	public void setNextdate(int nextdate) {
		this.nextdate = nextdate;
	}
    
    
	
}
