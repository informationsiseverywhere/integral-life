package com.csc.life.regularprocessing.dataaccess;

import com.csc.fsu.general.dataaccess.DdsupfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: DdsurnlTableDAM.java
 * Date: Sun, 30 Aug 2009 03:36:57
 * Class transformed from DDSURNL.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class DdsurnlTableDAM extends DdsupfTableDAM {

	public DdsurnlTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("DDSURNL");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "PAYRCOY"
		             + ", PAYRNUM"
		             + ", MANDREF"
		             + ", BILLCD"
		             + ", MANDSTAT";
		
		QUALIFIEDCOLUMNS = 
		            "PAYRCOY, " +
		            "PAYRNUM, " +
		            "MANDREF, " +
		            "BILLCD, " +
		            "DHNFLAG, " +
		            "LAPDAY, " +
		            "MANDSTAT, " +
		            "COMPANY, " +
		            "JOBNO, " +
		            "DEBITAMT, " +
		            "ORIGMANST, " +
		            "SUBDAT, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "PAYRCOY ASC, " +
		            "PAYRNUM ASC, " +
		            "MANDREF ASC, " +
		            "BILLCD DESC, " +
		            "MANDSTAT DESC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "PAYRCOY DESC, " +
		            "PAYRNUM DESC, " +
		            "MANDREF DESC, " +
		            "BILLCD ASC, " +
		            "MANDSTAT ASC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               payrcoy,
                               payrnum,
                               mandref,
                               billcd,
                               dhnflag,
                               lapday,
                               mandstat,
                               company,
                               jobno,
                               debitAmount,
                               originalMandstat,
                               subdat,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(43);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getPayrcoy().toInternal()
					+ getPayrnum().toInternal()
					+ getMandref().toInternal()
					+ getBillcd().toInternal()
					+ getMandstat().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, payrcoy);
			what = ExternalData.chop(what, payrnum);
			what = ExternalData.chop(what, mandref);
			what = ExternalData.chop(what, billcd);
			what = ExternalData.chop(what, mandstat);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(5);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(5);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(payrcoy.toInternal());
	nonKeyFiller20.setInternal(payrnum.toInternal());
	nonKeyFiller30.setInternal(mandref.toInternal());
	nonKeyFiller40.setInternal(billcd.toInternal());
	nonKeyFiller70.setInternal(mandstat.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(92);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ getDhnflag().toInternal()
					+ getLapday().toInternal()
					+ nonKeyFiller70.toInternal()
					+ getCompany().toInternal()
					+ getJobno().toInternal()
					+ getDebitAmount().toInternal()
					+ getOriginalMandstat().toInternal()
					+ getSubdat().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, dhnflag);
			what = ExternalData.chop(what, lapday);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, company);
			what = ExternalData.chop(what, jobno);
			what = ExternalData.chop(what, debitAmount);
			what = ExternalData.chop(what, originalMandstat);
			what = ExternalData.chop(what, subdat);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getPayrcoy() {
		return payrcoy;
	}
	public void setPayrcoy(Object what) {
		payrcoy.set(what);
	}
	public FixedLengthStringData getPayrnum() {
		return payrnum;
	}
	public void setPayrnum(Object what) {
		payrnum.set(what);
	}
	public FixedLengthStringData getMandref() {
		return mandref;
	}
	public void setMandref(Object what) {
		mandref.set(what);
	}
	public PackedDecimalData getBillcd() {
		return billcd;
	}
	public void setBillcd(Object what) {
		setBillcd(what, false);
	}
	public void setBillcd(Object what, boolean rounded) {
		if (rounded)
			billcd.setRounded(what);
		else
			billcd.set(what);
	}
	public FixedLengthStringData getMandstat() {
		return mandstat;
	}
	public void setMandstat(Object what) {
		mandstat.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getDhnflag() {
		return dhnflag;
	}
	public void setDhnflag(Object what) {
		dhnflag.set(what);
	}	
	public PackedDecimalData getLapday() {
		return lapday;
	}
	public void setLapday(Object what) {
		setLapday(what, false);
	}
	public void setLapday(Object what, boolean rounded) {
		if (rounded)
			lapday.setRounded(what);
		else
			lapday.set(what);
	}	
	public FixedLengthStringData getCompany() {
		return company;
	}
	public void setCompany(Object what) {
		company.set(what);
	}	
	public PackedDecimalData getJobno() {
		return jobno;
	}
	public void setJobno(Object what) {
		setJobno(what, false);
	}
	public void setJobno(Object what, boolean rounded) {
		if (rounded)
			jobno.setRounded(what);
		else
			jobno.set(what);
	}	
	public PackedDecimalData getDebitAmount() {
		return debitAmount;
	}
	public void setDebitAmount(Object what) {
		setDebitAmount(what, false);
	}
	public void setDebitAmount(Object what, boolean rounded) {
		if (rounded)
			debitAmount.setRounded(what);
		else
			debitAmount.set(what);
	}	
	public FixedLengthStringData getOriginalMandstat() {
		return originalMandstat;
	}
	public void setOriginalMandstat(Object what) {
		originalMandstat.set(what);
	}	
	public PackedDecimalData getSubdat() {
		return subdat;
	}
	public void setSubdat(Object what) {
		setSubdat(what, false);
	}
	public void setSubdat(Object what, boolean rounded) {
		if (rounded)
			subdat.setRounded(what);
		else
			subdat.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		payrcoy.clear();
		payrnum.clear();
		mandref.clear();
		billcd.clear();
		mandstat.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		dhnflag.clear();
		lapday.clear();
		nonKeyFiller70.clear();
		company.clear();
		jobno.clear();
		debitAmount.clear();
		originalMandstat.clear();
		subdat.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}