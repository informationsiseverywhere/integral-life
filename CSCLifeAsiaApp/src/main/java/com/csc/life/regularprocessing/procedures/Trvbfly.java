/*
 * File: Trvbfly.java
 * Date: 30 August 2009 2:46:05
 * Author: Quipoz Limited
 * 
 * Class transformed from TRVBFLY.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.regularprocessing.dataaccess.CovrbonTableDAM;
import com.csc.life.regularprocessing.recordstructures.Bonusrec;
import com.csc.life.regularprocessing.tablestructures.Tt551rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* REVERSIONARY BONUS FULL YEAR
* ----------------------------
*
* TRVBFLY is cloned from RVBFLYR. It will read new created table
*         TT551 (Expanded Reversionary Bonus Rate).
*
* TRVBFLY will be called from either B5018 (Company Anniversary)
* or   B5023  (Policy  Anniversary)   batch  jobs  to   allocate
* Reversionary  bonuses  for a full year.  ie.  no proportioning
* necessary.
*
* TRVBFLY will be  called from B5018/B5023 to  calculate the Sum
* Assured Reversionary Bonus due plus the Bonus on Bonus Due. It
* is called using  the linkage BONUSREC.
*
* This program will be called with a Function of CALC or spaces,
* if  SPACES, an ACMV will be  written.  Otherwise,  ACMV's  are
* skipped and the values just returned as calculated values.  If
* Function is neither then perform normal error handling.
*
* CALCULATE BONUS.
*
* Read
*              BONS-BONUS-CALC-METH
*              BONS-PREM-STATUS
*
* Make sure the Risk Unit on the extra data screen is valid.
*
* Read  ACBL  record  to  get  the  current  Reversionary  Bonus
* allocated.
*
* Calculate the Term in Force for contract to be able to do  the
* Rate look up on TT551.
*
*   Compute Bonus S/A =   ( Sum Assured * rate from TT551 )
*                       / ( TT551-Prem-Unit * TT551-Unit  )
*
*   Compute Bonus Bon = 0
*
*   Compute the Total Bonus = Bonus S/A + Bonus Bon.
*
* WRITE ACMV RECORD.
*
*       The  subroutine LIFACMV  will  be  called  to  add
*       this  ACMV record,  the following key  information
*       will be required.
*
*       LIFA-RDOCNUM           -  BONS-CHDR-CHDRNUM
*       LIFA-RLDGACCT          -  Full Entity Key:-
*                                 Chdrnum       (08).
*                                 Life          (02).
*                                 Coverage      (02).
*                                 Rider         (02).
*                                 Plan-Suffix   (02).
*
*       LIFA-BATCKEY           -  BONS-BATCKEY (Linkage)
*       LIFA-ORIGCURR          -  BONS-CNTCURR (Linkage)
*       LIFA-TRANNO            -  BONS-TRANNO (Linkage)
*       LIFA-JRNSEQ            -  0
*       LIFA-ORIGAMT           -  Total Bonus as Above.
*       LIFA-TRANREF           -  BONS-CHDRNUM
*       LIFA-SACSTYP           -  T5645 Sacstype-02
*       LIFA-SACSCODE          -  T5645 Sacscode-02
*       LIFA-GLCODE            -  T5645 Glcode-02
*       LIFA-GLSIGN            -  T5645 Glsign-02
*       LIFA-CNTTOT            -  T5645 Contot-02
*       LIFA-EFFDATE           -  BONS-EFFDATE (Linkage)
*
* UPDATE COVR RECORD.
*
* The  COVR is  read  and the  CURRFROM/CURRTO dates are updated
* and  the  record  is validflagged as  2.  The new COVR has the
* CURRFROM/CURRTO  dates  updated, as well as the unit statement
* date, and flagged as 1.
*
* UPDATE RETURNED LINKAGE.
*
* Move T6640 values,  Low cost rider/subroutine  to the BONUSREC
* values.  Call the  Bonus Routine  held here  which will reduce
* from the Sum Assured the Bonus Value Allocated.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Trvbfly extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "TRVBFLY";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSumassRate = new ZonedDecimalData(7, 2).setUnsigned();
	private ZonedDecimalData wsaaBonusRate = new ZonedDecimalData(7, 2).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaRvBonusSa = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRvBonusBon = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaBonusSum = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCurbal = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaTermInForce = new ZonedDecimalData(6, 0);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(6, 0);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBonusMethod = new FixedLengthStringData(4).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaPremStatus = new FixedLengthStringData(2).isAPartOf(wsaaItem, 4);
		/* ERRORS */
	private static final String e107 = "E107";
	private static final String tl22 = "TL22";
	private static final String g578 = "G578";
		/* FORMATS */
	private static final String covrbonrec = "COVRBONREC";
	private static final String acblrec = "ACBLREC";
	private static final String itemrec = "ITEMREC";
	private static final String itdmrec = "ITEMREC";
		/* TABLES */
	private static final String t5645 = "T5645";
	private static final String tt551 = "TT551";
	private AcblTableDAM acblIO = new AcblTableDAM();
	private CovrbonTableDAM covrbonIO = new CovrbonTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private T5645rec t5645rec = new T5645rec();
	private Tt551rec tt551rec = new Tt551rec();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Bonusrec bonusrec = new Bonusrec();

	public Trvbfly() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		bonusrec.bonusRec = convertAndSetParam(bonusrec.bonusRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		initialise200();
		readBonusLinkage300();
		/*EXIT*/
		exitProgram();
	}

protected void initialise200()
	{
		/*INITIALISE*/
		bonusrec.statuz.set("****");
		wsaaSumassRate.set(ZERO);
		wsaaBonusSum.set(ZERO);
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		syserrrec.subrname.set(wsaaSubr);
		/*EXIT*/
	}

protected void readBonusLinkage300()
	{
			beginReading310();
		}

protected void beginReading310()
	{
		/*  Check BONS-FUNCTION for CALC or SPACES.*/
		if (isNE(bonusrec.function,"CALC")
		&& isNE(bonusrec.function,SPACES)) {
			syserrrec.params.set(bonusrec.bonusRec);
			syserrrec.statuz.set(g578);
			systemError900();
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(bonusrec.chdrChdrcoy);
		itdmIO.setItmfrm(bonusrec.effectiveDate);
		itdmIO.setItemtabl(tt551);
		wsaaBonusMethod.set(bonusrec.bonusCalcMeth);
		wsaaPremStatus.set(bonusrec.premStatus);
		itdmIO.setItemitem(wsaaItem);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError800();
		}
		if ((isNE(itdmIO.getItemitem(),wsaaItem))
		|| (isNE(itdmIO.getItemcoy(),bonusrec.chdrChdrcoy))
		|| (isNE(itdmIO.getItemtabl(),tt551))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemitem(wsaaItem);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(tl22);
			systemError900();
		}
		tt551rec.tt551Rec.set(itdmIO.getGenarea());
		/* Check the Years in force for the component.*/
		checkYearsInForce400();
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bonusrec.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError800();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		acblIO.setParams(SPACES);
		wsaaRldgChdrnum.set(bonusrec.chdrChdrnum);
		wsaaRldgLife.set(bonusrec.lifeLife);
		wsaaRldgCoverage.set(bonusrec.covrCoverage);
		wsaaRldgRider.set(bonusrec.covrRider);
		wsaaPlan.set(bonusrec.plnsfx);
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		acblIO.setRldgacct(wsaaRldgacct);
		acblIO.setRldgcoy(bonusrec.chdrChdrcoy);
		acblIO.setOrigcurr(bonusrec.cntcurr);
		acblIO.setSacscode(t5645rec.sacscode01);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if ((isNE(acblIO.getStatuz(),varcom.oK))
		&& (isNE(acblIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			databaseError800();
		}
		if (isEQ(acblIO.getStatuz(),varcom.mrnf)) {
		}
		/* Calculate the sum assured and bonus bonuses.*/
		bonusCalculations500();
		if (isEQ(bonusrec.function,"CALC")) {
			return ;
		}
		/* Add up both bonuses for lifa-origamt.*/
		compute(wsaaBonusSum, 3).setRounded((add(bonusrec.rvBonusSa,bonusrec.rvBonusBon)));
		bonusrec.rvBalLy.set(wsaaBonusSum);
		/* Set up lifacmv fields.*/
		lifacmvrec1.lifacmvRec.set(SPACES);
		lifacmvrec1.rdocnum.set(bonusrec.chdrChdrnum);
		lifacmvrec1.jrnseq.set(0);
		lifacmvrec1.rldgcoy.set(bonusrec.chdrChdrcoy);
		lifacmvrec1.genlcoy.set(bonusrec.chdrChdrcoy);
		lifacmvrec1.batckey.set(bonusrec.batckey);
		lifacmvrec1.origcurr.set(bonusrec.cntcurr);
		lifacmvrec1.origamt.set(wsaaBonusSum);
		lifacmvrec1.rcamt.set(0);
		lifacmvrec1.crate.set(0);
		lifacmvrec1.acctamt.set(0);
		lifacmvrec1.contot.set(0);
		lifacmvrec1.tranno.set(bonusrec.tranno);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.effdate.set(bonusrec.effectiveDate);
		lifacmvrec1.tranref.set(bonusrec.chdrChdrnum);
		lifacmvrec1.substituteCode[1].set(bonusrec.cnttype);
		lifacmvrec1.substituteCode[6].set(bonusrec.crtable);
		/* Get item description.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bonusrec.chdrChdrcoy);
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaSubr);
		descIO.setLanguage(bonusrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		lifacmvrec1.trandesc.set(descIO.getLongdesc());
		wsaaRldgChdrnum.set(bonusrec.chdrChdrnum);
		wsaaRldgLife.set(bonusrec.lifeLife);
		wsaaRldgCoverage.set(bonusrec.covrCoverage);
		wsaaRldgRider.set(bonusrec.covrRider);
		wsaaPlan.set(bonusrec.plnsfx);
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec1.rldgacct.set(wsaaRldgacct);
		lifacmvrec1.transactionDate.set(varcom.vrcmDate);
		lifacmvrec1.transactionTime.set(varcom.vrcmTime);
		lifacmvrec1.user.set(bonusrec.user);
		lifacmvrec1.sacscode.set(t5645rec.sacscode02);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec1.glcode.set(t5645rec.glmap02);
		lifacmvrec1.glsign.set(t5645rec.sign02);
		lifacmvrec1.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			databaseError800();
		}
		covrbonIO.setDataKey(SPACES);
		covrbonIO.setChdrcoy(bonusrec.chdrChdrcoy);
		covrbonIO.setChdrnum(bonusrec.chdrChdrnum);
		covrbonIO.setLife(bonusrec.lifeLife);
		covrbonIO.setCoverage(bonusrec.covrCoverage);
		covrbonIO.setRider(bonusrec.covrRider);
		covrbonIO.setPlanSuffix(bonusrec.plnsfx);
		covrbonIO.setFunction(varcom.readh);
		covrbonIO.setFormat(covrbonrec);
		SmartFileCode.execute(appVars, covrbonIO);
		if (isNE(covrbonIO.getStatuz(),varcom.oK)
		&& isNE(covrbonIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrbonIO.getParams());
			syserrrec.statuz.set(covrbonIO.getStatuz());
			databaseError800();
		}
		covrbonIO.setCurrto(bonusrec.effectiveDate);
		covrbonIO.setValidflag("2");
		covrbonIO.setFunction(varcom.rewrt);
		covrbonIO.setFormat(covrbonrec);
		SmartFileCode.execute(appVars, covrbonIO);
		if (isNE(covrbonIO.getStatuz(),varcom.oK)
		&& isNE(covrbonIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrbonIO.getParams());
			syserrrec.statuz.set(covrbonIO.getStatuz());
			databaseError800();
		}
		covrbonIO.setValidflag("1");
		covrbonIO.setTranno(bonusrec.tranno);
		covrbonIO.setCurrfrom(bonusrec.effectiveDate);
		covrbonIO.setUnitStatementDate(bonusrec.effectiveDate);
		covrbonIO.setCurrto(varcom.vrcmMaxDate);
		covrbonIO.setFunction(varcom.writr);
		covrbonIO.setFormat(covrbonrec);
		SmartFileCode.execute(appVars, covrbonIO);
		if (isNE(covrbonIO.getStatuz(),varcom.oK)
		&& isNE(covrbonIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrbonIO.getParams());
			syserrrec.statuz.set(covrbonIO.getStatuz());
			databaseError800();
		}
		if (isEQ(bonusrec.lowCostRider,SPACES)
		|| isEQ(bonusrec.lowCostSubr,SPACES)) {
			return ;
		}
		if ((isNE(bonusrec.lowCostRider,SPACES)
		&& isNE(bonusrec.lowCostSubr,SPACES))) {
			callProgram(bonusrec.lowCostSubr, bonusrec.bonusRec);
		}
		if ((isNE(bonusrec.statuz,varcom.oK))) {
			syserrrec.params.set(bonusrec.bonusRec);
			syserrrec.statuz.set(bonusrec.statuz);
			systemError900();
		}
	}

protected void checkYearsInForce400()
	{
		start410();
	}

protected void start410()
	{
		wsaaTermInForce.set(bonusrec.termInForce);
		if (isEQ(wsaaTermInForce,0)) {
			if (isEQ(tt551rec.insprem,ZERO)) {
				syserrrec.params.set(bonusrec.bonusRec);
				syserrrec.statuz.set(e107);
				databaseError800();
			}
			else {
				wsaaSumassRate.set(tt551rec.insprem);
			}
		}
		else {
			/*       IF WSAA-TERM-IN-FORCE       = 100                         */
			/*          IF TT551-INSTPR = ZERO                                 */
			/*             MOVE BONS-BONUS-REC   TO SYSR-PARAMS                */
			/*             MOVE E107             TO SYSR-STATUZ                */
			/*             PERFORM 800-DATABASE-ERROR                          */
			/*          ELSE                                                   */
			/*             MOVE TT551-INSTPR     TO WSAA-SUMASS-RATE           */
			/*          END-IF                                                 */
			if (isGTE(wsaaTermInForce, 100)
			&& isLTE(wsaaTermInForce, 110)) {
				compute(wsaaIndex, 0).set(sub(wsaaTermInForce, 99));
				if (isEQ(tt551rec.instpr[wsaaIndex.toInt()], ZERO)) {
					syserrrec.params.set(bonusrec.bonusRec);
					syserrrec.statuz.set(e107);
					databaseError800();
				}
				else {
					wsaaSumassRate.set(tt551rec.instpr[wsaaIndex.toInt()]);
				}
			}
			else {
				if (isEQ(tt551rec.insprm[wsaaTermInForce.toInt()],ZERO)) {
					syserrrec.params.set(bonusrec.bonusRec);
					syserrrec.statuz.set(e107);
					databaseError800();
				}
				else {
					wsaaSumassRate.set(tt551rec.insprm[wsaaTermInForce.toInt()]);
				}
			}
		}
	}

protected void bonusCalculations500()
	{
		/*START*/
		compute(bonusrec.rvBonusSa, 3).setRounded(div((mult(bonusrec.sumin,wsaaSumassRate)),(mult(tt551rec.premUnit,tt551rec.unit))));
		zrdecplrec.amountIn.set(bonusrec.rvBonusSa);
		a000CallRounding();
		bonusrec.rvBonusSa.set(zrdecplrec.amountOut);
		bonusrec.rvBonusBon.set(0);
		/*EXIT*/
	}

protected void databaseError800()
	{
					start810();
					exit870();
				}

protected void start810()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit870()
	{
		bonusrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void systemError900()
	{
					start910();
					exit970();
				}

protected void start910()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit970()
	{
		bonusrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bonusrec.chdrChdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(bonusrec.cntcurr);
		zrdecplrec.batctrcde.set(bonusrec.transcd);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError900();
		}
		/*A000-EXIT*/
	}
}
