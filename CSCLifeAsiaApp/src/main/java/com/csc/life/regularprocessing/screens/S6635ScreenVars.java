package com.csc.life.regularprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6635
 * @version 1.0 generated on 30/08/09 06:54
 * @author Quipoz
 */
public class S6635ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1040);
	public FixedLengthStringData dataFields = new FixedLengthStringData(272).isAPartOf(dataArea, 0);
	public FixedLengthStringData bonuss = new FixedLengthStringData(70).isAPartOf(dataFields, 0);
	public ZonedDecimalData[] bonus = ZDArrayPartOfStructure(10, 7, 2, bonuss, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(70).isAPartOf(bonuss, 0, FILLER_REDEFINE);
	public ZonedDecimalData bonus01 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData bonus02 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,7);
	public ZonedDecimalData bonus03 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,14);
	public ZonedDecimalData bonus04 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,21);
	public ZonedDecimalData bonus05 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,28);
	public ZonedDecimalData bonus06 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,35);
	public ZonedDecimalData bonus07 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,42);
	public ZonedDecimalData bonus08 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,49);
	public ZonedDecimalData bonus09 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,56);
	public ZonedDecimalData bonus10 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,63);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,70);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,71);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,79);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,87);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,95);
	public FixedLengthStringData offsets = new FixedLengthStringData(30).isAPartOf(dataFields, 125);
	public ZonedDecimalData[] offset = ZDArrayPartOfStructure(10, 3, 0, offsets, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(offsets, 0, FILLER_REDEFINE);
	public ZonedDecimalData offset01 = DD.offset.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData offset02 = DD.offset.copyToZonedDecimal().isAPartOf(filler1,3);
	public ZonedDecimalData offset03 = DD.offset.copyToZonedDecimal().isAPartOf(filler1,6);
	public ZonedDecimalData offset04 = DD.offset.copyToZonedDecimal().isAPartOf(filler1,9);
	public ZonedDecimalData offset05 = DD.offset.copyToZonedDecimal().isAPartOf(filler1,12);
	public ZonedDecimalData offset06 = DD.offset.copyToZonedDecimal().isAPartOf(filler1,15);
	public ZonedDecimalData offset07 = DD.offset.copyToZonedDecimal().isAPartOf(filler1,18);
	public ZonedDecimalData offset08 = DD.offset.copyToZonedDecimal().isAPartOf(filler1,21);
	public ZonedDecimalData offset09 = DD.offset.copyToZonedDecimal().isAPartOf(filler1,24);
	public ZonedDecimalData offset10 = DD.offset.copyToZonedDecimal().isAPartOf(filler1,27);
	public FixedLengthStringData riskunits = new FixedLengthStringData(12).isAPartOf(dataFields, 155);
	public ZonedDecimalData[] riskunit = ZDArrayPartOfStructure(2, 6, 0, riskunits, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(12).isAPartOf(riskunits, 0, FILLER_REDEFINE);
	public ZonedDecimalData riskunit01 = DD.riskunit.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData riskunit02 = DD.riskunit.copyToZonedDecimal().isAPartOf(filler2,6);
	public FixedLengthStringData sumasss = new FixedLengthStringData(70).isAPartOf(dataFields, 167);
	public ZonedDecimalData[] sumass = ZDArrayPartOfStructure(10, 7, 2, sumasss, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(70).isAPartOf(sumasss, 0, FILLER_REDEFINE);
	public ZonedDecimalData sumass01 = DD.sumass.copyToZonedDecimal().isAPartOf(filler3,0);
	public ZonedDecimalData sumass02 = DD.sumass.copyToZonedDecimal().isAPartOf(filler3,7);
	public ZonedDecimalData sumass03 = DD.sumass.copyToZonedDecimal().isAPartOf(filler3,14);
	public ZonedDecimalData sumass04 = DD.sumass.copyToZonedDecimal().isAPartOf(filler3,21);
	public ZonedDecimalData sumass05 = DD.sumass.copyToZonedDecimal().isAPartOf(filler3,28);
	public ZonedDecimalData sumass06 = DD.sumass.copyToZonedDecimal().isAPartOf(filler3,35);
	public ZonedDecimalData sumass07 = DD.sumass.copyToZonedDecimal().isAPartOf(filler3,42);
	public ZonedDecimalData sumass08 = DD.sumass.copyToZonedDecimal().isAPartOf(filler3,49);
	public ZonedDecimalData sumass09 = DD.sumass.copyToZonedDecimal().isAPartOf(filler3,56);
	public ZonedDecimalData sumass10 = DD.sumass.copyToZonedDecimal().isAPartOf(filler3,63);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,237);
	public FixedLengthStringData yrsinfs = new FixedLengthStringData(30).isAPartOf(dataFields, 242);
	public ZonedDecimalData[] yrsinf = ZDArrayPartOfStructure(10, 3, 0, yrsinfs, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(30).isAPartOf(yrsinfs, 0, FILLER_REDEFINE);
	public ZonedDecimalData yrsinf01 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler4,0);
	public ZonedDecimalData yrsinf02 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler4,3);
	public ZonedDecimalData yrsinf03 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler4,6);
	public ZonedDecimalData yrsinf04 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler4,9);
	public ZonedDecimalData yrsinf05 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler4,12);
	public ZonedDecimalData yrsinf06 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler4,15);
	public ZonedDecimalData yrsinf07 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler4,18);
	public ZonedDecimalData yrsinf08 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler4,21);
	public ZonedDecimalData yrsinf09 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler4,24);
	public ZonedDecimalData yrsinf10 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler4,27);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(192).isAPartOf(dataArea, 272);
	public FixedLengthStringData bonussErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] bonusErr = FLSArrayPartOfStructure(10, 4, bonussErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(bonussErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData bonus01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData bonus02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData bonus03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData bonus04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData bonus05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData bonus06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData bonus07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData bonus08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData bonus09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData bonus10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData offsetsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData[] offsetErr = FLSArrayPartOfStructure(10, 4, offsetsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(40).isAPartOf(offsetsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData offset01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData offset02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData offset03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData offset04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData offset05Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);
	public FixedLengthStringData offset06Err = new FixedLengthStringData(4).isAPartOf(filler6, 20);
	public FixedLengthStringData offset07Err = new FixedLengthStringData(4).isAPartOf(filler6, 24);
	public FixedLengthStringData offset08Err = new FixedLengthStringData(4).isAPartOf(filler6, 28);
	public FixedLengthStringData offset09Err = new FixedLengthStringData(4).isAPartOf(filler6, 32);
	public FixedLengthStringData offset10Err = new FixedLengthStringData(4).isAPartOf(filler6, 36);
	public FixedLengthStringData riskunitsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData[] riskunitErr = FLSArrayPartOfStructure(2, 4, riskunitsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(8).isAPartOf(riskunitsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData riskunit01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData riskunit02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData sumasssErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData[] sumassErr = FLSArrayPartOfStructure(10, 4, sumasssErr, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(40).isAPartOf(sumasssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData sumass01Err = new FixedLengthStringData(4).isAPartOf(filler8, 0);
	public FixedLengthStringData sumass02Err = new FixedLengthStringData(4).isAPartOf(filler8, 4);
	public FixedLengthStringData sumass03Err = new FixedLengthStringData(4).isAPartOf(filler8, 8);
	public FixedLengthStringData sumass04Err = new FixedLengthStringData(4).isAPartOf(filler8, 12);
	public FixedLengthStringData sumass05Err = new FixedLengthStringData(4).isAPartOf(filler8, 16);
	public FixedLengthStringData sumass06Err = new FixedLengthStringData(4).isAPartOf(filler8, 20);
	public FixedLengthStringData sumass07Err = new FixedLengthStringData(4).isAPartOf(filler8, 24);
	public FixedLengthStringData sumass08Err = new FixedLengthStringData(4).isAPartOf(filler8, 28);
	public FixedLengthStringData sumass09Err = new FixedLengthStringData(4).isAPartOf(filler8, 32);
	public FixedLengthStringData sumass10Err = new FixedLengthStringData(4).isAPartOf(filler8, 36);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData yrsinfsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData[] yrsinfErr = FLSArrayPartOfStructure(10, 4, yrsinfsErr, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(40).isAPartOf(yrsinfsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData yrsinf01Err = new FixedLengthStringData(4).isAPartOf(filler9, 0);
	public FixedLengthStringData yrsinf02Err = new FixedLengthStringData(4).isAPartOf(filler9, 4);
	public FixedLengthStringData yrsinf03Err = new FixedLengthStringData(4).isAPartOf(filler9, 8);
	public FixedLengthStringData yrsinf04Err = new FixedLengthStringData(4).isAPartOf(filler9, 12);
	public FixedLengthStringData yrsinf05Err = new FixedLengthStringData(4).isAPartOf(filler9, 16);
	public FixedLengthStringData yrsinf06Err = new FixedLengthStringData(4).isAPartOf(filler9, 20);
	public FixedLengthStringData yrsinf07Err = new FixedLengthStringData(4).isAPartOf(filler9, 24);
	public FixedLengthStringData yrsinf08Err = new FixedLengthStringData(4).isAPartOf(filler9, 28);
	public FixedLengthStringData yrsinf09Err = new FixedLengthStringData(4).isAPartOf(filler9, 32);
	public FixedLengthStringData yrsinf10Err = new FixedLengthStringData(4).isAPartOf(filler9, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(576).isAPartOf(dataArea, 464);
	public FixedLengthStringData bonussOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] bonusOut = FLSArrayPartOfStructure(10, 12, bonussOut, 0);
	public FixedLengthStringData[][] bonusO = FLSDArrayPartOfArrayStructure(12, 1, bonusOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(120).isAPartOf(bonussOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] bonus01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] bonus02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] bonus03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] bonus04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] bonus05Out = FLSArrayPartOfStructure(12, 1, filler10, 48);
	public FixedLengthStringData[] bonus06Out = FLSArrayPartOfStructure(12, 1, filler10, 60);
	public FixedLengthStringData[] bonus07Out = FLSArrayPartOfStructure(12, 1, filler10, 72);
	public FixedLengthStringData[] bonus08Out = FLSArrayPartOfStructure(12, 1, filler10, 84);
	public FixedLengthStringData[] bonus09Out = FLSArrayPartOfStructure(12, 1, filler10, 96);
	public FixedLengthStringData[] bonus10Out = FLSArrayPartOfStructure(12, 1, filler10, 108);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData offsetsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 180);
	public FixedLengthStringData[] offsetOut = FLSArrayPartOfStructure(10, 12, offsetsOut, 0);
	public FixedLengthStringData[][] offsetO = FLSDArrayPartOfArrayStructure(12, 1, offsetOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(120).isAPartOf(offsetsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] offset01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] offset02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] offset03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] offset04Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public FixedLengthStringData[] offset05Out = FLSArrayPartOfStructure(12, 1, filler11, 48);
	public FixedLengthStringData[] offset06Out = FLSArrayPartOfStructure(12, 1, filler11, 60);
	public FixedLengthStringData[] offset07Out = FLSArrayPartOfStructure(12, 1, filler11, 72);
	public FixedLengthStringData[] offset08Out = FLSArrayPartOfStructure(12, 1, filler11, 84);
	public FixedLengthStringData[] offset09Out = FLSArrayPartOfStructure(12, 1, filler11, 96);
	public FixedLengthStringData[] offset10Out = FLSArrayPartOfStructure(12, 1, filler11, 108);
	public FixedLengthStringData riskunitsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 300);
	public FixedLengthStringData[] riskunitOut = FLSArrayPartOfStructure(2, 12, riskunitsOut, 0);
	public FixedLengthStringData[][] riskunitO = FLSDArrayPartOfArrayStructure(12, 1, riskunitOut, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(24).isAPartOf(riskunitsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] riskunit01Out = FLSArrayPartOfStructure(12, 1, filler12, 0);
	public FixedLengthStringData[] riskunit02Out = FLSArrayPartOfStructure(12, 1, filler12, 12);
	public FixedLengthStringData sumasssOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 324);
	public FixedLengthStringData[] sumassOut = FLSArrayPartOfStructure(10, 12, sumasssOut, 0);
	public FixedLengthStringData[][] sumassO = FLSDArrayPartOfArrayStructure(12, 1, sumassOut, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(120).isAPartOf(sumasssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] sumass01Out = FLSArrayPartOfStructure(12, 1, filler13, 0);
	public FixedLengthStringData[] sumass02Out = FLSArrayPartOfStructure(12, 1, filler13, 12);
	public FixedLengthStringData[] sumass03Out = FLSArrayPartOfStructure(12, 1, filler13, 24);
	public FixedLengthStringData[] sumass04Out = FLSArrayPartOfStructure(12, 1, filler13, 36);
	public FixedLengthStringData[] sumass05Out = FLSArrayPartOfStructure(12, 1, filler13, 48);
	public FixedLengthStringData[] sumass06Out = FLSArrayPartOfStructure(12, 1, filler13, 60);
	public FixedLengthStringData[] sumass07Out = FLSArrayPartOfStructure(12, 1, filler13, 72);
	public FixedLengthStringData[] sumass08Out = FLSArrayPartOfStructure(12, 1, filler13, 84);
	public FixedLengthStringData[] sumass09Out = FLSArrayPartOfStructure(12, 1, filler13, 96);
	public FixedLengthStringData[] sumass10Out = FLSArrayPartOfStructure(12, 1, filler13, 108);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData yrsinfsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 456);
	public FixedLengthStringData[] yrsinfOut = FLSArrayPartOfStructure(10, 12, yrsinfsOut, 0);
	public FixedLengthStringData[][] yrsinfO = FLSDArrayPartOfArrayStructure(12, 1, yrsinfOut, 0);
	public FixedLengthStringData filler14 = new FixedLengthStringData(120).isAPartOf(yrsinfsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] yrsinf01Out = FLSArrayPartOfStructure(12, 1, filler14, 0);
	public FixedLengthStringData[] yrsinf02Out = FLSArrayPartOfStructure(12, 1, filler14, 12);
	public FixedLengthStringData[] yrsinf03Out = FLSArrayPartOfStructure(12, 1, filler14, 24);
	public FixedLengthStringData[] yrsinf04Out = FLSArrayPartOfStructure(12, 1, filler14, 36);
	public FixedLengthStringData[] yrsinf05Out = FLSArrayPartOfStructure(12, 1, filler14, 48);
	public FixedLengthStringData[] yrsinf06Out = FLSArrayPartOfStructure(12, 1, filler14, 60);
	public FixedLengthStringData[] yrsinf07Out = FLSArrayPartOfStructure(12, 1, filler14, 72);
	public FixedLengthStringData[] yrsinf08Out = FLSArrayPartOfStructure(12, 1, filler14, 84);
	public FixedLengthStringData[] yrsinf09Out = FLSArrayPartOfStructure(12, 1, filler14, 96);
	public FixedLengthStringData[] yrsinf10Out = FLSArrayPartOfStructure(12, 1, filler14, 108);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6635screenWritten = new LongData(0);
	public LongData S6635protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6635ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, riskunit02, yrsinf01, yrsinf02, yrsinf03, yrsinf04, yrsinf05, yrsinf06, yrsinf07, yrsinf08, yrsinf09, yrsinf10, riskunit01, offset01, offset02, offset03, offset04, offset05, offset06, offset07, offset08, offset09, offset10, sumass01, sumass02, sumass03, sumass04, sumass05, sumass06, sumass07, sumass08, sumass09, sumass10, bonus01, bonus02, bonus03, bonus04, bonus05, bonus06, bonus07, bonus08, bonus09, bonus10};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, riskunit02Out, yrsinf01Out, yrsinf02Out, yrsinf03Out, yrsinf04Out, yrsinf05Out, yrsinf06Out, yrsinf07Out, yrsinf08Out, yrsinf09Out, yrsinf10Out, riskunit01Out, offset01Out, offset02Out, offset03Out, offset04Out, offset05Out, offset06Out, offset07Out, offset08Out, offset09Out, offset10Out, sumass01Out, sumass02Out, sumass03Out, sumass04Out, sumass05Out, sumass06Out, sumass07Out, sumass08Out, sumass09Out, sumass10Out, bonus01Out, bonus02Out, bonus03Out, bonus04Out, bonus05Out, bonus06Out, bonus07Out, bonus08Out, bonus09Out, bonus10Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, riskunit02Err, yrsinf01Err, yrsinf02Err, yrsinf03Err, yrsinf04Err, yrsinf05Err, yrsinf06Err, yrsinf07Err, yrsinf08Err, yrsinf09Err, yrsinf10Err, riskunit01Err, offset01Err, offset02Err, offset03Err, offset04Err, offset05Err, offset06Err, offset07Err, offset08Err, offset09Err, offset10Err, sumass01Err, sumass02Err, sumass03Err, sumass04Err, sumass05Err, sumass06Err, sumass07Err, sumass08Err, sumass09Err, sumass10Err, bonus01Err, bonus02Err, bonus03Err, bonus04Err, bonus05Err, bonus06Err, bonus07Err, bonus08Err, bonus09Err, bonus10Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6635screen.class;
		protectRecord = S6635protect.class;
	}

}
