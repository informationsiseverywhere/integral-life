package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class LinspfDAOImpl extends BaseDAOImpl<Linspf> implements LinspfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(LinspfDAOImpl.class);
    
    private static final String UNIQUE_NUMBER = "UNIQUE_NUMBER";

    @Override
    public void updateLinspf(List<Linspf> linspfList){

        if (linspfList != null && linspfList.size() > 0) {
            String SQL_UPDATE = "UPDATE LINSPF SET PAYFLAG='P',JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";

            PreparedStatement psLinsUpdate = getPrepareStatement(SQL_UPDATE);
            try {
                for (Linspf c : linspfList) {
                    psLinsUpdate.setString(1, getJobnm());
                    psLinsUpdate.setString(2, getUsrprf());
                    psLinsUpdate.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
                    psLinsUpdate.setLong(4, c.getUnique_number());
                    psLinsUpdate.addBatch();
                }
                psLinsUpdate.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("updateLinspf()", e);//IJTI-1485
                throw new SQLRuntimeException(e);
            } finally {
                close(psLinsUpdate, null);
            }
        }
    
    }
    
	public boolean insertLinsPF(List<? extends Linspf> linsList){
		boolean isInsertSuccessful = true;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO VM1DTA.LINSPF(CHDRCOY, CHDRNUM, CNTCURR, VALIDFLAG, BRANCH, INSTFROM, INSTTO, INSTAMT01, INSTAMT02, INSTAMT03, INSTAMT04, INSTAMT05, INSTAMT06, INSTFREQ, INSTJCTL, BILLCHNL, PAYFLAG, DUEFLG, TRANSCODE, CBILLAMT, BILLCURR, MANDREF, BILLCD, PAYRSEQNO, TAXRELMTH, ACCTMETH, PRORCNTFEE, PRORAMT, USRPRF, JOBNAME, DATIME, JOBNM)  "); 
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "); 
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());
			int seq;
            for (Linspf lins : linsList) {
            	seq=0;
				ps.setString(++seq, lins.getChdrcoy());
				ps.setString(++seq, lins.getChdrnum());
			    ps.setString(++seq, lins.getCntcurr());
			    ps.setString(++seq, lins.getValidflag());
			    ps.setString(++seq, lins.getBranch());			    
				ps.setInt(++seq, lins.getInstfrom());
				ps.setInt(++seq, lins.getInstto());
				ps.setBigDecimal(++seq, lins.getInstamt01());
				ps.setBigDecimal(++seq, lins.getInstamt02());
				ps.setBigDecimal(++seq, lins.getInstamt03());
				ps.setBigDecimal(++seq, lins.getInstamt04());
				ps.setBigDecimal(++seq, lins.getInstamt05());
				ps.setBigDecimal(++seq, lins.getInstamt06());	
			    ps.setString(++seq, lins.getInstfreq());				
			    ps.setString(++seq, lins.getInstjctl()); 				    
			    ps.setString(++seq, lins.getBillchnl());	
			    ps.setString(++seq, lins.getPayflag());
				ps.setString(++seq, lins.getDueflg());
				ps.setString(++seq, lins.getTranscode());
			    ps.setBigDecimal(++seq, lins.getCbillamt());	
			    ps.setString(++seq, lins.getBillcurr());			    
			    ps.setString(++seq, lins.getMandref());	
				ps.setInt(++seq, lins.getBillcd());				
				ps.setInt(++seq, lins.getPayrseqno());						
			    ps.setString(++seq, lins.getTaxrelmth());	
			    ps.setString(++seq, lins.getAcctmeth());
			    ps.setBigDecimal(++seq, lins.getProrcntfee());//ILIFE-8509
			    ps.setBigDecimal(++seq, lins.getProramt());
			    ps.setString(++seq, this.getUsrprf());
			    ps.setString(++seq, this.getJobnm().trim().length()>8 ? this.getJobnm().trim().substring(0, 7) : this.getJobnm().trim());
			    ps.setTimestamp(++seq, new Timestamp(System.currentTimeMillis()));
			    ps.setString(++seq, this.getJobnm());
			    ps.addBatch();
            }		
			ps.executeBatch();
		}catch (SQLException e) {
			LOGGER.error("insertLinsPF()",e);//IJTI-1485
			isInsertSuccessful = false;
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		return isInsertSuccessful;
	}		  
	
	// Ticket#ILIFE-4404
	public Map<String, List<Linspf>> searchLinsrnlRecord(List<String> chdrnumList){
		
		if(chdrnumList == null || chdrnumList.isEmpty()){
			return null;
		}
		StringBuilder sqlSelect1 = new StringBuilder();
		sqlSelect1.append("SELECT UNIQUE_NUMBER, CHDRCOY,CHDRNUM,CNTCURR,VALIDFLAG,BRANCH,INSTFROM,INSTTO,INSTAMT01,INSTAMT02,INSTAMT03,INSTAMT04,INSTAMT05,INSTAMT06,INSTFREQ,INSTJCTL,BILLCHNL,PAYFLAG,DUEFLG,TRANSCODE,CBILLAMT,BILLCURR,MANDREF,BILLCD,PAYRSEQNO,TAXRELMTH,ACCTMETH ");
	    sqlSelect1.append(" FROM LINSPF WHERE PAYFLAG <> 'P' AND ");
	    sqlSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
	    sqlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, INSTFROM ASC, UNIQUE_NUMBER DESC ");
	
	    PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
	    ResultSet sqlrs = null;
	    Map<String, List<Linspf>> resultMap = new HashMap<>();
	    try {
	    	sqlrs = executeQuery(psSelect);
	
	        while (sqlrs.next()) {
	        	
				Linspf linspf = new Linspf();
				linspf.setUnique_number(sqlrs.getLong(UNIQUE_NUMBER));
				linspf.setChdrcoy(sqlrs.getString("Chdrcoy"));
				linspf.setChdrnum(sqlrs.getString("Chdrnum"));
				linspf.setCntcurr(sqlrs.getString("Cntcurr"));
				linspf.setValidflag(sqlrs.getString("Validflag"));
				linspf.setBranch(sqlrs.getString("Branch"));
				linspf.setInstfrom(sqlrs.getInt("Instfrom"));
				linspf.setInstto(sqlrs.getInt("Instto"));
				linspf.setInstamt01(sqlrs.getBigDecimal("Instamt01"));
				linspf.setInstamt02(sqlrs.getBigDecimal("Instamt02"));
				linspf.setInstamt03(sqlrs.getBigDecimal("Instamt03"));
				linspf.setInstamt04(sqlrs.getBigDecimal("Instamt04"));
				linspf.setInstamt05(sqlrs.getBigDecimal("Instamt05"));
				linspf.setInstamt06(sqlrs.getBigDecimal("Instamt06"));
				linspf.setInstfreq(sqlrs.getString("Instfreq"));
				linspf.setInstjctl(sqlrs.getString("Instjctl"));
				linspf.setBillchnl(sqlrs.getString("Billchnl"));
				linspf.setPayflag(sqlrs.getString("Payflag"));
				linspf.setDueflg(sqlrs.getString("Dueflg"));
				linspf.setTranscode(sqlrs.getString("Transcode"));
				linspf.setCbillamt(sqlrs.getBigDecimal("Cbillamt"));
				linspf.setBillcurr(sqlrs.getString("Billcurr"));
				linspf.setMandref(sqlrs.getString("Mandref"));
				linspf.setBillcd(sqlrs.getInt("Billcd"));
				linspf.setPayrseqno(sqlrs.getInt("Payrseqno"));
				linspf.setTaxrelmth(sqlrs.getString("Taxrelmth"));
				linspf.setAcctmeth(sqlrs.getString("Acctmeth"));
				
				if (resultMap.containsKey(linspf.getChdrnum())) {
					resultMap.get(linspf.getChdrnum()).add(linspf);
				} else {
					List<Linspf> linspfList = new ArrayList<>();
					linspfList.add(linspf);
					resultMap.put(linspf.getChdrnum(), linspfList);
				}
	        }
	    } catch (SQLException e) {
	        LOGGER.error("searchLinsrnlRecord()", e);//IJTI-1485
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(psSelect, sqlrs);
	    }
		return resultMap;
	}
	
	// Ticket#ILIFE-4404
    public void updateLinspfDueflg(List<Linspf> linspfList){

        if (linspfList != null && !linspfList.isEmpty()) {
            String SQL_UPDATE = "UPDATE LINSPF SET Dueflg=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";

            PreparedStatement psLinsUpdate = getPrepareStatement(SQL_UPDATE);
            try {
                for (Linspf c : linspfList) {
                	psLinsUpdate.setString(1, c.getDueflg());
                    psLinsUpdate.setString(2, getJobnm());
                    psLinsUpdate.setString(3, getUsrprf());
                    psLinsUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
                    psLinsUpdate.setLong(5, c.getUnique_number());
                    psLinsUpdate.addBatch();
                }
                psLinsUpdate.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("updateLinspfDueflg()", e);//IJTI-1485
                throw new SQLRuntimeException(e);
            } finally {
                close(psLinsUpdate, null);
            }
        }
    
    }
    
    

    //ILIFE-5975
	@Override
	public Linspf getTotBilAmt(Linspf linspf) {
		StringBuilder sqlSchemeSelect1 = new StringBuilder(
				"SELECT SUM(CBILLAMT) CBILLAMT FROM LINSPF WHERE CHDRNUM=? AND VALIDFLAG='1' AND PAYFLAG=?");
		Linspf linspfRec = null;
		
		try {
		  PreparedStatement ps = getPrepareStatement(sqlSchemeSelect1.toString());
		  	ps.setString(1, linspf.getChdrnum());
			ps.setString(2, linspf.getPayflag());
			ResultSet rs = null;
				rs = ps.executeQuery();
			    if (rs.next()) {		
			    	linspfRec= new Linspf();
					linspfRec.setCbillamt(rs.getBigDecimal("CBILLAMT")!=null?rs.getBigDecimal("CBILLAMT"):BigDecimal.ZERO);
				}
		}
			catch(SQLException e)
			{
				LOGGER.error(" LinspfDAOImpl.getLinspfRecord()",e);//IJTI-1485
				throw new SQLRuntimeException(e);	
			}
			
			return linspfRec;
	}
	    //Ticket #ILIFE-5362
    public Linspf getLinsRecordByCoyAndNumAndflag(String chdrcoy, String chdrnum, String validflag){
    	StringBuilder sql = new StringBuilder("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, CNTCURR, BILLCURR, VALIDFLAG, BRANCH, INSTFROM, INSTTO, INSTAMT01, ");
    	sql.append("INSTAMT02, INSTAMT03, INSTAMT04,  INSTAMT05, INSTAMT06, INSTFREQ, INSTJCTL, CBILLAMT, BILLCHNL, PAYFLAG, DUEFLG, TRANSCODE, MANDREF, ");
    	sql.append("BILLCD, PAYRSEQNO, TAXRELMTH, ACCTMETH FROM LINSPF WHERE CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG=? ORDER BY CHDRCOY ASC, CHDRNUM ASC, UNIQUE_NUMBER ASC");
 
    	LOGGER.info(sql.toString());
    	
    	PreparedStatement ps = null;
		ResultSet rs = null;
		Linspf linspf = null;
		
		try{
			ps = getPrepareStatement(sql.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, validflag);
			
			rs = executeQuery(ps);
					
			if(rs.next()){
				
				linspf = new Linspf();
				
				linspf.setUnique_number(rs.getLong(UNIQUE_NUMBER));
				linspf.setChdrcoy(rs.getString("CHDRCOY"));
				linspf.setChdrnum(rs.getString("CHDRNUM"));
				linspf.setCntcurr(rs.getString("CNTCURR"));
				linspf.setBillcurr(rs.getString("BILLCURR"));
				linspf.setValidflag(rs.getString("VALIDFLAG"));
				linspf.setBranch(rs.getString("BRANCH"));
				linspf.setInstfrom(rs.getInt("INSTFROM"));
				linspf.setInstto(rs.getInt("INSTTO"));
				linspf.setInstamt01(rs.getBigDecimal("INSTAMT01"));
				linspf.setInstamt02(rs.getBigDecimal("INSTAMT02"));
				linspf.setInstamt03(rs.getBigDecimal("INSTAMT03"));
				linspf.setInstamt04(rs.getBigDecimal("INSTAMT04"));
				linspf.setInstamt05(rs.getBigDecimal("INSTAMT05"));
				linspf.setInstamt06(rs.getBigDecimal("INSTAMT06"));
				linspf.setInstfreq(rs.getString("INSTFREQ"));
				linspf.setInstjctl(rs.getString("INSTJCTL"));
				linspf.setCbillamt(rs.getBigDecimal("CBILLAMT"));
				linspf.setBillchnl(rs.getString("BILLCHNL"));
				linspf.setPayflag(rs.getString("PAYFLAG"));
				linspf.setDueflg(rs.getString("DUEFLG"));
				linspf.setTranscode(rs.getString("TRANSCODE"));
				linspf.setMandref(rs.getString("MANDREF"));
				linspf.setBillcd(rs.getInt("BILLCD"));
				linspf.setPayrseqno(rs.getInt("PAYRSEQNO"));
				linspf.setTaxrelmth(rs.getString("TAXRELMTH"));
				linspf.setAcctmeth(rs.getString("ACCTMETH"));
			}
			
			
		}catch(SQLException e){
			LOGGER.error("getLinsRecordByCoyAndNumAndflag()",e);//IJTI-1485
			throw new SQLRuntimeException(e);
		}finally{
			close(ps,rs);
		}
		
		return linspf;
    }

    public List<Linspf> searchLinscfiRecord(String coy,String chdrnum){
		
		StringBuilder sqlSelect1 = new StringBuilder();
		sqlSelect1.append("SELECT UNIQUE_NUMBER FROM LINSCFI WHERE CHDRCOY=? AND CHDRNUM = ? ");
	    sqlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, UNIQUE_NUMBER ASC ");
	
	    PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
	    ResultSet sqlrs = null;
	    List<Linspf> linspfList = new LinkedList<>();
	    try {
	    	psSelect.setString(1, coy);
	    	psSelect.setString(2, chdrnum);
	    	sqlrs = executeQuery(psSelect);
	
	        while (sqlrs.next()) {
				Linspf linspf = new Linspf();
				linspf.setUnique_number(sqlrs.getLong(UNIQUE_NUMBER));
				linspfList.add(linspf);
	        }
	    } catch (SQLException e) {
	        LOGGER.error("searchLinscfiRecord()", e);//IJTI-1485
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(psSelect, sqlrs);
	    }
		return linspfList;
	}
    
	public void deleteLinspfRecord(List<Linspf> linspfList){
		String sql = "DELETE FROM LINSPF WHERE UNIQUE_NUMBER=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		try {
			for(Linspf l:linspfList){
				ps.setLong(1, l.getUnique_number());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("deleteLinspfRecord()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

	protected final String LINSPF_COLUMNS = " CHDRCOY, CHDRNUM, PAYRSEQNO, INSTFROM, BILLCD, CNTCURR, BILLCURR, BILLCHNL, CBILLAMT, INSTAMT01, INSTAMT02, INSTAMT03, INSTAMT04, INSTAMT05, INSTAMT06, INSTFREQ ";

	@Override
	public int copyDataToTempTable(String sourceTableName, String tempTableName, int noOfSubseqThreads,	int wsaaEffdate, String wsaaCompany, String wsaaChdrnumFrom, String wsaaChdrnumTo) {
		/*
		* Real implementation for this method is provided in database type specific sub-class
		* 
		* Empty implementation is provided here to make this class non-abstract so that object of this class
		* can be used to call other methods which are not dependent on database type. 
		*/
		return 0;
	}
	public Map<String,Integer> searchTdayBillCDContractNos(List<String> chdrnumList,String chdrcoy)
	{
		StringBuilder sb = new StringBuilder("select CHDRNUM,MAX(BILLCD)BILLCD FROM LINSPF WHERE CHDRCOY=? AND ");
		sb.append(getSqlInStr("CHDRNUM", chdrnumList));
		sb.append(" GROUP BY CHDRNUM,CHDRCOY");		
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<String,Integer> data = new LinkedHashMap<>();
		try{
			ps = getPrepareStatement(sb.toString());				
			ps.setString(1, chdrcoy);          
			rs = ps.executeQuery();
			
			while(rs.next())
			{				
				data.put(rs.getString(1),rs.getInt(2));
			}
			
		}catch (SQLException e) {			
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}	
		return data;
	}
	
	
	//IJTI-1727 starts
	/*
	 * (non-Javadoc)
	 * @see com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO#getLinspfForCollection(com.csc.life.regularprocessing.dataaccess.model.Linspf)
	 */
	public List<Linspf> getLinspfForCollection(Linspf linspfInput) {
		StringBuilder sql = new StringBuilder(
				"SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, CNTCURR, VALIDFLAG, BRANCH, INSTFROM,");
		sql.append(" INSTTO, INSTAMT01, INSTAMT02, INSTAMT03, INSTAMT04, INSTAMT05, INSTAMT06,");
		sql.append(" INSTFREQ, INSTJCTL, BILLCHNL, PAYFLAG, DUEFLG, TRANSCODE, CBILLAMT, BILLCURR,");
		sql.append(" MANDREF, BILLCD, PAYRSEQNO, TAXRELMTH, ACCTMETH, USRPRF, JOBNAME, DATIME,");
		sql.append(" JOBNM, PRORCNTFEE, PRORAMT FROM LINSPF");
		sql.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG=? AND PAYFLAG != ? AND BILLCHNL != ?");
		sql.append(" ORDER BY INSTFROM ASC, UNIQUE_NUMBER ASC");
		List<Linspf> linspfOutputList = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = getPrepareStatement(sql.toString());
			ps.setString(1, linspfInput.getChdrcoy());
			ps.setString(2, linspfInput.getChdrnum());
			ps.setString(3, linspfInput.getValidflag());
			ps.setString(4, linspfInput.getPayflag());
			ps.setString(5, linspfInput.getBillchnl());
			rs = ps.executeQuery();
			while (rs.next()) {
				linspfOutputList.add(prepareLinspf(rs));

			}
		} catch (SQLException e) {
			LOGGER.error("Error occurred when reading LINSPF data", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}

		return linspfOutputList;
	}
	
	private Linspf prepareLinspf(ResultSet rs) throws SQLException{
		Linspf linspf = new Linspf();
		linspf.setUnique_number(rs.getLong(UNIQUE_NUMBER));
		linspf.setChdrcoy(rs.getString("CHDRCOY"));
		linspf.setChdrnum(rs.getString("CHDRNUM"));
		linspf.setCntcurr(rs.getString("CNTCURR"));
		linspf.setValidflag(rs.getString("VALIDFLAG"));
		linspf.setBranch(rs.getString("BRANCH"));
		linspf.setInstfrom(rs.getInt("INSTFROM"));
		linspf.setInstto(rs.getInt("INSTTO"));
		linspf.setInstamt01(rs.getBigDecimal("INSTAMT01"));
		linspf.setInstamt02(rs.getBigDecimal("INSTAMT02"));
		linspf.setInstamt03(rs.getBigDecimal("INSTAMT03"));
		linspf.setInstamt04(rs.getBigDecimal("INSTAMT04"));
		linspf.setInstamt05(rs.getBigDecimal("INSTAMT05"));
		linspf.setInstamt06(rs.getBigDecimal("INSTAMT06"));
		linspf.setInstfreq(rs.getString("INSTFREQ"));
		linspf.setInstjctl(rs.getString("INSTJCTL"));
		linspf.setBillchnl(rs.getString("BILLCHNL"));
		linspf.setPayflag(rs.getString("PAYFLAG"));
		linspf.setDueflg(rs.getString("DUEFLG"));
		linspf.setTranscode(rs.getString("TRANSCODE"));
		linspf.setCbillamt(rs.getBigDecimal("CBILLAMT"));
		linspf.setBillcurr(rs.getString("BILLCURR"));
		linspf.setMandref(rs.getString("MANDREF"));
		linspf.setBillcd(rs.getInt("BILLCD"));
		linspf.setPayrseqno(rs.getInt("PAYRSEQNO"));
		linspf.setTaxrelmth(rs.getString("TAXRELMTH"));
		linspf.setAcctmeth(rs.getString("ACCTMETH"));
		linspf.setUsrprf(rs.getString("USRPRF"));
		linspf.setJobname(rs.getString("JOBNM"));
		linspf.setDatime(rs.getString("DATIME"));
		linspf.setProrcntfee(rs.getBigDecimal("PRORCNTFEE"));
		linspf.setProramt(rs.getBigDecimal("PRORAMT"));
		return linspf;
	}

	@Override
	public Linspf getProratedRecord(String coy, String chdrnum, String proraterec) {
		StringBuilder sqlSelect = new StringBuilder("SELECT * FROM LINSPF WHERE CHDRCOY=? AND CHDRNUM=? AND PRORATEREC=? AND VALIDFLAG='1' AND PAYFLAG!='P' ORDER BY UNIQUE_NUMBER DESC ");
		Linspf linspfRec = null;
		try {
		  PreparedStatement ps = getPrepareStatement(sqlSelect.toString());
		  	ps.setString(1, coy);
			ps.setString(2, chdrnum);
			ps.setString(3, proraterec);
			ResultSet rs = null;
			rs = ps.executeQuery();
		    if (rs.next()) {		
		    	linspfRec= new Linspf();
		    	linspfRec.setUnique_number(rs.getLong(UNIQUE_NUMBER));
		    	linspfRec.setChdrcoy(rs.getString("Chdrcoy"));
		    	linspfRec.setChdrnum(rs.getString("Chdrnum"));
		    	linspfRec.setCntcurr(rs.getString("Cntcurr"));
		    	linspfRec.setValidflag(rs.getString("Validflag"));
		    	linspfRec.setBranch(rs.getString("Branch"));
		    	linspfRec.setInstfrom(rs.getInt("Instfrom"));
		    	linspfRec.setInstto(rs.getInt("Instto"));
		    	linspfRec.setInstamt01(rs.getBigDecimal("Instamt01"));
				linspfRec.setInstamt02(rs.getBigDecimal("Instamt02"));
				linspfRec.setInstamt03(rs.getBigDecimal("Instamt03"));
				linspfRec.setInstamt04(rs.getBigDecimal("Instamt04"));
				linspfRec.setInstamt05(rs.getBigDecimal("Instamt05"));
				linspfRec.setInstamt06(rs.getBigDecimal("Instamt06"));
				linspfRec.setInstfreq(rs.getString("Instfreq"));
				linspfRec.setInstjctl(rs.getString("Instjctl"));
				linspfRec.setBillchnl(rs.getString("Billchnl"));
				linspfRec.setPayflag(rs.getString("Payflag"));
				linspfRec.setDueflg(rs.getString("Dueflg"));
				linspfRec.setTranscode(rs.getString("Transcode"));
				linspfRec.setCbillamt(rs.getBigDecimal("Cbillamt"));
				linspfRec.setBillcurr(rs.getString("Billcurr"));
				linspfRec.setMandref(rs.getString("Mandref"));
				linspfRec.setBillcd(rs.getInt("Billcd"));
				linspfRec.setPayrseqno(rs.getInt("Payrseqno"));
				linspfRec.setTaxrelmth(rs.getString("Taxrelmth"));
				linspfRec.setAcctmeth(rs.getString("Acctmeth"));
				linspfRec.setProraterec(rs.getString("PRORATEREC"));
			}
		}
		catch(SQLException e) {
			LOGGER.error(" LinspfDAOImpl.getProratedRecord()",e);
			throw new SQLRuntimeException(e);	
		}
		return linspfRec;
	}
	
	//IJTI-1727 ends
	/**
	 * //Ticket #PINNACLE - 1363 starts
	 */
	@Override
	public List<Linspf> getPaidLinspfRecordList(String chdrnum) {
		StringBuilder sqlSchemeSelect1 = new StringBuilder(
				"SELECT * FROM LINSPF WHERE CHDRNUM=? AND VALIDFLAG='1' and PAYFLAG='P' ORDER BY BILLCD DESC");
		List<Linspf> tmpList = new ArrayList();
		try {
		  PreparedStatement ps = getPrepareStatement(sqlSchemeSelect1.toString());
		  	ps.setString(1, chdrnum);
			ResultSet rs = null;
				rs = ps.executeQuery();
			    while (rs.next()) {		
			    	Linspf linspfRec= new Linspf();
			    	linspfRec.setUnique_number(rs.getLong(UNIQUE_NUMBER));
			    	linspfRec.setChdrcoy(rs.getString("Chdrcoy"));
			    	linspfRec.setChdrnum(rs.getString("Chdrnum"));
			    	linspfRec.setCntcurr(rs.getString("Cntcurr"));
			    	linspfRec.setValidflag(rs.getString("Validflag"));
			    	linspfRec.setBranch(rs.getString("Branch"));
			    	linspfRec.setInstfrom(rs.getInt("Instfrom"));
			    	linspfRec.setInstto(rs.getInt("Instto"));
			    	linspfRec.setInstamt01(rs.getBigDecimal("Instamt01"));
					linspfRec.setInstamt02(rs.getBigDecimal("Instamt02"));
					linspfRec.setInstamt03(rs.getBigDecimal("Instamt03"));
					linspfRec.setInstamt04(rs.getBigDecimal("Instamt04"));
					linspfRec.setInstamt05(rs.getBigDecimal("Instamt05"));
					linspfRec.setInstamt06(rs.getBigDecimal("Instamt06"));
					linspfRec.setInstfreq(rs.getString("Instfreq"));
					linspfRec.setInstjctl(rs.getString("Instjctl"));
					linspfRec.setBillchnl(rs.getString("Billchnl"));
					linspfRec.setPayflag(rs.getString("Payflag"));
					linspfRec.setDueflg(rs.getString("Dueflg"));
					linspfRec.setTranscode(rs.getString("Transcode"));
					linspfRec.setCbillamt(rs.getBigDecimal("Cbillamt"));
					linspfRec.setBillcurr(rs.getString("Billcurr"));
					linspfRec.setMandref(rs.getString("Mandref"));
					linspfRec.setBillcd(rs.getInt("Billcd"));
					linspfRec.setPayrseqno(rs.getInt("Payrseqno"));
					linspfRec.setTaxrelmth(rs.getString("Taxrelmth"));
					linspfRec.setAcctmeth(rs.getString("Acctmeth"));
					tmpList.add(linspfRec);
				}
		}
			catch(SQLException e)
			{
				LOGGER.error(" LinspfDAOImpl.getLinspfRecord()",e);//IJTI-1485
				throw new SQLRuntimeException(e);	
			}
			
			return tmpList;
	}
	//Ticket #PINNACLE - 1363 ends
	
	@Override
	public Linspf getProratedRecordDishonored(String coy, String chdrnum, String proraterec, int instfrom, int instto) {
		StringBuilder sqlSelect = new StringBuilder("SELECT * FROM LINSPF WHERE CHDRCOY=? AND CHDRNUM=? AND PRORATEREC=? AND INSTFROM=? AND INSTTO=? ORDER BY UNIQUE_NUMBER DESC ");
		Linspf linspfRec = null;
		try {
		  PreparedStatement ps = getPrepareStatement(sqlSelect.toString());
		  	ps.setString(1, coy);
			ps.setString(2, chdrnum);
			ps.setString(3, proraterec);
			ps.setInt(4, instfrom);
			ps.setInt(5, instto);
			ResultSet rs = null;
			rs = ps.executeQuery();
		    if (rs.next()) {		
		    	linspfRec= new Linspf();
		    	linspfRec.setUnique_number(rs.getLong(UNIQUE_NUMBER));
		    	linspfRec.setChdrcoy(rs.getString("Chdrcoy"));
		    	linspfRec.setChdrnum(rs.getString("Chdrnum"));
		    	linspfRec.setCntcurr(rs.getString("Cntcurr"));
		    	linspfRec.setValidflag(rs.getString("Validflag"));
		    	linspfRec.setBranch(rs.getString("Branch"));
		    	linspfRec.setInstfrom(rs.getInt("Instfrom"));
		    	linspfRec.setInstto(rs.getInt("Instto"));
		    	linspfRec.setInstamt01(rs.getBigDecimal("Instamt01"));
				linspfRec.setInstamt02(rs.getBigDecimal("Instamt02"));
				linspfRec.setInstamt03(rs.getBigDecimal("Instamt03"));
				linspfRec.setInstamt04(rs.getBigDecimal("Instamt04"));
				linspfRec.setInstamt05(rs.getBigDecimal("Instamt05"));
				linspfRec.setInstamt06(rs.getBigDecimal("Instamt06"));
				linspfRec.setInstfreq(rs.getString("Instfreq"));
				linspfRec.setInstjctl(rs.getString("Instjctl"));
				linspfRec.setBillchnl(rs.getString("Billchnl"));
				linspfRec.setPayflag(rs.getString("Payflag"));
				linspfRec.setDueflg(rs.getString("Dueflg"));
				linspfRec.setTranscode(rs.getString("Transcode"));
				linspfRec.setCbillamt(rs.getBigDecimal("Cbillamt"));
				linspfRec.setBillcurr(rs.getString("Billcurr"));
				linspfRec.setMandref(rs.getString("Mandref"));
				linspfRec.setBillcd(rs.getInt("Billcd"));
				linspfRec.setPayrseqno(rs.getInt("Payrseqno"));
				linspfRec.setTaxrelmth(rs.getString("Taxrelmth"));
				linspfRec.setAcctmeth(rs.getString("Acctmeth"));
				linspfRec.setProraterec(rs.getString("PRORATEREC"));
			}
		}
		catch(SQLException e) {
			LOGGER.error(" LinspfDAOImpl.getProratedRecordDishonored()",e);
			throw new SQLRuntimeException(e);	
		}
		return linspfRec;
	}
}
