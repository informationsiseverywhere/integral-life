/*
 * File: B6689.java
 * Date: 29 August 2009 21:24:33
 * Author: Quipoz Limited
 * 
 * Class transformed from B6689.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.general.dataaccess.AcaxpfTableDAM;
import com.csc.fsu.general.dataaccess.dao.AcaxpfDAO;
import com.csc.fsu.general.dataaccess.model.Acaxpf;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*                     DEFERRED AGENT POSTINGS
*                    -------------------------
*
* This program updates the ACBL balances for agents if this
* processing has been deferred from collections.
*
* Due to the way Collections works, if multi-thread processing
* is used, it is possible for the processing to abort if one of
* if one of the threads tries to access an agent account balance
* (ACBL) record which is already held for update by another
* thread.
* To avoid this situation, it is now possible to defer the
* updates to the ACBL to allow the update records to be sorted
* and split in such a way as not to cause a clash.
*
* This program reads through the ACAX splitter file, accumulates
* the balance and on change of Key, updates the ACBLPF.
*
* Control Totals used in this program:
*
*    CT01 - Number of Agent records updated.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class B6689 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private AcaxpfTableDAM acaxpf = new AcaxpfTableDAM();
	//private AcaxpfTableDAM acaxpfRec = new AcaxpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B6689");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaAcaxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaAcaxFn, 0, FILLER).init("ACAX");
	private FixedLengthStringData wsaaAcaxRunid = new FixedLengthStringData(2).isAPartOf(wsaaAcaxFn, 4);
	private ZonedDecimalData wsaaAcaxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaAcaxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

	private FixedLengthStringData wsaaFirstTimeIn = new FixedLengthStringData(1).init("Y");
	private Validator firstTimeIn = new Validator(wsaaFirstTimeIn, "Y");
	private ZonedDecimalData wsaaSacscurbal = new ZonedDecimalData(17, 2);
	private FixedLengthStringData wsaaEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaAcaxKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaAcaxRldgcoy = new FixedLengthStringData(1).isAPartOf(wsaaAcaxKey, 0);
	private FixedLengthStringData wsaaAcaxSacscode = new FixedLengthStringData(2).isAPartOf(wsaaAcaxKey, 1);
	private FixedLengthStringData wsaaAcaxRldgacct = new FixedLengthStringData(16).isAPartOf(wsaaAcaxKey, 3);
	private FixedLengthStringData wsaaAcaxOrigcurr = new FixedLengthStringData(3).isAPartOf(wsaaAcaxKey, 19);
	private FixedLengthStringData wsaaAcaxSacstyp = new FixedLengthStringData(2).isAPartOf(wsaaAcaxKey, 22);
		/* FORMATS */
	private String acblrec = "ACBLREC";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Subsidiary account balance*/
	//private AcblTableDAM acblIO = new AcblTableDAM();
	
	// ILIFE-4397: Starts
	private int intBatchExtractSize;
    private int intBatchID = 0;
    private AcaxpfDAO acaxpfDAO = getApplicationContext().getBean("acaxpfDAO", AcaxpfDAO.class);
    private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
    private Map<String, List<Acblpf>> acblMap; 
    private Acblpf acblIO;
    private List<Acblpf> insertAcblpfList;
    private List<Acblpf> updateAcblpfList;
    private Iterator<Acaxpf> iteratorList;
    private Acaxpf acaxpfRec = null;
    private int ct01Value = 0;
 // ILIFE-4397: Ends

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		accumTotals3040, 
		exit3090
	}

	public B6689() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		wsaaAcaxRunid.set(bprdIO.getSystemParam04());
		wsaaAcaxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append("OVRDBF FILE(ACAXPF) TOFILE(");
		stringVariable1.append(delimitedExp(bprdIO.getRunLibrary(), SPACES));
		stringVariable1.append("/");
		stringVariable1.append(wsaaAcaxFn.toString());
		stringVariable1.append(") MBR(");
		stringVariable1.append(wsaaThreadMember.toString());
		stringVariable1.append(")  SEQONLY(*YES 1000)");
		wsaaQcmdexc.setLeft(stringVariable1.toString());
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		// ILIFE-4397: Starts
		if (bprdIO.systemParam01.isNumeric()){
            if (bprdIO.systemParam01.toInt() > 0){
                intBatchExtractSize = bprdIO.systemParam01.toInt();
            }else{
                intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
            }
        }else{
            intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();  
        }
		readChunkRecord();
		// ILIFE-4397: Ends
		/*EXIT*/
	}

//ILIFE-4397: Starts
private void readChunkRecord(){
	List<Acaxpf> hitxpfList = acaxpfDAO.getAcaxpfRecords(wsaaAcaxFn.toString(), 
			wsaaThreadMember.toString(), intBatchExtractSize, intBatchID);
	iteratorList = hitxpfList.iterator();
	List<String> rldgacctList = new ArrayList<>();
	if(hitxpfList != null){
		for(Acaxpf a:hitxpfList){
			rldgacctList.add(a.getRldgacct());
		}
		acblMap = acblpfDAO.searchAcblRecordByChdrnumList(rldgacctList);
	}
}
//ILIFE-4397: Ends

protected void readFile2000()
	{
		// ILIFE-4397: Starts
		if (iteratorList.hasNext()) {
			acaxpfRec = iteratorList.next();
		} else {
			intBatchID++;
			acaxpfRec = null;
			readChunkRecord();
			if (iteratorList.hasNext()) {
				acaxpfRec = iteratorList.next();
			} else {
				wsspEdterror.set(Varcom.endp);
			}
		}
		// ILIFE-4397: Ends
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					update3010();
					readAcbl3020();
					setWriteFunction3030();
				}
				case accumTotals3040: {
					accumTotals3040();
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void update3010()
	{
		if (isEQ(wsaaEdterror,varcom.endp)) {
			wsaaAcaxKey.set(SPACES);
		}
		else {
			wsaaAcaxRldgcoy.set(acaxpfRec.getRldgcoy());
			wsaaAcaxSacscode.set(acaxpfRec.getSacscode());
			wsaaAcaxRldgacct.set(acaxpfRec.getRldgacct());
			wsaaAcaxOrigcurr.set(acaxpfRec.getOrigcurr());
			wsaaAcaxSacstyp.set(acaxpfRec.getSacstyp());
		}
		if (firstTimeIn.isTrue()) {
			wsaaFirstTimeIn.set("N");
		}
		else {
			if (acblIO.getRldgcoy().equals(wsaaAcaxRldgcoy.toString())
					&& acblIO.getSacscode().equals(wsaaAcaxSacscode.toString())
					&& acblIO.getOrigcurr().equals(wsaaAcaxOrigcurr.toString())
					&& acblIO.getSacstyp().equals(wsaaAcaxSacstyp.toString())
					&& acblIO.getRldgacct().equals(wsaaAcaxRldgacct.toString())) {
				goTo(GotoLabel.accumTotals3040);
			}
			else {
				acblIO.setSacscurbal(wsaaSacscurbal.getbigdata());
				if(acblIO.getUniqueNumber() == null){
					if(insertAcblpfList == null){
						insertAcblpfList = new ArrayList<>();
					}
					insertAcblpfList.add(acblIO);
				}else{
					if(updateAcblpfList == null){
						updateAcblpfList = new ArrayList<>();
					}
					updateAcblpfList.add(acblIO);
					ct01Value++;
				}
			}
		}
		if (isEQ(wsaaEdterror,varcom.endp)) {
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit3090);
		}
	}

protected void readAcbl3020()
 {
		acblIO = null;
		if (acblMap != null && acblMap.containsKey(wsaaAcaxRldgacct)) {
			for (Acblpf al : acblMap.get(wsaaAcaxRldgacct)) {
				if (al.getRldgcoy().equals(wsaaAcaxRldgcoy.toString())
						&& al.getSacscode().equals(wsaaAcaxSacscode.toString())
						&& al.getOrigcurr().equals(wsaaAcaxOrigcurr.toString())
						&& al.getSacstyp().equals(wsaaAcaxSacstyp.toString())) {
					acblIO = al;
					break;
				}
			}
		}
	}

protected void setWriteFunction3030()
	{
		if (acblIO != null) {
			wsaaSacscurbal.set(acblIO.getSacscurbal());
		}
		else {
			acblIO = new Acblpf();
			acblIO.setRldgcoy(wsaaAcaxRldgcoy.toString());
			acblIO.setSacscode(wsaaAcaxSacscode.toString());
			acblIO.setOrigcurr(wsaaAcaxOrigcurr.toString());
			acblIO.setSacstyp(wsaaAcaxSacstyp.toString());
			acblIO.setRldgacct(wsaaAcaxRldgacct.toString());
			wsaaSacscurbal.set(ZERO);
		}
	}

protected void accumTotals3040()
	{
		PackedDecimalData sacscurbal = new PackedDecimalData(17, 2);
		sacscurbal.set(acaxpfRec.getSacscurbal());
		if (isEQ(acaxpfRec.getGlsign(),"-")) {
			wsaaSacscurbal.subtract(sacscurbal);
		}
		else {
			wsaaSacscurbal.add(sacscurbal);
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		contotrec.totval.set(ct01Value);
		contotrec.totno.set(ct01);
		callContot001();
		if(insertAcblpfList!=null&&!insertAcblpfList.isEmpty()){
			acblpfDAO.insertAcblpfList(insertAcblpfList);
		}
		if(updateAcblpfList!=null&&!updateAcblpfList.isEmpty()){
			acblpfDAO.updateAcblRecord(updateAcblpfList);
		}
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		acaxpf.close();
		wsaaQcmdexc.set("DLTOVR FILE(ACAXPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
