/*
 * File: Bd5hs.java
 * Date: 23 August 2018 22:30:12
 *
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.ALPHANUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.financials.procedures.Payreq;
import com.csc.fsu.financials.recordstructures.Payreqrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.reassurance.dataaccess.dao.AcmvpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.PyoupfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Pyoupf;
import com.csc.life.terminationclaims.tablestructures.Td5herec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Acmvpf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* *
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.

*   The basic procedure division logic is for reading via SQL and
*     printing a simple input primary file. The overall structure
*     is as follows:
*
*   Initialise
*     - retrieve and set up standard report headings.
*
*   Read
*     - read first primary file record
*
*   Perform     Until End of File
*
*      Edit
*       - Check if the primary file record is required
*       - Softlock it if the record is to be updated
*
*      Update
*       - update database files
*       - write details to report while not primary file EOF
*       - look up referred to records for output details
*       - if new page, write headings
*       - write details
*
*      Read next primary file record
*
*    End Perform
*
*   Control totals:
*     01  -  Number of pages printed
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*   These remarks must be replaced by what the program actually
*     does.
*
***********************************************************************
* </pre>
*/

public class Bd5hs extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BD5HS");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(2);
	private PackedDecimalData wsaaSacscurbal = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaTd5heKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrancode = new FixedLengthStringData(4).isAPartOf(wsaaTd5heKey, 0);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaTd5heKey, 4);

	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaTrandesc = new FixedLengthStringData(30);
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(4, 0);

	private FixedLengthStringData wsaaDatimeInit = new FixedLengthStringData(26);
	private FixedLengthStringData wsaaDatimeDate = new FixedLengthStringData(10).isAPartOf(wsaaDatimeInit, 0);
	private FixedLengthStringData wsaaDatimeTime = new FixedLengthStringData(10).isAPartOf(wsaaDatimeInit, 10);
	private FixedLengthStringData wsaaUser = new FixedLengthStringData(6).isAPartOf(wsaaDatimeInit, 20);
		/* TABLES */
	private static final String t3629 = "T3629";
	private static final String t5645 = "T5645";
	private static final String td5he = "TD5HE";
	private static final String t1688 = "T1688";
	
		/* ERRORS */
	private static final String hl63 = "HL63";
	private static final String g641 = "G641";
	private static final String e101 = "E101";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;
	private static final int ct07 = 7;

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Client logical file*/
	private ClntTableDAM clntIO = new ClntTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File for follow ups*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
		/*Receipt Details - Contract Enquiry.*/
	private T5645rec t5645rec = new T5645rec();
	private T3629rec t3629rec = new T3629rec();
	private Td5herec td5herec = new Td5herec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Payreqrec payreqrec = new Payreqrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Getdescrec getdescrec = new Getdescrec();
	private FormatsInner formatsInner = new FormatsInner();
	
	private PyoupfDAO pyoupfDAO = getApplicationContext().getBean("pyoupfDAO", PyoupfDAO.class);/*ICIL-254*/
	private AcmvpfDAO acmvpfDAO = getApplicationContext().getBean("acmvDAO", AcmvpfDAO.class);
	private Pyoupf pyoupf = new Pyoupf();
	private Acmvpf acmvpf = null;
    private List<Pyoupf> pyoupfList = null;
    private List<Acmvpf> acmvpfList = null;
    private Iterator<Pyoupf> iterator;
    
    private int minRecord = 1;
	private int incrRange;
	protected static final String wsaaAutoCheque = "1";

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		endOfFile2080,
		exit2090,
		validateDays2680,
		exit12690,
		exit2790
	}

	public Bd5hs() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		wsspEdterror.set(varcom.oK);
		wsaaCompany.set(bsprIO.getCompany());
		wsaaDatimeInit.set(bsscIO.getDatimeInit());
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsaaCompany);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		
		if (bprdIO.systemParam01.isNumeric()) {
	        if (bprdIO.systemParam01.toInt() > 0) {
	            incrRange = bprdIO.systemParam01.toInt();
	        } else {
	            incrRange = bprdIO.cyclesPerCommit.toInt();
	        }
	    } else {
	        incrRange = bprdIO.cyclesPerCommit.toInt();
	    }  
		
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		prepareLifacmv1100();
		preparePayreq1200();
		pyoupfList = pyoupfDAO.getPyoupfList(bsprIO.getCompany().toString(), minRecord, minRecord + incrRange);
   	    iterator=pyoupfList.iterator();
		
	}

protected void prepareLifacmv1100()
	{
		ArrayList<Object> initializeReplaceType1 = new ArrayList<Object>();
		ArrayList<Object> initializeReplaceBy1 = new ArrayList<Object>();
		initializeReplaceType1.add(NUMERIC);
		initializeReplaceBy1.add(ZERO);
		initializeReplaceType1.add(ALPHANUMERIC);
		initializeReplaceBy1.add(SPACES);
		initialize(lifacmvrec.lifacmvRec, initializeReplaceType1, initializeReplaceBy1);
		lifacmvrec.batccoy.set(batcdorrec.company);
		lifacmvrec.batcbrn.set(batcdorrec.branch);
		lifacmvrec.batcactyr.set(batcdorrec.actyear);
		lifacmvrec.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec.batctrcde.set(batcdorrec.trcde);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.rldgcoy.set(batcdorrec.company);
		lifacmvrec.genlcoy.set(batcdorrec.company);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.origamt.set(ZERO);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.transactionDate.set(bsscIO.getEffectiveDate());
		lifacmvrec.user.set(ZERO);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t1688);
		itemIO.setItemitem(bprdIO.getAuthCode());
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			lifacmvrec.trandesc.set(SPACES);
			wsaaTrandesc.set(SPACES);
		}
		else {
			lifacmvrec.trandesc.set(getdescrec.longdesc);
			wsaaTrandesc.set(getdescrec.longdesc);
		}
	}

protected void preparePayreq1200()
	{
		ArrayList<Object> initializeReplaceType1 = new ArrayList<Object>();
		ArrayList<Object> initializeReplaceBy1 = new ArrayList<Object>();
		initializeReplaceType1.add(NUMERIC);
		initializeReplaceBy1.add(ZERO);
		initializeReplaceType1.add(ALPHANUMERIC);
		initializeReplaceBy1.add(SPACES);
		initialize(payreqrec.rec, initializeReplaceType1, initializeReplaceBy1);
		payreqrec.effdate.set(bsscIO.getEffectiveDate());
		payreqrec.sacscode.set(t5645rec.sacscode03);
		payreqrec.sacstype.set(t5645rec.sacstype03);
		payreqrec.glcode.set(t5645rec.glmap03);
		payreqrec.sign.set(t5645rec.sign03);
		payreqrec.cnttot.set(t5645rec.cnttot03);
		payreqrec.trandesc.set(wsaaTrandesc);
		payreqrec.termid.set(varcom.vrcmTermid);
		payreqrec.user.set(wsaaUser);
		payreqrec.batctrcde.set(batcdorrec.trcde);
		payreqrec.batckey.set(batcdorrec.batchkey);
		payreqrec.tranref.set(bsscIO.getJobName());
		payreqrec.language.set(bsscIO.getLanguage());
		payreqrec.function.set("REQN");
	}

protected void readFile2000()
	{
	   wsspEdterror.set(varcom.oK);
	   if(iterator.hasNext()){
		   pyoupf = iterator.next();
		   readAcmv2020();
	   }else{
		   minRecord += incrRange;
		   pyoupfList = pyoupfDAO.getPyoupfList(bsprIO.getCompany().toString(), minRecord, minRecord + incrRange);
		   if(pyoupfList !=null && pyoupfList.size() > 0) {
	   	   iterator=pyoupfList.iterator();
		   pyoupf = iterator.next();
		   readAcmv2020();
		   }else {
		   endOfFile2080();
		   }
	   }
	}

protected void readAcmv2020() {
	
	wsaaSacscurbal.set(ZERO);
	acmvpf = new Acmvpf();
	acmvpf.setRldgcoy(pyoupf.getChdrcoy().charAt(0));
	if(pyoupf.getRldgacct() == null || isEQ(pyoupf.getRldgacct(), SPACES)){
		acmvpf.setRldgacct(pyoupf.getChdrnum());	
	}
	else
	{
	acmvpf.setRldgacct(pyoupf.getRldgacct());
	}
	acmvpf.setRdocnum(pyoupf.getChdrnum());
	acmvpf.setTranno(pyoupf.getTranno());
	acmvpf.setSacscode(t5645rec.sacscode03.toString());
	acmvpf.setSacstyp(t5645rec.sacstype03.toString());
	acmvpfList = acmvpfDAO.searchAcmvpfRecordbyTranno(acmvpf);
	if(acmvpfList != null && acmvpfList.size() > 0) {
		acmvpf = acmvpfList.get(0);
		wsaaSacscurbal.set(acmvpf.getOrigamt());
	}
	
}

protected void endOfFile2080()
	{
		wsspEdterror.set(varcom.endp);
		if (isEQ(wsaaEof,"Y")) {
			wsspEdterror.set(varcom.endp);
		}
	}

protected void edit2500()
	{
		wsspEdterror.set(varcom.oK);
		readChdrlnb2550();
		readTd5he2600();
		if (isEQ(wsspEdterror,varcom.oK)) {
			validPolicyOwner2700();
		}
		if (isEQ(wsspEdterror,varcom.oK)) {
			lockContract2800();
		}
	}

protected void readChdrlnb2550()
	{
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(pyoupf.getChdrcoy());
		chdrlnbIO.setChdrnum(pyoupf.getChdrnum());
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}

protected void readTd5he2600()
	{
		start2600();
		nextTd5he2610();
	}

protected void start2600()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsaaCompany);
		itemIO.setItemtabl(td5he);
		wsaaTrancode.set(pyoupf.getBatctrcde());
		wsaaCntcurr.set(chdrlnbIO.getCntcurr());
	}

protected void nextTd5he2610()
	{
		itemIO.setItemitem(wsaaTd5heKey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			if (isNE(wsaaTrancode,"****")) {
				wsaaTrancode.set("****");
				nextTd5he2610();
				return ;
			}
			else {
				contotrec.totno.set(ct02);
				contotrec.totval.set(1);
				callContot001();
				wsspEdterror.set(SPACES);
				return ;
			}
		}
		td5herec.td5heRec.set(itemIO.getGenarea());
		
		if ((isLT(td5herec.maxpayamnt,wsaaSacscurbal))     //kevin
		||   isGT(td5herec.minpayamnt,wsaaSacscurbal)) {
 			 contotrec.totno.set(ct03);
 			 contotrec.totval.set(1);
			 callContot001();
			 wsspEdterror.set(SPACES);
		}
	}

protected void validPolicyOwner2700()
	{
		clntIO.setParams(SPACES);
		clntIO.setClntpfx(chdrlnbIO.getCownpfx());
		clntIO.setClntcoy(chdrlnbIO.getCowncoy());
		clntIO.setClntnum(pyoupf.getPayrnum());
		clntIO.setFormat(formatsInner.clntrec);
		clntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(clntIO.getStatuz());
			syserrrec.params.set(clntIO.getParams());
			fatalError600();
		}
		if (isNE(clntIO.getCltstat(),"AC")
		&& isNE(clntIO.getCltdod(),varcom.vrcmMaxDate)) {
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
		}
	}

protected void lockContract2800()
	{
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(chdrlnbIO.getChdrcoy());
		sftlockrec.enttyp.set(chdrlnbIO.getChdrpfx());
		sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			conlogrec.error.set(e101);
			conlogrec.params.set(sftlockrec.sftlockRec);
			callConlog003();
			wsspEdterror.set(SPACES);
		}
	}

protected void update3000()
	{
		/*UPDATE*/
		writeContract3100();
		writePtrn3150();
		callLifacmv3200();
		callPayreq3250();
		updatepyoupf3300();
		releaseSftlock3330();
		/*EXIT*/
	}

protected void writeContract3100()
	{
		start3100();
	}

protected void start3100()
	{
		chdrlnbIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		setPrecision(chdrlnbIO.getTranno(), 0);
		chdrlnbIO.setTranno(add(chdrlnbIO.getTranno(),1));
		chdrlnbIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}

protected void writePtrn3150()
{
	ptrnIO.setParams(SPACES);
	ptrnIO.setBatcpfx(batcdorrec.prefix);
	ptrnIO.setBatccoy(batcdorrec.company);
	ptrnIO.setBatcbrn(batcdorrec.branch);
	ptrnIO.setBatcactyr(batcdorrec.actyear);
	ptrnIO.setBatcactmn(batcdorrec.actmonth);
	ptrnIO.setBatctrcde(batcdorrec.trcde);
	ptrnIO.setBatcbatch(batcdorrec.batch);
	ptrnIO.setChdrpfx(chdrlnbIO.getChdrpfx());
	ptrnIO.setChdrcoy(chdrlnbIO.getChdrcoy());
	ptrnIO.setChdrnum(chdrlnbIO.getChdrnum());
	ptrnIO.setTranno(chdrlnbIO.getTranno());
	ptrnIO.setValidflag("1");
	ptrnIO.setTransactionDate(getCobolDate());
	ptrnIO.setTransactionTime(getCobolTime());
	ptrnIO.setPtrneff(bsscIO.getEffectiveDate());
	ptrnIO.setTermid(varcom.vrcmTermid);
	ptrnIO.setUser(subString(bsscIO.getDatimeInit(), 21, 6));
	ptrnIO.setDatesub(bsscIO.getEffectiveDate());
	ptrnIO.setCrtuser(bsscIO.getUserName());
	ptrnIO.setFormat(formatsInner.ptrnrec);
	ptrnIO.setCrtuser(bsscIO.getUserName().toString());   //IJS-523
	ptrnIO.setFunction(varcom.writr);
	SmartFileCode.execute(appVars, ptrnIO);
	if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
		syserrrec.statuz.set(ptrnIO.getStatuz());
		syserrrec.params.set(ptrnIO.getParams());
		fatalError600();
	}
}

protected void callLifacmv3200()
{
	/*START*/
	wsaaJrnseq.set(1);
	wsaaSub.set(1);
	lifacmvrec.origamt.set(wsaaSacscurbal);
	a1000CallLifacmv();
	wsaaSub.set(2);
	lifacmvrec.origamt.set(wsaaSacscurbal);
	a1000CallLifacmv();
	/*EXIT*/
}

protected void callPayreq3250()
	{
		start3250();
	}

protected void start3250()
	{
		payreqrec.batccoy.set(chdrlnbIO.getChdrcoy());
		payreqrec.paycurr.set(chdrlnbIO.getCntcurr());
		payreqrec.pymt.set(wsaaSacscurbal);
		payreqrec.frmRldgacct.set(SPACES);
		payreqrec.frmRldgacct.set(chdrlnbIO.getChdrnum());
		payreqrec.trandesc.set(descIO.getLongdesc());
		payreqrec.clntcoy.set(chdrlnbIO.getCowncoy());
		payreqrec.clntnum.set(pyoupf.getPayrnum());
		readT36293260();
		payreqrec.bankcode.set(t3629rec.bankcode);
		payreqrec.tranref.set(SPACES);
		payreqrec.tranref.set(chdrlnbIO.getChdrnum());
		payreqrec.tranno.set(pyoupf.getTranno());
		payreqrec.zbatctrcde.set(pyoupf.getBatctrcde());
		if(isEQ(pyoupf.getBankkey(),SPACES) && isEQ(pyoupf.getBankacckey(),SPACES))
		{
			payreqrec.bankkey.set(SPACES);
			payreqrec.bankacckey.set(SPACES);
			payreqrec.reqntype.set(wsaaAutoCheque);
		}
		else
		{
			payreqrec.bankkey.set(pyoupf.getBankkey());
			payreqrec.bankacckey.set(pyoupf.getBankacckey());
			payreqrec.reqntype.set(pyoupf.getReqntype());
		}
		payreqrec.function.set("REQN");
		callProgram(Payreq.class, payreqrec.rec);
		
		if (isNE(payreqrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(payreqrec.statuz);
			syserrrec.params.set(payreqrec.rec);
			fatalError600();
		}
	}

protected void readT36293260()
	{
		start3260();
	}

protected void start3260()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(chdrlnbIO.getCntcurr());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			itemIO.setStatuz(g641);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		else {
			t3629rec.t3629Rec.set(itemIO.getGenarea());
			if (isEQ(t3629rec.bankcode,SPACES)) {
				itemIO.setStatuz(hl63);
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
	}

protected void updatepyoupf3300()
{
    pyoupfDAO.updatepayflag(pyoupf.getUniqueNumber());
}

protected void releaseSftlock3330()
	{
		start3330();
	}

protected void start3330()
	{
		sftlockrec.function.set("UNLK");
		sftlockrec.company.set(chdrlnbIO.getChdrcoy());
		sftlockrec.enttyp.set(chdrlnbIO.getChdrpfx());
		sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void a1000CallLifacmv()
	{
			a1000Start();
		}

protected void a1000Start()
	{
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			return ;
		}
		lifacmvrec.function.set("PSTW");
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub.toInt()]);
		lifacmvrec.rdocnum.set(chdrlnbIO.getChdrnum());
		lifacmvrec.tranno.set(chdrlnbIO.getTranno());
		lifacmvrec.tranref.set(chdrlnbIO.getTranno());
		lifacmvrec.effdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
		lifacmvrec.substituteCode[2].set(SPACES);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError600();
		}
		wsaaJrnseq.add(1);
	}

/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
		private FixedLengthStringData clntrec = new FixedLengthStringData(10).init("CLNTREC");
		private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
		private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	}
}
