package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2Pojo;
import com.csc.fsu.general.procedures.Datcon2Utils;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.contractservicing.dataaccess.dao.PrmhpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Prmhpf;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.Ta524rec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.terminationclaims.procedures.Calcfee;
import com.csc.life.terminationclaims.tablestructures.T6597rec;
import com.csc.life.terminationclaims.tablestructures.Th584rec;
import com.csc.smart.procedures.SftlockUtil;
import com.csc.smart.recordstructures.SftlockRecBean;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 * REMARKS
 * 			PREMIUM HOLIDAY REINSTATEMENT PROCESSING
 * --------------------------------------------------------------------------------------
 * This program is used to process premium holiday reinstatement on traditional products.
 * The program reads system parameters which are the statcodes, pstatcodes used to read CHDR
 * and the statuses that needs to be set if appropriate conditions are met. This program uses
 * the transaction code of Overdue Processing.
 * 
 * INITIALISE
 * 1.	Read system params
 * 2.	Read associated smart tables
 * 3.	Read CHDR, COVR, PAYR and PRMH files.
 * 4.	Read all CHDR records with STATCODE as IF/HP AND PSTATCODE as HA/HP
 * 
 * EDIT
 * 1. validate policy i.e., if cnttype is found in TA524 then proceed
 * 2. read HPADPF
 * 3. If NFO in HPADPF is found
 * 		I.		Lock Record
 *		II.		Read ACBLPF
 *				i.		If Premium holiday reinstatement issue transaction has been processed and
 *			   			and sufficient amount is available in suspense, set system param 03 statuses.
 *				ii.		If Premium holiday reinstatement issue transaction has been processed and
 *			   			and sufficient amount is not available in suspense, set lapse contract true
 *		III.	If policy is in Premium Holiday and has not been reinstated, set lapse contract true
 *		IV.		If policy is in Pending PH Reinstatement, set lapse contract true.
 *
 * UPDATE
 * 1. If lapse contract is true
 * 		i.		call LAPSE Subroutine from T6597
 * 		ii.		Update COVR Records with appropriate statuses
 * 		iii. 	Update CHDR Records with appropriate statuses
 * 		iv. 	Update PAYR Records with appropriate statuses
 * 		v. 		Write PTRN.
 * 2. If lapse contract is false
 * 		i.		Update COVR Records with appropriate statuses
 * 		ii. 	Update CHDR Records with appropriate statuses
 * 		iii. 	Update PAYR Records with appropriate statuses
 * 
 * SMART TABLES READ
 * 
 * 		TABLES		ITEM
 * --------------------------------------
 * 		T5679		B673
 * 		TA524		LPS, SLS
 * 		TH584		PHLLPS
 * 		T5645		BA587
 * 		T5687		Component (e.g. IPP1)
 * 		T6597		NF18
 * 
 * @author gsaluja2
 * @version 1.0
 * @since 29 May 2019
 */

public class Ba587 extends Mainb{
	/*	Batch params	*/
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BA587");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private static final Logger LOGGER = LoggerFactory.getLogger(Ba587.class);
	
	/*	DAO UTIL used	*/
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private PrmhpfDAO prmhpfDAO = getApplicationContext().getBean("prmhpfDAO", PrmhpfDAO.class);
	private Datcon2Utils datcon2Utils = getApplicationContext().getBean("datcon2Utils", Datcon2Utils.class);
	private SftlockUtil sftlockUtil = new SftlockUtil();
	
	/*	Rec's used	*/
	private T5679rec t5679rec = new T5679rec();
	private Ta524rec ta524rec = new Ta524rec();
	private P6671par p6671rec = new P6671par();
	private Th584rec th584rec = new Th584rec();
	private T5645rec t5645rec = new T5645rec();
	private T6597rec t6597rec = new T6597rec();
	private T5687rec t5687rec = new T5687rec();
	private Ovrduerec ovrduerec = new Ovrduerec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private SftlockRecBean sftlockRecBean = new SftlockRecBean();
	
	/*	Objects used	*/
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Map<String, Chdrpf> chdrpfMap;
	private List<String> chdrnumList;
	private Map<String, List<Covrpf>> covrpfMap;
	private Iterator<Payrpf> itr;
	private List<Payrpf> payrpfList;
	private Map<String, List<Prmhpf>> prmhListMap;
	private Hpadpf hpadpf;
	private Payrpf payrpf;
	private Chdrpf chdrpf;
	private List<Chdrpf> chdrpfUpdateList = new ArrayList<>();
	private List<Chdrpf> chdrpfInsertList = new ArrayList<>();
	private List<Payrpf> payrpfUpdateList = new ArrayList<>();
	private List<Payrpf> payrpfInsertList = new ArrayList<>();
	private List<Covrpf> covrpfUpdateList = new ArrayList<>();
	private List<Covrpf> covrpfInsertList = new ArrayList<>();
	private List<Ptrnpf> ptrnpfInsertList = new ArrayList<>();
	
	private List<Itempf> t5679List;
	private Map<String, Itempf> ta524Map;
	private Map<String, Itempf> th584Map;
	private Map<String, Itempf> t5645Map;
	private Map<String, List<Itempf>> t5687ListMap;
	private Map<String, List<Itempf>> t6597ListMap;
	private List<String> ta524List;
	
	/*	variables used	*/
	private String coyPfx;
	private String coy;
	private FixedLengthStringData wsaaChdrnumFrm = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8);
	private List<String> statcodeList;
	private List<String> pstatcodeList;
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private FixedLengthStringData chdrRiskStatus = new FixedLengthStringData(2);
	private FixedLengthStringData chdrPremStatus = new FixedLengthStringData(2);
	private FixedLengthStringData covrRiskStatus = new FixedLengthStringData(2);
	private FixedLengthStringData covrPremStatus = new FixedLengthStringData(2);
	private boolean lapseContract = false;
	private String[] statcode;
	private String[] pstatcode;
	
	/*	Smart Tables Used	*/
	private FixedLengthStringData ta524 = new FixedLengthStringData(5).init("TA524");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData th584 = new FixedLengthStringData(5).init("TH584");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t6597 = new FixedLengthStringData(5).init("T6597");
	
	/*	Control Totals Used	*/
	private ZonedDecimalData ct01 = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private ZonedDecimalData ct02 = new ZonedDecimalData(2, 0).init(2).setUnsigned();
	private ZonedDecimalData ct03 = new ZonedDecimalData(2, 0).init(3).setUnsigned();
	private ZonedDecimalData ct04 = new ZonedDecimalData(2, 0).init(4).setUnsigned();
	private ZonedDecimalData ct05 = new ZonedDecimalData(2, 0).init(5).setUnsigned();
	private ZonedDecimalData ct06 = new ZonedDecimalData(2, 0).init(6).setUnsigned();
	private ZonedDecimalData ct07 = new ZonedDecimalData(2, 0).init(7).setUnsigned();
	private ZonedDecimalData ct08 = new ZonedDecimalData(2, 0).init(8).setUnsigned();
	private ZonedDecimalData ct09 = new ZonedDecimalData(2, 0).init(9).setUnsigned();
	private ZonedDecimalData ct10 = new ZonedDecimalData(2, 0).init(10).setUnsigned();
	private int ct01Value = 0;
	private int ct02Value = 0;
	private int ct03Value = 0;
	private int ct04Value = 0;
	private int ct05Value = 0;
	private int ct06Value = 0;
	private int ct07Value = 0;
	private int ct08Value = 0;
	private int ct09Value = 0;
	private int ct10Value = 0;

	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	@Override
	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	@Override
	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}
	
	@Override
	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}

	@Override
	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	@Override
	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	@Override
	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	@Override
	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	@Override
	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

	@Override
	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	@Override
	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}

	@Override
	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	@Override
	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}

	@Override
	protected void restart0900() {
		/*	RESTART	*/
	}
	
	@Override
	public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	@Override
	protected void initialise1000() {
		wsspEdterror.set(varcom.oK);
		coyPfx = smtpfxcpy.item.toString();
		coy = bsprIO.getCompany().toString();
		if(isEQ(bprdIO.getSystemParam01(), SPACES) || isEQ(bprdIO.getSystemParam02(), SPACES)) {
			LOGGER.warn("BATCH SYS PARAM NOT FOUND");
			wsspEdterror.set(varcom.endp);
		}
		else {
			statcode = bprdIO.getSystemParam01().toString().trim().split("\\s");
			pstatcode = bprdIO.getSystemParam02().toString().trim().split("\\s");
		}
		if(isEQ(wsspEdterror, varcom.oK)) {
			readSmartTables();
			p6671rec.parmRecord.set(bupaIO.getParmarea());
			if(isNE(p6671rec.chdrnum, SPACES)) {
				wsaaChdrnumFrm.set(p6671rec.chdrnum);
			}
			if(isNE(p6671rec.chdrnum1, SPACES)) {
				wsaaChdrnumTo.set(p6671rec.chdrnum1);
			}
			loadData();
		}
	}
	
	protected void readSmartTables() {
		t5679List = itemDAO.getAllItemitem(coyPfx, coy, t5679.toString(), bprdIO.getAuthCode().toString());
		if (null == t5679List || t5679List.isEmpty()) {
			syserrrec.params.set("T5679");
			fatalError600();
		} else {
			t5679rec.t5679Rec.set(StringUtil.rawToString(t5679List.get(0).getGenarea()));
		}
		ta524Map = itemDAO.getItemMap(coyPfx, coy, ta524.toString());
		th584Map = itemDAO.getItemMap(coyPfx, coy, th584.toString());
		t5645Map = itemDAO.getItemMap(coyPfx, coy, t5645.toString());
		t5687ListMap = itemDAO.loadSmartTable(coyPfx, coy, t5687.toString());
		t6597ListMap = itemDAO.loadSmartTable(coyPfx, coy, t6597.toString());
		ta524List = new ArrayList<>();
		for(Map.Entry<String, Itempf> ta524KV : ta524Map.entrySet()) {
			ta524List.add(ta524KV.getKey());
		}
		statcodeList = new ArrayList<>(Arrays.asList(statcode));
		pstatcodeList = new ArrayList<>(Arrays.asList(pstatcode));
	}
	
	protected void loadData() {
		chdrpfMap = chdrpfDAO.validateChdrList("CH", coy, wsaaChdrnumFrm.toString(), wsaaChdrnumTo.toString(), ta524List, statcodeList, pstatcodeList);
		if(chdrpfMap != null && !chdrpfMap.isEmpty()) {
			chdrnumList = new ArrayList<>();
			for(Map.Entry<String, ?> chdrEntry : chdrpfMap.entrySet()) {
				chdrnumList.add(((Chdrpf) chdrEntry.getValue()).getChdrnum());
			}
		}
		else {
			wsspEdterror.set(varcom.endp);
		}
		if(isEQ(wsspEdterror, varcom.oK)) {
			prmhListMap = prmhpfDAO.getPrmhRecordByTodate(coy, chdrnumList, bsscIO.getEffectiveDate().toInt());
			if(prmhListMap!=null && !prmhListMap.isEmpty()) {
				chdrnumList.retainAll(prmhListMap.keySet());
				covrpfMap = covrpfDAO.searchCovrMap(coy, chdrnumList);
				payrpfList = payrpfDAO.searchPayrRecordByChdrnumList(coy, chdrnumList);
				if(payrpfList != null && !payrpfList.isEmpty())
					itr = payrpfList.iterator();
				else
					wsspEdterror.set(varcom.endp);
			}
			else
				wsspEdterror.set(varcom.endp);
		}
	}

	@Override
	protected void readFile2000() {
		if(!itr.hasNext()){
			wsspEdterror.set(varcom.endp);
			return;
		}
		ct01Value++;
	}

	@Override
	protected void edit2500() {
		wsspEdterror.set(varcom.oK);
		payrpf = itr.next();
		validatePolicy();
		if(isEQ(wsspEdterror, varcom.oK)) {
			if (chdrpfMap != null && chdrpfMap.containsKey(payrpf.getChdrnum())) {
				chdrpf = chdrpfMap.get(payrpf.getChdrnum());
			}
			readHpadpf();
		}
		if(isEQ(wsspEdterror, varcom.oK)) {
			lapseContract = true;
			sftlockRecBean.setFunction("LOCK");
			performSlckOperation();
			ct02Value++;
		}
	}
	
	protected void validatePolicy() {
		Itempf itempf;
		itempf = ta524Map.get(chdrpfMap.get(payrpf.getChdrnum()).getCnttype().trim());
		if(itempf != null) {
			if(bsscIO.getEffectiveDate().getbigdata().compareTo(itempf.getItmfrm()) >= 0 && bsscIO.getEffectiveDate().getbigdata().compareTo(itempf.getItmto()) <= 0) {
				ta524rec.ta524Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
		}
		else {
			wsspEdterror.set(varcom.endp);
			return;
		}
		itempf = t5645Map.get(wsaaProg.toString());
		if(itempf != null) {
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			wsspEdterror.set(varcom.endp);
			return;
		}
	}
	
	protected void readHpadpf() {
		Itempf itempf;
		hpadpf = hpadpfDAO.getHpadData(payrpf.getChdrcoy(), payrpf.getChdrnum());
		if(hpadpf != null) {
			if(hpadpf.getZnfopt() != null && !hpadpf.getZnfopt().trim().isEmpty()) {
				itempf = th584Map.get(hpadpf.getZnfopt().trim()+chdrpf.getCnttype().trim());
				if(itempf != null) {
					th584rec.th584Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					for(Itempf item: t6597ListMap.get(th584rec.nonForfeitMethod.toString().trim())) {
						if(bsscIO.getEffectiveDate().getbigdata().compareTo(item.getItmfrm()) >= 0 && bsscIO.getEffectiveDate().getbigdata().compareTo(item.getItmto()) <= 0) {
							t6597rec.t6597Rec.set(StringUtil.rawToString(item.getGenarea()));
							break;
						}
						else {
							wsspEdterror.set(varcom.endp);
						}
					}
				}
				else {
					wsspEdterror.set(varcom.endp);
				}
			}
		}
	}
	
	protected void performSlckOperation() {
    	sftlockRecBean.setCompany(bsprIO.getCompany().toString().trim());
    	sftlockRecBean.setEnttyp("CH");
    	sftlockRecBean.setEntity(chdrpf.getChdrnum().trim());
    	sftlockRecBean.setTransaction(bprdIO.getAuthCode().toString());
    	sftlockRecBean.setUser("99999");
    	sftlockUtil.process(sftlockRecBean, appVars);
    	if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())
    		&& !sftlockRecBean.getFunction().equals(sftlockRecBean.getStatuz())) {
    			syserrrec.statuz.set(sftlockRecBean.getStatuz());
    			fatalError600();
    	}
	}
		
	protected BigDecimal fetchTotalPremium(String chdrnum, BigDecimal sinstamt01, BigDecimal insttot03) {
		BigDecimal wsaaTotalPremium = BigDecimal.ZERO;
		for(Covrpf covrpf: covrpfMap.get(chdrnum)) {
			wsaaTotalPremium = wsaaTotalPremium.add(covrpf.getInstprem());
		}
		wsaaTotalPremium = wsaaTotalPremium.add(sinstamt01).add(insttot03);
		return wsaaTotalPremium;
	}

	@Override
	protected void update3000() {
		if(lapseContract) {
			for(Covrpf covrpf: covrpfMap.get(payrpf.getChdrnum())) {
				setupOverdue(covrpf);
				callProgram(t6597rec.premsubr04, ovrduerec.ovrdueRec);
				ovrduerec.pstatcode.set(t6597rec.cpstat04);
				covrPremStatus.set(t6597rec.cpstat04);
				ovrduerec.statcode.set(t6597rec.crstat04);
				covrRiskStatus.set(t6597rec.crstat04);
				updateCovr(covrpf);
			}
			updatePtrn();
			chdrRiskStatus.set(t5679rec.setCnRiskStat);
			chdrPremStatus.set(t5679rec.setCnPremStat);
			updateChdr();
			updatePayr();
			sftlockRecBean.setFunction("UNLK");
			performSlckOperation();
			ct10Value++;
		}
		else
			wsspEdterror.set(SPACE);
	}
	
	protected void setupOverdue(Covrpf covrpf) {
		wsaaTime.set(getCobolTime());
		List<Itempf> itempfList = t5687ListMap.get(covrpf.getCrtable());
		if(itempfList != null && !itempfList.isEmpty()) {
			for(Itempf itempf: itempfList) {
				if(bsscIO.getEffectiveDate().getbigdata().compareTo(itempf.getItmfrm()) >= 0 && bsscIO.getEffectiveDate().getbigdata().compareTo(itempf.getItmto()) <= 0) {
					t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					break;
				}
			}
		}
		ovrduerec.ovrdueRec.set(SPACES);
		ovrduerec.aloind.set(SPACES);
		ovrduerec.efdcode.set(SPACES);
		ovrduerec.procSeqNo.set(ZERO);
		ovrduerec.language.set(bsscIO.getLanguage());
		ovrduerec.chdrcoy.set(covrpf.getChdrcoy());
		ovrduerec.chdrnum.set(covrpf.getChdrnum());
		ovrduerec.life.set(covrpf.getLife());
		ovrduerec.coverage.set(covrpf.getCoverage());
		ovrduerec.rider.set(covrpf.getRider());
		ovrduerec.planSuffix.set(ZERO);
		ovrduerec.tranno.set(chdrpfMap.get(covrpf.getChdrnum()).getTranno());
		ovrduerec.cntcurr.set(payrpf.getCntcurr());
		ovrduerec.effdate.set(bsscIO.getEffectiveDate());
		ovrduerec.outstamt.set(payrpf.getOutstamt());
		ovrduerec.ptdate.set(payrpf.getPtdate());
		ovrduerec.btdate.set(payrpf.getBtdate());
		ovrduerec.ovrdueDays.set(getOverdueDays());
		ovrduerec.agntnum.set(chdrpfMap.get(covrpf.getChdrnum()).getAgntnum());
		ovrduerec.cownnum.set(chdrpfMap.get(covrpf.getChdrnum()).getCownnum());
		ovrduerec.trancode.set(bprdIO.getAuthCode());
		ovrduerec.acctyear.set(batcdorrec.actyear);
		ovrduerec.acctmonth.set(batcdorrec.actmonth);
		ovrduerec.batcbrn.set(batcdorrec.branch);
		ovrduerec.batcbatch.set(batcdorrec.batch);
		ovrduerec.user.set(payrpf.getUser());
		ovrduerec.company.set(chdrpfMap.get(covrpf.getChdrnum()).getCowncoy());
		ovrduerec.tranDate.set(bsscIO.getEffectiveDate());
		ovrduerec.tranTime.set(wsaaTime);
		ovrduerec.termid.set(varcom.vrcmTermid);
		ovrduerec.crtable.set(covrpf.getCrtable());
		ovrduerec.pumeth.set(t5687rec.pumeth);
		ovrduerec.billfreq.set(payrpf.getBillfreq());
		ovrduerec.instprem.set(ZERO);
		ovrduerec.sumins.set(ZERO);
		ovrduerec.crrcd.set(ZERO);
		ovrduerec.newCrrcd.set(ZERO);
		ovrduerec.newSingp.set(ZERO);
		ovrduerec.newAnb.set(ZERO);
		ovrduerec.newPua.set(ZERO);
		ovrduerec.newInstprem.set(ZERO);
		ovrduerec.newSumins.set(ZERO);
		ovrduerec.premCessDate.set(ZERO);
		ovrduerec.cnttype.set(chdrpfMap.get(covrpf.getChdrnum()).getCnttype());
		ovrduerec.occdate.set(chdrpfMap.get(covrpf.getChdrnum()).getOccdate());
		ovrduerec.polsum.set(chdrpfMap.get(covrpf.getChdrnum()).getPolsum());
		callProgram(Calcfee.class, ovrduerec.ovrdueRec);
		if (isNE(ovrduerec.statuz, varcom.oK)) {
			syserrrec.params.set(ovrduerec.ovrdueRec);
			syserrrec.statuz.set(ovrduerec.statuz);
			fatalError600();
		}
		ovrduerec.newRiskCessDate.set(varcom.vrcmMaxDate);
		ovrduerec.newPremCessDate.set(varcom.vrcmMaxDate);
		ovrduerec.newBonusDate.set(varcom.vrcmMaxDate);
		ovrduerec.newRerateDate.set(varcom.vrcmMaxDate);
		ovrduerec.cvRefund.set(ZERO);
		ovrduerec.surrenderValue.set(ZERO);
		ovrduerec.etiYears.set(ZERO);
		ovrduerec.etiDays.set(ZERO);
		ovrduerec.planSuffix.set(covrpf.getPlanSuffix());
		ovrduerec.instprem.set(covrpf.getInstprem());
		ovrduerec.sumins.set(covrpf.getSumins());
		ovrduerec.crrcd.set(covrpf.getCrrcd());
		ovrduerec.premCessDate.set(covrpf.getPremCessDate());
		ovrduerec.riskCessDate.set(covrpf.getRiskCessDate());
		ovrduerec.newInstprem.set(covrpf.getInstprem());
	}
	
	protected int getOverdueDays() {
		int wsaaOverdueDays;
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(payrpf.getPtdate());
		datcon3rec.intDate2.set(bsscIO.getEffectiveDate());
		datcon3rec.frequency.set("DY");
		datcon3rec.freqFactor.set(0);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		wsaaOverdueDays = datcon3rec.freqFactor.toInt();
		return wsaaOverdueDays;
	}
	
	protected void updateCovr(Covrpf covr) {
		Covrpf covrpf = new Covrpf(covr);
		covrpf.setCurrto(bsscIO.getEffectiveDate().toInt());
		covrpf.setValidflag("2");
		covrpfUpdateList.add(covrpf);
		ct03Value++;
		covr.setTranno(chdrpf.getTranno()+1);
		covr.setStatcode(covrRiskStatus.toString());
		covr.setPstatcode(covrPremStatus.toString());
		covr.setCurrfrom(bsscIO.getEffectiveDate().toInt());
		covrpfInsertList.add(covr);
		ct04Value++;
	}
	
	protected void updateChdr() {
		if(chdrpf != null) {
			Chdrpf chdrUpdate = new Chdrpf(chdrpf);
			if(lapseContract) {
				chdrUpdate.setCurrto(bsscIO.getEffectiveDate().toInt());
				chdrUpdate.setValidflag('2');
				chdrUpdate.setUniqueNumber(chdrpf.getUniqueNumber());
				chdrpfUpdateList.add(chdrUpdate);
				ct05Value++;
				Chdrpf chdrInsert = new Chdrpf(chdrpf);
				chdrInsert.setStatcode(chdrRiskStatus.toString());
				chdrInsert.setPstcde(chdrPremStatus.toString());
				chdrInsert.setTranno(chdrpf.getTranno()+1);
				chdrInsert.setCurrfrom(bsscIO.getEffectiveDate().toInt());
				chdrInsert.setUniqueNumber(chdrpf.getUniqueNumber());
				chdrpfInsertList.add(chdrInsert);
				ct06Value++;
			}
			else{
				chdrUpdate.setStatcode(chdrRiskStatus.toString());
				chdrUpdate.setPstcde(chdrPremStatus.toString());
				chdrUpdate.setUniqueNumber(chdrpf.getUniqueNumber());
				chdrpfUpdateList.add(chdrUpdate);
				ct05Value++;
			}
		}
	}
	
	protected void updatePayr() {
		Payrpf payrUpdate = new Payrpf(payrpf);
		if(lapseContract) {
			payrUpdate.setValidflag("2");
			payrpfUpdateList.add(payrUpdate);
			ct07Value++;
			Payrpf payrInsert = new Payrpf(payrpf);
			payrInsert.setPstatcode(chdrPremStatus.toString());
			payrInsert.setTranno(chdrpf.getTranno()+1);
			payrpfInsertList.add(payrInsert);
			ct08Value++;
		}
		else {
			payrUpdate.setPstatcode(chdrPremStatus.toString());
			payrpfUpdateList.add(payrUpdate);
			ct07Value++;
		}
	}
	
	protected void updatePtrn() {
		Ptrnpf ptrnpf = new Ptrnpf();
		ptrnpf.setBatcpfx(batcdorrec.prefix.toString());
		ptrnpf.setBatccoy(batcdorrec.company.toString());
		ptrnpf.setBatcbrn(batcdorrec.branch.toString());
		ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
		ptrnpf.setBatcbatch(batcdorrec.batch.toString());
		ptrnpf.setChdrpfx(chdrpf.getChdrpfx());
		ptrnpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrpf.getChdrnum());
		ptrnpf.setTranno(chdrpf.getTranno()+1);
		ptrnpf.setValidflag("1");
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setTrdt(varcom.vrcmDate.toInt());
		ptrnpf.setPtrneff(bsscIO.getEffectiveDate().toInt());
		ptrnpf.setTermid(varcom.vrcmTermid.toString());
		ptrnpf.setUserT(payrpf.getUser());
		ptrnpf.setDatesub(bsscIO.getEffectiveDate().toInt());
		ptrnpfInsertList.add(ptrnpf);
		ct09Value++;
	}
	
	protected int getLapseEffDate() {
		Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
		datcon2Pojo.setFreqFactor(1);
		datcon2Pojo.setIntDate1(Integer.toString(bsscIO.getEffectiveDate().toInt()));
		datcon2Pojo.setFrequency("DY");
		datcon2Utils.calDatcon2(datcon2Pojo);
		if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
			syserrrec.statuz.set(datcon2Pojo.getStatuz());
			syserrrec.params.set(datcon2Pojo.toString());
			fatalError600();
		}
		return Integer.parseInt(datcon2Pojo.getIntDate2());
	}
	
	@Override
	protected void commit3500() {
		if(chdrpfUpdateList != null && !chdrpfUpdateList.isEmpty()) {
			chdrpfDAO.updateChdrAmt(chdrpfUpdateList);
			chdrpfUpdateList.clear();
		}
		if(chdrpfInsertList != null && !chdrpfInsertList.isEmpty()) {
			chdrpfDAO.insertChdrValidCodeStatusRecord(chdrpfInsertList);
			chdrpfInsertList.clear();
		}
		if(payrpfUpdateList != null && !payrpfUpdateList.isEmpty()) {
			payrpfDAO.updatePayr(payrpfUpdateList);
			payrpfUpdateList.clear();
		}
		if(payrpfInsertList != null && !payrpfInsertList.isEmpty()) {
			payrpfDAO.insertPayrpfList(payrpfInsertList);
			payrpfInsertList.clear();
		}
		if(covrpfUpdateList != null && !covrpfUpdateList.isEmpty()) {
			covrpfDAO.updateCovrRPStat(covrpfUpdateList);
			covrpfUpdateList.clear();
		}
		if(covrpfInsertList != null && !covrpfInsertList.isEmpty()) {
			covrpfDAO.insertCovrpfList(covrpfInsertList);
			covrpfInsertList.clear();
		}
		if(ptrnpfInsertList != null && !ptrnpfInsertList.isEmpty()) {
			ptrnpfDAO.insertPtrnPF(ptrnpfInsertList);
			ptrnpfInsertList.clear();
		}
		commitControlTotals();
	}
	
	protected void commitControlTotals() {
		contotrec.totno.set(ct01);
		contotrec.totval.set(ct01Value);
		callContot001();
		ct01Value = 0;

		contotrec.totno.set(ct02);
		contotrec.totval.set(ct02Value);
		callContot001();
		ct02Value = 0;
		
		contotrec.totno.set(ct03);
		contotrec.totval.set(ct03Value);
		callContot001();
		ct03Value = 0;
		
		contotrec.totno.set(ct04);
		contotrec.totval.set(ct04Value);
		callContot001();
		ct04Value = 0;
		
		contotrec.totno.set(ct05);
		contotrec.totval.set(ct05Value);
		callContot001();
		ct05Value = 0;
		
		contotrec.totno.set(ct06);
		contotrec.totval.set(ct06Value);
		callContot001();
		ct06Value = 0;
		
		contotrec.totno.set(ct07);
		contotrec.totval.set(ct07Value);
		callContot001();
		ct07Value = 0;
		
		contotrec.totno.set(ct08);
		contotrec.totval.set(ct08Value);
		callContot001();
		ct08Value = 0;
		
		contotrec.totno.set(ct09);
		contotrec.totval.set(ct09Value);
		callContot001();
		ct09Value = 0;
		
		contotrec.totno.set(ct10);
		contotrec.totval.set(ct10Value);
		callContot001();
		ct10Value = 0;
	}

	@Override
	protected void close4000() {
		t5679List.clear();
		t5645Map.clear();
		t5687ListMap.clear();
		t6597ListMap.clear();
		ta524List.clear();
		ta524Map.clear();
		th584Map.clear();
		if(chdrpfMap != null)
			chdrpfMap.clear();
		if(covrpfMap != null)
			covrpfMap.clear();
		if(chdrnumList != null)
			chdrnumList.clear();
		itr = null;
		lsaaStatuz.set(varcom.oK);
	}

	@Override
	protected void rollback3600() {
		/*ROLLBACK*/
	}
}
