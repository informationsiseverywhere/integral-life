/*
 * File: B5134.java
 * Date: 29 August 2009 20:57:37
 * Author: Quipoz Limited
 *
 * Class transformed from B5134.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.tablestructures.Tr695rec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.linkage.procedures.LinkageInfoService;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.regularprocessing.dataaccess.CovipfTableDAM;
import com.csc.life.regularprocessing.dataaccess.IncrrgpTableDAM;
import com.csc.life.regularprocessing.dataaccess.LifergpTableDAM;
import com.csc.life.regularprocessing.dataaccess.dao.AinrpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.CovipfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.RertpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Ainrpf;
import com.csc.life.regularprocessing.dataaccess.model.Covipf;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.regularprocessing.dataaccess.model.Rertpf;
import com.csc.life.regularprocessing.recordstructures.Incrsrec;
import com.csc.life.regularprocessing.tablestructures.T5654rec;
import com.csc.life.regularprocessing.tablestructures.T5655rec;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.ArraySearch;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;


/**
* <pre>
*REMARKS.
*                  PENDING AUTOMATIC INCREASES
*                  ---------------------------
* Overview
* ________
*
*   This program is part of the Automatic Increases multi-thread
* batch suite. It runs directly after B5133 which 'splits' the
* COVRPF into a number of threads. References to COVR are via
* COVIPF - a temporary file holding all of the COVR records for
* this program to process.
*
*   Contracts are processed based upon the COVR CPI DATE. Table
* T5655 contains a field 'Lead Time' which, for Auto Increases,
* is accessed using transaction code plus 'O-K'.
*
*   N.B. It will be up to each client to ensure that the Lead
* Time before billing on T5655 is greater than or equal to the
* Billing Lead Time on T6654.
*
*   The following Control Totals are maintained:
*
*         01  -  Number of records read
*         02  -  LETC written
*         03  -  PAYR updated
*         04  -  INCR written
*         05  -  PTRN written
*         06  -  COVR updated
*         07  -  AINR written
*         08  -  No increase on COVR
*         09  -  Invalid contract
*         10  -  Invalid coverage
*         11  -  Records already locked
*
*
* INITIALISE.
*    - Read tables and where necessary store them in COVERAGE
*      storage arrays.
*
* READ.
*    - Read first primary file record.
*
* PERFORM Until End of File.
*
*      EDIT.
*       - Check if the primary file record is required
*       - If the contract number is different to the previous one
*         AND an INCR has been written, write a PTRN for the last
*         contract then check the OPTIONAL INDICATOR on T6658.
*         If is it not spaces then call LETRQST to generate a
*         a letter request for the contract.
*       - Softlock the record if it is to be updated
*
*      UPDATE.
*       - Search WSAA-T5687 using CRTABLE, to get Anniversary
*         Method.
*       - Search WSAA-T6658 with Anniversary Method, to get the
*         Increase Subroutine.
*       - Call the Subroutine. Using past INCR records will
*         assist in passing ORIGINAL Premium & Sum Assured
*         figures, as well as Premium & Sum Assured figures from
*         the LAST increase. This will allow calculation of
*         Simple or Compound Increases.
*       - Write an INCR record.
*       - Write an AINR record, which the Report Program B5135
*         will retrieve and print.
*       - Write the old coverage with a Valid Flag of '2',
*         setting CURRTO to the processing date. Write the new
*         coverage with COVR-INDEXATION-IND set as 'P' (pending).
*         Set CURRFROM to processing date and CURRTO as hi-date.
*       - Release softlock.
*
*      READ next primary file record.
*
* END PERFORM.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class B5134 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private CovipfTableDAM covipf = new CovipfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5134");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

		/* Holds the value taken from System Parameter 03,
		 which is used to indicate the AINR record types
		 used by this program. These are PEND/LETC.*/
	private FixedLengthStringData wsaaSysparam03 = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPend = new FixedLengthStringData(4).isAPartOf(wsaaSysparam03, 0);
		/* T5687 array variables.*/
	//private static final int wsaaT5687Size = 500;
	//ILIFE-2628 fixed--Array size increased 
	private static final int wsaaT5687Size = 1000;
	private PackedDecimalData wsaaT5687IxMax = new PackedDecimalData(5, 0).setUnsigned();
	private PackedDecimalData wsaaOldCpiDate = new PackedDecimalData(8, 0);
		/* Table T6658 array variables.*/
	//private static final int wsaaT6658Size = 30;
	//ILIFE-2628 fixed--Array size increased 
	private static final int wsaaT6658Size = 1000;
	private PackedDecimalData wsaaT6658IxMax = new PackedDecimalData(5, 0).setUnsigned();
		/* T6634 array variables
		01  WSAA-T6634-KEY2.
		01  WSAA-T6634-ARRAY.
		    03  WSAA-T6634-REC          OCCURS 800
		                                ASCENDING KEY WSAA-T6634-KEY
		                                INDEXED BY WSAA-T6634-IX.
		       05 WSAA-T6634-KEY        VALUE HIGH-VALUES.
		       05 WSAA-T6634-DATA.                                       */
	//private static final int wsaaTr384Size = 900;
	private static final int wsaaTr384Size = 2500;//ILIFE-1985
	private PackedDecimalData wsaaTr384IxMax = new PackedDecimalData(5, 0).setUnsigned();

	private FixedLengthStringData wsaaTr384Key2 = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTr384Cnttype2 = new FixedLengthStringData(3).isAPartOf(wsaaTr384Key2, 0);
	private FixedLengthStringData wsaaTr384Trcode2 = new FixedLengthStringData(4).isAPartOf(wsaaTr384Key2, 3);

	private FixedLengthStringData wsaaTr384Array = new FixedLengthStringData(37500);//ILIFE-1985
	private FixedLengthStringData[] wsaaTr384Rec = FLSArrayPartOfStructure(2500, 15, wsaaTr384Array, 0);//ILIFE-1985
	private FixedLengthStringData[] wsaaTr384Key = FLSDArrayPartOfArrayStructure(7, wsaaTr384Rec, 0, HIVALUES);
	private FixedLengthStringData[] wsaaTr384Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaTr384Key, 0);
	private FixedLengthStringData[] wsaaTr384Trcode = FLSDArrayPartOfArrayStructure(4, wsaaTr384Key, 3);
	private FixedLengthStringData[] wsaaTr384Data = FLSDArrayPartOfArrayStructure(8, wsaaTr384Rec, 7);
	private FixedLengthStringData[] wsaaTr384LetterType = FLSDArrayPartOfArrayStructure(8, wsaaTr384Data, 0);

	private FixedLengthStringData wsaaT5655Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5655Trcode = new FixedLengthStringData(4).isAPartOf(wsaaT5655Key, 0);
	private FixedLengthStringData wsaaT5655Asterisk = new FixedLengthStringData(4).isAPartOf(wsaaT5655Key, 4).init("****");

	private FixedLengthStringData wsaaTr695Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr695Coverage = new FixedLengthStringData(4).isAPartOf(wsaaTr695Key, 0);
	private FixedLengthStringData wsaaTr695Rider = new FixedLengthStringData(4).isAPartOf(wsaaTr695Key, 4);
	private FixedLengthStringData wsaaBasicCommMeth = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBascpy = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaSrvcpy = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaRnwcpy = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaChdrChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrChdrnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0);
	
	
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0).init(ZERO);

	private FixedLengthStringData wsaaValidCovr = new FixedLengthStringData(1).init("N");
	private Validator validComponent = new Validator(wsaaValidCovr, "Y");
	private Validator invalidComponent = new Validator(wsaaValidCovr, "N");

	private FixedLengthStringData wsaaValidChdr = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidChdr, "Y");
	private Validator invalidContract = new Validator(wsaaValidChdr, "N");

	private FixedLengthStringData wsaaProcessContract = new FixedLengthStringData(1).init("Y");
	private Validator processContract = new Validator(wsaaProcessContract, "Y");
	private Validator ignoreContract = new Validator(wsaaProcessContract, "N");

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init("Y");
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");
	private Validator invalidStatus = new Validator(wsaaValidStatus, "N");

	private FixedLengthStringData wsaaNewCoverage = new FixedLengthStringData(1).init("Y");
	private Validator newContract = new Validator(wsaaNewCoverage, "Y");
	private Validator sameContract = new Validator(wsaaNewCoverage, "N");
	private PackedDecimalData wsaaTransDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaTransTime = new PackedDecimalData(6, 0);

	private FixedLengthStringData wsaaIncrWritten = new FixedLengthStringData(1);
	private Validator incrWritten = new Validator(wsaaIncrWritten, "Y");
	private Validator incrNotWritten = new Validator(wsaaIncrWritten, "N");

	private FixedLengthStringData wsaaIncrExist = new FixedLengthStringData(1);
	private Validator incrExist = new Validator(wsaaIncrExist, "Y");
	private Validator incrNotExist = new Validator(wsaaIncrExist, "N");

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");
	private Validator notFirstTime = new Validator(wsaaFirstTime, "N");

	private FixedLengthStringData wsaaDataProcessed = new FixedLengthStringData(1);
	private Validator dataProcessed = new Validator(wsaaDataProcessed, "Y");
	private Validator dataNotProcessed = new Validator(wsaaDataProcessed, "N");
	private ZonedDecimalData wsaaT5679Sub = new ZonedDecimalData(3, 0);

		/* WSYS-SYSTEM-ERROR-PARAMS */
	private FixedLengthStringData wsysCovrkey = new FixedLengthStringData(22);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysCovrkey, 0);
	private FixedLengthStringData wsysCoverage = new FixedLengthStringData(2).isAPartOf(wsysCovrkey, 8);
		/* COVI Parameters.*/
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaCoviFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(wsaaCoviFn, 0, FILLER).init("COVI");
	private FixedLengthStringData wsaaCoviRunid = new FixedLengthStringData(2).isAPartOf(wsaaCoviFn, 4);
	private ZonedDecimalData wsaaCoviJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaCoviFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler2 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	//private PackedDecimalData wsaaIncrease = new PackedDecimalData(17, 2);
		/* ERRORS */
	private static final String e512 = "E512";
	private static final String h053 = "H053";
	private static final String h036 = "H036";
	private static final String h791 = "H791";
	private static final String ivrm = "IVRM";
		/* TABLES */
	private static final String t5654 = "T5654";
	private static final String t5655 = "T5655";
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
	private static final String tr384 = "TR384";
	private static final String t6658 = "T6658";
	private static final String tr695 = "TR695";
	private String t1693 = "T1693";
	private String t5675 = "T5675";
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaT5687Ix = new IntegerData();
	private IntegerData wsaaT6658Ix = new IntegerData();
	private IntegerData wsaaTr384Ix = new IntegerData();
//	private AinrTableDAM ainrIO = new AinrTableDAM();
//	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
//	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
//	private CovrincTableDAM covrincIO = new CovrincTableDAM();
//	private IncrTableDAM incrIO = new IncrTableDAM();
	private IncrrgpTableDAM incrrgpIO = new IncrrgpTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
//	private PayrTableDAM payrIO = new PayrTableDAM();
//	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Incrsrec incrsrec = new Incrsrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private T5654rec t5654rec = new T5654rec();
	private T5655rec t5655rec = new T5655rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6658rec t6658rec = new T6658rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Tr695rec tr695rec = new Tr695rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaT5687ArrayInner wsaaT5687ArrayInner = new WsaaT5687ArrayInner();
	private WsaaT6658ArrayInner wsaaT6658ArrayInner = new WsaaT6658ArrayInner();
	private Premiumrec premiumrec=new Premiumrec();
	private T5675rec t5675rec=new T5675rec();
	private Datcon3rec datcon3rec=new Datcon3rec();
	private AnnyTableDAM annyIO = new AnnyTableDAM();
	private LifergpTableDAM lifergpIO = new LifergpTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private String chdrlnbrec = "CHDRLNBREC";
	private Agecalcrec agecalcrec = new Agecalcrec();
	private T1693rec t1693rec = new T1693rec();
	private boolean ispermission = false;
	/*ILIFE-3915 	ALS-358 - premium is not increased when benefit amount is increased Start*/
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	private Rcvdpf rcvdPFObject= new Rcvdpf();
	private ExternalisedRules er = new ExternalisedRules();
	/*ILIFE-3915 End*/
	
	private List<Ptrnpf> insertPtrnList = null;
	private Map<String, List<Itempf>> t5687Map = null;
	private Map<String, List<Itempf>> t6658Map = null;
	private Map<String, List<Itempf>> tr384Map = null;
	private Map<String, List<Itempf>> tr695Map = null;
	private Map<String, List<Itempf>> t5654Map = null;
	private Map<String, List<Chdrpf>> chdrlifRgpMap = null;
	private Map<String, List<Covrpf>> covrincMap = null;
	private Map<String, List<Incrpf>> incrMap = null;
	private Map<String, List<Payrpf>> payrMap = null;
    private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
    private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
    private CovipfDAO covipfDAO = getApplicationContext().getBean("covipfDAO", CovipfDAO.class);
    private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
    private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
    private IncrpfDAO incrpfDAO = getApplicationContext().getBean("incrpfDAO", IncrpfDAO.class);
    private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
    private AinrpfDAO ainrpfDAO = getApplicationContext().getBean("ainrpfDAO", AinrpfDAO.class);
    
    private Iterator<Covipf> iteratorList; 
    private List<Covrpf> updateCovrincDateList;
    private List<Covrpf> updateCovrincList;
    private List<Covrpf> insertCovrincList;
    private List<Payrpf> updatePayrList;
    private List<Incrpf> insertIncrList;
    private List<Ainrpf> insertAinrList;
    private Map<Long, Chdrpf> updateChdrlifMapTranno;
    private int ct01Value = 0;
    private int ct02Value = 0;
    private int ct03Value = 0;
    private int ct04Value = 0;
    private int ct05Value = 0;
    private int ct06Value = 0;
    private int ct07Value = 0;
    private int ct08Value = 0;
    private int ct09Value = 0;
    private int ct10Value = 0;
    private int ct11Value = 0;
    
    private int intBatchID = 0;
    private int intBatchExtractSize;
    private Covipf covipfRec;
    private Chdrpf chdrlifIO;
    private boolean riskPremflag = false; //ILIFE-7845 
	private static final String  RISKPREM_FEATURE_ID="NBPRP094";//ILIFE-7845 
	private boolean stampDutyflag = false;
	private boolean dialdownflag = false;
	private boolean reinstflag = false;
	private List<Ptrnpf> ptrnrecords = null;
	private static final String ta85 = "TA85";
	private boolean isFoundPro = false;
	private boolean changeExist = false;
	private Payrpf payrobj = null;
	private Ptrnpf ptrnpfReinstate = new Ptrnpf();
	private PackedDecimalData wsaaProCpiDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaLastCpiDate = new PackedDecimalData(8, 0);
	private PackedDecimalData tranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaFreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaOriginst01 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaOriginst02 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaOriginst03= new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaOrigsum = new PackedDecimalData(17, 2);
	private boolean prmhldtrad = false;
	private boolean autoIncrflag = false;
	private boolean reinstated = false;//ILIFE-8509
	private RertpfDAO rertpfDAO = getApplicationContext().getBean("rertpfDAO",RertpfDAO.class);
	private Map<String, List<Rertpf>> rertMap = null;
	private List<Rertpf> rertpfList = new ArrayList<>();
	private PackedDecimalData wsaaRertBinstprem = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaRertInstprem = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaRertLinstprem = new PackedDecimalData(17, 2).init(ZERO);
	private Rertpf rertpf= null;
	private boolean rertFound = true;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit2609,
		exit2809,
		exit3259,
		exit3939,
		exit6090, 
		exit7009,
		seExit8090, 
		dbExit8190,
	}

	public B5134() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/***** Restarting of this program is handled by MAINB,*/
		/***** using a restart method of '3'.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1001();
		setUpHeadingDates1002();
	}

protected void initialise1001()
	{
		/* (Must have a restart method of 3 to skip completed cycles.)*/
		if (isNE(bprdIO.getRestartMethod(),"3")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		ispermission = FeaConfg.isFeatureExist(bprdIO.getCompany().toString(),"NBPROP08",appVars,"IT"); 
		/* Point to the correct member of COVIPF.*/
		wsaaCoviRunid.set(bprdIO.getSystemParam04());
		wsaaCoviJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
//		StringUtil stringVariable1 = new StringUtil();
//		stringVariable1.addExpression("OVRDBF FILE(COVIPF) TOFILE(");
//		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
//		stringVariable1.addExpression("/");
//		stringVariable1.addExpression(wsaaCoviFn);
//		stringVariable1.addExpression(") ");
//		stringVariable1.addExpression("MBR(");
//		stringVariable1.addExpression(wsaaThreadMember);
//		stringVariable1.addExpression(")");
//		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
//		stringVariable1.setStringInto(wsaaQcmdexc);
//		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
//		covipf.openInput();
		
        if (bprdIO.systemParam01.isNumeric()){
            if (bprdIO.systemParam01.toInt() > 0){
                intBatchExtractSize = bprdIO.systemParam01.toInt();
            }else{
                intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
            }
        }else{
            intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();  
        }
        readChunkRecord();
		wsspEdterror.set(varcom.oK);
		initialize(wsaaT5687ArrayInner.wsaaT5687Array);
		initialize(wsaaT6658ArrayInner.wsaaT6658Array);
		/* INITIALIZE WSAA-T6634-ARRAY.                                 */
		initialize(wsaaTr384Array);
		autoIncrflag = FeaConfg.isFeatureExist("2", "CSCOM011", appVars, "IT");
	}

protected void setUpHeadingDates1002()
	{
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaTransDate.set(datcon1rec.intDate);
		wsaaTransTime.set(getCobolTime());
		/* Read T5679 for valid status codes for contract header and*/
		/* coverage.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* Load in the information from T5687. This will be searched*/
		/* later to determine the anniversary method for the contract,*/
		/* which will then be used to access the T6658-Array.*/
	    String coy = bsprIO.getCompany().toString();
	    t5687Map = itemDAO.loadSmartTable("IT", coy, "T5687");
		wsaaT5687Ix.set(1);
		loadT56871100();
		/* Read T6658 and load in details of the subroutine.*/
		t6658Map = itemDAO.loadSmartTable("IT", coy, "T6658");
		wsaaT6658Ix.set(1);
			loadT66581200();

		/* Load T6634, Auto Letters, to get the letter type for each*/
		/* contract.*/
		tr384Map = itemDAO.loadSmartTable("IT", coy, "TR384");
		wsaaTr384Ix.set(1);
			loadTr3841300();

		/* Read T5655 for lead days and from it calculate*/
		/* the Effective Date.*/
		wsaaT5655Trcode.set(bprdIO.getAuthCode());
		itemIO.setDataArea(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5655);
		itemIO.setItemitem(wsaaT5655Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5655rec.t5655Rec.set(itemIO.getGenarea());
		if (isEQ(t5655rec.leadDays,SPACES)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.freqFactor.set(t5655rec.leadDays);
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError600();
		}
		wsaaSysparam03.set(bprdIO.getSystemParam03());
		processContract.setTrue();
		firstTime.setTrue();
		dataNotProcessed.setTrue();
		tr695Map = itemDAO.loadSmartTable("IT", coy, "TR695");
		t5654Map = itemDAO.loadSmartTable("IT", coy, "T5654");
		String stampdutyItem = "NBPROP01";
		stampDutyflag = FeaConfg.isFeatureExist(coy, stampdutyItem, appVars, "IT");
	}

protected void loadT56871100()
	{
		/* If no item is retrieved or the item retrieved is not from*/
		/* the correct table, if this is the first read, this is a*/
		/* fatal error. If on a subsequent read, simply move ENDP to*/
		/* the status to end the loop.*/
        if (t5687Map == null || t5687Map.size() == 0) {
            syserrrec.params.set("t5687");
				fatalError600();
			}
		/* If the number of items stored exceeds the working storage*/
		/* array size, this is a fatal error.*/
        if (t5687Map.size() > wsaaT5687Size) {
			syserrrec.statuz.set(h791);
            syserrrec.params.set("t5687");
			fatalError600();
		}
        for (List<Itempf> items : t5687Map.values()) {
            for (Itempf item : items) {
		/* Move the necessary data from the table to the working*/
		/* storage array.*/
                    t5687rec.t5687Rec.set(StringUtil.rawToString(item.getGenarea()));
                    wsaaT5687ArrayInner.wsaaT5687Currfrom[wsaaT5687Ix.toInt()].set(item.getItmfrm());
                    wsaaT5687ArrayInner.wsaaT5687Crtable[wsaaT5687Ix.toInt()].set(item.getItemitem());
		wsaaT5687ArrayInner.wsaaT5687Annmthd[wsaaT5687Ix.toInt()].set(t5687rec.anniversaryMethod);
		wsaaT5687ArrayInner.wsaaT5687Bcmthd[wsaaT5687Ix.toInt()].set(t5687rec.basicCommMeth);
		wsaaT5687ArrayInner.wsaaT5687Bascpy[wsaaT5687Ix.toInt()].set(t5687rec.bascpy);
		wsaaT5687ArrayInner.wsaaT5687Rnwcpy[wsaaT5687Ix.toInt()].set(t5687rec.rnwcpy);
		wsaaT5687ArrayInner.wsaaT5687Srvcpy[wsaaT5687Ix.toInt()].set(t5687rec.srvcpy);
		wsaaT5687Ix.add(1);
	}

        }
		}

protected void loadT66581200()
	{
		/* If no item is retrieved or the item retrieved is not from*/
		/* the correct table, if this is the first read, this is a*/
		/* fatal error. If on a subsequent read, simply move ENDP to*/
		/* the status to end the loop.*/
        if (t6658Map == null || t6658Map.size() == 0) {
            syserrrec.params.set("t6658");
				fatalError600();
			}
		/* If the number of items stored exceeds the working storage*/
		/* array size, this is a fatal error.*/
        if (t6658Map.size() > wsaaT6658Size) {
			syserrrec.statuz.set(h791);
            syserrrec.params.set("t6658");
			fatalError600();
		}
        for (List<Itempf> items : t6658Map.values()) {
            for (Itempf item : items) {
		/* Move the necessary data from the table to the working*/
		/* storage array.*/
        t6658rec.t6658Rec.set(StringUtil.rawToString(item.getGenarea()));
        wsaaT6658ArrayInner.wsaaT6658Annmthd[wsaaT6658Ix.toInt()].set(item.getItemitem());
        wsaaT6658ArrayInner.wsaaT6658Currfrom[wsaaT6658Ix.toInt()].set(item.getItmfrm());
		wsaaT6658ArrayInner.wsaaT6658Billfreq[wsaaT6658Ix.toInt()].set(t6658rec.billfreq);
		wsaaT6658ArrayInner.wsaaT6658Fixdtrm[wsaaT6658Ix.toInt()].set(t6658rec.fixdtrm);
		wsaaT6658ArrayInner.wsaaT6658Manopt[wsaaT6658Ix.toInt()].set(t6658rec.manopt);
		wsaaT6658ArrayInner.wsaaT6658Maxpcnt[wsaaT6658Ix.toInt()].set(t6658rec.maxpcnt);
		wsaaT6658ArrayInner.wsaaT6658Minpcnt[wsaaT6658Ix.toInt()].set(t6658rec.minpcnt);
		wsaaT6658ArrayInner.wsaaT6658Optind[wsaaT6658Ix.toInt()].set(t6658rec.optind);
		wsaaT6658ArrayInner.wsaaT6658Subprog[wsaaT6658Ix.toInt()].set(t6658rec.subprog);
		wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()].set(t6658rec.premsubr);
		//ILIFE-6569
		wsaaT6658ArrayInner.wsaaT6658SimpInd[wsaaT6658Ix.toInt()].set(t6658rec.simpleInd);
		wsaaT6658ArrayInner.wsaaT6658IncrFlg[wsaaT6658Ix.toInt()].set(t6658rec.incrFlg);
		wsaaT6658ArrayInner.wsaaT6658CompoundInd[wsaaT6658Ix.toInt()].set(t6658rec.compoundInd);
		wsaaT6658Ix.add(1);
            }
        }
	}

	/**
	* <pre>
	*1300-LOAD-T6634 SECTION.
	* </pre>
	*/
protected void loadTr3841300()
	{ 
        /* If no item is retrieved or the item retrieved is not from*/
        /* the correct table, if this is the first read, this is a*/
        /* fatal error. If on a subsequent read, simply move ENDP to*/
        /* the status to end the loop.*/
        if (tr384Map == null || tr384Map.size() == 0) {
            syserrrec.params.set("tr384");
				fatalError600();
			}
		/* If the number of items stored exceeds the working storage*/
		/* array size, this is a fatal error.*/
		/* IF  WSAA-T6634-IX            > WSAA-T6634-SIZE               */
        if (tr384Map.size() > wsaaTr384Size) {
			syserrrec.statuz.set(h791);
			/*     MOVE T6634              TO SYSR-PARAMS                   */
            syserrrec.params.set("tr384");
			fatalError600();
		}
        for (List<Itempf> items : tr384Map.values()) {
            for (Itempf item : items) {
		/* Move the necessary data from the table to the working*/
		/* storage array.*/
		/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.              */
		/* MOVE ITEM-ITEMITEM          TO WSAA-T6634-KEY                */
		/*                                    (WSAA-T6634-IX).          */
		/* MOVE T6634-LETTER-TYPE      TO WSAA-T6634-LETTER-TYPE        */
		/*                                    (WSAA-T6634-IX).          */
		/* SET WSAA-T6634-IX-MAX       TO WSAA-T6634-IX.*/
		/* SET WSAA-T6634-IX UP        BY 1.*/
                    tr384rec.tr384Rec.set(StringUtil.rawToString(item.getGenarea()));
                    wsaaTr384Key[wsaaTr384Ix.toInt()].set(item.getItemitem());
		wsaaTr384LetterType[wsaaTr384Ix.toInt()].set(tr384rec.letterType);
		wsaaTr384Ix.add(1);
	}

        }
		}

protected void readFile2000()
	{
		/* Make sure that the last contract goes through*/
		/* the normal change-of-contract processing*/
		/* (If no contracts processed, do not do this).*/
        if (!iteratorList.hasNext()) {
            intBatchID++;
            clearList2100();
            readChunkRecord();
            if (!iteratorList.hasNext()) {
			if (dataProcessed.isTrue()) {
						
				contractChange3900();
			}
			wsspEdterror.set(varcom.endp);
			return ;
		}
        }
        covipfRec = iteratorList.next();
        ct01Value++;
		/* Set NEW-CONTRACT to true.  If we are dealing with a*/
		/* multi-coverage contract we will capture that next.*/
		newContract.setTrue();
		prmhldtrad = FeaConfg.isFeatureExist(covipfRec.getChdrcoy(), "CSOTH010", appVars, "IT");/* IJTI-1523 */
		if(prmhldtrad) {
			 if (isEQ(covipfRec.getChdrnum(),wsysChdrnum))
				 sameContract.setTrue();
		}
		else {
	        if (isEQ(covipfRec.getChdrnum(),wsysChdrnum)
	        && isEQ(covipfRec.getCoverage(),wsysCoverage)) {//ILIFE-8037
				sameContract.setTrue();
			}
		}
        wsysChdrnum.set(covipfRec.getChdrnum());
        wsysCoverage.set(covipfRec.getCoverage());
	}

private void readChunkRecord(){
    List<Covipf> covipfList = covipfDAO.searchCoviRecord(wsaaCoviFn.toString(), wsaaThreadMember.toString(), intBatchExtractSize, intBatchID);
    iteratorList = covipfList.iterator();
    if(!covipfList.isEmpty()){
        Set<String> chdrnumSet = new LinkedHashSet<>();
        for(Covipf h:covipfList){
            chdrnumSet.add(h.getChdrnum());
        }
        List<String> chdrList = new ArrayList<>(chdrnumSet);
        chdrlifRgpMap = chdrpfDAO.searchChdrRecordByChdrnum(chdrList);
        covrincMap = covrpfDAO.searchValidCovrByChdrnum(chdrList);
        incrMap = incrpfDAO.searchIncrRecordByChdrnum(chdrList);
        payrMap = payrpfDAO.searchPayrRecordByChdrnumChdrcoy(bsprIO.getCompany().trim(),chdrList);
        rertMap = rertpfDAO.searchRertRecordByChdrnum(bsprIO.getCompany().toString().trim(), chdrList);
    }
}

    private void clearList2100() {
        iteratorList = null;
        if (chdrlifRgpMap != null) {
            chdrlifRgpMap.clear();
		}
        if (covrincMap != null) {
            covrincMap.clear();
        }
        if (incrMap != null) {
            incrMap.clear();
        }
        if (payrMap != null) {
            payrMap.clear();
        }
        if (rertMap != null) {
        	rertMap.clear();
        } 
    }
protected void edit2500()
	{
		/* Check record is required for processing.*/
		/* Note that if an individual component does not have*/
		/* the right status, that component is ignored but*/
		/* the rest of the contract is processed.*/
        if (isEQ(wsaaChdrChdrnum,covipfRec.getChdrnum())
        && isEQ(wsaaChdrChdrcoy,covipfRec.getChdrcoy())) {
			if (ignoreContract.isTrue()) {
				wsspEdterror.set(SPACES);
                ct09Value++;
				return ;
			}
		}
        Chdrpf chdrrgpIO = readChdrrgp2700();
		invalidStatus.setTrue();
        checkContractStatus2800(chdrrgpIO);
		if (invalidStatus.isTrue()) {
			ignoreContract.setTrue();
			wsspEdterror.set(SPACES);
            ct09Value++;
			return ;
		}
		invalidStatus.setTrue();
		checkComponentStatus2600();
		if (invalidStatus.isTrue()) {
			wsspEdterror.set(SPACES);
            ct10Value++;
			return ;
		}
		wsspEdterror.set(varcom.oK);
		/* Set up the break fields if we are here for the*/
		/* first time so that the break check below does*/
		/* not trigger on the first record we process.*/
		if (firstTime.isTrue()) {
            wsaaChdrChdrnum.set(covipfRec.getChdrnum());
            wsaaChdrChdrcoy.set(covipfRec.getChdrcoy());
			notFirstTime.setTrue();
		}
	}

protected void checkComponentStatus2600()
	{
		if(risk2601()){
			prem2602();
		}
			/* Expected exception for control flow purposes. */
	}

protected boolean risk2601()
	{
		invalidComponent.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub,12)
		|| validComponent.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()],covipfRec.getStatcode())) {
				validComponent.setTrue();
			}
		}
		if (!validComponent.isTrue()) {
			wsspEdterror.set(SPACES);
			return false;
		}
		return true;
	}

protected void prem2602()
	{
		invalidComponent.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub,12)
		|| validComponent.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()],covipfRec.getPstatcode())) {
				validComponent.setTrue();
			}
		}
		if (!validComponent.isTrue()) {
			wsspEdterror.set(SPACES);
			return ;
		}
		validStatus.setTrue();
	}

protected Chdrpf readChdrrgp2700()
	{
        Chdrpf chdrrgpIO = null;
        if (chdrlifRgpMap != null && chdrlifRgpMap.containsKey(covipfRec.getChdrnum())) {
            for (Chdrpf c : chdrlifRgpMap.get(covipfRec.getChdrnum())) {
                if (c.getChdrcoy().toString().equals(covipfRec.getChdrcoy())) {/* IJTI-1523 */
                    chdrrgpIO = c;
                    break;
	}
            }
        }
        if (chdrrgpIO == null) {
            syserrrec.params.set(covipfRec.getChdrnum());
			fatalError600();
		}
        return chdrrgpIO;
	}

protected void checkContractStatus2800(Chdrpf chdrrgpIO)
	{
		if(risk2801(chdrrgpIO)){
		    prem2802(chdrrgpIO);
		}
			/* Expected exception for control flow purposes. */
	}

protected boolean risk2801(Chdrpf chdrrgpIO)
	{
		invalidContract.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub,12)
		|| validContract.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Sub.toInt()],chdrrgpIO.getStatcode())) {
				validContract.setTrue();
			}
		}
		if (!validContract.isTrue()) {
			wsspEdterror.set(SPACES);
			return false;
		}
		return true;
	}

protected void prem2802(Chdrpf chdrrgpIO)
	{
		invalidContract.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub,12)
		|| validContract.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.cnPremStat[wsaaT5679Sub.toInt()],chdrrgpIO.getPstcde())) {
				validContract.setTrue();
			}
		}
		if (!validContract.isTrue()) {
			wsspEdterror.set(SPACES);
			return ;
		}
		validStatus.setTrue();
	}

protected void update3000()
	{

		/* Check for change of contract here.*/
        if (isNE(covipfRec.getChdrnum(),wsaaChdrChdrnum)
        || isNE(covipfRec.getChdrcoy(),wsaaChdrChdrcoy)) {
			//ILIFE-4323 start by dpuhawan
            if (newContract.isTrue() && chdrlifIO != null) {
                chdrlifIO.setTranno(chdrlifIO.getTranno()+1);
			}
			//ILIFE-4323 end				
			contractChange3900();
		}
		softlock5000();
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			ignoreContract.setTrue();
            ct11Value++;
			conlogrec.error.set("LOCK");
			StringUtil stringVariable1 = new StringUtil();
            stringVariable1.addExpression(covipfRec.getChdrcoy());
			stringVariable1.addExpression("|");
            stringVariable1.addExpression(covipfRec.getChdrnum());
			stringVariable1.addExpression("|");
            stringVariable1.addExpression(covipfRec.getLife());
			stringVariable1.addExpression("|");
            stringVariable1.addExpression(covipfRec.getCoverage());
			stringVariable1.addExpression("|");
            stringVariable1.addExpression(covipfRec.getRider());
			stringVariable1.addExpression("|");
            stringVariable1.addExpression(covipfRec.getCrtable());
			stringVariable1.setStringInto(conlogrec.params);
			callConlog003();
			wsspEdterror.set(SPACES);
			return ;
		}
		processContract.setTrue();
		readChdrlif3960();
		dataProcessed.setTrue();
		reinstflag = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "CSLRI003", appVars, "IT");
		if(reinstflag){
			wsaaSub.set(ZERO);
			markprorated();
		}
		if(isFoundPro){	
		while(isLT(covipfRec.getCpiDate(),wsaaProCpiDate)){
			searchT56875100();
	        if (isNE(covipfRec.getRider(),"00")) {
				a100ReadTableTr695();
			}
			searchT66585200();
			payrobj = callIncPremsubr3100();
			if ((isEQ(incrsrec.currsum,incrsrec.newsum)	&& isEQ(incrsrec.currinst01,incrsrec.newinst01)) || (isEQ(incrsrec.currsum,ZERO)
					&& isEQ(incrsrec.currinst01,ZERO))) {
				 setupNextCall();
				 noIncreaseOnCovr3050();
			}
			else{
				changeExist = true;
				wsaaSub.add(1);
				existingIncrease3400();
				writeIncr3300();
				writeAinr3700();
			    setupNextCall();	   
			}	
		 }
		if(changeExist){
			updateCovr3800();
			changeExist = false;
		}
		rewriteChdrlifPayr3950(payrobj);
	   }
		else{
		searchT56875100();
	       if (isNE(covipfRec.getRider(),"00")) {
			a100ReadTableTr695();
		}
		searchT66585200();
        Payrpf p = callIncPremsubr3100();
		/* If there has been no increase on the component even though*/
		/* one is allowed, the CPI-DATE on the COVR must still be*/
		/* updated. This will prevent selection until the next time an*/
		/* increase is due.*/
		if (isEQ(incrsrec.currsum,incrsrec.newsum)
		&& isEQ(incrsrec.currinst01,incrsrec.newinst01)) {
			noIncreaseOnCovr3050();
            rewriteChdrlifPayr3950(p);
			releaseSoftlock5500();
			return ;
		}
		if (isEQ(incrsrec.currsum,ZERO)
		&& isEQ(incrsrec.currinst01,ZERO)) {
			noIncreaseOnCovr3050();
            rewriteChdrlifPayr3950(p);
			releaseSoftlock5500();
			return ;
		}
		existingIncrease3400();
		writeIncr3300();
		writeAinr3700();
		updateCovr3800();
        rewriteChdrlifPayr3950(p);
		}
		releaseSoftlock5500();	
	}

protected void markprorated(){
	
   	ptrnrecords = ptrnpfDAO.searchPtrnenqRecord(covipfRec.getChdrcoy(), covipfRec.getChdrnum());
   	if(ptrnrecords != null){
   		for(Ptrnpf ptrn : ptrnrecords ){
   		if(isNE(ptrn.getBatctrcde(),ta85) && isNE(ptrn.getBatctrcde(),"B523")){
   			continue;
   		}
   		if(isEQ(ptrn.getBatctrcde(),"B523")){
   			break;
   		}
   		if(isEQ(ptrn.getBatctrcde(),ta85)){
   			isFoundPro = true;
   			ptrnpfReinstate = ptrnpfDAO.getPtrnData(covipfRec.getChdrcoy(), covipfRec.getChdrnum(), ta85);
   			calcProCpi();
   		}
   	}
  }
}

protected void calcProCpi(){
	
	datcon2rec.freqFactor.set(t5655rec.leadDays);
	datcon2rec.frequency.set("DY");
	datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz,varcom.oK)) {
		syserrrec.statuz.set(datcon2rec.statuz);
		syserrrec.params.set(datcon2rec.datcon2Rec);
		fatalError600();
	}
	wsaaProCpiDate.set(datcon2rec.intDate2);
	if(isEQ(wsaaLastCpiDate,ZERO)){
		wsaaLastCpiDate.set(chdrlifIO.getBtdate());
		tranno.set(chdrlifIO.getTranno());
		wsaaFreq.set(chdrlifIO.getBillfreq());
	}
		
}
protected void setupNextCall(){
	
	datcon2rec.datcon2Rec.set(SPACES);
	datcon2rec.intDate1.set(covipfRec.getCpiDate()); 
	datcon2rec.frequency.set("01");
	datcon2rec.freqFactor.set(wsaaT6658ArrayInner.wsaaT6658Billfreq[wsaaT6658Ix.toInt()]);
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz,varcom.oK)) {
		syserrrec.statuz.set(datcon2rec.statuz);
		syserrrec.params.set(datcon2rec.datcon2Rec);
		fatalError600();
	}
	covipfRec.setCpiDate(datcon2rec.intDate2.toInt());
	covipfRec.setInstprem(incrsrec.newinst01.getbigdata());
	covipfRec.setZbinstprem(incrsrec.newinst02.getbigdata());
	covipfRec.setZlinstprem(incrsrec.newinst03.getbigdata());
	covipfRec.setSumins(incrsrec.newsum.getbigdata());
	wsaaOriginst01.set(incrsrec.originst01);
	wsaaOriginst02.set(incrsrec.originst02);
	wsaaOriginst03.set(incrsrec.originst03);
	wsaaOrigsum.set(incrsrec.origsum);
	
}

protected void noIncreaseOnCovr3050()
	{
        Covrpf covrincIO = readCovrinc3051();
		rewriteCovrinc3052(covrincIO);
	}

protected Covrpf readCovrinc3051()
	{
        Covrpf covrincIO = null;
        if(covrincMap!=null&&covrincMap.containsKey(covipfRec.getChdrnum())){
            for(Covrpf c:covrincMap.get(covipfRec.getChdrnum())){
                if(covipfRec.getChdrcoy().equals(c.getChdrcoy())&&covipfRec.getLife().equals(c.getLife())&&covipfRec.getJlife().equals(c.getJlife())&&covipfRec.getCoverage().equals(c.getCoverage())&&covipfRec.getRider().equals(c.getRider())&&covipfRec.getPlanSuffix()==c.getPlanSuffix()){
                    covrincIO = c;
                    break;
                }
            }
		}
		/* Update the COVR record with a new CPI-DATE so that it does*/
		/* not get selected until the next increase 'Anniversary'.*/
		datcon2rec.datcon2Rec.set(SPACES);
		if (covrincIO != null){ //IJTI-462
			datcon2rec.intDate1.set(covrincIO.getCpiDate()); //IJTI-462
		} //IJTI-462
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(wsaaT6658ArrayInner.wsaaT6658Billfreq[wsaaT6658Ix.toInt()]);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError600();
		}
		return covrincIO;
	}

protected void rewriteCovrinc3052(Covrpf covrincIO)
	{
		covrincIO.setCpiDate(datcon2rec.intDate2.toInt());
		if(updateCovrincDateList == null){
		    updateCovrincDateList = new ArrayList<Covrpf>();
		}
		updateCovrincDateList.add(covrincIO);
		ct08Value++;
	}

protected Payrpf callIncPremsubr3100()
	{
        Payrpf payrIO = null;
        if(payrMap!=null&&payrMap.containsKey(covipfRec.getChdrnum())){
            for(Payrpf p:payrMap.get(covipfRec.getChdrnum())){
                if(covipfRec.getChdrcoy().equals(p.getChdrcoy())&& 1== p.getPayrseqno()){
                    payrIO = p;
                    break;
                }
            }
        }
        if(payrIO == null){
            syserrrec.params.set(covipfRec.getChdrnum());
			fatalError600();
		} 
        rertFound = false;
        if (rertMap != null && rertMap.containsKey(covipfRec.getChdrnum())) {
			List<Rertpf> rertList = rertMap.get(covipfRec.getChdrnum());
			for (Iterator<Rertpf> iterator = rertList.iterator(); iterator.hasNext();) {
				Rertpf rert = iterator.next();
				if (isEQ(rert.getEffdate(), covipfRec.getCpiDate()) &&rert.getLife().equals(covipfRec.getLife()) && rert.getCoverage().equals(covipfRec.getCoverage())
						&& rert.getRider().equals(covipfRec.getRider())
						&& Integer.compare(rert.getPlnsfx(), covipfRec.getPlanSuffix()) == 0) {
					rertFound = true;
					rertpf = rert;
					rert.setValidflag("2");
					rert.setTranno(chdrlifIO.getTranno());
					rertpfList.add(rert);
					break;
				}
			}
		}
        
		incrsrec.origsum.set(ZERO);
		incrsrec.lastsum.set(ZERO);
		incrsrec.newsum.set(ZERO);
		incrsrec.originst01.set(ZERO);
		incrsrec.lastinst01.set(ZERO);
		incrsrec.newinst01.set(ZERO);
		incrsrec.currinst01.set(ZERO);
		incrsrec.originst02.set(ZERO);
		incrsrec.lastinst02.set(ZERO);
		incrsrec.newinst02.set(ZERO);
		incrsrec.currinst02.set(ZERO);
		incrsrec.originst03.set(ZERO);
		incrsrec.lastinst03.set(ZERO);
		incrsrec.newinst03.set(ZERO);
		incrsrec.currinst03.set(ZERO);
		incrsrec.pctinc.set(ZERO);
		incrsrec.calcprem.set(ZERO);
		incrsrec.inputPrevPrem.set(ZERO);
		if(rertFound){
			wsaaRertInstprem.set(covipfRec.getInstprem());
			wsaaRertBinstprem.set(covipfRec.getZbinstprem());
			wsaaRertLinstprem.set(covipfRec.getZlinstprem());
			covipfRec.setInstprem(rertpf.getNewinst());
			covipfRec.setZbinstprem(rertpf.getZbnewinst());
			covipfRec.setZlinstprem(rertpf.getZlnewinst());	
		}
		incrsrec.currinst01.set(covipfRec.getInstprem());
		incrsrec.currinst02.set(covipfRec.getZbinstprem());
		incrsrec.currinst03.set(covipfRec.getZlinstprem());
        incrsrec.currsum.set(covipfRec.getSumins());
        getOrigLastDetails3200();
		incrsrec.function.set(SPACES);
		incrsrec.statuz.set(SPACES);
        incrsrec.chdrcoy.set(covipfRec.getChdrcoy());
        incrsrec.chdrnum.set(covipfRec.getChdrnum());
        incrsrec.coverage.set(covipfRec.getCoverage());
        incrsrec.life.set(covipfRec.getLife());
        incrsrec.rider.set(covipfRec.getRider());
        incrsrec.plnsfx.set(covipfRec.getPlanSuffix());
        incrsrec.effdate.set(covipfRec.getCpiDate());
        incrsrec.crtable.set(covipfRec.getCrtable());
        incrsrec.rcesdte.set(covipfRec.getRiskCessDate());
        incrsrec.mortcls.set(covipfRec.getMortcls());
		incrsrec.billfreq.set(payrIO.getBillfreq());
		incrsrec.mop.set(payrIO.getBillchnl());
		incrsrec.occdate.set(chdrlifIO.getOccdate());
        incrsrec.cntcurr.set(covipfRec.getPremCurrency());
		incrsrec.language.set(bsscIO.getLanguage());
		if(stampDutyflag) {
			incrsrec.zstpduty01.set(ZERO);
			incrsrec.zstpduty02.set(ZERO);
			incrsrec.zstpduty03.set(ZERO);
		}
		if (isEQ(wsaaT6658ArrayInner.wsaaT6658Maxpcnt[wsaaT6658Ix.toInt()], ZERO)) {
			incrsrec.maxpcnt.set(+99.999);
		}
		else {
			incrsrec.maxpcnt.set(wsaaT6658ArrayInner.wsaaT6658Maxpcnt[wsaaT6658Ix.toInt()]);
		}
		incrsrec.minpcnt.set(wsaaT6658ArrayInner.wsaaT6658Minpcnt[wsaaT6658Ix.toInt()]);
		incrsrec.fixdtrm.set(wsaaT6658ArrayInner.wsaaT6658Fixdtrm[wsaaT6658Ix.toInt()]);
		incrsrec.annvmeth.set(wsaaT5687ArrayInner.wsaaT5687Annmthd[wsaaT5687Ix.toInt()]);
		
		/*
		IVE-866 Life - Auto Increase Calculation Rework- Integration with latest PA compatible models started*/
		//callProgram(wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()], incrsrec.increaseRec);
		/*ILIFE-3915 	ALS-358 - premium is not increased when benefit amount is increased Start*/
		if(ispermission){
			incrsrec.autoincreaseindicator.set("");
			readT5654();
			if (isNE(t5654rec.indxflg,"N"))
			{
				if(isNE(t5654rec.cpidef,SPACES)||isNE(t5654rec.cpiallwd,SPACES))
					incrsrec.autoincreaseindicator.set("C");
				if(isNE(t5654rec.predef,SPACES)||isNE(t5654rec.predefallwd,SPACES))
					incrsrec.autoincreaseindicator.set("P");
			}

		}
		else
			incrsrec.autoincreaseindicator.set("P");
		
		/*ILIFE-8302 start*/
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable()
				&& er.isCallExternal(wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()].toString()) 
				&& er.isExternalized(chdrlifIO.getCnttype(), incrsrec.crtable.toString()))) /* IJTI-1523 */
		{
			callProgramX(wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()], incrsrec.increaseRec);		
		}
		/*ILIFE-8302 end*/
		else
		{
			premiumrec.waitperiod.set(SPACES);
			premiumrec.bentrm.set(SPACES);
			premiumrec.prmbasis.set(SPACES);
			premiumrec.poltyp.set(SPACES);
			premiumrec.occpcode.set(SPACES);
			if(ispermission){
				callReadRCVDPF();
				if(rcvdPFObject != null){
					premiumrec.waitperiod.set(rcvdPFObject.getWaitperiod());
					premiumrec.bentrm.set(rcvdPFObject.getBentrm());
					if(rcvdPFObject.getPrmbasis()!=null && isEQ(rcvdPFObject.getPrmbasis(),"S"))/*ILIFE-4171*/
						premiumrec.prmbasis.set("Y");
					else
						premiumrec.prmbasis.set("");
					premiumrec.poltyp.set(rcvdPFObject.getPoltyp());
					dialdownflag = FeaConfg.isFeatureExist(chdrlifIO.getChdrcoy().toString().trim(), "NBPRP012", appVars, "IT");
					if(dialdownflag) {
						if(rcvdPFObject.getDialdownoption() != null) {
							if(rcvdPFObject.getDialdownoption().startsWith("0")) {
								premiumrec.dialdownoption.set(rcvdPFObject.getDialdownoption().substring(1));
							}
							else {
								premiumrec.dialdownoption.set(rcvdPFObject.getDialdownoption());
							}
						}
					}
				}
			}
			/*ILIFE-3915 	End*/
			calc6005();
			premiumrec.cnttype.set(chdrlifIO.getCnttype()); 
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			
			premiumrec.function.set("INCR");
			/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
			itemIO.setDataArea(SPACES);
			itemIO.setStatuz(varcom.oK);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(incrsrec.chdrcoy);
			itemIO.setItemtabl(t5687);
			itemIO.setItemitem(incrsrec.crtable);
			itemIO.setFormat(formatsInner.itemrec);
			itemIO.setFunction(varcom.readr);
			itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itemIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)
					&& isNE(itemIO.getStatuz(),varcom.endp)) {
						syserrrec.params.set(itemIO.getParams());
						syserrrec.statuz.set(itemIO.getStatuz());
						fatalError600();
					}

			t5687rec.t5687Rec.set(itemIO.getGenarea());
			if (isEQ(lifergpIO.getStatuz(),varcom.mrnf)) {
				premiumrec.premMethod.set(t5687rec.premmeth);
			}
			else {
				premiumrec.premMethod.set(t5687rec.jlPremMeth);
			}
			
			Covrpf covrpf = null;
	        if (covrincMap != null && covrincMap.containsKey(covipfRec.getChdrnum())) {
	            for (Covrpf c : covrincMap.get(covipfRec.getChdrnum())) {
	                if (covipfRec.getChdrcoy().equals(c.getChdrcoy()) && covipfRec.getLife().equals(c.getLife())
	                        && covipfRec.getCoverage().equals(c.getCoverage()) && covipfRec.getRider().equals(c.getRider())
	                        && covipfRec.getPlanSuffix() == c.getPlanSuffix()) {
	                    covrpf = c;
	                    break;
	                }
	            }
	        }
	        premiumrec.rstate01.set(covrpf.getZclstate() == null ? SPACE : covrpf.getZclstate());
			if(stampDutyflag) {
				LinkageInfoService linkgService = new LinkageInfoService();;
				premiumrec.linkcov.set(linkgService.getLinkageInfo(covrpf.getLnkgno()));
			}
			
			/*ILIFE-4868 start*/
			
			if(isEQ(incrsrec.annvmeth,"AN14")){
					incrsrec.annvmeth.set("AN03");
			}
			if (isEQ(incrsrec.autoincreaseindicator,SPACE)){
				incrsrec.autoincreaseindicator.set("P");
			}
			
			/*ILIFE-4868 end*/
			
			/* ILIFE-3142 End */
			//ILIFE-6569
			incrsrec.simpleInd.set(wsaaT6658ArrayInner.wsaaT6658SimpInd[wsaaT6658Ix.toInt()]);
			incrsrec.compoundInd.set(wsaaT6658ArrayInner.wsaaT6658CompoundInd[wsaaT6658Ix.toInt()]);
			if(covrpf!=null && covrpf.getReinstated()!=null && "Y".equals(covrpf.getReinstated()))//ILIFE-8509
				reinstated = true;
			if(reinstated) {
				setIncrsrec();
			}
			callProgram(wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()], incrsrec.increaseRec,premiumrec,vpxlextrec, vpxacblrec);

			
			if (isEQ(incrsrec.statuz,varcom.bomb)) {
				syserrrec.statuz.set(incrsrec.statuz);
				syserrrec.params.set(incrsrec.increaseRec);
				fatalError600();
			}			
			if (isNE(incrsrec.newsum,ZERO)) {
				//ILIFE-2395 S6242_Premium Difference_After_Auto Increase  started
				//incrsrec.newsum.set(incrsrec.newsum);
				callProgram(wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()],incrsrec.increaseRec,premiumrec,vpxlextrec, vpxacblrec);
			}
			
			/*if (isNE(incrsrec.newinst01,ZERO)) {
				incrsrec.newinst01.set(sub(incrsrec.newinst01 ,incrsrec.currinst01));
			}
			if (isNE(incrsrec.newinst02,ZERO)) {
				incrsrec.newinst02.set(sub(incrsrec.newinst02 ,incrsrec.currinst02));
			}
			if (isNE(incrsrec.newinst03,ZERO)) {
				incrsrec.newinst03.set(sub(incrsrec.newinst03 ,incrsrec.currinst03));
			}*/
			
			//ILIFE-2395 S6242_Premium Difference_After_Auto Increase end
		
		}
		//ILIFE-7845 
		premiumrec.riskPrem.set(BigDecimal.ZERO);
		riskPremflag = FeaConfg.isFeatureExist(covipfRec.getChdrcoy().trim(), RISKPREM_FEATURE_ID, appVars, "IT");/* IJTI-1523 */
		if(riskPremflag)
		{
			premiumrec.riskPrem.set(ZERO);
			premiumrec.cnttype.set(chdrlifIO.getCnttype());
			premiumrec.crtable.set(covipfRec.getCrtable());
			premiumrec.calcTotPrem.set(covipfRec.getInstprem());
			callProgram("RISKPREMIUM", premiumrec.premiumRec);
			
	}
		//ILIFE-7845 End
		if (isEQ(incrsrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(incrsrec.statuz);
			syserrrec.params.set(incrsrec.increaseRec);
			fatalError600();
		}
		if (isEQ(incrsrec.newsum,ZERO)) {
			incrsrec.newsum.set(incrsrec.currsum);
		}
		if (isEQ(incrsrec.newinst01,ZERO)) {
			incrsrec.newinst01.set(incrsrec.currinst01);
		}
		if (isEQ(incrsrec.newinst02,ZERO)) {
			incrsrec.newinst02.set(incrsrec.currinst02);
		}
		if (isEQ(incrsrec.newinst03,ZERO)) {
			incrsrec.newinst03.set(incrsrec.currinst03);
		}
		if(stampDutyflag) {
			if(isNE(incrsrec.newinst01, ZERO) && isNE(incrsrec.zstpduty01, ZERO)) {
				incrsrec.newinst01.set(add(incrsrec.newinst01, incrsrec.zstpduty01));
			}
			if(isNE(incrsrec.newinst02, ZERO) && isNE(incrsrec.zstpduty02, ZERO)) {
				incrsrec.newinst02.set(add(incrsrec.newinst02, incrsrec.zstpduty02));
			}
			if(isNE(incrsrec.newinst03, ZERO) && isNE(incrsrec.zstpduty03, ZERO)) {
				incrsrec.newinst03.set(add(incrsrec.newinst03, incrsrec.zstpduty03));
			}
		}
        return payrIO;
	}

    private void readT5654() {
        Itempf item5654 = null;
        if (t5654Map != null && t5654Map.containsKey(chdrlifIO.getCnttype())) {
            item5654 = t5654Map.get(chdrlifIO.getCnttype()).get(0);

        }
        if (item5654 == null) {
            syserrrec.params.set("t5654:" + chdrlifIO.getCnttype());
            fatalError600();
        } else { //IJTI-462
        	t5654rec.t5654Rec.set(StringUtil.rawToString(item5654.getGenarea())); //IJTI-462
        } //IJTI-462
    }
/*ILIFE-3915 	ALS-358 - premium is not increased when benefit amount is increased Start*/
protected void callReadRCVDPF(){
	if(rcvdPFObject == null)
		rcvdPFObject= new Rcvdpf();
        rcvdPFObject.setChdrcoy(covipfRec.getChdrcoy());
        rcvdPFObject.setChdrnum(covipfRec.getChdrnum());
        rcvdPFObject.setLife(covipfRec.getLife());
        rcvdPFObject.setCoverage(covipfRec.getCoverage());
        rcvdPFObject.setRider(covipfRec.getRider());
        rcvdPFObject.setCrtable(covipfRec.getCrtable());
	rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
}
/*ILIFE-3915 	End*/
protected void calc6005()
	{
		premiumrec.function.set("INCR");
		premiumrec.chdrChdrcoy.set(incrsrec.chdrcoy);
		premiumrec.chdrChdrnum.set(incrsrec.chdrnum);
		premiumrec.lifeLife.set(incrsrec.life);
		premiumrec.lifeJlife.set("00");
		premiumrec.covrCoverage.set(incrsrec.coverage);
		premiumrec.covrRider.set(incrsrec.rider);
		premiumrec.crtable.set(incrsrec.crtable);
		premiumrec.effectdt.set(incrsrec.effdate);
		premiumrec.termdate.set(incrsrec.rcesdte);
		premiumrec.language.set(incrsrec.language);
		getLifeDetails7000();
		datcon3rec.intDate1.set(incrsrec.effdate);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError8000();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		premiumrec.currcode.set(incrsrec.cntcurr);
		premiumrec.sumin.set(incrsrec.origsum);
		premiumrec.mortcls.set(incrsrec.mortcls);
		premiumrec.billfreq.set(incrsrec.billfreq);
		premiumrec.mop.set(incrsrec.mop);
		premiumrec.ratingdate.set(incrsrec.occdate);
		premiumrec.reRateDate.set(incrsrec.occdate);
		premiumrec.calcPrem.set(incrsrec.currinst01);//ILIFE-7336
		premiumrec.calcBasPrem.set(ZERO);
		premiumrec.calcLoaPrem.set(ZERO);
		if (isEQ(t5675rec.premsubr,SPACES)) {
			incrsrec.newinst01.set(incrsrec.currinst01);
			//goTo(GotoLabel.exit6090);
		}
		getAnny8000();
		
		
	}

protected void getLifeDetails7000()
{
	lifergpIO.setChdrcoy(incrsrec.chdrcoy);
	lifergpIO.setChdrnum(incrsrec.chdrnum);
	lifergpIO.setLife(incrsrec.life);
	lifergpIO.setJlife("00");
	lifergpIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, lifergpIO);
	if (isNE(lifergpIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(lifergpIO.getParams());
		dbError8100();
	}
	calculateAnb7500();
	premiumrec.lage.set(wsaaAnb);
	premiumrec.lsex.set(lifergpIO.getCltsex());
	lifergpIO.setChdrcoy(incrsrec.chdrcoy);
	lifergpIO.setChdrnum(incrsrec.chdrnum);
	lifergpIO.setLife(incrsrec.life);
	lifergpIO.setJlife("01");
	lifergpIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, lifergpIO);
	if (isNE(lifergpIO.getStatuz(),varcom.oK)
	&& isNE(lifergpIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(lifergpIO.getParams());
		dbError8100();
	}
	if (isEQ(lifergpIO.getStatuz(),varcom.mrnf)) {
		premiumrec.jlsex.set(SPACES);
		premiumrec.jlage.set(0);
	}
	else {
		calculateAnb7500();
		premiumrec.jlage.set(wsaaAnb);
		premiumrec.jlsex.set(lifergpIO.getCltsex());
	}
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(incrsrec.chdrcoy);
	itemIO.setItemtabl(t5675);
	itemIO.setFunction(varcom.readr);
	if (isEQ(lifergpIO.getStatuz(),varcom.mrnf)) {
		itemIO.setItemitem(t5687rec.premmeth);
	}
	else {
		itemIO.setItemitem(t5687rec.jlPremMeth);
	}
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(),varcom.oK)
	&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(itemIO.getParams());
		dbError8100();
	}
	if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
		t5675rec.premsubr.set(SPACES);
        return;
	}
	t5675rec.t5675Rec.set(itemIO.getGenarea());
}
protected void systemError8000()
{
	try {
		se8000();
	}
	catch (GOTOException e){
	}
	finally{
		seExit8090();
	}
}
protected void getAnny8000()
{
    annyIO.setChdrcoy(incrsrec.chdrcoy);
    annyIO.setChdrnum(incrsrec.chdrnum);
    annyIO.setLife(incrsrec.life);
    annyIO.setCoverage(incrsrec.coverage);
    annyIO.setRider(incrsrec.rider);
    annyIO.setPlanSuffix(incrsrec.plnsfx);
    annyIO.setFunction(varcom.readr);
    SmartFileCode.execute(appVars, annyIO);
    if (isNE(annyIO.getStatuz(),varcom.oK)
    && isNE(annyIO.getStatuz(),varcom.mrnf)) {
        syserrrec.params.set(annyIO.getParams());
        syserrrec.statuz.set(annyIO.getStatuz());
        dbError8100();
    }
    if (isEQ(annyIO.getStatuz(),varcom.oK)) {
        premiumrec.freqann.set(annyIO.getFreqann());
        premiumrec.arrears.set(annyIO.getArrears());
        premiumrec.advance.set(annyIO.getAdvance());
        premiumrec.guarperd.set(annyIO.getGuarperd());
        premiumrec.intanny.set(annyIO.getIntanny());
        premiumrec.capcont.set(annyIO.getCapcont());
        premiumrec.withprop.set(annyIO.getWithprop());
        premiumrec.withoprop.set(annyIO.getWithoprop());
        premiumrec.ppind.set(annyIO.getPpind());
        premiumrec.nomlife.set(annyIO.getNomlife());
        premiumrec.dthpercn.set(annyIO.getDthpercn());
        premiumrec.dthperco.set(annyIO.getDthperco());
    }
    else {
        premiumrec.advance.set(SPACES);
        premiumrec.arrears.set(SPACES);
        premiumrec.freqann.set(SPACES);
        premiumrec.withprop.set(SPACES);
        premiumrec.withoprop.set(SPACES);
        premiumrec.ppind.set(SPACES);
        premiumrec.nomlife.set(SPACES);
        premiumrec.guarperd.set(ZERO);
        premiumrec.intanny.set(ZERO);
        premiumrec.capcont.set(ZERO);
        premiumrec.dthpercn.set(ZERO);
        premiumrec.dthperco.set(ZERO);
        }
}
protected void dbError8100()
{
    if (isEQ(syserrrec.statuz,varcom.bomb)) {
        return;
    }
    syserrrec.syserrStatuz.set(syserrrec.statuz);
    syserrrec.syserrType.set("1");
    callProgram(Syserr.class, syserrrec.syserrRec);
}


protected void se8000()
{
	if (isEQ(syserrrec.statuz,varcom.bomb)) {
		goTo(GotoLabel.seExit8090);
	}
	syserrrec.syserrStatuz.set(syserrrec.statuz);
	syserrrec.syserrType.set("2");
	callProgram(Syserr.class, syserrrec.syserrRec);
}


protected void seExit8090()
{
	incrsrec.statuz.set(varcom.bomb);
	exit090();
}




protected void dbExit8190()
{
	incrsrec.statuz.set(varcom.bomb);
	exit090();
}

protected void calculateAnb7500()
{
	chdrlnbIO.setFormat(chdrlnbrec);
	chdrlnbIO.setChdrcoy(incrsrec.chdrcoy);
	chdrlnbIO.setChdrnum(incrsrec.chdrnum);
	chdrlnbIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, chdrlnbIO);
	if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(chdrlnbIO.getParams());
		syserrrec.statuz.set(chdrlnbIO.getStatuz());
		dbError8100();
	}
	itemIO.setDataKey(SPACES);
	itemIO.setItempfx("IT");
	itemIO.setItemcoy("0");
	itemIO.setItemtabl(t1693);
	itemIO.setItemitem(incrsrec.chdrcoy);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(itemIO.getParams());
		syserrrec.statuz.set(itemIO.getStatuz());
		dbError8100();
	}
	t1693rec.t1693Rec.set(itemIO.getGenarea());
	initialize(agecalcrec.agecalcRec);
	agecalcrec.function.set("CALCP");
	agecalcrec.cnttype.set(chdrlnbIO.getCnttype());
	agecalcrec.intDate1.set(lifergpIO.getCltdob());
	agecalcrec.intDate2.set(incrsrec.effdate);
	agecalcrec.company.set(t1693rec.fsuco);
	agecalcrec.language.set(incrsrec.language);
	callProgram(Agecalc.class, agecalcrec.agecalcRec);
	if (isNE(agecalcrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(agecalcrec.statuz);
		systemError8000();
	}
	wsaaAnb.set(agecalcrec.agerating);
}
protected void exit090()
{
	exitProgram();
}

/*IVE-866 Life - Auto Increase Calculation Rework- Integration with latest PA compatible models end*/


protected void getOrigLastDetails3200()
	{
			setToCurrent3201();
			read3201();
		}

	/**
	* <pre>
	**** Before starting set the Last and Original figures to the
	**** current COVR details.
	* </pre>
	*/
protected void setToCurrent3201()
{
	if(isFoundPro && changeExist){
		incrsrec.originst01.set(wsaaOriginst01);
        incrsrec.originst02.set(wsaaOriginst02);
        incrsrec.originst03.set(wsaaOriginst03);
        incrsrec.origsum.set(wsaaOrigsum);
	}else{
    incrsrec.originst01.set(covipfRec.getInstprem());
    incrsrec.originst02.set(covipfRec.getZbinstprem());
    incrsrec.originst03.set(covipfRec.getZlinstprem());
    incrsrec.origsum.set(covipfRec.getSumins());
	}
    incrsrec.lastinst01.set(covipfRec.getInstprem());
    incrsrec.lastinst02.set(covipfRec.getZbinstprem());
    incrsrec.lastinst03.set(covipfRec.getZlinstprem());
    incrsrec.lastsum.set(covipfRec.getSumins());
    incrsrec.calcprem.set(covipfRec.getInstprem());
   }
	/**
	* <pre>
	**** Read the original INCR records to obtain the last and
	**** original premium / sum assured details.
	* </pre>
	*/
protected void read3201()
{
	if(isFoundPro && changeExist){
		return;
	}
    if (incrMap != null && incrMap.containsKey(covipfRec.getChdrnum())) {
        for (Incrpf c : incrMap.get(covipfRec.getChdrnum())) {
            if (covipfRec.getChdrcoy().equals(c.getChdrcoy()) && covipfRec.getLife().equals(c.getLife())
                    && covipfRec.getCoverage().equals(c.getCoverage()) && covipfRec.getRider().equals(c.getRider())
                    && covipfRec.getPlanSuffix() == c.getPlnsfx()) {
                /*
                 * For the first valid INCR record found, move the ORIG
                 * details to the INCC record. */
                if (isEQ(c.getRefusalFlag(), SPACES)) {
                	if(autoIncrflag) {
                		if(isEQ(wsaaT6658ArrayInner.wsaaT6658IncrFlg[wsaaT6658Ix.toInt()],"Y")) {
                		incrsrec.lastinst01.set(covipfRec.getInstprem());
                        incrsrec.currinst01.set(covipfRec.getInstprem());
                        incrsrec.lastinst02.set(covipfRec.getZbinstprem());
                        incrsrec.currinst02.set(covipfRec.getZbinstprem());
                        incrsrec.lastinst03.set(covipfRec.getZlinstprem());
                        incrsrec.currinst03.set(covipfRec.getZlinstprem());
                        incrsrec.lastsum.set(covipfRec.getSumins());
                        incrsrec.currsum.set(covipfRec.getSumins());
                        incrsrec.calcprem.set(covipfRec.getInstprem());
                		incrsrec.inputPrevPrem.set(ZERO);
                		}
                		else {
                			incrsrec.lastinst01.set(c.getNewinst());
                            incrsrec.currinst01.set(c.getNewinst());
                            incrsrec.lastinst02.set(c.getZbnewinst());
                            incrsrec.currinst02.set(c.getZbnewinst());
                            incrsrec.lastinst03.set(c.getZlnewinst());
                            incrsrec.currinst03.set(c.getZlnewinst());
                            incrsrec.lastsum.set(c.getNewsum());
                            incrsrec.currsum.set(c.getNewsum());
                            incrsrec.calcprem.set(c.getNewinst());
                    		incrsrec.inputPrevPrem.set(c.getLastInst());
        
                            if (isEQ(covipfRec.getInstprem(), incrsrec.originst01)
                                    && isEQ(covipfRec.getSumins(), incrsrec.origsum)) {
                                incrsrec.originst01.set(c.getOrigInst());
                                incrsrec.originst02.set(c.getZboriginst());
                                incrsrec.originst03.set(c.getZboriginst());
                                incrsrec.origsum.set(c.getOrigSum());
                                break;
                            }
                        }
                	}else {
                    incrsrec.lastinst01.set(c.getNewinst());
                    incrsrec.currinst01.set(c.getNewinst());
                    incrsrec.lastinst02.set(c.getZbnewinst());
                    incrsrec.currinst02.set(c.getZbnewinst());
                    incrsrec.lastinst03.set(c.getZlnewinst());
                    incrsrec.currinst03.set(c.getZlnewinst());
                    incrsrec.lastsum.set(c.getNewsum());
                    incrsrec.currsum.set(c.getNewsum());
	/* If the contract retrieved is not the required one, (after*/
	/* using BEGN), move ENDP to INCR-STATUZ to end the loop,*/
	/* then exit the section.*/
                    if (isEQ(covipfRec.getInstprem(), incrsrec.originst01)
                            && isEQ(covipfRec.getSumins(), incrsrec.origsum)) {
                        incrsrec.originst01.set(c.getOrigInst());
                        incrsrec.originst02.set(c.getZboriginst());
                        incrsrec.originst03.set(c.getZboriginst());
                        incrsrec.origsum.set(c.getOrigSum());
                        break;
                    }
                }
            }
		}
	}
}
}

protected void writeIncr3300()
	{
		if (incrExist.isTrue()) {
			return ;
		}
		/* Set up INCR.*/
        Incrpf incrIO = new Incrpf();
		incrIO.setTranno(chdrlifIO.getTranno());
		incrIO.setValidflag("1");
        incrIO.setStatcode(covipfRec.getStatcode());
        incrIO.setPstatcode(covipfRec.getPstatcode());
        incrIO.setCrtable(covipfRec.getCrtable());
        incrIO.setChdrcoy(incrsrec.chdrcoy.toString());
        incrIO.setChdrnum(incrsrec.chdrnum.toString());
        incrIO.setLife(incrsrec.life.toString());
        incrIO.setCoverage(incrsrec.coverage.toString());
        incrIO.setRider(incrsrec.rider.toString());
        incrIO.setPlnsfx(incrsrec.plnsfx.toInt());
        incrIO.setCrrcd(incrsrec.effdate.toInt());
        incrIO.setCrtable(incrsrec.crtable.toString());
        incrIO.setAnniversaryMethod(incrsrec.annvmeth.toString());
		/* MOVE WSAA-T5687-BCMTHD(WSAA-T5687-IX)                        */
		/*                             TO INCR-BASIC-COMM-METH.         */
		/* MOVE WSAA-T5687-BASCPY(WSAA-T5687-IX)                        */
		/*                             TO INCR-BASCPY.                  */
		/* MOVE WSAA-T5687-RNWCPY(WSAA-T5687-IX)                        */
		/*                             TO INCR-RNWCPY                   */
		/* MOVE WSAA-T5687-SRVCPY(WSAA-T5687-IX)                        */
		/*                             TO INCR-SRVCPY.                  */
        incrIO.setBasicCommMeth(wsaaBasicCommMeth.toString());
        incrIO.setBascpy(wsaaBascpy.toString());
        incrIO.setRnwcpy(wsaaRnwcpy.toString());
        incrIO.setSrvcpy(wsaaSrvcpy.toString());
        incrIO.setOrigSum(incrsrec.origsum.getbigdata());
        incrIO.setLastSum(incrsrec.lastsum.getbigdata());
        incrIO.setNewsum(incrsrec.newsum.getbigdata());
        incrIO.setOrigInst(incrsrec.originst01.getbigdata());
		if (rertFound) {
			incrIO.setLastInst(wsaaRertInstprem.getbigdata());
			incrIO.setZblastinst(wsaaRertBinstprem.getbigdata());
			incrIO.setZllastinst(wsaaRertLinstprem.getbigdata());
		} else {
			incrIO.setLastInst(incrsrec.lastinst01.getbigdata());
			incrIO.setZblastinst(incrsrec.lastinst02.getbigdata());
			incrIO.setZllastinst(incrsrec.lastinst03.getbigdata());
		}
        incrIO.setNewinst(incrsrec.newinst01.getbigdata());
        incrIO.setZboriginst(incrsrec.originst02.getbigdata());
        incrIO.setZbnewinst(incrsrec.newinst02.getbigdata());
        incrIO.setZloriginst(incrsrec.originst03.getbigdata());
        incrIO.setZlnewinst(incrsrec.newinst03.getbigdata());
        incrIO.setPctinc(incrsrec.pctinc.toInt());
        incrIO.setRefusalFlag(SPACES.toString());
        if(stampDutyflag) {
        	String vpmModule = "INCRSUM";
        	if((vpmModule).equals((wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()] != null && isNE(wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()], SPACES)) ? wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()].toString().trim() : false)) {
        		incrIO.setZstpduty01(incrsrec.zstpduty01.getbigdata());
        	}
        	else {
	        	if(incrsrec.zstpduty03.getbigdata().compareTo(BigDecimal.ZERO) != 0)
	        		incrIO.setZstpduty01(incrsrec.zstpduty03.getbigdata());
	        	else
	        		incrIO.setZstpduty01(incrsrec.zstpduty02.getbigdata());
        	}
        }
        rounding6000(incrIO);
        incrIO.setTransactionDate(wsaaTransDate.toInt());
        incrIO.setTransactionTime(wsaaTransTime.toInt());
        incrIO.setUser(subString(bsscIO.getDatimeInit(), 21, 6).toInt());
		varcom.vrcmTranid.set(SPACES);
        incrIO.setTermid(varcom.vrcmTermid.toString());
        if(insertIncrList == null){
            insertIncrList = new ArrayList<>();
		}
        insertIncrList.add(incrIO);
        ct04Value++;
		/* Update flag to indicate an INCR has been written for this*/
		/* component so that we write a PTRN for the contract later on*/
		incrWritten.setTrue();
	}

protected void existingIncrease3400()
	{
		incrNotExist.setTrue();
		incrrgpIO.setChdrcoy(incrsrec.chdrcoy);
		incrrgpIO.setChdrnum(incrsrec.chdrnum);
		incrrgpIO.setLife(incrsrec.life);
		incrrgpIO.setCoverage(incrsrec.coverage);
		incrrgpIO.setRider(incrsrec.rider);
		incrrgpIO.setPlanSuffix(incrsrec.plnsfx);
		incrrgpIO.setFormat(formatsInner.incrrgprec);
		incrrgpIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, incrrgpIO);
		if (isNE(incrrgpIO.getStatuz(),varcom.oK)
		&& isNE(incrrgpIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(incrrgpIO.getParams());
			syserrrec.statuz.set(incrrgpIO.getStatuz());
			fatalError600();
		}
		if (isEQ(incrrgpIO.getStatuz(),varcom.oK)
		&& isEQ(incrrgpIO.getCrrcd(),incrsrec.effdate)) {
			incrExist.setTrue();
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
        if(insertPtrnList !=null && !insertPtrnList.isEmpty()){
            ptrnpfDAO.insertPtrnPF(insertPtrnList);
            insertPtrnList.clear();
        }
        if(updateCovrincDateList !=null && !updateCovrincDateList.isEmpty()){
            covrpfDAO.updateCpiDate(updateCovrincDateList);
            updateCovrincDateList.clear();
        }
        if(updateCovrincList !=null && !updateCovrincList.isEmpty()){
            covrpfDAO.updateCovrRecord(updateCovrincList, 0);
            updateCovrincList.clear();
        }
        if(insertCovrincList !=null && !insertCovrincList.isEmpty()){
            covrpfDAO.insertCovrRecordForL2POLRNWL(insertCovrincList);
            insertCovrincList.clear();
        }
        if(updatePayrList !=null && !updatePayrList.isEmpty()){
            payrpfDAO.updatePayrTranno(updatePayrList);
            updatePayrList.clear();
        }
        if(!reinstated && insertIncrList !=null && !insertIncrList.isEmpty()){
            incrpfDAO.insertIncrList(insertIncrList);
            insertIncrList.clear();
        }
        if(insertAinrList !=null && !insertAinrList.isEmpty()){
            ainrpfDAO.insertAinrpfList(insertAinrList);
            insertAinrList.clear();
        }
        if(updateChdrlifMapTranno !=null && !updateChdrlifMapTranno.isEmpty()){
            chdrpfDAO.updateChdrTrannoByUniqueNo(new ArrayList<>(updateChdrlifMapTranno.values()));
            updateChdrlifMapTranno.clear();
        }
        if(rertpfList !=null && !rertpfList.isEmpty()){
            rertpfDAO.updateRertList(rertpfList);
            rertpfList.clear();
        }
        
        commitControlTotals();
		/*EXIT*/
	}

    private void commitControlTotals() {
        contotrec.totno.set(1);
        contotrec.totval.set(ct01Value);
        callContot001();
        ct01Value = 0;
        contotrec.totno.set(2);
        contotrec.totval.set(ct02Value);
        callContot001();
        ct02Value = 0;
        contotrec.totno.set(3);
        contotrec.totval.set(ct03Value);
        callContot001();
        ct03Value = 0;
        contotrec.totno.set(4);
        contotrec.totval.set(ct04Value);
        callContot001();
        ct04Value = 0;
        contotrec.totno.set(5);
        contotrec.totval.set(ct05Value);
        callContot001();
        ct05Value = 0;
        contotrec.totno.set(6);
        contotrec.totval.set(ct06Value);
        callContot001();
        ct06Value = 0;
        contotrec.totno.set(7);
        contotrec.totval.set(ct07Value);
        callContot001();
        ct07Value = 0;
        contotrec.totno.set(8);
        contotrec.totval.set(ct08Value);
        callContot001();
        ct08Value = 0;
        contotrec.totno.set(9);
        contotrec.totval.set(ct09Value);
        callContot001();
        ct09Value = 0;
        contotrec.totno.set(10);
        contotrec.totval.set(ct10Value);
        callContot001();
        ct10Value = 0;
        contotrec.totno.set(11);
        contotrec.totval.set(ct11Value);
        callContot001();
        ct11Value = 0;
    }
protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void writeAinr3700()
	{
        Ainrpf ainrIO = new Ainrpf();
        ainrIO.setChdrcoy(covipfRec.getChdrcoy());
        ainrIO.setPlanSuffix(covipfRec.getPlanSuffix());
        ainrIO.setChdrnum(covipfRec.getChdrnum());
        ainrIO.setLife(covipfRec.getLife());
        ainrIO.setCoverage(covipfRec.getCoverage());
        ainrIO.setRider(covipfRec.getRider());
        ainrIO.setCrtable(covipfRec.getCrtable());
		/*  MOVE INCC-CURRSUM           TO AINR-OLDSUM.*/
        ainrIO.setRd01Oldsum(incrsrec.currsum.getbigdata());
        ainrIO.setNewsumi(incrsrec.newsum.getbigdata());
        ainrIO.setOldinst(incrsrec.currinst01.getbigdata());
        ainrIO.setNewinst(incrsrec.newinst01.getbigdata());
        ainrIO.setRiskCessDate(covipfRec.getCpiDate());
        ainrIO.setCurrency(covipfRec.getPremCurrency());
		/* MOVE AINR-RD01-OLDSUM        TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 7000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO AINR-RD01-OLDSUM.            */
		if (isNE(ainrIO.getRd01Oldsum(), 0)) {
			zrdecplrec.amountIn.set(ainrIO.getRd01Oldsum());
			callRounding7000();
            ainrIO.setRd01Oldsum(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE AINR-NEWSUMI            TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 7000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO AINR-NEWSUMI.                */
		if (isNE(ainrIO.getNewsumi(), 0)) {
			zrdecplrec.amountIn.set(ainrIO.getNewsumi());
			callRounding7000();
            ainrIO.setNewsumi(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE AINR-OLDINST            TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 7000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO AINR-OLDINST.                */
		if (isNE(ainrIO.getOldinst(), 0)) {
			zrdecplrec.amountIn.set(ainrIO.getOldinst());
			callRounding7000();
            ainrIO.setOldinst(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE AINR-NEWINST            TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 7000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO AINR-NEWINST.                */
		if (isNE(ainrIO.getNewinst(), 0)) {
			zrdecplrec.amountIn.set(ainrIO.getNewinst());
			callRounding7000();
            ainrIO.setNewinst(zrdecplrec.amountOut.getbigdata());
		}
		/* Write AINR record, with a type of pending. This is so*/
		/* that the single thread report program which follows B5134*/
		/* can distinguish between the AINR records written by B5134*/
		/* and those written by B5137 (Auto Increases Actual).*/
        ainrIO.setAintype(wsaaPend.toString());
        if(insertAinrList == null){
            insertAinrList = new ArrayList<>();
		}
        insertAinrList.add(ainrIO);
        ct07Value++;
	}

protected void updateCovr3800()
	{
        Covrpf covrincIO = null;
        if (covrincMap != null && covrincMap.containsKey(covipfRec.getChdrnum())) {
            for (Covrpf c : covrincMap.get(covipfRec.getChdrnum())) {
                if (covipfRec.getChdrcoy().equals(c.getChdrcoy()) && covipfRec.getLife().equals(c.getLife())
                        && covipfRec.getCoverage().equals(c.getCoverage()) && covipfRec.getRider().equals(c.getRider())
                        && covipfRec.getPlanSuffix() == c.getPlanSuffix()) {
                    covrincIO = c;
                    break;
	}
            }
        }
        if (covrincIO == null) {
            syserrrec.params.set(covipfRec.getChdrnum());
			fatalError600();
		} //IJTI-462 START 
        else {
			/* Update the COVR record with a valid flag of 2 and set*/
			/* 'COVR-CURRTO' date to the effective date of the Increase.*/
	        Covrpf updateCovrincIO = new Covrpf(covrincIO);
	        updateCovrincIO.setValidflag("2");
	        updateCovrincIO.setCurrto(covrincIO.getCpiDate());
	        if (updateCovrincList == null) {
	            updateCovrincList = new ArrayList<>();
			}
	        updateCovrincList.add(updateCovrincIO);
			//ilife-5239 starts
	        if (newContract.isTrue() && chdrlifIO != null) {
	        	if(isFoundPro){
	        		 	compute(wsaaSub, 0).set(add(chdrlifIO.getTranno(),wsaaSub));
	        			chdrlifIO.setTranno(wsaaSub.toInt());
	        	}
	        	else{
	            chdrlifIO.setTranno(chdrlifIO.getTranno() + 1);
	        	}
			}
			
			//ilife-5239 ends
			covrincIO.setValidflag("1");
			if (chdrlifIO != null) {
				covrincIO.setTranno(chdrlifIO.getTranno());//ILIFE-8472
			}
			covrincIO.setCurrfrom(covrincIO.getCpiDate());
			wsaaOldCpiDate.set(covrincIO.getCpiDate());
	        covrincIO.setCurrto(varcom.vrcmMaxDate.toInt());
	        covrincIO.setTransactionDate(wsaaTransDate.toInt());
	        covrincIO.setTransactionTime(wsaaTransTime.toInt());
	        covrincIO.setUser(subString(bsscIO.getDatimeInit(), 21, 6).toInt());
			varcom.vrcmTranid.set(SPACES);
	        covrincIO.setTermid(varcom.vrcmTermid.toString());
			covrincIO.setIndexationInd("P");
			covrincIO.setRiskprem(premiumrec.riskPrem.getbigdata()); //ILIFE-7845 
		
	        if (insertCovrincList == null) {
	            insertCovrincList = new ArrayList<>();
			}
	        insertCovrincList.add(covrincIO);
	        ct06Value++;
		} //IJTI-462 END
	}

protected void contractChange3900()
	{
		/*CHANGE*/
		/* FOR THE OLD CONTRACT...*/
		/* Check to see if an INCR was written.*/
		/* If an INCR does exist for the old contract*/
		/*     write a PTRN and check the "Optional" indicator*/
		/*     if the optional indicator on T6658 not = spaces*/
		/*         generate a letter request for later*/
		if (incrWritten.isTrue() || (prmhldtrad && reinstated)) {
			if(isFoundPro){
				while(isLT(wsaaLastCpiDate,wsaaProCpiDate)){
					writePtrn3980();
					datcon2rec.datcon2Rec.set(SPACES);
					datcon2rec.intDate1.set(wsaaLastCpiDate); 
					datcon2rec.frequency.set("01");
					datcon2rec.freqFactor.set(wsaaFreq);
					callProgram(Datcon2.class, datcon2rec.datcon2Rec);
					if (isNE(datcon2rec.statuz,varcom.oK)) {
						syserrrec.statuz.set(datcon2rec.statuz);
						syserrrec.params.set(datcon2rec.datcon2Rec);
						fatalError600();
					}
					wsaaLastCpiDate.set(datcon2rec.intDate2.toInt());
				}
			}
			else{
			writePtrn3980();
			}
			if (isNE(wsaaT6658ArrayInner.wsaaT6658Optind[wsaaT6658Ix.toInt()], SPACES)
			|| isNE(wsaaT6658ArrayInner.wsaaT6658Manopt[wsaaT6658Ix.toInt()], SPACES)) {
				letterRequest3930();
			}
		}
		/* FOR THE NEW CONTRACT...*/
		/* Set various flags and save contract details for later.*/
		processContract.setTrue();
		incrNotWritten.setTrue();
		wsaaChdrChdrnum.set(covipfRec.getChdrnum());
		wsaaChdrChdrcoy.set(covipfRec.getChdrcoy());
		/*EXIT*/
	}

protected void letterRequest3930()
	{
		if(searchT6634Array3931()){
		    ct02Value++;
		}
			/* Expected exception for control flow purposes. */
	}

protected boolean searchT6634Array3931()
	{
		/* Search T6634 the Auto Letters table to get the letter type.*/
		/* Build key to T6634 from contract type & transaction code.*/
		/* MOVE CHDRLIF-CNTTYPE        TO WSAA-T6634-CNTTYPE2.          */
		/* MOVE BPRD-AUTH-CODE         TO WSAA-T6634-TRCODE2.           */
		wsaaTr384Cnttype2.set(chdrlifIO.getCnttype());
		wsaaTr384Trcode2.set(bprdIO.getAuthCode());
		/* SEARCH ALL WSAA-T6634-REC                                    */
		ArraySearch as1 = ArraySearch.getInstance(wsaaTr384Rec);
		as1.setIndices(wsaaTr384Ix);
		as1.addSearchKey(wsaaTr384Key, wsaaTr384Key2, true);
		if (as1.binarySearch()) {
			/*CONTINUE_STMT*/
		}
		else {
			/*     MOVE '***'              TO WSAA-T6634-CNTTYPE2           */
			/*     SEARCH ALL WSAA-T6634-REC                                */
			wsaaTr384Cnttype2.set("***");
			ArraySearch as3 = ArraySearch.getInstance(wsaaTr384Rec);
			as3.setIndices(wsaaTr384Ix);
			as3.addSearchKey(wsaaTr384Key, wsaaTr384Key2, true);
			if (as3.binarySearch()) {
				/*CONTINUE_STMT*/
			}
			else {
				return false;
				/*****         WHEN WSAA-T6634-KEY(WSAA-T6634-IX)                   */
				/*****                              = WSAA-T6634-KEY2               */
			}
			/*****         WHEN WSAA-T6634-KEY(WSAA-T6634-IX)                   */
			/*****                             = WSAA-T6634-KEY2                */
		}
		/* Now call LETRQST to generate the LETCPF record.*/
		initialize(letrqstrec.params);
		letrqstrec.requestCompany.set(bsprIO.getCompany());
		/* MOVE WSAA-T6634-LETTER-TYPE(WSAA-T6634-IX)                   */
		letrqstrec.letterType.set(wsaaTr384LetterType[wsaaTr384Ix.toInt()]);
		letrqstrec.letterRequestDate.set(wsaaTransDate);
		letrqstrec.clntcoy.set(chdrlifIO.getCowncoy());
		letrqstrec.clntnum.set(chdrlifIO.getCownnum());
		letrqstrec.rdocpfx.set(chdrlifIO.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrlifIO.getChdrnum());
		letrqstrec.chdrnum.set(chdrlifIO.getChdrnum());
		letrqstrec.tranno.set(chdrlifIO.getTranno());
		letrqstrec.branch.set(chdrlifIO.getCntbranch());
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz,varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
		return true;
	}

protected void rewriteChdrlifPayr3950(Payrpf payrIO)
	{
        /* Rewrite the CHDRLIF and PAYR records. */
        if (updateChdrlifMapTranno == null) {
            updateChdrlifMapTranno = new LinkedHashMap<>();
	}
        if (chdrlifIO != null) {
            updateChdrlifMapTranno.put(chdrlifIO.getUniqueNumber(), chdrlifIO);
	}

		/* Rewrite the CHDRLIF and PAYR records.*/
        if (updatePayrList == null) {
            updatePayrList = new ArrayList<>();
		}
		/*  Update the transaction number on the PAYR file*/
		/*  using the transaction number from CHDRLIF.*/
        if (chdrlifIO != null) { //IJTI-462
        	payrIO.setTranno(chdrlifIO.getTranno()); //IJTI-462
        } //IJTI-462
        updatePayrList.add(payrIO);
        ct03Value++;
		}

protected void readChdrlif3960()
	{
        if(chdrlifRgpMap != null && chdrlifRgpMap.containsKey(covipfRec.getChdrnum())){
            for(Chdrpf c:chdrlifRgpMap.get(covipfRec.getChdrnum())){
                if(covipfRec.getChdrcoy().equals(c.getChdrcoy().toString())){
                    chdrlifIO = c;
                    break;
	}
            }
	/**
	* <pre>
	**** Read and hold contract header using CHDRLIF.
	* </pre>
	*/
        }
        if(chdrlifIO == null){
            syserrrec.params.set(covipfRec.getChdrnum());
			fatalError600();
		}
		/* Only increment CHDRLIF-TRANNO once per contract*/
		//ILIFE-4323 start by dpuhawan
		/*
		if (newContract.isTrue()) {
			setPrecision(chdrlifIO.getTranno(), 0);
			chdrlifIO.setTranno(add(chdrlifIO.getTranno(),1));
		}
		*/
		//ILIFE-4323 end
	}

protected void writePtrn3980()
	{

		/* Write PTRN record.*/
        Ptrnpf ptrnIO = new Ptrnpf();
		/* INITIALIZE PTRNREC-KEY-DATA                                  */
		/*            PTRNREC-NON-KEY-DATA.                             */
        ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy().toString());
		ptrnIO.setChdrpfx(chdrlifIO.getChdrpfx());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		 if(isFoundPro){
        	 ptrnIO.setTranno(tranno.toInt());
        	 compute(tranno, 0).set(add(tranno,1));
        }
        else{
		ptrnIO.setTranno(chdrlifIO.getTranno());
        }
        ptrnIO.setTrdt(bsscIO.getEffectiveDate().toInt());
        ptrnIO.setTrtm(wsaaTransTime.toInt());
        if(isFoundPro){
         ptrnIO.setPtrneff(wsaaLastCpiDate.toInt());
        }
        else{
        ptrnIO.setPtrneff(wsaaOldCpiDate.toInt());
        }
        ptrnIO.setDatesub(bsscIO.getEffectiveDate().toInt());
        ptrnIO.setUserT(subString(bsscIO.getDatimeInit(), 21, 6).toInt());
		varcom.vrcmTranid.set(SPACES);
        ptrnIO.setTermid(SPACES.toString());
        ptrnIO.setBatcpfx(batcdorrec.prefix.toString());
        ptrnIO.setBatccoy(batcdorrec.company.toString());
        ptrnIO.setBatcbrn(batcdorrec.branch.toString());
        ptrnIO.setBatcactyr(batcdorrec.actyear.toInt());
        ptrnIO.setBatcactmn(batcdorrec.actmonth.toInt());
        ptrnIO.setBatctrcde(batcdorrec.trcde.toString());
        ptrnIO.setBatcbatch(batcdorrec.batch.toString());
        if(insertPtrnList == null){
            insertPtrnList = new ArrayList<>();
		}
        insertPtrnList.add(ptrnIO);
        ct05Value++;
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/* Close any open files.*/
		covipf.close();
		/* Delete the override on the temporary file*/
		wsaaQcmdexc.set("DLTOVR FILE(COVIPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void softlock5000()
	{

		sftlockrec.statuz.set(SPACES);
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(bsprIO.getCompany());
		sftlockrec.enttyp.set("CH");
        sftlockrec.entity.set(covipfRec.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(subString(bsscIO.getDatimeInit(), 21, 6));
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void searchT56875100()
	{

		/* Search table T5687 to get the anniversary method, using*/
		/* CRTABLE as the key.*/
		wsaaT5687Ix.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaT5687Ix, wsaaT5687ArrayInner.wsaaT5687Rec.length); wsaaT5687Ix.add(1)){
                if (isEQ(wsaaT5687ArrayInner.wsaaT5687Key[wsaaT5687Ix.toInt()], covipfRec.getCrtable())
                && isLTE(wsaaT5687ArrayInner.wsaaT5687Currfrom[wsaaT5687Ix.toInt()], covipfRec.getCpiDate())) {
					break searchlabel1;
				}
			}
			syserrrec.statuz.set(h053);
			itdmIO.setParams(SPACES);
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setItemcoy(bsprIO.getCompany());
			itdmIO.setItemtabl(t5687);
            itdmIO.setItemitem(covipfRec.getCrtable());
            itdmIO.setItmfrm(covipfRec.getCpiDate());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(wsaaT5687ArrayInner.wsaaT5687Annmthd[wsaaT5687Ix.toInt()], SPACES)) {
			syserrrec.statuz.set(h053);
			StringUtil stringVariable1 = new StringUtil();
            stringVariable1.addExpression(covipfRec.getChdrcoy());
			stringVariable1.addExpression("|");
            stringVariable1.addExpression(covipfRec.getChdrnum());
			stringVariable1.addExpression("|");
            stringVariable1.addExpression(covipfRec.getLife());
			stringVariable1.addExpression("|");
            stringVariable1.addExpression(covipfRec.getCoverage());
			stringVariable1.addExpression("|");
            stringVariable1.addExpression(covipfRec.getRider());
			stringVariable1.addExpression("|");
            stringVariable1.addExpression(covipfRec.getCrtable());
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();
		}
		wsaaBasicCommMeth.set(wsaaT5687ArrayInner.wsaaT5687Bcmthd[wsaaT5687Ix.toInt()]);
		wsaaBascpy.set(wsaaT5687ArrayInner.wsaaT5687Bascpy[wsaaT5687Ix.toInt()]);
		wsaaRnwcpy.set(wsaaT5687ArrayInner.wsaaT5687Rnwcpy[wsaaT5687Ix.toInt()]);
		wsaaSrvcpy.set(wsaaT5687ArrayInner.wsaaT5687Srvcpy[wsaaT5687Ix.toInt()]);
	}

protected void searchT66585200()
	{

		/* Search table T6658 with the anniversary method, to get the*/
		/* Increase Subroutine.*/
		wsaaT6658Ix.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaT6658Ix, wsaaT6658ArrayInner.wsaaT6658Rec.length); wsaaT6658Ix.add(1)){
				if (isEQ(wsaaT6658ArrayInner.wsaaT6658Annmthd[wsaaT6658Ix.toInt()], wsaaT5687ArrayInner.wsaaT5687Annmthd[wsaaT5687Ix.toInt()])
                && isLT(wsaaT6658ArrayInner.wsaaT6658Currfrom[wsaaT6658Ix.toInt()], covipfRec.getCpiDate())) {
					break searchlabel1;
				}
			}
			syserrrec.statuz.set(h036);
			itdmIO.setParams(SPACES);
			itdmIO.setFunction(varcom.begn);
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setItemcoy(bsprIO.getCompany());
			itdmIO.setItemtabl(t6658);
			itdmIO.setItemitem(wsaaT5687ArrayInner.wsaaT5687Annmthd[wsaaT5687Ix.toInt()]);
            itdmIO.setItmfrm(covipfRec.getCpiDate());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()], SPACES)) {
			syserrrec.statuz.set(e512);
			syserrrec.params.set(wsaaT5687ArrayInner.wsaaT5687Annmthd[wsaaT5687Ix.toInt()]);
			fatalError600();
		}
	}

protected void releaseSoftlock5500()
	{

		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(bsprIO.getCompany());
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(subString(bsscIO.getDatimeInit(), 21, 6));
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void rounding6000(Incrpf incrIO)
	{

		/* MOVE INCR-ORIG-SUM           TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 7000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO INCR-ORIG-SUM.               */
		if (isNE(incrIO.getOrigSum(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getOrigSum());
			callRounding7000();
            incrIO.setOrigSum(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-LAST-SUM           TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 7000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO INCR-LAST-SUM.               */
		if (isNE(incrIO.getLastSum(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getLastSum());
			callRounding7000();
            incrIO.setLastSum(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-NEWSUM             TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 7000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO INCR-NEWSUM.                 */
		if (isNE(incrIO.getNewsum(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getNewsum());
			callRounding7000();
            incrIO.setNewsum(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-ORIG-INST          TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 7000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO INCR-ORIG-INST.              */
		if (isNE(incrIO.getOrigInst(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getOrigInst());
			callRounding7000();
            incrIO.setOrigInst(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-LAST-INST          TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 7000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO INCR-LAST-INST.              */
		if (isNE(incrIO.getLastInst(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getLastInst());
			callRounding7000();
            incrIO.setLastInst(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-NEWINST            TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 7000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO INCR-NEWINST.                */
		if (isNE(incrIO.getNewinst(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getNewinst());
			callRounding7000();
            incrIO.setNewinst(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-ZBORIGINST         TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 7000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO INCR-ZBORIGINST.             */
		if (isNE(incrIO.getZboriginst(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getZboriginst());
			callRounding7000();
            incrIO.setZboriginst(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-ZBLASTINST         TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 7000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO INCR-ZBLASTINST.             */
		if (isNE(incrIO.getZblastinst(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getZblastinst());
			callRounding7000();
            incrIO.setZblastinst(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-ZBNEWINST          TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 7000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO INCR-ZBNEWINST.              */
		if (isNE(incrIO.getZbnewinst(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getZbnewinst());
			callRounding7000();
            incrIO.setZbnewinst(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-ZLORIGINST         TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 7000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO INCR-ZLORIGINST.             */
		if (isNE(incrIO.getZloriginst(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getZloriginst());
			callRounding7000();
            incrIO.setZloriginst(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-ZLLASTINST         TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 7000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO INCR-ZLLASTINST.             */
		if (isNE(incrIO.getZllastinst(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getZllastinst());
			callRounding7000();
            incrIO.setZllastinst(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-ZLNEWINST          TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 7000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO INCR-ZLNEWINST.              */
		if (isNE(incrIO.getZlnewinst(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getZlnewinst());
			callRounding7000();
            incrIO.setZlnewinst(zrdecplrec.amountOut.getbigdata());
		}
		
		if(stampDutyflag) {
			if(isNE(incrIO.getZstpduty01(), 0)) {
				zrdecplrec.amountIn.set(incrIO.getZstpduty01());
				callRounding7000();
	            incrIO.setZstpduty01(zrdecplrec.amountOut.getbigdata());
			}
		}
	}

protected void callRounding7000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(covipfRec.getPremCurrency());
		zrdecplrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void a100ReadTableTr695()
	{
        Covrpf covrincIO = null;
        if (covrincMap != null && covrincMap.containsKey(covipfRec.getChdrnum())) {
            for (Covrpf c : covrincMap.get(covipfRec.getChdrnum())) {
                if (covipfRec.getChdrcoy().equals(c.getChdrcoy()) && covipfRec.getLife().equals(c.getLife())
                        && covipfRec.getJlife().equals(c.getJlife()) && covipfRec.getCoverage().equals(c.getCoverage())
                        && covipfRec.getRider().equals(c.getRider()) && covipfRec.getPlanSuffix() == c.getPlanSuffix()) {
                    covrincIO = c;
                    break;
		}

            }
		}
        if (covrincIO != null) { //IJTI-462
        	wsaaTr695Coverage.set(covrincIO.getCrtable()); //IJTI-462
        } //IJTI-462
        wsaaTr695Rider.set(covipfRec.getCrtable());
        Itempf tr695Item = null;
        if (tr695Map != null && tr695Map.containsKey(wsaaTr695Key.toString())) {
            for (Itempf item : tr384Map.get(wsaaTr695Key.toString())) {
                if (item.getItmfrm().compareTo(new BigDecimal(covipfRec.getCpiDate())) <= 0) {
                    tr695Item = item;
                    break;
                }
		}
		}
        if (tr695Item == null) {
			wsaaTr695Rider.set("****");
            if (tr695Map != null && tr695Map.containsKey(wsaaTr695Key.toString())) {
                for (Itempf item : tr384Map.get(wsaaTr695Key.toString())) {
                    if (item.getItmfrm().compareTo(new BigDecimal(covipfRec.getCpiDate())) <= 0) {
                        tr695Item = item;
                        break;
                    }
			//performance improvement --  atiwari23
			}
            }
			}
        if (tr695Item == null) {
            return;
		}
        tr695rec.tr695Rec.set(StringUtil.rawToString(tr695Item.getGenarea()));
		wsaaBasicCommMeth.set(tr695rec.basicCommMeth);
		wsaaBascpy.set(tr695rec.bascpy);
		wsaaSrvcpy.set(tr695rec.srvcpy);
		wsaaRnwcpy.set(tr695rec.rnwcpy);
	}

protected void setIncrsrec() {//ILIFE-8509
	Incrpf incr = null;
	if (incrMap != null && incrMap.containsKey(covipfRec.getChdrnum())) {
        for (Incrpf incrpf : incrMap.get(covipfRec.getChdrnum())) {
            if (covipfRec.getChdrcoy().equals(incrpf.getChdrcoy()) && covipfRec.getLife().equals(incrpf.getLife())
                    && covipfRec.getCoverage().equals(incrpf.getCoverage()) && covipfRec.getRider().equals(incrpf.getRider())) {
                incr = incrpf;
                break;
            }
        }
    }
	if(incr!=null) {
		incrsrec.currinst01.set(incr.getLastInst());
	    incrsrec.currinst02.set(incr.getZblastinst());
	    incrsrec.currinst03.set(incr.getZllastinst());
	    incrsrec.currsum.set(incr.getLastSum());
	    incrsrec.originst01.set(incr.getOrigInst());
	    incrsrec.originst02.set(incr.getZboriginst());
	    incrsrec.originst03.set(incr.getZloriginst());
	    incrsrec.origsum.set(incr.getOrigSum());
	    incrsrec.lastinst01.set(incr.getLastInst());
	    incrsrec.lastinst02.set(incr.getZblastinst());
	    incrsrec.lastinst03.set(incr.getZllastinst());
	    incrsrec.lastsum.set(incr.getLastSum());
	}
}
/*
 * Class transformed  from Data Structure WSAA-T5687-ARRAY--INNER
 */
private static final class WsaaT5687ArrayInner {

	private FixedLengthStringData wsaaT5687Array = new FixedLengthStringData(17000);
	private FixedLengthStringData[] wsaaT5687Rec = FLSArrayPartOfStructure(500, 34, wsaaT5687Array, 0);
	private PackedDecimalData[] wsaaT5687Currfrom = PDArrayPartOfArrayStructure(8, 0, wsaaT5687Rec, 0);
	private FixedLengthStringData[] wsaaT5687Key = FLSDArrayPartOfArrayStructure(4, wsaaT5687Rec, 10, HIVALUES);
	private FixedLengthStringData[] wsaaT5687Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5687Key, 0);
	private FixedLengthStringData[] wsaaT5687Data = FLSDArrayPartOfArrayStructure(20, wsaaT5687Rec, 14);
	private FixedLengthStringData[] wsaaT5687Annmthd = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 0);
	private FixedLengthStringData[] wsaaT5687Bcmthd = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 4);
	private FixedLengthStringData[] wsaaT5687Bascpy = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 8);
	private FixedLengthStringData[] wsaaT5687Rnwcpy = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 12);
	private FixedLengthStringData[] wsaaT5687Srvcpy = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 16);
}
/*
 * Class transformed  from Data Structure WSAA-T6658-ARRAY--INNER
 */
private static final class WsaaT6658ArrayInner {

	private FixedLengthStringData wsaaT6658Array = new FixedLengthStringData(1440);
	private FixedLengthStringData[] wsaaT6658Rec = FLSArrayPartOfStructure(30, 48, wsaaT6658Array, 0);
	private PackedDecimalData[] wsaaT6658Currfrom = PDArrayPartOfArrayStructure(8, 0, wsaaT6658Rec, 0);
	private FixedLengthStringData[] wsaaT6658Key = FLSDArrayPartOfArrayStructure(4, wsaaT6658Rec, 10, HIVALUES);
	private FixedLengthStringData[] wsaaT6658Annmthd = FLSDArrayPartOfArrayStructure(4, wsaaT6658Key, 0);
	private FixedLengthStringData[] wsaaT6658Data = FLSDArrayPartOfArrayStructure(34, wsaaT6658Rec, 14);
	private FixedLengthStringData[] wsaaT6658Billfreq = FLSDArrayPartOfArrayStructure(2, wsaaT6658Data, 0);
	private ZonedDecimalData[] wsaaT6658Fixdtrm = ZDArrayPartOfArrayStructure(3, 0, wsaaT6658Data, 2);
	private FixedLengthStringData[] wsaaT6658Manopt = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 5);
	private PackedDecimalData[] wsaaT6658Maxpcnt = PDArrayPartOfArrayStructure(5, 2, wsaaT6658Data, 6);
	private PackedDecimalData[] wsaaT6658Minpcnt = PDArrayPartOfArrayStructure(5, 2, wsaaT6658Data, 9);
	private FixedLengthStringData[] wsaaT6658Optind = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 12);
	private FixedLengthStringData[] wsaaT6658Subprog = FLSDArrayPartOfArrayStructure(10, wsaaT6658Data, 13);
	private FixedLengthStringData[] wsaaT6658Premsubr = FLSDArrayPartOfArrayStructure(8, wsaaT6658Data, 23);
	//ILIFE-6569
	private FixedLengthStringData[] wsaaT6658SimpInd = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 31);
	private FixedLengthStringData[] wsaaT6658IncrFlg = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 32);
	private FixedLengthStringData[] wsaaT6658CompoundInd = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 33);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData incrrgprec = new FixedLengthStringData(10).init("INCRRGPREC");
}
}
