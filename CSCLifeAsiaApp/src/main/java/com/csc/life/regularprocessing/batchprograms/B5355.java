/*
 * File: B5355.java
 * Date: 29 August 2009 21:09:19
 * Author: Quipoz Limited
 *
 * Class transformed from B5355.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.MandpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Mandpf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Vflagcpy;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.Th5agrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.cashdividends.dataaccess.HpuaTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.OverpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Overpf;
import com.csc.life.contractservicing.recordstructures.Nfloanrec;
import com.csc.life.enquiries.procedures.Crtundwrt;
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.HltxTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.NlgtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Nlgtpf;
import com.csc.life.newbusiness.procedures.Nlgcalc;
import com.csc.life.newbusiness.recordstructures.Nlgcalcrec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.regularprocessing.dataaccess.LinsrnlTableDAM;
import com.csc.life.regularprocessing.dataaccess.dao.PayzpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Payzpf;
import com.csc.life.regularprocessing.procedures.Crtloan;
import com.csc.life.regularprocessing.procedures.Overbill;
import com.csc.life.regularprocessing.recordstructures.Crtloanrec;
import com.csc.life.regularprocessing.recordstructures.Ovrbillrec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.regularprocessing.tablestructures.T5399rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.procedures.Calcfee;
import com.csc.life.terminationclaims.tablestructures.T6597rec;
import com.csc.life.terminationclaims.tablestructures.Th584rec;
import com.csc.life.terminationclaims.tablestructures.Tr5b2rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.ArraySearch;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* B5355 - REGULAR PROCESSING - OVERDUE.
*
* Overview.
* ========
* B5355 replaces B6244. It has been written to run multi-thread
* and improve performance of the overdue process. It runs after
* B5354 which 'splits' the PAYRPF according to the number of
* overdue programs to run. All references to the PAYR are via
* PAYZPF, a temporary file holding all the PAYR records for
* this program to process.
*
* This program is part of the regular processing batch run.
* It performs all the processing of overdue contracts that is
* applicable to LIFE/400. A new subroutine, CRTLOAN, will be
* called from within the batch job to perform any processing
* related to the creation of APLs.
*
* All of the table items needed will be retrieved from working
* storage arrays initially loaded via the ITEMPF using the
* appropriate key, with the exception of tables T5645 and T5679.
* There is no need to store these in arrays since only one item
* per table is used throughout.
*
* 1000-INITIALISE.
* ================
* Issue an override to read the correct PAYZPF for this run.
* The PAYZ file read has the name 'PAYZ' + XX + 9999
* where 'XX' is the first 2 chars of BSSC-SYSTEM-PARAM04
* and 9999 is the last four digits of BSSC-SCHEDULE-NUMBER
* e.g. PAYZ2O0001 for the first run
*      PAYZ2O0002 for the second run, etc.
* Initialise the LIFA and PTRN fields.
* Frequently-referenced tables are stored in working storage
* arrays to minimize disk IO.
*
* 2000-READ-FILE.
* ===============
* Read the PAYZ records sequentially keeping a count of
* the number read.
*
* 2500-EDIT.
* ==========
* For each PAYZ record:
* - Read and validate CHDR.
* - Calculate the number of overdue days and compare with
*   the entries on T6654 to determine which type of overdue
*   processing is to be performed.
* - Softlock the contract.
*
* 3000-UPDATE.
* ============
* Read PAYR file.
* Call the appropriate letter subroutine, if one exists.
*
* If TYPE A:
* - Perform 3200-PROCESS-ARREARS.
* If TYPES 1, 2 or 3:
* - Perform 3300-PROCESS-OVERDUE.
*
* Perform A000-STATISTICS (agent/government statistics
* transaction subroutine).
* Release the softlock on the contract.
*
* 3200-PROCESS-ARREARS.
* =====================
* Calculate the number of months the contract has been
* in force.
* If T6654 has an APL method, do the following:
* - Determine whether the contract can have an APL attached
*   to it (i.e. if the surrender value is greater than the
*   outstanding amount). If not, proceed to next record. If
*   so, proceed as follows:
* - Read and rewrite the CHDR.
* - Call 'CRTLOAN' to process the loan.
* - Write a PTRN.
* If T6654 does not have an APL method:
* - Rewrite CHDR.
* - Call 'OVERBILL' to attempt to bring the PTDATE and BTDATE
*   into line.
* - Read CHDR file.
* - Rewrite the CHDR with validflag = 2.
* - Create a new CHDR record with validflag = 1.
* - If the PTDATE and BTDATE still do not match, ignore this
*   contract. Otherwise, read the COVRRNL to get all records
*   relevant to this contract. Store the new risk and premium
*   statuses (either from T6597 or the originals if there is
*   no non-forfeiture processing) as they are updated, in the
*   COVR array.  At the end of coverage processing, the T5399
*   risk statuses are read in turn, and for each one, the COVR
*   array is searched for a match.  If there is no match, the
*   next risk from the table is searched for and so on.  The
*   first risk status found causes the corresponding 'set'
*   contract status to be written to the risk status on the
*   contract.  The same processing is then repeated for the
*   premium statuses.  If no match is found in either of the
*   two phases, the corresponding 'set' value is taken from
*   T5679.
* - Rewrite the CHDR with the new statuses.
*
* 3300-PROCESS-OVERDUE.
* =====================
* Rewrite the CHDR with validflag = 2.
* Set the overdue suppression dates (APLSPFROM and APLSPTO) so
* that the record will not be picked up again until the next
* overdue action date.
* Create a new CHDR record with validflag = 1.
* Write a PTRN.
* Rewrite the PAYR with validflag = 2.
* Create a new PAYR record with validflag = 1.
*
* 4000-CLOSE.
* ===========
* Close temporary PAYZ file.
*
* Control Totals.
* ===============
* 01 : Number of PAYR records read
* 02 : Number of overdue type 1 records processed
* 03 : Total value of overdue type 1 records
* 04 : Number of overdue type 2 records processed
* 05 : Total value of overdue type 2 records
* 06 : Number of overdue type 3 records processed
* 07 : Total value of overdue type 3 records
* 08 : Number of records in arrears
* 09 : Total value of arrears records
* 10 : Number of APLs granted
* 11 : Total value of APLs
* 12 : Number of records not processed
*****************************************************************
* </pre>
*/
public class B5355 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5355");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	
	

	private FixedLengthStringData wsaaEndOfCovr = new FixedLengthStringData(1).init("N");
	

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");

	private FixedLengthStringData wsaaSkipContract = new FixedLengthStringData(1).init("N");
	private Validator skipContract = new Validator(wsaaSkipContract, "Y");
	private PackedDecimalData wsaaSub = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaIdx = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaType = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaType9 = new ZonedDecimalData(1, 0).init(0);
	private String wsaaAplFlag = "N";
		/* WSAA-TRANID */
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaTransactionDate = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTransactionTime = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaPayzFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaPayzFn, 0, FILLER).init("PAYZ");
	private FixedLengthStringData wsaaPayzRunid = new FixedLengthStringData(2).isAPartOf(wsaaPayzFn, 4);
	private ZonedDecimalData wsaaPayzJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaPayzFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();


	private FixedLengthStringData wsaaCovrRskMatch = new FixedLengthStringData(1).init("N");
	private Validator covrRskMatch = new Validator(wsaaCovrRskMatch, "Y");

	private FixedLengthStringData wsaaCovrPrmMatch = new FixedLengthStringData(1).init("N");
	private Validator covrPrmMatch = new Validator(wsaaCovrPrmMatch, "Y");
	private ZonedDecimalData wsaaOverdueDays = new ZonedDecimalData(4, 0).init(0);
	
	private ZonedDecimalData wsaaDurationInForce = new ZonedDecimalData(3, 0).init(ZERO);

	private FixedLengthStringData wsaaLastPtrn = new FixedLengthStringData(8).init(SPACES);
	private String wsaaChdrOk = "";
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPayrAplsupr = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaPayrAplspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrAplspto = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaCovrrnlStatiiSet = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCovrrnlPstat = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCovrrnlStat = new FixedLengthStringData(2);
	//private static final int wsaaT5687Size = 500;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5687Size = 1000;
	private PackedDecimalData wsaaT5687IxMax = new PackedDecimalData(5, 0);

		/* WSAA-T5687-ARRAY
		    03  WSAA-T5687-REC          OCCURS 200
		    03  WSAA-T5687-REC          OCCURS 300               <LAV4AQ>*/
	private FixedLengthStringData[] wsaaT5687Rec = FLSInittedArray (1000, 21);
	private FixedLengthStringData[] wsaaT5687Key = FLSDArrayPartOfArrayStructure(4, wsaaT5687Rec, 0, SPACES);
	private FixedLengthStringData[] wsaaT5687Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5687Key, 0);
	private FixedLengthStringData[] wsaaT5687Data = FLSDArrayPartOfArrayStructure(17, wsaaT5687Rec, 4);
	private FixedLengthStringData[] wsaaT5687NonForMethod = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 0);
	private PackedDecimalData[] wsaaT5687Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5687Data, 4);
	private FixedLengthStringData[] wsaaT5687Bbmeth = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 9);
	private FixedLengthStringData[] wsaaT5687Pumeth = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 13);
	//private static final int wsaaT6597Size = 100;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT6597Size = 1000;
	private PackedDecimalData wsaaT6597IxMax = new PackedDecimalData(5, 0);
	//private static final int wsaaT6647Size = 500;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT6647Size = 2500;	//ILIFE-6753 increased size to 2500
	private PackedDecimalData wsaaT6647IxMax = new PackedDecimalData(5, 0);


		/* WSAA-T6647-ARRAY */
	private FixedLengthStringData[] wsaaT6647Rec = FLSInittedArray (2500, 19);	//ILIFE-6753 increased size to 2500
	private FixedLengthStringData[] wsaaT6647Key = FLSDArrayPartOfArrayStructure(8, wsaaT6647Rec, 0, SPACES);
	private FixedLengthStringData[] wsaaT6647Batctrcde = FLSDArrayPartOfArrayStructure(4, wsaaT6647Key, 0);
	private FixedLengthStringData[] wsaaT6647Cnttype = FLSDArrayPartOfArrayStructure(4, wsaaT6647Key, 4);
	private FixedLengthStringData[] wsaaT6647Data = FLSDArrayPartOfArrayStructure(11, wsaaT6647Rec, 8);
	private PackedDecimalData[] wsaaT6647Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT6647Data, 0);
	private FixedLengthStringData[] wsaaT6647Aloind = FLSDArrayPartOfArrayStructure(1, wsaaT6647Data, 5);
	private FixedLengthStringData[] wsaaT6647Efdcode = FLSDArrayPartOfArrayStructure(2, wsaaT6647Data, 6);
	private ZonedDecimalData[] wsaaT6647SeqNo = ZDArrayPartOfArrayStructure(3, 0, wsaaT6647Data, 8);
	//private static final int wsaaT6654Size = 500;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT6654Size = 1000;

	private FixedLengthStringData wsaaT6654Key2 = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBillchnl2 = new FixedLengthStringData(1).isAPartOf(wsaaT6654Key2, 0);
	private FixedLengthStringData wsaaCnttype2 = new FixedLengthStringData(3).isAPartOf(wsaaT6654Key2, 1);
	//private static final int wsaaT5399Size = 100;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5399Size = 1000;

	private FixedLengthStringData wsaaT5399Key2 = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaAuthCode = new FixedLengthStringData(4).isAPartOf(wsaaT5399Key2, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT5399Key2, 4);
	private PackedDecimalData wsaaT5679Ix = new PackedDecimalData(3, 0).init(0);

		/*01  WSAA-T6634-KEY.                                      <PCPPRT>*/
	private FixedLengthStringData wsaaTr384Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTr384Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr384Key, 0);
	private FixedLengthStringData wsaaTr384Trcde = new FixedLengthStringData(4).isAPartOf(wsaaTr384Key, 3);

	private FixedLengthStringData wsaaCovrStatusArray = new FixedLengthStringData(192);
	private FixedLengthStringData[] wsaaCovrRec = FLSArrayPartOfStructure(48, 4, wsaaCovrStatusArray, 0);
	private FixedLengthStringData[] wsaaCovrRist = FLSDArrayPartOfArrayStructure(2, wsaaCovrRec, 0);
	private FixedLengthStringData[] wsaaCovrPrst = FLSDArrayPartOfArrayStructure(2, wsaaCovrRec, 2);

	private FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(97);
	private FixedLengthStringData wsysLinskey = new FixedLengthStringData(20).isAPartOf(wsysSystemErrorParams, 0);
	private FixedLengthStringData wsysChdrcoy = new FixedLengthStringData(2).isAPartOf(wsysLinskey, 0);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysLinskey, 3);
	protected ZonedDecimalData wsysBillcd = new ZonedDecimalData(8, 0).isAPartOf(wsysLinskey, 12).setUnsigned();
	private FixedLengthStringData wsysSysparams = new FixedLengthStringData(77).isAPartOf(wsysSystemErrorParams, 20);
		/* ERRORS */
	private static final String f454 = "F454";
	private static final String f781 = "F781";
	private static final String h791 = "H791";
	private static final String ivrm = "IVRM";
	private static final String stnf = "STNF";
	private static final String e101 = "E101";
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
	private static final String t6654 = "T6654";
	private static final String t6597 = "T6597";
	private static final String t6647 = "T6647";
	private static final String t5645 = "T5645";
	private static final String t5399 = "T5399";
	private static final String th584 = "TH584";
	private static final String tr384 = "TR384";
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaT5687Ix = new IntegerData();
	private IntegerData wsaaT6597Ix = new IntegerData();
	private IntegerData wsaaT6597SubrIx = new IntegerData();
	private IntegerData wsaaT6597DurmIx = new IntegerData();
	private IntegerData wsaaT6647Ix = new IntegerData();
	protected IntegerData wsaaT6654Ix = new IntegerData();
	private IntegerData wsaaT6654SubrIx = new IntegerData();
	private IntegerData wsaaT6654ExpyIx = new IntegerData();
	private IntegerData wsaaT5399Ix = new IntegerData();
	private IntegerData wsaaT5399CrskIx = new IntegerData();
	private IntegerData wsaaT5399CprmIx = new IntegerData();
	private IntegerData wsaaT5399SrskIx = new IntegerData();
	private IntegerData wsaaT5399SprmIx = new IntegerData();
	private IntegerData wsaaCovrIx = new IntegerData();
	private DescTableDAM descIO = new DescTableDAM();
	private HltxTableDAM hltxIO = new HltxTableDAM();	
	private HpuaTableDAM hpuaIO = new HpuaTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private LinsrnlTableDAM linsrnlIO = new LinsrnlTableDAM();
	private Crtundwrec crtundwrec = new Crtundwrec();
	private Vflagcpy vflagcpy = new Vflagcpy();
	private T5399rec t5399rec = new T5399rec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6597rec t6597rec = new T6597rec();
	private T6647rec t6647rec = new T6647rec();
	protected T6654rec t6654rec =  getT6654rec();
	private Th584rec th584rec = new Th584rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Ovrduerec ovrduerec = new Ovrduerec();
	private Nfloanrec nfloanrec = new Nfloanrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Ovrbillrec ovrbillrec = new Ovrbillrec();
	private Crtloanrec crtloanrec = new Crtloanrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private ControlTotalsInner controlTotalsInner = new ControlTotalsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaT5399ArrayInner wsaaT5399ArrayInner = new WsaaT5399ArrayInner();
	private WsaaT6597ArrayInner wsaaT6597ArrayInner = new WsaaT6597ArrayInner();
	protected WsaaT6654ArrayInner wsaaT6654ArrayInner = new WsaaT6654ArrayInner();
	//ILIFE-3997-STARTS
	private Nlgcalcrec nlgcalcrec = new Nlgcalcrec();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private NlgtpfDAO nlgtpfDAO = getApplicationContext().getBean("nlgtpfDAO", NlgtpfDAO.class);
	private List<Nlgtpf> nlgtpfList = new ArrayList<Nlgtpf>();
	Chdrpf chdrpf = null;
	Payrpf payrpf = null; //ILIFE-4323 by dpuhawan
	//ILIFE-3997-ENDS
	private PackedDecimalData wsaaMaxAge = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaMaxYear = new PackedDecimalData(2, 0);
	private FixedLengthStringData wsaaNlgflag = new FixedLengthStringData(1);
	
	private PackedDecimalData wsaaYear = new PackedDecimalData(2, 0);
	private boolean updateRequired=false;
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO",LifepfDAO.class);
	
	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private static final String tr5b2 = "TR5B2";
	private Tr5b2rec tr5b2rec = new Tr5b2rec();
	
	
	
	//fwang3
	private Payzpf payzpfRec = new Payzpf();
	protected Chdrpf chdrlifIO = new Chdrpf();
	private Covrpf covrrnlIO = new Covrpf();
	private Payrpf payrIO = new Payrpf();
	private Ptrnpf ptrnIO = new Ptrnpf();

    private PayzpfDAO payzpfDAO = getApplicationContext().getBean("payzpfDAO", PayzpfDAO.class);
    private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
    private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
    private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private List<Ptrnpf> ptrnpfList = new ArrayList<Ptrnpf>();
	private List<Itempf> itempfList = new ArrayList<Itempf>();
    private int batchID = 0;
    private int batchExtractSize;
	
	private Iterator<Payzpf> payzpfIter;
	private Map<String, List<Chdrpf>> chdrpfMap;
	private Map<String, List<Payrpf>> payrpfMap;
	private Map<String, List<Covrpf>> covrpfMap;
	private List<Chdrpf> chdrUpdateList = new ArrayList<Chdrpf>();
	private List<Chdrpf> chdrUpdateInsertedList = new ArrayList<Chdrpf>();
	private List<Payrpf> payrUpdatePstcdeList = new ArrayList<Payrpf>();	
	private List<Chdrpf> chdrInsertList = new ArrayList<Chdrpf>();
	private List<Payrpf> payrpfUpdateList = new ArrayList<Payrpf>();
	private List<Payrpf> payrpfInsertList = new ArrayList<Payrpf>();
	private List<Covrpf> covrpfInsertList = new ArrayList<Covrpf>();
	private List<Covrpf> covrpfInsertList2 = new ArrayList<Covrpf>();
	private int ct01Val;
	private int ct02Val;
	private BigDecimal ct03Val = BigDecimal.ZERO;
	private int ct04Val;
	private BigDecimal ct05Val = BigDecimal.ZERO;
	private int ct06Val;
	private BigDecimal ct07Val = BigDecimal.ZERO;
	private int ct08Val;
	private BigDecimal ct09Val = BigDecimal.ZERO;
	private int ct10Val;
	private BigDecimal ct11Val = BigDecimal.ZERO;
	private int ct12Val;
	private boolean goNext;
	private Map<String, List<Itempf>> t5399ListMap;
	private Map<String, List<Itempf>> t6654ListMap; //ILIFE-4323 by dpuhawan
	private ArrayList<String> riskStats = new ArrayList<String>();
	private ArrayList<String> premStats = new ArrayList<String>();
	private ArrayList<String> cnRiskStats = new ArrayList<String>();
	private ArrayList<String> cnPremStats = new ArrayList<String>();
	private ArrayList<String> covrPremStats = new ArrayList<String>();
	private ArrayList<String> covrRiskStats = new ArrayList<String>();
	private boolean recordFound = true;
	private boolean prmhldtrad = false;
	private Map<String, Itempf> ta524Map;
	private String ta524 = "TA524";
	boolean BTPRO028Permission  = false;
	private List<Ptrnpf> ptrnrecords = null;
	private MandpfDAO mandpfDAO =  getApplicationContext().getBean("mandpfDAO", MandpfDAO.class);
	private ClbapfDAO clbapfDAO =  getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	private Boolean japanBilling = false;
	private static final String ta85 = "TA85";
	private static final String BTPRO027 = "BTPRO027";
	protected Map<String, List<Itempf>> th5agListMap;
	private Th5agrec th5agrec = new Th5agrec();
	private static final String h134 = "H134";
	private FixedLengthStringData wsaaItemKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaYearjp = new FixedLengthStringData(4).isAPartOf(wsaaItemKey, 0);
	private FixedLengthStringData wsaaMop = new FixedLengthStringData(1).isAPartOf(wsaaItemKey, 4);
	private FixedLengthStringData wsaaFact = new FixedLengthStringData(2).isAPartOf(wsaaItemKey, 5);
	private ZonedDecimalData sub1 = new ZonedDecimalData(4, 0).init(0).setUnsigned();
    private OverpfDAO overpfDAO =  getApplicationContext().getBean("overpfDAO", OverpfDAO.class);
    private boolean isAiaAusDirectDebit; //PINNACLE-1683
	// IBPLIFE-9919
    private FixedLengthStringData[] wsaaT5687ToRec = FLSInittedArray (1000, 8);
    private PackedDecimalData[] wsaaT5687ItmTo = PDArrayPartOfArrayStructure(8, 0, wsaaT5687ToRec);
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		lockContract2570,
		ignoreContract2580,
		exit2590,
		error2850,
		exit2890,
		skipLetter3125,
		exit3190,
		lapsePolicy3220,
		controlTotals3280,
		exit3290,
		searchT65976120,
		a200Delet,
		a200Exit
	}

	public B5355() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public T6654rec getT6654rec() {
	return new T6654rec();
}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
        if (!recordFound){
        	wsspEdterror.set(SPACES);
        	return;
        }		
		loadTables1050();
	}

protected void initialise1010()
	{
		isAiaAusDirectDebit = FeaConfg.isFeatureExist(bsprIO.getCompany().trim(), "BTPRO029", appVars, "IT");	//PINNACLE-1683
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", "BTPRO028", appVars, "IT");
		japanBilling = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), BTPRO027, appVars, "IT");
		t5399ListMap = new HashMap<String, List<Itempf>>();
		t6654ListMap = new HashMap<String, List<Itempf>>();  //ILIFE-4323 by dpuhawan
		if (isNE(bprdIO.getRestartMethod(), "3")) {
			syserrrec.syserrStatuz.set(ivrm);
			fatalError600();
		}
		wsspEdterror.set(varcom.oK);
		wsaaPayzRunid.set(bprdIO.getSystemParam04());
		wsaaPayzJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		//fwang3
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				batchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				batchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			batchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
        readChunkRecord();
        if (!recordFound) return;
		/*    Access today's date using DATCON1.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		wsaaTime.set(getCobolTime());
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaTransactionDate.set(wsaaToday);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(ZERO);
		/*    Initialise and set up all necessary information.*/
		ptrnIO.setUserT(0);
		lifacmvrec.lifacmvRec.set(SPACES);
		ptrnIO.setPtrneff(bsscIO.getEffectiveDate().toInt());
		ptrnIO.setTrdt(bsscIO.getEffectiveDate().toInt());
		lifacmvrec.batccoy.set(batcdorrec.company);
		lifacmvrec.rldgcoy.set(batcdorrec.company);
		lifacmvrec.genlcoy.set(batcdorrec.company);
		ptrnIO.setBatccoy(batcdorrec.company.toString());
		lifacmvrec.batcactyr.set(batcdorrec.actyear);
		ptrnIO.setBatcactyr(batcdorrec.actyear.toInt());
		lifacmvrec.batcactmn.set(batcdorrec.actmonth);
		ptrnIO.setBatcactmn(batcdorrec.actmonth.toInt());
		lifacmvrec.batctrcde.set(batcdorrec.trcde);
		ptrnIO.setBatctrcde(batcdorrec.trcde.toString());
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		ptrnIO.setBatcbatch(batcdorrec.batch.toString());
		lifacmvrec.batcbrn.set(batcdorrec.branch);
		ptrnIO.setBatcbrn(batcdorrec.branch.toString());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.user.set(0);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		ptrnIO.setTrtm(wsaaTime.toInt());
		String prmhldtradItem = "CSOTH010";	//ILIFE-8179
		prmhldtrad = FeaConfg.isFeatureExist(batcdorrec.company.toString(), prmhldtradItem, appVars, "IT");
	}


	private void readChunkRecord() {
		List<Payzpf> payzpfList = payzpfDAO.findResult(wsaaPayzFn.toString(), wsaaThreadMember.toString(), batchExtractSize, batchID);
		if (null == payzpfList || payzpfList.isEmpty()){
			recordFound = false;
			return;
		}
		payzpfIter = payzpfList.iterator();
		List<String> chdrnumList = new ArrayList<>();
		for (Payzpf p : payzpfList) {
			chdrnumList.add(p.getChdrnum());
		}
		chdrpfMap = chdrpfDAO.searchChdrRecordByChdrnum(chdrnumList);
		payrpfMap = payrpfDAO.searchPayrRecordByChdrnumChdrcoy(bsprIO.getCompany().trim(),chdrnumList);
		covrpfMap = covrpfDAO.searchCovrrnlByChdrnum(chdrnumList);
	}

protected void loadTables1050()
	{
	
		String itemcoy = bsprIO.getCompany().toString();
		
		List<Itempf> items = itemDAO.getAllItemitem("IT", itemcoy, t5679, bprdIO.getAuthCode().toString());
		if (null == items || items.isEmpty()) {
			syserrrec.params.set(t5679);
			fatalError600();
		} else {//IJTI-320 START
			t5679rec.t5679Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
		}
		//IJTI-320 END
	
		
		wsaaT6647Ix.set(1);
		Map<String,List<Itempf>> itemMap = itemDAO.loadSmartTable("IT", itemcoy, t6647);
		if(itemMap!=null){
			for(String itemkey:itemMap.keySet()){
				if(itemkey.startsWith(bprdIO.getAuthCode().toString())){
					for(Itempf i:itemMap.get(itemkey)){
						if(i.getItmfrm().compareTo(varcom.vrcmMaxDate.getbigdata())<=0){
							if (isGT(wsaaT6647Ix, wsaaT6647Size)) {
								syserrrec.statuz.set(h791);
								syserrrec.params.set(t6647);
								fatalError600();
							}
							t6647rec.t6647Rec.set(StringUtil.rawToString(i.getGenarea()));
							wsaaT6647Key[wsaaT6647Ix.toInt()].set(i.getItemitem());
							wsaaT6647Itmfrm[wsaaT6647Ix.toInt()].set(i.getItmfrm());
							wsaaT6647Aloind[wsaaT6647Ix.toInt()].set(t6647rec.aloind);
							wsaaT6647Efdcode[wsaaT6647Ix.toInt()].set(t6647rec.efdcode);
							wsaaT6647SeqNo[wsaaT6647Ix.toInt()].set(t6647rec.procSeqNo);
							wsaaT6647IxMax.set(wsaaT6647Ix);
							wsaaT6647Ix.add(1);
						}
					}
				}
			}
		}
		
		wsaaT5687Ix.set(1);
		itemMap = itemDAO.loadSmartTable("IT", itemcoy, t5687);
		if(itemMap!=null){
			for (List<Itempf> pfList : itemMap.values()) {
				for (Itempf item : pfList) {
					if(item.getItmfrm().compareTo(varcom.vrcmMaxDate.getbigdata())<=0){
			if (isGT(wsaaT5687Ix, wsaaT5687Size)) {
				syserrrec.statuz.set(h791);
				syserrrec.params.set(t5687);
				fatalError600();
			}
			t5687rec.t5687Rec.set(StringUtil.rawToString(item.getGenarea()));
			wsaaT5687Crtable[wsaaT5687Ix.toInt()].set(item.getItemitem());
			wsaaT5687Itmfrm[wsaaT5687Ix.toInt()].set(item.getItmfrm());
			wsaaT5687ItmTo[wsaaT5687Ix.toInt()].set(item.getItmto());
			wsaaT5687Pumeth[wsaaT5687Ix.toInt()].set(t5687rec.pumeth);
			wsaaT5687NonForMethod[wsaaT5687Ix.toInt()].set(t5687rec.nonForfeitMethod);
			wsaaT5687IxMax.set(wsaaT5687Ix);
			wsaaT5687Ix.add(1);
		}
				}
			}
		}
		
		wsaaT6597Ix.set(1);
		itemMap = itemDAO.loadSmartTable("IT", itemcoy, t6597);
		if(itemMap!=null){
			for (List<Itempf> pfList : itemMap.values()) {
				for (Itempf item : pfList) {
					if(item.getItmfrm().compareTo(varcom.vrcmMaxDate.getbigdata())<=0){
			if (isGT(wsaaT6597Ix, wsaaT6597Size)) {
				syserrrec.statuz.set(h791);
				syserrrec.params.set(t6597);
				fatalError600();
			}
			t6597rec.t6597Rec.set(StringUtil.rawToString(item.getGenarea()));
			wsaaT6597ArrayInner.wsaaT6597Method[wsaaT6597Ix.toInt()].set(item.getItemitem());
			wsaaT6597ArrayInner.wsaaT6597Itmfrm[wsaaT6597Ix.toInt()].set(item.getItmfrm());
			for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
				wsaaT6597SubrIx.set(wsaaSub);
				wsaaT6597ArrayInner.wsaaT6597Premsub[wsaaT6597Ix.toInt()][wsaaT6597SubrIx.toInt()].set(t6597rec.premsubr[wsaaSub.toInt()]);
				wsaaT6597ArrayInner.wsaaT6597Cpstat[wsaaT6597Ix.toInt()][wsaaT6597SubrIx.toInt()].set(t6597rec.cpstat[wsaaSub.toInt()]);
				wsaaT6597ArrayInner.wsaaT6597Crstat[wsaaT6597Ix.toInt()][wsaaT6597SubrIx.toInt()].set(t6597rec.crstat[wsaaSub.toInt()]);
			}
			for (wsaaSub.set(1); !(isGT(wsaaSub, 3)); wsaaSub.add(1)){
				wsaaT6597DurmIx.set(wsaaSub);
				wsaaT6597ArrayInner.wsaaT6597Durmnth[wsaaT6597Ix.toInt()][wsaaT6597DurmIx.toInt()].set(t6597rec.durmnth[wsaaSub.toInt()]);
			}
			wsaaT6597IxMax.set(wsaaT6597Ix);
			wsaaT6597Ix.add(1);
		}
				}
			}
		}
		
		wsaaT6654Ix.set(1);
		//ILIFE-4323 start by dpuhawan
		t6654ListMap = itemDAO.loadSmartTable("IT", itemcoy, t6654);
		if(null == t6654ListMap || t6654ListMap.isEmpty()){
		
		/*
		//Map<String, List<Itempf>> t6654Map = itemDAO.loadSmartTable("IT", itemcoy, t6654);
		List<Itempf> pfList = itemDAO.getAllitems("IT", itemcoy, t6654);
		if(null == pfList || pfList.isEmpty()){*/
			syserrrec.params.set(t6654);
			fatalError600();
		}
		//for (List<Itempf> pfList : t6654Map.values()) {
		/*
			for (Itempf item : pfList) {
				if (isGT(wsaaT6654Ix, wsaaT6654Size)) {
					syserrrec.statuz.set(h791);
					syserrrec.params.set(t6654);
					fatalError600();
				}
				t6654rec.t6654Rec.set(StringUtil.rawToString(item.getGenarea()));
				wsaaT6654ArrayInner.wsaaT6654Key[wsaaT6654Ix.toInt()].set(item.getItemitem());
				wsaaT6654ArrayInner.wsaaT6654NonForfDays[wsaaT6654Ix.toInt()].set(t6654rec.nonForfietureDays);
				wsaaT6654ArrayInner.wsaaT6654ArrearsMethod[wsaaT6654Ix.toInt()].set(t6654rec.arrearsMethod);
				for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
					wsaaT6654SubrIx.set(wsaaSub);
					wsaaT6654ArrayInner.wsaaT6654Doctid[wsaaT6654Ix.toInt()][wsaaT6654SubrIx.toInt()].set(t6654rec.doctid[wsaaSub.toInt()]);
				}
				for (wsaaSub.set(1); !(isGT(wsaaSub, 3)); wsaaSub.add(1)){
					wsaaT6654ExpyIx.set(wsaaSub);
					wsaaT6654ArrayInner.wsaaT6654Daexpy[wsaaT6654Ix.toInt()][wsaaT6654ExpyIx.toInt()].set(t6654rec.daexpy[wsaaSub.toInt()]);
				}
				wsaaT6654Ix.add(1);
			}
			*/
		//ILIFE-4323 end
		//}

		
		wsaaT5399Ix.set(1);
		t5399ListMap = itemDAO.loadSmartTable("IT", itemcoy, t5399);

		/*
		for (List<Itempf> itemList : t5399Map.values()) {
			for (Itempf item : itemList) {
				if (isGT(wsaaT5399Ix, wsaaT5399Size)) {
					syserrrec.statuz.set(h791);
					syserrrec.params.set(t5399);
					fatalError600();
				}
				t5399rec.t5399Rec.set(StringUtil.rawToString(item.getGenarea()));
				wsaaT5399ArrayInner.wsaaT5399Key[wsaaT5399Ix.toInt()].set(item.getItemitem());
				wsaaT5399ArrayInner.wsaaT5399RiskStats[wsaaT5399Ix.toInt()].set(t5399rec.covRiskStats);
				wsaaT5399ArrayInner.wsaaT5399PremStats[wsaaT5399Ix.toInt()].set(t5399rec.covPremStats);
				wsaaT5399ArrayInner.wsaaT5399SetRiskStats[wsaaT5399Ix.toInt()].set(t5399rec.setCnRiskStats);
				wsaaT5399ArrayInner.wsaaT5399SetPremStats[wsaaT5399Ix.toInt()].set(t5399rec.setCnPremStats);
				wsaaT5399Ix.add(1);
			}
		}
		*/
		wsaaPrevChdrnum.set(SPACES);
		if(prmhldtrad)	//ILIFE-8179
			ta524Map = itemDAO.getItemMap("IT", itemcoy, ta524);
		if (japanBilling)
			th5agListMap = itemDAO.loadSmartTable("IT", bsprIO.getFsuco().toString().trim(), "TH5AG");
	}
protected boolean readT6654(String searchItem) {
	String strEffDate = bsscIO.getEffectiveDate().toString();
	String keyItemitem = searchItem.trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;	
	
	if (t6654ListMap.containsKey(keyItemitem)){	
		itempfList = t6654ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					wsaaT6654ArrayInner.wsaaT6654Key[wsaaT6654Ix.toInt()].set(itempf.getItemitem());
					wsaaT6654ArrayInner.wsaaT6654NonForfDays[wsaaT6654Ix.toInt()].set(t6654rec.nonForfietureDays);
					setAutoLapsePeriodCustomerSpecific();
					wsaaT6654ArrayInner.wsaaT6654ArrearsMethod[wsaaT6654Ix.toInt()].set(t6654rec.arrearsMethod);
					for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
						wsaaT6654SubrIx.set(wsaaSub);
						wsaaT6654ArrayInner.wsaaT6654Doctid[wsaaT6654Ix.toInt()][wsaaT6654SubrIx.toInt()].set(t6654rec.doctid[wsaaSub.toInt()]);
					}
					for (wsaaSub.set(1); !(isGT(wsaaSub, 3)); wsaaSub.add(1)){
						wsaaT6654ExpyIx.set(wsaaSub);
						wsaaT6654ArrayInner.wsaaT6654Daexpy[wsaaT6654Ix.toInt()][wsaaT6654ExpyIx.toInt()].set(t6654rec.daexpy[wsaaSub.toInt()]);
					}
					wsaaT6654Ix.add(1);					itemFound = true;
				}
			}else{
				t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				wsaaT6654ArrayInner.wsaaT6654Key[wsaaT6654Ix.toInt()].set(itempf.getItemitem());
				wsaaT6654ArrayInner.wsaaT6654NonForfDays[wsaaT6654Ix.toInt()].set(t6654rec.nonForfietureDays);
				wsaaT6654ArrayInner.wsaaT6654ArrearsMethod[wsaaT6654Ix.toInt()].set(t6654rec.arrearsMethod);
				setAutoLapsePeriodCustomerSpecific();
				for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
					wsaaT6654SubrIx.set(wsaaSub);
					wsaaT6654ArrayInner.wsaaT6654Doctid[wsaaT6654Ix.toInt()][wsaaT6654SubrIx.toInt()].set(t6654rec.doctid[wsaaSub.toInt()]);
				}
				for (wsaaSub.set(1); !(isGT(wsaaSub, 3)); wsaaSub.add(1)){
					wsaaT6654ExpyIx.set(wsaaSub);
					wsaaT6654ArrayInner.wsaaT6654Daexpy[wsaaT6654Ix.toInt()][wsaaT6654ExpyIx.toInt()].set(t6654rec.daexpy[wsaaSub.toInt()]);
				}
				wsaaT6654Ix.add(1);				itemFound = true;					
			}				
		}		
	}
	return itemFound;	
		
}

protected boolean readT5399(String searchItem){
	String strEffDate = bsscIO.getEffectiveDate().toString();
	String keyItemitem = searchItem.trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5399ListMap.containsKey(keyItemitem)){	
		itempfList = t5399ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t5399rec.t5399Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					riskStats = new ArrayList<> (Arrays.asList(t5399rec.covRiskStats.toString().trim().split("(?<=\\G.{2})")));
					premStats = new ArrayList<> (Arrays.asList( t5399rec.covPremStats.toString().trim().split("(?<=\\G.{2})")));					
					cnRiskStats = new ArrayList<> (Arrays.asList(t5399rec.setCnRiskStats.toString().trim().split("(?<=\\G.{2})")));					
					cnPremStats = new ArrayList<> (Arrays.asList(t5399rec.setCnPremStats.toString().trim().split("(?<=\\G.{2})")));					
					itemFound = true;
				}
			}else{
				t5399rec.t5399Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				riskStats = new ArrayList<> (Arrays.asList(t5399rec.covRiskStats.toString().trim().split("(?<=\\G.{2})")));
				premStats = new ArrayList<> (Arrays.asList( t5399rec.covPremStats.toString().trim().split("(?<=\\G.{2})")));					
				cnRiskStats = new ArrayList<> (Arrays.asList(t5399rec.setCnRiskStats.toString().trim().split("(?<=\\G.{2})")));					
				cnPremStats = new ArrayList<> (Arrays.asList(t5399rec.setCnPremStats.toString().trim().split("(?<=\\G.{2})")));					
				itemFound = true;					
			}				
		}		
	}
	return itemFound;
}

protected void readFile2000()
	{
//		payzpf.read(payzpfRec);
//		if (payzpf.isAtEnd()) {
//			wsspEdterror.set(varcom.endp);
//			return ;
//		}
		if (payzpfIter != null && payzpfIter.hasNext()) {
			payzpfRec = payzpfIter.next();
		} else {
			batchID++;
			//clearList2100();
			readChunkRecord();
			if (payzpfIter != null && payzpfIter.hasNext()) {
				payzpfRec = payzpfIter.next();
			} else {
				 wsspEdterror.set(varcom.endp);
	             return;
			}
		}
		
		chdrpf = chdrpfDAO.getchdrRecord(payzpfRec.getChdrcoy(),payzpfRec.getChdrnum());//ILIFE-3997
	}

protected void edit2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					edit2510();
				case lockContract2570:
					lockContract2570();
				case ignoreContract2580:
					ignoreContract2580();
				case exit2590:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void edit2510()
	{
		ct01Val++;
		wsspEdterror.set(varcom.oK);
		readChdr5000(); 
		readPayr5500();  //ILIFE-4323 by dpuhawan
		/*    If the contract number has changed, validate the contract.*/
		/*    If the contract status is not valid, read the next record.*/
		if (isNE(payzpfRec.getChdrnum(), wsaaPrevChdrnum)) {
			validateChdr5100();
			if (!validContract.isTrue()) {
				goTo(GotoLabel.ignoreContract2580);
			}
		}
		if (japanBilling) {
			ptrnrecords = ptrnpfDAO.searchPtrnenqRecord(payzpfRec.getChdrcoy(), payzpfRec.getChdrnum());
			if (ptrnrecords != null && !ptrnrecords.isEmpty() && isEQ(ptrnrecords.get(0).getBatctrcde(),ta85)) {
				Mandpf mandpf = mandpfDAO.searchMandpfRecordData(chdrlifIO.getCowncoy().toString(), chdrlifIO.getCownnum(), chdrlifIO.getMandref(), "MANDPF");
				if(null != mandpf){
					Clbapf clbapf = clbapfDAO.searchClbapfRecordData(mandpf.getBankkey(), mandpf.getBankacckey().trim(), chdrlifIO.getCowncoy().toString(), chdrlifIO.getCownnum(),"CN");
					if(null != clbapf){
						wsaaYearjp.set(batcdorrec.actyear);
						wsaaMop.set(chdrlifIO.getBillchnl());
						wsaaFact.set(clbapf.getFacthous());
						readTh5ag();
						sub1.set(bsscIO.getEffectiveDate().toString().substring(4, 6));
						if (isLT(bsscIO.getEffectiveDate(), th5agrec.prcbilldte[sub1.toInt()])) {
							goTo(GotoLabel.ignoreContract2580);
						}
					}
				}
			}
		}
	    	
		//ILIFE-3997 Starts
		readTr5b2();
		//ILIFE-3997 End
		//ILIFE-4323 start by dpuhawan
		String key;
		if (BTPRO028Permission) {
			 key = payrIO.getBillchnl().trim().concat(chdrlifIO.getCnttype().trim());
			 key = key.concat(chdrlifIO.getBillfreq().trim());
 		}
		else{
			 key = payrIO.getBillchnl().trim().concat(chdrlifIO.getCnttype().trim());/* IJTI-1523 IJTI-1584 */
		}
		if (!readT6654(key)) {
			if (BTPRO028Permission) {
				 key = payrIO.getBillchnl().trim().concat(chdrlifIO.getCnttype().trim());
				 key = key.concat("**");
			}
			else{
				key = payrIO.getBillchnl().trim().concat("***");/* IJTI-1523 */
			}
			if (!readT6654(key)) {
				if (BTPRO028Permission) {
					 key = payrIO.getBillchnl().trim().concat("*****");
					 if (!readT6654(key)) {
						 	syserrrec.statuz.set(h791);
							syserrrec.params.set(t6654);
							fatalError600();
					 }
				}	 
				else{
					syserrrec.statuz.set(h791);
					syserrrec.params.set(t6654);
					fatalError600();
				}
			}
		}	
		//ILIFE-4323 end
		calcOverdueDays2800();
		/*    Once this stage is reached, the contract has gone into*/
		/*    non-forfeiture and therefore T6654 should contain at*/
		/*    least one non-forfeiture entry. If there is no such*/
		/*    entry, report this, ignore the contract and continue*/
		/*    processing the next.*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 3)
		|| isNE(wsaaT6654ArrayInner.wsaaT6654NonForfDays[wsaaT6654Ix.toInt()], 0)
		|| isNE(wsaaT6654ArrayInner.wsaaT6654Daexpy[wsaaT6654Ix.toInt()][wsaaSub.toInt()], 0)); wsaaSub.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(wsaaSub, 3)) {
			conlogrec.error.set(f454);
			conlogrec.params.set(payzpfRec.getChdrnum());
			callConlog003();
			goTo(GotoLabel.ignoreContract2580);
		}
		/*    Determine which type of overdue processing by comparing*/
		/*    the number of overdue days with the entries in T6654. For*/
		/*    arrears, move 'A' to WSAA-TYPE and '4' to the subroutine*/
		/*    index. For types 1, 2 and 3 processing, move 1, 2 and 3*/
		/*    respectively to both WSAA-TYPE and the subroutine index.*/
		/*    If the number of overdue days does not correspond to any*/
		/*    of the entries in T6654, ignore this contract and proceed*/
		/*    to the next.*/
		if (BTPRO028Permission) {
			Overpf overpf = overpfDAO.getOverpfRecord(payzpfRec.getChdrcoy(), payzpfRec.getChdrnum(), "1");
			if(null!= overpf && isEQ(overpf.getOverdueflag(),"O") && isEQ(overpf.getEnddate(),varcom.vrcmMaxDate ))
				{
						goTo(GotoLabel.ignoreContract2580);
						
				}
		}
		
		if (isLTE(wsaaT6654ArrayInner.wsaaT6654NonForfDays[wsaaT6654Ix.toInt()], wsaaOverdueDays)
		&& isNE(wsaaT6654ArrayInner.wsaaT6654NonForfDays[wsaaT6654Ix.toInt()], 0)) {
			wsaaT6654SubrIx.set(4);
			wsaaType.set("A");
			goTo(GotoLabel.lockContract2570);
		}
		for (wsaaT6654ExpyIx.set(3); !(isEQ(wsaaT6654ExpyIx, 0)
		|| (isLTE(wsaaT6654ArrayInner.wsaaT6654Daexpy[wsaaT6654Ix.toInt()][wsaaT6654ExpyIx.toInt()], wsaaOverdueDays)
		&& isNE(wsaaT6654ArrayInner.wsaaT6654Daexpy[wsaaT6654Ix.toInt()][wsaaT6654ExpyIx.toInt()], 0))); wsaaT6654ExpyIx.add(-1))
{
			/*CONTINUE_STMT*/
		}
		if (isEQ(wsaaT6654ExpyIx, 0)) {
			goTo(GotoLabel.ignoreContract2580);
		}
		else {
			wsaaT6654SubrIx.set(wsaaT6654ExpyIx);
			wsaaType9.set(wsaaT6654ExpyIx);
			wsaaType.set(wsaaType9);
		}
	}

protected void readTh5ag() {

	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (th5agListMap.containsKey(wsaaItemKey.toString().trim())) {
		itempfList = th5agListMap.get(wsaaItemKey.toString().trim());
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			th5agrec.th5agRec.set(StringUtil.rawToString(itempf.getGenarea()));
			itemFound = true;
		}
	}
	if (!itemFound) {
		wsaaFact.set("**");
		if (th5agListMap.containsKey(wsaaItemKey)) {
			itempfList = th5agListMap.get(wsaaItemKey);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				th5agrec.th5agRec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;
			}
		}
	}
	if (!itemFound) {
		syserrrec.params.set("TH5AG".concat(wsaaItemKey.toString()));
		syserrrec.statuz.set(h134);
		fatalError600();
	}

}


private void readTr5b2() {
		itempfList = itemDAO.getAllItemitem("IT", bsprIO.getCompany().toString(), 
				tr5b2, chdrlifIO.getCnttype());/* IJTI-1523 */
		if (itempfList != null && !itempfList.isEmpty()) {
			for (Itempf item : itempfList) {
			tr5b2rec.tr5b2Rec.set(StringUtil.rawToString(item.getGenarea()));
		}
	}
	wsaaNlgflag.set(tr5b2rec.nlg);
	wsaaMaxAge.set(tr5b2rec.nlgczag);
	wsaaMaxYear.set(tr5b2rec.maxnlgyrs);
	
}


protected void lockContract2570()
	{
		softlockContract2600();
		/*    If contract is already locked, record this, ignore*/
		/*    contract and continue processing next.*/
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			/*     MOVE SFTL-STATUZ        TO CONL-ERROR                    */
			conlogrec.error.set(e101);
			conlogrec.params.set(sftlockrec.sftlockRec);
			callConlog003();
			/*CONTINUE_STMT*/
		}
		else {
			goTo(GotoLabel.exit2590);
		}
	}

protected void ignoreContract2580()
	{
		/*    Get here, therefore no overdue processing is required and*/
		/*    we can ignore this contract.*/
		wsspEdterror.set(SPACES);
		ct12Val++;
	}

protected void softlockContract2600()
	{
		para2610();
	}

protected void para2610()
	{
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(bsprIO.getCompany());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(payzpfRec.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			wsysSysparams.set(sftlockrec.sftlockRec);
			syserrrec.params.set(wsysSystemErrorParams);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void releaseSoftlock2700()
	{
		para2710();
	}

protected void para2710()
	{
		sftlockrec.function.set("UNLK");
		sftlockrec.company.set(bsprIO.getCompany());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(payzpfRec.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			wsysSysparams.set(sftlockrec.sftlockRec);
			syserrrec.params.set(wsysSystemErrorParams);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void calcOverdueDays2800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					days2810();
				case error2850:
					error2850();
				case exit2890:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void days2810()
	{
		/*    Call DATCON3 to calculate the no. of overdue days.*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(payzpfRec.getPtdate());
		datcon3rec.intDate2.set(bsscIO.getEffectiveDate());
		datcon3rec.frequency.set("DY");
		datcon3rec.freqFactor.set(0);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		wsaaOverdueDays.set(datcon3rec.freqFactor);
		/*    Search T6654 using billing channel and contract type as*/
		/*    the key. If none is found, search the table again using*/
		/*    contract type '***'. If neither case is found, produce*/
		/*    an error.*/
		wsaaBillchnl2.set(payzpfRec.getBillchnl());
		wsaaCnttype2.set(chdrlifIO.getCnttype());
		ArraySearch as1 = ArraySearch.getInstance(wsaaT6654ArrayInner.wsaaT6654Rec);
		as1.setIndices(wsaaT6654Ix);
		as1.addSearchKey(wsaaT6654ArrayInner.wsaaT6654Key, wsaaT6654Key2, true);
		if (as1.binarySearch()) {
			/*CONTINUE_STMT*/
		}
		else {
			wsaaCnttype2.set("***");
			ArraySearch as3 = ArraySearch.getInstance(wsaaT6654ArrayInner.wsaaT6654Rec);
			as3.setIndices(wsaaT6654Ix);
			as3.addSearchKey(wsaaT6654ArrayInner.wsaaT6654Key, wsaaT6654Key2, true);
			if (as3.binarySearch()) {
				/*CONTINUE_STMT*/
			}
			else {
				goTo(GotoLabel.error2850);
			}
		}
		goTo(GotoLabel.exit2890);
	}

protected void error2850()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t6654);
		itemIO.setItemitem(wsaaT6654Key2);
		itemIO.setFunction(varcom.readr);
		itemIO.setStatuz(stnf);
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}

protected void update3000()
	{
		/*UPDATE*/
		if (isEQ(wsaaType, "1")) {
			callT6654Subr3100();
		}
		if (isEQ(wsaaType, "2")) {
			callT6654Subr3100();
		}
		if (isEQ(wsaaType, "3")) {
			callT6654Subr3100();
		}
		if (isEQ(wsaaType, "A")) {
			callT6654Subr3100();
		}
		a000Statistics();
		releaseSoftlock2700();
		/*EXIT*/
	}

protected void callT6654Subr3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start3110();
				case skipLetter3125:
					skipLetter3125();
				case exit3190:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3110()
	{
		/*    MOVE CHDRLIF-TRANNO         TO WSAA-TRANNO.             <003>*/
		//readPayr5500(); //ILIFE-4323 by dpuhawan
		/*  If there is no letter subroutine, for arrears contracts we     */
		/*  still need to go into PROCESS-ARREARS because there may        */
		/*  nevertheless be non-forfeiture processing.  For all other types*/
		/*  nothing is done to the contract - A PTRN is NOT written either.*/
		if (isEQ(wsaaT6654ArrayInner.wsaaT6654Doctid[wsaaT6654Ix.toInt()][wsaaT6654SubrIx.toInt()], SPACES)) {
			if (isEQ(wsaaType, "A")) {
				goTo(GotoLabel.skipLetter3125);
			}
			else {
				controlTotal3350();
				goTo(GotoLabel.exit3190);
			}
		}
		
		if (isNE(wsaaType, "A")) {
			writeChdrValidflag25400();
			setPrecision(chdrlifIO.getTranno(), 0);
			chdrlifIO.setTranno(chdrlifIO.getTranno());
		}
		
		chdrlifIO.setTranno(chdrlifIO.getTranno()+1);
		setupOverdueRec3400();
		/*    Call the appropriate letter subroutine.*/
		callProgram(wsaaT6654ArrayInner.wsaaT6654Doctid[wsaaT6654Ix.toInt()][wsaaT6654SubrIx.toInt()], ovrduerec.ovrdueRec);
		if (isNE(ovrduerec.statuz, varcom.oK)) {
			syserrrec.subrname.set(wsaaT6654ArrayInner.wsaaT6654Doctid[wsaaT6654Ix.toInt()][wsaaT6654SubrIx.toInt()]);
			syserrrec.params.set(ovrduerec.ovrdueRec);
			syserrrec.statuz.set(ovrduerec.statuz);
			fatalError600();
		}
	//	chdrlifIO.setTranno(chdrlifIO.getTranno()-1);
		/*    ADD 1                       TO CHDRLIF-TRANNO.          <006>*/
		
	}

protected void skipLetter3125()
	{
		if (isEQ(wsaaType, "A")) {
			processArrears3200();
		}
		/*     If type 1, 2 or 3 processing has been performed, the PAYR*/
		/*     suppression details will have changed and the following*/
		/*     updating will be required:*/
		/*     - rewrite the PAYR record with validflag 2*/
		/*     - create a new PAYR record with validflag 1.*/
		if (isEQ(wsaaType, "1")
		|| isEQ(wsaaType, "2")
		|| isEQ(wsaaType, "3")) {
			processOverdue3300();
			//readhPayr5500a();
			rewritePayrValidflag25700();
			createPayrValidflag15600();
		}
	}

protected void processArrears3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					para3210();
				case lapsePolicy3220:
					lapsePolicy3220();
				case controlTotals3280:
					controlTotals3280();
				case exit3290:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3210()
	{
		/*    Read HPAD to determine if client has defined a non-forfeiture*/
		/*    option. If so, read TH584 with this option and the contract  */
		/*    type to obtain the non-forfeiture method, then use this      */
		/*    client defined method for arrear processing than T6654's.    */	
		if (isNE(payzpfRec.getZnfopt(),SPACES)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(bsprIO.getCompany());
			itemIO.setItemtabl(th584);
			itemIO.getItemitem().setSub1String(1, 3, payzpfRec.getZnfopt());
			itemIO.getItemitem().setSub1String(4, 3, chdrlifIO.getCnttype());
			itemIO.setFormat(formatsInner.itemrec);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			else {
				if (isEQ(itemIO.getStatuz(), varcom.oK)) {
					th584rec.th584Rec.set(itemIO.getGenarea());
					if (isEQ(th584rec.nonForfeitMethod, SPACES)) {
						/*             MOVE IVRM       TO SYSR-SYSERR-STATUZ    <Q83>   */
						/*             PERFORM 600-FATAL-ERROR                  <Q83>   */
						wsaaT6654ArrayInner.wsaaT6654ArrearsMethod[wsaaT6654Ix.toInt()].set(SPACES);
					}
					else {
						wsaaT6654ArrayInner.wsaaT6654ArrearsMethod[wsaaT6654Ix.toInt()].set(th584rec.nonForfeitMethod);
					}
				}
			}
		}
		wsaaSkipContract.set("N");
		wsaaChdrOk = "N";
		/*    Calculate the number of months the contract has been*/
		/*    in force.*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.function.set("CMDF");
		datcon3rec.frequency.set("12");
		datcon3rec.intDate1.set(chdrlifIO.getOccdate());
		datcon3rec.intDate2.set(payzpfRec.getPtdate());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		wsaaDurationInForce.set(datcon3rec.freqFactor);
		
		
		/*    Decide whether Contract is going to go to APL (automatic*/
		/*    policy loan ) and if so, process accordingly, otherwise*/
		/*    carry on as normal.*/
		if (isEQ(wsaaType, "A")
		&& isNE(wsaaT6654ArrayInner.wsaaT6654ArrearsMethod[wsaaT6654Ix.toInt()], SPACES)) {
			/*     PERFORM 5400-WRITE-CHDR-VALIDFLAG2                       */
			/*     PERFORM 5300-WRITE-CHDR-VALIDFLAG1                       */
			contractLevelNf7000();
			if (isEQ(wsaaAplFlag, "N")) {
				goTo(GotoLabel.lapsePolicy3220);
			}
			/* If the policy goes into APL - Advance with change of Premium    */
			/* (NONF-STATUZ = 'APLA'), we need to do Reverse Contract Billing  */
			/* to make PTDATE = BTDATE and without changing policy status.     */
			if (isEQ(nfloanrec.statuz, "APLA")) {
				//rewriteChdr5200();
				callOverbill9000();
				//readChdr5000();
				//ILIFE-7363 Start by dpuhawan
				Chdrpf chdrpf = chdrpfDAO.getchdrRecord(payzpfRec.getChdrcoy(), payzpfRec.getChdrnum());
				chdrlifIO = chdrpf;
				setPrecision(chdrlifIO.getTranno(), 0);
				chdrlifIO.setTranno(chdrlifIO.getTranno()-1);
				Integer wsaaTranno = chdrlifIO.getTranno();
				writeChdrValidflag25400();
				setPrecision(chdrlifIO.getTranno(), 0);
				chdrlifIO.setTranno(chdrlifIO.getTranno()+1);
				writeChdrValidflag15300();
				if (isEQ(wsaaType, "A")) {
					payrpf = payrpfDAO.getpayrRecord(payzpfRec.getChdrcoy(), payzpfRec.getChdrnum());
					payrIO = payrpf;
					setPrecision(payrIO.getTranno(), 0);
					payrIO.setTranno(wsaaTranno);
				}
				/*
				setPrecision(chdrlifIO.getTranno(), 0);
				chdrlifIO.setTranno(chdrlifIO.getTranno()-1);
				writeChdrValidflag25400();
				setPrecision(chdrlifIO.getTranno(), 0);
				chdrlifIO.setTranno(chdrlifIO.getTranno()+1);
				writeChdrValidflag15300();
				//readhPayr5500a();
				readPayr5500(); //ILIFE-4323
				setPrecision(payrIO.getTranno(), 0);
				payrIO.setTranno(payrIO.getTranno()-1);
				*/
				//ILIFE-7363 end 
				updatePayr5900();
				writePtrn5800();
				//ILIFE-3997-STARTS
				if(chdrpf != null && chdrpf.getNlgflg() != null && chdrpf.getNlgflg() == 'Y')
					writeNLGRec();
				//ILIFE-3997-ENDS
			}
			else updatePayr5900();
			goTo(GotoLabel.controlTotals3280);
		}
	}

private void readChdrpf() {

	/**chdrpfList = chdrpfDAO.getchdrnlgrecord(chdrlifIO.getChdrcoy().toString());
	if(chdrpfList != null && !chdrpfList.isEmpty())
	{
		for(Chdrpf chdr : chdrpfList)
		{
			if(isEQ(chdr.getChdrnum(),chdrlifIO.getChdrnum()))
			{*/
				if(chdrpf.getNlgflg() == 'Y' && isNE(tr5b2rec.nlg , "N"))
				{
					ovrduerec.statcode.set(covrrnlIO.getStatcode());
					updateRequired = true;
					
				}
			/**}
			
		}
	}*/
	
	
}







protected void checkYear2880(){
	
	wsaaYear.set(ZERO);
	datcon3rec.intDate1.set(chdrlifIO.getOccdate());
	datcon3rec.intDate2.set(bsscIO.getEffectiveDate());
	datcon3rec.frequency.set("01");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz,varcom.oK)) {
		syserrrec.params.set(datcon3rec.datcon3Rec);
		syserrrec.statuz.set(datcon3rec.statuz);
		fatalError600();
	}
	wsaaYear.set(datcon3rec.freqFactor);
}



protected void lapsePolicy3220()
	{
		/*    Because the Billing process writes a PRTN, and since this*/
		/*    contract is about to go into Non-forfeiture and the BTDATE*/
		/*    and PTDATE do not match, use the overdue processing reversal*/
		/*    subroutine OVERBILL to attempt to bring the two dates back*/
		/*    into line.*/
		/*    A report will be produced by OVERBILL detailing what*/
		/*    transactions have been reversed for the contract being*/
		/*    processed.*/
		/*    Check the BTDATE and PTDATE again as these get reset in the*/
		/*    in the 9000 section. If they still do not match, ignore*/
		/*    this contract.*/
		//rewriteChdr5200();
	
	
		callOverbill9000();
		Chdrpf chdrpf = chdrpfDAO.getchdrRecord(payzpfRec.getChdrcoy(), payzpfRec.getChdrnum());
		chdrlifIO = chdrpf;
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(chdrlifIO.getTranno()-1);
		Integer wsaaTranno = chdrlifIO.getTranno();
		writeChdrValidflag25400();
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(chdrlifIO.getTranno()+1);
		writeChdrValidflag15300();
		//ILIFE-4323 start by dpuhawan
		//setPrecision(payrIO.getTranno(), 0);
		//payrIO.setTranno(payrIO.getTranno()-1);
		if (isEQ(wsaaType, "A")) {
			payrpf = payrpfDAO.getpayrRecord(payzpfRec.getChdrcoy(), payzpfRec.getChdrnum());
			payrIO = payrpf;
			setPrecision(payrIO.getTranno(), 0);
			//payrIO.setTranno(payrIO.getTranno()-1);
			payrIO.setTranno(wsaaTranno);
		}	
		//ILIFE-4323 end
		updatePayr5900();
		if (isLT(payzpfRec.getPtdate(), payzpfRec.getBtdate())) {
			writePtrn5800();
			//ILIFE-3997 -STARTS
			if(chdrpf != null && chdrpf.getNlgflg() != null && chdrpf.getNlgflg() == 'Y')
				writeNLGRec();
			//ILIFE-3997-ENDS
			goTo(GotoLabel.controlTotals3280);
		}
		/*    Read COVRRNL to get all valid records relevant to           .*/
		/*    this contract.*/
		wsaaEndOfCovr.set(SPACES);
		
		covrrnlIO = null;
		initialize(wsaaCovrStatusArray);
		wsaaCovrIx.set(1);	
		goNext = false;
		if (covrpfMap != null && covrpfMap.containsKey(payzpfRec.getChdrnum())) {
			for (Covrpf c : covrpfMap.get(payzpfRec.getChdrnum())) {
				if (c.getChdrcoy().equals(payzpfRec.getChdrcoy())
						&& 0 == c.getPlanSuffix()) {
					covrrnlIO = c;
					callCovrrnlio6000();
					if (goNext) continue;
				}
			}
		}

		if (covrrnlIO == null) {
			syserrrec.params.set(payzpfRec.getChdrnum());
			fatalError600();
		}
		
		/*
		initialize(wsaaCovrStatusArray);
		wsaaCovrIx.set(1);
		while ( !(endOfCovr.isTrue())) {
			callCovrrnlio6000();
		}
		*/

		/* Skip Contract and write control total                           */
		if (skipContract.isTrue()) {
			writePtrn5800();
			//ILIFE-3997 -STARTS
			if(chdrpf != null && chdrpf.getNlgflg() != null && chdrpf.getNlgflg() == 'Y')
				writeNLGRec();
			//ILIFE-3997-ENDS
			contotrec.totno.set(controlTotalsInner.ct12);
			contotrec.totval.set(1);
			callContot001();
			ct12Val++;
			goTo(GotoLabel.exit3290);
		}
		endOfCovrProcess6800();
		if (isNE(chdrlifIO.getChdrnum(), wsaaLastPtrn)) {
			wsaaLastPtrn.set(chdrlifIO.getChdrnum());
			writePtrn5800();
			//ILIFE-3997 -STARTS
			if(chdrpf != null && chdrpf.getNlgflg() != null && chdrpf.getNlgflg() == 'Y')
				writeNLGRec();
			//ILIFE-3997-ENDS
		}
		/* IF WSAA-LEAVE-IN-FORCE      = 'Y'                            */
		/*     PERFORM 6800-END-OF-COVR-PROCESS                         */
		/* END-IF.                                                      */
		a200DelUnderwritting();
	}

protected void controlTotals3280()
	{
		ct08Val++;
		ct09Val = ct09Val.add(payrIO.getOutstamt());
	}

protected void processOverdue3300()
	{
		para3310();
		controlTotals3350();
	}

protected void para3310()
	{
		chdrlifIO.setAplsupr('Y');
		wsaaPayrAplsupr.set("Y");
		chdrlifIO.setAplspfrom(bsscIO.getEffectiveDate().toInt());
		wsaaPayrAplspfrom.set(bsscIO.getEffectiveDate());
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(payzpfRec.getPtdate());
		datcon2rec.intDate2.set(0);
		datcon2rec.frequency.set("DY");
		/*    Once we have processed an overdue record, we want to*/
		/*    indicate this on the contract and payer files so that*/
		/*    the record is not picked up again until the next overdue*/
		/*    action date. This date is calculated by adding the next*/
		/*    non-blank lag time to the paid-to-date. If all the*/
		/*    subsequent lag time fields are blank, record this and*/
		/*    use a default value of 10 days. This will ensure that*/
		/*    T6654 is set up correctly and that a record only gets*/
		/*    processed once for each type of overdue.*/
		for (wsaaSub.set(wsaaType9); !(isGT(wsaaSub, 2)
		|| (setPrecision(wsaaT6654ArrayInner.wsaaT6654Daexpy[wsaaT6654Ix.toInt()][add(wsaaSub, 1).toInt()], 0)
		&& isNE(wsaaT6654ArrayInner.wsaaT6654Daexpy[wsaaT6654Ix.toInt()][add(wsaaSub, 1).toInt()], 0))); wsaaSub.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(wsaaSub, 2)) {
			if (isEQ(wsaaT6654ArrayInner.wsaaT6654NonForfDays[wsaaT6654Ix.toInt()], SPACES)) {
				conlogrec.error.set(f454);
				conlogrec.params.set(payzpfRec.getChdrnum());
				callConlog003();
				datcon2rec.freqFactor.set(10);
			}
			else {
				datcon2rec.freqFactor.set(wsaaT6654ArrayInner.wsaaT6654NonForfDays[wsaaT6654Ix.toInt()]);
			}
		}
		else {
			compute(datcon2rec.freqFactor, 0).set(wsaaT6654ArrayInner.wsaaT6654Daexpy[wsaaT6654Ix.toInt()][add(wsaaSub, 1).toInt()]);
		}
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		chdrlifIO.setAplspto(datcon2rec.intDate2.toInt());
		wsaaPayrAplspto.set(datcon2rec.intDate2);
		writeChdrValidflag15300();
		writePtrn5800();
		//ILIFE-3997 -STARTS
		if(chdrpf != null && chdrpf.getNlgflg() != null && chdrpf.getNlgflg() == 'Y')
			writeNLGRec();
		//ILIFE-3997-ENDS
	}

protected void controlTotals3350()
	{
		controlTotal3350();
		/*EXIT*/
	}

protected void controlTotal3350()
	{
		start3351();
	}

protected void start3351()
	{
		if (isEQ(wsaaT6654SubrIx, 1)){
			ct02Val++;
			ct03Val = ct03Val.add(payrIO.getOutstamt());
		}
		else if (isEQ(wsaaT6654SubrIx, 2)){
			ct04Val++;
			ct05Val = ct05Val.add(payrIO.getOutstamt());
			
		}
		else if (isEQ(wsaaT6654SubrIx, 3)){
			ct06Val++;
			ct07Val = ct07Val.add(payrIO.getOutstamt());
		}
	}

protected void setupOverdueRec3400()
	{
		para3401();
	}

protected void para3401()
	{
		ovrduerec.ovrdueRec.set(SPACES);
		searchT66473450();
		ovrduerec.language.set(bsscIO.getLanguage());
		ovrduerec.chdrcoy.set(payzpfRec.getChdrcoy());
		ovrduerec.chdrnum.set(payzpfRec.getChdrnum());
		ovrduerec.life.set(covrrnlIO.getLife());
		ovrduerec.coverage.set(covrrnlIO.getCoverage());
		ovrduerec.rider.set(covrrnlIO.getRider());
		ovrduerec.planSuffix.set(ZERO);
		ovrduerec.tranno.set(chdrlifIO.getTranno());
		ovrduerec.trancode.set(batcdorrec.trcde.toString());
		ovrduerec.cntcurr.set(payrIO.getCntcurr());
		/* MOVE CHDRLIF-CCDATE         TO OVRD-EFFDATE.                 */
		ovrduerec.effdate.set(wsaaToday);
		ovrduerec.outstamt.set(payrIO.getOutstamt());
		ovrduerec.ptdate.set(payzpfRec.getPtdate());
		ovrduerec.btdate.set(payzpfRec.getBtdate());
		ovrduerec.ovrdueDays.set(wsaaOverdueDays);
		ovrduerec.agntnum.set(chdrlifIO.getAgntnum());
		ovrduerec.cownnum.set(chdrlifIO.getCownnum());
		ovrduerec.trancode.set(bprdIO.getAuthCode());
		ovrduerec.acctyear.set(batcdorrec.actyear);
		ovrduerec.acctmonth.set(batcdorrec.actmonth);
		ovrduerec.batcbrn.set(batcdorrec.branch);
		ovrduerec.batcbatch.set(batcdorrec.batch);
		ovrduerec.user.set(payrIO.getUser());
		/* MOVE BSPR-COMPANY           TO OVRD-COMPANY.                 */
		ovrduerec.company.set(chdrlifIO.getCowncoy());
		ovrduerec.tranDate.set(wsaaToday);
		ovrduerec.tranTime.set(wsaaTime);
		ovrduerec.termid.set(varcom.vrcmTermid);
		ovrduerec.crtable.set(covrrnlIO.getCrtable());
		ovrduerec.pumeth.set(wsaaT5687Pumeth[wsaaT5687Ix.toInt()]);
		ovrduerec.billfreq.set(payrIO.getBillfreq());
		ovrduerec.instprem.set(ZERO);
		ovrduerec.sumins.set(ZERO);
		ovrduerec.crrcd.set(ZERO);
		ovrduerec.newCrrcd.set(ZERO);
		ovrduerec.newSingp.set(ZERO);
		ovrduerec.newAnb.set(ZERO);
		ovrduerec.newPua.set(ZERO);
		ovrduerec.newInstprem.set(ZERO);
		ovrduerec.newSumins.set(ZERO);
		ovrduerec.premCessDate.set(ZERO);
		ovrduerec.cnttype.set(chdrlifIO.getCnttype());
		ovrduerec.occdate.set(chdrlifIO.getOccdate());
		ovrduerec.polsum.set(chdrlifIO.getPolsum());
		/*    Call the CALCFEE subroutine to calculate OVRD-PUPFEE.*/
		callProgram(Calcfee.class, ovrduerec.ovrdueRec);
		if (isNE(ovrduerec.statuz, varcom.oK)) {
			syserrrec.params.set(ovrduerec.ovrdueRec);
			syserrrec.statuz.set(ovrduerec.statuz);
			fatalError600();
		}
		ovrduerec.newRiskCessDate.set(varcom.vrcmMaxDate);
		ovrduerec.newPremCessDate.set(varcom.vrcmMaxDate);
		ovrduerec.newBonusDate.set(varcom.vrcmMaxDate);
		ovrduerec.newRerateDate.set(varcom.vrcmMaxDate);
		ovrduerec.cvRefund.set(ZERO);
		ovrduerec.surrenderValue.set(ZERO);
		ovrduerec.etiYears.set(ZERO);
		ovrduerec.etiDays.set(ZERO);
	}

protected void searchT66473450()
	{
		/*T6647*/
		/*    Search table T6647 to find correct dated item.*/
		for (wsaaT6647Ix.set(wsaaT6647IxMax); !(isEQ(wsaaT6647Ix, 0)
		|| (isEQ(chdrlifIO.getCnttype(), wsaaT6647Cnttype[wsaaT6647Ix.toInt()])
		&& isGTE(chdrlifIO.getOccdate(), wsaaT6647Itmfrm[wsaaT6647Ix.toInt()]))); wsaaT6647Ix.add(-1))
{
			/*CONTINUE_STMT*/
		}
		if (isEQ(wsaaT6647Ix, 0)) {
			ovrduerec.aloind.set(SPACES);
			ovrduerec.efdcode.set(SPACES);
			ovrduerec.procSeqNo.set(ZERO);
		}
		else {
			ovrduerec.aloind.set(wsaaT6647Aloind[wsaaT6647Ix.toInt()]);
			ovrduerec.efdcode.set(wsaaT6647Efdcode[wsaaT6647Ix.toInt()]);
			ovrduerec.procSeqNo.set(wsaaT6647SeqNo[wsaaT6647Ix.toInt()]);
		}
		/*EXIT*/
	}

	protected void commit3500() {
		commitControlTotals();
		chdrpfDAO.updateInvalidChdrRecord(this.chdrUpdateList);
		chdrUpdateList.clear();
		chdrpfDAO.insertChdrValidRecord(this.chdrInsertList);
		chdrInsertList.clear();
		chdrpfDAO.updateChdrLastInserted(chdrUpdateInsertedList);
		chdrUpdateInsertedList.clear();
		
		payrpfDAO.updatePayrRecord(payrpfUpdateList, bsscIO.getEffectiveDate().toInt());
		payrpfUpdateList.clear();		
		payrpfDAO.insertPayrpfList(payrpfInsertList);
		payrpfInsertList.clear();
		payrpfDAO.updatePayrPstCde(payrUpdatePstcdeList);
		payrUpdatePstcdeList.clear();
		
		
		covrpfDAO.updateCovrValidFlag(covrpfInsertList);
		covrpfInsertList.clear();
		
		covrpfDAO.insertCovrpfList(covrpfInsertList2);
		covrpfInsertList2.clear();
		
		ptrnpfDAO.insertPtrnPF(ptrnpfList);
		ptrnpfList.clear();
	}

	
	private void commitControlTotals() {
		contotrec.totno.set(controlTotalsInner.ct01);
		contotrec.totval.set(ct01Val);
		callContot001();
		ct01Val = 0;
		
		contotrec.totno.set(controlTotalsInner.ct02);
		contotrec.totval.set(ct02Val);
		callContot001();
		ct02Val=0;
		
		contotrec.totno.set(controlTotalsInner.ct03);
		contotrec.totval.set(ct03Val);
		callContot001();
		ct03Val=BigDecimal.ZERO;
		
		contotrec.totno.set(controlTotalsInner.ct04);
		contotrec.totval.set(ct04Val);
		callContot001();
		ct04Val=0;
		
		contotrec.totno.set(controlTotalsInner.ct05);
		contotrec.totval.set(ct05Val);
		callContot001();
		ct05Val=BigDecimal.ZERO;
		
		contotrec.totno.set(controlTotalsInner.ct06);
		contotrec.totval.set(ct06Val);
		callContot001();
		ct06Val=0;
		
		contotrec.totno.set(controlTotalsInner.ct07);
		contotrec.totval.set(ct07Val);
		callContot001();
		ct07Val=BigDecimal.ZERO;
		
		contotrec.totno.set(controlTotalsInner.ct08);
		contotrec.totval.set(ct08Val);
		callContot001();
		ct08Val=0;
		
		contotrec.totno.set(controlTotalsInner.ct09);
		contotrec.totval.set(ct09Val);
		callContot001();
		ct09Val=BigDecimal.ZERO;
		
		contotrec.totno.set(controlTotalsInner.ct10);
		contotrec.totval.set(ct10Val);
		callContot001();
		ct10Val=0;
		
		contotrec.totno.set(controlTotalsInner.ct11);
		contotrec.totval.set(ct11Val);
		callContot001();
		ct11Val=BigDecimal.ZERO;
		
		contotrec.totno.set(controlTotalsInner.ct12);
		contotrec.totval.set(ct12Val);
		callContot001();
		ct12Val=0;
	}


protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    Place any additional rollback processing in here.*/
		/*EXIT*/
	}

	protected void readChdr5000() {
		chdrlifIO = null;
		if (chdrpfMap != null && chdrpfMap.containsKey(payzpfRec.getChdrnum())) {
			for (Chdrpf c : chdrpfMap.get(payzpfRec.getChdrnum())) {
				if (c.getChdrcoy().toString().equals(payzpfRec.getChdrcoy()) && c.getValidflag() == '1') {
					chdrlifIO = c;
				}
			}
		}

		if (chdrlifIO == null) {
			syserrrec.params.set(payzpfRec.getChdrnum());
			fatalError600();
		}

		wsysChdrnum.set(payzpfRec.getChdrnum());
		wsysChdrcoy.set(payzpfRec.getChdrcoy());

		wsysBillcd.set(chdrlifIO.getBillcd());
	}

protected void validateChdr5100()
	{
		/*PARA*/
		/*    Validate the new contract status against T5679.*/
		wsaaValidContract.set("N");
		for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12)
		|| validContract.isTrue()); wsaaT5679Ix.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Ix.toInt()], chdrlifIO.getStatcode())) {
				for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12)
				|| validContract.isTrue()); wsaaT5679Ix.add(1)){
					if (isEQ(t5679rec.cnPremStat[wsaaT5679Ix.toInt()], chdrlifIO.getPstcde())) {
						wsaaValidContract.set("Y");
					}
				}
			}
		}
		wsaaPrevChdrnum.set(payzpfRec.getChdrnum());
		/*EXIT*/
	}

protected void rewriteChdr5200()
	{
		Chdrpf  chdr = new Chdrpf(chdrlifIO);
		chdr.setCurrto(payzpfRec.getPtdate());
		chdrUpdateList.add(chdr);
	}

protected void writeChdrValidflag15300()
	{
		chdrlifIO.setValidflag('1');
		chdrlifIO.setCurrto(varcom.vrcmMaxDate.toInt());
		chdrlifIO.setCurrfrom(payzpfRec.getPtdate());
		chdrInsertList.add(chdrlifIO);
	}

protected void writeChdrValidflag25400()
	{
		Chdrpf chdrpfInvalid = new Chdrpf(chdrlifIO);
		chdrpfInvalid.setValidflag('2');
		chdrpfInvalid.setCurrto(payzpfRec.getPtdate());
		chdrpfInvalid.setUniqueNumber(chdrlifIO.getUniqueNumber());
		chdrUpdateList.add(chdrpfInvalid);
	}

	protected void readPayr5500() {
		payrIO = null;
		if (payrpfMap != null && payrpfMap.containsKey(payzpfRec.getChdrnum())) {
			for (Payrpf c : payrpfMap.get(payzpfRec.getChdrnum())) {
				if (payzpfRec.getChdrcoy().equals(c.getChdrcoy())
						&& c.getPayrseqno() == payzpfRec.getPayrseqno()
						&& "1".equals(c.getValidflag())) {
					payrIO = c;
				}
			}
		}
	}

//protected void readhPayr5500a()
//	{
//		/*A-PARA*/
//		payrIO.setChdrcoy(payzpfRec.chdrcoy);
//		payrIO.setChdrnum(payzpfRec.chdrnum);
//		payrIO.setPayrseqno(payzpfRec.payrseqno);
//		payrIO.setValidflag("1");
//		payrIO.setFunction(varcom.readh);
//		payrIO.setFormat("PAYRREC");
//		SmartFileCode.execute(appVars, payrIO);
//		if (isNE(payrIO.getStatuz(), varcom.oK)) {
//			syserrrec.params.set(payrIO.getParams());
//			fatalError600();
//		}
//		/*A-EXIT*/
//	}

	protected void createPayrValidflag15600() {
		/*    Write the new PAYR with validflag = 1 and all fields*/
		/*    correctly updated.*/
		//ILIFE-4323 start by dpuhawan
		/*
		payrIO.setValidflag("1");
		payrIO.setTranno(chdrlifIO.getTranno());
		payrIO.setAplsupr(wsaaPayrAplsupr.toString());
		payrIO.setAplspfrom(wsaaPayrAplspfrom.toInt());
		payrIO.setAplspto(wsaaPayrAplspto.toInt());
		this.payrpfInsertList.add(payrIO);
		*/
		Payrpf payrInsert = new Payrpf(payrIO);
		payrInsert.setValidflag("1");
		payrInsert.setTranno(chdrlifIO.getTranno());
		payrInsert.setAplsupr(wsaaPayrAplsupr.toString());
		payrInsert.setAplspfrom(wsaaPayrAplspfrom.toInt());
		payrInsert.setAplspto(wsaaPayrAplspto.toInt());
		this.payrpfInsertList.add(payrInsert);
		//ILIFE-4323 end
	}

protected void rewritePayrValidflag25700()
	{
		/*PARA*/
		/*    Rewrite the PAYR with validflag = 2*/
		payrIO.setValidflag("2");
		this.payrpfUpdateList.add(payrIO);
	}

	protected void writePtrn5800() {
		ptrnIO = new Ptrnpf();
		ptrnIO.setBatcpfx(batcdorrec.prefix.toString());
		ptrnIO.setBatccoy(batcdorrec.company.toString());
		ptrnIO.setBatcbrn(batcdorrec.branch.toString());
		ptrnIO.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnIO.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnIO.setBatctrcde(batcdorrec.trcde.toString());
		ptrnIO.setBatcbatch(batcdorrec.batch.toString());
		ptrnIO.setChdrpfx(chdrlifIO.getChdrpfx());
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy().toString());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setValidflag("1");
		ptrnIO.setTrdt(wsaaTransactionDate.toInt());//IJS-371
		ptrnIO.setTrtm(wsaaTime.toInt());
		ptrnIO.setPtrneff(payzpfRec.getPtdate());
		ptrnIO.setTermid(varcom.vrcmTermid.toString());
		ptrnIO.setUserT(payrIO.getUser());
		ptrnIO.setDatesub(bsscIO.getEffectiveDate().toInt());
		
		ptrnpfList.add(ptrnIO);
	}
//ILIFE-3997-STARTS
	protected void writeNLGRec() {
		nlgtpfList = nlgtpfDAO.readNlgtpf(ptrnIO.getChdrcoy(), ptrnIO.getChdrnum());
		for (Nlgtpf nlgtpf : nlgtpfList) {
			if (("B521".equals(nlgtpf.getBatctrcde()) && isEQ(nlgtpf.getEffdate(), ptrnIO.getPtrneff())
					&& nlgtpf.getAmnt04().compareTo(BigDecimal.ZERO)!=0)
					) {
				return;				
			}
		}
		nlgcalcrec.chdrcoy.set(ptrnIO.getChdrcoy());
		nlgcalcrec.chdrnum.set(ptrnIO.getChdrnum());
		nlgcalcrec.effdate.set(ptrnIO.getPtrneff());
		nlgcalcrec.batcactyr.set(ptrnIO.getBatcactyr());
		nlgcalcrec.batcactmn.set(ptrnIO.getBatcactmn());
		nlgcalcrec.batctrcde.set(ptrnIO.getBatctrcde());
		nlgcalcrec.billfreq.set(chdrlifIO.getBillfreq());
		nlgcalcrec.cnttype.set(chdrlifIO.getCnttype());
		nlgcalcrec.language.set(bsscIO.getLanguage());
		nlgcalcrec.frmdate.set(chdrlifIO.getOccdate());
		nlgcalcrec.todate.set(varcom.vrcmMaxDate);
		nlgcalcrec.occdate.set(chdrlifIO.getOccdate());
		nlgcalcrec.ptdate.set(chdrlifIO.getPtdate());
		nlgcalcrec.billfreq.set(chdrlifIO.getBillfreq());
		nlgcalcrec.inputAmt.set(payrIO.getSinstamt01());
		compute(nlgcalcrec.inputAmt, 0).set(mult(-1, nlgcalcrec.inputAmt));
		nlgcalcrec.ovduePrem.set(payrIO.getSinstamt01());
		compute(nlgcalcrec.ovduePrem, 0).set(mult(-1, nlgcalcrec.ovduePrem));
		nlgcalcrec.function.set("OVDUE");
		callProgram(Nlgcalc.class, nlgcalcrec.nlgcalcRec);
		if (isNE(nlgcalcrec.status, varcom.oK)) {
			syserrrec.statuz.set(batcuprec.statuz);
			fatalError600();
		}
	}
	//ILIFE-3997-ENDS
	protected void updatePayr5900() {
		//ILIFE-4323 start by dpuhawan
		/*
		payrIO.setValidflag("2");
		payrpfUpdateList.add(payrIO);
		*/
		Payrpf payrUpdate = new Payrpf(payrIO);
		payrUpdate.setValidflag("2");
		payrUpdate.setUniqueNumber(payrIO.getUniqueNumber());
		this.payrpfUpdateList.add(payrUpdate);
		//ILIFE-4323 end
		
		/*payrIO.setValidflag("1");
		payrIO.setTranno(chdrlifIO.getTranno());
		payrIO.setEffdate(payzpfRec.getPtdate());
		payrpfInsertList.add(payrIO);*/
		
		//Added by Sheenam for ILIFE-4731
		Payrpf payrIOInsert = new Payrpf(payrIO);
		payrIOInsert.setValidflag("1");
		payrIOInsert.setTranno(chdrlifIO.getTranno());
		payrIOInsert.setEffdate(payzpfRec.getPtdate());
		payrpfInsertList.add(payrIOInsert);
		
	}

	protected void callCovrrnlio6000() {
		if (isNE(covrrnlIO.getValidflag(), vflagcpy.inForceVal)) {
			/*     PERFORM 6000A-CHECK-FOR-LOCK                     <V65L19>*/
			goNext = true;
			return ;
		}
		validateCoverageStatus6050();
		if (!validCoverage.isTrue()) {
			//loadCovrStatuses6020();
			loadCovrStatuses();
			goNext = true;
			return ;
		}
		searchT6597NonForfeit6100();
		if (isEQ(wsaaT5687NonForMethod[wsaaT5687Ix.toInt()], SPACES)) {
			/*     PERFORM 6000A-CHECK-FOR-LOCK                     <V65L19>*/
			goNext = true;
			return ;
		}
		wsaaCovrrnlStatiiSet.set(SPACES);
		wsaaCovrrnlPstat.set(SPACES);
		wsaaCovrrnlStat.set(SPACES);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 3) || (isLTE(wsaaDurationInForce,
				wsaaT6597ArrayInner.wsaaT6597Durmnth[wsaaT6597Ix.toInt()][wsaaSub.toInt()]) 
				&& isNE(wsaaT6597ArrayInner.wsaaT6597Premsub[wsaaT6597Ix.toInt()][wsaaSub.toInt()], SPACES))); wsaaSub.add(1)) {
			/*CONTINUE_STMT*/
		}
		if (isGT(wsaaSub, 3)) {
			//loadCovrStatuses6020();
			loadCovrStatuses();
			/*     PERFORM 6000A-CHECK-FOR-LOCK                     <V65L19>*/
			//covrrnlIO.setFunction(varcom.nextr);
			goNext = true;
			return ;
			/*****     NEXT SENTENCE                                            */
		}
		checkT65976200();
		if (skipContract.isTrue()) {
			wsaaEndOfCovr.set("Y");
			/*     PERFORM 6000A-CHECK-FOR-LOCK                     <V65L19>*/
			//covrrnlIO.setFunction(varcom.nextr);
			goNext = true;
			return ;
		}
		if (isEQ(ovrduerec.statuz, "OMIT")) {
			//covrrnlIO.setFunction(varcom.nextr);
			goNext = true;
			return ;
		}
		writeCovrHistory6300();
		if (isEQ(wsaaCovrrnlStatiiSet, "Y")) {
			covrrnlIO.setPstatcode(wsaaCovrrnlPstat.toString());
			covrrnlIO.setStatcode(wsaaCovrrnlStat.toString());
		}
		//loadCovrStatuses6020();
		loadCovrStatuses();
		writeCovrValidflag16400();
		//covrrnlIO.setFunction(varcom.nextr);
	}

protected void checkForLock6000a()
	{
		/*A-START*/
		/** If key break is detected, program is about to exit the loop.    */
		/** However, the previous record read is still being locked. To     */
		/** avoid locking problem, rewrite the record to release the lock   */
		/** before exiting the current loop.                                */
		/***** IF COVRRNL-CHDRNUM          NOT = CHDRNUM            <V65L19>*/
		/***** OR COVRRNL-CHDRCOY          NOT = CHDRCOY            <V65L19>*/
		/*****     MOVE REWRT              TO COVRRNL-FUNCTION      <V65L19>*/
		/*****     CALL 'COVRRNLIO'        USING COVRRNL-PARAMS     <V65L19>*/
		/*****                                                      <V65L19>*/
		/*****     IF  COVRRNL-STATUZ          NOT = O-K            <V65L19>*/
		/*****         MOVE COVRRNL-PARAMS     TO SYSR-PARAMS       <V65L19>*/
		/*****         PERFORM 600-FATAL-ERROR                      <V65L19>*/
		/*****     END-IF                                           <V65L19>*/
		/***** END-IF.                                              <V65L19>*/
		/*****                                                      <V65L19>*/
		/**6000A-EXIT.                                              <V65L19>*/
		/***** EXIT.                                                <V65L19>*/
	}

protected void loadCovrStatuses6020()
	{
		/*START*/
		/*  If this COVR's statuses are to be loaded, ensure we have not   */
		/*  exceeded the number of occurences in WSAA-COVR-REC.            */
		if (isGT(wsaaCovrIx, 48)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(covrrnlIO.getChdrnum());
			fatalError600();
		}
		/*  If the OVRD-STATUZ is 'OMIT', skip the loading of COVR status, */
		/*  this is to avoid incorrect status been set up on the Contract. */
		if (isEQ(ovrduerec.statuz, "OMIT")) {
			return ;
		}
		wsaaCovrPrst[wsaaCovrIx.toInt()].set(covrrnlIO.getPstatcode());
		wsaaCovrRist[wsaaCovrIx.toInt()].set(covrrnlIO.getStatcode());
		wsaaCovrIx.add(1);
		/*EXIT*/
	}

protected void loadCovrStatuses()
{
	/*START*/
	/*  If this COVR's statuses are to be loaded, ensure we have not   */
	/*  exceeded the number of occurences in WSAA-COVR-REC.            */
	if (isGT(wsaaCovrIx, 48)) {
		syserrrec.statuz.set(h791);
		syserrrec.params.set(covrrnlIO.getChdrnum());
		fatalError600();
	}
	/*  If the OVRD-STATUZ is 'OMIT', skip the loading of COVR status, */
	/*  this is to avoid incorrect status been set up on the Contract. */
	if (isEQ(ovrduerec.statuz, "OMIT")) {
		return ;
	}
	covrPremStats.add(covrrnlIO.getPstatcode());
	covrRiskStats.add(covrrnlIO.getStatcode());
	wsaaCovrIx.add(1);
}

protected void validateCoverageStatus6050()
	{
		/*START*/
		wsaaValidCoverage.set("N");
		for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12)
		|| validCoverage.isTrue()); wsaaT5679Ix.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Ix.toInt()], covrrnlIO.getStatcode())) {
				for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12)
				|| validCoverage.isTrue()); wsaaT5679Ix.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaT5679Ix.toInt()], covrrnlIO.getPstatcode())) {
						wsaaValidCoverage.set("Y");
					}
				}
			}
		}
		/*EXIT*/
	}

protected void searchT6597NonForfeit6100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					t65976110();
				case searchT65976120:
					searchT65976120();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void t65976110()
	{
		/*    If HPAD contains a non-forfeiture option, read TH584 with    */
		/*    this option and the component code, then use this client     */
		/*    defined method for non-forfeiture processing than T5687's.   */
		if (isNE(payzpfRec.getZnfopt(), SPACES)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(bsprIO.getCompany());
			itemIO.setItemtabl(th584);
			itemIO.getItemitem().setSub1String(1, 3, payzpfRec.getZnfopt());
			itemIO.getItemitem().setSub1String(4, 4, covrrnlIO.getCrtable());
			itemIO.setFormat(formatsInner.itemrec);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			else {
				if (isEQ(itemIO.getStatuz(), varcom.oK)) {
					th584rec.th584Rec.set(itemIO.getGenarea());
					if (isEQ(th584rec.nonForfeitMethod, SPACES)) {
						/*             MOVE IVRM       TO SYSR-SYSERR-STATUZ    <Q83>   */
						/*             PERFORM 600-FATAL-ERROR                  <Q83>   */
						/*NEXT_SENTENCE*/
					}
					else {
						wsaaT5687Ix.set(1);
						/*              MOVE 1          TO WSAA-T5687-IX         <S19FIX>*/
						wsaaT5687NonForMethod[wsaaT5687Ix.toInt()].set(th584rec.nonForfeitMethod);
						goTo(GotoLabel.searchT65976120);
					}
				}
			}
		}
		/*    Search T5687 for correct dated non-forfeiture method.*/
		for (wsaaT5687Ix.set(wsaaT5687IxMax); !(isEQ(wsaaT5687Ix, 0)
		|| (isEQ(wsaaT5687Crtable[wsaaT5687Ix.toInt()], covrrnlIO.getCrtable())
		&& isGTE(wsaaToday, wsaaT5687Itmfrm[wsaaT5687Ix.toInt()]) 
		&& isLTE(wsaaToday, wsaaT5687ItmTo[wsaaT5687Ix.toInt()]))); wsaaT5687Ix.add(-1))
{
			/*CONTINUE_STMT*/
		}
		if (isEQ(wsaaT5687Ix, 0)) {
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(bsprIO.getCompany());
			itemIO.setItemtabl(t5687);
			itemIO.setItemitem(covrrnlIO.getCrtable());
			itemIO.setFunction(varcom.readr);
			itemIO.setStatuz(f781);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void searchT65976120()
	{
		/*    Search T6597 using T5687-NON-FORFEIT-METHOD as the key.*/
		if (isNE(wsaaT5687NonForMethod[wsaaT5687Ix.toInt()], SPACES)) {
			for (wsaaT6597Ix.set(wsaaT6597IxMax); !(isEQ(wsaaT6597Ix, 0)
			|| (isEQ(wsaaT5687NonForMethod[wsaaT5687Ix.toInt()], wsaaT6597ArrayInner.wsaaT6597Method[wsaaT6597Ix.toInt()])
			&& isGTE(wsaaToday, wsaaT6597ArrayInner.wsaaT6597Itmfrm[wsaaT6597Ix.toInt()]))); wsaaT6597Ix.add(-1))
{
				/*CONTINUE_STMT*/
			}
		}
		if (isEQ(wsaaT6597Ix, 0)) {
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(bsprIO.getCompany());
			itemIO.setItemtabl(t6597);
			itemIO.setItemitem(wsaaT5687NonForMethod[wsaaT5687Ix.toInt()]);
			itemIO.setFunction(varcom.readr);
			itemIO.setStatuz(f781);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void checkT65976200()
	{
		t65976210();
	}

protected void t65976210()
	{
	
	if(chdrpf != null && chdrpf.getNlgflg() != null && chdrpf.getNlgflg() == 'Y')
		writeNLGRec();
		setupOverdueRec3400();

		readChdrpf();
		
		ovrduerec.planSuffix.set(covrrnlIO.getPlanSuffix());
		ovrduerec.instprem.set(covrrnlIO.getInstprem());
		ovrduerec.sumins.set(covrrnlIO.getSumins());
		ovrduerec.crrcd.set(covrrnlIO.getCrrcd());
		ovrduerec.premCessDate.set(covrrnlIO.getPremCessDate());
		ovrduerec.riskCessDate.set(covrrnlIO.getRiskCessDate());
		ovrduerec.newInstprem.set(covrrnlIO.getInstprem());
		//readChdr5000();
		//rewriteChdr5200();
		if(prmhldtrad) {	//ILIFE-8179
			Itempf itempf;
			itempf = ta524Map.get(chdrlifIO.getCnttype());
			if(!isAiaAusDirectDebit && itempf != null && itempf.getItmfrm().compareTo(bsscIO.getEffectiveDate().getbigdata()) <= 0 && itempf.getItmto().compareTo(bsscIO.getEffectiveDate().getbigdata()) >= 0)
				ovrduerec.statuz.set("LAPS");
			else
				callProgram(wsaaT6597ArrayInner.wsaaT6597Premsub[wsaaT6597Ix.toInt()][wsaaSub.toInt()], ovrduerec.ovrdueRec);
		}
		else
			callProgram(wsaaT6597ArrayInner.wsaaT6597Premsub[wsaaT6597Ix.toInt()][wsaaSub.toInt()], ovrduerec.ovrdueRec);
		if (isEQ(ovrduerec.statuz, varcom.oK)) {
			if (isNE(wsaaT6597ArrayInner.wsaaT6597Cpstat[wsaaT6597Ix.toInt()][wsaaSub.toInt()], SPACES)
			&& isNE(wsaaT6597ArrayInner.wsaaT6597Crstat[wsaaT6597Ix.toInt()][wsaaSub.toInt()], SPACES) ) {
				if(!updateRequired && isLTE(nlgcalcrec.nlgBalance, 0))
				{
					wsaaCovrrnlStatiiSet.set("Y");
					ovrduerec.pstatcode.set(wsaaT6597ArrayInner.wsaaT6597Cpstat[wsaaT6597Ix.toInt()][wsaaSub.toInt()]);
					wsaaCovrrnlPstat.set(wsaaT6597ArrayInner.wsaaT6597Cpstat[wsaaT6597Ix.toInt()][wsaaSub.toInt()]);
					ovrduerec.statcode.set(wsaaT6597ArrayInner.wsaaT6597Crstat[wsaaT6597Ix.toInt()][wsaaSub.toInt()]);
					wsaaCovrrnlStat.set(wsaaT6597ArrayInner.wsaaT6597Crstat[wsaaT6597Ix.toInt()][wsaaSub.toInt()]);
				}
			}
		}
		else {
			if (isEQ(ovrduerec.statuz, "LAPS") ) {
				wsaaSub.set(4);
				callProgram(wsaaT6597ArrayInner.wsaaT6597Premsub[wsaaT6597Ix.toInt()][wsaaSub.toInt()], ovrduerec.ovrdueRec);
				
					wsaaCovrrnlStatiiSet.set("Y");
					ovrduerec.pstatcode.set(wsaaT6597ArrayInner.wsaaT6597Cpstat[wsaaT6597Ix.toInt()][wsaaSub.toInt()]);
					wsaaCovrrnlPstat.set(wsaaT6597ArrayInner.wsaaT6597Cpstat[wsaaT6597Ix.toInt()][wsaaSub.toInt()]);
					ovrduerec.statcode.set(wsaaT6597ArrayInner.wsaaT6597Crstat[wsaaT6597Ix.toInt()][wsaaSub.toInt()]);
					wsaaCovrrnlStat.set(wsaaT6597ArrayInner.wsaaT6597Crstat[wsaaT6597Ix.toInt()][wsaaSub.toInt()]);
				
			}
			else {
				if (isEQ(ovrduerec.statuz, "SKIP")) {
					wsaaSkipContract.set("Y");
				}
				else {
					if (isNE(ovrduerec.statuz, "OMIT")) {
						syserrrec.subrname.set(wsaaT6597ArrayInner.wsaaT6597Premsub[wsaaT6597Ix.toInt()][wsaaSub.toInt()]);
						syserrrec.params.set(ovrduerec.ovrdueRec);
						syserrrec.statuz.set(ovrduerec.statuz);
						fatalError600();
						/**        ELSE                                             <LA3910>*/
						/**            MOVE 'Y'            TO WSAA-SKIP-CONTRACT    <LA3910>*/
					}
				}
			}
		}
		readChdr5000();
		wsaaChdrOk = "Y";
		if (isNE(chdrlifIO.getChdrnum(), wsaaLastPtrn)) {
			wsaaLastPtrn.set(chdrlifIO.getChdrnum());
			/*        PERFORM 5800-WRITE-PTRN                          <C16N>  */
			if (!skipContract.isTrue()) {
				chdrlifIO.setTranno(chdrlifIO.getTranno()+1);  //ILIFE-4323 by dpuhawan
				writePtrn5800();
			}
		}
	}

protected void writeCovrHistory6300()
	{
		/*HIST*/
		/*    Rewrite the COVR at this point with a valid flag '2',*/
		/*    for historical purposes.*/
		/*    MOVE BSSC-EFFECTIVE-DATE    TO COVRRNL-CURRTO.               */
		Covrpf covrrnlUpdate = new Covrpf(covrrnlIO);
		covrrnlUpdate.setCurrto(payzpfRec.getPtdate());
		covrrnlUpdate.setUniqueNumber(covrrnlIO.getUniqueNumber());
		covrpfInsertList.add(covrrnlUpdate);
	}

	protected void writeCovrValidflag16400() {
		/* <C16N> */
		/*                                                         <C16N>  */
		/*    Write a valid flag '1' record for COVR*/
		if (isNE(ovrduerec.newRiskCessDate, varcom.vrcmMaxDate)
		&& isNE(ovrduerec.newRiskCessDate, ZERO)) {
			covrrnlIO.setRiskCessDate(ovrduerec.newRiskCessDate.toInt());
		}
		if (isNE(ovrduerec.newPremCessDate, varcom.vrcmMaxDate)
		&& isNE(ovrduerec.newPremCessDate, ZERO)) {
			covrrnlIO.setPremCessDate(ovrduerec.newPremCessDate.toInt());
		}
		if (isNE(ovrduerec.newRerateDate, varcom.vrcmMaxDate)
		&& isNE(ovrduerec.newRerateDate, ZERO)) {
			covrrnlIO.setRerateDate(ovrduerec.newRerateDate.toInt());
		}
		if (isNE(ovrduerec.newSumins, ZERO)) {
			covrrnlIO.setSumins(ovrduerec.newSumins.getbigdata());
			covrrnlIO.setVarSumInsured(BigDecimal.ZERO);
		} else if (isGT(covrrnlIO.getVarSumInsured(), ZERO)) {
			setPrecision(covrrnlIO.getVarSumInsured(), 2);
			covrrnlIO.setVarSumInsured(
					sub(covrrnlIO.getVarSumInsured(), covrrnlIO.getSumins()).getbigdata());
			/** MOVE ZERO TO COVRRNL-SUMINS <C16N> */
		}
		/* MOVE COVRRNL-SUMINS         TO ZRDP-AMOUNT-IN.               */
		/* MOVE PAYR-CNTCURR           TO ZRDP-CURRENCY.                */
		/* PERFORM B000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO COVRRNL-SUMINS.               */
		if (isNE(covrrnlIO.getSumins(), 0)) {
			zrdecplrec.amountIn.set(covrrnlIO.getSumins());
			zrdecplrec.currency.set(payrIO.getCntcurr());
			b000CallRounding();
			covrrnlIO.setSumins(zrdecplrec.amountOut.getbigdata());
		}
		if (isNE(ovrduerec.newCrrcd, varcom.vrcmMaxDate)
		&& isNE(ovrduerec.newCrrcd, ZERO)) {
			covrrnlIO.setCrrcd(ovrduerec.newCrrcd.toInt());
		}
		if (isNE(ovrduerec.newSingp, ZERO)) {
			covrrnlIO.setSingp(ovrduerec.newSingp.getbigdata());
		}
		if (isNE(ovrduerec.newAnb, ZERO)) {
			covrrnlIO.setAnbAtCcd(ovrduerec.newAnb.toInt());
		}
		covrrnlIO.setInstprem(ovrduerec.newInstprem.getbigdata());
		/* MOVE COVRRNL-INSTPREM       TO ZRDP-AMOUNT-IN.               */
		/* MOVE PAYR-CNTCURR           TO ZRDP-CURRENCY.                */
		/* PERFORM B000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO COVRRNL-INSTPREM.             */
		if (isNE(covrrnlIO.getInstprem(), 0)) {
			zrdecplrec.amountIn.set(covrrnlIO.getInstprem());
			zrdecplrec.currency.set(payrIO.getCntcurr());
			b000CallRounding();
			covrrnlIO.setInstprem(zrdecplrec.amountOut.getbigdata());
		}
		if (isNE(ovrduerec.newPua, ZERO)) {
			setPrecision(covrrnlIO.getVarSumInsured(), 2);
			covrrnlIO.setVarSumInsured(add(covrrnlIO.getVarSumInsured(),
					ovrduerec.newPua).getbigdata());
		}
		/* MOVE COVRRNL-VAR-SUM-INSURED TO ZRDP-AMOUNT-IN.              */
		/* MOVE PAYR-CNTCURR            TO ZRDP-CURRENCY.               */
		/* PERFORM B000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO COVRRNL-VAR-SUM-INSURED.     */
		if (isNE(covrrnlIO.getVarSumInsured(), 0)) {
			zrdecplrec.amountIn.set(covrrnlIO.getVarSumInsured());
			zrdecplrec.currency.set(payrIO.getCntcurr());
			b000CallRounding();
			covrrnlIO.setVarSumInsured(zrdecplrec.amountOut.getbigdata());
		}
		if (isNE(ovrduerec.newBonusDate, varcom.vrcmMaxDate)
		&& isNE(ovrduerec.newBonusDate, ZERO)) {
			covrrnlIO.setUnitStatementDate(ovrduerec.newBonusDate.toInt());
		}
		covrrnlIO.setValidflag("1");
		/* MOVE MAXDATE                TO COVRRNL-CURRTO.               */
		covrrnlIO.setCurrto(varcom.vrcmMaxDate.toInt());
		/*    MOVE BSSC-EFFECTIVE-DATE    TO COVRRNL-CURRFROM.             */
		covrrnlIO.setCurrfrom(chdrlifIO.getPtdate());
		covrrnlIO.setTranno(chdrlifIO.getTranno());
		covrrnlIO.setTermid(wsaaTermid.toString());
		covrrnlIO.setTransactionDate(wsaaTransactionDate.toInt());
		covrrnlIO.setTransactionTime(wsaaTransactionTime.toInt());
		covrrnlIO.setUser(wsaaUser.toInt());
		/*    Lapse all the HPUA records and update COVRRNL-VARSI before   */
		/*    writing the new COVRRNL.                                     */
		lapsePua6600();
		
		covrpfInsertList2.add(covrrnlIO);
	}

	/**
	* <pre>
	*6500-VALIDATE-UPDATE-STATII SECTION.
	*6510-STATII.
	*    Validate the new coverage status against T5679.
	**** MOVE 'N'                    TO WSAA-VALID-STATCODE
	****                                WSAA-VALID-PSTATCODE.
	**** IF COVRRNL-RIDER            = SPACE
	**** OR COVRRNL-RIDER            = '00'
	****     PERFORM VARYING WSAA-T5679-IX
	****        FROM 1 BY 1
	****        UNTIL WSAA-T5679-IX > 12
	****        OR VALID-STATCODE
	****        IF T5679-COV-RISK-STAT(WSAA-T5679-IX) = OVRD-STATCODE
	****            MOVE 'Y'        TO WSAA-VALID-STATCODE
	****            PERFORM VARYING WSAA-T5679-IX
	****                FROM 1 BY 1
	****                UNTIL WSAA-T5679-IX > 12
	****                OR VALID-PSTATCODE
	****                IF T5679-COV-PREM-STAT (WSAA-T5679-IX)
	****                             = OVRD-PSTATCODE
	****                    MOVE 'Y' TO WSAA-PSTATCODE           <006>
	****                    MOVE 'Y' TO WSAA-VALID-PSTATCODE     <006>
	****                END-IF
	****            END-PERFORM
	****        END-IF
	****     END-PERFORM
	**** END-IF.
	**** IF  VALID-STATCODE
	**** AND VALID-PSTATCODE
	****     GO TO 6550-SKIP-VALIDATION
	**** END-IF.
	*    Validate the new rider status against T5679.
	**** IF  COVRRNL-RIDER           NOT = SPACE
	**** AND COVRRNL-RIDER           NOT = '00'
	****     PERFORM VARYING WSAA-T5679-IX
	****         FROM 1 BY 1
	****         UNTIL WSAA-T5679-IX > 12
	****         IF T5679-RID-RISK-STAT (WSAA-T5679-IX)
	****                             = OVRD-STATCODE
	****             MOVE 'Y'        TO WSAA-VALID-STATCODE
	****             PERFORM VARYING WSAA-T5679-IX
	****                 FROM 1 BY 1
	****                 UNTIL WSAA-T5679-IX > 12
	****                 OR VALID-PSTATCODE
	****                 IF T5679-RID-PREM-STAT (WSAA-T5679-IX)
	****                             = OVRD-PSTATCODE
	****                    MOVE 'Y' TO WSAA-VALID-PSTATCODE
	****                 END-IF
	****             END-PERFORM
	****         END-IF
	****     END-PERFORM
	**** END-IF.
	*6550-SKIP-VALIDATION.
	**** IF  OVRD-STATCODE           = T5679-SET-CN-RISK-STAT
	**** AND OVRD-PSTATCODE          = T5679-SET-CN-PREM-STAT
	****     MOVE 'Y'                TO WSAA-LEAVE-IN-FORCE
	**** END-IF.
	**** Update WSAA fields accordingly.
	**** IF VALID-STATCODE
	****     MOVE OVRD-STATCODE      TO WSAA-STATCODE
	****     MOVE OVRD-PSTATCODE     TO WSAA-PSTATCODE
	**** ELSE
	****     IF  T5679-SET-COV-PREM-STAT = OVRD-PSTATCODE
	****     AND NOT VALID-PSTATCODE
	****         MOVE OVRD-STATCODE  TO WSAA-STATCODE
	****         MOVE OVRD-PSTATCODE TO WSAA-PSTATCODE
	****     END-IF
	**** END-IF.
	**** MOVE NEXTR                  TO COVRRNL-FUNCTION.
	*6590-EXIT.
	*    EXIT.
	* </pre>
	*/
	protected void lapsePua6600() {
		hpuaIO.setParams(SPACES);
		hpuaIO.setChdrcoy(covrrnlIO.getChdrcoy());
		hpuaIO.setChdrnum(covrrnlIO.getChdrnum());
		hpuaIO.setLife(covrrnlIO.getLife());
		hpuaIO.setCoverage(covrrnlIO.getCoverage());
		hpuaIO.setRider(covrrnlIO.getRider());
		hpuaIO.setPlanSuffix(covrrnlIO.getPlanSuffix());
		hpuaIO.setPuAddNbr(ZERO);
		hpuaIO.setFormat(formatsInner.hpuarec);
		hpuaIO.setFunction(varcom.begn);
		while ( !(isEQ(hpuaIO.getStatuz(), varcom.endp))) {
			updateHpua6650();
		}

	}

	protected void updateHpua6650() {
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(), varcom.oK)
		&& isNE(hpuaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hpuaIO.getParams());
			fatalError600();
		}
		/* Key break                                                       */
		if (isNE(covrrnlIO.getChdrcoy(), hpuaIO.getChdrcoy())
		|| isNE(covrrnlIO.getChdrnum(), hpuaIO.getChdrnum())
		|| isNE(covrrnlIO.getLife(), hpuaIO.getLife())
		|| isNE(covrrnlIO.getCoverage(), hpuaIO.getCoverage())
		|| isNE(covrrnlIO.getRider(), hpuaIO.getRider())
		|| isNE(covrrnlIO.getPlanSuffix(), hpuaIO.getPlanSuffix())) {
			hpuaIO.setStatuz(varcom.endp);
		}
		if (isEQ(hpuaIO.getStatuz(), varcom.oK)) {
			if (isEQ(hpuaIO.getTranno(), ovrduerec.tranno)) {
				/*            ADD 1               TO HPUA-PU-ADD-NBR       <R67>   */
				/*            MOVE BEGN           TO HPUA-FUNCTION         <R67>   */
				hpuaIO.setFunction(varcom.nextr);
				return ;
			}
			/* Rewrite the old HPUA                                            */
			hpuaIO.setValidflag("2");
			hpuaIO.setFunction(varcom.keeps);
			hpuaio9100();
			hpuaIO.setFunction(varcom.writs);
			hpuaio9100();
			/* Update COVRRNL-VARSI                                            */
			if (isGT(covrrnlIO.getVarSumInsured(), ZERO)) {
				setPrecision(covrrnlIO.getVarSumInsured(), 2);
				covrrnlIO.setVarSumInsured(sub(covrrnlIO.getVarSumInsured(),
						hpuaIO.getSumin()).getbigdata());
			}
			/*                                                         <R67>   */
			/* Write a new HPUA                                        <R67>   */
			/*                                                         <R67>   */
			hpuaIO.setValidflag("1");
			hpuaIO.setTranno(covrrnlIO.getTranno());
			hpuaIO.setPstatcode(wsaaT6597ArrayInner.wsaaT6597Cpstat[wsaaT6597Ix.toInt()][4]);
			hpuaIO.setRstatcode(wsaaT6597ArrayInner.wsaaT6597Crstat[wsaaT6597Ix.toInt()][4]);
			hpuaIO.setRiskCessDate(covrrnlIO.getRiskCessDate());
			hpuaIO.setFunction(varcom.writr);
			hpuaio9100();
			/* Next HPUA record                                                */
			/*        ADD 1                   TO HPUA-PU-ADD-NBR       <R67>   */
			/*        MOVE BEGN               TO HPUA-FUNCTION         <R67>   */
			hpuaIO.setFunction(varcom.nextr);
		}
	}

protected void endOfCovrProcess6800()
	{
		endCovr6810();
	}

protected void endCovr6810()
	{
		/* Update statii fields in CHDRLIF.                             */
		/* IF WSAA-STATCODE = SPACE                                     */
		/*     MOVE T5679-SET-CN-PREM-STAT TO CHDRLIF-PSTATCODE         */
		/*     MOVE T5679-SET-CN-RISK-STAT TO CHDRLIF-STATCODE          */
		/* ELSE                                                         */
		/*     MOVE WSAA-PSTATCODE     TO CHDRLIF-PSTATCODE             */
		/*     MOVE WSAA-STATCODE      TO CHDRLIF-STATCODE              */
		/* END-IF.                                                      */
		wsaaCovrRskMatch.set("N");
		wsaaCovrPrmMatch.set("N");

		String keyItem = bprdIO.getAuthCode().toString().trim().concat(chdrlifIO.getCnttype().trim());/* IJTI-1523 */
		if (!readT5399(keyItem)){
			wsaaAuthCode.set(bprdIO.getAuthCode());
			wsaaCnttype.set(chdrlifIO.getCnttype());			
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(bsprIO.getCompany());
			itemIO.setItemtabl(t5399);
			itemIO.setItemitem(wsaaT5399Key2);
			itemIO.setFunction(varcom.readr);
			itemIO.setStatuz(stnf);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();			
		}
			
		/*
		ArraySearch as1 = ArraySearch.getInstance(wsaaT5399ArrayInner.wsaaT5399Rec);
		as1.setIndices(wsaaT5399Ix);
		as1.addSearchKey(wsaaT5399ArrayInner.wsaaT5399Key, wsaaT5399Key2, true);
		if (as1.binarySearch()) {*/
			/*CONTINUE_STMT*/
		/*
		}
		else {
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(bsprIO.getCompany());
			itemIO.setItemtabl(t5399);
			itemIO.setItemitem(wsaaT5399Key2);
			itemIO.setFunction(varcom.readr);
			itemIO.setStatuz(stnf);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		*/
		/*  We need to process the coverage risk statuses and premium      */
		/*  statuses in two separate parts.  Using T5399 as the driving    */
		/*  file, find the first match between risk status and coverage    */
		/*  risk status.   
		 *                                                 */
		/*
		for (wsaaT5399CrskIx.set(1); !(isGT(wsaaT5399CrskIx, 12)
		|| covrRskMatch.isTrue()); wsaaT5399CrskIx.add(1)){
			readRist6820();
		}
		*/
		readRist();
		/*  Use T5679 as a 'catch all' if the T5399 hierarchy does not     */
		/*  contain the coverage risk.                                     */
		//ILIFE-5297 start by dpuhawan
		//if (!covrRskMatch.isTrue() || (isNE(tr5b2rec.nlg , "Y") && !updateRequired)) {
		if (!covrRskMatch.isTrue() && (isNE(tr5b2rec.nlg , "Y")  && isNE(tr5b2rec.nlg , "") &&  !updateRequired)) {  
		//ILIFE-5297 END
			chdrlifIO.setStatcode(t5679rec.setCnRiskStat.toString());
		}
		/*  Perform the same processing as above with the premium statcode.*/
		/*
		for (wsaaT5399CprmIx.set(1); !(isGT(wsaaT5399CprmIx, 12)
		|| covrPrmMatch.isTrue()); wsaaT5399CprmIx.add(1)){
			readPrst6830();
		}
		*/
		readPrst();
		if (!covrPrmMatch.isTrue() && !updateRequired && isLTE(nlgcalcrec.nlgBalance,0)) {
			chdrlifIO.setPstcde(t5679rec.setCnPremStat.toString());
			payrIO.setPstatcode(t5679rec.setCnPremStat.toString());
		}
		if (isEQ(wsaaChdrOk, "N")) {
			readChdr5000();
		}
		//rewriteChdr5200();
		rewriteInsertedChdr();
		rewritePayrPstcde();
	}

protected void rewriteInsertedChdr(){
	this.chdrUpdateInsertedList.add(chdrlifIO);
}

protected void rewritePayrPstcde(){
	Payrpf payr = new Payrpf(payrIO);
	payr.setBtdate(payzpfRec.getBtdate());
	if(null!= payr.getBillday() && isNE(payr.getBillday(),SPACES))
		payr.setBillcd(Integer.valueOf(String.valueOf(payr.getBtdate()).substring(0, 6).concat(payr.getBillday())));
	else
		payr.setBillcd(payzpfRec.getBtdate());
	payr.setPstatcode(payrIO.getPstatcode()); //ILIFE-4323 by dpuhawan
	this.payrUpdatePstcdeList.add(payr);
}

protected void readRist(){
	if (riskStats.isEmpty()) return;
	for (String covrRisk : covrRiskStats){
		int riskIndex  = riskStats.indexOf(covrRisk);
		if (riskIndex >= 0){ //ILIFE-5297 start by dpuhawan
			chdrlifIO.setStatcode(cnRiskStats.get(riskStats.indexOf(covrRisk)));
			wsaaCovrRskMatch.set("Y");
			break;
		}		
	}
}


protected void readRist6820()
	{
		start6821();
	}

protected void start6821()
	{
		if (isEQ(wsaaT5399ArrayInner.wsaaT5399RiskStat[wsaaT5399Ix.toInt()][wsaaT5399CrskIx.toInt()], "  ")) {
			return ;
		}
		wsaaCovrIx.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaCovrIx, wsaaCovrRec.length); wsaaCovrIx.add(1)){
				if (isEQ(wsaaT5399ArrayInner.wsaaT5399RiskStat[wsaaT5399Ix.toInt()][wsaaT5399CrskIx.toInt()], wsaaCovrRist[wsaaCovrIx.toInt()])) {
					wsaaT5399SrskIx.set(wsaaT5399CrskIx);
					chdrlifIO.setStatcode(wsaaT5399ArrayInner.wsaaT5399SetRiskStat[wsaaT5399Ix.toInt()][wsaaT5399SrskIx.toInt()].toString());
					wsaaCovrRskMatch.set("Y");
					break searchlabel1;
				}
			}
			/*CONTINUE_STMT*/
		}
	}

protected void readPrst(){
	if (premStats.isEmpty()) return;
	for (String covrPrem : covrPremStats){
		int premIndex  = premStats.indexOf(covrPrem);
		if (premIndex >= 0 && !updateRequired && isLTE(nlgcalcrec.nlgBalance,0)){ //ILIFE-5297 start by dpuhawan
			chdrlifIO.setPstcde(cnPremStats.get(premStats.indexOf(covrPrem)));
			payrIO.setPstatcode(cnPremStats.get(premStats.indexOf(covrPrem)));
			wsaaCovrPrmMatch.set("Y");
			break;			
		}		
	}
}

protected void readPrst6830()
	{
		start6831();
	}

protected void start6831()
	{
		if (isEQ(wsaaT5399ArrayInner.wsaaT5399PremStat[wsaaT5399Ix.toInt()][wsaaT5399CprmIx.toInt()], "  ")) {
			return ;
		}
		wsaaCovrIx.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaCovrIx, wsaaCovrRec.length); wsaaCovrIx.add(1)){
				if (isEQ(wsaaT5399ArrayInner.wsaaT5399PremStat[wsaaT5399Ix.toInt()][wsaaT5399CprmIx.toInt()], wsaaCovrPrst[wsaaCovrIx.toInt()]) && !updateRequired && isLT(nlgcalcrec.nlgBalance,0)) {
					wsaaT5399SprmIx.set(wsaaT5399CprmIx);
					chdrlifIO.setPstcde((wsaaT5399ArrayInner.wsaaT5399SetPremStat[wsaaT5399Ix.toInt()][wsaaT5399SprmIx.toInt()].toString()));
					payrIO.setPstatcode(wsaaT5399ArrayInner.wsaaT5399SetPremStat[wsaaT5399Ix.toInt()][wsaaT5399SprmIx.toInt()].toString());
					wsaaCovrPrmMatch.set("Y");
					break searchlabel1;
				}
			}
			/*CONTINUE_STMT*/
		}
	}



protected void contractLevelNf7000()
	{
		/*NF*/
		wsaaAplFlag = "Y";
		searchT6597Arrears7100();
		/*    Process the T6597 record we have just read*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 3)
		|| (isLTE(wsaaDurationInForce, wsaaT6597ArrayInner.wsaaT6597Durmnth[wsaaT6597Ix.toInt()][wsaaSub.toInt()])
		&& isNE(wsaaT6597ArrayInner.wsaaT6597Premsub[wsaaT6597Ix.toInt()][wsaaSub.toInt()], SPACES))); wsaaSub.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isGT(wsaaSub, 3)) {
			/*NEXT_SENTENCE*/
		}
		else {
			/*     PERFORM 7200-PROCESS-T6597                               */
			if (isEQ(wsaaT6597ArrayInner.wsaaT6597Cpstat[wsaaT6597Ix.toInt()][wsaaSub.toInt()], SPACES)
			&& isEQ(wsaaT6597ArrayInner.wsaaT6597Crstat[wsaaT6597Ix.toInt()][wsaaSub.toInt()], SPACES)) {
				processT65977200();
			}
			else {
				wsaaAplFlag = "N";
			}
		}
		/*EXIT*/
	}

protected void searchT6597Arrears7100()
	{
		t65977110();
	}

protected void t65977110()
	{
		/*    Search T6597 using T6654-ARREARS-METHOD as the key.*/
		for (wsaaT6597Ix.set(wsaaT6597IxMax); !(isEQ(wsaaT6597Ix, 0)
		|| (isEQ(wsaaT6654ArrayInner.wsaaT6654ArrearsMethod[wsaaT6654Ix.toInt()], wsaaT6597ArrayInner.wsaaT6597Method[wsaaT6597Ix.toInt()])
		&& isGTE(wsaaToday, wsaaT6597ArrayInner.wsaaT6597Itmfrm[wsaaT6597Ix.toInt()]))); wsaaT6597Ix.add(-1))
{
			/*CONTINUE_STMT*/
		}
		if (isEQ(wsaaT6597Ix, 0)) {
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(bsprIO.getCompany());
			itemIO.setItemtabl(t6597);
			itemIO.setItemitem(wsaaT6654ArrayInner.wsaaT6654ArrearsMethod[wsaaT6654Ix.toInt()]);
			itemIO.setFunction(varcom.readr);
			itemIO.setStatuz(f781);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void processT65977200()
	{
		t65977210();
	}

protected void t65977210()
	{
		/* Get overdue instalement amount from LINSRNL                     */
		linsrnlIO.setChdrcoy(payzpfRec.getChdrcoy());
		linsrnlIO.setChdrnum(payzpfRec.getChdrnum());
		linsrnlIO.setInstfrom(payzpfRec.getPtdate());
		linsrnlIO.setFormat(formatsInner.linsrnlrec);
		linsrnlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, linsrnlIO);
		if (isNE(linsrnlIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(linsrnlIO.getStatuz());
			syserrrec.params.set(linsrnlIO.getParams());
			fatalError600();
		}
		nfloanrec.nfloanRec.set(SPACES);
		nfloanrec.language.set(bsscIO.getLanguage());
		nfloanrec.chdrcoy.set(payzpfRec.getChdrcoy());
		nfloanrec.chdrnum.set(payzpfRec.getChdrnum());
		nfloanrec.cntcurr.set(payrIO.getCntcurr());
		nfloanrec.effdate.set(bsscIO.getEffectiveDate());
		nfloanrec.ptdate.set(payzpfRec.getPtdate());
		nfloanrec.batcbrn.set(batcdorrec.branch);
		nfloanrec.company.set(bsprIO.getCompany());
		nfloanrec.billfreq.set(payrIO.getBillfreq());
		nfloanrec.cnttype.set(chdrlifIO.getCnttype());
		nfloanrec.polsum.set(chdrlifIO.getPolsum());
		nfloanrec.polinc.set(chdrlifIO.getPolinc());
		nfloanrec.batcBatctrcde.set(bprdIO.getAuthCode());
		/*    MOVE PAYR-OUTSTAMT          TO NONF-OUTSTAMT.                */
		nfloanrec.outstamt.set(linsrnlIO.getInstamt06());
		callProgram(wsaaT6597ArrayInner.wsaaT6597Premsub[wsaaT6597Ix.toInt()][wsaaSub.toInt()], nfloanrec.nfloanRec);
		if (isNE(nfloanrec.statuz, varcom.oK)
		&& isNE(nfloanrec.statuz, "NAPL")
		&& isNE(nfloanrec.statuz, "APLA")) {
			syserrrec.subrname.set(wsaaT6597ArrayInner.wsaaT6597Premsub[wsaaT6597Ix.toInt()][wsaaSub.toInt()]);
			syserrrec.params.set(nfloanrec.nfloanRec);
			syserrrec.statuz.set(nfloanrec.statuz);
			fatalError600();
		}
		/* IF NONF-STATUZ              = 'NAPL'                         */
		if (isEQ(nfloanrec.statuz, "NAPL")
		|| isEQ(nfloanrec.statuz, "APLA")) {
			return ;
		}
		/*    Get here so contract is OK to have an APL attached to it.*/
		/* PERFORM 5000-READ-CHDR.                                      */
		/* PERFORM 5200-REWRITE-CHDR.                                   */
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(chdrlifIO.getTranno()+1);
		writeChdrValidflag25400();
		writeChdrValidflag15300();
		callCrtloan8000();
		writeLetc8500();
		wsaaChdrOk = "Y";
		wsaaLastPtrn.set(chdrlifIO.getChdrnum());
		
		writePtrn5800();
		ct10Val++;
		ct11Val = ct11Val.add(payrIO.getOutstamt());
	}

protected void callCrtloan8000()
	{
		start8010();
	}

protected void start8010()
	{
		/*    Read T5645 to get the Account postings for APLs*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(payzpfRec.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/*    Get item description from T5645.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(nfloanrec.chdrcoy);
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaProg);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		/*    Set up linkage to CRTLOAN, which will write LOAN*/
		/*    and APL ACMV records.*/
		crtloanrec.longdesc.set(descIO.getLongdesc());
		crtloanrec.chdrcoy.set(nfloanrec.chdrcoy);
		crtloanrec.chdrnum.set(nfloanrec.chdrnum);
		crtloanrec.cnttype.set(nfloanrec.cnttype);
		crtloanrec.loantype.set("A");
		crtloanrec.cntcurr.set(nfloanrec.cntcurr);
		crtloanrec.billcurr.set(payrIO.getBillcurr());
		crtloanrec.tranno.set(chdrlifIO.getTranno());
		crtloanrec.occdate.set(chdrlifIO.getOccdate());
		crtloanrec.effdate.set(bsscIO.getEffectiveDate());
		crtloanrec.authCode.set(bprdIO.getAuthCode());
		crtloanrec.language.set(bsscIO.getLanguage());
		/*    MOVE PAYR-OUTSTAMT          TO CRTL-OUTSTAMT.                */
		crtloanrec.outstamt.set(linsrnlIO.getInstamt06());
		crtloanrec.batchkey.set(batcdorrec.batchkey);
		crtloanrec.sacscode01.set(t5645rec.sacscode01);
		crtloanrec.sacscode02.set(t5645rec.sacscode02);
		crtloanrec.sacscode03.set(t5645rec.sacscode03);
		crtloanrec.sacscode04.set(t5645rec.sacscode04);
		crtloanrec.sacstyp01.set(t5645rec.sacstype01);
		crtloanrec.sacstyp02.set(t5645rec.sacstype02);
		crtloanrec.sacstyp03.set(t5645rec.sacstype03);
		crtloanrec.sacstyp04.set(t5645rec.sacstype04);
		crtloanrec.glcode01.set(t5645rec.glmap01);
		crtloanrec.glcode02.set(t5645rec.glmap02);
		crtloanrec.glcode03.set(t5645rec.glmap03);
		crtloanrec.glcode04.set(t5645rec.glmap04);
		crtloanrec.glsign01.set(t5645rec.sign01);
		crtloanrec.glsign02.set(t5645rec.sign02);
		crtloanrec.glsign03.set(t5645rec.sign03);
		crtloanrec.glsign04.set(t5645rec.sign04);
		callProgram(Crtloan.class, crtloanrec.crtloanRec);
		if (isNE(crtloanrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(crtloanrec.statuz);
			syserrrec.params.set(crtloanrec.crtloanRec);
			fatalError600();
		}
	}

protected void writeLetc8500()
	{
		start8510();
	}

protected void start8510()
	{
		/* Read T6634 to obtain the Letter Type                            */
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		/* MOVE T6634                  TO ITEM-ITEMTABL.        <PCPPRT>*/
		/* MOVE CHDRLIF-CNTTYPE        TO WSAA-T6634-CNTTYPE.   <PCPPRT>*/
		/* MOVE BPRD-SYSTEM-PARAM01    TO WSAA-T6634-TRCDE.     <PCPPRT>*/
		/* MOVE WSAA-T6634-KEY         TO ITEM-ITEMITEM.        <PCPPRT>*/
		itemIO.setItemtabl(tr384);
		wsaaTr384Cnttype.set(chdrlifIO.getCnttype());
		wsaaTr384Trcde.set(bprdIO.getSystemParam01());
		itemIO.setItemitem(wsaaTr384Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.      <PCPPRT>*/
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		/* IF T6634-LETTER-TYPE        = SPACES                 <PCPPRT>*/
		if (isEQ(tr384rec.letterType, SPACES)) {
			return ;
		}
		/* Write HLTX to store information for printing            <LA2101>*/
		hltxIO.setParams(SPACES);
		for (wsaaIdx.set(1); !(isGT(wsaaIdx, 8)); wsaaIdx.add(1)){
			hltxIO.setHnumfld(wsaaIdx, ZERO);
		}
		hltxIO.setRequestCompany(bsprIO.getCompany());
		/* MOVE T6634-LETTER-TYPE      TO HLTX-LETTER-TYPE.     <PCPPRT>*/
		hltxIO.setLetterType(tr384rec.letterType);
		hltxIO.setClntcoy(chdrlifIO.getCowncoy());
		hltxIO.setClntnum(chdrlifIO.getCownnum());
		hltxIO.setLetterSeqno(chdrlifIO.getTranno());
		hltxIO.setChdrcoy(chdrlifIO.getChdrcoy());
		hltxIO.setChdrnum(chdrlifIO.getChdrnum());
		/* Modal Premium                                                   */
		hltxIO.setHnumfld01(payrIO.getSinstamt06());
		/* Overdue Premium                                                 */
		/*    MOVE PAYR-OUTSTAMT          TO HLTX-HNUMFLD02.       <LA2101>*/
		hltxIO.setHnumfld02(payrIO.getSinstamt06());
		/* Premium Default Date - PTD before APL                           */
		datcon1rec.intDate.set(payrIO.getPtdate());
		datcon1rec.function.set("CONV");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		hltxIO.setHshtchar01(datcon1rec.extDate);
		/* New Paid To Date     - PTD after APL                            */
		/*    MOVE PAYR-BTDATE            TO DTC1-INT-DATE.        <LA4352>*/
		/* Only 1 overdue premium for APL                                  */
		datcon2rec.intDate1.set(payrIO.getPtdate());
		datcon2rec.intDate2.set(0);
		datcon2rec.frequency.set(payrIO.getBillfreq());
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		datcon1rec.intDate.set(datcon2rec.intDate2);
		datcon1rec.function.set("CONV");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		hltxIO.setHshtchar02(datcon1rec.extDate);
		hltxIO.setHshtchar03(payrIO.getCntcurr());
		hltxIO.setFormat(formatsInner.hltxrec);
		hltxIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hltxIO);
		if (isNE(hltxIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hltxIO.getParams());
			fatalError600();
		}
		letrqstrec.requestCompany.set(bsprIO.getCompany());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaToday);
		letrqstrec.clntcoy.set(chdrlifIO.getCowncoy());
		letrqstrec.clntnum.set(chdrlifIO.getCownnum());
		letrqstrec.rdocpfx.set(chdrlifIO.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrlifIO.getChdrnum());
		letrqstrec.tranno.set(chdrlifIO.getTranno());
		letrqstrec.branch.set(chdrlifIO.getCntbranch());
		letrqstrec.otherKeys.set(bsscIO.getLanguage());
		letrqstrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrlifIO.getChdrnum());
		letrqstrec.function.set("ADD");
		/*    CALL 'LETRQST' USING LETRQST-PARAMS.                 <P002>  */
		/*  CALL 'HLETRQS' USING LETRQST-PARAMS.                 <PCPPRT>*/
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
	}

protected void callOverbill9000()
	{
		ovrbll9010();
	}

protected void ovrbll9010()
	{
		ovrbillrec.overbillRec.set(SPACES);
		ovrbillrec.company.set(payzpfRec.getChdrcoy());
		ovrbillrec.chdrnum.set(payzpfRec.getChdrnum());
		ovrbillrec.batchkey.set(batcdorrec.batchkey);
		compute(ovrbillrec.newTranno, 0).set(add(1, chdrlifIO.getTranno()));
		ovrbillrec.btdate.set(ZERO);
		ovrbillrec.ptdate.set(ZERO);
		ovrbillrec.language.set(bsscIO.getLanguage());
		ovrbillrec.tranDate.set(wsaaTransactionDate);
		ovrbillrec.tranTime.set(wsaaTransactionTime);
		ovrbillrec.user.set(wsaaUser);
		ovrbillrec.termid.set(wsaaTermid);
		callProgram(Overbill.class, ovrbillrec.overbillRec);
		if (isNE(ovrbillrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(ovrbillrec.statuz);
			syserrrec.params.set(ovrbillrec.overbillRec);
			fatalError600();
		}
		payzpfRec.setBtdate(ovrbillrec.btdate.toInt());
		payzpfRec.setPtdate(ovrbillrec.ptdate.toInt());
	}

protected void a000Statistics()
	{
		a010Start();
	}

protected void a010Start()
	{
		lifsttrrec.batccoy.set(batcdorrec.company);
		lifsttrrec.batcbrn.set(batcdorrec.branch);
		lifsttrrec.batcactyr.set(batcdorrec.actyear);
		lifsttrrec.batcactmn.set(batcdorrec.actmonth);
		lifsttrrec.batctrcde.set(bprdIO.getAuthCode());
		lifsttrrec.batcbatch.set(batcdorrec.batch);
		lifsttrrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrlifIO.getChdrnum());
		lifsttrrec.tranno.set(chdrlifIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			fatalError600();
		}
	}

protected void close4000()
	{
		lsaaStatuz.set(varcom.oK);
	}

protected void hpuaio9100()
	{
		/*START*/
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(hpuaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void a200DelUnderwritting()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					a200Ctrl();
				case a200Delet:
					a200Delet();
					a200Next();
				case a200Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a200Ctrl()
	{
		if (isEQ(chdrlifIO.getStatcode(), "IF")) {
			goTo(GotoLabel.a200Exit);
		}
		a300BegnLifeio();
	}

protected void a200Delet()
	{
		initialize(crtundwrec.parmRec);
		crtundwrec.clntnum.set(lifeIO.getLifcnum());
		crtundwrec.coy.set(chdrlifIO.getChdrcoy());
		crtundwrec.currcode.set(chdrlifIO.getCntcurr());
		crtundwrec.chdrnum.set(chdrlifIO.getChdrnum());
		crtundwrec.crtable.fill("*");
		crtundwrec.function.set("DEL");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status, varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			fatalError600();
		}
	}

protected void a200Next()
	{
		lifeIO.setFunction(varcom.nextr);
		lifeIO.setFormat(formatsInner.liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			return ;
		}
		if (isEQ(chdrlifIO.getChdrcoy(), lifeIO.getChdrcoy())
		&& isEQ(chdrlifIO.getChdrnum(), lifeIO.getChdrnum())) {
			goTo(GotoLabel.a200Delet);
		}
	}

protected void a300BegnLifeio()
	{
		a300Ctrl();
	}

protected void a300Ctrl()
	{
		lifeIO.setRecKeyData(SPACES);
		lifeIO.setChdrcoy(chdrlifIO.getChdrcoy());
		lifeIO.setChdrnum(chdrlifIO.getChdrnum());
		lifeIO.setFunction(varcom.begn);
		lifeIO.setFormat(formatsInner.liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (!(isEQ(lifeIO.getStatuz(), varcom.oK)
		&& isEQ(chdrlifIO.getChdrcoy(), lifeIO.getChdrcoy())
		&& isEQ(chdrlifIO.getChdrnum(), lifeIO.getChdrnum()))) {
			lifeIO.setStatuz(varcom.endp);
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			fatalError600();
		}
	}

protected void b000CallRounding()
	{
		/*B100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(batcdorrec.trcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*B900-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-T6597-ARRAY--INNER
 */
private static final class WsaaT6597ArrayInner {

		/* WSAA-T6597-ARRAY */
	private FixedLengthStringData[] wsaaT6597Rec = FLSInittedArray (100, 66);
	private FixedLengthStringData[] wsaaT6597Key = FLSDArrayPartOfArrayStructure(4, wsaaT6597Rec, 0, SPACES);
	private FixedLengthStringData[] wsaaT6597Method = FLSDArrayPartOfArrayStructure(4, wsaaT6597Key, 0);
	private FixedLengthStringData[] wsaaT6597Data = FLSDArrayPartOfArrayStructure(62, wsaaT6597Rec, 4);
	private PackedDecimalData[] wsaaT6597Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT6597Data, 0);
	private FixedLengthStringData[][] wsaaT6597Subroutine = FLSDArrayPartOfArrayStructure(4, 12, wsaaT6597Data, 5);
	private FixedLengthStringData[][] wsaaT6597Premsub = FLSDArrayPartOfArrayStructure(8, wsaaT6597Subroutine, 0);
	private FixedLengthStringData[][] wsaaT6597Cpstat = FLSDArrayPartOfArrayStructure(2, wsaaT6597Subroutine, 8);
	private FixedLengthStringData[][] wsaaT6597Crstat = FLSDArrayPartOfArrayStructure(2, wsaaT6597Subroutine, 10);
	private FixedLengthStringData[][] wsaaT6597Durmonth = FLSDArrayPartOfArrayStructure(3, 3, wsaaT6597Data, 53);
	private ZonedDecimalData[][] wsaaT6597Durmnth = ZDArrayPartOfArrayStructure(3, 0, wsaaT6597Durmonth, 0);
}
/*
 * Class transformed  from Data Structure WSAA-T6654-ARRAY--INNER
 */
public static final class WsaaT6654ArrayInner {

		/* WSAA-T6654-ARRAY
		 03  WSAA-T6654-REC          OCCURS 100                       */
	private FixedLengthStringData[] wsaaT6654Rec = FLSInittedArray (500, 52);
	private FixedLengthStringData[] wsaaT6654Key = FLSDArrayPartOfArrayStructure(4, wsaaT6654Rec, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT6654Billchnl = FLSDArrayPartOfArrayStructure(1, wsaaT6654Key, 0);
	private FixedLengthStringData[] wsaaT6654Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT6654Key, 1);
	private FixedLengthStringData[] wsaaT6654Data = FLSDArrayPartOfArrayStructure(48, wsaaT6654Rec, 4);
	public ZonedDecimalData[] wsaaT6654NonForfDays = ZDArrayPartOfArrayStructure(3, 0, wsaaT6654Data, 0);
	private FixedLengthStringData[] wsaaT6654ArrearsMethod = FLSDArrayPartOfArrayStructure(4, wsaaT6654Data, 3);
	private FixedLengthStringData[] wsaaT6654Subroutine = FLSDArrayPartOfArrayStructure(32, wsaaT6654Data, 7);
	private FixedLengthStringData[][] wsaaT6654Doctid = FLSDArrayPartOfArrayStructure(4, 8, wsaaT6654Subroutine, 0);
	private FixedLengthStringData[] wsaaT6654Expiry = FLSDArrayPartOfArrayStructure(9, wsaaT6654Data, 39);
	private ZonedDecimalData[][] wsaaT6654Daexpy = ZDArrayPartOfArrayStructure(3, 3, 0, wsaaT6654Expiry, 0);
}
/*
 * Class transformed  from Data Structure WSAA-T5399-ARRAY--INNER
 */
private static final class WsaaT5399ArrayInner {

		/* WSAA-T5399-ARRAY */
	//ILIFE-3472 Code Promotion for VPMS externalization of LIFE TRX product calculations to LIFE Target2 Environment
	private FixedLengthStringData[] wsaaT5399Rec = FLSInittedArray (200, 103);
	private FixedLengthStringData[] wsaaT5399Key = FLSDArrayPartOfArrayStructure(7, wsaaT5399Rec, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT5399AuthCode = FLSDArrayPartOfArrayStructure(4, wsaaT5399Key, 0);
	private FixedLengthStringData[] wsaaT5399Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5399Key, 4);
	private FixedLengthStringData[] wsaaT5399Data = FLSDArrayPartOfArrayStructure(96, wsaaT5399Rec, 7);
	private FixedLengthStringData[] wsaaT5399RiskStats = FLSDArrayPartOfArrayStructure(24, wsaaT5399Data, 0);
	private FixedLengthStringData[][] wsaaT5399RiskStat = FLSDArrayPartOfArrayStructure(12, 2, wsaaT5399RiskStats, 0);
	private FixedLengthStringData[] wsaaT5399PremStats = FLSDArrayPartOfArrayStructure(24, wsaaT5399Data, 24);
	private FixedLengthStringData[][] wsaaT5399PremStat = FLSDArrayPartOfArrayStructure(12, 2, wsaaT5399PremStats, 0);
	private FixedLengthStringData[] wsaaT5399SetRiskStats = FLSDArrayPartOfArrayStructure(24, wsaaT5399Data, 48);
	private FixedLengthStringData[][] wsaaT5399SetRiskStat = FLSDArrayPartOfArrayStructure(12, 2, wsaaT5399SetRiskStats, 0);
	private FixedLengthStringData[] wsaaT5399SetPremStats = FLSDArrayPartOfArrayStructure(24, wsaaT5399Data, 72);
	private FixedLengthStringData[][] wsaaT5399SetPremStat = FLSDArrayPartOfArrayStructure(12, 2, wsaaT5399SetPremStats, 0);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData itemrec = new FixedLengthStringData(7).init("ITEMREC");
	private FixedLengthStringData covrrnlrec = new FixedLengthStringData(10).init("COVRRNLREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData liferec = new FixedLengthStringData(10).init("LIFEREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData hpadrec = new FixedLengthStringData(10).init("HPADREC");
	private FixedLengthStringData hpuarec = new FixedLengthStringData(10).init("HPUAREC");
	private FixedLengthStringData hltxrec = new FixedLengthStringData(10).init("HLTXREC");
	private FixedLengthStringData linsrnlrec = new FixedLengthStringData(10).init("LINSRNLREC");
}
/*
 * Class transformed  from Data Structure CONTROL-TOTALS--INNER
 */
private static final class ControlTotalsInner {
		/* CONTROL-TOTALS */
	private ZonedDecimalData ct01 = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private ZonedDecimalData ct02 = new ZonedDecimalData(2, 0).init(2).setUnsigned();
	private ZonedDecimalData ct03 = new ZonedDecimalData(2, 0).init(3).setUnsigned();
	private ZonedDecimalData ct04 = new ZonedDecimalData(2, 0).init(4).setUnsigned();
	private ZonedDecimalData ct05 = new ZonedDecimalData(2, 0).init(5).setUnsigned();
	private ZonedDecimalData ct06 = new ZonedDecimalData(2, 0).init(6).setUnsigned();
	private ZonedDecimalData ct07 = new ZonedDecimalData(2, 0).init(7).setUnsigned();
	private ZonedDecimalData ct08 = new ZonedDecimalData(2, 0).init(8).setUnsigned();
	private ZonedDecimalData ct09 = new ZonedDecimalData(2, 0).init(9).setUnsigned();
	private ZonedDecimalData ct10 = new ZonedDecimalData(2, 0).init(10).setUnsigned();
	private ZonedDecimalData ct11 = new ZonedDecimalData(2, 0).init(11).setUnsigned();
	private ZonedDecimalData ct12 = new ZonedDecimalData(2, 0).init(12).setUnsigned();
}
protected void setAutoLapsePeriodCustomerSpecific(){
	
}
}