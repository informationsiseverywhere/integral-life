package com.csc.life.regularprocessing.dataaccess.model;

import java.math.BigDecimal;

public class Bonlpf {
    private long uniqueNumer;
    private String chdrcoy;
    private String chdrnum;
    private int currfrom;
    private int currto;
    private String datetexc;
    private String validflag;
    private String longdesc01;
    private String longdesc02;
    private String longdesc03;
    private String longdesc04;
    private String longdesc05;
    private String longdesc06;
    private String longdesc07;
    private String longdesc08;
    private String longdesc09;
    private String longdesc10;
    private BigDecimal sumins01 = BigDecimal.ZERO;
    private BigDecimal sumins02 = BigDecimal.ZERO;
    private BigDecimal sumins03 = BigDecimal.ZERO;
    private BigDecimal sumins04 = BigDecimal.ZERO;
    private BigDecimal sumins05 = BigDecimal.ZERO;
    private BigDecimal sumins06 = BigDecimal.ZERO;
    private BigDecimal sumins07 = BigDecimal.ZERO;
    private BigDecimal sumins08 = BigDecimal.ZERO;
    private BigDecimal sumins09 = BigDecimal.ZERO;
    private BigDecimal sumins10 = BigDecimal.ZERO;
    private String bonusCalcMeth01;
    private String bonusCalcMeth02;
    private String bonusCalcMeth03;
    private String bonusCalcMeth04;
    private String bonusCalcMeth05;
    private String bonusCalcMeth06;
    private String bonusCalcMeth07;
    private String bonusCalcMeth08;
    private String bonusCalcMeth09;
    private String bonusCalcMeth10;
    private BigDecimal bonPayThisYr01 = BigDecimal.ZERO;
    private BigDecimal bonPayThisYr02 = BigDecimal.ZERO;
    private BigDecimal bonPayThisYr03 = BigDecimal.ZERO;
    private BigDecimal bonPayThisYr04 = BigDecimal.ZERO;
    private BigDecimal bonPayThisYr05 = BigDecimal.ZERO;
    private BigDecimal bonPayThisYr06 = BigDecimal.ZERO;
    private BigDecimal bonPayThisYr07 = BigDecimal.ZERO;
    private BigDecimal bonPayThisYr08 = BigDecimal.ZERO;
    private BigDecimal bonPayThisYr09 = BigDecimal.ZERO;
    private BigDecimal bonPayThisYr10 = BigDecimal.ZERO;
    private BigDecimal bonPayLastYr01 = BigDecimal.ZERO;
    private BigDecimal bonPayLastYr02 = BigDecimal.ZERO;
    private BigDecimal bonPayLastYr03 = BigDecimal.ZERO;
    private BigDecimal bonPayLastYr04 = BigDecimal.ZERO;
    private BigDecimal bonPayLastYr05 = BigDecimal.ZERO;
    private BigDecimal bonPayLastYr06 = BigDecimal.ZERO;
    private BigDecimal bonPayLastYr07 = BigDecimal.ZERO;
    private BigDecimal bonPayLastYr08 = BigDecimal.ZERO;
    private BigDecimal bonPayLastYr09 = BigDecimal.ZERO;
    private BigDecimal bonPayLastYr10 = BigDecimal.ZERO;
    private BigDecimal totalBonus01 = BigDecimal.ZERO;
    private BigDecimal totalBonus02 = BigDecimal.ZERO;
    private BigDecimal totalBonus03 = BigDecimal.ZERO;
    private BigDecimal totalBonus04 = BigDecimal.ZERO;
    private BigDecimal totalBonus05 = BigDecimal.ZERO;
    private BigDecimal totalBonus06 = BigDecimal.ZERO;
    private BigDecimal totalBonus07 = BigDecimal.ZERO;
    private BigDecimal totalBonus08 = BigDecimal.ZERO;
    private BigDecimal totalBonus09 = BigDecimal.ZERO;
    private BigDecimal totalBonus10 = BigDecimal.ZERO;
    private String termid;
    private int user;
    private int transactionDate;
    private int transactionTime;
    private String userProfile;
    private String jobName;
    private String datime;

    public long getUniqueNumer() {
        return uniqueNumer;
    }

    public void setUniqueNumer(long uniqueNumer) {
        this.uniqueNumer = uniqueNumer;
    }

    public String getChdrcoy() {
        return chdrcoy;
    }

    public String getChdrnum() {
        return chdrnum;
    }

    public int getCurrfrom() {
        return currfrom;
    }

    public int getCurrto() {
        return currto;
    }

    public String getDatetexc() {
        return datetexc;
    }

    public String getValidflag() {
        return validflag;
    }

    public String getLongdesc01() {
        return longdesc01;
    }

    public String getLongdesc02() {
        return longdesc02;
    }

    public String getLongdesc03() {
        return longdesc03;
    }

    public String getLongdesc04() {
        return longdesc04;
    }

    public String getLongdesc05() {
        return longdesc05;
    }

    public String getLongdesc06() {
        return longdesc06;
    }

    public String getLongdesc07() {
        return longdesc07;
    }

    public String getLongdesc08() {
        return longdesc08;
    }

    public String getLongdesc09() {
        return longdesc09;
    }

    public String getLongdesc10() {
        return longdesc10;
    }

    public BigDecimal getSumins01() {
        return sumins01;
    }

    public BigDecimal getSumins02() {
        return sumins02;
    }

    public BigDecimal getSumins03() {
        return sumins03;
    }

    public BigDecimal getSumins04() {
        return sumins04;
    }

    public BigDecimal getSumins05() {
        return sumins05;
    }

    public BigDecimal getSumins06() {
        return sumins06;
    }

    public BigDecimal getSumins07() {
        return sumins07;
    }

    public BigDecimal getSumins08() {
        return sumins08;
    }

    public BigDecimal getSumins09() {
        return sumins09;
    }

    public BigDecimal getSumins10() {
        return sumins10;
    }

    public String getBonusCalcMeth01() {
        return bonusCalcMeth01;
    }

    public String getBonusCalcMeth02() {
        return bonusCalcMeth02;
    }

    public String getBonusCalcMeth03() {
        return bonusCalcMeth03;
    }

    public String getBonusCalcMeth04() {
        return bonusCalcMeth04;
    }

    public String getBonusCalcMeth05() {
        return bonusCalcMeth05;
    }

    public String getBonusCalcMeth06() {
        return bonusCalcMeth06;
    }

    public String getBonusCalcMeth07() {
        return bonusCalcMeth07;
    }

    public String getBonusCalcMeth08() {
        return bonusCalcMeth08;
    }

    public String getBonusCalcMeth09() {
        return bonusCalcMeth09;
    }

    public String getBonusCalcMeth10() {
        return bonusCalcMeth10;
    }

    public BigDecimal getBonPayThisYr01() {
        return bonPayThisYr01;
    }

    public BigDecimal getBonPayThisYr02() {
        return bonPayThisYr02;
    }

    public BigDecimal getBonPayThisYr03() {
        return bonPayThisYr03;
    }

    public BigDecimal getBonPayThisYr04() {
        return bonPayThisYr04;
    }

    public BigDecimal getBonPayThisYr05() {
        return bonPayThisYr05;
    }

    public BigDecimal getBonPayThisYr06() {
        return bonPayThisYr06;
    }

    public BigDecimal getBonPayThisYr07() {
        return bonPayThisYr07;
    }

    public BigDecimal getBonPayThisYr08() {
        return bonPayThisYr08;
    }

    public BigDecimal getBonPayThisYr09() {
        return bonPayThisYr09;
    }

    public BigDecimal getBonPayThisYr10() {
        return bonPayThisYr10;
    }

    public BigDecimal getBonPayLastYr01() {
        return bonPayLastYr01;
    }

    public BigDecimal getBonPayLastYr02() {
        return bonPayLastYr02;
    }

    public BigDecimal getBonPayLastYr03() {
        return bonPayLastYr03;
    }

    public BigDecimal getBonPayLastYr04() {
        return bonPayLastYr04;
    }

    public BigDecimal getBonPayLastYr05() {
        return bonPayLastYr05;
    }

    public BigDecimal getBonPayLastYr06() {
        return bonPayLastYr06;
    }

    public BigDecimal getBonPayLastYr07() {
        return bonPayLastYr07;
    }

    public BigDecimal getBonPayLastYr08() {
        return bonPayLastYr08;
    }

    public BigDecimal getBonPayLastYr09() {
        return bonPayLastYr09;
    }

    public BigDecimal getBonPayLastYr10() {
        return bonPayLastYr10;
    }

    public BigDecimal getTotalBonus01() {
        return totalBonus01;
    }

    public BigDecimal getTotalBonus02() {
        return totalBonus02;
    }

    public BigDecimal getTotalBonus03() {
        return totalBonus03;
    }

    public BigDecimal getTotalBonus04() {
        return totalBonus04;
    }

    public BigDecimal getTotalBonus05() {
        return totalBonus05;
    }

    public BigDecimal getTotalBonus06() {
        return totalBonus06;
    }

    public BigDecimal getTotalBonus07() {
        return totalBonus07;
    }

    public BigDecimal getTotalBonus08() {
        return totalBonus08;
    }

    public BigDecimal getTotalBonus09() {
        return totalBonus09;
    }

    public BigDecimal getTotalBonus10() {
        return totalBonus10;
    }

    public String getTermid() {
        return termid;
    }

    public int getUser() {
        return user;
    }

    public int getTransactionDate() {
        return transactionDate;
    }

    public int getTransactionTime() {
        return transactionTime;
    }

    public String getUserProfile() {
        return userProfile;
    }

    public String getJobName() {
        return jobName;
    }

    public String getDatime() {
        return datime;
    }

    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }

    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }

    public void setCurrfrom(int currfrom) {
        this.currfrom = currfrom;
    }

    public void setCurrto(int currto) {
        this.currto = currto;
    }

    public void setDatetexc(String datetexc) {
        this.datetexc = datetexc;
    }

    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }

    public void setLongdesc01(String longdesc01) {
        this.longdesc01 = longdesc01;
    }

    public void setLongdesc02(String longdesc02) {
        this.longdesc02 = longdesc02;
    }

    public void setLongdesc03(String longdesc03) {
        this.longdesc03 = longdesc03;
    }

    public void setLongdesc04(String longdesc04) {
        this.longdesc04 = longdesc04;
    }

    public void setLongdesc05(String longdesc05) {
        this.longdesc05 = longdesc05;
    }

    public void setLongdesc06(String longdesc06) {
        this.longdesc06 = longdesc06;
    }

    public void setLongdesc07(String longdesc07) {
        this.longdesc07 = longdesc07;
    }

    public void setLongdesc08(String longdesc08) {
        this.longdesc08 = longdesc08;
    }

    public void setLongdesc09(String longdesc09) {
        this.longdesc09 = longdesc09;
    }

    public void setLongdesc10(String longdesc10) {
        this.longdesc10 = longdesc10;
    }

    public void setSumins01(BigDecimal sumins01) {
        this.sumins01 = sumins01;
    }

    public void setSumins02(BigDecimal sumins02) {
        this.sumins02 = sumins02;
    }

    public void setSumins03(BigDecimal sumins03) {
        this.sumins03 = sumins03;
    }

    public void setSumins04(BigDecimal sumins04) {
        this.sumins04 = sumins04;
    }

    public void setSumins05(BigDecimal sumins05) {
        this.sumins05 = sumins05;
    }

    public void setSumins06(BigDecimal sumins06) {
        this.sumins06 = sumins06;
    }

    public void setSumins07(BigDecimal sumins07) {
        this.sumins07 = sumins07;
    }

    public void setSumins08(BigDecimal sumins08) {
        this.sumins08 = sumins08;
    }

    public void setSumins09(BigDecimal sumins09) {
        this.sumins09 = sumins09;
    }

    public void setSumins10(BigDecimal sumins10) {
        this.sumins10 = sumins10;
    }

    public void setBonusCalcMeth01(String bonusCalcMeth01) {
        this.bonusCalcMeth01 = bonusCalcMeth01;
    }

    public void setBonusCalcMeth02(String bonusCalcMeth02) {
        this.bonusCalcMeth02 = bonusCalcMeth02;
    }

    public void setBonusCalcMeth03(String bonusCalcMeth03) {
        this.bonusCalcMeth03 = bonusCalcMeth03;
    }

    public void setBonusCalcMeth04(String bonusCalcMeth04) {
        this.bonusCalcMeth04 = bonusCalcMeth04;
    }

    public void setBonusCalcMeth05(String bonusCalcMeth05) {
        this.bonusCalcMeth05 = bonusCalcMeth05;
    }

    public void setBonusCalcMeth06(String bonusCalcMeth06) {
        this.bonusCalcMeth06 = bonusCalcMeth06;
    }

    public void setBonusCalcMeth07(String bonusCalcMeth07) {
        this.bonusCalcMeth07 = bonusCalcMeth07;
    }

    public void setBonusCalcMeth08(String bonusCalcMeth08) {
        this.bonusCalcMeth08 = bonusCalcMeth08;
    }

    public void setBonusCalcMeth09(String bonusCalcMeth09) {
        this.bonusCalcMeth09 = bonusCalcMeth09;
    }

    public void setBonusCalcMeth10(String bonusCalcMeth10) {
        this.bonusCalcMeth10 = bonusCalcMeth10;
    }

    public void setBonPayThisYr01(BigDecimal bonPayThisYr01) {
        this.bonPayThisYr01 = bonPayThisYr01;
    }

    public void setBonPayThisYr02(BigDecimal bonPayThisYr02) {
        this.bonPayThisYr02 = bonPayThisYr02;
    }

    public void setBonPayThisYr03(BigDecimal bonPayThisYr03) {
        this.bonPayThisYr03 = bonPayThisYr03;
    }

    public void setBonPayThisYr04(BigDecimal bonPayThisYr04) {
        this.bonPayThisYr04 = bonPayThisYr04;
    }

    public void setBonPayThisYr05(BigDecimal bonPayThisYr05) {
        this.bonPayThisYr05 = bonPayThisYr05;
    }

    public void setBonPayThisYr06(BigDecimal bonPayThisYr06) {
        this.bonPayThisYr06 = bonPayThisYr06;
    }

    public void setBonPayThisYr07(BigDecimal bonPayThisYr07) {
        this.bonPayThisYr07 = bonPayThisYr07;
    }

    public void setBonPayThisYr08(BigDecimal bonPayThisYr08) {
        this.bonPayThisYr08 = bonPayThisYr08;
    }

    public void setBonPayThisYr09(BigDecimal bonPayThisYr09) {
        this.bonPayThisYr09 = bonPayThisYr09;
    }

    public void setBonPayThisYr10(BigDecimal bonPayThisYr10) {
        this.bonPayThisYr10 = bonPayThisYr10;
    }

    public void setBonPayLastYr01(BigDecimal bonPayLastYr01) {
        this.bonPayLastYr01 = bonPayLastYr01;
    }

    public void setBonPayLastYr02(BigDecimal bonPayLastYr02) {
        this.bonPayLastYr02 = bonPayLastYr02;
    }

    public void setBonPayLastYr03(BigDecimal bonPayLastYr03) {
        this.bonPayLastYr03 = bonPayLastYr03;
    }

    public void setBonPayLastYr04(BigDecimal bonPayLastYr04) {
        this.bonPayLastYr04 = bonPayLastYr04;
    }

    public void setBonPayLastYr05(BigDecimal bonPayLastYr05) {
        this.bonPayLastYr05 = bonPayLastYr05;
    }

    public void setBonPayLastYr06(BigDecimal bonPayLastYr06) {
        this.bonPayLastYr06 = bonPayLastYr06;
    }

    public void setBonPayLastYr07(BigDecimal bonPayLastYr07) {
        this.bonPayLastYr07 = bonPayLastYr07;
    }

    public void setBonPayLastYr08(BigDecimal bonPayLastYr08) {
        this.bonPayLastYr08 = bonPayLastYr08;
    }

    public void setBonPayLastYr09(BigDecimal bonPayLastYr09) {
        this.bonPayLastYr09 = bonPayLastYr09;
    }

    public void setBonPayLastYr10(BigDecimal bonPayLastYr10) {
        this.bonPayLastYr10 = bonPayLastYr10;
    }

    public void setTotalBonus01(BigDecimal totalBonus01) {
        this.totalBonus01 = totalBonus01;
    }

    public void setTotalBonus02(BigDecimal totalBonus02) {
        this.totalBonus02 = totalBonus02;
    }

    public void setTotalBonus03(BigDecimal totalBonus03) {
        this.totalBonus03 = totalBonus03;
    }

    public void setTotalBonus04(BigDecimal totalBonus04) {
        this.totalBonus04 = totalBonus04;
    }

    public void setTotalBonus05(BigDecimal totalBonus05) {
        this.totalBonus05 = totalBonus05;
    }

    public void setTotalBonus06(BigDecimal totalBonus06) {
        this.totalBonus06 = totalBonus06;
    }

    public void setTotalBonus07(BigDecimal totalBonus07) {
        this.totalBonus07 = totalBonus07;
    }

    public void setTotalBonus08(BigDecimal totalBonus08) {
        this.totalBonus08 = totalBonus08;
    }

    public void setTotalBonus09(BigDecimal totalBonus09) {
        this.totalBonus09 = totalBonus09;
    }

    public void setTotalBonus10(BigDecimal totalBonus10) {
        this.totalBonus10 = totalBonus10;
    }

    public void setTermid(String termid) {
        this.termid = termid;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public void setTransactionDate(int transactionDate) {
        this.transactionDate = transactionDate;
    }

    public void setTransactionTime(int transactionTime) {
        this.transactionTime = transactionTime;
    }

    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public void setDatime(String datime) {
        this.datime = datime;
    }

    public void setLongdesc(int indx, String what) {

        switch (indx) {
        case 1:
            setLongdesc01(what);
            break;
        case 2:
            setLongdesc02(what);
            break;
        case 3:
            setLongdesc03(what);
            break;
        case 4:
            setLongdesc04(what);
            break;
        case 5:
            setLongdesc05(what);
            break;
        case 6:
            setLongdesc06(what);
            break;
        case 7:
            setLongdesc07(what);
            break;
        case 8:
            setLongdesc08(what);
            break;
        case 9:
            setLongdesc09(what);
            break;
        case 10:
            setLongdesc10(what);
            break;
        default:
            return;
        }
    }

    public void setSumins(int indx, BigDecimal what) {

        switch (indx) {
        case 1:
            setSumins01(what);
            break;
        case 2:
            setSumins02(what);
            break;
        case 3:
            setSumins03(what);
            break;
        case 4:
            setSumins04(what);
            break;
        case 5:
            setSumins05(what);
            break;
        case 6:
            setSumins06(what);
            break;
        case 7:
            setSumins07(what);
            break;
        case 8:
            setSumins08(what);
            break;
        case 9:
            setSumins09(what);
            break;
        case 10:
            setSumins10(what);
            break;
        default:
            return;
        }
    }

    public void setBcalmeth(int indx, String what) {

        switch (indx) {
        case 1:
            setBonusCalcMeth01(what);
            break;
        case 2:
            setBonusCalcMeth02(what);
            break;
        case 3:
            setBonusCalcMeth03(what);
            break;
        case 4:
            setBonusCalcMeth04(what);
            break;
        case 5:
            setBonusCalcMeth05(what);
            break;
        case 6:
            setBonusCalcMeth06(what);
            break;
        case 7:
            setBonusCalcMeth07(what);
            break;
        case 8:
            setBonusCalcMeth08(what);
            break;
        case 9:
            setBonusCalcMeth09(what);
            break;
        case 10:
            setBonusCalcMeth10(what);
            break;
        default:
            return;
        }
    }

    public void setBpayty(int indx, BigDecimal what) {

        switch (indx) {
        case 1:
            setBonPayThisYr01(what);
            break;
        case 2:
            setBonPayThisYr02(what);
            break;
        case 3:
            setBonPayThisYr03(what);
            break;
        case 4:
            setBonPayThisYr04(what);
            break;
        case 5:
            setBonPayThisYr05(what);
            break;
        case 6:
            setBonPayThisYr06(what);
            break;
        case 7:
            setBonPayThisYr07(what);
            break;
        case 8:
            setBonPayThisYr08(what);
            break;
        case 9:
            setBonPayThisYr09(what);
            break;
        case 10:
            setBonPayThisYr10(what);
            break;
        default:
            return;
        }
    }

    public void setBpayny(int indx, BigDecimal what) {

        switch (indx) {
        case 1:
            setBonPayLastYr01(what);
            break;
        case 2:
            setBonPayLastYr02(what);
            break;
        case 3:
            setBonPayLastYr03(what);
            break;
        case 4:
            setBonPayLastYr04(what);
            break;
        case 5:
            setBonPayLastYr05(what);
            break;
        case 6:
            setBonPayLastYr06(what);
            break;
        case 7:
            setBonPayLastYr07(what);
            break;
        case 8:
            setBonPayLastYr08(what);
            break;
        case 9:
            setBonPayLastYr09(what);
            break;
        case 10:
            setBonPayLastYr10(what);
            break;
        default:
            return;
        }

    }

    public void setTotbon(int indx, BigDecimal what) {

        switch (indx) {
        case 1:
            setTotalBonus01(what);
            break;
        case 2:
            setTotalBonus02(what);
            break;
        case 3:
            setTotalBonus03(what);
            break;
        case 4:
            setTotalBonus04(what);
            break;
        case 5:
            setTotalBonus05(what);
            break;
        case 6:
            setTotalBonus06(what);
            break;
        case 7:
            setTotalBonus07(what);
            break;
        case 8:
            setTotalBonus08(what);
            break;
        case 9:
            setTotalBonus09(what);
            break;
        case 10:
            setTotalBonus10(what);
            break;
        default:
            return;
        }

    }
}
