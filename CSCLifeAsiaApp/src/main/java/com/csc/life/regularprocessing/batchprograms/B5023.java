/*
 * File: B5023.java
 * Date: 29 August 2009 20:51:39
 * Author: Quipoz Limited
 *
 * Class transformed from B5023.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2Pojo;
import com.csc.fsu.general.procedures.Datcon2Utils;
import com.csc.fsu.general.procedures.Datcon3Pojo;
import com.csc.fsu.general.procedures.Datcon3Utils;
import com.csc.fsu.general.procedures.Datcon6;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.ZrdecplcPojo;
import com.csc.fsu.general.procedures.ZrdecplcUtils;
import com.csc.fsu.general.recordstructures.Datcon6rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.newbusiness.tablestructures.T6640rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.regularprocessing.dataaccess.dao.B5023TempDAO;
import com.csc.life.regularprocessing.dataaccess.dao.BonlpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.BonspfDAO;
import com.csc.life.regularprocessing.dataaccess.model.B5023DTO;
import com.csc.life.regularprocessing.dataaccess.model.Bonlpf;
import com.csc.life.regularprocessing.dataaccess.model.Bonspf;
import com.csc.life.regularprocessing.dataaccess.model.T5679T5679Rec;
import com.csc.life.regularprocessing.recordstructures.Bonusrec;
import com.csc.life.regularprocessing.tablestructures.T6639rec;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.smart.procedures.BatcdorUtil;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.SftlockUtil;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.SftlockRecBean;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
/**
* <pre>
*
*REMARKS.
*
*          POLICY ANNIVERSARY BONUSES
*          ==========================
*
*   This batch job processes contracts which are due a bonus.
*  It is decided that the mainline batch processing program (B5023  will
*  call the Reversionary Bonus subroutine dependent on its compone ts
*  method, this is table driven. This subroutine will write the
*  appropriate ACMV's and return the bonus value to the calling
*  mainline. The mainline can accumulate the returned values for
*  production of a notice.
*  Set up batching parameters call call subroutine 'BATCDOR' to op n the
*  batch file. Function will be 'OPEN'.
*  SQL programming will be needed to select components (COVR'S) wh re
*  the UNIT-STATEMENT-DATE on the COVR is AT LEAST 1 year earlier  han
*  the Bonus Declatation Date.
*  Include in your select those COVR's which have a valid status o
*  T5679. As an addition to this you will need to read the CHDR fo  the
*  relevant period and check that the CHDR was also a valid status at
*  the time of bonus declaration.
*  Finally you will have to use the passed parameters for contract from
*  and to, to see if the contract number falls between the range.
*  With the selected components now process the following one at a time.
*
*  For each of the COVR's selected check the following ...
*   Add 1 year to the Unit-statement-date on the COVR record and t en
*   check if the Currfrom & Currto dates on the COVR straddle this date.
*   IF Not, read through the old COVR records until the correct re ord
*    is found
*    IF Not found, get next record from SQL retrieve  and start ag in
*   IF correct COVR found, check status with T5679
*    IF incorrect status, get next record from SQL retrieve & repe t
*       checks
*    IF Not found, get next record from SQL retrieve  and start ag in
*
*   IF correct COVR and correct status,
*     Make sure the CHDR Currfrom & Currto dates straddle the
*      (COVR Unit-statement-date + 1 year)
*     IF Not, read through the old CHDR records until the correct  ecord
*      is found
*      IF Not found, get next record from SQL retrieve  and start  gain
*     IF correct CHDR found, check status with T5679
*      IF incorrect status, get next record from SQL retrieve & re eat
*         checks
*      IF Not found, get next record from SQL retrieve  and start  gain
*   IF correct CHDR and correct status,
*      process COVR and apply bonuses where applicable
*
*  For each of the COVR's selected add 1 to Control Total 1.
*  Read and Softlock the CHDR record using logical file CHDRLIF.
*  Add 1 to CHDR-TRANNO. Rewrt CHDR.
*  For contracts running on Policy Anniversary it will be  safe to
*  assume that at least one full years premiums has been paid, as
*  anniversary has to be at least one full year.
*  Depending on the way bonuses are declared will determine how th
*  processing at anniversary will occur. If it is annual in arrear  then
*  the job can always be included as part of Renewals reading the  onus
*  rate for the previous year. If however, there is a delay in dec aring
*  a bonus (highly probable in most cases) it is suggested you rem ve
*  the job step from Renewals to such time as when the bonus is
*  declared. There will be catch up involved but this will be auto ated
*  by the batch processing.
*  Billing Change will hinder the winding back of a contract to
*  determine what it looked like at the time of declaration. A tru
*  windback will be necessary using Currfrom and To dates together with
*  a check on Frequency now and at the Bonus Declaration Date.
*  When the correct COVR has been retrieved the bonus has to be
*  calculated. This job must read T6639 using the bonus method fro
*  T6640 concatenated with the premium status of the component for the
*  key. Call the Reversionary Bonus Subroutine using BONUSREC copy ook
*  as your linkage. The Function to be used in the call will be sp ces.
*  This is a new linkage to be set up via Specification VI.
*  The subroutine will return the Reversionary Bonus value for thi  year
*  together with Total Reversionary Bonus. The subroutine will hav
*  written the appropriate ACMV records.
*  Release the Softlocked record.
*  General Housekeeping
*  Write a PTRN recrod.
*                Contract Key from contract header
*                Tranno from CHDR
*                Effective Date param date (Not Bonus Decl Date)
*                Batch key as set up earlier.
*  Update the batch header by calling 'BATCUP' with a function of  RITS
*  and the following parameters:-
*                Transaction Count = 1
*                All other amounts zero
*                Batch key as set up earlier in the program.
*  -------
*  A Reversionary Bonus Notice is to be produced, base will supply a
*  shell document which a client site can then customise.
*  The Letters subsystem should be used.
*  Create a LETC  which at the same time creates
*  a new record BONS that will store the bonus notice details
*  including the details above. The BONS record
*  will be a new physical file that will have a logical view
* </pre>
*/
public class B5023 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5023");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaCoveragePremStatus = new FixedLengthStringData(24);
	private FixedLengthStringData[] wsaaT5679CvpremStat = FLSArrayPartOfStructure(12, 2, wsaaCoveragePremStatus, 0);

	private FixedLengthStringData wsaaCoverageRiskStatus = new FixedLengthStringData(24);
	private FixedLengthStringData[] wsaaT5679CvriskStat = FLSArrayPartOfStructure(12, 2, wsaaCoverageRiskStatus, 0);

	private FixedLengthStringData wsaaContractPremStatus = new FixedLengthStringData(24);
	private FixedLengthStringData[] wsaaT5679CnpremStat = FLSArrayPartOfStructure(12, 2, wsaaContractPremStatus, 0);

	private FixedLengthStringData wsaaContractRiskStatus = new FixedLengthStringData(24);
	private FixedLengthStringData[] wsaaT5679CnriskStat = FLSArrayPartOfStructure(12, 2, wsaaContractRiskStatus, 0);

	//private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaPreviousComp = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaCurrentComp = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCurrentCover = new FixedLengthStringData(2).isAPartOf(wsaaCurrentComp, 0);
	private FixedLengthStringData wsaaCurrentRider = new FixedLengthStringData(2).isAPartOf(wsaaCurrentComp, 2);

	private FixedLengthStringData wsaaLcrider = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaLcsubr = new FixedLengthStringData(8);
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;

	private FixedLengthStringData p5023ParmRecord = new FixedLengthStringData(32);
	private FixedLengthStringData p5023Chdrnum = new FixedLengthStringData(8).isAPartOf(p5023ParmRecord, 0);
	private FixedLengthStringData p5023Chdrnum1 = new FixedLengthStringData(8).isAPartOf(p5023ParmRecord, 8);
	private ZonedDecimalData p5023Datefrm = new ZonedDecimalData(8, 0).isAPartOf(p5023ParmRecord, 16);
	private ZonedDecimalData p5023Dateto = new ZonedDecimalData(8, 0).isAPartOf(p5023ParmRecord, 24);
	private Varcom varcom = new Varcom();
	private Batcuprec batcuprec = new Batcuprec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon6rec datcon6rec = new Datcon6rec();
	private Bonusrec bonusrec = new Bonusrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Tr384rec tr384rec = new Tr384rec();
	private T6639rec t6639rec = new T6639rec();
	private T6640rec t6640rec = new T6640rec();
	private T6625rec t6625rec = new T6625rec();
	private T5679T5679Rec t5679T5679RecInner = new T5679T5679Rec();
	private WsaaContractTypeInner wsaaContractTypeInner = new WsaaContractTypeInner();
	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private BonlpfDAO bonlpfDAO = getApplicationContext().getBean("bonlpfDAO", BonlpfDAO.class);
	private BonspfDAO bonspfDAO = getApplicationContext().getBean("bonspfDAO", BonspfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private B5023TempDAO b5023TempDAO = getApplicationContext().getBean("b5023TempDAO", B5023TempDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private Iterator<B5023DTO> iterator;
	private List<B5023DTO> b5023DTOList;
	private Map<String, List<Covrpf>> covrMap;
	private Map<String, List<Bonlpf>> bonlMap;
	private Map<String, List<Bonspf>> bonsMap;
	private Map<String, List<Chdrpf>> chdrMap;
	private List<Chdrpf> chdrUpdateList;
	private List<Ptrnpf> ptrnpfListInsert;
	private List<Bonlpf> bonlpfListUpdate;
	private List<Bonlpf> bonlpfListInsert;
	private List<Bonspf> bonspfListUpdate;
	private List<Bonspf> bonspfListInsert;
    private boolean endOfFile = true;
    private B5023DTO covrpfInner = null;
    private String wsaaLastChdrnum;
    private int wsaaCbunstPlus;
    private boolean wsaaCovrStatusFlag;
    private boolean wsaaCovrRecStatus = false;
    private boolean wsaaProcessingStatus = false;
    private boolean breakFlag = false;
    private boolean wsaaContractLock = false;
    private boolean wsaaChdrStatusFlag = false;
    private List<String> wsaaBonlrvbLongdesc = new ArrayList<String>();
    private List<BigDecimal> wsaaBonlrvbSumins = new ArrayList<BigDecimal>();
    private List<String> wsaaBonlrvbBcalmeth = new ArrayList<String>();
    private List<BigDecimal> wsaaBonlrvbBpayty = new ArrayList<BigDecimal>();
    private List<BigDecimal> wsaaBonlrvbBpayny = new ArrayList<BigDecimal>();
    private List<BigDecimal> wsaaBonlrvbTotbon = new ArrayList<BigDecimal>();
    private int tempCount = 0;
    private Ptrnpf lastPtrnpf;
    private Bonspf lastBonspf;
    private Chdrpf chdrlif;
    private int batchID = 0;
    private String wsaaBonusMethod;
    private String wsaaBonusMethod1="****";    
    private int wsaaSqlFrmdate;
    private int wsaaSqlTodate;
    private int wsaaToday;
    private String wsaaCntcurr;
    private int wsaaTranno;
    private String wsaaCowncoy;
    private String wsaaCownnum;
    private String wsaaChdrmnaStatcode;
    private String wsaaChdrmnaPstatcode;
    private int batchExtractSize;
    private int RecExtractSize=1000;
    private int  MaxSize=5000;
    private int countCt01=0;
    private BigDecimal countCt02=BigDecimal.ZERO;
    private int countCt03=0;
    private ZonedDecimalData wsaaTransactionDate = new ZonedDecimalData(6, 0).init(0).setUnsigned();
    private boolean hasRecords = false;
    private static final Logger LOGGER = LoggerFactory.getLogger(B5023.class);
    
    private Datcon3Utils datcon3Utils = getApplicationContext().getBean("datcon3Utils", Datcon3Utils.class);
    private Datcon2Utils datcon2Utils = getApplicationContext().getBean("datcon2Utils", Datcon2Utils.class);
	private SftlockUtil sftlockUtil = new SftlockUtil();
	private SftlockRecBean sftlockRecBean = null;
	private Datcon3Pojo datcon3Pojo = null;
	private Datcon2Pojo datcon2Pojo = null;
	private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
	private ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit190,
		readPrimary420,
		readNextRecord460,
		exit490,
		fetchRecord510,
		eof580,
		exit590,
		exit1090,
		continue1750,
		exit1790,
		exit4090,
		writeRecord5030,
		writeRecord6030
	}

	public B5023() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		try {
			main110();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void main110()
	{
	try
	{
		initialise200();
		readFirstRecord300();
		if(iterator!=null && iterator.hasNext()){
		    hasRecords = true;
		}
		while (!endOfFile) {
			processRecs400();
			if (runparmrec.cmtnumber.gt(runparmrec.cmtpoint)) {
			    commit3500();
			}
		}
		if (hasRecords) {
		    commit3500();
		}
		finished900();
		goTo(GotoLabel.exit190);
	}
	catch(Exception ex)
	{
		LOGGER.error("error has occured in main110", ex);
	}
}

//changed for ILIFE-2893
protected void initialise200()
	{
	try
	{
		initialise210();
		openBatch220();
		readT5679230();
		setSqlSelectionDates240();
	}
	catch(Exception ex)
	{
		LOGGER.error("error has occured in initialise200", ex);
	}
}

protected void initialise210()
	{
		p5023ParmRecord.set(conjobrec.params);
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		varcom.vrcmUser.set(runparmrec.user);
		wsaaCoveragePremStatus.set(SPACES);
		wsaaCoverageRiskStatus.set(SPACES);
		wsaaContractPremStatus.set(SPACES);
		wsaaContractRiskStatus.set(SPACES);
		wsaaCurrentComp.set(SPACES);
		wsaaTransactionDate.set(runparmrec.effdate);
		// Add by yy for chunk read 
		// runparmrec.runparm1.set(bprdIO.getSystemParam02()); in Bchprogrun
        if (runparmrec.runparm1.isNumeric()) {
            if (runparmrec.runparm1.toInt() > 0) {
                batchExtractSize = runparmrec.runparm1.toInt();
            } else {
                batchExtractSize = runparmrec.cmtpoint.toInt();
            }
        } else {
            batchExtractSize = runparmrec.cmtpoint.toInt();
        } 
        
        if (batchExtractSize>=MaxSize)
        	batchExtractSize=RecExtractSize;
	}

protected void openBatch220(){
	batcdorrec.function.set("OPEN");
	batcdorrec.tranid.set(runparmrec.tranid);
	batcdorrec.company.set(runparmrec.company);
	batcdorrec.branch.set(runparmrec.batcbranch);
	batcdorrec.actyear.set(runparmrec.acctyear);
	batcdorrec.actmonth.set(runparmrec.acctmonth);
	batcdorrec.trcde.set(runparmrec.transcode);
	BatcdorUtil.getInstance().process(batcdorrec);
	if (isNE(batcdorrec.statuz, varcom.oK)) {
		conerrrec.statuz.set(batcdorrec.statuz);
		conerrrec.params.set(batcdorrec.batcdorRec);
		systemError005();
	}
}

protected void readT5679230(){
    Itempf itempf = new Itempf();
    itempf.setItempfx("IT");
	itempf.setItemcoy(runparmrec.company.toString());
	itempf.setItemtabl("T5679");
	itempf.setItemitem(runparmrec.transcode.toString());
    itempf = itemDAO.getItempfRecord(itempf);
    if(itempf != null){
    	t5679T5679RecInner.getT5679T5679Rec().set(StringUtil.rawToString(itempf.getGenarea()));
    	return;
    }else{	
        conerrrec.params.set("t5679 " + runparmrec.transcode);
        databaseError006();
    }
}

protected void setSqlSelectionDates240()
	{
		/* subtract 1 year from the bonus from & to effective dates*/
		datcon2Pojo = new Datcon2Pojo();
		datcon2Pojo.setFreqFactor(-1);
		datcon2Pojo.setIntDate1(p5023Datefrm.toString());
		datcon2Pojo.setFrequency("01");
		datcon2Utils.calDatcon2(datcon2Pojo);
		if (!"****".equals(datcon2Pojo.getStatuz())) {
			databaseError006();
		}
		wsaaSqlFrmdate= Integer.parseInt(datcon2Pojo.getIntDate2());
		
		datcon2Pojo = new Datcon2Pojo();
		datcon2Pojo.setFreqFactor(-1);
		datcon2Pojo.setIntDate1(p5023Dateto.toString());
		datcon2Pojo.setFrequency("01");
		datcon2Utils.calDatcon2(datcon2Pojo);
		if (!"****".equals(datcon2Pojo.getStatuz())) {
			databaseError006();
		}
		wsaaSqlTodate = Integer.parseInt(datcon2Pojo.getIntDate2());
		/*    Access today's date using DATCON1.                           */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			conerrrec.params.set(datcon1rec.datcon1Rec);
			conerrrec.statuz.set(datcon1rec.statuz);
			databaseError006();
		}
		
		wsaaToday = datcon1rec.intDate.toInt();
	}
//changed for ILIFE-2893
protected void readFirstRecord300()
 {
	try
	{
        String coy = runparmrec.company.toString();
        b5023TempDAO.initTempTable(coy, p5023Chdrnum.toString(), p5023Chdrnum1.toString(),
                wsaaSqlFrmdate, wsaaSqlTodate, t5679T5679RecInner);
        readRecords();
	}
	catch(Exception ex)
	{
		LOGGER.error("error has occured in readFirstRecord300", ex);
	}
}


private void readRecords(){
	
	try
	{
    String coy = runparmrec.company.toString();
    b5023DTOList = b5023TempDAO.searchRacdResult(batchExtractSize, batchID);
    batchID++;
    if (b5023DTOList != null && b5023DTOList.size() > 0) {
        iterator = b5023DTOList.iterator();
        Set<String> chdrnumSet = new HashSet<String>();
        for(B5023DTO b5023DTO:b5023DTOList){
            chdrnumSet.add(b5023DTO.getChdrnum());
        }
        List<String> chdrList = new ArrayList<String>(chdrnumSet);
        covrMap = covrpfDAO.searchCovrMap(coy, chdrList);
        bonlMap = bonlpfDAO.searchBonlpf(coy, chdrList);
        chdrMap = chdrpfDAO.searchChdrpf(coy, chdrList);
        bonsMap = bonspfDAO.searchBonspf(coy, chdrList);
        endOfFile = false;
    } else {
        endOfFile = true;
    }
	}
	catch(Exception ex)
	{
		LOGGER.error("error has occured in readRecords", ex);
	}
}

protected void processRecs400()
	{      
        GotoLabel nextMethod = GotoLabel.DEFAULT;
        while (true) {
        try {
            switch (nextMethod) {
            case DEFAULT:
                start410();
            case readPrimary420:
                readPrimary420();
            case exit490:
            }
            break;
        }
        catch (GOTOException e){
            nextMethod = (GotoLabel) e.getNextMethod();
        }
    }
        
}
protected void start410()
{
	try
	{
    updateReqd004();
	}
	catch(Exception ex)
	{
		LOGGER.error("error has occured in start410", ex);
	}
}

    private void commit3500() {
		try
		{
        if (chdrUpdateList != null && chdrUpdateList.size() > 0) {
            chdrpfDAO.updateChdrTrannoByUniqueNo(chdrUpdateList);
            chdrUpdateList.clear();
        }
        if (ptrnpfListInsert != null && ptrnpfListInsert.size() > 0) {
            ptrnpfDAO.insertPtrnPF(ptrnpfListInsert);
            ptrnpfListInsert.clear();
        }
        bonlpfDAO.updateBonlpf(bonlpfListUpdate);
        bonlpfDAO.insertBonlpf(bonlpfListInsert);
        bonspfDAO.updateBonspf(bonspfListUpdate);
        bonspfDAO.insertBonspf(bonspfListInsert);
        /* Update control total - we have just got ourselves a COVR !!!!! */
        if (countCt01 > 0) {
            contotrec.totno.set(ct01);
            contotrec.totval.set(countCt01);
            countCt01 = 0;
            callContot001();
        }
        if (countCt02.compareTo(BigDecimal.ZERO) > 0) {
            contotrec.totno.set(ct02);
            contotrec.totval.set(countCt02);
            countCt02 = BigDecimal.ZERO;
            callContot001();
        }
        if (countCt03 > 0) {
            contotrec.totno.set(ct03);
            contotrec.totval.set(countCt03);
            countCt03 = 0;
            callContot001();
        }
        
		}
		catch(Exception ex)
		{
			LOGGER.error("error has occured in commit3500", ex);
		}
    }

protected void readPrimary420()
 {
	try
	{
        fetchPrimary500();
        /* check for E-O-F and depending on whether we processed any */
        /* records or not, either write BONLRVB rec and exit or just */
        /* exit. */
        if (endOfFile) {
            if (wsaaLastChdrnum != null && !wsaaLastChdrnum.isEmpty() && !wsaaContractLock) {
                /* if WSAA-LAST-CHDRNUM not spaces, then we have processed at */
                /* least 1 contract */
                writeBonlrvbRec6000();
                unlockSoftlock7000();
            }
            goTo(GotoLabel.exit490);
        }
        if (!covrpfInner.getChdrnum().equals(wsaaLastChdrnum)
                && (wsaaLastChdrnum != null && !wsaaLastChdrnum.isEmpty()) && !wsaaContractLock) {
            /* Contract number has changed so we wish to write the BONLRVB */
            /* record we have been building up and UNLOCK contract header. */
            /* The not = spaces check is here to make sure we don't trip up */
            /* on the 1st time through. */
            writeBonlrvbRec6000();
            unlockSoftlock7000();
        }
        updateChdrTranno1000();
        /* If the contract is locked by another transaction then we must */
        /* ignore this contract and go and read the next COVR record. */
        if (wsaaContractLock) {
            goTo(GotoLabel.readPrimary420);
        }
        if (!wsaaCovrRecStatus) {
            goTo(GotoLabel.readPrimary420);
        }
        countCt01++;
        /* PERFORM 2000-READ-T6639-T6640-T6634. */
        readT6639T6640Tr3842000();
        callBonusSubroutine3000();
        writePtrn4000();
        writeLetcBons5000();
	}
	catch(Exception ex)
	{
		LOGGER.error("error has occured in readPrimary420", ex);
	}
  }


protected void fetchPrimary500()
{
	try
	{
        if(!iterator.hasNext()){
            readRecords();
        }
        while(iterator.hasNext()){
            covrpfInner = iterator.next();
            
            /* If the COVR record just read is for the Contract which is       */
            /* already locked by another transaction, then we must continue    */
            /* reading through the COVR records until there is a change in     */
            /* contract number.                                                */
            if (covrpfInner.getChdrnum().equals(wsaaLastChdrnum)
            && wsaaContractLock) {
               continue;
            }
            /* we need to check here that the CURRFROM & CURRTO dates on the*/
            /* COVR record read span the correct period for the bonus - if*/
            /* not, we need to go backwards until we find the correct record*/
            checkCovrDates600();
            if (!wsaaCovrRecStatus) {
               continue;
            }
            return;
        }
        
	}
	catch(Exception ex)
	{
		LOGGER.error("error has occured in fetchPrimary500", ex);
	}
}

protected void checkCovrDates600()
 {
    try
    {
        /* Add 1 year to CBUNST date from contract and then check that */
        /* it is between the CURRFROM & CURRTO dates of this record */
        /* - if not, read backwards through COVR validflag '2' records */
        /* to get get COVR for correct period. */
        wsaaCovrRecStatus = false;
        wsaaCoveragePremStatus.set(t5679T5679RecInner.getT5679CovPremStats());
        wsaaCoverageRiskStatus.set(t5679T5679RecInner.getT5679CovRiskStats());
      
        datcon2Pojo = new Datcon2Pojo();
		datcon2Pojo.setFreqFactor(1);
		datcon2Pojo.setIntDate1(String.valueOf(covrpfInner.getCbunst()));
		datcon2Pojo.setFrequency("01");
		datcon2Utils.calDatcon2(datcon2Pojo);
		if (!"****".equals(datcon2Pojo.getStatuz())) {
			databaseError006();
		}
		
        wsaaCbunstPlus = Integer.parseInt(datcon2Pojo.getIntDate2());
        if ((wsaaCbunstPlus >= covrpfInner.getCurrfrom()) && (wsaaCbunstPlus <= covrpfInner.getCurrto())) {
            /* then we have the correct COVR record for our bonus period */
            wsaaProcessingStatus = true;
            wsaaCovrRecStatus = true;
            return;
        }
        /* IF we get here then its the wrong COVR in the SQL retrieve so */
        /* we need to read backwards through the COVRRNL to get correct */
        /* record, starting at the record we have from the SQL query, */
        /* the only one with a VALIDFLAG '1'. */
        wsaaCovrRecStatus = false;
        Covrpf currCovr = null;
        String chdrnum = covrpfInner.getChdrnum();
        if (covrMap != null && covrMap.containsKey(chdrnum)) {
            List<Covrpf> covrpfList = covrMap.get(chdrnum);
            currCovr = covrpfList.remove(0);
            for (Covrpf c : covrpfList) {
                if (c.getLife().equals(covrpfInner.getLife()) && c.getRider().equals(covrpfInner.getRider())
                        && c.getCoverage().equals(covrpfInner.getCoverage())
                        && c.getPlanSuffix() == covrpfInner.getPlnsfx()) {
                    checkCovrStatus700(c);
                    currCovr = c;
                }
                if (breakFlag) {
                    break;
                }
            }
        }
        if ((!breakFlag) && (!wsaaCovrRecStatus)) {
            conerrrec.params.set(currCovr);
            databaseError006();
        }

        /* Need to make note of all COVR rec variables we need for */
        /* processing later on ( because the data from the SQL-read */
        /* COVR record may have changed since this record was valid */
        if (currCovr != null) {
            covrpfInner.setStatcode(currCovr.getStatcode());
            covrpfInner.setPstatcode(currCovr.getPstatcode());
            covrpfInner.setCrrcd(currCovr.getCrrcd());
            covrpfInner.setRcesdte(currCovr.getRiskCessDate());
            covrpfInner.setCrtable(currCovr.getCrtable());
            covrpfInner.setSumins(currCovr.getSumins());
        }
        
    }
    catch(Exception ex)
    {
    	LOGGER.error("error has occured in checkCovrDates600", ex);
    }
  }
//changed for ILIFE-2893
protected void checkCovrStatus700(Covrpf c)
 {
	try
	{
        /* we are going to read backwards through the COVRRNL file but */
        /* we will use a function of NEXTR because the file is defined */
        /* as LIFO ... Last in, First out. */

        if (wsaaCbunstPlus < c.getCurrfrom() && wsaaCbunstPlus > c.getCurrto()) {
            /* dates don't match so go and get next record */
            return;
        }
        /* now check whether coverage statii are valid */
        int wsaaStatCount = 0;
        wsaaCovrStatusFlag = false;
        while (wsaaStatCount != 12) {
            wsaaStatCount++;
            if (c.getStatcode().equals(wsaaT5679CvriskStat[wsaaStatCount])) {
                wsaaCovrStatusFlag = true;
                break;
            }
        }
        if (!wsaaCovrStatusFlag) {
            /* invalid coverage status so ignore contract/coverage and */
            /* we'll go and get next coverage record from SQL */
            breakFlag = true;
            wsaaProcessingStatus = false;
            return;
        }
        /* get here so coverage prem status ok - now check risk status */
        wsaaCovrStatusFlag = false;
        wsaaStatCount = 0;
        while (wsaaStatCount != 12) {
            wsaaStatCount++;
            if (c.getPstatcode().equals(wsaaT5679CvpremStat[wsaaStatCount])) {
                wsaaCovrStatusFlag = true;
                wsaaProcessingStatus = true;
                wsaaCovrRecStatus = true;
                breakFlag = true;
                return;
            }
        }
        wsaaProcessingStatus = false;
        
	}
	catch(Exception ex)
	{
		LOGGER.error("error has occured in checkCovrStatus700", ex);
	}
  }

protected void finished900()
 {
        chdrUpdateList = null;
        ptrnpfListInsert = null;
        bonlpfListInsert = null;
        bonlpfListUpdate = null;
        bonspfListUpdate = null;
        bonspfListInsert = null;
       
        if (b5023DTOList != null) {
            b5023DTOList.clear();
        }
        b5023DTOList = null;
        if (covrMap != null) {
            covrMap.clear();
        }
        covrMap = null;
        if (bonlMap != null) {
            bonlMap.clear();
        }
        bonlMap = null;
        if (bonsMap != null) {
            bonsMap.clear();
        }
        bonsMap = null;
        if (chdrMap != null) {
            chdrMap.clear();
        }
        chdrMap = null;
        if (chdrUpdateList != null) {
            chdrUpdateList.clear();
        }
        chdrUpdateList = null;
        if (ptrnpfListInsert != null) {
            ptrnpfListInsert.clear();
        }
        ptrnpfListInsert = null;
        if (bonlpfListUpdate != null) {
            bonlpfListUpdate.clear();
        }
        bonlpfListUpdate = null;
        if (bonlpfListInsert != null) {
            bonlpfListInsert.clear();
        }
        bonlpfListInsert = null;
        if (bonspfListUpdate != null) {
            bonspfListUpdate.clear();
        }
        bonspfListUpdate = null;
        if (bonspfListInsert != null) {
            bonspfListInsert.clear();
        }
        bonspfListInsert = null;
        if (wsaaBonlrvbLongdesc != null) {
            wsaaBonlrvbLongdesc.clear();
        }
        wsaaBonlrvbLongdesc = null;
        if (wsaaBonlrvbSumins != null) {
            wsaaBonlrvbSumins.clear();
        }
        wsaaBonlrvbSumins = null;
        if (wsaaBonlrvbBcalmeth != null) {
            wsaaBonlrvbBcalmeth.clear();
        }
        wsaaBonlrvbBcalmeth = null;
        if (wsaaBonlrvbBpayty != null) {
            wsaaBonlrvbBpayty.clear();
        }
        wsaaBonlrvbBpayty = null;
        if (wsaaBonlrvbBpayny != null) {
            wsaaBonlrvbBpayny.clear();
        }
        wsaaBonlrvbBpayny = null;
        if (wsaaBonlrvbTotbon != null) {
            wsaaBonlrvbTotbon.clear();
        }
        wsaaBonlrvbTotbon = null;

        closeFiles910();
        closeBatch920();
    }

protected void closeFiles910()
	{
		/*   Close the cursor*/
		//getAppVars().freeDBConnectionIgnoreErr(sqlcovr1conn, sqlcovr1ps, sqlcovr1rs);
	}

protected void closeBatch920()
	{
		batcdorrec.function.set("CLOSE");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(runparmrec.transcode);
		BatcdorUtil.getInstance().process(batcdorrec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			conerrrec.params.set(batcdorrec.batcdorRec);
			conerrrec.statuz.set(batcdorrec.statuz);
			systemError005();
		}
	}

protected void updateChdrTranno1000()
 {
	try
	{
        if (covrpfInner.getChdrnum().equals(wsaaLastChdrnum)) {
            return;
        }
        if (lockChdr1020()) {
            checkAndUpdateCtrt1030();
            saveFields1040();
        }
        
	}
	catch(Exception ex)
	{
		LOGGER.error("error has occured in updateChdrTranno1000", ex);
	}
}

	/**
	* <pre>
	* we are processing a new contract number, so lock the contract
	*  header record.
	* </pre>
	*/
protected boolean lockChdr1020(){
	sftlockRecBean = new SftlockRecBean();
	sftlockRecBean.setFunction("LOCK");
	sftlockRecBean.setCompany(runparmrec.company.toString().trim());
	sftlockRecBean.setEnttyp("CH");
	sftlockRecBean.setEntity(covrpfInner.getChdrnum());
	sftlockRecBean.setTransaction(runparmrec.transcode.toString());
	sftlockRecBean.setUser(runparmrec.user.toString());
	sftlockUtil.process(sftlockRecBean, appVars);
	if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())
		&& !"LOCK".equals(sftlockRecBean.getStatuz())) {
		conerrrec.statuz.set(sftlockRecBean.getStatuz());
		systemError005();
	}
	/* IF SFTL-STATUZ              = 'LOCK'                         */
	/*     MOVE SFTL-SFTLOCK-REC   TO CONR-PARAMS                   */
	/*     MOVE SFTL-STATUZ        TO CONR-STATUZ                   */
	/*     PERFORM 005-SYSTEM-ERROR.                                */
	/* If the contract is already locked by another transaction then   */
	/* Display an error message, increment the control total by 1      */
	/* and continue processing.                                        */
	if ("LOCK".equals(sftlockRecBean.getStatuz())) {
	    wsaaContractLock = true;
		wsaaLastChdrnum = covrpfInner.getChdrnum();
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(covrpfInner.getChdrnum());
		stringVariable1.addExpression(" Header record locked.");
		stringVariable1.setStringInto(conlogrec.message);
		conlogrec.error.set(sftlockRecBean.getStatuz());
		callConlog003();
		countCt03++;
		return false;
	}
	return true;
}

protected void checkAndUpdateCtrt1030()
 {
	
	try
	{
        List<Chdrpf> chdrList = chdrMap.get(covrpfInner.getChdrnum());
        chdrlif = chdrList.get(0);
        /* check contract CURRFROM/CURRTO dates and contract statii */
        if (wsaaCbunstPlus >= chdrlif.getCurrfrom() && wsaaCbunstPlus < chdrlif.getCurrto()) {
            /* don't need to check statii here becasuse this record was */
            /* checked during the SQL retrieve */
            wsaaProcessingStatus = true;
        } else {
            /* Need to read through the old CHDR records for this contract */
            /* until we find the correct CURRFROM/CURRTO period - we need */
            /* to then check the contract statii to make sure contract was */
            /* 'IN FORCE' during our Bonus period. - otherwise NO BONUS !! */
            checkOldChdrs1500(chdrList);
        }
        if (!wsaaProcessingStatus) {
            /* Haven't found a valid contract so unlock all records and drop */
            /* out and get next COVR from SQL retrieve */
            wsaaCovrRecStatus = false;
            sftlockRecBean = new SftlockRecBean();
            sftlockRecBean.setFunction("UNLK");
        	sftlockRecBean.setCompany(runparmrec.company.toString().trim());
        	sftlockRecBean.setEnttyp("CH");
        	sftlockRecBean.setEntity(covrpfInner.getChdrnum());
        	sftlockRecBean.setTransaction(runparmrec.transcode.toString());
        	sftlockRecBean.setUser(runparmrec.user.toString());
        	sftlockUtil.process(sftlockRecBean, appVars);
        	if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())
        		&& !varcom.mrnf.toString().equals(sftlockRecBean.getStatuz())) {
        			conerrrec.statuz.set(sftlockRecBean.getStatuz());
        			systemError005();
        	}
            
        }
        if (chdrUpdateList == null) {
            chdrUpdateList = new ArrayList<Chdrpf>();
        }
        chdrlif.setTranno(chdrlif.getTranno() + 1);
        chdrUpdateList.add(chdrlif);
        
	}
	catch(Exception ex)
	{
		LOGGER.error("error has occured in checkAndUpdateCtrt1030", ex);
	}
 }

protected void saveFields1040()
	{
		/* store current contract number and reset counter to zeros.*/
		wsaaLastChdrnum = covrpfInner.getChdrnum();
		wsaaCovrRecStatus = true;
		tempCount = 0;
		wsaaCntcurr = chdrlif.getCntcurr();
		wsaaContractTypeInner.wsaaCnttype.set(chdrlif.getCnttype());
		wsaaTranno = chdrlif.getTranno();
		if(chdrlif.getCowncoy()!=null){
		    wsaaCowncoy = chdrlif.getCowncoy().toString();
		}else{
		    wsaaCowncoy = "";
		}
		wsaaCownnum = chdrlif.getCownnum();
		wsaaContractLock = false;;
	}

protected void checkOldChdrs1500(List<Chdrpf> chdrList)
 {
        /* Do a BEGN on CHDRMNA file to get most recent Validflag '1' */
        /* record ( because LF is defined as LIFO ) then do NEXTR */
        /* to start checking the validflag '2' records. */
try
{
        wsaaContractPremStatus.set(t5679T5679RecInner.getT5679CnPremStats());
        wsaaContractRiskStatus.set(t5679T5679RecInner.getT5679CnRiskStats());
        for (int i = 0; i < chdrList.size(); i++) {
            Chdrpf c = chdrList.get(i);
            wsaaChdrmnaStatcode = c.getStatcode();
            wsaaChdrmnaPstatcode = c.getPstcde();

            if ((wsaaCbunstPlus < c.getCurrfrom()) || (wsaaCbunstPlus > c.getCurrto())) {
                if (wsaaCbunstPlus > c.getCurrto()) {
                    /* dates don't match but there is no CHDR record for our */
                    /* declaration period so we use 1st record after DCL date */
                    /* ie the previous record read */
                    /* now check whether contract statii are valid */
                    int wsaaStatCount = 0;
                    wsaaChdrStatusFlag = false;
                    while (wsaaStatCount!=12) {
                        wsaaStatCount++;
                        if ((isEQ(wsaaChdrmnaStatcode, wsaaT5679CnriskStat[wsaaStatCount]))) {
                            wsaaStatCount = 12;
                            wsaaChdrStatusFlag = true;
                        }
                    }

                    if (!wsaaChdrStatusFlag) {
                        /*
                         * invalid contract status so ignore contract/coverage
                         * and
                         */
                        /* we'll go and get next coverage record from SQL */
                        wsaaProcessingStatus = false;
                        return;
                    }
                    /* get here so contract status ok - now check PREM status */
                    wsaaChdrStatusFlag = false;
                    wsaaStatCount = 0;
                    while (wsaaStatCount != 12) {
                        wsaaStatCount++;
                        if ((isEQ(wsaaChdrmnaPstatcode, wsaaT5679CnpremStat[wsaaStatCount]))) {
                            wsaaStatCount = 12;
                            wsaaChdrStatusFlag = true;
                            wsaaProcessingStatus = true;
                            return;
                        }
                    }

                    wsaaProcessingStatus = false;
                }
                /* dates don't match so go and get next record */
            }
        }
}
	catch(Exception ex)
	{
		LOGGER.error("error has occured in checkOldChdrs1500", ex);
	}
}

	/**
	* <pre>
	*2000-READ-T6639-T6640-T6634 SECTION.
	* </pre>
	*/
protected void readT6639T6640Tr3842000()
	{
		readT66402010();
		readT66392020();
		readTr3842030();
	}

// changed for ILIFE-2893
protected void readT66402010(){
    List<Itempf> itempfList = itemDAO.getItdmByFrmdate(runparmrec.company.toString(),"T6640",covrpfInner.getCrtable(),runparmrec.effdate.toInt());
    if(!itempfList.isEmpty()){
    	 t6640rec.t6640Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
         wsaaLcrider.set(t6640rec.lcrider);
         wsaaLcsubr.set(t6640rec.lcsubr);
         wsaaBonusMethod = t6640rec.revBonusMeth.toString();
         return;
    }
    checkWhichTable2100();
}

//changed for ILIFE-2893
protected void readT66392020()
 {
        /* MOVE T6640-REV-BONUS-METH TO WSAA-T6640-BONUS. */
	 	String t6639key="";
		String t6639key1="";
	   
	   	t6639key = wsaaBonusMethod + covrpfInner.getPstatcode();	  
	   	t6639key1 = wsaaBonusMethod1 + covrpfInner.getPstatcode();
	   	List<Itempf> itempfList = itemDAO.getItdmByFrmdate(runparmrec.company.toString(),"T6639",t6639key,runparmrec.effdate.toInt());
	   	if(!itempfList.isEmpty()){
		   	t6639rec.t6639Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
	        return;
	   	}else{
	   		itempfList = itemDAO.getItdmByFrmdate(runparmrec.company.toString(),"T6639",t6639key1,runparmrec.effdate.toInt());
	   		if(!itempfList.isEmpty()){
			   	t6639rec.t6639Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		        return;
		   	}
	   	}
        conerrrec.params.set("t6639 " + t6639key);
        conerrrec.statuz.set("G408");
        databaseError006();
        
    }

	/**
	* <pre>
	*2030-READ-T6634.
	* </pre>
	*/
protected void readTr3842030()
	{
		/* build entry key to T6634 from contract type & transaction code*/
		/* - must find out length of contract type first*/
		String tr384ItemKey = "";
		if ((isEQ(wsaaContractTypeInner.wsaaSecondChar, SPACES))) {
			/*     MOVE WSAA-CNTTYPE-1     TO WSAA-T6634-CNTLEN1            */
			/*     MOVE PARM-TRANSCODE     TO WSAA-T6634-TCODE-1            */
			tr384ItemKey = wsaaContractTypeInner.wsaaCnttype1.toString().trim().concat(runparmrec.transcode.toString().trim());
		}
		else {
			if ((isEQ(wsaaContractTypeInner.wsaaThirdChar, SPACES))) {
				/*     MOVE WSAA-CNTTYPE-2     TO WSAA-T6634-CNTLEN2            */
				/*     MOVE PARM-TRANSCODE     TO WSAA-T6634-TCODE-2            */
				tr384ItemKey = wsaaContractTypeInner.wsaaCnttype2.toString().trim().concat(runparmrec.transcode.toString().trim());
			}
			else {
				if ((isEQ(wsaaContractTypeInner.wsaaFourthChar, SPACES))) {
					/*     MOVE WSAA-CNTTYPE-3     TO WSAA-T6634-CNTLEN3            */
					/*     MOVE PARM-TRANSCODE     TO WSAA-T6634-TCODE-3            */
					tr384ItemKey = wsaaContractTypeInner.wsaaCnttype3.toString().trim().concat(runparmrec.transcode.toString().trim());
				}
				else {
					/*     MOVE WSAA-CNTTYPE       TO WSAA-T6634-CNTLEN4            */
					/*     MOVE PARM-TRANSCODE     TO WSAA-T6634-TCODE-4            */
					tr384ItemKey = wsaaContractTypeInner.wsaaCnttype.toString().trim().concat(runparmrec.transcode.toString().trim());
				}
			}
		}
        Itempf itempf = new Itempf();
	    itempf.setItempfx("IT");
		itempf.setItemcoy(runparmrec.company.toString());
		itempf.setItemtabl("TR384");
		itempf.setItemitem(tr384ItemKey);
	    itempf = itemDAO.getItempfRecord(itempf);
	    if(itempf != null){
	    	tr384rec.tr384Rec.set(StringUtil.rawToString(itempf.getGenarea()));
            return;
	    }else{
	    	itempf = new Itempf();
		    itempf.setItempfx("IT");
			itempf.setItemcoy(runparmrec.company.toString());
			itempf.setItemtabl("TR384");
			itempf.setItemitem("***".concat(runparmrec.transcode.toString().trim()));
		    itempf = itemDAO.getItempfRecord(itempf);
		    if(itempf != null){
		    	tr384rec.tr384Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	            return;
		    }
	    }
        conerrrec.params.set("tr384 " + tr384ItemKey);
        conerrrec.statuz.set("G437");
        databaseError006();
        
	}

//changed for ILIFE-2893
protected void checkWhichTable2100(){
    List<Itempf> itempfList = itemDAO.getItdmByFrmdate(runparmrec.company.toString(),"T6625",covrpfInner.getCrtable(),runparmrec.effdate.toInt());
    if(!itempfList.isEmpty()){
    	t6625rec.t6625Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
        wsaaLcrider.set(SPACES);
        wsaaLcsubr.set(SPACES);
        wsaaBonusMethod = t6625rec.revBonusMeth.toString();
        return;
    }
    conerrrec.params.set("t6625 " + covrpfInner.getCrtable());
    conerrrec.statuz.set("H149");
    databaseError006();
}
//changed for ILIFE-2893
protected void callBonusSubroutine3000()
 {
        bonusrec.bonusRec.set(SPACES);
        bonusrec.rvBonusSa.set(ZERO);
        bonusrec.trmBonusSa.set(ZERO);
        bonusrec.intBonusSa.set(ZERO);
        bonusrec.extBonusSa.set(ZERO);
        bonusrec.rvBonusBon.set(ZERO);
        bonusrec.trmBonusBon.set(ZERO);
        bonusrec.intBonusBon.set(ZERO);
        bonusrec.rvBalLy.set(ZERO);
        bonusrec.extBonusBon.set(ZERO);
        /* BONS-BONUS-PERIOD */
        /* This is set to 1 since the program is 'policy anniversary' */
        /* and so each policy will earn its bonus yearly. */
        bonusrec.bonusPeriod.set(1);
        bonusrec.batckey.set(batcdorrec.batchkey);
        bonusrec.effectiveDate.set(wsaaCbunstPlus);
        bonusrec.language.set(runparmrec.language);
        bonusrec.transcd.set(runparmrec.transcode);
        bonusrec.tranno.set(wsaaTranno);
        bonusrec.cntcurr.set(wsaaCntcurr);
        bonusrec.cnttype.set(wsaaContractTypeInner.wsaaCnttype);
        bonusrec.chdrChdrcoy.set(covrpfInner.getChdrcoy());
        bonusrec.chdrChdrnum.set(covrpfInner.getChdrnum());
        bonusrec.lifeLife.set(covrpfInner.getLife());
        bonusrec.covrCoverage.set(covrpfInner.getCoverage());
        bonusrec.covrRider.set(covrpfInner.getRider());
        bonusrec.plnsfx.set(covrpfInner.getPlnsfx());
        bonusrec.premStatus.set(covrpfInner.getPstatcode());
        bonusrec.crtable.set(covrpfInner.getCrtable());
        bonusrec.sumin.set(covrpfInner.getSumins());
        bonusrec.allocMethod.set("P");
        /* MOVE T6640-REV-BONUS-METH TO BONS-BONUS-CALC-METH. */
        if (wsaaBonusMethod.trim().length()!=0)
           bonusrec.bonusCalcMeth.set(wsaaBonusMethod);
         
        
        bonusrec.user.set(runparmrec.user);
        bonusrec.unitStmtDate.set(covrpfInner.getCbunst());
        /* MOVE T6640-LCRIDER TO BONS-LOW-COST-RIDER. */
        bonusrec.lowCostRider.set(wsaaLcrider);
        /* MOVE T6640-LCSUBR TO BONS-LOW-COST-SUBR. */
        bonusrec.lowCostSubr.set(wsaaLcsubr);
        
        datcon3Pojo = new Datcon3Pojo();
		datcon3Pojo.setIntDate1(String.valueOf(covrpfInner.getCrrcd()));
		datcon3Pojo.setIntDate2(String.valueOf(wsaaCbunstPlus));
		datcon3Pojo.setFrequency("01");
		datcon3Utils.calDatcon3(datcon3Pojo);
		if (!"****".equals(datcon3Pojo.getStatuz())) {
            databaseError006();
        }
		bonusrec.termInForce.set(datcon3Pojo.getFreqFactor());
		datcon3Pojo = new Datcon3Pojo();
        datcon3Pojo.setIntDate1(String.valueOf(covrpfInner.getCrrcd()));
		datcon3Pojo.setIntDate2(String.valueOf(covrpfInner.getRcesdte()));
		datcon3Pojo.setFrequency("01");
		datcon3Utils.calDatcon3(datcon3Pojo);
		if (!"****".equals(datcon3Pojo.getStatuz())) {
            databaseError006();
        }
		
        bonusrec.duration.set(datcon3Pojo.getFreqFactor());
        if (isNE(t6639rec.revBonusProg,SPACE))
        {
	        callProgram(t6639rec.revBonusProg, bonusrec.bonusRec);
	        if (isNE(bonusrec.statuz, varcom.oK)) {
	            conerrrec.params.set(bonusrec.bonusRec);
	            //databaseError006();
	        }
        }
        else
        {
        	LOGGER.error("error has occured in t6639rec setup - revBonsProgram is empty ");
        	databaseError006();
        }
    }
//changed for ILIFE-2893
    protected void writePtrn4000() {
        if (lastPtrnpf!=null && covrpfInner.getChdrnum().equals(lastPtrnpf.getChdrnum())) {
            return;
        }
        writePtrn4020();
        updateBatchHeader4030();
    }

protected void writePtrn4020()
	{
        lastPtrnpf = new Ptrnpf();
		lastPtrnpf.setChdrcoy(covrpfInner.getChdrcoy());
		lastPtrnpf.setChdrpfx(batcdorrec.prefix.toString());
		lastPtrnpf.setChdrnum(covrpfInner.getChdrnum());
		lastPtrnpf.setTranno(chdrlif.getTranno());
		lastPtrnpf.setTrdt(wsaaTransactionDate.toInt());
		lastPtrnpf.setTrtm(varcom.vrcmTime.toInt());
		lastPtrnpf.setPtrneff(runparmrec.effdate.toInt());
		lastPtrnpf.setUserT(runparmrec.user.toInt());
		lastPtrnpf.setBatccoy(runparmrec.company.toString());
		lastPtrnpf.setBatcbrn(runparmrec.batcbranch.toString());
		lastPtrnpf.setBatcactyr(runparmrec.acctyear.toInt());
		lastPtrnpf.setBatctrcde(runparmrec.transcode.toString());
		lastPtrnpf.setBatcactmn(runparmrec.acctmonth.toInt());
		lastPtrnpf.setBatcbatch(batcdorrec.batch.toString());
		lastPtrnpf.setDatesub(runparmrec.effdate.toInt());
		if(ptrnpfListInsert == null){
		    ptrnpfListInsert = new ArrayList<Ptrnpf>();
		}
		ptrnpfListInsert.add(lastPtrnpf);
	}

protected void updateBatchHeader4030()
	{
		batcuprec.batcupRec.set(SPACES);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.batcpfx.set(batcdorrec.prefix);
		batcuprec.batccoy.set(runparmrec.company);
		batcuprec.batcbrn.set(runparmrec.batcbranch);
		batcuprec.batcactyr.set(runparmrec.acctyear);
		batcuprec.batctrcde.set(runparmrec.transcode);
		batcuprec.batcactmn.set(runparmrec.acctmonth);
		batcuprec.batcbatch.set(batcdorrec.batch);
		batcuprec.function.set("WRITS");
//		callProgram(Batcup.class, batcuprec.batcupRec);
//		Batcup.getInstance().mainline( new Object[] { batcuprec.batcupRec});
		Batcup batcup = new Batcup();
		batcup.mainline( new Object[] { batcuprec.batcupRec});
		if (isNE(batcuprec.statuz, varcom.oK)) {
			conerrrec.params.set(batcuprec.batcupRec);
			conerrrec.statuz.set(batcuprec.statuz);
			systemError005();
		}
	}

protected void writeLetcBons5000()
 {
        /* first call DATCON2 to get date for when bonus is paid to */
		datcon2Pojo = new Datcon2Pojo();
		datcon2Pojo.setFreqFactor(-1);
		datcon2Pojo.setIntDate1(String.valueOf(wsaaCbunstPlus));
		datcon2Pojo.setFrequency("DY");
		datcon2Utils.calDatcon2(datcon2Pojo);
		if (!"****".equals(datcon2Pojo.getStatuz())) {
			databaseError006();
		}
		
        String chdrnum = covrpfInner.getChdrnum();
        if (bonsMap.containsKey(chdrnum)) {
            List<Bonspf> bonsrvbList = bonsMap.get(chdrnum);
            for (Bonspf bonspf : bonsrvbList) {
                if (covrpfInner.getLife().equals(bonspf.getLife()) && covrpfInner.getRider().equals(bonspf.getRider())
                        && covrpfInner.getPlnsfx() == bonspf.getPlanSuffix()
                        && covrpfInner.getCoverage().equals(bonspf.getCoverage())) {
                    rewrt5020(bonspf.getUniqueNumber());
                }
            }
        }
        writeRecord5030();
        contot5040();
        writeLetcRecord5050();
    }

protected void rewrt5020(long uniqueNumber)
	{
		/* Record exists so VALIDFLAG '2' it so we can write a new one*/
        Bonspf bonspf = new Bonspf();
        bonspf.setUniqueNumber(uniqueNumber);
        /* Record exists so VALIDFLAG '2' it so we can write a new one*/
        bonspf.setTermid(varcom.vrcmTermid.toString());
        bonspf.setUser(runparmrec.user.toInt());
        bonspf.setTransactionDate(varcom.vrcmDate.toInt());
        bonspf.setTransactionTime(varcom.vrcmTime.toInt());
        bonspf.setValidflag("2");
        bonspf.setCurrto(Integer.parseInt(datcon2Pojo.getIntDate2()));
        if(bonspfListUpdate==null){
            bonspfListUpdate = new ArrayList<Bonspf>();
        }
        bonspfListUpdate.add(bonspf);
	}

protected void writeRecord5030()
 {
        lastBonspf = new Bonspf();
        lastBonspf.setBonPayLastYr(BigDecimal.ZERO);
        lastBonspf.setBonPayThisYr(BigDecimal.ZERO);
        lastBonspf.setTotalBonus(BigDecimal.ZERO);
        lastBonspf.setChdrcoy(covrpfInner.getChdrcoy());
        lastBonspf.setChdrnum(covrpfInner.getChdrnum());
        lastBonspf.setLife(covrpfInner.getLife());
        lastBonspf.setCoverage(covrpfInner.getCoverage());
        lastBonspf.setRider(covrpfInner.getRider());
        lastBonspf.setPlanSuffix(covrpfInner.getPlnsfx());
        lastBonspf.setValidflag("1");
        lastBonspf.setCurrfrom(wsaaCbunstPlus);
        lastBonspf.setCurrto(varcom.vrcmMaxDate.toInt());
        lastBonspf.setBonPayLastYr(lastBonspf.getTotalBonus());
        lastBonspf.setBonPayThisYr(bonusrec.rvBonusSa.getbigdata().add(bonusrec.rvBonusBon.getbigdata()));
        /* MOVE BONSRVB-BON-PAY-THIS-YR TO ZRDP-AMOUNT-IN. */
        /* PERFORM 8000-CALL-ROUNDING. */
        /* MOVE ZRDP-AMOUNT-OUT TO BONSRVB-BON-PAY-THIS-YR. */
        if (lastBonspf.getBonPayThisYr().compareTo(BigDecimal.ZERO) != 0) {
            zrdecplcPojo.setAmountIn(lastBonspf.getBonPayThisYr());
        	zrdecplcPojo.setCurrency(wsaaCntcurr);
    		zrdecplcPojo.setCompany(runparmrec.company.toString());
    		zrdecplcPojo.setBatctrcde(runparmrec.transcode.toString());
    		zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
    		if (!Varcom.oK.toString().equals(zrdecplcPojo.getStatuz())) {
    			conerrrec.statuz.set(zrdecplcPojo.getStatuz());
    			systemError005();
    		}
            lastBonspf.setBonPayThisYr(zrdecplcPojo.getAmountOut());
        }
        lastBonspf.setTotalBonus(lastBonspf.getTotalBonus().add(lastBonspf.getBonPayThisYr()));
        lastBonspf.setBonusCalcMeth(t6640rec.revBonusMeth.toString());
        lastBonspf.setTermid(varcom.vrcmTermid.toString());
        lastBonspf.setUser(runparmrec.user.toInt());
        lastBonspf.setTransactionDate(varcom.vrcmDate.toInt());
        lastBonspf.setTransactionTime(varcom.vrcmTime.toInt());
        if (bonspfListInsert == null) {
            bonspfListInsert = new ArrayList<Bonspf>();
        }
        bonspfListInsert.add(lastBonspf);
    }

protected void contot5040()
	{
        countCt02 = countCt02.add(lastBonspf.getBonPayThisYr());
		/* Decide whether component we are processing has changed or not*/
		wsaaCurrentComp.set(SPACES);
		wsaaCurrentCover.set(covrpfInner.getCoverage());
		wsaaCurrentRider.set(covrpfInner.getRider());
		if ((isNE(wsaaCurrentComp, wsaaPreviousComp))) {
		    tempCount++;
			wsaaPreviousComp.set(wsaaCurrentComp);
			wsaaBonlrvbBcalmeth.add(lastBonspf.getBonusCalcMeth());
			/*        get component description to be added to BONLRVB record*/
		}   
		/* Set up values for Reversionary Bonus Letters*/
		addBigDecimal(wsaaBonlrvbSumins, tempCount, covrpfInner.getSumins());
		addBigDecimal(wsaaBonlrvbBpayty, tempCount, lastBonspf.getBonPayThisYr());
		addBigDecimal(wsaaBonlrvbBpayny, tempCount, lastBonspf.getBonPayLastYr());
		addBigDecimal(wsaaBonlrvbTotbon, tempCount, lastBonspf.getTotalBonus());
		/* get component description to be added to BONLRVB record.*/
		getT5687Desc5500();
	}

    private void addBigDecimal(List<BigDecimal> bigList, int pos, BigDecimal value) {
        if (bigList.size() < pos) {
            bigList.add(BigDecimal.ZERO);
        }
        bigList.set(pos-1, bigList.get(pos-1).add(value));
    }

	/**
	* <pre>
	* Write LETC record
	* </pre>
	*/
//changed for ILIFE-2893
protected void writeLetcRecord5050()
	{
		/* IF LETRQST-OTHER-KEYS       = CHDRNUM                        */
		if (isEQ(letrqstrec.rdocnum, covrpfInner.getChdrnum())) {
			return ;
		}
		letrqstrec.function.set("ADD");
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(covrpfInner.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO  LETRQST-LETTER-TYPE.         */
		letrqstrec.letterType.set(tr384rec.letterType);
		/* MOVE WSAA-CBUNST-PLUS       TO  LETRQST-LETTER-REQUEST-DATE. */
		letrqstrec.letterRequestDate.set(wsaaToday);
		letrqstrec.clntcoy.set(wsaaCowncoy);
		letrqstrec.clntnum.set(wsaaCownnum);
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(covrpfInner.getChdrcoy());
		letrqstrec.rdocnum.set(covrpfInner.getChdrnum());
		letrqstrec.chdrcoy.set(covrpfInner.getChdrcoy());
		letrqstrec.chdrnum.set(covrpfInner.getChdrnum());
		letrqstrec.tranno.set(chdrlif.getTranno());
		letrqstrec.branch.set(chdrlif.getCntbranch());
		/* MOVE CHDRNUM                TO  LETRQST-OTHER-KEYS.          */
//		callProgram(Letrqst.class, letrqstrec.params);
		Letrqst.getInstance().mainline( new Object[] {letrqstrec.params });
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			conerrrec.statuz.set(letrqstrec.statuz);
			databaseError006();
		}
	}

protected void getT5687Desc5500(){
    /* Read table T5687 to get Component description. */
    Descpf descpf = descDAO.getdescData("IT","T5687", covrpfInner.getCrtable(),runparmrec.company.toString(),runparmrec.language.toString());
    if (descpf != null) {
        wsaaBonlrvbLongdesc.add(descpf.getLongdesc());
        return;
    }
    conerrrec.params.set("t5687 " + covrpfInner.getCrtable());
    conerrrec.statuz.set("E402");
    databaseError006();
}

protected void writeBonlrvbRec6000()
	{
        String bonlChdrnum = lastBonspf.getChdrnum(); 
        if(bonlMap.containsKey(bonlChdrnum)){
            rewrt6020(bonlMap.get(bonlChdrnum).get(0).getUniqueNumer());
        }

        writeRecord6030();
	}

protected void rewrt6020(long uniqueNumber)
	{
        Bonlpf bonlpf = new Bonlpf();
        bonlpf.setUniqueNumer(uniqueNumber);
		/* Record exists so VALIDFLAG '2' it so we can write a new one*/
        bonlpf.setTermid(varcom.vrcmTermid.toString());
        bonlpf.setUser(runparmrec.user.toInt());
        bonlpf.setTransactionDate(varcom.vrcmDate.toInt());
        bonlpf.setTransactionTime(varcom.vrcmTime.toInt());
        bonlpf.setValidflag("2");
        bonlpf.setCurrto(Integer.parseInt(datcon2Pojo.getIntDate2()));
        if(bonlpfListUpdate==null){
            bonlpfListUpdate = new ArrayList<Bonlpf>();
        }
        bonlpfListUpdate.add(bonlpf);
	}

//changed for ILIFE-2893
protected void writeRecord6030()
	{
        Bonlpf bonlpf = new Bonlpf();
        bonlpf.setChdrcoy(lastBonspf.getChdrcoy());
        bonlpf.setChdrnum(lastBonspf.getChdrnum());
        bonlpf.setValidflag("1");
        bonlpf.setCurrfrom(wsaaCbunstPlus);
        bonlpf.setCurrto(varcom.vrcmMaxDate.toInt());
		int wsaaCount = 0;
		tempCount = 0;
        while (wsaaCount != 10 && wsaaCount < wsaaBonlrvbLongdesc.size()) {
			wsaaCount++;
            bonlpf.setLongdesc(wsaaCount, wsaaBonlrvbLongdesc.get(wsaaCount - 1));
            bonlpf.setSumins(wsaaCount, wsaaBonlrvbSumins.get(wsaaCount - 1));
            bonlpf.setBcalmeth(wsaaCount, wsaaBonlrvbBcalmeth.get(wsaaCount - 1));
            bonlpf.setBpayty(wsaaCount, wsaaBonlrvbBpayty.get(wsaaCount - 1));
            bonlpf.setBpayny(wsaaCount, wsaaBonlrvbBpayny.get(wsaaCount - 1));
            bonlpf.setTotbon(wsaaCount, wsaaBonlrvbTotbon.get(wsaaCount - 1));
		}

		/* get bonus-to date in text form by calling DATCON6*/
		datcon6rec.datcon6Rec.set(SPACES);
		datcon6rec.language.set(runparmrec.language);
		datcon6rec.company.set(runparmrec.company);
		datcon6rec.intDate1.set(Integer.parseInt(datcon2Pojo.getIntDate2()));
//		callProgram(Datcon6.class, datcon6rec.datcon6Rec);
		Datcon6 datcon6 = new Datcon6();
		datcon6.mainline( new Object[] { datcon6rec.datcon6Rec});
		if (isNE(datcon6rec.statuz, varcom.oK)) {
			conerrrec.params.set(datcon6rec.datcon6Rec);
			databaseError006();
		}
		/* MOVE DTC6-INT-DATE-2          TO BONLRVB-DATETEXT.           */
		bonlpf.setDatetexc(datcon6rec.intDate2.toString());
		bonlpf.setTermid(lastBonspf.getTermid());
		bonlpf.setUser(lastBonspf.getUser());
		bonlpf.setTransactionDate(lastBonspf.getTransactionDate());
		bonlpf.setTransactionTime(lastBonspf.getTransactionTime());
		if(bonlpfListInsert == null){
		    bonlpfListInsert = new ArrayList<Bonlpf>();
		}
		bonlpfListInsert.add(bonlpf);
		/* re-initialise all variables ready for next contract*/
		wsaaPreviousComp.set(SPACES);
		wsaaBonlrvbLongdesc.clear();
        wsaaBonlrvbSumins.clear();
        wsaaBonlrvbBcalmeth.clear();
        wsaaBonlrvbBpayty.clear();
        wsaaBonlrvbBpayny.clear();
        wsaaBonlrvbTotbon.clear();
	}

protected void unlockSoftlock7000(){
	sftlockRecBean = new SftlockRecBean();
    sftlockRecBean.setFunction("UNLK");
	sftlockRecBean.setCompany(runparmrec.company.toString().trim());
	sftlockRecBean.setEnttyp("CH");
	sftlockRecBean.setEntity(wsaaLastChdrnum);
	sftlockRecBean.setTransaction(runparmrec.transcode.toString());
	sftlockRecBean.setUser(runparmrec.user.toString());
	sftlockUtil.process(sftlockRecBean, appVars);
	if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())
		&& !varcom.mrnf.toString().equals(sftlockRecBean.getStatuz())) {
			conerrrec.statuz.set(sftlockRecBean.getStatuz());
			systemError005();
	}
}
/*
 * Class transformed  from Data Structure WSAA-CONTRACT-TYPE--INNER
 */
private static final class WsaaContractTypeInner {
		/* WSAA-CONTRACT-TYPE */
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaCtypeSplit = new FixedLengthStringData(4).isAPartOf(wsaaCnttype, 0, REDEFINE);
	private FixedLengthStringData wsaaSecondChar = new FixedLengthStringData(1).isAPartOf(wsaaCtypeSplit, 1);
	private FixedLengthStringData wsaaThirdChar = new FixedLengthStringData(1).isAPartOf(wsaaCtypeSplit, 2);
	private FixedLengthStringData wsaaFourthChar = new FixedLengthStringData(1).isAPartOf(wsaaCtypeSplit, 3);

	private FixedLengthStringData wsaaOneLetter = new FixedLengthStringData(4).isAPartOf(wsaaCnttype, 0, REDEFINE);
	private FixedLengthStringData wsaaCnttype1 = new FixedLengthStringData(1).isAPartOf(wsaaOneLetter, 0);

	private FixedLengthStringData wsaaTwoLetters = new FixedLengthStringData(4).isAPartOf(wsaaCnttype, 0, REDEFINE);
	private FixedLengthStringData wsaaCnttype2 = new FixedLengthStringData(2).isAPartOf(wsaaTwoLetters, 0);

	private FixedLengthStringData wsaaThreeLetters = new FixedLengthStringData(4).isAPartOf(wsaaCnttype, 0, REDEFINE);
	private FixedLengthStringData wsaaCnttype3 = new FixedLengthStringData(3).isAPartOf(wsaaThreeLetters, 0);
}
}
