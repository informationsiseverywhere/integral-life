package com.csc.life.regularprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:10
 * Description:
 * Copybook name: T5399REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5399rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5399Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData covPremStats = new FixedLengthStringData(24).isAPartOf(t5399Rec, 0);
  	public FixedLengthStringData[] covPremStat = FLSArrayPartOfStructure(12, 2, covPremStats, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(covPremStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData covPremStat01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData covPremStat02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData covPremStat03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData covPremStat04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData covPremStat05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData covPremStat06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData covPremStat07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData covPremStat08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData covPremStat09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData covPremStat10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData covPremStat11 = new FixedLengthStringData(2).isAPartOf(filler, 20);
  	public FixedLengthStringData covPremStat12 = new FixedLengthStringData(2).isAPartOf(filler, 22);
  	public FixedLengthStringData covRiskStats = new FixedLengthStringData(24).isAPartOf(t5399Rec, 24);
  	public FixedLengthStringData[] covRiskStat = FLSArrayPartOfStructure(12, 2, covRiskStats, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(covRiskStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData covRiskStat01 = new FixedLengthStringData(2).isAPartOf(filler1, 0);
  	public FixedLengthStringData covRiskStat02 = new FixedLengthStringData(2).isAPartOf(filler1, 2);
  	public FixedLengthStringData covRiskStat03 = new FixedLengthStringData(2).isAPartOf(filler1, 4);
  	public FixedLengthStringData covRiskStat04 = new FixedLengthStringData(2).isAPartOf(filler1, 6);
  	public FixedLengthStringData covRiskStat05 = new FixedLengthStringData(2).isAPartOf(filler1, 8);
  	public FixedLengthStringData covRiskStat06 = new FixedLengthStringData(2).isAPartOf(filler1, 10);
  	public FixedLengthStringData covRiskStat07 = new FixedLengthStringData(2).isAPartOf(filler1, 12);
  	public FixedLengthStringData covRiskStat08 = new FixedLengthStringData(2).isAPartOf(filler1, 14);
  	public FixedLengthStringData covRiskStat09 = new FixedLengthStringData(2).isAPartOf(filler1, 16);
  	public FixedLengthStringData covRiskStat10 = new FixedLengthStringData(2).isAPartOf(filler1, 18);
  	public FixedLengthStringData covRiskStat11 = new FixedLengthStringData(2).isAPartOf(filler1, 20);
  	public FixedLengthStringData covRiskStat12 = new FixedLengthStringData(2).isAPartOf(filler1, 22);
  	public FixedLengthStringData setCnPremStats = new FixedLengthStringData(24).isAPartOf(t5399Rec, 48);
  	public FixedLengthStringData[] setCnPremStat = FLSArrayPartOfStructure(12, 2, setCnPremStats, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(setCnPremStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData setCnPremStat01 = new FixedLengthStringData(2).isAPartOf(filler2, 0);
  	public FixedLengthStringData setCnPremStat02 = new FixedLengthStringData(2).isAPartOf(filler2, 2);
  	public FixedLengthStringData setCnPremStat03 = new FixedLengthStringData(2).isAPartOf(filler2, 4);
  	public FixedLengthStringData setCnPremStat04 = new FixedLengthStringData(2).isAPartOf(filler2, 6);
  	public FixedLengthStringData setCnPremStat05 = new FixedLengthStringData(2).isAPartOf(filler2, 8);
  	public FixedLengthStringData setCnPremStat06 = new FixedLengthStringData(2).isAPartOf(filler2, 10);
  	public FixedLengthStringData setCnPremStat07 = new FixedLengthStringData(2).isAPartOf(filler2, 12);
  	public FixedLengthStringData setCnPremStat08 = new FixedLengthStringData(2).isAPartOf(filler2, 14);
  	public FixedLengthStringData setCnPremStat09 = new FixedLengthStringData(2).isAPartOf(filler2, 16);
  	public FixedLengthStringData setCnPremStat10 = new FixedLengthStringData(2).isAPartOf(filler2, 18);
  	public FixedLengthStringData setCnPremStat11 = new FixedLengthStringData(2).isAPartOf(filler2, 20);
  	public FixedLengthStringData setCnPremStat12 = new FixedLengthStringData(2).isAPartOf(filler2, 22);
  	public FixedLengthStringData setCnRiskStats = new FixedLengthStringData(24).isAPartOf(t5399Rec, 72);
  	public FixedLengthStringData[] setCnRiskStat = FLSArrayPartOfStructure(12, 2, setCnRiskStats, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(24).isAPartOf(setCnRiskStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData setCnRiskStat01 = new FixedLengthStringData(2).isAPartOf(filler3, 0);
  	public FixedLengthStringData setCnRiskStat02 = new FixedLengthStringData(2).isAPartOf(filler3, 2);
  	public FixedLengthStringData setCnRiskStat03 = new FixedLengthStringData(2).isAPartOf(filler3, 4);
  	public FixedLengthStringData setCnRiskStat04 = new FixedLengthStringData(2).isAPartOf(filler3, 6);
  	public FixedLengthStringData setCnRiskStat05 = new FixedLengthStringData(2).isAPartOf(filler3, 8);
  	public FixedLengthStringData setCnRiskStat06 = new FixedLengthStringData(2).isAPartOf(filler3, 10);
  	public FixedLengthStringData setCnRiskStat07 = new FixedLengthStringData(2).isAPartOf(filler3, 12);
  	public FixedLengthStringData setCnRiskStat08 = new FixedLengthStringData(2).isAPartOf(filler3, 14);
  	public FixedLengthStringData setCnRiskStat09 = new FixedLengthStringData(2).isAPartOf(filler3, 16);
  	public FixedLengthStringData setCnRiskStat10 = new FixedLengthStringData(2).isAPartOf(filler3, 18);
  	public FixedLengthStringData setCnRiskStat11 = new FixedLengthStringData(2).isAPartOf(filler3, 20);
  	public FixedLengthStringData setCnRiskStat12 = new FixedLengthStringData(2).isAPartOf(filler3, 22);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(404).isAPartOf(t5399Rec, 96, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5399Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5399Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}