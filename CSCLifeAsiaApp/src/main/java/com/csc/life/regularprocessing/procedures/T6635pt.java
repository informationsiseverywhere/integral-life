/*
 * File: T6635pt.java
 * Date: December 3, 2013 3:56:27 AM ICT
 * Author: CSC
 * 
 * Class transformed from T6635PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.regularprocessing.tablestructures.T6635rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6635.
*
*
*
****************************************************************** ****
* </pre>
*/
public class T6635pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6635rec t6635rec = new T6635rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6635pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6635rec.t6635Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		generalCopyLinesInner.fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		generalCopyLinesInner.fieldNo006.set(datcon1rec.extDate);
		generalCopyLinesInner.fieldNo008.set(t6635rec.riskunit02);
		generalCopyLinesInner.fieldNo009.set(t6635rec.yrsinf01);
		generalCopyLinesInner.fieldNo013.set(t6635rec.yrsinf02);
		generalCopyLinesInner.fieldNo017.set(t6635rec.yrsinf03);
		generalCopyLinesInner.fieldNo021.set(t6635rec.yrsinf04);
		generalCopyLinesInner.fieldNo025.set(t6635rec.yrsinf05);
		generalCopyLinesInner.fieldNo029.set(t6635rec.yrsinf06);
		generalCopyLinesInner.fieldNo033.set(t6635rec.yrsinf07);
		generalCopyLinesInner.fieldNo037.set(t6635rec.yrsinf08);
		generalCopyLinesInner.fieldNo041.set(t6635rec.yrsinf09);
		generalCopyLinesInner.fieldNo045.set(t6635rec.yrsinf10);
		generalCopyLinesInner.fieldNo007.set(t6635rec.riskunit01);
		generalCopyLinesInner.fieldNo010.set(t6635rec.offset01);
		generalCopyLinesInner.fieldNo014.set(t6635rec.offset02);
		generalCopyLinesInner.fieldNo018.set(t6635rec.offset03);
		generalCopyLinesInner.fieldNo022.set(t6635rec.offset04);
		generalCopyLinesInner.fieldNo026.set(t6635rec.offset05);
		generalCopyLinesInner.fieldNo030.set(t6635rec.offset06);
		generalCopyLinesInner.fieldNo034.set(t6635rec.offset07);
		generalCopyLinesInner.fieldNo038.set(t6635rec.offset08);
		generalCopyLinesInner.fieldNo042.set(t6635rec.offset09);
		generalCopyLinesInner.fieldNo046.set(t6635rec.offset10);
		generalCopyLinesInner.fieldNo011.set(t6635rec.sumass01);
		generalCopyLinesInner.fieldNo015.set(t6635rec.sumass02);
		generalCopyLinesInner.fieldNo019.set(t6635rec.sumass03);
		generalCopyLinesInner.fieldNo023.set(t6635rec.sumass04);
		generalCopyLinesInner.fieldNo027.set(t6635rec.sumass05);
		generalCopyLinesInner.fieldNo031.set(t6635rec.sumass06);
		generalCopyLinesInner.fieldNo035.set(t6635rec.sumass07);
		generalCopyLinesInner.fieldNo039.set(t6635rec.sumass08);
		generalCopyLinesInner.fieldNo043.set(t6635rec.sumass09);
		generalCopyLinesInner.fieldNo047.set(t6635rec.sumass10);
		generalCopyLinesInner.fieldNo012.set(t6635rec.bonus01);
		generalCopyLinesInner.fieldNo016.set(t6635rec.bonus02);
		generalCopyLinesInner.fieldNo020.set(t6635rec.bonus03);
		generalCopyLinesInner.fieldNo024.set(t6635rec.bonus04);
		generalCopyLinesInner.fieldNo028.set(t6635rec.bonus05);
		generalCopyLinesInner.fieldNo032.set(t6635rec.bonus06);
		generalCopyLinesInner.fieldNo036.set(t6635rec.bonus07);
		generalCopyLinesInner.fieldNo040.set(t6635rec.bonus08);
		generalCopyLinesInner.fieldNo044.set(t6635rec.bonus09);
		generalCopyLinesInner.fieldNo048.set(t6635rec.bonus10);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(47).isAPartOf(wsaaPrtLine001, 29, FILLER).init("Extra Bonus Rates                         S6635");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(48);
	private FixedLengthStringData filler7 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 22);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 32, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 38);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(74);
	private FixedLengthStringData filler9 = new FixedLengthStringData(40).isAPartOf(wsaaPrtLine004, 0, FILLER).init("  Years   Offset             Risk Unit");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine004, 40).setPattern("ZZZZZZ");
	private FixedLengthStringData filler10 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine004, 46, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine004, 57, FILLER).init("Risk Unit");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine004, 68).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(54);
	private FixedLengthStringData filler12 = new FixedLengthStringData(54).isAPartOf(wsaaPrtLine005, 0, FILLER).init(" in force  term    Sum Ass                       Bonus");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(57);
	private FixedLengthStringData filler13 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 3).setPattern("ZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 12).setPattern("ZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine006, 18).setPattern("ZZZZZ.ZZ-");
	private FixedLengthStringData filler16 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine006, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine006, 48).setPattern("ZZZZZ.ZZ-");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(57);
	private FixedLengthStringData filler17 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 3).setPattern("ZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 12).setPattern("ZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine007, 18).setPattern("ZZZZZ.ZZ-");
	private FixedLengthStringData filler20 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine007, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine007, 48).setPattern("ZZZZZ.ZZ-");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(57);
	private FixedLengthStringData filler21 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 3).setPattern("ZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 12).setPattern("ZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine008, 18).setPattern("ZZZZZ.ZZ-");
	private FixedLengthStringData filler24 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine008, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine008, 48).setPattern("ZZZZZ.ZZ-");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(57);
	private FixedLengthStringData filler25 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 3).setPattern("ZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 12).setPattern("ZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine009, 18).setPattern("ZZZZZ.ZZ-");
	private FixedLengthStringData filler28 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine009, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine009, 48).setPattern("ZZZZZ.ZZ-");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(57);
	private FixedLengthStringData filler29 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 3).setPattern("ZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 12).setPattern("ZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine010, 18).setPattern("ZZZZZ.ZZ-");
	private FixedLengthStringData filler32 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine010, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine010, 48).setPattern("ZZZZZ.ZZ-");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(57);
	private FixedLengthStringData filler33 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 3).setPattern("ZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 12).setPattern("ZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine011, 18).setPattern("ZZZZZ.ZZ-");
	private FixedLengthStringData filler36 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine011, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine011, 48).setPattern("ZZZZZ.ZZ-");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(57);
	private FixedLengthStringData filler37 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 3).setPattern("ZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 12).setPattern("ZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine012, 18).setPattern("ZZZZZ.ZZ-");
	private FixedLengthStringData filler40 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine012, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine012, 48).setPattern("ZZZZZ.ZZ-");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(57);
	private FixedLengthStringData filler41 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 3).setPattern("ZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 12).setPattern("ZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine013, 18).setPattern("ZZZZZ.ZZ-");
	private FixedLengthStringData filler44 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine013, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine013, 48).setPattern("ZZZZZ.ZZ-");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(57);
	private FixedLengthStringData filler45 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 3).setPattern("ZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 12).setPattern("ZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine014, 18).setPattern("ZZZZZ.ZZ-");
	private FixedLengthStringData filler48 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine014, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine014, 48).setPattern("ZZZZZ.ZZ-");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(57);
	private FixedLengthStringData filler49 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 3).setPattern("ZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 12).setPattern("ZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine015, 18).setPattern("ZZZZZ.ZZ-");
	private FixedLengthStringData filler52 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine015, 27, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(7, 2).isAPartOf(wsaaPrtLine015, 48).setPattern("ZZZZZ.ZZ-");
}
}
