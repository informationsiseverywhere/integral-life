/*
 * File: Lowcseab.java
 * Date: 29 August 2009 22:58:50
 * Author: Quipoz Limited
 * 
 * Class transformed from LOWCSEAB.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.regularprocessing.recordstructures.Bonusrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*              LOW COST ENDOWMENT
*              ------------------
*
* This routine is part of the Traditional Business Bonus
*  subroutine suite. This program gets called from the
*  Reversionary Bonus subroutines only when a Low Cost policy is
*  involved.
* This routine will update the relevant COVR record with the
*  reduced Sum Assured value.
*
* This will be a matter of doing a BEGN on the COVR's using the
*  key from BONUSREC. For each found check if it matches the
*  BONS-LOW-COST-RIDER. If no match found Go to Exit, it will not
*  be an error.
*
*  When the correct COVR is found do a READH on the COVR and
*   REWRT it immediately with VALIDFLAG = '2' and an updated
*   CURRTO value set to yesterdays date. This will be the history
*   record.
*
*  The TRANNO will have already been updated by 1 on the CHDR in the
*  the batch job. Thus, the current TRANNO has been passed in
*  BONS-TRANNO. Update the COVR-TRANNO with this field. Move '1'
*  back to the Validflag, update CURRFROM and CURRTO. Reduce the
*  COVR-SUMINS by the passed Bonus Payable amount, noting the
*  minimum will be zero!  WRITR the COVR record.
*
*
*****************************************************************
* </pre>
*/
public class Lowcseab extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "LOWCSEAB";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-INTERNALS */
	private PackedDecimalData wsaaCovrSumins = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalBonus = new PackedDecimalData(17, 2);

		/* WSAA-INDICATORS */
	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1);
	private Validator wsaaFound = new Validator(wsaaFlag, "Y");

	private FixedLengthStringData wsaaLowCost = new FixedLengthStringData(1);
	private Validator wsaaNotLowCost = new Validator(wsaaLowCost, "Y");
		/* ERRORS */
	private String e356 = "E356";
		/* FORMATS */
	private String covrrec = "COVRREC   ";
	private Bonusrec bonusrec = new Bonusrec();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit190, 
		exit1590, 
		exit99490, 
		exit99590
	}

	public Lowcseab() {
		super();
	}

public void mainline(Object... parmArray)
	{
		bonusrec.bonusRec = convertAndSetParam(bonusrec.bonusRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		try {
			main110();
		}
		catch (GOTOException e){
		}
		finally{
			exit190();
		}
	}

protected void main110()
	{
		initialise500();
		readCovrRecord1000();
		if (wsaaNotLowCost.isTrue()) {
			goTo(GotoLabel.exit190);
		}
		rewriteCovrRec2000();
		writeNewCovr3000();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialise500()
	{
		start500();
	}

protected void start500()
	{
		syserrrec.subrname.set(wsaaSubr);
		bonusrec.statuz.set(varcom.oK);
		wsaaCovrSumins.set(ZERO);
		wsaaTotalBonus.set(ZERO);
		if ((isEQ(bonusrec.lowCostRider,SPACES))) {
			syserrrec.params.set(bonusrec.bonusRec);
			syserrrec.statuz.set(e356);
			systemError99000();
		}
		datcon2rec.freqFactor.set(-1);
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(bonusrec.effectiveDate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			systemError99000();
		}
	}

protected void readCovrRecord1000()
	{
		start1000();
	}

protected void start1000()
	{
		wsaaFlag.set(SPACES);
		wsaaLowCost.set(SPACES);
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(bonusrec.chdrChdrcoy);
		covrIO.setChdrnum(bonusrec.chdrChdrnum);
		covrIO.setLife(bonusrec.lifeLife);
		covrIO.setCoverage(bonusrec.covrCoverage);
		covrIO.setRider(bonusrec.covrRider);
		covrIO.setPlanSuffix(bonusrec.plnsfx);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.begn);
		while ( !(wsaaFound.isTrue()
		|| wsaaNotLowCost.isTrue())) {
			
			//performance improvement --  atiwari23 
			covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE");
			readCovrLoop1500();
		}
		
	}

protected void readCovrLoop1500()
	{
		try {
			start1500();
		}
		catch (GOTOException e){
		}
	}

protected void start1500()
	{
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			databaseError99500();
		}
		if ((isNE(bonusrec.chdrChdrcoy,covrIO.getChdrcoy())
		|| isNE(bonusrec.chdrChdrnum,covrIO.getChdrnum())
		|| isNE(bonusrec.lifeLife,covrIO.getLife())
		|| isNE(bonusrec.covrCoverage,covrIO.getCoverage())
		|| isEQ(covrIO.getStatuz(),varcom.endp))) {
			wsaaLowCost.set("Y");
			goTo(GotoLabel.exit1590);
		}
		if ((isNE(covrIO.getCrtable(),bonusrec.lowCostRider))) {
			covrIO.setFunction(varcom.nextr);
		}
		else {
			wsaaFlag.set("Y");
			wsaaCovrSumins.set(covrIO.getSumins());
		}
	}

protected void rewriteCovrRec2000()
	{
		start2000();
	}

protected void start2000()
	{
		covrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			databaseError99500();
		}
		covrIO.setValidflag("2");
		covrIO.setCurrto(datcon2rec.intDate2);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			databaseError99500();
		}
	}

protected void writeNewCovr3000()
	{
		start3000();
	}

protected void start3000()
	{
		compute(wsaaTotalBonus, 2).set(add(bonusrec.rvBonusSa,bonusrec.rvBonusBon));
		if ((isGT(wsaaTotalBonus,wsaaCovrSumins))) {
			covrIO.setSumins(ZERO);
		}
		else {
			setPrecision(covrIO.getSumins(), 2);
			covrIO.setSumins(sub(wsaaCovrSumins,wsaaTotalBonus));
		}
		covrIO.setTranno(bonusrec.tranno);
		covrIO.setCurrfrom(bonusrec.effectiveDate);
		covrIO.setUnitStatementDate(bonusrec.effectiveDate);
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setValidflag("1");
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			databaseError99500();
		}
	}

protected void systemError99000()
	{
		try {
			start99000();
		}
		catch (GOTOException e){
		}
		finally{
			exit99490();
		}
	}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit99490);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		bonusrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError99500()
	{
		try {
			start99500();
		}
		catch (GOTOException e){
		}
		finally{
			exit99590();
		}
	}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit99590);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99590()
	{
		bonusrec.statuz.set(varcom.bomb);
		exit190();
	}
}
