package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.LinxpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Linxpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class LinxpfDAOImpl extends BaseDAOImpl<Linxpf> implements LinxpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(LinxpfDAOImpl.class);

    @Override
    public List<Linxpf> searchLinxpfResult(String tableId, String memName, int batchExtractSize, int batchID) {
        StringBuilder sqlStr = new StringBuilder();
        sqlStr.append(" SELECT * FROM (");
        sqlStr.append("  SELECT L.CHDRNUM,L.CHDRCOY,L.INSTFROM,L.CNTCURR,L.BILLCURR,L.PAYRSEQNO,L.CBILLAMT,L.BILLCD,L.BILLCHNL,L.INSTAMT01,L.INSTAMT02,L.INSTAMT03,L.INSTAMT04,L.INSTAMT05,L.INSTAMT06,L.INSTFREQ ");
        sqlStr.append(" ,C.CNTTYPE CHDRCNTTYPE,C.OCCDATE CHDROCCDATE,C.STATCODE CHDRSTATCODE,C.PSTCDE CHDRPSTATCODE,C.TRANNO CHDRTRANNO,C.INSTTOT01 CHDRINSTTOT01,C.INSTTOT02 CHDRINSTTOT02,C.INSTTOT03 CHDRINSTTOT03,C.INSTTOT04 CHDRINSTTOT04,C.INSTTOT05 CHDRINSTTOT05,C.INSTTOT06 CHDRINSTTOT06,C.OUTSTAMT CHDROUTSTAMT,C.AGNTNUM CHDRAGNTNUM,C.CNTCURR CHDRCNTCURR,C.PTDATE CHDRPTDATE,C.RNWLSUPR CHDRRNWLSUPR,C.RNWLSPFROM CHDRRNWLSPFROM,C.RNWLSPTO CHDRRNWLSPTO,C.COWNCOY CHDRCOWNCOY,C.COWNNUM CHDRCOWNNUM,C.CNTBRANCH CHDRCNTBRANCH,C.AGNTCOY CHDRAGNTCOY,C.REG CHDRREGISTER,C.CHDRPFX CHDRCHDRPFX ");
        sqlStr.append(" ,P.TAXRELMTH PAYRTAXRELMTH,P.INCSEQNO PAYRINCOMESEQNO,P.TRANNO PAYRTRANNO,P.OUTSTAMT PAYROUTSTAMT,P.BILLCHNL PAYRBILLCHNL,P.MANDREF PAYRMANDREF,P.CNTCURR PAYRCNTCURR,P.BILLFREQ PAYRBILLFREQ,P.BILLCD PAYRBILLCD,P.PTDATE PAYRPTDATE, P.BTDATE PAYRBTDATE, P.NEXTDATE PAYRNEXTDATE");
        sqlStr.append("  ,LS.INSTTO LSINSTTO, C.UNIQUE_NUMBER CUNIQUE_NUMBER, LS.UNIQUE_NUMBER LSUNIQUE_NUMBER, P.UNIQUE_NUMBER PSUNIQUE_NUMBER  ");
        sqlStr.append("   ,FLOOR((ROW_NUMBER()OVER( ORDER BY L.UNIQUE_NUMBER ASC, C.CHDRCOY ASC, C.CHDRNUM ASC, C.UNIQUE_NUMBER DESC, P.CHDRCOY ASC, P.CHDRNUM ASC, P.PAYRSEQNO ASC, P.UNIQUE_NUMBER DESC ");
        sqlStr.append("              ,LS.CHDRCOY ASC, LS.CHDRNUM ASC, LS.INSTFROM ASC, LS.UNIQUE_NUMBER ASC");
        sqlStr.append(")-1)/?) BATCHNUM ");
        sqlStr.append("  FROM ");
        sqlStr.append(tableId);
        sqlStr.append(" L ");
        sqlStr.append(" LEFT JOIN CHDRPF C ON C.CHDRCOY=L.CHDRCOY AND C.CHDRNUM=L.CHDRNUM AND C.VALIDFLAG='1' ");
        sqlStr.append(" LEFT JOIN PAYRPF P ON P.CHDRCOY=L.CHDRCOY AND P.CHDRNUM=L.CHDRNUM AND P.PAYRSEQNO = L.PAYRSEQNO AND P.VALIDFLAG='1' ");
        sqlStr.append(" LEFT JOIN LINSPF LS ON LS.CHDRCOY=L.CHDRCOY AND LS.CHDRNUM=L.CHDRNUM AND LS.INSTFROM=L.INSTFROM AND LS.PAYFLAG != 'P' ");
        sqlStr.append("   WHERE L.MEMBER_NAME = ? ");
        sqlStr.append(" ) MAIN WHERE BATCHNUM = ? ");
        PreparedStatement ps = getPrepareStatement(sqlStr.toString());
        List<Linxpf> linxList = new LinkedList<Linxpf>();
        ResultSet rs = null;
        try {
            ps.setInt(1, batchExtractSize);
            ps.setString(2, memName);
            ps.setInt(3, batchID);

            rs = ps.executeQuery();

            while (rs.next()) {
                Linxpf linxpf = new Linxpf();
                linxpf.setChdrnum(rs.getString(1) == null ? "" : rs.getString(1));
                linxpf.setChdrcoy(rs.getString(2) == null ? "" : rs.getString(2));
                linxpf.setInstfrom(rs.getInt(3));
                linxpf.setCntcurr(rs.getString(4) == null ? "" : rs.getString(4));
                linxpf.setBillcurr(rs.getString(5) == null ? "" : rs.getString(5));
                linxpf.setPayrseqno(rs.getInt(6));
                linxpf.setCbillamt(rs.getBigDecimal(7) == null ? BigDecimal.ZERO : rs.getBigDecimal(7));
                linxpf.setBillcd(rs.getInt(8));
                linxpf.setBillchnl(rs.getString(9) == null ? "" : rs.getString(9));
                linxpf.setInstamt01(rs.getBigDecimal(10) == null ? BigDecimal.ZERO : rs.getBigDecimal(10));
                linxpf.setInstamt02(rs.getBigDecimal(11) == null ? BigDecimal.ZERO : rs.getBigDecimal(11));
                linxpf.setInstamt03(rs.getBigDecimal(12) == null ? BigDecimal.ZERO : rs.getBigDecimal(12));
                linxpf.setInstamt04(rs.getBigDecimal(13) == null ? BigDecimal.ZERO : rs.getBigDecimal(13));
                linxpf.setInstamt05(rs.getBigDecimal(14) == null ? BigDecimal.ZERO : rs.getBigDecimal(14));
                linxpf.setInstamt06(rs.getBigDecimal(15) == null ? BigDecimal.ZERO : rs.getBigDecimal(15));
                linxpf.setInstfreq(rs.getString(16) == null ? "" : rs.getString(16));

                linxpf.setChdrCnttype(rs.getString(17) == null ? "" : rs.getString(17));
                linxpf.setChdrOccdate(rs.getInt(18));
                linxpf.setChdrStatcode(rs.getString(19) == null ? "" : rs.getString(19));
                linxpf.setChdrPstatcode(rs.getString(20) == null ? "" : rs.getString(20));
                linxpf.setChdrTranno(rs.getInt(21));
                linxpf.setChdrInsttot01(rs.getBigDecimal(22) == null ? BigDecimal.ZERO : rs.getBigDecimal(22));
                linxpf.setChdrInsttot02(rs.getBigDecimal(23) == null ? BigDecimal.ZERO : rs.getBigDecimal(23));
                linxpf.setChdrInsttot03(rs.getBigDecimal(24) == null ? BigDecimal.ZERO : rs.getBigDecimal(24));
                linxpf.setChdrInsttot04(rs.getBigDecimal(25) == null ? BigDecimal.ZERO : rs.getBigDecimal(25));
                linxpf.setChdrInsttot05(rs.getBigDecimal(26) == null ? BigDecimal.ZERO : rs.getBigDecimal(26));
                linxpf.setChdrInsttot06(rs.getBigDecimal(27) == null ? BigDecimal.ZERO : rs.getBigDecimal(27));
                linxpf.setChdrOutstamt(rs.getBigDecimal(28) == null ? BigDecimal.ZERO : rs.getBigDecimal(28));
                linxpf.setChdrAgntnum(rs.getString(29) == null ? "" : rs.getString(29));
                linxpf.setChdrCntcurr(rs.getString(30) == null ? "" : rs.getString(30));
                linxpf.setChdrPtdate(rs.getInt(31));
                linxpf.setChdrRnwlsupr(rs.getString(32) != null ? rs.getString(32) : "");
                linxpf.setChdrRnwlspfrom(rs.getInt(33));
                linxpf.setChdrRnwlspto(rs.getInt(34));
                linxpf.setChdrCowncoy(rs.getString(35) == null ? "" : rs.getString(35));
                linxpf.setChdrCownnum(rs.getString(36) == null ? "" : rs.getString(36));
                linxpf.setChdrCntbranch(rs.getString(37) == null ? "" : rs.getString(37));
                linxpf.setChdrAgntcoy(rs.getString(38) == null ? "" : rs.getString(38));
                linxpf.setChdrRegister(rs.getString(39) == null ? "" : rs.getString(39));
                linxpf.setChdrchdrpfx(rs.getString(40) == null ? "" : rs.getString(40));

                linxpf.setPayrTaxrelmth(rs.getString(41) == null ? "" : rs.getString(41));
                linxpf.setPayrIncomeseqno(rs.getInt(42));
                linxpf.setPayrTranno(rs.getInt(43));
                linxpf.setPayrOutstamt(rs.getBigDecimal(44) == null ? BigDecimal.ZERO : rs.getBigDecimal(44));
                linxpf.setPayrBillchnl(rs.getString(45) == null ? "" : rs.getString(45));
                linxpf.setPayrMandref(rs.getString(46) == null ? "" : rs.getString(46));
                linxpf.setPayrCntcurr(rs.getString(47) == null ? "" : rs.getString(47));
                linxpf.setPayrBillfreq(rs.getString(48) == null ? "" : rs.getString(48));
                linxpf.setPayrBillcd(rs.getInt(49));
                linxpf.setPayrPtdate(rs.getInt(50));
                linxpf.setPayrBtdate(rs.getInt(51));
                linxpf.setPayrNextDate(rs.getInt(52));
                linxpf.setLinsInstto(rs.getInt(53));
                linxpf.setChdrUniqueNumber(rs.getLong(54));
                linxpf.setLinsUniqueNumber(rs.getLong(55));
                linxpf.setPayrUniqueNumber(rs.getLong(56));
                linxList.add(linxpf);
            }

        } catch (SQLException e) {
            LOGGER.error("searchLinxpfResult()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return linxList;
    }

}