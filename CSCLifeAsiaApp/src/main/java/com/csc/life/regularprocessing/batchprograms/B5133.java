/*
 * File: B5133.java
 * Date: 29 August 2009 20:57:23
 * Author: Quipoz Limited
 * 
 * Class transformed from B5133.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.regularprocessing.dataaccess.CovipfTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.regularprocessing.tablestructures.T5655rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*            PENDING AUTOMATIC INCREASES (SPLITTER)
*            --------------------------------------
* Overview
* ________
*
* This Splitter runs directly before B5134, which is the multi-
* thread program that processes Pending Automatic Increase
* records.
*
* A temporary file COVIPF, based upon the physical file COVRPF,
* is created and COVRPF records are divided up into the
* temporary file members. This allows B5134 to retrieve the
* records in multi-thread mode for validation.
*
*           Ensure that only COVR records which has not had the       *
*           indexation indicator set to 'P' are selected.             *
*
* The records are selected from COVRPF and CHDRPF as follows:
*
*     COVRPF chdrnum   = CHDRPF chdrnum
*     COVRPF chdrcoy   = {run-company}
*     COVRPF cpidte    < {effective-date} and not = 0
*     COVRPF indxin    = 'P'
*     COVRPF chdrnum   between {chdrnumfrom} and {chdrnumto}
*     COVRPF validflag = '1'
*     CHDRPF validflag = '1'
*
* The resultant rows are ordered by:
*    CHDRPF cownnum, COVRPF chdrcoy, COVRPF chdrnum, COVRPF life,
*    COVRPF coverage, COVRPF rider, COVRPF plansfx.
*
* Note that the purpose of ordering by contract owner is to
* prevent record locking when writing LETCPF letter records
* within the processing program.
*
* Control totals used in this program:
*
*    01  -  No. of thread members
*    02  -  No. of extracted records
*
*------------------------------------------------------------------------
* A Splitter Program's purpose is to find pending transactions
* from the database and create multiple extract files for
* processing by multiple copies of the subsequent program, each
* running in its own thread. Each subsequent program therefore
* processes a discrete portion of the total transactions.
*
* Splitter programs should be simple. They should only deal with
* a single file. If there is insufficient data on the primary file
* to completely identify a transaction then the further editing
* should be done in the subsequent process. The objective of a
* Splitter is to rapidly isolate potential transactions, not
* to process the transaction. The primary concern for Splitter
* program design is elapsed time speed and this is achieved by
* minimising disk IO.
*
* Before the Splitter process is invoked, the program CRTTMPF
* should be run. The CRTTMPF (Create Temporary File) will create
* a duplicate of a physical file, (created under Smart and defined
* as a Query file), which will have as many members as is defined
* in that process's 'subsequent threads' field. The temporary file
* will have the name XXXXDD9999 where XXXX is the first four
* characters of the file which was duplicated, DD is the first two
* characters of the 4th system parameter and 9999 is the schedule
* run number. Each member added to the temporary file will be
* called THREAD999 where 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read records from a primary file
* and for each record it selects it will write a record to a
* temporary file member. Exactly which temporary file member
* is written to is decided by a working storage count. The
* objective of the count is to spread the transaction records
* evenly amongst each thread member.
*
* The Splitter program should always have a restart method of
* '1' indicating a re-run. In the event of a restart, the Splitter
* program will always start with empty file members. Apart from
* writing records to the temporary file members, Splitter programs
* should not be doing any further updates.
*
* There should be no non-standard CL commands in the CL Pgm which
* calls this program.
*
* Notes for programs which use Splitter Program Temporary Files.
* --------------------------------------------------------------
*
* The MTBE will be able to submit multiple versions of the program
* which use the file members created from this program. It is
* important that the process which runs the splitter program has
* the same 'number of subsequent threads' as the following
* process has defined in 'number of threads this process'.
*
* The subsequent program need only point itself to one member
* based around the field BSPR-SCHEDULE-THREAD-NO which indicates
* in which thread the copy of the subsequent program is running.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class B5133 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlcovrpfCursorrs = null;
	private java.sql.PreparedStatement sqlcovrpfCursorps = null;
	private java.sql.Connection sqlcovrpfCursorconn = null;
	private String sqlcovrpfCursor = "";
	private int covrpfCursorLoopIndex = 0;
	private DiskFileDAM covi01 = new DiskFileDAM("COVI01");
	private DiskFileDAM covi02 = new DiskFileDAM("COVI02");
	private DiskFileDAM covi03 = new DiskFileDAM("COVI03");
	private DiskFileDAM covi04 = new DiskFileDAM("COVI04");
	private DiskFileDAM covi05 = new DiskFileDAM("COVI05");
	private DiskFileDAM covi06 = new DiskFileDAM("COVI06");
	private DiskFileDAM covi07 = new DiskFileDAM("COVI07");
	private DiskFileDAM covi08 = new DiskFileDAM("COVI08");
	private DiskFileDAM covi09 = new DiskFileDAM("COVI09");
	private DiskFileDAM covi10 = new DiskFileDAM("COVI10");
	private DiskFileDAM covi11 = new DiskFileDAM("COVI11");
	private DiskFileDAM covi12 = new DiskFileDAM("COVI12");
	private DiskFileDAM covi13 = new DiskFileDAM("COVI13");
	private DiskFileDAM covi14 = new DiskFileDAM("COVI14");
	private DiskFileDAM covi15 = new DiskFileDAM("COVI15");
	private DiskFileDAM covi16 = new DiskFileDAM("COVI16");
	private DiskFileDAM covi17 = new DiskFileDAM("COVI17");
	private DiskFileDAM covi18 = new DiskFileDAM("COVI18");
	private DiskFileDAM covi19 = new DiskFileDAM("COVI19");
	private DiskFileDAM covi20 = new DiskFileDAM("COVI20");
	private CovipfTableDAM covipfData = new CovipfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5133");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");
	private Validator notFirstTime = new Validator(wsaaFirstTime, "N");
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaa1 = new FixedLengthStringData(1).init("1");

	private FixedLengthStringData wsaaT5655Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5655Trcode = new FixedLengthStringData(4).isAPartOf(wsaaT5655Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5655Asterisk = new FixedLengthStringData(4).isAPartOf(wsaaT5655Key, 4).init("****");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");
	private Validator notEofInBlock = new Validator(wsaaEofInBlock, "N");

		/* COVI member parameters.*/
	private FixedLengthStringData wsaaCoviFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaCoviFn, 0, FILLER).init("COVI");
	private FixedLengthStringData wsaaCoviRunid = new FixedLengthStringData(2).isAPartOf(wsaaCoviFn, 4);
	private ZonedDecimalData wsaaCoviJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaCoviFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaThreadMember, 9, FILLER).init(SPACES);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
		/*01  COVIPF-DATA.                                                 
		  COPY DDS-ALL-FORMATS OF COVIPF.                              
		 Pointers to point to the member to read (IY) and write (IZ).*/
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
		/* Define the number of rows in the block to be fetched.*/
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);

		/* WSAA-IND-ARRAY */
	private FixedLengthStringData[] wsaaCovrInd = FLSInittedArray (1000, 8);
	private BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(4, 4, 0, wsaaCovrInd, 0);
	private FixedLengthStringData wsaaChdrChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaP = new FixedLengthStringData(1).init("P");
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).init(0);
	private FixedLengthStringData wsaaPrevCownnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private static final String itemrec = "ITEMREC";
		/* TABLES */
	private static final String t5655 = "T5655";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private Validator notEndOfFile = new Validator(wsaaEof, "N");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private T5655rec t5655rec = new T5655rec();
	private P6671par p6671par = new P6671par();
	private WsaaFetchArrayInner wsaaFetchArrayInner = new WsaaFetchArrayInner();

	private int ct02Value = 0;
	public B5133() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/***** This section is invoked in restart mode. Note section*/
		/***** 1100 must clear each temporary file member as the members*/
		/***** are not under commitment control. Note also that Splitter*/
		/***** programs must have a Restart Method of 1 (re-run).*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/* Do an ovrdbf for each temporary file member. (Note we have*/
		/* allowed for a maximum of 20.)*/
		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		/* Since this program runs from a parameter screen, move the*/
		/* internal parameter area to the original parameter copybook*/
		/* to retrieve the contract range*/
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		wsaaChdrnumFrom.set(p6671par.chdrnum);
		wsaaChdrnumTo.set(p6671par.chdrnum1);
		if (isEQ(p6671par.chdrnum,SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
		}
		if (isEQ(p6671par.chdrnum1,SPACES)) {
			wsaaChdrnumTo.set(HIVALUE);
		}
		/* The next thing we must do is construct the name of the*/
		/* temporary file which we will be working with.*/
		wsaaCoviRunid.set(bprdIO.getSystemParam04());
		wsaaCoviJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		wsaaCompany.set(bsprIO.getCompany());
		/* Open the required number of temporary files, depending*/
		/* on IZ and determined by the number of threads specified*/
		/* for the subsequent process.*/
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		/* Log the number of threads being used*/
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		/* Read T5655 to obtain the lead days for Pending Increases.*/
		wsaaT5655Trcode.set(bprdIO.getSystemParam02());
		itemIO.setDataArea(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5655);
		itemIO.setItemitem(wsaaT5655Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5655rec.t5655Rec.set(itemIO.getGenarea());
		/* Add the number of lead days to the effective date.*/
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.freqFactor.set(t5655rec.leadDays);
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError600();
		}
		wsaaEffdate.set(datcon2rec.intDate2);
		sqlcovrpfCursor = " SELECT  T02.COWNNUM, T01.CHDRCOY, T01.CHDRNUM, T01.LIFE, T01.JLIFE, T01.COVERAGE, T01.RIDER, T01.PLNSFX, T01.STATCODE, T01.PSTATCODE, T01.CRRCD, T01.PRMCUR, T01.CRTABLE, T01.RCESDTE, T01.SUMINS, T01.INSTPREM, T01.ZBINSTPREM, T01.ZLINSTPREM, T01.SINGP, T01.CPIDTE, T01.INDXIN, T01.MORTCLS, T01.VALIDFLAG" +
" FROM   " + getAppVars().getTableNameOverriden("COVRPF") + "  T01,  " + getAppVars().getTableNameOverriden("CHDRPF") + "  T02" +
" WHERE T01.CHDRNUM = T02.CHDRNUM" +
" AND T01.CHDRCOY = ?" +
" AND T01.CPIDTE <> 0" +
" AND T01.CPIDTE <= ?" +
" AND T01.INDXIN <> ?" +
" AND T01.CHDRNUM BETWEEN ? AND ?" +
" AND T01.VALIDFLAG = ?" +
" AND T02.VALIDFLAG = ?" +
" AND T02.CHDRCOY = ?" +
" ORDER BY T02.COWNNUM, T01.CHDRCOY, T01.CHDRNUM, T01.LIFE, T01.COVERAGE, T01.RIDER, T01.PLNSFX";
		/* Initialise the array which will hold all the records*/
		/* returned from each fetch. (INITIALIZE will not work as*/
		/* the array is indexed).*/
		iy.set(1);
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1200();
		}
		wsaaInd.set(1);
		/* Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlcovrpfCursorconn = getAppVars().getDBConnectionForTable(new com.csc.smart400framework.dataaccess.SmartFileCode[] {new com.csc.life.newbusiness.dataaccess.CovrpfTableDAM(), new com.csc.fsu.general.dataaccess.ChdrpfTableDAM()});
			sqlcovrpfCursorps = getAppVars().prepareStatementEmbeded(sqlcovrpfCursorconn, sqlcovrpfCursor);
			getAppVars().setDBString(sqlcovrpfCursorps, 1, wsaaCompany);
			getAppVars().setDBNumber(sqlcovrpfCursorps, 2, wsaaEffdate);
			getAppVars().setDBString(sqlcovrpfCursorps, 3, wsaaP);
			getAppVars().setDBString(sqlcovrpfCursorps, 4, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlcovrpfCursorps, 5, wsaaChdrnumTo);
			getAppVars().setDBString(sqlcovrpfCursorps, 6, wsaa1);
			getAppVars().setDBString(sqlcovrpfCursorps, 7, wsaa1);
			getAppVars().setDBString(sqlcovrpfCursorps, 8, wsaaCompany);
			sqlcovrpfCursorrs = getAppVars().executeQuery(sqlcovrpfCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void openThreadMember1100()
	{
		/* The following code is used to initialise all members.*/
		/* It is also used in restart mode.*/
		/* Temporary files, previously used, are cleared.*/
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(),1),iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaCoviFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.syserrType.set("2");
			syserrrec.subrname.set("CLRTMPF");
			fatalError600();
		}
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(COVI");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaCoviFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/* Open the file.*/
		if (isEQ(iz,1)) {
			covi01.openOutput();
		}
		if (isEQ(iz,2)) {
			covi02.openOutput();
		}
		if (isEQ(iz,3)) {
			covi03.openOutput();
		}
		if (isEQ(iz,4)) {
			covi04.openOutput();
		}
		if (isEQ(iz,5)) {
			covi05.openOutput();
		}
		if (isEQ(iz,6)) {
			covi06.openOutput();
		}
		if (isEQ(iz,7)) {
			covi07.openOutput();
		}
		if (isEQ(iz,8)) {
			covi08.openOutput();
		}
		if (isEQ(iz,9)) {
			covi09.openOutput();
		}
		if (isEQ(iz,10)) {
			covi10.openOutput();
		}
		if (isEQ(iz,11)) {
			covi11.openOutput();
		}
		if (isEQ(iz,12)) {
			covi12.openOutput();
		}
		if (isEQ(iz,13)) {
			covi13.openOutput();
		}
		if (isEQ(iz,14)) {
			covi14.openOutput();
		}
		if (isEQ(iz,15)) {
			covi15.openOutput();
		}
		if (isEQ(iz,16)) {
			covi16.openOutput();
		}
		if (isEQ(iz,17)) {
			covi17.openOutput();
		}
		if (isEQ(iz,18)) {
			covi18.openOutput();
		}
		if (isEQ(iz,19)) {
			covi19.openOutput();
		}
		if (isEQ(iz,20)) {
			covi20.openOutput();
		}
	}

protected void initialiseArray1200()
	{
		/*START*/
		wsaaFetchArrayInner.wsaaCownnum[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaChdrcoy[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaLife[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaJlife[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaCoverage[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaRider[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaStatcode[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaPstatcode[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaPrmcur[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaCrtable[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaIndxin[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaMortcls[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaValidflag[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaPlnsfx[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaCrrcd[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaRcesdte[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaSumins[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaInstprem[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaZbinstprem[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaZlinstprem[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaSingp[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaCpidte[wsaaInd.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void readFile2000()
	{
			readFile2010();
		}

protected void readFile2010()
	{
		/* Now a block of records is fetched into the array.*/
		/* Also on the first entry into the program we must set up the*/
		/* WSAA-PREV-COWNNUM = the present cownnum.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd,1)) {
			return ;
		}
		sqlerrorflag = false;
		try {
			for (covrpfCursorLoopIndex = 1; isLT(covrpfCursorLoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlcovrpfCursorrs); covrpfCursorLoopIndex++ ){
				getAppVars().getDBObject(sqlcovrpfCursorrs, 1, wsaaFetchArrayInner.wsaaCownnum[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 2, wsaaFetchArrayInner.wsaaChdrcoy[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 3, wsaaFetchArrayInner.wsaaChdrnum[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 4, wsaaFetchArrayInner.wsaaLife[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 5, wsaaFetchArrayInner.wsaaJlife[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 6, wsaaFetchArrayInner.wsaaCoverage[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 7, wsaaFetchArrayInner.wsaaRider[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 8, wsaaFetchArrayInner.wsaaPlnsfx[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 9, wsaaFetchArrayInner.wsaaStatcode[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 10, wsaaFetchArrayInner.wsaaPstatcode[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 11, wsaaFetchArrayInner.wsaaCrrcd[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 12, wsaaFetchArrayInner.wsaaPrmcur[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 13, wsaaFetchArrayInner.wsaaCrtable[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 14, wsaaFetchArrayInner.wsaaRcesdte[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 15, wsaaFetchArrayInner.wsaaSumins[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 16, wsaaFetchArrayInner.wsaaInstprem[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 17, wsaaFetchArrayInner.wsaaZbinstprem[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 18, wsaaFetchArrayInner.wsaaZlinstprem[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 19, wsaaFetchArrayInner.wsaaSingp[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 20, wsaaFetchArrayInner.wsaaCpidte[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 21, wsaaFetchArrayInner.wsaaIndxin[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 22, wsaaFetchArrayInner.wsaaMortcls[covrpfCursorLoopIndex]);
				getAppVars().getDBObject(sqlcovrpfCursorrs, 23, wsaaFetchArrayInner.wsaaValidflag[covrpfCursorLoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/* We must detect :-*/
		/*    a) no rows returned on the first fetch*/
		/*    b) the last row returned (in the block)*/
		/* IF Either of the above cases occur, then an*/
		/*    SQLCODE = +100 is returned.*/
		/* The 3000 section is continued for case (b).*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaFetchArrayInner.wsaaCownnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		//tom chi modified, bug 1030
		else if(isEQ(wsaaFetchArrayInner.wsaaCownnum[wsaaInd.toInt()],SPACES)){
			wsspEdterror.set(varcom.endp);
		}
				//end
		if (firstTime.isTrue()) {
			wsaaPrevCownnum.set(wsaaFetchArrayInner.wsaaCownnum[wsaaInd.toInt()]);
			notFirstTime.setTrue();
		}
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		/* Re-initialise the block for next fetch and point*/
		/* to first row.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1200();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/* If the COWNNUM being processed is not equal to the previously*/
		/* stored COWNNUM, move to the next output file member.  The*/
		/* condition is here to allow for checking of the last COWNNUM*/
		/* of the old block with the first of the new and to write to*/
		/* the next COVI member if they have changed.*/
		if (isNE(wsaaFetchArrayInner.wsaaCownnum[wsaaInd.toInt()], wsaaPrevCownnum)) {
			wsaaPrevCownnum.set(wsaaFetchArrayInner.wsaaCownnum[wsaaInd.toInt()]);
			iy.add(1);
		}
		/* Load from storage all COVI data for the same Client until*/
		/* the COWNNUM on the query row has changed or the end of an*/
		/* incomplete block is reached.*/
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| isNE(wsaaFetchArrayInner.wsaaCownnum[wsaaInd.toInt()], wsaaPrevCownnum)
		|| eofInBlock.isTrue())) {
			loadSameClient3200();
		}
		
		/*EXIT*/
	}

protected void loadSameClient3200()
	{
		covipfData.chdrcoy.set(wsaaFetchArrayInner.wsaaChdrcoy[wsaaInd.toInt()]);
		covipfData.chdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
		covipfData.life.set(wsaaFetchArrayInner.wsaaLife[wsaaInd.toInt()]);
		covipfData.jlife.set(wsaaFetchArrayInner.wsaaJlife[wsaaInd.toInt()]);
		covipfData.coverage.set(wsaaFetchArrayInner.wsaaCoverage[wsaaInd.toInt()]);
		covipfData.rider.set(wsaaFetchArrayInner.wsaaRider[wsaaInd.toInt()]);
		covipfData.planSuffix.set(wsaaFetchArrayInner.wsaaPlnsfx[wsaaInd.toInt()]);
		covipfData.statcode.set(wsaaFetchArrayInner.wsaaStatcode[wsaaInd.toInt()]);
		covipfData.pstatcode.set(wsaaFetchArrayInner.wsaaPstatcode[wsaaInd.toInt()]);
		covipfData.crrcd.set(wsaaFetchArrayInner.wsaaCrrcd[wsaaInd.toInt()]);
		covipfData.premCurrency.set(wsaaFetchArrayInner.wsaaPrmcur[wsaaInd.toInt()]);
		covipfData.crtable.set(wsaaFetchArrayInner.wsaaCrtable[wsaaInd.toInt()]);
		covipfData.riskCessDate.set(wsaaFetchArrayInner.wsaaRcesdte[wsaaInd.toInt()]);
		covipfData.sumins.set(wsaaFetchArrayInner.wsaaSumins[wsaaInd.toInt()]);
		covipfData.instprem.set(wsaaFetchArrayInner.wsaaInstprem[wsaaInd.toInt()]);
		covipfData.zbinstprem.set(wsaaFetchArrayInner.wsaaZbinstprem[wsaaInd.toInt()]);
		covipfData.zlinstprem.set(wsaaFetchArrayInner.wsaaZlinstprem[wsaaInd.toInt()]);
		covipfData.singp.set(wsaaFetchArrayInner.wsaaSingp[wsaaInd.toInt()]);
		covipfData.cpiDate.set(wsaaFetchArrayInner.wsaaCpidte[wsaaInd.toInt()]);
		covipfData.indexationInd.set(wsaaFetchArrayInner.wsaaIndxin[wsaaInd.toInt()]);
		covipfData.mortcls.set(wsaaFetchArrayInner.wsaaMortcls[wsaaInd.toInt()]);
		covipfData.validflag.set(wsaaFetchArrayInner.wsaaValidflag[wsaaInd.toInt()]);
		if (isGT(iy,bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		if (isEQ(iy,1)) {
			covi01.write(covipfData);
		}
		if (isEQ(iy,2)) {
			covi02.write(covipfData);
		}
		if (isEQ(iy,3)) {
			covi03.write(covipfData);
		}
		if (isEQ(iy,4)) {
			covi04.write(covipfData);
		}
		if (isEQ(iy,5)) {
			covi05.write(covipfData);
		}
		if (isEQ(iy,6)) {
			covi06.write(covipfData);
		}
		if (isEQ(iy,7)) {
			covi07.write(covipfData);
		}
		if (isEQ(iy,8)) {
			covi08.write(covipfData);
		}
		if (isEQ(iy,9)) {
			covi09.write(covipfData);
		}
		if (isEQ(iy,10)) {
			covi10.write(covipfData);
		}
		if (isEQ(iy,11)) {
			covi11.write(covipfData);
		}
		if (isEQ(iy,12)) {
			covi12.write(covipfData);
		}
		if (isEQ(iy,13)) {
			covi13.write(covipfData);
		}
		if (isEQ(iy,14)) {
			covi14.write(covipfData);
		}
		if (isEQ(iy,15)) {
			covi15.write(covipfData);
		}
		if (isEQ(iy,16)) {
			covi16.write(covipfData);
		}
		if (isEQ(iy,17)) {
			covi17.write(covipfData);
		}
		if (isEQ(iy,18)) {
			covi18.write(covipfData);
		}
		if (isEQ(iy,19)) {
			covi19.write(covipfData);
		}
		if (isEQ(iy,20)) {
			covi20.write(covipfData);
		}
		/* Log the number of extracted records.*/
        ct02Value++;
		/* Set up the array for the next block of records.*/
		wsaaInd.add(1);
		/* Check for an incomplete block retrieved.*/		
		if (isLTE(wsaaInd,wsaaRowsInBlock) && isEQ(wsaaFetchArrayInner.wsaaCownnum[wsaaInd.toInt()],SPACES)) {
			eofInBlock.setTrue();
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/***** There is no pre-rollback processing to be done.*/
		/*EXIT*/
        contotrec.totval.set(ct02Value);
        contotrec.totno.set(ct02);
        callContot001();
        ct02Value = 0;
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/***** There is no pre-rollback processing to be done.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/* Close the open files and delete the overrides.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlcovrpfCursorconn, sqlcovrpfCursorps, sqlcovrpfCursorrs);
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		if (isEQ(iz,1)) {
			covi01.close();
		}
		if (isEQ(iz,2)) {
			covi02.close();
		}
		if (isEQ(iz,3)) {
			covi03.close();
		}
		if (isEQ(iz,4)) {
			covi04.close();
		}
		if (isEQ(iz,5)) {
			covi05.close();
		}
		if (isEQ(iz,6)) {
			covi06.close();
		}
		if (isEQ(iz,7)) {
			covi07.close();
		}
		if (isEQ(iz,8)) {
			covi08.close();
		}
		if (isEQ(iz,9)) {
			covi09.close();
		}
		if (isEQ(iz,10)) {
			covi10.close();
		}
		if (isEQ(iz,11)) {
			covi11.close();
		}
		if (isEQ(iz,12)) {
			covi12.close();
		}
		if (isEQ(iz,13)) {
			covi13.close();
		}
		if (isEQ(iz,14)) {
			covi14.close();
		}
		if (isEQ(iz,15)) {
			covi15.close();
		}
		if (isEQ(iz,16)) {
			covi16.close();
		}
		if (isEQ(iz,17)) {
			covi17.close();
		}
		if (isEQ(iz,18)) {
			covi18.close();
		}
		if (isEQ(iz,19)) {
			covi19.close();
		}
		if (isEQ(iz,20)) {
			covi20.close();
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure WSAA-FETCH-ARRAY--INNER
 */
private static final class WsaaFetchArrayInner { 

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaCovrData = FLSInittedArray (1000, 102);
	private FixedLengthStringData[] wsaaCownnum = FLSDArrayPartOfArrayStructure(8, wsaaCovrData, 0);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaCovrData, 8);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaCovrData, 9);
	private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaCovrData, 17);
	private FixedLengthStringData[] wsaaJlife = FLSDArrayPartOfArrayStructure(2, wsaaCovrData, 19);
	private FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaCovrData, 21);
	private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaCovrData, 23);
	private PackedDecimalData[] wsaaPlnsfx = PDArrayPartOfArrayStructure(4, 0, wsaaCovrData, 25);
	private FixedLengthStringData[] wsaaStatcode = FLSDArrayPartOfArrayStructure(2, wsaaCovrData, 28);
	private FixedLengthStringData[] wsaaPstatcode = FLSDArrayPartOfArrayStructure(2, wsaaCovrData, 30);
	private PackedDecimalData[] wsaaCrrcd = PDArrayPartOfArrayStructure(8, 0, wsaaCovrData, 32);
	private FixedLengthStringData[] wsaaPrmcur = FLSDArrayPartOfArrayStructure(3, wsaaCovrData, 37);
	private FixedLengthStringData[] wsaaCrtable = FLSDArrayPartOfArrayStructure(4, wsaaCovrData, 40);
	private PackedDecimalData[] wsaaRcesdte = PDArrayPartOfArrayStructure(8, 0, wsaaCovrData, 44);
	private PackedDecimalData[] wsaaSumins = PDArrayPartOfArrayStructure(17, 2, wsaaCovrData, 49);
	private PackedDecimalData[] wsaaInstprem = PDArrayPartOfArrayStructure(17, 2, wsaaCovrData, 58);
	private PackedDecimalData[] wsaaZbinstprem = PDArrayPartOfArrayStructure(17, 2, wsaaCovrData, 67);
	private PackedDecimalData[] wsaaZlinstprem = PDArrayPartOfArrayStructure(17, 2, wsaaCovrData, 76);
	private PackedDecimalData[] wsaaSingp = PDArrayPartOfArrayStructure(17, 2, wsaaCovrData, 85);
	private PackedDecimalData[] wsaaCpidte = PDArrayPartOfArrayStructure(8, 0, wsaaCovrData, 94);
	private FixedLengthStringData[] wsaaIndxin = FLSDArrayPartOfArrayStructure(1, wsaaCovrData, 99);
	private FixedLengthStringData[] wsaaMortcls = FLSDArrayPartOfArrayStructure(1, wsaaCovrData, 100);
	private FixedLengthStringData[] wsaaValidflag = FLSDArrayPartOfArrayStructure(1, wsaaCovrData, 101);
}
}
