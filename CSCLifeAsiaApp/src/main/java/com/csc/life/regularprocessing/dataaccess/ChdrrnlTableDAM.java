package com.csc.life.regularprocessing.dataaccess;

import com.csc.fsu.general.dataaccess.ChdrpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ChdrrnlTableDAM.java
 * Date: Sun, 30 Aug 2009 03:32:41
 * Class transformed from CHDRRNL.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ChdrrnlTableDAM extends ChdrpfTableDAM {

	public ChdrrnlTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("CHDRRNL");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "VALIDFLAG, " +
		            "TRANNO, " +
		            "TRANID, " +
		            "BTDATE, " +
		            "OUTSTAMT, " +
		            "BILLSUPR, " +
		            "BILLSPFROM, " +
		            "BILLSPTO, " +
		            "BILLFREQ, " +
		            "BILLCD, " +
		            "POLSUM, " +
		            "PTDATE, " +
		            "POLINC, " +
		            "STATCODE, " +
		            "PSTCDE, " +
		            "BILLCHNL, " +
		            "CNTTYPE, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
		            "SERVUNIT, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               validflag,
                               tranno,
                               tranid,
                               btdate,
                               outstamt,
                               billsupr,
                               billspfrom,
                               billspto,
                               billfreq,
                               billcd,
                               polsum,
                               ptdate,
                               polinc,
                               statcode,
                               pstatcode,
                               billchnl,
                               cnttype,
                               jobName,
                               userProfile,
                               datime,
                               servunit,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(247);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(127);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getValidflag().toInternal()
					+ getTranno().toInternal()
					+ getTranid().toInternal()
					+ getBtdate().toInternal()
					+ getOutstamt().toInternal()
					+ getBillsupr().toInternal()
					+ getBillspfrom().toInternal()
					+ getBillspto().toInternal()
					+ getBillfreq().toInternal()
					+ getBillcd().toInternal()
					+ getPolsum().toInternal()
					+ getPtdate().toInternal()
					+ getPolinc().toInternal()
					+ getStatcode().toInternal()
					+ getPstatcode().toInternal()
					+ getBillchnl().toInternal()
					+ getCnttype().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal()
					+ getServunit().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, tranid);
			what = ExternalData.chop(what, btdate);
			what = ExternalData.chop(what, outstamt);
			what = ExternalData.chop(what, billsupr);
			what = ExternalData.chop(what, billspfrom);
			what = ExternalData.chop(what, billspto);
			what = ExternalData.chop(what, billfreq);
			what = ExternalData.chop(what, billcd);
			what = ExternalData.chop(what, polsum);
			what = ExternalData.chop(what, ptdate);
			what = ExternalData.chop(what, polinc);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, billchnl);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);
			what = ExternalData.chop(what, servunit);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getTranid() {
		return tranid;
	}
	public void setTranid(Object what) {
		tranid.set(what);
	}	
	public PackedDecimalData getBtdate() {
		return btdate;
	}
	public void setBtdate(Object what) {
		setBtdate(what, false);
	}
	public void setBtdate(Object what, boolean rounded) {
		if (rounded)
			btdate.setRounded(what);
		else
			btdate.set(what);
	}	
	public PackedDecimalData getOutstamt() {
		return outstamt;
	}
	public void setOutstamt(Object what) {
		setOutstamt(what, false);
	}
	public void setOutstamt(Object what, boolean rounded) {
		if (rounded)
			outstamt.setRounded(what);
		else
			outstamt.set(what);
	}	
	public FixedLengthStringData getBillsupr() {
		return billsupr;
	}
	public void setBillsupr(Object what) {
		billsupr.set(what);
	}	
	public PackedDecimalData getBillspfrom() {
		return billspfrom;
	}
	public void setBillspfrom(Object what) {
		setBillspfrom(what, false);
	}
	public void setBillspfrom(Object what, boolean rounded) {
		if (rounded)
			billspfrom.setRounded(what);
		else
			billspfrom.set(what);
	}	
	public PackedDecimalData getBillspto() {
		return billspto;
	}
	public void setBillspto(Object what) {
		setBillspto(what, false);
	}
	public void setBillspto(Object what, boolean rounded) {
		if (rounded)
			billspto.setRounded(what);
		else
			billspto.set(what);
	}	
	public FixedLengthStringData getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(Object what) {
		billfreq.set(what);
	}	
	public PackedDecimalData getBillcd() {
		return billcd;
	}
	public void setBillcd(Object what) {
		setBillcd(what, false);
	}
	public void setBillcd(Object what, boolean rounded) {
		if (rounded)
			billcd.setRounded(what);
		else
			billcd.set(what);
	}	
	public PackedDecimalData getPolsum() {
		return polsum;
	}
	public void setPolsum(Object what) {
		setPolsum(what, false);
	}
	public void setPolsum(Object what, boolean rounded) {
		if (rounded)
			polsum.setRounded(what);
		else
			polsum.set(what);
	}	
	public PackedDecimalData getPtdate() {
		return ptdate;
	}
	public void setPtdate(Object what) {
		setPtdate(what, false);
	}
	public void setPtdate(Object what, boolean rounded) {
		if (rounded)
			ptdate.setRounded(what);
		else
			ptdate.set(what);
	}	
	public PackedDecimalData getPolinc() {
		return polinc;
	}
	public void setPolinc(Object what) {
		setPolinc(what, false);
	}
	public void setPolinc(Object what, boolean rounded) {
		if (rounded)
			polinc.setRounded(what);
		else
			polinc.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}	
	public FixedLengthStringData getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(Object what) {
		billchnl.set(what);
	}	
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	public FixedLengthStringData getServunit() {
		return servunit;
	}
	public void setServunit(Object what) {
		servunit.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getSuprtos() {
		return new FixedLengthStringData(billspto.toInternal()
);
	}
	public void setSuprtos(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSuprtos().getLength()).init(obj);
	
		what = ExternalData.chop(what, billspto);
	}
	public PackedDecimalData getSuprto(BaseData indx) {
		return getSuprto(indx.toInt());
	}
	public PackedDecimalData getSuprto(int indx) {

		switch (indx) {
			case 1 : return billspto;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSuprto(BaseData indx, Object what) {
		setSuprto(indx, what, false);
	}
	public void setSuprto(BaseData indx, Object what, boolean rounded) {
		setSuprto(indx.toInt(), what, rounded);
	}
	public void setSuprto(int indx, Object what) {
		setSuprto(indx, what, false);
	}
	public void setSuprto(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setBillspto(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getSuprfroms() {
		return new FixedLengthStringData(billspfrom.toInternal()
);
	}
	public void setSuprfroms(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSuprfroms().getLength()).init(obj);
	
		what = ExternalData.chop(what, billspfrom);
	}
	public PackedDecimalData getSuprfrom(BaseData indx) {
		return getSuprfrom(indx.toInt());
	}
	public PackedDecimalData getSuprfrom(int indx) {

		switch (indx) {
			case 1 : return billspfrom;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSuprfrom(BaseData indx, Object what) {
		setSuprfrom(indx, what, false);
	}
	public void setSuprfrom(BaseData indx, Object what, boolean rounded) {
		setSuprfrom(indx.toInt(), what, rounded);
	}
	public void setSuprfrom(int indx, Object what) {
		setSuprfrom(indx, what, false);
	}
	public void setSuprfrom(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setBillspfrom(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getSuprflags() {
		return new FixedLengthStringData(billsupr.toInternal()
);
	}
	public void setSuprflags(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSuprflags().getLength()).init(obj);
	
		what = ExternalData.chop(what, billsupr);
	}
	public FixedLengthStringData getSuprflag(BaseData indx) {
		return getSuprflag(indx.toInt());
	}
	public FixedLengthStringData getSuprflag(int indx) {

		switch (indx) {
			case 1 : return billsupr;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSuprflag(BaseData indx, Object what) {
		setSuprflag(indx.toInt(), what);
	}
	public void setSuprflag(int indx, Object what) {

		switch (indx) {
			case 1 : setBillsupr(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		validflag.clear();
		tranno.clear();
		tranid.clear();
		btdate.clear();
		outstamt.clear();
		billsupr.clear();
		billspfrom.clear();
		billspto.clear();
		billfreq.clear();
		billcd.clear();
		polsum.clear();
		ptdate.clear();
		polinc.clear();
		statcode.clear();
		pstatcode.clear();
		billchnl.clear();
		cnttype.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();
		servunit.clear();		
	}


}