package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.Zctnpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ZctnpfDAO extends BaseDAO<Zctnpf> {
    public void insertZctnpf(List<Zctnpf> zctnpfList);
    public List<Zctnpf> searchZctnrevRecord(String coy, String chdrnum);
}