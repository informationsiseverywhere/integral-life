package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.B5018DTO;
import com.csc.life.regularprocessing.dataaccess.model.T5679Rec;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface B5018TempDAO extends BaseDAO<B5018DTO>{
    public List<B5018DTO> searchB5018Temp( int batchID);
    public List<B5018DTO> getALLB5018Temp();
    public void initTempTable(String company,String chdrnumStart,String chdrnumEnd,String cbunst,int batchExtractSize,T5679Rec t579Rec);
}
