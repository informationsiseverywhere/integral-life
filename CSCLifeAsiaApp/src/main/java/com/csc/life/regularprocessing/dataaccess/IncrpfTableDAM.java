package com.csc.life.regularprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: IncrpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:38
 * Class transformed from INCRPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class IncrpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 234;
	public FixedLengthStringData incrrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData incrpfRecord = incrrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(incrrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(incrrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(incrrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(incrrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(incrrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(incrrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(incrrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(incrrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(incrrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(incrrec);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(incrrec);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(incrrec);
	public PackedDecimalData crrcd = DD.crrcd.copy().isAPartOf(incrrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(incrrec);
	public PackedDecimalData origInst = DD.originst.copy().isAPartOf(incrrec);
	public PackedDecimalData lastInst = DD.lastinst.copy().isAPartOf(incrrec);
	public PackedDecimalData newinst = DD.newinst.copy().isAPartOf(incrrec);
	public PackedDecimalData origSum = DD.origsum.copy().isAPartOf(incrrec);
	public PackedDecimalData lastSum = DD.lastsum.copy().isAPartOf(incrrec);
	public PackedDecimalData newsum = DD.newsum.copy().isAPartOf(incrrec);
	public FixedLengthStringData anniversaryMethod = DD.annvry.copy().isAPartOf(incrrec);
	public FixedLengthStringData basicCommMeth = DD.bascmeth.copy().isAPartOf(incrrec);
	public PackedDecimalData pctinc = DD.pctinc.copy().isAPartOf(incrrec);
	public FixedLengthStringData refusalFlag = DD.refflag.copy().isAPartOf(incrrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(incrrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(incrrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(incrrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(incrrec);
	public FixedLengthStringData bascpy = DD.bascpy.copy().isAPartOf(incrrec);
	public FixedLengthStringData ceaseInd = DD.ceaseind.copy().isAPartOf(incrrec);
	public FixedLengthStringData rnwcpy = DD.rnwcpy.copy().isAPartOf(incrrec);
	public FixedLengthStringData srvcpy = DD.srvcpy.copy().isAPartOf(incrrec);
	public PackedDecimalData zboriginst = DD.zboriginst.copy().isAPartOf(incrrec);
	public PackedDecimalData zblastinst = DD.zblastinst.copy().isAPartOf(incrrec);
	public PackedDecimalData zbnewinst = DD.zbnewinst.copy().isAPartOf(incrrec);
	public PackedDecimalData zloriginst = DD.zloriginst.copy().isAPartOf(incrrec);
	public PackedDecimalData zllastinst = DD.zllastinst.copy().isAPartOf(incrrec);
	public PackedDecimalData zlnewinst = DD.zlnewinst.copy().isAPartOf(incrrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(incrrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(incrrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(incrrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public IncrpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for IncrpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public IncrpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for IncrpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public IncrpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for IncrpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public IncrpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("INCRPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"EFFDATE, " +
							"TRANNO, " +
							"VALIDFLAG, " +
							"STATCODE, " +
							"PSTATCODE, " +
							"CRRCD, " +
							"CRTABLE, " +
							"ORIGINST, " +
							"LASTINST, " +
							"NEWINST, " +
							"ORIGSUM, " +
							"LASTSUM, " +
							"NEWSUM, " +
							"ANNVRY, " +
							"BASCMETH, " +
							"PCTINC, " +
							"REFFLAG, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"TERMID, " +
							"BASCPY, " +
							"CEASEIND, " +
							"RNWCPY, " +
							"SRVCPY, " +
							"ZBORIGINST, " +
							"ZBLASTINST, " +
							"ZBNEWINST, " +
							"ZLORIGINST, " +
							"ZLLASTINST, " +
							"ZLNEWINST, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     effdate,
                                     tranno,
                                     validflag,
                                     statcode,
                                     pstatcode,
                                     crrcd,
                                     crtable,
                                     origInst,
                                     lastInst,
                                     newinst,
                                     origSum,
                                     lastSum,
                                     newsum,
                                     anniversaryMethod,
                                     basicCommMeth,
                                     pctinc,
                                     refusalFlag,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     termid,
                                     bascpy,
                                     ceaseInd,
                                     rnwcpy,
                                     srvcpy,
                                     zboriginst,
                                     zblastinst,
                                     zbnewinst,
                                     zloriginst,
                                     zllastinst,
                                     zlnewinst,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		effdate.clear();
  		tranno.clear();
  		validflag.clear();
  		statcode.clear();
  		pstatcode.clear();
  		crrcd.clear();
  		crtable.clear();
  		origInst.clear();
  		lastInst.clear();
  		newinst.clear();
  		origSum.clear();
  		lastSum.clear();
  		newsum.clear();
  		anniversaryMethod.clear();
  		basicCommMeth.clear();
  		pctinc.clear();
  		refusalFlag.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		termid.clear();
  		bascpy.clear();
  		ceaseInd.clear();
  		rnwcpy.clear();
  		srvcpy.clear();
  		zboriginst.clear();
  		zblastinst.clear();
  		zbnewinst.clear();
  		zloriginst.clear();
  		zllastinst.clear();
  		zlnewinst.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getIncrrec() {
  		return incrrec;
	}

	public FixedLengthStringData getIncrpfRecord() {
  		return incrpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setIncrrec(what);
	}

	public void setIncrrec(Object what) {
  		this.incrrec.set(what);
	}

	public void setIncrpfRecord(Object what) {
  		this.incrpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(incrrec.getLength());
		result.set(incrrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}