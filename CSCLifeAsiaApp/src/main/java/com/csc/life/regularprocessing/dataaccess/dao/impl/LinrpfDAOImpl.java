package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.LinrpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Linrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class LinrpfDAOImpl extends BaseDAOImpl<Linrpf> implements LinrpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(LinrpfDAOImpl.class);

	public List<Linrpf> searchLinrpfRecord(String tableId, String memName, int batchExtractSize, int batchID) {
		StringBuilder sqlLinrSelect = new StringBuilder();
		sqlLinrSelect.append(" SELECT * FROM ( ");
		sqlLinrSelect.append(" SELECT UNIQUE_NUMBER,CHDRCOY, CHDRNUM, PAYRSEQNO, INSTFROM ");
		sqlLinrSelect.append(" ,FLOOR((ROW_NUMBER()OVER( ORDER BY CHDRCOY ASC, CHDRNUM ASC, UNIQUE_NUMBER DESC)-1)/?) BATCHNUM ");
		sqlLinrSelect.append("  FROM ");
		sqlLinrSelect.append(tableId);
		sqlLinrSelect.append("   WHERE MEMBER_NAME = ? ");
		sqlLinrSelect.append(" ) MAIN WHERE BATCHNUM = ? ");
		sqlLinrSelect.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, INSTFROM ASC ");

		PreparedStatement psLinrSelect = getPrepareStatement(sqlLinrSelect.toString());
		ResultSet sqllinrpf1rs = null;
		List<Linrpf> linrpfList = new ArrayList<>();
		try {
			psLinrSelect.setInt(1, batchExtractSize);
			psLinrSelect.setString(2, memName);
			psLinrSelect.setInt(3, batchID);

			sqllinrpf1rs = executeQuery(psLinrSelect);
			while (sqllinrpf1rs.next()) {
				Linrpf linrpf = new Linrpf();
				linrpf.setChdrcoy(sqllinrpf1rs.getString("CHDRCOY"));
				linrpf.setChdrnum(sqllinrpf1rs.getString("CHDRNUM"));
				linrpf.setPayrseqno(sqllinrpf1rs.getInt("PAYRSEQNO"));
				linrpf.setInstfrom(sqllinrpf1rs.getInt("INSTFROM"));
				linrpf.setUniqueNumber(sqllinrpf1rs.getLong("UNIQUE_NUMBER"));
				linrpfList.add(linrpf);
			}
		} catch (SQLException e) {
			LOGGER.error("searchLinrRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psLinrSelect, sqllinrpf1rs);
		}
		return linrpfList;
	}
}
