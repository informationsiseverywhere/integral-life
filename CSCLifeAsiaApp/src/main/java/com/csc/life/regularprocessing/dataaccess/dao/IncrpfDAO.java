package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface IncrpfDAO extends BaseDAO<Incrpf> {
	
	public List<Incrpf> getIncrpfList( String chdrcoy,String chdrnum);
	public Incrpf getIncrData(String chdrnum, String chdrcoy, String life, String coverage, String rider, int planSuffix);
	public int getIncrCount(String chdrnum, String chdrcoy, String life, String coverage, int planSuffix);
	public boolean insertIncrList(List<Incrpf> incrpfList);
	public void deleteIncrpf(long uniqueNumber);
	public List<Incrpf> searchIncrpfRecord(Incrpf incrpf) throws SQLRuntimeException;
	public Map<String, List<Incrpf>> getIncrgpMap(String coy, List<String> chdrnumList);
	public Map<String, List<Incrpf>> searchIncrRecordByChdrnum(List<String> chdrnumList);
	public void updateValidFlag(List<Incrpf> incrpfList);
    public Incrpf getIncrByCrrcdDate(String chdrcoy,String chdrnum,  String life, String coverage,String rider, int planSuffix, int cpidate, String validflag); 
    public Map<Integer, List<Incrpf>> getIncrDatedMap(String coy, String chdrnum);
    public void updateIncrList(List<Incrpf> incrpfUpdateList);//ILIFE-8509
}
