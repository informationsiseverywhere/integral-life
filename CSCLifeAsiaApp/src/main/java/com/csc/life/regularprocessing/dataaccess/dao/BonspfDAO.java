package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.regularprocessing.dataaccess.model.B5018ForUpdateDTO;
import com.csc.life.regularprocessing.dataaccess.model.Bonspf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface BonspfDAO extends BaseDAO<Bonspf> {
    public Map<String,List<Bonspf>> searchBonspf(String coy,List<String> chdrnum);
	public List<B5018ForUpdateDTO> searchBonsPfResult(String chdrcoy,String chdrnum,String life,String coverage,String ride,int plnsfx);
    public void insertBonspf(List<Bonspf> bonspfList);
    public void updateBonspf(List<Bonspf> bonspfList);
    public Bonspf readBonspf(String chdrcoy, String chdrnum, String validflag);
}