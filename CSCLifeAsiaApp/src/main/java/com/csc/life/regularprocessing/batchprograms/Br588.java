/*
 * File: Br588.java
 * Date: 29 August 2009 22:21:59
 * Author: Quipoz Limited
 *
 * Class transformed from BR588.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.life.contractservicing.dataaccess.dao.BfrqpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Bfrqpf;
import com.csc.life.contractservicing.tablestructures.T5541rec;
import com.csc.life.flexiblepremium.dataaccess.dao.AgcmpfDAO;
import com.csc.life.flexiblepremium.dataaccess.model.Agcmpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.tablestructures.T5567rec;
import com.csc.life.productdefinition.tablestructures.T5664rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*       APL - Advance Batch Frequency Change
*       ------------------------------------
*
*  This program is cloned from P6668AT program to do only
*  frequency change for policies under APL - Advance.
*
*  Batch Processing
*  ----------------
*    Files:  AGCMBCH - Agent commission details
*            COVRMJA - Coverage/Rider Details
*            CHDRMJA - Contract Header Details
*            BFRQ    - Billing Frequency Change Details
*
*    Tables: T5679 - Valid Status Codes
*            T5541 - Frequency Conversion Factors
*            T5671 - Coverage/Rider Generic Processing
*            T5687 - General Coverage/Rider Details
*            TR517 - Waiver of Premium Component
*            T5567 - Contract Fee Parameters
*            T5664 - Preimum Rates (Age Based)
*
*   Read Billing Frequency Change Details file BFRQ and process
*   sequentially for all records found in this file. For each
*   record read do a Frequency Change and update back BFRQ file
*   by setting Validflag to '2'.
*
*   Call 'SFTLOCK' using the LOCK function.
*
*    Check the return status codes for each of the following
*    records being updated.
*
*   PROCESS CONTRACT HEADER
*    Update the contract header as follows:-
*      - set the valid flag to '2' and set the effective
*        date to default to todays date and rewrite the
*        record using the REWRT function.
*      - write a new version of the contract header
*        as follows:-
*      - set the valid flag to '1'
*      - update the transaction number, increment by +1 and
*        update the transaction date using the default of
*        todays date
*      - update the contract status code with the status
*        code from T5679 (keyed by transaction number)
*        only if there is an entry on T5679
*      - update the premium status field if the status
*        field from T5679 has entries. If entries are
*        present, then check the Bill frequency from the
*        the BFRQ file. If it is '00', i.e. single premium,
*        otherwise use the regular premium status field
*      - set the 'Effective From' date to todays date
*      - set the 'Effective To' date to VRCM-MAX-DATE
*      - if the billing frequency has changed, you should
*        not be here unless it has changed, set the billing
*        frequency to the value from the BFRQ file
*      - write the new CHDR record using the WRITR function.
*
*   PROCESS COMPONENTS(Only for NWOP Riders)
*
*    Read all the Coverage/Rider records (COVRMJA) for this
*    contract and check their status against T5679 (keyed by
*    Transaction number) and only update the Component if a
*    match exists on T5679. Also, only update the Component if
*    the INSTPREM is greater than 0.
*
*   Update each COVR record as follows:-
*      - BEGNH on the COVR record for the relevant contract,
*
*    DOWHILE there are COVRs for this contract
*      - Read Table TR517 to process only for NWOP Riders, if
*        item found set WSAA-WOP-RIDER-FLAT to 'Y' and read
*        next component record
*      - set the valid flag to '2' and the 'Effective To'
*        date to todays date and UPDAT the record,
*      - calculate the new premium as follows, read T5687
*        using CRTABLE as the item to get the frequency
*        alteration basis which is then used to read T5541
*        Both the old and the new frequencies are read to
*        obtain their respective loading factors.
*        When they are found and no errors appear, the premium
*        is calculated as follows :
*
*        New Premium =    Old Premium
*                       * (Old frequency / New Frequency)
*                       * (New Freq loading factor /
*                          Old Freq loading factor  )
*
*      - update the status code, if an entry exists on
*        T5679. Use the regular premium status code.
*      - write a new COVR record as follows, update the
*        transaction number with the transaction number from
*        the contract header. Set the CURRFROM to BTD
*        set the CURRTO to VRCM-MAX-DATE and set
*        the valid flag to '1'. Write the record with a
*        WRITR function.
*      - store the NWOP component code & new premium amount
*        into table array.
*      - Set AGCMBCH-PARAMS to space.
*        Set AGCMBCH-CHDRCOY to COVRMJA-CHDRCOY.
*        Set AGCMBCH-CHDRNUM to COVRMJA-CHDRNUM.
*        Set AGCMBCH-LIFE to COVRMJA-LIFE.
*        Set AGCMBCH-COVERAGE to COVRMJA-COVERAGE.
*        Set AGCMBCH-RIDER to COVRMJA-RIDER.
*        Set AGCMBCH-PLAN-SUFFIX to COVRMJA-PLAN-SUFFIX.
*        Set AGCMBCH-FORMAT to 'AGCMBCHREC'.
*        Set AGCMBCH-FUNCTION to BEGNH.
*    Call 'AGCMBCHIO' to read and hold the record.
*        Perform normal error checking.
*        If end of file
*            next sentence
*        Else
*            If key break(Up to and include plan suffix)
*                Rewrite the record with function 'REWRT'
*                Perform normal error checking
*                Seq AGCMBCH-STATUZ to ENDP.
*        Perform PROCESS-READ-AGCMBCH
*            until AGCMBCH-STATUZ = ENDP.
*      - NEXTR on the COVRs
*     ENDDO
*
*   PROCESS COMPONENTS(Only for WOP Riders)
*
*     If WSAA-WOP-RIDER-FLAG = 'Y'
*        - Process components agian only for WOP riders
*        - Get the rate from rate table T5664 and calculate
*          premium based on the new frequency.
*        - update back new premium into coverage file
*     End-If.
*
*
*   GENERAL HOUSEKEEPING
*
*   1) Write a PTRN record:
*      - contract key from contract header
*      - transaction number from contract header
*      - transaction effective date is todays date
*   2) Update the batch header by calling BATCUP with a
*      function of WRITS and the following parameters:
*       - Transaction count equals 1
*       - zeroise all amounts
*       - batch key from the AT linkage.
*   3) Release Softlock
*       - Release the 'SFTLOCK' using the UNLK function.
*
*   PROCESS-READ-AGCMBCH Section
*   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*   Set AGCMBCH-CURRTO to EFFDATE.
*   Set AGCMBCH-VALIDFLAG to '2'.
*   Set AGCMBCH-FUNCTION to REWRT.
*   Call 'AGCMBCHIO' to rewrite the record.
*   Perform normal error checking.
*   Set AGCMBCH-TRANNO to the new TRANNO.
*   Set AGCMBCH-VALIDFLAG to '1'.
*   Set AGCMBCH-CURRFROM to BTD
*   Set AGCMBCH-CURRTO to 99999999.
*   Compute AGCMBCH-ANNPREM rounded = AGCMBCH-ANNPREM *
*                                  (New loading factor/
*                                   Old loading factor)
*   Note that old factor and new factor must be numeric.
*   Set AGCMBCH-FUNCTION to WRITR.
*   Call 'AGCMBCHIO' to write the new record.
*   Perform normal error checking.
*   Set AGCMBCH-FUNCTION to NEXTR.
*   Call 'AGCMBCHIO' to read and hold the next record.
*   Perform normal error checking.
*   If end of file
*     Next sentence
*   Else
*    If key break(Up to and include plan suffix)
*        Rewrite the record with function 'REWRT'
*        Perform normal error checking
*        Set AGCMBCH-STATUZ to ENDP.
*
* A000-STATISTICS SECTION.
* ~~~~~~~~~~~~~~~~~~~~~~~
* AGENT/GOVERNMENT STATISTICS TRANSACTION SUBROUTINE.
*
* LIFSTTR
* ~~~~~~~
* This subroutine has two basic functions;
* a) To reverse Statistical movement records.
* b) To create  Statistical movement records.
*
* The STTR file will hold all the relevant details of
* selected transactions which affect a contract and have
* to have a statistical record written for it.
*
* Two new coverage logicals have been created for the
* Statistical subsystem. These are covrsts and covrsta.
* Covrsts allow only validflag 1 records, and Covrsta
* allows only validflag 2 records.
* Another logical AGCMSTS has been set up for the agent's
* commission records. This allows only validflag 1 records
* to be read.
* The other logical AGCMSTA set up for the old agent's
* commission records, allows only validflag 2 records,
* dormant flag 'Y' to be read.
*
* This subroutine will be included in various AT's
* which affect contracts/coverages. It is designed to
* track a policy through it's life and report on any
* changes to it. The statistical records produced, form
* the basis for the Agent Statistical subsystem.
*
* The three tables used for Statistics are;
*     T6627, T6628, T6629.
* T6628 has as it's item name, the transaction code and
* the contract status of a contract, eg. T642IF. This
* table is read first to see if any statistical (STTR)
* records are required to be written. If not the program
* will finish without any processing needed. If the table
* has valid statistical categories, then these are used
* to access T6629. An item name for T6629 could be 'IF'.
* On T6629, there are two question fields, which state
* if agent and/or government details need accumulating.
*
* The four fields under each question are used to access
* T6627. These fields refer to Age, Sum insured, Risk term
* and Premium. T6627 has items which are used to check the
* value of these fields, eg. the Age of the policy holder
* is 35. On T6627, the Age item has several values on
* To and From fields. These could be 0 - 20, 21 - 40 etc.
* The Age will be found to be within a certain band, and
* the band label for that range, eg AB, will be moved to
* the STTR record. The same principal applies for the
* other T6629 fields.
*
* T6628 splits the statistical categories into four
* areas, PREVIOUS, CURRENT, INCREASE and DECREASE.
* All previous categories refer to Covrsta records, and
* current categories refer to Covrsts records. Agcmsts
* records can refer to both. On the coverage logicals,
* the INCREASE and DECREASE are for the premium, and
* on the Agcmsts, these refer to the commission.
*
* STTR records are written for each valid T6628 category.
* So for a current record, there could be several current
* categories, therefore the corresponding number of STTR's
* will be written. Also, if the premium has increased or
* decreased, a number of STTR's will be written. This process
* will be repeated for all the AGCMSTS records attached to
* that current record.
*
* When a reversal of STTR's is required, the linkage field
* LIFS-TRANNOR has the transaction number, which the STTR's
* have to be reversed back to. The LIFS-TRANNO has the
* current tranno for a particular coverage. All STTR records
* are reversed out upto and including the TRANNOR number.
*
*****************************************************************
* </pre>
*/
public class Br588 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR588");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String wsaaWopRiderFlag = "N";
	private PackedDecimalData wsaaShortTransDate = new PackedDecimalData(6, 0).setUnsigned();
	private FixedLengthStringData wsaaTr517Key = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaRate = new ZonedDecimalData(8, 2).init(ZERO).setUnsigned();
	private String wsaaWaiverFlag = "N";
	private PackedDecimalData wsaaWopPrem = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaWopSi = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaNewFreqFee = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaAgerateTot = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaRatesPerMillieTot = new PackedDecimalData(7, 0).init(0);
	private PackedDecimalData wsaaAdjustedAge = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaOldFreqFee = new PackedDecimalData(17, 2).init(0);
	private String wsaaWopAccrFlag = "N";
	private String wsaaWopAccrFee = "N";

	private FixedLengthStringData wsaaT5567Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT5567Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT5567Key, 0);
	private FixedLengthStringData wsaaT5567Cntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT5567Key, 3);

	private FixedLengthStringData wsaaT5664Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT5664Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5664Key, 0);
	private FixedLengthStringData wsaaT5664Mortcls = new FixedLengthStringData(1).isAPartOf(wsaaT5664Key, 4);
	private FixedLengthStringData wsaaT5664Sex = new FixedLengthStringData(1).isAPartOf(wsaaT5664Key, 5);

		/* WSAA-LEXT-OPPC-RECS */
	private FixedLengthStringData[] wsaaLextOppcs = FLSInittedArray (8, 3);
	private PackedDecimalData[] wsaaLextOppc = PDArrayPartOfArrayStructure(5, 2, wsaaLextOppcs, 0);
		/* WSAA-PRIMARY-KEY */
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);

		/* WSAA-R-TABLE
		    03  WSAA-RIDER-TABLE        OCCURS 10 TIMES.                 */
	private FixedLengthStringData[] wsaaCompTable = FLSInittedArray (10, 27);
	private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaCompTable, 0);
	private FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaCompTable, 2);
	private FixedLengthStringData[] wsaaCrtable = FLSDArrayPartOfArrayStructure(4, wsaaCompTable, 4);
	private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaCompTable, 8);
	private ZonedDecimalData[] wsaaPremAmt = ZDArrayPartOfArrayStructure(17, 2, wsaaCompTable, 10);
		/* WSAA-TRANSACTION-REC */
	private PackedDecimalData wsaaAtTransactionDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaAtTransactionTime = new PackedDecimalData(6, 0);
	private PackedDecimalData wsaaAtUser = new PackedDecimalData(6, 0);
	private FixedLengthStringData wsaaAtTermid = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaAtBillfreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaAtToday = new ZonedDecimalData(8, 0);

	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(16);
	private ZonedDecimalData wsaaTermid = new ZonedDecimalData(4, 0).isAPartOf(wsaaTranid, 0).setUnsigned();
	private ZonedDecimalData wsaaDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 4).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 10).setUnsigned();
		/* ERRORS */
	private static final String h947 = "H947";
	private static final String f294 = "F294";
	private static final String f290 = "F290";
	private static final String f151 = "F151";
	private static final String h966 = "H966";
	private static final String f358 = "F358";

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

		/* INDIC-AREA */
	private Indicator[] indicTable = IndicatorInittedArray(99, 1);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Agcmpf agcmbchIO = new Agcmpf();
	private Bfrqpf bfrqIO = new Bfrqpf();
//	private BfrqTableDAM bfrqIO = new BfrqTableDAM();
//	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private Chdrpf chdrmjaIO = new Chdrpf();
	private Covrpf covrmjaIO = new Covrpf();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lextpf lextIO = new Lextpf();
//	private PayrTableDAM payrIO = new PayrTableDAM();
//	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5679rec t5679rec = new T5679rec();
	private T5541rec t5541rec = new T5541rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private T5674rec t5674rec = new T5674rec();
	private Tr517rec tr517rec = new Tr517rec();
	private T5567rec t5567rec = new T5567rec();
	private T5664rec t5664rec = new T5664rec();
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
    
	private Payrpf payrIO = new Payrpf();
	
	private Iterator<Bfrqpf> iter;
	private int batchID;
    private int batchExtractSize;
    
//	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private BfrqpfDAO bfrqpfDAO = getApplicationContext().getBean("bfrqpfDAO", BfrqpfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
    private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
    private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
    private CovrpfDAO covrpfDAO =  getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
    private AgcmpfDAO agcmpfDAO =  getApplicationContext().getBean("agcmpfDAO", AgcmpfDAO.class);
    private LextpfDAO lextpfDAO =  getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
    
	private Map<String, List<Chdrpf>> chdrpfMap = new HashMap<String, List<Chdrpf>>();
	private Map<String, List<Payrpf>> payrpfMap = new HashMap<String, List<Payrpf>>();
	private Map<String, List<Covrpf>> covrpfMap = new HashMap<String, List<Covrpf>>();
	private Map<String, List<Agcmpf>> agcmpfMap = new HashMap<String, List<Agcmpf>>();
	private Map<String, List<Lextpf>> lextpfMap = new HashMap<String, List<Lextpf>>();
	
	private List<Ptrnpf> ptrnpfList = new ArrayList<Ptrnpf>();
	private List<Chdrpf> chdrpfListUpdt = new ArrayList<Chdrpf>();
	private List<Chdrpf> chdrpfListInst = new ArrayList<Chdrpf>();
	private List<Payrpf> payrpfListUpdt = new ArrayList<Payrpf>();
	private List<Payrpf> payrpfListInst = new ArrayList<Payrpf>();
	private List<Covrpf> covrpfListUpdt = new ArrayList<Covrpf>();
	private List<Covrpf> covrpfListInst = new ArrayList<Covrpf>();
	private List<Agcmpf> agcmpfListUpdt = new ArrayList<Agcmpf>();
	private List<Agcmpf> agcmpfListInst = new ArrayList<Agcmpf>();
	private List<Bfrqpf> bfrqpfList = new ArrayList<Bfrqpf>();	

	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextr2070,
		eof2080,
		exit2090,
		nextrCovr360,
		wopAccr385,
		updateStatus386,
		retrieveNextRecord393,
		nextrCovr360a,
		a110Check
	}

	public Br588() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		/* Open required files.*/
		wsspEdterror.set(varcom.oK);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaAtTransactionDate.set(datcon1rec.intDate);
		wsaaAtTransactionTime.set(varcom.vrcmTime);
		wsaaAtUser.set(ZERO);
		wsaaAtTermid.set(SPACES);
		/* Move fields to primary file key.*/
//		bfrqIO.setRecKeyData(SPACES);
//		bfrqIO.setFunction(varcom.begnh);
//		bfrqIO.setFormat(formatsInner.bfrqrec);
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				batchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				batchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			batchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
		this.readChunkRecord();
	}

	private void readChunkRecord() {
		List<Bfrqpf> pfList = this.bfrqpfDAO.getAll(this.batchExtractSize, this.batchID); 
		if (null == pfList || pfList.isEmpty()) {
			wsspEdterror.set(varcom.endp);
			return;
		}
		this.iter = pfList.iterator();
		List<String> chdrnumList = new ArrayList<>();
		for (Bfrqpf pf : pfList) {
			chdrnumList.add(pf.getChdrnum());
		}
		String coy = pfList.get(0).getChdrcoy();
		chdrpfMap = this.chdrpfDAO.searchChdrRecordByChdrnum(chdrnumList);
		// ILIFE-5133
		if(chdrpfMap!=null&&!chdrpfMap.isEmpty()){
			for(List<Chdrpf> chdrpfs:chdrpfMap.values()){
				for(Chdrpf c:chdrpfs){
					c.setChdrcoy(coy.charAt(0));
				}
			}
		}
		
		payrpfMap = this.payrpfDAO.getPayrLifMap(coy, chdrnumList);
		covrpfMap = this.covrpfDAO.searchCovrMap(coy, chdrnumList);
		agcmpfMap = this.agcmpfDAO.searchAgcmRecordByChdrnum(chdrnumList);
		lextpfMap = this.lextpfDAO.searchLextpfMap(coy, chdrnumList);
	}

	protected void readFile2000() {
		if (!iter.hasNext()) {
			batchID++;
			readChunkRecord();
			if(wsspEdterror.equals(varcom.endp)){
				return;
			}
		}
		this.bfrqIO = iter.next();

		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readFile2010();
					readChdr2020();
					softlock2030();
					updat2060();
				case nextr2070:
					nextr2070();
				case eof2080:
					eof2080();
				case exit2090:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
//		SmartFileCode.execute(appVars, bfrqIO);
//		if (isNE(bfrqIO.getStatuz(),varcom.oK)
//		&& isNE(bfrqIO.getStatuz(),varcom.endp)) {
//			syserrrec.params.set(bfrqIO.getParams());
//			syserrrec.statuz.set(bfrqIO.getStatuz());
//			fatalError600();
//		}
//		if (isEQ(bfrqIO.getStatuz(),varcom.endp)) {
//			goTo(GotoLabel.eof2080);
//		}
		wsaaChdrcoy.set(bfrqIO.getChdrcoy());
		wsaaChdrnum.set(bfrqIO.getChdrnum());
		wsaaAtBillfreq.set(bfrqIO.getBillfreq());
	}

protected void readChdr2020()
	{
//		chdrmjaIO.setDataArea(SPACES);
//		chdrmjaIO.setChdrcoy(wsaaChdrcoy);
//		chdrmjaIO.setChdrnum(wsaaChdrnum);
//		chdrmjaIO.setFunction(varcom.readr);
//		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
//		SmartFileCode.execute(appVars, chdrmjaIO);
//		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(chdrmjaIO.getParams());
//			syserrrec.statuz.set(chdrmjaIO.getStatuz());
//			fatalError600();
//		}
		if (chdrpfMap != null && chdrpfMap.containsKey(wsaaChdrnum)) {
			chdrmjaIO = chdrpfMap.get(wsaaChdrnum).get(0);
		} else {
			syserrrec.params.set(wsaaChdrnum);
			fatalError600();
		}
	
		if (isNE(chdrmjaIO.getPtdate(),chdrmjaIO.getBtdate())) {
			conlogrec.error.set(h947);
			conlogrec.params.set(chdrmjaIO.getChdrnum());
			callConlog003();
			goTo(GotoLabel.nextr2070);
		}
	}

protected void softlock2030()
	{
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(bsprIO.getCompany());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(bfrqIO.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			conlogrec.error.set(sftlockrec.statuz);
			conlogrec.params.set(sftlockrec.sftlockRec);
			callConlog003();
			goTo(GotoLabel.nextr2070);
		}
		initialise100();
		processContractHeader200();
		processPayrRecord250();
//		getFirstComponent300();
		wsaaMiscellaneousInner.wsaaI.set(ZERO);
		wsaaWopRiderFlag = "N";
		wsaaWopAccrFlag = "N";
		wsaaWopAccrFee = "N";
//		covrmjaIO.setStatuz(SPACES);
		if (covrpfMap != null && covrpfMap.containsKey(chdrmjaIO.getChdrnum())) {
			for (Covrpf covrpf : covrpfMap.get(chdrmjaIO.getChdrnum())) {
				if(!"2".equals(covrpf.getValidflag())){
					covrmjaIO = covrpf;
					processCompNwop330();
				}
			}
		}
//		while ( !(isEQ(covrmjaIO.getStatuz(),varcom.endp))) {

//		}
		if (isEQ(wsaaWopRiderFlag,"Y")) {
			//getFirstComponent300();
			//covrmjaIO.setStatuz(SPACES);
//			while ( !(isEQ(covrmjaIO.getStatuz(),varcom.endp))) {
//				processCompWop330a();
//			}
			if (covrpfMap != null && covrpfMap.containsKey(chdrmjaIO.getChdrnum())) {
				for (Covrpf covrpf : covrpfMap.get(chdrmjaIO.getChdrnum())) {
					covrmjaIO = covrpf;
					processCompWop330a();
				}
			}
		}
		accumInstpremWrtChdrmja400();
		writeHistoryRlseLocks500();
		a000Statistics();
	}

protected void updat2060()
	{
		bfrqIO.setValidflag("2");
		bfrqpfList.add(bfrqIO);
//		bfrqIO.setFunction(varcom.rewrt);
//		SmartFileCode.execute(appVars, bfrqIO);
//		if (isNE(bfrqIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(bfrqIO.getParams());
//			syserrrec.statuz.set(bfrqIO.getStatuz());
//			fatalError600();
//		}
	}

protected void nextr2070()
	{
		//bfrqIO.setFunction(varcom.nextr);
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		/*  Check record is required for processing.*/
		/*  Softlock the record if it is to be updated.*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/** Update database records.*/
		/*EXIT*/
	}

	protected void commit3500() {
		chdrpfDAO.updateInvalidChdrRecord(this.chdrpfListUpdt);
		chdrpfListUpdt.clear();
		chdrpfDAO.insertChdrpfMatureList(this.chdrpfListInst, datcon1rec.intDate.toInt());
		chdrpfListInst.clear();

		payrpfDAO.updatePayrRecord(this.payrpfListUpdt, wsaaShortTransDate.toInt());
		payrpfListUpdt.clear();
		payrpfDAO.insertPayrpfList(this.payrpfListInst);
		payrpfListInst.clear();

		covrpfDAO.updateCovrValidFlag(this.covrpfListUpdt);
		covrpfListUpdt.clear();
		covrpfDAO.insertCovrpfList(this.covrpfListInst);
		covrpfListInst.clear();
		
		agcmpfDAO.updateInvalidRecord(this.agcmpfListUpdt);
		agcmpfListUpdt.clear();
		agcmpfDAO.insertAgcmpfList(this.agcmpfListInst);
		agcmpfListInst.clear();
		
		bfrqpfDAO.updateBfrqpfInvalid(bfrqpfList);
		bfrqpfList.clear();
		
		ptrnpfDAO.insertPtrnPF(ptrnpfList);
		ptrnpfList.clear();
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

	protected void close4000() {
		chdrpfMap.clear();
		chdrpfMap = null;
		payrpfMap.clear();
		payrpfMap = null;
		covrpfMap.clear();
		covrpfMap=null;
		agcmpfMap.clear();
		agcmpfMap=null;
		lextpfMap.clear();
		lextpfMap=null;
		
		chdrpfListUpdt.clear();
		chdrpfListUpdt = null;
		chdrpfListInst.clear();
		chdrpfListInst = null;
		payrpfListUpdt.clear();
		payrpfListUpdt = null;
		payrpfListInst.clear();
		payrpfListInst = null;
		bfrqpfList.clear();
		bfrqpfList = null;
		ptrnpfList.clear();
		ptrnpfList = null;
		covrpfListUpdt.clear();
		covrpfListUpdt=null;
		covrpfListInst.clear();
		covrpfListInst=null;
		agcmpfListUpdt.clear();
		agcmpfListUpdt=null;
		agcmpfListInst.clear();
		agcmpfListInst=null;
		
		iter = null;
		lsaaStatuz.set(varcom.oK);
	}

protected void initialise100()
	{
		initVariables110();
		retrieveContractHeader120();
		convertTransactionDate130();
	}

protected void initVariables110()
	{
		wsaaMiscellaneousInner.wsaaTotInstprem.set(ZERO);
		for (wsaaMiscellaneousInner.wsaaI.set(1); !(isGT(wsaaMiscellaneousInner.wsaaI, 10)); wsaaMiscellaneousInner.wsaaI.add(1)){
			wsaaCrtable[wsaaMiscellaneousInner.wsaaI.toInt()].set(SPACES);
			wsaaLife[wsaaMiscellaneousInner.wsaaI.toInt()].set(SPACES);
			wsaaPremAmt[wsaaMiscellaneousInner.wsaaI.toInt()].set(ZERO);
		}
	}

protected void retrieveContractHeader120()
	{
//		chdrmjaIO.setFunction(varcom.readh);
//		SmartFileCode.execute(appVars, chdrmjaIO);
//		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(chdrmjaIO.getParams());
//			syserrrec.statuz.set(chdrmjaIO.getStatuz());
//			fatalError600();
//		}
		/* Read the PAYR file*/
//		payrIO.setDataArea(SPACES);
//		payrIO.setChdrcoy(wsaaChdrcoy);
//		payrIO.setChdrnum(wsaaChdrnum);
//		payrIO.setPayrseqno(1);
//		payrIO.setValidflag("1");
//		payrIO.setFunction(varcom.readh);
//		payrIO.setFormat(formatsInner.payrrec);
//		SmartFileCode.execute(appVars, payrIO);
//		if (isNE(payrIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(payrIO.getParams());
//			syserrrec.statuz.set(payrIO.getStatuz());
//			fatalError600();
//		}
		
		payrIO = null;
		if (payrpfMap != null && payrpfMap.containsKey(wsaaChdrnum)) {
			for (Payrpf c : payrpfMap.get(wsaaChdrnum)) {
				if (c.getPayrseqno() == 1 && "1".equals(c.getValidflag())) {
					payrIO = c;
					break;
				}
			}
		}
		
		if (payrIO == null) {
			syserrrec.params.set(wsaaChdrnum);
			fatalError600();
		}
		
		//covrmjaIO.setParams(SPACES);
	}

protected void convertTransactionDate130()
	{
		/* Truncate left two digits of date, the century, to leave YYMMDD*/
		/* format.  WSAA-AT-TRANSACTION-DATE then be in CCYYMMDD format,*/
		/* whilst WSAA-SHORT-TRANS-DATE will be in YYMMDD format.*/
		wsaaShortTransDate.set(wsaaAtTransactionDate);
		/*EXIT*/
	}

protected void processContractHeader200()
	{
		updateOriginalChdr210();
		createNewContractHeader240();
	}

protected void updateOriginalChdr210()
	{
		
		Chdrpf chdr = new Chdrpf(chdrmjaIO);
		chdr.setValidflag('2');
		chdr.setCurrto(chdr.getBtdate());
		this.chdrpfListUpdt.add(chdr);
//		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
//		chdrmjaIO.setFunction(varcom.rewrt);
//		SmartFileCode.execute(appVars, chdrmjaIO);
//		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(chdrmjaIO.getParams());
//			syserrrec.statuz.set(chdrmjaIO.getStatuz());
//			fatalError600();
//		}
	}

protected void createNewContractHeader240()
	{
		/*    Read Table T5679*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bfrqIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		else {
			t5679rec.t5679Rec.set(itemIO.getGenarea());
		}
		/*   Update the Status field*/
		if (isNE(t5679rec.setCnRiskStat,SPACES)) {
			chdrmjaIO.setStatcode(t5679rec.setCnRiskStat.toString());
		}
		/*   Update the Premium Status field*/
		if (isNE(t5679rec.setCnPremStat,SPACES)) {
			chdrmjaIO.setPstcde(t5679rec.setCnPremStat.toString());
		}
		/*   Update relevant billing fields*/
		chdrmjaIO.setValidflag('1');
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(),1).toInt());
		wsaaDate.set(wsaaShortTransDate);
		wsaaUser.set(wsaaAtUser);
		// ILIFE-5133
		chdrmjaIO.setTranid(wsaaTranid.toString().substring(0, 14));
		chdrmjaIO.setCurrfrom(chdrmjaIO.getBtdate());
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate.toInt());
		wsaaMiscellaneousInner.wsaaCompFreq.set(chdrmjaIO.getBillfreq());
		chdrmjaIO.setBillfreq(wsaaAtBillfreq.toString());
	}

protected void processPayrRecord250()
	{
		updateOriginalPayr210();
		createNewPayrRecord240();
	}

protected void updateOriginalPayr210()
	{
		Payrpf payrpf=new Payrpf(payrIO);
		payrpf.setValidflag("2");
		this.payrpfListUpdt.add(payrpf);
//		payrIO.setFormat(formatsInner.payrrec);
//		payrIO.setFunction(varcom.rewrt);
//		SmartFileCode.execute(appVars, payrIO);
//		if (isNE(payrIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(payrIO.getParams());
//			syserrrec.statuz.set(payrIO.getStatuz());
//			fatalError600();
//		}
	}

protected void createNewPayrRecord240()
	{
		/*   Update the Premium Status field*/
		if (isNE(t5679rec.setCnPremStat,SPACES)) {
			payrIO.setPstatcode(t5679rec.setCnPremStat.toString());
		}
		/*   Update relevant billing fields*/
		payrIO.setValidflag("1");
		payrIO.setTranno(chdrmjaIO.getTranno());
		payrIO.setTransactionDate(wsaaShortTransDate.toInt());
		payrIO.setTermid(wsaaAtTermid.toString());
		payrIO.setUser(wsaaAtUser.toInt());
		wsaaMiscellaneousInner.wsaaCompFreq.set(payrIO.getBillfreq());
		payrIO.setBillfreq(wsaaAtBillfreq.toString());
		payrIO.setEffdate(payrIO.getBtdate());
		/*EXIT1*/
	}

//protected void getFirstComponent300()
//	{
//		//begin310();
//	}

	/**
	* <pre>
	*    Retrieve first Coverage for this contract.
	* </pre>
	*/
//protected void begin310()
//	{
//		covrmjaIO.setDataArea(SPACES);
//		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
//		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
//		covrmjaIO.setPlanSuffix(9999);
//		covrmjaIO.setFormat(formatsInner.covrmjarec);
//		covrmjaIO.setFunction(varcom.begn);
//		//performance improvement --  atiwari23
//		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//		covrmjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
//
//		SmartFileCode.execute(appVars, covrmjaIO);
//		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
//		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
//			syserrrec.params.set(covrmjaIO.getParams());
//			syserrrec.statuz.set(covrmjaIO.getStatuz());
//			fatalError600();
//		}
//		if (isNE(chdrmjaIO.getChdrnum(),covrmjaIO.getChdrnum())
//		|| isNE(chdrmjaIO.getChdrcoy(),covrmjaIO.getChdrcoy())
//		|| isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
//			covrmjaIO.setStatuz(varcom.endp);
//		}
//		
//		
//		
//	}
	protected void processCompNwop330() {
		readTr517330();
		if("Y".equals(wsaaWopRiderFlag)){
			return;
		}
		readT5687340();
		checkStatusAndAction350();
		//nextrCovr360();
	}
//protected void processCompNwop330()
//	{
//		GotoLabel nextMethod = GotoLabel.DEFAULT;
//		while (true) {
//			try {
//				switch (nextMethod) {
//				case DEFAULT:
//					readTr517330();
//					readT5687340();
//					checkStatusAndAction350();
//				case nextrCovr360:
//					nextrCovr360();
//				}
//				break;
//			}
//			catch (GOTOException e){
//				nextMethod = (GotoLabel) e.getNextMethod();
//			}
//		}
//	}

	protected void readTr517330() {
		/* Read Table TR517 to check for WOP riders, If item found */
		/* don't process. */
		wsaaTr517Key.set(covrmjaIO.getCrtable());
		a300ReadTr517();
		if (isNE(tr517rec.tr517Rec, SPACES)) {
			if (isNE(tr517rec.zrwvflg04, "Y")) {
				wsaaWopRiderFlag = "Y";
				//goTo(GotoLabel.nextrCovr360);
				return;
			} else {
				wsaaWopAccrFlag = "Y";
				if (isEQ(tr517rec.zrwvflg03, "Y")) {
					wsaaWopAccrFee = "Y";
				}
			}
		}
	}

	/**
	* <pre>
	**** Read Table T5687 to obtain Single Premium Indicator and
	**** Frequency Alteration Basis.
	* </pre>
	*/
protected void readT5687340()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCurrfrom());
		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(formatsInner.itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),covrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(),covrmjaIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f294);
			fatalError600();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void checkStatusAndAction350()
	{
		/*    Check if component is to be updated.*/
		wsaaMiscellaneousInner.wsaaProcessComponent.set("N");
		if (isGT(covrmjaIO.getInstprem(),0)
		&& isNE(t5687rec.singlePremInd,"Y")) {
			checkComponent360();
			if (wsaaMiscellaneousInner.processComponent.isTrue()) {
				updateComponent380();
			}
		}
	}

//protected void nextrCovr360()
//	{
//		/*    Get next coverage record*/
//		covrmjaIO.setFunction(varcom.nextr);
//		SmartFileCode.execute(appVars, covrmjaIO);
//		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
//		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
//			syserrrec.params.set(covrmjaIO.getParams());
//			syserrrec.statuz.set(covrmjaIO.getStatuz());
//			fatalError600();
//		}
//		if (isNE(covrmjaIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
//		|| isNE(covrmjaIO.getChdrnum(),chdrmjaIO.getChdrnum())) {
//			covrmjaIO.setStatuz(varcom.endp);
//		}
//		/*EXIT*/
//	}

protected void checkComponent360()
	{
		check360();
	}

protected void check360()
	{
		/*  Check for match on Premium and Risk Statii for Coverage*/
		wsaaMiscellaneousInner.wsaaStatCount.set(1);
		if (isEQ(covrmjaIO.getRider(),"00")) {
			while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
			|| isEQ(t5679rec.covPremStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getPstatcode()))) {
				wsaaMiscellaneousInner.wsaaStatCount.add(1);
			}

			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.wsaaStatCount.set(1);
				while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
				|| (isEQ(t5679rec.covRiskStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getStatcode())))) {
					wsaaMiscellaneousInner.wsaaStatCount.add(1);
				}

			}
			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.processComponent.setTrue();
			}
		}
		else {
			/*   Check for match on Premium and Risk Statii for Rider*/
			wsaaMiscellaneousInner.wsaaStatCount.set(1);
			while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
			|| (isEQ(t5679rec.ridPremStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getPstatcode())))) {
				wsaaMiscellaneousInner.wsaaStatCount.add(1);
			}

			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.wsaaStatCount.set(1);
				while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
				|| (isEQ(t5679rec.ridRiskStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getStatcode())))) {
					wsaaMiscellaneousInner.wsaaStatCount.add(1);
				}

			}
			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.processComponent.setTrue();
			}
		}
	}

protected void updateComponent380()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					updateOriginalCovr382();
					createNewCovr385();
				case wopAccr385:
					wopAccr385();
				case updateStatus386:
					updateStatus386();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateOriginalCovr382()
	{
		Covrpf covrpf = new Covrpf(covrmjaIO);
		covrpf.setValidflag("2");
		covrpf.setCurrto(chdrmjaIO.getBtdate());
		this.covrpfListUpdt.add(covrpf);
//		covrmjaIO.setFunction(varcom.updat);
//		SmartFileCode.execute(appVars, covrmjaIO);
//		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(covrmjaIO.getParams());
//			syserrrec.statuz.set(covrmjaIO.getStatuz());
//			fatalError600();
//		}
	}

protected void createNewCovr385()
	{
		/*    For Accelerated Crisis Wavier, check whether it waives policy*/
		/*    fee, if yes, then we have to subtract old policy fee from old*/
		/*    SI.                                                          */
		if (isEQ(wsaaWopAccrFlag,"Y")
		&& isEQ(wsaaWopAccrFee,"Y")
		&& isNE(t5688rec.feemeth,SPACES)) {
			a400ReadT5567();
			setPrecision(covrmjaIO.getSumins(), 2);
			covrmjaIO.setSumins(sub(covrmjaIO.getSumins(),wsaaOldFreqFee).getbigdata());
		}
		/* Read table T5541 to obtain loading factors for both*/
		/* old and new frequencies.*/
		wsaaMiscellaneousInner.wsaaInstpremFound.set(SPACES);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5541);
		itdmIO.setItemitem(t5687rec.xfreqAltBasis);
		itdmIO.setItmfrm(covrmjaIO.getCurrfrom());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),covrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5541)
		|| isNE(itdmIO.getItemitem(),t5687rec.xfreqAltBasis)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			compute(wsaaMiscellaneousInner.wsaaTotInstprem, 2).set(add(wsaaMiscellaneousInner.wsaaTotInstprem, covrmjaIO.getInstprem()));
			goTo(GotoLabel.updateStatus386);
		}
		else {
			t5541rec.t5541Rec.set(itdmIO.getGenarea());
		}
		wsaaMiscellaneousInner.wsaaCalcNewfreq.set(wsaaAtBillfreq);
		wsaaMiscellaneousInner.wsaaCalcOldfreq.set(wsaaMiscellaneousInner.wsaaCompFreq);
		/* Get the Frequency Conversion Multiplication Factor*/
		/* Calculate the new INSTPREM*/
		/* Calculate the accumulating INSTPREM for SINSTAMT01*/
		wsaaMiscellaneousInner.wsaaNewFreqFound.set(SPACES);
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 12)
		|| isEQ(wsaaMiscellaneousInner.wsaaNewFreqFound, "Y")); wsaaMiscellaneousInner.wsaaSub.add(1)){
			if (isEQ(t5541rec.freqcy[wsaaMiscellaneousInner.wsaaSub.toInt()], wsaaMiscellaneousInner.wsaaCalcNewfreq)) {
				wsaaMiscellaneousInner.wsaaNewLfact.set(t5541rec.lfact[wsaaMiscellaneousInner.wsaaSub.toInt()]);
				wsaaMiscellaneousInner.wsaaNewFreqFound.set("Y");
			}
		}
		wsaaMiscellaneousInner.wsaaOldFreqFound.set(SPACES);
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 12)
		|| isEQ(wsaaMiscellaneousInner.wsaaOldFreqFound, "Y")); wsaaMiscellaneousInner.wsaaSub.add(1)){
			if (isEQ(t5541rec.freqcy[wsaaMiscellaneousInner.wsaaSub.toInt()], wsaaMiscellaneousInner.wsaaCalcOldfreq)) {
				wsaaMiscellaneousInner.wsaaOldLfact.set(t5541rec.lfact[wsaaMiscellaneousInner.wsaaSub.toInt()]);
				wsaaMiscellaneousInner.wsaaOldFreqFound.set("Y");
			}
		}
		if (isEQ(wsaaWopAccrFlag,"Y")) {
			goTo(GotoLabel.wopAccr385);
		}
		if (isEQ(wsaaMiscellaneousInner.wsaaOldFreqFound, "Y")
		&& isEQ(wsaaMiscellaneousInner.wsaaNewFreqFound, "Y")) {
			wsaaMiscellaneousInner.wsaaInstpremFound.set("Y");
			setPrecision(covrmjaIO.getInstprem(), 3);
			covrmjaIO.setInstprem(mult((div((mult(covrmjaIO.getInstprem(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))).getbigdata());
			setPrecision(covrmjaIO.getZbinstprem(), 3);
			covrmjaIO.setZbinstprem(mult((div((mult(covrmjaIO.getZbinstprem(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))).getbigdata());
			setPrecision(covrmjaIO.getZlinstprem(), 3);
			covrmjaIO.setZlinstprem(mult((div((mult(covrmjaIO.getZlinstprem(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))).getbigdata());
			compute(wsaaMiscellaneousInner.wsaaTotInstprem, 2).set(add(wsaaMiscellaneousInner.wsaaTotInstprem, covrmjaIO.getInstprem()));
		}
		else {
			compute(wsaaMiscellaneousInner.wsaaTotInstprem, 2).set(add(wsaaMiscellaneousInner.wsaaTotInstprem, covrmjaIO.getInstprem()));
		}
		goTo(GotoLabel.updateStatus386);
	}

protected void wopAccr385()
	{
		if (isEQ(wsaaMiscellaneousInner.wsaaOldFreqFound, "Y")
		&& isEQ(wsaaMiscellaneousInner.wsaaNewFreqFound, "Y")) {
			setPrecision(covrmjaIO.getSumins(), 3);
			covrmjaIO.setSumins(mult((div((mult(covrmjaIO.getSumins(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))).getbigdata());
			setPrecision(covrmjaIO.getSumins(), 2);
			covrmjaIO.setSumins(add(covrmjaIO.getSumins(),wsaaNewFreqFee).getbigdata());
		}
		wsaaWopSi.set(covrmjaIO.getSumins());
		a200CalcWopPrem();
		wsaaWopAccrFlag = "N";
	}

protected void updateStatus386()
	{
		/*   Update the Risk Status Codes*/
		if (isEQ(covrmjaIO.getRider(),"00")) {
			if (isNE(t5679rec.setCovRiskStat,SPACES)) {
				covrmjaIO.setStatcode(t5679rec.setCovRiskStat.toString());
			}
		}
		else {
			if (isNE(t5679rec.setRidRiskStat,SPACES)) {
				covrmjaIO.setStatcode(t5679rec.setRidRiskStat.toString());
			}
		}
		/*  Update Premium Status Code*/
		if (isEQ(covrmjaIO.getRider(),"00")) {
			if (isNE(t5679rec.setCovPremStat,SPACES)) {
				covrmjaIO.setPstatcode(t5679rec.setCovPremStat.toString());
			}
		}
		else {
			if (isNE(t5679rec.setRidPremStat,SPACES)) {
				covrmjaIO.setPstatcode(t5679rec.setRidPremStat.toString());
			}
		}
		/*    Write the updated record*/
		covrmjaIO.setValidflag("1");
		covrmjaIO.setCurrfrom(chdrmjaIO.getBtdate());
		covrmjaIO.setCurrto(varcom.vrcmMaxDate.toInt());
		covrmjaIO.setTranno(chdrmjaIO.getTranno());
		covrmjaIO.setTransactionDate(wsaaShortTransDate.toInt());
		covrmjaIO.setTransactionTime(wsaaAtTransactionTime.toInt());
		covrmjaIO.setTermid(wsaaTermid.toString());
		covrmjaIO.setUser(wsaaUser.toInt());
		this.covrpfListInst.add(covrmjaIO);
//		covrmjaIO.setFormat(formatsInner.covrmjarec);
//		covrmjaIO.setFunction(varcom.writr);
		wsaaMiscellaneousInner.wsaaI.add(1);
		wsaaCrtable[wsaaMiscellaneousInner.wsaaI.toInt()].set(covrmjaIO.getCrtable());
		wsaaPremAmt[wsaaMiscellaneousInner.wsaaI.toInt()].set(covrmjaIO.getInstprem());
		wsaaLife[wsaaMiscellaneousInner.wsaaI.toInt()].set(covrmjaIO.getLife());
//		SmartFileCode.execute(appVars, covrmjaIO);
//		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(covrmjaIO.getParams());
//			syserrrec.statuz.set(covrmjaIO.getStatuz());
//			fatalError600();
//		}
		/*   Update the Agent Commission Records   (AGCM)*/
		/*   Retrieve all AGCM records for each valid COVR record read*/
		
		if (agcmpfMap != null && agcmpfMap.containsKey(covrmjaIO.getChdrnum())) {
			for (Agcmpf c : agcmpfMap.get(covrmjaIO.getChdrnum())) {
				if (covrmjaIO.getLife().equals(c.getLife())
						&& covrmjaIO.getCoverage().equals(c.getCoverage())
						&& covrmjaIO.getRider().equals(c.getRider())
						&& covrmjaIO.getPlanSuffix() == c.getPlnsfx()
						&& covrmjaIO.getChdrcoy().equals(c.getChdrcoy())) {
					agcmbchIO = c;
					processReadAgcmbch390();
				}
			}
		}
		
		
//		agcmbchIO.setChdrcoy(covrmjaIO.getChdrcoy());
//		agcmbchIO.setChdrnum(covrmjaIO.getChdrnum());
//		agcmbchIO.setLife(covrmjaIO.getLife());
//		agcmbchIO.setCoverage(covrmjaIO.getCoverage());
//		agcmbchIO.setRider(covrmjaIO.getRider());
//		agcmbchIO.setPlnsfx(covrmjaIO.getPlanSuffix());
//		agcmbchIO.setFormat(formatsInner.agcmbchrec);
//		agcmbchIO.setFunction(varcom.begn);
//		SmartFileCode.execute(appVars, agcmbchIO);
//		if (isNE(agcmbchIO.getStatuz(),varcom.oK)
//		&& isNE(agcmbchIO.getStatuz(),varcom.endp)) {
//			syserrrec.params.set(agcmbchIO.getParams());
//			syserrrec.statuz.set(agcmbchIO.getStatuz());
//			fatalError600();
//		}
//		/*   Check if valid AGCM record retrived*/
//		if (isEQ(agcmbchIO.getStatuz(),varcom.endp)) {
//			/*NEXT_SENTENCE*/
//		}
//		else {
//			if (isNE(agcmbchIO.getChdrcoy(),covrmjaIO.getChdrcoy())
//			|| isNE(agcmbchIO.getChdrnum(),covrmjaIO.getChdrnum())
//			|| isNE(agcmbchIO.getLife(),covrmjaIO.getLife())
//			|| isNE(agcmbchIO.getCoverage(),covrmjaIO.getCoverage())
//			|| isNE(agcmbchIO.getRider(),covrmjaIO.getRider())
//			|| isNE(agcmbchIO.getPlanSuffix(),covrmjaIO.getPlanSuffix())) {
//				agcmbchIO.setStatuz(varcom.endp);
//			}
//		}
//		/*   Process AGCM records until end of file*/
//		while ( !(isEQ(agcmbchIO.getStatuz(),varcom.endp))) {
//			processReadAgcmbch390();
//		}

	}

//protected void processReadAgcmbch390()
//	{
//		GotoLabel nextMethod = GotoLabel.DEFAULT;
//		while (true) {
//			try {
//				switch (nextMethod) {
//				case DEFAULT:
//					setUpDataArea391();
//					createNewRecord397();
//				case retrieveNextRecord393:
//					retrieveNextRecord393();
//				}
//				break;
//			}
//			catch (GOTOException e){
//				nextMethod = (GotoLabel) e.getNextMethod();
//			}
//		}
//	}
	protected void processReadAgcmbch390() {
		/* Do not process single premium AGCMs.*/
		if (isEQ(agcmbchIO.getPtdate(),ZERO)) {
//			goTo(GotoLabel.retrieveNextRecord393);
			return;
		}
		setUpDataArea391();
		createNewRecord397();
		// retrieveNextRecord393();
	}

	/**
	* <pre>
	*   Set up Fields for AGCM
	* </pre>
	*/
protected void setUpDataArea391()
	{
		Agcmpf agcmpf = new Agcmpf(agcmbchIO);
		agcmpf.setCurrto(payrIO.getBtdate());
		agcmpf.setValidflag("2");
		this.agcmpfListUpdt.add(agcmpf);
//		agcmbchIO.setFunction(varcom.writd);
//		SmartFileCode.execute(appVars, agcmbchIO);
//		if (isNE(agcmbchIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(agcmbchIO.getParams());
//			syserrrec.statuz.set(agcmbchIO.getStatuz());
//			fatalError600();
//		}
	}

	/**
	* <pre>
	*   Create new AGCM record
	* </pre>
	*/
protected void createNewRecord397()
	{
		/*   Calculate new annual premium*/
		/*   The COVR calculation for the new premium uses the following*/
		/*   formula:*/
		/*   X = P*(A/C)*(D/B)   where P = the old premium*/
		/*                             A = old frequency*/
		/*                             B = old loading factor*/
		/*                             C = new frequency*/
		/*                             D = new loading factor*/
		/*                             X = new premium*/
		/*   the new annualised premium, Y, should therefore be X*C so*/
		/*   Y = P*(A/C)*(D/B)*C*/
		/*     = P*A*(D/B)*/
		/*     = Z*(D/B)         where Z = the old annualised premium*/
		wsaaMiscellaneousInner.wsaaCalcNewfreq.set(wsaaAtBillfreq);
		wsaaMiscellaneousInner.wsaaCalcOldfreq.set(wsaaMiscellaneousInner.wsaaCompFreq);
		if (isEQ(wsaaMiscellaneousInner.wsaaInstpremFound, "Y")) {
			setPrecision(agcmbchIO.getAnnprem(), 3);
			agcmbchIO.setAnnprem(mult(agcmbchIO.getAnnprem(), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))).getbigdata());
		}
		/*   Write new AGCM record*/
		agcmbchIO.setTrdt(wsaaShortTransDate.toInt());
		agcmbchIO.setTrtm(wsaaAtTransactionTime.toInt());
		agcmbchIO.setUserT(wsaaAtUser.toInt());
		agcmbchIO.setTranno(chdrmjaIO.getTranno());
		agcmbchIO.setValidflag("1");
		agcmbchIO.setCurrto(99999999);
		agcmbchIO.setCurrfrom(payrIO.getBtdate());
		this.agcmpfListInst.add(agcmbchIO);
//		agcmbchIO.setFormat(formatsInner.agcmbchrec);
//		agcmbchIO.setFunction(varcom.writr);
//		SmartFileCode.execute(appVars, agcmbchIO);
//		if (isNE(agcmbchIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(agcmbchIO.getParams());
//			syserrrec.statuz.set(agcmbchIO.getStatuz());
//			fatalError600();
//		}
	}

//protected void retrieveNextRecord393()
//	{
//		agcmbchIO.setFunction(varcom.nextr);
//		SmartFileCode.execute(appVars, agcmbchIO);
//		if (isNE(agcmbchIO.getStatuz(),varcom.oK)
//		&& isNE(agcmbchIO.getStatuz(),varcom.endp)) {
//			syserrrec.params.set(agcmbchIO.getParams());
//			syserrrec.statuz.set(agcmbchIO.getStatuz());
//			fatalError600();
//		}
//		/*   Check if valid record retrieved*/
//		if (isEQ(agcmbchIO.getStatuz(),varcom.endp)) {
//			/*NEXT_SENTENCE*/
//		}
//		else {
//			if (isNE(covrmjaIO.getChdrcoy(),agcmbchIO.getChdrcoy())
//			|| isNE(covrmjaIO.getChdrnum(),agcmbchIO.getChdrnum())
//			|| isNE(covrmjaIO.getLife(),agcmbchIO.getLife())
//			|| isNE(covrmjaIO.getCoverage(),agcmbchIO.getCoverage())
//			|| isNE(covrmjaIO.getRider(),agcmbchIO.getRider())
//			|| isNE(covrmjaIO.getPlanSuffix(),agcmbchIO.getPlanSuffix())) {
//				agcmbchIO.setStatuz(varcom.endp);
//			}
//		}
//		if (isEQ(agcmbchIO.getTranno(),chdrmjaIO.getTranno())
//		&& isNE(agcmbchIO.getStatuz(),varcom.endp)) {
//			goTo(GotoLabel.retrieveNextRecord393);
//		}
//	}

//protected void processCompWop330a()
//	{
//		GotoLabel nextMethod = GotoLabel.DEFAULT;
//		while (true) {
//			try {
//				switch (nextMethod) {
//				case DEFAULT:
//					readTr517330a();
//					readT5687340a();
//					checkStatusAndAction350a();
//				case nextrCovr360a:
//					nextrCovr360a();
//				}
//				break;
//			}
//			catch (GOTOException e){
//				nextMethod = (GotoLabel) e.getNextMethod();
//			}
//		}
//	}
	protected void processCompWop330a() {
		if(readTr517330a()){
			return;
		}
		readT5687340a();
		checkStatusAndAction350a();
		// nextrCovr360a();
	}
protected boolean readTr517330a()
	{
		/* Read Table TR517 to check for WOP riders, If item not found*/
		/* don't process.*/
		wsaaTr517Key.set(covrmjaIO.getCrtable());
		a300ReadTr517();
		if (isEQ(tr517rec.tr517Rec,SPACES)) {
			//goTo(GotoLabel.nextrCovr360a);
			return true;
		}
		else {
			if (isEQ(tr517rec.zrwvflg04,"Y")) {
//				goTo(GotoLabel.nextrCovr360a);
				return true;
			}
		}
		return false;
	}

	/**
	* <pre>
	**** Read Table T5687 to obtain Single Premium Indicator and
	**** Frequency Alteration Basis.
	* </pre>
	*/
protected void readT5687340a()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCurrfrom());
		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(formatsInner.itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),covrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(),covrmjaIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f294);
			fatalError600();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void checkStatusAndAction350a()
	{
		/*    Check if component is to be updated.*/
		wsaaMiscellaneousInner.wsaaProcessComponent.set("N");
		if (isGT(covrmjaIO.getInstprem(),0)
		&& isNE(t5687rec.singlePremInd,"Y")) {
			checkComponent360a();
			if (wsaaMiscellaneousInner.processComponent.isTrue()) {
				updateComponent380a();
			}
		}
	}

//protected void nextrCovr360a()
//	{
//		/*    Get next coverage record*/
//		covrmjaIO.setFunction(varcom.nextr);
//		SmartFileCode.execute(appVars, covrmjaIO);
//		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
//		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
//			syserrrec.params.set(covrmjaIO.getParams());
//			syserrrec.statuz.set(covrmjaIO.getStatuz());
//			fatalError600();
//		}
//		if (isNE(covrmjaIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
//		|| isNE(covrmjaIO.getChdrnum(),chdrmjaIO.getChdrnum())) {
//			covrmjaIO.setStatuz(varcom.endp);
//		}
//		/*A-EXIT*/
//	}

protected void checkComponent360a()
	{
		check360a();
	}

protected void check360a()
	{
		/*  Check for match on Premium and Risk Statii for Coverage*/
		wsaaMiscellaneousInner.wsaaStatCount.set(1);
		if (isEQ(covrmjaIO.getRider(),"00")) {
			while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
			|| isEQ(t5679rec.covPremStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getPstatcode()))) {
				wsaaMiscellaneousInner.wsaaStatCount.add(1);
			}

			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.wsaaStatCount.set(1);
				while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
				|| (isEQ(t5679rec.covRiskStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getStatcode())))) {
					wsaaMiscellaneousInner.wsaaStatCount.add(1);
				}

			}
			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.processComponent.setTrue();
			}
		}
		else {
			/*   Check for match on Premium and Risk Statii for Rider*/
			wsaaMiscellaneousInner.wsaaStatCount.set(1);
			while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
			|| (isEQ(t5679rec.ridPremStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getPstatcode())))) {
				wsaaMiscellaneousInner.wsaaStatCount.add(1);
			}

			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.wsaaStatCount.set(1);
				while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
				|| (isEQ(t5679rec.ridRiskStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getStatcode())))) {
					wsaaMiscellaneousInner.wsaaStatCount.add(1);
				}

			}
			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.processComponent.setTrue();
			}
		}
	}

protected void updateComponent380a()
	{
		updateOriginalCovr382a();
		createNewCovr385a();
		updateStatus386a();
	}

protected void updateOriginalCovr382a()
	{
		Covrpf covrpf = new Covrpf(covrmjaIO);
		covrpf.setValidflag("2");
		covrpf.setCurrto(chdrmjaIO.getBtdate());
		this.covrpfListUpdt.add(covrpf);
//		covrmjaIO.setFunction(varcom.updat);
//		SmartFileCode.execute(appVars, covrmjaIO);
//		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(covrmjaIO.getParams());
//			syserrrec.statuz.set(covrmjaIO.getStatuz());
//			fatalError600();
//		}
	}

protected void createNewCovr385a()
	{
		wsaaMiscellaneousInner.wsaaCalcNewfreq.set(wsaaAtBillfreq);
		wsaaWopSi.set(ZERO);
		wsaaOldFreqFee.set(ZERO);
		wsaaNewFreqFee.set(ZERO);
		if (isNE(t5688rec.feemeth,SPACES)) {
			a400ReadT5567();
		}
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 10)); wsaaMiscellaneousInner.wsaaSub.add(1)){
			if (isEQ(wsaaCrtable[wsaaMiscellaneousInner.wsaaSub.toInt()], SPACES)) {
				wsaaMiscellaneousInner.wsaaSub.set(11);
			}
			else {
				a100CheckCodeWaiver();
			}
		}
		if (isGT(wsaaWopSi,0)) {
			if (isEQ(tr517rec.zrwvflg03,"Y")) {
				wsaaWopSi.add(wsaaNewFreqFee);
			}
			a200CalcWopPrem();
			covrmjaIO.setSumins(wsaaWopSi.getbigdata());
		}
	}

protected void updateStatus386a()
	{
		/*   Update the Risk Status Codes*/
		if (isEQ(covrmjaIO.getRider(),"00")) {
			if (isNE(t5679rec.setCovRiskStat,SPACES)) {
				covrmjaIO.setStatcode(t5679rec.setCovRiskStat.toString());
			}
		}
		else {
			if (isNE(t5679rec.setRidRiskStat,SPACES)) {
				covrmjaIO.setStatcode(t5679rec.setRidRiskStat.toString());
			}
		}
		/*  Update Premium Status Code*/
		if (isEQ(covrmjaIO.getRider(),"00")) {
			if (isNE(t5679rec.setCovPremStat,SPACES)) {
				covrmjaIO.setPstatcode(t5679rec.setCovPremStat.toString());
			}
		}
		else {
			if (isNE(t5679rec.setRidPremStat,SPACES)) {
				covrmjaIO.setPstatcode(t5679rec.setRidPremStat.toString());
			}
		}
		/*    Write the updated record*/
		covrmjaIO.setValidflag("1");
		covrmjaIO.setCurrfrom(chdrmjaIO.getBtdate());
		covrmjaIO.setCurrto(varcom.vrcmMaxDate.toInt());
		covrmjaIO.setTranno(chdrmjaIO.getTranno());
		covrmjaIO.setTransactionDate(wsaaShortTransDate.toInt());
		covrmjaIO.setTransactionTime(wsaaAtTransactionTime.toInt());
		covrmjaIO.setTermid(wsaaTermid.toString());
		covrmjaIO.setUser(wsaaUser.toInt());
//		covrmjaIO.setFormat(formatsInner.covrmjarec);
//		covrmjaIO.setFunction(varcom.writr);
//		SmartFileCode.execute(appVars, covrmjaIO);
//		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(covrmjaIO.getParams());
//			syserrrec.statuz.set(covrmjaIO.getStatuz());
//			fatalError600();
//		}
		this.covrpfListInst.add(covrmjaIO);
		
		/*   Update the Agent Commission Records   (AGCM)*/
		/*   Retrieve all AGCM records for each valid COVR record read*/
		if (agcmpfMap != null && agcmpfMap.containsKey(covrmjaIO.getChdrnum())) {
			for (Agcmpf c : agcmpfMap.get(covrmjaIO.getChdrnum())) {
				if (covrmjaIO.getLife().equals(c.getLife())
						&& covrmjaIO.getCoverage().equals(c.getCoverage())
						&& covrmjaIO.getRider().equals(c.getRider())
						&& covrmjaIO.getPlanSuffix() == c.getPlnsfx()) {
					agcmbchIO = c;
					processReadAgcmbch390();
				}
			}
		}
		
//		agcmbchIO.setParams(SPACES);
//		agcmbchIO.setChdrcoy(covrmjaIO.getChdrcoy());
//		agcmbchIO.setChdrnum(covrmjaIO.getChdrnum());
//		agcmbchIO.setLife(covrmjaIO.getLife());
//		agcmbchIO.setCoverage(covrmjaIO.getCoverage());
//		agcmbchIO.setRider(covrmjaIO.getRider());
//		agcmbchIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
//		agcmbchIO.setFormat(formatsInner.agcmbchrec);
//		agcmbchIO.setFunction(varcom.begn);

		//performance improvement --  atiwari23
//		agcmbchIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//		agcmbchIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
//
//		SmartFileCode.execute(appVars, agcmbchIO);
//		if (isNE(agcmbchIO.getStatuz(),varcom.oK)
//		&& isNE(agcmbchIO.getStatuz(),varcom.endp)) {
//			syserrrec.params.set(agcmbchIO.getParams());
//			syserrrec.statuz.set(agcmbchIO.getStatuz());
//			fatalError600();
//		}
//		/*   Check if valid AGCM record retrived*/
//		if (isEQ(agcmbchIO.getStatuz(),varcom.endp)) {
//			/*NEXT_SENTENCE*/
//		}
//		else {
//			if (isNE(agcmbchIO.getChdrcoy(),covrmjaIO.getChdrcoy())
//			|| isNE(agcmbchIO.getChdrnum(),covrmjaIO.getChdrnum())
//			|| isNE(agcmbchIO.getLife(),covrmjaIO.getLife())
//			|| isNE(agcmbchIO.getCoverage(),covrmjaIO.getCoverage())
//			|| isNE(agcmbchIO.getRider(),covrmjaIO.getRider())
//			|| isNE(agcmbchIO.getPlanSuffix(),covrmjaIO.getPlanSuffix())) {
//				agcmbchIO.setStatuz(varcom.endp);
//			}
//		}
		/*   Process AGCM records until end of file*/
//		while ( !(isEQ(agcmbchIO.getStatuz(),varcom.endp))) {
//			processReadAgcmbch390();
//		}
	}

protected void accumInstpremWrtChdrmja400()
	{
		accumInstPrem410();
	}

protected void accumInstPrem410()
	{
		/*    Read the contract definition details T5688 for the contract*/
		/*    type held on the contract header.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);

		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(),chdrmjaIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setStatuz(f290);
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
		if (isNE(t5688rec.feemeth,SPACES)) {
			calcFee1200();
		}
		chdrmjaIO.setSinstamt01(wsaaMiscellaneousInner.wsaaTotInstprem.getbigdata());
		setPrecision(chdrmjaIO.getSinstamt06(), 2);
		chdrmjaIO.setSinstamt06(add(add(add(add(chdrmjaIO.getSinstamt01(),chdrmjaIO.getSinstamt02()),chdrmjaIO.getSinstamt03()),chdrmjaIO.getSinstamt04()),chdrmjaIO.getSinstamt05()).getbigdata());
		this.chdrpfListInst.add(chdrmjaIO);
//		chdrmjaIO.setFunction(varcom.writr);
//		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
//		SmartFileCode.execute(appVars, chdrmjaIO);
//		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(chdrmjaIO.getParams());
//			syserrrec.statuz.set(chdrmjaIO.getStatuz());
//			fatalError600();
//		}
		/*  Write the PAYR record.*/
		payrIO.setSinstamt01(wsaaMiscellaneousInner.wsaaTotInstprem.getbigdata());
		setPrecision(payrIO.getSinstamt06(), 2);
		payrIO.setSinstamt06(add(add(add(add(payrIO.getSinstamt01(),payrIO.getSinstamt02()),chdrmjaIO.getSinstamt03()),chdrmjaIO.getSinstamt04()),chdrmjaIO.getSinstamt05()).getbigdata());
		this.payrpfListInst.add(payrIO);
//		payrIO.setFunction(varcom.writr);
//		payrIO.setFormat(formatsInner.payrrec);
//		SmartFileCode.execute(appVars, payrIO);
//		if (isNE(payrIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(payrIO.getParams());
//			syserrrec.statuz.set(payrIO.getStatuz());
//			fatalError600();
//		}
	}

protected void writeHistoryRlseLocks500()
	{
		writePtrnRecord510();
		releaseSoftlock550();
	}

protected void writePtrnRecord510()
	{
		Ptrnpf ptrnIO = new Ptrnpf();
//		ptrnIO.setDataArea(SPACES);
		ptrnIO.setTermid(wsaaTermid.toString());
		ptrnIO.setTrdt(wsaaShortTransDate.toInt());
		ptrnIO.setTrtm(wsaaAtTransactionTime.toInt());
		ptrnIO.setUserT(wsaaUser.toInt());
		ptrnIO.setBatcpfx(batcdorrec.prefix.toString());
		ptrnIO.setBatccoy(batcdorrec.company.toString());
		ptrnIO.setBatcbrn(batcdorrec.branch.toString());
		ptrnIO.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnIO.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnIO.setBatctrcde(batcdorrec.trcde.toString());
		ptrnIO.setBatcbatch(batcdorrec.batch.toString());
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		/* MOVE WSAA-AT-TODAY          TO PTRN-PTRNEFF.                 */
		/* MOVE WSAA-AT-TODAY          TO PTRN-DATESUB.                 */
		ptrnIO.setPtrneff(bsscIO.getEffectiveDate().toInt());
		ptrnIO.setDatesub(bsscIO.getEffectiveDate().toInt());
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy().toString());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setChdrpfx(chdrmjaIO.getChdrpfx());
		ptrnIO.setValidflag("1"); // ILIFE-5114 
		this.ptrnpfList.add(ptrnIO);
//		ptrnIO.setFormat(formatsInner.ptrnrec);
//		ptrnIO.setFunction(varcom.writr);
//		SmartFileCode.execute(appVars, ptrnIO);
//		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(ptrnIO.getParams());
//			syserrrec.statuz.set(ptrnIO.getStatuz());
//			fatalError600();
//		}
	}

protected void releaseSoftlock550()
	{
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(chdrmjaIO.getChdrcoy());
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void calcFee1200()
	{
			readSubroutineTable1210();
		}

protected void readSubroutineTable1210()
	{
		/*    Reference T5674 to obtain the subroutine required to work*/
		/*    out the Fee amount by the correct method.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5674);
		itemIO.setItemitem(t5688rec.feemeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(f151);
			return ;
		}
		t5674rec.t5674Rec.set(itemIO.getGenarea());
		/* Check subroutine NOT = SPACES before attempting call.*/
		if (isEQ(t5674rec.commsubr,SPACES)) {
			return ;
		}
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(chdrmjaIO.getCnttype());
		mgfeelrec.billfreq.set(chdrmjaIO.getBillfreq());
		mgfeelrec.effdate.set(chdrmjaIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrmjaIO.getCntcurr());
		mgfeelrec.company.set(chdrmjaIO.getChdrcoy());
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz,varcom.oK)
		&& isNE(mgfeelrec.statuz,varcom.endp)) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			syserrrec.statuz.set(mgfeelrec.statuz);
			fatalError600();
		}
		chdrmjaIO.setSinstamt02(mgfeelrec.mgfee.getbigdata());
		payrIO.setSinstamt02(mgfeelrec.mgfee.getbigdata());
	}

protected void a000Statistics()
	{
		a010Start();
	}

protected void a010Start()
	{
		lifsttrrec.batccoy.set(batcdorrec.company);
		lifsttrrec.batcbrn.set(batcdorrec.branch);
		lifsttrrec.batcactyr.set(batcdorrec.actyear);
		lifsttrrec.batcactmn.set(batcdorrec.actmonth);
		lifsttrrec.batctrcde.set(batcdorrec.trcde);
		lifsttrrec.batcbatch.set(batcdorrec.batch);
		lifsttrrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrmjaIO.getChdrnum());
		lifsttrrec.tranno.set(chdrmjaIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			fatalError600();
		}
	}

protected void a100CheckCodeWaiver()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
				case a110Check:
					a110Check();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a110Check()
	{
		wsaaWaiverFlag = "N";
		for (wsaaMiscellaneousInner.wsaaI.set(1); !(isGT(wsaaMiscellaneousInner.wsaaI, 50)
		|| isEQ(wsaaWaiverFlag, "Y")); wsaaMiscellaneousInner.wsaaI.add(1)){
			if (isNE(tr517rec.zrwvflg02,"Y")
			&& isNE(wsaaLife[wsaaMiscellaneousInner.wsaaSub.toInt()], covrmjaIO.getLife())) {
				/*NEXT_SENTENCE*/
			}
			else {
				if (isEQ(wsaaCrtable[wsaaMiscellaneousInner.wsaaSub.toInt()], tr517rec.ctable[wsaaMiscellaneousInner.wsaaI.toInt()])) {
					/*          IF TR517-ZRWVFLG-04    =  'Y'                          */
					/*             IF WSAA-RIDER(WSAA-SUB) = '00'                      */
					/*                ADD WSAA-PREM-AMT(WSAA-SUB) TO WSAA-WOP-SI       */
					/*             ELSE                                                */
					/*                SUBTRACT WSAA-PREM-AMT(WSAA-SUB) FROM WSAA-WOP-SI*/
					/*             END-IF                                              */
					/*          ELSE                                                   */
					wsaaWopSi.add(wsaaPremAmt[wsaaMiscellaneousInner.wsaaSub.toInt()]);
					/*          END-IF                                                 */
					wsaaWaiverFlag = "Y";
				}
			}
		}
		if (isEQ(wsaaWaiverFlag,"N")
		&& isNE(tr517rec.contitem,SPACES)) {
			wsaaTr517Key.set(tr517rec.contitem);
			a300ReadTr517();
			goTo(GotoLabel.a110Check);
		}
	}

protected void a200CalcWopPrem()
	{
		a210Lext();
		a220Calc();
		a230WithoutLoading();
		a240WithLoading();
	}

protected void a210Lext()
	{
		wsaaAgerateTot.set(ZERO);
		wsaaRatesPerMillieTot.set(ZERO);
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 8)); wsaaMiscellaneousInner.wsaaSub.add(1)){
			wsaaLextOppc[wsaaMiscellaneousInner.wsaaSub.toInt()].set(ZERO);
		}
		
		if (lextpfMap != null && lextpfMap.containsKey(covrmjaIO.getChdrnum())) {
			for (Lextpf c : lextpfMap.get(covrmjaIO.getChdrnum())) {
				if (covrmjaIO.getLife().equals(c.getLife()) 
						&& covrmjaIO.getCoverage().equals(c.getCoverage())
						&& covrmjaIO.getRider().equals(c.getRider())
						&& 0 == c.getSeqnbr()) {
					lextIO = c;
					if (isEQ(lextIO.getReasind(),"1")
					&& isLTE(bsscIO.getEffectiveDate(),lextIO.getExtCessDate())) {
						wsaaMiscellaneousInner.wsaaSub.add(1);
						wsaaLextOppc[wsaaMiscellaneousInner.wsaaSub.toInt()].set(lextIO.getOppc());
						wsaaRatesPerMillieTot.add(lextIO.getInsprm());
						wsaaAgerateTot.add(lextIO.getAgerate());
					}
//					lextIO.setFunction(varcom.nextr);
				}
			}
		}
		
//		lextIO.setParams(SPACES);
//		lextIO.setChdrcoy(covrmjaIO.getChdrcoy());
//		lextIO.setChdrnum(covrmjaIO.getChdrnum());
//		lextIO.setLife(covrmjaIO.getLife());
//		lextIO.setCoverage(covrmjaIO.getCoverage());
//		lextIO.setRider(covrmjaIO.getRider());
//		lextIO.setSeqnbr(ZERO);
//		lextIO.setFormat(formatsInner.lextrec);
//		lextIO.setFunction(varcom.begn);
//		while ( !(isEQ(lextIO.getStatuz(),varcom.endp))) {
//
//
//			//performance improvement --  atiwari23
//			lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//			lextIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
//			SmartFileCode.execute(appVars, lextIO);
//			if (isNE(lextIO.getStatuz(),varcom.oK)
//			&& isNE(lextIO.getStatuz(),varcom.endp)) {
//				syserrrec.params.set(lextIO.getParams());
//				fatalError600();
//			}
//			if (isNE(lextIO.getChdrcoy(),covrmjaIO.getChdrcoy())
//			|| isNE(lextIO.getChdrnum(),covrmjaIO.getChdrnum())
//			|| isNE(lextIO.getLife(),covrmjaIO.getLife())
//			|| isNE(lextIO.getCoverage(),covrmjaIO.getCoverage())
//			|| isNE(lextIO.getRider(),covrmjaIO.getRider())
//			|| isNE(lextIO.getStatuz(),varcom.endp)) {
//				lextIO.setStatuz(varcom.endp);
//			}
//			else {
//				if (isEQ(lextIO.getReasind(),"1")
//				&& isLTE(bsscIO.getEffectiveDate(),lextIO.getExtCessDate())) {
//					wsaaMiscellaneousInner.wsaaSub.add(1);
//					wsaaLextOppc[wsaaMiscellaneousInner.wsaaSub.toInt()].set(lextIO.getOppc());
//					wsaaRatesPerMillieTot.add(lextIO.getInsprm());
//					wsaaAgerateTot.add(lextIO.getAgerate());
//				}
//				lextIO.setFunction(varcom.nextr);
//			}
//		}

	}

protected void a220Calc()
	{
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5664);
		wsaaT5664Crtable.set(covrmjaIO.getCrtable());
		wsaaT5664Mortcls.set(covrmjaIO.getMortcls());
		wsaaT5664Sex.set(covrmjaIO.getSex());
		itdmIO.setItemitem(wsaaT5664Key);
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMITEM");


		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(wsaaT5664Key,itdmIO.getItemitem())
		|| isNE(covrmjaIO.getChdrcoy(),itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5664)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(wsaaT5664Key);
			syserrrec.statuz.set(f358);
			fatalError600();
		}
		else {
			t5664rec.t5664Rec.set(itdmIO.getGenarea());
		}
	}

protected void a230WithoutLoading()
	{
		/*    COMPUTE WSAA-RATE      = T5664-INSPRM(COVRMJA-ANB-AT-CCD)    */
		/*                           / T5664-PREM-UNIT.                    */
		if (isEQ(covrmjaIO.getAnbAtCcd(),0)){
			compute(wsaaRate, 2).set(div(t5664rec.insprem,t5664rec.premUnit));
			/**      WHEN 100                                           <V73L03>*/
			/**      COMPUTE WSAA-RATE = T5664-INSTPR / T5664-PREM-UNIT <V73L03>*/
			/**  Extend the age band to 110.                                    */
		}
		else if (isGTE(covrmjaIO.getAnbAtCcd(), 100)
		&& isLTE(covrmjaIO.getAnbAtCcd(), 110)){
			compute(wsaaRate, 2).set(div(t5664rec.instpr[sub(covrmjaIO.getAnbAtCcd(), 99).toInt()], t5664rec.premUnit));
		}
		else{
			compute(wsaaRate, 2).set(div(t5664rec.insprm[covrmjaIO.getAnbAtCcd()],t5664rec.premUnit));
		}
		compute(wsaaWopPrem, 3).setRounded((div((mult(wsaaWopSi,wsaaRate)),t5664rec.unit)));
		covrmjaIO.setZbinstprem(wsaaWopPrem.getbigdata());
	}

protected void a240WithLoading()
	{
		compute(wsaaAdjustedAge, 0).set(add(wsaaAgerateTot,covrmjaIO.getAnbAtCcd()));
		/*    COMPUTE WSAA-RATE      = T5664-INSPRM(WSAA-ADJUSTED-AGE)     */
		/*                           / T5664-PREM-UNIT.                    */
		if (isEQ(wsaaAdjustedAge,0)){
			compute(wsaaRate, 2).set(div(t5664rec.insprem,t5664rec.premUnit));
			/**      WHEN 100                                           <V73L03>*/
			/**       COMPUTE WSAA-RATE = T5664-INSTPR / T5664-PREM-UNIT<V73L03>*/
			/**  Extend the age band to 110.                                    */
		}
		else if (isGTE(wsaaAdjustedAge, 100)
		&& isLTE(wsaaAdjustedAge, 110)){
			compute(wsaaRate, 2).set(div(t5664rec.instpr[sub(wsaaAdjustedAge, 99).toInt()], t5664rec.premUnit));
		}
		else{
			compute(wsaaRate, 2).set(div(t5664rec.insprm[wsaaAdjustedAge.toInt()],t5664rec.premUnit));
		}
		compute(wsaaRate, 2).set(add(wsaaRatesPerMillieTot,wsaaRate));
		compute(wsaaWopPrem, 3).setRounded((div((mult(wsaaWopSi,wsaaRate)),t5664rec.unit)));
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 8)); wsaaMiscellaneousInner.wsaaSub.add(1)){
			if (isNE(wsaaLextOppc[wsaaMiscellaneousInner.wsaaSub.toInt()], 0)) {
				compute(wsaaWopPrem, 3).setRounded((div((mult(wsaaWopPrem, wsaaLextOppc[wsaaMiscellaneousInner.wsaaSub.toInt()])), 100)));
			}
		}
		covrmjaIO.setInstprem(wsaaWopPrem.getbigdata());
		setPrecision(covrmjaIO.getZlinstprem(), 2);
		covrmjaIO.setZlinstprem(sub(covrmjaIO.getInstprem(),covrmjaIO.getZbinstprem()).getbigdata());
		compute(wsaaMiscellaneousInner.wsaaTotInstprem, 2).set(add(wsaaMiscellaneousInner.wsaaTotInstprem, covrmjaIO.getInstprem()));
	}

protected void a300ReadTr517()
	{
		a310Read();
	}

protected void a310Read()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.tr517);
		itdmIO.setItemitem(wsaaTr517Key);
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);

		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),covrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemitem(),wsaaTr517Key)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr517)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			tr517rec.tr517Rec.set(SPACES);
		}
		else {
			tr517rec.tr517Rec.set(itdmIO.getGenarea());
		}
	}

protected void a400ReadT5567()
	{
		a410Read();
	}

protected void a410Read()
	{
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5567);
		wsaaT5567Cnttype.set(chdrmjaIO.getCnttype());
		wsaaT5567Cntcurr.set(chdrmjaIO.getCntcurr());
		itdmIO.setItemitem(wsaaT5567Key);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);

		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5567)
		|| isNE(itdmIO.getItemitem(),wsaaT5567Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(wsaaT5567Key);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h966);
			fatalError600();
		}
		else {
			t5567rec.t5567Rec.set(itdmIO.getGenarea());
		}
		wsaaNewFreqFee.set(ZERO);
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 10)); wsaaMiscellaneousInner.wsaaSub.add(1)){
			if (isEQ(t5567rec.billfreq[wsaaMiscellaneousInner.wsaaSub.toInt()], wsaaAtBillfreq)) {
				wsaaNewFreqFee.set(t5567rec.cntfee[wsaaMiscellaneousInner.wsaaSub.toInt()]);
				wsaaMiscellaneousInner.wsaaSub.set(11);
			}
			}
		}
/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
private static final class WsaaMiscellaneousInner {
		/* WSAA-MISCELLANEOUS */
	private ZonedDecimalData wsaaStatCount = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaCompFreq = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaCalcFreqs = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaCalcOldfreq = new ZonedDecimalData(2, 0).isAPartOf(wsaaCalcFreqs, 0).setUnsigned();
	private ZonedDecimalData wsaaCalcNewfreq = new ZonedDecimalData(2, 0).isAPartOf(wsaaCalcFreqs, 2).setUnsigned();

	private FixedLengthStringData wsaaProcessComponent = new FixedLengthStringData(1);
	private Validator processComponent = new Validator(wsaaProcessComponent, "Y");
	private Validator notProcessComponent = new Validator(wsaaProcessComponent, "N");
	private FixedLengthStringData wsaaInstpremFound = new FixedLengthStringData(1);
	private PackedDecimalData wsaaTotInstprem = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaNewFreqFound = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaOldFreqFound = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaNewLfact = new ZonedDecimalData(5, 4);
	private ZonedDecimalData wsaaOldLfact = new ZonedDecimalData(5, 4);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaI = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData bfrqrec = new FixedLengthStringData(10).init("BFRQREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData agcmbchrec = new FixedLengthStringData(10).init("AGCMBCHREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData lextrec = new FixedLengthStringData(10).init("LEXTREC");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
	private FixedLengthStringData t5541 = new FixedLengthStringData(6).init("T5541");
	private FixedLengthStringData t5679 = new FixedLengthStringData(6).init("T5679");
	private FixedLengthStringData t5687 = new FixedLengthStringData(6).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5674 = new FixedLengthStringData(5).init("T5674");
	private FixedLengthStringData tr517 = new FixedLengthStringData(5).init("TR517");
	private FixedLengthStringData t5567 = new FixedLengthStringData(5).init("T5567");
	private FixedLengthStringData t5664 = new FixedLengthStringData(5).init("T5664");
	}
}
