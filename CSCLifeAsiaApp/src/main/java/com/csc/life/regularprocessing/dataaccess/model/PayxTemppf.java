package com.csc.life.regularprocessing.dataaccess.model;

public class PayxTemppf {
	private long uniqueNumber;
	private String chdrcoy; 
	private String chdrnum;	
	private int payrseqno;
	private String billsupr;
	private int billspfrom;
	private int billspto;
	private String billchnl;
	private int billcd;
	private String membername;
	private B5349DTO b5349DTO;
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public int getPayrseqno() {
		return payrseqno;
	}
	public void setPayrseqno(int payrseqno) {
		this.payrseqno = payrseqno;
	}
	public String getBillsupr() {
		return billsupr;
	}
	public void setBillsupr(String billsupr) {
		this.billsupr = billsupr;
	}
	public int getBillspfrom() {
		return billspfrom;
	}
	public void setBillspfrom(int billspfrom) {
		this.billspfrom = billspfrom;
	}
	public int getBillspto() {
		return billspto;
	}
	public void setBillspto(int billspto) {
		this.billspto = billspto;
	}
	public String getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(String billchnl) {
		this.billchnl = billchnl;
	}
	public int getBillcd() {
		return billcd;
	}
	public void setBillcd(int billcd) {
		this.billcd = billcd;
	}
	public String getMembername() {
		return membername;
	}
	public void setMembername(String membername) {
		this.membername = membername;
	}
	public B5349DTO getB5349DTO() {
		return b5349DTO;
	}
	public void setB5349DTO(B5349DTO b5349dto) {
		b5349DTO = b5349dto;
	}

}
