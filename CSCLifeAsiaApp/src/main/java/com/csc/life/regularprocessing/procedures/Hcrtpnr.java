/*
 * File: Hcrtpnr.java
 * Date: 29 August 2009 22:52:02
 * Author: Quipoz Limited
 * 
 * Class transformed from HCRTPNR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.smart.dataaccess.BprdTableDAM;
import com.csc.smart.dataaccess.BsprTableDAM;
import com.csc.smart.dataaccess.BsscTableDAM;
import com.csc.smart.dataaccess.BupaTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*    Premium Notice Reminder Subroutine
*
*    This subroutine used to generate a LETC record to produce
*    the Premium Reminder Notices.
*
*****************************************************************
* </pre>
*/
public class Hcrtpnr extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaOtherKeys = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 0);
	private ZonedDecimalData wsaaInstfrom = new ZonedDecimalData(8, 0).isAPartOf(wsaaOtherKeys, 1).setUnsigned();
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
		/* ERRORS */
	private static final String g437 = "G437";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
		/* TABLES */
	private static final String tr384 = "TR384";
	private BprdTableDAM bprdIO = new BprdTableDAM();
	private BsprTableDAM bsprIO = new BsprTableDAM();
	private BsscTableDAM bsscIO = new BsscTableDAM();
	private BupaTableDAM bupaIO = new BupaTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Tr384rec tr384rec = new Tr384rec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Ovrduerec ovrduerec = new Ovrduerec();

	public Hcrtpnr() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		ovrduerec.ovrdueRec = convertAndSetParam(ovrduerec.ovrdueRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		writeLetc1000();
		/*EXIT*/
		exitProgram();
	}

protected void writeLetc1000()
	{
		letc1010();
	}

protected void letc1010()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ovrduerec.chdrcoy);
		/* MOVE T6634                  TO ITEM-ITEMTABL.                */
		itemIO.setItemtabl(tr384);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(ovrduerec.cnttype);
		stringVariable1.addExpression(ovrduerec.trancode);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(g437);
			xxxxFatalError();
		}
		/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.              */
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		letrqstrec.statuz.set(SPACES);
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.          */
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(ovrduerec.effdate);
		letrqstrec.clntcoy.set(ovrduerec.company);
		letrqstrec.clntnum.set(ovrduerec.cownnum);
		letrqstrec.rdocpfx.set(fsupfxcpy.chdr);
		letrqstrec.requestCompany.set(ovrduerec.chdrcoy);
		letrqstrec.chdrcoy.set(ovrduerec.chdrcoy);
		letrqstrec.rdoccoy.set(ovrduerec.chdrcoy);
		letrqstrec.rdocnum.set(ovrduerec.chdrnum);
		letrqstrec.chdrnum.set(ovrduerec.chdrnum);
		letrqstrec.tranno.set(ovrduerec.tranno);
		letrqstrec.branch.set(ovrduerec.batcbrn);
		wsaaLanguage.set(ovrduerec.language);
		wsaaInstfrom.set(ovrduerec.ptdate);
		letrqstrec.otherKeys.set(wsaaOtherKeys);
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(letrqstrec.statuz);
			syserrrec.params.set(letrqstrec.params);
			xxxxFatalError();
		}
	}

protected void xxxxFatalError()
	{
					xxxxFatalErrors();
					xxxxErrorBomb();
				}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorBomb()
	{
		ovrduerec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}
}
