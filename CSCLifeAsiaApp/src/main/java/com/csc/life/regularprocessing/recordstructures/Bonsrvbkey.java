package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:50
 * Description:
 * Copybook name: BONSRVBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Bonsrvbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData bonsrvbFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData bonsrvbKey = new FixedLengthStringData(64).isAPartOf(bonsrvbFileKey, 0, REDEFINE);
  	public FixedLengthStringData bonsrvbChdrcoy = new FixedLengthStringData(1).isAPartOf(bonsrvbKey, 0);
  	public FixedLengthStringData bonsrvbChdrnum = new FixedLengthStringData(8).isAPartOf(bonsrvbKey, 1);
  	public FixedLengthStringData bonsrvbLife = new FixedLengthStringData(2).isAPartOf(bonsrvbKey, 9);
  	public FixedLengthStringData bonsrvbCoverage = new FixedLengthStringData(2).isAPartOf(bonsrvbKey, 11);
  	public FixedLengthStringData bonsrvbRider = new FixedLengthStringData(2).isAPartOf(bonsrvbKey, 13);
  	public PackedDecimalData bonsrvbPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(bonsrvbKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(bonsrvbKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(bonsrvbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bonsrvbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}