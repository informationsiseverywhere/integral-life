/*
 * File: B5353.java
 * Date: 29 August 2009 21:07:43
 * Author: Quipoz Limited
 *
 * Class transformed from B5353.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.AcagpfDAO;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ClrrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Acagpf;
import com.csc.fsu.general.dataaccess.model.Clrrpf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.AgecalcPojo;
import com.csc.fsu.general.procedures.AgecalcUtils;
import com.csc.fsu.general.procedures.AlocnoUtil;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.ZrdecplcPojo;//ILB-441
import com.csc.fsu.general.procedures.ZrdecplcUtils;//ILB-441
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Rdockey;
import com.csc.fsu.general.recordstructures.WsspFsu;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.ZrapTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5644rec;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.annuities.recordstructures.Prasrec;
import com.csc.life.annuities.tablestructures.T6687rec;
import com.csc.life.cashdividends.dataaccess.dao.HdivpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdivpf;
import com.csc.life.contractservicing.dataaccess.dao.PrmhpfDAO;
import com.csc.life.contractservicing.dataaccess.dao.RskppfDAO;
import com.csc.life.contractservicing.dataaccess.dao.TaxdpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Coprpf;
import com.csc.life.contractservicing.dataaccess.model.Overpf;
import com.csc.life.contractservicing.dataaccess.model.Prmhpf;
import com.csc.life.contractservicing.dataaccess.model.Rskppf;
import com.csc.life.contractservicing.dataaccess.model.Taxdpf;
import com.csc.life.contractservicing.procedures.Zorcompy;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.linkage.procedures.LinkageInfoService;
import com.csc.life.newbusiness.dataaccess.dao.AgcmpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.NlgtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Agcmpf;
import com.csc.life.newbusiness.dataaccess.model.Nlgtpf;
import com.csc.life.newbusiness.dataaccess.model.Pcddpf;
import com.csc.life.newbusiness.procedures.NlgcalcPojo;
import com.csc.life.newbusiness.procedures.NlgcalcUtils;
import com.csc.life.productdefinition.dataaccess.dao.AnnypfDAO;
import com.csc.life.productdefinition.dataaccess.dao.Br613TempDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Annypf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.LifrtrnPojo;
import com.csc.life.productdefinition.procedures.LifrtrnUtils;
import com.csc.life.productdefinition.procedures.RlpdlonPojo;
import com.csc.life.productdefinition.procedures.RlpdlonUtils;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.tablestructures.T5567rec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.regularprocessing.dataaccess.dao.AglfpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.B5353DAO;
import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.RertpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.TrwppfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.ZctnpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.ZptnpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Aglfpf;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
import com.csc.life.regularprocessing.dataaccess.model.Linxpf;
import com.csc.life.regularprocessing.dataaccess.model.Rertpf;
import com.csc.life.regularprocessing.dataaccess.model.Trwppf;
import com.csc.life.regularprocessing.dataaccess.model.Zctnpf;
import com.csc.life.regularprocessing.dataaccess.model.Zptnpf;
import com.csc.life.regularprocessing.recordstructures.Ddbtallrec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.regularprocessing.recordstructures.Rnlallrec;
import com.csc.life.regularprocessing.tablestructures.T5534rec;
import com.csc.life.terminationclaims.recordstructures.Calprpmrec;
import com.csc.life.terminationclaims.tablestructures.Td5j1rec;
import com.csc.life.terminationclaims.tablestructures.Td5j2rec;
import com.csc.smart.procedures.SftlockUtil;
import com.csc.smart.recordstructures.Csdopt;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.SftlockRecBean;
import com.csc.smart.recordstructures.Varcom;//ILB-441
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.DateUtils;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobUtils;
import com.quipoz.COBOLFramework.util.ArraySearch;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.contractservicing.dataaccess.dao.CoprpfDAO;
import com.csc.life.contractservicing.dataaccess.dao.OverpfDAO;

/**
* <pre>
*
*
*REMARKS
*                       COLLECTIONS.
*                       -----------
*
* This program collects any monies available from suspense
* accounts  to  pay  for  outstanding  instalments (LINSPF).
* When  instalments  are paid the appropriate record has its
* payment  flag  set to 'P' (Paid).  All accounting relevant
* to suspense  and  payment accounts is also handled in this
* program.
*
*    In  addition,  the  payment  of  instalments  can  also
* generate generic processing such as commission payment and
* unit   allocation   which  is  driven  by table parameters.
*
*    Collection processes the outstanding instalment records
* (LINSPF) which were selected by B5352.
*
* Processing.
* ----------
* B5353 runs directly after B5352 which 'splits' the LINSPF
* according to the number of collections programs to run.  All
* references to the LINS are via LINXPF - a temporary file
* holding all the LINS records for this program to process.
*
* This program has been written to improve performance of the
* collections process.  All of the table items needed will be
* retrieved from working storage arrays initially loaded via
* the ITEMPF using the appropriate key.
*
*  1000-INITIALISE.
*  ---------------
*  -  Frequently-referenced tables are stored in working storage
*     arrays to minimize disk IO.
*
*  -  Issue an override to read the correct LINXPF for this run.
*     The CRTTMPF process has already created a file specific to
*     the run using the  LINXPF format and is identified  by
*     concatenating the following:-
*
*     'LINX'
*      BPRD-SYSTEM-PARAM04
*      BSSC-SCHEDULE-NUMBER
*
*      eg LINX2C0001,  for the first run
*         LINX2C0002,  for the second etc.
*
*     The number of threads would have been created by the
*     CRTTMPF process given the parameters of the process
*     definition of the CRTTMPF.
*     To get the correct member of the above physical for B5353
*     to read from, concatenate :-
*
*     'THREAD'
*     BSPR-PROCESS-OCC-NUM
*
*     eg. THREAD001,   for the first member (thread 1)
*         THREAD002    for the second etc.  (thread 2)
*
*  -  Initialise the static values of the LIFACMV copybook.
*
*  2000-READ
*  ---------
*
*  -  Read the LINX records sequentially keeping the count of
*     the number read and storing the present LINS details for
*     the WSYS- error record.
*
*  2500-EDIT
*  ---------
*
*     For Each LINX  record:
*
*  -  Softlock the CHDR and increment the control total if locked
*
*  -  Read CHDRLIF and validate the statii.  We only validate
*     the contract if the contract on the LINX changes.
*
*  -  Read the PAYR and CLRF details.
*
*  -  Calculate the TAX if necessary.
*
*  -  Check if there is enough in suspense to cover the net
*     premium.  If not, check the tolerance limit and amend the
*     premium if applicable.  If the amount in suspense (or
*     suspense + tolerance) is still not enough increment the
*     control totals and unlock the contract.
*
*  3000-UPDATE
*  -----------
*
*  -  To continue processing, retrieve all the LINS details for
*     updating and update the CHDR now as the commission
*     subroutines later read the most recent CHDR details.
*
*  -  Call the necessary collection subroutine from T6654 and
*     update the control total if one isn't found for the
*     collection method.
*
*  -  Call LIFRTRN to debit the suspense account.
*
*  -  Write the ACMVs for the INSTAMTS on the LINS.
*
*  -  Read all the coverages for the contract.  Load all the
*     coverages even if their instfrom is not valid or if the
*     statcodes on T5679 are not equal. (These coverages will
*     have no generic processing or ACMVs written.)
*
*     For valid coverages ONLY, post ACMVs for contracts with:-
*     -  Component level accounting
*     -  NO revenue Accounting
*     -  COVR instprems > 0
*     -  Perform generic processing if they have a subroutine
*        on T5671 eg unit-linked contracts.
*
*  -  All COVRS loaded will have  AGCM processing if they
*     are found on the AGCM file.
*
*  -  If Payrseqno on any coverage is not equal to that on the
*     LINS then end all processing on that LINS and update the
*     control total. (DO not update the PTRN, LINS or PAYR)
*
*  -  For commission, the AGCM is the driving file.  For each
*     AGCM look up the coverage key on the COVR array.  Post the
*     commission for those found and update the appropriate AGCM.
*     WSAA-GL-SUB ranges from 1 - 45 corresponding to the entries
*     on T5645.
*
*  -  Rewrite CHDR with the new TRANNO  and release the softlock.
*
* Control totals used in this program:
*
*    01  -  No. of LINS  records read
*    02  -  No LINS recs collected
*    03  -  Tot. LINS amts collected
*    04  -  No of LINS not collected
*    05  -  Tot. LINS amts not collected
*    06  -  No LINS susp < cbillamt
*    07  -  No. LINS fail tolerance frequency
*    08  -  No. LINS CHDR locked
*    09  -  No  LINS invalid CHDR
*    10  -  No  LINS payrseqno error
*    11  -  No  DD collect subroutines missing
*    12  -  No  LINS invalid coverages
*
*     (BATC processing is handled in MAINB)
*
*****************************************************************
* </pre>
*/
public class B5353  extends Mainb  {
	private static final Logger LOGGER = LoggerFactory.getLogger(B5353.class);
    public static final String ROUTINE = QPUtilities.getThisClass();
    public int numberOfParameters = 0;
        /* ERRORS */
    private static final String h791 = "H791";
    private static final String e351 = "E351";
    private static final String h979 = "H979";
    private static final String e308 = "E308";
    protected static final String e101 = "E101";
    private static final String g450 = "G450";
    private static final String e103 = "E103";
    private static final String E652 = "E652";
    private static final String H255 = "H255";
    private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5353");
    private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
    private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
    private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
    protected FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
    private PackedDecimalData wsaaCompayKept = new PackedDecimalData(13, 2);
    private ZonedDecimalData wsaaAgentKept = new ZonedDecimalData(8, 0).setPattern("ZZZZZZZZ");
    protected PackedDecimalData wsaaXPrmdepst = new PackedDecimalData(15, 2);
        /*  LINX parameters*/
    private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
    private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

    protected FixedLengthStringData wsaaLinxFn = new FixedLengthStringData(10);
    private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaLinxFn, 0, FILLER).init("LINX");
    private FixedLengthStringData wsaaLinxRunid = new FixedLengthStringData(2).isAPartOf(wsaaLinxFn, 4);
    private ZonedDecimalData wsaaLinxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaLinxFn, 6).setUnsigned();

    protected FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
    private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
    private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
        /*  Calculated amounts*/
    protected ZonedDecimalData wsaaSuspAvail = new ZonedDecimalData(17, 2);
    private PackedDecimalData wsaaSuspCbillDiff = new PackedDecimalData(17, 2);
    protected PackedDecimalData wsaaToleranceAllowed = new PackedDecimalData(17, 2);
    protected PackedDecimalData wsaaAmtPaid = new PackedDecimalData(17, 2);
    private PackedDecimalData wsaaImRatio = new PackedDecimalData(18, 9);
    private PackedDecimalData wsaaCntcurrReceived = new PackedDecimalData(17, 2);
    protected PackedDecimalData wsaaNetCbillamt = new PackedDecimalData(17, 2);
    protected PackedDecimalData wsaaApaRequired = new PackedDecimalData(17, 2);
    protected PackedDecimalData wsaaDivRequired = new PackedDecimalData(17, 2);
    private ZonedDecimalData wsaaBascpyDue = new ZonedDecimalData(17, 2);
    private ZonedDecimalData wsaaBascpyErn = new ZonedDecimalData(17, 2);
    private ZonedDecimalData wsaaBascpyPay = new ZonedDecimalData(17, 2);
    private ZonedDecimalData wsaaSrvcpyDue = new ZonedDecimalData(17, 2);
    private ZonedDecimalData wsaaSrvcpyErn = new ZonedDecimalData(17, 2);
    private ZonedDecimalData wsaaRnwcpyErn = new ZonedDecimalData(17, 2);
    private ZonedDecimalData wsaaRnwcpyDue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaRnwcpyPay = new ZonedDecimalData(17, 2);
    private ZonedDecimalData wsaaOvrdBascpyDue = new ZonedDecimalData(17, 2);
    private ZonedDecimalData wsaaOvrdBascpyErn = new ZonedDecimalData(17, 2);
    private ZonedDecimalData wsaaOvrdBascpyPay = new ZonedDecimalData(17, 2);
    private ZonedDecimalData wsaaCommethNo = new ZonedDecimalData(1, 0).init(0).setUnsigned();
    private ZonedDecimalData wsaaAcblSacscurbal = new ZonedDecimalData(17, 2);
    protected ZonedDecimalData wsaaWaiverAvail = new ZonedDecimalData(17, 2).init(ZERO);
    
        /* WSAA-TX-KEYS */
    protected FixedLengthStringData wsaaTxLife = new FixedLengthStringData(2);
    protected FixedLengthStringData wsaaTxCoverage = new FixedLengthStringData(2);
    protected FixedLengthStringData wsaaTxRider = new FixedLengthStringData(2);
    protected ZonedDecimalData wsaaTxPlansfx = new ZonedDecimalData(2, 0).setUnsigned();
    protected FixedLengthStringData wsaaTxCrtable = new FixedLengthStringData(4);

    private FixedLengthStringData wsaaToleranceChk = new FixedLengthStringData(1);
    protected Validator toleranceLimit1 = new Validator(wsaaToleranceChk, "1");
    protected Validator toleranceLimit2 = new Validator(wsaaToleranceChk, "2");

    private FixedLengthStringData wsaaAgtTerminateFlag = new FixedLengthStringData(1);
    protected Validator agtTerminated = new Validator(wsaaAgtTerminateFlag, "Y");
    private Validator agtNotTerminated = new Validator(wsaaAgtTerminateFlag, "N");

        /* WSAA-DATES */
    private ZonedDecimalData wsaaTerm = new ZonedDecimalData(11, 5);

    private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaTerm, 0, FILLER_REDEFINE);
    private ZonedDecimalData wsaaTermLeftInteger = new ZonedDecimalData(6, 0).isAPartOf(filler3, 0).setUnsigned();
    private ZonedDecimalData wsaaTermLeftRemain = new ZonedDecimalData(5, 0).isAPartOf(filler3, 6).setUnsigned();

        /* WSAA-BILLFREQ-VAR */
    private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);

    private FixedLengthStringData filler5 = new FixedLengthStringData(2).isAPartOf(wsaaBillfreq, 0, FILLER_REDEFINE);
    private ZonedDecimalData wsaaBillfreqNum = new ZonedDecimalData(2, 0).isAPartOf(filler5, 0).setUnsigned();
        /* WSAA-PLAN-SUFF */
    private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

    private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
    private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

    protected FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
    private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
    private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
    private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
    private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
    private FixedLengthStringData wsaaRldgPlanSuff = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);

    private FixedLengthStringData wsaaRldgagnt = new FixedLengthStringData(16);
    private FixedLengthStringData wsaaAgntChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgagnt, 0);
    private FixedLengthStringData wsaaAgntLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 8);
    private FixedLengthStringData wsaaAgntCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 10);
    private FixedLengthStringData wsaaAgntRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 12);
    private FixedLengthStringData wsaaAgntPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 14);

        /* Arrays to store regularly-referenced data*/
    private static final int wsaaCovrSize = 500;

        /* WSAA-COVR-ARRAY
        03  WSAA-COVR-REC      OCCURS 100                             */
    private FixedLengthStringData[] wsaaCovrRec = FLSInittedArray (500, 39);
    private FixedLengthStringData[] wsaaCovrKey = FLSDArrayPartOfArrayStructure(17, wsaaCovrRec, 0);
    private FixedLengthStringData[] wsaaCovrChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaCovrKey, 0);
    private FixedLengthStringData[] wsaaCovrLife = FLSDArrayPartOfArrayStructure(2, wsaaCovrKey, 8);
    private FixedLengthStringData[] wsaaCovrCovrg = FLSDArrayPartOfArrayStructure(2, wsaaCovrKey, 10);
    private FixedLengthStringData[] wsaaCovrRider = FLSDArrayPartOfArrayStructure(2, wsaaCovrKey, 12);
    private PackedDecimalData[] wsaaCovrPlanSuff = PDArrayPartOfArrayStructure(4, 0, wsaaCovrKey, 14);
    private FixedLengthStringData[] wsaaCovrData = FLSDArrayPartOfArrayStructure(22, wsaaCovrRec, 17);
    private FixedLengthStringData[] wsaaCovrCrtable = FLSDArrayPartOfArrayStructure(4, wsaaCovrData, 0);
    private FixedLengthStringData[] wsaaCovrValid = FLSDArrayPartOfArrayStructure(1, wsaaCovrData, 4);
    private ZonedDecimalData[] wsaaCovrProratePrem = ZDArrayPartOfArrayStructure(17, 2, wsaaCovrData, 5);//ILIFE-8509
        /*  Storage for T5644 table items.*/
    //private static final int wsaaT5644Size = 50
    //ILIFE-2628 fixed--Array size increased 
    private static final int wsaaT5644Size = 1000;

        /* WSAA-T5644-ARRAY
         03  WSAA-T5644-REC  OCCURS 30                                */
    private FixedLengthStringData[] wsaaT5644Rec = FLSInittedArray (1000, 11);
    private FixedLengthStringData[] wsaaT5644Key = FLSDArrayPartOfArrayStructure(4, wsaaT5644Rec, 0);
    private FixedLengthStringData[] wsaaT5644Commth = FLSDArrayPartOfArrayStructure(4, wsaaT5644Key, 0, HIVALUE);
    private FixedLengthStringData[] wsaaT5644Data = FLSDArrayPartOfArrayStructure(7, wsaaT5644Rec, 4);
    private FixedLengthStringData[] wsaaT5644Comsub = FLSDArrayPartOfArrayStructure(7, wsaaT5644Data, 0);
        /*  Storage for T5645 table items.*/
    //private static final int wsaaT5645Size = 45;
    //private static final int wsaaT5645Size = 500;//ILIFE-1985
    //ILIFE-2628 fixed--Array size increased 
    private static final int wsaaT5645Size = 1000;

        /* WSAA-T5645-ARRAY */
    //private FixedLengthStringData[] wsaaT5645Data = FLSInittedArray (45, 21);
    private FixedLengthStringData[] wsaaT5645Data = FLSInittedArray (1000, 21);//ILIFE-1985
    protected ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaT5645Data, 0);
    protected FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaT5645Data, 2);
    protected FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 16);
    protected FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 18);
    protected FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaT5645Data, 20);
        /*  Storage for T5667 table items.*/
    private ZonedDecimalData wsaaPrmtol = new ZonedDecimalData(4, 2);
    private ZonedDecimalData wsaaMaxAmount = new ZonedDecimalData(17, 2);
    private ZonedDecimalData wsaaPrmtoln = new ZonedDecimalData(4, 2);
    private ZonedDecimalData wsaaMaxamt = new ZonedDecimalData(17, 2);
    private FixedLengthStringData wsaaSfind = new FixedLengthStringData(1);
     //private static final int wsaaT5667Size = 10; 
    //ILIFE-2628 fixed--Array size increased 
    private static final int wsaaT5667Size = 1000;
    //private static final int wsaaT5671Size = 500;
    //ILIFE-2628 fixed--Array size increased 
    private static final int wsaaT5671Size = 1000;

        /* WSAA-T5671-ARRAY
         03  WSAA-T5671-REC OCCURS 50
         03  WSAA-T5671-REC OCCURS 250                        <V65L21>*/
    private FixedLengthStringData[] wsaaT5671Rec = FLSInittedArray (1000, 48);
    private FixedLengthStringData[] wsaaT5671Key = FLSDArrayPartOfArrayStructure(8, wsaaT5671Rec, 0);
    private FixedLengthStringData[] wsaaT5671Trcde = FLSDArrayPartOfArrayStructure(4, wsaaT5671Key, 0, HIVALUE);
    private FixedLengthStringData[] wsaaT5671Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5671Key, 4, HIVALUE);
    private FixedLengthStringData[] wsaaT5671Data = FLSDArrayPartOfArrayStructure(40, wsaaT5671Rec, 8);
    private FixedLengthStringData[] wsaaT5671Subprogs = FLSDArrayPartOfArrayStructure(40, wsaaT5671Data, 0);
    private FixedLengthStringData[][] wsaaT5671Subprog = FLSDArrayPartOfArrayStructure(4, 10, wsaaT5671Subprogs, 0);

    private FixedLengthStringData wsaaTrcdeCrtable = new FixedLengthStringData(8);
    private FixedLengthStringData wsaaAuthCode = new FixedLengthStringData(4).isAPartOf(wsaaTrcdeCrtable, 0);
    private FixedLengthStringData wsaaCovrlnbCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTrcdeCrtable, 4);
        /*  Storage for T5688 table items.*/
    //private static final int wsaaT5688Size = 60;
    //ILIFE-2628 fixed--Array size increased 
    private static final int wsaaT5688Size = 1000;

        /* WSAA-T5688-ARRAY */
    private FixedLengthStringData[] wsaaT5688Rec = FLSInittedArray (1000, 10);
    private FixedLengthStringData[] wsaaT5688Key = FLSDArrayPartOfArrayStructure(3, wsaaT5688Rec, 0);
    private FixedLengthStringData[] wsaaT5688Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5688Key, 0, SPACES);
    private FixedLengthStringData[] wsaaT5688Data = FLSDArrayPartOfArrayStructure(7, wsaaT5688Rec, 3);
    private PackedDecimalData[] wsaaT5688Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5688Data, 0);
    protected FixedLengthStringData[] wsaaT5688Comlvlacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 5);
    protected FixedLengthStringData[] wsaaT5688Revacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 6);
        /*  Storage for T6654 table items.*/
     // private static final int wsaaT6654Size = 500;
    //ILIFE-2628 fixed--Array size increased 
    private static final int wsaaT6654Size = 1000;

    private FixedLengthStringData wsaaT6654Key2 = new FixedLengthStringData(4);
    private FixedLengthStringData wsaaBillchnl2 = new FixedLengthStringData(1).isAPartOf(wsaaT6654Key2, 0);
    private FixedLengthStringData wsaaCnttype2 = new FixedLengthStringData(3).isAPartOf(wsaaT6654Key2, 1);

        /* WSAA-T6654-ARRAY
         03  WSAA-T6654-REC OCCURS 80                                 */
    private FixedLengthStringData[] wsaaT6654Rec = FLSInittedArray (1000, 12);
    private FixedLengthStringData[] wsaaT6654Key = FLSDArrayPartOfArrayStructure(4, wsaaT6654Rec, 0);
    private FixedLengthStringData[] wsaaT6654Billchnl = FLSDArrayPartOfArrayStructure(1, wsaaT6654Key, 0, HIVALUE);
    private FixedLengthStringData[] wsaaT6654Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT6654Key, 1, HIVALUE);
    private FixedLengthStringData[] wsaaT6654Data = FLSDArrayPartOfArrayStructure(8, wsaaT6654Rec, 4);
    private FixedLengthStringData[] wsaaT6654Collsub = FLSDArrayPartOfArrayStructure(8, wsaaT6654Data, 0);
        /*  Storage for T6687 table items.*/
     //private static final int wsaaT6687Size = 10;
    //ILIFE-2628 fixed--Array size increased 
    private static final int wsaaT6687Size = 1000;

        /* WSAA-T6687-ARRAY */
    private FixedLengthStringData[] wsaaT6687Rec = FLSInittedArray (1000, 16);
    private FixedLengthStringData[] wsaaT6687Key = FLSDArrayPartOfArrayStructure(8, wsaaT6687Rec, 0);
    private FixedLengthStringData[] wsaaT6687Taxrelmth = FLSDArrayPartOfArrayStructure(8, wsaaT6687Key, 0, HIVALUE);
    private FixedLengthStringData[] wsaaT6657Data = FLSDArrayPartOfArrayStructure(8, wsaaT6687Rec, 8);
    private FixedLengthStringData[] wsaaT6687Taxrelsubr = FLSDArrayPartOfArrayStructure(8, wsaaT6657Data, 0);

    protected FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(97);
    private FixedLengthStringData wsysLinskey = new FixedLengthStringData(21).isAPartOf(wsysSystemErrorParams, 0);
    private FixedLengthStringData wsysChdrcoy = new FixedLengthStringData(2).isAPartOf(wsysLinskey, 0);
    private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysLinskey, 2);
    protected FixedLengthStringData wsysCnttype = new FixedLengthStringData(3).isAPartOf(wsysLinskey, 10);
    private ZonedDecimalData wsysInstfrom = new ZonedDecimalData(8, 0).isAPartOf(wsysLinskey, 13).setUnsigned();
    protected FixedLengthStringData wsysSysparams = new FixedLengthStringData(76).isAPartOf(wsysSystemErrorParams, 21);

        /*01  WSAA-ITEM-T6634.                                     <PCPPRT>*/
    private FixedLengthStringData wsaaItemTr384 = new FixedLengthStringData(8);
    private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemTr384, 0);
    private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItemTr384, 3);
        /*  Storage for T5687 table items.                                 */
    //private static final int wsaaT5687Size = 500;
    //ILIFE-2628 fixed--Array size increased 
    private static final int wsaaT5687Size = 1000;

        /* WSAA-T5687-ARRAY */
    private FixedLengthStringData[] wsaaT5687Rec = FLSInittedArray (1000, 13);
    private FixedLengthStringData[] wsaaT5687Key = FLSDArrayPartOfArrayStructure(4, wsaaT5687Rec, 0);
    private FixedLengthStringData[] wsaaT5687Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5687Key, 0, SPACES);
    private FixedLengthStringData[] wsaaT5687Data = FLSDArrayPartOfArrayStructure(9, wsaaT5687Rec, 4);
    private PackedDecimalData[] wsaaT5687Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5687Data, 0);
    private FixedLengthStringData[] wsaaT5687Bbmeth = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 5);
        /*  Storage for T5534 table items.                                 */
    //private static final int wsaaT5534Size = 100;
    //ILIFE-2628 fixed--Array size increased 
    private static final int wsaaT5534Size = 1000;

        /* WSAA-T5534-ARRAY */
    private FixedLengthStringData[] wsaaT5534Rec = FLSInittedArray (1000, 6);
    private FixedLengthStringData[] wsaaT5534Key = FLSDArrayPartOfArrayStructure(4, wsaaT5534Rec, 0);
    private FixedLengthStringData[] wsaaT5534Bbmeth = FLSDArrayPartOfArrayStructure(4, wsaaT5534Key, 0, SPACES);
    private FixedLengthStringData[] wsaaT5534Data = FLSDArrayPartOfArrayStructure(2, wsaaT5534Rec, 4);
    private FixedLengthStringData[] wsaaT5534UnitFreq = FLSDArrayPartOfArrayStructure(2, wsaaT5534Data, 0);
    private FixedLengthStringData wsaaBillfq = new FixedLengthStringData(2);
    private ZonedDecimalData wsaaBillfq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfq, 0, REDEFINE).setUnsigned();
    private static final int wsaaAgcmIxSize = 100;
    private ZonedDecimalData wsaaAgcmIx = new ZonedDecimalData(3, 0).init(0).setUnsigned();
    private ZonedDecimalData wsaaAgcmIa = new ZonedDecimalData(3, 0).init(0).setUnsigned();
    private static final int wsaaAgcmIySize = 500;
    private ZonedDecimalData wsaaAgcmIb = new ZonedDecimalData(3, 0).init(0).setUnsigned();

        /* WSAA-AGCM-SUMMARY */
    private FixedLengthStringData[] wsaaAgcmRec = FLSInittedArray (100, 8515);
    private FixedLengthStringData[] wsaaAgcmChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaAgcmRec, 0);
    private FixedLengthStringData[] wsaaAgcmChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaAgcmRec, 1);
    private FixedLengthStringData[] wsaaAgcmLife = FLSDArrayPartOfArrayStructure(2, wsaaAgcmRec, 9);
    private FixedLengthStringData[] wsaaAgcmCoverage = FLSDArrayPartOfArrayStructure(2, wsaaAgcmRec, 11);
    private FixedLengthStringData[] wsaaAgcmRider = FLSDArrayPartOfArrayStructure(2, wsaaAgcmRec, 13);
    private FixedLengthStringData[][] wsaaAgcmPremRec = FLSDArrayPartOfArrayStructure(500, 17, wsaaAgcmRec, 15);
    private ZonedDecimalData[][] wsaaAgcmSeqno = ZDArrayPartOfArrayStructure(3, 0, wsaaAgcmPremRec, 0, UNSIGNED_TRUE);
    private PackedDecimalData[][] wsaaAgcmEfdate = PDArrayPartOfArrayStructure(8, 0, wsaaAgcmPremRec, 3, UNSIGNED_TRUE);
    private PackedDecimalData[][] wsaaAgcmAnnprem = PDArrayPartOfArrayStructure(17, 2, wsaaAgcmPremRec, 8);
    private PackedDecimalData wsaaAgcmPremium = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaAgcmFound = new FixedLengthStringData(1);
	private Validator agcmFound = new Validator(wsaaAgcmFound, "Y");
    private FixedLengthStringData wsaaPremType = new FixedLengthStringData(1);
    private Validator singPrem = new Validator(wsaaPremType, "S");
    private Validator firstPrem = new Validator(wsaaPremType, "I");
    private Validator renPrem = new Validator(wsaaPremType, "R");
        /* Flag, indicate the previous LINS is successful collected
         or not.                                                         */
    protected String wsaaPrevLins = "";
        /* WSAA-LAST-COVER */
    private FixedLengthStringData wsaaLastChdrnum = new FixedLengthStringData(8);
    private FixedLengthStringData wsaaLastLife = new FixedLengthStringData(2);
    private FixedLengthStringData wsaaLastCoverage = new FixedLengthStringData(2);
    private FixedLengthStringData wsaaLastRider = new FixedLengthStringData(2);
    private PackedDecimalData wsaaLastPlanSuff = new PackedDecimalData(4, 0);
    private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
    private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
    private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
    private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
    private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
    private IntegerData wsaaCovrIx = new IntegerData();
    private IntegerData wsaaT5644Ix = new IntegerData();
    private IntegerData wsaaT5671Ix = new IntegerData();
    private IntegerData wsaaSubprogIx = new IntegerData();
   
    private IntegerData wsaaT6654Ix = new IntegerData();
    private IntegerData wsaaT6687Ix = new IntegerData();
    private IntegerData wsaaT5687Ix = new IntegerData();
    private IntegerData wsaaT5534Ix = new IntegerData();
    private ZrapTableDAM zrapIO = new ZrapTableDAM();
    private Rdockey wsaaRdockey = new Rdockey();
    private Comlinkrec comlinkrec = new Comlinkrec();
    private Letrqstrec letrqstrec = new Letrqstrec();
    private Csdopt csdopt = new Csdopt();
    private Ddbtallrec ddbtallrec = new Ddbtallrec();
    private Rnlallrec rnlallrec = new Rnlallrec();
    protected Lifacmvrec lifacmvrec = new Lifacmvrec();
    private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
    protected Prasrec prasrec = new Prasrec();
    private P6671par p6671par = new P6671par();
    private Alocnorec alocnorec = new Alocnorec();
    protected Zorlnkrec zorlnkrec = new Zorlnkrec();
    private Txcalcrec txcalcrec = new Txcalcrec();
    private Zrdecplrec zrdecplrec = new Zrdecplrec();
    protected T3695rec t3695rec = new T3695rec();
    private T5644rec t5644rec = new T5644rec();
    private T5645rec t5645rec = new T5645rec();
    private T5671rec t5671rec = new T5671rec();
    private T5667rec t5667rec = new T5667rec();
    private T5679rec t5679rec = new T5679rec();
    private T5688rec t5688rec = new T5688rec();
    private Tr384rec tr384rec = new Tr384rec();
    private T6654rec t6654rec = new T6654rec();
    private T6687rec t6687rec = new T6687rec();
    private T5687rec t5687rec = new T5687rec();
    private T5534rec t5534rec = new T5534rec();
    protected Th605rec th605rec = new Th605rec();
    private Tr52drec tr52drec = new Tr52drec();
    private Tr52erec tr52erec = new Tr52erec();
    private WsaaT5667ArrayInner wsaaT5667ArrayInner = new WsaaT5667ArrayInner();
    private BigDecimal wsaaTotTax = BigDecimal.ZERO;
    protected String wsaaPayrnum;
    protected String wsaaPayrcoy;
    protected int wsaaGlSub;
    private int wsaaT5679Sub;
    private boolean wsaaDateFound;
    protected boolean wsaaValidContract;
    private boolean payrseqnoError;
    protected int wsaaT5688Ix=0;
    protected String wsaaPrevChdrnum;
    private Map<String, List<Itempf>> t5645Map = null;
    private Map<String, List<Itempf>> t3695Map = null;
    private Map<String, String> prorateMap = new HashMap<String, String>();
    //List<String> proratevalues = null;
    private List<Itempf> t6687List = null;
    private List<Itempf> t5688List = null;
    private Map<String, List<Itempf>> t5679Map = null;
    private List<Itempf> t5644List = null;
    private List<Itempf> t6654List = null;
    private Map<String, List<Itempf>> t5667Map = null;
    private Map<String, List<Itempf>> t5671Map = null;
    private Map<String, List<Itempf>> th605Map = null;
    private List<Itempf> t5687List = null;
    private List<Itempf> t5534List = null;
    private Map<String, Descpf> t1688Map = null;
    private Map<String, List<Itempf>> tr384Map = null;
    private Map<String, List<Itempf>> tr52dMap = null;
    private Map<String, List<Itempf>> tr52eMap = null;
    private Map<String, List<Itempf>> tr2enMap = null; //PINNACLE-2005
    protected ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
    private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
    private TaxdpfDAO taxdpfDAO = getApplicationContext().getBean("taxdpfDAO", TaxdpfDAO.class);
    protected  AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
    private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
    private AgcmpfDAO agcmpfDAO = getApplicationContext().getBean("agcmDAO", AgcmpfDAO.class);
    private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
    private AcagpfDAO acagpfDAO = getApplicationContext().getBean("acagpfDAO", AcagpfDAO.class);
    private LinspfDAO linspfDAO =  getApplicationContext().getBean("linspfDAO", LinspfDAO.class);
    private PayrpfDAO payrpfDAO =  getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
    private HdivpfDAO hdivpfDAO =  getApplicationContext().getBean("hdivpfDAO", HdivpfDAO.class);
    private ZptnpfDAO zptnpfDAO =  getApplicationContext().getBean("zptnpfDAO", ZptnpfDAO.class);
    private ZctnpfDAO zctnpfDAO =  getApplicationContext().getBean("zctnpfDAO", ZctnpfDAO.class);
    private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
    private TrwppfDAO trwppfDAO =  getApplicationContext().getBean("trwppfDAO", TrwppfDAO.class);
    private ClrrpfDAO clrrpfDAO =  getApplicationContext().getBean("clrrpfDAO", ClrrpfDAO.class);
    
	//ILIFE-3997-STARTS
//	private Nlgcalcrec nlgcalcrec = new Nlgcalcrec();
	//private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrDAO", ChdrpfDAOImpl.class);
	private List<Nlgtpf> nlgtpfList = new ArrayList<Nlgtpf>();
	private NlgtpfDAO nlgtpfDAO = getApplicationContext().getBean("nlgtpfDAO", NlgtpfDAO.class);
		//ILIFE-3997-ENDS

    private Map<String, List<Taxdpf>> taxdMap = new HashMap<String, List<Taxdpf>>();
    protected Map<String, List<Acblpf>> acblMap = new HashMap<String,  List<Acblpf>>();
    protected Map<String, List<Acblpf>> acblGLMap = new HashMap<String,  List<Acblpf>>();
    private Map<String, List<Covrpf>> covrMap = new HashMap<String,  List<Covrpf>>();
    private Map<String, List<Agcmpf>> agcmMap = new HashMap<String,  List<Agcmpf>>();
    protected Map<String, List<Clrrpf>> clrrMap = new HashMap<String,  List<Clrrpf>>();
    
    private Iterator<Linxpf> iteratorList; 
    protected Descpf descpfT1688;
    protected Linxpf linxpfResult; 
    protected int intBatchID = 0;
    protected int intBatchExtractSize;
    private int contot01 = 0;
    protected int contot02 = 0;
    protected ZonedDecimalData temptot = new ZonedDecimalData(2, 0).init(3).setUnsigned(); 
    protected ZonedDecimalData contot03 = new ZonedDecimalData(2, 0).init(3).setUnsigned();
    protected ZonedDecimalData contot05 = new ZonedDecimalData(2, 0).init(3).setUnsigned();
    protected int contot04 = 0;
    protected int contot06 = 0;
    private int contot07 = 0;//
    protected int contot08 = 0;//
    protected int contot09 = 0;
    private int contot10 = 0;
    private int contot11 = 0;
    private int contot12 = 0;
    protected int contot13 = 0;
    
    protected List<Chdrpf> chdrUpdateList;
    private List<Acagpf> acagInsertList;
    protected List<Linspf> linsUpdateList;
    protected List<Payrpf> payrUpdateList;
    private List<Hdivpf> hdivInsertList;
    private List<Taxdpf> taxdUpdateList;
    private List<Zptnpf> zptnInsertList;
    private List<Zctnpf> zctnInsertList;   
    private List<Ptrnpf> ptrnpfInsertList;
    private List<Trwppf> trwppfInsertList;
    private List<Agcmpf> agcmpfUpdateList = new ArrayList<Agcmpf>(); 
    private List<Agcmpf> agcmpfInsertList = new ArrayList<Agcmpf>();
    private List<Agcmpf> agcmpfInvUpdateList = new ArrayList<Agcmpf>();
    //Clrrpf clrrpf;
    protected Clrrpf clrrpf;
    protected Map<String, Integer> trannoMap;
    protected PackedDecimalData wsaaWaiveAmtPaid = new PackedDecimalData(17, 2).init(ZERO);
    
    //private Chdrpf readChdrpf = new Chdrpf();	//ILIFE-7075
    // changed by yy
    private NlgcalcUtils nlgcalcUtils = getApplicationContext().getBean("nlgcalcUtils", NlgcalcUtils.class);
    private SftlockUtil sftlockUtil = new SftlockUtil();
    private boolean noRecordFound;
    private B5353DAO b5353DAO =  getApplicationContext().getBean("b5353DAO", B5353DAO.class);
      //ILB-441 start
    private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
    private ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
	protected Map<String, List<Itempf>> td5j2ListMap;
	protected Map<String, List<Itempf>> td5j1ListMap;
	private Td5j2rec td5j2rec = new Td5j2rec();
	private Td5j1rec td5j1rec = new Td5j1rec();
    private static final String td5j2 = "TD5J2";
	private static final String ta85 = "TA85";
	private Calprpmrec calprpmrec = new Calprpmrec();
	private boolean isFoundPro = false;
	protected boolean isShortTerm = false;
    private List<Ptrnpf> ptrnrecords = null;
    protected Ptrnpf ptrnpfReinstate = new Ptrnpf();
    protected boolean reinstflag = false;
    private boolean isnullflag = false;// ILIFE-7911
    //ILB-441 end
    //ILB-448 start
	private LifrtrnUtils lifrtrnUtils = getApplicationContext().getBean("lifrtrnUtils", LifrtrnUtils.class);
	private LifrtrnPojo lifrtrnPojo = new LifrtrnPojo();
	private RlpdlonUtils rlpdlonUtils = getApplicationContext().getBean("rlpdlonUtils", RlpdlonUtils.class);
	private RlpdlonPojo rlpdlonPojo = new RlpdlonPojo();
	//ILB-448 end

	private Map<Integer, List<Incrpf>> incrDatedMap;
	private IncrpfDAO incrpfDAO =  getApplicationContext().getBean("incrpfDAO", IncrpfDAO.class);
	private Incrpf currIncrPro = null;
	private boolean incrFoundPro = false;

	 /*ILIFE-7965 : Starts*/
    private PackedDecimalData wsaaCompErnInitCommGst = new PackedDecimalData(17, 2);
    private PackedDecimalData wsaaCompErnRnwlCommGst = new PackedDecimalData(17, 2);	
	private boolean gstOnCommFlag = false;
	/*ILIFE-7965 : Ends*/
	private boolean riskPremflag = false; //ILIFE-7845 
	private static final String  RISKPREM_FEATURE_ID="NBPRP094";//ILIFE-7845
	private List<Covrpf> covrpfList;
	private Premiumrec premiumrec = new Premiumrec();
	private Map<String, List<Rskppf>> covrRiskPremMap = new HashMap<String,  List<Rskppf>>();
	private Map<String, List<Rskppf>> riskPremMap = new HashMap<String,  List<Rskppf>>();
	private Map<String, List<Rskppf>> rskpMap = new HashMap<String,  List<Rskppf>>();
	private RskppfDAO rskppfDAO =  getApplicationContext().getBean("rskppfDAO", RskppfDAO.class);
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	private List<Rskppf> updateRiskPremList = new ArrayList<>();
	private List<Rskppf> insertRiskPremList = new ArrayList<>();
	private Map<String, List<Itempf>> t5567Map;
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemitem, 0);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaItemitem, 3);
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private T5567rec t5567rec = new T5567rec();
	private ZonedDecimalData wsaapolFee = new ZonedDecimalData(8, 2);
	Chdrpf chdrpf = null;
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private boolean prmhldtrad = false;//ILIFE-8509
	private Map<String, Itempf> ta524Map;
	private Prmhpf prmhpf;
	private PrmhpfDAO prmhpfDAO = getApplicationContext().getBean("prmhpfDAO", PrmhpfDAO.class);
	//ILIFE-8919 Starts
	private static final String SYS_COY = "0";
	//ILIFE-8919 End
	private boolean isDMSFlgOn = false;	//ILIFE-8880
	private static final String DMS_FEATURE = "SAFCF001";	//ILIFE-8880
	private List<Acblpf> acblpfInsertList = new ArrayList<Acblpf>();
	private List<Acblpf> acblpfUpdateList = new ArrayList<Acblpf>();
	private List<Acblpf> acblpfInsertAll = new ArrayList<Acblpf>();
	private List<Acblpf> acblpfLifrtrnList = new ArrayList<Acblpf>();
	private Boolean isBatchSkipFeatureEnabled;// IBPTE-74
	private Boolean japanBilling = false;
	private boolean initFlag = false;
	private static final String BTPRO027 = "BTPRO027";
	private boolean BTPRO028Permission  = false;
	 private OverpfDAO overpfDAO =  getApplicationContext().getBean("overpfDAO", OverpfDAO.class);
	private static final String BTPRO028 = "BTPRO028";
	private AglfpfDAO aglfpfDAO = getApplicationContext().getBean("aglfpfDAO",AglfpfDAO.class);
	private FixedLengthStringData wsaaReporttoAgnt = new FixedLengthStringData(8);
	protected ZonedDecimalData wsaaStampDutyAcc = new ZonedDecimalData(17, 2);
	private boolean stampDutyflag = false;
	private Map<String, List<Rertpf>> rertpfMap = new HashMap<String,  List<Rertpf>>();
	private Map<String, List<Incrpf>> incrpfMap = new HashMap<String, List<Incrpf>>();
	private RertpfDAO rertpfDAO =  getApplicationContext().getBean("rertpfDAO", RertpfDAO.class);
	private Rertpf rertpf = null;
	protected List<Covrpf> covrUpdateList;
	private List<Covrpf> covrInsertList = new ArrayList<Covrpf>();
	protected List<Rertpf> rertUpdateList = new ArrayList<Rertpf>(); 
	protected ZonedDecimalData wsaaOutstandAmt = new ZonedDecimalData(17, 2).init(ZERO);
	private boolean outstamtIncr  = false;
	private Chdrpf chdrpfObj = null;
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO",ClntpfDAO.class); //PINNACLE-2005
	private boolean stampDutyFlagcomp;
	private SftlockRecBean sftlockRecBean = new SftlockRecBean();
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCedagent = new FixedLengthStringData(8);
	private Map<String, List<Pcddpf>> pcddpfMap = new HashMap<String,  List<Pcddpf>>();
	private FixedLengthStringData wsaaAgntFound = new FixedLengthStringData(1);
	private Validator agentFound = new Validator(wsaaAgntFound, "Y");
	protected Br613TempDAO br613TempDAO = getApplicationContext().getBean("br613TempDAO", Br613TempDAO.class);
	private PackedDecimalData wsaaAnnprem = new PackedDecimalData(17, 2);
	private T5675rec t5675rec = new T5675rec();
	private Freqcpy freqcpy = new Freqcpy();
	private Payrpf payrpf;
	
	private boolean billChgFlag;
	private PackedDecimalData wsaaLastRerateDate = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaRerateDate = new ZonedDecimalData(8, 0).init(0);
	private PackedDecimalData prorateprem  = new PackedDecimalData(17, 2);
	private PackedDecimalData prorateSD  = new PackedDecimalData(17, 2);
	private PackedDecimalData prorateTotalPrem  = new PackedDecimalData(17, 2);
	private AnnypfDAO annypfDAO = getApplicationContext().getBean("annypfDAO", AnnypfDAO.class);
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0);
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO", RcvdpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private static final String BTPRO036 = "BTPRO036";
	private boolean BTPRO036Permission  = false;
	private int deffDays = 0;
	private boolean isFirstCovr;
	private boolean outerrecordfound;
	private boolean innerrecordfound;
	private boolean BTPRO033Permission;
	private List<Coprpf> coprList = new ArrayList<Coprpf>();
	private List<Coprpf> updateCoprList = new ArrayList<Coprpf>();
	private CoprpfDAO coprpfDAO = getApplicationContext().getBean("coprpfDAO", CoprpfDAO.class);
	private ZonedDecimalData payrOutstamt = new ZonedDecimalData(17, 2).init(ZERO);
	private ZonedDecimalData chdrOutstamt = new ZonedDecimalData(17, 2).init(ZERO);
	
/**
 * Contains all possible labels used by goTo action.
 */
    protected enum GotoLabel implements GOTOInterface {
        DEFAULT,
        failTolerance2855,
        exit2899,
        exit2999a,
        exit2999,
        advancePremDep3320,
        exit3399,
        next3515,
        exit3519,
        next3535,
        exit3539,
        a399Exit,
        b220Call,
        b280Next,
        b290Exit,
        b320Loop,
        b380Next,
        b390Exit,
        b610NextRec,
        b610Exit,
        b710NextRec,
        b710Exit
    }

    public B5353() {
        super();
    }

protected FixedLengthStringData getWsaaProg() {
    return wsaaProg;
    }

protected PackedDecimalData getWsaaCommitCnt() {
    return wsaaCommitCnt;
    }

protected PackedDecimalData getWsaaCycleCnt() {
    return wsaaCycleCnt;
    }

protected FixedLengthStringData getWsspEdterror() {
    return wsspEdterror;
    }

protected FixedLengthStringData getLsaaStatuz() {
    return lsaaStatuz;
    }

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
    this.lsaaStatuz = lsaaStatuz;
    }

protected FixedLengthStringData getLsaaBsscrec() {
    return lsaaBsscrec;
    }

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
    this.lsaaBsscrec = lsaaBsscrec;
    }

protected FixedLengthStringData getLsaaBsprrec() {
    return lsaaBsprrec;
    }

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
    this.lsaaBsprrec = lsaaBsprrec;
    }

protected FixedLengthStringData getLsaaBprdrec() {
    return lsaaBprdrec;
    }

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
    this.lsaaBprdrec = lsaaBprdrec;
    }

protected FixedLengthStringData getLsaaBuparec() {
    return lsaaBuparec;
    }

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
    this.lsaaBuparec = lsaaBuparec;
    }


    /**
    * The mainline method is the default entry point to the class
    */
public void mainline(Object... parmArray)
    {
        lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
        lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
        lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
        lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
        lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
        try {
            super.mainline();
        }
        catch (COBOLExitProgramException e) {
        // Expected exception for control flow purposes
        }
    }

protected void restart0900()
    {
        /*RESTART*/
        /** Place any additional restart processing in here.*/
        /*EXIT*/
    }

protected void initialise1000()
    {
	LOGGER.info("initialise1000 start");
        initialise1010();
    	//IBPTE-74 starts
		isBatchSkipFeatureEnabled = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "BTPRO025", appVars,
				"IT");
    	if(isBatchSkipFeatureEnabled) {
    		setSkippedEntities(getAlreadySkippedEntities("CH"));
    	}
    	//IBPTE-74 ends
    	LOGGER.info("initialise1000 end");
    }

protected void initialise1010()
 {
        /* Move the parameter screen data from P6671 into its original */
        /* field map. */
	    BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
	    stampDutyflag = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "NBPROP01", appVars, "IT");
        p6671par.parmRecord.set(bupaIO.getParmarea());
        wsspEdterror.set(varcom.oK);
        /* Point to correct member of LINXPF. */
        wsaaLinxRunid.set(bprdIO.getSystemParam04());
        wsaaLinxJobno.set(bsscIO.getScheduleNumber());
        wsaaThreadNumber.set(bsprIO.getProcessOccNum());
        japanBilling = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), BTPRO027, appVars, "IT");
        stampDutyFlagcomp = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "CTISS013", appVars, "IT");
        BTPRO033Permission  = FeaConfg.isFeatureExist("2", "BTPRO033", appVars, "IT");
        if (bprdIO.systemParam01.isNumeric()){
            if (bprdIO.systemParam01.toInt() > 0){
                intBatchExtractSize = bprdIO.systemParam01.toInt();
            }else{
                intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
            }
        }else{
            intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();  
        }
        // changed by yy
        noRecordFound = false;
       int extractRows = populateB5353Temp1010();
        if(extractRows <= 0){
        	noRecordFound = true;
	        return ;
        }
        readChunkRecord();
        
        /* Get transaction description. */
        String coy = bsprIO.getCompany().toString();
        t1688Map = descDAO.getItems("IT", coy, "T1688", bsscIO.getLanguage().toString());
        if (t1688Map.containsKey(bprdIO.getAuthCode())) {
            descpfT1688 = t1688Map.get(bprdIO.getAuthCode());
        } else {
            descpfT1688 = new Descpf();
            descpfT1688.setLongdesc("");
        }
        /* Initialise common LIFA, LIFR, RNLA and PTRN fields */
        varcom.vrcmDate.set(getCobolDate());
        lifacmvrec.lifacmvRec.set(SPACES);
        rnlallrec.rnlallRec.set(SPACES);
        lifacmvrec.jrnseq.set(ZERO);
        rnlallrec.effdate.set(bsscIO.getEffectiveDate());
        rnlallrec.moniesDate.set(bsscIO.getEffectiveDate());
        lifrtrnPojo.setBatccoy(batcdorrec.company.toString());
        lifrtrnPojo.setRldgcoy(batcdorrec.company.toString());
        lifrtrnPojo.setGenlcoy(batcdorrec.company.toString());
        lifacmvrec.batccoy.set(batcdorrec.company);
        lifacmvrec.rldgcoy.set(batcdorrec.company);
        lifacmvrec.genlcoy.set(batcdorrec.company);
        rnlallrec.batccoy.set(batcdorrec.company);
        rnlallrec.language.set(bsscIO.getLanguage());
        lifrtrnPojo.setBatcactyr(batcdorrec.actyear.toInt());
        lifacmvrec.batcactyr.set(batcdorrec.actyear);
        rnlallrec.batcactyr.set(batcdorrec.actyear);
        lifrtrnPojo.setBatcactmn(batcdorrec.actmonth.toInt());
        lifacmvrec.batcactmn.set(batcdorrec.actmonth);
        rnlallrec.batcactmn.set(batcdorrec.actmonth);
        lifrtrnPojo.setBatctrcde(batcdorrec.trcde.toString());
        lifacmvrec.batctrcde.set(batcdorrec.trcde);
        rnlallrec.batctrcde.set(batcdorrec.trcde);
        lifrtrnPojo.setBatcbatch(batcdorrec.batch.toString());
        lifacmvrec.batcbatch.set(batcdorrec.batch);
        rnlallrec.batcbatch.set(batcdorrec.batch);
        lifrtrnPojo.setBatcbrn(batcdorrec.branch.toString());
        lifacmvrec.batcbrn.set(batcdorrec.branch);
        rnlallrec.batcbrn.set(batcdorrec.branch);
        lifrtrnPojo.setRcamt(0);
        lifacmvrec.rcamt.set(0);
        lifrtrnPojo.setCrate(BigDecimal.ZERO);
        lifacmvrec.crate.set(0);
        lifrtrnPojo.setAcctamt(BigDecimal.ZERO);
        lifacmvrec.acctamt.set(0);
        lifrtrnPojo.setUser(0);
        lifacmvrec.user.set(0);
        rnlallrec.user.set(0);
        lifrtrnPojo.setFrcdate(varcom.vrcmMaxDate.toInt());
        lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
        lifrtrnPojo.setTransactionTime(varcom.vrcmTime.toInt());

        lifrtrnPojo.setTrandesc(descpfT1688.getLongdesc());
        lifacmvrec.trandesc.set(descpfT1688.getLongdesc());
        /* MOVE BSSC-EFFECTIVE-DATE TO LIFR-TRANSACTION-DATE. */
        lifrtrnPojo.setTransactionDate(varcom.vrcmDate.toInt());

        t5645Map = itemDAO.loadSmartTable("IT", coy, "T5645");
        t3695Map = itemDAO.loadSmartTable("IT", coy, "T3695");
        t6687List = itemDAO.getAllitems("IT", coy, "T6687");
        t5688List = itemDAO.getAllitems("IT", coy, "T5688");
        t5679Map = itemDAO.loadSmartTable("IT", coy, "T5679");
        t5644List = itemDAO.getAllitems("IT", coy, "T5644");
        t6654List = itemDAO.getAllitems("IT", coy, "T6654");
        t5667Map = itemDAO.loadSmartTable("IT", coy, "T5667");
        t5671Map = itemDAO.loadSmartTable("IT", coy, "T5671");
        th605Map = itemDAO.loadSmartTable("IT", coy, "TH605");
        t5687List = itemDAO.getAllitems("IT", coy, "T5687");
        t5534List = itemDAO.getAllitems("IT", coy, "T5534");

        tr384Map = itemDAO.loadSmartTable("IT", coy, "TR384");
        tr52dMap = itemDAO.loadSmartTable("IT", coy, "TR52D");
        tr52eMap = itemDAO.loadSmartTable("IT", coy, "TR52E");
        tr2enMap = itemDAO.loadSmartTable("IT", bsprIO.getFsuco().toString(), "TR2EN");
        isDMSFlgOn =  FeaConfg.isFeatureExist(SYS_COY, DMS_FEATURE, appVars, "IT"); //ILIFE-8880
        reinstflag = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "CSLRI003", appVars, "IT");
        if(reinstflag){
        	td5j2ListMap = itemDAO.loadSmartTable("IT", coy, "TD5J2");	
        	td5j1ListMap = itemDAO.loadSmartTable("IT", coy, "TD5J1");	
        }

		isnullflag = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "CSOTH011", appVars, "IT");// ILIFE-7911

		/* Load account codes */
		if (!t5645Map.containsKey(wsaaProg)) {
			syserrrec.params.set("t5645:" + wsaaProg);
			fatalError600();
		}
		List<Itempf> t5645List = t5645Map.get(wsaaProg);
		if (t5645List.size() > wsaaT5645Size) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set("t5645");
			fatalError600();
		}
		loadT56451100(t5645List);

        /* Retrieve the suspense account sign */
        if (!t3695Map.containsKey(wsaaT5645Sacstype[1])) {
            syserrrec.params.set("t3695:" + wsaaT5645Sacstype[1]);
            fatalError600();
        }
        Itempf item3695 = t3695Map.get(wsaaT5645Sacstype[1]).get(0);
        t3695rec.t3695Rec.set(StringUtil.rawToString(item3695.getGenarea()));

        /* Load tax relief methods. */
        if (t6687List.size() > wsaaT6687Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set("t6687");
            fatalError600();
        }
        loadT66871300(t6687List);

        /* Load contract type details. */
        if (t5688List.size() > wsaaT5688Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set("t5688");
            fatalError600();
        }
        loadT56881400(t5688List);

        /* Load Contract statii. */
        if (!t5679Map.containsKey(bprdIO.getAuthCode())) {
            syserrrec.params.set("t5679:" + bprdIO.getAuthCode());
            fatalError600();
        }
        Itempf item5679 = t5679Map.get(bprdIO.getAuthCode()).get(0);
        t5679rec.t5679Rec.set(StringUtil.rawToString(item5679.getGenarea()));

        /* Load the commission methods. */
        if (t5644List.size() > wsaaT5644Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set("t5644");
            fatalError600();
        }
        loadT56441600(t5644List);

        /* Load the Collection routines */
        if (t6654List.size() > wsaaT6654Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set("t6654");
            fatalError600();
        }
        loadT66541700(t6654List);

        List<Itempf> t5667List = new ArrayList<Itempf>();
        /* Load the tolerance limits. */
        for(String itemitemKey:t5667Map.keySet()){
            if(itemitemKey.substring(0,4).equals(bprdIO.getAuthCode().toString())){
                t5667List.addAll(t5667Map.get(itemitemKey));
            }
        }
        if(!japanBilling && isNE(bprdIO.getSystemParam05(), "JPN")){
        if (t5667List.size() == 0) {
            syserrrec.params.set("t5667:" + bprdIO.getAuthCode());
            fatalError600();
        }
        if (t5667List.size() > wsaaT5667Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set("t5667");
            fatalError600();
        }
        }
        loadT56671800(t5667List);

        List<Itempf> t5671List = new ArrayList<Itempf>();
        /* Load the tolerance limits. */
        for(String itemitemKey:t5671Map.keySet()){
            if(itemitemKey.substring(0,4).equals(bprdIO.getAuthCode().toString())){
                t5671List.addAll(t5671Map.get(itemitemKey));
            }
        }
        if(!japanBilling && isNE(bprdIO.getSystemParam05(), "JPN")){
        if (t5671List.size() == 0) {
            syserrrec.params.set("t5671:" + bprdIO.getAuthCode());
            fatalError600();
        }
        /* Load the coverage switching details. */
        if (t5671List.size() > wsaaT5671Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set("t5671");
            fatalError600();
        }
        }
        loadT56711900(t5671List);

        /* Load Company Defaults */
        if (!th605Map.containsKey(bsprIO.getCompany())) {
            syserrrec.params.set("th605:" + bsprIO.getCompany());
            fatalError600();
        }
        Itempf itemh605 = th605Map.get(bprdIO.getCompany()).get(0);
        th605rec.th605Rec.set(StringUtil.rawToString(itemh605.getGenarea()));
        if (isNE(th605rec.bonusInd, "Y")) {
            return;
        }
        /* Load coverage details. */
        if (t5687List.size() > wsaaT5687Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set("t5687");
            fatalError600();
        }
        loadT56871a00(t5687List);

        /* Load the benefit billing details. */
        if (t5534List.size() > wsaaT5534Size) {
            syserrrec.statuz.set(h791);
            syserrrec.params.set("t5534");
            fatalError600();
        }
        loadT55341b00(t5534List);
        gstOnCommFlag = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "CTISS007", appVars, "IT");//ILIFE-7965
        String prmhldtradItem = "CSOTH010";//ILIFE-8509
		prmhldtrad = FeaConfg.isFeatureExist(batcdorrec.company.toString(), prmhldtradItem, appVars, "IT");
		if(prmhldtrad)
			ta524Map = itemDAO.getItemMap("IT", coy, "TA524");
    }

    private void readChunkRecord() {
    	List<Linxpf> linxpfList = loadDataByBatch();
        iteratorList = linxpfList.iterator();
        Map<String, List<String>> chdrInfor = new HashMap<String, List<String>>();
        Map<String, List<String>> clrrInfor = new HashMap<String, List<String>>();
        Map<String, List<String>> chdrGLInfor = new HashMap<String, List<String>>();
        for (Linxpf lx : linxpfList) {
            if (chdrInfor.containsKey(lx.getChdrcoy())) {
                chdrInfor.get(lx.getChdrcoy()).add(lx.getChdrnum());
                chdrGLInfor.get(lx.getChdrcoy()).add(lx.getChdrnum() + "00");
                clrrInfor.get(lx.getChdrcoy()).add(lx.getChdrnum()+lx.getPayrseqno());
            } else {
                List<String> chdrnumList = new ArrayList<String>();
                chdrnumList.add(lx.getChdrnum());
                chdrInfor.put(lx.getChdrcoy(), chdrnumList);

                List<String> chdrnumGLList = new ArrayList<String>();
                chdrnumGLList.add(lx.getChdrnum() + "00");
                chdrGLInfor.put(lx.getChdrcoy(), chdrnumGLList);
                
                List<String> clrrInforList = new ArrayList<String>();
                clrrInforList.add(lx.getChdrnum()+lx.getPayrseqno());
                clrrInfor.put(lx.getChdrcoy(), clrrInforList);
            }
        }
        taxdMap.clear();
        acblMap.clear();
        acblGLMap.clear();
        covrMap.clear();
        agcmMap.clear();
        clrrMap.clear();
        rertpfMap.clear();
        incrpfMap.clear();
        pcddpfMap.clear();
        riskPremflag = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), RISKPREM_FEATURE_ID, appVars, "IT");
        if(riskPremflag)
        	rskpMap.clear();
        for (String coyKey : chdrInfor.keySet()) {
            taxdMap.putAll(taxdpfDAO.searchTaxdRecord(coyKey, chdrInfor.get(coyKey)));
            acblMap.putAll(acblpfDAO.searchAcblRecord(coyKey, chdrInfor.get(coyKey)));
            acblGLMap.putAll(acblpfDAO.searchAcblRecord(coyKey, chdrGLInfor.get(coyKey)));
            covrMap.putAll(covrpfDAO.searchCovrlnb(coyKey, chdrInfor.get(coyKey)));//ILIFE-7593 by dpuhawan
            agcmMap.putAll(agcmpfDAO.searchAgcmrnl(coyKey, chdrInfor.get(coyKey)));//ILIFE-7593 by dpuhawan
            clrrMap.putAll(clrrpfDAO.searchClrrpfRecord(coyKey, clrrInfor.get(coyKey)));
            if(riskPremflag)
            	rskpMap.putAll(rskppfDAO.searchRskppf(coyKey, chdrInfor.get(coyKey)));
            rertpfMap.putAll(rertpfDAO.searchRertRecordByChdrnum(coyKey, chdrInfor.get(coyKey)));
            incrpfMap.putAll(incrpfDAO.getIncrgpMap(coyKey, chdrInfor.get(coyKey)));
            pcddpfMap = br613TempDAO.getPcddpfRecords(coyKey,chdrInfor.get(coyKey));
        }
    }
    
protected List<Linxpf>  loadDataByBatch(){
	return b5353DAO.loadDataByBatch(intBatchID, wsaaThreadMember.toString());
}

protected void loadT56451100(List<Itempf> itemList)
 {
        int wsaaT5645Offset = 1;
        for(Itempf itempf:itemList){
            t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
            int wsaaT5645Sub =1;
            while ( !(isGT(wsaaT5645Sub, 15))) {
                wsaaT5645Cnttot[wsaaT5645Offset].set(t5645rec.cnttot[wsaaT5645Sub]);
                wsaaT5645Glmap[wsaaT5645Offset].set(t5645rec.glmap[wsaaT5645Sub]);
                wsaaT5645Sacscode[wsaaT5645Offset].set(t5645rec.sacscode[wsaaT5645Sub]);
                wsaaT5645Sacstype[wsaaT5645Offset].set(t5645rec.sacstype[wsaaT5645Sub]);
                wsaaT5645Sign[wsaaT5645Offset].set(t5645rec.sign[wsaaT5645Sub]);
                wsaaT5645Sub++;
                wsaaT5645Offset++;
            }
        }
    }

protected void loadT66871300(List<Itempf> itemList)
 {
        int i = 1;
        for (Itempf itempf : itemList) {
            t6687rec.t6687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
            wsaaT6687Taxrelsubr[i].set(t6687rec.taxrelsub);
            wsaaT6687Taxrelmth[i].set(itempf.getItemitem());
            i++;
        }
    }

protected void loadT56881400(List<Itempf> itemList)
 {
        int i = 1;
        for (Itempf itempf : itemList) {
            t5688rec.t5688Rec.set(StringUtil.rawToString(itempf.getGenarea()));
            wsaaT5688Itmfrm[i].set(itempf.getItmfrm());
            wsaaT5688Cnttype[i].set(itempf.getItemitem());
            wsaaT5688Comlvlacc[i].set(t5688rec.comlvlacc);
            wsaaT5688Revacc[i].set(t5688rec.revacc);
            i++;
        }
    }

protected void loadT56441600(List<Itempf> itemList)
 {
        int i = 1;
        for (Itempf itempf : itemList) {
            t5644rec.t5644Rec.set(StringUtil.rawToString(itempf.getGenarea()));
            wsaaT5644Commth[i].set(itempf.getItemitem());
            wsaaT5644Comsub[i].set(t5644rec.compysubr);
            i++;
        }
    }


protected void loadT66541700(List<Itempf> itemList)
 {
        int i = 1;
        for (Itempf itempf : itemList) {
            t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
            wsaaT6654Key[i].set(itempf.getItemitem());
            wsaaT6654Collsub[i].set(t6654rec.collectsub);
            i++;
        }
    }

protected void loadT56671800(List<Itempf> itemList)
 {
        int i = 1;
        for (Itempf itempf : itemList) {
            t5667rec.t5667Rec.set(StringUtil.rawToString(itempf.getGenarea()));
            wsaaT5667ArrayInner.wsaaT5667Key[i].set(itempf.getItemitem());
            wsaaT5667ArrayInner.wsaaT5667Sfind[i].set(t5667rec.sfind);
            wsaaT5667ArrayInner.wsaaT5667Freqs[i].set(t5667rec.freqs);
            wsaaT5667ArrayInner.wsaaT5667Prmtols[i].set(t5667rec.prmtols);
            wsaaT5667ArrayInner.wsaaT5667MaxAmounts[i].set(t5667rec.maxAmounts);
            wsaaT5667ArrayInner.wsaaT5667Prmtolns[i].set(t5667rec.prmtolns);
            wsaaT5667ArrayInner.wsaaT5667Maxamts[i].set(t5667rec.maxamts);
            i++;
        }
    }

protected void loadT56711900(List<Itempf> itemList)
 {
        int i = 1;
        for (Itempf itempf : itemList) {
            t5671rec.t5671Rec.set(StringUtil.rawToString(itempf.getGenarea()));
            wsaaT5671Key[i].set(itempf.getItemitem());
            wsaaT5671Subprogs[i].set(t5671rec.subprogs);
            i++;
        }
    }

protected void loadT56871a00(List<Itempf> itemList)
 {
        int i = 1;
        for (Itempf itempf : itemList) {
            t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
            wsaaT5687Itmfrm[i].set(itempf.getItmfrm());
            wsaaT5687Crtable[i].set(itempf.getItemitem());
            wsaaT5687Bbmeth[i].set(t5687rec.bbmeth);
            i++;
        }
    }

protected void loadT55341b00(List<Itempf> itemList)
 {
        int i = 1;
        for (Itempf itempf : itemList) {
            t5534rec.t5534Rec.set(StringUtil.rawToString(itempf.getGenarea()));
            wsaaT5534Bbmeth[i].set(itempf.getItemitem());
            wsaaT5534UnitFreq[i].set(t5534rec.unitFreq);
            i++;
        }
    }

protected void readTd5j2(String crtable){
	List<Itempf> itempfList = new ArrayList<Itempf>();
//	boolean itemFound = false;
	if (td5j2ListMap.containsKey(crtable)){	
		itempfList = td5j2ListMap.get(crtable);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = iterator.next();
//			itempf = ;
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(bsscIO.getEffectiveDate().toString()) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(bsscIO.getEffectiveDate().toString()) <= Integer.parseInt(itempf.getItmto().toString())){
					td5j2rec.td5j2Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
//					itemFound = true;
				}
			}else{
				td5j2rec.td5j2Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
//				itemFound = true;					
			}				
		}		
	}
	else {
		td5j2rec.td5j2Rec.set(SPACES);
	}	
}

protected void readTd5j1(String cnttype){
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (td5j1ListMap.containsKey(cnttype)){	
		itempfList = td5j1ListMap.get(cnttype);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(bsscIO.getEffectiveDate().toString()) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(bsscIO.getEffectiveDate().toString()) <= Integer.parseInt(itempf.getItmto().toString())){
					td5j1rec.td5j1Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
					itemFound = true;
				}
			}else{
				td5j1rec.td5j1Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
				itemFound = true;					
			}				
		}		
	}
	else {
		td5j1rec.td5j1Rec.set(SPACES);
	}	
}

protected void readFile2000()
    {
	LOGGER.info("readFile2000 start");
        /*READ-FILE*/
        /*  Read the records from the temporary LINS file.*/
		if(noRecordFound){
            wsspEdterror.set(varcom.endp);
            return;
		}
        if(!iteratorList.hasNext()){
            intBatchID++;
            readChunkRecord();
            if(!iteratorList.hasNext()){
                wsspEdterror.set(varcom.endp);
                return ;
            }
        }
        linxpfResult = iteratorList.next();
//        rdFl2000CustomerSpecific();
        /*  Set up the key for the SYSR- copybook should a system error*/
        /*  for this instalment occur.*/
        wsysChdrcoy.set(linxpfResult.getChdrcoy());
        wsysChdrnum.set(linxpfResult.getChdrnum());
        wsysInstfrom.set(linxpfResult.getInstfrom());
        if (!linxpfResult.getChdrnum().equals(wsaaPrevChdrnum)) {
        	 payrOutstamt.set(ZERO);
             chdrOutstamt.set(ZERO);
        }
        if(isEQ(payrOutstamt,ZERO)) {
        	payrOutstamt.set(linxpfResult.getPayrOutstamt());
        }
        if(isEQ(chdrOutstamt,ZERO)) {
        	chdrOutstamt.set(linxpfResult.getChdrOutstamt());
        }
        contot01++;
        /*EXIT*/
        LOGGER.info("readFile2000 end");
    }
//protected void  rdFl2000CustomerSpecific(){
//	
//}

protected void edit2500()
 {
	LOGGER.info("edit2500 start");
        wsspEdterror.set(varcom.oK);
        wsaaApaRequired.set(ZERO);
        wsysCnttype.set(linxpfResult.getChdrCnttype());
        /* See if the instalment being processed has the same contract */
        /* as the previous instalment, we do not need to validate the */
        /* CHDR details again. */
        if(japanBilling){
        	if(isEQ(linxpfResult.getPayrPtdate(),ZERO) 
        			|| isLTE(linxpfResult.getPayrPtdate(),linxpfResult.getChdrOccdate())){
        		if (isEQ(bprdIO.getSystemParam05(), "JPN")) {
        			initFlag = true;
        		}
        		else{
        			wsspEdterror.set(SPACES);
        			return;
        		}
        	}
        	else{
        		if (isEQ(bprdIO.getSystemParam05(), "JPN")) {
        			 wsspEdterror.set(SPACES);
            	     return;
        		}
        	}
        }
        else{
        	if (isEQ(bprdIO.getSystemParam05(), "JPN")) {
				wsspEdterror.set(SPACES);
				return;
			}
        }
        if (!linxpfResult.getChdrnum().equals(wsaaPrevChdrnum)) {
            wsaaPrevLins = "Y";
            validateContract2300();
        }
        if (!wsaaValidContract) {
            /* Increment LINS not collected */
            contot09++;
            wsspEdterror.set(SPACES);
            return;
        }
        if ("N".equals(wsaaPrevLins)) {
            /* Log no. LINS still short of the cbillamt. */
            contot06++;
            /* Increment the total of outstanding premiums. */
            temptot.set(linxpfResult.getInstamt06());
            contot05.add(temptot);
            /* Log no. LINS not collected. */
            contot04++;//ILIFE-4731
            wsspEdterror.set(SPACES);
            return;
        }
        String forenum = linxpfResult.getChdrnum() + linxpfResult.getPayrseqno();
        for(Clrrpf c:clrrMap.get(forenum)){
            if(c.getForepfx().equals(linxpfResult.getChdrchdrpfx())){
                clrrpf = c;
            }
        }
        wsaaPayrnum = clrrpf.getClntnum();
        wsaaPayrcoy = clrrpf.getClntcoy();
        /* Calculate tax */
        b600GetTax();
        /* Check for tax relief */
        prasrec.taxrelamt.set(ZERO);
        if(prmhldtrad && ta524Map.get(linxpfResult.getChdrCnttype()) != null)//ILIFE-8509
			prmhpf = prmhpfDAO.getPrmhRecord(linxpfResult.getChdrcoy(), linxpfResult.getChdrnum());
        else
        	prmhpf = null;
        if(prmhpf != null && bsscIO.getEffectiveDate().toInt()<=prmhpf.getTodate())
        	wsaaNetCbillamt.set(linxpfResult.getProramt().add(linxpfResult.getProrcntfee()));
        else
        	wsaaNetCbillamt.set(linxpfResult.getCbillamt());
        
        wsaaNetCbillamt.getbigdata().add(wsaaTotTax);
        
        if(reinstflag)
        	markprorated();
        if (linxpfResult.getPayrTaxrelmth()!=null && !linxpfResult.getPayrTaxrelmth().isEmpty()) {
            calcTaxRelief2600();
        }
        /* If waiver-holding is required, validate the amount required */
        /* is already in the holding account. */
        if (BigDecimal.ZERO.compareTo(linxpfResult.getInstamt05())!=0) {
            waiverHolding2900b();
			/*
			 * if (isEQ(wsaaT5645Sign[7], "-")) { compute(wsaaWaiverAvail, 2).set((sub(0,
			 * wsaaWaiverAvail))); }
			 */
            if (isGT(wsaaWaiverAvail, linxpfResult.getInstamt05())) {
                /* Insufficient waiver holding */
                contot13++;
                /* Log no. LINS not collected. */
                contot04++;
                wsspEdterror.set(SPACES);
                return;
            }
        }
        if(isLT(wsaaWaiverAvail, ZERO)) {
        	compute(wsaaWaiverAvail, 2).set(mult(-1.00, wsaaWaiverAvail));
        }
        if (isGTE(wsaaWaiverAvail, wsaaNetCbillamt)) {
            wsaaSuspAvail.set(wsaaWaiverAvail);
            wsaaWaiveAmtPaid.set(wsaaWaiverAvail);
        } else {
            suspenseAvail2700();
            if (isGT(wsaaWaiverAvail, ZERO)) {
                wsaaSuspAvail.add(wsaaWaiverAvail);
                wsaaWaiveAmtPaid.set(wsaaWaiverAvail);
            }
        }

        /* Check money is available */
        // suspenseAvail2700();
        // ILIFE-1828 END

        /* If insufficient suspense, pull Advance Premium Deposit (APA) */
        /* available into suspense before checking for tolerance. */
        /* Check tolerance if there is insufficient money. */
        if (isLTE(wsaaNetCbillamt, wsaaSuspAvail)) {
            linxpfResult.setInstamt03(BigDecimal.ZERO);
        } else {
            cashDivAvail2900a();
            if (isGT(wsaaNetCbillamt, wsaaSuspAvail)) {
                apaAvail2900();
            }
            /* PERFORM 2900-APA-AVAIL <V42009> */
            wsaaWaiveAmtPaid.set(wsaaWaiverAvail);
            wsaaAmtPaid.set(sub(wsaaSuspAvail,wsaaWaiveAmtPaid));
            a400CheckAgent();
            calcTolerance2800();
        }
        LOGGER.info("edit2500 end");
    }



protected void softlockContract2100()
 {
//        sftlockrec.sftlockRec.set(SPACES);
//        sftlockrec.enttyp.set("CH");
//        sftlockrec.company.set(bsprIO.getCompany());
//        sftlockrec.user.set(99999);
//        sftlockrec.transaction.set(bprdIO.getAuthCode());
//        sftlockrec.function.set("LOCK");
//        sftlockrec.entity.set(linxpfResult.getChdrnum());
//        sftlockrec.statuz.set(varcom.oK);
//        callProgram(Sftlock.class, sftlockrec.sftlockRec);
//        if (isNE(sftlockrec.statuz, varcom.oK) && isNE(sftlockrec.statuz, "LOCK")) {
//            wsysSysparams.set(sftlockrec.sftlockRec);
//            syserrrec.params.set(wsysSystemErrorParams);
//            syserrrec.statuz.set(sftlockrec.statuz);
//            fatalError600();
//        }
		// changed by yy
        sftlockRecBean = new SftlockRecBean();
    	sftlockRecBean.setFunction("LOCK");
    	sftlockRecBean.setCompany(bsprIO.getCompany().toString().trim());
    	sftlockRecBean.setEnttyp("CH");
    	sftlockRecBean.setEntity(linxpfResult.getChdrnum().trim());
    	sftlockRecBean.setTransaction(bprdIO.getAuthCode().toString());
    	sftlockRecBean.setUser("99999");
    	sftlockUtil.process(sftlockRecBean, appVars);
    	if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())
    		&& !"LOCK".equals(sftlockRecBean.getStatuz())) {
    			syserrrec.statuz.set(sftlockRecBean.getStatuz());
    			fatalError600();
    	}
    }

protected void validateContract2300()
 {
        /* For each contract retrieved we must look up the contract type */
        /* details on T5688 */
        wsaaT5688Ix = 1;
        boolean foundFlag = false;
        for (; wsaaT5688Ix < wsaaT5688Rec.length; wsaaT5688Ix++) {
            if (isEQ(wsaaT5688Key[wsaaT5688Ix], linxpfResult.getChdrCnttype())) {
                foundFlag = true;
                break;
            }
        }
        if (!foundFlag) {
            syserrrec.statuz.set(e308);
            StringUtil stringVariable1 = new StringUtil();
            stringVariable1.addExpression("t5688");
            stringVariable1.addExpression(wsysSystemErrorParams);
            stringVariable1.setStringInto(syserrrec.params);
            fatalError600();
        }
        /* We must find the effective T5688 entry for the contract */
        /* (T5688 entries will have been loaded in descending sequence */
        /* into the array). */
        wsaaDateFound = false;

        for (; wsaaT5688Ix < wsaaT5688Cnttype.length; wsaaT5688Ix++) {
            if (isGTE(linxpfResult.getChdrOccdate(), wsaaT5688Itmfrm[wsaaT5688Ix])) {
                wsaaDateFound = true;
                break;
            }
        }

        if (!wsaaDateFound) {
            syserrrec.statuz.set(h979);
            syserrrec.params.set(wsysSystemErrorParams);
            fatalError600();
        }
        /* Validate the statii of the contract */
        wsaaValidContract = false;
        for (wsaaT5679Sub = 1; wsaaT5679Sub <= 12; wsaaT5679Sub++) {
            if (isEQ(t5679rec.cnRiskStat[wsaaT5679Sub], linxpfResult.getChdrStatcode())) {
                for (wsaaT5679Sub = 1; wsaaT5679Sub <= 12; wsaaT5679Sub++) {
                    if (isEQ(t5679rec.cnPremStat[wsaaT5679Sub], linxpfResult.getChdrPstatcode())) {
                        wsaaT5679Sub = 13;
                        wsaaValidContract = true;
                    }
                }
            }
        }
        wsaaPrevChdrnum = linxpfResult.getChdrnum();
    }

protected void calcTaxRelief2600()
 {
        ArraySearch as1 = ArraySearch.getInstance(wsaaT6687Rec);
        as1.setIndices(wsaaT6687Ix);
        as1.addSearchKey(wsaaT6687Key, linxpfResult.getPayrTaxrelmth(), true);
        if (!as1.binarySearch()) {
            StringUtil stringVariable1 = new StringUtil();
            stringVariable1.addExpression("t6687");
            stringVariable1.addExpression(linxpfResult.getPayrTaxrelmth());
            stringVariable1.setStringInto(wsysSysparams);
            syserrrec.params.set(wsysSystemErrorParams);
            fatalError600();
        }
        if (isEQ(wsaaT6687Taxrelsubr[wsaaT6687Ix.toInt()], SPACES)) {
            return;
        }
        prasrec.cnttype.set(linxpfResult.getChdrCnttype());
        prasrec.clntnum.set(clrrpf.getClntnum());
        prasrec.clntcoy.set(clrrpf.getClntcoy());
        prasrec.incomeSeqNo.set(linxpfResult.getPayrIncomeseqno());
        prasrec.taxrelmth.set(linxpfResult.getPayrTaxrelmth());
        prasrec.effdate.set(linxpfResult.getBillcd());
        prasrec.company.set(linxpfResult.getChdrcoy());
        if(prmhpf != null && bsscIO.getEffectiveDate().toInt()<=prmhpf.getTodate())//ILIFE-8509
        	prasrec.grossprem.set(linxpfResult.getProramt().add(linxpfResult.getProrcntfee()));
        else
        	prasrec.grossprem.set(linxpfResult.getInstamt06());
        prasrec.grossprem.subtract(PackedDecimalData.parseObject(wsaaTotTax));
        prasrec.statuz.set(varcom.oK);
        callProgram(wsaaT6687Taxrelsubr[wsaaT6687Ix.toInt()], prasrec.prascalcRec);
        if (isNE(prasrec.statuz, varcom.oK)) {
            syserrrec.subrname.set(wsaaT6687Taxrelsubr[wsaaT6687Ix.toInt()]);
            wsysSysparams.set(prasrec.prascalcRec);
            syserrrec.params.set(wsysSystemErrorParams);
            fatalError600();
        }
        if (isNE(prasrec.taxrelamt, 0)) {
        //ILB-441 start
        //	zrdecplrec.amountIn.set(prasrec.taxrelamt);
        	zrdecplcPojo.setAmountIn(prasrec.taxrelamt.getbigdata());
        	zrdecplcPojo.setCurrency(linxpfResult.getCntcurr());
        	
            c000CallRounding(zrdecplcPojo);
           // prasrec.taxrelamt.set(zrdecplrec.amountOut);
            prasrec.taxrelamt.set(zrdecplcPojo.getAmountOut());
            //ILB-441 ends
        }
        compute(wsaaNetCbillamt, 2).set(sub(linxpfResult.getCbillamt(), prasrec.taxrelamt));
    }


protected void suspenseAvail2700()
 {
		Acblpf acblpfResult = null;
		// Follow by MTI Ticket ILIFE-7869
//        List<Acblpf> acblpfList = acblMap.get(linxpfResult.getChdrnum());
		List<Acblpf> acblpfList = acblpfDAO.searchAcblenqRecord(linxpfResult.getChdrcoy(), linxpfResult.getChdrnum());
		// ILIFE-7911 starts
		if (isnullflag) {
			if ((null != acblpfList) && !acblpfList.isEmpty()) {
				for (Acblpf acblpf : acblpfList) {
					if(initFlag){
						if (acblpf.getOrigcurr().equals(linxpfResult.getBillcurr())
							&& isEQ(acblpf.getSacscode(), wsaaT5645Sacscode[39])
							&& isEQ(acblpf.getSacstyp(), wsaaT5645Sacstype[39])) {
							acblpfResult = acblpf;
							break;
						}
					}
					if (acblpf.getOrigcurr().equals(linxpfResult.getBillcurr())
							&& isEQ(acblpf.getSacscode(), wsaaT5645Sacscode[1])
							&& isEQ(acblpf.getSacstyp(), wsaaT5645Sacstype[1])) {
						acblpfResult = acblpf;
						break;
					}
				}
			}
		} else {
			for (Acblpf acblpf : acblpfList) {
				if(initFlag){
					if (acblpf.getOrigcurr().equals(linxpfResult.getBillcurr())
							&& isEQ(acblpf.getSacscode(), wsaaT5645Sacscode[39])
							&& isEQ(acblpf.getSacstyp(), wsaaT5645Sacstype[39])) {
						acblpfResult = acblpf;
						break;
					  }
				}
				else{
				if (acblpf.getOrigcurr().equals(linxpfResult.getBillcurr())
						&& isEQ(acblpf.getSacscode(), wsaaT5645Sacscode[1])
						&& isEQ(acblpf.getSacstyp(), wsaaT5645Sacstype[1])) {
					acblpfResult = acblpf;
					break;
				  }
				}
			}
		}
		// ILIFE-7911 ends
		if (acblpfResult != null) {
			if (isEQ(t3695rec.sign, "-")) {
				compute(wsaaSuspAvail, 2).set(sub(0, acblpfResult.getSacscurbal()));
			} else {
				wsaaSuspAvail.set(acblpfResult.getSacscurbal());
			}
		} else {
			wsaaSuspAvail.set(0);
		}
		BigDecimal totSuspense = BigDecimal.ZERO;                         //IBPLIFE-1447 Start
		if(!(acblpfLifrtrnList.isEmpty())) {
		for(Acblpf a : acblpfLifrtrnList) {
			if(isEQ(a.getRldgacct(),linxpfResult.getChdrnum()))
					totSuspense = totSuspense.add(a.getSacscurbal());
		}
		}
		compute(wsaaSuspAvail, 2).set(sub(wsaaSuspAvail, totSuspense));   //IBPLIFE-1447 end
	}

protected void calcTolerance2800()
    {
        GotoLabel nextMethod = GotoLabel.DEFAULT;
        while (true) {
            try {
                switch (nextMethod) {
                case DEFAULT:
                    start2810();
                    computeTolerance2820();
                case failTolerance2855:
                    failTolerance2855();
                case exit2899:
                }
                break;
            }
            catch (GOTOException e){
                nextMethod = (GotoLabel) e.getNextMethod();
            }
        }
    }

// changed by yy for ILIFE-3109
protected boolean start2810()
 {
        /* Look up for any tolerance attributed to this instalment. */
        int wsaaT5667Ix = 1;
        boolean foundFlag = false;
        for (; wsaaT5667Ix < wsaaT5667ArrayInner.wsaaT5667Rec.length; wsaaT5667Ix++) {
            if (isEQ(linxpfResult.getBillcurr(), wsaaT5667ArrayInner.wsaaT5667Curr[wsaaT5667Ix])) {
                foundFlag = true;
                break;
            }
        }
        if (!foundFlag) {
            return false;
        }
        /* Having found the currency we must look for the frequency */
        wsaaToleranceChk.set("1");
        boolean freqFound = false;
        for (int wsaaT5667Sub = 1; wsaaT5667Sub <= 11; wsaaT5667Sub++) {
            if (isEQ(wsaaT5667ArrayInner.wsaaT5667Freq[wsaaT5667Ix][wsaaT5667Sub], linxpfResult.getInstfreq())) {
                wsaaSfind.set(wsaaT5667ArrayInner.wsaaT5667Sfind[wsaaT5667Ix]);
                wsaaPrmtol.set(wsaaT5667ArrayInner.wsaaT5667Prmtol[wsaaT5667Ix][wsaaT5667Sub]);
                wsaaPrmtoln.set(wsaaT5667ArrayInner.wsaaT5667Prmtoln[wsaaT5667Ix][wsaaT5667Sub]);
                wsaaMaxAmount.set(wsaaT5667ArrayInner.wsaaT5667MaxAmount[wsaaT5667Ix][wsaaT5667Sub]);
                wsaaMaxamt.set(wsaaT5667ArrayInner.wsaaT5667Maxamt[wsaaT5667Ix][wsaaT5667Sub]);
                freqFound = true;
                return true;
            }
        }
        /* Log no. of LINS where FREQ not on T5667 */
        contot07++;
        return false;
    }

protected void computeTolerance2820()
    {
        /*  Using the tolerance entry found calculate the overall amount*/
        /*  to be collected.*/
        if (toleranceLimit1.isTrue()) {
            compute(wsaaToleranceAllowed, 3).setRounded(div((mult(wsaaPrmtol, wsaaNetCbillamt)), 100));
            if (isGT(wsaaToleranceAllowed, wsaaMaxAmount)) {
                wsaaToleranceAllowed.set(wsaaMaxAmount);
            }
        }
        else {
            compute(wsaaToleranceAllowed, 3).setRounded(div((mult(wsaaPrmtoln, wsaaNetCbillamt)), 100));
            if (isGT(wsaaToleranceAllowed, wsaaMaxamt)) {
                wsaaToleranceAllowed.set(wsaaMaxamt);
            }
        }
        /* MOVE WSAA-TOLERANCE-ALLOWED TO ZRDP-AMOUNT-IN.               */
        /* MOVE BILLCURR               TO ZRDP-CURRENCY.                */
        /* PERFORM C000-CALL-ROUNDING.                                  */
        /* MOVE ZRDP-AMOUNT-OUT        TO WSAA-TOLERANCE-ALLOWED.       */
        //ILB-441 starts
        if (isNE(wsaaToleranceAllowed, 0)) {
        	zrdecplcPojo.setAmountIn(wsaaToleranceAllowed.getbigdata());
        	zrdecplcPojo.setCurrency(linxpfResult.getBillcurr());
          /*  zrdecplrec.amountIn.set(wsaaToleranceAllowed);
            zrdecplrec.currency.set(linxpfResult.getBillcurr());*/
            c000CallRounding(zrdecplcPojo);
           // wsaaToleranceAllowed.set(zrdecplrec.amountOut);
            wsaaToleranceAllowed.set(zrdecplcPojo.getAmountOut());
            //ILB-441 ends
        }
        compute(wsaaSuspCbillDiff, 3).setRounded((sub(wsaaNetCbillamt, wsaaSuspAvail)));
        /*  Update the suspense as being enough to cover the premium if*/
        /*  it is within the allowable tolerance.  Convert the CBILLAMT*/
        /*  (the billing currency's amount) for multi-currency contracts.*/
        /*  By-pass the outstanding instalment totals.*/
        if (isLTE(wsaaSuspCbillDiff, wsaaToleranceAllowed)) {
            wsaaToleranceAllowed.set(wsaaSuspCbillDiff);
            wsaaSuspAvail.add(wsaaSuspCbillDiff);
            if (isEQ(linxpfResult.getCntcurr(), linxpfResult.getBillcurr())) {
                linxpfResult.setInstamt03(wsaaToleranceAllowed.getbigdata());
            }
            else {
                compute(wsaaImRatio, 10).setRounded(div(wsaaAmtPaid, wsaaNetCbillamt));
                compute(wsaaCntcurrReceived, 10).setRounded(mult(linxpfResult.getInstamt06(), wsaaImRatio));
                if (isNE(wsaaCntcurrReceived, 0)) {
                	//ILB-441 starts
                	zrdecplcPojo.setAmountIn(wsaaCntcurrReceived.getbigdata());
                	zrdecplcPojo.setCurrency(linxpfResult.getCntcurr());
                   /* zrdecplrec.amountIn.set(wsaaCntcurrReceived);
                    zrdecplrec.currency.set(linxpfResult.getCntcurr());*/
                    c000CallRounding(zrdecplcPojo);
                   // wsaaCntcurrReceived.set(zrdecplc.amountOut);
                    wsaaCntcurrReceived.set(zrdecplcPojo.getAmountOut());
                    //ILB-441 ends
                }
                linxpfResult.setInstamt03(linxpfResult.getInstamt06().subtract(wsaaCntcurrReceived.getbigdata()));
            }
            goTo(GotoLabel.exit2899);
        }
        if (toleranceLimit1.isTrue()
        && isNE(wsaaMaxamt, 0)) {
            if (agtNotTerminated.isTrue()
            || (agtTerminated.isTrue()
            && isEQ(wsaaSfind, "2"))) {
                wsaaToleranceChk.set("2");
                computeTolerance2820();
                return ;
            }
        }
    }

protected int populateB5353Temp1010(){
	if(japanBilling){
		if (isEQ(bprdIO.getSystemParam05(), "JPN"))
			return b5353DAO.populateB5353TempJpn(intBatchExtractSize, wsaaLinxFn.toString(), wsaaThreadMember.toString());
		else
		return b5353DAO.getB5353DataCount(wsaaThreadMember.toString());
	}
	else{
		if (isEQ(bprdIO.getSystemParam05(), "JPN"))
			return 0;
		else
			return b5353DAO.populateB5353Temp(intBatchExtractSize, wsaaLinxFn.toString(), wsaaThreadMember.toString());
	}
		
}

protected void failTolerance2855()
    {
        /*    PERFORM 3800-UNLK-CHDR.                                      */
        /*  Log no. LINS  still short of the cbillamt.*/
        contot06++;
        /*  Log no. LINS not collected.*/
        contot04++;
        wsaaPrevLins = "N";
        /*  Increment the total of outstanding premiums.*/
        if(prmhpf != null && bsscIO.getEffectiveDate().toInt()<=prmhpf.getTodate())//ILIFE-8509
        	temptot.set(linxpfResult.getProramt().add(linxpfResult.getProrcntfee()));
        else
        	temptot.set(linxpfResult.getInstamt06());
        contot05.add(temptot);
        wsaaToleranceAllowed.set(ZERO);
        wsspEdterror.set(SPACES);
    }

protected void cashDivAvail2900a()
    {
            wsaaDivRequired.set(ZERO);
            if (isGT(wsaaNetCbillamt, wsaaSuspAvail)) {
                if(checkDivAvailable2950a()){
                    checkDivAmount2960a();
                }
            }
    }

protected boolean checkDivAvailable2950a()
 {
		if (linxpfResult.getHcsdZdivopt() != null && !linxpfResult.getHcsdZdivopt().isEmpty()) {
			csdopt.divOption.set(linxpfResult.getHcsdZdivopt());
			if (csdopt.csdPremSettlement.isTrue()) {
				return true;
			}
		}
		return false;
	}

protected void checkDivAmount2960a()
 {
        Acblpf acblpfResult = null;
        List<Acblpf> acblpfList = acblMap.get(linxpfResult.getChdrnum());
		// ILIFE-7911 starts
		if (isnullflag) {
			if ((null != acblpfList) && !acblpfList.isEmpty()) {
				for (Acblpf acblpf : acblpfList) {
					if (acblpf.getOrigcurr().equals(linxpfResult.getBillcurr())
							&& isEQ(acblpf.getSacscode(), wsaaT5645Sacscode[34])
							&& isEQ(acblpf.getSacstyp(), wsaaT5645Sacstype[34])) {
						acblpfResult = acblpf;
						break;
					}
				}
			}
		} else {
			for (Acblpf acblpf : acblpfList) {
				if (acblpf.getOrigcurr().equals(linxpfResult.getBillcurr())
						&& isEQ(acblpf.getSacscode(), wsaaT5645Sacscode[34])
						&& isEQ(acblpf.getSacstyp(), wsaaT5645Sacstype[34])) {
					acblpfResult = acblpf;
					break;
				}
			}
		}
		// ILIFE-7911 ends
        if (acblpfResult != null) {
            if (isEQ(wsaaT5645Sign[34], "-")) {
                wsaaAcblSacscurbal.set(acblpfResult.getSacscurbal());
            } else {
                compute(wsaaAcblSacscurbal, 2).set(sub(0, acblpfResult.getSacscurbal()));
            }
        } else {
            wsaaAcblSacscurbal.set(0);
        }
        compute(wsaaDivRequired, 2).set(sub(wsaaNetCbillamt, wsaaSuspAvail));
        if (isLT(wsaaAcblSacscurbal, wsaaDivRequired)) {
            wsaaDivRequired.set(wsaaAcblSacscurbal);
        }
        wsaaSuspAvail.add(wsaaDivRequired);
    }

protected void apaAvail2900()
 {
        wsaaApaRequired.set(ZERO);
        if (isGT(wsaaNetCbillamt, wsaaSuspAvail)) {
            wsaaXPrmdepst.set(ZERO);
            if (isEQ(wsaaT5645Sacscode[37], SPACES) || isEQ(wsaaT5645Sacstype[37], SPACES)
                    || isEQ(wsaaT5645Sacscode[38], SPACES) || isEQ(wsaaT5645Sacstype[38], SPACES)) {
                syserrrec.statuz.set(g450);
                syserrrec.params.set("T5645");
                fatalError600();
            }
            readPrincipalApa2960();
            readInterestAccruel2965();
        }
    }

protected void readPrincipalApa2960()
 {
		List<Acblpf> acblpfList;
		String glChdrnum = linxpfResult.getChdrnum();
		acblpfList = acblGLMap.get(glChdrnum + "00");
		if (acblpfList == null) acblpfList = acblMap.get(glChdrnum);
		// ILIFE-7911 starts
		if(isnullflag) {
		if (null != acblpfList && !acblpfList.isEmpty()) {
			for (Acblpf a : acblpfList) {
				if (a.getSacscode().equals(wsaaT5645Sacscode[37]) && a.getSacscode().equals(wsaaT5645Sacstype[37])) {
					if (BigDecimal.ZERO.compareTo(a.getSacscurbal()) == 0) {
						continue;
					}
					/* Negate the Account balances if it is negative (for the */
					/* case of Cash accounts). */
					if (isLT(a.getSacscurbal(), 0)) {
						a.setSacscurbal(a.getSacscurbal().negate());
					}
					wsaaXPrmdepst.add(PackedDecimalData.parseObject(a.getSacscurbal()));
				}
			}
		}
		}
		else {
			if (null != acblpfList && !acblpfList.isEmpty()) {
			for (Acblpf a : acblpfList) {
				if (a.getSacscode().equals(wsaaT5645Sacscode[37]) && a.getSacscode().equals(wsaaT5645Sacstype[37])) {
					if (BigDecimal.ZERO.compareTo(a.getSacscurbal()) == 0) {
						continue;
					}
					/* Negate the Account balances if it is negative (for the */
					/* case of Cash accounts). */
					if (isLT(a.getSacscurbal(), 0)) {
						a.setSacscurbal(a.getSacscurbal().negate());
					}
					wsaaXPrmdepst.add(PackedDecimalData.parseObject(a.getSacscurbal()));
					}
				}
			}
		}
		
	}
	// ILIFE-7911 ends
	protected void readInterestAccruel2965() {
		List<Acblpf> acblpfList;
        String glChdrnum = linxpfResult.getChdrnum() ;
        acblpfList = acblGLMap.get(glChdrnum + "00");
        if (acblpfList  == null) acblpfList = acblMap.get(glChdrnum); 
		// ILIFE-7911 starts
		if (isnullflag) {
			if (null != acblpfList && !acblpfList.isEmpty()) {
				for (Acblpf a : acblpfList) {
					if (a.getSacscode().equals(wsaaT5645Sacscode[38])
							&& a.getSacscode().equals(wsaaT5645Sacstype[38])) {
						if (BigDecimal.ZERO.compareTo(a.getSacscurbal()) == 0) {
							continue;
						}
						/* Negate the Account balances if it is negative (for the */
						/* case of Cash accounts). */
						if (isLT(a.getSacscurbal(), 0)) {
							a.setSacscurbal(a.getSacscurbal().negate());
						}
						wsaaXPrmdepst.add(PackedDecimalData.parseObject(a.getSacscurbal()));
					}
				}
			}
		} else {
			if (null != acblpfList && !acblpfList.isEmpty()) {
				for (Acblpf a : acblpfList) {
					if (isEQ(a.getSacscode(),wsaaT5645Sacscode[38])
							&& isEQ(a.getSacscode(),wsaaT5645Sacstype[38])) {
						if (BigDecimal.ZERO.compareTo(a.getSacscurbal()) == 0) {
							continue;
						}
						/* Negate the Account balances if it is negative (for the */
						/* case of Cash accounts). */
						if (isLT(a.getSacscurbal(), 0)) {
							a.setSacscurbal(a.getSacscurbal().negate());
						}
						wsaaXPrmdepst.add(PackedDecimalData.parseObject(a.getSacscurbal()));
					}
				}
			}
		}
		// ILIFE-7911 ends
        compute(wsaaApaRequired, 2).set(sub(wsaaNetCbillamt, wsaaSuspAvail));
        if (isLT(wsaaXPrmdepst, wsaaApaRequired)) {
            wsaaApaRequired.set(wsaaXPrmdepst);
        }
        /* Sum the total suspense avaible to one variable. Makesure the */
        /* APA amount is book into suspense UPDATE-SUSPENSE in 3000-UPDATE */
        wsaaSuspAvail.add(wsaaApaRequired);
    }


protected void waiverHolding2900b()
 {
        Acblpf acblpfResult = null;
        List<Acblpf> acblpfList = acblMap.get(linxpfResult.getChdrnum());
		// ILIFE-7911 starts
		if (isnullflag) {
			if (null != acblpfList && !acblpfList.isEmpty()) {
				for (Acblpf acblpf : acblpfList) {
					if (acblpf.getOrigcurr().equals(linxpfResult.getBillcurr())
							&& isEQ(acblpf.getSacscode(), wsaaT5645Sacscode[7])
							&& isEQ(acblpf.getSacstyp(), wsaaT5645Sacstype[7])) {
						acblpfResult = acblpf;
						break;
					}
				}
			}
		} else {
			for (Acblpf acblpf : acblpfList) {
				if (acblpf.getOrigcurr().equals(linxpfResult.getBillcurr())
						&& isEQ(acblpf.getSacscode(), wsaaT5645Sacscode[7])
						&& isEQ(acblpf.getSacstyp(), wsaaT5645Sacstype[7])) {
					acblpfResult = acblpf;
					break;
				}
			}
		}
		// ILIFE-7911 ends
        if (acblpfResult != null) {
            wsaaWaiverAvail.set(acblpfResult.getSacscurbal());
        } else {
            wsaaWaiverAvail.set(0);
        }
    }

protected void update3000()
 {
	LOGGER.info("update3000 start");
		long start = 0;
		long finish = 0;
		long timeElapsed = 0;
		
		start = System.currentTimeMillis();
        softlockContract2100();
        boolean isAlreadyAddedSkippedEntity = false; //IBPTE-74
        boolean isSkipToNextRecord = false; //IBPTE-74
        if (isEQ(sftlockRecBean.getStatuz(), "LOCK")) {
        	conlogrec.error.set(e101);
    		//conlogrec.params.set(sftlockrec.sftlockRec);
    		callConlog003();
    		contot08++;
    		wsspEdterror.set(SPACES);
			// IBPTE-74 starts
    		// store skipped entity 
			if (isBatchSkipFeatureEnabled) {
				addSkippedEntity("CH", linxpfResult.getChdrnum());
				isAlreadyAddedSkippedEntity = true;
			}
			// IBPTE-74 ends
			isSkipToNextRecord = true;
        }
        
		// IBPTE-74 starts
        // If entity was skipped by previous batch due to soft lock
     	// and by the time this batch is run and entity was released from soft lock, 
     	// this batch should also skip this.
		if (!isSkipToNextRecord && isBatchSkipFeatureEnabled && isEntitySkippedBefore(linxpfResult.getChdrnum())) {
			wsspEdterror.set(SPACES);
			if (!isAlreadyAddedSkippedEntity) {
				addSkippedEntity("CH", linxpfResult.getChdrnum());
			}
			isSkipToNextRecord = true;
		}
		
		if(isSkipToNextRecord) {
			return;
		}
		// IBPTE-74 ends
		if(initFlag && isEQ(linxpfResult.getChdrPtdate(),ZERO)){
		updateChdr3101();
		writePtrn3600();
		updateLinsPayr3700();
		}
		else{
		finish = System.currentTimeMillis();
		timeElapsed = finish - start;
		LOGGER.debug("Softlock {} ms", timeElapsed);
        /* N.B CHDRPF MUST BE UPDATED HERE FOR THE COMMISSION SUBROUTINES */
        /* TO READ LATER. */
		start = System.currentTimeMillis();
        updateChdr3100();
		finish = System.currentTimeMillis();
		timeElapsed = finish - start;
		LOGGER.debug("updateChdr3100 {} ms", timeElapsed);
		
		start = System.currentTimeMillis();
        callPremiumCollect3200();
		finish = System.currentTimeMillis();
		timeElapsed = finish - start;
		LOGGER.debug("callPremiumCollect3200 {} ms", timeElapsed);
		
		start = System.currentTimeMillis();
        updateSuspense3300();
		finish = System.currentTimeMillis();
		timeElapsed = finish - start;
		LOGGER.debug("updateSuspense3300 {} ms", timeElapsed);
        
		start = System.currentTimeMillis();
        individualLedger3400();
		finish = System.currentTimeMillis();
		timeElapsed = finish - start;
		LOGGER.debug("individualLedger3400 {} ms", timeElapsed);
		
		start = System.currentTimeMillis();		
        covrsAcmvsLinsPtrn3500();
		finish = System.currentTimeMillis();
		timeElapsed = finish - start;
		LOGGER.debug("covrsAcmvsLinsPtrn3500 {} ms", timeElapsed);
		updateChdr3102();
		start = System.currentTimeMillis();
        a3000WriteLetter();
		finish = System.currentTimeMillis();
		timeElapsed = finish - start;
		LOGGER.debug("a3000WriteLetter {} ms", timeElapsed);
		
		start = System.currentTimeMillis();
        unlkChdr3800();
		finish = System.currentTimeMillis();
		timeElapsed = finish - start;
		LOGGER.debug("unlkChdr3800 {} ms", timeElapsed);
		
		start = System.currentTimeMillis();
        updateHdiv3900();
		finish = System.currentTimeMillis();
		timeElapsed = finish - start;
		LOGGER.debug("updateHdiv3900 {} ms", timeElapsed);  
		
		start = System.currentTimeMillis();
        riskPremflag = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), RISKPREM_FEATURE_ID, appVars, "IT");
		if(riskPremflag)
		{
			updateRiskPrem();
		}
		finish = System.currentTimeMillis();
		
		start = System.currentTimeMillis();
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", "BTPRO028", appVars, "IT");
		if(BTPRO028Permission)
		{
			Overpf overpf = overpfDAO.getOverpfRecord(linxpfResult.getChdrcoy(), linxpfResult.getChdrnum(), "1");
			if(null!= overpf && isEQ(overpf.getOverdueflag(),"O") && isEQ(overpf.getEnddate(),varcom.vrcmMaxDate ))
			{
				overpfDAO.updateOverpfRecord(bsscIO.getEffectiveDate().toInt(),overpf.getUnique_number());
			}
		}
		
		finish = System.currentTimeMillis();
		timeElapsed = finish - start;
		LOGGER.debug("updateRiskPrem {} ms", timeElapsed);
		LOGGER.info("update3000 end");
		}
    }
	protected void updateChdr3101() {
		if(chdrUpdateList==null){
            chdrUpdateList = new ArrayList<Chdrpf>();
        }
        Chdrpf chdrpf = new Chdrpf();
        chdrpf.setUniqueNumber(linxpfResult.getChdrUniqueNumber());
        if(trannoMap == null){
        	trannoMap = new HashMap<>();
        }
        int curTranno = 0;
        if(trannoMap.containsKey(linxpfResult.getChdrnum())){
        	curTranno = trannoMap.get(linxpfResult.getChdrnum());
        }else{
        	curTranno = linxpfResult.getChdrTranno();
        }
    	curTranno++;
    	trannoMap.put(linxpfResult.getChdrnum(), curTranno);
    	linxpfResult.setChdrTranno(curTranno);
        chdrpf.setTranno(linxpfResult.getChdrTranno());
        chdrUpdateList.add(chdrpf);
        chdrpfDAO.updateChdrTrannoByUniqueNo(chdrUpdateList);
        chdrUpdateList.clear();
	}
	
	protected void updateChdr3102() {
		if (outstamtIncr) {
			chdrpfObj.setOutstamt(wsaaOutstandAmt.getbigdata());
		} else {
//			chdrpfObj.setOutstamt(linxpfResult.getChdrOutstamt().subtract(linxpfResult.getInstamt06()));
			compute(chdrOutstamt, 2).set(sub(chdrOutstamt, linxpfResult.getInstamt06()));
			chdrpfObj.setOutstamt(chdrOutstamt.getbigdata());
		}
		if(linxpfResult.getProraterec()!=null && linxpfResult.getProraterec().equals("Y")) {
        	Payrpf existPayrpf = payrpfDAO.getPayrRecordByCoyAndNumAndSeq(linxpfResult.getChdrcoy().toString(), linxpfResult.getChdrnum().toString());
        	if (existPayrpf == null) {
        		syserrrec.statuz.set("MRNF");
        		syserrrec.params.set("2".concat(linxpfResult.getChdrnum().toString()));
        		fatalError600();
        	}
        	chdrpfObj.setSinstamt01(existPayrpf.getSinstamt01());
        	chdrpfObj.setSinstamt06(existPayrpf.getSinstamt01().add(existPayrpf.getSinstamt02()).add(existPayrpf.getSinstamt03()).add(existPayrpf.getSinstamt04()).add(existPayrpf.getSinstamt05()));
        } else {
        	chdrpfObj.setSinstamt01(linxpfResult.getInstamt01());
        	chdrpfObj.setSinstamt06(linxpfResult.getInstamt06());
        }

		chdrUpdateList.add(chdrpfObj);
		// IBPLIFE-2369 If the contract type is RUL, it will call the subroutine
		// to collect the premium which will inquery the chdrpf physical table
		chdrpfDAO.updateChdrpfAmt(chdrUpdateList,true);
		chdrUpdateList.clear();
	}

	protected void updateRiskPrem() {
		List<Rskppf> riskPremList = null;
		if(rskpMap != null && !rskpMap.isEmpty())
			riskPremList = rskpMap.get(linxpfResult.getChdrnum());
		readT5567();
		for(int i = 0; i < riskPremList.size(); i++) {
			BigDecimal tempRiskPrem = riskPremList.get(i).getRiskprem();
			BigDecimal tempPolFee = wsaapolFee.getbigdata();
			premiumrec.cnttype.set(riskPremList.get(i).getCnttype());
			premiumrec.crtable.set(riskPremList.get(i).getCrtable());
			premiumrec.calcTotPrem.set(riskPremList.get(i).getCovrinstprem());
			//premiumrec.calcTotPrem.set(riskPremList.get(i).getInstprem());
			callProgram("RISKPREMIUM", premiumrec.premiumRec);
					if(bsscIO.getEffectiveDate().toInt() >= riskPremList.get(i).getDatefrm() && bsscIO.getEffectiveDate().toInt() <= riskPremList.get(i).getDateto()) {
						riskPremList.get(i).setRiskprem(premiumrec.riskPrem.getbigdata().add(tempRiskPrem));
						riskPremList.get(i).setPolfee(riskPremList.get(i).getPolfee().add(tempPolFee));
						if (covrRiskPremMap.containsKey(riskPremList.get(i).getChdrnum())) {
							covrRiskPremMap.get(riskPremList.get(i).getChdrnum()).add(riskPremList.get(i));
			            } else {
			                List<Rskppf> rskList = new ArrayList<Rskppf>();
			                rskList.add(riskPremList.get(i));
			                covrRiskPremMap.put(riskPremList.get(i).getChdrnum(), rskList);
			            }
					}
					else if(bsscIO.getEffectiveDate().toInt() > riskPremList.get(i).getDateto()) {
						riskPremList.get(i).setRiskprem(premiumrec.riskPrem.getbigdata());
						riskPremList.get(i).setPolfee(tempPolFee);
						setRiskPremData(riskPremList.get(i));
					}
			}
	}

	protected void readT5567() {
		chdrpf = chdrpfDAO.getchdrRecordData(linxpfResult.getChdrcoy(),linxpfResult.getChdrnum());//IJTI-1485
		t5567Map = itemDao.loadSmartTable("IT", linxpfResult.getChdrcoy(), "T5567");//IJTI-1485
		wsaaCnttype.set(chdrpf.getCnttype());//IJTI-1485
		wsaaCntcurr.set(chdrpf.getCntcurr());
		if(t5567Map.containsKey(wsaaItemitem.toString())) {
			List<Itempf> t5567List = t5567Map.get(wsaaItemitem.toString());
	        for(Itempf t5567Item: t5567List) {
	             	t5567rec.t5567Rec.set(StringUtil.rawToString(t5567Item.getGenarea()));           
	        }
		}else {
			t5567rec.t5567Rec.set(SPACES);
		}
		
		for (wsaaSub2.set(1); !(isGT(wsaaSub2,10)); wsaaSub2.add(1)){
			if (isEQ(t5567rec.billfreq[wsaaSub2.toInt()],chdrpf.getBillfreq())) {
				wsaapolFee.set(t5567rec.cntfee[wsaaSub2.toInt()]);
				wsaaSub2.set(11);
			}else {
				wsaapolFee.set(ZERO);
			}
		}
		
	}
	
	protected void setRiskPremData(Rskppf rskppf) {
				rskppf.setEffDate(bsscIO.getEffectiveDate().toInt());
				rskppf = calculateDate(rskppf);
				rskppf.setChdrcoy(rskppf.getChdrcoy());
				rskppf.setChdrnum(rskppf.getChdrnum());
				rskppf.setCnttype(rskppf.getCnttype());
				rskppf.setLife(rskppf.getLife());
				rskppf.setCoverage(rskppf.getCoverage());
				rskppf.setRider(rskppf.getRider());
				rskppf.setCnttype(rskppf.getCnttype());
				rskppf.setCrtable(rskppf.getCrtable());
				rskppf.setInstprem(rskppf.getCovrinstprem());
				rskppf.setRiskprem(rskppf.getRiskprem());
				rskppf.setPolfee(rskppf.getPolfee());
				insertRiskPremList.add(rskppf);
	}
	
	protected Rskppf calculateDate(Rskppf rskppf) {
		Calendar proposalDate = Calendar.getInstance();
	    Calendar taxFromDate = Calendar.getInstance();
	    Calendar taxToDate = Calendar.getInstance();
		try {
			proposalDate.setTime(dateFormat.parse(String.valueOf(rskppf.getEffDate())));
		    int month=proposalDate.get(Calendar.MONTH)+1;
		    if(month>=7) {
		    	taxFromDate.set(proposalDate.get(Calendar.YEAR), 6, 01);
		    	taxToDate.set(proposalDate.get(Calendar.YEAR)+1, 5, 30);
		    }
		    else { 
		    	taxFromDate.set(proposalDate.get(Calendar.YEAR)-1, 6, 01);
		    	taxToDate.set(proposalDate.get(Calendar.YEAR), 5, 30);
		    }
		    rskppf.setDatefrm(Integer.parseInt(dateFormat.format(taxFromDate.getTime())));
		    rskppf.setDateto(Integer.parseInt(dateFormat.format(taxToDate.getTime())));
		    rskppf.setEffDate(bsscIO.getEffectiveDate().toInt());
		}
		catch (ParseException e) {		
			e.printStackTrace();
		}
		return rskppf;
	}

protected void updateChdr3100()
    {
        if(chdrUpdateList==null){
            chdrUpdateList = new ArrayList<Chdrpf>();
        }
        chdrpfObj = new Chdrpf();
        chdrpfObj.setUniqueNumber(linxpfResult.getChdrUniqueNumber());
        chdrpfObj.setInstjctl("");
        if(trannoMap == null){
        	trannoMap = new HashMap<>();
        }
        int curTranno = 0;
        if(trannoMap.containsKey(linxpfResult.getChdrnum())){
        	curTranno = trannoMap.get(linxpfResult.getChdrnum());
        }else{
        	curTranno = linxpfResult.getChdrTranno();
        }
    	curTranno++;
    	trannoMap.put(linxpfResult.getChdrnum(), curTranno);
    	linxpfResult.setChdrTranno(curTranno);
    	chdrpfObj.setTranno(linxpfResult.getChdrTranno());
    	chdrpfObj.setPtdate(linxpfResult.getLinsInstto());
    	chdrpfObj.setInstto(linxpfResult.getLinsInstto());
    	chdrpfObj.setInstfrom(linxpfResult.getInstfrom());
    	chdrpfObj.setInsttot01(linxpfResult.getChdrInsttot01().add(linxpfResult.getInstamt01()));
    	chdrpfObj.setInsttot02(linxpfResult.getChdrInsttot02().add(linxpfResult.getInstamt02()));
    	chdrpfObj.setInsttot03(linxpfResult.getChdrInsttot03().add(linxpfResult.getInstamt03()));
    	chdrpfObj.setInsttot04(linxpfResult.getChdrInsttot04().add(linxpfResult.getInstamt04()));
    	chdrpfObj.setInsttot05(linxpfResult.getChdrInsttot05().add(linxpfResult.getInstamt05()));
    	chdrpfObj.setInsttot06(linxpfResult.getChdrInsttot06().add(linxpfResult.getInstamt06()));
    	chdrpfObj.setInsttot01(chdrpfObj.getInsttot01().subtract(linxpfResult.getInstamt03()));        
       
    	chdrpfObj.setSinstamt06(linxpfResult.getInstamt01().add(linxpfResult.getInstamt02()).add(linxpfResult.getInstamt03()).add(linxpfResult.getInstamt04()).add(linxpfResult.getInstamt05()));
       // chdrpf.setSinstamt05(linxpfResult.getInstamt05().add(wsaaWaiverAvail.getbigdata()));        
    }

protected void updateLinsPayr3700()
 {
        Linspf l = new Linspf();
        l.setPayflag("P");
        l.setUnique_number(linxpfResult.getLinsUniqueNumber());
        if (linsUpdateList == null) {
            linsUpdateList = new ArrayList<Linspf>();
        }
        linsUpdateList.add(l);
        contot02++;
        if(prmhpf != null && bsscIO.getEffectiveDate().toInt()<=prmhpf.getTodate())//ILIFE-8509
        	temptot.set(linxpfResult.getProramt().add(linxpfResult.getProrcntfee()));
        else
        	temptot.set(linxpfResult.getInstamt06());
        contot03.add(temptot);

        Payrpf payrpf = new Payrpf();
        payrpf.setTranno(linxpfResult.getChdrTranno());//ILIFE-4688
        payrpf.setPtdate(linxpfResult.getLinsInstto());
        payrpf.setBtdate(linxpfResult.getPayrBtdate());
        if(outstamtIncr){
			payrpf.setOutstamt(wsaaOutstandAmt.getbigdata());
		}
        else{
//        	payrpf.setOutstamt(linxpfResult.getPayrOutstamt().subtract(linxpfResult.getInstamt06()));
        	compute(payrOutstamt, 2).set(sub(payrOutstamt, linxpfResult.getInstamt06()));
        	payrpf.setOutstamt(payrOutstamt.getbigdata());
        }
        payrpf.setPayrseqno(linxpfResult.getPayrseqno());
        payrpf.setUniqueNumber(linxpfResult.getPayrUniqueNumber());
        payrpf.setChdrcoy(linxpfResult.getChdrcoy());
        payrpf.setChdrnum(linxpfResult.getChdrnum());
        payrpf.setBillcd(linxpfResult.getPayrBillcd());
        payrpf.setNextdate(linxpfResult.getPayrNextDate());
        if(linxpfResult.getProraterec()!=null && linxpfResult.getProraterec().equals("Y")) {
        	Payrpf existPayrpf = payrpfDAO.getPayrRecordByCoyAndNumAndSeq(linxpfResult.getChdrcoy().toString(), linxpfResult.getChdrnum().toString());
        	if (existPayrpf == null) {
        		syserrrec.statuz.set("MRNF");
        		syserrrec.params.set("2".concat(linxpfResult.getChdrnum().toString()));
        		fatalError600();
        	}
        	payrpf.setSinstamt01(existPayrpf.getSinstamt01());
        	payrpf.setSinstamt06(existPayrpf.getSinstamt01().add(existPayrpf.getSinstamt02()).add(existPayrpf.getSinstamt03()).add(existPayrpf.getSinstamt04()).add(existPayrpf.getSinstamt05()));
        } else {
        	payrpf.setSinstamt01(linxpfResult.getInstamt01());
	        payrpf.setSinstamt06(linxpfResult.getInstamt06());
        }
        payrpf.setOrgbillcd(linxpfResult.getPayOrgbillcd());     
	 // payrpf.setSinstamt05(linxpfResult.getInstamt05().add(wsaaWaiverAvail.getbigdata()));   
        if (payrUpdateList == null) {
            payrUpdateList = new ArrayList<Payrpf>();
        }
        payrUpdateList.add(payrpf);
    }

	private void unlkChdr3800() {
		/*
		 * sftlockrec.sftlockRec.set(SPACES); 
		 * sftlockrec.enttyp.set("CH");
		 * sftlockrec.company.set(bsprIO.getCompany());
		 * sftlockrec.user.set(99999);
		 * sftlockrec.transaction.set(bprdIO.getAuthCode());
		 * sftlockrec.statuz.set(varcom.oK);
		 * sftlockrec.entity.set(linxpfResult.getChdrnum());
		 * sftlockrec.function.set("UNLK");
		 * callProgram(Sftlock.class, sftlockrec.sftlockRec);
		 * if (isNE(sftlockrec.statuz, varcom.oK)) {
		 * 	wsysSysparams.set(sftlockrec.sftlockRec);
		 * 	syserrrec.params.set(wsysSystemErrorParams);
		 * 	syserrrec.statuz.set(sftlockrec.statuz); fatalError600();
		 * }
		 */
		SftlockRecBean sftlockRecBean = new SftlockRecBean();
		sftlockRecBean.setFunction("UNLK");
		sftlockRecBean.setCompany(bsprIO.getCompany().toString().trim());
		sftlockRecBean.setEnttyp("CH");
		sftlockRecBean.setEntity(linxpfResult.getChdrnum().trim());
		sftlockRecBean.setTransaction(bprdIO.getAuthCode().toString());
		sftlockRecBean.setUser("99999");
		sftlockUtil.process(sftlockRecBean, appVars);
		if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())) {
			syserrrec.statuz.set(sftlockRecBean.getStatuz());
			fatalError600();
		}
	}


protected void updateHdiv3900()
 {
        /* Write a record in HDIV with negative amount to denote */
        /* cash dividend withdrawal if WSAA-DIV-REQUIRED > 0. */
        if (isEQ(wsaaDivRequired, 0)) {
            return;
        }

        Hdivpf hdivpf = new Hdivpf();
        hdivpf.setChdrcoy(linxpfResult.getChdrcoy());
        hdivpf.setChdrnum(linxpfResult.getChdrnum());
        hdivpf.setLife("01");
        hdivpf.setJlife("  ");
        hdivpf.setCoverage("01");
        hdivpf.setRider("00");
        hdivpf.setPlnsfx(0);
        hdivpf.setTranno(linxpfResult.getChdrTranno());
        hdivpf.setEffdate(bsscIO.getEffectiveDate().toInt());
        hdivpf.setHdvaldt(bsscIO.getEffectiveDate().toInt());
        hdivpf.setHincapdt(linxpfResult.getHdisHcapndt());
        hdivpf.setCntcurr(linxpfResult.getCntcurr());
        setPrecision(hdivpf.getHdvamt(), 2);
        hdivpf.setHdvamt(mult(wsaaDivRequired, -1).getbigdata());
        hdivpf.setHdvrate(BigDecimal.ZERO);
        hdivpf.setHdveffdt(varcom.vrcmMaxDate.toInt());
        hdivpf.setBatccoy(batcdorrec.company.toString());
        hdivpf.setBatcbrn(batcdorrec.branch.toString());
        hdivpf.setBatcactyr(batcdorrec.actyear.toInt());
        hdivpf.setBatcactmn(batcdorrec.actmonth.toInt());
        hdivpf.setBatctrcde(batcdorrec.trcde.toString());
        hdivpf.setBatcbatch(batcdorrec.batch.toString());
        hdivpf.setHdvtyp("C");
        hdivpf.setZdivopt(linxpfResult.getHcsdZdivopt());
        hdivpf.setZcshdivmth(linxpfResult.getHcsdZcshdivmth());
        hdivpf.setHdvopttx(0);
        hdivpf.setHdvcaptx(0);
        hdivpf.setHdvsmtno(0);
        hdivpf.setHpuanbr(0);
        if (hdivInsertList == null) {
            hdivInsertList = new ArrayList<Hdivpf>();
        }
        hdivInsertList.add(hdivpf);
    }


protected void callPremiumCollect3200() {
    wsaaBillchnl2.set(linxpfResult.getPayrBillchnl());
    wsaaCnttype2.set(linxpfResult.getChdrCnttype());
    /*  Look up the subroutine starting with the contract type then*/
    /*  if not found, use '***'.  If neither case is found on T6654,*/
    /*  then no record is written, and processing continues.*/
    ArraySearch as1 = ArraySearch.getInstance(wsaaT6654Rec);
    String T6654Key = linxpfResult.getPayrBillchnl().trim()+linxpfResult.getChdrCnttype().trim();
    as1.setIndices(wsaaT6654Ix);
    if(BTPRO028Permission) {
    as1.addSearchKey(wsaaT6654Key, T6654Key.trim().concat(linxpfResult.getPayrBillfreq()), true);	
    }
    else {
    as1.addSearchKey(wsaaT6654Key, wsaaT6654Key2, true);
    }
    if (!as1.binarySearch()) {
        /*  Use '***'*/
    	
        ArraySearch as3 = ArraySearch.getInstance(wsaaT6654Rec);
        as3.setIndices(wsaaT6654Ix);
        if(BTPRO028Permission) {
        	as3.addSearchKey(wsaaT6654Key, T6654Key.trim().concat("**"), true);
    	}
    	else {
    		wsaaCnttype2.set("***");
    		as3.addSearchKey(wsaaT6654Key, wsaaT6654Key2, true);
    	}
        if (!as3.binarySearch()) {
        	if(BTPRO028Permission) {
        		ArraySearch as4 = ArraySearch.getInstance(wsaaT6654Rec);
                as4.setIndices(wsaaT6654Ix);
                as4.addSearchKey(wsaaT6654Key, linxpfResult.getPayrBillchnl().trim().concat("*****"), true);
                if (!as4.binarySearch()) {
                	contot10++;
                    return ;
                }
        	}
        	else {
        		contot10++;
                return ;
        	}
            
        }
    }
    if (isEQ(wsaaT6654Collsub[wsaaT6654Ix.toInt()], SPACES)) {
        return ;
    }
    ddbtallrec.ddbtRec.set(SPACES);
    ddbtallrec.statuz.set(varcom.oK);
    ddbtallrec.payrcoy.set(wsaaPayrcoy);
    ddbtallrec.payrnum.set(wsaaPayrnum);
    ddbtallrec.mandref.set(linxpfResult.getPayrMandref());
    ddbtallrec.billamt.set(linxpfResult.getCbillamt());
    ddbtallrec.lastUsedDate.set(bsscIO.getEffectiveDate());
    callProgram(wsaaT6654Collsub[wsaaT6654Ix.toInt()], ddbtallrec.ddbtRec);
    if (isNE(ddbtallrec.statuz, varcom.oK)) {
        wsysSysparams.set(ddbtallrec.ddbtRec);
        syserrrec.params.set(wsysSystemErrorParams);
        syserrrec.statuz.set(ddbtallrec.statuz);
        fatalError600();
    }
}

protected void updateSuspense3300()
    {
        GotoLabel nextMethod = GotoLabel.DEFAULT;
        while (true) {
            try {
                switch (nextMethod) {
                case DEFAULT:
                    start3310();
                case advancePremDep3320:
                    advancePremDep3320();
                case exit3399:
                }
                break;
            }
            catch (GOTOException e){
                nextMethod = (GotoLabel) e.getNextMethod();
            }
        }
    }

protected void start3310()
    {
        if (isEQ(wsaaToleranceAllowed, 0)) {
        	if (isGTE(wsaaWaiveAmtPaid, wsaaNetCbillamt)) 
        		lifrtrnPojo.setOrigamt(BigDecimal.ZERO);
        	else
        		lifrtrnPojo.setOrigamt(new BigDecimal((sub(wsaaNetCbillamt,wsaaWaiveAmtPaid)).toString()));
        }
        else {
        	lifrtrnPojo.setOrigamt(new BigDecimal(wsaaAmtPaid.toString()));
        }
        if (lifrtrnPojo.getOrigamt().compareTo(BigDecimal.ZERO) == 0) {
            goTo(GotoLabel.exit3399);
        }
        lifrtrnPojo.setRdocnum(linxpfResult.getChdrnum());
        lifrtrnPojo.setTranref(linxpfResult.getChdrnum());
        lifrtrnPojo.setJrnseq(0);
        if(initFlag){
        	lifrtrnPojo.setContot(wsaaT5645Cnttot[39].toInt());
        	lifrtrnPojo.setSacscode(wsaaT5645Sacscode[39].toString());
        	lifrtrnPojo.setSacstyp(wsaaT5645Sacstype[39].toString());
        	lifrtrnPojo.setGlsign(wsaaT5645Sign[39].toString());
        	lifrtrnPojo.setGlcode(wsaaT5645Glmap[39].toString());
        }
        else{
        	lifrtrnPojo.setContot(wsaaT5645Cnttot[1].toInt());
        	lifrtrnPojo.setSacscode(wsaaT5645Sacscode[1].toString());
        	lifrtrnPojo.setSacstyp(wsaaT5645Sacstype[1].toString());
        	lifrtrnPojo.setGlsign(wsaaT5645Sign[1].toString());
        	lifrtrnPojo.setGlcode(wsaaT5645Glmap[1].toString());
        }
        lifrtrnPojo.setTranno(linxpfResult.getChdrTranno());
        lifrtrnPojo.setOrigcurr(linxpfResult.getBillcurr());
        lifrtrnPojo.setEffdate(linxpfResult.getInstfrom());
        if (lifrtrnPojo.getSubstituteCodes() != null) {
        	lifrtrnPojo.getSubstituteCodes()[0] = linxpfResult.getChdrCnttype();
        } else {
        	String[] substituteCodes = new String[5];
            substituteCodes[0] = linxpfResult.getChdrCnttype();
            lifrtrnPojo.setSubstituteCodes(substituteCodes);
        }
        
        lifrtrnPojo.setRldgacct(linxpfResult.getChdrnum());
        lifrtrnPojo.setFunction("PSTW");
		String terminalId = JobUtils.getTerminalId();
		appVars.setTerminalId(terminalId);        
        
        List<Acblpf> acblpfLtrnList = lifrtrnUtils.calcLitrtrnAcbl(lifrtrnPojo);
        if (!acblpfLtrnList.isEmpty()) 
        	acblpfLifrtrnList.addAll(acblpfLtrnList);
        if (isNE(Varcom.oK,lifrtrnPojo.getStatuz())) {
            wsysSysparams.set(lifrtrnPojo);
            syserrrec.params.set(wsysSystemErrorParams);
            syserrrec.statuz.set(lifrtrnPojo.getStatuz());
            fatalError600();
        }
        /* Pull Cash Dividend required.                                    */
        if (isEQ(wsaaDivRequired, 0)) {
            goTo(GotoLabel.advancePremDep3320);
        }
        lifrtrnPojo.setOrigamt(new BigDecimal(wsaaDivRequired.toString()));
        lifrtrnPojo.setRdocnum(linxpfResult.getChdrnum());
        lifrtrnPojo.setTranref(linxpfResult.getChdrnum());
        lifrtrnPojo.setJrnseq(0);
        if(initFlag){
        	lifrtrnPojo.setContot(wsaaT5645Cnttot[40].toInt());
        	lifrtrnPojo.setSacscode(wsaaT5645Sacscode[40].toString());
        	lifrtrnPojo.setSacstyp(wsaaT5645Sacstype[40].toString());
        	lifrtrnPojo.setGlsign(wsaaT5645Sign[40].toString());
        	lifrtrnPojo.setGlcode(wsaaT5645Glmap[40].toString());
        }
        else{
        	lifrtrnPojo.setContot(wsaaT5645Cnttot[35].toInt());
        	lifrtrnPojo.setSacscode(wsaaT5645Sacscode[35].toString());
        	lifrtrnPojo.setSacstyp(wsaaT5645Sacstype[35].toString());
        	lifrtrnPojo.setGlsign(wsaaT5645Sign[35].toString());
        	lifrtrnPojo.setGlcode(wsaaT5645Glmap[35].toString());
        }
        lifrtrnPojo.setTranno(linxpfResult.getChdrTranno());
        lifrtrnPojo.setOrigcurr(linxpfResult.getBillcurr());
        lifrtrnPojo.setEffdate(linxpfResult.getInstfrom());
//        lifrtrnPojo.setSubstituteCodes(substituteCodes);
        lifrtrnPojo.setRldgacct(linxpfResult.getChdrnum());
        lifrtrnPojo.setFunction("PSTW");
        appVars.setTerminalId(terminalId);
        List<Acblpf> acblpfLtrnList2 = lifrtrnUtils.calcLitrtrnAcbl(lifrtrnPojo);
        if (!acblpfLtrnList2.isEmpty()) 
        	acblpfLifrtrnList.addAll(acblpfLtrnList2);
        if (isNE(Varcom.oK,lifrtrnPojo.getStatuz())) {
            wsysSysparams.set(lifrtrnPojo);
            syserrrec.params.set(wsysSystemErrorParams);
            syserrrec.statuz.set(lifrtrnPojo.getStatuz());
            fatalError600();
        }
    }

protected void advancePremDep3320()
    {
        /* Pull Advance premium deposit required.                          */
        a300UpdateApa();
        if (isEQ(linxpfResult.getCntcurr(), linxpfResult.getBillcurr())) {
            return ;
        }
        /*  Do the following if the accounting currency and the billing*/
        /*  currency are different, starting with the accounting currency.*/
        if (isEQ(wsaaToleranceAllowed, 0)) {
            lifacmvrec.origamt.set(linxpfResult.getInstamt06());
        }
        else {
            lifacmvrec.origamt.set(wsaaCntcurrReceived);
        }
        lifacmvrec.rldgacct.set(linxpfResult.getChdrnum());
        lifacmvrec.tranref.set(linxpfResult.getChdrnum());
        lifacmvrec.effdate.set(linxpfResult.getInstfrom());
        lifacmvrec.substituteCode[6].set(SPACES);
        /* MOVE 0                        TO LIFA-JRNSEQ.          <005><*/
        if (isNE(lifacmvrec.origamt, 0)) {
            wsaaGlSub = 2;
            lifacmvrec.origcurr.set(linxpfResult.getCntcurr());
            callLifacmv6000();
        }
        /*  ... then the billing currency,*/
        if (isEQ(wsaaToleranceAllowed, 0)) {
            lifacmvrec.origamt.set(linxpfResult.getCbillamt());
        }
        else {
            lifacmvrec.origamt.set(wsaaAmtPaid);
        }
        if (isNE(lifacmvrec.origamt, 0)) {
            wsaaGlSub = 15;
            lifacmvrec.origcurr.set(linxpfResult.getBillcurr());
            callLifacmv6000();
        }
    }

protected void individualLedger3400()
 {

        wsaaToleranceAllowed.set(ZERO);
        /* Post the Premium-due. */
        lifacmvrec.effdate.set(linxpfResult.getInstfrom());
        lifacmvrec.tranref.set(linxpfResult.getChdrnum());
        lifacmvrec.rldgacct.set(linxpfResult.getChdrnum());
        /* MOVE 0 TO LIFA-JRNSEQ. */
        lifacmvrec.origcurr.set(linxpfResult.getPayrCntcurr());
        /* IF INSTAMT06 >= 0 */
        if (linxpfResult.getInstamt06().compareTo(BigDecimal.ZERO) > 0 && isEQ(wsaaT5688Revacc[wsaaT5688Ix], "Y")) {
            wsaaGlSub = 32;
            if(prmhpf != null && bsscIO.getEffectiveDate().toInt()<=prmhpf.getTodate())//ILIFE-8509
            	lifacmvrec.origamt.set(linxpfResult.getProramt().add(linxpfResult.getProrcntfee()));
            else
            	lifacmvrec.origamt.set(linxpfResult.getInstamt06());
            callLifacmv6000();
        }
        /* Post the Premium-Income. */
        if (linxpfResult.getInstamt01().compareTo(BigDecimal.ZERO) > 0
                && (isNE(wsaaT5688Comlvlacc[wsaaT5688Ix], "Y") && isNE(wsaaT5688Revacc[wsaaT5688Ix], "Y"))) {
        	if(prmhpf != null && bsscIO.getEffectiveDate().toInt()<=prmhpf.getTodate())//ILIFE-8509
        		lifacmvrec.origamt.set(linxpfResult.getProramt());
        	else
        		lifacmvrec.origamt.set(linxpfResult.getInstamt01());
            wsaaGlSub = 3;
            callLifacmv6000();
        }
        /* Post the Fees. */
	    if(prmhpf != null && bsscIO.getEffectiveDate().toInt()<=prmhpf.getTodate()) {//ILIFE-8509
	    	if (linxpfResult.getProrcntfee().compareTo(BigDecimal.ZERO) > 0 && isNE(wsaaT5688Revacc[wsaaT5688Ix], "Y")) {
	    		lifacmvrec.origamt.set(linxpfResult.getProrcntfee());
	    		wsaaGlSub = 4;
		    	callLifacmv6000();
	    	}
	    }
	    else if(linxpfResult.getInstamt02().compareTo(BigDecimal.ZERO) > 0 && isNE(wsaaT5688Revacc[wsaaT5688Ix], "Y")){
		    lifacmvrec.origamt.set(linxpfResult.getInstamt02());
		    wsaaGlSub = 4;
		    callLifacmv6000();
        }
        /* Post the Tolerance. */
        /* IF INSTAMT03 > 0 */
        /* MOVE INSTAMT03 TO LIFA-ORIGAMT */
        /* MOVE 5 TO WSAA-GL-SUB */
        /* PERFORM 6000-CALL-LIFACMV */
        /* END-IF. */
        if (isGT(linxpfResult.getInstamt03(), 0)) {
            lifacmvrec.origamt.set(linxpfResult.getInstamt03());
            if (toleranceLimit1.isTrue()) {
                wsaaGlSub = 5;
            }
            if (toleranceLimit2.isTrue()) {
                if (agtTerminated.isTrue()) {
                    wsaaGlSub = 5;
                } else {
                    wsaaGlSub = 36;
                    lifacmvrec.rldgacct.set(linxpfResult.getChdrAgntnum());
                }
            }
            callLifacmv6000();
            if (wsaaGlSub == 36) {
                /* Override Commission */
                if (isEQ(th605rec.indic, "Y")) {
                    zorlnkrec.annprem.set(lifacmvrec.origamt);
                    zorlnkrec.effdate.set(linxpfResult.getChdrOccdate());
                    b500CallZorcompy();
                }
                lifacmvrec.rldgacct.set(linxpfResult.getChdrnum());
            }
        }
        /* Post the Stamp-Duty. */
        if (linxpfResult.getInstamt04().compareTo(BigDecimal.ZERO) > 0 && isNE(wsaaT5688Revacc[wsaaT5688Ix], "Y")) {
            lifacmvrec.origamt.set(linxpfResult.getInstamt04());
            wsaaGlSub = 6;
            callLifacmv6000();
        }
        /* Post Dividend Suspense utilized */
        if (isGT(wsaaDivRequired, 0)) {
            lifacmvrec.origamt.set(wsaaDivRequired);
            wsaaGlSub = 34;
            callLifacmv6000();
        }
        /* Post the Waiver-holding. */
        if (linxpfResult.getInstamt05().compareTo(BigDecimal.ZERO) != 0 && isNE(wsaaT5688Revacc[wsaaT5688Ix], "Y")) {
            lifacmvrec.origamt.set(wsaaWaiveAmtPaid);
            wsaaGlSub = 7;
            callLifacmv6000();
        }
        if (isNE(prasrec.taxrelamt, 0)) {
            lifacmvrec.origcurr.set(linxpfResult.getBillcurr());
            lifacmvrec.origamt.set(prasrec.taxrelamt);
            /* MOVE PRAS-INREVNUM TO LIFA-RLDGACCT */
            lifacmvrec.rldgacct.set(linxpfResult.getChdrnum());
            wsaaGlSub = 19;
            callLifacmv6000();
        }
        /* Post the calculated taxes */
        wsaaTxLife.set(SPACES);
        wsaaTxCoverage.set(SPACES);
        wsaaTxRider.set(SPACES);
        /* MOVE SPACES TO WSAA-TX-PLANSFX <S19FIX> */
        wsaaTxPlansfx.set(ZERO);
        wsaaTxCrtable.set(SPACES);
        b700PostTax();

    }

protected void callLifacmv6000()
 {
        /* This is the only place LIFACMV parameters are referenced. LIFA */
        /* fields that are changeable are set outside this section within */
        /* a working storagwe variable. */
        lifacmvrec.rdocnum.set(linxpfResult.getChdrnum());
        lifacmvrec.tranno.set(linxpfResult.getChdrTranno());
        /* If the ACMV is being created for the COVR-INSTPREM, the */
        /* seventh table entries will be used. Move the appropriate */
        /* value for the COVR posting. */
		if(reinstflag && isShortTerm ){
            lifacmvrec.effdate.set(ptrnpfReinstate.getDatesub());
        }
        else{
        lifacmvrec.effdate.set(linxpfResult.getInstfrom());
        }
        lifacmvrec.termid.set(varcom.vrcmTermid);
        lifacmvrec.user.set(0);
        lifacmvrec.substituteCode[1].set(linxpfResult.getChdrCnttype());
        //PINNACLE-2005 START
        if(stampDutyflag) {
	        Clntpf clnt = clntpfDAO.findClientByClntnum(linxpfResult.getChdrCownnum());
	        if(null != clnt && null != clnt.getClntStateCd()) {
		        String stateCode = clnt.getClntStateCd().substring(3, 8).trim();
		        //verify item valid in TR2EN 
		        if(tr2enMap!=null && tr2enMap.containsKey(clnt.getClntStateCd().trim())) {
		        	lifacmvrec.substituteCode[4].set(stateCode);
		        }else { //outside australia
		        	lifacmvrec.substituteCode[4].set("01");
		        }
	        }
        }
        //PINNACLE-2005 END
        lifacmvrec.trandesc.set(descpfT1688.getLongdesc());
        lifacmvrec.jrnseq.add(1);
        lifacmvrec.sacscode.set(wsaaT5645Sacscode[wsaaGlSub]);
        lifacmvrec.sacstyp.set(wsaaT5645Sacstype[wsaaGlSub]);
        lifacmvrec.glsign.set(wsaaT5645Sign[wsaaGlSub]);
        lifacmvrec.glcode.set(wsaaT5645Glmap[wsaaGlSub]);
        lifacmvrec.contot.set(wsaaT5645Cnttot[wsaaGlSub]);
        /* Where the system parameter 3 on the process definition */
        /* is not spaces, LIFACMV will be called with the function */
        /* NPSTW instead of PSTW, so that the ACBL file will not */
        /* be updated by this program. */
        /* MOVE 'PSTW' TO LIFA-FUNCTION. */
        if (isEQ(bprdIO.getSystemParam03(), SPACES)) {
            lifacmvrec.function.set("PSTW");
        } else {
            if (isEQ(bprdIO.getSystemParam03(), wsaaT5645Sacscode[wsaaGlSub])) {
                lifacmvrec.function.set("NPSTW");
                deferredPostings6100();
            } else {
                lifacmvrec.function.set("PSTW");
            }
        }
        lifacmvrec.agnt.set("N");
		List<Acblpf> acblpfLst = new ArrayList<Acblpf>();
		callProgram(Lifacmv.class, new Object[] {lifacmvrec.lifacmvRec, acblpfLst});
		acblpfInsertAll.addAll(acblpfLst);
       
        if (isNE(lifacmvrec.statuz, varcom.oK)) {
            /* MOVE LIFR-LIFRTRN-REC TO WSYS-SYSPARAMS */
            wsysSysparams.set(lifacmvrec.lifacmvRec);
            syserrrec.params.set(wsysSystemErrorParams);
            syserrrec.statuz.set(lifacmvrec.statuz);
            fatalError600();
        }
        wsaaRldgacct.set(SPACES);
    }

protected void callLifacmv6000(Boolean agnt)
{
		if (agnt) {
		       /* This is the only place LIFACMV parameters are referenced. LIFA */
		       /* fields that are changeable are set outside this section within */
		       /* a working storagwe variable. */
		       lifacmvrec.rdocnum.set(linxpfResult.getChdrnum());
		       lifacmvrec.tranno.set(linxpfResult.getChdrTranno());
		       /* If the ACMV is being created for the COVR-INSTPREM, the */
		       /* seventh table entries will be used. Move the appropriate */
		       /* value for the COVR posting. */
				if(reinstflag && isShortTerm ){
		           lifacmvrec.effdate.set(ptrnpfReinstate.getDatesub());
		       }
		       else{
		       lifacmvrec.effdate.set(linxpfResult.getInstfrom());
		       }
		       lifacmvrec.termid.set(varcom.vrcmTermid);
		       lifacmvrec.user.set(0);
		       lifacmvrec.substituteCode[1].set(linxpfResult.getChdrCnttype());
		       lifacmvrec.trandesc.set(descpfT1688.getLongdesc());
		       lifacmvrec.jrnseq.add(1);
		       lifacmvrec.sacscode.set(wsaaT5645Sacscode[wsaaGlSub]);
		       lifacmvrec.sacstyp.set(wsaaT5645Sacstype[wsaaGlSub]);
		       lifacmvrec.glsign.set(wsaaT5645Sign[wsaaGlSub]);
		       lifacmvrec.glcode.set(wsaaT5645Glmap[wsaaGlSub]);
		       lifacmvrec.contot.set(wsaaT5645Cnttot[wsaaGlSub]);
		       /* Where the system parameter 3 on the process definition */
		       /* is not spaces, LIFACMV will be called with the function */
		       /* NPSTW instead of PSTW, so that the ACBL file will not */
		       /* be updated by this program. */
		       /* MOVE 'PSTW' TO LIFA-FUNCTION. */
		       if (isEQ(bprdIO.getSystemParam03(), SPACES)) {
		           lifacmvrec.function.set("PSTW");
		       } else {
		           if (isEQ(bprdIO.getSystemParam03(), wsaaT5645Sacscode[wsaaGlSub])) {
		               lifacmvrec.function.set("NPSTW");
		               deferredPostings6100();
		           } else {
		               lifacmvrec.function.set("PSTW");
		           }
		       }
		       lifacmvrec.agnt.set("Y");
		       List<Acblpf> acblpfLst = new ArrayList<Acblpf>();
		       List<Acblpf> acblpfUpdLst = new ArrayList<Acblpf>();
		       callProgram(Lifacmv.class, new Object[] {lifacmvrec.lifacmvRec, acblpfLst, acblpfUpdLst});
		       acblpfInsertList.addAll(acblpfLst);
		       acblpfUpdateList.addAll(acblpfUpdLst);
		       if (isNE(lifacmvrec.statuz, varcom.oK)) {
		           /* MOVE LIFR-LIFRTRN-REC TO WSYS-SYSPARAMS */
		           wsysSysparams.set(lifacmvrec.lifacmvRec);
		           syserrrec.params.set(wsysSystemErrorParams);
		           syserrrec.statuz.set(lifacmvrec.statuz);
		           fatalError600();
		       }
		       wsaaRldgacct.set(SPACES);			
		}

   }

protected void deferredPostings6100()
 {
        Acagpf acgapf = new Acagpf();
        acgapf.setRldgcoy(lifacmvrec.rldgcoy.toString());
        acgapf.setSacscode(lifacmvrec.sacscode.toString());
        acgapf.setRldgacct(lifacmvrec.rldgacct.toString());
        acgapf.setOrigcurr(lifacmvrec.origcurr.toString());
        acgapf.setSacstyp(lifacmvrec.sacstyp.toString());
        acgapf.setSacscurbal(lifacmvrec.origamt.getbigdata());
        acgapf.setRdocnum(lifacmvrec.rdocnum.toString());
        acgapf.setGlsign(lifacmvrec.glsign.toString());
        if (acagInsertList == null) {
            acagInsertList = new ArrayList<Acagpf>();
        }
        acagInsertList.add(acgapf);
    }

protected void markprorated(){
	
       	ptrnrecords = ptrnpfDAO.searchPtrnenqRecord(linxpfResult.getChdrcoy(), linxpfResult.getChdrnum());
       	if(ptrnrecords != null){
       		for(Ptrnpf ptrn : ptrnrecords ){
       		if(isNE(ptrn.getBatctrcde(),ta85) && isNE(ptrn.getBatctrcde(),"B522")){
       			continue;
       		}
       		if(isEQ(ptrn.getBatctrcde(),"B522")){
       			break;
       		}
       		if(isEQ(ptrn.getBatctrcde(),ta85)){
       			
       		readTd5j1(linxpfResult.getChdrCnttype());
     		if(isNE(td5j1rec.td5j1Rec,SPACES)){
     			if(isEQ(td5j1rec.subroutine,SPACES))
     				return;	
     			else{
     				isFoundPro = true;
     				ptrnpfReinstate  = ptrnpfDAO.getPtrnData(linxpfResult.getChdrcoy(), linxpfResult.getChdrnum(), ta85);
     				incrDatedMap = new HashMap<Integer, List<Incrpf>>();
       			    incrDatedMap = incrpfDAO.getIncrDatedMap(bsprIO.getCompany().toString().trim(), 
       			    		linxpfResult.getChdrnum());//IJTI-1485
     			}
     		}
       		}
       	}
    }
  
}

protected void checkTd5j2Term(String crtable){

	 readTd5j2(crtable);
	 if(isNE(td5j2rec.td5j2Rec,SPACES)){
		 if(isEQ(td5j2rec.shortTerm,"Y")){
			 isShortTerm = true;
		 }
	 }
}

protected void covrsAcmvsLinsPtrn3500()
 {
        if (isEQ(th605rec.bonusInd, "Y")) {
            /* Initialize the AGCM arrays */
            b100InitializeArrays();
        }
        wsaaAgcmIx.set(0);
        wsaaCovrIx.set(1);
        wsaaLastChdrnum.set(SPACES);
        wsaaLastLife.set(SPACES);
        wsaaLastCoverage.set(SPACES);
        wsaaLastRider.set(SPACES);
        wsaaLastPlanSuff.set(ZERO);
        isFirstCovr = true;
        /* Create a posting for every COVR-INSTPREMs for the contract. */
        if(stampDutyflag)
        	wsaaStampDutyAcc.set(0);
        if(BTPRO033Permission && linxpfResult.getProraterec()!=null && linxpfResult.getProraterec().equals("Y")) {
			Coprpf coprpf = new Coprpf();
			coprpf.setChdrcoy(linxpfResult.getChdrcoy());
			coprpf.setChdrnum(linxpfResult.getChdrnum());
			coprpf.setInstfrom(linxpfResult.getInstfrom());
			coprpf.setInstto(linxpfResult.getLinsInstto());
			coprpf.setValidflag("1");
			coprList = coprpfDAO.getCoprpfRecord(coprpf);
        }
        List<Covrpf> covrList = covrMap.get(linxpfResult.getChdrnum());
        	for (Covrpf covr : covrList) {
        		Covrpf covrpf = new Covrpf(covr);
        		if(BTPRO033Permission && linxpfResult.getProraterec()!=null && linxpfResult.getProraterec().equals("Y") && !coprList.isEmpty()) {
        			for(Coprpf copr: coprList) {
        				if(copr.getLife().equals(covrpf.getLife()) && copr.getCoverage().equals(covrpf.getCoverage()) && copr.getRider().equals(covrpf.getRider())) {
        					covrpf.setInstprem(copr.getInstprem());
        					covrpf.setZbinstprem(copr.getZbinstprem());
        					covrpf.setZlinstprem(copr.getZlinstprem());
        					covrpf.setZstpduty01(copr.getZstpduty01());
        					break;
        				}
        			}
        		}
                if (processCovrs3510(covrpf)) {
                    continue;
                } else {
                    break;
                }
            }
       if(stampDutyflag && isNE(wsaaStampDutyAcc,0) && !stampDutyFlagcomp)
    	   postStampDuty();
        /* VARYING WSAA-COVR-IX */
        /* FROM 1 */
        /* BY 1 */
        if (payrseqnoError) {
            /* Log the no. of payrseqno errors */
            contot10++;
            return;
        }
        /* MOVE 0 TO LIFA-JRNSEQ. */
        wsaaRldgacct.set(SPACES);
        List<Agcmpf> agcmList = agcmMap.get(linxpfResult.getChdrnum());
        wsaaCompayKept.set(ZERO);
        wsaaAgentKept.set(ZERO);
        for (Agcmpf a : agcmList) {
            if (isEQ(a.getPtdate(), ZERO)) {
                continue;
            }
            readAgcm3530(a);
        }
        writePtrn3600();
		
		updateLinsPayr3700();
		if(BTPRO033Permission) {
			updateCopr();
		}
		if(null !=prorateMap && !prorateMap.isEmpty())
			prorateMap.clear();
	}

protected void updateCopr() {
	if(!coprList.isEmpty()) {
		for(Coprpf c:coprList) {
			c.setValidflag("2");
			c.setTranno(linxpfResult.getChdrTranno());
			updateCoprList.add(c);
		}
		coprList.clear();
	}
}

protected void getRcvdpf(Covrpf covrpf) {
	boolean dialdownFlag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPRP012", appVars, "IT");
	boolean incomeProtectionflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP02", appVars, "IT");
	boolean premiumflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP03", appVars, "IT");
	if (incomeProtectionflag || premiumflag || dialdownFlag) {
		Rcvdpf rcvdPFObject = new Rcvdpf();
		rcvdPFObject.setChdrcoy(covrpf.getChdrcoy());
		rcvdPFObject.setChdrnum(covrpf.getChdrnum());
		rcvdPFObject.setLife(covrpf.getLife());
		rcvdPFObject.setCoverage(covrpf.getCoverage());
		rcvdPFObject.setRider(covrpf.getRider());
		rcvdPFObject.setCrtable(covrpf.getCrtable());
		rcvdPFObject = rcvdDAO.readRcvdpf(rcvdPFObject);
		premiumrec.bentrm.set(rcvdPFObject.getBentrm());
		if (rcvdPFObject.getPrmbasis() != null && isEQ("S", rcvdPFObject.getPrmbasis())) {
			premiumrec.prmbasis.set("Y");
		} else {
			premiumrec.prmbasis.set("");
		}
		premiumrec.poltyp.set(rcvdPFObject.getPoltyp());
		premiumrec.occpcode.set("");
		premiumrec.dialdownoption.set(rcvdPFObject.getDialdownoption());
		premiumrec.waitperiod.set(rcvdPFObject.getWaitperiod());
	}
}
protected void readTables(Covrpf c) {
	Itempf itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(linxpfResult.getChdrcoy());
	itempf.setItemtabl("T5687");
	itempf.setItemitem(c.getCrtable());
	itempf.setItmfrm(new BigDecimal(chdrpf.getOccdate()));
	itempf.setItmto(new BigDecimal(chdrpf.getOccdate()));
	List<Itempf> t5687ItemList = itempfDAO.findByItemDates(itempf);
	if (t5687ItemList.isEmpty()) {
		t5687rec.t5687Rec.set(SPACES);
		syserrrec.params.set("IT".concat(linxpfResult.getChdrcoy().toString()).concat("T5675")
				.concat(t5687rec.premmeth.toString()));
		fatalError600();
	} else {
		t5687rec.t5687Rec.set(StringUtil.rawToString(t5687ItemList.get(0).getGenarea()));
	}
	List<Itempf> t5675ItempfList = itempfDAO.getAllItemitem("IT", linxpfResult.getChdrcoy().toString(), "T5675",
			t5687rec.premmeth.toString());
	if (t5675ItempfList.isEmpty()) {
		t5675rec.t5675Rec.set(SPACES);
		syserrrec.params.set("IT".concat(linxpfResult.getChdrcoy().toString()).concat("T5675")
				.concat(t5687rec.premmeth.toString()));
		fatalError600();
	} else {
		t5675rec.t5675Rec.set(StringUtil.rawToString(t5675ItempfList.get(0).getGenarea()));
	}
}

protected void calculateAnb3260(Lifepf lifepf) {
	wsaaAnb.set(ZERO);
	AgecalcPojo agecalcPojo = new AgecalcPojo();
	AgecalcUtils agecalcUtils = getApplicationContext().getBean("agecalcUtils", AgecalcUtils.class);

	agecalcPojo.setFunction("CALCP");
	agecalcPojo.setLanguage(bsscIO.getLanguage().toString());
	agecalcPojo.setCnttype(chdrpf.getCnttype());
	agecalcPojo.setIntDate1(Integer.toString(lifepf.getCltdob()));
	agecalcPojo.setIntDate2(premiumrec.effectdt.toString());
	agecalcPojo.setCompany(bsprIO.getFsuco().toString());
	agecalcUtils.calcAge(agecalcPojo);
	if (isNE(agecalcPojo.getStatuz(), Varcom.oK)) {
		syserrrec.params.set(agecalcPojo.toString());
		syserrrec.statuz.set(agecalcPojo.getStatuz());
		fatalError600();
	}
	wsaaAnb.set(agecalcPojo.getAgerating());
}

protected void getAnny3280(Covrpf c) {
	Annypf annypf = annypfDAO.getAnnyRecordByAnnyKey(c.getChdrcoy(), c.getChdrnum(), c.getLife(), c.getCoverage(),
			c.getRider(), c.getPlanSuffix());
	if (annypf != null) {
		premiumrec.freqann.set(annypf.getFreqann());
		premiumrec.advance.set(annypf.getAdvance());
		premiumrec.arrears.set(annypf.getArrears());
		premiumrec.guarperd.set(annypf.getGuarperd());
		premiumrec.intanny.set(annypf.getIntanny());
		premiumrec.capcont.set(annypf.getCapcont());
		premiumrec.withprop.set(annypf.getWithprop());
		premiumrec.withoprop.set(annypf.getWithoprop());
		premiumrec.ppind.set(annypf.getPpind());
		premiumrec.nomlife.set(annypf.getNomlife());
		premiumrec.dthpercn.set(annypf.getDthpercn());
		premiumrec.dthperco.set(annypf.getDthperco());
	} else {
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
	}
}
protected void start5510ForBilltmpChg(Covrpf covr) {
	wsaaLastRerateDate.set(covr.getRerateDate());

	datcon2rec.datcon2Rec.set(SPACES);
	datcon2rec.intDate2.set(ZERO);
	datcon2rec.frequency.set(t5687rec.rtrnwfreq);
	compute(datcon2rec.freqFactor, 0).set(sub(ZERO, t5687rec.rtrnwfreq));

	while (!(isLTE(wsaaLastRerateDate, chdrpf.getPtdate()))) {
		datcon2rec.intDate1.set(wsaaLastRerateDate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		wsaaLastRerateDate.set(datcon2rec.intDate2);
	}

	if (isLT(wsaaLastRerateDate, covr.getCrrcd())) {
		wsaaLastRerateDate.set(covr.getCrrcd());
	}
}

protected void start5510(Covrpf covr) {
	payrpf = payrpfDAO.getPayrRecordByCoyAndNumAndSeq(linxpfResult.getChdrcoy().toString(),
			linxpfResult.getChdrnum().toString());

	if (payrpf == null) {
		syserrrec.statuz.set("MRNF");
		syserrrec.params.set("2".concat(linxpfResult.getChdrnum().toString()));
		fatalError600();
	}
	wsaaLastRerateDate.set(covr.getRerateDate());
	datcon4rec.billday.set(payrpf.getDuedd());
	datcon4rec.billmonth.set(payrpf.getDuemm());
	compute(datcon4rec.freqFactor, 0).set(sub(ZERO, t5687rec.rtrnwfreq));
	datcon4rec.frequency.set(t5687rec.rtrnwfreq);
	while (!(isLTE(wsaaLastRerateDate, chdrpf.getPtdate()))) {
		datcon4rec.intDate1.set(wsaaLastRerateDate);
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, Varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			fatalError600();
		}
		wsaaLastRerateDate.set(datcon4rec.intDate2);
	}

	if (isLT(wsaaLastRerateDate, covr.getCrrcd())) {
		wsaaLastRerateDate.set(covr.getCrrcd());
	}
}


protected void traceLastRerateDate5500(Covrpf covr) {
	billChgFlag = FeaConfg.isFeatureExist(covr.getChdrcoy().trim(), "CSBIL005", appVars, "IT");
	if (billChgFlag) {
		start5510ForBilltmpChg(covr);
	} else {
		start5510(covr);
	}
}

protected void getBillfrmdt() {
	datcon2rec.frequency.set(linxpfResult.getPayrBillfreq());
	datcon2rec.intDate1.set(linxpfResult.getInstfrom());
	datcon2rec.freqFactor.set(1);
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon3rec.datcon3Rec);
		fatalError600();
	}
}
protected void callPmexSubroutine(Covrpf covrpf) {
	
	
	chdrpf = chdrpfDAO.getchdrRecordData(linxpfResult.getChdrcoy(),linxpfResult.getChdrnum());
	boolean lnkgFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString(), "NBPRP055", appVars, "IT");
	premiumrec.premiumRec.set(SPACES);
	initialize(premiumrec.premiumRec);
	premiumrec.updateRequired.set("N");
	premiumrec.proratPremCalcFlag.set("N");
	if (isFirstCovr) {
		premiumrec.firstCovr.set("Y");
	} else {
		premiumrec.firstCovr.set("N");
		premiumrec.lifeLife.set(covrpf.getLife());
		premiumrec.covrCoverage.set(covrpf.getCoverage());
		premiumrec.covrRider.set(covrpf.getRider());
		callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		if (isNE(premiumrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		return;
	}
	premiumrec.riskPrem.set(ZERO);
	premiumrec.language.set(bsscIO.getLanguage());
	premiumrec.billfreq.set(linxpfResult.getPayrBillfreq());
	getBillfrmdt();
	premiumrec.bilfrmdt.set(datcon2rec.intDate2);
	premiumrec.biltodt.set(linxpfResult.getLinsInstto());
	readTables(covrpf);
	if (AppVars.getInstance().getAppConfig().isVpmsEnable()) {
		premiumrec.premMethod.set(t5687rec.premmeth);
	}
	if (premiumrec.premMethod.toString().trim().equals("PMEX")) {
		premiumrec.setPmexCall.set("Y");
		premiumrec.batcBatctrcde.set(bprdIO.getAuthCode());
		premiumrec.cownnum.set(chdrpf.getCownnum());
		premiumrec.occdate.set(chdrpf.getOccdate());
	}
	premiumrec.chdrChdrcoy.set(chdrpf.getChdrcoy());
	premiumrec.chdrChdrnum.set(chdrpf.getChdrnum());
	premiumrec.cnttype.set(chdrpf.getCnttype());
	premiumrec.effectdt.set(covrpf.getCrrcd());
	if (isNE(t5687rec.rtrnwfreq, ZERO)) {
		traceLastRerateDate5500(covrpf);
		premiumrec.effectdt.set(wsaaLastRerateDate);
		premiumrec.ratingdate.set(wsaaLastRerateDate);
		premiumrec.reRateDate.set(wsaaLastRerateDate);
	} else {
		premiumrec.ratingdate.set(covrpf.getCrrcd());
		premiumrec.reRateDate.set(covrpf.getCrrcd());
	}
	
	premiumrec.mop.set(chdrpf.getBillchnl());
	premiumrec.commissionPrem.set(ZERO);
	premiumrec.zstpduty01.set(ZERO);
	premiumrec.zstpduty02.set(ZERO);
	premiumrec.crtable.set(covrpf.getCrtable());
	premiumrec.lifeLife.set(covrpf.getLife());
	if (("PMEX".equals(t5675rec.premsubr.toString().trim())) && (lnkgFlag == true)) {
		if (null == covrpf.getLnkgsubrefno() || covrpf.getLnkgsubrefno().trim().isEmpty()) {
			premiumrec.lnkgSubRefNo.set(SPACE);
		} else {
			premiumrec.lnkgSubRefNo.set(covrpf.getLnkgsubrefno().trim());
		}
		if (null == covrpf.getLnkgno() || covrpf.getLnkgno().trim().isEmpty()) {
			premiumrec.linkcov.set(SPACE);
		} else {
			LinkageInfoService linkgService = new LinkageInfoService();
			FixedLengthStringData linkgCov = new FixedLengthStringData(
					linkgService.getLinkageInfo(covrpf.getLnkgno().trim()));
			premiumrec.linkcov.set(linkgCov);
		}
	}
	premiumrec.lifeJlife.set(covrpf.getJlife());
	premiumrec.covrCoverage.set(covrpf.getCoverage());
	premiumrec.covrRider.set(covrpf.getRider());
	premiumrec.termdate.set(covrpf.getPremCessDate());
	Lifepf lifepf = lifepfDAO.getLifeEnqRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "00");
	premiumrec.lsex.set(lifepf.getCltsex());
	Lifepf jlife = lifepfDAO.getLifelnbRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "01");
	if (isNE(t5687rec.rtrnwfreq, ZERO)) {
		calculateAnb3260(lifepf);
		premiumrec.lage.set(wsaaAnb);
		if (jlife != null) {
			calculateAnb3260(jlife);
			premiumrec.jlage.set(wsaaAnb);
			premiumrec.jlsex.set(jlife.getCltsex());
		} else {
			premiumrec.jlsex.set(SPACES);
			premiumrec.jlage.set(ZERO);
		}
	} else {
		premiumrec.lage.set(covrpf.getAnbAtCcd());
		if (jlife != null) {
			premiumrec.jlsex.set(jlife.getCltsex());
			premiumrec.jlage.set(jlife.getAnbAtCcd());
		} else {
			premiumrec.jlsex.set(SPACES);
			premiumrec.jlage.set(ZERO);
		}
	}
	Clntpf clntpf = clntpfDAO.searchClntRecord("CN", bsprIO.getFsuco().toString(), lifepf.getLifcnum());
	if (stampDutyflag) {
		if (clntpf.getClntStateCd() != null) {
			premiumrec.rstate01.set(clntpf.getClntStateCd().substring(3).trim());
		}
	}
	datcon3rec.intDate1.set(premiumrec.effectdt);
	datcon3rec.intDate2.set(premiumrec.termdate);
	datcon3rec.frequency.set("01");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, Varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		fatalError600();
	}
	datcon3rec.freqFactor.add(0.99999);
	premiumrec.duration.set(datcon3rec.freqFactor);
	datcon3rec.intDate1.set(premiumrec.effectdt);
	datcon3rec.intDate2.set(covrpf.getRiskCessDate());
	datcon3rec.frequency.set("01");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, Varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		fatalError600();
	}
	datcon3rec.freqFactor.add(0.99999);
	premiumrec.riskCessTerm.set(datcon3rec.freqFactor);
	premiumrec.currcode.set(covrpf.getPremCurrency());
	if (isEQ(covrpf.getPlanSuffix(), ZERO) && isNE(chdrpf.getPolinc(), 1)) {
		compute(premiumrec.sumin, 3).setRounded(div(covrpf.getSumins(), chdrpf.getPolsum()));
	} else {
		premiumrec.sumin.set(covrpf.getSumins());
	}
	compute(premiumrec.sumin, 3).setRounded(mult(premiumrec.sumin, chdrpf.getPolinc()));
	premiumrec.mortcls.set(covrpf.getMortcls());
	premiumrec.calcPrem.set(covrpf.getInstprem());
	premiumrec.calcBasPrem.set(covrpf.getZbinstprem());
	premiumrec.calcLoaPrem.set(covrpf.getZlinstprem());
	getAnny3280(covrpf);
	premiumrec.commTaxInd.set("Y");
	premiumrec.prevSumIns.set(ZERO);
	premiumrec.inputPrevPrem.set(ZERO);
	com.csc.smart400framework.dataaccess.dao.ClntpfDAO clntDao = DAOFactory.getClntpfDAO();
	com.csc.smart400framework.dataaccess.model.Clntpf clnt = clntDao.getClientByClntnum(lifepf.getLifcnum());
	if (null != clnt && null != clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()) {
		premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
	} else {
		premiumrec.stateAtIncep.set(clntpf.getClntStateCd());
	}
	premiumrec.validind.set("2");
	premiumrec.validflag.set("Y");
	getRcvdpf(covrpf);
	callProgram(t5675rec.premsubr, premiumrec.premiumRec);
	if (isNE(premiumrec.statuz, Varcom.oK)) {
		syserrrec.statuz.set(premiumrec.statuz);
		fatalError600();
	}
	
	
}


protected boolean processCovrs3510(Covrpf covrpf)
 {
	prorateprem.set("0");
	prorateSD.set("0");
	prorateTotalPrem.set("0");
	if (isNE(covrpf.getValidflag(), "1")) {
		return true;
	}
	BTPRO036Permission = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), BTPRO036, appVars, "IT");
	if(BTPRO036Permission) {
	deffDays = DateUtils.calDays(Integer.toString(linxpfResult.getInstfrom()),Integer.toString(linxpfResult.getLinsInstto()));
	if (isEQ(linxpfResult.getInstfreq(), freqcpy.fortnightly) && deffDays>14 && deffDays<17 && (linxpfResult.getProraterec()==null || isEQ(linxpfResult.getProraterec().trim(),""))) {
		callPmexSubroutine(covrpf);
		prorateprem.set(premiumrec.calcPrem);
		prorateSD.set(premiumrec.zstpduty01); 
		String key= covrpf.getChdrnum()+covrpf.getLife()+covrpf.getCoverage()+covrpf.getRider();
		compute(prorateTotalPrem,2).set(add(prorateprem,prorateSD));
		prorateMap.put(key, prorateTotalPrem.toString());
	}
	}
        payrseqnoError = false;
        /* If the Payrseqno is not equal to that of the LINS then no */
        /* further processing must be carried out on this LINS. */
        if (covrpf.getPayrseqno() != linxpfResult.getPayrseqno()) {
            payrseqnoError = true;
            return false;
        }
        /* If the working storage array has been exceeded, error. */
        if (isGT(wsaaCovrIx, wsaaCovrSize)) {
            syserrrec.statuz.set(h791);
            wsysSysparams.set("WSAA-COVR-REC");
            syserrrec.params.set(wsysSystemErrorParams);
            fatalError600();
        }
        if (isNE(covrpf.getValidflag(), "1")) {
    		return true;
    	}
        if(reinstflag && isFoundPro){
        	getIncr(covrpf);
        	isShortTerm = false;
			 checkTd5j2Term(covrpf.getCrtable());//IJTI-1485
			 if(isShortTerm){
				 if(isLT(linxpfResult.getLinsInstto(),ptrnpfReinstate.getDatesub())){
					 isShortTerm = false;
					 return true;
				 }
				 else{
					 calprpmrec.prem.set(ZERO);
					 calprpmrec.reinstDate.set(ptrnpfReinstate.getDatesub());
					 calprpmrec.lastPTDate.set(linxpfResult.getInstfrom());
					 calprpmrec.nextPTDate.set(linxpfResult.getLinsInstto());
					 calprpmrec.transcode.set(batcdorrec.trcde);
					 calprpmrec.uniqueno.set(covrpf.getUniqueNumber());
					 calprpmrec.prem.set(covrpf.getInstprem());
					 calprpmrec.chdrcoy.set(bsprIO.getCompany());
					 calprpmrec.chdrnum.set(linxpfResult.getChdrnum());
					 callProgram(td5j1rec.subroutine, calprpmrec.calprpmRec,null);
					 if (isNE(calprpmrec.statuz, varcom.oK)) {
							syserrrec.params.set(calprpmrec.statuz);
							fatalError600();
					 }	 
				 }
			 }		 
        }
        
        //readChdrpf = chdrpfDAO.getchdrRecord(linxpfResult.getChdrcoy(), linxpfResult.getChdrnum());	//ILIFE-7075
        /* We will only store the coverage info when it passed the */
        /* validation check. */
        /* Store the CRTABLES of the coverages ready for posting in */
        /* commission so we dont have to read the COVR again. Store them */
        /* in ascending sequence. */
        if((!(linxpfResult.getInstfrom() < covrpf.getCurrfrom() && linxpfResult.getPayrBillfreq().equals("12"))) 
        		&& (linxpfResult.getInstfrom() >= covrpf.getCurrto())){	//ILIFE-7075
        		return true;
        }
        if(!initFlag){
        boolean validCoverage = false;
        wsaaCovrValid[wsaaCovrIx.toInt()].set("N");
        for (wsaaT5679Sub = 1; wsaaT5679Sub <= 12; wsaaT5679Sub++) {
            if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub], covrpf.getStatcode())) {
                for (wsaaT5679Sub = 1; wsaaT5679Sub <= 12; wsaaT5679Sub++) {
                    if (isEQ(t5679rec.covPremStat[wsaaT5679Sub], covrpf.getPstatcode())) {
                        wsaaT5679Sub = 13;
                        validCoverage = true;
                        wsaaCovrValid[wsaaCovrIx.toInt()].set("Y");
                        break;
                    }
                }
            }
        }
        if (!validCoverage) {
            contot12++;
            return true;
        }
        }
        if (isEQ(covrpf.getChdrnum(), wsaaLastChdrnum) && isEQ(covrpf.getLife(), wsaaLastLife)
                && isEQ(covrpf.getCoverage(), wsaaLastCoverage) && isEQ(covrpf.getRider(), wsaaLastRider)
                && isEQ(covrpf.getPlanSuffix(), wsaaLastPlanSuff)) {
            return true;
        }
        /* Store the CRTABLES of the coverages ready for posting in */
        /* commission so we dont have to read the COVR again. Store them */
        /* in ascending sequence. */
        wsaaCovrChdrnum[wsaaCovrIx.toInt()].set(covrpf.getChdrnum());
        wsaaLastChdrnum.set(covrpf.getChdrnum());
        wsaaCovrLife[wsaaCovrIx.toInt()].set(covrpf.getLife());
        wsaaLastLife.set(covrpf.getLife());
        wsaaCovrCovrg[wsaaCovrIx.toInt()].set(covrpf.getCoverage());
        wsaaLastCoverage.set(covrpf.getCoverage());
        wsaaCovrRider[wsaaCovrIx.toInt()].set(covrpf.getRider());
        wsaaLastRider.set(covrpf.getRider());
        wsaaCovrPlanSuff[wsaaCovrIx.toInt()].set(covrpf.getPlanSuffix());
        wsaaLastPlanSuff.set(covrpf.getPlanSuffix());
        wsaaCovrCrtable[wsaaCovrIx.toInt()].set(covrpf.getCrtable());
        if(initFlag){
       	 wsaaCovrValid[wsaaCovrIx.toInt()].set("Y");
       }
       getIncrpfData(covrpf);
       if(!outstamtIncr)
    	   getRertRecord(covrpf);
        if(null != rertpf){
        	Covrpf updateCovrpf = new Covrpf(covrpf);
        	updateCovrpf.setValidflag("2");
        	updateCovrpf.setCurrto(bsscIO.getEffectiveDate().toInt());
        	if (covrUpdateList == null) {
        		covrUpdateList = new ArrayList<Covrpf>();
        	}
        	covrUpdateList.add(updateCovrpf);
        	covrpf.setValidflag("1");
        	covrpf.setCurrfrom(bsscIO.getEffectiveDate().toInt());
        	covrpf.setTranno(linxpfResult.getChdrTranno());
        	
        	covrpf.setInstprem(add(sub(rertpf.getNewinst(),rertpf.getLastInst()),covrpf.getInstprem()).getbigdata());
        	covrpf.setZbinstprem(add(sub(rertpf.getZbnewinst(),rertpf.getZblastinst()),covrpf.getZbinstprem()).getbigdata());
        	covrpf.setZlinstprem(add(sub(rertpf.getZlnewinst(),rertpf.getZllastinst()),covrpf.getZlinstprem()).getbigdata());
        	covrpf.setZstpduty01(add(sub(rertpf.getNewzstpduty(),rertpf.getLastzstpduty()),covrpf.getZstpduty01()).getbigdata());
        	covrpf.setCommprem(rertpf.getCommprem());
        
        	covrInsertList.add(covrpf);
        	rertpf.setValidflag("2");
        	rertpf.setTranno(linxpfResult.getChdrTranno());
        	rertUpdateList.add(rertpf);
        	List<Agcmpf> tempList = agcmMap.get(covrpf.getChdrnum());
        	List<Agcmpf> list = new ArrayList<Agcmpf>();
        	Agcmpf insertAgcmpf;
        	for(Agcmpf agcmpf : tempList) {
        		if(agcmpf.getChdrcoy().equals(covrpf.getChdrcoy()) && 
        		   agcmpf.getChdrnum().equals(covrpf.getChdrnum()) &&
        		   agcmpf.getLife().equals(covrpf.getLife()) && 
        		   agcmpf.getCoverage().equals(covrpf.getCoverage()) && 
        		   agcmpf.getRider().equals(covrpf.getRider()) && 
        		   agcmpf.getPlanSuffix()== covrpf.getPlanSuffix()) {
        			        		
        		agcmpf.setValidflag("2");
        		agcmpf.setCurrto(bsscIO.getEffectiveDate().toInt());
        		agcmpfInvUpdateList.add(agcmpf); 
        		insertAgcmpf = new Agcmpf(agcmpf);
        		insertAgcmpf.setValidflag("1");
        		insertAgcmpf.setCurrfrom(bsscIO.getEffectiveDate().toInt());
        		insertAgcmpf.setCurrto(99999999);
        		insertAgcmpf.setAnnprem(agentAnnprem3730(covrpf, agcmpf));
        		insertAgcmpf.setTranno(linxpfResult.getChdrTranno());
        		list.add(insertAgcmpf);   
        	}else {
        		list.add(agcmpf);
        	}
         
        	}	
        	agcmMap.replace(covrpf.getChdrnum(), list);
     
    		
        }

        if(prmhpf != null && bsscIO.getEffectiveDate().toInt()<=prmhpf.getTodate())//ILIFE-8509
        	wsaaCovrProratePrem[wsaaCovrIx.toInt()].set(covrpf.getProrateprem());
        /* For VALID coverages perform component level accounting if */
        /* necessary and if the COVRLNB-INSTPREM not = 0 */
        if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix], "Y") && isNE(wsaaT5688Revacc[wsaaT5688Ix], "Y")
                && isNE(covrpf.getInstprem(), 0)) {
            wsaaGlSub = 22;
            wsaaRldgChdrnum.set(covrpf.getChdrnum());
            wsaaRldgLife.set(covrpf.getLife());
            wsaaRldgCoverage.set(covrpf.getCoverage());
            wsaaRldgRider.set(covrpf.getRider());
            wsaaPlan.set(covrpf.getPlanSuffix());
            wsaaRldgPlanSuff.set(wsaaPlansuff);
            lifacmvrec.rldgacct.set(wsaaRldgacct);
            /* MOVE 0 TO LIFA-JRNSEQ */
			 if(reinstflag && isShortTerm ){
				lifacmvrec.origamt.set(calprpmrec.prem);
            }
            else{
            	if(isFoundPro && incrFoundPro){
				lifacmvrec.origamt.set(currIncrPro.getNewinst());
            	}
            	else{
            	//lifacmvrec.origamt.set(covrpf.getInstprem());
            	compute(lifacmvrec.origamt, 2).set(add(covrpf.getInstprem(), prorateTotalPrem));
            	}
            }
			if(prmhpf != null && bsscIO.getEffectiveDate().toInt()<=prmhpf.getTodate())//ILIFE-8509
				lifacmvrec.origamt.set(covrpf.getProrateprem() != null ? covrpf.getProrateprem() : BigDecimal.ZERO);
            lifacmvrec.origcurr.set(linxpfResult.getChdrCntcurr());
            
            lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
            lifacmvrec.effdate.set(linxpfResult.getInstfrom());
            callLifacmv6000();
        }
        if(stampDutyflag) { //here
        	compute(wsaaStampDutyAcc, 2).set(add(wsaaStampDutyAcc,add(covrpf.getZstpduty01(),prorateSD)));	
        }
        if(stampDutyFlagcomp && stampDutyflag && isNE(wsaaStampDutyAcc,0)) {
        	wsaaRldgChdrnum.set(covrpf.getChdrnum());
            wsaaRldgLife.set(covrpf.getLife());
            wsaaRldgCoverage.set(covrpf.getCoverage());
            wsaaRldgRider.set(covrpf.getRider());
            wsaaPlan.set(covrpf.getPlanSuffix());
            wsaaRldgPlanSuff.set(wsaaPlansuff);
    		lifacmvrec.origamt.set(add(covrpf.getZstpduty01(),prorateSD));
        	postStampDutyComponent();
        }
        
        /* Bonus Workbench * */
        if (isNE(th605rec.bonusInd, "Y")) {
            next3515(covrpf);
            return true;
        }
        wsaaT5687Ix.set(1);
        searchlabel1: {
            for (; isLT(wsaaT5687Ix, wsaaT5687Rec.length); wsaaT5687Ix.add(1)) {
                if (isEQ(wsaaT5687Key[wsaaT5687Ix.toInt()], covrpf.getCrtable())) {
                    break searchlabel1;
                }
            }
            syserrrec.statuz.set(E652);
            StringUtil stringVariable1 = new StringUtil();
            stringVariable1.addExpression("t5687");
            stringVariable1.addExpression(wsysSystemErrorParams);
            stringVariable1.setStringInto(syserrrec.params);
            fatalError600();
        }
        wsaaDateFound = false;
        while (!(isNE(covrpf.getCrtable(), wsaaT5687Crtable[wsaaT5687Ix.toInt()]) || isGT(wsaaT5687Ix, wsaaT5687Size))) {
            if (isGTE(covrpf.getCrrcd(), wsaaT5687Itmfrm[wsaaT5687Ix.toInt()])) {
                wsaaDateFound = true;
                break;
            } else {
                wsaaT5687Ix.add(1);
            }
        }

        if (!wsaaDateFound) {
            syserrrec.statuz.set(H255);
            syserrrec.params.set(wsysSystemErrorParams);
            fatalError600();
        }
        if (covrpf.getInstprem().compareTo(BigDecimal.ZERO) != 0) {
            wsaaBillfq.set(linxpfResult.getPayrBillfreq());
        } else {
            if (isNE(wsaaT5687Bbmeth[wsaaT5687Ix.toInt()], SPACES)) {
                wsaaT5534Ix.set(1);
                searchlabel2: {
                    for (; isLT(wsaaT5534Ix, wsaaT5534Rec.length); wsaaT5534Ix.add(1)) {
                        if (isEQ(wsaaT5534Key[wsaaT5534Ix.toInt()], wsaaT5687Bbmeth[wsaaT5687Ix.toInt()])) {
                            break searchlabel2;
                        }
                    }
                    StringUtil stringVariable2 = new StringUtil();
                    stringVariable2.addExpression("t5534");
                    stringVariable2.addExpression(wsaaT5687Ix);
                    stringVariable2.setStringInto(wsysSysparams);
                    syserrrec.params.set(wsysSystemErrorParams);
                    fatalError600();
                }
                wsaaBillfq.set(wsaaT5534UnitFreq[wsaaT5534Ix.toInt()]);
                if (isNE(wsaaBillfq9, NUMERIC)) {
                    StringUtil stringVariable3 = new StringUtil();
                    stringVariable3.addExpression("t5534");
                    stringVariable3.addExpression(wsaaT5687Ix);
                    stringVariable3.setStringInto(wsysSysparams);
                    syserrrec.params.set(wsysSystemErrorParams);
                    fatalError600();
                } else {
                    if (isEQ(wsaaBillfq9, ZERO)) {
                        wsaaBillfq.set("01");
                    }
                }
            } else {
                wsaaBillfq.set(linxpfResult.getPayrBillfreq());
            }
        }
        if (covrpf.getInstprem().compareTo(BigDecimal.ZERO) != 0)  {
            wsaaAgcmIx.add(1);
            wsaaAgcmChdrcoy[wsaaAgcmIx.toInt()].set(covrpf.getChdrcoy());
            wsaaAgcmChdrnum[wsaaAgcmIx.toInt()].set(covrpf.getChdrnum());
            wsaaAgcmLife[wsaaAgcmIx.toInt()].set(covrpf.getLife());
            wsaaAgcmCoverage[wsaaAgcmIx.toInt()].set(covrpf.getCoverage());
            wsaaAgcmRider[wsaaAgcmIx.toInt()].set(covrpf.getRider());
            b200PremiumHistory(covrpf);
            b300WriteArrays(covrpf);
        } else {
            wsaaAgcmIx.add(1);
            wsaaAgcmChdrcoy[wsaaAgcmIx.toInt()].set(covrpf.getChdrcoy());
            wsaaAgcmChdrnum[wsaaAgcmIx.toInt()].set(covrpf.getChdrnum());
            wsaaAgcmLife[wsaaAgcmIx.toInt()].set(covrpf.getLife());
            wsaaAgcmCoverage[wsaaAgcmIx.toInt()].set(covrpf.getCoverage());
            wsaaAgcmRider[wsaaAgcmIx.toInt()].set(covrpf.getRider());
            b200PremiumHistory(covrpf);
        }
        next3515(covrpf);
        isFirstCovr = false;
        return true;
    }
protected BigDecimal agentAnnprem3730(Covrpf covrpf, Agcmpf agcmpf)
{
    wsaaBillfreq.set(linxpfResult.getPayrBillfreq());

	wsaaAgntnum.set(agcmpf.getAgntnum());
	if (isEQ(agcmpf.getOvrdcat(),"O")) {
		wsaaAgntFound.set("N");
		wsaaCedagent.set(agcmpf.getCedagent());
		while ( !(agentFound.isTrue())) {
			findBasicAgent3740(agcmpf);
		}

	}
	List<Pcddpf> pcddpfList = pcddpfMap.get(covrpf.getChdrnum());
	boolean isPcddFound =false;
	for (Pcddpf pcddpf : pcddpfList) {
		if (pcddpf.getAgntNum().equals(wsaaAgntnum.toString())) {
			isPcddFound =true;
			wsaaAnnprem.set(ZERO);
			wsaaAnnprem.set(agcmpf.getAnnprem());
			if (isNE(t5687rec.zrrcombas, SPACES)) {
				setPrecision(wsaaAnnprem, 3);
				wsaaAnnprem.setRounded(
						mult((mult(covrpf.getZbinstprem(), wsaaBillfreqNum)), (div(pcddpf.getSplitC(), 100))));
			} else {
				setPrecision(wsaaAnnprem, 3);
				wsaaAnnprem.setRounded(
						mult((mult(covrpf.getInstprem(), wsaaBillfreqNum)), (div(pcddpf.getSplitC(), 100))));
			}
			/* MOVE AGCMBCH-ANNPREM TO ZRDP-AMOUNT-IN. */
			/* PERFORM 6000-CALL-ROUNDING. */
			/* MOVE ZRDP-AMOUNT-OUT TO AGCMBCH-ANNPREM. */
			if (isNE(wsaaAnnprem, 0)) {
				zrdecplcPojo.setAmountIn(wsaaAnnprem.getbigdata());
				c000CallRounding(zrdecplcPojo);
				wsaaAnnprem.set(zrdecplcPojo.getAmountIn());
			}
			return wsaaAnnprem.getbigdata();
		}
	}
	if(!isPcddFound){
		syserrrec.params.set("PCDDPF AGNT" + wsaaAgntnum.toString());
		syserrrec.statuz.set(Varcom.mrnf);
		fatalError600();
	}
	if (pcddpfList == null || pcddpfList.size() == 0) {
		syserrrec.params.set("PCDDPF");
		syserrrec.statuz.set(Varcom.mrnf);
		fatalError600();
	}
	return null;
}

protected void findBasicAgent3740(Agcmpf agcmpf)
{
agcmpf.setChdrcoy(agcmpf.getChdrcoy());
agcmpf.setChdrnum(agcmpf.getChdrnum());
agcmpf.setLife(agcmpf.getLife());
agcmpf.setCoverage(agcmpf.getCoverage());
agcmpf.setRider(agcmpf.getRider());
agcmpf.setPlanSuffix(agcmpf.getPlanSuffix());
agcmpf.setAgntnum(wsaaCedagent.toString());
Agcmpf agcm = agcmpfDAO.searchAgcmRecord(agcmpf);
if (agcm == null) {
	fatalError600();
}
if (isEQ(agcm.getOvrdcat(),"B")) {
	wsaaAgntFound.set("Y");
	wsaaAgntnum.set(agcm.getAgntnum());
}
else {
	wsaaCedagent.set(agcm.getCedagent());
}
}


	protected void postStampDuty() {		    
		lifacmvrec.rldgacct.set(linxpfResult.getChdrnum());
		lifacmvrec.origamt.set(wsaaStampDutyAcc);
		wsaaGlSub = 6;
		callLifacmv6000();
		wsaaGlSub = 42;
		callLifacmv6000();
	}
	
	protected void postStampDutyComponent() {
		
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		wsaaGlSub = 6;
		callLifacmv6000();
		lifacmvrec.rldgacct.set(linxpfResult.getChdrnum());
		wsaaGlSub = 42;
		callLifacmv6000();	
		acblpfInsertAll = adjustSacsbalforDuplicates(acblpfInsertAll); //PINNACLE-2450
		
	}
	
protected void getRertRecord(Covrpf covrpf){
	rertpf = null;
	if (rertpfMap != null && rertpfMap.containsKey(linxpfResult.getChdrnum())) {
		List<Rertpf> rertpfList = rertpfMap.get(linxpfResult.getChdrnum());
		for (Iterator<Rertpf> iterator = rertpfList.iterator(); iterator.hasNext();) {
			Rertpf rert = iterator.next();
			if (isEQ(rert.getEffdate(), linxpfResult.getInstfrom()) && rert.getLife().equals(covrpf.getLife())
					&& rert.getCoverage().equals(covrpf.getCoverage()) && rert.getRider().equals(covrpf.getRider())
					&& Integer.compare(rert.getPlnsfx(), covrpf.getPlanSuffix()) == 0 && rert.getValidflag().equals("1") ) {
				rertpf = rert;		
				break;
			}
			/*if (isLTE(rert.getEffdate(), linxpfResult.getLinsInstto()) && rert.getLife().equals(covrpf.getLife())
					&& rert.getCoverage().equals(covrpf.getCoverage()) && rert.getRider().equals(covrpf.getRider())
					&& Integer.compare(rert.getPlnsfx(), covrpf.getPlanSuffix()) == 0 && rert.getValidflag().equals("1") ) {
				outstamtIncr = true;
				wsaaOutstandAmt.add(rert.getNewinst().doubleValue());
			}*/
		}
	}
}
	
protected void getIncr(Covrpf covrpf){
	 
	incrFoundPro = false;
	int date = linxpfResult.getInstfrom();
	if (incrDatedMap != null && incrDatedMap.containsKey(date)) {
		List<Incrpf> incrpfList = incrDatedMap.get(date);
		for (Iterator<Incrpf> iterator = incrpfList.iterator(); iterator.hasNext(); ){
			Incrpf incr = iterator.next();
			if (incr.getChdrnum().equals(covrpf.getChdrnum())
			&&	incr.getLife().equals(covrpf.getLife())
    		&&  incr.getCoverage().equals(covrpf.getCoverage())
    		&& 	incr.getRider().equals(covrpf.getRider())
    		&& Integer.compare(incr.getPlnsfx(),covrpf.getPlanSuffix()) == 0) {
					currIncrPro = incr;
					incrFoundPro = true;
					break;		
			 }
		}
	  }
}

protected void getIncrpfData(Covrpf covrpf){
	 
	outstamtIncr = false;
	if (incrpfMap != null && incrpfMap.containsKey(linxpfResult.getChdrnum())) {
		List<Incrpf> incrpfList = incrpfMap.get(linxpfResult.getChdrnum());
		for (Iterator<Incrpf> iterator = incrpfList.iterator(); iterator.hasNext(); ){
			Incrpf incr = iterator.next();
			if (isLT(incr.getCrrcd(), linxpfResult.getLinsInstto())
			&&	incr.getLife().equals(covrpf.getLife())
    		&&  incr.getCoverage().equals(covrpf.getCoverage())
    		&& 	incr.getRider().equals(covrpf.getRider())
    		&& Integer.compare(incr.getPlnsfx(),covrpf.getPlanSuffix()) == 0) {
					outstamtIncr = true;
					wsaaOutstandAmt.add(incr.getNewinst().doubleValue());
					break;		
			 }
		}
	  }
}

protected void next3515(Covrpf covrpf)
    {
        /*  Post the calculated taxes                                      */
        wsaaTxLife.set(covrpf.getLife());
        wsaaTxCoverage.set(covrpf.getCoverage());
        wsaaTxRider.set(covrpf.getRider());
        wsaaTxPlansfx.set(covrpf.getPlanSuffix());
        wsaaTxCrtable.set(covrpf.getCrtable());
        b700PostTax();
        genericProcessing5000(covrpf);
        wsaaCovrIx.add(1);
    }

protected void genericProcessing5000(Covrpf covrpf)
 {
        wsaaAuthCode.set(bprdIO.getAuthCode());
        wsaaCovrlnbCrtable.set(covrpf.getCrtable());
        ArraySearch as1 = ArraySearch.getInstance(wsaaT5671Rec);
        as1.setIndices(wsaaT5671Ix);
        as1.addSearchKey(wsaaT5671Key, wsaaTrcdeCrtable, true);
        if (!as1.binarySearch()) {
            return;
        }
        for (wsaaSubprogIx.set(1); !(isGT(wsaaSubprogIx, 4)); wsaaSubprogIx.add(1)) {
            if (isNE(wsaaT5671Subprog[wsaaT5671Ix.toInt()][wsaaSubprogIx.toInt()], SPACES)) {
                callGeneric5100(covrpf);
            }
        }
    }


protected void callGeneric5100(Covrpf covrpf)
 {
        rnlallrec.company.set(linxpfResult.getChdrcoy());
        rnlallrec.chdrnum.set(linxpfResult.getChdrnum());
        rnlallrec.life.set(covrpf.getLife());
        rnlallrec.coverage.set(covrpf.getCoverage());
        rnlallrec.rider.set(covrpf.getRider());
        rnlallrec.planSuffix.set(covrpf.getPlanSuffix());
        rnlallrec.crdate.set(covrpf.getCrrcd());
        rnlallrec.crtable.set(covrpf.getCrtable());
        rnlallrec.billcd.set(linxpfResult.getPayrBillcd());
        rnlallrec.tranno.set(linxpfResult.getChdrTranno());
        if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix], "Y")) {
            rnlallrec.sacscode.set(wsaaT5645Sacscode[30]);
            rnlallrec.sacstyp.set(wsaaT5645Sacstype[30]);
            rnlallrec.genlcde.set(wsaaT5645Glmap[30]);
            rnlallrec.sacscode02.set(wsaaT5645Sacscode[31]);
            rnlallrec.sacstyp02.set(wsaaT5645Sacstype[31]);
            rnlallrec.genlcde02.set(wsaaT5645Glmap[31]);
        } else {
            rnlallrec.sacscode.set(wsaaT5645Sacscode[20]);
            rnlallrec.sacstyp.set(wsaaT5645Sacstype[20]);
            rnlallrec.genlcde.set(wsaaT5645Glmap[20]);
            rnlallrec.sacscode02.set(wsaaT5645Sacscode[21]);
            rnlallrec.sacstyp02.set(wsaaT5645Sacstype[21]);
            rnlallrec.genlcde02.set(wsaaT5645Glmap[21]);
        }
        rnlallrec.cntcurr.set(linxpfResult.getPayrCntcurr());
        rnlallrec.cnttype.set(linxpfResult.getChdrCnttype());
        rnlallrec.billfreq.set(linxpfResult.getPayrBillfreq());
        rnlallrec.duedate.set(linxpfResult.getInstfrom());
        rnlallrec.anbAtCcd.set(covrpf.getAnbAtCcd());
        /* Calculate the term left to run. It is needed in the generic */
        /* processing routine to work out the initial unit discount factor */
        datcon3rec.intDate1.set(bsscIO.getEffectiveDate());
        datcon3rec.intDate2.set(covrpf.getPremCessDate());
        datcon3rec.frequency.set("01");
        callProgram(Datcon3.class, datcon3rec.datcon3Rec);
        if (isNE(datcon3rec.statuz, varcom.oK)) {
            datcon3rec.freqFactorx.set(ZERO);
        }
        wsaaTerm.set(datcon3rec.freqFactor);
        if (isNE(wsaaTermLeftRemain, 0)) {
            wsaaTerm.add(1);
        }
        rnlallrec.termLeftToRun.set(wsaaTermLeftInteger);
        /* Get the total premium from the temporary table for the coverage */
        if(prmhpf != null)//ILIFE-8509
        	rnlallrec.covrInstprem.set(covrpf.getProrateprem() != null ? covrpf.getProrateprem() : BigDecimal.ZERO);
        else
        	rnlallrec.covrInstprem.set(add(covrpf.getInstprem(),prorateTotalPrem));
        rnlallrec.totrecd.set(ZERO);
        callProgram(wsaaT5671Subprog[wsaaT5671Ix.toInt()][wsaaSubprogIx.toInt()], rnlallrec.rnlallRec);
        if (isNE(rnlallrec.statuz, varcom.oK)) {
            wsysSysparams.set(rnlallrec.rnlallRec);
            syserrrec.params.set(wsysSystemErrorParams);
            syserrrec.statuz.set(rnlallrec.statuz);
            fatalError600();
        }
    }


protected void readAgcm3530(Agcmpf a)
 {
        /* Do not process single premium AGCMs (PTDATE = zeroes). */
        if (readcovrlnb(a)) {
            return;
        }
        /* Look up the COVRs for this AGCM from the array and the CRTABLE */
        /* for the commission processing */
        wsaaCovrIx.set(1);
        searchlabel1: {
            for (; isLT(wsaaCovrIx, wsaaCovrRec.length); wsaaCovrIx.add(1)) {
                if (isEQ(a.getChdrnum(), wsaaCovrChdrnum[wsaaCovrIx.toInt()])
                        && isEQ(a.getLife(), wsaaCovrLife[wsaaCovrIx.toInt()])
                        && isEQ(a.getCoverage(), wsaaCovrCovrg[wsaaCovrIx.toInt()])
                        && isEQ(a.getRider(), wsaaCovrRider[wsaaCovrIx.toInt()])
                        && isEQ(a.getPlanSuffix(), wsaaCovrPlanSuff[wsaaCovrIx.toInt()])) {
                    break searchlabel1;
                }
            }
            if(reinstflag && isFoundPro){
            	return;
            }
            syserrrec.statuz.set(e351);
            StringUtil stringVariable1 = new StringUtil();
            stringVariable1.addExpression(a.getChdrnum());
            stringVariable1.addExpression(a.getLife());
            stringVariable1.addExpression(a.getCoverage());
            stringVariable1.addExpression(a.getRider());
            stringVariable1.setStringInto(wsysSysparams);
            syserrrec.params.set(wsysSystemErrorParams);
            fatalError600();
        }
        /* Do not process AGCMs attached to components of invalid */
        /* status. */
        if (isNE(wsaaCovrValid[wsaaCovrIx.toInt()], "Y")) {
            return;
        }
        /* PERFORM 3540-UPDATE-AGCM */
		if (gstOnCommFlag) {
			Aglfpf aglfpf = aglfpfDAO.searchAglfRecord(a.getChdrcoy(), a.getAgntnum());
			if (null != aglfpf && null != aglfpf.getReportag() && isNE(aglfpf.getReportag(), SPACES)) {
				wsaaReporttoAgnt.set(aglfpf.getReportag());
			} else {
				wsaaReporttoAgnt.set(SPACES);
			}
		}
        updateAgcm3540(a);
    }

//ILIFE-896, if status is 'LA' skip that record
protected boolean readcovrlnb(Agcmpf a)
 {
        List<Covrpf> covrList = covrMap.get(a.getChdrnum());
        boolean foundFlag = false;
        for (Covrpf c : covrList) {
            if (c.getLife().equals(a.getLife()) && c.getCoverage().equals(a.getCoverage())
                    && c.getRider().equals(a.getRider()) && c.getPlanSuffix() == a.getPlanSuffix()) {
                foundFlag = true;
                if ("LA".equals(c.getStatcode())) {
                    return true;
                }
            }
        }
        if (!foundFlag) {
            syserrrec.params.set("Covrpf:" + a.getChdrnum());
            fatalError600();
        }
        return false;
    }

protected void updateAgcm3540(Agcmpf a)
 {
        wsaaBillfreq.set(linxpfResult.getPayrBillfreq());
        comlinkrec.billfreq.set(linxpfResult.getPayrBillfreq());
        if(prmhpf != null && bsscIO.getEffectiveDate().toInt()<=prmhpf.getTodate()) {//ILIFE-8509
        	compute(comlinkrec.instprem, 3).setRounded(wsaaCovrProratePrem[wsaaCovrIx.toInt()]);
        }
        else {
        	compute(comlinkrec.instprem, 3).setRounded(div(a.getAnnprem(), wsaaBillfreqNum)); // add prorate prem here
			if(BTPRO033Permission && linxpfResult.getProraterec()!=null && linxpfResult.getProraterec().equals("Y") && !coprList.isEmpty()) {
				for(Coprpf copr: coprList) {
					if(copr.getLife().equals(a.getLife()) && copr.getCoverage().equals(a.getCoverage()) && copr.getRider().equals(a.getRider())) {
						compute(comlinkrec.instprem, 3).setRounded(copr.getInstprem());
						break;
					}
				}
			}
        	if(null !=prorateMap && !prorateMap.isEmpty()) {
        		String key= a.getChdrnum()+a.getLife()+a.getCoverage()+a.getRider();
        		compute(comlinkrec.instprem, 3).set(add(comlinkrec.instprem,prorateMap.get(key)));
        	}
        }
        comlinkrec.chdrnum.set(linxpfResult.getChdrnum());
        comlinkrec.chdrcoy.set(linxpfResult.getChdrcoy());
        comlinkrec.cnttype.set(linxpfResult.getChdrCnttype());//ILIFE-7965
        comlinkrec.life.set(a.getLife());
        comlinkrec.agent.set(a.getAgntnum());
        comlinkrec.coverage.set(a.getCoverage());
        comlinkrec.annprem.set(a.getAnnprem());
        comlinkrec.rider.set(a.getRider());
        comlinkrec.planSuffix.set(a.getPlanSuffix());
        comlinkrec.crtable.set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
        comlinkrec.agentClass.set(a.getAgentClass());
        comlinkrec.icommtot.set(a.getInitcom());
        comlinkrec.icommpd.set(a.getCompay());
        comlinkrec.icommernd.set(a.getComern());
        comlinkrec.effdate.set(a.getEfdate());
        //Ilife-6944 start
        //comlinkrec.ptdate.set(linxpfResult.getChdrPtdate());
        comlinkrec.ptdate.set(linxpfResult.getLinsInstto());
        //Ilife-6944 Stop
        comlinkrec.language.set(bsscIO.getLanguage());
        comlinkrec.currto.set(0);
        comlinkrec.targetPrem.set(0);
        /* Call the three types of commission routine from AGCM */
        for (wsaaCommethNo.set(1); !(isGT(wsaaCommethNo, 3)); wsaaCommethNo.add(1)) {
            setUpCommMethod3550(a);
        }
        /* Write an ACMV for each CHDRNUM/AGNTNUM/COVR/COMMISSION AMOUNT */
        postCommission3570(a);
        a.setPtdate(linxpfResult.getLinsInstto());
        /* MOVE REWRT TO AGCMRNL-FUNCTION. */
        //PINNACLE-2260 : Renewal commission Earned Balance Issue
        //PINNACLE-2260 - START
        
        outerrecordfound = false; //PINNACLE-3193
        innerrecordfound = false; //PINNACLE-3193
		if (!agcmpfInvUpdateList.isEmpty()) {
			for (Agcmpf agcmpf : agcmpfInvUpdateList) {
				if (agcmpf.getChdrcoy().equals(a.getChdrcoy()) 
						&& agcmpf.getChdrnum().equals(a.getChdrnum())
						&& agcmpf.getLife().equals(a.getLife()) 
						&& agcmpf.getCoverage().equals(a.getCoverage())
						&& agcmpf.getRider().equals(a.getRider()) 
						&& agcmpf.getPlanSuffix() == a.getPlanSuffix()) {
					outerrecordfound = true;
					if (!agcmpfInsertList.isEmpty()) {
						for (Agcmpf inAgcmpf : agcmpfInsertList) {
							if (inAgcmpf.getChdrcoy().equals(a.getChdrcoy())
									&& inAgcmpf.getChdrnum().equals(a.getChdrnum())
									&& inAgcmpf.getLife().equals(a.getLife())
									&& inAgcmpf.getCoverage().equals(a.getCoverage())
									&& inAgcmpf.getRider().equals(a.getRider())
									&& inAgcmpf.getPlanSuffix() == a.getPlanSuffix()
									&& inAgcmpf.getSeqno() == a.getSeqno()) { //PINNACLE-3193
								innerrecordfound = true;
								
								agcmpfInsertList.remove(inAgcmpf);
								agcmpfInsertList.add(a);
								break;
							}
						}
						if (!innerrecordfound) {
							agcmpfInsertList.add(a);
						}
					} else {
						agcmpfInsertList.add(a);
					}
					break;
				}
			}
			if (!outerrecordfound) {
				agcmpfUpdateList.add(a);
			}

		} else {
			agcmpfUpdateList.add(a);
		}

		// PINNACLE-2260 - END

    }

protected void setUpCommMethod3550(Agcmpf a)
 {
        if (isEQ(wsaaCommethNo, 1)) {
            comlinkrec.method.set(a.getBascpy());
        } else if (isEQ(wsaaCommethNo, 2)) {
            comlinkrec.method.set(a.getSrvcpy());
        } else if (isEQ(wsaaCommethNo, 3)) {
            comlinkrec.method.set(a.getRnwcpy());
        }
        ArraySearch as1 = ArraySearch.getInstance(wsaaT5644Rec);
        as1.setIndices(wsaaT5644Ix);
        as1.addSearchKey(wsaaT5644Key, comlinkrec.method, true);
        if (!as1.binarySearch()) {
            return;
        }
        /* Check initial commission for BASCPY method only. */
        if (isEQ(wsaaCommethNo, 1)) {
            if (isNE(a.getCompay(), a.getInitcom())
                    || isNE(a.getComern(), a.getInitcom())) {
                callCommRoutine3560(a);
            }
        } else {
            if (isEQ(wsaaCommethNo, 3) && isEQ(linxpfResult.getChdrRnwlsupr(), "Y")
                    && isGTE(linxpfResult.getPayrPtdate(), linxpfResult.getChdrRnwlspfrom())
                    && isLTE(linxpfResult.getPayrPtdate(), linxpfResult.getChdrRnwlspto())) {
                /* NEXT_SENTENCE */
            } else {
                callCommRoutine3560(a);
            }
        }

    }

protected void callCommRoutine3560(Agcmpf a)
 {
        comlinkrec.payamnt.set(0);
        comlinkrec.erndamt.set(0);
        if(gstOnCommFlag) {
        	comlinkrec.gstAmount.set(0);		
		}
        /* MOVE 0 TO CLNK-ERNDAMT. */
        /* Read ZRAP to get ZRORCODE, this one is valid only for Overide */
        /* Agent */
        /* PERFORM A100-READ-ZRAP */
        /* Call the subroutine relating to the CLNK-METHOD */
        callProgram(wsaaT5644Comsub[wsaaT5644Ix.toInt()], comlinkrec.clnkallRec);
        if (isNE(comlinkrec.statuz, varcom.oK)) {
            syserrrec.params.set(comlinkrec.clnkallRec);
            fatalError600();
        }
        /* MOVE CLNK-PAYAMNT TO ZRDP-AMOUNT-IN. */
        /* MOVE CNTCURR TO ZRDP-CURRENCY. */
        /* PERFORM C000-CALL-ROUNDING. */
        /* MOVE ZRDP-AMOUNT-OUT TO CLNK-PAYAMNT. */
        if (isNE(comlinkrec.payamnt, 0)) {
        	//ILB-441 starts
        	//zrdecplrec.amountIn.set(comlinkrec.payamnt);
        	zrdecplcPojo.setAmountIn(comlinkrec.payamnt.getbigdata());
        	zrdecplcPojo.setCurrency(linxpfResult.getCntcurr());
        /*    zrdecplrec.amountIn.set(comlinkrec.payamnt);
            zrdecplrec.currency.set(linxpfResult.getCntcurr());*/
            c000CallRounding(zrdecplcPojo);
         //   comlinkrec.payamnt.set(zrdecplrec.amountOut);
            comlinkrec.payamnt.set(zrdecplcPojo.getAmountOut());
            //ILB-441 ends
        }
        /* MOVE CLNK-ERNDAMT TO ZRDP-AMOUNT-IN. */
        /* MOVE CNTCURR TO ZRDP-CURRENCY. */
        /* PERFORM C000-CALL-ROUNDING. */
        /* MOVE ZRDP-AMOUNT-OUT TO CLNK-ERNDAMT. */
        if (isNE(comlinkrec.erndamt, 0)) {
        	//ILB-441 starts
        	zrdecplcPojo.setAmountIn(comlinkrec.erndamt.getbigdata());
        	zrdecplcPojo.setCurrency(linxpfResult.getCntcurr());
           /* zrdecplrec.amountIn.set(comlinkrec.erndamt);
            zrdecplrec.currency.set(linxpfResult.getCntcurr());*/
            c000CallRounding(zrdecplcPojo);
            //comlinkrec.erndamt.set(zrdecplrec.amountOut);
            comlinkrec.erndamt.set(zrdecplcPojo.getAmountOut());
            //ILB-441 ends
        }
        /* Basic Commission */
        if (isEQ(wsaaCommethNo, 1)) {
            if (isEQ(a.getOvrdcat(), "O")) {
                wsaaOvrdBascpyDue.set(comlinkrec.payamnt);
                wsaaOvrdBascpyErn.set(comlinkrec.erndamt);
                compute(wsaaOvrdBascpyPay, 2)
                        .set(add(wsaaOvrdBascpyPay, (sub(comlinkrec.payamnt, comlinkrec.erndamt))));
            } else {
                wsaaBascpyDue.set(comlinkrec.erndamt); // ILIFE-8392
                wsaaBascpyErn.set(comlinkrec.erndamt);               
                /*ILIFE-7965 : Starts*/
                /* GST on Basic Commission*/
                if(gstOnCommFlag && a.getInitCommGst()!=null) {
                	compute(wsaaCompErnInitCommGst, 3).setRounded(add(wsaaCompErnInitCommGst, comlinkrec.gstAmount));    			   			
                	a.setInitCommGst(add(a.getInitCommGst(), wsaaCompErnInitCommGst).getbigdata());
                	compute(wsaaBascpyPay, 2).setRounded(add(wsaaBascpyPay,(sub(add(comlinkrec.erndamt, comlinkrec.gstAmount),comlinkrec.payamnt))));//ILIFE-7965	
                }else{
                	compute(wsaaBascpyPay, 2).set(add(wsaaBascpyPay, (sub(add(comlinkrec.erndamt, comlinkrec.gstAmount),comlinkrec.payamnt))));//ILIFE-7965	
                }
                /*ILIFE-7965 : Ends*/
                /***** IF WSAA-COMPAY-KEPT = ZEROES */
                /***** MOVE AGCMRNL-AGNTNUM TO WSAA-AGENT-KEPT */
                /***** MOVE CLNK-PAYAMNT TO WSAA-COMPAY-KEPT */
                /***** END-IF */
            }
            a.setCompay(add(a.getCompay(), comlinkrec.payamnt).getbigdata());
            a.setComern(add(a.getComern(), comlinkrec.erndamt).getbigdata());
        }
        /* Service Commission */
        if (isEQ(wsaaCommethNo, 2)) {
            wsaaSrvcpyDue.set(comlinkrec.payamnt);
            wsaaSrvcpyErn.set(comlinkrec.erndamt);
            a.setScmearn(add(a.getScmearn(), comlinkrec.erndamt).getbigdata());
            a.setScmdue(add(a.getScmdue(), comlinkrec.payamnt).getbigdata());
        }
        /* Renewal Commission */
        if (isEQ(wsaaCommethNo, 3) && !isDMSFlgOn) {	//ILIFE-8880
			if(gstOnCommFlag) {
				wsaaRnwcpyDue.set(comlinkrec.erndamt); // ILIFE-8392
			} else {
				wsaaRnwcpyDue.set(comlinkrec.payamnt);
			}
            wsaaRnwcpyErn.set(comlinkrec.erndamt);
            a.setRnlcdue(add(a.getRnlcdue(), comlinkrec.payamnt).getbigdata());
            a.setRnlcearn(add(a.getRnlcearn(), comlinkrec.erndamt).getbigdata());
            /*ILIFE-7965 : Starts*/
            /* GST on Renewal Commission*/
            if(gstOnCommFlag && a.getRnwlCommGst()!=null) {
            	compute(wsaaCompErnRnwlCommGst, 3).setRounded(add(wsaaCompErnRnwlCommGst, comlinkrec.gstAmount));    					
            	a.setRnwlCommGst(add(a.getRnwlCommGst(), wsaaCompErnRnwlCommGst).getbigdata());
				compute(wsaaRnwcpyPay, 2).setRounded(add(wsaaRnwcpyPay,(sub(add(comlinkrec.erndamt, comlinkrec.gstAmount),comlinkrec.payamnt))));
            }
            /*ILIFE-7965 : Ends*/
        }
        /* Bonus Workbench * */
        if (isNE(th605rec.bonusInd, "Y")) {
            return;
        }
        /* Skip overriding commission */
        if (isNE(a.getOvrdcat(), "B")) {
            return;
        }
        if (isEQ(wsaaCommethNo, "1")) {
            firstPrem.setTrue();
        } else if (isEQ(wsaaCommethNo, "3")) {
            renPrem.setTrue();
        } else {
            return;
        }
        if (isNE(comlinkrec.payamnt, ZERO)) {
            
            Zctnpf zctnpf = new Zctnpf();
            zctnpf.setAgntcoy(a.getChdrcoy());
            zctnpf.setAgntnum(a.getAgntnum());
            zctnpf.setCommAmt(comlinkrec.payamnt.getbigdata());
            b400LocatePremium(a);
            setPrecision(zctnpf.getPremium(), 3);
            zctnpf.setPremium(div(wsaaAgcmPremium, wsaaBillfq9).getbigdata());
            if (isNE(zctnpf.getPremium(), 0)) {
            	//ILB-441 starts
            	zrdecplcPojo.setAmountIn(zctnpf.getPremium());
            	zrdecplcPojo.setCurrency(linxpfResult.getCntcurr());
               /* zrdecplrec.amountIn.set(zctnpf.getPremium());
                zrdecplrec.currency.set(linxpfResult.getCntcurr());*/
                c000CallRounding(zrdecplcPojo);
                zctnpf.setPremium(zrdecplrec.amountOut.getbigdata());
                //ILB-441 ends
            }
            setPrecision(zctnpf.getSplitBcomm(), 3);
            zctnpf.setSplitBcomm(div(mult(a.getAnnprem(), 100), wsaaAgcmPremium).getbigdata());
            if (isNE(zctnpf.getSplitBcomm(), 0)) {
            	//ILB-441 start
            	zrdecplcPojo.setAmountIn(zctnpf.getSplitBcomm());
            	zrdecplcPojo.setCurrency(linxpfResult.getCntcurr());
                c000CallRounding(zrdecplcPojo);
                zctnpf.setSplitBcomm(zrdecplcPojo.getAmountOut());
                //ILB-441 ends
            }
            zctnpf.setZprflg(wsaaPremType.toString());
            zctnpf.setChdrcoy(comlinkrec.chdrcoy.toString());
            zctnpf.setChdrnum(comlinkrec.chdrnum.toString());
            zctnpf.setLife(comlinkrec.life.toString());
            zctnpf.setCoverage(comlinkrec.coverage.toString());
            zctnpf.setRider(comlinkrec.rider.toString());
            zctnpf.setTranno(linxpfResult.getChdrTranno());
            zctnpf.setTransCode(bprdIO.getAuthCode().toString());
            zctnpf.setEffdate(linxpfResult.getInstfrom());
            zctnpf.setTrandate(bsscIO.getEffectiveDate().toInt());
            
            if(zctnInsertList == null){
                zctnInsertList = new ArrayList<Zctnpf>();
            }
            zctnInsertList.add(zctnpf);
        }

    }

protected void postCommission3570(Agcmpf a)
 {
        /* Initialise the ACMV area and move the contract type to its */
        /* relevant field (same for all calls to LIFACMV). */
        lifacmvrec.rldgacct.set(SPACES);
        lifacmvrec.substituteCode[1].set(linxpfResult.getChdrCnttype());
        wsaaAgntChdrnum.set(wsaaCovrChdrnum[wsaaCovrIx.toInt()]);
        wsaaAgntLife.set(wsaaCovrLife[wsaaCovrIx.toInt()]);
        wsaaAgntCoverage.set(wsaaCovrCovrg[wsaaCovrIx.toInt()]);
        wsaaAgntRider.set(wsaaCovrRider[wsaaCovrIx.toInt()]);
        wsaaPlan.set(wsaaCovrPlanSuff[wsaaCovrIx.toInt()]);
        wsaaAgntPlanSuffix.set(wsaaPlansuff);
        postInitialCommission7000(a);
        postServiceCommission8000(a);
        postRenewalCommission9000(a);
        postOverridCommission10000(a);
        wsaaBascpyDue.set(0);
        wsaaBascpyErn.set(0);
        wsaaBascpyPay.set(0);
        wsaaSrvcpyDue.set(0);
        wsaaSrvcpyErn.set(0);
        wsaaRnwcpyDue.set(0);
		wsaaRnwcpyPay.set(0);
        wsaaRnwcpyErn.set(0);
        wsaaOvrdBascpyPay.set(0);
        wsaaOvrdBascpyDue.set(0);
        wsaaOvrdBascpyErn.set(0);
        /*ILIFE-7965 : Starts*/
        if(gstOnCommFlag){
        	 wsaaCompErnRnwlCommGst.set(0);
        	 wsaaCompErnInitCommGst.set(0);
        }      
        /*ILIFE-7965 : Ends*/

    }

protected void writePtrn3600()
 {
        Ptrnpf p = getPtrnpf();
        p.setTranno(linxpfResult.getChdrTranno());
        p.setChdrcoy(linxpfResult.getChdrcoy());
        p.setChdrnum(linxpfResult.getChdrnum());
        p.setBatcpfx(batcdorrec.prefix.toString());
        p.setPtrneff(linxpfResult.getInstfrom());
        p.setDatesub(bsscIO.getEffectiveDate().toInt());
        if (ptrnpfInsertList == null) {
            ptrnpfInsertList = new ArrayList<Ptrnpf>();
        }
        ptrnpfInsertList.add(p);
        //ILIFE-3997-STARTS
        //chdrpf = chdrpfDAO.getchdrRecord(p.getChdrcoy(),p.getChdrnum());//IJTI-1485
        if(linxpfResult.getChdrnlgflg() != null && linxpfResult.getChdrnlgflg() == "Y")
            writeNLGRec(p, chdrpf );
        //ILIFE-3997-ENDS
    }

    protected Ptrnpf getPtrnpf() {
        Ptrnpf p = new Ptrnpf();
        p.setUserT(0);
        p.setPtrneff(bsscIO.getEffectiveDate().toInt());
        p.setTrdt(varcom.vrcmDate.toInt());
        p.setBatccoy(batcdorrec.company.toString());
        p.setBatcactyr(batcdorrec.actyear.toInt());
        p.setBatcactmn(batcdorrec.actmonth.toInt());
        p.setBatctrcde(batcdorrec.trcde.toString());
        p.setBatcbatch(batcdorrec.batch.toString());
        p.setBatcbrn(batcdorrec.branch.toString());
        p.setTrtm(varcom.vrcmTime.toInt());
        p.setValidflag("");
        return p;
    }

protected void postInitialCommission7000(Agcmpf a)
 {
        /* Initial commission due */
        if (isNE(wsaaBascpyDue, 0)) {
            /* ADD +1 TO LIFA-JRNSEQ */
            wsaaGlSub = 8;
            wsaaRldgChdrnum.set(a.getChdrnum());
            wsaaRldgLife.set(a.getLife());
            wsaaRldgCoverage.set(a.getCoverage());
            wsaaRldgRider.set(a.getRider());
            wsaaPlan.set(a.getPlanSuffix());
            wsaaRldgPlanSuff.set(wsaaPlansuff);
            lifacmvrec.tranref.set(wsaaRldgacct);
            lifacmvrec.substituteCode[6].set(SPACES);
            lifacmvrec.origamt.set(wsaaBascpyDue);
            lifacmvrec.rldgacct.set(a.getAgntnum());
            lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
            callLifacmv6000();
            /* Override Commission */
			if (isEQ(th605rec.indic, "Y")) {
				/* MOVE AGCMRNL-ANNPREM TO ZORL-ANNPREM <LA4973> */
				zorlnkrec.annprem.set(comlinkrec.instprem);
				/* MOVE AGCMRNL-EFDATE TO ZORL-EFFDATE <LA4973> */
				zorlnkrec.effdate.set(linxpfResult.getInstfrom());
				b500CallZorcompy();
				if (gstOnCommFlag  && isNE(zorlnkrec.gstAmount,0)) { 
					gstPostings(zorlnkrec.gstAmount.getbigdata(), wsaaReporttoAgnt.toString(), 40); // ILIFE-8468
				}
			}
        }
        /* Initial commission earned */
        if (isNE(wsaaBascpyErn, 0)) {
            /* ADD +1 TO LIFA-JRNSEQ */
            lifacmvrec.origamt.set(wsaaBascpyErn);
            if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix], "Y")) {
                wsaaGlSub = 23;
                lifacmvrec.rldgacct.set(wsaaRldgagnt);
                lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
            } else {
                wsaaGlSub = 9;
                lifacmvrec.rldgacct.set(linxpfResult.getChdrnum());
            }
            lifacmvrec.tranref.set(a.getAgntnum());
            callLifacmv6000();
            /*ILIFE-7965 : Starts */
            if(gstOnCommFlag && isNE(wsaaCompErnInitCommGst,0)) { 
				gstPostings(wsaaCompErnInitCommGst.getbigdata(), wsaaRldgagnt.toString(), 39);
			}
            /*ILIFE-7965 : Ends */
        }
        /* Initial commission paid */
        if (isNE(wsaaBascpyPay, 0)) {
            lifacmvrec.origamt.set(wsaaBascpyPay);
            /* ADD +1 TO LIFA-JRNSEQ */
            if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix], "Y")) {
            	if(gstOnCommFlag) {
            		wsaaGlSub = 8;
            		lifacmvrec.rldgacct.set(a.getAgntnum());
            	} else {
	                wsaaGlSub = 24;
	                lifacmvrec.rldgacct.set(wsaaRldgagnt);
	                lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
            	}
            } else {
                wsaaGlSub = 10;
                lifacmvrec.rldgacct.set(linxpfResult.getChdrnum());
            }
            lifacmvrec.tranref.set(a.getAgntnum());
            callLifacmv6000();
        }

    }

protected void postServiceCommission8000(Agcmpf a)
 {
        /* Service commission due */
        if (isNE(wsaaSrvcpyDue, 0)) {
            /* ADD +1 TO LIFA-JRNSEQ */
            lifacmvrec.origamt.set(wsaaSrvcpyDue);
            /* MOVE 9 TO WSAA-GL-SUB */
            wsaaGlSub = 11;
            lifacmvrec.substituteCode[6].set(SPACES);
            lifacmvrec.rldgacct.set(a.getAgntnum());
            lifacmvrec.tranref.set(linxpfResult.getChdrnum());
            callLifacmv6000(true);
            /* Override Commission */
            if (isEQ(th605rec.indic, "Y")) {
                /* MOVE AGCMRNL-ANNPREM TO ZORL-ANNPREM <LA4973> */
                zorlnkrec.annprem.set(comlinkrec.instprem);
                /* MOVE AGCMRNL-EFDATE TO ZORL-EFFDATE <LA4973> */
                zorlnkrec.effdate.set(linxpfResult.getInstfrom());
                b500CallZorcompy();
            }
        }
        /* Service commission earned */
        if (isNE(wsaaSrvcpyErn, 0)) {
            /* ADD +1 TO LIFA-JRNSEQ */
            lifacmvrec.origamt.set(wsaaSrvcpyErn);
            if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix], "Y")) {
                wsaaGlSub = 25;
                lifacmvrec.rldgacct.set(wsaaRldgagnt);
                lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
            } else {
                wsaaGlSub = 12;
                lifacmvrec.substituteCode[6].set(SPACES);
                lifacmvrec.rldgacct.set(linxpfResult.getChdrnum());
            }
            lifacmvrec.tranref.set(a.getAgntnum());
            callLifacmv6000(true);
        }
    }

//ILIFE-3997-STARTS
	protected void writeNLGRec(Ptrnpf ptrnIO,Chdrpf chdr) {
	
	   
        Iterator<Nlgtpf> iteratorListNlg;
		nlgtpfList = nlgtpfDAO.readNlgtpf(ptrnIO.getChdrcoy(), ptrnIO.getChdrnum());//IJTI-1485
			if (! (nlgtpfList.isEmpty())){
				iteratorListNlg = nlgtpfList.iterator();
				while (iteratorListNlg.hasNext()) {
					Nlgtpf nlgtpf = new Nlgtpf();
					nlgtpf = iteratorListNlg.next();
			     if ((nlgtpf.getBatctrcde().equals("B521") && isEQ(nlgtpf.getEffdate(), ptrnIO.getPtrneff())
					&& nlgtpf.getAmnt04().doubleValue() != 0)
					|| (nlgtpf.getBatctrcde().equals("B673") && isEQ(nlgtpf.getEffdate(), ptrnIO.getPtrneff())
							&& nlgtpf.getAmnt03().doubleValue() != 0)) {

//				nlgcalcrec.chdrcoy.set(ptrnIO.getChdrcoy());
//				nlgcalcrec.chdrnum.set(ptrnIO.getChdrnum());
//				nlgcalcrec.effdate.set(ptrnIO.getPtrneff());
//				nlgcalcrec.tranno.set(ptrnIO.getTranno());
//				nlgcalcrec.batcactyr.set(ptrnIO.getBatcactyr());
//				nlgcalcrec.batcactmn.set(ptrnIO.getBatcactmn());
//				nlgcalcrec.batctrcde.set(ptrnIO.getBatctrcde());
//				nlgcalcrec.batctrcde.set(ptrnIO.getBatctrcde());
//				nlgcalcrec.billfreq.set(chdr.getBillfreq());
//				nlgcalcrec.cnttype.set(chdr.getCnttype());
//				nlgcalcrec.language.set(bsscIO.getLanguage());
//				nlgcalcrec.frmdate.set(chdr.getOccdate());
//				nlgcalcrec.todate.set(varcom.vrcmMaxDate);
//				nlgcalcrec.occdate.set(chdr.getOccdate());
//				nlgcalcrec.ptdate.set(chdr.getPtdate());
//				nlgcalcrec.inputAmt.set(0);
//				nlgcalcrec.unpaidPrem.set(0);
//				nlgcalcrec.ovduePrem.set(0);
//				nlgcalcrec.function.set("COLCT");
//				callProgram(Nlgcalc.class, nlgcalcrec.nlgcalcRec);
//				if (isNE(nlgcalcrec.status, varcom.oK)) {
//					syserrrec.statuz.set(batcuprec.statuz);
//					fatalError600();
//				}
			    // changed by yy 
			    NlgcalcPojo nlgcalcPojo = new NlgcalcPojo();
				nlgcalcPojo.setChdrcoy(ptrnIO.getChdrcoy());
				nlgcalcPojo.setChdrnum(ptrnIO.getChdrnum());
				nlgcalcPojo.setEffdate(ptrnIO.getPtrneff());
				nlgcalcPojo.setTranno(ptrnIO.getTranno());
				nlgcalcPojo.setBatcactyr(ptrnIO.getBatcactyr());
				nlgcalcPojo.setBatcactmn(ptrnIO.getBatcactmn());
				nlgcalcPojo.setBatctrcde(ptrnIO.getBatctrcde());
				nlgcalcPojo.setBatctrcde(ptrnIO.getBatctrcde());
				nlgcalcPojo.setBillfreq(chdr.getBillfreq());
				nlgcalcPojo.setCnttype(chdr.getCnttype());
				nlgcalcPojo.setLanguage(bsscIO.getLanguage().toString());
				nlgcalcPojo.setFrmdate(chdr.getOccdate());
				nlgcalcPojo.setTodate(varcom.vrcmMaxDate.toInt());
				nlgcalcPojo.setOccdate(chdr.getOccdate());
				nlgcalcPojo.setPtdate(chdr.getPtdate());
				nlgcalcPojo.setInputAmt(BigDecimal.ZERO);
				nlgcalcPojo.setUnpaidPrem(BigDecimal.ZERO);
				nlgcalcPojo.setOvduePrem(BigDecimal.ZERO);
				nlgcalcPojo.setFunction("COLCT");
				nlgcalcUtils.mainline(nlgcalcPojo);
				if (!varcom.oK.toString().equals(nlgcalcPojo.getStatus())) {
					syserrrec.statuz.set(batcuprec.statuz);
					fatalError600();
				}
				break;
			}
		}
			}
	}
	
	//ILIFE-3997-ENDS

protected void postRenewalCommission9000(Agcmpf a)
 {
        /* Renewal commission due */
        if (isNE(wsaaRnwcpyDue, 0)) {
            /* ADD +1 TO LIFA-JRNSEQ */
            wsaaGlSub = 13;
            lifacmvrec.substituteCode[6].set(SPACES);
            lifacmvrec.origamt.set(wsaaRnwcpyDue);
            lifacmvrec.rldgacct.set(a.getAgntnum());
            /* MOVE CHDRNUM TO LIFA-TRANREF */
            wsaaRldgChdrnum.set(a.getChdrnum());
            wsaaRldgLife.set(a.getLife());
            wsaaRldgCoverage.set(a.getCoverage());
            wsaaRldgRider.set(a.getRider());
            wsaaPlan.set(a.getPlanSuffix());
            wsaaRldgPlanSuff.set(wsaaPlansuff);
            lifacmvrec.tranref.set(wsaaRldgacct);
            lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
            callLifacmv6000(true);
            /* Override Commission */
            if (isEQ(th605rec.indic, "Y")) {
                /* MOVE AGCMRNL-ANNPREM TO ZORL-ANNPREM <LA4973> */
                zorlnkrec.annprem.set(comlinkrec.instprem);
                /* MOVE AGCMRNL-EFDATE TO ZORL-EFFDATE <LA4973> */
                zorlnkrec.effdate.set(linxpfResult.getInstfrom());
                b500CallZorcompy();
                if(gstOnCommFlag && isNE(zorlnkrec.gstAmount,0)) { 
    				gstPostings(zorlnkrec.gstAmount.getbigdata(),wsaaReporttoAgnt.toString(),40); // ILIFE-8468
    			}     
                
            }
        }
        /* Renewal commission earned */
        if (isNE(wsaaRnwcpyErn, 0)) {
            lifacmvrec.origamt.set(wsaaRnwcpyErn);
            /* ADD +1 TO LIFA-JRNSEQ */
            if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix], "Y")) {
                wsaaGlSub = 26;
                lifacmvrec.rldgacct.set(wsaaRldgagnt);
                lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
            } else {
                wsaaGlSub = 14;
                lifacmvrec.substituteCode[6].set(SPACES);
                lifacmvrec.rldgacct.set(linxpfResult.getChdrnum());
            }
            lifacmvrec.tranref.set(a.getAgntnum());
            callLifacmv6000(true);
            /*ILIFE-7965 : Starts */
            if(gstOnCommFlag && isNE(wsaaCompErnRnwlCommGst,0)) { 
				gstPostings(wsaaCompErnRnwlCommGst.getbigdata(), a.getAgntnum(),41);
			}
            /*ILIFE-7965 : Ends */
        }
		/* Renewal commission paid IJS-450*/
        if (isNE(wsaaRnwcpyPay, 0)) {
            lifacmvrec.origamt.set(wsaaRnwcpyPay);
            /* ADD +1 TO LIFA-JRNSEQ */
            if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix], "Y")) {
            	if(gstOnCommFlag) {
            		wsaaGlSub = 13;
            		lifacmvrec.rldgacct.set(a.getAgntnum());
            	} else {
	                wsaaGlSub = 26;
	                lifacmvrec.rldgacct.set(wsaaRldgagnt);
	                lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
            	}
            } else {
                wsaaGlSub = 14;
                lifacmvrec.rldgacct.set(linxpfResult.getChdrnum());
            }
            lifacmvrec.tranref.set(a.getAgntnum());
            callLifacmv6000();
        }

    }
/*ILIFE-7965 : Starts*/
protected void gstPostings(BigDecimal origamt, String agntNum, int gstSub) {	
	lifacmvrec.origamt.set(origamt);
	/* ADD +1 TO LIFA-JRNSEQ */	
    if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix], "Y")) {
        wsaaGlSub = gstSub;
		lifacmvrec.rldgacct.set(agntNum);
        lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
    }
	lifacmvrec.tranref.set(agntNum);
	callLifacmv6000();
}
/*ILIFE-7965 : Ends*/


protected void postOverridCommission10000(Agcmpf a)
 {
        /* Initial over-riding commission due */
        if (isNE(wsaaOvrdBascpyDue, 0)) {
            /* ADD 1 TO LIFA-JRNSEQ */
            wsaaGlSub = 16;
            lifacmvrec.origamt.set(wsaaOvrdBascpyDue);
            lifacmvrec.rldgacct.set(a.getAgntnum());
            lifacmvrec.substituteCode[6].set(SPACES);
            lifacmvrec.tranref.set(a.getCedagent());
            lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
            callLifacmv6000();
        }
        /* Initial over-riding commission earned */
        if (isNE(wsaaOvrdBascpyErn, 0)) {
            lifacmvrec.origamt.set(wsaaOvrdBascpyErn);
            /* ADD +1 TO LIFA-JRNSEQ */
            if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix], "Y")) {
                wsaaGlSub = 27;
                lifacmvrec.tranref.set(a.getAgntnum());
                lifacmvrec.rldgacct.set(wsaaRldgagnt);
                lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
            } else {
                wsaaGlSub = 17;
                lifacmvrec.tranref.set(a.getAgntnum());
                lifacmvrec.rldgacct.set(linxpfResult.getChdrnum());
                lifacmvrec.substituteCode[6].set(SPACES);
            }
            callLifacmv6000();
        }
        /* Initial over-riding commission paid */
        if (isNE(wsaaOvrdBascpyPay, 0)) {
            /* ADD +1 TO LIFA-JRNSEQ */
            lifacmvrec.origamt.set(wsaaOvrdBascpyPay);
            if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix], "Y")) {
                wsaaGlSub = 28;
                lifacmvrec.tranref.set(a.getAgntnum());
                /* MOVE WSAA-RLDGACCT TO LIFA-RLDGACCT */
                lifacmvrec.rldgacct.set(wsaaRldgagnt);
                lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
            } else {
                wsaaGlSub = 18;
                lifacmvrec.tranref.set(a.getAgntnum());
                lifacmvrec.substituteCode[6].set(SPACES);
                lifacmvrec.rldgacct.set(linxpfResult.getChdrnum());
            }
            callLifacmv6000();
        }

    }

protected void commit3500()
 {
	LOGGER.info("commit3500 start");
	if (getWsspEdterror().equals(varcom.endp)&& !noRecordFound){
        contotrec.totno.set(1);
        contotrec.totval.set(contot01);
        callContot001();
        contotrec.totno.set(2);
        contotrec.totval.set(contot02);
        callContot001();
        contotrec.totno.set(3);
        contotrec.totval.set(contot03);
        callContot001();
        contotrec.totno.set(4);
        contotrec.totval.set(contot04);
        callContot001();
        contotrec.totno.set(5);
        contotrec.totval.set(contot05);
        callContot001();
        contotrec.totno.set(6);
        contotrec.totval.set(contot06);
        callContot001();
        contotrec.totno.set(7);
        contotrec.totval.set(contot07);
        callContot001();
        contotrec.totno.set(8);
        contotrec.totval.set(contot08);
        callContot001();
        contotrec.totno.set(9);
        contotrec.totval.set(contot09);
        callContot001();
        contotrec.totno.set(10);
        contotrec.totval.set(contot10);
        callContot001();
        contotrec.totno.set(11);
        contotrec.totval.set(contot11);
        callContot001();
        contotrec.totno.set(12);
        contotrec.totval.set(contot12);
        callContot001();
        contotrec.totno.set(13);
        contotrec.totval.set(contot13);
        callContot001();
		
        if (acagInsertList != null && !acagInsertList.isEmpty()) acagpfDAO.insertAcagpf(acagInsertList);
        if (linsUpdateList != null && !linsUpdateList.isEmpty()) linspfDAO.updateLinspf(linsUpdateList);
        if (!updateCoprList.isEmpty()) coprpfDAO.updateCoprpf(updateCoprList);
        if (payrUpdateList != null && !payrUpdateList.isEmpty()) payrpfDAO.updatePayrPF(payrUpdateList,false,true);
        if (hdivInsertList != null && !hdivInsertList.isEmpty()) hdivpfDAO.insertHdivpf(hdivInsertList);
        if (taxdUpdateList != null && !taxdUpdateList.isEmpty()) taxdpfDAO.updateTaxdpf(taxdUpdateList);
        if (zptnInsertList != null && !zptnInsertList.isEmpty()) zptnpfDAO.insertZptnpf(zptnInsertList);
        if (covrUpdateList != null && !covrUpdateList.isEmpty()) covrpfDAO.updateRerateData(covrUpdateList);
        if (covrInsertList != null && !covrInsertList.isEmpty()) covrpfDAO.insertCovrBulk(covrInsertList);
        if (rertUpdateList != null && !rertUpdateList.isEmpty()) rertpfDAO.updateRertList(rertUpdateList);
        if (zctnInsertList != null && !zctnInsertList.isEmpty()) 
        	zctnpfDAO.insertZctnpf(zctnInsertList);
        if (agcmpfInvUpdateList != null && !agcmpfInvUpdateList.isEmpty()) agcmpfDAO.updateAgcmByUniqNum(agcmpfInvUpdateList);
        if (agcmpfInsertList != null && !agcmpfInsertList.isEmpty()) agcmpfDAO.insertAgcmpfList(agcmpfInsertList);
        if (agcmpfUpdateList != null && !agcmpfUpdateList.isEmpty()) agcmpfDAO.updateAgcmByUniqNum(agcmpfUpdateList);
        
														 
        if (ptrnpfInsertList != null && !ptrnpfInsertList.isEmpty()) 
        	ptrnpfDAO.insertPtrnPF(ptrnpfInsertList);
        if (trwppfInsertList != null && !trwppfInsertList.isEmpty()) 
        	trwppfDAO.insertTrwppf(trwppfInsertList); 
        if (acblpfInsertList != null && !acblpfInsertList.isEmpty()) 
        	acblpfDAO.processAcblpfList(acblpfInsertList, false);
        if (acblpfUpdateList != null && !acblpfUpdateList.isEmpty()) 
        	acblpfDAO.processAcblpfList(acblpfUpdateList, true); 
        if (acblpfInsertAll != null && !acblpfInsertAll.isEmpty()) 
        	acblpfDAO.processAcblpfList(acblpfInsertAll, false);  
        if (acblpfLifrtrnList != null && !acblpfLifrtrnList.isEmpty()) 
        	acblpfDAO.processAcblpfList(acblpfLifrtrnList, false);        
        
        if (acagInsertList != null) {
            acagInsertList.clear();
            acagInsertList = null;
        }
        if (linsUpdateList != null) {
            linsUpdateList.clear();
            linsUpdateList = null;
        }
        if (payrUpdateList != null) {
            payrUpdateList.clear();
            payrUpdateList = null;
        }
        if (hdivInsertList != null) {
            hdivInsertList.clear();
            hdivInsertList = null;
        }
        if (taxdUpdateList != null) {
            taxdUpdateList.clear();
            taxdUpdateList = null;
        }
        if (zptnInsertList != null) {
            zptnInsertList.clear();
            zptnInsertList = null;
        }
        if (zctnInsertList != null) {
            zctnInsertList.clear();
            zctnInsertList = null;
        }
        if (agcmpfUpdateList != null) {
            agcmpfUpdateList.clear();
            agcmpfUpdateList = null;
        }
        if (agcmpfInsertList != null) {
            agcmpfInsertList.clear();
            agcmpfInsertList = null;
        }
        if (agcmpfInvUpdateList != null) {
        	agcmpfInvUpdateList.clear();
        	agcmpfInvUpdateList = null;
        }
        if (ptrnpfInsertList != null) {
            ptrnpfInsertList.clear();
            ptrnpfInsertList = null;
        }
        if (trwppfInsertList != null) {
            trwppfInsertList.clear();
            trwppfInsertList = null;
        }
        if (acblpfInsertList != null) {
        	acblpfInsertList.clear();
        	acblpfInsertList = null;
        }
        if (acblpfUpdateList != null) {
        	acblpfUpdateList.clear();
        	acblpfUpdateList = null;
        } 
        if (acblpfInsertAll != null) {
        	acblpfInsertAll.clear();
        	acblpfInsertAll = null;
        }     
        if (acblpfLifrtrnList != null) {
        	acblpfLifrtrnList.clear();
        	acblpfLifrtrnList = null;
        }         
        if (covrUpdateList != null) {
        	covrUpdateList.clear();
        	covrUpdateList = null;
        }  
        if (covrRiskPremMap != null && !covrRiskPremMap.isEmpty()) rskppfDAO.updateRiskPremium(covrRiskPremMap);
        if (insertRiskPremList != null && !insertRiskPremList.isEmpty()) 
        	rskppfDAO.insertRecords(insertRiskPremList);
        if (rertUpdateList != null) {
        	rertUpdateList.clear();
        	rertUpdateList = null;
        }       
	}
	LOGGER.info("commit3500 end");
}

protected void rollback3600()
    {
        /*ROLLBACK*/
        /** Place any additional rollback processing in here.*/
        /*EXIT*/
    }

protected void a3000WriteLetter()
    {
        /* Read Table T6634 for get letter-type.                           */
        wsaaItemBatctrcde.set(batcdorrec.trcde);
        wsaaItemCnttype.set(linxpfResult.getChdrCnttype());
        a3020ReadTr384();
    }

    /**
    * <pre>
    *A3020-READ-T6634.                                        <PCPPRT>
    * </pre>
    */
protected void a3020ReadTr384()
 {
        if (tr384Map.containsKey(wsaaItemTr384)) {
            tr384rec.tr384Rec.set(StringUtil.rawToString(tr384Map.get(wsaaItemTr384).get(0).getGenarea()));
        } else {
            wsaaItemCnttype.set("***");
            if (tr384Map.containsKey(wsaaItemTr384)) {
                tr384rec.tr384Rec.set(StringUtil.rawToString(tr384Map.get(wsaaItemTr384).get(0).getGenarea()));
            } else {
                syserrrec.params.set("tr384:" + wsaaItemTr384);
                fatalError600();
            }
        }
        if (isEQ(tr384rec.letterType, SPACES)) {
            return;
        }
        /* Allocate next automatic Receipt Number. */
        alocnorec.function.set("NEXT ");
        alocnorec.prefix.set("RW");
        alocnorec.genkey.set(bsscIO.getInitBranch());
        alocnorec.company.set(bsprIO.getCompany());
      //  callProgram(Alocno.class, alocnorec.alocnoRec);
        alocnorec = AlocnoUtil.getInstance().process(alocnorec); // improvement
        if (isNE(alocnorec.statuz, varcom.oK)) {
            syserrrec.params.set(alocnorec.alocnoRec);
            syserrrec.statuz.set(alocnorec.statuz);
            fatalError600();
        }
        /* WRITE INTO TRWP FILE */
        Trwppf trwppf = new Trwppf();
        trwppf.setChdrcoy(linxpfResult.getChdrcoy());
        trwppf.setChdrnum(linxpfResult.getChdrnum());
        trwppf.setEffdate(bsscIO.getEffectiveDate().toInt());
        trwppf.setTranno(linxpfResult.getChdrTranno());
        trwppf.setValidflag("1");
        trwppf.setRdocpfx("RW");
        trwppf.setRdoccoy(bsprIO.getCompany().toString());
        trwppf.setRdocnum(alocnorec.alocNo.toString());
        trwppf.setOrigcurr(linxpfResult.getBillcurr());
        trwppf.setOrigamt(linxpfResult.getCbillamt());

        if (trwppfInsertList == null) {
            trwppfInsertList = new ArrayList<Trwppf>();
        }
        trwppfInsertList.add(trwppf);
        /* Get set-up parameter for call 'HLETRQS' */
        letrqstrec.statuz.set(SPACES);
        letrqstrec.requestCompany.set(linxpfResult.getChdrcoy());
        /* MOVE T6634-LETTER-TYPE TO LETRQST-LETTER-TYPE. <PCPPRT> */
        letrqstrec.letterType.set(tr384rec.letterType);
        letrqstrec.letterRequestDate.set(bsscIO.getEffectiveDate());
        /* MOVE 'RW' TO LETRQST-RDOCPFX. <PCPPRT> */
        letrqstrec.rdocpfx.set("CH");
        letrqstrec.rdoccoy.set(bsprIO.getCompany());
        /* MOVE ALNO-ALOC-NO TO LETRQST-RDOCNUM. <PCPPRT> */
        letrqstrec.rdocnum.set(linxpfResult.getChdrnum());
        letrqstrec.otherKeys.set(bsscIO.getLanguage());
        letrqstrec.clntcoy.set(linxpfResult.getChdrCowncoy());
        letrqstrec.clntnum.set(linxpfResult.getChdrCownnum());
        letrqstrec.chdrcoy.set(linxpfResult.getChdrcoy());
        letrqstrec.chdrnum.set(linxpfResult.getChdrnum());
        letrqstrec.tranno.set(linxpfResult.getChdrTranno());
        letrqstrec.branch.set(linxpfResult.getChdrCntbranch());
	letrqstrec.trcde.set(bprdIO.getAuthCode());
        letrqstrec.function.set("ADD");
        /* CALL 'HLETRQS' USING LETRQST-PARAMS. <PCPPRT> */
        callProgram(Letrqst.class, letrqstrec.params);
        if (isNE(letrqstrec.statuz, varcom.oK)) {
            syserrrec.params.set(letrqstrec.params);
            syserrrec.statuz.set(letrqstrec.statuz);
            fatalError600();
        }
    }

protected void close4000()
 {
		if(!noRecordFound){
	        taxdMap.clear();
	        taxdMap = null;
	        acblMap.clear();
	        acblMap = null;
	        acblGLMap.clear();
	        acblGLMap = null;
	        covrMap.clear();
	        covrMap = null;
	        agcmMap.clear();
	        agcmMap = null;
	        clrrMap.clear();
	        clrrMap = null;
	        t5645Map.clear();
	        t5645Map = null;
	        t3695Map.clear();
	        t3695Map = null;
	        t6687List.clear();
	        t6687List = null;
	        t5688List.clear();
	        t5688List = null;
	        t5679Map.clear();
	        t5679Map = null;
	        t5644List.clear();
	        t5644List = null;
	        t6654List.clear();
	        t6654List = null;
	        t5667Map.clear();
	        t5667Map = null;
	        t5671Map.clear();
	        t5671Map = null;
	        th605Map.clear();
	        th605Map = null;
	        t5687List.clear();
	        t5687List = null;
	        t5534List.clear();
	        t5534List = null;
	        t1688Map.clear();
	        t1688Map = null;
	        tr384Map.clear();
	        tr384Map = null;
	        tr52dMap.clear();
	        tr52dMap = null;
	        tr52eMap.clear();
	        tr52eMap = null;
	        tr2enMap.clear();
	        tr2enMap = null;
	        
	        clrrpf = null;
	        if(reinstflag){
				td5j2ListMap.clear();
				td5j2ListMap = null;
				
				td5j1ListMap.clear();
				td5j1ListMap = null;
			}
		}
        /* START */
        wsaaQcmdexc.set("DLTOVR FILE(LINXPF)");
        com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
        lsaaStatuz.set(varcom.oK);
        /* EXIT */
    }

protected void a100ReadZrap()
    {
        /*A100-START*/
        zrapIO.setAgntcoy(linxpfResult.getChdrcoy());
        zrapIO.setAgntnum(wsaaAgentKept);
        zrapIO.setFunction(varcom.begn);
        zrapIO.setStatuz(varcom.oK);
        while ( !(isEQ(zrapIO.getStatuz(),varcom.endp))) {

          //performance improvement --  atiwari23
            zrapIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
            zrapIO.setFitKeysSearch("AGNTNUM");
            a200SearchZrap();
        }

        comlinkrec.zcomcode.set(comlinkrec.method);
        comlinkrec.payamnt.set(wsaaCompayKept);
        /*A100-EXIT*/
    }

protected void a200SearchZrap()
    {
        a200Start();
    }

protected void a200Start()
    {
        SmartFileCode.execute(appVars, zrapIO);
        if (isNE(zrapIO.getStatuz(), varcom.oK)
        && isNE(zrapIO.getStatuz(), varcom.endp)) {
            syserrrec.params.set(zrapIO.getParams());
            syserrrec.statuz.set(zrapIO.getStatuz());
            comlinkrec.payamnt.set(ZERO);
            fatalError600();
        }
        if (isNE(zrapIO.getAgntnum(), wsaaAgentKept)
        || isEQ(zrapIO.getStatuz(), varcom.endp)) {
            comlinkrec.payamnt.set(ZERO);
            zrapIO.setStatuz(varcom.endp);
            return ;
        }
        if (isNE(zrapIO.getStatuz(), varcom.oK)) {
            comlinkrec.payamnt.set(ZERO);
            zrapIO.setStatuz(varcom.endp);
            return ;
        }
        if (isEQ(zrapIO.getReportag(), comlinkrec.agent)) {
            comlinkrec.zorcode.set(zrapIO.getZrorcode());
            comlinkrec.efdate.set(zrapIO.getEffdate());
            zrapIO.setStatuz(varcom.endp);
            return ;
        }
        zrapIO.setFunction(varcom.nextr);
    }

/**
* <pre>
* This paragraph is required before the UPDATE-SUSPENSE so that
* suspense will have enough money for the amount due. If there
* has extract suspense for the amount due then NEXT SENTENCE. If
* Adv Prem Deposit amount (APA) is required to pay the amount due
* then move DELT to withdraw money from APA and put into suspense.
* </pre>
*/
protected void a300UpdateApa() {
    if (isEQ(wsaaApaRequired, ZERO)) {
        return;
    }
    else {
        if (isGT(wsaaApaRequired, ZERO)) {
        	rlpdlonPojo.setChdrcoy(linxpfResult.getChdrcoy());
        	rlpdlonPojo.setChdrnum(linxpfResult.getChdrnum());
        	rlpdlonPojo.setFunction(varcom.delt.toString());
        	rlpdlonPojo.setPrmdepst(new BigDecimal(wsaaApaRequired.toString()));
        }
        else {
            return;
        }
    }
    a302Params();
}

protected void a302Params()
{
	rlpdlonPojo.setPstw("PSTW");
	rlpdlonPojo.setChdrcoy(linxpfResult.getChdrcoy());
	rlpdlonPojo.setChdrnum(linxpfResult.getChdrnum());
        wsaaRdockey.rdocRdocpfx.set("CH");
        wsaaRdockey.rdocRdoccoy.set(linxpfResult.getChdrcoy());
        wsaaRdockey.rdocRdocnum.set(linxpfResult.getChdrnum());
        wsaaRdockey.rdocTranseq.set(lifacmvrec.jrnseq);
	rlpdlonPojo.setDoctkey(wsaaRdockey.toString());
	rlpdlonPojo.setEffdate(linxpfResult.getInstfrom());
	rlpdlonPojo.setCurrency(linxpfResult.getBillcurr());
	rlpdlonPojo.setTranno(linxpfResult.getChdrTranno());
	rlpdlonPojo.setTranseq(lifacmvrec.jrnseq.toInt());
	rlpdlonPojo.setLongdesc(descpfT1688.getLongdesc());
	rlpdlonPojo.setLanguage(bsscIO.getLanguage().toString());
	rlpdlonPojo.setPrefix(batcdorrec.prefix.toString());
	rlpdlonPojo.setCompany(batcdorrec.company.toString());
	rlpdlonPojo.setBranch(batcdorrec.branch.toString());
	rlpdlonPojo.setActyear(batcdorrec.actyear.toInt());
	rlpdlonPojo.setActmonth(batcdorrec.actmonth.toInt());
	rlpdlonPojo.setTrcde(batcdorrec.trcde.toString());
	rlpdlonPojo.setAuthCode(batcdorrec.trcde.toString());
	rlpdlonPojo.setBatch(batcdorrec.batch.toString());
	rlpdlonPojo.setTime(varcom.vrcmTime.toInt());
	rlpdlonPojo.setDate_var(varcom.vrcmDate.toInt());
	rlpdlonPojo.setUser(0);
	rlpdlonPojo.setTermid(varcom.vrcmTermid.toString());
	
	rlpdlonUtils.callRlpdlon(rlpdlonPojo);
	if (isNE(rlpdlonPojo.getStatuz(), varcom.oK)) {
	    syserrrec.statuz.set(rlpdlonPojo.getStatuz());
	    syserrrec.params.set(rlpdlonPojo);
            fatalError600();
        }
        lifacmvrec.jrnseq.set(rlpdlonPojo.getTranseq());
    }

protected void a400CheckAgent()
 {
        wsaaAgtTerminateFlag.set("N");
        if (isLT(linxpfResult.getAglfDtetrm(), bsscIO.getEffectiveDate()) || isLT(linxpfResult.getAglfDteexp(), bsscIO.getEffectiveDate())
                || isGT(linxpfResult.getAglfDteapp(), bsscIO.getEffectiveDate())) {
            wsaaAgtTerminateFlag.set("Y");
        }
    }

protected void b100InitializeArrays()
 {
        /* B110-INIT */
        for (wsaaAgcmIx.set(1); !(isGT(wsaaAgcmIx, wsaaAgcmIxSize)); wsaaAgcmIx.add(1)) {
            wsaaAgcmChdrcoy[wsaaAgcmIx.toInt()].set(SPACES);
            wsaaAgcmChdrnum[wsaaAgcmIx.toInt()].set(SPACES);
            wsaaAgcmLife[wsaaAgcmIx.toInt()].set(SPACES);
            wsaaAgcmCoverage[wsaaAgcmIx.toInt()].set(SPACES);
            wsaaAgcmRider[wsaaAgcmIx.toInt()].set(SPACES);
            for (int wsaaAgcmIy = 1; wsaaAgcmIy <= wsaaAgcmIySize; wsaaAgcmIy++) {
                wsaaAgcmSeqno[wsaaAgcmIx.toInt()][wsaaAgcmIy].set(ZERO);
                wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy].set(ZERO);
                wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy].set(ZERO);
            }
        }
        /* B190-EXIT */
    }

protected void b200PremiumHistory(Covrpf covrpf)
 {
        List<Agcmpf> agcmList = agcmMap.get(covrpf.getChdrnum());
        //Ticket #PINNACLE-1237: AIA-33-Batch F9AUTOALOC and L2POLRNWL failed on Test-48
        if(CollectionUtils.isEmpty(agcmList)) {
            LOGGER.info("agcmList is empty for policy number {}",covrpf.getChdrnum());
            return;
        }
        for (Agcmpf agcmpf : agcmList) {
            if (agcmpf.getLife().equals(covrpf.getLife()) && agcmpf.getCoverage().equals(covrpf.getCoverage())
                    && agcmpf.getRider().equals(covrpf.getRider())) {
                /* Skip those irrelevant records */
                if (isNE(agcmpf.getOvrdcat(), "B") || isEQ(agcmpf.getPtdate(), ZERO) || isEQ(agcmpf.getEfdate(), ZERO)
                        || isEQ(agcmpf.getEfdate(), varcom.vrcmMaxDate)) {
                    continue;
                }
                b230Summary(agcmpf);
            }
        }
    }

protected void b230Summary(Agcmpf agcmpf)
 {
        boolean wsaaAgcmFound = false;
        int i;
        for (i=1; i<= wsaaAgcmIySize && isNE(
                wsaaAgcmEfdate[wsaaAgcmIx.toInt()][i], ZERO); i++) {
            if (isEQ(agcmpf.getSeqno(), wsaaAgcmSeqno[wsaaAgcmIx.toInt()][i])) {
                wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][i].add(PackedDecimalData.parseObject(agcmpf
                        .getAnnprem()));
                wsaaAgcmFound = true;
                break;
            }
        }
        if (!wsaaAgcmFound) {
            if (isGT(i, wsaaAgcmIySize)) {
                syserrrec.statuz.set(e103);
                fatalError600();
            }
            wsaaAgcmSeqno[wsaaAgcmIx.toInt()][i].set(agcmpf.getSeqno());
            wsaaAgcmEfdate[wsaaAgcmIx.toInt()][i].set(agcmpf.getEfdate());
            wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][i].set(agcmpf.getAnnprem());
        }
    }

protected void b300WriteArrays(Covrpf covrpf)
    {
        int wsaaAgcmIy =1;
        while(isNE(wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy], ZERO)) {
            if (isGT(wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy], linxpfResult.getInstfrom())) {
            	wsaaAgcmIy++;
                continue;
            }
            b330Writ(wsaaAgcmIy, covrpf);
            wsaaAgcmIy++;
        }
    }
protected void b330Writ(int wsaaAgcmIy, Covrpf covrpf)
 {
        initialize(datcon3rec.datcon3Rec);
        datcon3rec.intDate1.set(wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy]);
        datcon3rec.intDate2.set(linxpfResult.getInstfrom());
        datcon3rec.frequency.set("01");
        callProgram(Datcon3.class, datcon3rec.datcon3Rec);
        if (isNE(datcon3rec.statuz, varcom.oK)) {
            syserrrec.statuz.set(datcon3rec.statuz);
            syserrrec.params.set(datcon3rec.datcon3Rec);
            fatalError600();
        }
        if (isGTE(datcon3rec.freqFactor, 1)) {
            renPrem.setTrue();
        } else {
            firstPrem.setTrue();
        }
        if (isNE(wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy], ZERO)) {

            Zptnpf zptnpf = new Zptnpf();
            zptnpf.setChdrcoy(covrpf.getChdrcoy());
            zptnpf.setChdrnum(covrpf.getChdrnum());
            zptnpf.setLife(covrpf.getLife());
            zptnpf.setCoverage(covrpf.getCoverage());
            zptnpf.setRider(covrpf.getRider());
            zptnpf.setTranno(linxpfResult.getChdrTranno());
            zptnpf.setOrigamt(div(wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy], wsaaBillfq9).getbigdata());
            zptnpf.setTransCode(bprdIO.getAuthCode().toString());
            zptnpf.setEffdate(linxpfResult.getInstfrom());
            zptnpf.setInstfrom(linxpfResult.getInstfrom());
            zptnpf.setBillcd(linxpfResult.getBillcd());
            zptnpf.setInstto(linxpfResult.getLinsInstto());
            zptnpf.setTrandate(bsscIO.getEffectiveDate().toInt());
            zptnpf.setZprflg(wsaaPremType.toString());

            if (zptnInsertList == null) {
                zptnInsertList = new ArrayList<Zptnpf>();
            }
            zptnInsertList.add(zptnpf);
        }
    }

protected void b400LocatePremium(Agcmpf a)
    {
        boolean wsaaAgcmFound = false;
        wsaaAgcmPremium.set(ZERO);
        for (wsaaAgcmIa.set(1); !(isGT(wsaaAgcmIa, wsaaAgcmIxSize)||wsaaAgcmFound); wsaaAgcmIa.add(1)){
            if (isEQ(a.getChdrcoy(), wsaaAgcmChdrcoy[wsaaAgcmIa.toInt()])
            && isEQ(a.getChdrnum(), wsaaAgcmChdrnum[wsaaAgcmIa.toInt()])
            && isEQ(a.getLife(), wsaaAgcmLife[wsaaAgcmIa.toInt()])
            && isEQ(a.getCoverage(), wsaaAgcmCoverage[wsaaAgcmIa.toInt()])
            && isEQ(a.getRider(), wsaaAgcmRider[wsaaAgcmIa.toInt()])) {
                for (wsaaAgcmIb.set(1); !(isGT(wsaaAgcmIb, wsaaAgcmIySize)); wsaaAgcmIb.add(1)){
                    if (isEQ(a.getSeqno(), wsaaAgcmSeqno[wsaaAgcmIa.toInt()][wsaaAgcmIb.toInt()])) {
                        wsaaAgcmPremium.set(wsaaAgcmAnnprem[wsaaAgcmIa.toInt()][wsaaAgcmIb.toInt()]);
                        wsaaAgcmFound = true;
                        break;
                    }
                }
            }
        }
    }

protected void b500CallZorcompy()
 {
        zorlnkrec.function.set(SPACES);
        zorlnkrec.clawback.set(SPACES);
        zorlnkrec.agent.set(lifacmvrec.rldgacct);
        zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
        zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
        zorlnkrec.crtable.set(lifacmvrec.substituteCode[6]);
        zorlnkrec.ptdate.set(linxpfResult.getLinsInstto()); //IJS-46
        zorlnkrec.origcurr.set(lifacmvrec.origcurr);
        zorlnkrec.crate.set(lifacmvrec.crate);
        zorlnkrec.origamt.set(lifacmvrec.origamt);
        zorlnkrec.tranno.set(linxpfResult.getChdrTranno());
        zorlnkrec.trandesc.set(lifacmvrec.trandesc);
        zorlnkrec.tranref.set(lifacmvrec.tranref);
        zorlnkrec.genlcur.set(lifacmvrec.genlcur);
        zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
        zorlnkrec.termid.set(lifacmvrec.termid);
        zorlnkrec.cnttype.set(lifacmvrec.substituteCode[1]);
        zorlnkrec.batchKey.set(lifacmvrec.batckey);
        callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
        if (isNE(zorlnkrec.statuz, varcom.oK)) {
            syserrrec.statuz.set(zorlnkrec.statuz);
            syserrrec.params.set(zorlnkrec.zorlnkRec);
            fatalError600();
        }
    }


protected void b600GetTax()
 {
        /* Read table TR52D */
        Itempf tr52dItem = null;
        if (!tr52dMap.containsKey(linxpfResult.getChdrRegister())) {
            if (!tr52dMap.containsKey("***")) {
                syserrrec.params.set("tr52d:" + linxpfResult.getChdrRegister());
                fatalError600();
            } else {
                tr52dItem = tr52dMap.get("***").get(0);
            }
        } else {
            tr52dItem = tr52dMap.get(linxpfResult.getChdrRegister()).get(0);
        }
        if(tr52dItem != null){//IJTI-320 START
        	tr52drec.tr52dRec.set(StringUtil.rawToString(tr52dItem.getGenarea()));
        }//IJTI-320 END
        wsaaTotTax = BigDecimal.ZERO;
        //ILIFE-5798 Start
        if (taxdMap.containsKey(linxpfResult.getChdrnum())) {
	        List<Taxdpf> taxdpfList = taxdMap.get(linxpfResult.getChdrnum());
	        for (Taxdpf taxdpf : taxdpfList) {
	            b610ProcessTax(taxdpf);
	        }
        }
      //ILIFE-5798 End
    }

protected void b610ProcessTax(Taxdpf taxdpf)
 {
        if (taxdpf.getInstfrom() == linxpfResult.getInstfrom()) {
        	if(taxdpf.getPostflg() == null || taxdpf.getPostflg().trim().equals("") || isEQ(taxdpf.getPostflg(), SPACES)) {
                if (!"Y".equals(taxdpf.getTxabsind01())) {
                    wsaaTotTax = wsaaTotTax.add(taxdpf.getTaxamt01());
                }
                if (!"Y".equals(taxdpf.getTxabsind02())) {
                    wsaaTotTax = wsaaTotTax.add(taxdpf.getTaxamt02());
                }
                if (!"Y".equals(taxdpf.getTxabsind03())) {
                    wsaaTotTax = wsaaTotTax.add(taxdpf.getTaxamt03());
                }
            }
        }
    }


protected void b700PostTax()
 {
        if (taxdMap.containsKey(linxpfResult.getChdrnum())) {
            List<Taxdpf> taxdpfList = taxdMap.get(linxpfResult.getChdrnum());
            for (Taxdpf taxdpf : taxdpfList) {
                if (taxdpf.getInstfrom() == linxpfResult.getInstfrom() && taxdpf.getLife().equals(wsaaTxLife.toString())
                        && taxdpf.getCoverage().equals(wsaaTxCoverage.toString()) && taxdpf.getRider().equals(wsaaTxRider.toString())
                        && isEQ(taxdpf.getPlansfx(), wsaaTxPlansfx)) {
                    if (taxdpf.getPostflg() != null && !taxdpf.getPostflg().trim().equals("")) {
                        continue;
                    } else {
                        List<Itempf> tr52eItems = tr52eMap.get(subString(taxdpf.getTranref(), 1, 8));
                        for (Itempf item : tr52eItems) {
                            if (item.getItmfrm().compareTo(new BigDecimal(taxdpf.getEffdate())) == 0) {
                                tr52erec.tr52eRec.set(StringUtil.rawToString(item.getGenarea()));
                                break;
                            }
                        }
                        b710Start(taxdpf);
                    }
                }
            }
        }
    }

protected void b710Start(Taxdpf taxdpf)
    {
 
        /* Call tax subroutine                                             */
        initialize(txcalcrec.linkRec);
        txcalcrec.function.set("POST");
        txcalcrec.statuz.set(varcom.oK);
        txcalcrec.chdrcoy.set(taxdpf.getChdrcoy());
        txcalcrec.chdrnum.set(taxdpf.getChdrnum());
        txcalcrec.life.set(taxdpf.getLife());
        txcalcrec.coverage.set(taxdpf.getCoverage());
        txcalcrec.rider.set(taxdpf.getRider());
        txcalcrec.planSuffix.set(taxdpf.getPlansfx());
        txcalcrec.crtable.set(wsaaTxCrtable);
        if (isEQ(wsaaTxCrtable, SPACES)) {
            txcalcrec.cntTaxInd.set("Y");
        }
        txcalcrec.cnttype.set(linxpfResult.getChdrCnttype());
        txcalcrec.register.set(linxpfResult.getChdrRegister());
        txcalcrec.taxrule.set(subString(taxdpf.getTranref(), 1, 8));
        txcalcrec.rateItem.set(subString(taxdpf.getTranref(), 9, 8));
        txcalcrec.amountIn.set(taxdpf.getBaseamt());
        txcalcrec.effdate.set(taxdpf.getEffdate());
        txcalcrec.transType.set(taxdpf.getTrantype());
        txcalcrec.taxType[1].set(taxdpf.getTxtype01());
        txcalcrec.taxType[2].set(taxdpf.getTxtype02());
        txcalcrec.taxAmt[1].set(taxdpf.getTaxamt01());
        txcalcrec.taxAmt[2].set(taxdpf.getTaxamt02());
        txcalcrec.taxAbsorb[1].set(taxdpf.getTxabsind01());
        txcalcrec.taxAbsorb[2].set(taxdpf.getTxabsind02());
        txcalcrec.batckey.set(batcdorrec.batchkey);
        txcalcrec.ccy.set(linxpfResult.getChdrCntcurr());
        txcalcrec.language.set(bsscIO.getLanguage());
        txcalcrec.jrnseq.set(lifacmvrec.jrnseq);
        txcalcrec.tranno.set(linxpfResult.getChdrTranno());
        callProgram(tr52drec.txsubr, txcalcrec.linkRec);
        if (isNE(txcalcrec.statuz, varcom.oK)) {
            syserrrec.params.set(txcalcrec.linkRec);
            syserrrec.statuz.set(txcalcrec.statuz);
            fatalError600();
        }
        lifacmvrec.jrnseq.set(txcalcrec.jrnseq);
        Taxdpf t = new Taxdpf();
        t.setUnique_number(taxdpf.getUnique_number());
        t.setPostflg("P");
        if(taxdUpdateList == null){
            taxdUpdateList = new ArrayList<Taxdpf>();
        }
        taxdUpdateList.add(t);
    }

protected void c000CallRounding(ZrdecplcPojo zrdecplcPojo)
    {
        /*C100-CALL*/
		//ILB-441 starts
		//ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
		zrdecplcPojo.setCompany(bsprIO.getCompany().toString());
		zrdecplcPojo.setBatctrcde(batcdorrec.trcde.toString());
		zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
		
		if (!Varcom.oK.toString().equals(zrdecplcPojo.getStatuz())) {
			syserrrec.statuz.set(zrdecplcPojo.getStatuz());
			syserrrec.params.set(zrdecplcPojo.toString());
			fatalError600();
		}
        /*zrdecplrec.function.set(SPACES);
        zrdecplrec.company.set(bsprIO.getCompany());
        zrdecplrec.statuz.set(varcom.oK);
        zrdecplrec.batctrcde.set(batcdorrec.trcde);
        callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
        if (isNE(zrdecplrec.statuz, varcom.oK)) {
            syserrrec.statuz.set(zrdecplrec.statuz);
            syserrrec.params.set(zrdecplrec.zrdecplRec);
            fatalError600();
        }*/
        /*C900-EXIT*/
		//ILB-441 ends
    }

/*
 * Class transformed  from Data Structure WSAA-T5667-ARRAY--INNER
 */
private static final class WsaaT5667ArrayInner {

        /* WSAA-T5667-ARRAY */
    private FixedLengthStringData[] wsaaT5667Rec = FLSInittedArray (10, 492);
    private FixedLengthStringData[] wsaaT5667Key = FLSDArrayPartOfArrayStructure(7, wsaaT5667Rec, 0);
    private FixedLengthStringData[] wsaaT5667Trcde = FLSDArrayPartOfArrayStructure(4, wsaaT5667Key, 0, SPACES);
    private FixedLengthStringData[] wsaaT5667Curr = FLSDArrayPartOfArrayStructure(3, wsaaT5667Key, 4, SPACES);
    private FixedLengthStringData[] wsaaT5667ShortfallInd = FLSDArrayPartOfArrayStructure(1, wsaaT5667Rec, 7);
    private FixedLengthStringData[] wsaaT5667Sfind = FLSDArrayPartOfArrayStructure(1, wsaaT5667ShortfallInd, 0, SPACES);
    private FixedLengthStringData[] wsaaT5667Data = FLSDArrayPartOfArrayStructure(484, wsaaT5667Rec, 8);
    private FixedLengthStringData[] wsaaT5667Freqs = FLSDArrayPartOfArrayStructure(22, wsaaT5667Data, 0);
    private FixedLengthStringData[][] wsaaT5667Freq = FLSDArrayPartOfArrayStructure(11, 2, wsaaT5667Freqs, 0);
    private FixedLengthStringData[] wsaaT5667MaxAmounts = FLSDArrayPartOfArrayStructure(187, wsaaT5667Data, 22);
    private ZonedDecimalData[][] wsaaT5667MaxAmount = ZDArrayPartOfArrayStructure(11, 17, 2, wsaaT5667MaxAmounts, 0);
    private FixedLengthStringData[] wsaaT5667Maxamts = FLSDArrayPartOfArrayStructure(187, wsaaT5667Data, 209);
    private ZonedDecimalData[][] wsaaT5667Maxamt = ZDArrayPartOfArrayStructure(11, 17, 2, wsaaT5667Maxamts, 0);
    private FixedLengthStringData[] wsaaT5667Prmtols = FLSDArrayPartOfArrayStructure(44, wsaaT5667Data, 396);
    private ZonedDecimalData[][] wsaaT5667Prmtol = ZDArrayPartOfArrayStructure(11, 4, 2, wsaaT5667Prmtols, 0);
    private FixedLengthStringData[] wsaaT5667Prmtolns = FLSDArrayPartOfArrayStructure(44, wsaaT5667Data, 440);
    private ZonedDecimalData[][] wsaaT5667Prmtoln = ZDArrayPartOfArrayStructure(11, 4, 2, wsaaT5667Prmtolns, 0);
}
public List<Acblpf> adjustSacsbalforDuplicates(List<Acblpf> insertAcblpfList) { //PINNACLE-2450

Map<String,Acblpf> mapAcbl=new HashMap<String,Acblpf>(insertAcblpfList.size());
// Create a new ArrayList
List<Acblpf> newAcblpfList = new ArrayList<Acblpf>();
// Traverse through the first list
for (Acblpf acblpf : insertAcblpfList) {
StringBuffer acblkey=new StringBuffer();
acblkey.append(acblpf.getRldgcoy()+"_"+acblpf.getRldgacct()+"_"+acblpf.getSacscode()+"_"+acblpf.getSacstyp()+"_"+acblpf.getOrigcurr());
if (mapAcbl!=null && mapAcbl.containsKey(acblkey.toString())) {
Acblpf existingacblpf=mapAcbl.get(acblkey.toString());
existingacblpf.setSacscurbal(existingacblpf.getSacscurbal().add(acblpf.getSacscurbal()));
}else {
newAcblpfList.add(acblpf);
mapAcbl.put(acblkey.toString(),acblpf);
}
}
return newAcblpfList;//PINNACLE-2450
}
} 
