/*
 * File: T6658pt.java
 * Date: 30 August 2009 2:29:25
 * Author: Quipoz Limited
 * 
 * Class transformed from T6658PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6658.
*
*
*****************************************************************
* </pre>
*/
public class T6658pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6658rec t6658rec = new T6658rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6658pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6658rec.t6658Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		generalCopyLinesInner.fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		generalCopyLinesInner.fieldNo006.set(datcon1rec.extDate);
		generalCopyLinesInner.fieldNo007.set(t6658rec.subprog);
		generalCopyLinesInner.fieldNo008.set(t6658rec.billfreq);
		generalCopyLinesInner.fieldNo009.set(t6658rec.simpleInd);
		generalCopyLinesInner.fieldNo010.set(t6658rec.premsubr);
		generalCopyLinesInner.fieldNo011.set(t6658rec.compoundInd);
		generalCopyLinesInner.fieldNo012.set(t6658rec.addnew);
		generalCopyLinesInner.fieldNo013.set(t6658rec.optind);
		generalCopyLinesInner.fieldNo014.set(t6658rec.addexist);
		generalCopyLinesInner.fieldNo015.set(t6658rec.manopt);
		generalCopyLinesInner.fieldNo016.set(t6658rec.trevsub);
		generalCopyLinesInner.fieldNo017.set(t6658rec.minctrm);
		generalCopyLinesInner.fieldNo018.set(t6658rec.minpcnt);
		generalCopyLinesInner.fieldNo019.set(t6658rec.agemax);
		generalCopyLinesInner.fieldNo020.set(t6658rec.maxpcnt);
		generalCopyLinesInner.fieldNo021.set(t6658rec.fixdtrm);
		generalCopyLinesInner.fieldNo022.set(t6658rec.comind);
		generalCopyLinesInner.fieldNo023.set(t6658rec.statind);
		generalCopyLinesInner.fieldNo024.set(t6658rec.nocommind);
		generalCopyLinesInner.fieldNo025.set(t6658rec.nostatin);
		generalCopyLinesInner.fieldNo026.set(t6658rec.maxRefusals);
		generalCopyLinesInner.fieldNo027.set(t6658rec.refusalPeriod);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine017);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine001, 28, FILLER).init("Automatic Increase Coverage Rules          S6658");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(74);
	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 0, FILLER).init("Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 9);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 10, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 20);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 25, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 33);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 44);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(44);
	private FixedLengthStringData filler7 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 14);
	private FixedLengthStringData filler8 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 24, FILLER).init("    To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 34);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(48);
	private FixedLengthStringData filler9 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Anniversary Processing Subroutine. .");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine004, 38);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(22);
	private FixedLengthStringData filler10 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine005, 0, FILLER).init(" Increase Processing:-");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(73);
	private FixedLengthStringData filler11 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine006, 0, FILLER).init(" Frequency Factor . . . . . . . . . .");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 38);
	private FixedLengthStringData filler12 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine006, 40, FILLER).init(SPACES);
	private FixedLengthStringData filler13 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine006, 51, FILLER).init("Simple . . . . . . .");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 72);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(73);
	private FixedLengthStringData filler14 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine007, 0, FILLER).init(" Subroutine . . . . . . . . . . . . .");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 38);
	private FixedLengthStringData filler15 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 46, FILLER).init(SPACES);
	private FixedLengthStringData filler16 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine007, 51, FILLER).init("Compound . . . . . .");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 72);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(73);
	private FixedLengthStringData filler17 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine008, 0, FILLER).init(" New component  . . . . . . . . . . .");
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 38);
	private FixedLengthStringData filler18 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine008, 39, FILLER).init(SPACES);
	private FixedLengthStringData filler19 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine008, 51, FILLER).init("Optional . . . . . .");
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 72);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(73);
	private FixedLengthStringData filler20 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine009, 0, FILLER).init(" Add to Existing Component  . . . . .");
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 38);
	private FixedLengthStringData filler21 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 39, FILLER).init(SPACES);
	private FixedLengthStringData filler22 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine009, 51, FILLER).init("Mandatory  . . . . .");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 72);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(48);
	private FixedLengthStringData filler23 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine010, 0, FILLER).init(" Reversal subroutine  . . . . . . . .");
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine010, 38);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(78);
	private FixedLengthStringData filler24 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine011, 0, FILLER).init(" Minimum Term to Cessation  . . . . .");
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 38).setPattern("ZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 41, FILLER).init(SPACES);
	private FixedLengthStringData filler26 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine011, 51, FILLER).init("Minimum Percentage .");
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 72).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(78);
	private FixedLengthStringData filler27 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine012, 0, FILLER).init(" Maximum Age  . . . . . . . . . . . .");
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 38).setPattern("ZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 41, FILLER).init(SPACES);
	private FixedLengthStringData filler29 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine012, 51, FILLER).init("Maximum Percentage .");
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 72).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(41);
	private FixedLengthStringData filler30 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine013, 0, FILLER).init(" Fixed No. of Increases . . . . . . .");
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 38).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(73);
	private FixedLengthStringData filler31 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine014, 0, FILLER).init(" Commission . . . . . . . . . . . . .");
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 38);
	private FixedLengthStringData filler32 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine014, 39, FILLER).init(SPACES);
	private FixedLengthStringData filler33 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine014, 51, FILLER).init("Statistics . . . . .");
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 72);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(73);
	private FixedLengthStringData filler34 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine015, 0, FILLER).init(" No Commission  . . . . . . . . . . .");
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 38);
	private FixedLengthStringData filler35 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine015, 39, FILLER).init(SPACES);
	private FixedLengthStringData filler36 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine015, 51, FILLER).init("No Statistics  . . .");
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 72);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(40);
	private FixedLengthStringData filler37 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine016, 0, FILLER).init(" Maximum No. of Refusals allowable. .");
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 38).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(41);
	private FixedLengthStringData filler38 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine017, 0, FILLER).init(" Permitted Refusal Period (Days). . .");
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine017, 38).setPattern("ZZZ");
	}
}
