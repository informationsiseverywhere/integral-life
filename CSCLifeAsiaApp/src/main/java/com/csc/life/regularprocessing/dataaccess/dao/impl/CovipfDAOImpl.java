package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.CovipfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Covipf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class CovipfDAOImpl extends BaseDAOImpl<Covipf> implements CovipfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(CovipfDAOImpl.class);

    public List<Covipf> searchCoviRecord(String tableId, String memName, int batchExtractSize, int batchID) {
        StringBuilder sqlCoviSelect = new StringBuilder();
        sqlCoviSelect.append(" SELECT * FROM (");
        sqlCoviSelect
                .append("  SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,STATCODE,PSTATCODE,CRRCD,PRMCUR,CRTABLE,SINGP,RCESDTE,SUMINS,MORTCLS,INDXIN,INSTPREM,ZBINSTPREM,ZLINSTPREM,CPIDTE ");
        sqlCoviSelect.append("   ,FLOOR((ROW_NUMBER()OVER( ORDER BY CHDRCOY ASC, CHDRNUM ASC)-1)/?) BATCHNUM FROM ");
        sqlCoviSelect.append(tableId);
        sqlCoviSelect.append("   WHERE MEMBER_NAME = ? ");
        sqlCoviSelect.append(" ) MAIN WHERE batchnum = ? ");

        PreparedStatement psCoviSelect = getPrepareStatement(sqlCoviSelect.toString());
        ResultSet sqlcovipf1rs = null;
        List<Covipf> covipfList = new ArrayList<>();
        try {
            psCoviSelect.setInt(1, batchExtractSize);
            psCoviSelect.setString(2, memName);
            psCoviSelect.setInt(3, batchID);

            sqlcovipf1rs = executeQuery(psCoviSelect);
            while (sqlcovipf1rs.next()) {
                Covipf covipf = new Covipf();
                covipf.setUniqueNumber(sqlcovipf1rs.getLong("UNIQUE_NUMBER"));
                covipf.setChdrcoy(sqlcovipf1rs.getString("Chdrcoy"));
                covipf.setChdrnum(sqlcovipf1rs.getString("Chdrnum"));
                covipf.setLife(sqlcovipf1rs.getString("Life"));
                covipf.setJlife(sqlcovipf1rs.getString("Jlife"));
                covipf.setCoverage(sqlcovipf1rs.getString("Coverage"));
                covipf.setRider(sqlcovipf1rs.getString("Rider"));
                covipf.setPlanSuffix(sqlcovipf1rs.getInt("PLNSFX"));
                covipf.setValidflag(sqlcovipf1rs.getString("Validflag"));
                covipf.setStatcode(sqlcovipf1rs.getString("Statcode"));
                covipf.setPstatcode(sqlcovipf1rs.getString("Pstatcode"));
                covipf.setCrrcd(sqlcovipf1rs.getInt("Crrcd"));
                covipf.setPremCurrency(sqlcovipf1rs.getString("PRMCUR"));
                covipf.setCrtable(sqlcovipf1rs.getString("Crtable"));
                covipf.setSingp(sqlcovipf1rs.getBigDecimal("Singp"));
                covipf.setRiskCessDate(sqlcovipf1rs.getInt("rcesdte"));
                covipf.setSumins(sqlcovipf1rs.getBigDecimal("Sumins"));
                covipf.setMortcls(sqlcovipf1rs.getString("Mortcls"));
                covipf.setIndexationInd(sqlcovipf1rs.getString("indxin"));
                covipf.setInstprem(sqlcovipf1rs.getBigDecimal("Instprem"));
                covipf.setZbinstprem(sqlcovipf1rs.getBigDecimal("Zbinstprem"));
                covipf.setZlinstprem(sqlcovipf1rs.getBigDecimal("Zlinstprem"));
                covipf.setCpiDate(sqlcovipf1rs.getInt("cpidte"));
                covipfList.add(covipf);
            }
        } catch (SQLException e) {
            LOGGER.error("searchCoviRecord()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(psCoviSelect, sqlcovipf1rs);
        }
        return covipfList;

    }

}