package com.csc.life.regularprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6658screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 1, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6658ScreenVars sv = (S6658ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6658screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6658ScreenVars screenVars = (S6658ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.subprog.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.simpleInd.setClassString("");
		screenVars.premsubr.setClassString("");
		screenVars.compoundInd.setClassString("");
		screenVars.addnew.setClassString("");
		screenVars.optind.setClassString("");
		screenVars.addexist.setClassString("");
		screenVars.manopt.setClassString("");
		screenVars.trevsub.setClassString("");
		screenVars.minctrm.setClassString("");
		screenVars.minpcnt.setClassString("");
		screenVars.agemax.setClassString("");
		screenVars.maxpcnt.setClassString("");
		screenVars.fixdtrm.setClassString("");
		screenVars.comind.setClassString("");
		screenVars.statind.setClassString("");
		screenVars.nocommind.setClassString("");
		screenVars.nostatin.setClassString("");
		screenVars.maxRefusals.setClassString("");
		screenVars.refusalPeriod.setClassString("");
		screenVars.incrFlg.setClassString("");
	}

/**
 * Clear all the variables in S6658screen
 */
	public static void clear(VarModel pv) {
		S6658ScreenVars screenVars = (S6658ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.subprog.clear();
		screenVars.billfreq.clear();
		screenVars.simpleInd.clear();
		screenVars.premsubr.clear();
		screenVars.compoundInd.clear();
		screenVars.addnew.clear();
		screenVars.optind.clear();
		screenVars.addexist.clear();
		screenVars.manopt.clear();
		screenVars.trevsub.clear();
		screenVars.minctrm.clear();
		screenVars.minpcnt.clear();
		screenVars.agemax.clear();
		screenVars.maxpcnt.clear();
		screenVars.fixdtrm.clear();
		screenVars.comind.clear();
		screenVars.statind.clear();
		screenVars.nocommind.clear();
		screenVars.nostatin.clear();
		screenVars.maxRefusals.clear();
		screenVars.refusalPeriod.clear();
		screenVars.incrFlg.clear();
	}
}
