/*
 * File: Br525.java
 * Date: 29 August 2009 22:15:29
 * Author: Quipoz Limited
 * 
 * Class transformed from BR525.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.contractservicing.dataaccess.dao.TpdbpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Tpdbpf;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.general.tablestructures.T5611rec;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.CnfsTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.CovrClmDAO;
import com.csc.life.terminationclaims.dataaccess.dao.HsudpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.SurdpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.impl.CovrClmDAOImpl;
import com.csc.life.terminationclaims.dataaccess.dao.impl.HsudpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.dao.impl.SurdpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.model.CovrClm;
import com.csc.life.terminationclaims.dataaccess.model.Hsudpf;
import com.csc.life.terminationclaims.dataaccess.model.Surdpf;
import com.csc.life.terminationclaims.procedures.P5084at;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.recordstructures.SurtaxRec;
import com.csc.life.terminationclaims.tablestructures.Tr691rec;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*           BATCH AUTOMATIC NONFORFEITURE SURRENDER
*
*                                                                     *
* </pre>
*/
public class Br525 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR525");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).init(0);
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).setUnsigned();

	private FixedLengthStringData wsaaInit = new FixedLengthStringData(26);
	private FixedLengthStringData wsaaDate = new FixedLengthStringData(10).isAPartOf(wsaaInit, 0);
	private FixedLengthStringData wsaaTime = new FixedLengthStringData(10).isAPartOf(wsaaInit, 10);
	private FixedLengthStringData wsaaVrcmUser = new FixedLengthStringData(6).isAPartOf(wsaaInit, 20);
	private String wsaaMaturityFlag = "";

	private FixedLengthStringData wsaaT5611Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5611Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5611Item, 0);
	private FixedLengthStringData wsaaT5611Pstatcode = new FixedLengthStringData(2).isAPartOf(wsaaT5611Item, 4);
	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaT5611Item, 6, FILLER).init(SPACES);
	private FixedLengthStringData wsaaStoredLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredRider = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredCurrency = new FixedLengthStringData(3).init(SPACES);
	private PackedDecimalData wsaaHeldCurrLoans = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaClamamt = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaOtheradjst = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaLoanValue = new PackedDecimalData(18, 2).init(0);
	private FixedLengthStringData wsaaSurrenderRecStore = new FixedLengthStringData(150);

	private ZonedDecimalData wsaaCurrencySwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator detailsSameCurrency = new Validator(wsaaCurrencySwitch, "0");
	private Validator detailsDifferent = new Validator(wsaaCurrencySwitch, "1");
	private Validator totalCurrDifferent = new Validator(wsaaCurrencySwitch, "2");
	private PackedDecimalData wsaaActvalue = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaTaxAmt = new PackedDecimalData(18, 2).init(0);
	private String wsaaValidStatus = "";
	private FixedLengthStringData wsaaRacdChdrcoy = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaRacdChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaRacdLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRacdCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRacdRider = new FixedLengthStringData(2).init(SPACES);

	private ZonedDecimalData wsaaSwitch2 = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator firstTime = new Validator(wsaaSwitch2, "1");

	private FixedLengthStringData wsaaFirstTimeFlag = new FixedLengthStringData(1).init("Y");
	private Validator firstTimeThrough = new Validator(wsaaFirstTimeFlag, "Y");
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaActualTot = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaPenaltyTot = new PackedDecimalData(18, 2).init(0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).init(SPACES);
	private String wsaaNoPrice = "";
	private FixedLengthStringData wsaaSumFlag = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
		/* ERRORS */
	private static final String f294 = "F294";

		/* INDIC-AREA */
	private Indicator[] indicTable = IndicatorInittedArray(99, 1);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
	private CnfsTableDAM cnfsIO = new CnfsTableDAM();
	private SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
	private T5679rec t5679rec = new T5679rec();
	private T5611rec t5611rec = new T5611rec();
	private Tr691rec tr691rec = new Tr691rec();
	private Totloanrec totloanrec = new Totloanrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaAtmodRecInner wsaaAtmodRecInner = new WsaaAtmodRecInner();
	private ExternalisedRules er = new ExternalisedRules();
	
	
	//ILIFE-4406 by wii31	
	private Map<String, List<Itempf>> t5611ListMap;
	private Map<String, List<Itempf>> tr691ListMap;
	private Map<String, List<Itempf>> t5679ListMap;
	private Map<String, List<Itempf>> t5687ListMap;
	private Map<String, List<Itempf>> t6598ListMap;
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private TpdbpfDAO tpdbpfDAO =  getApplicationContext().getBean("tpdbpfDAO", TpdbpfDAO.class);
	private CovrClmDAO covrClmDAO =  getApplicationContext().getBean("covrClmDAO", CovrClmDAOImpl.class);
	private SurdpfDAO surdpfDAO =  getApplicationContext().getBean("surdpfDAO", SurdpfDAOImpl.class);
	private HsudpfDAO hsudpfDAO =  getApplicationContext().getBean("hsudpfDAO", HsudpfDAOImpl.class);
	private FixedLengthStringData wsaaTr691Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr691Cnttype = new FixedLengthStringData(1).isAPartOf(wsaaTr691Key, 0);
	private FixedLengthStringData wsaaTr691Cntcurr = new FixedLengthStringData(4).isAPartOf(wsaaTr691Key, 1);
	private CovrClm covrclm = new CovrClm();
	private Surdpf surdclm = new Surdpf();
	private List<Hsudpf> hsudpfList = new ArrayList<Hsudpf>();
	private List<Surdpf> surdpfList = new ArrayList<Surdpf>();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		read3394
	}

	public Br525() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

//ILIFE-4406 by wii31
private void readSmartTables(String company){
	t5611ListMap = itemDAO.loadSmartTable("IT", "T5611");
	tr691ListMap = itemDAO.loadSmartTable("IT", company, "TR691");
	t5679ListMap = itemDAO.loadSmartTable("IT", company, "T5679");	
	t5687ListMap = itemDAO.loadSmartTable("IT", company, "T5687");
	t6598ListMap = itemDAO.loadSmartTable("IT", company, "T6598");	
}
protected void initialise1000()
	{
		/*INITIALISE*/
		varcom.vrcmDate.set(getCobolDate());
		wsaaInit.set(bsscIO.getDatimeInit());
		varcom.vrcmUser.set(wsaaVrcmUser);
		wsspEdterror.set(varcom.oK);
		cnfsIO.setDataKey(SPACES);
		cnfsIO.setFunction(varcom.begn);
		cnfsIO.setFormat(formatsInner.cnfsrec);
		//ILIFE-4406 by wii31
		readSmartTables(bsprIO.getCompany().toString());
		/*EXIT*/
	}
protected void readFile2000()
	{
		/*READ-FILE*/
		SmartFileCode.execute(appVars, cnfsIO);
		if (isNE(cnfsIO.getStatuz(), varcom.oK)
		&& isNE(cnfsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(cnfsIO.getParams());
			syserrrec.statuz.set(cnfsIO.getStatuz());
			fatalError600();
		}
		if (isEQ(cnfsIO.getStatuz(), varcom.endp)) {
			wsspEdterror.set(varcom.endp);
		}
		/*EXIT*/
	}

protected void edit2500()
	{
		wsspEdterror.set(varcom.oK);
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(bsprIO.getCompany());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(cnfsIO.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void update3000()
	{
		holdCnfs3010();
		read3020();
	}

protected void holdCnfs3010()
	{
		cnfsIO.setFunction(varcom.readh);
		cnfsIO.setFormat(formatsInner.cnfsrec);
		SmartFileCode.execute(appVars, cnfsIO);
		if (isNE(cnfsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cnfsIO.getParams());
			syserrrec.statuz.set(cnfsIO.getStatuz());
			fatalError600();
		}
		cnfsIO.setValidflag("2");
		cnfsIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, cnfsIO);
		if (isNE(cnfsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cnfsIO.getParams());
			syserrrec.statuz.set(cnfsIO.getStatuz());
			fatalError600();
		}
		cnfsIO.setFunction(varcom.nextr);
		chdrsurIO.setDataKey(SPACES);
		chdrsurIO.setChdrcoy(cnfsIO.getChdrcoy());
		chdrsurIO.setChdrnum(cnfsIO.getChdrnum());
		chdrsurIO.setFunction(varcom.readr);
		chdrsurIO.setFormat(formatsInner.chdrsurrec);
		SmartFileCode.execute(appVars, chdrsurIO);
		if (isNE(chdrsurIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrsurIO.getParams());
			syserrrec.statuz.set(chdrsurIO.getStatuz());
			fatalError600();
		}
		compute(wsaaTranno, 0).set(add(1, chdrsurIO.getTranno()));
	}

	protected void read3020(){
		//ILIFE-4406 by liwei
		List<CovrClm> covrpfList = covrClmDAO.searchCovrclmRecord(chdrsurIO.getChdrcoy().toString(), chdrsurIO.getChdrnum().toString());
		if(covrpfList != null && covrpfList.size() > 0){
			for(int i = 0; i < covrpfList.size(); i++){
				covrclm = covrpfList.get(i);
				processComponents3300();
			}
		}else{
			return;
		}
		// IVE-801 - RUL Product - Surrender Tax Calculation - Integration with latest PA compatible models - START
		/*  Computation of tax amount to be imposed.                       */
		/*ILIFE-2397 Start */
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("SURTAX")))
		{
			getTaxAmount3392();
		}
		else
		{
			SurtaxRec surtaxRec = new SurtaxRec();	
			surtaxRec.cnttype.set(chdrsurIO.cnttype);
			surtaxRec.cntcurr.set(chdrsurIO.cntcurr);
			surtaxRec.effectiveDate.set(srcalcpy.effdate);
			surtaxRec.occDate.set(srcalcpy.effdate);
			surtaxRec.actualAmount.set(wsaaActualTot);						
			callProgram("SURTAX", surtaxRec.surtaxRec);			
			wsaaTaxAmt.set(surtaxRec.taxAmount);
		}
		/*ILIFE-2397 End */
		// IVE-801 - RUL Product - Surrender Tax Calculation - Integration with latest PA compatible models - END		
		
		
		/*  Get the value of any Loans held against this component.        */
		getLoanDetails3393();
		getPolicyDebt3394();
		/*  call the at module ATREQ*/
		surhclmIO.setDataArea(SPACES);
		surhclmIO.setPlanSuffix(wsaaPlanSuffix);
		surhclmIO.setChdrcoy(chdrsurIO.getChdrcoy());
		surhclmIO.setChdrnum(chdrsurIO.getChdrnum());
		surhclmIO.setLife(covrclm.getLife());
		surhclmIO.setJlife(covrclm.getJlife());
		surhclmIO.setCurrcd(chdrsurIO.getCntcurr());
		surhclmIO.setEffdate(cnfsIO.getEffdate());
		surhclmIO.setTranno(wsaaTranno);
		surhclmIO.setCnttype(chdrsurIO.getCnttype());
		surhclmIO.setReasoncd(SPACES);
		surhclmIO.setResndesc(SPACES);
		surhclmIO.setPolicyloan(wsaaHeldCurrLoans);
		surhclmIO.setTdbtamt(wsaaAtmodRecInner.wsaaTdbtamt);
		surhclmIO.setZrcshamt(wsaaAtmodRecInner.wsaaZrcshamt);
		surhclmIO.setOtheradjst(wsaaOtheradjst);
		surhclmIO.setTaxamt(wsaaTaxAmt);
		surhclmIO.setFunction(varcom.writr);
		surhclmIO.setFormat(formatsInner.surhclmrec);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(surhclmIO.getParams());
			fatalError600();
		}
		wsaaAtmodRecInner.wsaaAtmodRec.set(SPACES);
		wsaaAtmodRecInner.wsaaAtdmBatchKey.set(batcdorrec.batchkey);
		wsaaAtmodRecInner.wsaaAtdmLanguage.set(bsscIO.getLanguage());
		wsaaAtmodRecInner.wsaaAtdmCompany.set(bsprIO.getCompany());
		wsaaAtmodRecInner.wsaaPrimaryChdrnum.set(chdrsurIO.getChdrnum());
		wsaaAtmodRecInner.wsaaTransDate.set(varcom.vrcmDate);
		wsaaAtmodRecInner.wsaaTransTime.set(varcom.vrcmTime);
		wsaaAtmodRecInner.wsaaTransUser.set(varcom.vrcmUser);
		wsaaAtmodRecInner.wsaaTransTermid.set(varcom.vrcmTerm);
		wsaaAtmodRecInner.wsaaAtdmStatuz.set("****");
		callProgram(P5084at.class, wsaaAtmodRecInner.wsaaAtmodRec);
		if (isNE(wsaaAtmodRecInner.wsaaAtdmStatuz, varcom.oK)) {
			syserrrec.params.set(wsaaAtmodRecInner.wsaaAtmodRec);
			fatalError600();
		}
	}

protected void processComponents3300()
	{
		/* Read table T5687 to find the surrender calculation subroutine*/
		wsaaCrtable.set(covrclm.getCrtable());
		obtainSurrenderCalc3350();
		srcalcpy.currcode.set(covrclm.getPremCurrency());
		validateStatusesWp3360();
		if (isEQ(wsaaValidStatus, "Y")
		&& isLT(covrclm.getRiskCessDate(), cnfsIO.getEffdate())) {
			/* Nothing to do. */
		}
		srcalcpy.endf.set(SPACES);
		srcalcpy.ptdate.set(chdrsurIO.getPtdate());
		srcalcpy.planSuffix.set(covrclm.getPlanSuffix());
		if (isNE(covrclm.getRider(), "00")
		&& isNE(covrclm.getRider(), "  ")) {
			srcalcpy.currcode.set(covrclm.getPremCurrency());
		}
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.effdate.set(cnfsIO.getEffdate());
		srcalcpy.chdrChdrcoy.set(covrclm.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrclm.getChdrnum());
		srcalcpy.lifeLife.set(covrclm.getLife());
		srcalcpy.lifeJlife.set(covrclm.getJlife());
		srcalcpy.covrCoverage.set(covrclm.getCoverage());
		srcalcpy.covrRider.set(covrclm.getRider());
		srcalcpy.crtable.set(covrclm.getCrtable());
		srcalcpy.crrcd.set(covrclm.getCrrcd());
		srcalcpy.convUnits.set(covrclm.getConvertInitialUnits());
		srcalcpy.pstatcode.set(covrclm.getPstatcode());
		srcalcpy.status.set(SPACES);
		srcalcpy.polsum.set(chdrsurIO.getPolsum());
		srcalcpy.language.set(bsscIO.getLanguage());
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			srcalcpy.billfreq.set("00");
		}
		else {
			srcalcpy.billfreq.set(chdrsurIO.getBillfreq());
		}
		while ( !(isEQ(srcalcpy.status, varcom.endp))) {
			callSurMethodWhole3370();
		}
		
	}

//ILIFE-4406 by liwei
protected void readT5687(){
	String keyItemitem = wsaaCrtable.toString().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5687ListMap.containsKey(keyItemitem)){	
		itempfList = t5687ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(itempf.getItmfrm().compareTo(new BigDecimal(covrclm.getCrrcd())) < 1 ){
				t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
				itemFound = true;
			}				
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(t5687rec.t5687Rec);
		syserrrec.statuz.set(f294);
		fatalError600();		
	}	
}
protected void readT5679(){
	String keyItemitem = bprdIO.getAuthCode().toString().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5679ListMap.containsKey(keyItemitem)){	
		itempfList = t5679ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
			itemFound = true;
		}		
	}
	if (!itemFound) {
		syserrrec.params.set(t5679rec.t5679Rec);
		syserrrec.statuz.set(f294);
		fatalError600();		
	}	
}
protected void obtainSurrenderCalc3350(){
		readT5687();
		String keyItemitem = t5687rec.svMethod.toString().trim();
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (t6598ListMap.containsKey(keyItemitem)){	
			itempfList = t6598ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				t6598rec.t6598Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}		
		}else{
			t6598rec.t6598Rec.set(SPACES);
		}
	}

protected void validateStatusesWp3360(){
		//ILIFE-4406 by liwei
		readT5679();
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			riskStatusCheckWp3362();
		}
		if (isEQ(wsaaValidStatus, "Y")) {
			wsaaValidStatus = "N";
			for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
			|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
				premStatusCheckWp3363();
			}
		}
	}

protected void riskStatusCheckWp3362()
	{
		/*START*/
		if (isEQ(covrclm.getRider(), ZERO)) {
			if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrclm.getStatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		if (isGT(covrclm.getRider(), ZERO)) {
			if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrclm.getStatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		/*EXIT*/
	}

protected void premStatusCheckWp3363()
	{
		/*START*/
		if (isEQ(covrclm.getRider(), ZERO)) {
			if (isNE(t5679rec.covPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrclm.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		if (isGT(covrclm.getRider(), ZERO)) {
			if (isNE(t5679rec.ridPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()], covrclm.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		/*EXIT*/
	}

protected void callSurMethodWhole3370()
	{
		srcalcpy.effdate.set(cnfsIO.getEffdate());
		srcalcpy.type.set("F");
		srcalcpy.singp.set(covrclm.getInstprem());
		/* if no surrender method found print zeros as surrender value*/
		if (isEQ(t6598rec.calcprog, SPACES)
		|| isEQ(wsaaValidStatus, "N")) {
			srcalcpy.status.set(varcom.endp);
			return ;
		}
		
		/*IVE-796 RUL Product - Partial Surrender Calculation started*/
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		//VPMS code start
		
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())))
		{
			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		else
		{			
	 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);			

			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrsurIO);//VPMS call
			
			
			if(isEQ(srcalcpy.type,"L"))
			{
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
		}
		
		/*IVE-796 RUL Product - Partial Surrender Calculation end*/
		if (isEQ(srcalcpy.status, varcom.bomb)) {
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		if (isNE(srcalcpy.status, varcom.oK)
		&& isNE(srcalcpy.status, varcom.endp)
		&& isNE(srcalcpy.status, "NOPR")) {
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		if (isEQ(srcalcpy.status, "NOPR")) {
			/* Nothing to do. */
		}
		/* MOVE SURC-ACTUAL-VAL         TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 5000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO SURC-ACTUAL-VAL.             */
		if (isNE(srcalcpy.actualVal, 0)) {
			if (isNE(srcalcpy.currcode, SPACES)) {
				zrdecplrec.currency.set(srcalcpy.currcode);
			}
			else {
				zrdecplrec.currency.set(srcalcpy.chdrCurr);
			}
			zrdecplrec.amountIn.set(srcalcpy.actualVal);
			callRounding5000();
			srcalcpy.actualVal.set(zrdecplrec.amountOut);
		}
		/* MOVE SURC-ESTIMATED-VAL      TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 5000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO SURC-ESTIMATED-VAL.          */
		if (isNE(srcalcpy.estimatedVal, 0)) {
			if (isNE(srcalcpy.currcode, SPACES)) {
				zrdecplrec.currency.set(srcalcpy.currcode);
			}
			else {
				zrdecplrec.currency.set(srcalcpy.chdrCurr);
			}
			zrdecplrec.amountIn.set(srcalcpy.estimatedVal);
			callRounding5000();
			srcalcpy.estimatedVal.set(zrdecplrec.amountOut);
		}
		/*    IF  SURC-ESTIMATED-VAL      =  ZERO*/
		/*    AND SURC-ACTUAL-VAL         =  ZERO*/
		/*        GO   TO  3379-EXIT.*/
		/* Check the amount returned for being negative. In the case of    */
		/* SUM products this is possible and so set these values to zero.  */
		/* Note SUM products do not have to have the same PT & BT dates.   */
		checkT56113390();
		if (isNE(wsaaSumFlag, SPACES)) {
			if (isLT(srcalcpy.actualVal, ZERO)) {
				srcalcpy.actualVal.set(ZERO);
			}
		}
		Surdpf s = new Surdpf();
		s.setPlnsfx(Integer.parseInt(wsaaPlanSuffix.toString()));
		s.setChdrcoy(bsprIO.getCompany().toString());
		s.setChdrnum(chdrsurIO.getChdrnum().toString());
		s.setLife(covrclm.getLife());
		s.setCrtable(covrclm.getCrtable());
		s.setJlife(covrclm.getJlife());
		s.setCoverage(covrclm.getCoverage());
		s.setRider(covrclm.getRider());
		s.setVrtfund(srcalcpy.fund.toString());
		s.setShortds(srcalcpy.description.toString());
		s.setTranno(Integer.parseInt(wsaaTranno.toString()));
		s.setCurrcd(chdrsurIO.getCntcurr().toString());
		s.setEmv(new BigDecimal(srcalcpy.estimatedVal.toString()));
		s.setActvalue(new BigDecimal(srcalcpy.actualVal.toString()));
		s.setTypeT(srcalcpy.type.toString());
		surdpfDAO.insertSurdpfRecord(s);
		/* For every SURDCLM written, write a HSUD                         */
		//ILIFE-4406 by liwei
		Hsudpf hsudpf = new Hsudpf();
		hsudpf.setChdrcoy(s.getChdrcoy());
		hsudpf.setChdrnum(s.getChdrnum());
		hsudpf.setLife(s.getLife());
		hsudpf.setCoverage(s.getCoverage());/* IJTI-1523 */
		hsudpf.setRider(s.getRider());/* IJTI-1523 */
		hsudpf.setPlanSuffix(s.getPlnsfx());
		hsudpf.setCrtable(s.getCrtable());/* IJTI-1523 */
		hsudpf.setJlife(s.getJlife());/* IJTI-1523 */
		hsudpf.setTranno(s.getTranno());
		hsudpf.setHactval(new BigDecimal(srcalcpy.actualVal.toString()));
		hsudpf.setHemv(new BigDecimal(srcalcpy.estimatedVal.toString()));
		hsudpf.setHcnstcur(chdrsurIO.getCntcurr().toString());
		hsudpf.setFieldType(srcalcpy.type.toString());
		hsudpfList.add(hsudpf);

		
		if (firstTime.isTrue()) {
			wsaaSwitch2.set(0);
			wsaaStoredCurrency.set(chdrsurIO.getCntcurr());
			if (isEQ(srcalcpy.type, "C")) {
				compute(wsaaActualTot, 2).set(sub(wsaaActualTot, srcalcpy.actualVal));
				compute(wsaaPenaltyTot, 2).set(add(wsaaPenaltyTot, srcalcpy.actualVal));
			}
			else {
				compute(wsaaActualTot, 2).set(add(wsaaActualTot, srcalcpy.actualVal));
			}
		}
		else {
			if (isEQ(chdrsurIO.getCntcurr(), wsaaStoredCurrency)) {
				if (isEQ(srcalcpy.description, "PENALTY")) {
					compute(wsaaActualTot, 2).set(sub(wsaaActualTot, srcalcpy.actualVal));
					compute(wsaaPenaltyTot, 2).set(add(wsaaPenaltyTot, srcalcpy.actualVal));
				}
				else {
					compute(wsaaActualTot, 2).set(add(wsaaActualTot, srcalcpy.actualVal));
				}
			}
			else {
				wsaaActualTot.set(ZERO);
				wsaaCurrencySwitch.set(1);
			}
		}
		/* Check the effective date of the surrender against the paid to   */
		/* date of the contract. If there are to be premiums paid or       */
		/* refunded then call the appropriate subroutine held on T5611.    */
		/* Note that if the item does not exist then the coverage does not */
		/* use the SUM calculation package & is hence not applicable....   */
		if (isNE(srcalcpy.effdate, srcalcpy.ptdate)
		&& isNE(wsaaSumFlag, SPACES)
		&& isNE(t5611rec.calcprog, SPACES)) {
			checkPremadj3391();
		}
	}


protected void checkT56113390(){
		/* Read the Coverage Surrender/Paid-Up/Bonus parameter table.      */
		//ILIFE -4406 by liwei
		wsaaT5611Crtable.set(srcalcpy.crtable);
		wsaaT5611Pstatcode.set(srcalcpy.pstatcode);
		String keyItemitem = wsaaT5611Item.toString().trim();
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (t5611ListMap.containsKey(keyItemitem)){	
			itempfList = t5611ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(itempf.getItmfrm().compareTo(new BigDecimal(srcalcpy.effdate.toString())) < 1 && itempf.getItemcoy().equals(srcalcpy.chdrChdrcoy.toString())){
					t5611rec.t5611Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
					wsaaSumFlag.set("Y");
				}else{
					wsaaSumFlag.set(SPACES);
				}				
			}		
		}
		else{
			wsaaSumFlag.set(SPACES);
		}
		/*if (!itemFound) {
			syserrrec.params.set(t5687rec.t5687Rec);
			fatalError600();		
		}*/
	}

protected void checkPremadj3391()
	{
		/* Call the surrender routine for premium adjustments. Note that   */
		/* for the moment this uses the same copy-book as the surrender    */
		/* routine, which is stored and then re-instated .........         */
		wsaaSurrenderRecStore.set(srcalcpy.surrenderRec);
		/* If this is a Single Premium Component then use a Billing        */
		/* frequency of '00'.                                              */
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			srcalcpy.billfreq.set("00");
		}
		else {
			srcalcpy.billfreq.set(chdrsurIO.getBillfreq());
		}
		srcalcpy.estimatedVal.set(covrclm.getInstprem());
		srcalcpy.actualVal.set(ZERO);
		callProgram(t5611rec.calcprog, srcalcpy.surrenderRec);
		if (isEQ(srcalcpy.status, varcom.bomb)) {
			syserrrec.params.set(srcalcpy.surrenderRec);
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		else {
			if (isNE(srcalcpy.status, varcom.oK)
			&& isNE(srcalcpy.status, varcom.endp)) {
				syserrrec.params.set(srcalcpy.surrenderRec);
				syserrrec.statuz.set(srcalcpy.status);
				fatalError600();
			}
		}
		/* Note adjustments are subtracted from the total.                 */
		/* The actual value here is the premium adjustment returned...     */
		/* MOVE SURC-ACTUAL-VAL         TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 5000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO SURC-ACTUAL-VAL.             */
		if (isNE(srcalcpy.actualVal, 0)) {
			if (isNE(srcalcpy.currcode, SPACES)) {
				zrdecplrec.currency.set(srcalcpy.currcode);
			}
			else {
				zrdecplrec.currency.set(srcalcpy.chdrCurr);
			}
			zrdecplrec.amountIn.set(srcalcpy.actualVal);
			callRounding5000();
			srcalcpy.actualVal.set(zrdecplrec.amountOut);
		}
		wsaaOtheradjst.add(srcalcpy.actualVal);
		srcalcpy.surrenderRec.set(wsaaSurrenderRecStore);
	}

//ILIFE-4406 by liwei
protected boolean readTr691(String keyItemitem) {
	boolean itemFound = false;
	List<Itempf> itempfList = new ArrayList<Itempf>();
	if (tr691ListMap.containsKey(keyItemitem)) {
		itempfList = tr691ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			tr691rec.tr691Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			itemFound = true;
		}
	}
	return itemFound;
}
protected void getTaxAmount3392(){
		wsaaTaxAmt.set(0);
		wsaaTr691Key.set(SPACES);
		wsaaTr691Cntcurr.set(chdrsurIO.getCntcurr());
		wsaaTr691Cnttype.set(chdrsurIO.getCnttype());
		String keyItemitem = wsaaTr691Key.toString();
		boolean itemfound = readTr691(keyItemitem);
		if(!itemfound){
			wsaaTr691Key.set(SPACES);
			wsaaTr691Cnttype.set("***");
			wsaaTr691Cntcurr.set(chdrsurIO.getCntcurr());
			itemfound = readTr691(wsaaTr691Key.toString().trim());
			if(!itemfound){
				return;
			}
		}
		
		datcon3rec.intDate2.set(cnfsIO.getEffdate());
		datcon3rec.intDate1.set(chdrsurIO.getOccdate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isGTE(datcon3rec.freqFactor, tr691rec.tyearno)) {
			return ;
		}
		if (isNE(tr691rec.pcnt, 0)) {
			compute(wsaaTaxAmt, 3).setRounded(mult(wsaaActualTot, (div(tr691rec.pcnt, 100))));
		}
		/* MOVE WSAA-TAX-AMT            TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 5000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO WSAA-TAX-AMT.                */
		if (isNE(wsaaTaxAmt, 0)) {
			zrdecplrec.amountIn.set(wsaaTaxAmt);
			zrdecplrec.currency.set(chdrsurIO.getCntcurr());
			callRounding5000();
			wsaaTaxAmt.set(zrdecplrec.amountOut);
		}
		if (isNE(tr691rec.flatrate, 0)) {
			wsaaTaxAmt.set(tr691rec.flatrate);
		}
	}

protected void getLoanDetails3393()
	{
		start3393();
	}

	/**
	* <pre>
	*  Get the details of all loans currently held against this       
	*  Contract. If this is not the first component within the        
	*  current Surrender transaction then need to read the SURD       
	*  to get details of all previous surrender records for this      
	*  transaction.                                                   
	*  If there are no records (there will be none for Unit Linked),  
	*  then call TOTLOAN to get the current loan value. If there are  
	*  records then sum up their values before calling TOTLOAN and    
	*  subtracting the total value from the LOAN VALUE returned from  
	*  TOTLOAN.                                                       
	*  Previous surrenders may have been done in a different          
	*  currency to the present one and hence, a check should be       
	*  made for this and a conversion done where necessary before     
	*  the accumulation is done. Note that for the initial screen     
	*  display, the currency will always be CHDR currency.            
	*  Note also that TOTLOAN always returns details in the CHDR      
	*  currency.                                                      
	* </pre>
	*/
protected void start3393()
	{
		List<Surdpf> surdpfList = surdpfDAO.searchSurdclmRecord(chdrsurIO.getChdrcoy().toString().trim(), chdrsurIO.getChdrnum().toString().trim(), ZERO.toString().trim(), ZERO.toString().trim(), ZERO.toString().trim(), 
				Integer.parseInt(ZERO.toString().trim()), Integer.parseInt(chdrsurIO.getTranno().toString().trim()));
		wsaaLoanValue.set(0);
		for(int i = 0; i<surdpfList.size(); i++){
			surdclm = surdpfList.get(i);
			if (isNE(surdclm.getCurrcd(), chdrsurIO.getCntcurr())) {
				readjustSurd3395();
			}
			else {
				wsaaActvalue.set(surdclm.getActvalue());
			}
			wsaaLoanValue.add(wsaaActvalue);
		}
		
		
		/*  Apply the adjustment for any previous surrendered policies     */
		/*  where this is part of a multipolicy surrender.                 */
		if (!firstTimeThrough.isTrue()) {
			wsaaLoanValue.add(wsaaOtheradjst);
		}
		/*  Read TOTLOAN to get value of loans for this contract.          */
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrsurIO.getChdrcoy());
		totloanrec.chdrnum.set(chdrsurIO.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(cnfsIO.getEffdate());
		totloanrec.function.set("LOAN");
		totloanrec.language.set(bsscIO.getLanguage());
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(wsaaHeldCurrLoans, 2).set(add(totloanrec.principal, totloanrec.interest));
		if (isLT(wsaaLoanValue, wsaaHeldCurrLoans)) {
			wsaaHeldCurrLoans.subtract(wsaaLoanValue);
		}
		else {
			wsaaHeldCurrLoans.set(0);
		}
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrsurIO.getChdrcoy());
		totloanrec.chdrnum.set(chdrsurIO.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(cnfsIO.getEffdate());
		totloanrec.function.set("CASH");
		totloanrec.language.set(bsscIO.getLanguage());
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(wsaaAtmodRecInner.wsaaZrcshamt, 2).set(add(totloanrec.principal, totloanrec.interest));
		/* Change the sign to reflect the Client's context.             */
		if (isLT(wsaaAtmodRecInner.wsaaZrcshamt, 0)) {
			compute(wsaaAtmodRecInner.wsaaZrcshamt, 0).set(mult(wsaaAtmodRecInner.wsaaZrcshamt, (-1)));
		}
	}
//ILIFE-4406 by liwei
	protected void getPolicyDebt3394() {
		wsaaAtmodRecInner.wsaaTdbtamt.set(ZERO);
		List<Tpdbpf> tpdbpfList = tpdbpfDAO.getTpoldbt(chdrsurIO.getChdrcoy().toString(), chdrsurIO.getChdrnum().toString());
		if (tpdbpfList != null && tpdbpfList.size() > 0) {
			for (Tpdbpf tpdbpf : tpdbpfList) {
				wsaaAtmodRecInner.wsaaTdbtamt.add(ZonedDecimalData.parseObject(tpdbpf.getTdbtamt()));
			}
		} else {
			//fatalError600();
			return; 
		}

	}

protected void readjustSurd3395()
	{
		/*  Convert the SURD into the CHDR currency.                       */
		conlinkrec.function.set("SURR");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(surdclm.getCurrcd());
		conlinkrec.cashdate.set(varcom.vrcmMaxDate);
		conlinkrec.currOut.set(chdrsurIO.getCntcurr());
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.amountIn.set(surdclm.getActvalue());
		conlinkrec.company.set(chdrsurIO.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		/* MOVE CLNK-AMOUNT-OUT         TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 5000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO CLNK-AMOUNT-OUT.             */
		if (isNE(conlinkrec.amountOut, 0)) {
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			zrdecplrec.currency.set(conlinkrec.currOut);
			callRounding5000();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
		}
		wsaaActvalue.set(conlinkrec.amountOut);
	}

protected void close4000(){

	if (t5611ListMap != null) {
		t5611ListMap.clear();
    }
	t5611ListMap = null;
	
	if (tr691ListMap != null) {
		tr691ListMap.clear();
    }
	tr691ListMap = null;
	
	if (t5679ListMap != null) {
		t5679ListMap.clear();
    }
	t5679ListMap = null;
	
	if (t5687ListMap != null) {
		t5687ListMap.clear();
    }
	t5687ListMap = null;
	
	if (t6598ListMap != null) {
		t6598ListMap.clear();
    }
	t6598ListMap = null;
	lsaaStatuz.set(varcom.oK);
	
	hsudpfList.clear();
	hsudpfList = null;
	surdpfList.clear();
	surdpfList = null;
}

	protected void commit3500() {
		if (hsudpfList.size() > 0) {
			hsudpfDAO.insertHsudpfList(hsudpfList);
		}
		/*if (surdpfList.size() > 0) {
			surdpfDAO.insertSurdclmpfDataBulk(surdpfList);
		}*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}


protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		/* MOVE CHDRSUR-CNTCURR        TO ZRDP-CURRENCY.                */
		zrdecplrec.batctrcde.set(wsaaAtmodRecInner.wsaaBatcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-ATMOD-REC--INNER
 */
private static final class WsaaAtmodRecInner { 

	private FixedLengthStringData wsaaAtmodRec = new FixedLengthStringData(310);
	private FixedLengthStringData wsaaAtdmStatuz = new FixedLengthStringData(4).isAPartOf(wsaaAtmodRec, 0);
	private FixedLengthStringData wsaaAtdmBatchKey = new FixedLengthStringData(22).isAPartOf(wsaaAtmodRec, 4);
	private FixedLengthStringData wsaaBatcBatcpfx = new FixedLengthStringData(2).isAPartOf(wsaaAtdmBatchKey, 0);
	private FixedLengthStringData wsaaBatcBatccoy = new FixedLengthStringData(1).isAPartOf(wsaaAtdmBatchKey, 2);
	private FixedLengthStringData wsaaBatcBatcbrn = new FixedLengthStringData(2).isAPartOf(wsaaAtdmBatchKey, 3);
	private ZonedDecimalData wsaaBatcBatcactyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaAtdmBatchKey, 5);
	private ZonedDecimalData wsaaBatcBatcactmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaAtdmBatchKey, 9);
	private FixedLengthStringData wsaaBatcBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaAtdmBatchKey, 11);
	private FixedLengthStringData wsaaBatcBatcbatch = new FixedLengthStringData(5).isAPartOf(wsaaAtdmBatchKey, 15);
	private FixedLengthStringData wsaaFiller = new FixedLengthStringData(2).isAPartOf(wsaaAtdmBatchKey, 20);
	private FixedLengthStringData wsaaAtdmLanguage = new FixedLengthStringData(1).isAPartOf(wsaaAtmodRec, 26);
	private FixedLengthStringData wsaaAtdmCompany = new FixedLengthStringData(1).isAPartOf(wsaaAtmodRec, 27);
	private FixedLengthStringData wsaaAtdmPrimaryKey = new FixedLengthStringData(36).isAPartOf(wsaaAtmodRec, 28);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaAtdmPrimaryKey, 0);
	private FixedLengthStringData wsaaAtdmPrintQueue = new FixedLengthStringData(10).isAPartOf(wsaaAtmodRec, 64);
	private FixedLengthStringData wsaaAtdmTransArea = new FixedLengthStringData(200).isAPartOf(wsaaAtmodRec, 74);
	private PackedDecimalData wsaaTransDate = new PackedDecimalData(6, 0).isAPartOf(wsaaAtdmTransArea, 0);
	private PackedDecimalData wsaaTransTime = new PackedDecimalData(6, 0).isAPartOf(wsaaAtdmTransArea, 4);
	private PackedDecimalData wsaaTransUser = new PackedDecimalData(6, 0).isAPartOf(wsaaAtdmTransArea, 8);
	private FixedLengthStringData wsaaTransTermid = new FixedLengthStringData(4).isAPartOf(wsaaAtdmTransArea, 12);
	private FixedLengthStringData filler1 = new FixedLengthStringData(184).isAPartOf(wsaaAtdmTransArea, 16, FILLER).init(SPACES);
	private FixedLengthStringData wsaaAtdmReqterm = new FixedLengthStringData(4).isAPartOf(wsaaAtmodRec, 274);
	private ZonedDecimalData wsaaTdbtamt = new ZonedDecimalData(18, 2).isAPartOf(wsaaAtmodRec, 278);
	private ZonedDecimalData wsaaZrcshamt = new ZonedDecimalData(14, 2).isAPartOf(wsaaAtmodRec, 296);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData cnfsrec = new FixedLengthStringData(10).init("CNFSREC");
	private FixedLengthStringData chdrsurrec = new FixedLengthStringData(10).init("CHDRSURREC");
	private FixedLengthStringData covrclmrec = new FixedLengthStringData(10).init("COVRCLMREC");
	private FixedLengthStringData tpoldbtrec = new FixedLengthStringData(10).init("TPOLDBTREC");
	private FixedLengthStringData surdclmrec = new FixedLengthStringData(10).init("SURDCLMREC");
	private FixedLengthStringData surhclmrec = new FixedLengthStringData(10).init("SURHCLMREC");
	private FixedLengthStringData hsudrec = new FixedLengthStringData(10).init("HSUDREC");
}
}
