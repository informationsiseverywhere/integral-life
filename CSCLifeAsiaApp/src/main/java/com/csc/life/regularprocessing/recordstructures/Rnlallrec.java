package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:10:06
 * Description:
 * Copybook name: RNLALLREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rnlallrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rnlallRec = new FixedLengthStringData(171);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(rnlallRec, 0);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(rnlallRec, 4);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rnlallRec, 5);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(rnlallRec, 13);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(rnlallRec, 15);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(rnlallRec, 17);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(rnlallRec, 19);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(rnlallRec, 22);
  	public PackedDecimalData billcd = new PackedDecimalData(8, 0).isAPartOf(rnlallRec, 24);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(rnlallRec, 29);
  	public FixedLengthStringData sacscode = new FixedLengthStringData(2).isAPartOf(rnlallRec, 32);
  	public FixedLengthStringData sacstyp = new FixedLengthStringData(2).isAPartOf(rnlallRec, 34);
  	public FixedLengthStringData genlcde = new FixedLengthStringData(14).isAPartOf(rnlallRec, 36);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(rnlallRec, 50);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(rnlallRec, 53);
  	public FixedLengthStringData batchkey = new FixedLengthStringData(19).isAPartOf(rnlallRec, 56);
  	public FixedLengthStringData contkey = new FixedLengthStringData(14).isAPartOf(batchkey, 0);
  	public FixedLengthStringData batcpfx = new FixedLengthStringData(2).isAPartOf(contkey, 0);
  	public FixedLengthStringData batccoy = new FixedLengthStringData(1).isAPartOf(contkey, 2);
  	public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(contkey, 3);
  	public PackedDecimalData batcactyr = new PackedDecimalData(4, 0).isAPartOf(contkey, 5);
  	public PackedDecimalData batcactmn = new PackedDecimalData(2, 0).isAPartOf(contkey, 8);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(contkey, 10);
  	public FixedLengthStringData batcbatch = new FixedLengthStringData(5).isAPartOf(batchkey, 14);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(rnlallRec, 75).setUnsigned();
  	public ZonedDecimalData user = new ZonedDecimalData(6, 0).isAPartOf(rnlallRec, 83).setUnsigned();
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(rnlallRec, 89);
  	public ZonedDecimalData crdate = new ZonedDecimalData(8, 0).isAPartOf(rnlallRec, 93).setUnsigned();
  	public ZonedDecimalData moniesDate = new ZonedDecimalData(8, 0).isAPartOf(rnlallRec, 101).setUnsigned();
  	public ZonedDecimalData anbAtCcd = new ZonedDecimalData(3, 0).isAPartOf(rnlallRec, 109).setUnsigned();
  	public ZonedDecimalData termLeftToRun = new ZonedDecimalData(3, 0).isAPartOf(rnlallRec, 112).setUnsigned();
  	public ZonedDecimalData covrInstprem = new ZonedDecimalData(17, 2).isAPartOf(rnlallRec, 115);
  	public FixedLengthStringData sacscode02 = new FixedLengthStringData(2).isAPartOf(rnlallRec, 132);
  	public FixedLengthStringData sacstyp02 = new FixedLengthStringData(2).isAPartOf(rnlallRec, 134);
  	public FixedLengthStringData genlcde02 = new FixedLengthStringData(14).isAPartOf(rnlallRec, 136);
  	public ZonedDecimalData duedate = new ZonedDecimalData(8, 0).isAPartOf(rnlallRec, 150).setUnsigned();
  	public PackedDecimalData totrecd = new PackedDecimalData(17, 2).isAPartOf(rnlallRec, 158);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(rnlallRec, 167);
  	public FixedLengthStringData filler = new FixedLengthStringData(3).isAPartOf(rnlallRec, 168, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(rnlallRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rnlallRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}