package com.csc.life.regularprocessing.dataaccess.model;

import java.math.BigDecimal;

import com.quipoz.framework.datatype.PackedDecimalData;

public class B5023DTO {
    /* COVRPF */
    private String chdrcoy;
    private String chdrnum;
    private String life;
    private String coverage;
    private String rider;
    private int plnsfx;
    private String crtable;
    private String pstatcode;
    private String statcode;
    private int crrcd;
    private int cbunst;
    private int rcesdte;
    private BigDecimal sumins;
    private String validflag;
    private int currfrom;
    private int currto;
    // CHDR
    private int chcurrfrm;
    private int chcurrto;
    private long chunique;
    
    
    public int getChcurrfrm() {
        return chcurrfrm;
    }
    public int getChcurrto() {
        return chcurrto;
    }
    public long getChunique() {
        return chunique;
    }
    public void setChcurrfrm(int chcurrfrm) {
        this.chcurrfrm = chcurrfrm;
    }
    public void setChcurrto(int chcurrto) {
        this.chcurrto = chcurrto;
    }
    public void setChunique(long chunique) {
        this.chunique = chunique;
    }
    public String getChdrcoy() {
        return chdrcoy;
    }
    public String getChdrnum() {
        return chdrnum;
    }
    public String getLife() {
        return life;
    }
    public String getCoverage() {
        return coverage;
    }
    public String getRider() {
        return rider;
    }
    public int getPlnsfx() {
        return plnsfx;
    }
    public String getCrtable() {
        return crtable;
    }
    public String getPstatcode() {
        return pstatcode;
    }
    public String getStatcode() {
        return statcode;
    }
    public int getCrrcd() {
        return crrcd;
    }
    public int getCbunst() {
        return cbunst;
    }
    public int getRcesdte() {
        return rcesdte;
    }
    public BigDecimal getSumins() {
        return sumins;
    }
    public String getValidflag() {
        return validflag;
    }
    public int getCurrfrom() {
        return currfrom;
    }
    public int getCurrto() {
        return currto;
    }
    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }
    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }
    public void setLife(String life) {
        this.life = life;
    }
    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }
    public void setRider(String rider) {
        this.rider = rider;
    }
    public void setPlnsfx(int plnsfx) {
        this.plnsfx = plnsfx;
    }
    public void setCrtable(String crtable) {
        this.crtable = crtable;
    }
    public void setPstatcode(String pstatcode) {
        this.pstatcode = pstatcode;
    }
    public void setStatcode(String statcode) {
        this.statcode = statcode;
    }
    public void setCrrcd(int crrcd) {
        this.crrcd = crrcd;
    }
    public void setCbunst(int cbunst) {
        this.cbunst = cbunst;
    }
    public void setRcesdte(int rcesdte) {
        this.rcesdte = rcesdte;
    }
    public void setSumins(BigDecimal sumins) {
        this.sumins = sumins;
    }
    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }
    public void setCurrfrom(int currfrom) {
        this.currfrom = currfrom;
    }
    public void setCurrto(int currto) {
        this.currto = currto;
    }
}
