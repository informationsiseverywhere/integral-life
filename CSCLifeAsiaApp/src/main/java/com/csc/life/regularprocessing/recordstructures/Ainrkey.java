package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:12
 * Description:
 * Copybook name: AINRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ainrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ainrFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ainrKey = new FixedLengthStringData(64).isAPartOf(ainrFileKey, 0, REDEFINE);
  	public FixedLengthStringData ainrAintype = new FixedLengthStringData(4).isAPartOf(ainrKey, 0);
  	public FixedLengthStringData ainrChdrcoy = new FixedLengthStringData(1).isAPartOf(ainrKey, 4);
  	public FixedLengthStringData ainrChdrnum = new FixedLengthStringData(8).isAPartOf(ainrKey, 5);
  	public FixedLengthStringData ainrLife = new FixedLengthStringData(2).isAPartOf(ainrKey, 13);
  	public FixedLengthStringData ainrCoverage = new FixedLengthStringData(2).isAPartOf(ainrKey, 15);
  	public FixedLengthStringData ainrRider = new FixedLengthStringData(2).isAPartOf(ainrKey, 17);
  	public PackedDecimalData ainrPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(ainrKey, 19);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(ainrKey, 22, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ainrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ainrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}