package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.InczpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Inczpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class InczpfDAOImpl extends BaseDAOImpl<Inczpf> implements InczpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(InczpfDAOImpl.class);

    public List<Inczpf> searchInczRecord(String tableId, String memName, int batchExtractSize, int batchID) {
        StringBuilder sqlInczSelect = new StringBuilder();
        sqlInczSelect.append(" SELECT * FROM (");
        sqlInczSelect
                .append("  SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,CRRCD,CRTABLE,VALIDFLAG,STATCODE,PSTATCODE,ANNVRY,BASCMETH,BASCPY,RNWCPY,SRVCPY,NEWINST,LASTINST,NEWSUM,ZBNEWINST,ZBLASTINST,ZLNEWINST,ZLLASTINST ");
        sqlInczSelect.append("   ,FLOOR((ROW_NUMBER()OVER( ORDER BY CHDRCOY ASC, CHDRNUM ASC)-1)/?) BATCHNUM FROM ");
        sqlInczSelect.append(tableId);
        sqlInczSelect.append("   WHERE MEMBER_NAME = ? ");
        sqlInczSelect.append(" ) MAIN WHERE BATCHNUM = ? ");

        PreparedStatement psInczSelect = getPrepareStatement(sqlInczSelect.toString());
        ResultSet sqlinczpf1rs = null;
        List<Inczpf> inczpfList = new ArrayList<>();
        try {
            psInczSelect.setInt(1, batchExtractSize);
            psInczSelect.setString(2, memName);
            psInczSelect.setInt(3, batchID);

            sqlinczpf1rs = executeQuery(psInczSelect);
            while (sqlinczpf1rs.next()) {
                Inczpf inczpf = new Inczpf();
                inczpf.setUniqueNumber(sqlinczpf1rs.getLong("unique_Number"));
                inczpf.setChdrcoy(sqlinczpf1rs.getString("chdrcoy"));
                inczpf.setChdrnum(sqlinczpf1rs.getString("chdrnum"));
                inczpf.setLife(sqlinczpf1rs.getString("life"));
                inczpf.setJlife(sqlinczpf1rs.getString("jlife"));
                inczpf.setCoverage(sqlinczpf1rs.getString("coverage"));
                inczpf.setRider(sqlinczpf1rs.getString("rider"));
                inczpf.setPlanSuffix(sqlinczpf1rs.getInt("plnsfx"));
                inczpf.setCrrcd(sqlinczpf1rs.getInt("crrcd"));
                inczpf.setCrtable(sqlinczpf1rs.getString("crtable"));
                inczpf.setValidflag(sqlinczpf1rs.getString("validflag"));
                inczpf.setStatcode(sqlinczpf1rs.getString("statcode"));
                inczpf.setPstatcode(sqlinczpf1rs.getString("pstatcode"));
                inczpf.setAnniversaryMethod(sqlinczpf1rs.getString("annvry"));
                inczpf.setBasicCommMeth(sqlinczpf1rs.getString("bascmeth"));
                inczpf.setBascpy(sqlinczpf1rs.getString("bascpy"));
                inczpf.setRnwcpy(sqlinczpf1rs.getString("rnwcpy"));
                inczpf.setSrvcpy(sqlinczpf1rs.getString("srvcpy"));
                inczpf.setNewinst(sqlinczpf1rs.getBigDecimal("newinst"));
                inczpf.setLastInst(sqlinczpf1rs.getBigDecimal("lastInst"));
                inczpf.setNewsum(sqlinczpf1rs.getBigDecimal("newsum"));
                inczpf.setZbnewinst(sqlinczpf1rs.getBigDecimal("zbnewinst"));
                inczpf.setZblastinst(sqlinczpf1rs.getBigDecimal("zblastinst"));
                inczpf.setZlnewinst(sqlinczpf1rs.getBigDecimal("zlnewinst"));
                inczpf.setZllastinst(sqlinczpf1rs.getBigDecimal("zllastinst"));

                inczpfList.add(inczpf);
            }
        } catch (SQLException e) {
            LOGGER.error("searchInczRecord()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(psInczSelect, sqlinczpf1rs);
        }
        return inczpfList;

    }

}