package com.csc.life.regularprocessing.dataaccess.model;

import java.math.BigDecimal;


public class Covxpf {

	private long uniqueNumber;
	
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int planSuffix;
	private String jlife;
	private String premCurrency;
	private int benBillDate;
	private BigDecimal sumins;
	private int premCessDate;
	private String crtable;
	private String mortcls;
	private String subprog;
	private String unitFreq;
	private String premmeth;
	private String jlPremMeth;
	private String svMethod;
	private String adfeemth;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public String getJlife() {
		return jlife;
	}
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	public String getPremCurrency() {
		return premCurrency;
	}
	public void setPremCurrency(String premCurrency) {
		this.premCurrency = premCurrency;
	}
	public int getBenBillDate() {
		return benBillDate;
	}
	public void setBenBillDate(int benBillDate) {
		this.benBillDate = benBillDate;
	}
	public BigDecimal getSumins() {
		return sumins;
	}
	public void setSumins(BigDecimal sumins) {
		this.sumins = sumins;
	}
	public int getPremCessDate() {
		return premCessDate;
	}
	public void setPremCessDate(int premCessDate) {
		this.premCessDate = premCessDate;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getMortcls() {
		return mortcls;
	}
	public void setMortcls(String mortcls) {
		this.mortcls = mortcls;
	}
	public String getSubprog() {
		return subprog;
	}
	public void setSubprog(String subprog) {
		this.subprog = subprog;
	}
	public String getUnitFreq() {
		return unitFreq;
	}
	public void setUnitFreq(String unitFreq) {
		this.unitFreq = unitFreq;
	}
	public String getPremmeth() {
		return premmeth;
	}
	public void setPremmeth(String premmeth) {
		this.premmeth = premmeth;
	}
	public String getJlPremMeth() {
		return jlPremMeth;
	}
	public void setJlPremMeth(String jlPremMeth) {
		this.jlPremMeth = jlPremMeth;
	}
	public String getSvMethod() {
		return svMethod;
	}
	public void setSvMethod(String svMethod) {
		this.svMethod = svMethod;
	}
	public String getAdfeemth() {
		return adfeemth;
	}
	public void setAdfeemth(String adfeemth) {
		this.adfeemth = adfeemth;
	}
}