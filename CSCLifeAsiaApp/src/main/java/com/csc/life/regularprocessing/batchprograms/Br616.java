/*
 * File: Br616.java
 * Date: 29 August 2009 22:26:56
 * Author: Quipoz Limited
 * 
 * Class transformed from BR616.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.life.regularprocessing.dataaccess.UstxpfTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 2005.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*  SPLITTER PROGRAM (For Unit Statement Trigger batch processing)
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* This program is to be run before the Unit Statement Trigger
* Program, BR617.
* It is run in single-thread, and writes the selected CHDR
* records to multiple files . Each of the members will be read
* by a copy of BR617 run in multi-thread mode.
*
* SQL will be used to access the CHDR physical file. The
* splitter program will extract CHDR records that meet the
* following criteria:
*
* i    Service Unit =  'LP'
* ii   Chdroy     =  <run-company>
* iii  Validflag  =  '1'
* iv   Statement Date  <>  0
* v    Statement Date  <=  Batch Effective Date
* vi   Billing Channel <>  'N'
* vii  Chdrnum    between  <p6671-chdrnumfrom>
*                     and  <p6671-chdrnumto>
*      order by chdrcoy, chdrnum
*
* Control totals used in this program:
*
*    01  -  No. of thread members
*    02  -  No. of extracted records
*
*
* A Splitter Program's purpose is to find a pending transactions f om
* the database and create multiple extract files for processing
* by multiple copies of the subsequent program, each running in it s
* own thread. Each subsequent program therefore processes a descre e
* portion of the total transactions.
*
* Splitter programs should be simple. They should only deal with
* a single file. If there is insufficient data on the primary file
* to completely identify a transaction then the further editing
* should be done in the subsequent process. The objective of a
* Splitter is to rapidily isolate potential transactions, not
* to process the transaction. The primary concern for Splitter
* program design is elasped time speed and this is acheived by
* minimising disk IO.
*
* Before the Splitter process is invoked the, the program CRTTMPF
* should be run. The CRTTMPF, (Create Temporary File), will create
* a duplicate of a physical file, (created under Smart and defined
* as a Query file), which will have as many members as is defined
* in that process's 'subsequent threads' field. The temporary file
* will have the name XXXXDD9999 where XXXX is the first four
* characters of the file which was duplicated, DD is the first two
* characters of the 4th system parameter and 9999 is the schedule
* run number. Each member added to the temporary file will be
* called THREAD999 where 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read records from a primary file
* and for each record it selects it will write a record to a
* temporary file member. Exactly which temporary file member
* is written to is decided by a working storage count. The
* objective of the count is spread the transaction records evenly
* amoungst each thread member.
*
* The Splitter program should always have a restart method of
* '1' indicating a re-run. In the event of a restart the Splitter
* program will always start with empty file members. Apart from
* writing records to the temporary file members Splitter programs
* should not be doing any further updates.
*
* There should be no non-standard CL commands in the CL Pgm which
* calls this program.
*
* Notes for programs which use Splitter Program Temporary Files.
* --------------------------------------------------------------
*
* The MTBE will be able to submit multiple versions of the program
* which use the file members created from this program. It is
* important that the process which runs the splitter program has
* the same 'number of subsequent threads' as the following
* process has defined in 'number of threads this process'.
*
* The subsequent program need only point itself to one member
* based around the field BSPR-SCHEDULE-THREAD-NO which indicates
* in which thread the copy of the subsequent program is running.
*
****************************************************************** ****
* </pre>
*/
public class Br616 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlustxCursorrs = null;
	private java.sql.PreparedStatement sqlustxCursorps = null;
	private java.sql.Connection sqlustxCursorconn = null;
	private String sqlustxCursor = "";
	private int ustxCursorLoopIndex = 0;
	private DiskFileDAM ustx01 = new DiskFileDAM("USTX01");
	private DiskFileDAM ustx02 = new DiskFileDAM("USTX02");
	private DiskFileDAM ustx03 = new DiskFileDAM("USTX03");
	private DiskFileDAM ustx04 = new DiskFileDAM("USTX04");
	private DiskFileDAM ustx05 = new DiskFileDAM("USTX05");
	private DiskFileDAM ustx06 = new DiskFileDAM("USTX06");
	private DiskFileDAM ustx07 = new DiskFileDAM("USTX07");
	private DiskFileDAM ustx08 = new DiskFileDAM("USTX08");
	private DiskFileDAM ustx09 = new DiskFileDAM("USTX09");
	private DiskFileDAM ustx10 = new DiskFileDAM("USTX10");
	private DiskFileDAM ustx11 = new DiskFileDAM("USTX11");
	private DiskFileDAM ustx12 = new DiskFileDAM("USTX12");
	private DiskFileDAM ustx13 = new DiskFileDAM("USTX13");
	private DiskFileDAM ustx14 = new DiskFileDAM("USTX14");
	private DiskFileDAM ustx15 = new DiskFileDAM("USTX15");
	private DiskFileDAM ustx16 = new DiskFileDAM("USTX16");
	private DiskFileDAM ustx17 = new DiskFileDAM("USTX17");
	private DiskFileDAM ustx18 = new DiskFileDAM("USTX18");
	private DiskFileDAM ustx19 = new DiskFileDAM("USTX19");
	private DiskFileDAM ustx20 = new DiskFileDAM("USTX20");
	private UstxpfTableDAM ustxpf = new UstxpfTableDAM();
	private UstxpfTableDAM ustxpfData = new UstxpfTableDAM();
		/* Change the record length to that of the temporary file. This
		 can be found by doing a DSPFD of the file being duplicated
		 by the CRTTMPF process.*/
	private FixedLengthStringData ustx01Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx02Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx03Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx04Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx05Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx06Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx07Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx08Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx09Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx10Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx11Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx12Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx13Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx14Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx15Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx16Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx17Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx18Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx19Rec = new FixedLengthStringData(31);
	private FixedLengthStringData ustx20Rec = new FixedLengthStringData(31);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR616");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

	private FixedLengthStringData wsaaUstxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaUstxFn, 0, FILLER).init("USTX");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaUstxFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaUstxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThread = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThread, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThread, 6).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaThread, 9, FILLER).init(SPACES);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaUstxData = FLSInittedArray (1000, 40);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaUstxData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaUstxData, 1);
	private ZonedDecimalData[] wsaaStmdte = ZDArrayPartOfArrayStructure(8, 0, wsaaUstxData, 9);
	private ZonedDecimalData[] wsaaPtdate = ZDArrayPartOfArrayStructure(8, 0, wsaaUstxData, 17);
	private FixedLengthStringData[] wsaaCnttype = FLSDArrayPartOfArrayStructure(3, wsaaUstxData, 25);
	private FixedLengthStringData[] wsaaStatcode = FLSDArrayPartOfArrayStructure(2, wsaaUstxData, 28);
	private FixedLengthStringData[] wsaaPstcde = FLSDArrayPartOfArrayStructure(2, wsaaUstxData, 30);
	private ZonedDecimalData[] wsaaOccdate = ZDArrayPartOfArrayStructure(8, 0, wsaaUstxData, 32);

		/* WSAA-IND-ARRAY */
	private FixedLengthStringData[] wsaaUstxInd = FLSInittedArray (1000, 8);
	private BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(4, 4, 0, wsaaUstxInd, 0);
		/* WSAA-HOST-VARIABLES */
	private FixedLengthStringData wsaaN = new FixedLengthStringData(2).init("N ");
	private FixedLengthStringData wsaa1 = new FixedLengthStringData(1).init("1");
	private FixedLengthStringData wsaaLp = new FixedLengthStringData(2).init("LP");
	private ZonedDecimalData wsaaZeroes = new ZonedDecimalData(8, 0).init(ZERO);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).init(ZERO);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
		/* ERRORS */
	private static final String ivrm = "IVRM";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
	private DescTableDAM descIO = new DescTableDAM();
	private P6671par p6671par = new P6671par();

	public Br616() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** This section is invoked in restart mode. Note section 1100 must*/
		/** clear each temporary file member as the members are not under*/
		/** commitment control. Note also that Splitter programs must have*/
		/** a Restart Method of 1 (re-run). Thus no updating should occur*/
		/** which would result in different records being returned from the*/
		/** primary file in a restart!*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/*    Check the restart method is compatible with the program.*/
		/*    This program is restartable but will always re-run from*/
		/*    the beginning. This is because the temporary file members*/
		/*    are not under commitment control.*/
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*    Do a ovrdbf for each temporary file member. (Note we have*/
		/*    allowed for a maximum of 20.)*/
		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		/*    Since this program runs from a parameter screen move the*/
		/*    internal parameter area to the original parameter copybook*/
		/*    to retrieve the contract range*/
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES)
		&& isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		/*    The next thing we must do is construct the name of the*/
		/*    temporary file which we will be working with.*/
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		/*    Do a ovrdbf for each temporary file member. (Note we have*/
		/*    allowed for a maximum of 20.)*/
		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		/*    Log the number of threads being used*/
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		wsaaCompany.set(bsprIO.getCompany());
		wsaaEffdate.set(bsscIO.getEffectiveDate());
		sqlustxCursor = " SELECT  CHDRCOY, CHDRNUM, STMDTE, PTDATE, CNTTYPE, STATCODE, PSTCDE, OCCDATE" +
" FROM   " + getAppVars().getTableNameOverriden("CHDRPF") + " " +
" WHERE SERVUNIT = ?" +
" AND CHDRCOY = ?" +
" AND VALIDFLAG = ?" +
" AND STMDTE <> ?" +
" AND STMDTE <= ?" +
" AND BILLCHNL <> ?" +
" AND CHDRNUM BETWEEN ? AND ?" +
" ORDER BY CHDRCOY, CHDRNUM";
		/*    Initialise the array which will hold all the records*/
		/*    returned from each fetch. (INITIALIZE will not work as the*/
		/*    the array is indexed.*/
		iy.set(1);
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		sqlerrorflag = false;
		try {
			sqlustxCursorconn = getAppVars().getDBConnectionForTable(new com.csc.fsu.general.dataaccess.ChdrpfTableDAM());
			sqlustxCursorps = getAppVars().prepareStatementEmbeded(sqlustxCursorconn, sqlustxCursor, "CHDRPF");
			getAppVars().setDBString(sqlustxCursorps, 1, wsaaLp);
			getAppVars().setDBString(sqlustxCursorps, 2, wsaaCompany);
			getAppVars().setDBString(sqlustxCursorps, 3, wsaa1);
			getAppVars().setDBNumber(sqlustxCursorps, 4, wsaaZeroes);
			getAppVars().setDBNumber(sqlustxCursorps, 5, wsaaEffdate);
			getAppVars().setDBString(sqlustxCursorps, 6, wsaaN);
			getAppVars().setDBString(sqlustxCursorps, 7, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlustxCursorps, 8, wsaaChdrnumTo);
			sqlustxCursorrs = getAppVars().executeQuery(sqlustxCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
	}

protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

protected void openThreadMember1110()
	{
		/*    Clear the member for this thread in case we are in restart*/
		/*    mode in which case it might already contain data.*/
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(), 1), iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaUstxFn, wsaaThread, wsaaStatuz);
		if (isNE(wsaaStatuz, varcom.oK)) {
			syserrrec.syserrType.set("2");
			syserrrec.subrname.set("CLRTMPF");
			syserrrec.statuz.set(wsaaStatuz);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(USTX");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaUstxFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThread);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*    Open the file.*/
		if (isEQ(iz, 1)) {
			ustx01.openOutput();
		}
		if (isEQ(iz, 2)) {
			ustx02.openOutput();
		}
		if (isEQ(iz, 3)) {
			ustx03.openOutput();
		}
		if (isEQ(iz, 4)) {
			ustx04.openOutput();
		}
		if (isEQ(iz, 5)) {
			ustx05.openOutput();
		}
		if (isEQ(iz, 6)) {
			ustx06.openOutput();
		}
		if (isEQ(iz, 7)) {
			ustx07.openOutput();
		}
		if (isEQ(iz, 8)) {
			ustx08.openOutput();
		}
		if (isEQ(iz, 9)) {
			ustx09.openOutput();
		}
		if (isEQ(iz, 10)) {
			ustx10.openOutput();
		}
		if (isEQ(iz, 11)) {
			ustx11.openOutput();
		}
		if (isEQ(iz, 12)) {
			ustx12.openOutput();
		}
		if (isEQ(iz, 13)) {
			ustx13.openOutput();
		}
		if (isEQ(iz, 14)) {
			ustx14.openOutput();
		}
		if (isEQ(iz, 15)) {
			ustx15.openOutput();
		}
		if (isEQ(iz, 16)) {
			ustx16.openOutput();
		}
		if (isEQ(iz, 17)) {
			ustx17.openOutput();
		}
		if (isEQ(iz, 18)) {
			ustx18.openOutput();
		}
		if (isEQ(iz, 19)) {
			ustx19.openOutput();
		}
		if (isEQ(iz, 20)) {
			ustx20.openOutput();
		}
	}

protected void initialiseArray1500()
	{
		/*START*/
		wsaaStmdte[wsaaInd.toInt()].set(ZERO);
		wsaaPtdate[wsaaInd.toInt()].set(ZERO);
		wsaaOccdate[wsaaInd.toInt()].set(ZERO);
		wsaaChdrcoy[wsaaInd.toInt()].set(SPACES);
		wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaCnttype[wsaaInd.toInt()].set(SPACES);
		wsaaStatcode[wsaaInd.toInt()].set(SPACES);
		wsaaPstcde[wsaaInd.toInt()].set(SPACES);
		/*EXIT*/
	}

protected void readFile2000()
	{
		readFiles2010();
	}

protected void readFiles2010()
	{
		/*  Now a block of records is fetched into the array.*/
		/*  Also on the first entry into the program we must set up the*/
		/*  WSAA-PREV-CHDRNUM = the present chdrnum.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd, 1)) {
			return ;
		}
		sqlerrorflag = false;
		try {
			for (ustxCursorLoopIndex = 1; isLT(ustxCursorLoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlustxCursorrs); ustxCursorLoopIndex++ ){
				getAppVars().getDBObject(sqlustxCursorrs, 1, wsaaChdrcoy[ustxCursorLoopIndex], wsaaNullInd[ustxCursorLoopIndex][1]);
				getAppVars().getDBObject(sqlustxCursorrs, 2, wsaaChdrnum[ustxCursorLoopIndex], wsaaNullInd[ustxCursorLoopIndex][2]);
				getAppVars().getDBObject(sqlustxCursorrs, 3, wsaaStmdte[ustxCursorLoopIndex], wsaaNullInd[ustxCursorLoopIndex][3]);
				getAppVars().getDBObject(sqlustxCursorrs, 4, wsaaPtdate[ustxCursorLoopIndex], wsaaNullInd[ustxCursorLoopIndex][4]);
				getAppVars().getDBObject(sqlustxCursorrs, 5, wsaaCnttype[ustxCursorLoopIndex]);
				getAppVars().getDBObject(sqlustxCursorrs, 6, wsaaStatcode[ustxCursorLoopIndex]);
				getAppVars().getDBObject(sqlustxCursorrs, 7, wsaaPstcde[ustxCursorLoopIndex]);
				getAppVars().getDBObject(sqlustxCursorrs, 8, wsaaOccdate[ustxCursorLoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		/*  We must detect :-*/
		/*      a) no rows returned on the first fetch*/
		/*      b) the last row returned (in the block)*/
		/*  IF Either of the above cases occur, then an*/
		/*      SQLCODE = +100 is returned.*/
		/*  The 3000 section is continued for case (b).*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(varcom.endp);
		}
		//tom chi modified, bug 1030
		else if(isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)){
			wsspEdterror.set(varcom.endp);
		}
		//end
		else {
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}
	}

protected void edit2500()
	{
		/*READ*/
		/*  Check record is required for processsing but do not do any othe*/
		/*  reads to any other file. The exception to this rule are reads t*/
		/*  ITEM and these should be kept to a mimimum. Do not do any soft*/
		/*  locking in Splitter programs. The soft lock should be done by*/
		/*  the subsequent program which reads the temporary file.*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/* In the update section we write a record to the temporary file*/
		/* member identified by the value of IY. If it is possible to*/
		/* to include the RRN from the primary file we should pass*/
		/* this data as the subsequent program can then use it to do*/
		/* a direct read which is faster than a normal read.*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		/*  Re-initialise the block for next fetch and point to first row.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*  If the the CHDRNUM being processed is not equal to the*/
		/*  to the previous we should move to the next output file member.*/
		/*  The condition is here to allow for checking the last chdrnum*/
		/*  of the old block with the first of the new and to move to the*/
		/*  next PAYX member to write to if they have changed.*/
		if (isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)) {
			wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
			iy.add(1);
		}
		/*  Load from storage all USTX data for the same contract until*/
		/*  the CHDRNUM on USTX has changed,*/
		/*    OR*/
		/*  The end of an incomplete block is reached.*/
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		start3200();
	}

protected void start3200()
	{
		ustxpfData.chdrcoy.set(wsaaChdrcoy[wsaaInd.toInt()]);
		ustxpfData.chdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
		ustxpfData.statementDate.set(wsaaStmdte[wsaaInd.toInt()]);
		ustxpfData.ptdate.set(wsaaPtdate[wsaaInd.toInt()]);
		ustxpfData.cnttype.set(wsaaCnttype[wsaaInd.toInt()]);
		ustxpfData.statcode.set(wsaaStatcode[wsaaInd.toInt()]);
		ustxpfData.pstatcode.set(wsaaPstcde[wsaaInd.toInt()]);
		ustxpfData.occdate.set(wsaaOccdate[wsaaInd.toInt()]);
		if (isGT(iy, bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		/*    Write records to their corresponding temporary file*/
		/*    members, determined by the working storage count, IY.*/
		/*    This spreads the transaction members evenly across*/
		/*    the thread members.*/
		if (isEQ(iy, 1)) {
			ustx01.write(ustxpfData);
		}
		if (isEQ(iy, 2)) {
			ustx02.write(ustxpfData);
		}
		if (isEQ(iy, 3)) {
			ustx03.write(ustxpfData);
		}
		if (isEQ(iy, 4)) {
			ustx04.write(ustxpfData);
		}
		if (isEQ(iy, 5)) {
			ustx05.write(ustxpfData);
		}
		if (isEQ(iy, 6)) {
			ustx06.write(ustxpfData);
		}
		if (isEQ(iy, 7)) {
			ustx07.write(ustxpfData);
		}
		if (isEQ(iy, 8)) {
			ustx08.write(ustxpfData);
		}
		if (isEQ(iy, 9)) {
			ustx09.write(ustxpfData);
		}
		if (isEQ(iy, 10)) {
			ustx10.write(ustxpfData);
		}
		if (isEQ(iy, 11)) {
			ustx11.write(ustxpfData);
		}
		if (isEQ(iy, 12)) {
			ustx12.write(ustxpfData);
		}
		if (isEQ(iy, 13)) {
			ustx13.write(ustxpfData);
		}
		if (isEQ(iy, 14)) {
			ustx14.write(ustxpfData);
		}
		if (isEQ(iy, 15)) {
			ustx15.write(ustxpfData);
		}
		if (isEQ(iy, 16)) {
			ustx16.write(ustxpfData);
		}
		if (isEQ(iy, 17)) {
			ustx17.write(ustxpfData);
		}
		if (isEQ(iy, 18)) {
			ustx18.write(ustxpfData);
		}
		if (isEQ(iy, 19)) {
			ustx19.write(ustxpfData);
		}
		if (isEQ(iy, 20)) {
			ustx20.write(ustxpfData);
		}
		/*    Log the number of extacted records.*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
		/*  Set up the array for the next block of records.*/
		wsaaInd.add(1);
		/*  Check for an incomplete block retrieved.*/		
		if (isLTE(wsaaInd,wsaaRowsInBlock) && isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)) {
			wsaaEofInBlock.set("Y");
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/**    There is no pre-commit processing to be done*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close the open files and remove the overrides.*/
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		close4110();
	}

protected void close4110()
	{
		if (isEQ(iz, 1)) {
			ustx01.close();
		}
		if (isEQ(iz, 2)) {
			ustx02.close();
		}
		if (isEQ(iz, 3)) {
			ustx03.close();
		}
		if (isEQ(iz, 4)) {
			ustx04.close();
		}
		if (isEQ(iz, 5)) {
			ustx05.close();
		}
		if (isEQ(iz, 6)) {
			ustx06.close();
		}
		if (isEQ(iz, 7)) {
			ustx07.close();
		}
		if (isEQ(iz, 8)) {
			ustx08.close();
		}
		if (isEQ(iz, 9)) {
			ustx09.close();
		}
		if (isEQ(iz, 10)) {
			ustx10.close();
		}
		if (isEQ(iz, 11)) {
			ustx11.close();
		}
		if (isEQ(iz, 12)) {
			ustx12.close();
		}
		if (isEQ(iz, 13)) {
			ustx13.close();
		}
		if (isEQ(iz, 14)) {
			ustx14.close();
		}
		if (isEQ(iz, 15)) {
			ustx15.close();
		}
		if (isEQ(iz, 16)) {
			ustx16.close();
		}
		if (isEQ(iz, 17)) {
			ustx17.close();
		}
		if (isEQ(iz, 18)) {
			ustx18.close();
		}
		if (isEQ(iz, 19)) {
			ustx19.close();
		}
		if (isEQ(iz, 20)) {
			ustx20.close();
		}
	}
}
