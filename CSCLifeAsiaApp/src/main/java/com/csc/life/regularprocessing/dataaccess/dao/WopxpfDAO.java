package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.Wopxpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface WopxpfDAO extends BaseDAO<Wopxpf> {
	public List<Wopxpf> searchWopxRecord(String tableId, String memName, int batchExtractSize, int batchID);
}