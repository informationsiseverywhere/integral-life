package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.Linxpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface B5353DAO extends BaseDAO<Linxpf> {
	public List<Linxpf> loadDataByBatch(int batchID, String threadNumber);
	public int populateB5353Temp(int batchExtractSize, String linxtempTable, String memberName);
	public void initializeB5353Temp();
	public int populateB5353TempJpn(int batchExtractSize, String linxtempTable, String memberName);
	public int getB5353DataCount(String memberName);
}
