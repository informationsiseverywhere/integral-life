package com.csc.life.regularprocessing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:57
 * Description:
 * Copybook name: OVRBILLREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ovrbillrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData overbillRec = new FixedLengthStringData(68);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(overbillRec, 0);
  	public FixedLengthStringData function = new FixedLengthStringData(4).isAPartOf(overbillRec, 4);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(overbillRec, 8);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(overbillRec, 9);
  	public FixedLengthStringData batchkey = new FixedLengthStringData(19).isAPartOf(overbillRec, 17);
  	public FixedLengthStringData batcpfx = new FixedLengthStringData(2).isAPartOf(batchkey, 0);
  	public FixedLengthStringData batccoy = new FixedLengthStringData(1).isAPartOf(batchkey, 2);
  	public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(batchkey, 3);
  	public PackedDecimalData batcactyr = new PackedDecimalData(4, 0).isAPartOf(batchkey, 5);
  	public PackedDecimalData batcactmn = new PackedDecimalData(2, 0).isAPartOf(batchkey, 8);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(batchkey, 10);
  	public FixedLengthStringData batcbatch = new FixedLengthStringData(5).isAPartOf(batchkey, 14);
  	public PackedDecimalData newTranno = new PackedDecimalData(5, 0).isAPartOf(overbillRec, 36);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(overbillRec, 39);
  	public PackedDecimalData ptdate = new PackedDecimalData(8, 0).isAPartOf(overbillRec, 40);
  	public PackedDecimalData btdate = new PackedDecimalData(8, 0).isAPartOf(overbillRec, 45);
  	public PackedDecimalData tranDate = new PackedDecimalData(6, 0).isAPartOf(overbillRec, 50);
  	public PackedDecimalData tranTime = new PackedDecimalData(6, 0).isAPartOf(overbillRec, 54);
  	public ZonedDecimalData user = new ZonedDecimalData(6, 0).isAPartOf(overbillRec, 58).setUnsigned();
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(overbillRec, 64);


	public void initialize() {
		COBOLFunctions.initialize(overbillRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		overbillRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}