/*
 * File: Bh594.java
 * Date: 29 August 2009 21:36:16
 * Author: Quipoz Limited
 * 
 * Class transformed from BH594.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.model.Bextpf;
import com.csc.fsu.general.dataaccess.dao.BextpfDAO;
import com.csc.fsu.general.dataaccess.dao.impl.BextpfDAOImpl;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.anticipatedendowment.dataaccess.model.Loanpf;
import com.csc.life.enquiries.dataaccess.dao.ItdmpfDAO;
import com.csc.life.enquiries.dataaccess.dao.impl.ItdmpfDAOImpl;
import com.csc.life.enquiries.dataaccess.model.Itdmpf;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;

import com.csc.life.newbusiness.dataaccess.model.Payrpf;
import com.csc.life.productdefinition.batchprograms.Br523;
import com.csc.life.productdefinition.dataaccess.model.Br523DTO;
import com.csc.life.regularprocessing.dataaccess.HbxtpntTableDAM;
import com.csc.life.regularprocessing.dataaccess.dao.HbxtpntDAO;
import com.csc.life.regularprocessing.dataaccess.dao.impl.HbxtpntDAOImpl;
import com.csc.life.regularprocessing.dataaccess.model.Hbxtpnt;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*    This subroutine is aimed to produce premium notice LETC
*    records for those contracts using cash paying method for
*    premium collection and the associated next instalements
*    are due immediately.
*
*    N.B. There is no premium notice for Flexible Premium
*         Type contract.
*
*    CT01 - No. of BEXT records read
*    CT02 - No. of Premium Notice Produced
*
*****************************************************************
* </pre>
*/
public class Bh594 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	
	private java.sql.ResultSet sqlbextpf1rs = null;
	private java.sql.PreparedStatement sqlbextpf1ps = null;
	private java.sql.Connection sqlbextpf1conn = null;
	private String sqlbextpf1 = "";
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH594");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaDateFrom = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaDateTo = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaBillchnl = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBilflag = new FixedLengthStringData(1).init("Y");

	private FixedLengthStringData wsaaSqlBextDets = new FixedLengthStringData(17);
	private FixedLengthStringData wsaaSqlCompany = new FixedLengthStringData(1).isAPartOf(wsaaSqlBextDets, 0);
	private FixedLengthStringData wsaaSqlChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaSqlBextDets, 1);
	private PackedDecimalData wsaaSqlInstfrom = new PackedDecimalData(8, 0).isAPartOf(wsaaSqlBextDets, 9);
	private FixedLengthStringData wsaaSqlBillchnl = new FixedLengthStringData(2).isAPartOf(wsaaSqlBextDets, 14);
	private FixedLengthStringData wsaaSqlBilflag = new FixedLengthStringData(1).isAPartOf(wsaaSqlBextDets, 16);
	private ZonedDecimalData wsaaEffectiveDate = new ZonedDecimalData(8, 0).setUnsigned();

		/*01  WSAA-OTHER-KEYS.                                             */
	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);
		/* ERRORS */
	private static final String f379 = "F379";
	private static final String g437 = "G437";
	private static final String esql = "ESQL";
	private static final String payrrec = "PAYRREC";
	private static final String itemrec = "ITEMREC";
	private static final String itdmrec = "ITEMREC";
	private static final String hbxtpntrec = "HBXTPNTREC";
	private static final String t5729 = "T5729";
	private static final String tr384 = "TR384";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	
	private DescTableDAM descIO = new DescTableDAM();
	private HbxtpntTableDAM hbxtpntIO = new HbxtpntTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private P6671par p6671par = new P6671par();
	private Tr384rec tr384rec = new Tr384rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private static final Logger LOGGER = LoggerFactory.getLogger(Bh594.class);
	private Bextpf bextpf;
	Payrpf payrpf;
	private BextpfDAO bextpfdao=getApplicationContext().getBean("bextpfDAO", BextpfDAO.class);
	private HbxtpntDAO hbxtpntdao= getApplicationContext().getBean("hbxtpntdao", HbxtpntDAO.class);
	private ItdmpfDAO itdmdao=getApplicationContext().getBean("itdmdao", ItdmpfDAO.class);
//	private PayrpfDAOImpl payrdao=new PayrpfDAOImpl(); 
	private List<Bextpf> bextpflist= new LinkedList<Bextpf>();
	private List<Hbxtpnt> hbxtpntlist= new LinkedList<Hbxtpnt>();
	private List<Itdmpf> itdmlist= new LinkedList<Itdmpf>();
	private List<Payrpf> payrpflist= new LinkedList<Payrpf>();
	private Iterator<Bextpf> iteratorList; 
	private Iterator<Hbxtpnt> iteratorhbxt; 
	private Iterator<Itdmpf> iteratoritdm;
	private Iterator<Payrpf> iteratorpayrpf; 
	private int intBatchExtractSize;
	private Hbxtpnt hbxtpnt;
	private Itdmpf itdm;
	private Map<String, List<Itempf>> tr384ListMap;
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private int intBatchStep;


/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		eof2080, 
		exit2090
	}

	public Bh594() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/* Open required files.*/
		wsspEdterror.set(varcom.oK);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		wsaaDateFrom.set(bsscIO.getEffectiveDate());
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.frequency.set("12");
		datcon2rec.freqFactor.set(1);
		datcon2rec.intDate1.set(wsaaEffectiveDate);
		intBatchStep = 0;
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError600();
		}
		wsaaDateTo.set(datcon2rec.intDate2);
		wsaaCompany.set(bsprIO.getCompany());
		wsaaBillchnl.set(bprdIO.getSystemParam01());
		if (isEQ(p6671par.chdrnum, SPACES)
		&& isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set("0");
			wsaaChdrnumTo.set("99999999");
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		
		if (bprdIO.systemParam02.isNumeric()){
            if (bprdIO.systemParam02.toInt() > 0){
                intBatchExtractSize = bprdIO.systemParam02.toInt();
            }else{
                intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
            }
        }else{
            intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();  
        }
        readChunkRecord();
				/*move to dao*/
				/* Declare cursor to read BEXT by Billing Channel.
				sqlbextpf1 = " SELECT  CHDRCOY, CHDRNUM, INSTFROM, BILLCHNL, BILFLAG" +
		" FROM   " + getAppVars().getTableNameOverriden("BEXTPF") + " " +
		" WHERE CHDRCOY = ?" +
		" AND (CHDRNUM BETWEEN ? AND ?)" +
		" AND (INSTFROM BETWEEN ? AND ?)" +
		" AND BILLCHNL = ?" +
		" AND BILFLAG <> ?";
				/* Open cursor to read BEXT records.
				sqlerrorflag = false;
				try {
					sqlbextpf1conn = getAppVars().getDBConnectionForTable(new com.csc.fsu.general.dataaccess.BextpfTableDAM());
					sqlbextpf1ps = getAppVars().prepareStatementEmbeded(sqlbextpf1conn, sqlbextpf1, "BEXTPF");
					getAppVars().setDBString(sqlbextpf1ps, 1, wsaaCompany);
					getAppVars().setDBString(sqlbextpf1ps, 2, wsaaChdrnumFrom);
					getAppVars().setDBString(sqlbextpf1ps, 3, wsaaChdrnumTo);
					getAppVars().setDBNumber(sqlbextpf1ps, 4, wsaaDateFrom);
					getAppVars().setDBNumber(sqlbextpf1ps, 5, wsaaDateTo);
					getAppVars().setDBString(sqlbextpf1ps, 6, wsaaBillchnl);
					getAppVars().setDBString(sqlbextpf1ps, 7, wsaaBilflag);
					sqlbextpf1rs = getAppVars().executeQuery(sqlbextpf1ps);
				}
				catch (SQLException ex){
					sqlerrorflag = true;
					getAppVars().setSqlErrorCode(ex);
				}
				if (sqlerrorflag) {
					sqlError9000();
				}*/
				
		
		
	}
private void readChunkRecord(){
	
	/*bextpflist=bextpfdao.read(getAppVars().getTableNameOverriden("BEXTPF"),wsaaCompany.toString(),wsaaChdrnumFrom.toString(),wsaaChdrnumTo.toString(),wsaaDateFrom.toInt(),wsaaDateTo.toInt(),wsaaBillchnl.toString(),wsaaBilflag.toString());
	if (bextpflist.isEmpty()) 
	{
		wsspEdterror.set(varcom.endp);
	}
	else 
	{
		iteratorList = bextpflist.iterator();		
	}*/
	int batchid=intBatchStep;
	bextpflist=bextpfdao.readBextpf(getAppVars().getTableNameOverriden("BEXTPF"),wsaaCompany.toString(),wsaaChdrnumFrom.toString(),wsaaChdrnumTo.toString(),wsaaDateFrom.toInt(),wsaaDateTo.toInt(),wsaaBillchnl.toString(),wsaaBilflag.toString(),intBatchExtractSize,batchid);
	if (bextpflist.isEmpty()) 
	{
		wsspEdterror.set(varcom.endp);
	}
	else 
	{
		iteratorList = bextpflist.iterator();		
	}
	
}

/*protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case eof2080: 
					eof2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}*/

protected void readFile2000()
{
	wsspEdterror.set(varcom.oK);
	if (!bextpflist.isEmpty()){
		if (!iteratorList.hasNext()) {
			intBatchStep++;	
			readChunkRecord();
			if (!bextpflist.isEmpty()){
				bextpf = new Bextpf();
				bextpf = iteratorList.next();
				//br539dto = loanpf.getBr539DTO();			
			}
		}else {		
			bextpf = new Bextpf();
			bextpf = iteratorList.next();
			//br539dto = bextpf.getBr539DTO();
		}
		wsaaSqlCompany.set(bextpf.getChdrcoy());
		wsaaSqlChdrnum.set(bextpf.getChdrnum());
		wsaaSqlInstfrom.set(bextpf.getInstfrom());
		wsaaSqlBillchnl.set(bextpf.getBillchnl());
		wsaaSqlBilflag.set(bextpf.getBilflag());
	}
	else wsspEdterror.set(varcom.endp);

}

/*protected void readFile2010()
	{
		wsspEdterror.set(varcom.oK);
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlbextpf1rs)) {
				getAppVars().getDBObject(sqlbextpf1rs, 1, wsaaSqlCompany);
				getAppVars().getDBObject(sqlbextpf1rs, 2, wsaaSqlChdrnum);
				getAppVars().getDBObject(sqlbextpf1rs, 3, wsaaSqlInstfrom);
				getAppVars().getDBObject(sqlbextpf1rs, 4, wsaaSqlBillchnl);
				getAppVars().getDBObject(sqlbextpf1rs, 5, wsaaSqlBilflag);
			}
			else {
				goTo(GotoLabel.eof2080);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError9000();
		}
		goTo(GotoLabel.exit2090);
	}*/

/*protected void eof2080()
	{
		 Close cursor of reading BEXT records.
		getAppVars().freeDBConnectionIgnoreErr(sqlbextpf1conn, sqlbextpf1ps, sqlbextpf1rs);
		wsspEdterror.set(varcom.endp);
	}*/

protected void edit2500()
	{
		edit2510();
	}

protected void edit2510()
	{
		/*  Check record is required for processing.*/
		/*  Softlock the record if it is to be updated.*/
		wsspEdterror.set(varcom.oK);
		/*  Accumulate no. of BEXT records read.*/
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		/*  Exclude Flexible Premium type contracts.*/
		/*hbxtpntIO.setParams(SPACES);
		hbxtpntIO.setChdrcoy(wsaaSqlCompany);
		hbxtpntIO.setChdrnum(wsaaSqlChdrnum);
		hbxtpntIO.setInstfrom();
		hbxtpntIO.setFormat(hbxtpntrec);
		hbxtpntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hbxtpntIO);
		if (isNE(hbxtpntIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hbxtpntIO.getParams());
			syserrrec.statuz.set(hbxtpntIO.getStatuz());
			fatalError600();
		}*/
		hbxtpntlist=hbxtpntdao.readHbxtpnt(wsaaSqlCompany.toString(), wsaaSqlChdrnum.toString(), wsaaSqlInstfrom.toInt());
		iteratorhbxt=hbxtpntlist.iterator();
		if(!hbxtpntlist.isEmpty())
		{
			hbxtpnt=new Hbxtpnt();
			hbxtpnt=iteratorhbxt.next();
			
		}
		else
		{
			wsspEdterror.set(varcom.endp);
			return;
		}
	
		itdmlist=itdmdao.readItdm(smtpfxcpy.item.toString(), wsaaSqlCompany.toString(), "t5729", hbxtpnt.getCnttype(), wsaaSqlInstfrom.toInt());
		//iteratoritdm=itdmlist.iterator();
		if(!itdmlist.isEmpty())
		{
			boolean flag=hbxtpntdao.delete(hbxtpnt);
			if (!flag) 
			{
				fatalError600();
			}
			wsspEdterror.set(varcom.endp);
			return;
		}
		/*else
		{
			itdm=new Itdmpf();
			itdm=iteratoritdm.next();
		}*/
		/*if (itdm.getItemitem() == null || hbxtpnt.getCnttype() == null) {
			return;
		}*/
		/*if (isEQ(itdm.getItemitem(), hbxtpnt.getCnttype())) 
		{
							boolean flag=hbxtpntdao.delete(hbxtpnt);
							
							if (!flag) 
							{
								fatalError600();
							}
		}*/

		/*itdmIO.setStatuz(SPACES);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(wsaaSqlCompany);
		itdmIO.setItemtabl(t5729);
		itdmIO.setItemitem(hbxtpntIO.getCnttype());
		itdmIO.setItmfrm(wsaaSqlInstfrom);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getItemitem(), hbxtpntIO.getCnttype())
			&& isEQ(itdmIO.getStatuz(),varcom.oK)) {
			hbxtpntIO.setFunction(varcom.deltd);
			SmartFileCode.execute(appVars, hbxtpntIO);
			if (isNE(hbxtpntIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hbxtpntIO.getStatuz());
				syserrrec.params.set(hbxtpntIO.getParams());
				fatalError600();
			}
			wsspEdterror.set(SPACES);
		}*/
	}

protected void update3000()
	{
		update3010();
		continue3080();
	}

protected void update3010()
	{
		/*  Produce the LETC record if BEXT record match the selection*/
		/*  Criteria.*/
		/*hbxtpntIO.setParams(SPACES);
		hbxtpntIO.setChdrcoy(wsaaSqlCompany);
		hbxtpntIO.setChdrnum(wsaaSqlChdrnum);
		hbxtpntIO.setInstfrom(wsaaSqlInstfrom);
		hbxtpntIO.setFormat(hbxtpntrec);
		hbxtpntIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hbxtpntIO);
		if (isNE(hbxtpntIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(hbxtpntIO.getStatuz());
			syserrrec.params.set(hbxtpntIO.getParams());
			fatalError600();
		}*/
	
		hbxtpntlist=hbxtpntdao.readHbxtpnt(wsaaSqlCompany.toString(), wsaaSqlChdrnum.toString(), wsaaSqlInstfrom.toInt());
		iteratorhbxt=hbxtpntlist.iterator();
		if(!hbxtpntlist.isEmpty())
		{
			hbxtpnt=new Hbxtpnt();
			hbxtpnt=iteratorhbxt.next();
			
		}

		payrpflist=hbxtpntdao.readPayrpfTranno(1, hbxtpnt);
		//payrpflist=payrdao.readpayr(hbxtpnt.getChdrcoy(), hbxtpnt.getChdrnum(), 1);
		iteratorpayrpf=payrpflist.iterator();
		if(!payrpflist.isEmpty())
		{
			payrpf=new Payrpf();
			payrpf=iteratorpayrpf.next();
		}
		else
		{
			wsspEdterror.set(varcom.endp);
		}	
		
		/*payrIO.setParams(SPACES);
		payrIO.setChdrcoy(hbxtpntIO.getChdrcoy());
		payrIO.setChdrnum(hbxtpntIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)
		&& isNE(payrIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(payrIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(f379);
			fatalError600();
		}*/
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(hbxtpnt.getCnttype());
			stringVariable1.addExpression(bprdIO.getAuthCode());
			String keyItemitem = stringVariable1.toString().trim();
		    tr384ListMap=itemDAO.loadSmartTable(smtpfxcpy.item.toString(), hbxtpnt.getChdrcoy(), tr384);
			List<Itempf> itempfList = new ArrayList<Itempf>();
			boolean itemFound = false;
			if (tr384ListMap.containsKey(keyItemitem)){	
				itempfList = tr384ListMap.get(keyItemitem);
				Iterator<Itempf> iterator = itempfList.iterator();
				while (iterator.hasNext()) {
					Itempf itempf = new Itempf();
					itempf = iterator.next();
					itemFound = true;
					tr384rec.tr384Rec.set(StringUtil.rawToString(itempf.getGenarea()));
						}
									
					}				

			if (!itemFound) {
				syserrrec.params.set(tr384rec.tr384Rec);
				syserrrec.statuz.set(g437);
				fatalError600();		
			}	
			/*itemIO.setParams(SPACES);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(hbxtpnt.getChdrcoy());
			 MOVE T6634                  TO ITEM-ITEMTABL.                
			itemIO.setItemtabl(tr384);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(hbxtpnt.getCnttype());
			stringVariable1.addExpression(bprdIO.getAuthCode());
			stringVariable1.setStringInto(itemIO.getItemitem());
			itemIO.setItemseq(SPACES);
			itemIO.setFormat(itemrec);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.statuz.set(g437);
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}*/
			/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.              */
			//tr384rec.tr384Rec.set(itemIO.getGenarea());
			if (isEQ(tr384rec.letterType, SPACES)) {
				return ;
			}
			letrqstrec.statuz.set(SPACES);
			/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.          */
			letrqstrec.letterType.set(tr384rec.letterType);
			letrqstrec.letterRequestDate.set(wsaaDateFrom);
			letrqstrec.clntcoy.set(hbxtpnt.getCowncoy());
			letrqstrec.clntnum.set(hbxtpnt.getCownnum());
			letrqstrec.rdocpfx.set(hbxtpnt.getChdrpfx());
			letrqstrec.requestCompany.set(hbxtpnt.getChdrcoy());
			letrqstrec.chdrcoy.set(hbxtpnt.getChdrcoy());
			letrqstrec.rdoccoy.set(hbxtpnt.getChdrcoy());
			letrqstrec.rdocnum.set(hbxtpnt.getChdrnum());
			letrqstrec.chdrnum.set(hbxtpnt.getChdrnum());
			letrqstrec.branch.set(hbxtpnt.getCntbranch());
			letrqstrec.tranno.set(payrpf.getTranno());
			letrqstrec.trcde.set(bprdIO.getAuthCode());
			/*    MOVE BSSC-LANGUAGE          TO WSAA-LANGUAGE.                */
			/*    MOVE HBXTPNT-INSTFROM       TO WSAA-OTH-DATE-FROM.           */
			/*    MOVE WSAA-OTHER-KEYS        TO LETRQST-OTHER-KEYS.           */
			letcokcpy.ldDate.set(hbxtpnt.getInstfrom());
			letrqstrec.otherKeys.set(letcokcpy.saveOtherKeys);
			letrqstrec.function.set("ADD");
			callProgram(Letrqst.class, letrqstrec.params);
			if (isNE(letrqstrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(letrqstrec.statuz);
				syserrrec.params.set(letrqstrec.params);
				fatalError600();
			}
			/*  Accumulate no. of Premium Notice printed.*/
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
		
		
		
		
	}

protected void continue3080()
	{
		boolean updatedrecord=hbxtpntdao.update("Y", hbxtpnt);
		if (!updatedrecord) 
		{
			fatalError600();
		}
		/*hbxtpntIO.setBilflag("Y");
		hbxtpntIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, hbxtpntIO);
		if (isNE(hbxtpntIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(hbxtpntIO.getStatuz());
			syserrrec.params.set(hbxtpntIO.getParams());
			fatalError600();*/
	}
		/*EXIT*/
	

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close any open files.*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError9000()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
