package com.csc.life.regularprocessing.dataaccess.model;

import com.csc.life.regularprocessing.dataaccess.model.Bonlpf;
import com.csc.life.regularprocessing.dataaccess.model.Bonspf;
public class B5018ForUpdateDTO {
	private Bonspf bonspf;
	private Bonlpf bonlpf;
	public Bonspf getBonspf() {
		return bonspf;
	}
	public void setBonspf(Bonspf bonspf) {
		this.bonspf = bonspf;
	}
	public Bonlpf getBonlpf() {
		return bonlpf;
	}
	public void setBonlpf(Bonlpf bonlpf) {
		this.bonlpf = bonlpf;
	}
	
}
