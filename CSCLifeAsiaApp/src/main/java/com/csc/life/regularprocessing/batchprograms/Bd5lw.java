/*
 * File: Bd5lw.java
 * Date: 28 Jan 2001 21:09:19
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.dataaccess.dao.Bd5lwTempDAO;
import com.csc.life.regularprocessing.dataaccess.model.Bd5lwDTO;
import com.csc.life.regularprocessing.procedures.Overbill;
import com.csc.life.regularprocessing.recordstructures.Ovrbillrec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Contot;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.math.BigDecimal;
import java.util.*;

import static com.quipoz.COBOLFramework.COBOLFunctions.*;

/**
*/
public class Bd5lw extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Bd5lw");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaprevChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaacurrChdrnum = new FixedLengthStringData(8);
	private int minRecord = 1;
	private int incrRange;
	private int ctrCT01;
	private int ctrCT02;
	private FixedLengthStringData wsaaOverBillFlag = new FixedLengthStringData(1).init("Y");
	private FixedLengthStringData wsaaUpdateFlag = new FixedLengthStringData(1).init("N");
	private FixedLengthStringData wsaaSurrFlag = new FixedLengthStringData(1).init("N");
	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init("N");
    private FixedLengthStringData wsaaPayrNotexist = new FixedLengthStringData(1).init("N");
	private PackedDecimalData wsaaLoanValue = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaCashValue = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();

	/* ERRORS */
	private static final String f294 = "F294";
	private static final String ivrm = "IVRM";
	/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	/* TABLES */
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Totloanrec totloanrec = new Totloanrec();
	private P6671par p6671par = new P6671par();

	private Bd5lwTempDAO bd5lwTempDAO = getApplicationContext().getBean("bd5lwTempDAO", Bd5lwTempDAO.class);
	private Bd5lwDTO bd5lwdto = new Bd5lwDTO();
	private List<Bd5lwDTO> bd5lwdtoList = new ArrayList<Bd5lwDTO>();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
    private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
    private Payrpf payrpf = new Payrpf();
	private Iterator<Bd5lwDTO> iterator;

    private Srcalcpy srcalcpy = new Srcalcpy();
	private ExternalisedRules er = new ExternalisedRules();
	private Map<String, List<Itempf>> t5679ListMap;
	private Map<String, List<Itempf>> t5687ListMap;
	private Map<String, List<Itempf>> t6598ListMap;
    private Map<String, List<Itempf>> t5540ListMap; //ICIL-1032
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
    private T5540rec t5540rec = new T5540rec(); //ICIL-1032
    private Chdrpf chdrlifIO = new Chdrpf();

	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO =  getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private static final int wsaaUser = 0;
	private ZonedDecimalData wsaaTransactionDate = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTransactionTime = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private Ptrnpf ptrnpf;
	private ZonedDecimalData wsaaNewTranno = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaCurrTranno = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private List<Ptrnpf> ptrnBulkInsList = new ArrayList<Ptrnpf>();
	private static final Logger LOGGER = LoggerFactory.getLogger(Bd5lw.class);

	private List<Chdrpf> updateChdrpfInvalidList = null;
	private List<Chdrpf> insertChdrpfValidList = null;

	private List<Covrpf> covrlnbList = null;
	private List<Covrpf> updateCovrlnbList = null;
	private List<Covrpf> insertCovrlnbList = null;
    private List<Payrpf> payrpfUpdateList = new ArrayList<Payrpf>();
    private List<Payrpf> payrpfInsertList = new ArrayList<Payrpf>();

	private FixedLengthStringData wsaaTrcde = new FixedLengthStringData(1);
	private Validator trcdeMatch = new Validator(wsaaTrcde, "Y");
	private Validator trcdeNotMatch = new Validator(wsaaTrcde, "N");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private boolean noRecordFound;
    private Ovrbillrec ovrbillrec = new Ovrbillrec();
    private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4);

	public Bd5lw() {
		super();
	}


	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}

	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}

	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray) {
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void restart0900() {
		/*RESTART*/
		/*EXIT*/
	}

	protected void initialise1000() {
		if (isNE(bprdIO.getRestartMethod(), "3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*    Get today's date by using DATCON1*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		noRecordFound = false;
        wsaaTermid.set(varcom.vrcmTermid);
		wsspEdterror.set(varcom.oK);
		ctrCT01 = 0;
		ctrCT02 = 0;
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES)
				&& isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		} else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}

		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				incrRange = bprdIO.systemParam01.toInt();
			} else {
				incrRange = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			incrRange = bprdIO.cyclesPerCommit.toInt();
		}
		performDataChunk();
		readSmartTables(bsprIO.getCompany().toString());

		wsaaTransactionDate.set(getCobolDate());
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaTransactionDate.set(wsaaToday);
	}

	protected void performDataChunk() {
		if (bd5lwTempDAO.deleteBd5lwTempAllRecords()) {
			if (bd5lwTempDAO.insertBd5lwTempRecords(bsprIO.getCompany().toString(), wsaaChdrnumFrom.toString(), wsaaChdrnumTo.toString())) {
				bd5lwdtoList = bd5lwTempDAO.loadData(minRecord, minRecord + incrRange);
			} else {
				noRecordFound = true;
				syserrrec.params.set("Data Chunk Error");
				syserrrec.statuz.set("MRNF");
				fatalError600();
			}
		}

		if (bd5lwdtoList != null && bd5lwdtoList.size() > 0) {
			iterator = bd5lwdtoList.iterator();
		} else {
			wsspEdterror.set(varcom.endp);
		}
	}

	private void readSmartTables(String company){
		t5679ListMap = itemDAO.loadSmartTable("IT", company, "T5679");
		t5687ListMap = itemDAO.loadSmartTable("IT", company, "T5687");
		t6598ListMap = itemDAO.loadSmartTable("IT", company, "T6598");
        t5540ListMap = itemDAO.loadSmartTable("IT", company, "T5540"); //ICIL-1032
	}

	protected void readFile2000() {
		if (bd5lwdtoList != null && bd5lwdtoList.size() > 0) {
			if (iterator.hasNext()) {
				bd5lwdto = iterator.next();
			} else {
				minRecord += incrRange;
				bd5lwdtoList = bd5lwTempDAO.loadData(minRecord, minRecord + incrRange);
				if (bd5lwdtoList != null && bd5lwdtoList.size() > 0) {
					iterator = bd5lwdtoList.iterator();
					if (iterator.hasNext()) {
						bd5lwdto = iterator.next();
					}
				} else {
					wsspEdterror.set(varcom.endp);
				}
			}
		} else {
			wsspEdterror.set(varcom.endp);
		}
	}

	protected void commitControlTotals(){
		//ct01
		contotrec.totno.set(ct01);
		contotrec.totval.set(ctrCT01);
		callProgram(Contot.class, contotrec.contotRec);
		ctrCT01 = 0;
		//ct02
		contotrec.totno.set(ct02);
		contotrec.totval.set(ctrCT02);
		callProgram(Contot.class, contotrec.contotRec);
		ctrCT02 = 0;

	}

	protected void edit2500() {
		wsspEdterror.set(varcom.oK);
		wsaacurrChdrnum.set(bd5lwdto.getChdrnum());
		if (isNE(wsaaprevChdrnum, wsaacurrChdrnum)) {
			wsaaprevChdrnum.set(wsaacurrChdrnum);
		} else {
			wsspEdterror.set(SPACES);
			return;
		}
		wsaaUpdateFlag.set("N");
		CompCashvalueCash2600();
		if ((isEQ(wsaaUpdateFlag, "Y"))
		&& (isEQ(wsspEdterror, varcom.oK))){
			ctrCT01++;
			softlock2700();
			if (isEQ(sftlockrec.statuz, "LOCK")) {
				wsspEdterror.set(SPACES);
				return;
			}
		} else {
			wsspEdterror.set(SPACES);
			ctrCT02++;
		}
	}

    //ICIL-1032 new section
    protected void readT55402550() {
        String keyItemitem = covrpf.getCrtable().trim();
        List<Itempf> itempfList = new ArrayList<Itempf>();
        boolean itemFound = false;
        if (t5540ListMap.containsKey(keyItemitem)){
            wsspEdterror.set(SPACES);
        }
    }

    protected void CompCashvalueCash2600() {
		wsaaLoanValue.set(0);
		wsaaCashValue.set(0);
		wsaaPayrNotexist.set("N");
		calcTotalLoan2610();
		calcCashvalue2620();
		if (isGT(wsaaLoanValue, wsaaCashValue)
        &&  isNE(wsaaPayrNotexist, "Y")) {
			wsaaUpdateFlag.set("Y");
		}
	}

	private void calcTotalLoan2610() {
		/*  Read TOTLOAN to get value of loans for this contract.          */
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(bd5lwdto.getChdrcoy());
		totloanrec.chdrnum.set(bd5lwdto.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(bsscIO.getEffectiveDate().toInt());
		totloanrec.function.set("LOAN");
		totloanrec.language.set(bsscIO.getLanguage());
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(wsaaLoanValue, 2).set(add(totloanrec.principal, totloanrec.interest));
	}

	private void calcCashvalue2620() {
		wsaaSurrFlag.set("N");
		getValidSurr2630();
	}

	private void getValidSurr2630() {
		List<Covrpf> list = covrpfDAO.getCovrsurByComAndNum(bd5lwdto.getChdrcoy(), bd5lwdto.getChdrnum());/* IJTI-1523 */
		if (list == null || list.size() == 0) {
			syserrrec.params.set(bd5lwdto.getChdrcoy() + bd5lwdto.getChdrnum());/* IJTI-1523 */
			syserrrec.statuz.set("MRNF");
			fatalError600();
		} else {
			Iterator<Covrpf> iterator = list.iterator();
			while (iterator.hasNext()) {
				covrpf = iterator.next();
				//ICIL-1032 start
				readT55402550();
				if (isNE(wsspEdterror, varcom.oK)) {
					return;
				}
				//ICIL-1032 end
				readT56872650();
				if (isEQ(wsaaSurrFlag, "Y")) {
					CalcSurrvalue2640();
					wsaaSurrFlag.set("N");
				}
			}
		}
	}

	protected void readT56872650(){
		String keyItemitem = covrpf.getCrtable().trim();/* IJTI-1523 */
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (t5687ListMap.containsKey(keyItemitem)){
			itempfList = t5687ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext() && isEQ(wsaaSurrFlag, "N")) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(itempf.getItmfrm().compareTo(new BigDecimal(covrpf.getCrrcd())) < 1 ){
					t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;
					if (isNE(t5687rec.svMethod.toString().trim(),' ')) {
						wsaaSurrFlag.set("Y");
					}
				}
			}
		}
		if (!itemFound) {
			syserrrec.params.set(t5687rec.t5687Rec);
			syserrrec.statuz.set(f294);
			fatalError600();
		}
	}

	private void CalcSurrvalue2640() {
		getSurrSubr2650();
		srcalcpy.ptdate.set(bd5lwdto.getPtdate());
		srcalcpy.currcode.set(covrpf.getPremCurrency());
		valCovrStatus2660();
		srcalcpy.endf.set(SPACE);
        srcalcpy.tsvtot.set(ZERO);
        srcalcpy.tsv1tot.set(ZERO);
        srcalcpy.estimatedVal.set(ZERO);
        srcalcpy.actualVal.set(ZERO);
        srcalcpy.chdrChdrcoy.set(covrpf.getChdrcoy());
        srcalcpy.chdrChdrnum.set(covrpf.getChdrnum());
        srcalcpy.lifeLife.set(covrpf.getLife());
        srcalcpy.lifeJlife.set(covrpf.getJlife());
        srcalcpy.covrCoverage.set(covrpf.getCoverage());
        srcalcpy.covrRider.set(covrpf.getRider());
        srcalcpy.planSuffix.set(covrpf.getPlanSuffix());
        srcalcpy.crtable.set(covrpf.getCrtable());
        srcalcpy.convUnits.set(covrpf.getConvertInitialUnits());
        srcalcpy.crrcd.set(covrpf.getCrrcd());
        srcalcpy.status.set(SPACES);
        srcalcpy.pstatcode.set(covrpf.getPstatcode());
        srcalcpy.polsum.set(bd5lwdto.getPolsum());
        srcalcpy.chdrCurr.set(bd5lwdto.getCntcurr());
        srcalcpy.language.set(bsscIO.getLanguage());
        payrpf = payrpfDAO.getpayrRecord(covrpf.getChdrcoy(),covrpf.getChdrnum(),1);/* IJTI-1523 */
        if(payrpf !=null){
			if(isEQ(t5687rec.singlePremInd, "Y")) {
				srcalcpy.billfreq.set("00");
			} else {
				srcalcpy.billfreq.set(payrpf.getBillfreq());
			}
			while ( !(isEQ(srcalcpy.status,varcom.endp))) {
				callSurrSubr2680();
			}
		} else {
            wsaaPayrNotexist.set("Y");
        }

	}

	private void getSurrSubr2650() {
		String keyItemitem = t5687rec.svMethod.toString().trim();
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (t6598ListMap.containsKey(keyItemitem)){
			itempfList = t6598ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				t6598rec.t6598Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;
			}
		}else{
			t6598rec.t6598Rec.set(SPACES);
		}
	}

	private void valCovrStatus2660() {
		readT56792670();
		wsaaValidStatus.set("N");
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
				|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			riskStatusCheckWp2661();
		}
		if (isEQ(wsaaValidStatus, "Y")) {
			wsaaValidStatus.set("N");
			for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
					|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
				premStatusCheckWp2662();
			}
		}
	}

	private void riskStatusCheckWp2661() {
		if (isEQ(covrpf.getRider(), ZERO)) {
			if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrpf.getStatcode()))) {
					wsaaValidStatus.set("Y");
					return ;
				}
			}
		}
		if (isGT(covrpf.getRider(), ZERO)) {
			if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrpf.getStatcode()))) {
					wsaaValidStatus.set("Y");
					return ;
				}
			}
		}
	}

	private void premStatusCheckWp2662() {
		if (isEQ(covrpf.getRider(), ZERO)) {
			if (isNE(t5679rec.covPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrpf.getPstatcode()))) {
					wsaaValidStatus.set("Y");
					return ;
				}
			}
		}
		if (isGT(covrpf.getRider(), ZERO)) {
			if (isNE(t5679rec.ridPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()], covrpf.getPstatcode()))) {
					wsaaValidStatus.set("Y");
					return ;
				}
			}
		}
	}

	private void readT56792670() {
		String keyItemitem = bprdIO.getAuthCode().toString().trim();
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (t5679ListMap.containsKey(keyItemitem)){
			itempfList = t5679ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;
			}
		}
		if (!itemFound) {
			syserrrec.params.set(t5679rec.t5679Rec);
			syserrrec.statuz.set(f294);
			fatalError600();
		}
	}

	private void callSurrSubr2680() {
        if (isEQ(t6598rec.calcprog,SPACES)
                ||  isEQ(wsaaValidStatus,"N")) {
            srcalcpy.status.set(varcom.endp);
            return;
        }

        srcalcpy.effdate.set(bsscIO.getEffectiveDate().toInt());
        if (isLT(covrpf.getRiskCessDate(), srcalcpy.effdate)) {
            srcalcpy.effdate.set(covrpf.getRiskCessDate());
        }
        srcalcpy.type.set("F");
        if (isGT(covrpf.getInstprem(),ZERO)) {
            srcalcpy.singp.set(covrpf.getInstprem());
        } else {
            srcalcpy.singp.set(covrpf.getSingp());
        }
        srcalcpy.tsvtot.set(ZERO);
        srcalcpy.tsv1tot.set(ZERO);

        callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);

        if (isEQ(srcalcpy.status,varcom.bomb)) {
            syserrrec.statuz.set(srcalcpy.status);
            fatalError600();
        }
        if (isNE(srcalcpy.status,varcom.oK)
                && isNE(srcalcpy.status,varcom.endp)) {
            syserrrec.statuz.set(srcalcpy.status);
            fatalError600();
        }
        if (isEQ(srcalcpy.status, varcom.oK)) {
            wsaaCashValue.add(srcalcpy.actualVal);
        }
	}

	protected void softlock2700() {
		/* Soft lock the contract, if it is to be processed.               */
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(bd5lwdto.getChdrcoy());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(bd5lwdto.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	protected void releaseSoftlock2700() {
		sftlockrec.function.set("UNLK");
		sftlockrec.company.set(bd5lwdto.getChdrcoy());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(bd5lwdto.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	protected void update3000() {
		/*UPDATE*/
		callOverbill9000();
        updateChdr3100();
		updateCovr3200();
		updatePayr5900();
		ptrnBatcup3300();
		releaseSoftlock2700();
		/*EXIT*/
	}

	protected void updateChdr3100()
	{
        if(updateChdrpfInvalidList == null){
            updateChdrpfInvalidList = new ArrayList<Chdrpf>();
        }
        chdrlifIO = chdrpfDAO.getchdrRecord(bd5lwdto.getChdrcoy(),bd5lwdto.getChdrnum());
        if(isEQ(wsaaOverBillFlag,'Y')){
			chdrlifIO.setTranno(chdrlifIO.getTranno()-1);
		}
        Chdrpf chdrpfOld = new Chdrpf(chdrlifIO);
        chdrpfOld.setUniqueNumber(chdrlifIO.getUniqueNumber());
        chdrpfOld.setValidflag('2');
        chdrpfOld.setTranno(chdrlifIO.getTranno());
        updateChdrpfInvalidList.add(chdrpfOld);

        if((insertChdrpfValidList == null)
		|| isEQ(insertChdrpfValidList.size(), 0)){
            insertChdrpfValidList = new ArrayList<>();
        }
        Chdrpf chdrpfNew = new Chdrpf(chdrlifIO);
        chdrpfNew.setUniqueNumber(chdrlifIO.getUniqueNumber());
        chdrpfNew.setValidflag('1');
        chdrpfNew.setCurrfrom(wsaaToday.toInt());
        chdrpfNew.setCurrto(varcom.vrcmMaxDate.toInt());
        /*   Update the Status field*/
        if (isNE(t5679rec.setCnRiskStat, SPACES)) {
            chdrpfNew.setStatcode(t5679rec.setCnRiskStat.toString());
        }
        /*   Update the Premium Status field*/
        if (isNE(t5679rec.setCnPremStat, SPACES)) {
            chdrpfNew.setPstcde(t5679rec.setCnPremStat.toString());
        }
        chdrpfNew.setTranno(chdrpfNew.getTranno()+1);
        wsaaNewTranno.set(chdrpfNew.getTranno());
        insertChdrpfValidList.add(chdrpfNew);
	}

	protected void updateCovr3200()
	{
		if (covrlnbList == null) {
			covrlnbList = covrpfDAO.getCovrsurByComAndNum(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		}
		for (Covrpf covr : covrlnbList) {
			callCovrlnb3201(covr);
		}
	}

	protected void callCovrlnb3201(Covrpf covrlnbIO)
	{
		if (isEQ(covrlnbIO.getTranno(), chdrpf.getTranno())
				|| isEQ(covrlnbIO.getValidflag(), "2")) {
			return;
		}
		/*    Verify the Contract Risk Statuz*/
		trcdeNotMatch.setTrue();
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
				|| trcdeMatch.isTrue()); wsaaSub.add(1)){
			if (isEQ(covrlnbIO.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				trcdeMatch.setTrue();
			}
		}
		if (trcdeNotMatch.isTrue()) {
			return;
		}
		Covrpf covrpfOld = new Covrpf(covrlnbIO);
		covrpfOld.setValidflag("2");
		covrpfOld.setCurrto(wsaaToday.toInt());
		if(updateCovrlnbList == null){
			updateCovrlnbList = new LinkedList<>();
		}
		updateCovrlnbList.add(covrpfOld);

		Covrpf covrpfNew = new Covrpf(covrlnbIO);
		covrpfNew.setValidflag("1");
		covrpfNew.setCurrto(0);
		covrpfNew.setTranno(wsaaNewTranno.toInt());
		covrpfNew.setRerateDate(0);
		covrpfNew.setBenBillDate(0);
		covrpfNew.setCoverageDebt(BigDecimal.ZERO);
		if (isEQ(covrpfNew.getRider(), "00")) {
			covrpfNew.setStatcode(t5679rec.setCovRiskStat.toString());
			covrpfNew.setPstatcode(t5679rec.setCovPremStat.toString());
		}
		else {
			covrpfNew.setStatcode(t5679rec.setRidRiskStat.toString());
			covrpfNew.setPstatcode(t5679rec.setRidPremStat.toString());
		}
		if(insertCovrlnbList == null){
			insertCovrlnbList = new LinkedList<>();
		}
		insertCovrlnbList.add(covrpfNew);
	}

    protected void callOverbill9000()
    {
        ovrbll9010();
    }

    protected void ovrbll9010()
    {
		wsaaOverBillFlag.set("Y");
        chdrpf = chdrpfDAO.getchdrRecord(bd5lwdto.getChdrcoy(),bd5lwdto.getChdrnum());
        if (isEQ(chdrpf.getBtdate(), chdrpf.getPtdate())) {
			wsaaOverBillFlag.set("N");
        	return;
		}

        ovrbillrec.overbillRec.set(SPACES);
        ovrbillrec.company.set(bd5lwdto.getChdrcoy());
        ovrbillrec.chdrnum.set(bd5lwdto.getChdrnum());
        ovrbillrec.batchkey.set(batcdorrec.batchkey);
        ovrbillrec.newTranno.set(chdrpf.getTranno()+1);
        ovrbillrec.btdate.set(ZERO);
        ovrbillrec.ptdate.set(ZERO);
        ovrbillrec.language.set(bsscIO.getLanguage());
        ovrbillrec.tranDate.set(wsaaTransactionDate);
        ovrbillrec.tranTime.set(wsaaTransactionTime);
        ovrbillrec.user.set(wsaaUser);
        ovrbillrec.termid.set(wsaaTermid);
        callProgram(Overbill.class, ovrbillrec.overbillRec);
        if (isNE(ovrbillrec.statuz, varcom.oK)) {
            syserrrec.statuz.set(ovrbillrec.statuz);
            syserrrec.params.set(ovrbillrec.overbillRec);
            fatalError600();
        }
    }

    protected void ptrnBatcup3300()
	{
		if (ptrnpf != null && isEQ(ptrnpf.getChdrnum(), bd5lwdto.getChdrnum())) {
			return ;
		}
		ptrnpf = new Ptrnpf();
		ptrnpf.setChdrcoy(bd5lwdto.getChdrcoy());
		ptrnpf.setChdrpfx("CH");
		ptrnpf.setChdrnum(bd5lwdto.getChdrnum());
		ptrnpf.setTranno(wsaaNewTranno.toInt());
		ptrnpf.setTrdt(wsaaTransactionDate.toInt());
		ptrnpf.setTrtm(wsaaTransactionTime.toInt());
		ptrnpf.setPtrneff(bsscIO.getEffectiveDate().toInt());
		ptrnpf.setUserT(wsaaUser);
		ptrnpf.setBatccoy(batcdorrec.company.toString());
		ptrnpf.setBatcbrn(batcdorrec.branch.toString());
		ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
		ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnpf.setBatcbatch(batcdorrec.batch.toString());
		ptrnpf.setDatesub(bsscIO.getEffectiveDate().toInt());
		ptrnBulkInsList.add(ptrnpf);

		batcuprec.batcupRec.set(SPACES);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.batcpfx.set("BA");
		batcuprec.batccoy.set(batcdorrec.company);
		batcuprec.batcbrn.set(batcdorrec.branch);
		batcuprec.batcactyr.set(batcdorrec.actyear);
		batcuprec.batctrcde.set(batcdorrec.trcde);
		batcuprec.batcactmn.set(batcdorrec.actmonth);
		batcuprec.batcbatch.set(batcdorrec.batch);
		batcuprec.function.set("WRITS");
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			syserrrec.statuz.set(batcuprec.statuz);
			fatalError600();
		}
	}

    protected void updatePayr5900() {
        payrpf = payrpfDAO.getpayrRecord(bd5lwdto.getChdrcoy(), bd5lwdto.getChdrnum());
        Payrpf payrOld = new Payrpf(payrpf);
        payrOld.setValidflag("2");
        payrOld.setUniqueNumber(payrpf.getUniqueNumber());
        payrpfUpdateList.add(payrOld);
        Payrpf payrNew = new Payrpf(payrpf);
        payrNew.setValidflag("1");
        payrNew.setTranno(wsaaNewTranno.toInt());
        payrNew.setEffdate(bd5lwdto.getPtdate());
        payrpfInsertList.add(payrNew);
    }

	protected void commit3500() {
		if (!noRecordFound){
			commitControlTotals();
			commitChdrBulkUpdate();
			commitCovrBulkUpdate();
			commitPayrBulkUpdate();
			commitPtrnBulkInsert();
		}
	}

	protected void commitChdrBulkUpdate()
	{
		if (updateChdrpfInvalidList != null && !updateChdrpfInvalidList.isEmpty()) {
			chdrpfDAO.updateInvalidChdrRecords(updateChdrpfInvalidList);
			updateChdrpfInvalidList.clear();
		}
		if (insertChdrpfValidList != null && !insertChdrpfValidList.isEmpty()) {
			chdrpfDAO.insertChdrValidCodeStatusRecord(insertChdrpfValidList);
			insertChdrpfValidList.clear();
		}
	}

	protected void commitCovrBulkUpdate()
	{
		if (updateCovrlnbList != null && !updateCovrlnbList.isEmpty()) {
			covrpfDAO.updateCovrValidFlag(updateCovrlnbList);
			updateCovrlnbList.clear();
		}
		if (insertCovrlnbList != null && !insertCovrlnbList.isEmpty()) {
			covrpfDAO.insertCovrRecord(insertCovrlnbList);
			insertCovrlnbList.clear();
		}
	}

    protected void commitPayrBulkUpdate()
	{
		payrpfDAO.updatePayrRecords(payrpfUpdateList, bsscIO.getEffectiveDate().toInt());
		payrpfUpdateList.clear();
		payrpfDAO.insertPayrpfList(payrpfInsertList);
		payrpfInsertList.clear();
	}

	protected void commitPtrnBulkInsert()
	{
		boolean isInsertPtrnPF = ptrnpfDAO.insertPtrnPF(ptrnBulkInsList);
		if (!isInsertPtrnPF) {
			LOGGER.error("Insert PtrnPF record failed.");
			fatalError600();
		}else {
			ptrnBulkInsList.clear();
		}
	}

	protected void rollback3600() {
		/*ROLLBACK*/
		/**    Place any additional rollback processing in here.*/
		/*EXIT*/
	}

	protected void close4000() {
		lsaaStatuz.set(varcom.oK);
	}

}
