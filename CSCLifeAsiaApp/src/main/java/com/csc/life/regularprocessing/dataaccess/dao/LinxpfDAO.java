package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.B5023DTO;
import com.csc.life.regularprocessing.dataaccess.model.Linxpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface LinxpfDAO extends BaseDAO<Linxpf> {
    public List<Linxpf> searchLinxpfResult(String tableId, String memName, int batchExtractSize, int batchID);
}