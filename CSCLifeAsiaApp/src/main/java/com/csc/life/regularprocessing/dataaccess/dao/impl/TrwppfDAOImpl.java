package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.TrwppfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Trwppf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class TrwppfDAOImpl extends BaseDAOImpl<Trwppf> implements TrwppfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(TrwppfDAOImpl.class);

	@Override
	public void insertTrwppf(List<Trwppf> trwppfList) {
		if (trwppfList != null && trwppfList.size() > 0) {
			String sql = "INSERT INTO TRWPPF(CHDRCOY,CHDRNUM,EFFDATE,TRANNO,VALIDFLAG,RDOCPFX,RDOCCOY,RDOCNUM,ORIGCURR,ORIGAMT,USRPRF,JOBNM,DATIME)VALUES"
					+ "(?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement ps = getPrepareStatement(sql);
			try {
				for (Trwppf c : trwppfList) {
					ps.setString(1, c.getChdrcoy());
					ps.setString(2, c.getChdrnum());
					ps.setInt(3, c.getEffdate());
					ps.setInt(4, c.getTranno());
					ps.setString(5, c.getValidflag());
					ps.setString(6, c.getRdocpfx());
					ps.setString(7, c.getRdoccoy());
					ps.setString(8, c.getRdocnum());
					ps.setString(9, c.getOrigcurr());
					ps.setBigDecimal(10, c.getOrigamt());
					ps.setString(11, c.getUserProfile());
					ps.setString(12, c.getJobName());
					ps.setTimestamp(13,new Timestamp(System.currentTimeMillis()));
					ps.addBatch();
				}
				ps.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("insertTrwppf()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);
			}
			trwppfList.clear();
		}
	}

}
