/*
 * File: B5352.java
 * Date: May 25, 2011 6:57:09 AM IST
 * Author: Quipoz Limited
 * 
 * Class transformed from B5352.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.time.YearMonth;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.regularprocessing.dataaccess.dao.B5353DAO;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* This program is the Collections Splitter program to be run
* before Collections (B5353) in multi-thread.
*
* The LINSPF will be read via SQL with the following criteria:-
* i   Payflag  not = 'P'
* ii  Billchnl not = 'N'
* iii Billcd   <=  <run date>
* iv  chdrcoy   =  <run company>
* v   branch    =  <run branch>
* vi  validflag =  '1'
* vii chdrnum   BETWEEN  <P6671-chdrnum>
*               AND      <P6671-chdrnum-1>
*     order by chdrcoy, chdrnum
*
* Control totals used in this program:
*
*    01  -  No. of LINS extracted records
*    02  -  No. of thread members
*
* Splitter programs should be simple. They should only deal with
* a single file. If there is insufficient data on the primary file
* to completely identify a transaction then the further editing
* should be done in the subsequent process. The objective of a
* Splitter is to rapidly isolate potential transactions, not
* to process the transaction.   The primary concern for Splitter
* program design is to minimise elapsed time and this is achieved
* by minimising disk IO.
*
* Before the Splitter process is invoked, the program CRTTMPF
* should be run.  CRTTMPF (Create Temporary File) will create
* a duplicate of a physical file (created under Smart and defined
* as a Query file) which will have as many members as are defined
* in that process's 'subsequent threads' field. The temporary file
* will have the name XXXXDD9999 where XXXX is the first four
* characters of the file which was duplicated, DD is the first two
* characters of the 4th system parameter and 9999 is the schedule
* run number. Each member added to the temporary file will be
* called THREAD999 where 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read records from a primary file
* and, for each record selected,  it will write a record to a
* temporary file member. Exactly which temporary file member
* is written to is decided by a working storage count. The
* objective of the count is to spread the transaction records
* evenly amongst each thread member.
*
* The Splitter program should always have a restart method of
* '1' indicating a re-run. In the event of a restart, the Splitter
* program will always start with empty file members. Apart from
* writing records to the temporary file members, Splitter programs
* should not be doing any further updates.
*
*
* Notes for programs which use Splitter Program Temporary Files.
* --------------------------------------------------------------
*
* The Multi Thread Batch Environment will be able to submit multip e
* versions of the program which use the file members created from
* this program. It is important that the process which runs the
* splitter program has the same 'number of subsequent threads' as
* the following process has defined in 'number of threads this
* process'.
*
* The subsequent program need only point itself to one member
* based around the field BSPR-SCHEDULE-THREAD-NO which indicates
* in which thread the copy of the subsequent program is running.
*
*****************************************************************
* </pre>
*/
public class B5352 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5352");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

		/*  LINX member parametres.*/
	private FixedLengthStringData wsaaLinxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaLinxFn, 0, FILLER).init("LINX");
	private FixedLengthStringData wsaaLinxRunid = new FixedLengthStringData(2).isAPartOf(wsaaLinxFn, 4);
	private ZonedDecimalData wsaaLinxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaLinxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaa1 = new FixedLengthStringData(1).init("1");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");
	private FixedLengthStringData wsaaClrtmpfError = new FixedLengthStringData(97);
		/*  Pointers to point to the member to read (IY) and write (IZ).*/

		/*  SQL error message formatting.*/
	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
		/*  Define the number of rows in the block to be fetched.*/

		/* WSAA-HOST-VARIABLES */
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).init(0);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private P6671par p6671par = new P6671par();
	
	private B5353DAO b5353DAO =  getApplicationContext().getBean("b5353DAO", B5353DAO.class);	
	private LinspfDAO linspfDAO = getApplicationContext().getBean("linspfDAO", LinspfDAO.class);
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Boolean japanBilling = false;
	private static final String BTPRO027 = "BTPRO027";
	private ZonedDecimalData monthLastDate = new ZonedDecimalData(8, 0);
	public B5352() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** This section is invoked in restart mode. Note section 1100 must*/
		/** clear each temporary file member as the members are not under*/
		/** commitment control. Note also that Splitter programs must have*/
		/** a Restart Method of 1 (re-run). Thus no updating should occur*/
		/** which would result in different records being returned from the*/
		/** primary file in a restart!*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/*    Check the restart method is compatible with the program.*/
		/*    This program is restartable but will always re-run from*/
		/*    the beginning. This is because the temporary file members*/
		/*    are not under commitment control.*/
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*    Do a ovrdbf for each temporary file member. (Note we have*/
		/*    allowed for a maximum of 20.)*/
//		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
//			bprdIO.setThreadsSubsqntProc(20);
//		}
		wsaaLinxRunid.set(bprdIO.getSystemParam04());
		wsaaLinxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		/*    Since this program runs from a parameter screen move the*/
		/*    internal parameter area to the original parameter copybook*/
		/*    to retrieve the contract range*/
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES)
		&& isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		openThreadMember1100();
		
//		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
//			openThreadMember1100();
//		}
		/*    Log the number of threads being used*/
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		/*    Prepare the host effective date from the parameter screen*/
		japanBilling = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), BTPRO027, appVars, "IT");
		if(japanBilling){
			monthLastDate.set(getMonthEnd(bsscIO.getEffectiveDate().toString()));
			datcon2rec.freqFactor.set(1);
			datcon2rec.frequency.set("01");
			datcon2rec.intDate1.set(monthLastDate);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				fatalError600();
			}
			wsaaEffdate.set(datcon2rec.intDate2);
			datcon2rec.freqFactor.set(1);
			datcon2rec.frequency.set("12");
			datcon2rec.intDate1.set(wsaaEffdate);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				fatalError600();
			}
			wsaaEffdate.set(datcon2rec.intDate2);
		}
		else{
		wsaaEffdate.set(bsscIO.getEffectiveDate());
		}
		wsaaCompany.set(bsprIO.getCompany());

		/* Where parameter 3 on the schedule definition is not          */
		/* spaces, clear the ACAG file.                                 */
		if (isNE(bprdIO.getSystemParam03(), SPACES)) {
			clearAcag1200();
		}
	}

protected String getMonthEnd(String date) {
	int lengthMonth = YearMonth
			.of(Integer.valueOf(date.substring(0, 4).trim()), Integer.valueOf(date.substring(4, 6).trim()))
			.lengthOfMonth();
	return date.substring(0, 6).concat(String.format("%d", lengthMonth));
}

protected void openThreadMember1100()
	{
		/*    The next thing we must do is construct the name of the*/
		/*    temporary file which we will be working with.*/
		/* MOVE IZ                     TO WSAA-THREAD-NUMBER.           */
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(), 1), 1));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaLinxFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz, varcom.oK)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression("CLRTMPF", SPACES);
			stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
			stringVariable1.addExpression(SPACES);
			stringVariable1.addExpression(wsaaLinxFn);
			stringVariable1.addExpression(SPACES);
			stringVariable1.addExpression(wsaaThreadMember);
			stringVariable1.setStringInto(wsaaClrtmpfError);
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.params.set(wsaaClrtmpfError);
			fatalError600();
		}
	}

protected void clearAcag1200()
	{
//		/*START*/
//		/* Check for multi-thread processing. If it is, clear down      */
//		/* the ACAGPF file.                                             */
//		if (isNE(bprdIO.getSystemParam03(), SPACES)) {
//			fileprcrec.function.set("CLRF");
//			fileprcrec.file1.set("ACAGPF");
//			noSource("FILEPROC", fileprcrec.params);
//			if (isNE(fileprcrec.statuz, varcom.oK)) {
//				syserrrec.statuz.set(fileprcrec.statuz);
//				syserrrec.params.set(fileprcrec.params);
//				fatalError600();
//			}
//		}
//		/*EXIT*/
		/*START*/
		/* Check for multi-thread processing. If it is, clear down      */
		/* the ACAGPF file.                                             */
		if (isNE(bprdIO.getSystemParam03(), SPACES)) {
			wsaaQcmdexc.set(SPACES);
			wsaaQcmdexc.set("CLRPFM FILE(ACAGPF)");
			com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
			/**        MOVE 'CLRF'           TO FILEPR-FUNCTION         <LTW001>*/
			/**        MOVE 'ACAGPF'         TO FILEPR-FILE1            <LTW001>*/
			/**        CALL 'FILEPROC'         USING FILEPR-PARAMS      <LTW001>*/
			/**        IF FILEPR-STATUZ   NOT = O-K                     <LTW001>*/
			/**            MOVE FILEPR-STATUZ TO SYSR-STATUZ            <LTW001>*/
			/**            MOVE FILEPR-PARAMS TO SYSR-PARAMS            <LTW001>*/
			/**            PERFORM 600-FATAL-ERROR                      <LTW001>*/
			/**        END-IF                                           <LTW001>*/
		}
		/*EXIT*/
	}

protected void readFile2000()
	{
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
		}
	}

protected void readFiles2010()
	{
		/*  We only need to FETCH records when a full block has been*/
		/*  processed or if the end of file was retrieved in the last*/
		/*  block.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
	}

protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		StringBuilder acaxTempTableName = new StringBuilder("");
		acaxTempTableName.append("LINX");
		acaxTempTableName.append(bprdIO.getSystemParam04().toString().trim().toUpperCase());
		String scheduleNumber = bsscIO.getScheduleNumber().toString();
		scheduleNumber = scheduleNumber.substring(scheduleNumber.length()-4);
		acaxTempTableName.append(String.format("%04d", Integer.parseInt(scheduleNumber.trim())));
	
		int noOfSubThreads = bprdIO.getThreadsSubsqntProc().toInt();
		int noOfCopiedRec = linspfDAO.copyDataToTempTable(appVars.getTableNameOverriden("LINSPF"), acaxTempTableName.toString(), noOfSubThreads,
				 wsaaEffdate.toInt(), wsaaCompany.toString(), wsaaChdrnumFrom.toString(), wsaaChdrnumTo.toString());
		contotrec.totval.set(noOfCopiedRec);
		contotrec.totno.set(ct02);
		callContot001();
		wsaaEofInBlock.set("Y");
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/**    There is no pre-commit processing to be done*/
		/*EXIT*/
		b5353DAO.initializeB5353Temp();
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close the open files and remove the overrides.*/
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
