/*
 * File: Revrevac.java
 * Date: 30 August 2009 2:11:21
 * Author: Quipoz Limited
 * 
 * Class transformed from REVREVAC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.regularprocessing.dataaccess.LinsrnlTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*  REVREVAC - Reversal Revenue Accounting.
*  ---------------------------------------
*
*  This subroutine is called by the Full Contract Reversal program
*  P5155 via Table T6661. The parameter passed to this subroutine
*  is contained in the REVERSEREC copybook.
*
*  This subroutine is designed to reverse out the changes that
*  B6275 performs.
*
*  REVERSE LIFE INSTALMENT RECORDS
*
*       The flag on the LINS record for Due ie. Due Flag, is
*       reset back to spaces. This is so they can be selected
*       again.
*
*  CONTRACT REVERSAL ACCOUNTING
*
*       Do a BEGNH on the ACMV file for this transaction no. and
*       call LIFACMV to post the reversal to the sub-account.
*       The Premium postings are Multiplied by -1 to restore
*       these back to pre-update values.
*
*****************************************************************
* </pre>
*/
public class Revrevac extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVREVAC";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String linsrnlrec = "LINSRNLREC";
	private static final String acmvrevrec = "ACMVREVREC";
	private static final String arcmrec = "ARCMREC";
	protected String endOfFile = "N";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	protected LinsrnlTableDAM linsrnlIO = new LinsrnlTableDAM();
	protected TaxdrevTableDAM taxdrevIO = new TaxdrevTableDAM();
	private Itemkey wsaaItemkey = new Itemkey();
	protected Varcom varcom = new Varcom();
	protected Syserrrec syserrrec = new Syserrrec();
	private Batckey wsaaBatckey = new Batckey();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	protected Reverserec reverserec = new Reverserec();

/**
 * Contains all possible labels used by goTo action.
 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readLins1205, 
		ledgerAccountingUpdates1300, 
		nextAcmv7550
	}

	public Revrevac() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		main110();
		exit190();
	}

protected void main110()
	{
		initialise200();
		while ( !(isEQ(endOfFile, "Y"))) {
			mainProcessing1000();
		}
		
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialise200()
	{
		/*INITIALISE*/
		endOfFile = "N";
		wsaaBatckey.set(reverserec.batchkey);
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		/*EXIT*/
	}

protected void mainProcessing1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					processLinsRecord1200();
				case readLins1205: 
					readLins1205();
				case ledgerAccountingUpdates1300: 
					ledgerAccountingUpdates1300();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void processLinsRecord1200()
	{
		/* Reverse the Dueflag value on the LINS record so that it*/
		/* can be reprocessed by B6275 if necessary.*/
		/* Read and hold LINSRNL.*/
		linsrnlIO.setParams(SPACES);
		linsrnlIO.setChdrnum(reverserec.chdrnum);
		linsrnlIO.setChdrcoy(reverserec.company);
		linsrnlIO.setInstfrom(reverserec.effdate1);
		linsrnlIO.setFormat(linsrnlrec);
		/* MOVE BEGNH                  TO LINSRNL-FUNCTION.             */
		linsrnlIO.setFunction(varcom.begn);
	}

protected void readLins1205()
	{//performance improvement --  atiwari23 
	linsrnlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	linsrnlIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
	
		SmartFileCode.execute(appVars, linsrnlIO);
		if ((isNE(linsrnlIO.getStatuz(), varcom.oK))
		&& (isNE(linsrnlIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(linsrnlIO.getParams());
			syserrrec.statuz.set(linsrnlIO.getStatuz());
			dbError8100();
		}
		if ((isNE(reverserec.chdrnum, linsrnlIO.getChdrnum()))
		|| (isNE(reverserec.company, linsrnlIO.getChdrcoy()))) {
			linsrnlIO.setStatuz(varcom.endp);
		}
		if (isEQ(linsrnlIO.getStatuz(), varcom.endp)) {
			endOfFile = "Y";
			goTo(GotoLabel.ledgerAccountingUpdates1300);
		}
		if (isEQ(linsrnlIO.getDueflg(), " ")) {
			linsrnlIO.setFunction(varcom.nextr);
			goTo(GotoLabel.readLins1205);
		}
		if (isNE(linsrnlIO.getInstfrom(), reverserec.effdate1)) {
			linsrnlIO.setFunction(varcom.nextr);
			goTo(GotoLabel.readLins1205);
		}
		linsrnlIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, linsrnlIO);
		if (isNE(linsrnlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(linsrnlIO.getParams());
			syserrrec.statuz.set(linsrnlIO.getStatuz());
			dbError8100();
		}
		/* Reverse LINSRNL DUEFLAG set to ' '.*/
		linsrnlIO.setInstfrom(reverserec.effdate1);
		linsrnlIO.setDueflg(" ");
		linsrnlIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, linsrnlIO);
		if ((isNE(linsrnlIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(linsrnlIO.getParams());
			syserrrec.statuz.set(linsrnlIO.getStatuz());
			dbError8100();
		}
		/* Updated LINS so set END-OF-FILE flag.                           */
		endOfFile = "Y";
		taxdrevIO.setParams(SPACES);
		taxdrevIO.setChdrcoy(reverserec.company);
		taxdrevIO.setChdrnum(reverserec.chdrnum);
		taxdrevIO.setEffdate(reverserec.effdate1);
		taxdrevIO.setFunction(varcom.begn);
		taxdrevIO.setStatuz(varcom.oK);
		while ( !(isEQ(taxdrevIO.getStatuz(), varcom.endp))) {
			reverseTax1000a();
		}
		
	}

protected void ledgerAccountingUpdates1300()
	{
		/* Reverse the instalment from "Premiums Due" INSTAMT06.*/
		readChdrlif2000();
		updatePremiumsDue3000();
		processAcmvOptical4000();
		/*EXIT*/
	}

protected void reverseTax1000a()
	{
		start1000a();
	}

protected void start1000a()
	{
		SmartFileCode.execute(appVars, taxdrevIO);
		if (isNE(taxdrevIO.getStatuz(), varcom.oK)
		&& isNE(taxdrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(taxdrevIO.getParams());
			syserrrec.statuz.set(taxdrevIO.getStatuz());
			dbError8100();
		}
		if (isNE(reverserec.company, taxdrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, taxdrevIO.getChdrnum())
		|| isNE(reverserec.effdate1, taxdrevIO.getEffdate())) {
			taxdrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(linsrnlIO.getInstfrom(), taxdrevIO.getInstfrom())
		&& isEQ(linsrnlIO.getInstto(), taxdrevIO.getInstto())
		&& isEQ(taxdrevIO.getPostflg(), "P")) {
			taxdrevIO.setPostflg(SPACES);
			taxdrevIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, taxdrevIO);
			if (isNE(taxdrevIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(taxdrevIO.getParams());
				syserrrec.statuz.set(taxdrevIO.getStatuz());
				dbError8100();
			}
		}
		taxdrevIO.setFunction(varcom.nextr);
	}

protected void readChdrlif2000()
	{
		/*PARA*/
		/* Read and hold contract header record using CHDRLIF.*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		/*  MOVE READH                  TO CHDRLIF-FUNCTION.             */
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			syserrrec.params.set(chdrlifIO.getParams());
			dbError8100();
		}
		/*EXIT*/
	}

protected void updatePremiumsDue3000()
	{
		premDue3100();
	}

protected void premDue3100()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setBatctrcde(SPACES);
		//performance improvement --  atiwari23 
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	
		while ( !((isEQ(acmvrevIO.getStatuz(),varcom.endp))
		|| (isEQ(acmvrevIO.getBatctrcde(),reverserec.oldBatctrcde)))) {
			
			
			SmartFileCode.execute(appVars, acmvrevIO);
			if ((isNE(acmvrevIO.getStatuz(), varcom.oK))
			&& (isNE(acmvrevIO.getStatuz(), varcom.endp))) {
				syserrrec.params.set(acmvrevIO.getParams());
				dbError8100();
			}
			if ((isNE(acmvrevIO.getRldgcoy(), reverserec.company))
			|| (isNE(acmvrevIO.getRdocnum(), reverserec.chdrnum))
			|| (isNE(acmvrevIO.getTranno(), reverserec.tranno))) {
				acmvrevIO.setStatuz(varcom.endp);
			}
			acmvrevIO.setFunction(varcom.nextr);
		}
		
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvRecs7500();
		}
		
	}

protected void processAcmvOptical4000()
	{
		start4010();
	}

protected void start4010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			dbError8100();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			dbError8100();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvRecs7500();
		}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				dbError8100();
			}
		}
	}

protected void reverseAcmvRecs7500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readAcmv7510();
				case nextAcmv7550: 
					nextAcmv7550();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readAcmv7510()
	{
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batccoy.set(reverserec.company);
		lifacmvrec.rldgcoy.set(reverserec.company);
		lifacmvrec.genlcoy.set(reverserec.company);
		lifacmvrec.batcactyr.set(reverserec.batcactyr);
		lifacmvrec.batctrcde.set(reverserec.batctrcde);
		lifacmvrec.batcactmn.set(reverserec.batcactmn);
		lifacmvrec.batcbatch.set(reverserec.batcbatch);
		lifacmvrec.batcbrn.set(reverserec.batcbrn);
		lifacmvrec.rdocnum.set(reverserec.chdrnum);
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), -1));
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), -1));
		/* MULTIPLY ACMVREV-RCAMT   BY -1 GIVING LIFA-RCAMT.            */
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(reverserec.company);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		descIO.setDescitem(acmvrevIO.getTranref());
		getDescription7600();
		/* MOVE REVE-EFFDATE-1         TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		/* MOVE ACMVREV-FRCDATE        TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.user.set(acmvrevIO.getUser());
		lifacmvrec.transactionTime.set(acmvrevIO.getTransactionDate());
		lifacmvrec.transactionDate.set(acmvrevIO.getTransactionTime());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			dbError8100();
		}
	}

protected void nextAcmv7550()
	{
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'         USING ACMVREV-PARAMS.               */
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if ((isNE(acmvrevIO.getStatuz(), varcom.oK))
		&& (isNE(acmvrevIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(acmvrevIO.getStatuz());
			dbError8100();
		}
		if ((isEQ(acmvrevIO.getStatuz(), varcom.endp))
		|| (isNE(acmvrevIO.getRldgcoy(), reverserec.company))
		|| (isNE(acmvrevIO.getRdocnum(), reverserec.chdrnum))
		|| (isNE(acmvrevIO.getTranno(), reverserec.tranno))) {
			acmvrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			goTo(GotoLabel.nextAcmv7550);
		}
	}

protected void getDescription7600()
	{
		para7610();
	}

protected void para7610()
	{
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set("IT");
		wsaaItemkey.itemItemcoy.set(reverserec.company);
		wsaaItemkey.itemItemtabl.set(t1688);
		wsaaItemkey.itemItemitem.set(reverserec.batctrcde);
		descIO.setDataKey(wsaaItemkey);
		descIO.setLanguage(reverserec.language);
		descIO.setFunction("READR");
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			lifacmvrec.trandesc.set(SPACES);
		}
		lifacmvrec.trandesc.set(descIO.getLongdesc());
	}

protected void systemError8000()
	{
		se8000();
		seExit8090();
	}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		reverserec.statuz.set(varcom.bomb);
		exit190();
	}

protected void dbError8100()
	{
		db8100();
		dbExit8190();
	}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		reverserec.statuz.set(varcom.bomb);
		exit190();
	}
}
