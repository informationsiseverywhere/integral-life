package com.csc.life.regularprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AinrpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:51
 * Class transformed from AINRPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AinrpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 140;
	public FixedLengthStringData ainrrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData ainrpfRecord = ainrrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(ainrrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(ainrrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(ainrrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(ainrrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(ainrrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(ainrrec);
	public PackedDecimalData rd01Oldsum = DD.oldsum.copy().isAPartOf(ainrrec);
	public PackedDecimalData newsumi = DD.newsumi.copy().isAPartOf(ainrrec);
	public PackedDecimalData oldinst = DD.oldinst.copy().isAPartOf(ainrrec);
	public PackedDecimalData newinst = DD.newinst.copy().isAPartOf(ainrrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(ainrrec);
	public PackedDecimalData cmdate = DD.cmdate.copy().isAPartOf(ainrrec);
	public FixedLengthStringData rasnum = DD.rasnum.copy().isAPartOf(ainrrec);
	public FixedLengthStringData rngmnt = DD.rngmnt.copy().isAPartOf(ainrrec);
	public PackedDecimalData raAmount = DD.raamount.copy().isAPartOf(ainrrec);
	public PackedDecimalData riskCessDate = DD.rcesdte.copy().isAPartOf(ainrrec);
	public FixedLengthStringData currency = DD.currency.copy().isAPartOf(ainrrec);
	public FixedLengthStringData aintype = DD.aintype.copy().isAPartOf(ainrrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(ainrrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(ainrrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(ainrrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AinrpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AinrpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AinrpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AinrpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AinrpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AinrpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AinrpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("AINRPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"CRTABLE, " +
							"OLDSUM, " +
							"NEWSUMI, " +
							"OLDINST, " +
							"NEWINST, " +
							"PLNSFX, " +
							"CMDATE, " +
							"RASNUM, " +
							"RNGMNT, " +
							"RAAMOUNT, " +
							"RCESDTE, " +
							"CURRENCY, " +
							"AINTYPE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     crtable,
                                     rd01Oldsum,
                                     newsumi,
                                     oldinst,
                                     newinst,
                                     planSuffix,
                                     cmdate,
                                     rasnum,
                                     rngmnt,
                                     raAmount,
                                     riskCessDate,
                                     currency,
                                     aintype,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		crtable.clear();
  		rd01Oldsum.clear();
  		newsumi.clear();
  		oldinst.clear();
  		newinst.clear();
  		planSuffix.clear();
  		cmdate.clear();
  		rasnum.clear();
  		rngmnt.clear();
  		raAmount.clear();
  		riskCessDate.clear();
  		currency.clear();
  		aintype.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAinrrec() {
  		return ainrrec;
	}

	public FixedLengthStringData getAinrpfRecord() {
  		return ainrpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAinrrec(what);
	}

	public void setAinrrec(Object what) {
  		this.ainrrec.set(what);
	}

	public void setAinrpfRecord(Object what) {
  		this.ainrpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(ainrrec.getLength());
		result.set(ainrrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}