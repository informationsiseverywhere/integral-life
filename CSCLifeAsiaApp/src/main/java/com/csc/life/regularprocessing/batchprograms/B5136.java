/*
 * File: B5136.java
 * Date: 29 August 2009 20:58:28
 * Author: Quipoz Limited
 * 
 * Class transformed from B5136.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.life.regularprocessing.dataaccess.InczpfTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* COMPILE-OPTIONS-SQL   CSRSQLCSR(*ENDJOB)        <Do not delete>
*
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*           AUTOMATIC INCREASE ACTUAL SPLITTER PROGRAM
*           ------------------------------------------
*
* This Splitter runs directly before B5137, which is the multi-
* thread program that processes Actual Automatic Increase records.
*
* A temporary file INCZPF, based upon the physical file INCRPF,
* is created and INCZPF records are divided up into the
* temporary file members. This allows B5137 to retrieve the
* records in multi-thread mode for validation and processing.
*
* The records are selected from INCRPF and LIFEPF as follows:
*
*      INCR.validflag    =  '1'
*      LIFE.validflag    =  '1'
*      INCR.chdrnum      =  LIFE.chdrnum
*      INCR.chdrnum      between  <chdrnumfrm>
*                        and  <chdrnumto>
*      INCR.chdrcoy      =  <run-company>
*      INCR.CRRCD        <> 0
*      INCR.CRRCD        <= WSAA-EFFDATE
*      order by LIFE.lifcnum, INCR.chdrcoy, INCR.chdrnum,
*               INCR.life, INCR.coverage, INCR.rider,
*               INCR.plnsfx
*
* Note that the purpose of ordering by contract life  is to
* prevent record locking when writing LETCPF letter records
* within the processing program.
*
* Control totals used in this program:
*
*    01  -  No. of thread members
*    02  -  No. of extracted records
*
*------------------------------------------------------------------------
* Splitter programs should be simple. They should only deal
* with a single file. If there is insufficient data on the
* primary file to completely identify a transaction then
* further editing should be done in the subsequent process.
* The objective of a splitter is to rapidly isolate potential
* transactions, not process the transaction. The primary concern
* for splitter program design is elapsed time speed and this is
* achieved by minimising disk IO.
*
* Before the Splitter process is invoked the program CRTTMPF
* should be run. The CRTTMPF (Create Temporary File) will create
* a duplicate of a physical file (created under Smart and defined
* as a Query file) which will have as many members as are defined
* in that process's subsequent threads field. The temporary file
* will have the name XXXXDD9999 where XXXX is the first four
* characters of the file which was duplicated, DD is the first
* two characters of the 4th system parameter and 9999 is the
* schedule run number. Each member added to the temporary file
* will be called THREAD999 where 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read records from a primary file
* and for each record it selects it will write a record to a
* temporary file member. Exactly which temporary file member
* is written to is decided by a working storage count. The
* objective of the count is spread the transaction records
* evenly across the thread members.
*
* The Splitter program should always have a restart method of
* '1' indicating a re-run. In the event of a restart the Splitter
* program will always start with empty file members. Apart from
* writing records to the temporary file members Splitter programs
* should not be doing any further updates.
*
* There should be no non-standard CL commands in the CL Pgm which
* calls this program.
*
* Notes for programs which use Splitter Program Temporary Files.
* --------------------------------------------------------------
*
* The MTBE will be able to submit multiple versions of the program
* which use the file members created from this program. It is
* important that the process which runs the splitter program has
* the same 'number of subsequent threads' as the following
* process has defined in 'number of threads this process'.
*
* The subsequent program need only point itself to one member
* based around the field BSPR-SCHEDULE-THREAD-NO which indicates
* in which thread the copy of the subsequent program is running.
*
***********************************************************************
* </pre>
*/
public class B5136 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlincrpfCursorrs = null;
	private java.sql.PreparedStatement sqlincrpfCursorps = null;
	private java.sql.Connection sqlincrpfCursorconn = null;
	private String sqlincrpfCursor = "";
	private int incrpfCursorLoopIndex = 0;
	private DiskFileDAM incz01 = new DiskFileDAM("INCZ01");
	private DiskFileDAM incz02 = new DiskFileDAM("INCZ02");
	private DiskFileDAM incz03 = new DiskFileDAM("INCZ03");
	private DiskFileDAM incz04 = new DiskFileDAM("INCZ04");
	private DiskFileDAM incz05 = new DiskFileDAM("INCZ05");
	private DiskFileDAM incz06 = new DiskFileDAM("INCZ06");
	private DiskFileDAM incz07 = new DiskFileDAM("INCZ07");
	private DiskFileDAM incz08 = new DiskFileDAM("INCZ08");
	private DiskFileDAM incz09 = new DiskFileDAM("INCZ09");
	private DiskFileDAM incz10 = new DiskFileDAM("INCZ10");
	private DiskFileDAM incz11 = new DiskFileDAM("INCZ11");
	private DiskFileDAM incz12 = new DiskFileDAM("INCZ12");
	private DiskFileDAM incz13 = new DiskFileDAM("INCZ13");
	private DiskFileDAM incz14 = new DiskFileDAM("INCZ14");
	private DiskFileDAM incz15 = new DiskFileDAM("INCZ15");
	private DiskFileDAM incz16 = new DiskFileDAM("INCZ16");
	private DiskFileDAM incz17 = new DiskFileDAM("INCZ17");
	private DiskFileDAM incz18 = new DiskFileDAM("INCZ18");
	private DiskFileDAM incz19 = new DiskFileDAM("INCZ19");
	private DiskFileDAM incz20 = new DiskFileDAM("INCZ20");
	private InczpfTableDAM inczpfData = new InczpfTableDAM();
		/* Change the record length to that of the temporary file.*/
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5136");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

		/* INCZ member parameters.                                      */
	private FixedLengthStringData wsaaInczFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaInczFn, 0, FILLER).init("INCZ");
	private FixedLengthStringData wsaaInczRunid = new FixedLengthStringData(2).isAPartOf(wsaaInczFn, 4);
	private ZonedDecimalData wsaaInczJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaInczFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaThreadMember, 9, FILLER).init(SPACES);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
		/*01  INCZPF-DATA.                                         <S19FIX>
		    COPY DDS-ALL-FORMATS OF INCZPF.                      <S19FIX>
		 Pointers to point to the member to read (IY) and write (IZ).*/
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
		/* Define the number of rows in the block to be fetched.*/
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);

		/* WSAA-IND-ARRAY */
	private FixedLengthStringData[] wsaaIncrInd = FLSInittedArray (1000, 8);
	private BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(4, 4, 0, wsaaIncrInd, 0);
		/* WSAA-HOST-VARIABLES */
	private FixedLengthStringData wsaaChdrnumfrm = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumto = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrChdrnum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).init(0);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaPrevCownnum = new FixedLengthStringData(8).init(SPACES);
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
	private P6671par p6671par = new P6671par();
	private WsaaFetchArrayInner wsaaFetchArrayInner = new WsaaFetchArrayInner();

	private int ct02Value = 0;
	
	public B5136() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		wsaaChdrnumfrm.set(p6671par.chdrnum);
		wsaaChdrnumto.set(p6671par.chdrnum1);
		if (isEQ(p6671par.chdrnum,SPACES)) {
			wsaaChdrnumfrm.set(LOVALUE);
		}
		if (isEQ(p6671par.chdrnum1,SPACES)) {
			wsaaChdrnumto.set(HIVALUE);
		}
		/* The next thing we must do is construct the name of the*/
		/* temporary file which we will be working with.*/
		wsaaInczRunid.set(bprdIO.getSystemParam04());
		wsaaInczJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		wsaaCompany.set(bsprIO.getCompany());
		wsaaEffdate.set(bsscIO.getEffectiveDate());
		sqlincrpfCursor = " SELECT  T02.LIFCNUM, T01.CHDRCOY, T01.CHDRNUM, T01.LIFE, T01.JLIFE, T01.COVERAGE, T01.RIDER, T01.PLNSFX, T01.CRRCD, T01.CRTABLE, T01.VALIDFLAG, T01.STATCODE, T01.PSTATCODE, T01.ANNVRY, T01.BASCMETH, T01.BASCPY, T01.RNWCPY, T01.SRVCPY, T01.NEWINST, T01.LASTINST, T01.NEWSUM, T01.ZBNEWINST, T01.ZBLASTINST, T01.ZLNEWINST, T01.ZLLASTINST" +
" FROM   " + getAppVars().getTableNameOverriden("INCRPF") + "  T01,  " + getAppVars().getTableNameOverriden("LIFEPF") + "  T02" +
" WHERE T01.CHDRCOY = ?" +
" AND T01.CRRCD <> 0" +
" AND T01.CRRCD <= ?" +
" AND T01.CHDRNUM = T02.CHDRNUM" +
" AND T01.CHDRNUM BETWEEN ? AND ?" +
" AND T01.VALIDFLAG = '1'" +
" AND T02.VALIDFLAG = '1'" +
" AND T02.LIFE = '01'" +
" AND T02.JLIFE = '00'" +
" ORDER BY T02.LIFCNUM, T01.CHDRCOY, T01.CHDRNUM, T01.LIFE, T01.COVERAGE, T01.RIDER, T01.PLNSFX";
		iy.set(1);
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1200();
		}
		wsaaInd.set(1);
		sqlerrorflag = false;
		try {
			sqlincrpfCursorconn = getAppVars().getDBConnectionForTable(new com.csc.smart400framework.dataaccess.SmartFileCode[] {new com.csc.life.regularprocessing.dataaccess.IncrpfTableDAM(), new com.csc.life.newbusiness.dataaccess.LifepfTableDAM()});
			sqlincrpfCursorps = getAppVars().prepareStatementEmbeded(sqlincrpfCursorconn, sqlincrpfCursor);
			getAppVars().setDBString(sqlincrpfCursorps, 1, wsaaCompany);
			getAppVars().setDBNumber(sqlincrpfCursorps, 2, wsaaEffdate);
			getAppVars().setDBString(sqlincrpfCursorps, 3, wsaaChdrnumfrm);
			getAppVars().setDBString(sqlincrpfCursorps, 4, wsaaChdrnumto);
			sqlincrpfCursorrs = getAppVars().executeQuery(sqlincrpfCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void openThreadMember1100()
	{
        compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(),1),iz));
        callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaInczFn, wsaaThreadMember, wsaaStatuz);
        if (isNE(wsaaStatuz,varcom.oK)) {
            syserrrec.statuz.set(wsaaStatuz);
            syserrrec.syserrType.set("2");
            syserrrec.subrname.set("CLRTMPF");
            fatalError600();
        }
        StringUtil stringVariable1 = new StringUtil();
        stringVariable1.addExpression("OVRDBF FILE(INCZ");
        stringVariable1.addExpression(iz);
        stringVariable1.addExpression(") TOFILE(");
        stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
        stringVariable1.addExpression("/");
        stringVariable1.addExpression(wsaaInczFn);
        stringVariable1.addExpression(") MBR(");
        stringVariable1.addExpression(wsaaThreadMember);
        stringVariable1.addExpression(")");
        stringVariable1.addExpression(" SEQONLY(*YES 1000)");
        stringVariable1.setStringInto(wsaaQcmdexc);
        com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
        /* Open the file.*/
        if (isEQ(iz,1)) {
            incz01.openOutput();
        }
        if (isEQ(iz,2)) {
            incz02.openOutput();
        }
        if (isEQ(iz,3)) {
            incz03.openOutput();
        }
        if (isEQ(iz,4)) {
            incz04.openOutput();
        }
        if (isEQ(iz,5)) {
            incz05.openOutput();
        }
        if (isEQ(iz,6)) {
            incz06.openOutput();
        }
        if (isEQ(iz,7)) {
            incz07.openOutput();
        }
        if (isEQ(iz,8)) {
            incz08.openOutput();
        }
        if (isEQ(iz,9)) {
            incz09.openOutput();
        }
        if (isEQ(iz,10)) {
            incz10.openOutput();
        }
        if (isEQ(iz,11)) {
            incz11.openOutput();
        }
        if (isEQ(iz,12)) {
            incz12.openOutput();
        }
        if (isEQ(iz,13)) {
            incz13.openOutput();
        }
        if (isEQ(iz,14)) {
            incz14.openOutput();
        }
        if (isEQ(iz,15)) {
            incz15.openOutput();
        }
        if (isEQ(iz,16)) {
            incz16.openOutput();
        }
        if (isEQ(iz,17)) {
            incz17.openOutput();
        }
        if (isEQ(iz,18)) {
            incz18.openOutput();
        }
        if (isEQ(iz,19)) {
            incz19.openOutput();
        }
        if (isEQ(iz,20)) {
            incz20.openOutput();
        }

	}

protected void initialiseArray1200()
	{
		/*START*/
		wsaaFetchArrayInner.wsaaCownnum[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaChdrcoy[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaLife[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaJlife[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaCoverage[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaRider[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaCrtable[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaValidflag[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaStatcode[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaPstatcode[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaAnnvry[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaBascmeth[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaBascpy[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaRnwcpy[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaSrvcpy[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaPlnsfx[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaCrrcd[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaNewinst[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaLastInst[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaBnewinst[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaBlastInst[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaLnewinst[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaLlastInst[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaNewsum[wsaaInd.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void readFile2000()
	{
        if (eofInBlock.isTrue()) {
            wsspEdterror.set(varcom.endp);
            return ;
        }
        if (isNE(wsaaInd,1)) {
            return ;
        }
        sqlerrorflag = false;
        try {
            for (incrpfCursorLoopIndex = 1; isLT(incrpfCursorLoopIndex, wsaaRowsInBlock.toInt())
            && getAppVars().fetchNext(sqlincrpfCursorrs); incrpfCursorLoopIndex++ ){
                getAppVars().getDBObject(sqlincrpfCursorrs, 1, wsaaFetchArrayInner.wsaaCownnum[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 2, wsaaFetchArrayInner.wsaaChdrcoy[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 3, wsaaFetchArrayInner.wsaaChdrnum[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 4, wsaaFetchArrayInner.wsaaLife[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 5, wsaaFetchArrayInner.wsaaJlife[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 6, wsaaFetchArrayInner.wsaaCoverage[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 7, wsaaFetchArrayInner.wsaaRider[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 8, wsaaFetchArrayInner.wsaaPlnsfx[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 9, wsaaFetchArrayInner.wsaaCrrcd[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 10, wsaaFetchArrayInner.wsaaCrtable[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 11, wsaaFetchArrayInner.wsaaValidflag[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 12, wsaaFetchArrayInner.wsaaStatcode[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 13, wsaaFetchArrayInner.wsaaPstatcode[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 14, wsaaFetchArrayInner.wsaaAnnvry[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 15, wsaaFetchArrayInner.wsaaBascmeth[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 16, wsaaFetchArrayInner.wsaaBascpy[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 17, wsaaFetchArrayInner.wsaaRnwcpy[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 18, wsaaFetchArrayInner.wsaaSrvcpy[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 19, wsaaFetchArrayInner.wsaaNewinst[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 20, wsaaFetchArrayInner.wsaaLastInst[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 21, wsaaFetchArrayInner.wsaaNewsum[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 22, wsaaFetchArrayInner.wsaaBnewinst[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 23, wsaaFetchArrayInner.wsaaBlastInst[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 24, wsaaFetchArrayInner.wsaaLnewinst[incrpfCursorLoopIndex]);
                getAppVars().getDBObject(sqlincrpfCursorrs, 25, wsaaFetchArrayInner.wsaaLlastInst[incrpfCursorLoopIndex]);
            }
        }
        catch (SQLException ex){
            sqlerrorflag = true;
            getAppVars().setSqlErrorCode(ex);
        }
        if (sqlerrorflag) {
            sqlError500();
        }
        /* We must detect :-*/
        /*    a) no rows returned on the first fetch*/
        /*    b) the last row returned (in the block)*/
        /* IF Either of the above cases occur, then an*/
        /*    SQLCODE = +100 is returned.*/
        /* The 3000 section is continued for case (b).*/
        if (isEQ(getAppVars().getSqlErrorCode(), 100)
        && isEQ(wsaaFetchArrayInner.wsaaCownnum[wsaaInd.toInt()], SPACES)) {
            wsspEdterror.set(varcom.endp);
            return ;
        }
        //tom chi modified, bug 1030
        else if(isEQ(wsaaFetchArrayInner.wsaaCownnum[wsaaInd.toInt()],SPACES)){
            wsspEdterror.set(varcom.endp);
        }
                //end
        if (firstTime.isTrue()) {
            wsaaPrevCownnum.set(wsaaFetchArrayInner.wsaaCownnum[wsaaInd.toInt()]);
            wsaaFirstTime.set("N");
        }
	}


protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1200();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/* If the the COWNNUM being processed is not equal to the*/
		/* previously stored COWNNUM, move to the next output file*/
		/* member. The condition is here to allow for checking of the*/
		/* COWNNUM of the old block with the first of the new and to*/
		/* to write to the next INCZ member if they have changed.       */
		if (isNE(wsaaFetchArrayInner.wsaaCownnum[wsaaInd.toInt()], wsaaPrevCownnum)) {
			wsaaPrevCownnum.set(wsaaFetchArrayInner.wsaaCownnum[wsaaInd.toInt()]);
			iy.add(1);
		}
		/* Load from storage all INCZ data for the same client until    */
		/* the COWNNUM has changed or the end of an incomplete block*/
		/* is reached.*/
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| isNE(wsaaFetchArrayInner.wsaaCownnum[wsaaInd.toInt()], wsaaPrevCownnum)
		|| eofInBlock.isTrue())) {
			loadSameClient3200();
		}
		
		/*EXIT*/
	}

protected void loadSameClient3200()
	{
        inczpfData.chdrcoy.set(wsaaFetchArrayInner.wsaaChdrcoy[wsaaInd.toInt()]);
        inczpfData.chdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
        inczpfData.life.set(wsaaFetchArrayInner.wsaaLife[wsaaInd.toInt()]);
        inczpfData.coverage.set(wsaaFetchArrayInner.wsaaCoverage[wsaaInd.toInt()]);
        inczpfData.rider.set(wsaaFetchArrayInner.wsaaRider[wsaaInd.toInt()]);
        inczpfData.planSuffix.set(wsaaFetchArrayInner.wsaaPlnsfx[wsaaInd.toInt()]);
        inczpfData.crrcd.set(wsaaFetchArrayInner.wsaaCrrcd[wsaaInd.toInt()]);
        inczpfData.crtable.set(wsaaFetchArrayInner.wsaaCrtable[wsaaInd.toInt()]);
        inczpfData.validflag.set(wsaaFetchArrayInner.wsaaValidflag[wsaaInd.toInt()]);
        inczpfData.statcode.set(wsaaFetchArrayInner.wsaaStatcode[wsaaInd.toInt()]);
        inczpfData.pstatcode.set(wsaaFetchArrayInner.wsaaPstatcode[wsaaInd.toInt()]);
        inczpfData.anniversaryMethod.set(wsaaFetchArrayInner.wsaaAnnvry[wsaaInd.toInt()]);
        inczpfData.basicCommMeth.set(wsaaFetchArrayInner.wsaaBascmeth[wsaaInd.toInt()]);
        inczpfData.bascpy.set(wsaaFetchArrayInner.wsaaBascpy[wsaaInd.toInt()]);
        inczpfData.rnwcpy.set(wsaaFetchArrayInner.wsaaRnwcpy[wsaaInd.toInt()]);
        inczpfData.srvcpy.set(wsaaFetchArrayInner.wsaaSrvcpy[wsaaInd.toInt()]);
        inczpfData.newinst.set(wsaaFetchArrayInner.wsaaNewinst[wsaaInd.toInt()]);
        inczpfData.lastInst.set(wsaaFetchArrayInner.wsaaLastInst[wsaaInd.toInt()]);
        inczpfData.zbnewinst.set(wsaaFetchArrayInner.wsaaBnewinst[wsaaInd.toInt()]);
        inczpfData.zblastinst.set(wsaaFetchArrayInner.wsaaBlastInst[wsaaInd.toInt()]);
        inczpfData.zlnewinst.set(wsaaFetchArrayInner.wsaaLnewinst[wsaaInd.toInt()]);
        inczpfData.zllastinst.set(wsaaFetchArrayInner.wsaaLlastInst[wsaaInd.toInt()]);
        inczpfData.newsum.set(wsaaFetchArrayInner.wsaaNewsum[wsaaInd.toInt()]);
        if (isGT(iy,bprdIO.getThreadsSubsqntProc())) {
            iy.set(1);
        }
        if (isEQ(iy,1)) {
            incz01.write(inczpfData);
        }
        if (isEQ(iy,2)) {
            incz02.write(inczpfData);
        }
        if (isEQ(iy,3)) {
            incz03.write(inczpfData);
        }
        if (isEQ(iy,4)) {
            incz04.write(inczpfData);
        }
        if (isEQ(iy,5)) {
            incz05.write(inczpfData);
        }
        if (isEQ(iy,6)) {
            incz06.write(inczpfData);
        }
        if (isEQ(iy,7)) {
            incz07.write(inczpfData);
        }
        if (isEQ(iy,8)) {
            incz08.write(inczpfData);
        }
        if (isEQ(iy,9)) {
            incz09.write(inczpfData);
        }
        if (isEQ(iy,10)) {
            incz10.write(inczpfData);
        }
        if (isEQ(iy,11)) {
            incz11.write(inczpfData);
        }
        if (isEQ(iy,12)) {
            incz12.write(inczpfData);
        }
        if (isEQ(iy,13)) {
            incz13.write(inczpfData);
        }
        if (isEQ(iy,14)) {
            incz14.write(inczpfData);
        }
        if (isEQ(iy,15)) {
            incz15.write(inczpfData);
        }
        if (isEQ(iy,16)) {
            incz16.write(inczpfData);
        }
        if (isEQ(iy,17)) {
            incz17.write(inczpfData);
        }
        if (isEQ(iy,18)) {
            incz18.write(inczpfData);
        }
        if (isEQ(iy,19)) {
            incz19.write(inczpfData);
        }
        if (isEQ(iy,20)) {
            incz20.write(inczpfData);
        }
        /* Log the number of extacted records.*/
        ct02Value++;
        /* Set up the array for the next block of records.*/
        wsaaInd.add(1);
        /* Check for an incomplete block retrieved.*/       
        if (isLTE(wsaaInd,wsaaRowsInBlock) && isEQ(wsaaFetchArrayInner.wsaaCownnum[wsaaInd.toInt()],SPACES)) {
            wsaaEofInBlock.set("Y");
        }
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
        contotrec.totval.set(ct02Value);
        contotrec.totno.set(ct02);
        callContot001();
        ct02Value = 0;
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/* Close the open files and delete the overrides.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlincrpfCursorconn, sqlincrpfCursorps, sqlincrpfCursorrs);
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
        if (isEQ(iz,1)) {
            incz01.close();
        }
        if (isEQ(iz,2)) {
            incz02.close();
        }
        if (isEQ(iz,3)) {
            incz03.close();
        }
        if (isEQ(iz,4)) {
            incz04.close();
        }
        if (isEQ(iz,5)) {
            incz05.close();
        }
        if (isEQ(iz,6)) {
            incz06.close();
        }
        if (isEQ(iz,7)) {
            incz07.close();
        }
        if (isEQ(iz,8)) {
            incz08.close();
        }
        if (isEQ(iz,9)) {
            incz09.close();
        }
        if (isEQ(iz,10)) {
            incz10.close();
        }
        if (isEQ(iz,11)) {
            incz11.close();
        }
        if (isEQ(iz,12)) {
            incz12.close();
        }
        if (isEQ(iz,13)) {
            incz13.close();
        }
        if (isEQ(iz,14)) {
            incz14.close();
        }
        if (isEQ(iz,15)) {
            incz15.close();
        }
        if (isEQ(iz,16)) {
            incz16.close();
        }
        if (isEQ(iz,17)) {
            incz17.close();
        }
        if (isEQ(iz,18)) {
            incz18.close();
        }
        if (isEQ(iz,19)) {
            incz19.close();
        }
        if (isEQ(iz,20)) {
            incz20.close();
        }

	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure WSAA-FETCH-ARRAY--INNER
 */
private static final class WsaaFetchArrayInner { 

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaIncrData = FLSInittedArray (1000, 125);
	private FixedLengthStringData[] wsaaCownnum = FLSDArrayPartOfArrayStructure(8, wsaaIncrData, 0);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaIncrData, 8);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaIncrData, 9);
	private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaIncrData, 17);
	private FixedLengthStringData[] wsaaJlife = FLSDArrayPartOfArrayStructure(2, wsaaIncrData, 19);
	private FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaIncrData, 21);
	private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaIncrData, 23);
	private PackedDecimalData[] wsaaPlnsfx = PDArrayPartOfArrayStructure(4, 0, wsaaIncrData, 25);
	private PackedDecimalData[] wsaaCrrcd = PDArrayPartOfArrayStructure(8, 0, wsaaIncrData, 28);
	private FixedLengthStringData[] wsaaCrtable = FLSDArrayPartOfArrayStructure(4, wsaaIncrData, 33);
	private FixedLengthStringData[] wsaaValidflag = FLSDArrayPartOfArrayStructure(1, wsaaIncrData, 37);
	private FixedLengthStringData[] wsaaStatcode = FLSDArrayPartOfArrayStructure(2, wsaaIncrData, 38);
	private FixedLengthStringData[] wsaaPstatcode = FLSDArrayPartOfArrayStructure(2, wsaaIncrData, 40);
	private FixedLengthStringData[] wsaaAnnvry = FLSDArrayPartOfArrayStructure(4, wsaaIncrData, 42);
	private FixedLengthStringData[] wsaaBascmeth = FLSDArrayPartOfArrayStructure(4, wsaaIncrData, 46);
	private FixedLengthStringData[] wsaaBascpy = FLSDArrayPartOfArrayStructure(4, wsaaIncrData, 50);
	private FixedLengthStringData[] wsaaRnwcpy = FLSDArrayPartOfArrayStructure(4, wsaaIncrData, 54);
	private FixedLengthStringData[] wsaaSrvcpy = FLSDArrayPartOfArrayStructure(4, wsaaIncrData, 58);
	private PackedDecimalData[] wsaaNewinst = PDArrayPartOfArrayStructure(17, 2, wsaaIncrData, 62);
	private PackedDecimalData[] wsaaLastInst = PDArrayPartOfArrayStructure(17, 2, wsaaIncrData, 71);
	private PackedDecimalData[] wsaaNewsum = PDArrayPartOfArrayStructure(17, 2, wsaaIncrData, 80);
	private PackedDecimalData[] wsaaBnewinst = PDArrayPartOfArrayStructure(17, 2, wsaaIncrData, 89);
	private PackedDecimalData[] wsaaBlastInst = PDArrayPartOfArrayStructure(17, 2, wsaaIncrData, 98);
	private PackedDecimalData[] wsaaLnewinst = PDArrayPartOfArrayStructure(17, 2, wsaaIncrData, 107);
	private PackedDecimalData[] wsaaLlastInst = PDArrayPartOfArrayStructure(17, 2, wsaaIncrData, 116);
}
}
