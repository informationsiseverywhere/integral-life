package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;
import com.csc.life.regularprocessing.dataaccess.dao.B5353DAO;
import com.csc.life.regularprocessing.dataaccess.model.Linxpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class B5353DAOImpl extends BaseDAOImpl<Linxpf> implements B5353DAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(LinxpfDAOImpl.class);

	public List<Linxpf> loadDataByBatch(int batchID, String threadNumber) {
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT * FROM B5353DATA "); //ILB-475
		sb.append("WHERE BATCHNUM = ? AND RTRIM(MEMBER_NAME) = ? ");
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC,INSTFROM ASC, PAYRSEQNO DESC ");/*BASE ISSUE fixed under ILIFE-7616 */
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Linxpf> linxList = new ArrayList<Linxpf>();
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, Integer.toString(batchID));
			ps.setString(2, threadNumber.trim());
			rs = ps.executeQuery();
			while (rs.next()) {
				Linxpf linxpf = new Linxpf();

				linxpf.setChdrnum(rs.getString("chdrnum") != null ? (rs.getString("chdrnum").trim()) : "");
				linxpf.setChdrcoy(rs.getString("chdrcoy") != null ? (rs.getString("chdrcoy").trim()) : "");
				linxpf.setInstfrom(rs.getInt("instfrom"));
				linxpf.setCntcurr(rs.getString("cntcurr") != null ? (rs.getString("cntcurr").trim()) : "");
				linxpf.setBillcurr(rs.getString("billcurr") != null ? (rs.getString("billcurr").trim()) : "");
				linxpf.setPayrseqno(rs.getInt("payrseqno"));
				linxpf.setCbillamt(rs.getBigDecimal("cbillamt"));
				linxpf.setBillcd(rs.getInt("billcd"));
				linxpf.setBillchnl(rs.getString("billchnl") != null ? (rs.getString("billchnl").trim()) : "");
				linxpf.setInstamt01(rs.getBigDecimal("instamt01"));
				linxpf.setInstamt02(rs.getBigDecimal("instamt02"));
				linxpf.setInstamt03(rs.getBigDecimal("instamt03"));
				linxpf.setInstamt04(rs.getBigDecimal("instamt04"));
				linxpf.setInstamt05(rs.getBigDecimal("instamt05"));
				linxpf.setInstamt06(rs.getBigDecimal("instamt06"));
				linxpf.setInstfreq(rs.getString("instfreq") != null ? (rs.getString("instfreq").trim()) : "");
				linxpf.setProraterec(rs.getString("proraterec") != null ? (rs.getString("proraterec").trim()) : "");

				linxpf.setChdrCnttype(rs.getString("chdrcnttype") != null ? (rs.getString("chdrcnttype").trim()) : "");
				linxpf.setChdrOccdate(rs.getInt("chdroccdate"));
				linxpf.setChdrStatcode(rs.getString("chdrstatcode") != null ? (rs.getString("chdrstatcode").trim())
						: "");
				linxpf.setChdrPstatcode(rs.getString("chdrpstatcode") != null ? (rs.getString("chdrpstatcode").trim())
						: "");
				linxpf.setChdrTranno(rs.getInt("chdrtranno"));
				linxpf.setChdrInsttot01(rs.getBigDecimal("chdrinsttot01"));
				linxpf.setChdrInsttot02(rs.getBigDecimal("chdrinsttot02"));
				linxpf.setChdrInsttot03(rs.getBigDecimal("chdrinsttot03"));
				linxpf.setChdrInsttot04(rs.getBigDecimal("chdrinsttot04"));
				linxpf.setChdrInsttot05(rs.getBigDecimal("chdrinsttot05"));
				linxpf.setChdrInsttot06(rs.getBigDecimal("chdrinsttot06"));
				linxpf.setChdrOutstamt(rs.getBigDecimal("chdroutstamt"));
				linxpf.setChdrAgntnum(rs.getString("chdragntnum") != null ? (rs.getString("chdragntnum").trim()) : "");
				linxpf.setChdrCntcurr(rs.getString("chdrcntcurr") != null ? (rs.getString("chdrcntcurr").trim()) : "");
				linxpf.setChdrPtdate(rs.getInt("chdrptdate"));
				linxpf.setChdrRnwlsupr(rs.getString("chdrrnwlsupr") != null ? (rs.getString("chdrrnwlsupr").trim())
						: "");
				linxpf.setChdrRnwlspfrom(rs.getInt("chdrrnwlspfrom"));
				linxpf.setChdrRnwlspto(rs.getInt("chdrrnwlspto"));
				linxpf.setChdrCowncoy(rs.getString("chdrcowncoy") != null ? (rs.getString("chdrcowncoy").trim()) : "");
				linxpf.setChdrCownnum(rs.getString("chdrcownnum") != null ? (rs.getString("chdrcownnum").trim()) : "");
				linxpf.setChdrCntbranch(rs.getString("chdrcntbranch") != null ? (rs.getString("chdrcntbranch").trim())
						: "");
				linxpf.setChdrAgntcoy(rs.getString("chdragntcoy") != null ? (rs.getString("chdragntcoy").trim()) : "");
				linxpf.setChdrRegister(rs.getString("chdrregister") != null ? (rs.getString("chdrregister").trim())
						: "");
				linxpf.setChdrchdrpfx(rs.getString("chdrchdrpfx") != null ? (rs.getString("chdrchdrpfx").trim()) : "");
				linxpf.setChdrnlgflg(rs.getString("chdrnlgflg") != null ? (rs.getString("chdrnlgflg").trim()) : "");
				linxpf.setPayrTaxrelmth(rs.getString("payrtaxrelmth") != null ? (rs.getString("payrtaxrelmth").trim())
						: "");
				linxpf.setPayrIncomeseqno(rs.getInt("payrincomeseqno"));
				linxpf.setPayrTranno(rs.getInt("payrtranno"));
				linxpf.setPayrOutstamt(rs.getBigDecimal("payroutstamt"));
				linxpf.setPayrBillchnl(rs.getString("payrbillchnl") != null ? (rs.getString("payrbillchnl").trim())
						: "");
				linxpf.setPayrMandref(rs.getString("payrmandref") != null ? (rs.getString("payrmandref").trim()) : "");
				linxpf.setPayrCntcurr(rs.getString("payrcntcurr") != null ? (rs.getString("payrcntcurr").trim()) : "");
				linxpf.setPayrBillfreq(rs.getString("payrbillfreq") != null ? (rs.getString("payrbillfreq").trim())
						: "");
				linxpf.setPayrBillcd(rs.getInt("payrbillcd"));
				linxpf.setPayrPtdate(rs.getInt("payrptdate"));
				linxpf.setPayrBtdate(rs.getInt("payrbtdate"));
				linxpf.setPayrNextDate(rs.getInt("payrnextdate"));
				linxpf.setPayOrgbillcd(rs.getInt("payorgbillcd"));
				linxpf.setLinsInstto(rs.getInt("lsinstto"));
				linxpf.setChdrUniqueNumber(rs.getLong("cunique_number"));
				linxpf.setLinsUniqueNumber(rs.getLong("lsunique_number"));
				linxpf.setPayrUniqueNumber(rs.getLong("psunique_number"));

				linxpf.setAglfDteapp(rs.getInt("AglfDteapp"));
				linxpf.setAglfDteexp(rs.getInt("AglfDteexp"));
				linxpf.setAglfDtetrm(rs.getInt("AglfDtetrm"));
				linxpf.setHcsdZcshdivmth(rs.getString("HCSDZCSHDIVMTH"));
				linxpf.setHcsdZdivopt(rs.getString("HCSDZDIVOPT"));
				linxpf.setHdisHcapndt(rs.getInt("HDISHCAPNDT"));
				linxpf.setProrcntfee(rs.getBigDecimal("PRORCNTFEE") != null ? rs.getBigDecimal("PRORCNTFEE") : BigDecimal.ZERO);
				linxpf.setProramt(rs.getBigDecimal("PRORAMT") != null ? rs.getBigDecimal("PRORAMT") : BigDecimal.ZERO);
				
				linxList.add(linxpf);
			}
		} catch (SQLException e) {
			LOGGER.error("loadDataByBatch()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return linxList;
	}

	public void initializeB5353Temp() {
		StringBuilder sb = new StringBuilder("");
		sb.append("DELETE FROM B5353DATA "); //ILB-475
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.executeUpdate();

		} catch (SQLException e) {
			LOGGER.error("initializeB5353Temp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
	}

	public int populateB5353Temp(int batchExtractSize, String linxtempTable, String memberName) {
		int rows = 0;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO B5353DATA "); //ILB-475
		sb.append("(CHDRNUM, CHDRCOY, INSTFROM, CNTCURR, BILLCURR, PAYRSEQNO, CBILLAMT, BILLCD, BILLCHNL, INSTAMT01, INSTAMT02, INSTAMT03, INSTAMT04, INSTAMT05, INSTAMT06, INSTFREQ, PRORATEREC, CHDRCNTTYPE, CHDROCCDATE, CHDRSTATCODE, CHDRPSTATCODE, CHDRTRANNO, CHDRINSTTOT01, CHDRINSTTOT02, CHDRINSTTOT03, CHDRINSTTOT04, CHDRINSTTOT05, CHDRINSTTOT06, CHDROUTSTAMT, CHDRAGNTNUM, CHDRCNTCURR, CHDRPTDATE, CHDRRNWLSUPR, CHDRRNWLSPFROM, CHDRRNWLSPTO, CHDRCOWNCOY, CHDRCOWNNUM, CHDRCNTBRANCH, CHDRAGNTCOY, CHDRREGISTER, CHDRCHDRPFX, CHDRNLGFLG, PAYRTAXRELMTH, PAYRINCOMESEQNO, PAYRTRANNO, PAYROUTSTAMT, PAYRBILLCHNL, PAYRMANDREF, PAYRCNTCURR, PAYRBILLFREQ, PAYRBILLCD, PAYRPTDATE, PAYRBTDATE, PAYRNEXTDATE, PAYORGBILLCD, LSINSTTO, CUNIQUE_NUMBER, LSUNIQUE_NUMBER, PSUNIQUE_NUMBER, PRORCNTFEE, PRORAMT, BATCHNUM, AGLFDTEAPP, AGLFDTEEXP, AGLFDTETRM, HCSDZDIVOPT, HCSDZCSHDIVMTH, HDISHCAPNDT, MEMBER_NAME) ");
		sb.append(" SELECT DISTINCT L.CHDRNUM, L.CHDRCOY, L.INSTFROM, L.CNTCURR, L.BILLCURR, L.PAYRSEQNO, L.CBILLAMT, L.BILLCD, L.BILLCHNL, L.INSTAMT01, L.INSTAMT02, L.INSTAMT03, L.INSTAMT04, L.INSTAMT05, L.INSTAMT06, L.INSTFREQ, L.PRORATEREC, C.CNTTYPE CHDRCNTTYPE, C.OCCDATE CHDROCCDATE, C.STATCODE CHDRSTATCODE, C.PSTCDE CHDRPSTATCODE, C.TRANNO CHDRTRANNO, C.INSTTOT01 CHDRINSTTOT01, C.INSTTOT02 CHDRINSTTOT02, C.INSTTOT03 CHDRINSTTOT03, C.INSTTOT04 CHDRINSTTOT04, C.INSTTOT05 CHDRINSTTOT05, C.INSTTOT06 CHDRINSTTOT06, C.OUTSTAMT CHDROUTSTAMT, C.AGNTNUM CHDRAGNTNUM, C.CNTCURR CHDRCNTCURR, C.PTDATE CHDRPTDATE, C.RNWLSUPR CHDRRNWLSUPR, C.RNWLSPFROM CHDRRNWLSPFROM, C.RNWLSPTO CHDRRNWLSPTO, C.COWNCOY CHDRCOWNCOY, C.COWNNUM CHDRCOWNNUM, C.CNTBRANCH CHDRCNTBRANCH, C.AGNTCOY CHDRAGNTCOY, C.REG CHDRREGISTER, C.CHDRPFX CHDRCHDRPFX, C.NLGFLG CHDRNLGFLG, P.TAXRELMTH PAYRTAXRELMTH, P.INCSEQNO PAYRINCOMESEQNO, P.TRANNO PAYRTRANNO, P.OUTSTAMT PAYROUTSTAMT, P.BILLCHNL PAYRBILLCHNL, P.MANDREF PAYRMANDREF, P.CNTCURR PAYRCNTCURR, P.BILLFREQ PAYRBILLFREQ, P.BILLCD PAYRBILLCD, P.PTDATE PAYRPTDATE, P.BTDATE PAYRBTDATE, P.NEXTDATE PAYRNEXTDATE, P.ORGBILLCD PAYORGBILLCD, LS.INSTTO LSINSTTO, C.UNIQUE_NUMBER CUNIQUE_NUMBER, LS.UNIQUE_NUMBER LSUNIQUE_NUMBER, P.UNIQUE_NUMBER PSUNIQUE_NUMBER, LS.PRORCNTFEE, LS.PRORAMT, FLOOR( (ROW_NUMBER() OVER( ORDER BY L.UNIQUE_NUMBER ASC,C.CHDRCOY ASC,C.CHDRNUM ASC,C.UNIQUE_NUMBER DESC,P.CHDRCOY ASC,P.CHDRNUM ASC,P.PAYRSEQNO ASC,P.UNIQUE_NUMBER DESC,LS.CHDRCOY ASC,LS.CHDRNUM ASC,LS.INSTFROM ASC,LS.UNIQUE_NUMBER ASC ) - 1) / ?) BATCHNUM ");
		sb.append(" , AG.DTETRM AGLFDTEAPP, AG.DTEEXP AGLFDTEEXP, AG.DTEAPP AGLFDTETRM ");
		sb.append(" , HC.ZDIVOPT HCSDZDIVOPT, HC.ZCSHDIVMTH HCSDZCSHDIVMTH ");
		sb.append(" , HD.HCAPNDT HDISHCAPNDT, L.MEMBER_NAME ");
		sb.append(" FROM ");
		sb.append(linxtempTable);
		sb.append(" L ");
		sb.append("LEFT JOIN CHDRPF C ON C.CHDRCOY = L.CHDRCOY AND C.CHDRNUM = L.CHDRNUM AND C.VALIDFLAG = '1' ");
		sb.append("LEFT JOIN PAYRPF P ON P.CHDRCOY = L.CHDRCOY AND P.CHDRNUM = L.CHDRNUM AND P.PAYRSEQNO = L.PAYRSEQNO AND P.VALIDFLAG = '1' ");
		sb.append("LEFT JOIN LINSPF LS ON LS.CHDRCOY = L.CHDRCOY AND LS.CHDRNUM = L.CHDRNUM AND LS.INSTFROM = L.INSTFROM AND LS.PAYFLAG != 'P' ");
		sb.append("LEFT JOIN AGLFPF AG ON AG.AGNTCOY = L.CHDRCOY AND AG.AGNTNUM = C.AGNTNUM ");
		sb.append("LEFT JOIN HCSDPF HC ON HC.CHDRCOY = L.CHDRCOY AND LIFE='01' AND COVERAGE='01' AND RIDER='00' AND PLNSFX=0 AND HC.CHDRNUM = L.CHDRNUM AND HC.VALIDFLAG = '1' "); //fix ICIL-743
		sb.append("LEFT JOIN HDISPF HD ON HD.CHDRCOY = L.CHDRCOY AND HD.CHDRNUM = L.CHDRNUM AND HD.VALIDFLAG = '1' ");
		sb.append(" WHERE L.MEMBER_NAME = ?");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setInt(1, batchExtractSize);
			ps.setString(2, memberName);
			rows = ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("populateB5353Temp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return rows;
	}

	public int populateB5353TempJpn(int batchExtractSize, String linxtempTable, String memberName) {
		int rows = 0;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO B5353DATA "); //ILB-475
		sb.append("(CHDRNUM, CHDRCOY, INSTFROM, CNTCURR, BILLCURR, PAYRSEQNO, CBILLAMT, BILLCD, BILLCHNL, INSTAMT01,");
		sb.append( " INSTAMT02, INSTAMT03, INSTAMT04, INSTAMT05, INSTAMT06, INSTFREQ, CHDRCNTTYPE, CHDROCCDATE, ");
		sb.append( "CHDRSTATCODE, CHDRPSTATCODE, CHDRTRANNO, CHDRINSTTOT01, CHDRINSTTOT02, ");
		sb.append( "CHDRINSTTOT03, CHDRINSTTOT04, CHDRINSTTOT05,");
		sb.append( " CHDRINSTTOT06, CHDROUTSTAMT, CHDRAGNTNUM, CHDRCNTCURR, CHDRPTDATE, CHDRRNWLSUPR, CHDRRNWLSPFROM, ");
		sb.append( " CHDRRNWLSPTO, CHDRCOWNCOY, CHDRCOWNNUM, CHDRCNTBRANCH, CHDRAGNTCOY, ");
		sb.append( "CHDRREGISTER, CHDRCHDRPFX, CHDRNLGFLG,  ");
		sb.append( "PAYRTAXRELMTH, PAYRINCOMESEQNO, PAYRTRANNO, PAYROUTSTAMT, PAYRBILLCHNL,");
		sb.append( " PAYRMANDREF, PAYRCNTCURR, PAYRBILLFREQ, ");
		sb.append( " PAYRBILLCD, PAYRPTDATE, PAYRBTDATE, PAYRNEXTDATE, LSINSTTO, CUNIQUE_NUMBER,");
		sb.append( " LSUNIQUE_NUMBER, PSUNIQUE_NUMBER, ");
		sb.append( " PRORCNTFEE, PRORAMT, BATCHNUM, AGLFDTEAPP, AGLFDTEEXP, AGLFDTETRM, ");
		sb.append( "HCSDZDIVOPT, HCSDZCSHDIVMTH, HDISHCAPNDT, ");
		sb.append("  MEMBER_NAME) SELECT DISTINCT L.CHDRNUM, L.CHDRCOY, L.INSTFROM, L.CNTCURR, L.BILLCURR, L.PAYRSEQNO, ");
		sb.append( "L.CBILLAMT, L.BILLCD, L.BILLCHNL, L.INSTAMT01, L.INSTAMT02, L.INSTAMT03, L.INSTAMT04, L.INSTAMT05, ");
		sb.append( "L.INSTAMT06, L.INSTFREQ, C.CNTTYPE CHDRCNTTYPE, C.OCCDATE CHDROCCDATE, C.STATCODE CHDRSTATCODE,");
		sb.append( " C.PSTCDE CHDRPSTATCODE, C.TRANNO CHDRTRANNO, C.INSTTOT01 CHDRINSTTOT01, C.INSTTOT02 CHDRINSTTOT02, ");
		sb.append( "C.INSTTOT03 CHDRINSTTOT03, C.INSTTOT04 CHDRINSTTOT04, C.INSTTOT05 CHDRINSTTOT05,");
		sb.append( " C.INSTTOT06 CHDRINSTTOT06, C.OUTSTAMT CHDROUTSTAMT, ");
		sb.append( " C.AGNTNUM CHDRAGNTNUM, C.CNTCURR CHDRCNTCURR, C.PTDATE CHDRPTDATE, ");
		sb.append( "C.RNWLSUPR CHDRRNWLSUPR,  C.RNWLSPFROM CHDRRNWLSPFROM,");
		sb.append( " C.RNWLSPTO CHDRRNWLSPTO, C.COWNCOY CHDRCOWNCOY, C.COWNNUM CHDRCOWNNUM,");
		sb.append( " C.CNTBRANCH CHDRCNTBRANCH, C.AGNTCOY CHDRAGNTCOY, C.REG CHDRREGISTER, C.CHDRPFX CHDRCHDRPFX,");
		sb.append("  C.NLGFLG CHDRNLGFLG, P.TAXRELMTH PAYRTAXRELMTH, P.INCSEQNO PAYRINCOMESEQNO,");
		sb.append( " P.TRANNO PAYRTRANNO, P.OUTSTAMT PAYROUTSTAMT,");
		sb.append( " P.BILLCHNL PAYRBILLCHNL, P.MANDREF PAYRMANDREF, P.CNTCURR PAYRCNTCURR,");
		sb.append( " P.BILLFREQ PAYRBILLFREQ, P.BILLCD PAYRBILLCD, ");
		sb.append( "P.PTDATE PAYRPTDATE, P.BTDATE PAYRBTDATE, P.NEXTDATE PAYRNEXTDATE, ");
		sb.append( "LS.INSTTO LSINSTTO, C.UNIQUE_NUMBER CUNIQUE_NUMBER,");
		sb.append( " LS.UNIQUE_NUMBER LSUNIQUE_NUMBER, P.UNIQUE_NUMBER PSUNIQUE_NUMBER, LS.PRORCNTFEE, LS.PRORAMT,");
		sb.append( " FLOOR( (ROW_NUMBER() OVER( ORDER BY L.UNIQUE_NUMBER ASC,C.CHDRCOY ASC,");
		sb.append( "C.CHDRNUM ASC,C.UNIQUE_NUMBER DESC,P.CHDRCOY ASC,");
		sb.append( "P.CHDRNUM ASC,P.PAYRSEQNO ASC,P.UNIQUE_NUMBER DESC,LS.CHDRCOY ASC,LS.CHDRNUM ASC,LS.INSTFROM ASC,");
		sb.append( "LS.UNIQUE_NUMBER ASC ) - 1) / ?) BATCHNUM ");
		sb.append(" , AG.DTETRM AGLFDTEAPP, AG.DTEEXP AGLFDTEEXP, AG.DTEAPP AGLFDTETRM ");
		sb.append(" , HC.ZDIVOPT HCSDZDIVOPT, HC.ZCSHDIVMTH HCSDZCSHDIVMTH ");
		sb.append(" , HD.HCAPNDT HDISHCAPNDT, L.MEMBER_NAME ");
		sb.append(" FROM ");
		sb.append(linxtempTable);
		sb.append(" L ");
		sb.append("LEFT JOIN CHDRPF C ON C.CHDRCOY = L.CHDRCOY AND C.CHDRNUM = L.CHDRNUM");
		sb.append( " AND (C.VALIDFLAG = '1' OR C.VALIDFLAG = '3') ");
		sb.append("LEFT JOIN PAYRPF P ON P.CHDRCOY = L.CHDRCOY AND P.CHDRNUM = L.CHDRNUM AND");
		sb.append( " P.PAYRSEQNO = L.PAYRSEQNO AND P.VALIDFLAG = '1' ");
		sb.append("LEFT JOIN LINSPF LS ON LS.CHDRCOY = L.CHDRCOY AND LS.CHDRNUM = L.CHDRNUM ");
		sb.append( "AND LS.INSTFROM = L.INSTFROM AND LS.PAYFLAG != 'P' ");
		sb.append("LEFT JOIN AGLFPF AG ON AG.AGNTCOY = L.CHDRCOY AND AG.AGNTNUM = C.AGNTNUM ");
		sb.append("LEFT JOIN HCSDPF HC ON HC.CHDRCOY = L.CHDRCOY AND LIFE='01' AND COVERAGE='01' ");
		sb.append( "AND RIDER='00' AND PLNSFX=0 AND HC.CHDRNUM = L.CHDRNUM AND HC.VALIDFLAG = '1' "); //fix ICIL-743
		sb.append("LEFT JOIN HDISPF HD ON HD.CHDRCOY = L.CHDRCOY AND HD.CHDRNUM = L.CHDRNUM AND HD.VALIDFLAG = '1' ");
		sb.append(" WHERE L.MEMBER_NAME = ?");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setInt(1, batchExtractSize);
			ps.setString(2, memberName);
			rows = ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("populateB5353Temp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return rows;
	}
	public int getB5353DataCount(String memberName){
		int rows = 0;
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT COUNT (*) FROM B5353DATA WHERE MEMBER_NAME = ?  "); 
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, StringUtil.fillSpace(memberName.trim(), DD.mbr.length));
			rs = ps.executeQuery();
			while(rs.next()){
				rows = rs.getInt(1);
			}
			
		}catch (SQLException e) {
			LOGGER.error("getB5353DataCount()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
	 return rows;
	}

}