package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.dao.impl.BextpfDAOImpl;
import com.csc.life.newbusiness.dataaccess.model.Payrpf;
import com.csc.life.regularprocessing.dataaccess.dao.HbxtpntDAO;
import com.csc.life.regularprocessing.dataaccess.model.Hbxtpnt;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class HbxtpntDAOImpl extends BaseDAOImpl<Hbxtpnt> implements HbxtpntDAO {


	private static final Logger LOGGER = LoggerFactory.getLogger(BextpfDAOImpl.class);
@Override
	public List<Hbxtpnt> readHbxtpnt(String chdrcoy, String chdrnum, int instfrom) {
	List<Hbxtpnt> hbxtpntlist=new LinkedList<Hbxtpnt>();
	StringBuilder query = new StringBuilder("");
	query.append("SELECT * FROM HBXTPNT ");
	query.append("WHERE CHDRCOY = ? AND CHDRNUM = ? AND INSTFROM = ? ");
	LOGGER.info(query.toString());

	PreparedStatement ps = null;
	ResultSet rs = null;		
	try {
		ps = getPrepareStatement(query.toString());
		ps.setString(1,chdrcoy);	
		ps.setString(2,chdrnum);
		ps.setInt(3,instfrom);
		rs=executeQuery(ps);
		while(rs.next())
		{
			Hbxtpnt hbxtpntobj=new Hbxtpnt();
			hbxtpntobj.setChdrcoy(rs.getString("CHDRCOY"));
			hbxtpntobj.setChdrnum(rs.getString("CHDRNUM"));
			hbxtpntobj.setCnttype(rs.getString("CNTTYPE"));
			hbxtpntobj.setCowncoy(rs.getString("COWNCOY"));
			hbxtpntobj.setCownnum(rs.getString("COWNNUM"));
			hbxtpntobj.setChdrpfx(rs.getString("CHDRPFX"));
			hbxtpntobj.setCntbranch(rs.getString("CNTBRANCH"));
			hbxtpntobj.setInstfrom(rs.getInt("INSTFROM"));
			hbxtpntlist.add(hbxtpntobj);
		}
		
	} catch (SQLException e) {
		LOGGER.error("read()", e);//IJTI-1561
		throw new SQLRuntimeException(e);
	} finally {
		close(ps, rs);
	}
	return hbxtpntlist;
	}

	public boolean delete(Hbxtpnt hbxtpnt)
	{
		boolean flag=false;
		StringBuilder query = new StringBuilder("");
		query.append("Delete HBXTPNT WHERE CNTTYPE = ?");
		LOGGER.info(query.toString());

		PreparedStatement ps = null;
		ResultSet rs = null;		
		try {
			ps = getPrepareStatement(query.toString());
			ps.setString(1,hbxtpnt.getCnttype());	
			ps.executeUpdate();
			flag=true;
			
			
		} catch (SQLException e) {
			LOGGER.error("delete()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return flag;
		
	}
	
	public boolean update(String flag,Hbxtpnt hbxtpnt)
	{
		boolean updated=false;
		StringBuilder query = new StringBuilder("");
		query.append("Update HBXTPNT SET BILFLAG = ? WHERE CHDRCOY = ? AND CHDRNUM = ? AND INSTFROM = ?");
		LOGGER.info(query.toString());

		PreparedStatement ps = null;
		ResultSet rs = null;		
		try {
			ps = getPrepareStatement(query.toString());
			ps.setString(1,flag);
			ps.setString(2,hbxtpnt.getChdrcoy());
			ps.setString(3,hbxtpnt.getChdrnum());
			ps.setInt(4,hbxtpnt.getInstfrom());
			
			ps.executeUpdate();
			updated=true;
			
			
		} catch (SQLException e) {
			LOGGER.error("update()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return updated;
	}

	public List<Payrpf> readPayrpfTranno(int seqno,Hbxtpnt hbxtpnt)
	{
		List<Payrpf> list=new LinkedList<Payrpf>();
		StringBuilder query = new StringBuilder("");
		query.append("SELECT PAYR.TRANNO ");
		query.append("FROM PAYR ");
		query.append("INNER JOIN HBXTPNT ON PAYR.CHDRCOY = HBXTPNT.CHDRCOY AND PAYR.CHDRNUM = HBXTPNT.CHDRNUM  where PAYR.PAYRSEQNO=? AND HBXTPNT.CHDRCOY=? AND HBXTPNT.CHDRNUM = ?");
		LOGGER.info(query.toString());

		PreparedStatement ps = null;
		ResultSet rs = null;		
		try {
			ps = getPrepareStatement(query.toString());
			ps.setInt(1,seqno);	
			ps.setString(2,hbxtpnt.getChdrcoy());
			ps.setString(3,hbxtpnt.getChdrnum());
			rs=executeQuery(ps);
			while(rs.next())
			{
				Payrpf payrpf=new Payrpf();
				payrpf.setTranno(rs.getInt(1));
				
				list.add(payrpf);
			}
			
		} catch (SQLException e) {
			LOGGER.error("readPayrpfTranno()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
	}
		return list;
}
}
