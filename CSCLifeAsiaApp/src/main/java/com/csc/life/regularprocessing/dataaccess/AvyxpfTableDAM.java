package com.csc.life.regularprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AvyxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:56
 * Class transformed from AVYXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AvyxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 29;
	public FixedLengthStringData avyxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData avyxpfRecord = avyxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(avyxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(avyxrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(avyxrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(avyxrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(avyxrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(avyxrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(avyxrec);
	public PackedDecimalData crrcd = DD.crrcd.copy().isAPartOf(avyxrec);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(avyxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AvyxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for AvyxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AvyxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AvyxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AvyxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AvyxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AvyxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("AVYXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"CRTABLE, " +
							"CRRCD, " +
							"STATCODE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     crtable,
                                     crrcd,
                                     statcode,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		crtable.clear();
  		crrcd.clear();
  		statcode.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAvyxrec() {
  		return avyxrec;
	}

	public FixedLengthStringData getAvyxpfRecord() {
  		return avyxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAvyxrec(what);
	}

	public void setAvyxrec(Object what) {
  		this.avyxrec.set(what);
	}

	public void setAvyxpfRecord(Object what) {
  		this.avyxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(avyxrec.getLength());
		result.set(avyxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}