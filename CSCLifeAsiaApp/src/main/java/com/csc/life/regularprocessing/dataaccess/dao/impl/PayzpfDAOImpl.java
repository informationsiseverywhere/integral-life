package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.PayzpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Payzpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class PayzpfDAOImpl extends BaseDAOImpl<Payzpf> implements PayzpfDAO {

	/**private static final Logger LOGGER = LoggerFactory.getLogger(PayzpfDAOImpl.class);*/
    
	@Override
	public List<Payzpf> findResult(String tableName, String memName, int batchExtractSize, int batchID) {
        StringBuilder sqlStr = new StringBuilder();
        sqlStr.append("SELECT * FROM (");
        sqlStr.append("SELECT FLOOR((ROW_NUMBER() OVER (ORDER BY TE.CHDRCOY, TE.CHDRNUM)-1)/?) ROWNM, TE.* FROM ");
        sqlStr.append(tableName);
        sqlStr.append(" TE");
        sqlStr.append(" WHERE MEMBER_NAME = ? )cc WHERE cc.ROWNM = ?");
        PreparedStatement ps = getPrepareStatement(sqlStr.toString());
		List<Payzpf> pfList = new LinkedList<>();
		ResultSet rs = null;
        try {
        	ps.setInt(1, batchExtractSize);
        	ps.setString(2, memName);
        	ps.setInt(3, batchID);
        	rs = ps.executeQuery();
            while (rs.next()) {
            	Payzpf pf = new Payzpf();
            	pf.setChdrcoy(rs.getString("CHDRCOY"));
            	pf.setChdrnum(rs.getString("CHDRNUM"));
            	pf.setPayrseqno(rs.getInt("PAYRSEQNO"));
            	pf.setBtdate(rs.getInt("BTDATE"));
            	pf.setPtdate(rs.getInt("PTDATE"));
            	pf.setBillchnl(rs.getString("BILLCHNL"));
            	pf.setZnfopt(rs.getString("ZNFOPT"));
            	pfList.add(pf);
            }

        } catch (SQLException e) {           
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return pfList;

	}

}
