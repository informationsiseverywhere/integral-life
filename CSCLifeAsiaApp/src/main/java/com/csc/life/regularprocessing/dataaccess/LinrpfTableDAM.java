package com.csc.life.regularprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: LinrpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:44
 * Class transformed from LINRPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LinrpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 15;
	public FixedLengthStringData linrrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData linrpfRecord = linrrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(linrrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(linrrec);
	public PackedDecimalData payrseqno = DD.payrseqno.copy().isAPartOf(linrrec);
	public PackedDecimalData instfrom = DD.instfrom.copy().isAPartOf(linrrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public LinrpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for LinrpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public LinrpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for LinrpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public LinrpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for LinrpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public LinrpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("LINRPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"PAYRSEQNO, " +
							"INSTFROM, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     payrseqno,
                                     instfrom,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		payrseqno.clear();
  		instfrom.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getLinrrec() {
  		return linrrec;
	}

	public FixedLengthStringData getLinrpfRecord() {
  		return linrpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setLinrrec(what);
	}

	public void setLinrrec(Object what) {
  		this.linrrec.set(what);
	}

	public void setLinrpfRecord(Object what) {
  		this.linrpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(linrrec.getLength());
		result.set(linrrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}