package com.csc.life.regularprocessing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5135.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5135Report extends SMARTReportLayout { 

	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData coverage = new FixedLengthStringData(2);
	private FixedLengthStringData crtable = new FixedLengthStringData(4);
	private FixedLengthStringData currency = new FixedLengthStringData(3);
	private FixedLengthStringData life = new FixedLengthStringData(2);
	private ZonedDecimalData newinst = new ZonedDecimalData(17, 2);
	private ZonedDecimalData newsumi = new ZonedDecimalData(15, 0);
	private ZonedDecimalData oldinst = new ZonedDecimalData(17, 2);
	private ZonedDecimalData oldsum = new ZonedDecimalData(15, 0);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0);
	private FixedLengthStringData rcesdte = new FixedLengthStringData(10);
	private FixedLengthStringData rider = new FixedLengthStringData(2);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5135Report() {
		super();
	}


	/**
	 * Print the XML for R5135d01
	 */
	public void printR5135d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		life.setFieldName("life");
		life.setInternal(subString(recordData, 9, 2));
		coverage.setFieldName("coverage");
		coverage.setInternal(subString(recordData, 11, 2));
		rider.setFieldName("rider");
		rider.setInternal(subString(recordData, 13, 2));
		plnsfx.setFieldName("plnsfx");
		plnsfx.setInternal(subString(recordData, 15, 4));
		crtable.setFieldName("crtable");
		crtable.setInternal(subString(recordData, 19, 4));
		oldsum.setFieldName("oldsum");
		oldsum.setInternal(subString(recordData, 23, 15));
		newsumi.setFieldName("newsumi");
		newsumi.setInternal(subString(recordData, 38, 15));
		oldinst.setFieldName("oldinst");
		oldinst.setInternal(subString(recordData, 53, 17));
		newinst.setFieldName("newinst");
		newinst.setInternal(subString(recordData, 70, 17));
		rcesdte.setFieldName("rcesdte");
		rcesdte.setInternal(subString(recordData, 87, 10));
		currency.setFieldName("currency");
		currency.setInternal(subString(recordData, 97, 3));
		printLayout("R5135d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				life,
				coverage,
				rider,
				plnsfx,
				crtable,
				oldsum,
				newsumi,
				oldinst,
				newinst,
				rcesdte,
				currency
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for R5135h01
	 */
	public void printR5135h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 32, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 42, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 44, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("R5135h01",			// Record name
			new BaseData[]{			// Fields:
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(10);
	}


}
