package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:59
 * Description:
 * Copybook name: INCRRGPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Incrrgpkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData incrrgpFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData incrrgpKey = new FixedLengthStringData(64).isAPartOf(incrrgpFileKey, 0, REDEFINE);
  	public FixedLengthStringData incrrgpChdrcoy = new FixedLengthStringData(1).isAPartOf(incrrgpKey, 0);
  	public FixedLengthStringData incrrgpChdrnum = new FixedLengthStringData(8).isAPartOf(incrrgpKey, 1);
  	public FixedLengthStringData incrrgpLife = new FixedLengthStringData(2).isAPartOf(incrrgpKey, 9);
  	public FixedLengthStringData incrrgpCoverage = new FixedLengthStringData(2).isAPartOf(incrrgpKey, 11);
  	public FixedLengthStringData incrrgpRider = new FixedLengthStringData(2).isAPartOf(incrrgpKey, 13);
  	public PackedDecimalData incrrgpPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(incrrgpKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(incrrgpKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(incrrgpFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		incrrgpFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}