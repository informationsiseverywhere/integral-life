package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.ZctnpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Zctnpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ZctnpfDAOImpl extends BaseDAOImpl<Zctnpf> implements ZctnpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(ZctnpfDAOImpl.class);

	@Override
	public void insertZctnpf(List<Zctnpf> zctnpfList) {
		if (zctnpfList != null && zctnpfList.size() > 0) {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO ZCTNPF(AGNTCOY,AGNTNUM,EFFDATE,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,TRANNO,TRCDE,CMAMT,PREMIUM,SPLITC,ZPRFLG,")
				.append("TRANDATE,USRPRF,JOBNM,DATIME)")
				.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			PreparedStatement ps = getPrepareStatement(sql.toString());
			try {
				for (Zctnpf c : zctnpfList) {
					ps.setString(1, c.getAgntcoy());
					ps.setString(2, c.getAgntnum());
					ps.setInt(3, c.getEffdate());
					ps.setString(4, c.getChdrcoy());
					ps.setString(5, c.getChdrnum());
					ps.setString(6, c.getLife());
					ps.setString(7, c.getCoverage());
					ps.setString(8, c.getRider());
					ps.setInt(9, c.getTranno());
					ps.setString(10, c.getTransCode());
					ps.setBigDecimal(11, c.getCommAmt());
					ps.setBigDecimal(12, c.getPremium());
					ps.setBigDecimal(13, c.getSplitBcomm());
					ps.setString(14, c.getZprflg());
					ps.setInt(15, c.getTrandate());
					ps.setString(16, getUsrprf());
					ps.setString(17, getJobnm());
					ps.setTimestamp(18,new Timestamp(System.currentTimeMillis()));
					ps.addBatch();
				}
				ps.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("insertZctnpf()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);
			}
			zctnpfList.clear();
		}

	}
	
	public List<Zctnpf> searchZctnrevRecord(String coy, String chdrnum) {
		String sql = "SELECT UNIQUE_NUMBER,AGNTCOY,AGNTNUM,EFFDATE,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,TRANNO,PREMIUM,ZPRFLG,TRANDATE FROM ZCTNREV WHERE CHDRCOY=? AND CHDRNUM=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		ResultSet rs = null;
		List<Zctnpf> searchResult = new LinkedList<>();
		try {
			ps.setString(1, coy);
			ps.setString(2, chdrnum);
			rs = executeQuery(ps);

			while (rs.next()) {
				Zctnpf t = new Zctnpf();
				t.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				t.setAgntcoy(rs.getString("agntcoy"));
				t.setAgntnum(rs.getString("agntnum"));
				t.setEffdate(rs.getInt("effdate"));
				t.setChdrcoy(rs.getString("chdrcoy"));
				t.setChdrnum(rs.getString("chdrnum"));
				t.setLife(rs.getString("life"));
				t.setCoverage(rs.getString("coverage"));
				t.setRider(rs.getString("rider"));
				t.setTranno(rs.getInt("tranno"));
				t.setPremium(rs.getBigDecimal("premium"));
				t.setZprflg(rs.getString("zprflg"));
				t.setTrandate(rs.getInt("trandate"));
				searchResult.add(t);
			}

		} catch (SQLException e) {
			LOGGER.error("searchZctnrevRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return searchResult;
	}  

}
