package com.csc.life.regularprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6658
 * @version 1.0 generated on 30/08/09 06:56
 * @author Quipoz
 */
public class S6658ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(573);
	public FixedLengthStringData dataFields = new FixedLengthStringData(125).isAPartOf(dataArea, 0);
	public FixedLengthStringData addexist = DD.addexist.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData addnew = DD.addnew.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData agemax = DD.agemax.copyToZonedDecimal().isAPartOf(dataFields,2);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,5);
	public FixedLengthStringData comind = DD.comind.copy().isAPartOf(dataFields,7);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData compoundInd = DD.compind.copy().isAPartOf(dataFields,9);
	public ZonedDecimalData fixdtrm = DD.fixdtrm.copyToZonedDecimal().isAPartOf(dataFields,10);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,13);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,21);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,29);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,37);
	public FixedLengthStringData manopt = DD.manopt.copy().isAPartOf(dataFields,67);
	public ZonedDecimalData maxpcnt = DD.maxpcnt.copyToZonedDecimal().isAPartOf(dataFields,68);
	public ZonedDecimalData maxRefusals = DD.maxrfusl.copyToZonedDecimal().isAPartOf(dataFields,73);
	public ZonedDecimalData minctrm = DD.minctrm.copyToZonedDecimal().isAPartOf(dataFields,75);
	public ZonedDecimalData minpcnt = DD.minpcnt.copyToZonedDecimal().isAPartOf(dataFields,78);
	public FixedLengthStringData nocommind = DD.nocomind.copy().isAPartOf(dataFields,83);
	public FixedLengthStringData nostatin = DD.nostatin.copy().isAPartOf(dataFields,84);
	public FixedLengthStringData optind = DD.optind.copy().isAPartOf(dataFields,85);
	public ZonedDecimalData refusalPeriod = DD.refperd.copyToZonedDecimal().isAPartOf(dataFields,86);
	public FixedLengthStringData simpleInd = DD.simpind.copy().isAPartOf(dataFields,89);
	public FixedLengthStringData statind = DD.statind.copy().isAPartOf(dataFields,90);
	public FixedLengthStringData subprog = DD.subprog.copy().isAPartOf(dataFields,91);
	public FixedLengthStringData premsubr = DD.subr.copy().isAPartOf(dataFields,101);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,109);
	public FixedLengthStringData trevsub = DD.trevsub.copy().isAPartOf(dataFields,114);
	public FixedLengthStringData incrFlg = DD.incrFlg.copy().isAPartOf(dataFields,124);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(112).isAPartOf(dataArea, 125);
	public FixedLengthStringData addexistErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData addnewErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agemaxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData comindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData compindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData fixdtrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData manoptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData maxpcntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData maxrfuslErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData minctrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData minpcntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData nocomindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData nostatinErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData optindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData refperdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData simpindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData statindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData subprogErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData subrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData trevsubErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData incrFlgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(336).isAPartOf(dataArea, 237);
	public FixedLengthStringData[] addexistOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] addnewOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agemaxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] comindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] compindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] fixdtrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] manoptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] maxpcntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] maxrfuslOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] minctrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] minpcntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] nocomindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] nostatinOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] optindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] refperdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] simpindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] statindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] subprogOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] subrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] trevsubOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] incrFlgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6658screenWritten = new LongData(0);
	public LongData S6658protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6658ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(subprogOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreqOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(simpindOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subrOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(compindOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(addnewOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(optindOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(addexistOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(manoptOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trevsubOut,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minctrmOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minpcntOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agemaxOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxpcntOut,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fixdtrmOut,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(comindOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(statindOut,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(nocomindOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(nostatinOut,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(maxrfuslOut,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(refperdOut,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(incrFlgOut,new String[] {"23",null, "-23","24", null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, subprog, billfreq, simpleInd, premsubr, compoundInd, addnew, optind, addexist, manopt, trevsub, minctrm, minpcnt, agemax, maxpcnt, fixdtrm, comind, statind, nocommind, nostatin, maxRefusals, refusalPeriod, incrFlg};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, subprogOut, billfreqOut, simpindOut, subrOut, compindOut, addnewOut, optindOut, addexistOut, manoptOut, trevsubOut, minctrmOut, minpcntOut, agemaxOut, maxpcntOut, fixdtrmOut, comindOut, statindOut, nocomindOut, nostatinOut, maxrfuslOut, refperdOut, incrFlgOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, subprogErr, billfreqErr, simpindErr, subrErr, compindErr, addnewErr, optindErr, addexistErr, manoptErr, trevsubErr, minctrmErr, minpcntErr, agemaxErr, maxpcntErr, fixdtrmErr, comindErr, statindErr, nocomindErr, nostatinErr, maxrfuslErr, refperdErr , incrFlgErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6658screen.class;
		protectRecord = S6658protect.class;
	}

}
