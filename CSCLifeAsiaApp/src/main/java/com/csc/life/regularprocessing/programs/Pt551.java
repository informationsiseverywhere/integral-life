/*
 * File: Pt551.java
 * Date: 30 August 2009 2:01:58
 * Author: Quipoz Limited
 * 
 * Class transformed from PT551.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.regularprocessing.screens.St551ScreenVars;
import com.csc.life.regularprocessing.tablestructures.Tt551rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* TT551 - EXPANDED REVERSIONARY BONUS RATE.
*
* * * * * * * * * * * *  W A R N I N G  * * * * * * * * * * * *
*
* EXTRA DATA SCREEN CONTAINS MORE THAN 500 BYTES - ( MAX SIZE OF
* ITEM-GENAREA ). - THEREFORE FIELDS HAVE BEEN CHANGED TO COMP-3.
*
* THIS GENERATED PROGRAM HAS BEEN AMENDED TO MAKE INDIVIDUAL FIELD
* MOVES TO AVOID CONVERSION OF COMP-3 FIELDS WHICH WAS CAUSING
* DECIMAL DATA ERRORS.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pt551 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PT551");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private Tt551rec tt551rec = new Tt551rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private St551ScreenVars sv = ScreenProgram.getScreenVars( St551ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1020, 
		exit3090
	}

	public Pt551() {
		super();
		screenVars = sv;
		new ScreenModel("St551", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case generalArea1020: 
					generalArea1020();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		tt551rec.tt551Rec.set(itmdIO.getItemGenarea());
		/*    IF THE EXTRA DATA AREA WAS NOT USED BEFORE THE NUMERIC*/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.*/
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1020);
		}
		wsaaSub1.set(0);
		for (int loopVar1 = 0; !(loopVar1 == 99); loopVar1 += 1){
			initInsprms1500();
		}
		wsaaSub1.set(0);
		for (int loopVar2 = 0; !(loopVar2 == 11); loopVar2 += 1){
			initInstprs1700();
		}
		/*    MOVE ZERO                   TO TT551-INSTPR                  */
		/*                                   TT551-INSPREM         <V73L03>*/
		tt551rec.insprem.set(ZERO);
		tt551rec.premUnit.set(ZERO);
		tt551rec.unit.set(ZERO);
	}

protected void generalArea1020()
	{
		wsaaSub1.set(0);
		for (int loopVar3 = 0; !(loopVar3 == 99); loopVar3 += 1){
			moveInsprms1600();
		}
		wsaaSub1.set(0);
		for (int loopVar4 = 0; !(loopVar4 == 11); loopVar4 += 1){
			moveInstprs1800();
		}
		/*    MOVE TT551-INSTPR           TO ST551-INSTPR.                 */
		sv.insprem.set(tt551rec.insprem);
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.premUnit.set(tt551rec.premUnit);
		sv.unit.set(tt551rec.unit);
	}

protected void initInsprms1500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		tt551rec.insprm[wsaaSub1.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void moveInsprms1600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		sv.insprm[wsaaSub1.toInt()].set(tt551rec.insprm[wsaaSub1.toInt()]);
		/*EXIT*/
	}

protected void initInstprs1700()
	{
		/*PARA*/
		wsaaSub1.add(1);
		tt551rec.instpr[wsaaSub1.toInt()].set(ZERO);
		/*EXIT*/
		}

protected void moveInstprs1800()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isEQ(tt551rec.instpr[wsaaSub1.toInt()], NUMERIC)) {
			sv.instpr[wsaaSub1.toInt()].set(tt551rec.instpr[wsaaSub1.toInt()]);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
					screenIo2010();
					exit2090();
				}

protected void screenIo2010()
	{
		/*    CALL 'ST551IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          ST551-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag,"I")) {
			return ;
		}
	}

	/**
	* <pre>
	*    Enter screen validation here.
	* </pre>
	*/
protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		try {
			preparation3010();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
		checkChanges3100();
		/*  ALWAYS DO UPDATE TO RELEASE HELD RECORD*/
		/*IF WSAA-UPDATE-FLAG         NOT = 'Y'*/
		/*    GO TO 3080-OTHER.*/
		itmdIO.setItemGenarea(tt551rec.tt551Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		wsaaSub1.set(0);
		for (int loopVar5 = 0; !(loopVar5 == 99); loopVar5 += 1){
			updateInsprms3500();
		}
		wsaaSub1.set(0);
		for (int loopVar6 = 0; !(loopVar6 == 11); loopVar6 += 1){
			updateInstprs3600();
		}
		/*    IF ST551-INSTPR             NOT = TT551-INSTPR               */
		/*       MOVE ST551-INSTPR        TO TT551-INSTPR                  */
		/*       MOVE 'Y' TO WSAA-UPDATE-FLAG.                             */
		if (isNE(sv.insprem,tt551rec.insprem)) {
			tt551rec.insprem.set(sv.insprem);
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
		}
		if (isNE(sv.premUnit,tt551rec.premUnit)) {
			tt551rec.premUnit.set(sv.premUnit);
		}
		if (isNE(sv.unit,tt551rec.unit)) {
			tt551rec.unit.set(sv.unit);
		}
	}

protected void updateInsprms3500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.insprm[wsaaSub1.toInt()],tt551rec.insprm[wsaaSub1.toInt()])) {
			tt551rec.insprm[wsaaSub1.toInt()].set(sv.insprm[wsaaSub1.toInt()]);
		}
		/*EXIT*/
	}

protected void updateInstprs3600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.instpr[wsaaSub1.toInt()], tt551rec.instpr[wsaaSub1.toInt()])) {
			tt551rec.instpr[wsaaSub1.toInt()].set(sv.instpr[wsaaSub1.toInt()]);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
