/*
 * File: P6283.java
 * Date: 30 August 2009 0:42:16
 * Author: Quipoz Limited
 * 
 * Class transformed from P6283.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.life.regularprocessing.recordstructures.P6283par;
import com.csc.life.regularprocessing.screens.S6283ScreenVars;
import com.csc.smart.dataaccess.BparTableDAM;
import com.csc.smart.dataaccess.BppdTableDAM;
import com.csc.smart.dataaccess.BscdTableDAM;
import com.csc.smart.dataaccess.BsscTableDAM;
import com.csc.smart.dataaccess.BupaTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*P6283 - Dishonour Batch Processing Parameter Prompt
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*Overview
*~~~~~~~~
*
*Dishonour processing subsystem has two parts, the first  part  is
*the  register  of  dishonour,  the  second  part  is  the  actual
*processing required which is carried out in batch.  This  program
*is  associated with the batch processing of the subsystem. Before
*we run the batch job, we need to know which group of  records  we
*should  be processing and that is where this program comes in. It
*is to capture the parameters required to run the batch job.
*
*
*S6283
*~~~~~
*
*The parameter prompt screen, (in addition to  the  usual  heading
*details - Ref: S2705), holds the following information:
*
*    Processing job number from:  99999999
*                            to:  99999999
*
*                Effective Date:  BBBBBBBBBB
*
*where  job  number  from  and  job number to are a 8 byte numeric
*field and the effective date is the field EFFDATE.
*
*Compile the screen with type *PARM. Create help  for  new  fields
*and screen.
*
*
*P6283
*~~~~~
*
*Add the following to the program.
*
*
*1000-INITIALISATION Section
*~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*Default S6283-JOBNO-FROM to 1.
*Default S6283-JOBNO-TO to 99999999.
*Default S6283-EFFDATE to today's date.
*
*
*2000-SCREEN-EDIT Section
*~~~~~~~~~~~~~~~~~~~~~~~~
*
*If S6283-JOBNO-FROM > S6283-JOBNO-TO
*    Redisplay screen with error message 'From must not >
*    To'.
*****************************************************************
* </pre>
*/
public class P6283 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6283");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
		/* ERRORS */
	private static final String a123 = "A123";
		/* FORMATS */
	private static final String bscdrec = "BSCDREC";
	private static final String bsscrec = "BSSCREC";
	private static final String buparec = "BUPAREC";
	private static final String bppdrec = "BPPDREC";
	private static final String bparrec = "BPARREC";
	private BparTableDAM bparIO = new BparTableDAM();
	private BppdTableDAM bppdIO = new BppdTableDAM();
	private BscdTableDAM bscdIO = new BscdTableDAM();
	private BsscTableDAM bsscIO = new BsscTableDAM();
	private BupaTableDAM bupaIO = new BupaTableDAM();
	private P6283par p6283par = new P6283par();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6283ScreenVars sv = ScreenProgram.getScreenVars( S6283ScreenVars.class);

	public P6283() {
		super();
		screenVars = sv;
		new ScreenModel("S6283", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/* Retrieve Schedule.*/
		bsscIO.setFormat(bsscrec);
		bsscIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bsscIO);
		if (isNE(bsscIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bsscIO.getParams());
			fatalError600();
		}
		/* Retrieve Schedule Definition.*/
		bscdIO.setFormat(bscdrec);
		bscdIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bscdIO);
		if (isNE(bscdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bscdIO.getParams());
			fatalError600();
		}
		/* Retrieve Parameter Prompt Definition.*/
		bppdIO.setFormat(bppdrec);
		bppdIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bppdIO);
		if (isNE(bppdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bppdIO.getParams());
			fatalError600();
		}
		sv.dataArea.set(SPACES);
		sv.jobnofrom.set(ZERO);
		sv.jobnoto.set(ZERO);
		sv.scheduleName.set(bsscIO.getScheduleName());
		sv.scheduleNumber.set(bsscIO.getScheduleNumber());
		sv.effdate.set(bsscIO.getEffectiveDate());
		sv.acctmonth.set(bsscIO.getAcctMonth());
		sv.acctyear.set(bsscIO.getAcctYear());
		sv.jobq.set(bscdIO.getJobq());
		sv.bcompany.set(bppdIO.getCompany());
		sv.bbranch.set(bsscIO.getInitBranch());
		if (isNE(wsspcomn.flag, "I")
		&& isNE(wsspcomn.flag, "M")) {
			return ;
		}
		bparIO.setParams(SPACES);
		bparIO.setScheduleName(bsscIO.getScheduleName());
		bparIO.setCompany(bppdIO.getCompany());
		bparIO.setParmPromptProg(wsaaProg);
		bparIO.setBruntype(subString(wsspcomn.inqkey, 1, 8));
		bparIO.setBrunoccur(subString(wsspcomn.inqkey, 9, 3));
		bparIO.setEffectiveDate(bsscIO.getEffectiveDate());
		bparIO.setAcctmonth(bsscIO.getAcctMonth());
		bparIO.setAcctyear(bsscIO.getAcctYear());
		bparIO.setFunction(varcom.readr);
		bparIO.setFormat(bparrec);
		SmartFileCode.execute(appVars, bparIO);
		if (isNE(bparIO.getStatuz(), varcom.oK)
		&& isNE(bparIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(bparIO.getStatuz());
			syserrrec.params.set(bparIO.getParams());
			fatalError600();
		}
		if (isEQ(bparIO.getStatuz(), varcom.mrnf)) {
			scrnparams.errorCode.set(a123);
			return ;
		}
		p6283par.parmRecord.set(bparIO.getParmarea());
		sv.effdates.set(p6283par.effdates);
		sv.jobnofrom.set(p6283par.jobnofrom);
		sv.jobnoto.set(p6283par.jobnoto);
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		screenIo2010();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/*VALIDATE*/
		/**    Validate fields*/
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*LOAD-FIELDS*/
		moveParameters3100();
		writeParamRecord3200();
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATING SECTIONS
	* </pre>
	*/
protected void moveParameters3100()
	{
		/*MOVE-TO-PARM-RECORD*/
		p6283par.effdates.set(sv.effdates);
		p6283par.jobnofrom.set(sv.jobnofrom);
		p6283par.jobnoto.set(sv.jobnoto);
		/*EXIT*/
	}

protected void writeParamRecord3200()
	{
		writBupa3210();
	}

protected void writBupa3210()
	{
		bupaIO.setParams(SPACES);
		bupaIO.setScheduleName(sv.scheduleName);
		bupaIO.setScheduleNumber(sv.scheduleNumber);
		bupaIO.setEffectiveDate(sv.effdate);
		bupaIO.setAcctMonth(sv.acctmonth);
		bupaIO.setAcctYear(sv.acctyear);
		bupaIO.setCompany(sv.bcompany);
		bupaIO.setBranch(sv.bbranch);
		bupaIO.setParmarea(p6283par.parmRecord);
		bupaIO.setParmPromptProg(bppdIO.getParmPromptProg());
		bupaIO.setFormat(buparec);
		bupaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, bupaIO);
		if (isNE(bupaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bupaIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
