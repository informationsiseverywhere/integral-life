package com.csc.life.regularprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6660screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 17, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6660ScreenVars sv = (S6660ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6660screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6660ScreenVars screenVars = (S6660ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.cntcurr04.setClassString("");
		screenVars.cntcurr05.setClassString("");
		screenVars.cntcurr06.setClassString("");
		screenVars.cntcurr07.setClassString("");
		screenVars.cntcurr08.setClassString("");
		screenVars.cntcurr09.setClassString("");
		screenVars.cntcurr01.setClassString("");
		screenVars.cntcurr02.setClassString("");
		screenVars.cntcurr03.setClassString("");
		screenVars.acctccy01.setClassString("");
		screenVars.acctccy02.setClassString("");
		screenVars.acctccy03.setClassString("");
		screenVars.acctccy04.setClassString("");
		screenVars.acctccy05.setClassString("");
		screenVars.acctccy06.setClassString("");
		screenVars.acctccy07.setClassString("");
		screenVars.acctccy08.setClassString("");
		screenVars.acctccy09.setClassString("");
		screenVars.acctccy10.setClassString("");
		screenVars.acctccy11.setClassString("");
		screenVars.acctccy12.setClassString("");
		screenVars.acctccy13.setClassString("");
		screenVars.acctccy14.setClassString("");
		screenVars.acctccy15.setClassString("");
		screenVars.acctccy16.setClassString("");
		screenVars.acctccy17.setClassString("");
		screenVars.acctccy18.setClassString("");
		screenVars.acctccy19.setClassString("");
		screenVars.acctccy20.setClassString("");
		screenVars.acctccy21.setClassString("");
		screenVars.acctccy22.setClassString("");
		screenVars.acctccy23.setClassString("");
		screenVars.acctccy24.setClassString("");
		screenVars.acctccy25.setClassString("");
		screenVars.acctccy26.setClassString("");
		screenVars.acctccy27.setClassString("");
		screenVars.acctccy28.setClassString("");
		screenVars.acctccy29.setClassString("");
		screenVars.acctccy30.setClassString("");
		screenVars.acctccy31.setClassString("");
		screenVars.acctccy32.setClassString("");
		screenVars.acctccy33.setClassString("");
		screenVars.acctccy34.setClassString("");
		screenVars.acctccy35.setClassString("");
		screenVars.acctccy36.setClassString("");
		screenVars.acctccy37.setClassString("");
		screenVars.acctccy38.setClassString("");
		screenVars.acctccy39.setClassString("");
		screenVars.acctccy40.setClassString("");
		screenVars.acctccy41.setClassString("");
		screenVars.acctccy42.setClassString("");
		screenVars.acctccy43.setClassString("");
		screenVars.acctccy44.setClassString("");
		screenVars.acctccy45.setClassString("");
		screenVars.acctccy46.setClassString("");
		screenVars.acctccy47.setClassString("");
		screenVars.acctccy48.setClassString("");
		screenVars.acctccy49.setClassString("");
		screenVars.acctccy50.setClassString("");
		screenVars.acctccy51.setClassString("");
		screenVars.acctccy52.setClassString("");
		screenVars.acctccy53.setClassString("");
		screenVars.acctccy54.setClassString("");
		screenVars.acctccy55.setClassString("");
		screenVars.acctccy56.setClassString("");
		screenVars.acctccy57.setClassString("");
		screenVars.acctccy58.setClassString("");
		screenVars.acctccy59.setClassString("");
		screenVars.acctccy60.setClassString("");
		screenVars.acctccy61.setClassString("");
		screenVars.acctccy62.setClassString("");
		screenVars.acctccy63.setClassString("");
		screenVars.acctccy64.setClassString("");
		screenVars.acctccy65.setClassString("");
		screenVars.acctccy66.setClassString("");
		screenVars.acctccy67.setClassString("");
		screenVars.acctccy68.setClassString("");
		screenVars.acctccy69.setClassString("");
		screenVars.acctccy70.setClassString("");
		screenVars.acctccy71.setClassString("");
		screenVars.acctccy72.setClassString("");
		screenVars.acctccy73.setClassString("");
		screenVars.acctccy74.setClassString("");
		screenVars.acctccy75.setClassString("");
		screenVars.acctccy76.setClassString("");
		screenVars.acctccy77.setClassString("");
		screenVars.acctccy78.setClassString("");
		screenVars.acctccy79.setClassString("");
		screenVars.acctccy80.setClassString("");
		screenVars.acctccy81.setClassString("");
		screenVars.acctccy82.setClassString("");
		screenVars.acctccy83.setClassString("");
		screenVars.acctccy84.setClassString("");
		screenVars.acctccy85.setClassString("");
		screenVars.acctccy86.setClassString("");
		screenVars.acctccy87.setClassString("");
		screenVars.acctccy88.setClassString("");
		screenVars.acctccy89.setClassString("");
		screenVars.acctccy90.setClassString("");
	}

/**
 * Clear all the variables in S6660screen
 */
	public static void clear(VarModel pv) {
		S6660ScreenVars screenVars = (S6660ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.cntcurr04.clear();
		screenVars.cntcurr05.clear();
		screenVars.cntcurr06.clear();
		screenVars.cntcurr07.clear();
		screenVars.cntcurr08.clear();
		screenVars.cntcurr09.clear();
		screenVars.cntcurr01.clear();
		screenVars.cntcurr02.clear();
		screenVars.cntcurr03.clear();
		screenVars.acctccy01.clear();
		screenVars.acctccy02.clear();
		screenVars.acctccy03.clear();
		screenVars.acctccy04.clear();
		screenVars.acctccy05.clear();
		screenVars.acctccy06.clear();
		screenVars.acctccy07.clear();
		screenVars.acctccy08.clear();
		screenVars.acctccy09.clear();
		screenVars.acctccy10.clear();
		screenVars.acctccy11.clear();
		screenVars.acctccy12.clear();
		screenVars.acctccy13.clear();
		screenVars.acctccy14.clear();
		screenVars.acctccy15.clear();
		screenVars.acctccy16.clear();
		screenVars.acctccy17.clear();
		screenVars.acctccy18.clear();
		screenVars.acctccy19.clear();
		screenVars.acctccy20.clear();
		screenVars.acctccy21.clear();
		screenVars.acctccy22.clear();
		screenVars.acctccy23.clear();
		screenVars.acctccy24.clear();
		screenVars.acctccy25.clear();
		screenVars.acctccy26.clear();
		screenVars.acctccy27.clear();
		screenVars.acctccy28.clear();
		screenVars.acctccy29.clear();
		screenVars.acctccy30.clear();
		screenVars.acctccy31.clear();
		screenVars.acctccy32.clear();
		screenVars.acctccy33.clear();
		screenVars.acctccy34.clear();
		screenVars.acctccy35.clear();
		screenVars.acctccy36.clear();
		screenVars.acctccy37.clear();
		screenVars.acctccy38.clear();
		screenVars.acctccy39.clear();
		screenVars.acctccy40.clear();
		screenVars.acctccy41.clear();
		screenVars.acctccy42.clear();
		screenVars.acctccy43.clear();
		screenVars.acctccy44.clear();
		screenVars.acctccy45.clear();
		screenVars.acctccy46.clear();
		screenVars.acctccy47.clear();
		screenVars.acctccy48.clear();
		screenVars.acctccy49.clear();
		screenVars.acctccy50.clear();
		screenVars.acctccy51.clear();
		screenVars.acctccy52.clear();
		screenVars.acctccy53.clear();
		screenVars.acctccy54.clear();
		screenVars.acctccy55.clear();
		screenVars.acctccy56.clear();
		screenVars.acctccy57.clear();
		screenVars.acctccy58.clear();
		screenVars.acctccy59.clear();
		screenVars.acctccy60.clear();
		screenVars.acctccy61.clear();
		screenVars.acctccy62.clear();
		screenVars.acctccy63.clear();
		screenVars.acctccy64.clear();
		screenVars.acctccy65.clear();
		screenVars.acctccy66.clear();
		screenVars.acctccy67.clear();
		screenVars.acctccy68.clear();
		screenVars.acctccy69.clear();
		screenVars.acctccy70.clear();
		screenVars.acctccy71.clear();
		screenVars.acctccy72.clear();
		screenVars.acctccy73.clear();
		screenVars.acctccy74.clear();
		screenVars.acctccy75.clear();
		screenVars.acctccy76.clear();
		screenVars.acctccy77.clear();
		screenVars.acctccy78.clear();
		screenVars.acctccy79.clear();
		screenVars.acctccy80.clear();
		screenVars.acctccy81.clear();
		screenVars.acctccy82.clear();
		screenVars.acctccy83.clear();
		screenVars.acctccy84.clear();
		screenVars.acctccy85.clear();
		screenVars.acctccy86.clear();
		screenVars.acctccy87.clear();
		screenVars.acctccy88.clear();
		screenVars.acctccy89.clear();
		screenVars.acctccy90.clear();
	}
}
