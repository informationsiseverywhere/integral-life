package com.csc.life.regularprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:42
 * Description:
 * Copybook name: T6639REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6639rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6639Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData addBonusProg = new FixedLengthStringData(8).isAPartOf(t6639Rec, 0);
  	public FixedLengthStringData intBonusProg = new FixedLengthStringData(8).isAPartOf(t6639Rec, 8);
  	public FixedLengthStringData revBonusProg = new FixedLengthStringData(8).isAPartOf(t6639Rec, 16);
  	public FixedLengthStringData trmBonusProg = new FixedLengthStringData(8).isAPartOf(t6639Rec, 24);
  	public FixedLengthStringData filler = new FixedLengthStringData(468).isAPartOf(t6639Rec, 32, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6639Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6639Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}