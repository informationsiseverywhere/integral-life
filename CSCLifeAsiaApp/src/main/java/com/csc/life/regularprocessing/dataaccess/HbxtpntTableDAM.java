package com.csc.life.regularprocessing.dataaccess;

import com.csc.fsu.general.dataaccess.BextpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HbxtpntTableDAM.java
 * Date: Sun, 30 Aug 2009 03:40:07
 * Class transformed from HBXTPNT.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HbxtpntTableDAM extends BextpfTableDAM {

	public HbxtpntTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("HBXTPNT");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", INSTFROM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRPFX, " +
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "SERVUNIT, " +
		            "CNTTYPE, " +
		            "CNTCURR, " +
		            "OCCDATE, " +
		            "CCDATE, " +
		            "PTDATE, " +
		            "BTDATE, " +
		            "BILLDATE, " +
		            "BILLCHNL, " +
		            "BANKCODE, " +
		            "INSTFROM, " +
		            "INSTTO, " +
		            "INSTBCHNL, " +
		            "INSTCCHNL, " +
		            "INSTFREQ, " +
		            "INSTAMT01, " +
		            "INSTAMT02, " +
		            "INSTAMT03, " +
		            "INSTAMT04, " +
		            "INSTAMT05, " +
		            "INSTAMT06, " +
		            "INSTAMT07, " +
		            "INSTAMT08, " +
		            "INSTAMT09, " +
		            "INSTAMT10, " +
		            "INSTJCTL, " +
		            "GRUPKEY, " +
		            "MEMBSEL, " +
		            "FACTHOUS, " +
		            "BANKKEY, " +
		            "BANKACCKEY, " +
		            "COWNPFX, " +
		            "COWNCOY, " +
		            "COWNNUM, " +
		            "PAYRPFX, " +
		            "PAYRCOY, " +
		            "PAYRNUM, " +
		            "CNTBRANCH, " +
		            "AGNTPFX, " +
		            "AGNTCOY, " +
		            "AGNTNUM, " +
		            "PAYFLAG, " +
		            "BILFLAG, " +
		            "OUTFLAG, " +
		            "SUPFLAG, " +
		            "BILLCD, " +
		            "MANDREF, " +
		            "SACSCODE, " +
		            "SACSTYP, " +
		            "GLMAP, " +
		            "MANDSTAT, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "INSTFROM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "INSTFROM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrpfx,
                               chdrcoy,
                               chdrnum,
                               servunit,
                               cnttype,
                               cntcurr,
                               occdate,
                               ccdate,
                               ptdate,
                               btdate,
                               billdate,
                               billchnl,
                               bankcode,
                               instfrom,
                               instto,
                               instbchnl,
                               instcchnl,
                               instfreq,
                               instamt01,
                               instamt02,
                               instamt03,
                               instamt04,
                               instamt05,
                               instamt06,
                               instamt07,
                               instamt08,
                               instamt09,
                               instamt10,
                               instjctl,
                               grupkey,
                               membsel,
                               facthous,
                               bankkey,
                               bankacckey,
                               cownpfx,
                               cowncoy,
                               cownnum,
                               payrpfx,
                               payrcoy,
                               payrnum,
                               cntbranch,
                               agntpfx,
                               agntcoy,
                               agntnum,
                               payflag,
                               bilflag,
                               outflag,
                               supflag,
                               billcd,
                               mandref,
                               sacscode,
                               sacstyp,
                               glmap,
                               mandstat,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(242);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getInstfrom().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, instfrom);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller140 = new FixedLengthStringData(5);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller20.setInternal(chdrcoy.toInternal());
	nonKeyFiller30.setInternal(chdrnum.toInternal());
	nonKeyFiller140.setInternal(instfrom.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(347);
		
		nonKeyData.set(
					getChdrpfx().toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ getServunit().toInternal()
					+ getCnttype().toInternal()
					+ getCntcurr().toInternal()
					+ getOccdate().toInternal()
					+ getCcdate().toInternal()
					+ getPtdate().toInternal()
					+ getBtdate().toInternal()
					+ getBilldate().toInternal()
					+ getBillchnl().toInternal()
					+ getBankcode().toInternal()
					+ nonKeyFiller140.toInternal()
					+ getInstto().toInternal()
					+ getInstbchnl().toInternal()
					+ getInstcchnl().toInternal()
					+ getInstfreq().toInternal()
					+ getInstamt01().toInternal()
					+ getInstamt02().toInternal()
					+ getInstamt03().toInternal()
					+ getInstamt04().toInternal()
					+ getInstamt05().toInternal()
					+ getInstamt06().toInternal()
					+ getInstamt07().toInternal()
					+ getInstamt08().toInternal()
					+ getInstamt09().toInternal()
					+ getInstamt10().toInternal()
					+ getInstjctl().toInternal()
					+ getGrupkey().toInternal()
					+ getMembsel().toInternal()
					+ getFacthous().toInternal()
					+ getBankkey().toInternal()
					+ getBankacckey().toInternal()
					+ getCownpfx().toInternal()
					+ getCowncoy().toInternal()
					+ getCownnum().toInternal()
					+ getPayrpfx().toInternal()
					+ getPayrcoy().toInternal()
					+ getPayrnum().toInternal()
					+ getCntbranch().toInternal()
					+ getAgntpfx().toInternal()
					+ getAgntcoy().toInternal()
					+ getAgntnum().toInternal()
					+ getPayflag().toInternal()
					+ getBilflag().toInternal()
					+ getOutflag().toInternal()
					+ getSupflag().toInternal()
					+ getBillcd().toInternal()
					+ getMandref().toInternal()
					+ getSacscode().toInternal()
					+ getSacstyp().toInternal()
					+ getGlmap().toInternal()
					+ getMandstat().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrpfx);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, servunit);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, occdate);
			what = ExternalData.chop(what, ccdate);
			what = ExternalData.chop(what, ptdate);
			what = ExternalData.chop(what, btdate);
			what = ExternalData.chop(what, billdate);
			what = ExternalData.chop(what, billchnl);
			what = ExternalData.chop(what, bankcode);
			what = ExternalData.chop(what, nonKeyFiller140);
			what = ExternalData.chop(what, instto);
			what = ExternalData.chop(what, instbchnl);
			what = ExternalData.chop(what, instcchnl);
			what = ExternalData.chop(what, instfreq);
			what = ExternalData.chop(what, instamt01);
			what = ExternalData.chop(what, instamt02);
			what = ExternalData.chop(what, instamt03);
			what = ExternalData.chop(what, instamt04);
			what = ExternalData.chop(what, instamt05);
			what = ExternalData.chop(what, instamt06);
			what = ExternalData.chop(what, instamt07);
			what = ExternalData.chop(what, instamt08);
			what = ExternalData.chop(what, instamt09);
			what = ExternalData.chop(what, instamt10);
			what = ExternalData.chop(what, instjctl);
			what = ExternalData.chop(what, grupkey);
			what = ExternalData.chop(what, membsel);
			what = ExternalData.chop(what, facthous);
			what = ExternalData.chop(what, bankkey);
			what = ExternalData.chop(what, bankacckey);
			what = ExternalData.chop(what, cownpfx);
			what = ExternalData.chop(what, cowncoy);
			what = ExternalData.chop(what, cownnum);
			what = ExternalData.chop(what, payrpfx);
			what = ExternalData.chop(what, payrcoy);
			what = ExternalData.chop(what, payrnum);
			what = ExternalData.chop(what, cntbranch);
			what = ExternalData.chop(what, agntpfx);
			what = ExternalData.chop(what, agntcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, payflag);
			what = ExternalData.chop(what, bilflag);
			what = ExternalData.chop(what, outflag);
			what = ExternalData.chop(what, supflag);
			what = ExternalData.chop(what, billcd);
			what = ExternalData.chop(what, mandref);
			what = ExternalData.chop(what, sacscode);
			what = ExternalData.chop(what, sacstyp);
			what = ExternalData.chop(what, glmap);
			what = ExternalData.chop(what, mandstat);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getInstfrom() {
		return instfrom;
	}
	public void setInstfrom(Object what) {
		setInstfrom(what, false);
	}
	public void setInstfrom(Object what, boolean rounded) {
		if (rounded)
			instfrom.setRounded(what);
		else
			instfrom.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(Object what) {
		chdrpfx.set(what);
	}	
	public FixedLengthStringData getServunit() {
		return servunit;
	}
	public void setServunit(Object what) {
		servunit.set(what);
	}	
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public PackedDecimalData getOccdate() {
		return occdate;
	}
	public void setOccdate(Object what) {
		setOccdate(what, false);
	}
	public void setOccdate(Object what, boolean rounded) {
		if (rounded)
			occdate.setRounded(what);
		else
			occdate.set(what);
	}	
	public PackedDecimalData getCcdate() {
		return ccdate;
	}
	public void setCcdate(Object what) {
		setCcdate(what, false);
	}
	public void setCcdate(Object what, boolean rounded) {
		if (rounded)
			ccdate.setRounded(what);
		else
			ccdate.set(what);
	}	
	public PackedDecimalData getPtdate() {
		return ptdate;
	}
	public void setPtdate(Object what) {
		setPtdate(what, false);
	}
	public void setPtdate(Object what, boolean rounded) {
		if (rounded)
			ptdate.setRounded(what);
		else
			ptdate.set(what);
	}	
	public PackedDecimalData getBtdate() {
		return btdate;
	}
	public void setBtdate(Object what) {
		setBtdate(what, false);
	}
	public void setBtdate(Object what, boolean rounded) {
		if (rounded)
			btdate.setRounded(what);
		else
			btdate.set(what);
	}	
	public PackedDecimalData getBilldate() {
		return billdate;
	}
	public void setBilldate(Object what) {
		setBilldate(what, false);
	}
	public void setBilldate(Object what, boolean rounded) {
		if (rounded)
			billdate.setRounded(what);
		else
			billdate.set(what);
	}	
	public FixedLengthStringData getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(Object what) {
		billchnl.set(what);
	}	
	public FixedLengthStringData getBankcode() {
		return bankcode;
	}
	public void setBankcode(Object what) {
		bankcode.set(what);
	}	
	public PackedDecimalData getInstto() {
		return instto;
	}
	public void setInstto(Object what) {
		setInstto(what, false);
	}
	public void setInstto(Object what, boolean rounded) {
		if (rounded)
			instto.setRounded(what);
		else
			instto.set(what);
	}	
	public FixedLengthStringData getInstbchnl() {
		return instbchnl;
	}
	public void setInstbchnl(Object what) {
		instbchnl.set(what);
	}	
	public FixedLengthStringData getInstcchnl() {
		return instcchnl;
	}
	public void setInstcchnl(Object what) {
		instcchnl.set(what);
	}	
	public FixedLengthStringData getInstfreq() {
		return instfreq;
	}
	public void setInstfreq(Object what) {
		instfreq.set(what);
	}	
	public PackedDecimalData getInstamt01() {
		return instamt01;
	}
	public void setInstamt01(Object what) {
		setInstamt01(what, false);
	}
	public void setInstamt01(Object what, boolean rounded) {
		if (rounded)
			instamt01.setRounded(what);
		else
			instamt01.set(what);
	}	
	public PackedDecimalData getInstamt02() {
		return instamt02;
	}
	public void setInstamt02(Object what) {
		setInstamt02(what, false);
	}
	public void setInstamt02(Object what, boolean rounded) {
		if (rounded)
			instamt02.setRounded(what);
		else
			instamt02.set(what);
	}	
	public PackedDecimalData getInstamt03() {
		return instamt03;
	}
	public void setInstamt03(Object what) {
		setInstamt03(what, false);
	}
	public void setInstamt03(Object what, boolean rounded) {
		if (rounded)
			instamt03.setRounded(what);
		else
			instamt03.set(what);
	}	
	public PackedDecimalData getInstamt04() {
		return instamt04;
	}
	public void setInstamt04(Object what) {
		setInstamt04(what, false);
	}
	public void setInstamt04(Object what, boolean rounded) {
		if (rounded)
			instamt04.setRounded(what);
		else
			instamt04.set(what);
	}	
	public PackedDecimalData getInstamt05() {
		return instamt05;
	}
	public void setInstamt05(Object what) {
		setInstamt05(what, false);
	}
	public void setInstamt05(Object what, boolean rounded) {
		if (rounded)
			instamt05.setRounded(what);
		else
			instamt05.set(what);
	}	
	public PackedDecimalData getInstamt06() {
		return instamt06;
	}
	public void setInstamt06(Object what) {
		setInstamt06(what, false);
	}
	public void setInstamt06(Object what, boolean rounded) {
		if (rounded)
			instamt06.setRounded(what);
		else
			instamt06.set(what);
	}	
	public PackedDecimalData getInstamt07() {
		return instamt07;
	}
	public void setInstamt07(Object what) {
		setInstamt07(what, false);
	}
	public void setInstamt07(Object what, boolean rounded) {
		if (rounded)
			instamt07.setRounded(what);
		else
			instamt07.set(what);
	}	
	public PackedDecimalData getInstamt08() {
		return instamt08;
	}
	public void setInstamt08(Object what) {
		setInstamt08(what, false);
	}
	public void setInstamt08(Object what, boolean rounded) {
		if (rounded)
			instamt08.setRounded(what);
		else
			instamt08.set(what);
	}	
	public PackedDecimalData getInstamt09() {
		return instamt09;
	}
	public void setInstamt09(Object what) {
		setInstamt09(what, false);
	}
	public void setInstamt09(Object what, boolean rounded) {
		if (rounded)
			instamt09.setRounded(what);
		else
			instamt09.set(what);
	}	
	public PackedDecimalData getInstamt10() {
		return instamt10;
	}
	public void setInstamt10(Object what) {
		setInstamt10(what, false);
	}
	public void setInstamt10(Object what, boolean rounded) {
		if (rounded)
			instamt10.setRounded(what);
		else
			instamt10.set(what);
	}	
	public FixedLengthStringData getInstjctl() {
		return instjctl;
	}
	public void setInstjctl(Object what) {
		instjctl.set(what);
	}	
	public FixedLengthStringData getGrupkey() {
		return grupkey;
	}
	public void setGrupkey(Object what) {
		grupkey.set(what);
	}	
	public FixedLengthStringData getMembsel() {
		return membsel;
	}
	public void setMembsel(Object what) {
		membsel.set(what);
	}	
	public FixedLengthStringData getFacthous() {
		return facthous;
	}
	public void setFacthous(Object what) {
		facthous.set(what);
	}	
	public FixedLengthStringData getBankkey() {
		return bankkey;
	}
	public void setBankkey(Object what) {
		bankkey.set(what);
	}	
	public FixedLengthStringData getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(Object what) {
		bankacckey.set(what);
	}	
	public FixedLengthStringData getCownpfx() {
		return cownpfx;
	}
	public void setCownpfx(Object what) {
		cownpfx.set(what);
	}	
	public FixedLengthStringData getCowncoy() {
		return cowncoy;
	}
	public void setCowncoy(Object what) {
		cowncoy.set(what);
	}	
	public FixedLengthStringData getCownnum() {
		return cownnum;
	}
	public void setCownnum(Object what) {
		cownnum.set(what);
	}	
	public FixedLengthStringData getPayrpfx() {
		return payrpfx;
	}
	public void setPayrpfx(Object what) {
		payrpfx.set(what);
	}	
	public FixedLengthStringData getPayrcoy() {
		return payrcoy;
	}
	public void setPayrcoy(Object what) {
		payrcoy.set(what);
	}	
	public FixedLengthStringData getPayrnum() {
		return payrnum;
	}
	public void setPayrnum(Object what) {
		payrnum.set(what);
	}	
	public FixedLengthStringData getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(Object what) {
		cntbranch.set(what);
	}	
	public FixedLengthStringData getAgntpfx() {
		return agntpfx;
	}
	public void setAgntpfx(Object what) {
		agntpfx.set(what);
	}	
	public FixedLengthStringData getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(Object what) {
		agntcoy.set(what);
	}	
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}	
	public FixedLengthStringData getPayflag() {
		return payflag;
	}
	public void setPayflag(Object what) {
		payflag.set(what);
	}	
	public FixedLengthStringData getBilflag() {
		return bilflag;
	}
	public void setBilflag(Object what) {
		bilflag.set(what);
	}	
	public FixedLengthStringData getOutflag() {
		return outflag;
	}
	public void setOutflag(Object what) {
		outflag.set(what);
	}	
	public FixedLengthStringData getSupflag() {
		return supflag;
	}
	public void setSupflag(Object what) {
		supflag.set(what);
	}	
	public PackedDecimalData getBillcd() {
		return billcd;
	}
	public void setBillcd(Object what) {
		setBillcd(what, false);
	}
	public void setBillcd(Object what, boolean rounded) {
		if (rounded)
			billcd.setRounded(what);
		else
			billcd.set(what);
	}	
	public FixedLengthStringData getMandref() {
		return mandref;
	}
	public void setMandref(Object what) {
		mandref.set(what);
	}	
	public FixedLengthStringData getSacscode() {
		return sacscode;
	}
	public void setSacscode(Object what) {
		sacscode.set(what);
	}	
	public FixedLengthStringData getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(Object what) {
		sacstyp.set(what);
	}	
	public FixedLengthStringData getGlmap() {
		return glmap;
	}
	public void setGlmap(Object what) {
		glmap.set(what);
	}	
	public FixedLengthStringData getMandstat() {
		return mandstat;
	}
	public void setMandstat(Object what) {
		mandstat.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getInstamts() {
		return new FixedLengthStringData(instamt01.toInternal()
										+ instamt02.toInternal()
										+ instamt03.toInternal()
										+ instamt04.toInternal()
										+ instamt05.toInternal()
										+ instamt06.toInternal()
										+ instamt07.toInternal()
										+ instamt08.toInternal()
										+ instamt09.toInternal()
										+ instamt10.toInternal());
	}
	public void setInstamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getInstamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, instamt01);
		what = ExternalData.chop(what, instamt02);
		what = ExternalData.chop(what, instamt03);
		what = ExternalData.chop(what, instamt04);
		what = ExternalData.chop(what, instamt05);
		what = ExternalData.chop(what, instamt06);
		what = ExternalData.chop(what, instamt07);
		what = ExternalData.chop(what, instamt08);
		what = ExternalData.chop(what, instamt09);
		what = ExternalData.chop(what, instamt10);
	}
	public PackedDecimalData getInstamt(BaseData indx) {
		return getInstamt(indx.toInt());
	}
	public PackedDecimalData getInstamt(int indx) {

		switch (indx) {
			case 1 : return instamt01;
			case 2 : return instamt02;
			case 3 : return instamt03;
			case 4 : return instamt04;
			case 5 : return instamt05;
			case 6 : return instamt06;
			case 7 : return instamt07;
			case 8 : return instamt08;
			case 9 : return instamt09;
			case 10 : return instamt10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setInstamt(BaseData indx, Object what) {
		setInstamt(indx, what, false);
	}
	public void setInstamt(BaseData indx, Object what, boolean rounded) {
		setInstamt(indx.toInt(), what, rounded);
	}
	public void setInstamt(int indx, Object what) {
		setInstamt(indx, what, false);
	}
	public void setInstamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInstamt01(what, rounded);
					 break;
			case 2 : setInstamt02(what, rounded);
					 break;
			case 3 : setInstamt03(what, rounded);
					 break;
			case 4 : setInstamt04(what, rounded);
					 break;
			case 5 : setInstamt05(what, rounded);
					 break;
			case 6 : setInstamt06(what, rounded);
					 break;
			case 7 : setInstamt07(what, rounded);
					 break;
			case 8 : setInstamt08(what, rounded);
					 break;
			case 9 : setInstamt09(what, rounded);
					 break;
			case 10 : setInstamt10(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		instfrom.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		chdrpfx.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		servunit.clear();
		cnttype.clear();
		cntcurr.clear();
		occdate.clear();
		ccdate.clear();
		ptdate.clear();
		btdate.clear();
		billdate.clear();
		billchnl.clear();
		bankcode.clear();
		nonKeyFiller140.clear();
		instto.clear();
		instbchnl.clear();
		instcchnl.clear();
		instfreq.clear();
		instamt01.clear();
		instamt02.clear();
		instamt03.clear();
		instamt04.clear();
		instamt05.clear();
		instamt06.clear();
		instamt07.clear();
		instamt08.clear();
		instamt09.clear();
		instamt10.clear();
		instjctl.clear();
		grupkey.clear();
		membsel.clear();
		facthous.clear();
		bankkey.clear();
		bankacckey.clear();
		cownpfx.clear();
		cowncoy.clear();
		cownnum.clear();
		payrpfx.clear();
		payrcoy.clear();
		payrnum.clear();
		cntbranch.clear();
		agntpfx.clear();
		agntcoy.clear();
		agntnum.clear();
		payflag.clear();
		bilflag.clear();
		outflag.clear();
		supflag.clear();
		billcd.clear();
		mandref.clear();
		sacscode.clear();
		sacstyp.clear();
		glmap.clear();
		mandstat.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}