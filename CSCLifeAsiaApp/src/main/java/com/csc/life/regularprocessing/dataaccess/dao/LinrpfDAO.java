package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.Linrpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface LinrpfDAO extends BaseDAO<Linrpf> {
	public List<Linrpf> searchLinrpfRecord(String tableId, String memName, int batchExtractSize, int batchID);
}