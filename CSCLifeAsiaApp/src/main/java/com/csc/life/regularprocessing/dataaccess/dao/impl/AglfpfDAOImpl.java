package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DataDictionaryAtoC;
import com.csc.life.regularprocessing.dataaccess.dao.AglfpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Aglfpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AglfpfDAOImpl extends BaseDAOImpl<Aglfpf> implements AglfpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(AglfpfDAOImpl.class);

	public Map<String, Aglfpf> searchAglfRecord(String coy, List<String> agntnumList) {
		StringBuilder sqlAglfSelect1 = new StringBuilder("SELECT * FROM AGLFPF WHERE AGNTCOY=? AND ");
		sqlAglfSelect1.append(getSqlInStr("AGNTNUM", agntnumList));
		sqlAglfSelect1.append(" ORDER BY AGNTCOY ASC, AGNTNUM ASC, UNIQUE_NUMBER DESC");
		PreparedStatement psAglfSelect = getPrepareStatement(sqlAglfSelect1.toString());
		ResultSet rs = null;
		Map<String, Aglfpf> aglfpfMap = new HashMap<String, Aglfpf>();
		try {
			psAglfSelect.setInt(1, Integer.parseInt(coy));
			rs = executeQuery(psAglfSelect);

			while (rs.next()) {
				Aglfpf aglfpf = new Aglfpf();
				aglfpf.setAgntcoy(rs.getString("AGNTCOY"));
				aglfpf.setAgntnum(rs.getString("AGNTNUM"));
				aglfpf.setTermid(rs.getString("TERMID"));
				aglfpf.setTransactionDate(rs.getInt("TRDT"));
				aglfpf.setTransactionTime(rs.getInt("TRTM"));
				aglfpf.setUser(rs.getInt("USER_T"));
				aglfpf.setCurrfrom(rs.getInt("CURRFROM"));
				aglfpf.setCurrto(rs.getInt("CURRTO"));
				aglfpf.setDteapp(rs.getInt("DTEAPP"));
				aglfpf.setDtetrm(rs.getInt("DTETRM"));
				aglfpf.setTrmcde(rs.getString("TRMCDE"));
				aglfpf.setDteexp(rs.getInt("DTEEXP"));
				aglfpf.setPftflg(rs.getString("PFTFLG"));
				aglfpf.setRasflg(rs.getString("RASFLG"));
				aglfpf.setBcmtab(rs.getString("BCMTAB"));
				aglfpf.setRcmtab(rs.getString("RCMTAB"));
				aglfpf.setScmtab(rs.getString("SCMTAB"));
				aglfpf.setAgentClass(rs.getString("AGCLS"));
				aglfpf.setOcmtab(rs.getString("OCMTAB"));
				aglfpf.setReportag(rs.getString("REPORTAG"));
				aglfpf.setOvcpc(rs.getBigDecimal("OVCPC"));
				aglfpf.setTaxmeth(rs.getString("TAXMETH"));
				aglfpf.setIrdno(rs.getString("IRDNO"));
				aglfpf.setTaxcde(rs.getString("TAXCDE"));
				aglfpf.setTaxalw(rs.getBigDecimal("TAXALW"));
				aglfpf.setSprschm(rs.getString("SPRSCHM"));
				aglfpf.setSprprc(rs.getBigDecimal("SPRPRC"));
				aglfpf.setPayclt(rs.getString("PAYCLT"));
				aglfpf.setPaymth(rs.getString("PAYMTH"));
				aglfpf.setPayfrq(rs.getString("PAYFRQ"));
				aglfpf.setFacthous(rs.getString("FACTHOUS"));
				aglfpf.setBankkey(rs.getString("BANKKEY"));
				aglfpf.setBankacckey(rs.getString("BANKACCKEY"));
				aglfpf.setDtepay(rs.getInt("DTEPAY"));
				aglfpf.setCurrcode(rs.getString("CURRCODE"));
				aglfpf.setIntcrd(rs.getBigDecimal("INTCRD"));
				aglfpf.setFixprc(rs.getBigDecimal("FIXPRC"));
				aglfpf.setBmaflg(rs.getString("BMAFLG"));
				aglfpf.setExclAgmt(rs.getString("EXCAGR"));
				aglfpf.setHouseLoan(rs.getString("HSELN"));
				aglfpf.setComputerLoan(rs.getString("COMLN"));
				aglfpf.setCarLoan(rs.getString("CARLN"));
				aglfpf.setOfficeRent(rs.getString("OFFRENT"));
				aglfpf.setOtherLoans(rs.getString("OTHLN"));
				aglfpf.setAracde(rs.getString("ARACDE"));
				aglfpf.setMinsta(rs.getBigDecimal("MINSTA"));
				aglfpf.setZrorcode(rs.getString("ZRORCODE"));
				aglfpf.setEffdate(rs.getInt("EFFDATE"));
				aglfpf.setTagsusind(rs.getString("TAGSUSIND"));
				aglfpf.setTlaglicno(rs.getString("TLAGLICNO"));
				aglfpf.setTlicexpdt(rs.getInt("TLICEXPDT"));
				aglfpf.setTcolprct(rs.getBigDecimal("TCOLPRCT"));
				aglfpf.setTcolmax(rs.getBigDecimal("TCOLMAX"));
				aglfpf.setTsalesunt(rs.getString("TSALESUNT"));
				aglfpf.setPrdagent(rs.getString("PRDAGENT"));
				aglfpf.setAgccqind(rs.getString("AGCCQIND"));
				aglfpf.setZrecruit(rs.getString("ZRECRUIT"));
				aglfpf.setValidflag(rs.getString("VALIDFLAG"));
				String agntnum = aglfpf.getAgntnum();
				if (aglfpfMap.containsKey(agntnum)) {
					continue;
				} else {
					aglfpfMap.put(agntnum, aglfpf);
				}
			}

		} catch (SQLException e) {
			LOGGER.error("searchAglfRecord()" , e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psAglfSelect, rs);
		}
		return aglfpfMap;
	}
    
    public Aglfpf searchAglfRecord(String coy, String agntnum) {
        StringBuilder sqlAglfSelect1 = new StringBuilder("SELECT * FROM AGLFPF WHERE AGNTCOY=? AND AGNTNUM=? ");
        sqlAglfSelect1.append("ORDER BY AGNTCOY ASC, AGNTNUM ASC, UNIQUE_NUMBER DESC");
        PreparedStatement psAglfSelect = getPrepareStatement(sqlAglfSelect1.toString());
        ResultSet rs = null;
        Aglfpf aglfpf = null;
        try {
            psAglfSelect.setInt(1, Integer.parseInt(coy.trim()));//ILIFE-8413
            psAglfSelect.setString(2, agntnum.trim());
            rs = executeQuery(psAglfSelect);

            if (rs.next()) {
                aglfpf = new Aglfpf();
                aglfpf.setAgntcoy(rs.getString("AGNTCOY"));
                aglfpf.setAgntnum(rs.getString("AGNTNUM"));
                aglfpf.setTermid(rs.getString("TERMID"));
                aglfpf.setTransactionDate(rs.getInt("TRDT"));
                aglfpf.setTransactionTime(rs.getInt("TRTM"));
                aglfpf.setUser(rs.getInt("USER_T"));
                aglfpf.setCurrfrom(rs.getInt("CURRFROM"));
                aglfpf.setCurrto(rs.getInt("CURRTO"));
                aglfpf.setDteapp(rs.getInt("DTEAPP"));
                aglfpf.setDtetrm(rs.getInt("DTETRM"));
                aglfpf.setTrmcde(rs.getString("TRMCDE"));
                aglfpf.setDteexp(rs.getInt("DTEEXP"));
                aglfpf.setPftflg(rs.getString("PFTFLG"));
                aglfpf.setRasflg(rs.getString("RASFLG"));
                aglfpf.setBcmtab(rs.getString("BCMTAB"));
                aglfpf.setRcmtab(rs.getString("RCMTAB"));
                aglfpf.setScmtab(rs.getString("SCMTAB"));
                aglfpf.setAgentClass(rs.getString("AGCLS"));
                aglfpf.setOcmtab(rs.getString("OCMTAB"));
                aglfpf.setReportag(rs.getString("REPORTAG"));
                aglfpf.setOvcpc(rs.getBigDecimal("OVCPC"));
                aglfpf.setTaxmeth(rs.getString("TAXMETH"));
                aglfpf.setIrdno(rs.getString("IRDNO"));
                aglfpf.setTaxcde(rs.getString("TAXCDE"));
                aglfpf.setTaxalw(rs.getBigDecimal("TAXALW"));
                aglfpf.setSprschm(rs.getString("SPRSCHM"));
                aglfpf.setSprprc(rs.getBigDecimal("SPRPRC"));
                aglfpf.setPayclt(rs.getString("PAYCLT"));
                aglfpf.setPaymth(rs.getString("PAYMTH"));
                aglfpf.setPayfrq(rs.getString("PAYFRQ"));
                aglfpf.setFacthous(rs.getString("FACTHOUS"));
                aglfpf.setBankkey(rs.getString("BANKKEY"));
                aglfpf.setBankacckey(rs.getString("BANKACCKEY"));
                aglfpf.setDtepay(rs.getInt("DTEPAY"));
                aglfpf.setCurrcode(rs.getString("CURRCODE"));
                aglfpf.setIntcrd(rs.getBigDecimal("INTCRD"));
                aglfpf.setFixprc(rs.getBigDecimal("FIXPRC"));
                aglfpf.setBmaflg(rs.getString("BMAFLG"));
                aglfpf.setExclAgmt(rs.getString("EXCAGR"));
                aglfpf.setHouseLoan(rs.getString("HSELN"));
                aglfpf.setComputerLoan(rs.getString("COMLN"));
                aglfpf.setCarLoan(rs.getString("CARLN"));
                aglfpf.setOfficeRent(rs.getString("OFFRENT"));
                aglfpf.setOtherLoans(rs.getString("OTHLN"));
                aglfpf.setAracde(rs.getString("ARACDE"));
                aglfpf.setMinsta(rs.getBigDecimal("MINSTA"));
                aglfpf.setZrorcode(rs.getString("ZRORCODE"));
                aglfpf.setEffdate(rs.getInt("EFFDATE"));
                aglfpf.setTagsusind(rs.getString("TAGSUSIND"));
                aglfpf.setTlaglicno(rs.getString("TLAGLICNO"));
                aglfpf.setTlicexpdt(rs.getInt("TLICEXPDT"));
                aglfpf.setTcolprct(rs.getBigDecimal("TCOLPRCT"));
                aglfpf.setTcolmax(rs.getBigDecimal("TCOLMAX"));
                aglfpf.setTsalesunt(rs.getString("TSALESUNT"));
                aglfpf.setPrdagent(rs.getString("PRDAGENT"));
                aglfpf.setAgccqind(rs.getString("AGCCQIND"));
                aglfpf.setZrecruit(rs.getString("ZRECRUIT"));
                aglfpf.setValidflag(rs.getString("VALIDFLAG"));
           
            }

        } catch (SQLException e) {
            LOGGER.error("searchAglfRecord()" , e);
            throw new SQLRuntimeException(e);
        } finally {
            close(psAglfSelect, rs);
        }
        return aglfpf;
	}
	public Aglfpf searchAglflnb(String coy, String agntNum) {
		StringBuilder sqlAglfSelect1 = new StringBuilder("SELECT * FROM AGLFLNB WHERE AGNTCOY='").append(coy).append("' AND AGNTNUM=? ");
		PreparedStatement psAglfSelect = getPrepareStatement(sqlAglfSelect1.toString());
		ResultSet rs = null;
		Aglfpf aglfpf = null;
		try {
			psAglfSelect.setString(1, StringUtil.fillSpace(agntNum.trim(), DataDictionaryAtoC.agntnum.length)); //ILIFE-8792
			rs = executeQuery(psAglfSelect);
			  
			if (rs.next()) {
				aglfpf = new Aglfpf();
				aglfpf.setAgntcoy(rs.getString("AGNTCOY"));
				aglfpf.setAgntnum(rs.getString("AGNTNUM"));
				aglfpf.setClntnum(rs.getString("CLNTNUM"));
				aglfpf.setDteapp(rs.getInt("DTEAPP"));
				aglfpf.setDtetrm(rs.getInt("DTETRM"));
				aglfpf.setDteexp(rs.getInt("DTEEXP"));
				aglfpf.setBcmtab(rs.getString("BCMTAB"));
				aglfpf.setRcmtab(rs.getString("RCMTAB"));
				aglfpf.setScmtab(rs.getString("SCMTAB"));
				aglfpf.setAgentClass(rs.getString("AGCLS"));
				aglfpf.setReportag(rs.getString("REPORTAG"));
				aglfpf.setOvcpc(rs.getBigDecimal("OVCPC"));
				aglfpf.setAracde(rs.getString("ARACDE"));
				aglfpf.setZrorcode(rs.getString("ZRORCODE"));
				aglfpf.setEffdate(rs.getInt("EFFDATE"));           
				aglfpf.setTlaglicno(rs.getString("TLAGLICNO"));
				aglfpf.setTlicexpdt(rs.getInt("TLICEXPDT"));           
				aglfpf.setPrdagent(rs.getString("PRDAGENT"));
				aglfpf.setAgccqind(rs.getString("AGCCQIND"));
				aglfpf.setZrecruit(rs.getString("ZRECRUIT"));
				aglfpf.setValidflag(rs.getString("VALIDFLAG"));
				aglfpf.setAgntbr(rs.getString("AGNTBR")); //ILIFE-8709
			}
		} catch (SQLException e) {
			LOGGER.error("searchAglflnb()" , e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psAglfSelect, rs);
		}
		return aglfpf;
	}
}          