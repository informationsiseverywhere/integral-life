package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;
import com.csc.life.regularprocessing.dataaccess.dao.B5349DAO;
import com.csc.life.regularprocessing.dataaccess.model.B5349DTO;
import com.csc.life.regularprocessing.dataaccess.model.PayxTemppf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class B5349DAOImpl extends BaseDAOImpl<PayxTemppf>implements B5349DAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(B5349DAOImpl.class);
	
	public List<PayxTemppf> loadDataByBatch(int batchID, String threadNumber) {
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT * FROM B5349DATA "); //ILB-475
		sb.append("WHERE BATCHID = ? AND RTRIM(MEMBER_NAME) = ? ");
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, PAYRSEQNO DESC ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		List<PayxTemppf> payxList = new ArrayList<PayxTemppf>();
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, Integer.toString(batchID));
			ps.setString(2, threadNumber.trim());
			rs = ps.executeQuery();
			while(rs.next()){
				PayxTemppf payxpf = new PayxTemppf();
				B5349DTO b5349DTO = new B5349DTO();
				payxpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				payxpf.setChdrcoy(rs.getString("CHDRCOY")!= null ? (rs.getString("CHDRCOY").trim()) : "");
				payxpf.setChdrnum(rs.getString("CHDRNUM")!= null ? (rs.getString("CHDRNUM").trim()) : "");
				payxpf.setPayrseqno(rs.getInt("PAYRSEQNO"));
				payxpf.setBillsupr(rs.getString("BILLSUPR")!= null ? (rs.getString("BILLSUPR").trim()) : "");
				payxpf.setBillspfrom(rs.getInt("BILLSPFROM"));
				payxpf.setBillspto(rs.getInt("BILLSPTO"));
				payxpf.setBillchnl(rs.getString("BILLCHNL")!= null ? (rs.getString("BILLCHNL").trim()) : "");
				payxpf.setBillcd(rs.getInt("BILLCD"));
				payxpf.setMembername(rs.getString("MEMBER_NAME")!= null ? (rs.getString("MEMBER_NAME").trim()) : "");
				b5349DTO.setChdrtranno(rs.getInt("PAYRTRANNO"));
				b5349DTO.setPayrbillcd(rs.getInt("PAYRBILLCD"));
				b5349DTO.setBtdate(rs.getInt("BTDATE"));
				b5349DTO.setPtdate(rs.getInt("PTDATE"));
				b5349DTO.setOutstamt(rs.getBigDecimal("OUTSTAMT"));
				b5349DTO.setNextdate(rs.getInt("NEXTDATE"));
				b5349DTO.setPayrbillcurr(rs.getString("PAYRBILLCURR")!= null ? (rs.getString("PAYRBILLCURR").trim()) : "");
				b5349DTO.setBillfreq(rs.getString("BILLFREQ")!= null ? (rs.getString("BILLFREQ").trim()) : "");
				b5349DTO.setDuedd(rs.getString("DUEDD") != null ? (rs.getString("DUEDD").trim()) : "");
				b5349DTO.setDuemm(rs.getString("DUEMM")!= null ? (rs.getString("DUEMM").trim()) : "");
				b5349DTO.setBillday(rs.getString("BILLDAY")!= null ? (rs.getString("BILLDAY").trim()) : "");
				b5349DTO.setBillmonth(rs.getString("BILLMONTH"));
				b5349DTO.setSinstamt01(rs.getBigDecimal("SINSTAMT01"));
				b5349DTO.setSinstamt02(rs.getBigDecimal("SINSTAMT02"));	
				b5349DTO.setSinstamt03(rs.getBigDecimal("SINSTAMT03"));
				b5349DTO.setSinstamt04(rs.getBigDecimal("SINSTAMT04"));				
				b5349DTO.setSinstamt05(rs.getBigDecimal("SINSTAMT05"));
				b5349DTO.setSinstamt06(rs.getBigDecimal("SINSTAMT06"));				
				b5349DTO.setPayrcntcurr(rs.getString("PAYRCNTCURR")!= null ? (rs.getString("PAYRCNTCURR").trim()) : "");
				b5349DTO.setMandref(rs.getString("MANDREF") != null ? (rs.getString("MANDREF")) : "");				
				b5349DTO.setGrupcoy(rs.getString("GRUPCOY")!= null ? (rs.getString("GRUPCOY").trim()) : "");
				b5349DTO.setGrupnum(rs.getString("GRUPNUM")!= null ? (rs.getString("GRUPNUM").trim()) : "");				
				b5349DTO.setMembsel(rs.getString("MEMBSEL")!= null ? (rs.getString("MEMBSEL").trim()) : "");
				b5349DTO.setTaxrelmth(rs.getString("TAXRELMTH")!= null ? (rs.getString("TAXRELMTH").trim()) : "");				
				b5349DTO.setIncseqno(rs.getInt("INCSEQNO"));
				b5349DTO.setEffdate(rs.getInt("EFFDATE"));
				b5349DTO.setOrgbillcd(rs.getInt("ORGBILLCD"));
				b5349DTO.setChdruniqueno(rs.getLong("CHDRUNIQUENO"));				
				b5349DTO.setChdrtranno(rs.getInt("CHDRTRANNO"));
				b5349DTO.setCnttype(rs.getString("CNTTYPE")!= null ? (rs.getString("CNTTYPE").trim()) : "");				
				b5349DTO.setOccdate(rs.getInt("OCCDATE"));
				b5349DTO.setStatcode(rs.getString("STATCODE")!= null ? (rs.getString("STATCODE").trim()) : "");				
				b5349DTO.setPstcde(rs.getString("PSTCDE")!= null ? (rs.getString("PSTCDE").trim()) : "");
				b5349DTO.setReg(rs.getString("REG")!= null ? (rs.getString("REG").trim()) : "");				
				b5349DTO.setChdrcntcurr(rs.getString("CHDRCNTCURR")!= null ? (rs.getString("CHDRCNTCURR").trim()) : "");
				b5349DTO.setBillcurr(rs.getString("BILLCURR")!= null ? (rs.getString("BILLCURR").trim()) : "");	
				b5349DTO.setChdrpfx(rs.getString("CHDRPFX")!= null ? (rs.getString("CHDRPFX").trim()) : "");
				b5349DTO.setCowncoy(rs.getString("COWNCOY")!= null ? (rs.getString("COWNCOY").trim()) : "");
				b5349DTO.setCownnum(rs.getString("COWNNUM")!= null ? (rs.getString("COWNNUM").trim()) : "");
				b5349DTO.setCntbranch(rs.getString("CNTBRANCH")!= null ? (rs.getString("CNTBRANCH").trim()) : "");
				b5349DTO.setServunit(rs.getString("SERVUNIT")!= null ? (rs.getString("SERVUNIT").trim()) : "");
				b5349DTO.setCcdate(rs.getInt("CCDATE"));
				b5349DTO.setCollchnl(rs.getString("COLLCHNL")!= null ? (rs.getString("COLLCHNL").trim()) : "");
				b5349DTO.setCownpfx(rs.getString("COWNPFX")!= null ? (rs.getString("COWNPFX").trim()) : "");
				b5349DTO.setAgntpfx(rs.getString("AGNTPFX")!= null ? (rs.getString("AGNTPFX").trim()) : "");
				b5349DTO.setAgntcoy(rs.getString("AGNTCOY")!= null ? (rs.getString("AGNTCOY").trim()) : "");
				b5349DTO.setAgntnum(rs.getString("AGNTNUM")!= null ? (rs.getString("AGNTNUM").trim()) : "");
				b5349DTO.setChdrbillchnl(rs.getString("CHDRBILLCHNL")!= null ? (rs.getString("CHDRBILLCHNL").trim()) : "");				
				b5349DTO.setAcctmeth(rs.getString("ACCTMETH")!= null ? (rs.getString("ACCTMETH").trim()) : "");
				b5349DTO.setRcopt(rs.getString("RCOPT")!= null ? (rs.getString("RCOPT").trim()) : "");
				b5349DTO.setSrcebus(rs.getString("SRCEBUS")!= null ? (rs.getString("SRCEBUS").trim()) : "");
				b5349DTO.setClntpfx(rs.getString("CLNTPFX")!= null ? (rs.getString("CLNTPFX").trim()) : "");
				b5349DTO.setClntcoy(rs.getString("CLNTCOY")!= null ? (rs.getString("CLNTCOY").trim()) : "");
				b5349DTO.setClntnum(rs.getString("CLNTNUM")!= null ? (rs.getString("CLNTNUM").trim()) : "");
				b5349DTO.setBankkey(rs.getString("BANKKEY") != null ? (rs.getString("BANKKEY")) : "");
				b5349DTO.setBankacckey(rs.getString("BANKACCKEY") != null ? (rs.getString("BANKACCKEY")) : "");
				b5349DTO.setMandstat(rs.getString("MANDSTAT") != null ? (rs.getString("MANDSTAT")) : "");	
				b5349DTO.setFacthous(rs.getString("FACTHOUS") != null ? (rs.getString("FACTHOUS")) : "");
				
				payxpf.setB5349DTO(b5349DTO);
				payxList.add(payxpf);
			}
		}catch (SQLException e) {
			LOGGER.error("loadDataByBatch()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return payxList;
	}	
	
	public void initializeB5349Temp() {
		StringBuilder sb = new StringBuilder("");
		sb.append("DELETE FROM B5349DATA "); //ILB-475
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			int rows = ps.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.error("initializeB5349Temp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
	}	
	
	@Override
	public int populateB5349Temp(int batchExtractSize, String payxtempTable, String memberName) {
		//initializeB5349Temp();
		return 0;
	}	
	public int populateB5349TempJpn(int batchExtractSize, String payxtempTable, String memberName) {
		//populateB5349TempJpn();
		return 0;
	}	
	
	public int getB5349DataCount(String memberName){
		int rows = 0;
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT COUNT (*) FROM B5349DATA WHERE MEMBER_NAME = ?  "); 
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, StringUtil.fillSpace(memberName.trim(), DD.mbr.length));
			rs = ps.executeQuery();
			while(rs.next()){
				rows = rs.getInt(1);
			}
		}catch (SQLException e) {
			LOGGER.error("getB5349DataCount()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
	 return rows;
	}
}