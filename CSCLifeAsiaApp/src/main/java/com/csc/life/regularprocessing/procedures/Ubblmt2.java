/*
 * File: Ubblmt2.java
 * Date: 30 August 2009 2:48:10
 * Author: Quipoz Limited
 * 
 * Class transformed from UBBLMT2.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.dataaccess.LifernlTableDAM;
import com.csc.life.regularprocessing.recordstructures.Ubblallpar;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrstTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrstnudTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5691rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
/**
* <pre>
*REMARKS.
*
* This subroutine controls  the  'BENEFIT  BILLING'  processing,
* being called by various parts of the system.
*
* Basically this program  will  use  the  parameters  passed  to
* call a calculation subroutine to work out the  benefit  billed
* amount. This amount  is  calculated  based  on the sum at risk
* which can be calculated as follows:
*
*   Sum at risk = Sum insured - Surrender value of component
*
* Surrender value: if the  Surrender  method  is  not  blank  on
* table T5534, the  value  can  be  calculated  by  calling  the
* subroutine passed in from the linkage area.
*
* The premium to be debited  is  calculated dependent on entries
* (Premium Calculation  Method)  set  up  in  T5534,  T5675  and
* whether there is a joint life or not.
*
* Then the appropriate  subroutine  is  called  to  perform  the
* calculation if the methods are not blank.
*
* The Administration Fee amounts will  be  added  to the premium
* caluculated above. To calculate the amounts, an Administration
* Fee method  will  be  passed  from  table T5534 and, with this
* method  and the Contract Currency, table T5691 will be read to
* get the Initial and Periodic Fee amounts.
*
* Once the premium has been obtained, the corresponding coverage
* debt is then updated with this amount.
*
* This  premium is the Benefit Billed amount for that particular
* coverage or rider.
*
* This subroutine will also post accouting entries via T5645.
* These should be :
*    - Debit PREMIUM + INITIAL ADM FEES + PERIODIC ADM FEES
*      in 'Coverage Debt'
*    - Credit PREMIUM in 'Coverage Debt'
*    - Credit INITIAL ADM FEES  at 'Initial-Fees'
*    - Credit PERIODIC ADM FEES  at 'Periodic-Fees'
*
*
*    ===================================================
*              LIFE ASIA BASE VERSION 1.0 AMENDMENTS.
*    ===================================================
*
*
*    LIFE ASIA BASE Version 1.0 has changed this Subroutine to
*    include the following functionality.
*
*    Include administrative charges during
*    Contract Issue & Top Up.
*    The % charge is defined in T5691 & is
*    calculated based on (Admin Charges % *  Single Premium).
*    During TopUp, exclude Mortality Charges &
*    Regular Fees.
*
*    Age to be calculated as the number of
*    years between birthdate and effective
*    date. Call subroutine AGECALC to do this.
*
*    If surrender value of units is more than
*    the sum assured, set CPRM-SUMIN to 0.
*
*    Program checks the Periodic Fee value from
*    T5691 when processing the Initial Fee.
*                                                                     *
*    The Program also writes a ZRST when there
*    is Mortality charge, Policy fee, Initial
*    fee or Admin fee for a contract.
*    This file will be read during statement
*    printing.
*                                                                     *
*    To check for admin fee and do not create
*    accounting movement if zero.
*                                                                     *
*    Surrender values are now calculated in
*    the Fund currency.
*    For postings 'LE IF' 'LE FE',
*    the full coverage key should also be
*    used as RLDGACCT. Also the rider CRTABLE should
*    be used for substitution code.
*    For 'CD' posting an ACMV must be
*    written, even if the amount is zero, as
*    the ACMV record is used to reset Coverage
*    details when Contract Reversal is applied.
*
*    Missing condition to ignore mortality
*    charge during Topup.
*    Readjusted conditions to flow through all
*    types of fees.
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
* Initialise new fields which have been added to the PREMIUMREC
* Copybook.  These fields enable both Basic and Loaded
* Premiums to be calculated.
*
****************************************************************
* </pre>
*/
public class Ubblmt2 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaCallSubr = new FixedLengthStringData(10).init(SPACES);
	private final String wsaaSubr = "UBBLMT2";
	private FixedLengthStringData wsaaPremMeth = new FixedLengthStringData(4).init(SPACES);

	private FixedLengthStringData wsaaT5691Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5691Adm = new FixedLengthStringData(4).isAPartOf(wsaaT5691Key, 0);
	private FixedLengthStringData wsaaT5691Curr = new FixedLengthStringData(3).isAPartOf(wsaaT5691Key, 4);
		/* This field will have the total amount that has to be posted
		 to COVERAGE DEBT, ie, CPRM-CALC-PREM + Administration Fees.*/
	private ZonedDecimalData wsaaCovrTot = new ZonedDecimalData(17, 2).setUnsigned();
		/* Note only 2 places of decimals were actually used.....  <D96NUM>*/
	private ZonedDecimalData wsaaAccumSurrenderValue = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaAccumEstimatedValue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(13, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaTrefChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaTranref, 0);
	private FixedLengthStringData wsaaTrefChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaTranref, 1);
	private FixedLengthStringData wsaaTrefLife = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 9);
	private FixedLengthStringData wsaaTrefCoverage = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 11);
	private FixedLengthStringData wsaaTrefRider = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 13);
	private FixedLengthStringData wsaaTrefPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 15);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private FixedLengthStringData wsaaPrevRegister = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaPrevTxcode = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaPrevCrtable = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaPrevCnttype = new FixedLengthStringData(3).init(SPACES);
	private ZonedDecimalData wsaaTotalTax01 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaTotalTax02 = new ZonedDecimalData(17, 2);
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaBatckey = new FixedLengthStringData(19);
	private FixedLengthStringData wsaaBatccoy = new FixedLengthStringData(1).isAPartOf(wsaaBatckey, 2);
	private FixedLengthStringData wsaaBatcbrn = new FixedLengthStringData(2).isAPartOf(wsaaBatckey, 3);
	private PackedDecimalData wsaaBatcactyr = new PackedDecimalData(4, 0).isAPartOf(wsaaBatckey, 5);
	private PackedDecimalData wsaaBatcactmn = new PackedDecimalData(2, 0).isAPartOf(wsaaBatckey, 8);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaBatckey, 10);
	private FixedLengthStringData wsaaBatcbatch = new FixedLengthStringData(5).isAPartOf(wsaaBatckey, 14);
	private PackedDecimalData wsaaPerFee = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaIniFee = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaAdmChgs = new PackedDecimalData(12, 3);
	private PackedDecimalData wsaaTopupFee = new PackedDecimalData(12, 3);
	private PackedDecimalData wsaaTopupAdmChgs = new PackedDecimalData(12, 3);
	private PackedDecimalData wsaaTotalCd = new PackedDecimalData(16, 3);
	private PackedDecimalData wsaaSeqno = new PackedDecimalData(2, 0);
		/* TABLES */
	private static final String t5675 = "T5675";
	private static final String t1688 = "T1688";
	private static final String t5645 = "T5645";
	private static final String t6598 = "T6598";
	private static final String t5691 = "T5691";
	private static final String t5688 = "T5688";
	private static final String tr52d = "TR52D";
	private static final String tr52e = "TR52E";
	private static final String e308 = "E308";
	private static final String t071 = "T071";
	private static final String f401 = "F401";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String covrrec = "COVRREC";
	private static final String lifernlrec = "LIFERNLREC";
	private static final String covrsurrec = "COVRSURREC";
	private static final String zrstrec = "ZRSTREC";
	private static final String zrstnudrec = "ZRSTNUDREC";
	private static final String taxdrec = "TAXDREC";
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifernlTableDAM lifernlIO = new LifernlTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	private ZrstTableDAM zrstIO = new ZrstTableDAM();
	private ZrstnudTableDAM zrstnudIO = new ZrstnudTableDAM();
	private Itemkey wsaaItemkey = new Itemkey();
	private Varcom varcom = new Varcom();
	private Getdescrec getdescrec = new Getdescrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Freqcpy freqcpy = new Freqcpy();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private T5675rec t5675rec = new T5675rec();
	private T6598rec t6598rec = new T6598rec();
	private T5645rec t5645rec = new T5645rec();
	private T5691rec t5691rec = new T5691rec();
	private T5688rec t5688rec = new T5688rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Premiumrec premiumrec = new Premiumrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Ubblallpar ubblallpar = new Ubblallpar();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ExternalisedRules er = new ExternalisedRules();
	private ZonedDecimalData znofdue = new ZonedDecimalData(4, 0).init(1).setUnsigned();
	private ZonedDecimalData znofdue2 = new ZonedDecimalData(4, 0).init(2).setUnsigned();
	private boolean coiFeature = false;
	private static final String  FMC_FEATURE_ID = "BTPRO026";
	private static final String  RFMC = "RFMC";
	private boolean fmcOnFlag = false;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		skipReadingT56881050, 
		skipReadingT16881060, 
		exit1490, 
		skipReadingT65982150, 
		findPremCalcSubroutine2400, 
		skipReadingT56752450, 
		exit2900, 
		skipTr52d3400, 
		skipTr52e3400, 
		periodicFeeTax3400, 
		issueFeeTax3400, 
		adminChargeTax3400, 
		topupAdminTax3400, 
		topupFeeTax3400, 
		continue3400, 
		exit3400, 
		postPrem4020, 
		postFee4030, 
		perfeeamn4200, 
		postInitFee4050, 
		inifeeamn4300, 
		postAdminCharges4400
	}

	public Ubblmt2() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		ubblallpar.ubblallRec = convertAndSetParam(ubblallpar.ubblallRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		start110();
		exit180();
	}

protected void start110()
	{
		initialise1000();
		determinePremCalcMeth1500();
		calculateRiderPremium2000();
		updateCoverage3000();
		wsaaTotalCd.set(ZERO);
		wsaaSeqno.set(ZERO);
		/* INITIALIZE                  ZRST-PARAMS.             <V65L16>*/
		/* MOVE UBBL-CHDR-CHDRCOY      TO ZRST-CHDRCOY.         <V65L16>*/
		/* MOVE UBBL-CHDR-CHDRNUM      TO ZRST-CHDRNUM.         <V65L16>*/
		/* MOVE UBBL-LIFE-LIFE         TO ZRST-LIFE.            <V65L16>*/
		/*    MOVE UBBL-LIFE-JLIFE        TO ZRST-JLIFE.           <LIF2.1>*/
		/* MOVE SPACES                 TO ZRST-COVERAGE         <V65L16>*/
		/*                                ZRST-JLIFE            <V65L16>*/
		/*                                ZRST-RIDER.           <V65L16>*/
		/* MOVE ZRSTREC                TO ZRST-FORMAT.          <V65L16>*/
		/* MOVE BEGN                   TO ZRST-FUNCTION.        <V65L16>*/
		/* PERFORM B300-CHECK-ZRST-EXISTS UNTIL ZRST-STATUZ = ENDP.     */
		zrstnudIO.setParams(SPACES);
		zrstnudIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		zrstnudIO.setChdrnum(ubblallpar.chdrChdrnum);
		zrstnudIO.setLife(ubblallpar.lifeLife);
		zrstnudIO.setFormat(zrstnudrec);
		zrstnudIO.setFunction(varcom.begn);
		while ( !(isEQ(zrstnudIO.getStatuz(),varcom.endp))) {
			//performance improvement --  atiwari23 
			zrstnudIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			zrstnudIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");
			b300CheckZrstExists();
		}
		
		processTax3400();
		rewriteCovrRecord3500();
		updateLedgerAccounts4000();
		/* INITIALIZE                  ZRST-PARAMS.             <V65L16>*/
		/* MOVE UBBL-CHDR-CHDRCOY      TO ZRST-CHDRCOY.         <V65L16>*/
		/* MOVE UBBL-CHDR-CHDRNUM      TO ZRST-CHDRNUM.         <V65L16>*/
		/* MOVE UBBL-LIFE-LIFE         TO ZRST-LIFE.            <V65L16>*/
		/*    MOVE UBBL-LIFE-JLIFE        TO ZRST-JLIFE.           <LIF2.1>*/
		/* MOVE SPACES                 TO ZRST-COVERAGE         <V65L16>*/
		/*                                ZRST-JLIFE            <V65L16>*/
		/*                                ZRST-RIDER.           <V65L16>*/
		/* MOVE ZRSTREC                TO ZRST-FORMAT.          <V65L16>*/
		/*MOVE BEGNH                  TO ZRST-FUNCTION.        <CAS1.0>*/
		/* MOVE BEGN                   TO ZRST-FUNCTION.        <V65L16>*/
		/* PERFORM B400-UPDATE-TOTAL-CD UNTIL ZRST-STATUZ = ENDP.       */
		zrstnudIO.setParams(SPACES);
		zrstnudIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		zrstnudIO.setChdrnum(ubblallpar.chdrChdrnum);
		zrstnudIO.setLife(ubblallpar.lifeLife);
		zrstnudIO.setFormat(zrstnudrec);
		zrstnudIO.setFunction(varcom.begn);
		while ( !(isEQ(zrstnudIO.getStatuz(),varcom.endp))) {
			//performance improvement --  atiwari23 
			zrstnudIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			zrstnudIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");
			b400UpdateTotalCd();
		}
		
	}

protected void exit180()
	{
		exitProgram();
	}

protected void stop190()
	{
		stopRun();
	}

protected void systemError570()
	{
			para570();
			exit579();
		}

protected void para570()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit579()
	{
		ubblallpar.statuz.set(varcom.bomb);
		exit180();
	}

protected void databaseError580()
	{
			para580();
			exit589();
		}

protected void para580()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit589()
	{
		ubblallpar.statuz.set(varcom.bomb);
		exit180();
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1010();
				case skipReadingT56881050: 
					skipReadingT56881050();
				case skipReadingT16881060: 
				case exit1490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1010()
	{
		syserrrec.subrname.set(wsaaSubr);
		ubblallpar.statuz.set(varcom.oK);
		fmcOnFlag = FeaConfg.isFeatureExist(ubblallpar.chdrChdrcoy.toString().trim(), FMC_FEATURE_ID, appVars, "IT");
		if(fmcOnFlag){
			coiFeature = isEQ(ubblallpar.function , "RENL") && isEQ(ubblallpar.znofdue , znofdue2);
		}
		wsaaAccumSurrenderValue.set(0);
		wsaaSequenceNo.set(1);
		wsaaCovrTot.set(0);
		varcom.vrcmTime.set(getCobolTime());
		/* Read T5691 for Administration Fees Amounts.*/
		wsaaT5691Curr.set(ubblallpar.cntcurr);
		wsaaT5691Adm.set(ubblallpar.adfeemth);
		wsaaItemkey.set(SPACES);
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itdmIO.setItemtabl(t5691);
		itdmIO.setItmfrm(ubblallpar.effdate);
		itdmIO.setItemitem(wsaaT5691Key);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	


		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isNE(itdmIO.getItemcoy(),ubblallpar.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5691)
		|| isNE(itdmIO.getItemitem(),wsaaT5691Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(wsaaT5691Key);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(t071);
			databaseError580();
			goTo(GotoLabel.exit1490);
		}
		else {
			t5691rec.t5691Rec.set(itdmIO.getGenarea());
		}
		/* MOVE SMTP-ITEM              TO WSKY-ITEM-ITEMPFX.            */
		/* MOVE UBBL-CHDR-CHDRCOY      TO WSKY-ITEM-ITEMCOY.            */
		/* MOVE T5691                  TO WSKY-ITEM-ITEMTABL.           */
		/* MOVE WSAA-T5691-KEY         TO WSKY-ITEM-ITEMITEM.           */
		/* MOVE WSAA-ITEMKEY           TO ITEM-DATA-KEY.                */
		/* MOVE ITEMREC                TO ITEM-FORMAT.                  */
		/* MOVE READR                  TO ITEM-FUNCTION.                */
		/* CALL 'ITEMIO' USING ITEM-PARAMS.                             */
		/* IF ITEM-STATUZ             NOT = O-K                         */
		/*    MOVE ITEM-STATUZ         TO SYSR-STATUZ                   */
		/*    MOVE ITEM-PARAMS         TO SYSR-PARAMS                   */
		/*    PERFORM 580-DATABASE-ERROR.                               */
		/* MOVE ITEM-GENAREA           TO T5691-T5691-REC.              */
		/* Read T5645 for subaccount details.*/
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(ubblallpar.chdrChdrcoy);
		wsaaItemkey.itemItemtabl.set(t5645);
		wsaaItemkey.itemItemitem.set(wsaaSubr);
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			databaseError580();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Read T5688 to find out if contract type wants Component         */
		/*  level Accounting.                                              */
		if (isNE(ubblallpar.comlvlacc,SPACES)) {
			t5688rec.comlvlacc.set(ubblallpar.comlvlacc);
			goTo(GotoLabel.skipReadingT56881050);
		}
		wsaaItemkey.set(SPACES);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItmfrm(ubblallpar.effdate);
		itdmIO.setItemitem(ubblallpar.cnttype);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	


		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isNE(itdmIO.getItemcoy(),ubblallpar.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),ubblallpar.cnttype)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(ubblallpar.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			databaseError580();
			goTo(GotoLabel.exit1490);
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}

protected void skipReadingT56881050()
	{
		/* Get transaction description.*/
		if (isNE(ubblallpar.trandesc,SPACES)) {
			getdescrec.longdesc.set(ubblallpar.trandesc);
			goTo(GotoLabel.skipReadingT16881060);
		}
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(ubblallpar.chdrChdrcoy);
		wsaaItemkey.itemItemtabl.set(t1688);
		wsaaItemkey.itemItemitem.set(ubblallpar.batctrcde);
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set(ubblallpar.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			getdescrec.longdesc.set(SPACES);
		}
	}

protected void determinePremCalcMeth1500()
	{
			para1505();
			para1510();
		}

	/**
	* <pre>
	* Read LIFERNL to obtain sex & age for prem calculation
	* </pre>
	*/
protected void para1505()
	{
		premiumrec.premiumRec.set(SPACES);
		lifernlIO.setDataArea(SPACES);
		lifernlIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		lifernlIO.setChdrnum(ubblallpar.chdrChdrnum);
		lifernlIO.setLife(ubblallpar.lifeLife);
		lifernlIO.setJlife(ZERO);
		lifernlIO.setFormat(lifernlrec);
		lifernlIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		lifernlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifernlIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");
	


		SmartFileCode.execute(appVars, lifernlIO);
		if (isNE(lifernlIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifernlIO.getParams());
			databaseError580();
		}
		if ((isNE(lifernlIO.getChdrcoy(),ubblallpar.chdrChdrcoy))
		|| (isNE(lifernlIO.getChdrnum(),ubblallpar.chdrChdrnum))
		|| (isNE(lifernlIO.getLife(),ubblallpar.lifeLife))) {
			lifernlIO.setStatuz(varcom.endp);
			syserrrec.params.set(lifernlIO.getParams());
			databaseError580();
		}
		premiumrec.lsex.set(lifernlIO.getCltsex());
		/* MOVE LIFERNL-ANB-AT-CCD     TO CPRM-LAGE.                    */
		/* get correct age                                                 */
		/* PERFORM 1700-READ-CONTRACT.                          <V65L16>*/
		calcCorrectAge1800();
	}

protected void para1510()
	{
		/* Read LIFERNL to see if a joint life exists.*/
		/* Which benefit billing to be used is dependent on whether there*/
		/* is a joint life.*/
		lifernlIO.setDataArea(SPACES);
		lifernlIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		lifernlIO.setChdrnum(ubblallpar.chdrChdrnum);
		lifernlIO.setLife(ubblallpar.lifeLife);
		lifernlIO.setJlife("01");
		lifernlIO.setFormat(lifernlrec);
		lifernlIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		lifernlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifernlIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");
		
		SmartFileCode.execute(appVars, lifernlIO);
		if ((isNE(lifernlIO.getStatuz(),varcom.oK))
		&& (isNE(lifernlIO.getStatuz(),varcom.endp))) {
			syserrrec.statuz.set(lifernlIO.getStatuz());
			syserrrec.params.set(lifernlIO.getParams());
			databaseError580();
		}
		/* End of file, hence no joint life.*/
		if ((isEQ(lifernlIO.getStatuz(),varcom.endp))) {
			wsaaPremMeth.set(ubblallpar.premMeth);
			premiumrec.jlage.set(ZERO);
			premiumrec.jlsex.set(SPACES);
			return ;
		}
		/*     GO TO 1900-EXIT.                                         */
		/* If key breaks, no joint life else joint life exists.*/
		if ((isNE(lifernlIO.getChdrcoy(),ubblallpar.chdrChdrcoy))
		|| (isNE(lifernlIO.getChdrnum(),ubblallpar.chdrChdrnum))
		|| (isNE(lifernlIO.getLife(),ubblallpar.lifeLife))) {
			wsaaPremMeth.set(ubblallpar.premMeth);
			premiumrec.jlage.set(ZERO);
			premiumrec.jlsex.set(SPACES);
		}
		else {
			wsaaPremMeth.set(ubblallpar.jlifePremMeth);
			premiumrec.jlsex.set(lifernlIO.getCltsex());
			/*     MOVE LIFERNL-ANB-AT-CCD   TO CPRM-JLAGE.                 */
			calcCorrectAge1800();
		}
	}

	/**
	* <pre>
	*1700-READ-CONTRACT SECTION.                              <V65L16>
	****                                                      <V65L16>
	*1700-START.                                              <V65L16>
	****                                                      <V65L16>
	**** MOVE SPACES                 TO CHDRENQ-PARAMS.       <V65L16>
	**** MOVE UBBL-CHDR-CHDRCOY      TO CHDRENQ-CHDRCOY.      <V65L16>
	**** MOVE UBBL-CHDR-CHDRNUM      TO CHDRENQ-CHDRNUM.      <V65L16>
	**** MOVE CHDRENQREC             TO CHDRENQ-FORMAT.       <V65L16>
	**** MOVE READR                  TO CHDRENQ-FUNCTION.     <V65L16>
	****                                                      <V65L16>
	**** CALL 'CHDRENQIO'            USING CHDRENQ-PARAMS.    <V65L16>
	****                                                      <V65L16>
	**** IF CHDRENQ-STATUZ           NOT = O-K                <V65L16>
	****     MOVE CHDRENQ-PARAMS     TO SYSR-PARAMS           <V65L16>
	****     MOVE CHDRENQ-STATUZ     TO SYSR-STATUZ           <V65L16>
	****     PERFORM 570-SYSTEM-ERROR                         <V65L16>
	**** END-IF.                                              <V65L16>
	****                                                      <V65L16>
	*1790-EXIT.                                               <V65L16>
	****  EXIT.                                               <V65L16>
	* </pre>
	*/
protected void calcCorrectAge1800()
	{
			start1800();
		}

protected void start1800()
	{
		/*                                                         <CAS1.0>*/
		/* MOVE SPACES                 TO DTC3-DATCON3-REC.     <CAS1.0>*/
		/* MOVE CHDRENQ-OCCDATE        TO DTC3-INT-DATE-1.      <CAS1.0>*/
		/* MOVE UBBL-EFFDATE           TO DTC3-INT-DATE-2.      <CAS1.0>*/
		/* MOVE '01'                   TO DTC3-FREQUENCY.       <CAS1.0>*/
		/* MOVE ZEROS                  TO DTC3-FREQ-FACTOR.     <CAS1.0>*/
		/*                                                      <CAS1.0>*/
		/* CALL 'DATCON3'              USING DTC3-DATCON3-REC.  <CAS1.0>*/
		/*                                                      <CAS1.0>*/
		/* IF DTC3-STATUZ              NOT = O-K                <CAS1.0>*/
		/*     MOVE DTC3-DATCON3-REC   TO SYSR-PARAMS           <CAS1.0>*/
		/*     MOVE DTC3-STATUZ        TO SYSR-STATUZ           <CAS1.0>*/
		/*     PERFORM 570-SYSTEM-ERROR                         <CAS1.0>*/
		/* END-IF.                                              <CAS1.0>*/
		/*                                                      <CAS1.0>*/
		/* IF LIFERNL-JLIFE            = '01'                   <CAS1.0>*/
		/*     ADD LIFERNL-ANB-AT-CCD  DTC3-FREQ-FACTOR         <CAS1.0>*/
		/*                             GIVING CPRM-JLAGE        <CAS1.0>*/
		/*                                                      <CAS1.0>*/
		/* ELSE                                                 <CAS1.0>*/
		/*     ADD LIFERNL-ANB-AT-CCD  DTC3-FREQ-FACTOR         <CAS1.0>*/
		/*                             GIVING CPRM-LAGE         <CAS1.0>*/
		/* END-IF.                                              <CAS1.0>*/
		/*                                                      <CAS1.0>*/
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(ubblallpar.language);
		/* MOVE CHDRENQ-CNTTYPE        TO AGEC-CNTTYPE.         <V65L16>*/
		agecalcrec.cnttype.set(ubblallpar.cnttype);
		agecalcrec.intDate1.set(lifernlIO.getCltdob());
		agecalcrec.intDate2.set(ubblallpar.effdate);
		agecalcrec.company.set("9");
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isEQ(agecalcrec.statuz,"IVFD")) {
			syserrrec.statuz.set(f401);
			systemError570();
			return ;
		}
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			systemError570();
		}
		if (isEQ(lifernlIO.getJlife(),"01")) {
			premiumrec.jlage.set(agecalcrec.agerating);
		}
		else {
			premiumrec.lage.set(agecalcrec.agerating);
		}
	}

protected void calculateRiderPremium2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					findSurrenderSubroutine2100();
				case skipReadingT65982150: 
					skipReadingT65982150();
				case findPremCalcSubroutine2400: 
					findPremCalcSubroutine2400();
				case skipReadingT56752450: 
					skipReadingT56752450();
					calculatePremium2500();
				case exit2900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void findSurrenderSubroutine2100()
	{
		/* Use UBBL-SV-METHOD to access T6598 for the surrender value*/
		/* calculation subroutine.*/
		/* Check first if a surrender value exists.*/
		if (isNE(ubblallpar.svCalcprog,SPACES)) {
			t6598rec.calcprog.set(ubblallpar.svCalcprog);
			goTo(GotoLabel.skipReadingT65982150);
		}
		if (isEQ(ubblallpar.svMethod,SPACES)) {
			wsaaAccumSurrenderValue.set(ZERO);
			goTo(GotoLabel.findPremCalcSubroutine2400);
		}
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemitem(ubblallpar.svMethod);
		itemIO.setItemtabl(t6598);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError580();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
	}

protected void skipReadingT65982150()
	{
		if (isEQ(t6598rec.calcprog,SPACES)) {
			wsaaAccumSurrenderValue.set(0);
			goTo(GotoLabel.findPremCalcSubroutine2400);
		}
		/*CALCULATE-SURRENDER-VALUE*/
		/* To be put in!!*/
		/* As of 10/7/92, it has been put it !!                            */
		callSurrSubroutine5000();
	}

protected void findPremCalcSubroutine2400()
	{
		/* Use UBBL-PREM-METH or UBBL-JLIFE-PREM-METH T5675 for the*/
		/* premium calculation subroutine.*/
		/* Check if UBBL-PREM-METH or UBBL-JLIFE-PREM-METH exists.*/
		if (isNE(ubblallpar.premsubr,SPACES)
		&& isEQ(premiumrec.jlsex,SPACES)) {
			t5675rec.premsubr.set(ubblallpar.premsubr);
			goTo(GotoLabel.skipReadingT56752450);
		}
		if (isNE(ubblallpar.jpremsubr,SPACES)
		&& isNE(premiumrec.jlsex,SPACES)) {
			t5675rec.premsubr.set(ubblallpar.jpremsubr);
			goTo(GotoLabel.skipReadingT56752450);
		}
		if (isEQ(wsaaPremMeth,SPACES)) {
			premiumrec.calcPrem.set(0);
			premiumrec.calcBasPrem.set(0);
			premiumrec.calcLoaPrem.set(0);
			goTo(GotoLabel.exit2900);
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(t5675);
		itemIO.setItemitem(wsaaPremMeth);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError580();
		}
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

protected void skipReadingT56752450()
	{
		if (isNE(t5675rec.premsubr,SPACES)) {
			wsaaCallSubr.set(t5675rec.premsubr);
		}
		else {
			premiumrec.calcPrem.set(0);
			premiumrec.calcBasPrem.set(0);
			premiumrec.calcLoaPrem.set(0);
			goTo(GotoLabel.exit2900);
		}
	}

protected void calculatePremium2500()
	{
		/* Calculation routine is known, so set up parameters and called*/
		/* the subroutine.*/
		premiumrec.effectdt.set(0);
		premiumrec.calcPrem.set(0);
		premiumrec.calcBasPrem.set(0);
		premiumrec.calcLoaPrem.set(0);
		premiumrec.reRateDate.set(0);
		premiumrec.ratingdate.set(0);
		premiumrec.effectdt.set(ubblallpar.effdate);
		premiumrec.reRateDate.set(ubblallpar.effdate);
		premiumrec.ratingdate.set(ubblallpar.effdate);
		premiumrec.function.set(varcom.calc);
		premiumrec.crtable.set(ubblallpar.crtable);
		premiumrec.chdrChdrcoy.set(ubblallpar.chdrChdrcoy);
		premiumrec.chdrChdrnum.set(ubblallpar.chdrChdrnum);
		premiumrec.lifeLife.set(ubblallpar.lifeLife);
		premiumrec.lifeJlife.set(ubblallpar.lifeJlife);
		premiumrec.covrCoverage.set(ubblallpar.covrCoverage);
		premiumrec.covrRider.set(ubblallpar.covrRider);
		premiumrec.termdate.set(ubblallpar.premCessDate);
		premiumrec.billfreq.set(ubblallpar.billfreq);
		premiumrec.plnsfx.set(ubblallpar.planSuffix);
		premiumrec.effectdt.set(ubblallpar.effdate);
		premiumrec.currcode.set(ubblallpar.cntcurr);
		premiumrec.mop.set(ubblallpar.billchnl);
		premiumrec.mortcls.set(ubblallpar.mortcls);
		datcon3rec.function.set(SPACES);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError570();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/* Work out CPRM-SUMIN as the sum at risk.*/
		/* Check if Sum at risk is negative - if so zeroise it.            */
		compute(premiumrec.sumin, 3).setRounded(sub(ubblallpar.sumins,wsaaAccumSurrenderValue));
		if (isLT(premiumrec.sumin,0)) {
			premiumrec.sumin.set(ZERO);
		}
		/* As this is a  Unit Linked Program, there is no               */
		/* need for the Linkage to be set up with ANNY details.         */
		/* Therefore, all fields are initialised before the call        */
		/* to the Subroutine.                                           */
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
		premiumrec.language.set(ubblallpar.language);
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		//Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(wsaaCallSubr.toString())  && er.isExternalized(ubblallpar.cnttype.toString(), ubblallpar.crtable.toString())))
		{
			callProgram(wsaaCallSubr, premiumrec.premiumRec);
		}
		else
		{		
		
		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
		vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
		Vpxlextrec vpxlextrec = new Vpxlextrec();
		vpxlextrec.function.set("INIT");
		callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
		
		Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
		vpxchdrrec.function.set("INIT");
		callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
		premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
		premiumrec.cnttype.set(ubblallpar.cnttype);
		Vpxacblrec vpxacblrec=new Vpxacblrec();
		callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]			
		premiumrec.premMethod.set(wsaaPremMeth);
		callProgram(wsaaCallSubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		//Ticket #IVE-792 - End
		/*Ticket #ILIFE-2005 - End*/
		if (isNE(premiumrec.statuz,varcom.oK)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			systemError570();
		}
		zrdecplrec.amountIn.set(premiumrec.calcPrem);
		callRounding8000();
		premiumrec.calcPrem.set(zrdecplrec.amountOut);
	}

protected void updateCoverage3000()
	{
		readCovr3010();
	}

	/**
	* <pre>
	* The amount should add to the CRDEBT field in the COVERAGE
	* record even the benefit billed amount is for a attached rider.
	* </pre>
	*/
protected void readCovr3010()
	{
		covrIO.setDataArea(SPACES);
		covrIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		covrIO.setChdrnum(ubblallpar.chdrChdrnum);
		covrIO.setLife(ubblallpar.lifeLife);
		covrIO.setCoverage(ubblallpar.covrCoverage);
		covrIO.setRider("00");
		covrIO.setPlanSuffix(ubblallpar.planSuffix);
		covrIO.setFunction(varcom.readh);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			databaseError580();
		}
		/* ADD CPRM-CALC-PREM          TO COVR-COVERAGE-DEBT.           */
		/* MOVE CPRM-CALC-PREM         TO WSAA-COVR-TOT.                */
		if (isNE(ubblallpar.function,"TOPUP") && isNE(ubblallpar.function , RFMC)) {
			setPrecision(covrIO.getCoverageDebt(), 2);
			covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(),premiumrec.calcPrem));
			wsaaCovrTot.set(premiumrec.calcPrem);
		}
		/* Check if this subroutine is called from the Issue program or*/
		/* from the Renewals program. Add the Administration Fee amount*/
		/* to the coverage debt.*/
		wsaaIniFee.set(ZERO);
		wsaaPerFee.set(ZERO);
		wsaaAdmChgs.set(ZERO);
		wsaaTopupFee.set(ZERO);
		wsaaTopupAdmChgs.set(ZERO);
		if (isEQ(ubblallpar.function,"ISSUE") || (isEQ(ubblallpar.function , RFMC) && isEQ(ubblallpar.znofdue, znofdue))){
			/*    We must first correctly proportion the initial Admin fee     */
			/*     across the policies we are processing                       */
			if (isNE(ubblallpar.planSuffix,ZERO)) {
				/*            if plan-suffix is not zeros, then we are dealing     */
				/*             with a broken-out policy so to get the fee for      */
				/*             it we just divide the fee by the number of          */
				/*             policies in the plan (POLINC ).                     */
				compute(wsaaIniFee, 4).setRounded(div(t5691rec.inifeeamn, ubblallpar.polinc));
				zrdecplrec.amountIn.set(wsaaIniFee);
				callRounding8000();
				wsaaIniFee.set(zrdecplrec.amountOut);
			}
			if (isEQ(ubblallpar.planSuffix,ZERO)) {
				/*            if we get here, then we either have a fully or       */
				/*            partially summarised record.                         */
				/*            - if a full summary, then the no of                  */
				/*              summarised policies will be the same as            */
				/*              the total no of policies in the plan...            */
				/*            i.e. POLSUM = POLINC.                                */
				/*               Unless there is only 1 policy in the plan, in     */
				/*               which case POLSUM = 0 and thus the whole fee      */
				/*               is applied.                                       */
				/*            - if a partial summary, then the no of               */
				/*              summarised policies in the plan will be LESS       */
				/*              than the total no of policies in the plan...       */
				/*            i.e. POLSUM < POLINC.                                */
				if (isEQ(ubblallpar.polsum,ubblallpar.polinc)
				|| isEQ(ubblallpar.polsum,ZERO)) {
					wsaaIniFee.set(t5691rec.inifeeamn);
				}
				else {
					compute(wsaaIniFee, 4).setRounded(mult((div(t5691rec.inifeeamn, ubblallpar.polinc)), ubblallpar.polsum));
					zrdecplrec.amountIn.set(wsaaIniFee);
					callRounding8000();
					wsaaIniFee.set(zrdecplrec.amountOut);
				}
			}
			/*       ADD the initial fee to the total Coverage debt            */
			setPrecision(covrIO.getCoverageDebt(), 3);
			covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(),wsaaIniFee));
			wsaaCovrTot.add(wsaaIniFee);
			/* Add the Administrative Charges to the Total Coverage Debt*/
			compute(wsaaAdmChgs, 4).setRounded((div((mult(covrIO.getSingp(), t5691rec.zradmnpc)), 100)));
			zrdecplrec.amountIn.set(wsaaAdmChgs);
			callRounding8000();
			wsaaAdmChgs.set(zrdecplrec.amountOut);
			setPrecision(covrIO.getCoverageDebt(), 3);
			covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(),wsaaAdmChgs));
			wsaaCovrTot.add(wsaaAdmChgs);
			/*****    ADD T5691-INIFEEAMN      TO COVR-COVERAGE-DEBT            */
			/*****    ADD T5691-INIFEEAMN      TO WSAA-COVR-TOT                 */
		}
		/* To calculate the TopUp Fees & Admin Charges.                    */
		if (isEQ(ubblallpar.function,"TOPUP")) {
			if (isNE(ubblallpar.planSuffix,ZERO)) {
				compute(wsaaTopupFee, 4).setRounded(div(t5691rec.zrtupfee, ubblallpar.polinc));
				zrdecplrec.amountIn.set(wsaaTopupFee);
				callRounding8000();
				wsaaTopupFee.set(zrdecplrec.amountOut);
			}
			if (isEQ(ubblallpar.planSuffix,ZERO)) {
				if (isEQ(ubblallpar.polsum,ubblallpar.polinc)
				|| isEQ(ubblallpar.polsum,ZERO)) {
					wsaaTopupFee.set(t5691rec.zrtupfee);
				}
				else {
					compute(wsaaTopupFee, 4).setRounded(mult((div(t5691rec.zrtupfee, ubblallpar.polinc)), ubblallpar.polsum));
					zrdecplrec.amountIn.set(wsaaTopupFee);
					callRounding8000();
					wsaaTopupFee.set(zrdecplrec.amountOut);
				}
			}
			/* Add the TopUp fee to the total Coverage debt                    */
			setPrecision(covrIO.getCoverageDebt(), 3);
			covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(),wsaaTopupFee));
			wsaaCovrTot.add(wsaaTopupFee);
			/* Add the TopUp Administrative Charges to the Total Coverage Debt */
			compute(wsaaTopupAdmChgs, 4).setRounded((div((mult(t5691rec.zrtupadpc, ubblallpar.singp)), 100)));
			zrdecplrec.amountIn.set(wsaaTopupAdmChgs);
			callRounding8000();
			wsaaTopupAdmChgs.set(zrdecplrec.amountOut);
			setPrecision(covrIO.getCoverageDebt(), 3);
			covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(),wsaaTopupAdmChgs));
			wsaaCovrTot.add(wsaaTopupAdmChgs);
		}
		/*    We must  proportion the Periodic Admin fee                   */
		/*     across the policies we are processing                       */
		if(!coiFeature){
		if (isNE(ubblallpar.planSuffix,ZERO)) {
			/*        if plan-suffix is not zeros, then we are dealing         */
			/*         with a broken-out policy so to get the fee for          */
			/*         it we just divide the fee by the number of              */
			/*         policies in the plan (POLINC ).                         */
			compute(wsaaPerFee, 4).setRounded(div(t5691rec.perfeeamn, ubblallpar.polinc));
			zrdecplrec.amountIn.set(wsaaPerFee);
			callRounding8000();
			wsaaPerFee.set(zrdecplrec.amountOut);
		}
		if (isEQ(ubblallpar.planSuffix,ZERO)) {
			/*        if we get here, then we either have a fully or           */
			/*        partially summarised record.                             */
			/*        - if a full summary, then the no of                      */
			/*          summarised policies will be the same as                */
			/*          the total no of policies in the plan...                */
			/*        i.e. POLSUM = POLINC.                                    */
			/*           Unless there is only 1 policy in the plan, in         */
			/*           which case POLSUM = 0 and thus the whole fee          */
			/*           is applied.                                           */
			/*        - if a partial summary, then the no of                   */
			/*          summarised policies in the plan will be LESS           */
			/*          than the total no of policies in the plan...           */
			/*        i.e. POLSUM < POLINC.                                    */
			if (isEQ(ubblallpar.polsum,ubblallpar.polinc)
			|| isEQ(ubblallpar.polsum,ZERO)) {
				wsaaPerFee.set(t5691rec.perfeeamn);
			}
			else {
				compute(wsaaPerFee, 4).setRounded(mult((div(t5691rec.perfeeamn, ubblallpar.polinc)), ubblallpar.polsum));
				zrdecplrec.amountIn.set(wsaaPerFee);
				callRounding8000();
				wsaaPerFee.set(zrdecplrec.amountOut);
			}
		}
		/*       ADD the periodic fee to the total Coverage debt           */
		/* For TopUp, there is no periodic fee.                            */
		if (isEQ(ubblallpar.function,"TOPUP")) {
			wsaaPerFee.set(ZERO);
		}
		setPrecision(covrIO.getCoverageDebt(), 3);
		covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(),wsaaPerFee));
		wsaaCovrTot.add(wsaaPerFee);
		}
	}

protected void processTax3400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3400();
				case skipTr52d3400: 
					skipTr52d3400();
					readTaxControl3400();
				case skipTr52e3400: 
					skipTr52e3400();
					mortalityChargeTax3400();
				case periodicFeeTax3400: 
					periodicFeeTax3400();
				case issueFeeTax3400: 
					issueFeeTax3400();
				case adminChargeTax3400: 
					adminChargeTax3400();
				case topupAdminTax3400: 
					topupAdminTax3400();
				case topupFeeTax3400: 
					topupFeeTax3400();
				case continue3400: 
					continue3400();
				case exit3400: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3400()
	{
		wsaaTotalTax01.set(0);
		wsaaTotalTax02.set(0);
		if (isEQ(premiumrec.calcPrem, 0)
		&& isEQ(wsaaIniFee, 0)
		&& isEQ(wsaaAdmChgs, 0)
		&& isEQ(wsaaTopupFee, 0)
		&& isEQ(wsaaTopupAdmChgs, 0)
		&& isEQ(wsaaPerFee, 0)) {
			goTo(GotoLabel.exit3400);
		}
		if (isEQ(wsaaPrevRegister, ubblallpar.chdrRegister)) {
			goTo(GotoLabel.skipTr52d3400);
		}
		wsaaPrevRegister.set(ubblallpar.chdrRegister);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tr52d);
		itemIO.setItemitem(ubblallpar.chdrRegister);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			databaseError580();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
			itemIO.setItemtabl(tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				databaseError580();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
	}

protected void skipTr52d3400()
	{
		if (isEQ(tr52drec.txcode, SPACES)) {
			goTo(GotoLabel.exit3400);
		}
	}

protected void readTaxControl3400()
	{
		if (isEQ(wsaaPrevTxcode, tr52drec.txcode)
		&& isEQ(wsaaPrevCnttype, ubblallpar.cnttype)
		&& isEQ(wsaaPrevCrtable, ubblallpar.crtable)) {
			goTo(GotoLabel.skipTr52e3400);
		}
		wsaaPrevTxcode.set(tr52drec.txcode);
		wsaaPrevCnttype.set(ubblallpar.cnttype);
		wsaaPrevCrtable.set(ubblallpar.crtable);
		/* Read table TR52E                                                */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tr52e);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(ubblallpar.cnttype);
		wsaaTr52eCrtable.set(ubblallpar.crtable);
		itemIO.setItemitem(wsaaTr52eKey);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError580();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
			itemIO.setItemtabl(tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(ubblallpar.cnttype);
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				databaseError580();
			}
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
			itemIO.setItemtabl(tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				databaseError580();
			}
		}
		tr52erec.tr52eRec.set(itemIO.getGenarea());
	}

protected void skipTr52e3400()
	{
		/*  Setup the common variables used in calculating the tax         */
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(ubblallpar.chdrChdrcoy);
		txcalcrec.chdrnum.set(ubblallpar.chdrChdrnum);
		txcalcrec.life.set(ubblallpar.lifeLife);
		txcalcrec.coverage.set(ubblallpar.covrCoverage);
		txcalcrec.rider.set(ubblallpar.covrRider);
		txcalcrec.planSuffix.set(ubblallpar.planSuffix);
		txcalcrec.crtable.set(ubblallpar.crtable);
		txcalcrec.cnttype.set(ubblallpar.cnttype);
		txcalcrec.register.set(ubblallpar.chdrRegister);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.rateItem.set(SPACES);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaCntCurr.set(ubblallpar.cntcurr);
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		wsaaBatccoy.set(ubblallpar.batccoy);
		wsaaBatcbrn.set(ubblallpar.batcbrn);
		wsaaBatcactyr.set(ubblallpar.batcactyr);
		wsaaBatcactmn.set(ubblallpar.batcactmn);
		wsaaBatctrcde.set(ubblallpar.batctrcde);
		wsaaBatcbatch.set(ubblallpar.batch);
		txcalcrec.batckey.set(wsaaBatckey);
		txcalcrec.ccy.set(ubblallpar.cntcurr);
		txcalcrec.effdate.set(ubblallpar.effdate);
		txcalcrec.taxAmt[1].set(0);
		txcalcrec.taxAmt[2].set(0);
		txcalcrec.tranno.set(ubblallpar.tranno);
		txcalcrec.language.set(ubblallpar.language);
		/*  Set the value of TAXD record.                                  */
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		taxdIO.setChdrnum(ubblallpar.chdrChdrnum);
		taxdIO.setLife(ubblallpar.lifeLife);
		taxdIO.setCoverage(ubblallpar.covrCoverage);
		taxdIO.setRider(ubblallpar.covrRider);
		taxdIO.setPlansfx(ubblallpar.planSuffix);
		taxdIO.setEffdate(ubblallpar.effdate);
		taxdIO.setInstfrom(ubblallpar.effdate);
		taxdIO.setInstto(varcom.vrcmMaxDate);
		taxdIO.setBillcd(varcom.vrcmMaxDate);
		taxdIO.setTranno(ubblallpar.tranno);
	}

protected void mortalityChargeTax3400()
	{
		if (isEQ(tr52erec.taxind06, "Y")
		&& isNE(premiumrec.calcPrem, 0)
		&& isNE(ubblallpar.function, "TOPUP")) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.periodicFeeTax3400);
		}
		if (isEQ(tr52erec.zbastyp, "Y")) {
			txcalcrec.amountIn.set(premiumrec.calcBasPrem);
		}
		else {
			txcalcrec.amountIn.set(premiumrec.calcPrem);
		}
		txcalcrec.transType.set("MCHG");
		calcTax3600();
	}

protected void periodicFeeTax3400()
	{
		if (isEQ(tr52erec.taxind07, "Y")
		&& isNE(wsaaPerFee, 0)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.issueFeeTax3400);
		}
		txcalcrec.amountIn.set(wsaaPerFee);
		txcalcrec.transType.set("PERF");
		calcTax3600();
	}

protected void issueFeeTax3400()
	{
		if (isEQ(tr52erec.taxind08, "Y")
		&& isNE(wsaaIniFee, 0)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.adminChargeTax3400);
		}
		txcalcrec.amountIn.set(wsaaIniFee);
		txcalcrec.transType.set("ISSF");
		calcTax3600();
	}

protected void adminChargeTax3400()
	{
		if (isEQ(tr52erec.taxind09, "Y")
		&& isNE(wsaaAdmChgs, 0)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.topupAdminTax3400);
		}
		txcalcrec.amountIn.set(wsaaAdmChgs);
		txcalcrec.transType.set("ADMF");
		calcTax3600();
	}

protected void topupAdminTax3400()
	{
		if (isEQ(tr52erec.taxind10, "Y")
		&& isNE(wsaaTopupAdmChgs, 0)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.topupFeeTax3400);
		}
		txcalcrec.amountIn.set(wsaaTopupAdmChgs);
		txcalcrec.transType.set("TOPA");
		calcTax3600();
	}

protected void topupFeeTax3400()
	{
		if (isEQ(tr52erec.taxind10, "Y")
		&& isNE(wsaaTopupFee, 0)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.continue3400);
		}
		txcalcrec.amountIn.set(wsaaTopupFee);
		txcalcrec.transType.set("TOPF");
		calcTax3600();
	}

protected void continue3400()
	{
		setPrecision(covrIO.getCoverageDebt(), 2);
		covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(), add(wsaaTotalTax01, wsaaTotalTax02)));
		compute(wsaaCovrTot, 2).add(add(wsaaTotalTax01, wsaaTotalTax02));
		if (isNE(wsaaTotalTax01, 0)) {
			lifacmvrec.sacstyp.set(txcalcrec.taxType[1]);
			lifacmvrec.origamt.set(wsaaTotalTax01);
			b200WriteZrst();
		}
		if (isNE(wsaaTotalTax02, 0)) {
			lifacmvrec.sacstyp.set(txcalcrec.taxType[2]);
			lifacmvrec.origamt.set(wsaaTotalTax02);
			b200WriteZrst();
		}
	}

protected void rewriteCovrRecord3500()
	{
		/*START*/
		covrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			databaseError580();
		}
		/*EXIT*/
	}

protected void calcTax3600()
	{
		start3600();
		writeTaxdRecord3600();
	}

protected void start3600()
	{
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.taxAmt[1].set(0);
		txcalcrec.taxAmt[2].set(0);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			systemError570();
		}
	}

protected void writeTaxdRecord3600()
	{
		taxdIO.setTrantype(txcalcrec.transType);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(wsaaTranref);
		taxdIO.setTranref(wsaaTranref);
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(0);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setPostflg(" ");
		taxdIO.setFormat(taxdrec);
		taxdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			databaseError580();
		}
		if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaTotalTax01.add(txcalcrec.taxAmt[1]);
		}
		if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaTotalTax02.add(txcalcrec.taxAmt[2]);
		}
	}

protected void updateLedgerAccounts4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4010();
				case postPrem4020: 
					postPrem4020();
					calcPrem4100();
				case postFee4030: 
					postFee4030();
				case perfeeamn4200: 
					perfeeamn4200();
				case postInitFee4050: 
					postInitFee4050();
				case inifeeamn4300: 
					inifeeamn4300();
				case postAdminCharges4400: 
					postAdminCharges4400();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4010()
	{
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account*/
		/* movements.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(ubblallpar.chdrChdrnum);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		lifacmvrec.batccoy.set(ubblallpar.chdrChdrcoy);
		lifacmvrec.rldgcoy.set(ubblallpar.chdrChdrcoy);
		lifacmvrec.genlcoy.set(ubblallpar.chdrChdrcoy);
		lifacmvrec.batcactyr.set(ubblallpar.batcactyr);
		lifacmvrec.batctrcde.set(ubblallpar.batctrcde);
		lifacmvrec.batcactmn.set(ubblallpar.batcactmn);
		lifacmvrec.batcbatch.set(ubblallpar.batch);
		lifacmvrec.batcbrn.set(ubblallpar.batcbrn);
		/* MOVE T5645-SACSCODE-01      TO LIFA-SACSCODE.                */
		/* MOVE T5645-SACSTYPE-01      TO LIFA-SACSTYP.                 */
		lifacmvrec.origcurr.set(ubblallpar.cntcurr);
		/* Intead of sending 3 times values to CONT-DEBT, send the total*/
		/* amount (WSAA-COVR-TOT) just once.*/
		/*  MOVE CPRM-CALC-PREM         TO LIFA-ORIGAMT.*/
		lifacmvrec.origamt.set(wsaaCovrTot);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.tranno.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.tranno.set(ubblallpar.tranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.effdate.set(ubblallpar.effdate);
		/* MOVE UBBL-CHDR-CHDRNUM      TO LIFA-TRANREF.                 */
		/* Set up TRANREF to contain the Full component key of the         */
		/*  Coverage/Rider that is Creating the debt so that if a          */
		/*  reversal is required at a later date, the correct component    */
		/*  can be identified.                                             */
		wsaaTranref.set(SPACES);
		wsaaTrefChdrcoy.set(ubblallpar.chdrChdrcoy);
		wsaaTrefChdrnum.set(ubblallpar.chdrChdrnum);
		wsaaPlan.set(ubblallpar.planSuffix);
		wsaaTrefLife.set(ubblallpar.lifeLife);
		wsaaTrefCoverage.set(ubblallpar.covrCoverage);
		wsaaTrefRider.set(ubblallpar.covrRider);
		wsaaTrefPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.tranref.set(wsaaTranref);
		/* MOVE T5645-SIGN-01          TO LIFA-GLSIGN.                  */
		/* MOVE T5645-GLMAP-01         TO LIFA-GLCODE.                  */
		lifacmvrec.user.set(ubblallpar.user);
		/* MOVE T5645-CNTTOT-01        TO LIFA-CONTOT.                  */
		lifacmvrec.trandesc.set(getdescrec.longdesc);
		/* MOVE UBBL-CHDR-CHDRNUM      TO LIFA-RLDGACCT.                */
		/* MOVE UBBL-CHDR-CHDRNUM      TO WSAA-RLDG-CHDRNUM.       <001>*/
		/* MOVE UBBL-PLAN-SUFFIX       TO WSAA-PLAN.               <001>*/
		/* MOVE UBBL-LIFE-LIFE         TO WSAA-RLDG-LIFE.          <001>*/
		/* MOVE UBBL-COVR-COVERAGE     TO WSAA-RLDG-COVERAGE.      <001>*/
		/* MOVE UBBL-COVR-RIDER        TO WSAA-RLDG-RIDER.         <001>*/
		/* MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX.   <001>*/
		/* MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT.           <001>*/
		/*        Check for Component level accounting & act accordingly   */
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			lifacmvrec.sacscode.set(t5645rec.sacscode05);
			lifacmvrec.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec.glsign.set(t5645rec.sign05);
			lifacmvrec.glcode.set(t5645rec.glmap05);
			lifacmvrec.contot.set(t5645rec.cnttot05);
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(ubblallpar.chdrChdrnum);
			wsaaPlan.set(ubblallpar.planSuffix);
			wsaaRldgLife.set(ubblallpar.lifeLife);
			wsaaRldgCoverage.set(ubblallpar.covrCoverage);
			/*     MOVE UBBL-COVR-RIDER        TO WSAA-RLDG-RIDER      <007>*/
			/*        Post ACMV against the Coverage, not Rider since the      */
			/*         debt is also set against the Coverage                   */
			wsaaRldgRider.set("00");
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			/*     MOVE UBBL-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6)<7>*/
			/*        Set correct substitution code                            */
			lifacmvrec.substituteCode[6].set(covrIO.getCrtable());
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode01);
			lifacmvrec.sacstyp.set(t5645rec.sacstype01);
			lifacmvrec.glsign.set(t5645rec.sign01);
			lifacmvrec.glcode.set(t5645rec.glmap01);
			lifacmvrec.contot.set(t5645rec.cnttot01);
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(ubblallpar.chdrChdrnum);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		/* MOVE UBBL-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6). <002>*/
		lifacmvrec.substituteCode[1].set(ubblallpar.cnttype);
		lifacmvrec.transactionDate.set(ubblallpar.effdate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.function.set("PSTW");
		/* Write a Coverage/Debt ACMV in all cases. Billing details will   */
		/* not be updated during Contract Reversal if this ACMV is missing */
		/*                                                            <003>*/
		/* IF WSAA-COVR-TOT            = ZERO                      <003>*/
		/*    GO                       TO 4100-CALC-PREM.          <003>*/
		if (isEQ(wsaaCovrTot,ZERO)
		&& isEQ(ubblallpar.function,"TOPUP")) {
			goTo(GotoLabel.postPrem4020);
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
	}

protected void postPrem4020()
	{
		if (isEQ(premiumrec.calcPrem,ZERO)
		|| isEQ(ubblallpar.function,"TOPUP")) {
			goTo(GotoLabel.postFee4030);
		}
	}

protected void calcPrem4100()
	{
		if(fmcOnFlag && !coiFeature){
			return;
		}
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		lifacmvrec.origamt.set(premiumrec.calcPrem);
		/* MOVE UBBL-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6)  <014>*/
		/* MOVE T5645-SACSCODE-02      TO LIFA-SACSCODE.                */
		/* MOVE T5645-SACSTYPE-02      TO LIFA-SACSTYP.                 */
		/* MOVE T5645-GLMAP-02         TO LIFA-GLCODE.                  */
		/* MOVE T5645-SIGN-02          TO LIFA-GLSIGN.                  */
		/* MOVE T5645-CNTTOT-02        TO LIFA-CONTOT.                  */
		/*        Set correct posting type ( i.e. LE/LC/LP from T5645 )    */
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			wsaaRldgRider.set(ubblallpar.covrRider);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.sacscode.set(t5645rec.sacscode06);
			lifacmvrec.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec.glcode.set(t5645rec.glmap06);
			lifacmvrec.glsign.set(t5645rec.sign06);
			lifacmvrec.contot.set(t5645rec.cnttot06);
			lifacmvrec.substituteCode[6].set(ubblallpar.crtable);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode02);
			lifacmvrec.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec.glcode.set(t5645rec.glmap02);
			lifacmvrec.glsign.set(t5645rec.sign02);
			lifacmvrec.contot.set(t5645rec.cnttot02);
		}
		if (isEQ(premiumrec.calcPrem,ZERO)) {
			if ((isEQ(t5688rec.comlvlacc,"Y"))) {
				wsaaRldgRider.set("00");
				lifacmvrec.rldgacct.set(wsaaRldgacct);
			}
			goTo(GotoLabel.perfeeamn4200);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			wsaaRldgRider.set("00");
			lifacmvrec.rldgacct.set(wsaaRldgacct);
		}
		txcalcrec.transType.set("MCHG");
		b600PostTax();
		b200WriteZrst();
	}

protected void postFee4030()
	{
		if (isEQ(t5691rec.perfeeamn,ZERO)
		|| isEQ(ubblallpar.function,"TOPUP")) {
			goTo(GotoLabel.postInitFee4050);
		}
	}

protected void perfeeamn4200()
	{
		if(fmcOnFlag && isNE(ubblallpar.function , RFMC)){
			return;
		}
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		/* MOVE T5691-PERFEEAMN        TO LIFA-ORIGAMT.                 */
		lifacmvrec.origamt.set(wsaaPerFee);
		/*        Post ACMV against the Coverage, not Rider since the      */
		/*         Fee is also set against the Coverage                    */
		/* MOVE COVR-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6). <014>*/
		lifacmvrec.substituteCode[6].set(ubblallpar.crtable);
		/* MOVE T5645-SACSCODE-03      TO LIFA-SACSCODE.                */
		/* MOVE T5645-SACSTYPE-03      TO LIFA-SACSTYP.                 */
		/* MOVE T5645-GLMAP-03         TO LIFA-GLCODE.                  */
		/* MOVE T5645-SIGN-03          TO LIFA-GLSIGN.                  */
		/* MOVE T5645-CNTTOT-03        TO LIFA-CONTOT.                  */
		/*        Set correct posting type ( i.e. LE/LC/LP from T5645 )    */
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			wsaaRldgRider.set(ubblallpar.covrRider);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.sacscode.set(t5645rec.sacscode07);
			lifacmvrec.sacstyp.set(t5645rec.sacstype07);
			lifacmvrec.glcode.set(t5645rec.glmap07);
			lifacmvrec.glsign.set(t5645rec.sign07);
			lifacmvrec.contot.set(t5645rec.cnttot07);
			lifacmvrec.substituteCode[6].set(ubblallpar.crtable);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode03);
			lifacmvrec.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec.glcode.set(t5645rec.glmap03);
			lifacmvrec.glsign.set(t5645rec.sign03);
			lifacmvrec.contot.set(t5645rec.cnttot03);
		}
		if (isEQ(t5691rec.perfeeamn,ZERO)) {
			goTo(GotoLabel.inifeeamn4300);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
		txcalcrec.transType.set("PERF");
		b600PostTax();
		b200WriteZrst();
	}

protected void postInitFee4050()
	{
		/* IF T5691-PERFEEAMN           = ZERO                  <CAS1.0>*/
		if (isEQ(t5691rec.inifeeamn,ZERO)
		&& isEQ(t5691rec.zrtupfee,ZERO)) {
			goTo(GotoLabel.postAdminCharges4400);
			/*****    GO TO 4900-EXIT                                           */
		}
		if (isNE(ubblallpar.function,"ISSUE")
		&& isNE(ubblallpar.function,"TOPUP")) {
			goTo(GotoLabel.postAdminCharges4400);
			/*****    GO TO 4900-EXIT                                           */
		}
	}

protected void inifeeamn4300()
	{
		if(fmcOnFlag && isNE(ubblallpar.function , RFMC)){
			return;
		}
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		/* MOVE T5691-INIFEEAMN        TO LIFA-ORIGAMT.                 */
		/* MOVE WSAA-INI-FEE           TO LIFA-ORIGAMT.                 */
		if (isEQ(ubblallpar.function,"TOPUP")) {
			lifacmvrec.origamt.set(wsaaTopupFee);
		}
		else {
			lifacmvrec.origamt.set(wsaaIniFee);
		}
		/*        Post ACMV against the Coverage, not Rider since the      */
		/*         Fee is also set against the Coverage                    */
		/* MOVE COVR-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6). <014>*/
		/* MOVE T5645-SACSCODE-04      TO LIFA-SACSCODE.                */
		/* MOVE T5645-SACSTYPE-04      TO LIFA-SACSTYP.                 */
		/* MOVE T5645-GLMAP-04         TO LIFA-GLCODE.                  */
		/* MOVE T5645-SIGN-04          TO LIFA-GLSIGN.                  */
		/* MOVE T5645-CNTTOT-04        TO LIFA-CONTOT.                  */
		/*        Set correct posting type ( i.e. LE/LC/LP from T5645 )    */
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			wsaaRldgRider.set(ubblallpar.covrRider);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.sacscode.set(t5645rec.sacscode08);
			lifacmvrec.sacstyp.set(t5645rec.sacstype08);
			lifacmvrec.glcode.set(t5645rec.glmap08);
			lifacmvrec.glsign.set(t5645rec.sign08);
			lifacmvrec.contot.set(t5645rec.cnttot08);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode04);
			lifacmvrec.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec.glcode.set(t5645rec.glmap04);
			lifacmvrec.glsign.set(t5645rec.sign04);
			lifacmvrec.contot.set(t5645rec.cnttot04);
		}
		/* IF T5691-INIFEEAMN          = ZERO                   <CAS1.0>*/
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			/*    GO                       TO 4900-EXIT.            <CAS1.0>*/
			goTo(GotoLabel.postAdminCharges4400);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
		if (isEQ(ubblallpar.function, "TOPUP")) {
			txcalcrec.transType.set("TOPF");
		}
		else {
			txcalcrec.transType.set("ISSF");
		}
		b600PostTax();
		b200WriteZrst();
	}

protected void postAdminCharges4400()
	{
		if(fmcOnFlag && isNE(ubblallpar.function , RFMC)){
			return;
		}
		if (isEQ(t5691rec.zradmnpc,ZERO)
		&& isEQ(t5691rec.zrtupadpc,ZERO)) {
			return ;
		}
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		if (isEQ(ubblallpar.function,"TOPUP")) {
			lifacmvrec.origamt.set(wsaaTopupAdmChgs);
		}
		else {
			lifacmvrec.origamt.set(wsaaAdmChgs);
		}
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			return ;
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaRldgRider.set(ubblallpar.covrRider);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.sacscode.set(t5645rec.sacscode10);
			lifacmvrec.sacstyp.set(t5645rec.sacstype10);
			lifacmvrec.glcode.set(t5645rec.glmap10);
			lifacmvrec.glsign.set(t5645rec.sign10);
			lifacmvrec.contot.set(t5645rec.cnttot10);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode09);
			lifacmvrec.sacstyp.set(t5645rec.sacstype09);
			lifacmvrec.glcode.set(t5645rec.glmap09);
			lifacmvrec.glsign.set(t5645rec.sign09);
			lifacmvrec.contot.set(t5645rec.cnttot09);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
		if (isEQ(ubblallpar.function, "TOPUP")) {
			txcalcrec.transType.set("TOPA");
		}
		else {
			txcalcrec.transType.set("ADMF");
		}
		b600PostTax();
		b200WriteZrst();
	}

protected void b200WriteZrst()
	{
		b210WriteZrst();
	}

protected void b210WriteZrst()
	{
		zrstIO.setParams(SPACES);
		zrstIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		zrstIO.setChdrnum(ubblallpar.chdrChdrnum);
		zrstIO.setLife(ubblallpar.lifeLife);
		/*    MOVE UBBL-LIFE-JLIFE        TO ZRST-JLIFE.           <LIF2.1>*/
		zrstIO.setJlife(SPACES);
		zrstIO.setCoverage(ubblallpar.covrCoverage);
		zrstIO.setRider(ubblallpar.covrRider);
		zrstIO.setBatctrcde(ubblallpar.batctrcde);
		zrstIO.setTranno(ubblallpar.tranno);
		zrstIO.setTrandate(ubblallpar.effdate);
		zrstIO.setZramount01(lifacmvrec.origamt);
		wsaaTotalCd.add(lifacmvrec.origamt);
		wsaaSeqno.add(1);
		zrstIO.setZramount02(ZERO);
		zrstIO.setXtranno(ZERO);
		zrstIO.setSeqno(wsaaSeqno);
		zrstIO.setSacstyp(lifacmvrec.sacstyp);
		zrstIO.setUstmno(ZERO);
		zrstIO.setFormat(zrstrec);
		zrstIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(zrstIO.getStatuz());
			syserrrec.params.set(zrstIO.getParams());
			databaseError580();
		}
	}

protected void b300CheckZrstExists()
	{
			b310Para();
		}

protected void b310Para()
	{
		/* CALL 'ZRSTIO'               USING ZRST-PARAMS.       <V65L16>*/
		/* IF  ZRST-STATUZ         NOT = O-K                    <V65L16>*/
		/* AND ZRST-STATUZ         NOT = ENDP                   <V65L16>*/
		/*     MOVE ZRST-STATUZ        TO SYSR-STATUZ           <V65L16>*/
		/*     MOVE ZRST-PARAMS        TO SYSR-PARAMS           <V65L16>*/
		/*     PERFORM 580-DATABASE-ERROR                       <V65L16>*/
		/* END-IF.                                              <V65L16>*/
		/* IF ZRST-CHDRCOY         NOT = UBBL-CHDR-CHDRCOY      <V65L16>*/
		/* OR ZRST-CHDRNUM         NOT = UBBL-CHDR-CHDRNUM      <V65L16>*/
		/* OR ZRST-LIFE            NOT = UBBL-LIFE-LIFE         <V65L16>*/
		/*    OR ZRST-JLIFE           NOT = UBBL-LIFE-JLIFE        <LIF2.1>*/
		/* OR ZRST-STATUZ              = ENDP                   <V65L16>*/
		/*    MOVE ENDP                TO ZRST-STATUZ           <V65L16>*/
		/*    GO TO B390-EXIT                                   <V65L16>*/
		/* END-IF.                                              <V65L16>*/
		/* Feedback-ind = spaces ie. Unitdeal is not done.                 */
		/*    AND ZRST-SEQNO              = 1                      <LIF2.1>*/
		/*    AND ZRST-RIDER              = '00'                   <LIF2.1>*/
		/* AND ZRST-COVERAGE           = UBBL-COVR-COVERAGE     <V65L16>*/
		/*        ADD ZRST-ZRAMOUNT02     TO WSAA-TOTAL-CD         <LIF2.1>*/
		/*     ADD ZRST-ZRAMOUNT01     TO WSAA-TOTAL-CD         <V65L16>*/
		/* END-IF.                                              <V65L16>*/
		/* MOVE NEXTR                  TO ZRST-FUNCTION.        <V65L16>*/
		SmartFileCode.execute(appVars, zrstnudIO);
		if (isNE(zrstnudIO.getStatuz(),varcom.oK)
		&& isNE(zrstnudIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(zrstnudIO.getParams());
			syserrrec.statuz.set(zrstnudIO.getStatuz());
			databaseError580();
		}
		if (isEQ(zrstnudIO.getStatuz(),varcom.endp)
		|| isNE(zrstnudIO.getChdrcoy(),ubblallpar.chdrChdrcoy)
		|| isNE(zrstnudIO.getChdrnum(),ubblallpar.chdrChdrnum)
		|| isNE(zrstnudIO.getLife(),ubblallpar.lifeLife)) {
			zrstnudIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(zrstnudIO.getCoverage(),ubblallpar.covrCoverage)) {
			wsaaTotalCd.add(zrstnudIO.getZramount01());
		}
		zrstnudIO.setFunction(varcom.nextr);
	}

protected void b400UpdateTotalCd()
	{
			b410UpdateTotalCd();
		}

protected void b410UpdateTotalCd()
	{
		/* CALL 'ZRSTIO'               USING ZRST-PARAMS.       <V65L16>*/
		/* IF  ZRST-STATUZ         NOT = O-K                    <V65L16>*/
		/* AND ZRST-STATUZ         NOT = ENDP                   <V65L16>*/
		/*     MOVE ZRST-STATUZ        TO SYSR-STATUZ           <V65L16>*/
		/*     MOVE ZRST-PARAMS        TO SYSR-PARAMS           <V65L16>*/
		/*     PERFORM 580-DATABASE-ERROR                       <V65L16>*/
		/* END-IF.                                              <V65L16>*/
		/* IF ZRST-CHDRCOY         NOT = UBBL-CHDR-CHDRCOY      <V65L16>*/
		/* OR ZRST-CHDRNUM         NOT = UBBL-CHDR-CHDRNUM      <V65L16>*/
		/* OR ZRST-LIFE            NOT = UBBL-LIFE-LIFE         <V65L16>*/
		/*    OR ZRST-JLIFE           NOT = UBBL-LIFE-JLIFE        <LIF2.1>*/
		/* OR ZRST-STATUZ              = ENDP                   <V65L16>*/
		/*    MOVE ENDP                TO ZRST-STATUZ           <V65L16>*/
		/*    GO TO B490-EXIT                                   <V65L16>*/
		/* END-IF.                                              <V65L16>*/
		/* AND ZRST-COVERAGE           = UBBL-COVR-COVERAGE     <V65L16>*/
		/*     MOVE WSAA-TOTAL-CD      TO ZRST-ZRAMOUNT02       <V65L16>*/
		/* MOVE REWRT              TO ZRST-FUNCTION         <CAS1.0>*/
		/*     MOVE WRITD              TO ZRST-FUNCTION         <V65L16>*/
		/*     MOVE ZRSTREC            TO ZRST-FORMAT           <V65L16>*/
		/*     CALL 'ZRSTIO'           USING ZRST-PARAMS        <V65L16>*/
		/*     IF ZRST-STATUZ      NOT = O-K                    <V65L16>*/
		/*        MOVE ZRST-STATUZ     TO SYSR-STATUZ           <V65L16>*/
		/*        MOVE ZRST-PARAMS     TO SYSR-PARAMS           <V65L16>*/
		/*        PERFORM 580-DATABASE-ERROR                    <V65L16>*/
		/*     END-IF                                           <V65L16>*/
		/* END-IF.                                              <V65L16>*/
		/* MOVE NEXTR                  TO ZRST-FUNCTION.        <V65L16>*/
		SmartFileCode.execute(appVars, zrstnudIO);
		if (isNE(zrstnudIO.getStatuz(),varcom.oK)
		&& isNE(zrstnudIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(zrstnudIO.getParams());
			syserrrec.statuz.set(zrstnudIO.getStatuz());
			databaseError580();
		}
		if (isEQ(zrstnudIO.getStatuz(),varcom.endp)
		|| isNE(zrstnudIO.getChdrcoy(),ubblallpar.chdrChdrcoy)
		|| isNE(zrstnudIO.getChdrnum(),ubblallpar.chdrChdrnum)
		|| isNE(zrstnudIO.getLife(),ubblallpar.lifeLife)) {
			zrstnudIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(zrstnudIO.getCoverage(),ubblallpar.covrCoverage)) {
			zrstnudIO.setZramount02(wsaaTotalCd);
			zrstnudIO.setFunction(varcom.writd);
			zrstnudIO.setFormat(zrstnudrec);
			SmartFileCode.execute(appVars, zrstnudIO);
			if (isNE(zrstnudIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(zrstnudIO.getParams());
				syserrrec.statuz.set(zrstnudIO.getStatuz());
				databaseError580();
			}
		}
		zrstnudIO.setFunction(varcom.nextr);
	}

protected void b600PostTax()
	{
		b600Start();
	}

protected void b600Start()
	{
		/*  Set the value of TAXD record.                                  */
		if (isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		taxdIO.setChdrnum(ubblallpar.chdrChdrnum);
		taxdIO.setLife(ubblallpar.lifeLife);
		taxdIO.setCoverage(ubblallpar.covrCoverage);
		taxdIO.setRider(ubblallpar.covrRider);
		taxdIO.setPlansfx(ubblallpar.planSuffix);
		taxdIO.setInstfrom(ubblallpar.effdate);
		taxdIO.setTrantype(txcalcrec.transType);
		taxdIO.setStatuz(varcom.oK);
		taxdIO.setFormat(taxdrec);
		taxdIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)
		&& isNE(taxdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			databaseError580();
		}
		if (isEQ(taxdIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		/* Call tax subroutine                                             */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("POST");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(taxdIO.getChdrcoy());
		txcalcrec.chdrnum.set(taxdIO.getChdrnum());
		txcalcrec.life.set(taxdIO.getLife());
		txcalcrec.coverage.set(taxdIO.getCoverage());
		txcalcrec.rider.set(taxdIO.getRider());
		txcalcrec.planSuffix.set(taxdIO.getPlansfx());
		txcalcrec.crtable.set(ubblallpar.crtable);
		txcalcrec.cnttype.set(ubblallpar.cnttype);
		txcalcrec.register.set(ubblallpar.chdrRegister);
		txcalcrec.taxrule.set(subString(taxdIO.getTranref(), 1, 8));
		txcalcrec.rateItem.set(subString(taxdIO.getTranref(), 9, 8));
		txcalcrec.amountIn.set(taxdIO.getBaseamt());
		txcalcrec.transType.set(taxdIO.getTrantype());
		txcalcrec.effdate.set(taxdIO.getEffdate());
		txcalcrec.tranno.set(taxdIO.getTranno());
		txcalcrec.jrnseq.set(wsaaSequenceNo);
		txcalcrec.batckey.set(wsaaBatckey);
		txcalcrec.ccy.set(ubblallpar.cntcurr);
		txcalcrec.taxType[1].set(taxdIO.getTxtype01());
		txcalcrec.taxType[2].set(taxdIO.getTxtype02());
		txcalcrec.taxAmt[1].set(taxdIO.getTaxamt01());
		txcalcrec.taxAmt[2].set(taxdIO.getTaxamt02());
		txcalcrec.taxAbsorb[1].set(taxdIO.getTxabsind01());
		txcalcrec.taxAbsorb[2].set(taxdIO.getTxabsind02());
		txcalcrec.ccy.set(ubblallpar.cntcurr);
		txcalcrec.language.set(ubblallpar.language);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			databaseError580();
		}
		wsaaSequenceNo.set(txcalcrec.jrnseq);
		/* Update TAXD record                                              */
		taxdIO.setPostflg("P");
		taxdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			databaseError580();
		}
	}

protected void callSurrSubroutine5000()
	{
			start5000();
		}

protected void start5000()
	{
		/* Read the Coverage file to get Coverage/Rider details for        */
		/*  call to Surrender value calc. subroutine.                      */
		/*  If no valid COVRSUR record found, then exit program.           */
		wsaaAccumSurrenderValue.set(ZERO);
		covrsurIO.setParams(SPACES);
		covrsurIO.setFunction(varcom.readr);
		covrsurIO.setFormat(covrsurrec);
		covrsurIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		covrsurIO.setChdrnum(ubblallpar.chdrChdrnum);
		covrsurIO.setLife(ubblallpar.lifeLife);
		covrsurIO.setCoverage(ubblallpar.covrCoverage);
		covrsurIO.setRider(ubblallpar.covrRider);
		covrsurIO.setPlanSuffix(ubblallpar.planSuffix);
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrsurIO.getParams());
			syserrrec.statuz.set(covrsurIO.getStatuz());
			databaseError580();
		}
		if (isEQ(ubblallpar.function,"ISSUE")) {
			wsaaAccumSurrenderValue.set(covrsurIO.getSingp());
			return ;
		}
		/* We have at found the COVR record.                               */
		/* Set up linkage area to Call Surrender value calculation         */
		/*  subroutines.                                                   */
		srcalcpy.surrenderRec.set(SPACES);
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrsurIO.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrsurIO.getChdrnum());
		srcalcpy.planSuffix.set(covrsurIO.getPlanSuffix());
		srcalcpy.polsum.set(ubblallpar.polsum);
		srcalcpy.lifeLife.set(covrsurIO.getLife());
		srcalcpy.lifeJlife.set(covrsurIO.getJlife());
		srcalcpy.covrCoverage.set(covrsurIO.getCoverage());
		srcalcpy.covrRider.set(covrsurIO.getRider());
		srcalcpy.crtable.set(covrsurIO.getCrtable());
		srcalcpy.crrcd.set(covrsurIO.getCrrcd());
		srcalcpy.ptdate.set(ubblallpar.ptdate);
		srcalcpy.effdate.set(ubblallpar.effdate);
		srcalcpy.convUnits.set(covrsurIO.getConvertInitialUnits());
		srcalcpy.language.set(ubblallpar.language);
		srcalcpy.currcode.set(covrsurIO.getPremCurrency());
		srcalcpy.chdrCurr.set(ubblallpar.cntcurr);
		srcalcpy.pstatcode.set(covrsurIO.getPstatcode());
		if (isGT(covrsurIO.getInstprem(),ZERO)) {
			srcalcpy.singp.set(covrsurIO.getInstprem());
		}
		else {
			srcalcpy.singp.set(covrsurIO.getSingp());
		}
		srcalcpy.billfreq.set(ubblallpar.billfreq);
		srcalcpy.status.set(SPACES);
		srcalcpy.type.set("F");
		/* Now call the Surrender value calculation subroutine.            */
		while ( !(isEQ(srcalcpy.status,varcom.endp))) {
			getSurrValue5100();
		}
		
	}

protected void getSurrValue5100()
	{
		start5100();
	}

protected void start5100()
	{
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		/*IVE-796 RUL Product - Partial Surrender Calculation started*/
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		/*ILIFE-7548 Start*/
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString()) && er.isExternalized(ubblallpar.cnttype.toString(),srcalcpy.crtable.toString())))
		{
			callProgramX(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		/*ILIFE-7548 end*/
		else
		{
	 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
		
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);
			chdrlnbIO.setChdrcoy(srcalcpy.chdrChdrcoy);
			chdrlnbIO.setChdrnum(srcalcpy.chdrChdrnum);
			chdrlnbIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrlnbIO);	

			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrlnbIO);//VPMS call
							
			if(isEQ(srcalcpy.type,"L"))
			{
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
			/*ILIFE-3292 Start */
			if(vpxsurcrec.statuz.equals(varcom.endp))
				srcalcpy.status.set(varcom.endp);
			/*ILIFE-3292 End */
		}

		/*IVE-796 RUL Product - Partial Surrender Calculation end*/
		if (isNE(srcalcpy.status,varcom.oK)
		&& isNE(srcalcpy.status,varcom.endp)) {
			syserrrec.params.set(srcalcpy.surrenderRec);
			syserrrec.statuz.set(srcalcpy.status);
			systemError570();
		}
		/* Add values returned to our running Surrender value total        */
		/* The Surrender value of the units is returned in the estimated   */
		/*  variable.....                                                  */
		/* Convert the Estimated value to the fund currency.               */
		zrdecplrec.amountIn.set(srcalcpy.estimatedVal);
		callRounding8000();
		srcalcpy.estimatedVal.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(srcalcpy.actualVal);
		callRounding8000();
		srcalcpy.actualVal.set(zrdecplrec.amountOut);
		/* ADD SURC-ESTIMATED-VAL      TO WSAA-ACCUM-SURRENDER-VALUE.005*/
		if (isNE(srcalcpy.estimatedVal,ZERO)) {
			/* AND SURC-CURRCODE        NOT = UBBL-CNTCURR         <V4LAQR> */
			if (isNE(srcalcpy.currcode,ubblallpar.cntcurr)) {
				/*    MOVE SPACES              TO CLNK-CLNK002-REC      <V4LAQR>*/
				/*    MOVE SURC-ESTIMATED-VAL  TO CLNK-AMOUNT-IN        <V4LAQR>*/
				/*    PERFORM 5200-CONVERT-VALUE                        <V4LAQR>*/
				/*                                                      <V4LAQR>*/
				/*    ADD CLNK-AMOUNT-OUT TO WSAA-ACCUM-ESTIMATED-VALUE <V4LAQR>*/
				/*                         WSAA-ACCUM-SURRENDER-VALUE   <V4LAQR>*/
				conlinkrec.clnk002Rec.set(SPACES);
				conlinkrec.amountIn.set(srcalcpy.estimatedVal);
				convertValue5200();
				wsaaAccumSurrenderValue.add(conlinkrec.amountOut);
			}
			else {
				wsaaAccumSurrenderValue.add(srcalcpy.estimatedVal);
			}
		}
		/*  The ACTUAL-VALue returned from the subroutine called is the    */
		/*   PENALTY value - if you want the PENALTY included in the       */
		/*   Surrender value figures,remove the comment on the line below. */
		/* ADD SURC-ACTUAL-VAL         TO WSAA-ACCUM-SURRENDER-VALUE.   */
		/* As of change number <009>, we do want to include the Penalty    */
		/*  value.                                                         */
		/* Convert the Actual value to the fund currency.                  */
		/* ADD SURC-ACTUAL-VAL         TO WSAA-ACCUM-SURRENDER-VALUE.009*/
		wsaaAccumSurrenderValue.add(srcalcpy.actualVal);
	}

	/**
	* <pre>
	*     CONVERT THE VALUE TO THE FUND CURRENCY.                     
	* </pre>
	*/
protected void convertValue5200()
	{
		call5210();
	}

protected void call5210()
	{
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.cashdate.set(ubblallpar.effdate);
		conlinkrec.currIn.set(srcalcpy.currcode);
		conlinkrec.currOut.set(ubblallpar.cntcurr);
		conlinkrec.company.set(ubblallpar.chdrChdrcoy);
		conlinkrec.function.set("REAL");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			systemError570();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		callRounding8000();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(ubblallpar.chdrChdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(ubblallpar.cntcurr);
		zrdecplrec.batctrcde.set(ubblallpar.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			databaseError580();
		}
		/*EXIT*/
	}
}
