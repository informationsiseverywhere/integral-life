/*
 * File: Extbonus.java
 * Date: 29 August 2009 22:47:52
 * Author: Quipoz Limited
 * 
 * Class transformed from EXTBONUS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.regularprocessing.recordstructures.Bonusrec;
import com.csc.life.regularprocessing.tablestructures.T6635rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*              EXTRA/ADDITIONAL BONUS
*              ----------------------
*
* This program is part of the Traditional Business Bonus
*  suite.
* It is called to calculate the Extra/Additional bonus for a
*  specified component in a contract.
* The Extra/Addt. Bonus bonus rates are stored in table T6635 and
*  the key to this table is Bonus Method & Premium Status ( - FP
*   ..Fully Paid, PP ..Premium Paying, SP ..Single Premium ).
*  It will be a dated table that will be added to each year
*   bonuses are declared.
*                From Date 1991/01/01     To Date 1991/12/31
*                From Date 1990/01/01     To Date 1990/12/31
*                Etc.
*
*  The table is stored so that you can have the possibility of
*   Sum Assured based bonuses only, Bonus on Bonus or both.
*  Eg. Sum Ass Rate is 4.0 and Bonus Rate is 0
*      Calc = Component Sum Assured * 4/100 = Bonus
*      or
*      Sum Ass Rate is 4.0 and Bonus Rate is 5.0
*      Calc = Component Sum Assured * 4/100
*             + Component Bonuses * 5/100
*
*  The table also contains an 'offset term' value for each
*   'Years in Force' value.
*
*  The Sum assured and Bonus on bonus values are calculated and
*   returned to the calling program.
*
*  CALCULATIONS.
*  -------------
*
*  Sum Assured bonus =
*                     (component duration - T6635-offset-term ) *
*                     BONS-SUMIN *
*                     T6635-rate-per-risk-unit
*
*  Bonus on bonus    =
*                     (component duration - T6635-offset-term ) *
*                     ACBL Balance per T5645 entry *
*                     T6635-rate-per-risk-unit
*
* where T6635-rate-per-risk-unit is =
*                                   T6635-rate / T6635-risk-unit,
*
* the component duration is the actual length of time (years)
*  that the component has been in force,
*
* and the ACBL field used is  = ACBL-SACSCURBAL
*
*
* NOTE.
* -----
*      The 1st entry in T5645 is used to provide part of the
*        key for the read of the ACBL ( account balance ) file.
*      The ACBL record read holds the Reversionary bonus value
*       for the component we are processing.
*
*****************************************************************
* </pre>
*/
public class Extbonus extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "EXTBONUS";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-INTERNALS */
	private PackedDecimalData wsaaAcblSacscurbal = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSaBonus = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaBonBonus = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaSaBonusRate = new ZonedDecimalData(7, 2);
	private ZonedDecimalData wsaaBonBonusRate = new ZonedDecimalData(7, 2);
	private ZonedDecimalData wsaaSaRiskUnit = new ZonedDecimalData(6, 0);
	private ZonedDecimalData wsaaBonRiskUnit = new ZonedDecimalData(6, 0);
	private ZonedDecimalData wsaaBonusTerm = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaT6635Item = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaBonusCode = new FixedLengthStringData(4).isAPartOf(wsaaT6635Item, 0);
	private FixedLengthStringData wsaaPremiumStatus = new FixedLengthStringData(2).isAPartOf(wsaaT6635Item, 4);

	private FixedLengthStringData wsaaAcblKey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaAcblChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaAcblKey, 0);
	private FixedLengthStringData wsaaAcblLife = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 8);
	private FixedLengthStringData wsaaAcblCoverage = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 10);
	private FixedLengthStringData wsaaAcblRider = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 12);
	private FixedLengthStringData wsaaAcblPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
		/* ERRORS */
	private String g547 = "G547";
	private String g711 = "G711";
	private String g712 = "G712";
	private String itemrec = "ITEMREC   ";
	private String itdmrec = "ITEMREC   ";
	private String acblrec = "ACBLREC   ";
		/* TABLES */
	private String t6635 = "T6635";
	private String t5645 = "T5645";
		/*Subsidiary account balance*/
	private AcblTableDAM acblIO = new AcblTableDAM();
	private Bonusrec bonusrec = new Bonusrec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T6635rec t6635rec = new T6635rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2590, 
		exit99490, 
		exit99590
	}

	public Extbonus() {
		super();
	}

public void mainline(Object... parmArray)
	{
		bonusrec.bonusRec = convertAndSetParam(bonusrec.bonusRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		initialise500();
		controlProcessing1000();
		finish10000();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialise500()
	{
		start500();
	}

protected void start500()
	{
		syserrrec.subrname.set(wsaaSubr);
		bonusrec.statuz.set(varcom.oK);
		bonusrec.extBonusSa.set(ZERO);
		bonusrec.extBonusBon.set(ZERO);
		wsaaAcblSacscurbal.set(ZERO);
		wsaaSaBonus.set(ZERO);
		wsaaBonBonus.set(ZERO);
		wsaaSaBonusRate.set(ZERO);
		wsaaBonBonusRate.set(ZERO);
		wsaaSaRiskUnit.set(ZERO);
		wsaaBonRiskUnit.set(ZERO);
		wsaaBonusTerm.set(ZERO);
		wsaaCount.set(ZERO);
		wsaaPlan.set(ZERO);
		wsaaAcblKey.set(SPACES);
		wsaaT6635Item.set(SPACES);
		wsaaBonusCode.set(bonusrec.bonusCalcMeth);
		wsaaPremiumStatus.set(bonusrec.premStatus);
	}

protected void controlProcessing1000()
	{
		/*START*/
		readT66352000();
		readT56453000();
		readAcbl4000();
		calculateValues5000();
		finish10000();
	}

protected void readT66352000()
	{
		/*START*/
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(bonusrec.chdrChdrcoy);
		itdmIO.setItemtabl(t6635);
		itdmIO.setItemitem(wsaaT6635Item);
		itdmIO.setItmfrm(bonusrec.effectiveDate);
		itdmIO.setItempfx("IT");
		itdmIO.setFormat(itdmrec);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError99500();
		}
		if ((isNE(itdmIO.getItemcoy(),bonusrec.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t6635)
		|| isNE(itdmIO.getItemitem(),wsaaT6635Item)
		|| isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemitem(wsaaT6635Item);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g712);
			databaseError99500();
		}
		else {
			t6635rec.t6635Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t6635rec.riskunit01,ZERO)
		|| isEQ(t6635rec.riskunit02,ZERO)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g547);
			databaseError99500();
		}
		descIO.setParams(SPACES);
		descIO.setDescitem(wsaaT6635Item);
		descIO.setDesctabl(t6635);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bonusrec.chdrChdrcoy);
		descIO.setLanguage(bonusrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			bonusrec.description.set(descIO.getShortdesc());
		}
		else {
			bonusrec.description.fill("?");
		}
		selectT6635Values2500();
	}

protected void selectT6635Values2500()
	{
		try {
			start2500();
		}
		catch (GOTOException e){
		}
	}

protected void start2500()
	{
		wsaaCount.set(1);
		while ( !(isGT(wsaaCount,10))) {
			if ((isGT(bonusrec.termInForce,t6635rec.yrsinf[wsaaCount.toInt()]))) {
				wsaaCount.add(1);
			}
			else {
				wsaaSaRiskUnit.set(t6635rec.riskunit01);
				wsaaSaBonusRate.set(t6635rec.sumass[wsaaCount.toInt()]);
				wsaaBonRiskUnit.set(t6635rec.riskunit02);
				wsaaBonBonusRate.set(t6635rec.bonus[wsaaCount.toInt()]);
				if ((isLT(bonusrec.termInForce,t6635rec.offset[wsaaCount.toInt()]))) {
					wsaaBonusTerm.set(ZERO);
				}
				else {
					compute(wsaaBonusTerm, 5).set(sub(bonusrec.termInForce,t6635rec.offset[wsaaCount.toInt()]));
				}
				goTo(GotoLabel.exit2590);
			}
		}
		
		syserrrec.params.set(itdmIO.getParams());
		syserrrec.statuz.set(g711);
		databaseError99500();
	}

protected void readT56453000()
	{
		start3000();
	}

protected void start3000()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bonusrec.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		else {
			t5645rec.t5645Rec.set(itemIO.getGenarea());
		}
	}

protected void readAcbl4000()
	{
		start4000();
	}

protected void start4000()
	{
		acblIO.setParams(SPACES);
		acblIO.setRldgcoy(bonusrec.chdrChdrcoy);
		acblIO.setSacscode(t5645rec.sacscode01);
		wsaaAcblChdrnum.set(bonusrec.chdrChdrnum);
		wsaaAcblLife.set(bonusrec.lifeLife);
		wsaaAcblCoverage.set(bonusrec.covrCoverage);
		wsaaAcblRider.set(bonusrec.covrRider);
		wsaaPlan.set(bonusrec.plnsfx);
		wsaaAcblPlanSuffix.set(wsaaPlansuff);
		acblIO.setRldgacct(wsaaAcblKey);
		acblIO.setOrigcurr(bonusrec.cntcurr);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if ((isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			databaseError99500();
		}
		if ((isEQ(acblIO.getStatuz(),varcom.mrnf))) {
			wsaaAcblSacscurbal.set(ZERO);
		}
		else {
			wsaaAcblSacscurbal.set(acblIO.getSacscurbal());
		}
	}

protected void calculateValues5000()
	{
		/*START*/
		compute(wsaaSaBonus, 3).setRounded(div((mult(mult(bonusrec.sumin,wsaaSaBonusRate),wsaaBonusTerm)),wsaaSaRiskUnit));
		compute(wsaaBonBonus, 3).setRounded(div((mult(mult(wsaaAcblSacscurbal,wsaaBonBonusRate),wsaaBonusTerm)),wsaaBonRiskUnit));
		/*EXIT*/
	}

protected void finish10000()
	{
		/*START*/
		bonusrec.extBonusSa.set(wsaaSaBonus);
		bonusrec.extBonusBon.set(wsaaBonBonus);
		/*EXIT*/
	}

protected void systemError99000()
	{
		try {
			start99000();
		}
		catch (GOTOException e){
		}
		finally{
			exit99490();
		}
	}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit99490);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		bonusrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError99500()
	{
		try {
			start99500();
		}
		catch (GOTOException e){
		}
		finally{
			exit99590();
		}
	}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit99590);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99590()
	{
		bonusrec.statuz.set(varcom.bomb);
		exit190();
	}
}
