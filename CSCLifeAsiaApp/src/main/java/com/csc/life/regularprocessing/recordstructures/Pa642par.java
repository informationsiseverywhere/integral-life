package com.csc.life.regularprocessing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Pa642par extends ExternalData {


	  //*******************************
	  //Attribute Declarations
	  //*******************************
	  
	  	public FixedLengthStringData parmRecord = new FixedLengthStringData(9);
	  	public FixedLengthStringData option = new FixedLengthStringData(1).isAPartOf(parmRecord, 0);
	  	public ZonedDecimalData fromdate = new ZonedDecimalData(8).isAPartOf(parmRecord, 1);


		public void initialize() {
			COBOLFunctions.initialize(parmRecord);
		}	

		
		public FixedLengthStringData getBaseString() {
	  		if (baseString == null) {
	   			baseString = new FixedLengthStringData(getLength());
	    		parmRecord.isAPartOf(baseString, true);
	   			baseString.resetIsAPartOfOffset();
	  		}
	  		return baseString;
		}
}
