package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class IncrpfDAOImpl extends BaseDAOImpl<Incrpf> implements IncrpfDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IncrpfDAOImpl.class);

	@Override
	public List<Incrpf> getIncrpfList( String chdrcoy,String chdrnum) {
		List<Incrpf> listIncr = null;
		Incrpf incr = null;
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, EFFDATE, TRANNO, VALIDFLAG, STATCODE, PSTATCODE, CRRCD, CRTABLE, ORIGINST, LASTINST, NEWINST, ORIGSUM, LASTSUM, NEWSUM, ANNVRY, BASCMETH, PCTINC, REFFLAG, TRDT, TRTM, USER_T, TERMID, BASCPY, CEASEIND, RNWCPY, SRVCPY, ZBORIGINST, ZBLASTINST, ZBNEWINST, ZLORIGINST, ZLLASTINST, ZLNEWINST, ZSTPDUTY01,  USRPRF, JOBNM, DATIME,UNIQUE_NUMBER FROM INCRPF "); 
		sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? AND  VALIDFLAG = '1' "); 
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			listIncr = new ArrayList<Incrpf>();
			rs = ps.executeQuery();
			while(rs.next()) {
				incr = new Incrpf();
				incr.setChdrcoy(rs.getString(1));
				incr.setChdrnum(rs.getString(2));
				incr.setLife(rs.getString(3));
				incr.setJlife(rs.getString(4));
				incr.setCoverage(rs.getString(5));
				incr.setRider(rs.getString(6));
				incr.setPlnsfx(rs.getInt(7));
				incr.setEffdate(rs.getInt(8));
				incr.setTranno(rs.getInt(9));
				incr.setValidflag(rs.getString(10));
				incr.setStatcode(rs.getString(11));
				incr.setPstatcode(rs.getString(12));
				incr.setCrrcd(rs.getInt(13));
				incr.setCrtable(rs.getString(14));
				incr.setOrigInst(rs.getBigDecimal(15));
				incr.setLastInst(rs.getBigDecimal(16));
				incr.setNewinst(rs.getBigDecimal(17));
				incr.setOrigSum(rs.getBigDecimal(18));
				incr.setLastSum(rs.getBigDecimal(19));
				incr.setNewsum(rs.getBigDecimal(20));
				incr.setAnniversaryMethod(rs.getString(21));
				incr.setBasicCommMeth(rs.getString(22));
				incr.setPctinc(rs.getInt(23));
				incr.setRefusalFlag(rs.getString(24));
				incr.setTransactionDate(rs.getInt(25));
				incr.setTransactionTime(rs.getInt(26));
				incr.setUser(rs.getInt(27));
				incr.setTermid(rs.getString(28));
				incr.setBascpy(rs.getString(29));
				incr.setCeaseInd(rs.getString(30));
				incr.setRnwcpy(rs.getString(31));
				incr.setSrvcpy(rs.getString(32));
				incr.setZboriginst(rs.getBigDecimal(33));
				incr.setZblastinst(rs.getBigDecimal(34));
				incr.setZbnewinst(rs.getBigDecimal(35));
				incr.setZloriginst(rs.getBigDecimal(36));
				incr.setZllastinst(rs.getBigDecimal(37));
				incr.setZlnewinst(rs.getBigDecimal(38));
				incr.setZstpduty01(rs.getBigDecimal(39));//ILIFE-8509
				incr.setUserProfile(rs.getString(40));
				incr.setJobName(rs.getString(41));
				incr.setDatime(rs.getDate(42));
				incr.setUniqueNumber(rs.getLong(43));
				listIncr.add(incr);
			}
			
		} catch (SQLException e) {
			LOGGER.error("getIncrpfList()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return listIncr;
	}
	
	@Override
	public Incrpf getIncrData(String chdrcoy,String chdrnum,  String life, String coverage,String rider, int planSuffix) {
		Incrpf incr = null;
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, EFFDATE, TRANNO, VALIDFLAG, STATCODE, PSTATCODE, CRRCD, CRTABLE, TRDT, TRTM, USER, TERMID,UNIQUE_NUMBER FROM INCRPF ");
		sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? AND   LIFE = ? AND COVERAGE = ? ");
		sb.append(" AND RIDER = ? AND PLNSFX = ? ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, chdrnum);
			ps.setString(2, chdrcoy);
			ps.setString(3, life);
			ps.setString(4, coverage);	
			ps.setString(5, rider);
			ps.setInt(6, planSuffix);
			rs = ps.executeQuery();
			while(rs.next()){
			incr = new Incrpf();
			incr.setChdrcoy(rs.getString(1));
			incr.setChdrcoy(rs.getString(2));
			incr.setLife(rs.getString(3));
			incr.setJlife(rs.getString(4));
			incr.setCoverage(rs.getString(5));
			incr.setRider(rs.getString(6));
			incr.setPlnsfx(rs.getInt(7));
			incr.setEffdate(rs.getInt(8));
			incr.setTranno(rs.getInt(9));
			incr.setValidflag(rs.getString(10));
			incr.setStatcode(rs.getString(11));
			incr.setPstatcode(rs.getString(12));
			incr.setCrrcd(rs.getInt(13));
			incr.setCrtable(rs.getString(14));
			incr.setTransactionDate(rs.getInt(15));
			incr.setTransactionTime(rs.getInt(16));
			incr.setUser(rs.getInt(17));
			incr.setTermid(rs.getString(18));
			incr.setUniqueNumber(rs.getLong(19));
			}
		} catch (SQLException e) {
			LOGGER.error("getIncrData()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return incr;
	}

	@Override
	public int getIncrCount(String chdrnum, String chdrcoy, String life, String coverage, int planSuffix) {
		int count = 0;
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT COUNT(*) FROM INCRPF ");
		sb.append(" WHERE CHDRNUM = ? AND CHDRCOY = ? AND LIFE = ? AND COVERAGE = ? AND PLNSFX = ? ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, chdrnum);
			ps.setString(2, chdrcoy);
			ps.setString(3, life);
			ps.setString(4, coverage);
			ps.setInt(5, planSuffix);
			rs = ps.executeQuery();
			rs.next();
			count = rs.getInt(1);
			
		} catch (SQLException e) {
			LOGGER.error("getIncrCount()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return count;
	}

	@Override
	public boolean insertIncrList(List<Incrpf> incrpfList) {
		boolean isInsertSuccessful = true;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO INCRPF(CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, EFFDATE, TRANNO, VALIDFLAG, STATCODE, PSTATCODE, CRRCD, CRTABLE, ORIGINST, LASTINST, NEWINST, ORIGSUM, LASTSUM, NEWSUM, ANNVRY, bascmeth, PCTINC, REFFLAG, TRDT, TRTM, USER_T, TERMID, BASCPY, CEASEIND, RNWCPY, SRVCPY, ZBORIGINST, ZBLASTINST, ZBNEWINST, ZLORIGINST, ZLLASTINST, ZLNEWINST, ZSTPDUTY01, USRPRF, JOBNM, DATIME) "); 
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "); 
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for(Incrpf incr : incrpfList) {
				ps.setString(1, incr.getChdrcoy());
				ps.setString(2, incr.getChdrnum());
				ps.setString(3, incr.getLife());
				ps.setString(4, incr.getJlife());
				ps.setString(5, incr.getCoverage());
				ps.setString(6, incr.getRider());
				ps.setInt(7, incr.getPlnsfx());
				ps.setInt(8, incr.getEffdate());
				ps.setInt(9, incr.getTranno());
				ps.setString(10, incr.getValidflag());
				ps.setString(11, incr.getStatcode());
				ps.setString(12, incr.getPstatcode());
				ps.setInt(13, incr.getCrrcd());
				ps.setString(14, incr.getCrtable());
				ps.setBigDecimal(15, incr.getOrigInst());
				ps.setBigDecimal(16, incr.getLastInst());
				ps.setBigDecimal(17, incr.getNewinst());
				ps.setBigDecimal(18, incr.getOrigSum());
				ps.setBigDecimal(19, incr.getLastSum());
				ps.setBigDecimal(20, incr.getNewsum());
				ps.setString(21, incr.getAnniversaryMethod());
				ps.setString(22, incr.getBasicCommMeth());
				ps.setInt(23, incr.getPctinc());
				ps.setString(24, incr.getRefusalFlag());
				ps.setInt(25, incr.getTransactionDate());
				ps.setInt(26, incr.getTransactionTime());
				ps.setInt(27, incr.getUser());
				ps.setString(28, incr.getTermid());
				ps.setString(29, incr.getBascpy());
				ps.setString(30, incr.getCeaseInd());
				ps.setString(31, incr.getRnwcpy());
				ps.setString(32, incr.getSrvcpy());
				ps.setBigDecimal(33, incr.getZboriginst());
				ps.setBigDecimal(34, incr.getZblastinst());
				ps.setBigDecimal(35, incr.getZbnewinst());
				ps.setBigDecimal(36, incr.getZloriginst());
				ps.setBigDecimal(37, incr.getZllastinst());
				ps.setBigDecimal(38, incr.getZlnewinst());
				ps.setBigDecimal(39, incr.getZstpduty01() == null ? BigDecimal.ZERO : incr.getZstpduty01());
				ps.setString(40, this.getUsrprf());
				ps.setString(41, this.getJobnm());
				ps.setTimestamp(42, new Timestamp(System.currentTimeMillis()));
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertIncrList()", e); /* IJTI-1479 */	
			isInsertSuccessful = false;
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}	
		return isInsertSuccessful;
	}

	@Override
	public void deleteIncrpf(long uniqueNumber) {
		StringBuilder sb = new StringBuilder("");
		sb.append("DELETE INCRPF WHERE UNIQUE_NUMBER = " + uniqueNumber);
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.error("deleteIncrpf()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		
	}
	
	public List<Incrpf> searchIncrpfRecord(Incrpf incrpf) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();
		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlSelect.append("RIDER, PLNSFX, TRANNO, VALIDFLAG, ");
		sqlSelect.append("STATCODE, PSTATCODE, CRRCD, CRTABLE, ORIGINST, ");
		sqlSelect.append("LASTINST, NEWINST, ORIGSUM, LASTSUM, NEWSUM, ");
		sqlSelect.append("ANNVRY, BASCMETH, BASCPY, PCTINC, REFFLAG, ");
		sqlSelect.append("CEASEIND, TERMID, TRDT, TRTM ");
		
		sqlSelect.append(" FROM INCRPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND VALIDFLAG = ?");//IJTI-1726
		
		//IJTI-1726 starts
		if(incrpf.getPlnsfx() != null) {
			sqlSelect.append(" AND PLNSFX = ? ");
		}
		//IJTI-1726 ends
		if(incrpf.getCoverage() != null && incrpf.getRider() != null)
		{
			sqlSelect.append(" AND COVERAGE = ? AND RIDER = ?");
		}
		sqlSelect.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement psIncrpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlIncrpfRs = null;
		List<Incrpf> outputList = new ArrayList<>();

		try {
			
			psIncrpfSelect.setString(1, incrpf.getChdrcoy());
			psIncrpfSelect.setString(2, incrpf.getChdrnum());
			//IJTI-1726 starts
			psIncrpfSelect.setString(3, "1");
			
			int index = 4;
			if(incrpf.getPlnsfx() != null) {
				psIncrpfSelect.setInt(index, incrpf.getPlnsfx());
				index++;
			}
			
			if(incrpf.getCoverage() != null && incrpf.getRider() != null)
			{
				psIncrpfSelect.setString(index, incrpf.getCoverage());
				index++;
				psIncrpfSelect.setString(index, incrpf.getRider());
				index++;
			}
			//IJTI-1726 ends
			sqlIncrpfRs = executeQuery(psIncrpfSelect);

			while (sqlIncrpfRs.next()) {
				Incrpf incrpfnew = prepareIncrpf(sqlIncrpfRs);//IJTI-1726
				outputList.add(incrpfnew);
			}

		} catch (SQLException e) {
			LOGGER.error("searchIncrpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psIncrpfSelect, sqlIncrpfRs);
		}

		return outputList;
	}	
	//IJTI-1726 starts
	private Incrpf prepareIncrpf(ResultSet sqlIncrpfRs) throws SQLException {
		Incrpf incrpfnew = new Incrpf();
		incrpfnew.setUniqueNumber(sqlIncrpfRs.getInt("UNIQUE_NUMBER"));
		incrpfnew.setChdrcoy(sqlIncrpfRs.getString("CHDRCOY"));
		incrpfnew.setChdrnum(sqlIncrpfRs.getString("CHDRNUM"));
		incrpfnew.setLife(sqlIncrpfRs.getString("LIFE"));
		incrpfnew.setJlife(sqlIncrpfRs.getString("JLIFE"));
		incrpfnew.setCoverage(sqlIncrpfRs.getString("COVERAGE"));
		incrpfnew.setRider(sqlIncrpfRs.getString("RIDER"));
		incrpfnew.setPlnsfx(sqlIncrpfRs.getInt("PLNSFX"));
		incrpfnew.setTranno(sqlIncrpfRs.getInt("TRANNO"));
		incrpfnew.setValidflag(sqlIncrpfRs.getString("VALIDFLAG"));
		incrpfnew.setStatcode(sqlIncrpfRs.getString("STATCODE"));
		incrpfnew.setPstatcode(sqlIncrpfRs.getString("PSTATCODE"));
		incrpfnew.setCrrcd(sqlIncrpfRs.getInt("CRRCD"));
		incrpfnew.setCrtable(sqlIncrpfRs.getString("CRTABLE"));
		incrpfnew.setOrigInst(sqlIncrpfRs.getBigDecimal("ORIGINST"));
		incrpfnew.setLastInst(sqlIncrpfRs.getBigDecimal("LASTINST"));
		incrpfnew.setNewinst(sqlIncrpfRs.getBigDecimal("NEWINST"));
		incrpfnew.setOrigSum(sqlIncrpfRs.getBigDecimal("ORIGSUM"));
		incrpfnew.setLastSum(sqlIncrpfRs.getBigDecimal("LASTSUM"));
		incrpfnew.setNewsum(sqlIncrpfRs.getBigDecimal("NEWSUM"));
		incrpfnew.setAnniversaryMethod(sqlIncrpfRs.getString("ANNVRY"));
		incrpfnew.setBasicCommMeth(sqlIncrpfRs.getString("BASCMETH"));
		incrpfnew.setBascpy(sqlIncrpfRs.getString("BASCPY"));
		incrpfnew.setPctinc(sqlIncrpfRs.getInt("PCTINC"));
		incrpfnew.setRefusalFlag(sqlIncrpfRs.getString("REFFLAG"));
		incrpfnew.setCeaseInd(sqlIncrpfRs.getString("CEASEIND"));
		incrpfnew.setTermid(sqlIncrpfRs.getString("TERMID"));
		incrpfnew.setTransactionDate(sqlIncrpfRs.getInt("TRDT"));
		incrpfnew.setTransactionTime(sqlIncrpfRs.getInt("TRTM"));
		return incrpfnew;
	}
	//IJTI-1726 ends
    public Map<String, List<Incrpf>> getIncrgpMap(String coy, List<String> chdrnumList) {
        StringBuilder sb = new StringBuilder("");
        sb.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, EFFDATE, TRANNO, VALIDFLAG, STATCODE, PSTATCODE, CRRCD, CRTABLE, ORIGINST, LASTINST, NEWINST, ORIGSUM, LASTSUM, NEWSUM, ANNVRY, BASCMETH, PCTINC, REFFLAG, TRDT, TRTM, USER_T, TERMID, BASCPY, CEASEIND, RNWCPY, SRVCPY, ZBORIGINST, ZBLASTINST, ZBNEWINST, ZLORIGINST, ZLLASTINST, ZLNEWINST, USRPRF, JOBNM, DATIME  ");
        sb.append("FROM VM1DTA.INCRPF WHERE VALIDFLAG = '1' AND CHDRCOY=? AND ");
        sb.append(getSqlInStr("CHDRNUM", chdrnumList));
        sb.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, UNIQUE_NUMBER DESC ");    
        LOGGER.info(sb.toString());	
        PreparedStatement ps = getPrepareStatement(sb.toString());
        ResultSet rs = null;
        Map<String, List<Incrpf>> incrMap = new HashMap<String, List<Incrpf>>();
        try {
            ps.setInt(1, Integer.parseInt(coy));
            rs = executeQuery(ps);

            while (rs.next()) {
            	Incrpf incrpf = new Incrpf();
                incrpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
                incrpf.setChdrcoy(rs.getString("CHDRCOY"));
                incrpf.setChdrnum(rs.getString("CHDRNUM"));
                incrpf.setLife(rs.getString("LIFE"));
                incrpf.setJlife(rs.getString("JLIFE"));                
                incrpf.setCoverage(rs.getString("COVERAGE"));
                incrpf.setRider(rs.getString("RIDER"));               
                incrpf.setPlnsfx(rs.getInt("PLNSFX"));
                incrpf.setEffdate(rs.getInt("EFFDATE"));                
                incrpf.setTranno(rs.getInt("TRANNO"));                               
                incrpf.setValidflag(rs.getString("VALIDFLAG"));
                incrpf.setStatcode(rs.getString("STATCODE"));
                incrpf.setPstatcode(rs.getString("PSTATCODE"));
                incrpf.setCrrcd(rs.getInt("CRRCD"));
                incrpf.setCrtable(rs.getString("CRTABLE"));
                incrpf.setOrigInst(rs.getBigDecimal("ORIGINST"));
                incrpf.setLastInst(rs.getBigDecimal("LASTINST"));
                incrpf.setNewinst(rs.getBigDecimal("NEWINST"));
                incrpf.setOrigSum(rs.getBigDecimal("ORIGSUM"));
                incrpf.setLastSum(rs.getBigDecimal("LASTSUM"));
                incrpf.setNewsum(rs.getBigDecimal("NEWSUM"));
                incrpf.setAnniversaryMethod(rs.getString("ANNVRY"));
                incrpf.setBasicCommMeth(rs.getString("BASCMETH"));                
                incrpf.setPctinc(rs.getInt("PCTINC"));
                incrpf.setRefusalFlag(rs.getString("REFFLAG"));
                incrpf.setTransactionDate(rs.getInt("TRDT"));
                incrpf.setTransactionTime(rs.getInt("TRTM"));
                incrpf.setUser(rs.getInt("USER_T"));
                incrpf.setTermid(rs.getString("TERMID")); 
                incrpf.setBascpy(rs.getString("BASCPY"));                
                incrpf.setCeaseInd(rs.getString("CEASEIND"));                             
                incrpf.setRnwcpy(rs.getString("RNWCPY"));                
                incrpf.setSrvcpy(rs.getString("SRVCPY"));
                incrpf.setZboriginst(rs.getBigDecimal("ZBORIGINST"));
                incrpf.setZblastinst(rs.getBigDecimal("ZBLASTINST"));
                incrpf.setZbnewinst(rs.getBigDecimal("ZBNEWINST"));
                incrpf.setZloriginst(rs.getBigDecimal("ZLORIGINST"));
                incrpf.setZllastinst(rs.getBigDecimal("ZLLASTINST"));
                incrpf.setZlnewinst(rs.getBigDecimal("ZLNEWINST"));                
                incrpf.setUserProfile(rs.getString("USRPRF"));
                incrpf.setJobName(rs.getString("JOBNM"));                
                incrpf.setDatime(rs.getDate("DATIME"));                

                if (incrMap.containsKey(incrpf.getChdrnum())) {
                	incrMap.get(incrpf.getChdrnum()).add(incrpf);
                } else {
                    List<Incrpf> incrList = new ArrayList<Incrpf>();
                    incrList.add(incrpf);
                    incrMap.put(incrpf.getChdrnum(), incrList);
                }
            }

        } catch (SQLException e) {
			LOGGER.error("getIncrgpMap()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return incrMap;

    }				

	  public Map<String, List<Incrpf>> searchIncrRecordByChdrnum(List<String> chdrnumList){
	    	
			StringBuilder sqlSelect1 = new StringBuilder();
			sqlSelect1.append("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,EFFDATE,TRANNO,VALIDFLAG,STATCODE,PSTATCODE,CRRCD,CRTABLE,ORIGINST,LASTINST,NEWINST,ORIGSUM,LASTSUM,NEWSUM,ANNVRY,BASCMETH,PCTINC,REFFLAG,TRDT,TRTM,TERMID,BASCPY,CEASEIND,RNWCPY,SRVCPY,ZBORIGINST,ZBLASTINST,ZBNEWINST,ZLORIGINST,ZLLASTINST,ZLNEWINST,ZSTPDUTY01  ");
		    sqlSelect1.append("FROM INCRPF WHERE  ");
		    sqlSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		    sqlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");
		 
		    PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
		    ResultSet sqlrs = null;
		    Map<String, List<Incrpf>> resultMap = new HashMap<>();
		    
		    try {
		    	sqlrs = executeQuery(psSelect);
		
		        while (sqlrs.next()) {
		        	
					Incrpf incrpf = new Incrpf();
					incrpf.setUniqueNumber(sqlrs.getLong("Unique_number"));
					incrpf.setChdrcoy(sqlrs.getString("chdrcoy"));
					incrpf.setChdrnum(sqlrs.getString("chdrnum"));
					incrpf.setLife(sqlrs.getString("life"));
					incrpf.setJlife(sqlrs.getString("jlife"));
					incrpf.setCoverage(sqlrs.getString("coverage"));
					incrpf.setRider(sqlrs.getString("rider"));
					incrpf.setPlnsfx(sqlrs.getInt("plnsfx"));
					incrpf.setEffdate(sqlrs.getInt("effdate"));
					incrpf.setTranno(sqlrs.getInt("tranno"));
					incrpf.setValidflag(sqlrs.getString("validflag"));
					incrpf.setStatcode(sqlrs.getString("statcode"));
					incrpf.setPstatcode(sqlrs.getString("pstatcode"));
					incrpf.setCrrcd(sqlrs.getInt("crrcd"));
					incrpf.setCrtable(sqlrs.getString("crtable"));
					incrpf.setOrigInst(sqlrs.getBigDecimal("originst"));
					incrpf.setLastInst(sqlrs.getBigDecimal("lastinst"));
					incrpf.setNewinst(sqlrs.getBigDecimal("newinst"));
					incrpf.setOrigSum(sqlrs.getBigDecimal("origsum"));
					incrpf.setLastSum(sqlrs.getBigDecimal("lastsum"));
					incrpf.setNewsum(sqlrs.getBigDecimal("newsum"));
					incrpf.setAnniversaryMethod(sqlrs.getString("annvry"));
					incrpf.setBasicCommMeth(sqlrs.getString("bascmeth"));
					incrpf.setPctinc(sqlrs.getInt("pctinc"));
					incrpf.setRefusalFlag(sqlrs.getString("refflag"));
					incrpf.setTransactionDate(sqlrs.getInt("trdt"));
					incrpf.setTransactionTime(sqlrs.getInt("trtm"));
					incrpf.setTermid(sqlrs.getString("termid"));
					incrpf.setBascpy(sqlrs.getString("bascpy"));
					incrpf.setCeaseInd(sqlrs.getString("ceaseind"));
					incrpf.setRnwcpy(sqlrs.getString("rnwcpy"));
					incrpf.setSrvcpy(sqlrs.getString("srvcpy"));
					incrpf.setZboriginst(sqlrs.getBigDecimal("zboriginst"));
					incrpf.setZblastinst(sqlrs.getBigDecimal("zblastinst"));
					incrpf.setZbnewinst(sqlrs.getBigDecimal("zbnewinst"));
					incrpf.setZloriginst(sqlrs.getBigDecimal("zloriginst"));
					incrpf.setZllastinst(sqlrs.getBigDecimal("zllastinst"));
					incrpf.setZlnewinst(sqlrs.getBigDecimal("zlnewinst"));
					incrpf.setZstpduty01(sqlrs.getBigDecimal("zstpduty01"));
					
					if (resultMap.containsKey(incrpf.getChdrnum())) {
						resultMap.get(incrpf.getChdrnum()).add(incrpf);
					} else {
						List<Incrpf> incrpfList = new ArrayList<>();
						incrpfList.add(incrpf);
						resultMap.put(incrpf.getChdrnum(), incrpfList);
					}
		        }
		    } catch (SQLException e) {
			LOGGER.error("searchIncrRecordByChdrnum()", e); /* IJTI-1479 */
		        throw new SQLRuntimeException(e);
		    } finally {
		        close(psSelect, sqlrs);
		    }
			return resultMap;
		}
	  
	  @Override
    public void updateValidFlag(List<Incrpf> incrpfList) {
        if (incrpfList != null && incrpfList.size() > 0) {
            String SQL_COVR_UPDATE = "UPDATE INCRPF SET VALIDFLAG=? ,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";

            PreparedStatement psIncrUpdate = getPrepareStatement(SQL_COVR_UPDATE);
            try {
                for (Incrpf c : incrpfList) {
                    psIncrUpdate.setString(1, c.getValidflag());
                    psIncrUpdate.setString(2, getJobnm());
                    psIncrUpdate.setString(3, getUsrprf());
                    psIncrUpdate.setTimestamp(4, getDatime());
                    psIncrUpdate.setLong(5, c.getUniqueNumber());
                    psIncrUpdate.addBatch();
                }
                psIncrUpdate.executeBatch();
            } catch (SQLException e) {
				LOGGER.error("updateValidFlag()", e); /* IJTI-1479 */
                throw new SQLRuntimeException(e);
            } finally {
                close(psIncrUpdate, null);
            }
        }
    }
	  public Incrpf getIncrByCrrcdDate(String chdrcoy,String chdrnum,  String life, String coverage,String rider, int planSuffix, int cpidate, String validflag) {
			Incrpf incr = null;
			StringBuilder sb = new StringBuilder("");
			sb.append("SELECT CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, EFFDATE, TRANNO, VALIDFLAG, STATCODE, PSTATCODE, CRRCD, CRTABLE, TRDT, TRTM, TERMID,UNIQUE_NUMBER, ORIGSUM, NEWSUM  FROM INCRPF ");
			sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? AND   LIFE = ? AND COVERAGE = ? ");
			sb.append(" AND RIDER = ? AND PLNSFX = ? and CRRCD <= ? and VALIDFLAG= ? ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
			LOGGER.info(sb.toString());
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				
				ps = getPrepareStatement(sb.toString());
				ps.setString(1, chdrcoy);
				ps.setString(2, chdrnum);
				ps.setString(3, life);
				ps.setString(4, coverage);	
				ps.setString(5, rider);
				ps.setInt(6, planSuffix);
				ps.setInt(7,cpidate);
				ps.setString(8, validflag);
				rs = ps.executeQuery();
				while(rs.next()){
				incr = new Incrpf();
				incr.setChdrcoy(rs.getString(1));
				incr.setChdrcoy(rs.getString(2));
				incr.setLife(rs.getString(3));
				incr.setJlife(rs.getString(4));
				incr.setCoverage(rs.getString(5));
				incr.setRider(rs.getString(6));
				incr.setPlnsfx(rs.getInt(7));
				incr.setEffdate(rs.getInt(8));
				incr.setTranno(rs.getInt(9));
				incr.setValidflag(rs.getString(10));
				incr.setStatcode(rs.getString(11));
				incr.setPstatcode(rs.getString(12));
				incr.setCrrcd(rs.getInt(13));
				incr.setCrtable(rs.getString(14));
				incr.setTransactionDate(rs.getInt(15));
				incr.setTransactionTime(rs.getInt(16));
				incr.setTermid(rs.getString(17));
				incr.setUniqueNumber(rs.getLong(18));
				incr.setOrigSum(rs.getBigDecimal(19));
				incr.setNewsum(rs.getBigDecimal(20));
				break;
				}
			} catch (SQLException e) {
			LOGGER.error("getIncrByCrrcdDate()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);
			}
			return incr;
		}

	  public Map<Integer, List<Incrpf>> getIncrDatedMap(String coy, String chdrnum) {
	        StringBuilder sb = new StringBuilder("");
	        sb.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, EFFDATE, TRANNO, VALIDFLAG, STATCODE, PSTATCODE, CRRCD, CRTABLE, ORIGINST, LASTINST, NEWINST, ORIGSUM, LASTSUM, NEWSUM, ANNVRY, BASCMETH, PCTINC, REFFLAG, TRDT, TRTM, USER_T, TERMID, BASCPY, CEASEIND, RNWCPY, SRVCPY, ZBORIGINST, ZBLASTINST, ZBNEWINST, ZLORIGINST, ZLLASTINST, ZLNEWINST, USRPRF, JOBNM, DATIME  ");
	        sb.append("FROM VM1DTA.INCRPF WHERE  CHDRCOY=? AND CHDRNUM=? ");
	        sb.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, UNIQUE_NUMBER DESC ");    
	        LOGGER.info(sb.toString());	
	        PreparedStatement ps = getPrepareStatement(sb.toString());
	        ResultSet rs = null;
	        Map<Integer, List<Incrpf>> incrMap = new HashMap<Integer, List<Incrpf>>();
	        try {
	            ps.setInt(1, Integer.parseInt(coy));
	            ps.setInt(2, Integer.parseInt(chdrnum));
	            rs = executeQuery(ps);

	            while (rs.next()) {
	            	Incrpf incrpf = new Incrpf();
	                incrpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
	                incrpf.setChdrcoy(rs.getString("CHDRCOY"));
	                incrpf.setChdrnum(rs.getString("CHDRNUM"));
	                incrpf.setLife(rs.getString("LIFE"));
	                incrpf.setJlife(rs.getString("JLIFE"));                
	                incrpf.setCoverage(rs.getString("COVERAGE"));
	                incrpf.setRider(rs.getString("RIDER"));               
	                incrpf.setPlnsfx(rs.getInt("PLNSFX"));
	                incrpf.setEffdate(rs.getInt("EFFDATE"));                
	                incrpf.setTranno(rs.getInt("TRANNO"));                               
	                incrpf.setValidflag(rs.getString("VALIDFLAG"));
	                incrpf.setStatcode(rs.getString("STATCODE"));
	                incrpf.setPstatcode(rs.getString("PSTATCODE"));
	                incrpf.setCrrcd(rs.getInt("CRRCD"));
	                incrpf.setCrtable(rs.getString("CRTABLE"));
	                incrpf.setOrigInst(rs.getBigDecimal("ORIGINST"));
	                incrpf.setLastInst(rs.getBigDecimal("LASTINST"));
	                incrpf.setNewinst(rs.getBigDecimal("NEWINST"));
	                incrpf.setOrigSum(rs.getBigDecimal("ORIGSUM"));
	                incrpf.setLastSum(rs.getBigDecimal("LASTSUM"));
	                incrpf.setNewsum(rs.getBigDecimal("NEWSUM"));
	                incrpf.setAnniversaryMethod(rs.getString("ANNVRY"));
	                incrpf.setBasicCommMeth(rs.getString("BASCMETH"));                
	                incrpf.setPctinc(rs.getInt("PCTINC"));
	                incrpf.setRefusalFlag(rs.getString("REFFLAG"));
	                incrpf.setTransactionDate(rs.getInt("TRDT"));
	                incrpf.setTransactionTime(rs.getInt("TRTM"));
	                incrpf.setUser(rs.getInt("USER_T"));
	                incrpf.setTermid(rs.getString("TERMID")); 
	                incrpf.setBascpy(rs.getString("BASCPY"));                
	                incrpf.setCeaseInd(rs.getString("CEASEIND"));                             
	                incrpf.setRnwcpy(rs.getString("RNWCPY"));                
	                incrpf.setSrvcpy(rs.getString("SRVCPY"));
	                incrpf.setZboriginst(rs.getBigDecimal("ZBORIGINST"));
	                incrpf.setZblastinst(rs.getBigDecimal("ZBLASTINST"));
	                incrpf.setZbnewinst(rs.getBigDecimal("ZBNEWINST"));
	                incrpf.setZloriginst(rs.getBigDecimal("ZLORIGINST"));
	                incrpf.setZllastinst(rs.getBigDecimal("ZLLASTINST"));
	                incrpf.setZlnewinst(rs.getBigDecimal("ZLNEWINST"));                
	                incrpf.setUserProfile(rs.getString("USRPRF"));
	                incrpf.setJobName(rs.getString("JOBNM"));                
	                incrpf.setDatime(rs.getDate("DATIME"));                

	                if (incrMap.containsKey(incrpf.getCrrcd())) {
	                	incrMap.get(incrpf.getCrrcd()).add(incrpf);
	                } else {
	                    List<Incrpf> incrList = new ArrayList<Incrpf>();
	                    incrList.add(incrpf);
	                    incrMap.put(incrpf.getCrrcd(), incrList);
	                }
	            }

	        } catch (SQLException e) {
			LOGGER.error("getIncrDatedMap()", e); /* IJTI-1479 */
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, rs);
	        }
	        return incrMap;

	    }				
	  public void updateIncrList(List<Incrpf> incrpfUpdateList) {//ILIFE-8509
		  String sql = "UPDATE INCRPF SET STATCODE=?, PSTATCODE=?, TRANNO=?, JOBNM=?, USRPRF=?, DATIME=? WHERE UNIQUE_NUMBER=? ";

          PreparedStatement psIncrUpdate = getPrepareStatement(sql);
          try {
        	  int i = 1;
              for (Incrpf incr : incrpfUpdateList) {
                  psIncrUpdate.setString(i++, incr.getStatcode());
                  psIncrUpdate.setString(i++, incr.getPstatcode());
                  psIncrUpdate.setInt(i++, incr.getTranno());
                  psIncrUpdate.setString(i++, getJobnm());
                  psIncrUpdate.setString(i++, getUsrprf());
                  psIncrUpdate.setTimestamp(i++, getDatime());
                  psIncrUpdate.setLong(i++, incr.getUniqueNumber());
                  psIncrUpdate.addBatch();
              }
              psIncrUpdate.executeBatch();
          } catch (SQLException e) {
			LOGGER.error("updateIncrList()", e); /* IJTI-1479 */
              throw new SQLRuntimeException(e);
          } finally {
              close(psIncrUpdate, null);
          }
	  }
}
