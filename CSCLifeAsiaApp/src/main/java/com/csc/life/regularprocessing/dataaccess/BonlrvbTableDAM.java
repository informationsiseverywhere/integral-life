package com.csc.life.regularprocessing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: BonlrvbTableDAM.java
 * Date: Sun, 30 Aug 2009 03:30:04
 * Class transformed from BONLRVB.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class BonlrvbTableDAM extends BonlpfTableDAM {

	public BonlrvbTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("BONLRVB");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "DATETEXC, " +
		            "VALIDFLAG, " +
		            "LONGDESC01, " +
		            "LONGDESC02, " +
		            "LONGDESC03, " +
		            "LONGDESC04, " +
		            "LONGDESC05, " +
		            "LONGDESC06, " +
		            "LONGDESC07, " +
		            "LONGDESC08, " +
		            "LONGDESC09, " +
		            "LONGDESC10, " +
		            "SUMINS01, " +
		            "SUMINS02, " +
		            "SUMINS03, " +
		            "SUMINS04, " +
		            "SUMINS05, " +
		            "SUMINS06, " +
		            "SUMINS07, " +
		            "SUMINS08, " +
		            "SUMINS09, " +
		            "SUMINS10, " +
		            "BCALMETH01, " +
		            "BCALMETH02, " +
		            "BCALMETH03, " +
		            "BCALMETH04, " +
		            "BCALMETH05, " +
		            "BCALMETH06, " +
		            "BCALMETH07, " +
		            "BCALMETH08, " +
		            "BCALMETH09, " +
		            "BCALMETH10, " +
		            "BPAYTY01, " +
		            "BPAYTY02, " +
		            "BPAYTY03, " +
		            "BPAYTY04, " +
		            "BPAYTY05, " +
		            "BPAYTY06, " +
		            "BPAYTY07, " +
		            "BPAYTY08, " +
		            "BPAYTY09, " +
		            "BPAYTY10, " +
		            "BPAYNY01, " +
		            "BPAYNY02, " +
		            "BPAYNY03, " +
		            "BPAYNY04, " +
		            "BPAYNY05, " +
		            "BPAYNY06, " +
		            "BPAYNY07, " +
		            "BPAYNY08, " +
		            "BPAYNY09, " +
		            "BPAYNY10, " +
		            "TOTBON01, " +
		            "TOTBON02, " +
		            "TOTBON03, " +
		            "TOTBON04, " +
		            "TOTBON05, " +
		            "TOTBON06, " +
		            "TOTBON07, " +
		            "TOTBON08, " +
		            "TOTBON09, " +
		            "TOTBON10, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               currfrom,
                               currto,
                               datetexc,
                               validflag,
                               longdesc01,
                               longdesc02,
                               longdesc03,
                               longdesc04,
                               longdesc05,
                               longdesc06,
                               longdesc07,
                               longdesc08,
                               longdesc09,
                               longdesc10,
                               sumins01,
                               sumins02,
                               sumins03,
                               sumins04,
                               sumins05,
                               sumins06,
                               sumins07,
                               sumins08,
                               sumins09,
                               sumins10,
                               bonusCalcMeth01,
                               bonusCalcMeth02,
                               bonusCalcMeth03,
                               bonusCalcMeth04,
                               bonusCalcMeth05,
                               bonusCalcMeth06,
                               bonusCalcMeth07,
                               bonusCalcMeth08,
                               bonusCalcMeth09,
                               bonusCalcMeth10,
                               bonPayThisYr01,
                               bonPayThisYr02,
                               bonPayThisYr03,
                               bonPayThisYr04,
                               bonPayThisYr05,
                               bonPayThisYr06,
                               bonPayThisYr07,
                               bonPayThisYr08,
                               bonPayThisYr09,
                               bonPayThisYr10,
                               bonPayLastYr01,
                               bonPayLastYr02,
                               bonPayLastYr03,
                               bonPayLastYr04,
                               bonPayLastYr05,
                               bonPayLastYr06,
                               bonPayLastYr07,
                               bonPayLastYr08,
                               bonPayLastYr09,
                               bonPayLastYr10,
                               totalBonus01,
                               totalBonus02,
                               totalBonus03,
                               totalBonus04,
                               totalBonus05,
                               totalBonus06,
                               totalBonus07,
                               totalBonus08,
                               totalBonus09,
                               totalBonus10,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(55);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(834);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getDatetexc().toInternal()
					+ getValidflag().toInternal()
					+ getLongdesc01().toInternal()
					+ getLongdesc02().toInternal()
					+ getLongdesc03().toInternal()
					+ getLongdesc04().toInternal()
					+ getLongdesc05().toInternal()
					+ getLongdesc06().toInternal()
					+ getLongdesc07().toInternal()
					+ getLongdesc08().toInternal()
					+ getLongdesc09().toInternal()
					+ getLongdesc10().toInternal()
					+ getSumins01().toInternal()
					+ getSumins02().toInternal()
					+ getSumins03().toInternal()
					+ getSumins04().toInternal()
					+ getSumins05().toInternal()
					+ getSumins06().toInternal()
					+ getSumins07().toInternal()
					+ getSumins08().toInternal()
					+ getSumins09().toInternal()
					+ getSumins10().toInternal()
					+ getBonusCalcMeth01().toInternal()
					+ getBonusCalcMeth02().toInternal()
					+ getBonusCalcMeth03().toInternal()
					+ getBonusCalcMeth04().toInternal()
					+ getBonusCalcMeth05().toInternal()
					+ getBonusCalcMeth06().toInternal()
					+ getBonusCalcMeth07().toInternal()
					+ getBonusCalcMeth08().toInternal()
					+ getBonusCalcMeth09().toInternal()
					+ getBonusCalcMeth10().toInternal()
					+ getBonPayThisYr01().toInternal()
					+ getBonPayThisYr02().toInternal()
					+ getBonPayThisYr03().toInternal()
					+ getBonPayThisYr04().toInternal()
					+ getBonPayThisYr05().toInternal()
					+ getBonPayThisYr06().toInternal()
					+ getBonPayThisYr07().toInternal()
					+ getBonPayThisYr08().toInternal()
					+ getBonPayThisYr09().toInternal()
					+ getBonPayThisYr10().toInternal()
					+ getBonPayLastYr01().toInternal()
					+ getBonPayLastYr02().toInternal()
					+ getBonPayLastYr03().toInternal()
					+ getBonPayLastYr04().toInternal()
					+ getBonPayLastYr05().toInternal()
					+ getBonPayLastYr06().toInternal()
					+ getBonPayLastYr07().toInternal()
					+ getBonPayLastYr08().toInternal()
					+ getBonPayLastYr09().toInternal()
					+ getBonPayLastYr10().toInternal()
					+ getTotalBonus01().toInternal()
					+ getTotalBonus02().toInternal()
					+ getTotalBonus03().toInternal()
					+ getTotalBonus04().toInternal()
					+ getTotalBonus05().toInternal()
					+ getTotalBonus06().toInternal()
					+ getTotalBonus07().toInternal()
					+ getTotalBonus08().toInternal()
					+ getTotalBonus09().toInternal()
					+ getTotalBonus10().toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, datetexc);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, longdesc01);
			what = ExternalData.chop(what, longdesc02);
			what = ExternalData.chop(what, longdesc03);
			what = ExternalData.chop(what, longdesc04);
			what = ExternalData.chop(what, longdesc05);
			what = ExternalData.chop(what, longdesc06);
			what = ExternalData.chop(what, longdesc07);
			what = ExternalData.chop(what, longdesc08);
			what = ExternalData.chop(what, longdesc09);
			what = ExternalData.chop(what, longdesc10);
			what = ExternalData.chop(what, sumins01);
			what = ExternalData.chop(what, sumins02);
			what = ExternalData.chop(what, sumins03);
			what = ExternalData.chop(what, sumins04);
			what = ExternalData.chop(what, sumins05);
			what = ExternalData.chop(what, sumins06);
			what = ExternalData.chop(what, sumins07);
			what = ExternalData.chop(what, sumins08);
			what = ExternalData.chop(what, sumins09);
			what = ExternalData.chop(what, sumins10);
			what = ExternalData.chop(what, bonusCalcMeth01);
			what = ExternalData.chop(what, bonusCalcMeth02);
			what = ExternalData.chop(what, bonusCalcMeth03);
			what = ExternalData.chop(what, bonusCalcMeth04);
			what = ExternalData.chop(what, bonusCalcMeth05);
			what = ExternalData.chop(what, bonusCalcMeth06);
			what = ExternalData.chop(what, bonusCalcMeth07);
			what = ExternalData.chop(what, bonusCalcMeth08);
			what = ExternalData.chop(what, bonusCalcMeth09);
			what = ExternalData.chop(what, bonusCalcMeth10);
			what = ExternalData.chop(what, bonPayThisYr01);
			what = ExternalData.chop(what, bonPayThisYr02);
			what = ExternalData.chop(what, bonPayThisYr03);
			what = ExternalData.chop(what, bonPayThisYr04);
			what = ExternalData.chop(what, bonPayThisYr05);
			what = ExternalData.chop(what, bonPayThisYr06);
			what = ExternalData.chop(what, bonPayThisYr07);
			what = ExternalData.chop(what, bonPayThisYr08);
			what = ExternalData.chop(what, bonPayThisYr09);
			what = ExternalData.chop(what, bonPayThisYr10);
			what = ExternalData.chop(what, bonPayLastYr01);
			what = ExternalData.chop(what, bonPayLastYr02);
			what = ExternalData.chop(what, bonPayLastYr03);
			what = ExternalData.chop(what, bonPayLastYr04);
			what = ExternalData.chop(what, bonPayLastYr05);
			what = ExternalData.chop(what, bonPayLastYr06);
			what = ExternalData.chop(what, bonPayLastYr07);
			what = ExternalData.chop(what, bonPayLastYr08);
			what = ExternalData.chop(what, bonPayLastYr09);
			what = ExternalData.chop(what, bonPayLastYr10);
			what = ExternalData.chop(what, totalBonus01);
			what = ExternalData.chop(what, totalBonus02);
			what = ExternalData.chop(what, totalBonus03);
			what = ExternalData.chop(what, totalBonus04);
			what = ExternalData.chop(what, totalBonus05);
			what = ExternalData.chop(what, totalBonus06);
			what = ExternalData.chop(what, totalBonus07);
			what = ExternalData.chop(what, totalBonus08);
			what = ExternalData.chop(what, totalBonus09);
			what = ExternalData.chop(what, totalBonus10);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public FixedLengthStringData getDatetexc() {
		return datetexc;
	}
	public void setDatetexc(Object what) {
		datetexc.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getLongdesc01() {
		return longdesc01;
	}
	public void setLongdesc01(Object what) {
		longdesc01.set(what);
	}	
	public FixedLengthStringData getLongdesc02() {
		return longdesc02;
	}
	public void setLongdesc02(Object what) {
		longdesc02.set(what);
	}	
	public FixedLengthStringData getLongdesc03() {
		return longdesc03;
	}
	public void setLongdesc03(Object what) {
		longdesc03.set(what);
	}	
	public FixedLengthStringData getLongdesc04() {
		return longdesc04;
	}
	public void setLongdesc04(Object what) {
		longdesc04.set(what);
	}	
	public FixedLengthStringData getLongdesc05() {
		return longdesc05;
	}
	public void setLongdesc05(Object what) {
		longdesc05.set(what);
	}	
	public FixedLengthStringData getLongdesc06() {
		return longdesc06;
	}
	public void setLongdesc06(Object what) {
		longdesc06.set(what);
	}	
	public FixedLengthStringData getLongdesc07() {
		return longdesc07;
	}
	public void setLongdesc07(Object what) {
		longdesc07.set(what);
	}	
	public FixedLengthStringData getLongdesc08() {
		return longdesc08;
	}
	public void setLongdesc08(Object what) {
		longdesc08.set(what);
	}	
	public FixedLengthStringData getLongdesc09() {
		return longdesc09;
	}
	public void setLongdesc09(Object what) {
		longdesc09.set(what);
	}	
	public FixedLengthStringData getLongdesc10() {
		return longdesc10;
	}
	public void setLongdesc10(Object what) {
		longdesc10.set(what);
	}	
	public PackedDecimalData getSumins01() {
		return sumins01;
	}
	public void setSumins01(Object what) {
		setSumins01(what, false);
	}
	public void setSumins01(Object what, boolean rounded) {
		if (rounded)
			sumins01.setRounded(what);
		else
			sumins01.set(what);
	}	
	public PackedDecimalData getSumins02() {
		return sumins02;
	}
	public void setSumins02(Object what) {
		setSumins02(what, false);
	}
	public void setSumins02(Object what, boolean rounded) {
		if (rounded)
			sumins02.setRounded(what);
		else
			sumins02.set(what);
	}	
	public PackedDecimalData getSumins03() {
		return sumins03;
	}
	public void setSumins03(Object what) {
		setSumins03(what, false);
	}
	public void setSumins03(Object what, boolean rounded) {
		if (rounded)
			sumins03.setRounded(what);
		else
			sumins03.set(what);
	}	
	public PackedDecimalData getSumins04() {
		return sumins04;
	}
	public void setSumins04(Object what) {
		setSumins04(what, false);
	}
	public void setSumins04(Object what, boolean rounded) {
		if (rounded)
			sumins04.setRounded(what);
		else
			sumins04.set(what);
	}	
	public PackedDecimalData getSumins05() {
		return sumins05;
	}
	public void setSumins05(Object what) {
		setSumins05(what, false);
	}
	public void setSumins05(Object what, boolean rounded) {
		if (rounded)
			sumins05.setRounded(what);
		else
			sumins05.set(what);
	}	
	public PackedDecimalData getSumins06() {
		return sumins06;
	}
	public void setSumins06(Object what) {
		setSumins06(what, false);
	}
	public void setSumins06(Object what, boolean rounded) {
		if (rounded)
			sumins06.setRounded(what);
		else
			sumins06.set(what);
	}	
	public PackedDecimalData getSumins07() {
		return sumins07;
	}
	public void setSumins07(Object what) {
		setSumins07(what, false);
	}
	public void setSumins07(Object what, boolean rounded) {
		if (rounded)
			sumins07.setRounded(what);
		else
			sumins07.set(what);
	}	
	public PackedDecimalData getSumins08() {
		return sumins08;
	}
	public void setSumins08(Object what) {
		setSumins08(what, false);
	}
	public void setSumins08(Object what, boolean rounded) {
		if (rounded)
			sumins08.setRounded(what);
		else
			sumins08.set(what);
	}	
	public PackedDecimalData getSumins09() {
		return sumins09;
	}
	public void setSumins09(Object what) {
		setSumins09(what, false);
	}
	public void setSumins09(Object what, boolean rounded) {
		if (rounded)
			sumins09.setRounded(what);
		else
			sumins09.set(what);
	}	
	public PackedDecimalData getSumins10() {
		return sumins10;
	}
	public void setSumins10(Object what) {
		setSumins10(what, false);
	}
	public void setSumins10(Object what, boolean rounded) {
		if (rounded)
			sumins10.setRounded(what);
		else
			sumins10.set(what);
	}	
	public FixedLengthStringData getBonusCalcMeth01() {
		return bonusCalcMeth01;
	}
	public void setBonusCalcMeth01(Object what) {
		bonusCalcMeth01.set(what);
	}	
	public FixedLengthStringData getBonusCalcMeth02() {
		return bonusCalcMeth02;
	}
	public void setBonusCalcMeth02(Object what) {
		bonusCalcMeth02.set(what);
	}	
	public FixedLengthStringData getBonusCalcMeth03() {
		return bonusCalcMeth03;
	}
	public void setBonusCalcMeth03(Object what) {
		bonusCalcMeth03.set(what);
	}	
	public FixedLengthStringData getBonusCalcMeth04() {
		return bonusCalcMeth04;
	}
	public void setBonusCalcMeth04(Object what) {
		bonusCalcMeth04.set(what);
	}	
	public FixedLengthStringData getBonusCalcMeth05() {
		return bonusCalcMeth05;
	}
	public void setBonusCalcMeth05(Object what) {
		bonusCalcMeth05.set(what);
	}	
	public FixedLengthStringData getBonusCalcMeth06() {
		return bonusCalcMeth06;
	}
	public void setBonusCalcMeth06(Object what) {
		bonusCalcMeth06.set(what);
	}	
	public FixedLengthStringData getBonusCalcMeth07() {
		return bonusCalcMeth07;
	}
	public void setBonusCalcMeth07(Object what) {
		bonusCalcMeth07.set(what);
	}	
	public FixedLengthStringData getBonusCalcMeth08() {
		return bonusCalcMeth08;
	}
	public void setBonusCalcMeth08(Object what) {
		bonusCalcMeth08.set(what);
	}	
	public FixedLengthStringData getBonusCalcMeth09() {
		return bonusCalcMeth09;
	}
	public void setBonusCalcMeth09(Object what) {
		bonusCalcMeth09.set(what);
	}	
	public FixedLengthStringData getBonusCalcMeth10() {
		return bonusCalcMeth10;
	}
	public void setBonusCalcMeth10(Object what) {
		bonusCalcMeth10.set(what);
	}	
	public PackedDecimalData getBonPayThisYr01() {
		return bonPayThisYr01;
	}
	public void setBonPayThisYr01(Object what) {
		setBonPayThisYr01(what, false);
	}
	public void setBonPayThisYr01(Object what, boolean rounded) {
		if (rounded)
			bonPayThisYr01.setRounded(what);
		else
			bonPayThisYr01.set(what);
	}	
	public PackedDecimalData getBonPayThisYr02() {
		return bonPayThisYr02;
	}
	public void setBonPayThisYr02(Object what) {
		setBonPayThisYr02(what, false);
	}
	public void setBonPayThisYr02(Object what, boolean rounded) {
		if (rounded)
			bonPayThisYr02.setRounded(what);
		else
			bonPayThisYr02.set(what);
	}	
	public PackedDecimalData getBonPayThisYr03() {
		return bonPayThisYr03;
	}
	public void setBonPayThisYr03(Object what) {
		setBonPayThisYr03(what, false);
	}
	public void setBonPayThisYr03(Object what, boolean rounded) {
		if (rounded)
			bonPayThisYr03.setRounded(what);
		else
			bonPayThisYr03.set(what);
	}	
	public PackedDecimalData getBonPayThisYr04() {
		return bonPayThisYr04;
	}
	public void setBonPayThisYr04(Object what) {
		setBonPayThisYr04(what, false);
	}
	public void setBonPayThisYr04(Object what, boolean rounded) {
		if (rounded)
			bonPayThisYr04.setRounded(what);
		else
			bonPayThisYr04.set(what);
	}	
	public PackedDecimalData getBonPayThisYr05() {
		return bonPayThisYr05;
	}
	public void setBonPayThisYr05(Object what) {
		setBonPayThisYr05(what, false);
	}
	public void setBonPayThisYr05(Object what, boolean rounded) {
		if (rounded)
			bonPayThisYr05.setRounded(what);
		else
			bonPayThisYr05.set(what);
	}	
	public PackedDecimalData getBonPayThisYr06() {
		return bonPayThisYr06;
	}
	public void setBonPayThisYr06(Object what) {
		setBonPayThisYr06(what, false);
	}
	public void setBonPayThisYr06(Object what, boolean rounded) {
		if (rounded)
			bonPayThisYr06.setRounded(what);
		else
			bonPayThisYr06.set(what);
	}	
	public PackedDecimalData getBonPayThisYr07() {
		return bonPayThisYr07;
	}
	public void setBonPayThisYr07(Object what) {
		setBonPayThisYr07(what, false);
	}
	public void setBonPayThisYr07(Object what, boolean rounded) {
		if (rounded)
			bonPayThisYr07.setRounded(what);
		else
			bonPayThisYr07.set(what);
	}	
	public PackedDecimalData getBonPayThisYr08() {
		return bonPayThisYr08;
	}
	public void setBonPayThisYr08(Object what) {
		setBonPayThisYr08(what, false);
	}
	public void setBonPayThisYr08(Object what, boolean rounded) {
		if (rounded)
			bonPayThisYr08.setRounded(what);
		else
			bonPayThisYr08.set(what);
	}	
	public PackedDecimalData getBonPayThisYr09() {
		return bonPayThisYr09;
	}
	public void setBonPayThisYr09(Object what) {
		setBonPayThisYr09(what, false);
	}
	public void setBonPayThisYr09(Object what, boolean rounded) {
		if (rounded)
			bonPayThisYr09.setRounded(what);
		else
			bonPayThisYr09.set(what);
	}	
	public PackedDecimalData getBonPayThisYr10() {
		return bonPayThisYr10;
	}
	public void setBonPayThisYr10(Object what) {
		setBonPayThisYr10(what, false);
	}
	public void setBonPayThisYr10(Object what, boolean rounded) {
		if (rounded)
			bonPayThisYr10.setRounded(what);
		else
			bonPayThisYr10.set(what);
	}	
	public PackedDecimalData getBonPayLastYr01() {
		return bonPayLastYr01;
	}
	public void setBonPayLastYr01(Object what) {
		setBonPayLastYr01(what, false);
	}
	public void setBonPayLastYr01(Object what, boolean rounded) {
		if (rounded)
			bonPayLastYr01.setRounded(what);
		else
			bonPayLastYr01.set(what);
	}	
	public PackedDecimalData getBonPayLastYr02() {
		return bonPayLastYr02;
	}
	public void setBonPayLastYr02(Object what) {
		setBonPayLastYr02(what, false);
	}
	public void setBonPayLastYr02(Object what, boolean rounded) {
		if (rounded)
			bonPayLastYr02.setRounded(what);
		else
			bonPayLastYr02.set(what);
	}	
	public PackedDecimalData getBonPayLastYr03() {
		return bonPayLastYr03;
	}
	public void setBonPayLastYr03(Object what) {
		setBonPayLastYr03(what, false);
	}
	public void setBonPayLastYr03(Object what, boolean rounded) {
		if (rounded)
			bonPayLastYr03.setRounded(what);
		else
			bonPayLastYr03.set(what);
	}	
	public PackedDecimalData getBonPayLastYr04() {
		return bonPayLastYr04;
	}
	public void setBonPayLastYr04(Object what) {
		setBonPayLastYr04(what, false);
	}
	public void setBonPayLastYr04(Object what, boolean rounded) {
		if (rounded)
			bonPayLastYr04.setRounded(what);
		else
			bonPayLastYr04.set(what);
	}	
	public PackedDecimalData getBonPayLastYr05() {
		return bonPayLastYr05;
	}
	public void setBonPayLastYr05(Object what) {
		setBonPayLastYr05(what, false);
	}
	public void setBonPayLastYr05(Object what, boolean rounded) {
		if (rounded)
			bonPayLastYr05.setRounded(what);
		else
			bonPayLastYr05.set(what);
	}	
	public PackedDecimalData getBonPayLastYr06() {
		return bonPayLastYr06;
	}
	public void setBonPayLastYr06(Object what) {
		setBonPayLastYr06(what, false);
	}
	public void setBonPayLastYr06(Object what, boolean rounded) {
		if (rounded)
			bonPayLastYr06.setRounded(what);
		else
			bonPayLastYr06.set(what);
	}	
	public PackedDecimalData getBonPayLastYr07() {
		return bonPayLastYr07;
	}
	public void setBonPayLastYr07(Object what) {
		setBonPayLastYr07(what, false);
	}
	public void setBonPayLastYr07(Object what, boolean rounded) {
		if (rounded)
			bonPayLastYr07.setRounded(what);
		else
			bonPayLastYr07.set(what);
	}	
	public PackedDecimalData getBonPayLastYr08() {
		return bonPayLastYr08;
	}
	public void setBonPayLastYr08(Object what) {
		setBonPayLastYr08(what, false);
	}
	public void setBonPayLastYr08(Object what, boolean rounded) {
		if (rounded)
			bonPayLastYr08.setRounded(what);
		else
			bonPayLastYr08.set(what);
	}	
	public PackedDecimalData getBonPayLastYr09() {
		return bonPayLastYr09;
	}
	public void setBonPayLastYr09(Object what) {
		setBonPayLastYr09(what, false);
	}
	public void setBonPayLastYr09(Object what, boolean rounded) {
		if (rounded)
			bonPayLastYr09.setRounded(what);
		else
			bonPayLastYr09.set(what);
	}	
	public PackedDecimalData getBonPayLastYr10() {
		return bonPayLastYr10;
	}
	public void setBonPayLastYr10(Object what) {
		setBonPayLastYr10(what, false);
	}
	public void setBonPayLastYr10(Object what, boolean rounded) {
		if (rounded)
			bonPayLastYr10.setRounded(what);
		else
			bonPayLastYr10.set(what);
	}	
	public PackedDecimalData getTotalBonus01() {
		return totalBonus01;
	}
	public void setTotalBonus01(Object what) {
		setTotalBonus01(what, false);
	}
	public void setTotalBonus01(Object what, boolean rounded) {
		if (rounded)
			totalBonus01.setRounded(what);
		else
			totalBonus01.set(what);
	}	
	public PackedDecimalData getTotalBonus02() {
		return totalBonus02;
	}
	public void setTotalBonus02(Object what) {
		setTotalBonus02(what, false);
	}
	public void setTotalBonus02(Object what, boolean rounded) {
		if (rounded)
			totalBonus02.setRounded(what);
		else
			totalBonus02.set(what);
	}	
	public PackedDecimalData getTotalBonus03() {
		return totalBonus03;
	}
	public void setTotalBonus03(Object what) {
		setTotalBonus03(what, false);
	}
	public void setTotalBonus03(Object what, boolean rounded) {
		if (rounded)
			totalBonus03.setRounded(what);
		else
			totalBonus03.set(what);
	}	
	public PackedDecimalData getTotalBonus04() {
		return totalBonus04;
	}
	public void setTotalBonus04(Object what) {
		setTotalBonus04(what, false);
	}
	public void setTotalBonus04(Object what, boolean rounded) {
		if (rounded)
			totalBonus04.setRounded(what);
		else
			totalBonus04.set(what);
	}	
	public PackedDecimalData getTotalBonus05() {
		return totalBonus05;
	}
	public void setTotalBonus05(Object what) {
		setTotalBonus05(what, false);
	}
	public void setTotalBonus05(Object what, boolean rounded) {
		if (rounded)
			totalBonus05.setRounded(what);
		else
			totalBonus05.set(what);
	}	
	public PackedDecimalData getTotalBonus06() {
		return totalBonus06;
	}
	public void setTotalBonus06(Object what) {
		setTotalBonus06(what, false);
	}
	public void setTotalBonus06(Object what, boolean rounded) {
		if (rounded)
			totalBonus06.setRounded(what);
		else
			totalBonus06.set(what);
	}	
	public PackedDecimalData getTotalBonus07() {
		return totalBonus07;
	}
	public void setTotalBonus07(Object what) {
		setTotalBonus07(what, false);
	}
	public void setTotalBonus07(Object what, boolean rounded) {
		if (rounded)
			totalBonus07.setRounded(what);
		else
			totalBonus07.set(what);
	}	
	public PackedDecimalData getTotalBonus08() {
		return totalBonus08;
	}
	public void setTotalBonus08(Object what) {
		setTotalBonus08(what, false);
	}
	public void setTotalBonus08(Object what, boolean rounded) {
		if (rounded)
			totalBonus08.setRounded(what);
		else
			totalBonus08.set(what);
	}	
	public PackedDecimalData getTotalBonus09() {
		return totalBonus09;
	}
	public void setTotalBonus09(Object what) {
		setTotalBonus09(what, false);
	}
	public void setTotalBonus09(Object what, boolean rounded) {
		if (rounded)
			totalBonus09.setRounded(what);
		else
			totalBonus09.set(what);
	}	
	public PackedDecimalData getTotalBonus10() {
		return totalBonus10;
	}
	public void setTotalBonus10(Object what) {
		setTotalBonus10(what, false);
	}
	public void setTotalBonus10(Object what, boolean rounded) {
		if (rounded)
			totalBonus10.setRounded(what);
		else
			totalBonus10.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getTotbons() {
		return new FixedLengthStringData(totalBonus01.toInternal()
										+ totalBonus02.toInternal()
										+ totalBonus03.toInternal()
										+ totalBonus04.toInternal()
										+ totalBonus05.toInternal()
										+ totalBonus06.toInternal()
										+ totalBonus07.toInternal()
										+ totalBonus08.toInternal()
										+ totalBonus09.toInternal()
										+ totalBonus10.toInternal());
	}
	public void setTotbons(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getTotbons().getLength()).init(obj);
	
		what = ExternalData.chop(what, totalBonus01);
		what = ExternalData.chop(what, totalBonus02);
		what = ExternalData.chop(what, totalBonus03);
		what = ExternalData.chop(what, totalBonus04);
		what = ExternalData.chop(what, totalBonus05);
		what = ExternalData.chop(what, totalBonus06);
		what = ExternalData.chop(what, totalBonus07);
		what = ExternalData.chop(what, totalBonus08);
		what = ExternalData.chop(what, totalBonus09);
		what = ExternalData.chop(what, totalBonus10);
	}
	public PackedDecimalData getTotbon(BaseData indx) {
		return getTotbon(indx.toInt());
	}
	public PackedDecimalData getTotbon(int indx) {

		switch (indx) {
			case 1 : return totalBonus01;
			case 2 : return totalBonus02;
			case 3 : return totalBonus03;
			case 4 : return totalBonus04;
			case 5 : return totalBonus05;
			case 6 : return totalBonus06;
			case 7 : return totalBonus07;
			case 8 : return totalBonus08;
			case 9 : return totalBonus09;
			case 10 : return totalBonus10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setTotbon(BaseData indx, Object what) {
		setTotbon(indx, what, false);
	}
	public void setTotbon(BaseData indx, Object what, boolean rounded) {
		setTotbon(indx.toInt(), what, rounded);
	}
	public void setTotbon(int indx, Object what) {
		setTotbon(indx, what, false);
	}
	public void setTotbon(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setTotalBonus01(what, rounded);
					 break;
			case 2 : setTotalBonus02(what, rounded);
					 break;
			case 3 : setTotalBonus03(what, rounded);
					 break;
			case 4 : setTotalBonus04(what, rounded);
					 break;
			case 5 : setTotalBonus05(what, rounded);
					 break;
			case 6 : setTotalBonus06(what, rounded);
					 break;
			case 7 : setTotalBonus07(what, rounded);
					 break;
			case 8 : setTotalBonus08(what, rounded);
					 break;
			case 9 : setTotalBonus09(what, rounded);
					 break;
			case 10 : setTotalBonus10(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getSuminss() {
		return new FixedLengthStringData(sumins01.toInternal()
										+ sumins02.toInternal()
										+ sumins03.toInternal()
										+ sumins04.toInternal()
										+ sumins05.toInternal()
										+ sumins06.toInternal()
										+ sumins07.toInternal()
										+ sumins08.toInternal()
										+ sumins09.toInternal()
										+ sumins10.toInternal());
	}
	public void setSuminss(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSuminss().getLength()).init(obj);
	
		what = ExternalData.chop(what, sumins01);
		what = ExternalData.chop(what, sumins02);
		what = ExternalData.chop(what, sumins03);
		what = ExternalData.chop(what, sumins04);
		what = ExternalData.chop(what, sumins05);
		what = ExternalData.chop(what, sumins06);
		what = ExternalData.chop(what, sumins07);
		what = ExternalData.chop(what, sumins08);
		what = ExternalData.chop(what, sumins09);
		what = ExternalData.chop(what, sumins10);
	}
	public PackedDecimalData getSumins(BaseData indx) {
		return getSumins(indx.toInt());
	}
	public PackedDecimalData getSumins(int indx) {

		switch (indx) {
			case 1 : return sumins01;
			case 2 : return sumins02;
			case 3 : return sumins03;
			case 4 : return sumins04;
			case 5 : return sumins05;
			case 6 : return sumins06;
			case 7 : return sumins07;
			case 8 : return sumins08;
			case 9 : return sumins09;
			case 10 : return sumins10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSumins(BaseData indx, Object what) {
		setSumins(indx, what, false);
	}
	public void setSumins(BaseData indx, Object what, boolean rounded) {
		setSumins(indx.toInt(), what, rounded);
	}
	public void setSumins(int indx, Object what) {
		setSumins(indx, what, false);
	}
	public void setSumins(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setSumins01(what, rounded);
					 break;
			case 2 : setSumins02(what, rounded);
					 break;
			case 3 : setSumins03(what, rounded);
					 break;
			case 4 : setSumins04(what, rounded);
					 break;
			case 5 : setSumins05(what, rounded);
					 break;
			case 6 : setSumins06(what, rounded);
					 break;
			case 7 : setSumins07(what, rounded);
					 break;
			case 8 : setSumins08(what, rounded);
					 break;
			case 9 : setSumins09(what, rounded);
					 break;
			case 10 : setSumins10(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getLongdescs() {
		return new FixedLengthStringData(longdesc01.toInternal()
										+ longdesc02.toInternal()
										+ longdesc03.toInternal()
										+ longdesc04.toInternal()
										+ longdesc05.toInternal()
										+ longdesc06.toInternal()
										+ longdesc07.toInternal()
										+ longdesc08.toInternal()
										+ longdesc09.toInternal()
										+ longdesc10.toInternal());
	}
	public void setLongdescs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getLongdescs().getLength()).init(obj);
	
		what = ExternalData.chop(what, longdesc01);
		what = ExternalData.chop(what, longdesc02);
		what = ExternalData.chop(what, longdesc03);
		what = ExternalData.chop(what, longdesc04);
		what = ExternalData.chop(what, longdesc05);
		what = ExternalData.chop(what, longdesc06);
		what = ExternalData.chop(what, longdesc07);
		what = ExternalData.chop(what, longdesc08);
		what = ExternalData.chop(what, longdesc09);
		what = ExternalData.chop(what, longdesc10);
	}
	public FixedLengthStringData getLongdesc(BaseData indx) {
		return getLongdesc(indx.toInt());
	}
	public FixedLengthStringData getLongdesc(int indx) {

		switch (indx) {
			case 1 : return longdesc01;
			case 2 : return longdesc02;
			case 3 : return longdesc03;
			case 4 : return longdesc04;
			case 5 : return longdesc05;
			case 6 : return longdesc06;
			case 7 : return longdesc07;
			case 8 : return longdesc08;
			case 9 : return longdesc09;
			case 10 : return longdesc10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setLongdesc(BaseData indx, Object what) {
		setLongdesc(indx.toInt(), what);
	}
	public void setLongdesc(int indx, Object what) {

		switch (indx) {
			case 1 : setLongdesc01(what);
					 break;
			case 2 : setLongdesc02(what);
					 break;
			case 3 : setLongdesc03(what);
					 break;
			case 4 : setLongdesc04(what);
					 break;
			case 5 : setLongdesc05(what);
					 break;
			case 6 : setLongdesc06(what);
					 break;
			case 7 : setLongdesc07(what);
					 break;
			case 8 : setLongdesc08(what);
					 break;
			case 9 : setLongdesc09(what);
					 break;
			case 10 : setLongdesc10(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getBpaytys() {
		return new FixedLengthStringData(bonPayThisYr01.toInternal()
										+ bonPayThisYr02.toInternal()
										+ bonPayThisYr03.toInternal()
										+ bonPayThisYr04.toInternal()
										+ bonPayThisYr05.toInternal()
										+ bonPayThisYr06.toInternal()
										+ bonPayThisYr07.toInternal()
										+ bonPayThisYr08.toInternal()
										+ bonPayThisYr09.toInternal()
										+ bonPayThisYr10.toInternal());
	}
	public void setBpaytys(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getBpaytys().getLength()).init(obj);
	
		what = ExternalData.chop(what, bonPayThisYr01);
		what = ExternalData.chop(what, bonPayThisYr02);
		what = ExternalData.chop(what, bonPayThisYr03);
		what = ExternalData.chop(what, bonPayThisYr04);
		what = ExternalData.chop(what, bonPayThisYr05);
		what = ExternalData.chop(what, bonPayThisYr06);
		what = ExternalData.chop(what, bonPayThisYr07);
		what = ExternalData.chop(what, bonPayThisYr08);
		what = ExternalData.chop(what, bonPayThisYr09);
		what = ExternalData.chop(what, bonPayThisYr10);
	}
	public PackedDecimalData getBpayty(BaseData indx) {
		return getBpayty(indx.toInt());
	}
	public PackedDecimalData getBpayty(int indx) {

		switch (indx) {
			case 1 : return bonPayThisYr01;
			case 2 : return bonPayThisYr02;
			case 3 : return bonPayThisYr03;
			case 4 : return bonPayThisYr04;
			case 5 : return bonPayThisYr05;
			case 6 : return bonPayThisYr06;
			case 7 : return bonPayThisYr07;
			case 8 : return bonPayThisYr08;
			case 9 : return bonPayThisYr09;
			case 10 : return bonPayThisYr10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setBpayty(BaseData indx, Object what) {
		setBpayty(indx, what, false);
	}
	public void setBpayty(BaseData indx, Object what, boolean rounded) {
		setBpayty(indx.toInt(), what, rounded);
	}
	public void setBpayty(int indx, Object what) {
		setBpayty(indx, what, false);
	}
	public void setBpayty(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setBonPayThisYr01(what, rounded);
					 break;
			case 2 : setBonPayThisYr02(what, rounded);
					 break;
			case 3 : setBonPayThisYr03(what, rounded);
					 break;
			case 4 : setBonPayThisYr04(what, rounded);
					 break;
			case 5 : setBonPayThisYr05(what, rounded);
					 break;
			case 6 : setBonPayThisYr06(what, rounded);
					 break;
			case 7 : setBonPayThisYr07(what, rounded);
					 break;
			case 8 : setBonPayThisYr08(what, rounded);
					 break;
			case 9 : setBonPayThisYr09(what, rounded);
					 break;
			case 10 : setBonPayThisYr10(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getBpaynys() {
		return new FixedLengthStringData(bonPayLastYr01.toInternal()
										+ bonPayLastYr02.toInternal()
										+ bonPayLastYr03.toInternal()
										+ bonPayLastYr04.toInternal()
										+ bonPayLastYr05.toInternal()
										+ bonPayLastYr06.toInternal()
										+ bonPayLastYr07.toInternal()
										+ bonPayLastYr08.toInternal()
										+ bonPayLastYr09.toInternal()
										+ bonPayLastYr10.toInternal());
	}
	public void setBpaynys(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getBpaynys().getLength()).init(obj);
	
		what = ExternalData.chop(what, bonPayLastYr01);
		what = ExternalData.chop(what, bonPayLastYr02);
		what = ExternalData.chop(what, bonPayLastYr03);
		what = ExternalData.chop(what, bonPayLastYr04);
		what = ExternalData.chop(what, bonPayLastYr05);
		what = ExternalData.chop(what, bonPayLastYr06);
		what = ExternalData.chop(what, bonPayLastYr07);
		what = ExternalData.chop(what, bonPayLastYr08);
		what = ExternalData.chop(what, bonPayLastYr09);
		what = ExternalData.chop(what, bonPayLastYr10);
	}
	public PackedDecimalData getBpayny(BaseData indx) {
		return getBpayny(indx.toInt());
	}
	public PackedDecimalData getBpayny(int indx) {

		switch (indx) {
			case 1 : return bonPayLastYr01;
			case 2 : return bonPayLastYr02;
			case 3 : return bonPayLastYr03;
			case 4 : return bonPayLastYr04;
			case 5 : return bonPayLastYr05;
			case 6 : return bonPayLastYr06;
			case 7 : return bonPayLastYr07;
			case 8 : return bonPayLastYr08;
			case 9 : return bonPayLastYr09;
			case 10 : return bonPayLastYr10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setBpayny(BaseData indx, Object what) {
		setBpayny(indx, what, false);
	}
	public void setBpayny(BaseData indx, Object what, boolean rounded) {
		setBpayny(indx.toInt(), what, rounded);
	}
	public void setBpayny(int indx, Object what) {
		setBpayny(indx, what, false);
	}
	public void setBpayny(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setBonPayLastYr01(what, rounded);
					 break;
			case 2 : setBonPayLastYr02(what, rounded);
					 break;
			case 3 : setBonPayLastYr03(what, rounded);
					 break;
			case 4 : setBonPayLastYr04(what, rounded);
					 break;
			case 5 : setBonPayLastYr05(what, rounded);
					 break;
			case 6 : setBonPayLastYr06(what, rounded);
					 break;
			case 7 : setBonPayLastYr07(what, rounded);
					 break;
			case 8 : setBonPayLastYr08(what, rounded);
					 break;
			case 9 : setBonPayLastYr09(what, rounded);
					 break;
			case 10 : setBonPayLastYr10(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getBcalmeths() {
		return new FixedLengthStringData(bonusCalcMeth01.toInternal()
										+ bonusCalcMeth02.toInternal()
										+ bonusCalcMeth03.toInternal()
										+ bonusCalcMeth04.toInternal()
										+ bonusCalcMeth05.toInternal()
										+ bonusCalcMeth06.toInternal()
										+ bonusCalcMeth07.toInternal()
										+ bonusCalcMeth08.toInternal()
										+ bonusCalcMeth09.toInternal()
										+ bonusCalcMeth10.toInternal());
	}
	public void setBcalmeths(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getBcalmeths().getLength()).init(obj);
	
		what = ExternalData.chop(what, bonusCalcMeth01);
		what = ExternalData.chop(what, bonusCalcMeth02);
		what = ExternalData.chop(what, bonusCalcMeth03);
		what = ExternalData.chop(what, bonusCalcMeth04);
		what = ExternalData.chop(what, bonusCalcMeth05);
		what = ExternalData.chop(what, bonusCalcMeth06);
		what = ExternalData.chop(what, bonusCalcMeth07);
		what = ExternalData.chop(what, bonusCalcMeth08);
		what = ExternalData.chop(what, bonusCalcMeth09);
		what = ExternalData.chop(what, bonusCalcMeth10);
	}
	public FixedLengthStringData getBcalmeth(BaseData indx) {
		return getBcalmeth(indx.toInt());
	}
	public FixedLengthStringData getBcalmeth(int indx) {

		switch (indx) {
			case 1 : return bonusCalcMeth01;
			case 2 : return bonusCalcMeth02;
			case 3 : return bonusCalcMeth03;
			case 4 : return bonusCalcMeth04;
			case 5 : return bonusCalcMeth05;
			case 6 : return bonusCalcMeth06;
			case 7 : return bonusCalcMeth07;
			case 8 : return bonusCalcMeth08;
			case 9 : return bonusCalcMeth09;
			case 10 : return bonusCalcMeth10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setBcalmeth(BaseData indx, Object what) {
		setBcalmeth(indx.toInt(), what);
	}
	public void setBcalmeth(int indx, Object what) {

		switch (indx) {
			case 1 : setBonusCalcMeth01(what);
					 break;
			case 2 : setBonusCalcMeth02(what);
					 break;
			case 3 : setBonusCalcMeth03(what);
					 break;
			case 4 : setBonusCalcMeth04(what);
					 break;
			case 5 : setBonusCalcMeth05(what);
					 break;
			case 6 : setBonusCalcMeth06(what);
					 break;
			case 7 : setBonusCalcMeth07(what);
					 break;
			case 8 : setBonusCalcMeth08(what);
					 break;
			case 9 : setBonusCalcMeth09(what);
					 break;
			case 10 : setBonusCalcMeth10(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		currfrom.clear();
		currto.clear();
		datetexc.clear();
		validflag.clear();
		longdesc01.clear();
		longdesc02.clear();
		longdesc03.clear();
		longdesc04.clear();
		longdesc05.clear();
		longdesc06.clear();
		longdesc07.clear();
		longdesc08.clear();
		longdesc09.clear();
		longdesc10.clear();
		sumins01.clear();
		sumins02.clear();
		sumins03.clear();
		sumins04.clear();
		sumins05.clear();
		sumins06.clear();
		sumins07.clear();
		sumins08.clear();
		sumins09.clear();
		sumins10.clear();
		bonusCalcMeth01.clear();
		bonusCalcMeth02.clear();
		bonusCalcMeth03.clear();
		bonusCalcMeth04.clear();
		bonusCalcMeth05.clear();
		bonusCalcMeth06.clear();
		bonusCalcMeth07.clear();
		bonusCalcMeth08.clear();
		bonusCalcMeth09.clear();
		bonusCalcMeth10.clear();
		bonPayThisYr01.clear();
		bonPayThisYr02.clear();
		bonPayThisYr03.clear();
		bonPayThisYr04.clear();
		bonPayThisYr05.clear();
		bonPayThisYr06.clear();
		bonPayThisYr07.clear();
		bonPayThisYr08.clear();
		bonPayThisYr09.clear();
		bonPayThisYr10.clear();
		bonPayLastYr01.clear();
		bonPayLastYr02.clear();
		bonPayLastYr03.clear();
		bonPayLastYr04.clear();
		bonPayLastYr05.clear();
		bonPayLastYr06.clear();
		bonPayLastYr07.clear();
		bonPayLastYr08.clear();
		bonPayLastYr09.clear();
		bonPayLastYr10.clear();
		totalBonus01.clear();
		totalBonus02.clear();
		totalBonus03.clear();
		totalBonus04.clear();
		totalBonus05.clear();
		totalBonus06.clear();
		totalBonus07.clear();
		totalBonus08.clear();
		totalBonus09.clear();
		totalBonus10.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}