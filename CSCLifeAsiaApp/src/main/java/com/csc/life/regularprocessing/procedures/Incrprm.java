/*
 * File: Incrprm.java
 * Date: 29 August 2009 22:57:02
 * Author: Quipoz Limited
 * 
 * Class transformed from INCRPRM.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.regularprocessing.dataaccess.LifergpTableDAM;
import com.csc.life.regularprocessing.recordstructures.Incrsrec;
import com.csc.life.regularprocessing.tablestructures.T5648rec;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*               INCREASE INSTALMENT PREMIUM SUBROUTINE
*
* This  can  be called to increase premium and as a result
* the sum assured.
*
* By  reading new table T5648  (Automatic Increase Rates) this
* program will calculate a new Premium as follows:-
*
* - read  T5648 using Anniversary Method.
* - read  T6658 using Anniversary Method.
*
* If T6658 answer to 'Simple' is not spaces then:-
*
* - Calculate Premium Increase =
*       (INCC-ORIGINST * T5648-PERCENTAGE) / 100
*        INCC-NEWINST = INCC-CURRINST + Premium Increase
*
* If T6658 answer to 'Compound' is not spaces then:-
*
* - Calculate Premium Increase =
*       (INCC-CURRINST * T5648-PERCENTAGE) / 100
*        INCC-NEWINST = INCC-CURRINST + Premium Increase
*
* Recalculate Sum Assured based on New Premium as follows:-
*
*   Read T5687 with INCC-CRTABLE
*   If the T5687 Premium Calculation Method not = spaces
*      Read T5675 with T5687 Premium Calculation Method
*      If T5675 subroutine not = spaces
*         Call T5675 Subroutine using PREMIUMREC (Increased
*                                                 Portion)
*         Perform normal error handling
*         Add to  INCC-CURRSUM the CPRM-SUMIN
*                 Giving INCC-NEWSUM
*      else
*         Move INCC-CURRSUM to INCC-NEWSUM.
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
* Calculate increases in basic and loaded premiums.                   *
*
*****************************************************************
* </pre>
*/
public class Incrprm extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "INCRPRM";
	private PackedDecimalData wsaaIncrease = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaIncreaseBas = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaIncreaseLoa = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0).init(ZERO);
		/* ERRORS */
	private String e652 = "E652";
	private String h036 = "H036";
	private String h065 = "H065";
		/* TABLES */
	private String t5648 = "T5648";
	private String t5687 = "T5687";
	private String t6658 = "T6658";
	private String t5675 = "T5675";
	private String t1693 = "T1693";
	private String chdrlnbrec = "CHDRLNBREC";
	private Agecalcrec agecalcrec = new Agecalcrec();
		/*Annuity Details*/
	private AnnyTableDAM annyIO = new AnnyTableDAM();
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Incrsrec incrsrec = new Incrsrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*LIFE Reg Processing*/
	private LifergpTableDAM lifergpIO = new LifergpTableDAM();
	private Premiumrec premiumrec = new Premiumrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T1693rec t1693rec = new T1693rec();
	private T5648rec t5648rec = new T5648rec();
	private T5675rec t5675rec = new T5675rec();
	private T5687rec t5687rec = new T5687rec();
	private T6658rec t6658rec = new T6658rec();
	private Varcom varcom = new Varcom();
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit6090, 
		exit7009, 
		seExit8090, 
		dbExit8190
	}

	public Incrprm() {
		super();
	}

public void mainline(Object... parmArray)
	{
		incrsrec.increaseRec = convertAndSetParam(incrsrec.increaseRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		incrsrec.statuz.set(varcom.oK);
		readT56481000();
		readT66582000();
		readT56873000();
		if (isNE(t6658rec.simpleInd,SPACES)) {
			calcIncrPremS4000();
		}
		else {
			if (isNE(t6658rec.compoundInd,SPACES)) {
				calcIncrPremC5000();
			}
		}
		recalcPrem6000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readT56481000()
	{
		para1000();
	}

protected void para1000()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(incrsrec.chdrcoy);
		itdmIO.setItemtabl(t5648);
		itdmIO.setItemitem(incrsrec.annvmeth);
		itdmIO.setItmfrm(incrsrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError8000();
		}
		if (isNE(itdmIO.getItemcoy(),incrsrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5648)
		|| isNE(itdmIO.getItemitem(),incrsrec.annvmeth)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incrsrec.annvmeth);
			syserrrec.statuz.set(h065);
			systemError8000();
		}
		else {
			t5648rec.t5648Rec.set(itdmIO.getGenarea());
		}
		if (isGT(t5648rec.pctinc,incrsrec.maxpcnt)) {
			incrsrec.pctinc.set(incrsrec.maxpcnt);
		}
		else {
			incrsrec.pctinc.set(t5648rec.pctinc);
		}
		if (isLT(t5648rec.pctinc,incrsrec.minpcnt)) {
			incrsrec.pctinc.set(ZERO);
		}
	}

protected void readT66582000()
	{
		readT66582005();
	}

protected void readT66582005()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(incrsrec.chdrcoy);
		itdmIO.setItmfrm(incrsrec.effdate);
		itdmIO.setItemtabl(t6658);
		itdmIO.setItemitem(incrsrec.annvmeth);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			systemError8000();
		}
		if (isNE(itdmIO.getItemcoy(),incrsrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t6658)
		|| isNE(itdmIO.getItemitem(),incrsrec.annvmeth)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(incrsrec.chdrcoy);
			itdmIO.setItemtabl(t6658);
			itdmIO.setItemitem(incrsrec.annvmeth);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h036);
			systemError8000();
		}
		t6658rec.t6658Rec.set(itdmIO.getGenarea());
	}

protected void readT56873000()
	{
		readT56873005();
	}

protected void readT56873005()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(incrsrec.chdrcoy);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(incrsrec.crtable);
		itdmIO.setItmfrm(incrsrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError8000();
		}
		if (isNE(itdmIO.getItemcoy(),incrsrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),incrsrec.crtable)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(incrsrec.chdrcoy);
			itdmIO.setItemtabl(t5687);
			itdmIO.setItemitem(incrsrec.crtable);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e652);
			systemError8000();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
	}

protected void calcIncrPremS4000()
	{
		/*CALC-INCR-PREM*/
		compute(wsaaIncrease, 2).set(div((mult(incrsrec.originst01,incrsrec.pctinc)),100));
		compute(incrsrec.newinst01, 2).set(add(incrsrec.currinst01,wsaaIncrease));
		compute(wsaaIncreaseBas, 2).set(div((mult(incrsrec.originst02,incrsrec.pctinc)),100));
		compute(incrsrec.newinst02, 2).set(add(incrsrec.currinst02,wsaaIncreaseBas));
		compute(wsaaIncreaseLoa, 2).set(div((mult(incrsrec.originst03,incrsrec.pctinc)),100));
		compute(incrsrec.newinst03, 2).set(add(incrsrec.currinst03,wsaaIncreaseLoa));
		/*EXIT*/
	}

protected void calcIncrPremC5000()
	{
		/*CALC-INCR-PREM*/
		compute(wsaaIncrease, 2).set(div((mult(incrsrec.currinst01,incrsrec.pctinc)),100));
		compute(incrsrec.newinst01, 2).set(add(incrsrec.currinst01,wsaaIncrease));
		compute(wsaaIncreaseBas, 2).set(div((mult(incrsrec.currinst02,incrsrec.pctinc)),100));
		compute(incrsrec.newinst02, 2).set(add(incrsrec.currinst02,wsaaIncreaseBas));
		compute(wsaaIncreaseLoa, 2).set(div((mult(incrsrec.currinst03,incrsrec.pctinc)),100));
		compute(incrsrec.newinst03, 2).set(add(incrsrec.currinst03,wsaaIncreaseLoa));
		/*EXIT*/
	}

protected void recalcPrem6000()
	{
		try {
			calc6005();
		}
		catch (GOTOException e){
		}
	}

protected void calc6005()
	{
		premiumrec.function.set("INCR");
		premiumrec.chdrChdrcoy.set(incrsrec.chdrcoy);
		premiumrec.chdrChdrnum.set(incrsrec.chdrnum);
		premiumrec.lifeLife.set(incrsrec.life);
		premiumrec.lifeJlife.set("00");
		premiumrec.covrCoverage.set(incrsrec.coverage);
		premiumrec.covrRider.set(incrsrec.rider);
		premiumrec.crtable.set(incrsrec.crtable);
		premiumrec.effectdt.set(incrsrec.effdate);
		premiumrec.termdate.set(incrsrec.rcesdte);
		premiumrec.language.set(incrsrec.language);
		getLifeDetails7000();
		datcon3rec.intDate1.set(incrsrec.effdate);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError8000();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		premiumrec.currcode.set(incrsrec.cntcurr);
		premiumrec.calcPrem.set(wsaaIncrease);
		premiumrec.calcBasPrem.set(wsaaIncreaseBas);
		premiumrec.calcLoaPrem.set(wsaaIncreaseLoa);
		premiumrec.mortcls.set(incrsrec.mortcls);
		premiumrec.billfreq.set(incrsrec.billfreq);
		premiumrec.mop.set(incrsrec.mop);
		premiumrec.ratingdate.set(incrsrec.occdate);
		premiumrec.reRateDate.set(incrsrec.occdate);
		premiumrec.sumin.set(ZERO);
		if (isEQ(t5675rec.premsubr,SPACES)) {
			incrsrec.newsum.set(incrsrec.currsum);
			goTo(GotoLabel.exit6090);
		}
		getAnny8000();
		
		callProgramX(t5675rec.premsubr, premiumrec.premiumRec);

		/*Ticket #IVE-792 - End		*/
		/*Ticket #ILIFE-2005 - End		*/
		if (isNE(premiumrec.statuz,varcom.oK)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			systemError8000();
		}
		compute(incrsrec.newsum, 2).set(add(premiumrec.sumin,incrsrec.currsum));
	}

protected void getLifeDetails7000()
	{
		try {
			life7005();
		}
		catch (GOTOException e){
		}
	}

protected void life7005()
	{
		lifergpIO.setChdrcoy(incrsrec.chdrcoy);
		lifergpIO.setChdrnum(incrsrec.chdrnum);
		lifergpIO.setLife(incrsrec.life);
		lifergpIO.setJlife("00");
		lifergpIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifergpIO);
		if (isNE(lifergpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifergpIO.getParams());
			dbError8100();
		}
		calculateAnb7500();
		premiumrec.lage.set(wsaaAnb);
		premiumrec.lsex.set(lifergpIO.getCltsex());
		lifergpIO.setChdrcoy(incrsrec.chdrcoy);
		lifergpIO.setChdrnum(incrsrec.chdrnum);
		lifergpIO.setLife(incrsrec.life);
		lifergpIO.setJlife("01");
		lifergpIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifergpIO);
		if (isNE(lifergpIO.getStatuz(),varcom.oK)
		&& isNE(lifergpIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifergpIO.getParams());
			dbError8100();
		}
		if (isEQ(lifergpIO.getStatuz(),varcom.mrnf)) {
			premiumrec.jlsex.set(SPACES);
			premiumrec.jlage.set(0);
		}
		else {
			calculateAnb7500();
			premiumrec.jlage.set(wsaaAnb);
			premiumrec.jlsex.set(lifergpIO.getCltsex());
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(incrsrec.chdrcoy);
		itemIO.setItemtabl(t5675);
		itemIO.setFunction(varcom.readr);
		if (isEQ(lifergpIO.getStatuz(),varcom.mrnf)) {
			itemIO.setItemitem(t5687rec.premmeth);
		}
		else {
			itemIO.setItemitem(t5687rec.jlPremMeth);
		}
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			t5675rec.premsubr.set(SPACES);
			goTo(GotoLabel.exit7009);
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(itemIO.getItemitem());
		}
		/*IVE-3142 End */
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

protected void calculateAnb7500()
	{
		para7505();
	}

protected void para7505()
	{
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setChdrcoy(incrsrec.chdrcoy);
		chdrlnbIO.setChdrnum(incrsrec.chdrnum);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			dbError8100();
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(incrsrec.chdrcoy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.cnttype.set(chdrlnbIO.getCnttype());
		agecalcrec.intDate1.set(lifergpIO.getCltdob());
		agecalcrec.intDate2.set(incrsrec.effdate);
		agecalcrec.company.set(t1693rec.fsuco);
		agecalcrec.language.set(incrsrec.language);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			systemError8000();
		}
		wsaaAnb.set(agecalcrec.agerating);
	}

protected void systemError8000()
	{
		try {
			se8000();
		}
		catch (GOTOException e){
		}
		finally{
			seExit8090();
		}
	}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit8090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		incrsrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
		try {
			db8100();
		}
		catch (GOTOException e){
		}
		finally{
			dbExit8190();
		}
	}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit8190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		incrsrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void getAnny8000()
	{
		begins8010();
	}

protected void begins8010()
	{
		annyIO.setChdrcoy(incrsrec.chdrcoy);
		annyIO.setChdrnum(incrsrec.chdrnum);
		annyIO.setLife(incrsrec.life);
		annyIO.setCoverage(incrsrec.coverage);
		annyIO.setRider(incrsrec.rider);
		annyIO.setPlanSuffix(incrsrec.plnsfx);
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)
		&& isNE(annyIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			dbError8100();
		}
		if (isEQ(annyIO.getStatuz(),varcom.oK)) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		}
		else {
			premiumrec.advance.set(SPACES);
			premiumrec.arrears.set(SPACES);
			premiumrec.freqann.set(SPACES);
			premiumrec.withprop.set(SPACES);
			premiumrec.withoprop.set(SPACES);
			premiumrec.ppind.set(SPACES);
			premiumrec.nomlife.set(SPACES);
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
	}
}
