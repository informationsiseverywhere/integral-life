package com.csc.life.regularprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:40
 * Description:
 * Copybook name: TRWPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Trwpkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData trwpFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData trwpKey = new FixedLengthStringData(256).isAPartOf(trwpFileKey, 0, REDEFINE);
  	public FixedLengthStringData trwpChdrcoy = new FixedLengthStringData(1).isAPartOf(trwpKey, 0);
  	public FixedLengthStringData trwpChdrnum = new FixedLengthStringData(8).isAPartOf(trwpKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(trwpKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(trwpFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		trwpFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}