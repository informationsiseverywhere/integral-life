/*
 * File: B5034.java
 * Date: 29 August 2009 20:53:21
 * Author: Quipoz Limited
 * 
 * Class transformed from B5034.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.regularprocessing.dataaccess.LifergpTableDAM;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*   This  batch  program  alters existing COVR records to contain
*   a valid date in the COVR-CPI-DATE field. To determine whether
*   this field should be HI-DATE or a valid date will be based on
*   the following criteria:-
*
*   - Read all COVR records, for each found read T5687 using
*     the  CRTABLE.  If the Anniversary Method is not spaces
*     read T6658 to determine  the  frequency at  which  the
*     automatic  increase  should  occur.  If the  Automatic
*     Increase Method IS spaces, no date will be calculated.
*
*   - Using  the  frequency  and  the  Risk Commencement Date
*     call DATCON2  with  the  frequency until  the Parameter
*     Date is Passed.
*
*   - If  the  coverage  is  Single  Premium  (ie. Instalment
*     Premium field = zeroes) then the CPI-DATE is set to all
*     9's.
*
*   - LIFE  details  are retrieved  to get current Age. If no
*     DOB is found  on  the LIFE file the  CPI-DATE is set to
*     all 9's.
*
*   - Term left to Premium Cessation is calculated.
*
*   - If the Term left to Cessation < T6658 Min Term to Cess
*     Or Calculated Age             > T6658 Max Age (if not 0)
*     Or CPI-DATE calculated        > COVR-PREM-CESS-DATE
*        A message is logged, and will appear on job report.
*
*   NB. NO  SOFTLOCKING IS  PERFORMED, IT IS ASSUMED THIS JOB
*       WILL BE RUN WHEN THE LIVE ENVIRONMENT IS NOT RUNNING,
*       AND THAT NO LOCKS WILL EXIST ON THE COVR FILE.
*
*   Control totals:
*     01  -  Number of records selected
*     02  -  Number of COVR records updated
*     03  -  Number of COVR records eligible
*     04  -  Number of COVR records no method
*
*
*****************************************************************
* </pre>
*/
public class B5034 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5034");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaAnb = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
		/* ERRORS */
	private static final String e652 = "E652";
	private static final String h036 = "H036";
		/* FORMATS */
	private static final String covrrec = "COVRREC";
	private static final String chdrlnbrec = "CHDRLNBREC";
		/* TABLES */
	private static final String t5687 = "T5687";
	private static final String t6658 = "T6658";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*LIFE Reg Processing*/
	private LifergpTableDAM lifergpIO = new LifergpTableDAM();
	private T5687rec t5687rec = new T5687rec();
	private T6658rec t6658rec = new T6658rec();
	private Varcom varcom = new Varcom();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readNextRecord2006, 
		calcDate6007, 
		bypassCalc6009
	}

	public B5034() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		readFirstRecord1000();
		while ( !(endOfFile.isTrue())) {
			processCovrs2000();
		}
		
		/*EXIT*/
	}

protected void readFirstRecord1000()
	{
		selectCovrRecords1000();
	}

protected void selectCovrRecords1000()
	{
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(runparmrec.company);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(covrIO.getParams());
			conerrrec.statuz.set(covrIO.getParams());
			databaseError006();
		}
		if (isNE(covrIO.getChdrcoy(),runparmrec.company)) {
			covrIO.setStatuz(varcom.endp);
		}
		if (isEQ(covrIO.getStatuz(),varcom.endp)) {
			wsaaEof.set("Y");
		}
	}

protected void processCovrs2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					selectCovr2000();
				case readNextRecord2006: 
					readNextRecord2006();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void selectCovr2000()
	{
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		readTableT56874000();
		if (isEQ(t5687rec.anniversaryMethod,SPACES)) {
			contotrec.totno.set(ct04);
			contotrec.totval.set(1);
			callContot001();
			goTo(GotoLabel.readNextRecord2006);
		}
		readTableT66585000();
		if (isEQ(t6658rec.premsubr,SPACES)) {
			contotrec.totno.set(ct04);
			contotrec.totval.set(1);
			callContot001();
			goTo(GotoLabel.readNextRecord2006);
		}
		updateCovr6000();
	}

protected void readNextRecord2006()
	{
		covrIO.setFunction(varcom.rewrt);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(covrIO.getParams());
			conerrrec.statuz.set(covrIO.getStatuz());
			databaseError006();
		}
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(covrIO.getParams());
			conerrrec.statuz.set(covrIO.getParams());
			databaseError006();
		}
		if (isNE(covrIO.getChdrcoy(),runparmrec.company)) {
			covrIO.setStatuz(varcom.endp);
		}
		if (isEQ(covrIO.getStatuz(),varcom.endp)) {
			wsaaEof.set("Y");
		}
	}

	/**
	* <pre>
	*  This section retrieves the Maturity Calculation Method from
	*  table T5687.
	* </pre>
	*/
protected void readTableT56874000()
	{
		readT56874005();
	}

protected void readT56874005()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrIO.getCrtable());
		itdmIO.setItmfrm(runparmrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		if (isNE(itdmIO.getItemcoy(),runparmrec.company)
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),covrIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(runparmrec.company);
			itdmIO.setItemtabl(t5687);
			itdmIO.setItemitem(covrIO.getCrtable());
			conerrrec.params.set(itdmIO.getParams());
			conerrrec.statuz.set(e652);
			databaseError006();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
	}

protected void readTableT66585000()
	{
		readT66585005();
	}

	/**
	* <pre>
	*  This section retrieves the Calculation Program from table
	*  T6658.
	* </pre>
	*/
protected void readT66585005()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItmfrm(runparmrec.effdate);
		itdmIO.setItemtabl(t6658);
		itdmIO.setItemitem(t5687rec.anniversaryMethod);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		if (isNE(itdmIO.getItemcoy(),runparmrec.company)
		|| isNE(itdmIO.getItemtabl(),t6658)
		|| isNE(itdmIO.getItemitem(),t5687rec.anniversaryMethod)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(runparmrec.company);
			itdmIO.setItemtabl(t6658);
			itdmIO.setItemitem(t5687rec.anniversaryMethod);
			conerrrec.params.set(itdmIO.getParams());
			conerrrec.statuz.set(h036);
			systemError005();
		}
		t6658rec.t6658Rec.set(itdmIO.getGenarea());
	}

protected void updateCovr6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					covr6005();
				case calcDate6007: 
					calcDate6007();
					calcDate6008();
				case bypassCalc6009: 
					bypassCalc6009();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void covr6005()
	{
		/* Bypass calculation if Single Premium*/
		if (isEQ(covrIO.getInstprem(), ZERO)) {
			covrIO.setCpiDate(ZERO);
			goTo(GotoLabel.bypassCalc6009);
		}
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(covrIO.getCrrcd());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(t6658rec.billfreq);
	}

protected void calcDate6007()
	{
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			conerrrec.params.set(datcon2rec.datcon2Rec);
			conerrrec.statuz.set(datcon2rec.statuz);
			databaseError006();
		}
		if (isLT(datcon2rec.intDate2,runparmrec.effdate)) {
			datcon2rec.intDate1.set(datcon2rec.intDate2);
			goTo(GotoLabel.calcDate6007);
		}
		else {
			covrIO.setCpiDate(datcon2rec.intDate2);
		}
		getLifergpDetails7000();
	}

protected void calcDate6008()
	{
		if (isEQ(covrIO.getCpiDate(),ZERO)) {
			goTo(GotoLabel.bypassCalc6009);
		}
		datcon3rec.intDate1.set(covrIO.getCpiDate());
		datcon3rec.intDate2.set(covrIO.getPremCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			conerrrec.params.set(datcon3rec.datcon3Rec);
			conerrrec.statuz.set(datcon3rec.statuz);
			databaseError006();
		}
		datcon3rec.freqFactor.add(0.99999);
	}

protected void bypassCalc6009()
	{
		/*    IF  T6658-MAX-AGE        NOT > 0                             */
		/*        MOVE 99                 TO T6658-MAX-AGE.                */
		/*    IF  T6658-MINCTRM            > DTC3-FREQ-FACTOR              */
		/*    OR  T6658-MAX-AGE            < WSAA-ANB                      */
		/*    OR  COVR-CPI-DATE            > COVR-PREM-CESS-DATE           */
		/*    OR  COVR-CPI-DATE            = ZEROES                        */
		if (isLTE(t6658rec.agemax, 0)) {
			/*        MOVE 99                 TO T6658-AGEMAX.         <V73L03>*/
			t6658rec.agemax.set(999);
		}
		if (isGT(t6658rec.minctrm, datcon3rec.freqFactor)
		|| isLT(t6658rec.agemax, wsaaAnb)
		|| isGT(covrIO.getCpiDate(), covrIO.getPremCessDate())
		|| isEQ(covrIO.getCpiDate(), ZERO)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(covrIO.getChdrnum());
			stringVariable1.addExpression(" Coverage record ineligible.");
			stringVariable1.setStringInto(conlogrec.message);
			callConlog003();
			covrIO.setCpiDate(99999999);
			contotrec.totno.set(ct03);
			contotrec.totval.set(1);
			callContot001();
			return ;
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
	}

protected void getLifergpDetails7000()
	{
		lifergp7005();
	}

protected void lifergp7005()
	{
		/* LIFE ASSURED AND JOINT LIFE DETAILS*/
		/* To obtain the LIFE assured and joint-LIFE details (if any) do*/
		/* the following;-*/
		/*  - read  the LIFE details using LIFERGP (LIFE number from*/
		/*       COVR, joint LIFE number '00').*/
		lifergpIO.setChdrcoy(covrIO.getChdrcoy());
		lifergpIO.setChdrnum(covrIO.getChdrnum());
		lifergpIO.setLife(covrIO.getLife());
		lifergpIO.setJlife("00");
		lifergpIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifergpIO);
		if (isNE(lifergpIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(lifergpIO.getParams());
			databaseError006();
		}
		/*    Save Main LIFE details within Working Storage for later use.*/
		calculateAnb7500();
		/*  - read the joint LIFE details using LIFERGP (LIFE number*/
		/*       from COVR,  joint  LIFE number '01').  If found,*/
		/*       look up the name from the client details (CLTS) and*/
		/*       format as a "confirmation name".*/
		lifergpIO.setChdrcoy(covrIO.getChdrcoy());
		lifergpIO.setChdrnum(covrIO.getChdrnum());
		lifergpIO.setLife(covrIO.getLife());
		lifergpIO.setJlife("01");
		lifergpIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifergpIO);
		if (isNE(lifergpIO.getStatuz(),varcom.oK)
		&& isNE(lifergpIO.getStatuz(),varcom.mrnf)) {
			conerrrec.params.set(lifergpIO.getParams());
			databaseError006();
		}
		if (isNE(lifergpIO.getStatuz(),varcom.mrnf)) {
			calculateAnb7500();
		}
	}

protected void calculateAnb7500()
	{
		para7505();
	}

protected void para7505()
	{
		if (isEQ(lifergpIO.getCltdob(), ZERO)) {
			covrIO.setCpiDate(ZERO);
			return ;
		}
		/*    MOVE SPACES                 TO DTC3-FUNCTION.                */
		/*    MOVE LIFERGP-CLTDOB         TO DTC3-INT-DATE-1.              */
		/*    MOVE COVR-CPI-DATE          TO DTC3-INT-DATE-2.              */
		/*    MOVE '01'                   TO DTC3-FREQUENCY.               */
		/*    CALL 'DATCON3' USING DTC3-DATCON3-REC.                       */
		/*    IF DTC3-STATUZ              NOT = O-K                        */
		/*        MOVE DTC3-DATCON3-REC   TO CONR-PARAMS                   */
		/*        MOVE DTC3-STATUZ        TO CONR-STATUZ                   */
		/*        PERFORM 005-SYSTEM-ERROR.                                */
		/*    ADD .99999 DTC3-FREQ-FACTOR GIVING WSAA-ANB.                 */
		/* New routine to calculate Age next/nearest/last birthday         */
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setChdrcoy(runparmrec.company);
		chdrlnbIO.setChdrnum(lifergpIO.getChdrnum());
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(chdrlnbIO.getParams());
			databaseError006();
		}
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCB");
		agecalcrec.language.set(runparmrec.language);
		agecalcrec.cnttype.set(chdrlnbIO.getCnttype());
		agecalcrec.intDate1.set(lifergpIO.getCltdob());
		agecalcrec.intDate2.set(covrIO.getCpiDate());
		agecalcrec.company.set(runparmrec.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			conerrrec.statuz.set(agecalcrec.statuz);
			systemError005();
		}
		wsaaAnb.set(agecalcrec.agerating);
	}
}
