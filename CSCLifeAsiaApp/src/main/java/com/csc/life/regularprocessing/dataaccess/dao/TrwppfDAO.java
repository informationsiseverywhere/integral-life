package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.Trwppf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface TrwppfDAO extends BaseDAO<Trwppf> {
    public void insertTrwppf(List<Trwppf> trwppfList);
}