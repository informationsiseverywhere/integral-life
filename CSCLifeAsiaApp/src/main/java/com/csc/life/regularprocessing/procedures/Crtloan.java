/*
 * File: Crtloan.java
 * Date: 29 August 2009 22:42:33
 * Author: Quipoz Limited
 *
 * Class transformed from CRTLOAN.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.dataaccess.LoanTableDAM;
import com.csc.life.contractservicing.dataaccess.LoanenqTableDAM;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.regularprocessing.recordstructures.Crtloanrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
* REMARKS.
*
* Overview.
* =========
* This subroutine is called from B5355 (overdue processing)
* to perform the following:
* 1. Read the loan file to find the last loan , if one exists,
*    for the contract in question.
* 2. Calculate the next interest billing date and the next
*    capitalisation date, using the dates set in T6633 (Loan
*    Interest Rules.)
* 3. Write a LOAN record.
* 4. Write an APL ACMV record.
*
* Linkage Area.
* =============
* FUNCTION      PIC X(05)
* STATUZ        PIC X(04)
* CHDRCOY       PIC X(01)
* CHDRNUM       PIC X(08)
* TRANNO        PIC S9(05) COMP-3
* CNTTYPE       PIC X(03)
* CNTCURR       PIC X(03)
* EFFDATE       PIC S9(08) COMP-3
* OCCDATE       PIC S9(08) COMP-3
* AUTH-CODE     PIC X(04)
* LANGUAGE      PIC X(01)
* OUTSTAMT      PIC S9(15)V9(02) COMP-3
* BATCHKEY
*   PREFIX      PIC X(02)
*   COMPANY     PIC X(01)
*   BRANCH      PIC X(02)
*   ACTYEAR     PIC S9(02) COMP-3
*   ACTMONTH    PIC S9(02) COMP-3
*   TRCDE       PIC X(04)
* SACSCODE-01   PIC X(02)
* SACSCODE-02   PIC X(02)
* SACSTYP-01    PIC X(02)
* SACSTYP-02    PIC X(02)
* GLCODE-01     PIC X(14)
* GLCODE-02     PIC X(14)
* GLSIGN-01     PIC X(01)
* GLSIGN-02     PIC X(01)
* LONGDESC      PIC X(30)
*
* These fields are all contained within CRTLOANREC.
*
* Processing.
* ===========
* 1000-SETUP-LOAN-FILE.
* Find the last loan record for this contract using the logical
* LOANENQ. If one exists, add 1 to its loan number to obtain the
* number for the new loan.
*
* 2000-READ-T6633.
* Read T6633 (Loan Interest Rules) for flags and dates needed to
* calculate next capitalisation and next interest billing dates.
*
* 3000-CALC-NEXT-CAPN-DATE.
* Calculate the next capitalisation date according to the entries
* in T6633. Check these entries in the following order:
*   i) Capitalise on loan anniversary
*  ii) Capitalise on policy anniversary
* iii) Frequency and fixed date
*
* 4000-CALC-NEXT-INTEREST-DATE.
* Calculate the next interest billing date according to the
* entries in T6633. Check these entries in the following order:
*   i) Calculate interest on loan anniversary
*  ii) Calculate interest on policy anniversary
* iii) Frequency and fixed date
*
* 5000-WRITE-LOAN-RECORD.
* Write a new LOAN record for this contract for the installment
* amount that has not been paid.
*
* 6000-WRITE-APL-ACMV.
* Write ACMVs for this new loan as per the entry in T5645
* (Transaction Accounting Rules) for 'B5355'.
*****************************************************************
* </pre>
*/
public class Crtloan extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("CRTLOAN");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(6, 0).init(0).setUnsigned();
		/* ERRORS */
	private static final String e723 = "E723";
		/* FORMATS */
	private static final String loanenqrec = "LOANENQREC";
	private static final String itdmrec = "ITEMREC";
	private static final String loanrec = "LOANREC";
		/* TABLES */
	private static final String t6633 = "T6633";

	private FixedLengthStringData wsaaT6633Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6633Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6633Key, 0);
	private FixedLengthStringData wsaaT6633Type = new FixedLengthStringData(1).isAPartOf(wsaaT6633Key, 3);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT6633Key, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaRldgLoanno = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
		/* WSAA-C-DATE */
	private ZonedDecimalData wsaaContractDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaCtrtDate = new FixedLengthStringData(8).isAPartOf(wsaaContractDate, 0, REDEFINE);
	private FixedLengthStringData wsaaContractMd = new FixedLengthStringData(4).isAPartOf(wsaaCtrtDate, 4);
		/* WSAA-L-DATE */
	private ZonedDecimalData wsaaLoanDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaLndt = new FixedLengthStringData(8).isAPartOf(wsaaLoanDate, 0, REDEFINE);
	private ZonedDecimalData wsaaLoanYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaLndt, 0).setUnsigned();
	private FixedLengthStringData wsaaLoanMd = new FixedLengthStringData(4).isAPartOf(wsaaLndt, 4);
	private ZonedDecimalData wsaaLoanMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 0).setUnsigned();
	private ZonedDecimalData wsaaLoanDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 2).setUnsigned();
		/* WSAA-N-DATE */
	private ZonedDecimalData wsaaNewDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNdate = new FixedLengthStringData(8).isAPartOf(wsaaNewDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNewYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNdate, 0).setUnsigned();
	private FixedLengthStringData wsaaNewMd = new FixedLengthStringData(4).isAPartOf(wsaaNdate, 4);
	private ZonedDecimalData wsaaNewMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 0).setUnsigned();
	private ZonedDecimalData wsaaNewDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 2).setUnsigned();

	private ZonedDecimalData wsaaDayCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator daysLessThan28 = new Validator(wsaaDayCheck, new ValueRange("00","28"));
//	private Validator daysInJan = new Validator(wsaaDayCheck, 31);
//	private Validator daysInFeb = new Validator(wsaaDayCheck, 28);
//	private Validator daysInApr = new Validator(wsaaDayCheck, 30);

	private Validator daysInJan = new Validator(wsaaDayCheck, "31");
	private Validator daysInFeb = new Validator(wsaaDayCheck, "28");
	private Validator daysInApr = new Validator(wsaaDayCheck, "30");

	private PackedDecimalData wsaaResult = new PackedDecimalData(5, 0);
	private String wsaaLeapYear = "";
	private PackedDecimalData wsaaIntYear = new PackedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData wsaaCenturys = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaCenturyx = new FixedLengthStringData(4).isAPartOf(wsaaCenturys, 0, REDEFINE);
	private ZonedDecimalData wsaaCenturyYear = new ZonedDecimalData(2, 0).isAPartOf(wsaaCenturyx, 2).setUnsigned();
	private static final int wsaaLeapFebruaryDays = 29;
	private static final int wsaaFebruaryDays = 28;
	private static final int wsaaAprilDays = 30;
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private LoanTableDAM loanIO = new LoanTableDAM();
	private LoanenqTableDAM loanenqIO = new LoanenqTableDAM();
	private T6633rec t6633rec = new T6633rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Varcom varcom = new Varcom();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Crtloanrec crtloanrec = new Crtloanrec();
	private WsaaMonthCheckInner wsaaMonthCheckInner = new WsaaMonthCheckInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		postPrincipal6030
	}

	public Crtloan() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		crtloanrec.crtloanRec = convertAndSetParam(crtloanrec.crtloanRec, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main010()
	{
		mainlineStart010();
		mainlineExit010();
	}

protected void mainlineStart010()
	{
		setupLoanFile1000();
		readT66332000();
		calcNextCapnDate3000();
		calcNextInterestDate4000();
		writeLoanRecord5000();
		if (isNE(crtloanrec.function, "NPSTW")) {
			writeAplAcmv6000();
		}
	}

protected void mainlineExit010()
	{
		exitProgram();
	}

protected void setupLoanFile1000()
	{
		start1010();
	}

protected void start1010()
	{
		crtloanrec.statuz.set(varcom.oK);
		wsaaTime.set(getCobolTime());
		/*    Read LOAN file to see if there are any existing LOANS for*/
		/*    this contract. If so, find last one and add 1 to its*/
		/*    loan number (LOANENQ-LOAN-NUMBER) to get the new loan*/
		/*    number (LOAN-LOAN-NUMBER). If not, set the new loan number*/
		/*    to 1.*/
		loanenqIO.setChdrcoy(crtloanrec.chdrcoy);
		loanenqIO.setChdrnum(crtloanrec.chdrnum);
		loanenqIO.setLoanNumber(99);
		loanenqIO.setFunction(varcom.endr);
		loanenqIO.setFormat(loanenqrec);
		SmartFileCode.execute(appVars, loanenqIO);
		if (isNE(loanenqIO.getStatuz(), varcom.oK)
		&& isNE(loanenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(loanenqIO.getParams());
			dbError580();
		}
		if (isNE(loanenqIO.getChdrcoy(), crtloanrec.chdrcoy)
		|| isNE(loanenqIO.getChdrnum(), crtloanrec.chdrnum)
		|| isEQ(loanenqIO.getStatuz(), varcom.endp)) {
			loanenqIO.setLoanNumber(ZERO);
			loanenqIO.setStatuz(varcom.endp);
		}
		setPrecision(loanIO.getLoanNumber(), 0);
		loanIO.setLoanNumber(add(1, loanenqIO.getLoanNumber()));
		crtloanrec.loanno.set(loanIO.getLoanNumber());
	}

protected void readT66332000()
	{
		t66332010();
	}

protected void t66332010()
	{
		/*    Read T6633 to see whether there are any loan rules.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(crtloanrec.chdrcoy);
		itdmIO.setItemtabl(t6633);
		/* MOVE CRTL-CNTTYPE           TO ITDM-ITEMITEM.                */
		wsaaT6633Cnttype.set(crtloanrec.cnttype);
		wsaaT6633Type.set(crtloanrec.loantype);
		itdmIO.setItemitem(wsaaT6633Key);
		itdmIO.setItmfrm(crtloanrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");


		itdmIO.setFormat(itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
		}
		if (isNE(itdmIO.getItemcoy(), crtloanrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t6633)
		|| isNE(itdmIO.getItemitem(), wsaaT6633Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(e723);
			syserrrec.params.set(itdmIO.getParams());
			dbError580();
			/**        MOVE SPACES             TO T6633-T6633-REC               */
		}
		else {
			t6633rec.t6633Rec.set(itdmIO.getGenarea());
		}
	}

protected void calcNextCapnDate3000()
	{
		next3010();
	}

protected void next3010()
	{
		/*    We need to set the next interest billing date (NXTINTBDTE)*/
		/*    and the next capitalisation date (NXTCAPDATE).*/
		/*    Set these dates depending on what is set in the T6633 table.*/
		/*    Check the capitalisation details on T6633 in the following*/
		/*    order: i) Capitalise on Loan anniv ... Y/N*/
		/*          ii) Capitalise on Policy anniv.. Y/N*/
		/*         iii) Check frequency and fixed date.*/
		wsaaLoanDate.set(crtloanrec.effdate);
		wsaaContractDate.set(crtloanrec.occdate);
		/*    Check if loan anniversary flag is set.*/
		if (isEQ(t6633rec.annloan, "Y")) {
			/*    Call Datcon2 to add 1 year on to loan start date.*/
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(crtloanrec.effdate);
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			callDatcon27000();
			loanIO.setNextCapnDate(datcon2rec.intDate2);
			return ;
		}
		/*    Check if contract anniversary flag is set.*/
		if (isEQ(t6633rec.annpoly, "Y")
		&& isEQ(wsaaContractDate, wsaaLoanDate)) {
			/*    Contract start date = Loan start date*/
			/*    so add 1 year on to loan date.*/
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(crtloanrec.effdate);
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			callDatcon27000();
			loanIO.setNextCapnDate(datcon2rec.intDate2);
			return ;
		}
		/*    Contract start date must be before loan date - cannot have*/
		/*    a loan starting before a contract !!!!!*/
		if (isEQ(t6633rec.annpoly, "Y")
		&& isGT(wsaaContractMd, wsaaLoanMd)) {
			/*    The month & day on the contract are later in the year*/
			/*    than the month & day on the loan therefore the first*/
			/*    capitalisation calculation date is on the contract*/
			/*    anniversary later the same year.*/
			/*    Example...*/
			/*        Contract start date..................... 19910303*/
			/*        Loan start date......................... 19920107*/
			/*        Next capitalisation calculation date ... 19920303*/
			wsaaNewMd.set(wsaaContractMd);
			wsaaNewYear.set(wsaaLoanYear);
			loanIO.setNextCapnDate(wsaaNewDate);
			return ;
		}
		if (isEQ(t6633rec.annpoly, "Y")
		&& isLTE(wsaaContractMd, wsaaLoanMd)) {
			/*    The month & day on the contract are earlier in the year*/
			/*    than the month & day on the loan, therefore the first*/
			/*    capitalisation calculation date is on the contract*/
			/*    anniversary in the following year.*/
			/*    Example...*/
			/*        Contract start date.................... 19910303*/
			/*        Loan start date........................ 19920406*/
			/*        Next capitalisation calculation date... 19930303*/
			wsaaNewMd.set(wsaaContractMd);
			wsaaNewYear.set(wsaaLoanYear);
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(wsaaNewDate);
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			callDatcon27000();
			loanIO.setNextCapnDate(datcon2rec.intDate2);
			return ;
		}
		fixedCapitalisation3100();
	}

protected void fixedCapitalisation3100()
	{
		fixed3110();
	}

protected void fixed3110()
	{
		/*    Get here so the next capitalisation date isn't based on loan*/
		/*    or contract anniversarys.*/
		wsaaNewDate.set(ZERO);
		/*    Check for a fixed date on T6633. If none is specified,*/
		/*    use the Loan day and month.*/
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaLoanMonth);
		wsaaIntYear.set(wsaaLoanYear);
		if (isNE(t6633rec.day, 0)) {
			wsaaDayCheck.set(t6633rec.day);
			if (!daysLessThan28.isTrue()) {
				dateSet8000();
			}
			else {
				/*     END-IF                                                   */
				wsaaNewDay.set(t6633rec.day);
			}
		}
		else {
			wsaaNewDay.set(wsaaLoanDay);
		}
		
		if (isEQ(wsaaNewDay,ZERO))  //MTL-350
		{
			wsaaNewDay.set(wsaaLoanDay);
		}
		

		wsaaNewYear.set(wsaaLoanYear);
		wsaaNewMonth.set(wsaaLoanMonth);
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(wsaaNewDate);
		datcon2rec.freqFactor.set(1);
		/*    Check for a fixed frequency on T6633 for capitalisation*/
		/*    calculations. If none is specified, use 1 year as the*/
		/*    default value.*/
		if (isEQ(t6633rec.compfreq, SPACES)) {
			datcon2rec.frequency.set("01");
		}
		else {
			datcon2rec.frequency.set(t6633rec.compfreq);
		}
		callDatcon27000();
		wsaaNewDate.set(datcon2rec.intDate2);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaNewMonth);
		wsaaIntYear.set(wsaaNewYear);
		if (!daysLessThan28.isTrue()) {
			dateSet8000();
			loanIO.setNextCapnDate(wsaaNewDate);
		}
		else {
			loanIO.setNextCapnDate(datcon2rec.intDate2);
		}
	}

protected void calcNextInterestDate4000()
	{
		next4010();
	}

protected void next4010()
	{
		/*    Check the interest  details on T6633 in the following*/
		/*    order: i) Calculate interest on Loan anniv ... Y/N*/
		/*          ii) Calculate interest on Policy anniv.. Y/N*/
		/*         iii) Check frequency and fixed date.*/
		/*    Check if loan anniversary flag is set.*/
		if (isEQ(t6633rec.loanAnnivInterest, "Y")) {
			/*    Call datcon2 to add 1 year on to loan start date*/
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(crtloanrec.effdate);
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			callDatcon27000();
			loanIO.setNextIntBillDate(datcon2rec.intDate2);
			return ;
		}
		/*    Check if contract anniversary flag is set.*/
		if (isEQ(t6633rec.policyAnnivInterest, "Y")
		&& isEQ(wsaaContractDate, wsaaLoanDate)) {
			/*    Contract start date = Loan start date*/
			/*    so add 1 year on to loan date.*/
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(crtloanrec.effdate);
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			callDatcon27000();
			loanIO.setNextIntBillDate(datcon2rec.intDate2);
			return ;
		}
		/*    Contract start date must be before loan date - cannot have*/
		/*    a loan starting before a contract !!!!!*/
		if (isEQ(t6633rec.policyAnnivInterest, "Y")
		&& isGT(wsaaContractMd, wsaaLoanMd)) {
			/*    The month & day on the contract are later in the year than*/
			/*    the month & day on the loan therefore the first interest*/
			/*    calculation date is on the contract anniversary later in*/
			/*    the same year.*/
			/*    Example...*/
			/*              Contract start date............... 19910303*/
			/*              Loan start date................... 19920107*/
			/*              Next interest calculation date ... 19920303*/
			wsaaNewMd.set(wsaaContractMd);
			wsaaNewYear.set(wsaaLoanYear);
			loanIO.setNextIntBillDate(wsaaNewDate);
			return ;
		}
		if (isEQ(t6633rec.policyAnnivInterest, "Y")
		&& isLTE(wsaaContractMd, wsaaLoanMd)) {
			/*    The month & day on the contract are earlier in the year than*/
			/*    the month & day on the loan therefore the first interest*/
			/*    calculation date is on the contract anniversary in the*/
			/*    following year.*/
			/*    Example...*/
			/*              Contract start date.............. 19910303*/
			/*              Loan start date.................. 19920406*/
			/*              Next interest calculation date... 19930303*/
			wsaaNewMd.set(wsaaContractMd);
			wsaaNewYear.set(wsaaLoanYear);
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(wsaaNewDate);
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			callDatcon27000();
			loanIO.setNextIntBillDate(datcon2rec.intDate2);
			return ;
		}
		fixedInterest4100();
	}

protected void fixedInterest4100()
	{
		fixed4110();
	}

protected void fixed4110()
	{
		/*    Get here so the next interest calculation date is not*/
		/*    based on loan or contract anniversarys.*/
		wsaaNewDate.set(ZERO);
		/*    Check for a fixed date on T6633. If none is specified,*/
		/*    use the Loan day and month.*/
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaLoanMonth);
		wsaaIntYear.set(wsaaNewYear);
		if (isNE(t6633rec.interestDay, 0)) {
			wsaaDayCheck.set(t6633rec.interestDay);
			if (!daysLessThan28.isTrue()) {
				dateSet8000();
			}
			else {
				/*     END-IF                                                   */
				wsaaNewDay.set(t6633rec.interestDay);
			}
		}
		else {
			wsaaNewDay.set(wsaaLoanDay);
		}
		
		if (isEQ(wsaaNewDay,ZERO)) //MTL-350
		{
			wsaaNewDay.set(wsaaLoanDay);
		}
		
		wsaaNewYear.set(wsaaLoanYear);
		wsaaNewMonth.set(wsaaLoanMonth);
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(wsaaNewDate);
		datcon2rec.freqFactor.set(1);
		/*    Check for a fixed frequency on T6633 for interest*/
		/*    calculations. If none is specified, use 1 year as*/
		/*    the default value.*/
		if (isEQ(t6633rec.interestFrequency, SPACES)) {
			datcon2rec.frequency.set("01");
		}
		else {
			datcon2rec.frequency.set(t6633rec.interestFrequency);
		}
		callDatcon27000();
		wsaaNewDate.set(datcon2rec.intDate2);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaNewMonth);
		wsaaIntYear.set(wsaaNewYear);
		if (!daysLessThan28.isTrue()) {
			dateSet8000();
			loanIO.setNextIntBillDate(wsaaNewDate);
		}
		else {
			loanIO.setNextIntBillDate(datcon2rec.intDate2);
		}
	}

protected void writeLoanRecord5000()
	{
		start5010();
	}

protected void start5010()
	{
		initialize(loanIO.getTplstmdty());
		loanIO.setFormat(loanrec);
		loanIO.setChdrcoy(crtloanrec.chdrcoy);
		loanIO.setChdrnum(crtloanrec.chdrnum);
		loanIO.setFirstTranno(crtloanrec.tranno);
		loanIO.setLastTranno(ZERO);
		/* MOVE 'A'                    TO LOAN-LOAN-TYPE.               */
		loanIO.setLoanType(crtloanrec.loantype);
		loanIO.setLoanCurrency(crtloanrec.cntcurr);
		loanIO.setLoanOriginalAmount(crtloanrec.outstamt);
		loanIO.setLastCapnLoanAmt(crtloanrec.outstamt);
		loanIO.setLoanStartDate(crtloanrec.effdate);
		loanIO.setLastCapnDate(crtloanrec.effdate);
		loanIO.setLastIntBillDate(crtloanrec.effdate);
		loanIO.setTransactionDate(crtloanrec.effdate);
		loanIO.setTermid(varcom.vrcmTermid);
		loanIO.setTransactionTime(wsaaTime);
		loanIO.setUser(0);
		loanIO.setValidflag("1");
		loanIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, loanIO);
		if (isNE(loanIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(loanIO.getParams());
			dbError580();
		}
	}

protected void writeAplAcmv6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start6010();
					chkPstw6020();
				case postPrincipal6030:
					postPrincipal6030();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start6010()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(crtloanrec.chdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(crtloanrec.chdrcoy);
		lifacmvrec.rldgcoy.set(crtloanrec.chdrcoy);
		lifacmvrec.genlcoy.set(crtloanrec.chdrcoy);
		lifacmvrec.batckey.set(crtloanrec.batchkey);
		/*    Set correct Transaction code on ACMV records*/
		lifacmvrec.batctrcde.set(crtloanrec.authCode);
		lifacmvrec.genlcur.set(SPACES);
		/* MOVE CRTL-CNTCURR           TO LIFA-ORIGCURR.                */
		/* MOVE CRTL-OUTSTAMT          TO LIFA-ORIGAMT.                 */
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.tranno.set(crtloanrec.tranno);
		/* MOVE MAXDATE                TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.effdate.set(crtloanrec.effdate);
		lifacmvrec.tranref.set(crtloanrec.chdrnum);
		lifacmvrec.substituteCode[1].set(crtloanrec.cnttype);
		lifacmvrec.trandesc.set(crtloanrec.longdesc);
		wsaaRldgacct.set(SPACES);
		wsaaRldgChdrnum.set(crtloanrec.chdrnum);
		/* For non APL case, we want identified the Loan/Deposits by    */
		/* supplying the Loan number in the Ledger account.             */
		/* For Prem deposit, loan suspense account is the premium suspense */
		/* account. Loan no. not required.                                 */
		if (isNE(loanIO.getLoanType(), "A")
		&& isNE(loanIO.getLoanType(), "D")) {
			wsaaRldgLoanno.set(loanIO.getLoanNumber());
		}
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.transactionDate.set(crtloanrec.effdate);
		lifacmvrec.transactionTime.set(wsaaTime);
		lifacmvrec.user.set(0);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		/* Convert Loan amount to Billing Currency if not the same         */
		/* as Loan Currency.                                               */
		if (isNE(crtloanrec.cntcurr, crtloanrec.billcurr)) {
			convertLoan6100();
			lifacmvrec.origcurr.set(crtloanrec.billcurr);
			lifacmvrec.origamt.set(conlinkrec.amountOut);
		}
		else {
			lifacmvrec.origcurr.set(crtloanrec.cntcurr);
			lifacmvrec.origamt.set(crtloanrec.outstamt);
		}
	}

protected void chkPstw6020()
	{
		if (isEQ(crtloanrec.function, "PSTW")) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(crtloanrec.function, "PSLN")) {
				goTo(GotoLabel.postPrincipal6030);
			}
		}
		/*    Post to suspense the loan amount*/
		lifacmvrec.sacscode.set(crtloanrec.sacscode01);
		lifacmvrec.sacstyp.set(crtloanrec.sacstyp01);
		lifacmvrec.glcode.set(crtloanrec.glcode01);
		lifacmvrec.glsign.set(crtloanrec.glsign01);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserr570();
		}
	}

	/**
	* <pre>
	*    Post to loan principal account
	* </pre>
	*/
protected void postPrincipal6030()
	{
		wsaaRldgacct.set(SPACES);
		wsaaRldgChdrnum.set(crtloanrec.chdrnum);
		wsaaRldgLoanno.set(loanIO.getLoanNumber());
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.sacscode.set(crtloanrec.sacscode02);
		lifacmvrec.sacstyp.set(crtloanrec.sacstyp02);
		lifacmvrec.glcode.set(crtloanrec.glcode02);
		lifacmvrec.glsign.set(crtloanrec.glsign02);
		lifacmvrec.origcurr.set(crtloanrec.cntcurr);
		lifacmvrec.origamt.set(crtloanrec.outstamt);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserr570();
		}
		/* Post to currency conversion accounts if currencies differ.      */
		if (isEQ(crtloanrec.cntcurr, crtloanrec.billcurr)) {
			return ;
		}
		wsaaRldgacct.set(SPACES);
		wsaaRldgChdrnum.set(crtloanrec.chdrnum);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.origcurr.set(crtloanrec.cntcurr);
		lifacmvrec.origamt.set(crtloanrec.outstamt);
		lifacmvrec.glcode.set(crtloanrec.glcode03);
		lifacmvrec.sacscode.set(crtloanrec.sacscode03);
		lifacmvrec.sacstyp.set(crtloanrec.sacstyp03);
		lifacmvrec.glsign.set(crtloanrec.glsign03);
		lifacmvrec.jrnseq.add(1);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserr570();
		}
		lifacmvrec.origcurr.set(crtloanrec.billcurr);
		lifacmvrec.origamt.set(conlinkrec.amountOut);
		lifacmvrec.glcode.set(crtloanrec.glcode04);
		lifacmvrec.sacscode.set(crtloanrec.sacscode04);
		lifacmvrec.sacstyp.set(crtloanrec.sacstyp04);
		lifacmvrec.glsign.set(crtloanrec.glsign04);
		lifacmvrec.jrnseq.add(1);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserr570();
		}
	}

protected void convertLoan6100()
	{
		start6110();
	}

protected void start6110()
	{
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.company.set(crtloanrec.chdrcoy);
		conlinkrec.cashdate.set(crtloanrec.effdate);
		conlinkrec.currIn.set(crtloanrec.cntcurr);
		conlinkrec.currOut.set(crtloanrec.billcurr);
		conlinkrec.amountIn.set(crtloanrec.outstamt);
		conlinkrec.function.set("SURR");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			syserr570();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void callDatcon27000()
	{
		/*START*/
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			syserr570();
		}
		/*EXIT*/
	}

protected void dateSet8000()
	{
		start8010();
	}

protected void start8010()
	{
		/*    We have to check that the date we are going to call Datcon2*/
		/*    with is a valid from-date. If the interest/capn day in*/
		/*    T6633 is greater than the number of days in the from-date*/
		/*    month i.e. day = 30 and month = February, we want to set it*/
		/*    to the maximum day corresponding to that month i.e. for*/
		/*    February, use 28 days; for June, use 30 days.*/
		if (wsaaMonthCheckInner.january.isTrue()
		|| wsaaMonthCheckInner.march.isTrue()
		|| wsaaMonthCheckInner.may.isTrue()
		|| wsaaMonthCheckInner.july.isTrue()
		|| wsaaMonthCheckInner.august.isTrue()
		|| wsaaMonthCheckInner.october.isTrue()
		|| wsaaMonthCheckInner.december.isTrue()) {
			wsaaNewDay.set(wsaaDayCheck);
			return ;
		}
		if (wsaaMonthCheckInner.april.isTrue()
		|| wsaaMonthCheckInner.june.isTrue()
		|| wsaaMonthCheckInner.september.isTrue()
		|| wsaaMonthCheckInner.november.isTrue()) {
			if (daysInJan.isTrue()) {
				wsaaNewDay.set(wsaaAprilDays);
			}
			else {
				wsaaNewDay.set(wsaaDayCheck);
			}
			return ;
		}
		if (wsaaMonthCheckInner.february.isTrue()) {
			checkLeapYear5000();
			if (isEQ(wsaaLeapYear, "Y")) {
				wsaaNewDay.set(wsaaLeapFebruaryDays);
			}
			else {
				wsaaNewDay.set(wsaaFebruaryDays);
			}
		}
	}

protected void syserr570()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaProg);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		crtloanrec.statuz.set(varcom.bomb);
		mainlineExit010();
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaProg);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		crtloanrec.statuz.set(varcom.bomb);
		mainlineExit010();
	}

protected void checkLeapYear5000()
	{
		chkLeapYear5000();
		setInd5020();
	}

protected void chkLeapYear5000()
	{
		wsaaCenturys.set(wsaaIntYear);
		if (isEQ(wsaaCenturyYear, ZERO)) {
			compute(wsaaResult, 0).set(div(wsaaIntYear, 400));
			wsaaResult.multiply(400);
			return ;
		}
		compute(wsaaResult, 0).set(div(wsaaIntYear, 4));
		wsaaResult.multiply(4);
	}

protected void setInd5020()
	{
		if (isEQ(wsaaResult, wsaaIntYear)) {
			wsaaLeapYear = "Y";
		}
		else {
			wsaaLeapYear = "N";
		}
		/*EXIT1*/
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(crtloanrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(crtloanrec.billcurr);
		zrdecplrec.batctrcde.set(crtloanrec.authCode);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			syserr570();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-MONTH-CHECK--INNER
 */
private static final class WsaaMonthCheckInner {

	private ZonedDecimalData wsaaMonthCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator january = new Validator(wsaaMonthCheck, "01");
	private Validator february = new Validator(wsaaMonthCheck, "02");
	private Validator march = new Validator(wsaaMonthCheck, "03");
	private Validator april = new Validator(wsaaMonthCheck, "04");
	private Validator may = new Validator(wsaaMonthCheck, "05");
	private Validator june = new Validator(wsaaMonthCheck, "06");
	private Validator july = new Validator(wsaaMonthCheck, "07");
	private Validator august = new Validator(wsaaMonthCheck, "08");
	private Validator september = new Validator(wsaaMonthCheck, "09");
	private Validator october = new Validator(wsaaMonthCheck, "10");
	private Validator november = new Validator(wsaaMonthCheck, "11");
	private Validator december = new Validator(wsaaMonthCheck, "12");
}
}
