package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.ZptnpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Zptnpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ZptnpfDAOImpl extends BaseDAOImpl<Zptnpf> implements ZptnpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(ZptnpfDAOImpl.class);

	@Override
    public void insertZptnpf(List<Zptnpf> zptnpfList) {
        if (zptnpfList != null && zptnpfList.size() > 0) {
            StringBuilder sql = new StringBuilder();
            sql.append(
                    "INSERT INTO ZPTNPF(CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,TRANNO,EFFDATE,ORIGAMT,TRCDE,BILLCD,INSTFROM,INSTTO,ZPRFLG")
                    .append(",TRANDATE,USRPRF,JOBNM,DATIME)").append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = getPrepareStatement(sql.toString());
            try {
                for (Zptnpf c : zptnpfList) {
                    ps.setString(1, c.getChdrcoy());
                    ps.setString(2, c.getChdrnum());
                    ps.setString(3, c.getLife());
                    ps.setString(4, c.getCoverage());
                    ps.setString(5, c.getRider());
                    ps.setInt(6, c.getTranno());
                    ps.setInt(7, c.getEffdate());
                    ps.setBigDecimal(8, c.getOrigamt());
                    ps.setString(9, c.getTransCode());
                    ps.setInt(10, c.getBillcd());
                    ps.setInt(11, c.getInstfrom());
                    ps.setInt(12, c.getInstto());
                    ps.setString(13, c.getZprflg());
                    ps.setInt(14, c.getTrandate());
                    ps.setString(15, getUsrprf());
                    ps.setString(16, getJobnm());
                    ps.setTimestamp(17, new Timestamp(System.currentTimeMillis()));
                    ps.addBatch();
                }
                ps.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("insertZptnpf()", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(ps, null);
            }
            zptnpfList.clear();
        }
    }
	
	public List<Zptnpf> searchZptnrevRecord(String coy, String chdrnum) {
		String sql = "SELECT UNIQUE_NUMBER,chdrcoy,chdrnum,life,coverage,rider,tranno,effdate,origamt,billcd,instfrom,instto,zprflg,trandate FROM zptnrev WHERE CHDRCOY=? AND CHDRNUM=? ";
		PreparedStatement ps = getPrepareStatement(sql);
		ResultSet rs = null;
		List<Zptnpf> searchResult = new LinkedList<>();
		try {
			ps.setString(1, coy);
			ps.setString(2, chdrnum);
			rs = executeQuery(ps);

			while (rs.next()) {
				Zptnpf t = new Zptnpf();
				t.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				t.setChdrnum(rs.getString("chdrnum"));
				t.setChdrcoy(rs.getString("chdrcoy"));
				t.setLife(rs.getString("Life"));
				t.setCoverage(rs.getString("Coverage"));
				t.setRider(rs.getString("Rider"));
				t.setTranno(rs.getInt("Tranno"));
				t.setEffdate(rs.getInt("Effdate"));
				t.setOrigamt(rs.getBigDecimal("Origamt"));
				t.setBillcd(rs.getInt("Billcd"));
				t.setInstfrom(rs.getInt("Instfrom"));
				t.setInstto(rs.getInt("Instto"));
				t.setZprflg(rs.getString("Zprflg"));
				t.setTrandate(rs.getInt("Trandate"));
				searchResult.add(t);
			}

		} catch (SQLException e) {
			LOGGER.error("searchZptnrevRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return searchResult;
	}  

}
