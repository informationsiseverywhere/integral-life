package com.csc.life.regularprocessing.recordstructures;


import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;


/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 12 Feb 2021 03:06:44
 * Description:
 * Copybook name: FMCCALCREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */

public class FMCCalcrec extends ExternalData{

	   //*******************************
	   //Attribute Declarations
	   //*******************************
		//IBPLIFE-1369
	  	public FixedLengthStringData fmcCalcRec = new FixedLengthStringData(129);
	  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(fmcCalcRec, 0);
	  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(fmcCalcRec, 5);
	  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(fmcCalcRec, 9);
	  	public FixedLengthStringData fund = new FixedLengthStringData(4).isAPartOf(fmcCalcRec, 12);
	  	public PackedDecimalData lage = new PackedDecimalData(3, 0).isAPartOf(fmcCalcRec, 16);
	  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(fmcCalcRec, 19);
	  	public FixedLengthStringData gender = new FixedLengthStringData(1).isAPartOf(fmcCalcRec, 23);
	  	public FixedLengthStringData mortality = new FixedLengthStringData(1).isAPartOf(fmcCalcRec, 24);
	  	public PackedDecimalData sumins = new PackedDecimalData(17, 2).isAPartOf(fmcCalcRec, 25);
	  	public PackedDecimalData fundValue = new PackedDecimalData(17,2).isAPartOf(fmcCalcRec, 42);
	  	public PackedDecimalData outputFundMgtChg = new PackedDecimalData(9,2).isAPartOf(fmcCalcRec, 59);
	  	public PackedDecimalData outputSumAtRisk = new PackedDecimalData(9,2).isAPartOf(fmcCalcRec, 68);
	  	public PackedDecimalData outputMortality = new PackedDecimalData(9,2).isAPartOf(fmcCalcRec, 77);
	  	public PackedDecimalData totalMortality = new PackedDecimalData(17,2).isAPartOf(fmcCalcRec, 86);
	  	public PackedDecimalData totalFundChg = new PackedDecimalData(17,2).isAPartOf(fmcCalcRec, 103);
	  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(fmcCalcRec, 123);
	  	public FixedLengthStringData deathCalcMethod = new FixedLengthStringData(5).isAPartOf(fmcCalcRec, 124);
	  	
	
		public void initialize() {
			COBOLFunctions.initialize(fmcCalcRec);
		}	
		
		public FixedLengthStringData getBaseString() {
	  		if (baseString == null) {
	   			baseString = new FixedLengthStringData(getLength());
	   			fmcCalcRec.isAPartOf(baseString, true);
	   			baseString.resetIsAPartOfOffset();
	  		}
	  		return baseString;
		}

}
