package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:50
 * Description:
 * Copybook name: UTRNRNLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrnrnlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrnrnlFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrnrnlKey = new FixedLengthStringData(64).isAPartOf(utrnrnlFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrnrnlChdrcoy = new FixedLengthStringData(1).isAPartOf(utrnrnlKey, 0);
  	public FixedLengthStringData utrnrnlChdrnum = new FixedLengthStringData(8).isAPartOf(utrnrnlKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(utrnrnlKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrnrnlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrnrnlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}