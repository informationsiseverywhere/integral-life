package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.RertpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Rertpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class RertpfDAOImpl extends BaseDAOImpl<Rertpf> implements RertpfDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RertpfDAOImpl.class);

	@Override
	public List<Rertpf> getRertpfList(String chdrcoy, String chdrnum, String validflag, int tranno) {
		List<Rertpf> listRert = null;
		Rertpf rert = null;
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, EFFDATE, VALIDFLAG,");
		sb.append(" LASTINST, NEWINST, ZBLASTINST, ZBNEWINST, ZLLASTINST, ZLNEWINST, LASTZSTPDUTY, NEWZSTPDUTY, "); 
		sb.append("  TRANNO FROM RERTPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND  VALIDFLAG = ? ");
		if (tranno != 0) {
			sb.append(" AND TRANNO = ? ");
		}
		sb.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, TRANNO DESC, UNIQUE_NUMBER DESC ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, validflag);
			if (tranno != 0) {
				ps.setInt(4, tranno);
			}
			listRert = new ArrayList<Rertpf>();
			rs = ps.executeQuery();
			while(rs.next()) {
				rert = new Rertpf();
				rert.setUniqueNumber(rs.getLong(1));
				rert.setChdrcoy(rs.getString(2));
				rert.setChdrnum(rs.getString(3));
				rert.setLife(rs.getString(4));
				rert.setJlife(rs.getString(5));
				rert.setCoverage(rs.getString(6));
				rert.setRider(rs.getString(7));
				rert.setPlnsfx(rs.getInt(8));
				rert.setEffdate(rs.getInt(9));
				rert.setValidflag(rs.getString(10));
				rert.setLastInst(rs.getBigDecimal(11));
				rert.setNewinst(rs.getBigDecimal(12));
				rert.setZblastinst(rs.getBigDecimal(13));
				rert.setZbnewinst(rs.getBigDecimal(14));
				rert.setZllastinst(rs.getBigDecimal(15));
				rert.setZlnewinst(rs.getBigDecimal(16));
				rert.setLastzstpduty(rs.getBigDecimal(17));
				rert.setNewzstpduty(rs.getBigDecimal(18));
				rert.setTranno(rs.getInt(19));
				listRert.add(rert);
			}
			
		} catch (SQLException e) {
			LOGGER.error("getrertpfList()", e); 
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return listRert;
	}

	@Override
	public boolean insertRertList(List<Rertpf> rertpfList) {
		boolean isInsertSuccessful = true;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO RERTPF(CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, EFFDATE, VALIDFLAG, LASTINST,");
		sb.append(" NEWINST, ZBLASTINST, ZBNEWINST, ZLLASTINST, ZLNEWINST, LASTZSTPDUTY, NEWZSTPDUTY, USRPRF, JOBNM, DATIME, CREATED_AT, TRANNO, COMMPREM) ");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "); 
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for(Rertpf rert : rertpfList) {
				ps.setString(1, rert.getChdrcoy());
				ps.setString(2, rert.getChdrnum());
				ps.setString(3, rert.getLife());
				ps.setString(4, rert.getJlife());
				ps.setString(5, rert.getCoverage());
				ps.setString(6, rert.getRider());
				ps.setInt(7, rert.getPlnsfx());
				ps.setInt(8, rert.getEffdate());
				ps.setString(9, rert.getValidflag());
				ps.setBigDecimal(10, rert.getLastInst());
				ps.setBigDecimal(11, rert.getNewinst());
				ps.setBigDecimal(12, rert.getZblastinst());
				ps.setBigDecimal(13, rert.getZbnewinst());
				ps.setBigDecimal(14, rert.getZllastinst());
				ps.setBigDecimal(15, rert.getZlnewinst());
				ps.setBigDecimal(16, rert.getLastzstpduty() == null ? BigDecimal.ZERO : rert.getLastzstpduty());
				ps.setBigDecimal(17, rert.getNewzstpduty() == null ? BigDecimal.ZERO : rert.getNewzstpduty());
				ps.setString(18, this.getUsrprf());
				ps.setString(19, this.getJobnm());
				ps.setTimestamp(20, new Timestamp(System.currentTimeMillis()));
				ps.setTimestamp(21, new Timestamp(System.currentTimeMillis()));
				ps.setInt(22, rert.getTranno());
				ps.setBigDecimal(23, rert.getCommprem() == null ? BigDecimal.ZERO : rert.getCommprem());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertRertList()", e); 
			isInsertSuccessful = false;
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}	
		return isInsertSuccessful;
	}

	@Override
	public Map<String, List<Rertpf>> searchRertRecordByChdrnum(String chdrcoy, List<String> chdrnumList) {
        StringBuilder sb = new StringBuilder("");
        sb.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, EFFDATE, VALIDFLAG, ");
        sb.append(" LASTINST, NEWINST, ZBLASTINST, ZBNEWINST, ZLLASTINST, ZLNEWINST, LASTZSTPDUTY, NEWZSTPDUTY, "); 
		sb.append(" USRPRF, JOBNM, DATIME, TRANNO FROM RERTPF WHERE CHDRCOY = ? AND VALIDFLAG = '1' AND  "); 
        sb.append(getSqlInStr("CHDRNUM", chdrnumList));
        sb.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, UNIQUE_NUMBER DESC ");    
        LOGGER.info(sb.toString());	
        PreparedStatement ps = getPrepareStatement(sb.toString());
        ResultSet rs = null;
        Map<String, List<Rertpf>> rertMap = new HashMap<String, List<Rertpf>>();
        try {
            ps.setInt(1, Integer.parseInt(chdrcoy));
            rs = executeQuery(ps);
            while (rs.next()) {
            	Rertpf rert = new Rertpf();
            	rert.setUniqueNumber(rs.getLong(1));
				rert.setChdrcoy(rs.getString(2));
				rert.setChdrnum(rs.getString(3));
				rert.setLife(rs.getString(4));
				rert.setJlife(rs.getString(5));
				rert.setCoverage(rs.getString(6));
				rert.setRider(rs.getString(7));
				rert.setPlnsfx(rs.getInt(8));
				rert.setEffdate(rs.getInt(9));
				rert.setValidflag(rs.getString(10));
				rert.setLastInst(rs.getBigDecimal(11));
				rert.setNewinst(rs.getBigDecimal(12));
				rert.setZblastinst(rs.getBigDecimal(13));
				rert.setZbnewinst(rs.getBigDecimal(14));
				rert.setZllastinst(rs.getBigDecimal(15));
				rert.setZlnewinst(rs.getBigDecimal(16));
				rert.setLastzstpduty(rs.getBigDecimal(17));
				rert.setNewzstpduty(rs.getBigDecimal(18));
				rert.setUsrprf(rs.getString(19));
				rert.setJobnm(rs.getString(20));
				rert.setDatime(rs.getDate(21));     
				rert.setTranno(rs.getInt(22));
                if (rertMap.containsKey(rert.getChdrnum())) {
                	rertMap.get(rert.getChdrnum()).add(rert);
                } else {
                    List<Rertpf> rertList = new ArrayList<Rertpf>();
                    rertList.add(rert);
                    rertMap.put(rert.getChdrnum(), rertList);
                }
            }
        } catch (SQLException e) {
		LOGGER.error("searchRertRecordByChdrnum()", e); 
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return rertMap;
    }

	@Override
	public void updateRertList(List<Rertpf> rertpfUpdateList) { 
		if (rertpfUpdateList != null && rertpfUpdateList.size() > 0) {
        String SQL_RERT_UPDATE = "UPDATE RERTPF SET VALIDFLAG=?, JOBNM=?,USRPRF=?,DATIME=?,TRANNO=? WHERE UNIQUE_NUMBER=? ";

        PreparedStatement psRertUpdate = getPrepareStatement(SQL_RERT_UPDATE);
        try {
            for (Rertpf c : rertpfUpdateList) {
            	psRertUpdate.setString(1, c.getValidflag());
            	psRertUpdate.setString(2, getJobnm());
            	psRertUpdate.setString(3, getUsrprf());
            	psRertUpdate.setTimestamp(4, getDatime());
            	psRertUpdate.setInt(5, c.getTranno());
            	psRertUpdate.setLong(6, c.getUniqueNumber());
            	psRertUpdate.addBatch();
            }
            psRertUpdate.executeBatch();
        } catch (SQLException e) {
			LOGGER.error("updateRertList()", e); 
            throw new SQLRuntimeException(e);
        } finally {
            close(psRertUpdate, null);
        }
    }
	}
	
	public List<Rertpf> getRertpfList(String chdrcoy, String chdrnum, String life, String jlife, String coverage, String rider, int plnsfx){
		List<Rertpf> listRert = null;
		Rertpf rert = null;
		StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, EFFDATE, VALIDFLAG, LASTINST,");
		sb.append(" NEWINST, ZBLASTINST, ZBNEWINST, ZLLASTINST, ZLNEWINST, LASTZSTPDUTY, NEWZSTPDUTY,  USRPRF, JOBNM, DATIME, TRANNO FROM RERTPF");
		sb.append("  WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND JLIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND VALIDFLAG = '1'"); 
		sb.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, TRANNO DESC");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, life);
			ps.setString(4, jlife);
			ps.setString(5, coverage);
			ps.setString(6, rider);
			ps.setInt(7, plnsfx);
			listRert = new ArrayList<Rertpf>();
			rs = ps.executeQuery();
			while(rs.next()) {
				rert = new Rertpf();
				rert.setUniqueNumber(rs.getLong(1));
				rert.setChdrcoy(rs.getString(2));
				rert.setChdrnum(rs.getString(3));
				rert.setLife(rs.getString(4));
				rert.setJlife(rs.getString(5));
				rert.setCoverage(rs.getString(6));
				rert.setRider(rs.getString(7));
				rert.setPlnsfx(rs.getInt(8));
				rert.setEffdate(rs.getInt(9));
				rert.setValidflag(rs.getString(10));
				rert.setLastInst(rs.getBigDecimal(11));
				rert.setNewinst(rs.getBigDecimal(12));
				rert.setZblastinst(rs.getBigDecimal(13));
				rert.setZbnewinst(rs.getBigDecimal(14));
				rert.setZllastinst(rs.getBigDecimal(15));
				rert.setZlnewinst(rs.getBigDecimal(16));
				rert.setLastzstpduty(rs.getBigDecimal(17));
				rert.setNewzstpduty(rs.getBigDecimal(18));
				rert.setUsrprf(rs.getString(19));
				rert.setJobnm(rs.getString(20));
				rert.setDatime(rs.getDate(21));
				rert.setTranno(rs.getInt(22));
				listRert.add(rert);
			}
			
		} catch (SQLException e) {
			LOGGER.error("getRertpfList()", e); 
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return listRert;
	}

	@Override
	public void deleteRertpf(List<Rertpf> rertList) {
		String sql = "DELETE FROM RERTPF WHERE UNIQUE_NUMBER=? ";
        PreparedStatement ps = getPrepareStatement(sql);
        try {
            for (Rertpf r : rertList) {
            	ps.setLong(1, r.getUniqueNumber());					
            	ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            LOGGER.error("deleteRertpf()", e);
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
	}

}
