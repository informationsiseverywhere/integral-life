package com.csc.life.regularprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:33
 * Description:
 * Copybook name: T5655REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5655rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5655Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData leadDays = new ZonedDecimalData(3, 0).isAPartOf(t5655Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(497).isAPartOf(t5655Rec, 3, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5655Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5655Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}