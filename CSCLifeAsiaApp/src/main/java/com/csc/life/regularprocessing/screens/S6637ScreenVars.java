package com.csc.life.regularprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6637
 * @version 1.0 generated on 30/08/09 06:55
 * @author Quipoz
 */
public class S6637ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(850);
	public FixedLengthStringData dataFields = new FixedLengthStringData(242).isAPartOf(dataArea, 0);
	public FixedLengthStringData bonuss = new FixedLengthStringData(70).isAPartOf(dataFields, 0);
	public ZonedDecimalData[] bonus = ZDArrayPartOfStructure(10, 7, 2, bonuss, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(70).isAPartOf(bonuss, 0, FILLER_REDEFINE);
	public ZonedDecimalData bonus01 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData bonus02 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,7);
	public ZonedDecimalData bonus03 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,14);
	public ZonedDecimalData bonus04 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,21);
	public ZonedDecimalData bonus05 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,28);
	public ZonedDecimalData bonus06 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,35);
	public ZonedDecimalData bonus07 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,42);
	public ZonedDecimalData bonus08 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,49);
	public ZonedDecimalData bonus09 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,56);
	public ZonedDecimalData bonus10 = DD.bonus.copyToZonedDecimal().isAPartOf(filler,63);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,70);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,71);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,79);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,87);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,95);
	public FixedLengthStringData riskunits = new FixedLengthStringData(12).isAPartOf(dataFields, 125);
	public ZonedDecimalData[] riskunit = ZDArrayPartOfStructure(2, 6, 0, riskunits, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(12).isAPartOf(riskunits, 0, FILLER_REDEFINE);
	public ZonedDecimalData riskunit01 = DD.riskunit.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData riskunit02 = DD.riskunit.copyToZonedDecimal().isAPartOf(filler1,6);
	public FixedLengthStringData sumasss = new FixedLengthStringData(70).isAPartOf(dataFields, 137);
	public ZonedDecimalData[] sumass = ZDArrayPartOfStructure(10, 7, 2, sumasss, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(70).isAPartOf(sumasss, 0, FILLER_REDEFINE);
	public ZonedDecimalData sumass01 = DD.sumass.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData sumass02 = DD.sumass.copyToZonedDecimal().isAPartOf(filler2,7);
	public ZonedDecimalData sumass03 = DD.sumass.copyToZonedDecimal().isAPartOf(filler2,14);
	public ZonedDecimalData sumass04 = DD.sumass.copyToZonedDecimal().isAPartOf(filler2,21);
	public ZonedDecimalData sumass05 = DD.sumass.copyToZonedDecimal().isAPartOf(filler2,28);
	public ZonedDecimalData sumass06 = DD.sumass.copyToZonedDecimal().isAPartOf(filler2,35);
	public ZonedDecimalData sumass07 = DD.sumass.copyToZonedDecimal().isAPartOf(filler2,42);
	public ZonedDecimalData sumass08 = DD.sumass.copyToZonedDecimal().isAPartOf(filler2,49);
	public ZonedDecimalData sumass09 = DD.sumass.copyToZonedDecimal().isAPartOf(filler2,56);
	public ZonedDecimalData sumass10 = DD.sumass.copyToZonedDecimal().isAPartOf(filler2,63);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,207);
	public FixedLengthStringData yrsinfs = new FixedLengthStringData(30).isAPartOf(dataFields, 212);
	public ZonedDecimalData[] yrsinf = ZDArrayPartOfStructure(10, 3, 0, yrsinfs, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(30).isAPartOf(yrsinfs, 0, FILLER_REDEFINE);
	public ZonedDecimalData yrsinf01 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler3,0);
	public ZonedDecimalData yrsinf02 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler3,3);
	public ZonedDecimalData yrsinf03 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler3,6);
	public ZonedDecimalData yrsinf04 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler3,9);
	public ZonedDecimalData yrsinf05 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler3,12);
	public ZonedDecimalData yrsinf06 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler3,15);
	public ZonedDecimalData yrsinf07 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler3,18);
	public ZonedDecimalData yrsinf08 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler3,21);
	public ZonedDecimalData yrsinf09 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler3,24);
	public ZonedDecimalData yrsinf10 = DD.yrsinf.copyToZonedDecimal().isAPartOf(filler3,27);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(152).isAPartOf(dataArea, 242);
	public FixedLengthStringData bonussErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] bonusErr = FLSArrayPartOfStructure(10, 4, bonussErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(40).isAPartOf(bonussErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData bonus01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData bonus02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData bonus03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData bonus04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData bonus05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData bonus06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData bonus07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData bonus08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData bonus09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData bonus10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData riskunitsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData[] riskunitErr = FLSArrayPartOfStructure(2, 4, riskunitsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(riskunitsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData riskunit01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData riskunit02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData sumasssErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData[] sumassErr = FLSArrayPartOfStructure(10, 4, sumasssErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(40).isAPartOf(sumasssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData sumass01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData sumass02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData sumass03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData sumass04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData sumass05Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);
	public FixedLengthStringData sumass06Err = new FixedLengthStringData(4).isAPartOf(filler6, 20);
	public FixedLengthStringData sumass07Err = new FixedLengthStringData(4).isAPartOf(filler6, 24);
	public FixedLengthStringData sumass08Err = new FixedLengthStringData(4).isAPartOf(filler6, 28);
	public FixedLengthStringData sumass09Err = new FixedLengthStringData(4).isAPartOf(filler6, 32);
	public FixedLengthStringData sumass10Err = new FixedLengthStringData(4).isAPartOf(filler6, 36);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData yrsinfsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData[] yrsinfErr = FLSArrayPartOfStructure(10, 4, yrsinfsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(40).isAPartOf(yrsinfsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData yrsinf01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData yrsinf02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData yrsinf03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData yrsinf04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData yrsinf05Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);
	public FixedLengthStringData yrsinf06Err = new FixedLengthStringData(4).isAPartOf(filler7, 20);
	public FixedLengthStringData yrsinf07Err = new FixedLengthStringData(4).isAPartOf(filler7, 24);
	public FixedLengthStringData yrsinf08Err = new FixedLengthStringData(4).isAPartOf(filler7, 28);
	public FixedLengthStringData yrsinf09Err = new FixedLengthStringData(4).isAPartOf(filler7, 32);
	public FixedLengthStringData yrsinf10Err = new FixedLengthStringData(4).isAPartOf(filler7, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(456).isAPartOf(dataArea, 394);
	public FixedLengthStringData bonussOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] bonusOut = FLSArrayPartOfStructure(10, 12, bonussOut, 0);
	public FixedLengthStringData[][] bonusO = FLSDArrayPartOfArrayStructure(12, 1, bonusOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(120).isAPartOf(bonussOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] bonus01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] bonus02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] bonus03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] bonus04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] bonus05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] bonus06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] bonus07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] bonus08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] bonus09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] bonus10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData riskunitsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 180);
	public FixedLengthStringData[] riskunitOut = FLSArrayPartOfStructure(2, 12, riskunitsOut, 0);
	public FixedLengthStringData[][] riskunitO = FLSDArrayPartOfArrayStructure(12, 1, riskunitOut, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(24).isAPartOf(riskunitsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] riskunit01Out = FLSArrayPartOfStructure(12, 1, filler9, 0);
	public FixedLengthStringData[] riskunit02Out = FLSArrayPartOfStructure(12, 1, filler9, 12);
	public FixedLengthStringData sumasssOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 204);
	public FixedLengthStringData[] sumassOut = FLSArrayPartOfStructure(10, 12, sumasssOut, 0);
	public FixedLengthStringData[][] sumassO = FLSDArrayPartOfArrayStructure(12, 1, sumassOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(120).isAPartOf(sumasssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] sumass01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] sumass02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] sumass03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] sumass04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] sumass05Out = FLSArrayPartOfStructure(12, 1, filler10, 48);
	public FixedLengthStringData[] sumass06Out = FLSArrayPartOfStructure(12, 1, filler10, 60);
	public FixedLengthStringData[] sumass07Out = FLSArrayPartOfStructure(12, 1, filler10, 72);
	public FixedLengthStringData[] sumass08Out = FLSArrayPartOfStructure(12, 1, filler10, 84);
	public FixedLengthStringData[] sumass09Out = FLSArrayPartOfStructure(12, 1, filler10, 96);
	public FixedLengthStringData[] sumass10Out = FLSArrayPartOfStructure(12, 1, filler10, 108);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData yrsinfsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 336);
	public FixedLengthStringData[] yrsinfOut = FLSArrayPartOfStructure(10, 12, yrsinfsOut, 0);
	public FixedLengthStringData[][] yrsinfO = FLSDArrayPartOfArrayStructure(12, 1, yrsinfOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(120).isAPartOf(yrsinfsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] yrsinf01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] yrsinf02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] yrsinf03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] yrsinf04Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public FixedLengthStringData[] yrsinf05Out = FLSArrayPartOfStructure(12, 1, filler11, 48);
	public FixedLengthStringData[] yrsinf06Out = FLSArrayPartOfStructure(12, 1, filler11, 60);
	public FixedLengthStringData[] yrsinf07Out = FLSArrayPartOfStructure(12, 1, filler11, 72);
	public FixedLengthStringData[] yrsinf08Out = FLSArrayPartOfStructure(12, 1, filler11, 84);
	public FixedLengthStringData[] yrsinf09Out = FLSArrayPartOfStructure(12, 1, filler11, 96);
	public FixedLengthStringData[] yrsinf10Out = FLSArrayPartOfStructure(12, 1, filler11, 108);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6637screenWritten = new LongData(0);
	public LongData S6637protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6637ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, riskunit02, yrsinf01, yrsinf02, yrsinf03, yrsinf04, yrsinf05, yrsinf06, yrsinf07, yrsinf08, yrsinf09, yrsinf10, riskunit01, sumass01, sumass02, sumass03, sumass04, sumass05, sumass06, sumass07, sumass08, sumass09, sumass10, bonus01, bonus02, bonus03, bonus04, bonus05, bonus06, bonus07, bonus08, bonus09, bonus10};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, riskunit02Out, yrsinf01Out, yrsinf02Out, yrsinf03Out, yrsinf04Out, yrsinf05Out, yrsinf06Out, yrsinf07Out, yrsinf08Out, yrsinf09Out, yrsinf10Out, riskunit01Out, sumass01Out, sumass02Out, sumass03Out, sumass04Out, sumass05Out, sumass06Out, sumass07Out, sumass08Out, sumass09Out, sumass10Out, bonus01Out, bonus02Out, bonus03Out, bonus04Out, bonus05Out, bonus06Out, bonus07Out, bonus08Out, bonus09Out, bonus10Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, riskunit02Err, yrsinf01Err, yrsinf02Err, yrsinf03Err, yrsinf04Err, yrsinf05Err, yrsinf06Err, yrsinf07Err, yrsinf08Err, yrsinf09Err, yrsinf10Err, riskunit01Err, sumass01Err, sumass02Err, sumass03Err, sumass04Err, sumass05Err, sumass06Err, sumass07Err, sumass08Err, sumass09Err, sumass10Err, bonus01Err, bonus02Err, bonus03Err, bonus04Err, bonus05Err, bonus06Err, bonus07Err, bonus08Err, bonus09Err, bonus10Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6637screen.class;
		protectRecord = S6637protect.class;
	}

}
