package com.csc.life.regularprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:25
 * Description:
 * Copybook name: T5534REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5534rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5534Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData adfeemth = new FixedLengthStringData(4).isAPartOf(t5534Rec, 0);
  	public FixedLengthStringData jlPremMeth = new FixedLengthStringData(4).isAPartOf(t5534Rec, 4);
  	public FixedLengthStringData premmeth = new FixedLengthStringData(4).isAPartOf(t5534Rec, 8);
  	public FixedLengthStringData subprog = new FixedLengthStringData(10).isAPartOf(t5534Rec, 12);
  	public FixedLengthStringData svMethod = new FixedLengthStringData(4).isAPartOf(t5534Rec, 22);
  	public FixedLengthStringData unitFreq = new FixedLengthStringData(2).isAPartOf(t5534Rec, 26);
  	public FixedLengthStringData filler = new FixedLengthStringData(472).isAPartOf(t5534Rec, 28, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5534Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5534Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}