/*
 * File: B5350.java
 * Date: 29 August 2009 21:06:01
 * Author: Quipoz Limited
 * 
 * Class transformed from B5350.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.life.regularprocessing.dataaccess.LinrpfTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.        REVENUE ACCOUNTING (SPLITTER)
*                -----------------------------
*
* This program is the Revenue Accounting Splitter program to be
* run before Revenue Accounting (B5351) in multi-thread.
*
* The LINSPF will be read via SQL with the following criteria:-
* i)   Acctmeth = 'R'
* ii)  Payflag  not = 'P'
* iii) Dueflag  not = 'Y'
* iv)  Billchnl not = 'N'
* v)   Instfrom  <  <effective date>
* vi)  Chdrcoy   =  <run company)
* vii) Branch    =  <run branch)
* viii)Validflag =  '1'
* ix)  Chdrnum   between <P6671-chdrnum>
* x)             and     <P6671-CHDRNUM-1>
*     order by chdrcoy, chdrnum, payrseqno, instfrom
*
* Control totals used in this program:
*
*    01  -  No. of LINS extracted records
*    02  -  No. of thread members
*
* The Purpose of a Splitter Program.
* ---------------------------------
* Splitter programs should be simple. They should only deal with
* a single file. If there is insufficient data on the primary file
* to completely identify a transaction then the further editing
* should be done in the subsequent process. The objective of a
* Splitter is to rapidily isolate potential transactions, not
* to process the transaction. The primary concern for Splitter
* program design is elasped time speed and this is acheived by
* minimising disk IO.
*
* Before the Splitter process is invoked the, the program CRTTMPF
* should be run. The CRTTMPF, (Create Temporary File), will create
* a duplicate of a physical file, (created under Smart and defined
* as a Query file), which will have as many members as is defined
* in that process's 'subsequent threads' field. The temporary file
* will have the name XXXXDD9999 where XXXX is the first four
* characters of the file which was duplicated, DD is the first two
* characters of the 4th system parameter and 9999 is the schedule
* run number. Each member added to the temporary file will be
* called THREAD999 where 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read records from a primary file
* and for each record it selects it will write a record to a
* temporary file member. Exactly which temporary file member
* is written to is decided by a working storage count. The
* objective of the count is spread the transaction records evenly
* amoungst each thread member.
*
* The Splitter program should always have a restart method of
* '1' indicating a re-run. In the event of a restart the Splitter
* program will always start with empty file members. Apart from
* writing records to the temporary file members Splitter programs
* should not be doing any further updates.
*
* There should be no non-standard CL commands in the CL Pgm which
* calls this program.
*
* Notes for programs which use Splitter Program Temporary Files.
* --------------------------------------------------------------
*
* The MTBE will be able to submit multiple versions of the program
* which use the file members created from this program. It is
* important that the process which runs the splitter program has
* the same 'number of subsequent threads' as the following
* process has defined in 'number of threads this process'.
*
* The subsequent program need only point itself to one member
* based around the field BSPR-SCHEDULE-THREAD-NO which indicates
* in which thread the copy of the subsequent program is running.
*
* Only the following code should be needed to perform an OVRDBF
* to point to the correct member:
*
*****************************************************************
* </pre>
*/
public class B5350 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqllinsCursorrs = null;
	private java.sql.PreparedStatement sqllinsCursorps = null;
	private java.sql.Connection sqllinsCursorconn = null;
	private String sqllinsCursor = "";
	private int linsCursorLoopIndex = 0;
	private DiskFileDAM linr01 = new DiskFileDAM("LINR01");
	private DiskFileDAM linr02 = new DiskFileDAM("LINR02");
	private DiskFileDAM linr03 = new DiskFileDAM("LINR03");
	private DiskFileDAM linr04 = new DiskFileDAM("LINR04");
	private DiskFileDAM linr05 = new DiskFileDAM("LINR05");
	private DiskFileDAM linr06 = new DiskFileDAM("LINR06");
	private DiskFileDAM linr07 = new DiskFileDAM("LINR07");
	private DiskFileDAM linr08 = new DiskFileDAM("LINR08");
	private DiskFileDAM linr09 = new DiskFileDAM("LINR09");
	private DiskFileDAM linr10 = new DiskFileDAM("LINR10");
	private DiskFileDAM linr11 = new DiskFileDAM("LINR11");
	private DiskFileDAM linr12 = new DiskFileDAM("LINR12");
	private DiskFileDAM linr13 = new DiskFileDAM("LINR13");
	private DiskFileDAM linr14 = new DiskFileDAM("LINR14");
	private DiskFileDAM linr15 = new DiskFileDAM("LINR15");
	private DiskFileDAM linr16 = new DiskFileDAM("LINR16");
	private DiskFileDAM linr17 = new DiskFileDAM("LINR17");
	private DiskFileDAM linr18 = new DiskFileDAM("LINR18");
	private DiskFileDAM linr19 = new DiskFileDAM("LINR19");
	private DiskFileDAM linr20 = new DiskFileDAM("LINR20");
	private LinrpfTableDAM linrpfData = new LinrpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5350");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

		/*  Set up LINR parameters.*/
	private FixedLengthStringData wsaaLinrFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaLinrFn, 0, FILLER).init("LINR");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaLinrFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaLinrFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaa1 = new FixedLengthStringData(1).init("1");

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");
	private FixedLengthStringData wsaaClrtmpfError = new FixedLengthStringData(97);
		/*  Pointers to point to the member to read (IY) and write (IZ).*/
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();

		/*  SQL error message formating.*/
	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
		/*  Define the number of rows in the block to be fetched.*/
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(100);

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaLinsData = FLSInittedArray (100, 15);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaLinsData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaLinsData, 1);
	private PackedDecimalData[] wsaaPayrseqno = PDArrayPartOfArrayStructure(1, 0, wsaaLinsData, 9);
	private PackedDecimalData[] wsaaInstfrom = PDArrayPartOfArrayStructure(8, 0, wsaaLinsData, 10);

		/* WSAA-IND-ARRAY */
	private FixedLengthStringData[] wsaaLinsInd = FLSInittedArray (100, 4);
	private BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(2, 4, 0, wsaaLinsInd, 0);
		/* WSAA-HOST-VARIABLES */
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).init(0);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private int wsaaInd;
	private P6671par p6671par = new P6671par();

	private int ct01Value = 0;
	public B5350() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** This section is invoked in restart mode. Note section 1100 must*/
		/** clear each temporary file member as the members are not under*/
		/** commitment control. Note also that Splitter programs must have*/
		/** a Restart Method of 1 (re-run). Thus no updating should occur*/
		/** which would result in different records being returned from the*/
		/** primary file in a restart!*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*    Check the restart method is compatible with the program.*/
		/*    This program is restartable but will always re-run from*/
		/*    the beginning. This is because the temporary file members*/
		/*    are not under commitment control.*/
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*    Do a ovrdbf for each temporary file member. (Note we have*/
		/*    allowed for a maximum of 20.)*/
		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		/*    Since this program runs from a parameter screen move the*/
		/*    internal parameter area to the original parameter copybook*/
		/*    to retrieve the contract range*/
		p6671par.parmRecord.set(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES)
		&& isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		/*    Log the number of threads being used*/
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct02);
		callContot001();
		/*    Prepare the host effective date from the parameter screen*/
		wsaaEffdate.set(bsscIO.getEffectiveDate());
		wsaaCompany.set(bsprIO.getCompany());
		iy.set(1);
		sqllinsCursor = " SELECT  CHDRCOY, CHDRNUM, PAYRSEQNO, INSTFROM" +
	" FROM   " + getAppVars().getTableNameOverriden("LINSPF") + " " +
	" WHERE ACCTMETH = 'R'" +
	" AND PAYFLAG <> 'P'" +
	" AND DUEFLG <> 'Y'" +
	" AND VALIDFLAG = ?" +
	" AND BILLCHNL <> 'N'" +
	" AND INSTFROM <= ?" +
	" AND CHDRCOY = ?" +
	" AND CHDRNUM BETWEEN ? AND ?" +
	" ORDER BY CHDRCOY, CHDRNUM, INSTFROM, PAYRSEQNO";
		/*    Initialise the array which will hold all the records*/
		/*    returned from each fetch.*/
		for (wsaaInd = 1; !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd++){
			wsaaChdrcoy[wsaaInd].set(SPACES);
			wsaaChdrnum[wsaaInd].set(SPACES);
			wsaaPayrseqno[wsaaInd].set(0);
			wsaaInstfrom[wsaaInd].set(0);
		}
		wsaaInd = 1;
		sqlerrorflag = false;
		try {
			sqllinsCursorconn = getAppVars().getDBConnectionForTable(new com.csc.life.regularprocessing.dataaccess.LinspfTableDAM());
			sqllinsCursorps = getAppVars().prepareStatementEmbeded(sqllinsCursorconn, sqllinsCursor, "LINSPF");
			getAppVars().setDBString(sqllinsCursorps, 1, wsaa1);
			getAppVars().setDBNumber(sqllinsCursorps, 2, wsaaEffdate);
			getAppVars().setDBString(sqllinsCursorps, 3, wsaaCompany);
			getAppVars().setDBString(sqllinsCursorps, 4, wsaaChdrnumFrom);
			getAppVars().setDBString(sqllinsCursorps, 5, wsaaChdrnumTo);
			sqllinsCursorrs = getAppVars().executeQuery(sqllinsCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}

	}

protected void openThreadMember1100()
	{
		/*    The next thing we must do is construct the name of the*/
		/*    temporary file which we will be working with.*/
		/* MOVE IZ                     TO WSAA-THREAD-NUMBER.           */
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(), 1), iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaLinrFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz, varcom.oK)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression("CLRTMPF", SPACES);
			stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
			stringVariable1.addExpression(SPACES);
			stringVariable1.addExpression(wsaaLinrFn);
			stringVariable1.addExpression(SPACES);
			stringVariable1.addExpression(wsaaThreadMember);
			stringVariable1.setStringInto(wsaaClrtmpfError);
			syserrrec.statuz.set(wsaaStatuz);
			wsaaClrtmpfError.set(wsaaClrtmpfError);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression("OVRDBF FILE(LINR");
		stringVariable2.addExpression(iz);
		stringVariable2.addExpression(") TOFILE(");
		stringVariable2.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable2.addExpression("/");
		stringVariable2.addExpression(wsaaLinrFn);
		stringVariable2.addExpression(") MBR(");
		stringVariable2.addExpression(wsaaThreadMember);
		stringVariable2.addExpression(")");
		stringVariable2.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		if (isEQ(iz, 1)) {
			linr01.openOutput();
		}
		if (isEQ(iz, 2)) {
			linr02.openOutput();
		}
		if (isEQ(iz, 3)) {
			linr03.openOutput();
		}
		if (isEQ(iz, 4)) {
			linr04.openOutput();
		}
		if (isEQ(iz, 5)) {
			linr05.openOutput();
		}
		if (isEQ(iz, 6)) {
			linr06.openOutput();
		}
		if (isEQ(iz, 7)) {
			linr07.openOutput();
		}
		if (isEQ(iz, 8)) {
			linr08.openOutput();
		}
		if (isEQ(iz, 9)) {
			linr09.openOutput();
		}
		if (isEQ(iz, 10)) {
			linr10.openOutput();
		}
		if (isEQ(iz, 11)) {
			linr11.openOutput();
		}
		if (isEQ(iz, 12)) {
			linr12.openOutput();
		}
		if (isEQ(iz, 13)) {
			linr13.openOutput();
		}
		if (isEQ(iz, 14)) {
			linr14.openOutput();
		}
		if (isEQ(iz, 15)) {
			linr15.openOutput();
		}
		if (isEQ(iz, 16)) {
			linr16.openOutput();
		}
		if (isEQ(iz, 17)) {
			linr17.openOutput();
		}
		if (isEQ(iz, 18)) {
			linr18.openOutput();
		}
		if (isEQ(iz, 19)) {
			linr19.openOutput();
		}
		if (isEQ(iz, 20)) {
			linr20.openOutput();
		}
	}

protected void readFile2000()
	{
		/*  We only need to FETCH records when a full block has been*/
		/*  processed or if the end of file was retrieved in the last*/
		/*  block.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd, 1)) {
			return ;
		}
		sqlerrorflag = false;
		try {
			for (linsCursorLoopIndex = 1; isLT(linsCursorLoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqllinsCursorrs); linsCursorLoopIndex++ ){
				getAppVars().getDBObject(sqllinsCursorrs, 1, wsaaChdrcoy[linsCursorLoopIndex], wsaaNullInd[linsCursorLoopIndex][1]);
				getAppVars().getDBObject(sqllinsCursorrs, 2, wsaaChdrnum[linsCursorLoopIndex], wsaaNullInd[linsCursorLoopIndex][2]);
				getAppVars().getDBObject(sqllinsCursorrs, 3, wsaaPayrseqno[linsCursorLoopIndex]);
				getAppVars().getDBObject(sqllinsCursorrs, 4, wsaaInstfrom[linsCursorLoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*  An SQLCODE = +100 is returned when :-*/
		/*      a) no rows are returned on the first fetch*/
		/*      b) the last row of the cursor is either in the block or is*/
		/*         the last row of the block.*/
		/*  We must continue processing to the 3000- section for case b)*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaChdrnum[wsaaInd], SPACES)) {
			wsspEdterror.set(varcom.endp);
		}
		//tom chi modified, bug 1030
		else if(isEQ(wsaaChdrnum[wsaaInd],SPACES)){
			wsspEdterror.set(varcom.endp);
		}
		//end
		else {
			/*  On the first entry to the program we must set up the*/
			/*  WSAA-PREV-CHDRNUM = present chdrnum.*/
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd]);
				wsaaFirstTime.set("N");
			}
		}
	}

protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		wsaaInd = 1;
		/*  Load threads until end of array or an incomplete block is*/
		/*  detected.*/
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		/*  Re-initialise the block for next fetch and point to first row.*/
		for (wsaaInd = 1; !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd++){
			wsaaChdrcoy[wsaaInd].set(SPACES);
			wsaaChdrnum[wsaaInd].set(SPACES);
			wsaaPayrseqno[wsaaInd].set(0);
			wsaaInstfrom[wsaaInd].set(0);
		}
		wsaaInd = 1;
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*  If the the CHDRNUM being processed is not equal to the*/
		/*  to the previous we should move to the next output file member.*/
		/*  The condition is here to allow for checking the last chdrnum*/
		/*  of the old block with the first of the new and to move to the*/
		/*  next LINR member to write to if they have changed.*/
		if (isNE(wsaaChdrnum[wsaaInd], wsaaPrevChdrnum)) {
			iy.add(1);
			wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd]);
		}
		/*  Load from storage all LINR data for the same contract until*/
		/*  the CHDRNUM on LINR has changed.*/
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| isNE(wsaaChdrnum[wsaaInd], wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		linrpfData.chdrcoy.set(wsaaChdrcoy[wsaaInd]);
		linrpfData.chdrnum.set(wsaaChdrnum[wsaaInd]);
		linrpfData.payrseqno.set(wsaaPayrseqno[wsaaInd]);
		linrpfData.instfrom.set(wsaaInstfrom[wsaaInd]);
		if (isGT(iy, bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		if (isEQ(iy, 1)) {
			linr01.write(linrpfData);
		}
		if (isEQ(iy, 2)) {
			linr02.write(linrpfData);
		}
		if (isEQ(iy, 3)) {
			linr03.write(linrpfData);
		}
		if (isEQ(iy, 4)) {
			linr04.write(linrpfData);
		}
		if (isEQ(iy, 5)) {
			linr05.write(linrpfData);
		}
		if (isEQ(iy, 6)) {
			linr06.write(linrpfData);
		}
		if (isEQ(iy, 7)) {
			linr07.write(linrpfData);
		}
		if (isEQ(iy, 8)) {
			linr08.write(linrpfData);
		}
		if (isEQ(iy, 9)) {
			linr09.write(linrpfData);
		}
		if (isEQ(iy, 10)) {
			linr10.write(linrpfData);
		}
		if (isEQ(iy, 11)) {
			linr11.write(linrpfData);
		}
		if (isEQ(iy, 12)) {
			linr12.write(linrpfData);
		}
		if (isEQ(iy, 13)) {
			linr13.write(linrpfData);
		}
		if (isEQ(iy, 14)) {
			linr14.write(linrpfData);
		}
		if (isEQ(iy, 15)) {
			linr15.write(linrpfData);
		}
		if (isEQ(iy, 16)) {
			linr16.write(linrpfData);
		}
		if (isEQ(iy, 17)) {
			linr17.write(linrpfData);
		}
		if (isEQ(iy, 18)) {
			linr18.write(linrpfData);
		}
		if (isEQ(iy, 19)) {
			linr19.write(linrpfData);
		}
		if (isEQ(iy, 20)) {
			linr20.write(linrpfData);
		}
		/*    Log the number of extacted records.*/
		ct01Value++;
		/*  If the record written out above is the last in the array*/
		/*  we have already stored it to compare it with the first of the*/
		/*  next set of records to be fetched from the instalment file.*/
		/*  Set up the array for the next block of records.*/
		/*  Otherwise process the next record in the array.*/
		wsaaInd++;
		/*  Check for an incomplete block retrieved.*/
		if (isLTE(wsaaInd, wsaaRowsInBlock) && isEQ(wsaaChdrnum[wsaaInd], SPACES)) {
			wsaaEofInBlock.set("Y");
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/**    There is no pre-commit processing to be done*/
		contotrec.totval.set(ct01Value);
		contotrec.totno.set(ct01);
		callContot001();
		ct01Value = 0;
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close the open files and remove the overrides.*/
		getAppVars().freeDBConnectionIgnoreErr(sqllinsCursorconn, sqllinsCursorps, sqllinsCursorrs);
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		if (isEQ(iz, 1)) {
			linr01.close();
		}
		if (isEQ(iz, 2)) {
			linr02.close();
		}
		if (isEQ(iz, 3)) {
			linr03.close();
		}
		if (isEQ(iz, 4)) {
			linr04.close();
		}
		if (isEQ(iz, 5)) {
			linr05.close();
		}
		if (isEQ(iz, 6)) {
			linr06.close();
		}
		if (isEQ(iz, 7)) {
			linr07.close();
		}
		if (isEQ(iz, 8)) {
			linr08.close();
		}
		if (isEQ(iz, 9)) {
			linr09.close();
		}
		if (isEQ(iz, 10)) {
			linr10.close();
		}
		if (isEQ(iz, 11)) {
			linr11.close();
		}
		if (isEQ(iz, 12)) {
			linr12.close();
		}
		if (isEQ(iz, 13)) {
			linr13.close();
		}
		if (isEQ(iz, 14)) {
			linr14.close();
		}
		if (isEQ(iz, 15)) {
			linr15.close();
		}
		if (isEQ(iz, 16)) {
			linr16.close();
		}
		if (isEQ(iz, 17)) {
			linr17.close();
		}
		if (isEQ(iz, 18)) {
			linr18.close();
		}
		if (isEQ(iz, 19)) {
			linr19.close();
		}
		if (isEQ(iz, 20)) {
			linr20.close();
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
