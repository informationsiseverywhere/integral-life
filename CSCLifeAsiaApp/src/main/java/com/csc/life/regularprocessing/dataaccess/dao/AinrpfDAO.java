package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.Ainrpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface AinrpfDAO extends BaseDAO<Ainrpf> {
	
	public void insertAinrpfList(List<Ainrpf> incrpfList);
	public List<Ainrpf> searchAinrpfRecord(String chdrcoy, String aintype, int batchExtractSize, int batchID);
}
