package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:21
 * Description:
 * Copybook name: COVRBONKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrbonkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrbonFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrbonKey = new FixedLengthStringData(64).isAPartOf(covrbonFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrbonChdrcoy = new FixedLengthStringData(1).isAPartOf(covrbonKey, 0);
  	public FixedLengthStringData covrbonChdrnum = new FixedLengthStringData(8).isAPartOf(covrbonKey, 1);
  	public FixedLengthStringData covrbonLife = new FixedLengthStringData(2).isAPartOf(covrbonKey, 9);
  	public FixedLengthStringData covrbonCoverage = new FixedLengthStringData(2).isAPartOf(covrbonKey, 11);
  	public FixedLengthStringData covrbonRider = new FixedLengthStringData(2).isAPartOf(covrbonKey, 13);
  	public PackedDecimalData covrbonPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrbonKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covrbonKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrbonFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrbonFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}