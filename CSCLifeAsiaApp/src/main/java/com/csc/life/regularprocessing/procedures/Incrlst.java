/*
 * File: Incrlst.java
 * Date: 29 August 2009 22:56:59
 * Author: Quipoz Limited
 * 
 * Class transformed from INCRLST.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.life.regularprocessing.recordstructures.Incrsrec;
import com.csc.life.regularprocessing.tablestructures.T5648rec;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*                  INCREASE LOW START MORTGAGE SUBROUTINE
*
* This can be called to increase premium, it will predominantly
* be used by Low Start contracts.
*
* By  reading new table  T5648  (Automatic Increase Rates) this
* program will calculate a new Premium as follows:-
*
* - read  T5648 using method  and effective date passed in  the
*   linkage.
*
* - Calculate Premium Increase =
*       (INCC-ORIGINST01 * T5648-PERCENTAGE) / 100
*        INCC-NEWINST01 = INCC-ORIGINST01 + Premium Increase
*
* Note that the calculation is based on Simple interest only.
*
* This value is returned to the calling program.
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
* Calculate increases in basic and loaded premiums.                   *
*
*****************************************************************
* </pre>
*/
public class Incrlst extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "INCRLST";
	private PackedDecimalData wsaaIncrease = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaIncreaseBas = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaIncreaseLoa = new PackedDecimalData(17, 2);
		/* ERRORS */
	private String h065 = "H065";
	private String h036 = "H036";
		/* TABLES */
	private String t5648 = "T5648";
	private String t6658 = "T6658";
	private Incrsrec incrsrec = new Incrsrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T5648rec t5648rec = new T5648rec();
	private T6658rec t6658rec = new T6658rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		seExit8090, 
		dbExit8190
	}

	public Incrlst() {
		super();
	}

public void mainline(Object... parmArray)
	{
		incrsrec.increaseRec = convertAndSetParam(incrsrec.increaseRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		incrsrec.statuz.set(varcom.oK);
		readT56481000();
		readT66581500();
		if (isNE(t6658rec.simpleInd,SPACES)) {
			calcIncrPrem2000();
		}
		else {
			if (isNE(t6658rec.compoundInd,SPACES)) {
				calcIncrPremC5000();
			}
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readT56481000()
	{
		para1000();
	}

protected void para1000()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(incrsrec.chdrcoy);
		itdmIO.setItemtabl(t5648);
		itdmIO.setItemitem(incrsrec.annvmeth);
		itdmIO.setItmfrm(incrsrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),incrsrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5648)
		|| isNE(itdmIO.getItemitem(),incrsrec.annvmeth)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incrsrec.annvmeth);
			syserrrec.statuz.set(h065);
			systemError8000();
		}
		else {
			t5648rec.t5648Rec.set(itdmIO.getGenarea());
		}
		if (isGT(t5648rec.pctinc,incrsrec.maxpcnt)) {
			incrsrec.pctinc.set(incrsrec.maxpcnt);
		}
		else {
			incrsrec.pctinc.set(t5648rec.pctinc);
		}
		if (isLT(t5648rec.pctinc,incrsrec.minpcnt)) {
			incrsrec.pctinc.set(ZERO);
		}
	}

protected void readT66581500()
	{
		readT66581505();
	}

protected void readT66581505()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(incrsrec.chdrcoy);
		itdmIO.setItmfrm(incrsrec.effdate);
		itdmIO.setItemtabl(t6658);
		itdmIO.setItemitem(incrsrec.annvmeth);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			systemError8000();
		}
		if (isNE(itdmIO.getItemcoy(),incrsrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t6658)
		|| isNE(itdmIO.getItemitem(),incrsrec.annvmeth)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(incrsrec.chdrcoy);
			itdmIO.setItemtabl(t6658);
			itdmIO.setItemitem(incrsrec.annvmeth);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h036);
			systemError8000();
		}
		t6658rec.t6658Rec.set(itdmIO.getGenarea());
	}

protected void calcIncrPrem2000()
	{
		/*CALC-INCR-PREM*/
		compute(wsaaIncrease, 2).set(div((mult(incrsrec.originst01,incrsrec.pctinc)),100));
		compute(incrsrec.newinst01, 2).set(add(incrsrec.lastinst01,wsaaIncrease));
		compute(wsaaIncreaseBas, 2).set(div((mult(incrsrec.originst02,incrsrec.pctinc)),100));
		compute(incrsrec.newinst02, 2).set(add(incrsrec.lastinst02,wsaaIncreaseBas));
		compute(wsaaIncreaseLoa, 2).set(div((mult(incrsrec.originst03,incrsrec.pctinc)),100));
		compute(incrsrec.newinst03, 2).set(add(incrsrec.lastinst03,wsaaIncreaseLoa));
		/*EXIT*/
	}

protected void calcIncrPremC5000()
	{
		/*CALC-INCR-PREM*/
		compute(wsaaIncrease, 2).set(div((mult(incrsrec.currinst01,incrsrec.pctinc)),100));
		compute(incrsrec.newinst01, 2).set(add(incrsrec.currinst01,wsaaIncrease));
		compute(wsaaIncreaseBas, 2).set(div((mult(incrsrec.currinst02,incrsrec.pctinc)),100));
		compute(incrsrec.newinst02, 2).set(add(incrsrec.currinst02,wsaaIncreaseBas));
		compute(wsaaIncreaseLoa, 2).set(div((mult(incrsrec.currinst03,incrsrec.pctinc)),100));
		compute(incrsrec.newinst03, 2).set(add(incrsrec.currinst03,wsaaIncreaseLoa));
		/*EXIT*/
	}

protected void systemError8000()
	{
		try {
			se8000();
		}
		catch (GOTOException e){
		}
		finally{
			seExit8090();
		}
	}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit8090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		incrsrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
		try {
			db8100();
		}
		catch (GOTOException e){
		}
		finally{
			dbExit8190();
		}
	}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit8190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		incrsrec.statuz.set(varcom.bomb);
		exit090();
	}
}
