/*
 * File: Overdue3.java
 * Date: 29 August 2009 23:02:23
 * Author: Quipoz Limited
 * 
 * Class transformed from OVERDUE3.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  This routine is normally used in the 2nd Overdue Parameter
*  in T6654 to trigger for overdue 1st reminder.
*
* This subroutine is called by program B5355 and it
* in turn calls LETRQST program which writes LETC
* records.
*
* Parameters passed by this subroutine to LETRQST are
* listed below.
*
*    1. OVRD-COMPANY
*    2. OVRD-LETTER-TYPE
*    3. OVRD-EFFDATE
*    4. OVRD-CHDRCOY
*    5. OVRD-COWNNUM
*
****************************************************************
* </pre>
*/
public class Overdue3 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "OVERDUE3";
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Ovrduerec ovrduerec = new Ovrduerec();

	public Overdue3() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		ovrduerec.ovrdueRec = convertAndSetParam(ovrduerec.ovrdueRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		addLetter110();
		exit190();
	}

	/**
	* <pre>
	* The following section writes a new record on LETCPF with
	* the 'NEXT LETTER TYPE'.
	* </pre>
	*/
protected void addLetter110()
	{
		syserrrec.subrname.set(wsaaSubr);
		ovrduerec.statuz.set(varcom.oK);
		letrqstrec.statuz.set(SPACES);
		/* MOVE OVRD-COMPANY           TO LETRQST-REQUEST-COMPANY.      */
		letrqstrec.letterType.set(wsaaSubr);
		letrqstrec.letterRequestDate.set(ovrduerec.effdate);
		/* MOVE OVRD-CHDRCOY           TO LETRQST-CLNTCOY.              */
		letrqstrec.clntcoy.set(ovrduerec.company);
		letrqstrec.requestCompany.set(ovrduerec.chdrcoy);
		letrqstrec.clntnum.set(ovrduerec.cownnum);
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(ovrduerec.chdrcoy);
		letrqstrec.chdrcoy.set(ovrduerec.chdrcoy);
		/* MOVE OVRD-COMPANY           TO LETRQST-RDOCCOY.       <PCPRT>*/
		letrqstrec.rdocnum.set(ovrduerec.chdrnum);
		letrqstrec.chdrnum.set(ovrduerec.chdrnum);
		letrqstrec.tranno.set(ovrduerec.tranno);
		letrqstrec.trcde.set(ovrduerec.trancode);
		/* MOVE OVRD-CHDRNUM           TO LETRQST-OTHER-KEYS.           */
		letrqstrec.branch.set(ovrduerec.batcbrn);
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(letrqstrec.statuz);
			ovrduerec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
	}

protected void exit190()
	{
		exitProgram();
	}

protected void fatalError600()
	{
		/*START*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
	}
}
