package com.csc.life.regularprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: WopxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:57
 * Class transformed from WOPXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class WopxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 18;
	public FixedLengthStringData wopxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData wopxpfRecord = wopxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(wopxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(wopxrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(wopxrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(wopxrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(wopxrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(wopxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public WopxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for WopxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public WopxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for WopxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public WopxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for WopxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public WopxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("WOPXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getWopxrec() {
  		return wopxrec;
	}

	public FixedLengthStringData getWopxpfRecord() {
  		return wopxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setWopxrec(what);
	}

	public void setWopxrec(Object what) {
  		this.wopxrec.set(what);
	}

	public void setWopxpfRecord(Object what) {
  		this.wopxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(wopxrec.getLength());
		result.set(wopxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}