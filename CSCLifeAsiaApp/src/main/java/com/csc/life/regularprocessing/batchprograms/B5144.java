/*
 * File: B5144.java
 * Date: 29 August 2009 20:59:50
 * Author: Quipoz Limited
 * 
 * Class transformed from B5144.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.regularprocessing.dataaccess.AinrTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* REMARKS.
*         CLEAR AUTOMATIC INCREASE REPORT FILE BATCH PROGRAM
*         --------------------------------------------------
*
* Overview
* ________
*
* This program reads records from the AINRPF physical file and
* deletes either pending or actual reporting records depending on
* the value in system parameter 3 of the process definition.
* The effect of the program is to clear out AINRPF records from a
* previous run before the processing program is run so that the
* reporting program only reports on records from the latest run.
*
* The Control Totals maintained within this program are :
*     01  -  Number of records read.
*     02  -  Number of records deleted.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class B5144 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5144");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/* FORMATS */
	private String ainrrec = "AINRREC";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
		/* ERRORS */
	private String ivrm = "IVRM";

	private FixedLengthStringData wsaaSysparam03 = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaType = new FixedLengthStringData(4).isAPartOf(wsaaSysparam03, 0);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Auto Increase Reporting Logical View*/
	private AinrTableDAM ainrIO = new AinrTableDAM();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090
	}

	public B5144() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(),"3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		wsaaSysparam03.set(bprdIO.getSystemParam03());
		bupaIO.setDataArea(lsaaBuparec);
		ainrIO.setRecKeyData(SPACES);
		ainrIO.setRecNonKeyData(SPACES);
		ainrIO.setStatuz(varcom.oK);
		ainrIO.setChdrcoy(bsprIO.getCompany());
		ainrIO.setAintype(wsaaSysparam03);
		ainrIO.setFormat(ainrrec);
		ainrIO.setFunction(varcom.begnh);
	}

protected void readFile2000()
	{
		try {
			readFile2010();
		}
		catch (GOTOException e){
		}
	}

protected void readFile2010()
	{
		wsspEdterror.set(varcom.oK);
		SmartFileCode.execute(appVars, ainrIO);
		if (isNE(ainrIO.getStatuz(),varcom.oK)
		&& isNE(ainrIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(ainrIO.getStatuz());
			syserrrec.params.set(ainrIO.getParams());
			fatalError600();
		}
		if (isNE(ainrIO.getAintype(),wsaaType)
		|| isNE(ainrIO.getChdrcoy(),bsprIO.getCompany())
		|| isEQ(ainrIO.getStatuz(),varcom.endp)) {
			ainrIO.setStatuz(varcom.endp);
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		ainrIO.setStatuz(varcom.oK);
		ainrIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, ainrIO);
		if (isNE(ainrIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ainrIO.getStatuz());
			syserrrec.params.set(ainrIO.getParams());
			fatalError600();
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		ainrIO.setFunction(varcom.nextr);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
