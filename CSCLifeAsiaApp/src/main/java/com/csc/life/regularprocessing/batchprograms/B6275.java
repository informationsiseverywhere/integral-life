/*
 * File: B6275.java
 * Date: 29 August 2009 21:21:21
 * Author: Quipoz Limited
 *
 * Class transformed from B6275.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.dataaccess.LinspfTableDAM;
import com.csc.life.regularprocessing.dataaccess.LinsrnlTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Jctlkey;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Bachmain;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
* REMARKS
* B6275 - Regular Processing - Due Date Accounting
*
*     The system must be able to handle both Cash and Due Date
* Accounting of contracts. Cash accounting requires that the premiums
* are only accounted for when the money is available. This is catered
* for in B6209 - Collection. Due date accounting (aka Revenue or
* Accrual Accounting) requires for the premiums to be accounted for as
* soon as the are due, the contra entry for the Revenue postings
* being made to a "Premiums due" control account. These are the
* postings which are to be performed in the program.
*
*     (When the money is available, Collection, then Supense is
* debited and the monies credited to the control account. ie in B6209)
*
*     An OPNQRYF in C6275 will only present to this program only
* those LINSPF records which are Due, which have not been accounted
* for (DUEFLG not = "Y") and which are "Revenue Accounted".
*
* Sequence of events:
*     Open a Batch
*     For Each LINSPF record:
*         Update DUEFLG (so it won't get processed again)
*         Softlock CHDR, increment and update TRANNO
*         Release softlock and rewrite CHDR
*         Post total premium to "Premiums Due"
*         Post all disections to appropriate Revenue Accounts
*         Write a PTRN for this transaction
*         Update Batch totals.
*     Close Batch.
*
*****************************************************************
* </pre>
*/
public class B6275 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private LinspfTableDAM linspf = new LinspfTableDAM();
	private LinspfTableDAM linspfRec = new LinspfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B6275");

	private FixedLengthStringData wsaaRunparm1 = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaRunparm1TranCode = new FixedLengthStringData(4).isAPartOf(wsaaRunparm1, 0);
	private FixedLengthStringData wsaaRunparm1Rest = new FixedLengthStringData(6).isAPartOf(wsaaRunparm1, 4);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsbbEffdate = new PackedDecimalData(8, 0);
		/* ERRORS */
	private String e308 = "E308";
	private String h134 = "H134";
		/* TABLES */
	private String t1688 = "T1688";
	private String t5645 = "T5645";
	private String t5679 = "T5679";
	private String t5688 = "T5688";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String chdrlifrec = "CHDRLIFREC";
	private String linsrnlrec = "LINSRNLREC";
	private String ptrnrec = "PTRNREC";
	private String covrrec = "COVRREC";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private int ct06 = 6;
	private int ct07 = 7;
	private String endOfFile = "N";
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).init(0);
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Batcuprec batcuprec = new Batcuprec();
		/*Contract Header Life Fields*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
		/*Life renewals - instalments billed*/
	private LinsrnlTableDAM linsrnlIO = new LinsrnlTableDAM();
		/*Payor Details Logical File*/
	private PayrTableDAM payrIO = new PayrTableDAM();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Sftlockrec sftlockrec1 = new Sftlockrec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private Varcom varcom = new Varcom();
	private Itemkey wsaaItemkey = new Itemkey();
	private Jctlkey wsaaJctlkey = new Jctlkey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		readLins1205,
		callCovr1400,
		next1600,
		housekeeping1800,
		exit1900,
		exit3990,
		fees4200,
		tolerance4300,
		stampDuty4400,
		wvrHolding4500,
		exit4900
	}

	public B6275() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		main110();
		openBatch120();
		process130();
		closeBatch170();
		out185();
	}

protected void main110()
	{
		linspf.openInput();
		initialise200();
	}

protected void openBatch120()
	{
		batcdorrec.function.set("OPEN");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(wsaaRunparm1TranCode);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			conerrrec.params.set(batcdorrec.batcdorRec);
			conerrrec.statuz.set(batcdorrec.statuz);
			systemError005();
		}
	}

protected void process130()
	{
		while ( !(isEQ(endOfFile,"Y"))) {
			mainProcessing1000();
		}

	}

protected void closeBatch170()
	{
		batcdorrec.function.set("CLOSE");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(wsaaRunparm1TranCode);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			conerrrec.params.set(batcdorrec.batcdorRec);
			conerrrec.statuz.set(batcdorrec.statuz);
			systemError005();
		}
	}

protected void out185()
	{
		linspf.close();
		/*EXIT*/
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		endOfFile = "N";
		varcom.vrcmDate.set(getCobolDate());
		varcom.vrcmTimex.set(getCobolTime());
		varcom.vrcmTime.set(varcom.vrcmTimen);
		wsaaRunparm1.set(runparmrec.runparm1);
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set("IT");
		wsaaItemkey.itemItemcoy.set(runparmrec.company);
		wsaaItemkey.itemItemtabl.set(t5645);
		wsaaItemkey.itemItemitem.set(wsaaProg);
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(itemIO.getParams());
			conerrrec.statuz.set(h134);
			databaseError006();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set("IT");
		wsaaItemkey.itemItemcoy.set(runparmrec.company);
		wsaaItemkey.itemItemtabl.set(t1688);
		wsaaItemkey.itemItemitem.set(wsaaRunparm1TranCode);
		descIO.setDataKey(wsaaItemkey);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction("READR");
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		wsaaTransDesc.set(descIO.getLongdesc());
		wsaaJctlkey.jctlKey.set(SPACES);
		wsaaJctlkey.jctlJobsts.set("A");
		wsaaJctlkey.jctlJctlpfx.set("JC");
		wsaaJctlkey.jctlJctlcoy.set(runparmrec.company);
		wsaaJctlkey.jctlJctljn.set(runparmrec.jobname);
		wsaaJctlkey.jctlJctlacyr.set(runparmrec.acctyear);
		wsaaJctlkey.jctlJctlacmn.set(runparmrec.acctmonth);
		wsaaJctlkey.jctlJctljnum.set(runparmrec.jobnum);
	}

protected void mainProcessing1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					read1100();
					processLinsRecord1200();
				}
				case readLins1205: {
					readLins1205();
					readChdr1208();
					ledgerAccountingUpdates1300();
				}
				case callCovr1400: {
					callCovr1400();
				}
				case next1600: {
					next1600();
				}
				case housekeeping1800: {
					housekeeping1800();
				}
				case exit1900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read1100()
	{
		linspf.read(linspfRec);
		if (linspf.isAtEnd()) {
			endOfFile = "Y";
			goTo(GotoLabel.exit1900);
		}
		updateReqd004();
		if (isEQ(controlrec.flag,"N")) {
			goTo(GotoLabel.exit1900);
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(linspfRec.chdrcoy);
		chdrlifIO.setChdrnum(linspfRec.chdrnum);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)
		&& isNE(chdrlifIO.getStatuz(),varcom.mrnf)) {
			conerrrec.params.set(chdrlifIO.getParams());
			conerrrec.statuz.set(chdrlifIO.getStatuz());
			databaseError006();
		}
		if (isNE(chdrlifIO.getAcctmeth(),"R")) {
			goTo(GotoLabel.exit1900);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		  //performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(itdmIO.getParams());
			conerrrec.statuz.set(itdmIO.getStatuz());
			databaseError006();
		}
		if (isNE(itdmIO.getItemcoy(),runparmrec.company)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),chdrlifIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(chdrlifIO.getCnttype());
			conerrrec.params.set(itdmIO.getParams());
			conerrrec.statuz.set(e308);
			databaseError006();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		wsaaItemkey.set(SPACES);
		wsaaRunparm1.set(runparmrec.runparm1);
		wsaaItemkey.itemItempfx.set("IT");
		wsaaItemkey.itemItemcoy.set(runparmrec.company);
		wsaaItemkey.itemItemtabl.set(t5679);
		wsaaItemkey.itemItemitem.set(wsaaRunparm1TranCode);
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(itemIO.getParams());
			conerrrec.statuz.set(itemIO.getStatuz());
			databaseError006();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		payrIO.setDataKey(SPACES);
		payrIO.setChdrcoy(linspfRec.chdrcoy);
		payrIO.setChdrnum(linspfRec.chdrnum);
		payrIO.setPayrseqno(linspfRec.payrseqno);
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(payrIO.getParams());
			conerrrec.statuz.set(payrIO.getStatuz());
			databaseError006();
		}
	}

protected void processLinsRecord1200()
	{
		linsrnlIO.setParams(SPACES);
		linsrnlIO.setChdrnum(linspfRec.chdrnum);
		linsrnlIO.setChdrcoy(linspfRec.chdrcoy);
		linsrnlIO.setInstfrom(linspfRec.instfrom);
		linsrnlIO.setFormat(linsrnlrec);
		linsrnlIO.setFunction(varcom.begnh);
	}

protected void readLins1205()
	{
		SmartFileCode.execute(appVars, linsrnlIO);
		if ((isNE(linsrnlIO.getStatuz(),varcom.oK)
		&& isNE(linsrnlIO.getStatuz(),varcom.endp))) {
			conerrrec.params.set(linsrnlIO.getParams());
			conerrrec.statuz.set(linsrnlIO.getStatuz());
			databaseError006();
		}
		if (isEQ(linsrnlIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit1900);
		}
		if (isEQ(linsrnlIO.getDueflg(),"Y")) {
			linsrnlIO.setFunction(varcom.nextr);
			goTo(GotoLabel.readLins1205);
		}
		if (isNE(linsrnlIO.getInstfrom(),linspfRec.instfrom)) {
			linsrnlIO.setFunction(varcom.nextr);
			goTo(GotoLabel.readLins1205);
		}
		if (isNE(linsrnlIO.getChdrnum(),linspfRec.chdrnum)) {
			goTo(GotoLabel.exit1900);
		}
		wsbbEffdate.set(linspfRec.instfrom);
		linsrnlIO.setDueflg("Y");
		linsrnlIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, linsrnlIO);
		if ((isNE(linsrnlIO.getStatuz(),varcom.oK))) {
			conerrrec.params.set(linsrnlIO.getParams());
			conerrrec.statuz.set(linsrnlIO.getStatuz());
			databaseError006();
		}
	}

protected void readChdr1208()
	{
		softlockReadChdrlif2000();
		wsaaValidStatus.set("N");
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
			validateChdrStatus2600();
		}
		if (isEQ(wsaaValidStatus,"N")) {
			rewriteUnlockChdrlif2500();
			goTo(GotoLabel.exit1900);
		}
		writeUnlockChdrlif2400();
	}

protected void ledgerAccountingUpdates1300()
	{
		updatePremiumsDue3000();
		updatePremiumIncome4000();
		covrIO.setDataKey(SPACES);
		covrIO.setChdrnum(linspfRec.chdrnum);
		covrIO.setChdrcoy(linspfRec.chdrcoy);
		covrIO.setPlanSuffix(0);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.begn);
	}

protected void callCovr1400()
	{
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(),varcom.oK))
		&& (isNE(covrIO.getStatuz(),varcom.endp))) {
			conerrrec.params.set(covrIO.getParams());
			conerrrec.statuz.set(covrIO.getStatuz());
			databaseError006();
		}
		if ((isEQ(covrIO.getStatuz(),varcom.endp))) {
			goTo(GotoLabel.housekeeping1800);
		}
		if ((isNE(covrIO.getChdrnum(),linspfRec.chdrnum))
		|| (isNE(covrIO.getChdrcoy(),linspfRec.chdrcoy))) {
			goTo(GotoLabel.housekeeping1800);
		}
		if (isNE(covrIO.getValidflag(),"1")) {
			covrIO.setFunction(varcom.nextr);
			goTo(GotoLabel.callCovr1400);
		}
		if (isNE(t5688rec.comlvlacc,"Y")) {
			goTo(GotoLabel.next1600);
		}
		wsaaValidStatus.set("N");
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
			validateCovrStatus2700();
		}
		if (isEQ(wsaaValidStatus,"N")) {
			goTo(GotoLabel.next1600);
		}
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(linspfRec.chdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(runparmrec.company);
		lifacmvrec.rldgcoy.set(runparmrec.company);
		lifacmvrec.genlcoy.set(runparmrec.company);
		lifacmvrec.batcactyr.set(runparmrec.acctyear);
		lifacmvrec.batctrcde.set(wsaaRunparm1TranCode);
		lifacmvrec.batcactmn.set(runparmrec.acctmonth);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.batcbrn.set(runparmrec.batcbranch);
		lifacmvrec.sacscode.set(t5645rec.sacscode07);
		lifacmvrec.sacstyp.set(t5645rec.sacstype07);
		lifacmvrec.glsign.set(t5645rec.sign07);
		lifacmvrec.glcode.set(t5645rec.glmap07);
		lifacmvrec.contot.set(t5645rec.cnttot07);
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.origamt.set(covrIO.getInstprem());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.effdate.set(wsbbEffdate);
		lifacmvrec.tranref.set(linspfRec.chdrnum);
		lifacmvrec.user.set(runparmrec.user);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.substituteCode[6].set(covrIO.getCrtable());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		wsaaRldgChdrnum.set(covrIO.getChdrnum());
		wsaaRldgLife.set(covrIO.getLife());
		wsaaRldgCoverage.set(covrIO.getCoverage());
		wsaaRldgRider.set(covrIO.getRider());
		wsaaPlan.set(covrIO.getPlanSuffix());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.transactionDate.set(varcom.vrcmDate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.function.set("PSTW");
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			goTo(GotoLabel.next1600);
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			conerrrec.params.set(lifacmvrec.lifacmvRec);
			conerrrec.statuz.set(lifacmvrec.statuz);
			systemError005();
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(lifacmvrec.origamt);
		callContot001();
	}

protected void next1600()
	{
		covrIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callCovr1400);
	}

protected void housekeeping1800()
	{
		ptrnIO.setParams(SPACES);
		ptrnIO.setChdrcoy(linspfRec.chdrcoy);
		ptrnIO.setChdrnum(linspfRec.chdrnum);
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setPtrneff(wsbbEffdate);
		ptrnIO.setUser(runparmrec.user);
		ptrnIO.setBatccoy(runparmrec.company);
		ptrnIO.setBatcbrn(runparmrec.batcbranch);
		ptrnIO.setBatcactyr(runparmrec.acctyear);
		ptrnIO.setBatctrcde(wsaaRunparm1TranCode);
		ptrnIO.setBatcactmn(runparmrec.acctmonth);
		ptrnIO.setBatcbatch(batcdorrec.batch);
		ptrnIO.setDatesub(runparmrec.effdate);
		String userid = ((SMARTAppVars)SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setCrtuser(userid);//PINNACLE-2954
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(ptrnIO.getParams());
			conerrrec.statuz.set(ptrnIO.getStatuz());
			databaseError006();
		}
		batcuprec.batcupRec.set(SPACES);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.batcpfx.set(batcdorrec.prefix);
		batcuprec.batccoy.set(runparmrec.company);
		batcuprec.batcbrn.set(runparmrec.batcbranch);
		batcuprec.batcactyr.set(runparmrec.acctyear);
		batcuprec.batctrcde.set(wsaaRunparm1TranCode);
		batcuprec.batcactmn.set(runparmrec.acctmonth);
		batcuprec.batcbatch.set(batcdorrec.batch);
		batcuprec.function.set("WRITS");
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			conerrrec.params.set(batcuprec.batcupRec);
			conerrrec.statuz.set(batcuprec.statuz);
			systemError005();
		}
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
	}

protected void softlockReadChdrlif2000()
	{
		para2010();
	}

protected void para2010()
	{
		sftlockrec1.function.set("LOCK");
		sftlockrec1.company.set(runparmrec.company);
		sftlockrec1.enttyp.set("CH");
		sftlockrec1.entity.set(linspfRec.chdrnum);
		sftlockrec1.transaction.set(wsaaRunparm1TranCode);
		sftlockrec1.user.set(runparmrec.user);
		callProgram(Sftlock.class, sftlockrec1.sftlockRec);
		if (isNE(sftlockrec1.statuz,varcom.oK)) {
			conerrrec.params.set(sftlockrec1.sftlockRec);
			conerrrec.statuz.set(sftlockrec1.statuz);
			systemError005();
		}
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(linspfRec.chdrcoy);
		chdrlifIO.setChdrnum(linspfRec.chdrnum);
		chdrlifIO.setFunction(varcom.readh);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(chdrlifIO.getParams());
			conerrrec.statuz.set(chdrlifIO.getStatuz());
			databaseError006();
		}
	}

protected void writeUnlockChdrlif2400()
	{
		para2410();
	}

protected void para2410()
	{
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(add(chdrlifIO.getTranno(),1));
		chdrlifIO.setFunction(varcom.rewrt);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(chdrlifIO.getParams());
			conerrrec.statuz.set(chdrlifIO.getStatuz());
			databaseError006();
		}
		sftlockrec1.sftlockRec.set(SPACES);
		sftlockrec1.company.set(runparmrec.company);
		sftlockrec1.entity.set(linspfRec.chdrnum);
		sftlockrec1.enttyp.set("CH");
		sftlockrec1.user.set(runparmrec.user);
		sftlockrec1.transaction.set(wsaaRunparm1TranCode);
		sftlockrec1.statuz.set(SPACES);
		sftlockrec1.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec1.sftlockRec);
		if (isNE(sftlockrec1.statuz,varcom.oK)) {
			conerrrec.params.set(sftlockrec1.sftlockRec);
			conerrrec.statuz.set(sftlockrec1.statuz);
			systemError005();
		}
	}

protected void rewriteUnlockChdrlif2500()
	{
		para2510();
	}

protected void para2510()
	{
		chdrlifIO.setFunction(varcom.rewrt);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(chdrlifIO.getParams());
			conerrrec.statuz.set(chdrlifIO.getStatuz());
			databaseError006();
		}
		sftlockrec1.sftlockRec.set(SPACES);
		sftlockrec1.company.set(runparmrec.company);
		sftlockrec1.entity.set(linspfRec.chdrnum);
		sftlockrec1.enttyp.set("CH");
		sftlockrec1.user.set(runparmrec.user);
		sftlockrec1.transaction.set(wsaaRunparm1TranCode);
		sftlockrec1.statuz.set(SPACES);
		sftlockrec1.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec1.sftlockRec);
		if (isNE(sftlockrec1.statuz,varcom.oK)) {
			conerrrec.params.set(sftlockrec1.sftlockRec);
			conerrrec.statuz.set(sftlockrec1.statuz);
			systemError005();
		}
	}

protected void validateChdrStatus2600()
	{
		/*PARA*/
		if (isEQ(t5679rec.cnRiskStat[wsaaIndex.toInt()],chdrlifIO.getStatcode())) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
				validateChdrPremStatus2800();
			}
		}
		/*EXIT*/
	}

protected void validateCovrStatus2700()
	{
		/*PARA*/
		if (isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()],covrIO.getStatcode())) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
				validateCovrPremStatus2900();
			}
		}
		/*EXIT*/
	}

protected void validateChdrPremStatus2800()
	{
		/*PARA*/
		if (isEQ(t5679rec.cnPremStat[wsaaIndex.toInt()],chdrlifIO.getPstatcode())) {
			wsaaIndex.set(13);
			wsaaValidStatus.set("Y");
		}
		/*EXIT*/
	}

protected void validateCovrPremStatus2900()
	{
		/*PARA*/
		if (isEQ(t5679rec.covPremStat[wsaaIndex.toInt()],covrIO.getPstatcode())) {
			wsaaIndex.set(13);
			wsaaValidStatus.set("Y");
		}
		/*EXIT*/
	}

protected void updatePremiumsDue3000()
	{
		try {
			premDue3100();
		}
		catch (GOTOException e){
		}
	}

protected void premDue3100()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(linspfRec.chdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(runparmrec.company);
		lifacmvrec.rldgcoy.set(runparmrec.company);
		lifacmvrec.genlcoy.set(runparmrec.company);
		lifacmvrec.batcactyr.set(runparmrec.acctyear);
		lifacmvrec.batctrcde.set(wsaaRunparm1TranCode);
		lifacmvrec.batcactmn.set(runparmrec.acctmonth);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.batcbrn.set(runparmrec.batcbranch);
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.origamt.set(linspfRec.instamt06);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.effdate.set(wsbbEffdate);
		lifacmvrec.tranref.set(linspfRec.chdrnum);
		lifacmvrec.user.set(runparmrec.user);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(linspfRec.chdrnum);
		lifacmvrec.transactionDate.set(varcom.vrcmDate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.function.set("PSTW");
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			goTo(GotoLabel.exit3990);
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			conerrrec.params.set(lifacmvrec.lifacmvRec);
			conerrrec.statuz.set(lifacmvrec.statuz);
			systemError005();
		}
		contotrec.totno.set(ct04);
		contotrec.totval.set(linspfRec.instamt06);
		callContot001();
	}

protected void updatePremiumIncome4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					updatePremiumIncomePara4000();
				}
				case fees4200: {
					fees4200();
				}
				case tolerance4300: {
					tolerance4300();
				}
				case stampDuty4400: {
					stampDuty4400();
				}
				case wvrHolding4500: {
					wvrHolding4500();
				}
				case exit4900: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updatePremiumIncomePara4000()
	{
		if (isLTE(linspfRec.instamt01,0)) {
			goTo(GotoLabel.fees4200);
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			goTo(GotoLabel.fees4200);
		}
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(linspfRec.chdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(runparmrec.company);
		lifacmvrec.rldgcoy.set(runparmrec.company);
		lifacmvrec.genlcoy.set(runparmrec.company);
		lifacmvrec.batcactyr.set(runparmrec.acctyear);
		lifacmvrec.batctrcde.set(wsaaRunparm1TranCode);
		lifacmvrec.batcactmn.set(runparmrec.acctmonth);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.batcbrn.set(runparmrec.batcbranch);
		lifacmvrec.sacscode.set(t5645rec.sacscode02);
		lifacmvrec.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec.glsign.set(t5645rec.sign02);
		lifacmvrec.glcode.set(t5645rec.glmap02);
		lifacmvrec.contot.set(t5645rec.cnttot02);
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.origamt.set(linspfRec.instamt01);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.effdate.set(wsbbEffdate);
		lifacmvrec.tranref.set(linspfRec.chdrnum);
		lifacmvrec.user.set(runparmrec.user);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(linspfRec.chdrnum);
		lifacmvrec.transactionDate.set(varcom.vrcmDate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			conerrrec.params.set(lifacmvrec.lifacmvRec);
			conerrrec.statuz.set(lifacmvrec.statuz);
			systemError005();
		}
		contotrec.totno.set(ct05);
		contotrec.totval.set(linspfRec.instamt01);
		callContot001();
	}

protected void fees4200()
	{
		if (isLTE(linsrnlIO.getInstamt02(),0)) {
			goTo(GotoLabel.tolerance4300);
		}
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(linspfRec.chdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(runparmrec.company);
		lifacmvrec.rldgcoy.set(runparmrec.company);
		lifacmvrec.genlcoy.set(runparmrec.company);
		lifacmvrec.batcactyr.set(runparmrec.acctyear);
		lifacmvrec.batctrcde.set(wsaaRunparm1TranCode);
		lifacmvrec.batcactmn.set(runparmrec.acctmonth);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.batcbrn.set(runparmrec.batcbranch);
		lifacmvrec.sacscode.set(t5645rec.sacscode03);
		lifacmvrec.sacstyp.set(t5645rec.sacstype03);
		lifacmvrec.glsign.set(t5645rec.sign03);
		lifacmvrec.glcode.set(t5645rec.glmap03);
		lifacmvrec.contot.set(t5645rec.cnttot03);
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.origamt.set(linsrnlIO.getInstamt02());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.effdate.set(wsbbEffdate);
		lifacmvrec.tranref.set(linspfRec.chdrnum);
		lifacmvrec.user.set(runparmrec.user);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(linspfRec.chdrnum);
		lifacmvrec.transactionDate.set(varcom.vrcmDate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			conerrrec.params.set(lifacmvrec.lifacmvRec);
			conerrrec.statuz.set(lifacmvrec.statuz);
			systemError005();
		}
		contotrec.totno.set(ct06);
		contotrec.totval.set(linsrnlIO.getInstamt02());
		callContot001();
	}

protected void tolerance4300()
	{
		if (isLTE(linsrnlIO.getInstamt03(),0)) {
			goTo(GotoLabel.stampDuty4400);
		}
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(linspfRec.chdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(runparmrec.company);
		lifacmvrec.rldgcoy.set(runparmrec.company);
		lifacmvrec.genlcoy.set(runparmrec.company);
		lifacmvrec.batcactyr.set(runparmrec.acctyear);
		lifacmvrec.batctrcde.set(wsaaRunparm1TranCode);
		lifacmvrec.batcactmn.set(runparmrec.acctmonth);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.batcbrn.set(runparmrec.batcbranch);
		lifacmvrec.sacscode.set(t5645rec.sacscode04);
		lifacmvrec.sacstyp.set(t5645rec.sacstype04);
		lifacmvrec.glsign.set(t5645rec.sign04);
		lifacmvrec.glcode.set(t5645rec.glmap04);
		lifacmvrec.contot.set(t5645rec.cnttot04);
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.origamt.set(linsrnlIO.getInstamt03());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.effdate.set(wsbbEffdate);
		lifacmvrec.tranref.set(linspfRec.chdrnum);
		lifacmvrec.user.set(runparmrec.user);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(linspfRec.chdrnum);
		lifacmvrec.transactionDate.set(varcom.vrcmDate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			conerrrec.params.set(lifacmvrec.lifacmvRec);
			conerrrec.statuz.set(lifacmvrec.statuz);
			systemError005();
		}
	}

protected void stampDuty4400()
	{
		if (isLTE(linsrnlIO.getInstamt04(),0)) {
			goTo(GotoLabel.wvrHolding4500);
		}
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(linspfRec.chdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(runparmrec.company);
		lifacmvrec.rldgcoy.set(runparmrec.company);
		lifacmvrec.genlcoy.set(runparmrec.company);
		lifacmvrec.batcactyr.set(runparmrec.acctyear);
		lifacmvrec.batctrcde.set(wsaaRunparm1TranCode);
		lifacmvrec.batcactmn.set(runparmrec.acctmonth);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.batcbrn.set(runparmrec.batcbranch);
		lifacmvrec.sacscode.set(t5645rec.sacscode05);
		lifacmvrec.sacstyp.set(t5645rec.sacstype05);
		lifacmvrec.glsign.set(t5645rec.sign05);
		lifacmvrec.glcode.set(t5645rec.glmap05);
		lifacmvrec.contot.set(t5645rec.cnttot05);
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.origamt.set(linsrnlIO.getInstamt04());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.effdate.set(wsbbEffdate);
		lifacmvrec.tranref.set(linspfRec.chdrnum);
		lifacmvrec.user.set(runparmrec.user);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(linspfRec.chdrnum);
		lifacmvrec.transactionDate.set(varcom.vrcmDate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			conerrrec.params.set(lifacmvrec.lifacmvRec);
			conerrrec.statuz.set(lifacmvrec.statuz);
			systemError005();
		}
	}

protected void wvrHolding4500()
	{
		if (isEQ(linsrnlIO.getInstamt05(),0)) {
			goTo(GotoLabel.exit4900);
		}
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(linspfRec.chdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(runparmrec.company);
		lifacmvrec.rldgcoy.set(runparmrec.company);
		lifacmvrec.genlcoy.set(runparmrec.company);
		lifacmvrec.batcactyr.set(runparmrec.acctyear);
		lifacmvrec.batctrcde.set(wsaaRunparm1TranCode);
		lifacmvrec.batcactmn.set(runparmrec.acctmonth);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.batcbrn.set(runparmrec.batcbranch);
		lifacmvrec.sacscode.set(t5645rec.sacscode06);
		lifacmvrec.sacstyp.set(t5645rec.sacstype06);
		lifacmvrec.glsign.set(t5645rec.sign06);
		lifacmvrec.glcode.set(t5645rec.glmap06);
		lifacmvrec.contot.set(t5645rec.cnttot06);
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.origamt.set(linsrnlIO.getInstamt05());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.effdate.set(wsbbEffdate);
		lifacmvrec.tranref.set(linspfRec.chdrnum);
		lifacmvrec.user.set(runparmrec.user);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(linspfRec.chdrnum);
		lifacmvrec.transactionDate.set(varcom.vrcmDate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			conerrrec.params.set(lifacmvrec.lifacmvRec);
			conerrrec.statuz.set(lifacmvrec.statuz);
			systemError005();
		}
		contotrec.totno.set(ct07);
		contotrec.totval.set(linsrnlIO.getInstamt05());
		callContot001();
	}
}
