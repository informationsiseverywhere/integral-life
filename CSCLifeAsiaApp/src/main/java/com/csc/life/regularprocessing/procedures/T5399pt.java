/*
 * File: T5399pt.java
 * Date: 30 August 2009 2:20:39
 * Author: Quipoz Limited
 * 
 * Class transformed from T5399PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.regularprocessing.tablestructures.T5399rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5399.
*
*
*****************************************************************
* </pre>
*/
public class T5399pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(51).isAPartOf(wsaaPrtLine001, 25, FILLER).init("Set Contract Risk/Premium Statuses            S5399");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(61);
	private FixedLengthStringData filler7 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(55).isAPartOf(wsaaPrtLine003, 6, FILLER).init("Coverage      Set                     Coverage      Set");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(66);
	private FixedLengthStringData filler9 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(60).isAPartOf(wsaaPrtLine004, 6, FILLER).init("Risk          Contract                Premium       Contract");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(65);
	private FixedLengthStringData filler11 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(59).isAPartOf(wsaaPrtLine005, 6, FILLER).init("Status        Risk                    Status        Premium");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(64);
	private FixedLengthStringData filler13 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(58).isAPartOf(wsaaPrtLine006, 6, FILLER).init("Hierarchy     Status                  Hierarchy     Status");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(62);
	private FixedLengthStringData filler15 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 8);
	private FixedLengthStringData filler16 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 22);
	private FixedLengthStringData filler17 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine007, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 46);
	private FixedLengthStringData filler18 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 60);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(62);
	private FixedLengthStringData filler19 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 8);
	private FixedLengthStringData filler20 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine008, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 22);
	private FixedLengthStringData filler21 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine008, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 46);
	private FixedLengthStringData filler22 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine008, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 60);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(62);
	private FixedLengthStringData filler23 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 8);
	private FixedLengthStringData filler24 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 22);
	private FixedLengthStringData filler25 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine009, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 46);
	private FixedLengthStringData filler26 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 60);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(62);
	private FixedLengthStringData filler27 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 8);
	private FixedLengthStringData filler28 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine010, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 22);
	private FixedLengthStringData filler29 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine010, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 46);
	private FixedLengthStringData filler30 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine010, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 60);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(62);
	private FixedLengthStringData filler31 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 8);
	private FixedLengthStringData filler32 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine011, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 22);
	private FixedLengthStringData filler33 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine011, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 46);
	private FixedLengthStringData filler34 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine011, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 60);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(62);
	private FixedLengthStringData filler35 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 8);
	private FixedLengthStringData filler36 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine012, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 22);
	private FixedLengthStringData filler37 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine012, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 46);
	private FixedLengthStringData filler38 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine012, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 60);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(62);
	private FixedLengthStringData filler39 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 8);
	private FixedLengthStringData filler40 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine013, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 22);
	private FixedLengthStringData filler41 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine013, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 46);
	private FixedLengthStringData filler42 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine013, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 60);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(62);
	private FixedLengthStringData filler43 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 8);
	private FixedLengthStringData filler44 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine014, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 22);
	private FixedLengthStringData filler45 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine014, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 46);
	private FixedLengthStringData filler46 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine014, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 60);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(62);
	private FixedLengthStringData filler47 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 8);
	private FixedLengthStringData filler48 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine015, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 22);
	private FixedLengthStringData filler49 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine015, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 46);
	private FixedLengthStringData filler50 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine015, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 60);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(62);
	private FixedLengthStringData filler51 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 8);
	private FixedLengthStringData filler52 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine016, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 22);
	private FixedLengthStringData filler53 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine016, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 46);
	private FixedLengthStringData filler54 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine016, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 60);

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(62);
	private FixedLengthStringData filler55 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 8);
	private FixedLengthStringData filler56 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine017, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 22);
	private FixedLengthStringData filler57 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine017, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 46);
	private FixedLengthStringData filler58 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine017, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 60);

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(62);
	private FixedLengthStringData filler59 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine018, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine018, 8);
	private FixedLengthStringData filler60 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine018, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine018, 22);
	private FixedLengthStringData filler61 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine018, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine018, 46);
	private FixedLengthStringData filler62 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine018, 48, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine018, 60);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5399rec t5399rec = new T5399rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5399pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5399rec.t5399Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(t5399rec.covRiskStat01);
		fieldNo009.set(t5399rec.covRiskStat02);
		fieldNo013.set(t5399rec.covRiskStat03);
		fieldNo017.set(t5399rec.covRiskStat04);
		fieldNo021.set(t5399rec.covRiskStat05);
		fieldNo025.set(t5399rec.covRiskStat06);
		fieldNo029.set(t5399rec.covRiskStat07);
		fieldNo033.set(t5399rec.covRiskStat08);
		fieldNo037.set(t5399rec.covRiskStat09);
		fieldNo041.set(t5399rec.covRiskStat10);
		fieldNo045.set(t5399rec.covRiskStat11);
		fieldNo049.set(t5399rec.covRiskStat12);
		fieldNo006.set(t5399rec.setCnRiskStat01);
		fieldNo010.set(t5399rec.setCnRiskStat02);
		fieldNo014.set(t5399rec.setCnRiskStat03);
		fieldNo018.set(t5399rec.setCnRiskStat04);
		fieldNo022.set(t5399rec.setCnRiskStat05);
		fieldNo026.set(t5399rec.setCnRiskStat06);
		fieldNo030.set(t5399rec.setCnRiskStat07);
		fieldNo034.set(t5399rec.setCnRiskStat08);
		fieldNo038.set(t5399rec.setCnRiskStat09);
		fieldNo042.set(t5399rec.setCnRiskStat10);
		fieldNo046.set(t5399rec.setCnRiskStat11);
		fieldNo050.set(t5399rec.setCnRiskStat12);
		fieldNo007.set(t5399rec.covPremStat01);
		fieldNo008.set(t5399rec.setCnPremStat01);
		fieldNo011.set(t5399rec.covPremStat02);
		fieldNo012.set(t5399rec.setCnPremStat02);
		fieldNo015.set(t5399rec.covPremStat03);
		fieldNo016.set(t5399rec.setCnPremStat03);
		fieldNo019.set(t5399rec.covPremStat04);
		fieldNo020.set(t5399rec.setCnPremStat04);
		fieldNo023.set(t5399rec.covPremStat05);
		fieldNo024.set(t5399rec.setCnPremStat05);
		fieldNo027.set(t5399rec.covPremStat06);
		fieldNo028.set(t5399rec.setCnPremStat06);
		fieldNo031.set(t5399rec.covPremStat07);
		fieldNo032.set(t5399rec.setCnPremStat07);
		fieldNo035.set(t5399rec.covPremStat08);
		fieldNo036.set(t5399rec.setCnPremStat08);
		fieldNo039.set(t5399rec.covPremStat09);
		fieldNo040.set(t5399rec.setCnPremStat09);
		fieldNo043.set(t5399rec.covPremStat10);
		fieldNo044.set(t5399rec.setCnPremStat10);
		fieldNo047.set(t5399rec.covPremStat11);
		fieldNo048.set(t5399rec.setCnPremStat11);
		fieldNo051.set(t5399rec.covPremStat12);
		fieldNo052.set(t5399rec.setCnPremStat12);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
