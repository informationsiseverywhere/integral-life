package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:11
 * Description:
 * Copybook name: LINSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Linskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData linsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData linsKey = new FixedLengthStringData(64).isAPartOf(linsFileKey, 0, REDEFINE);
  	public FixedLengthStringData linsChdrcoy = new FixedLengthStringData(1).isAPartOf(linsKey, 0);
  	public FixedLengthStringData linsChdrnum = new FixedLengthStringData(8).isAPartOf(linsKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(linsKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(linsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		linsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}