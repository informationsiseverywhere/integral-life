package com.csc.life.regularprocessing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 
 * @author: Quipoz Limited
 *          PD5ENPAR
 *
 *          Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Fmcvpmsrec extends ExternalData {

	// *******************************
	// Attribute Declarations
	// *******************************

	public FixedLengthStringData statuz = new FixedLengthStringData(4);
  	public FixedLengthStringData product = new FixedLengthStringData(4);
  	public FixedLengthStringData lifeLife = new FixedLengthStringData(2);
  	public FixedLengthStringData lifeJlife = new FixedLengthStringData(2);
  	public FixedLengthStringData covrCoverage = new FixedLengthStringData(4);
  	public FixedLengthStringData covrRider = new FixedLengthStringData(2);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0);
  	public PackedDecimalData effectdt = new PackedDecimalData(8, 0);
  	public PackedDecimalData ratedt = new PackedDecimalData(8, 0);
  	public FixedLengthStringData fmcapplicable = new FixedLengthStringData(3);
  	public FixedLengthStringData ridderapplicable = new FixedLengthStringData(1);
	public FixedLengthStringData outRate = new FixedLengthStringData(20);
	public FixedLengthStringData frequnce = new FixedLengthStringData(4);
	public PackedDecimalData mfactor = new PackedDecimalData(10, 5);
	public PackedDecimalData nfoperiod = new PackedDecimalData(4, 0);
	public FixedLengthStringData avapin = new FixedLengthStringData(4);
	public FixedLengthStringData avapcomp = new FixedLengthStringData(4);
	public FixedLengthStringData outputRiderPost = new FixedLengthStringData(1);
	public FixedLengthStringData fund = new FixedLengthStringData(4);

	public void initialize() {
		COBOLFunctions.initialize(statuz);
	}

	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());			
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}

}