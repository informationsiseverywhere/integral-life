package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:20
 * Description:
 * Copybook name: COVRBBLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrbblkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrbblFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrbblKey = new FixedLengthStringData(64).isAPartOf(covrbblFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrbblChdrcoy = new FixedLengthStringData(1).isAPartOf(covrbblKey, 0);
  	public FixedLengthStringData covrbblChdrnum = new FixedLengthStringData(8).isAPartOf(covrbblKey, 1);
  	public PackedDecimalData covrbblBenBillDate = new PackedDecimalData(8, 0).isAPartOf(covrbblKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(covrbblKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrbblFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrbblFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}