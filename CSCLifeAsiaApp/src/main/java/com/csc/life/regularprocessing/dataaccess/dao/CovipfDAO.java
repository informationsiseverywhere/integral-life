package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.Covipf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface CovipfDAO extends BaseDAO<Covipf> {
    public List<Covipf> searchCoviRecord(String tableId, String memName, int batchExtractSize, int batchID);
}