/*
 * File: Br623.java
 * Date: 29 August 2009 22:29:02
 * Author: Quipoz Limited
 * 
 * Class transformed from BR623.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.life.statistics.dataaccess.AgprpfTableDAM;
import com.csc.life.statistics.dataaccess.AgpxpfTableDAM;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*                    AGPRPF SPLITTER PROGRAM
*                   -------------------------
*
*
* This program is the Splitter program which will split the AGPR
* file containing all pending updates for any deferred Agent
* production.
*
* All records from AGPRPF will be read via SQL and ordered
* by AGNTCOY, AGNTNUM, ACCTYR, MNTH, CHDRNUM, and EFFDATE
*
* Control totals used in this program:
*
*    01  -  No. of AGPR extracted records.
*    02  -  No. of thread members.
*
* The temporary file for this splitter program is AGPXPF with
* many AGPX members. Extracted records will be written to as many
* output members as threads specified in the 'No. of subsequent
* threads' parameter on the process definition for this program.
* Each AGPR record is written to a different AGPX member, unless
* there are several AGPR records for the same agent, in which
* case, these are written to the same AGPX member.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Br623 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlagprCursorrs = null;
	private java.sql.PreparedStatement sqlagprCursorps = null;
	private java.sql.Connection sqlagprCursorconn = null;
	private String sqlagprCursor = "";
	private int agprCursorLoopIndex = 0;
	private AgpxpfTableDAM agpxpf = new AgpxpfTableDAM();
	private DiskFileDAM agpx01 = new DiskFileDAM("AGPX01");
	private DiskFileDAM agpx02 = new DiskFileDAM("AGPX02");
	private DiskFileDAM agpx03 = new DiskFileDAM("AGPX03");
	private DiskFileDAM agpx04 = new DiskFileDAM("AGPX04");
	private DiskFileDAM agpx05 = new DiskFileDAM("AGPX05");
	private DiskFileDAM agpx06 = new DiskFileDAM("AGPX06");
	private DiskFileDAM agpx07 = new DiskFileDAM("AGPX07");
	private DiskFileDAM agpx08 = new DiskFileDAM("AGPX08");
	private DiskFileDAM agpx09 = new DiskFileDAM("AGPX09");
	private DiskFileDAM agpx10 = new DiskFileDAM("AGPX10");
	private DiskFileDAM agpx11 = new DiskFileDAM("AGPX11");
	private DiskFileDAM agpx12 = new DiskFileDAM("AGPX12");
	private DiskFileDAM agpx13 = new DiskFileDAM("AGPX13");
	private DiskFileDAM agpx14 = new DiskFileDAM("AGPX14");
	private DiskFileDAM agpx15 = new DiskFileDAM("AGPX15");
	private DiskFileDAM agpx16 = new DiskFileDAM("AGPX16");
	private DiskFileDAM agpx17 = new DiskFileDAM("AGPX17");
	private DiskFileDAM agpx18 = new DiskFileDAM("AGPX18");
	private DiskFileDAM agpx19 = new DiskFileDAM("AGPX19");
	private DiskFileDAM agpx20 = new DiskFileDAM("AGPX20");
	private FixedLengthStringData agpx01Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx02Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx03Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx04Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx05Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx06Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx07Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx08Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx09Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx10Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx11Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx12Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx13Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx14Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx15Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx16Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx17Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx18Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx19Rec = new FixedLengthStringData(585);
	private FixedLengthStringData agpx20Rec = new FixedLengthStringData(585);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR623");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaClrtmpfError = new FixedLengthStringData(97);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/* ERRORS */
	private String ivrm = "IVRM";
	private String esql = "ESQL";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

	private FixedLengthStringData wsaaAgpxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(wsaaAgpxFn, 0, FILLER).init("AGPX");
	private FixedLengthStringData wsaaAgpxRunid = new FixedLengthStringData(2).isAPartOf(wsaaAgpxFn, 4);
	private ZonedDecimalData wsaaAgpxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaAgpxFn, 6).setUnsigned();
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaIndArray = new FixedLengthStringData(12000);
	private FixedLengthStringData[] wsaaAgprInd = FLSArrayPartOfStructure(1000, 12, wsaaIndArray, 0);
	private BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(6, 4, 0, wsaaAgprInd, 0);
		/* WSAA-HOST-VARIABLES */
	private FixedLengthStringData wsaaPrevAgntcoy = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaPrevAgntnum = new FixedLengthStringData(8).init(SPACES);

	private FixedLengthStringData wsaaFetchArray = new FixedLengthStringData(583000);
	private FixedLengthStringData[] wsaaAgprData = FLSArrayPartOfStructure(1000, 583, wsaaFetchArray, 0);
	private FixedLengthStringData[] wsaaAgntcoy = FLSDArrayPartOfArrayStructure(1, wsaaAgprData, 0);
	private FixedLengthStringData[] wsaaAgntnum = FLSDArrayPartOfArrayStructure(8, wsaaAgprData, 1);
	private PackedDecimalData[] wsaaEffdate = PDArrayPartOfArrayStructure(8, 0, wsaaAgprData, 9);
	private PackedDecimalData[] wsaaAcctyr = PDArrayPartOfArrayStructure(4, 0, wsaaAgprData, 14);
	private PackedDecimalData[] wsaaMnth = PDArrayPartOfArrayStructure(2, 0, wsaaAgprData, 17);
	private FixedLengthStringData[] wsaaCnttype = FLSDArrayPartOfArrayStructure(3, wsaaAgprData, 19);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaAgprData, 22);
	private PackedDecimalData[] wsaaMlperpp01 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 30);
	private PackedDecimalData[] wsaaMlperpp02 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 39);
	private PackedDecimalData[] wsaaMlperpp03 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 48);
	private PackedDecimalData[] wsaaMlperpp04 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 57);
	private PackedDecimalData[] wsaaMlperpp05 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 66);
	private PackedDecimalData[] wsaaMlperpp06 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 75);
	private PackedDecimalData[] wsaaMlperpp07 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 84);
	private PackedDecimalData[] wsaaMlperpp08 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 93);
	private PackedDecimalData[] wsaaMlperpp09 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 102);
	private PackedDecimalData[] wsaaMlperpp10 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 111);
	private PackedDecimalData[] wsaaMlperpc01 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 120);
	private PackedDecimalData[] wsaaMlperpc02 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 129);
	private PackedDecimalData[] wsaaMlperpc03 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 138);
	private PackedDecimalData[] wsaaMlperpc04 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 147);
	private PackedDecimalData[] wsaaMlperpc05 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 156);
	private PackedDecimalData[] wsaaMlperpc06 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 165);
	private PackedDecimalData[] wsaaMlperpc07 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 174);
	private PackedDecimalData[] wsaaMlperpc08 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 183);
	private PackedDecimalData[] wsaaMlperpc09 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 192);
	private PackedDecimalData[] wsaaMlperpc10 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 201);
	private PackedDecimalData[] wsaaMldirpp01 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 210);
	private PackedDecimalData[] wsaaMldirpp02 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 219);
	private PackedDecimalData[] wsaaMldirpp03 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 228);
	private PackedDecimalData[] wsaaMldirpp04 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 237);
	private PackedDecimalData[] wsaaMldirpp05 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 246);
	private PackedDecimalData[] wsaaMldirpp06 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 255);
	private PackedDecimalData[] wsaaMldirpp07 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 264);
	private PackedDecimalData[] wsaaMldirpp08 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 273);
	private PackedDecimalData[] wsaaMldirpp09 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 282);
	private PackedDecimalData[] wsaaMldirpp10 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 291);
	private PackedDecimalData[] wsaaMldirpc01 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 300);
	private PackedDecimalData[] wsaaMldirpc02 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 309);
	private PackedDecimalData[] wsaaMldirpc03 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 318);
	private PackedDecimalData[] wsaaMldirpc04 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 327);
	private PackedDecimalData[] wsaaMldirpc05 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 336);
	private PackedDecimalData[] wsaaMldirpc06 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 345);
	private PackedDecimalData[] wsaaMldirpc07 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 354);
	private PackedDecimalData[] wsaaMldirpc08 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 363);
	private PackedDecimalData[] wsaaMldirpc09 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 372);
	private PackedDecimalData[] wsaaMldirpc10 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 381);
	private PackedDecimalData[] wsaaMlgrppp01 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 390);
	private PackedDecimalData[] wsaaMlgrppp02 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 399);
	private PackedDecimalData[] wsaaMlgrppp03 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 408);
	private PackedDecimalData[] wsaaMlgrppp04 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 417);
	private PackedDecimalData[] wsaaMlgrppp05 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 426);
	private PackedDecimalData[] wsaaMlgrppp06 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 435);
	private PackedDecimalData[] wsaaMlgrppp07 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 444);
	private PackedDecimalData[] wsaaMlgrppp08 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 453);
	private PackedDecimalData[] wsaaMlgrppp09 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 462);
	private PackedDecimalData[] wsaaMlgrppp10 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 471);
	private PackedDecimalData[] wsaaMlgrppc01 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 480);
	private PackedDecimalData[] wsaaMlgrppc02 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 489);
	private PackedDecimalData[] wsaaMlgrppc03 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 498);
	private PackedDecimalData[] wsaaMlgrppc04 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 507);
	private PackedDecimalData[] wsaaMlgrppc05 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 516);
	private PackedDecimalData[] wsaaMlgrppc06 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 525);
	private PackedDecimalData[] wsaaMlgrppc07 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 534);
	private PackedDecimalData[] wsaaMlgrppc08 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 543);
	private PackedDecimalData[] wsaaMlgrppc09 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 552);
	private PackedDecimalData[] wsaaMlgrppc10 = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 561);
	private PackedDecimalData[] wsaaCntcount = PDArrayPartOfArrayStructure(6, 0, wsaaAgprData, 570);
	private PackedDecimalData[] wsaaSumins = PDArrayPartOfArrayStructure(17, 2, wsaaAgprData, 574);
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
		/*DEFERRED AGENT PRODUCTION DETAILS*/
	private AgprpfTableDAM agprpfData = new AgprpfTableDAM();
		/*TEMP FILE FOR AGT PRDT DEFERRED UPDATE*/
	private AgpxpfTableDAM agpxpfData = new AgpxpfTableDAM();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090
	}

	public Br623() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		wsaaAgpxRunid.set(bprdIO.getSystemParam04());
		wsaaAgpxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		iy.set(1);
		sqlagprCursor = " SELECT  AGNTCOY, AGNTNUM, EFFDATE, ACCTYR, MNTH, CNTTYPE, CHDRNUM, MLPERPP01, MLPERPP02, MLPERPP03, MLPERPP04, MLPERPP05, MLPERPP06, MLPERPP07, MLPERPP08, MLPERPP09, MLPERPP10, MLPERPC01, MLPERPC02, MLPERPC03, MLPERPC04, MLPERPC05, MLPERPC06, MLPERPC07, MLPERPC08, MLPERPC09, MLPERPC10, MLDIRPP01, MLDIRPP02, MLDIRPP03, MLDIRPP04, MLDIRPP05, MLDIRPP06, MLDIRPP07, MLDIRPP08, MLDIRPP09, MLDIRPP10, MLDIRPC01, MLDIRPC02, MLDIRPC03, MLDIRPC04, MLDIRPC05, MLDIRPC06, MLDIRPC07, MLDIRPC08, MLDIRPC09, MLDIRPC10, MLGRPPP01, MLGRPPP02, MLGRPPP03, MLGRPPP04, MLGRPPP05, MLGRPPP06, MLGRPPP07, MLGRPPP08, MLGRPPP09, MLGRPPP10, MLGRPPC01, MLGRPPC02, MLGRPPC03, MLGRPPC04, MLGRPPC05, MLGRPPC06, MLGRPPC07, MLGRPPC08, MLGRPPC09, MLGRPPC10, CNTCOUNT, SUMINS" +
" FROM   " + appVars.getTableNameOverriden("AGPRPF") + " " +
" ORDER BY AGNTCOY, AGNTNUM, EFFDATE, ACCTYR, MNTH, CNTTYPE";
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		sqlerrorflag = false;
		try {
			sqlagprCursorconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.AgprpfTableDAM());
			sqlagprCursorps = appVars.prepareStatementEmbeded(sqlagprCursorconn, sqlagprCursor, "AGPRPF");
			sqlagprCursorrs = appVars.executeQuery(sqlagprCursorps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

protected void openThreadMember1110()
	{
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(),1),iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaAgpxFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz,varcom.oK)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append("CLRTMPF");
			stringVariable1.append(delimitedExp(bprdIO.getRunLibrary(), SPACES));
			stringVariable1.append(SPACES);
			stringVariable1.append(wsaaAgpxFn.toString());
			stringVariable1.append(SPACES);
			stringVariable1.append(wsaaThreadMember.toString());
			wsaaClrtmpfError.setLeft(stringVariable1.toString());
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.params.set(wsaaClrtmpfError);
			fatalError600();
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append("OVRDBF FILE(AGPX");
		stringVariable2.append(iz.toString());
		stringVariable2.append(") TOFILE(");
		stringVariable2.append(delimitedExp(bprdIO.getRunLibrary(), SPACES));
		stringVariable2.append("/");
		stringVariable2.append(wsaaAgpxFn.toString());
		stringVariable2.append(") MBR(");
		stringVariable2.append(wsaaThreadMember.toString());
		stringVariable2.append(")");
		stringVariable2.append(" SEQONLY(*YES 1000)");
		wsaaQcmdexc.setLeft(stringVariable2.toString());
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		if (isEQ(iz,1)) {
			agpx01.openOutput();
		}
		if (isEQ(iz,2)) {
			agpx02.openOutput();
		}
		if (isEQ(iz,3)) {
			agpx03.openOutput();
		}
		if (isEQ(iz,4)) {
			agpx04.openOutput();
		}
		if (isEQ(iz,5)) {
			agpx05.openOutput();
		}
		if (isEQ(iz,6)) {
			agpx06.openOutput();
		}
		if (isEQ(iz,7)) {
			agpx07.openOutput();
		}
		if (isEQ(iz,8)) {
			agpx08.openOutput();
		}
		if (isEQ(iz,9)) {
			agpx09.openOutput();
		}
		if (isEQ(iz,10)) {
			agpx10.openOutput();
		}
		if (isEQ(iz,11)) {
			agpx11.openOutput();
		}
		if (isEQ(iz,12)) {
			agpx12.openOutput();
		}
		if (isEQ(iz,13)) {
			agpx13.openOutput();
		}
		if (isEQ(iz,14)) {
			agpx14.openOutput();
		}
		if (isEQ(iz,15)) {
			agpx15.openOutput();
		}
		if (isEQ(iz,16)) {
			agpx16.openOutput();
		}
		if (isEQ(iz,17)) {
			agpx17.openOutput();
		}
		if (isEQ(iz,18)) {
			agpx18.openOutput();
		}
		if (isEQ(iz,19)) {
			agpx19.openOutput();
		}
		if (isEQ(iz,20)) {
			agpx20.openOutput();
		}
	}

protected void initialiseArray1500()
	{
		/*START*/
		wsaaAcctyr[wsaaInd.toInt()].set(ZERO);
		wsaaMnth[wsaaInd.toInt()].set(ZERO);
		wsaaEffdate[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpp01[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpp02[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpp03[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpp04[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpp05[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpp06[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpp07[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpp08[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpp09[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpp10[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpc01[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpc02[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpc03[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpc04[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpc05[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpc06[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpc07[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpc08[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpc09[wsaaInd.toInt()].set(ZERO);
		wsaaMlperpc10[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpp01[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpp02[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpp03[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpp04[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpp05[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpp06[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpp07[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpp08[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpp09[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpp10[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpc01[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpc02[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpc03[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpc04[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpc05[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpc06[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpc07[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpc08[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpc09[wsaaInd.toInt()].set(ZERO);
		wsaaMldirpc10[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppp01[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppp02[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppp03[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppp04[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppp05[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppp06[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppp07[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppp08[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppp09[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppp10[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppc01[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppc02[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppc03[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppc04[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppc05[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppc06[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppc07[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppc08[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppc09[wsaaInd.toInt()].set(ZERO);
		wsaaMlgrppc10[wsaaInd.toInt()].set(ZERO);
		wsaaCntcount[wsaaInd.toInt()].set(ZERO);
		wsaaSumins[wsaaInd.toInt()].set(ZERO);
		wsaaAgntcoy[wsaaInd.toInt()].set(SPACES);
		wsaaAgntnum[wsaaInd.toInt()].set(SPACES);
		wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaCnttype[wsaaInd.toInt()].set(SPACES);
		/*EXIT*/
	}

protected void readFile2000()
	{
		try {
			readFiles2010();
		}
		catch (GOTOException e){
		}
	}

protected void readFiles2010()
	{
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		if (isNE(wsaaInd,1)) {
			goTo(GotoLabel.exit2090);
		}
		sqlerrorflag = false;
		try {
			for (agprCursorLoopIndex = 1; isLTE(agprCursorLoopIndex,wsaaRowsInBlock.toInt())
			&& sqlagprCursorrs.next(); agprCursorLoopIndex++ ){
				//tom chi bug 1130
//				appVars.getDBObject(sqlagprCursorrs, 1, wsaaAgprData[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 1, wsaaAgntcoy[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 2, wsaaAgntnum[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 3, wsaaEffdate[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 4, wsaaAcctyr[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 5, wsaaMnth[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 6, wsaaCnttype[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 7, wsaaChdrnum[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 8, wsaaMlperpp01[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 9, wsaaMlperpp02[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 10, wsaaMlperpp03[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 11, wsaaMlperpp04[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 12, wsaaMlperpp05[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 13, wsaaMlperpp06[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 14, wsaaMlperpp07[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 15, wsaaMlperpp08[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 16, wsaaMlperpp09[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 17, wsaaMlperpp10[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 18, wsaaMlperpc01[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 19, wsaaMlperpc02[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 20, wsaaMlperpc03[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 21, wsaaMlperpc04[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 22, wsaaMlperpc05[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 23, wsaaMlperpc06[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 24, wsaaMlperpc07[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 25, wsaaMlperpc08[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 26, wsaaMlperpc09[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 27, wsaaMlperpc10[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 28, wsaaMldirpp01[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 29, wsaaMldirpp02[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 30, wsaaMldirpp03[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 31, wsaaMldirpp04[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 32, wsaaMldirpp05[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 33, wsaaMldirpp06[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 34, wsaaMldirpp07[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 35, wsaaMldirpp08[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 36, wsaaMldirpp09[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 37, wsaaMldirpp10[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 38, wsaaMldirpc01[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 39, wsaaMldirpc02[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 40, wsaaMldirpc03[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 41, wsaaMldirpc04[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 42, wsaaMldirpc05[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 43, wsaaMldirpc06[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 44, wsaaMldirpc07[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 45, wsaaMldirpc08[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 46, wsaaMldirpc09[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 47, wsaaMldirpc10[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 48, wsaaMlgrppp01[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 49, wsaaMlgrppp02[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 50, wsaaMlgrppp03[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 51, wsaaMlgrppp04[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 52, wsaaMlgrppp05[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 53, wsaaMlgrppp06[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 54, wsaaMlgrppp07[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 55, wsaaMlgrppp08[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 56, wsaaMlgrppp09[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 57, wsaaMlgrppp10[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 58, wsaaMlgrppc01[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 59, wsaaMlgrppc02[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 60, wsaaMlgrppc03[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 61, wsaaMlgrppc04[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 62, wsaaMlgrppc05[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 63, wsaaMlgrppc06[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 64, wsaaMlgrppc07[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 65, wsaaMlgrppc08[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 66, wsaaMlgrppc09[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 67, wsaaMlgrppc10[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 68, wsaaCntcount[agprCursorLoopIndex]);
				appVars.getDBObject(sqlagprCursorrs, 69, wsaaSumins[agprCursorLoopIndex]);		
//				appVars.getDBObject(sqlagprCursorrs, 2, wsaaAgprInd[agprCursorLoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		if (isEQ(sqlca.getErrorCode(),100)) {
			wsspEdterror.set(varcom.endp);
		}		//tom chi modified, bug 1030
		else if(isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)){
			wsspEdterror.set(varcom.endp);
		}
		//end	
		else {
			if (firstTime.isTrue()) {
				wsaaPrevAgntcoy.set(wsaaAgntcoy[wsaaInd.toInt()]);
				wsaaPrevAgntnum.set(wsaaAgntnum[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}
	}

protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		if (isNE(wsaaAgntcoy[wsaaInd.toInt()],wsaaPrevAgntcoy)
		|| isNE(wsaaAgntnum[wsaaInd.toInt()],wsaaPrevAgntnum)) {
			iy.add(1);
			wsaaPrevAgntcoy.set(SPACES);
			wsaaPrevAgntnum.set(SPACES);
			wsaaPrevAgntcoy.set(wsaaAgntcoy[wsaaInd.toInt()]);
			wsaaPrevAgntnum.set(wsaaAgntnum[wsaaInd.toInt()]);
		}
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| isNE(wsaaAgntcoy[wsaaInd.toInt()],wsaaPrevAgntcoy)
		|| isNE(wsaaAgntnum[wsaaInd.toInt()],wsaaPrevAgntnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		start3200();
	}

protected void start3200()
	{
		agpxpfData.agntcoy.set(wsaaAgntcoy[wsaaInd.toInt()]);
		agpxpfData.agntnum.set(wsaaAgntnum[wsaaInd.toInt()]);
		agpxpfData.effdate.set(wsaaEffdate[wsaaInd.toInt()]);
		agpxpfData.acctyr.set(wsaaAcctyr[wsaaInd.toInt()]);
		agpxpfData.mnth.set(wsaaMnth[wsaaInd.toInt()]);
		agpxpfData.chdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
		agpxpfData.cnttype.set(wsaaCnttype[wsaaInd.toInt()]);
		agpxpfData.mlperpp01.set(wsaaMlperpp01[wsaaInd.toInt()]);
		agpxpfData.mlperpp02.set(wsaaMlperpp02[wsaaInd.toInt()]);
		agpxpfData.mlperpp03.set(wsaaMlperpp03[wsaaInd.toInt()]);
		agpxpfData.mlperpp04.set(wsaaMlperpp04[wsaaInd.toInt()]);
		agpxpfData.mlperpp05.set(wsaaMlperpp05[wsaaInd.toInt()]);
		agpxpfData.mlperpp06.set(wsaaMlperpp06[wsaaInd.toInt()]);
		agpxpfData.mlperpp07.set(wsaaMlperpp07[wsaaInd.toInt()]);
		agpxpfData.mlperpp08.set(wsaaMlperpp08[wsaaInd.toInt()]);
		agpxpfData.mlperpp09.set(wsaaMlperpp09[wsaaInd.toInt()]);
		agpxpfData.mlperpp10.set(wsaaMlperpp10[wsaaInd.toInt()]);
		agpxpfData.mlperpc01.set(wsaaMlperpc01[wsaaInd.toInt()]);
		agpxpfData.mlperpc02.set(wsaaMlperpc02[wsaaInd.toInt()]);
		agpxpfData.mlperpc03.set(wsaaMlperpc03[wsaaInd.toInt()]);
		agpxpfData.mlperpc04.set(wsaaMlperpc04[wsaaInd.toInt()]);
		agpxpfData.mlperpc05.set(wsaaMlperpc05[wsaaInd.toInt()]);
		agpxpfData.mlperpc06.set(wsaaMlperpc06[wsaaInd.toInt()]);
		agpxpfData.mlperpc07.set(wsaaMlperpc07[wsaaInd.toInt()]);
		agpxpfData.mlperpc08.set(wsaaMlperpc08[wsaaInd.toInt()]);
		agpxpfData.mlperpc09.set(wsaaMlperpc09[wsaaInd.toInt()]);
		agpxpfData.mlperpc10.set(wsaaMlperpc10[wsaaInd.toInt()]);
		agpxpfData.mldirpp01.set(wsaaMldirpp01[wsaaInd.toInt()]);
		agpxpfData.mldirpp02.set(wsaaMldirpp02[wsaaInd.toInt()]);
		agpxpfData.mldirpp03.set(wsaaMldirpp03[wsaaInd.toInt()]);
		agpxpfData.mldirpp04.set(wsaaMldirpp04[wsaaInd.toInt()]);
		agpxpfData.mldirpp05.set(wsaaMldirpp05[wsaaInd.toInt()]);
		agpxpfData.mldirpp06.set(wsaaMldirpp06[wsaaInd.toInt()]);
		agpxpfData.mldirpp07.set(wsaaMldirpp07[wsaaInd.toInt()]);
		agpxpfData.mldirpp08.set(wsaaMldirpp08[wsaaInd.toInt()]);
		agpxpfData.mldirpp09.set(wsaaMldirpp09[wsaaInd.toInt()]);
		agpxpfData.mldirpp10.set(wsaaMldirpp10[wsaaInd.toInt()]);
		agpxpfData.mldirpc01.set(wsaaMldirpc01[wsaaInd.toInt()]);
		agpxpfData.mldirpc02.set(wsaaMldirpc02[wsaaInd.toInt()]);
		agpxpfData.mldirpc03.set(wsaaMldirpc03[wsaaInd.toInt()]);
		agpxpfData.mldirpc04.set(wsaaMldirpc04[wsaaInd.toInt()]);
		agpxpfData.mldirpc05.set(wsaaMldirpc05[wsaaInd.toInt()]);
		agpxpfData.mldirpc06.set(wsaaMldirpc06[wsaaInd.toInt()]);
		agpxpfData.mldirpc07.set(wsaaMldirpc07[wsaaInd.toInt()]);
		agpxpfData.mldirpc08.set(wsaaMldirpc08[wsaaInd.toInt()]);
		agpxpfData.mldirpc09.set(wsaaMldirpc09[wsaaInd.toInt()]);
		agpxpfData.mldirpc10.set(wsaaMldirpc10[wsaaInd.toInt()]);
		agpxpfData.mlgrppp01.set(wsaaMlgrppp01[wsaaInd.toInt()]);
		agpxpfData.mlgrppp02.set(wsaaMlgrppp02[wsaaInd.toInt()]);
		agpxpfData.mlgrppp03.set(wsaaMlgrppp03[wsaaInd.toInt()]);
		agpxpfData.mlgrppp04.set(wsaaMlgrppp04[wsaaInd.toInt()]);
		agpxpfData.mlgrppp05.set(wsaaMlgrppp05[wsaaInd.toInt()]);
		agpxpfData.mlgrppp06.set(wsaaMlgrppp06[wsaaInd.toInt()]);
		agpxpfData.mlgrppp07.set(wsaaMlgrppp07[wsaaInd.toInt()]);
		agpxpfData.mlgrppp08.set(wsaaMlgrppp08[wsaaInd.toInt()]);
		agpxpfData.mlgrppp09.set(wsaaMlgrppp09[wsaaInd.toInt()]);
		agpxpfData.mlgrppp10.set(wsaaMlgrppp10[wsaaInd.toInt()]);
		agpxpfData.mlgrppc01.set(wsaaMlgrppc01[wsaaInd.toInt()]);
		agpxpfData.mlgrppc02.set(wsaaMlgrppc02[wsaaInd.toInt()]);
		agpxpfData.mlgrppc03.set(wsaaMlgrppc03[wsaaInd.toInt()]);
		agpxpfData.mlgrppc04.set(wsaaMlgrppc04[wsaaInd.toInt()]);
		agpxpfData.mlgrppc05.set(wsaaMlgrppc05[wsaaInd.toInt()]);
		agpxpfData.mlgrppc06.set(wsaaMlgrppc06[wsaaInd.toInt()]);
		agpxpfData.mlgrppc07.set(wsaaMlgrppc07[wsaaInd.toInt()]);
		agpxpfData.mlgrppc08.set(wsaaMlgrppc08[wsaaInd.toInt()]);
		agpxpfData.mlgrppc09.set(wsaaMlgrppc09[wsaaInd.toInt()]);
		agpxpfData.mlgrppc10.set(wsaaMlgrppc10[wsaaInd.toInt()]);
		agpxpfData.cntcount.set(wsaaCntcount[wsaaInd.toInt()]);
		agpxpfData.sumins.set(wsaaSumins[wsaaInd.toInt()]);
		if (isGT(iy,bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		if (isEQ(iy,1)) {
			agpx01.write(agpxpfData);
		}
		if (isEQ(iy,2)) {
			agpx02.write(agpxpfData);
		}
		if (isEQ(iy,3)) {
			agpx03.write(agpxpfData);
		}
		if (isEQ(iy,4)) {
			agpx04.write(agpxpfData);
		}
		if (isEQ(iy,5)) {
			agpx05.write(agpxpfData);
		}
		if (isEQ(iy,6)) {
			agpx06.write(agpxpfData);
		}
		if (isEQ(iy,7)) {
			agpx07.write(agpxpfData);
		}
		if (isEQ(iy,8)) {
			agpx08.write(agpxpfData);
		}
		if (isEQ(iy,9)) {
			agpx09.write(agpxpfData);
		}
		if (isEQ(iy,10)) {
			agpx10.write(agpxpfData);
		}
		if (isEQ(iy,11)) {
			agpx11.write(agpxpfData);
		}
		if (isEQ(iy,12)) {
			agpx12.write(agpxpfData);
		}
		if (isEQ(iy,13)) {
			agpx13.write(agpxpfData);
		}
		if (isEQ(iy,14)) {
			agpx14.write(agpxpfData);
		}
		if (isEQ(iy,15)) {
			agpx15.write(agpxpfData);
		}
		if (isEQ(iy,16)) {
			agpx16.write(agpxpfData);
		}
		if (isEQ(iy,17)) {
			agpx17.write(agpxpfData);
		}
		if (isEQ(iy,18)) {
			agpx18.write(agpxpfData);
		}
		if (isEQ(iy,19)) {
			agpx19.write(agpxpfData);
		}
		if (isEQ(iy,20)) {
			agpx20.write(agpxpfData);
		}
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
		wsaaInd.add(1);
		if (isLTE(wsaaInd,wsaaRowsInBlock) && isEQ(wsaaAgntnum[wsaaInd.toInt()],SPACES)) {
			wsaaEofInBlock.set("Y");
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		appVars.freeDBConnectionIgnoreErr(sqlagprCursorconn, sqlagprCursorps, sqlagprCursorrs);
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		close4110();
	}

protected void close4110()
	{
		if (isEQ(iz,1)) {
			agpx01.close();
		}
		if (isEQ(iz,2)) {
			agpx02.close();
		}
		if (isEQ(iz,3)) {
			agpx03.close();
		}
		if (isEQ(iz,4)) {
			agpx04.close();
		}
		if (isEQ(iz,5)) {
			agpx05.close();
		}
		if (isEQ(iz,6)) {
			agpx06.close();
		}
		if (isEQ(iz,7)) {
			agpx07.close();
		}
		if (isEQ(iz,8)) {
			agpx08.close();
		}
		if (isEQ(iz,9)) {
			agpx09.close();
		}
		if (isEQ(iz,10)) {
			agpx10.close();
		}
		if (isEQ(iz,11)) {
			agpx11.close();
		}
		if (isEQ(iz,12)) {
			agpx12.close();
		}
		if (isEQ(iz,13)) {
			agpx13.close();
		}
		if (isEQ(iz,14)) {
			agpx14.close();
		}
		if (isEQ(iz,15)) {
			agpx15.close();
		}
		if (isEQ(iz,16)) {
			agpx16.close();
		}
		if (isEQ(iz,17)) {
			agpx17.close();
		}
		if (isEQ(iz,18)) {
			agpx18.close();
		}
		if (isEQ(iz,19)) {
			agpx19.close();
		}
		if (isEQ(iz,20)) {
			agpx20.close();
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
