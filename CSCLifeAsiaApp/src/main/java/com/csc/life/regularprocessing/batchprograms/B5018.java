/*
 * File: B5018.java
 * Date: 29 August 2009 20:51:16
 * Author: Quipoz Limited
 *
 * Class transformed from B5018.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.dao.impl.ChdrpfDAOImpl;
import com.csc.fsu.general.dataaccess.dao.impl.PtrnpfDAOImpl;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon6;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon6rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.tablestructures.T6640rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.impl.CovrpfDAOImpl;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.regularprocessing.dataaccess.dao.B5018TempDAO;
import com.csc.life.regularprocessing.dataaccess.dao.BonlpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.BonspfDAO;
import com.csc.life.regularprocessing.dataaccess.model.B5018DTO;
import com.csc.life.regularprocessing.dataaccess.model.B5018ForUpdateDTO;
import com.csc.life.regularprocessing.dataaccess.model.Bonlpf;
import com.csc.life.regularprocessing.dataaccess.model.Bonspf;
import com.csc.life.regularprocessing.dataaccess.model.T5679Rec;
import com.csc.life.regularprocessing.recordstructures.Bonusrec;
import com.csc.life.regularprocessing.tablestructures.T6639rec;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*          COMPANY ANNIVERSARY BONUSES
*          ===========================
*
*   This batch job processes contracts which are due a bonus.
*  It is decided that the mainline batch processing program (B5018  will
*  call the Reversionary Bonus subroutine dependent on its compone ts
*  method, this is table driven. This subroutine will write the
*  appropriate ACMV's and return the bonus value to the calling
*  mainline. The mainline can accumulate the returned values for
*  production of a notice.
*  The parameter screen - S5018 will appear as follows:-
*  --------------------------------------------------------------- -----
*                                                                - -----
*                    Reversionary Bonus (Company Anniv. )          S5018
*  Effective Date DD/MM/YYYY   Accounting Month: MM  Year: YY
*  Job Name: REVBON   Run Id: RB Job Queue: QBATCH  Library: &LIBL
*        Extract all contract from . . . ________
*                               to . . . ________
*        Bonus Payable Period  . . . . . ________
*                                                                - -----
*  --------------------------------------------------------------- -
*  The CL program C5018 will set the parameters for the contract f om
*  and to fields as zeroes and nines respectively if no informatio  is
*  entered.
*  Set up batching parameters call call subroutine 'BATCDOR' to op n the
*  batch file. Function will be 'OPEN'.
*  SQL programming will be needed to select components (COVR'S) wh re
*  the (UNIT-STATEMENT-DATE < DECLARATION DATE or = 99999999 ).
*  Note that the UNIT-STAT-DATE will be the picture at the time of the
*  Bonus Declaration Date, not today!
*  This is true for all figures retrieved for the Bonus Run. Ie. C VR
* </pre>
*/
public class B5018 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private java.sql.ResultSet sqlcovr1rs = null;
	private java.sql.PreparedStatement sqlcovr1ps = null;
	private java.sql.Connection sqlcovr1conn = null;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5018");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaT6639Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6640Bonus = new FixedLengthStringData(4).isAPartOf(wsaaT6639Key, 0);
	private FixedLengthStringData wsaaPremStat = new FixedLengthStringData(2).isAPartOf(wsaaT6639Key, 4);
	private FixedLengthStringData wsaaParmCompany = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private String wsaaCovrRecStatus = "";
	private String wsaaProcessingStatus = "";
	private FixedLengthStringData wsaaChdrStatusFlag = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCovrStatusFlag = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaCoveragePremStatus = new FixedLengthStringData(24);
	private FixedLengthStringData[] wsaaT5679CvpremStat = FLSArrayPartOfStructure(12, 2, wsaaCoveragePremStatus, 0);

	private FixedLengthStringData wsaaCoverageRiskStatus = new FixedLengthStringData(24);
	private FixedLengthStringData[] wsaaT5679CvriskStat = FLSArrayPartOfStructure(12, 2, wsaaCoverageRiskStatus, 0);

	private FixedLengthStringData wsaaContractPremStatus = new FixedLengthStringData(24);
	private FixedLengthStringData[] wsaaT5679CnpremStat = FLSArrayPartOfStructure(12, 2, wsaaContractPremStatus, 0);

	private FixedLengthStringData wsaaContractRiskStatus = new FixedLengthStringData(24);
	private FixedLengthStringData[] wsaaT5679CnriskStat = FLSArrayPartOfStructure(12, 2, wsaaContractRiskStatus, 0);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3);
	
	private FixedLengthStringData wsaaCowncoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCownnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLastChdrnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaStatCount = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaPreviousComp = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaCurrentComp = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCurrentCover = new FixedLengthStringData(2).isAPartOf(wsaaCurrentComp, 0);
	private FixedLengthStringData wsaaCurrentRider = new FixedLengthStringData(2).isAPartOf(wsaaCurrentComp, 2);
	private FixedLengthStringData wsaaChdrmnaStatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrmnaPstatcode = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaBonlrvbVars = new FixedLengthStringData(730);
	private String[] wsaaBonlrvbLongdesc = new String[10];
	private BigDecimal[] wsaaBonlrvbSumins = new BigDecimal[10];
	private String[] wsaaBonlrvbBcalmeth =  new String[10];
	private BigDecimal[] wsaaBonlrvbBpayty = new BigDecimal[10];
	private BigDecimal[] wsaaBonlrvbBpayny = new BigDecimal[10];
	private BigDecimal[] wsaaBonlrvbTotbon = new BigDecimal[10];

	private FixedLengthStringData wsaaContractLock = new FixedLengthStringData(1).init("N");
	private Validator contractLocked = new Validator(wsaaContractLock, "Y");
	private Validator contractNotLocked = new Validator(wsaaContractLock, "N");
	private FixedLengthStringData wsaaBonusMethod = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaLcrider = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaLcsubr = new FixedLengthStringData(8);
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
		/* ERRORS */
	private static final String e402 = "E402";
	private static final String g408 = "G408";
	private static final String g437 = "G437";
	private static final String h149 = "H149";
		/* TABLES */
	private static final String t5679 = "T5679";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler1, 5);

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private boolean endOfFile = false;

		/*01  P5018-PARM-RECORD.                                           */
	private FixedLengthStringData p5018ParmRecord = new FixedLengthStringData(35);
	private ZonedDecimalData p5018BonusPeriod = new ZonedDecimalData(11, 5).isAPartOf(p5018ParmRecord, 0);
	private FixedLengthStringData p5018Chdrnum = new FixedLengthStringData(8).isAPartOf(p5018ParmRecord, 11);
	private FixedLengthStringData p5018Chdrnum1 = new FixedLengthStringData(8).isAPartOf(p5018ParmRecord, 19);
	private ZonedDecimalData p5018DclDate = new ZonedDecimalData(8, 0).isAPartOf(p5018ParmRecord, 27);

	private ItemTableDAM itemIO = new ItemTableDAM();

	private Varcom varcom = new Varcom();
	private Batcuprec batcuprec = new Batcuprec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon6rec datcon6rec = new Datcon6rec();
	private Bonusrec bonusrec = new Bonusrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Sftlockrec sftlockrec1 = new Sftlockrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Tr384rec tr384rec = new Tr384rec();
	private T6639rec t6639rec = new T6639rec();
	private T6640rec t6640rec = new T6640rec();
	private T6625rec t6625rec = new T6625rec();
	private T5679T5679RecInner t5679T5679RecInner = new T5679T5679RecInner();
	private WsaaContractTypeInner wsaaContractTypeInner = new WsaaContractTypeInner();
	private WsaaTr384KeyInner wsaaTr384KeyInner = new WsaaTr384KeyInner();
	private B5018TempDAO b5018TempDAO = getApplicationContext().getBean("b5018TempDAO", B5018TempDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAOImpl.class);
	private ChdrpfDAO chdrDAO = getApplicationContext().getBean("chdrDAO", ChdrpfDAOImpl.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAOImpl.class);
	private BonlpfDAO bonlpfDAO = getApplicationContext().getBean("bonlpfDAO", BonlpfDAO.class);
	private BonspfDAO bonspfDAO = getApplicationContext().getBean("bonspfDAO", BonspfDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private T5679Rec t5679rec = new T5679Rec();
	private List<B5018DTO> b5018DTOList = null;
	private List<Covrpf> covrrnlList = new ArrayList<Covrpf>();
	private B5018DTO b5018DTO = null;
	private Iterator<B5018DTO> iterator;
	private boolean corvvnlEndFlag = false;
	private boolean chdrEndFlag = false;
	private Covrpf covrpf = null;
	private Chdrpf chdrpf = null;
	private Ptrnpf ptrnpf = null;
	private List<Ptrnpf> ptrnpfList = new ArrayList<Ptrnpf>();
	private List<Chdrpf> chdrpfList = new ArrayList<Chdrpf>();
	private List<B5018ForUpdateDTO> b5018UpdateDTOList = new ArrayList<B5018ForUpdateDTO>();
	private List<Bonspf> bonsrvbTotalUpdateList = new ArrayList<Bonspf>();
	private List<Bonspf> bonsrvbTotalInsertList = new ArrayList<Bonspf>();
	
	private List<Bonlpf> bonlrvbTotalUpdateList = new ArrayList<Bonlpf>();
	private List<Bonlpf> bonlrvbTotalInsertList = new ArrayList<Bonlpf>();
	/*ILIFE-3274 Starts */
	private List<Letrqstrec> letrqstList = new ArrayList<Letrqstrec>();
	/*ILIFE-3274 Ends */
	private Bonspf bonspf = null;
	private Bonlpf bonlpf = null;
    private int count=0;

	private Map<String, List<Itempf>> t6640ListMap;
	private Map<String, List<Itempf>> tr384ListMap;
	private Map<String, List<Itempf>> t6639ListMap;
	private Map<String, List<Itempf>> t6625ListMap;
	private Map<String, Descpf> t5687Map;
	
    private int countCt01=0;
    private BigDecimal countCt02=BigDecimal.ZERO;
    private int countCt03=0;
    
    private ZonedDecimalData wsaaTransactionDate = new ZonedDecimalData(6, 0).init(0).setUnsigned();
    private boolean hasRecords = false;
    
    private int batchID = 0;
    private int batchExtractSize;
    
    private int wsaaTranno;
    
    private boolean ausCtxFlag = false;
    
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit190,
		readPrimary420,
		readNextRecord460,
		exit490,
		eof580,
		exit590,
		exit1090,
		continue1750,
		exit1790,
		exit4090,
		writeRecord5030,
		writeRecord6030
	}

	public B5018() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		try {
			main110();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void main110()
	{
		initialise200();
		//ILIFE-3782 improve
		/*if(iterator!=null && iterator.hasNext()){
		    hasRecords = true;
		}*/
		processAll();
		
		/* while ( !endOfFile) {
			processRecs400();
			if (runparmrec.cmtnumber.gt(runparmrec.cmtpoint)) {
			    commit3500();
			}
		}*/
		if (hasRecords) {
		    commit3500();
		}
		//ILIFE-3782 improve
		finished900();
		goTo(GotoLabel.exit190);
	}

protected void sqlError180()
	{
		sqlError();
	}

protected void initialise200()
	{
		initialise210();
		openBatch220();
		// ILIFE-3782 improve
		setAccessDay();
		// ILIFE-3782 improve
		readSmartTables(wsaaParmCompany.toString());
		readFirstRecord300();
	}

private void readSmartTables(String company){
	// ILIFE-3782 improve
	itemIO.setParams(SPACES);
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(runparmrec.company);
	itemIO.setItemtabl(t5679);
	itemIO.setItemitem(runparmrec.transcode);
	itemIO.setFormat(itemrec);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(), varcom.oK)) {
		conerrrec.params.set(itemIO.getParams());
		databaseError006();
	}
	t5679T5679RecInner.t5679T5679Rec.set(itemIO.getGenarea());
	/*List<String> itemItem = new ArrayList();
	itemItem.add(runparmrec.transcode.toString());
	List<Itempf> t5679List = itemDAO.getItemItems("IT", runparmrec.company.toString(), t5679, itemItem);
	if (t5679List == null || t5679List.isEmpty()) {
		conerrrec.params.set(itemIO.getParams());
		databaseError006();
	}
	t5679T5679RecInner.t5679T5679Rec.set(t5679List.get(0).getGenarea());*/
	// ILIFE-3782 improve
	
	t6640ListMap = itemDAO.loadSmartTable("IT", company, "T6640");
	t6639ListMap = itemDAO.loadSmartTable("IT", company, "T6639");
	tr384ListMap = itemDAO.loadSmartTable("IT", company, "TR384");	
	t6625ListMap = itemDAO.loadSmartTable("IT", company, "T6625");
	t5687Map = descDAO.getItems("IT", company, "T5687",runparmrec.language.toString());
	String ctxItem = "BTPRO017";
	ausCtxFlag = FeaConfg.isFeatureExist(company, ctxItem, appVars, "IT");
}

protected void initialise210()
	{
		p5018ParmRecord.set(conjobrec.params);
		wsaaParmCompany.set(runparmrec.company);
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		wsaaLastChdrnum.set(SPACES);
		count = 0;
		wsaaCovrRecStatus = "N";
		wsaaContractLock.set("N");
		wsaaProcessingStatus = "N";
		wsaaCovrStatusFlag.set(SPACES);
		wsaaChdrStatusFlag.set(SPACES);
		wsaaCoveragePremStatus.set(SPACES);
		wsaaCoverageRiskStatus.set(SPACES);
		wsaaContractPremStatus.set(SPACES);
		wsaaContractRiskStatus.set(SPACES);
		wsaaTransactionDate.set(runparmrec.effdate);
		count = 0;
		wsaaStatCount.set(ZERO);
		wsaaBonlrvbVars.set(SPACES);
		wsaaCurrentComp.set(SPACES);
		while ( !(isEQ(count, 10))) {
			wsaaBonlrvbSumins[count] = new BigDecimal(0);
			wsaaBonlrvbBpayty[count] = new BigDecimal(0);
			wsaaBonlrvbBpayny[count] = new BigDecimal(0);
			wsaaBonlrvbTotbon[count] = new BigDecimal(0);
			count++;
		}
		count = 0;
        if (runparmrec.runparm1.isNumeric()) {
            if (runparmrec.runparm1.toInt() > 0) {
            	batchExtractSize = runparmrec.runparm1.toInt();
            } else {
            	batchExtractSize = runparmrec.cmtpoint.toInt();
            }
        } else {
        	batchExtractSize = runparmrec.cmtpoint.toInt();
        }

	}

protected void openBatch220()
	{
		batcdorrec.function.set("OPEN");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(runparmrec.transcode);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			conerrrec.statuz.set(batcdorrec.statuz);
			conerrrec.params.set(batcdorrec.batcdorRec);
			systemError005();
		}
	}
//ILIFE-3782 improve
protected void setAccessDay()
	{
		
		/*    Access today's date using DATCON1.                           */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			conerrrec.params.set(datcon1rec.datcon1Rec);
			conerrrec.statuz.set(datcon1rec.statuz);
			databaseError006();
		}
		wsaaToday.set(datcon1rec.intDate);
	}
//ILIFE-3782 improve

protected void readFirstRecord300(){

	t5679rec.t5679T5679Rec.set(t5679T5679RecInner.t5679T5679Rec);
	b5018TempDAO.initTempTable(wsaaParmCompany.toString(), p5018Chdrnum.toString(), p5018Chdrnum1.toString(), p5018DclDate.toString(),batchExtractSize, t5679rec);
	//ILIFE-3782 improve
	// readDataFromTempTable();
	//ILIFE-3782 improve
}
protected void readDataFromTempTable(){
	b5018DTOList = b5018TempDAO.searchB5018Temp(batchID);
	batchID++;
    if (b5018DTOList != null && b5018DTOList.size() > 0) 
    {
        iterator = b5018DTOList.iterator();
        endOfFile = false;
    }
    else
    {
    	endOfFile = true;
    }
}

//ILIFE-3782 improve
private void processAll() {
	b5018DTOList = b5018TempDAO.getALLB5018Temp();
	if (b5018DTOList.isEmpty()) {
		endOfFile = true;
	}
	for (B5018DTO b5018DTOT : b5018DTOList) {
		hasRecords = true;
		this.b5018DTO = b5018DTOT;
		// same as function start410()
		updateReqd004();
		if (isEQ(controlrec.flag, "N")) {
			continue;
		}
		
		// same as readPrimary420       
        /* If the COVR record just read is for the Contract which is       */
        /* already locked by another transaction, then we must continue    */
        /* reading through the COVR records until there is a change in     */
        /* contract number.                                                */
        if (b5018DTO.getChdrnum().equals(wsaaLastChdrnum.toString())
        	&& contractLocked.isTrue()) {
           continue;
        }
        /* we need to check here that the CURRFROM & CURRTO dates on the*/
        /* COVR record read span the correct period for the bonus - if*/
        /* not, we need to go backwards until we find the correct record*/
        checkCovrDates600();
        if ((isNE(wsaaCovrRecStatus, "Y"))) {
           continue;
        }
		
		if (((isNE(wsaaLastChdrnum, b5018DTO.getChdrnum()))
		&& (isNE(wsaaLastChdrnum, SPACES)))
		&& contractNotLocked.isTrue()) {
			/* Contract number has changed so we wish to write the BONLRVB*/
			/*  record we have been building up and UNLOCK contract header.*/
			/* The not = spaces check is here to make sure we don't trip up*/
			/*  on the 1st time through.*/
			writeBonlrvbRec6000();
			unlockSoftlock7000();
		}
		updateChdrTranno1000();
		/* If the contract is locked by another transaction then we must   */
		/* ignore this contract and go and read the next COVR record.      */
		if (contractLocked.isTrue()) {
			continue;
//			goTo(GotoLabel.readNextRecord460);
		}
		if ((isNE(wsaaCovrRecStatus, "Y"))) {
			continue;
//			goTo(GotoLabel.readNextRecord460);
		}
		
		countCt01++;
		/* PERFORM 2000-READ-T6639-T6640-T6634.                         */
		readT6639T6640Tr3842000();
		callBonusSubroutine3000();
		writePtrn4000();
		writeLetcBons5000();
	//	unlockSoftlock7000(); // ILIFE-7904

		// commit records as configure
		if (runparmrec.cmtnumber.gt(runparmrec.cmtpoint)) {
		    commit3500();
		}
	}
	/* check for E-O-F and depending on whether we processed any*/
	/*  records or not, either write BONLRVB rec and exit or just*/
	/*  exit.*/
	/* If the contract is already loked by another transaction*/
	/* then this contract will not be processed by this job.*/
	if (!endOfFile) {
		if ((isNE(wsaaLastChdrnum, SPACES))
			&& contractNotLocked.isTrue()) {
			/*  if WSAA-LAST-CHDRNUM not spaces, then we have processed at*/
			/*   least 1 contract*/
			writeBonlrvbRec6000();
			unlockSoftlock7000();
		}
		return;
		//goTo(GotoLabel.exit490);
	}
}
//ILIFE-3782 improve

protected void processRecs400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start410();
				case readPrimary420:
					readPrimary420();
				case readNextRecord460:
					readNextRecord460();
				case exit490:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start410()
	{
		updateReqd004();
		if (isEQ(controlrec.flag, "N")) {
			goTo(GotoLabel.readNextRecord460);
		}
	}

protected void readPrimary420()
	{
		//fetchPrimary500();		
		
		  if (!iterator.hasNext()) {
		    	readDataFromTempTable();
		    } 
			 while(iterator.hasNext())
			 {
				 b5018DTO = iterator.next();		        
		        /* If the COVR record just read is for the Contract which is       */
		        /* already locked by another transaction, then we must continue    */
		        /* reading through the COVR records until there is a change in     */
		        /* contract number.                                                */
		        if (b5018DTO.getChdrnum().equals(wsaaLastChdrnum.toString())
		        && contractLocked.isTrue()) {
		           continue;
		        }
		        /* we need to check here that the CURRFROM & CURRTO dates on the*/
		        /* COVR record read span the correct period for the bonus - if*/
		        /* not, we need to go backwards until we find the correct record*/
		        checkCovrDates600();
		        if ((isNE(wsaaCovrRecStatus, "Y"))) {
		           continue;
		        }
					/* check for E-O-F and depending on whether we processed any*/
				/*  records or not, either write BONLRVB rec and exit or just*/
				/*  exit.*/
				/* If the contract is already loked by another transaction*/
				/* then this contract will not be processed by this job.*/
				if (endOfFile) {
					if ((isNE(wsaaLastChdrnum, SPACES))
					&& contractNotLocked.isTrue()) {
						/*  if WSAA-LAST-CHDRNUM not spaces, then we have processed at*/
						/*   least 1 contract*/
						writeBonlrvbRec6000();
						unlockSoftlock7000();
					}
					goTo(GotoLabel.exit490);
				}
				if (((isNE(wsaaLastChdrnum, b5018DTO.getChdrnum()))
				&& (isNE(wsaaLastChdrnum, SPACES)))
				&& contractNotLocked.isTrue()) {
					/* Contract number has changed so we wish to write the BONLRVB*/
					/*  record we have been building up and UNLOCK contract header.*/
					/* The not = spaces check is here to make sure we don't trip up*/
					/*  on the 1st time through.*/
					writeBonlrvbRec6000();
					unlockSoftlock7000();
				}
				updateChdrTranno1000();
				/* If the contract is locked by another transaction then we must   */
				/* ignore this contract and go and read the next COVR record.      */
				if (contractLocked.isTrue()) {
					goTo(GotoLabel.readNextRecord460);
				}
				if ((isNE(wsaaCovrRecStatus, "Y"))) {
					goTo(GotoLabel.readNextRecord460);
				}
				
				countCt01++;
				/* PERFORM 2000-READ-T6639-T6640-T6634.                         */
				readT6639T6640Tr3842000();
				callBonusSubroutine3000();
				writePtrn4000();
				writeLetcBons5000();
				unlockSoftlock7000(); // ILIFE-7904
		
			 }
			 
			 if (endOfFile) 
			 {
					
					goTo(GotoLabel.exit490);
			 }
	}

protected void readNextRecord460()
	{
		goTo(GotoLabel.readPrimary420);
	}

protected void fetchPrimary500(){       
    if (!iterator.hasNext()) {
    	readDataFromTempTable();
    } 
	 while(iterator.hasNext()){
		 b5018DTO = iterator.next();
        
        /* If the COVR record just read is for the Contract which is       */
        /* already locked by another transaction, then we must continue    */
        /* reading through the COVR records until there is a change in     */
        /* contract number.                                                */
        if (b5018DTO.getChdrnum().equals(wsaaLastChdrnum.toString())
        && contractLocked.isTrue()) {
           continue;
        }
        /* we need to check here that the CURRFROM & CURRTO dates on the*/
        /* COVR record read span the correct period for the bonus - if*/
        /* not, we need to go backwards until we find the correct record*/
        checkCovrDates600();
        if ((isNE(wsaaCovrRecStatus, "Y"))) {
           continue;
        }
        return;
    }
}

protected void eof580()
	{
		wsaaEof.set("Y");
	}

protected void checkCovrDates600()
	{
		/*  Check that the P5018 bonus declaration date is*/
		/*  between the CURRFROM & CURRTO dates of this record*/
		/* - if not, read backwards through COVR validflag '2' records*/
		/*   to get get COVR for correct period.*/
		wsaaCovrRecStatus = "N";
		wsaaCoveragePremStatus.set(t5679T5679RecInner.t5679CovPremStats);
		wsaaCoverageRiskStatus.set(t5679T5679RecInner.t5679CovRiskStats);
		if ((isGTE(p5018DclDate, b5018DTO.getCurrfrom())
		&& isLTE(p5018DclDate, b5018DTO.getCurrto()))) {
			/* then we have the correct COVR record for our bonus period*/
			wsaaProcessingStatus = "Y";
			wsaaCovrRecStatus = "Y";
			return ;
		}
		/* IF we get here then its the wrong COVR in the SQL retrieve so*/
		/*  we need to read backwards through the COVRRNL to get  correct*/
		/*  record, starting at the record we have from the SQL query,*/
		/*  the only one with a VALIDFLAG '1'.*/
		wsaaCovrRecStatus = "N";
		covrrnlList = covrpfDAO.searchCovrrnlRecord(b5018DTO.getChdrcoy(), b5018DTO.getChdrnum(), b5018DTO.getLife(), b5018DTO.getCoverage(), b5018DTO.getRider(), b5018DTO.getPlnsfx());

		if (covrrnlList == null /*&& covrrnlList.size() < 0*/) {//IJTI-320
			databaseError006();
	    }
		for(int i = 0;i < covrrnlList.size();i++){
			if(i > 0 && !corvvnlEndFlag){
				covrpf = covrrnlList.get(i);
				checkCovrStatus700();
			}
			
		}

		/* Need to make note of all COVR rec variables we need for*/
		/*  processing later on ( because the data from the SQL-read*/
		/*  COVR record may have changed since this record was valid*/
		//ILIFE-3274 Starts
		if (covrpf != null) {
		//ILIFE-3274 Ends
			b5018DTO.setStatcode(covrpf.getStatcode());
			b5018DTO.setPstatcode(covrpf.getPstatcode());
			b5018DTO.setCrrcd(covrpf.getCrrcd());
			b5018DTO.setRcesdte(covrpf.getRiskCessDate());
			b5018DTO.setCrtable(covrpf.getCrtable());
			b5018DTO.setSumins(covrpf.getSumins());
		}
	}

protected void checkCovrStatus700()
	{
		/* we are going to read backwards through the COVRRNL file but*/
		/*  we will use a function of NEXTR because the file is defined*/
		/*  as LIFO ... Last in, First out.*/
//		covrrnlIO.setFunction(varcom.nextr);
//		SmartFileCode.execute(appVars, covrrnlIO);

		if ((isNE(covrpf.getChdrcoy(), b5018DTO.getChdrcoy())
		|| isNE(covrpf.getChdrnum(), b5018DTO.getChdrnum())
		|| isNE(covrpf.getLife(), b5018DTO.getLife())
		|| isNE(covrpf.getCoverage(), b5018DTO.getCoverage())
		|| isNE(covrpf.getRider(), b5018DTO.getRider())
		|| isNE(covrpf.getPlanSuffix(), b5018DTO.getPlnsfx())
		|| corvvnlEndFlag)) {
			corvvnlEndFlag = true;
			return ;
		}
		if ((isLT(p5018DclDate, covrpf.getCurrfrom())
		&& isGT(p5018DclDate, covrpf.getCurrto()))) {
			/*         dates don't match so go and get next record*/
			return ;
		}
		/*    now check whether coverage statii are valid*/
		wsaaStatCount.set(ZERO);
		wsaaCovrStatusFlag.set("N");
		while ( !(isEQ(wsaaStatCount, 12))) {
			wsaaStatCount.add(1);
			if ((isEQ(covrpf.getStatcode(), wsaaT5679CvriskStat[wsaaStatCount.toInt()]))) {
				wsaaStatCount.set(12);
				wsaaCovrStatusFlag.set("Y");
			}
		}

		if ((isEQ(wsaaCovrStatusFlag, "N"))) {
			/*    invalid coverage status so ignore contract/coverage and*/
			/*    we'll go and get next coverage record from SQL*/
			corvvnlEndFlag = true;
			wsaaProcessingStatus = "N";
			return ;
		}
		/*    get here so coverage prem status ok - now check risk status*/
		wsaaCovrStatusFlag.set("N");
		wsaaStatCount.set(ZERO);
		while ( !(isEQ(wsaaStatCount, 12))) {
			wsaaStatCount.add(1);
			if ((isEQ(b5018DTO.getPstatcode(), wsaaT5679CvpremStat[wsaaStatCount.toInt()]))) 
			{
				wsaaStatCount.set(12);
				wsaaCovrStatusFlag.set("Y");
				wsaaProcessingStatus = "Y";
				wsaaCovrRecStatus = "Y";
				corvvnlEndFlag = true;
				return ;
			}
		}

		wsaaProcessingStatus = "N";
	}

protected void finished900(){
	if (covrrnlList != null) {
		covrrnlList.clear();
    }
	covrrnlList = null;
	
	if (b5018UpdateDTOList != null) {
		b5018UpdateDTOList.clear();
    }
	b5018UpdateDTOList = null;
	
	if (b5018DTOList != null) {
		b5018DTOList.clear();
    }
	b5018DTOList = null;
	/*ILIFE-3274 Starts */
	 if (letrqstList!= null) {
     	letrqstList.clear();
     }
	 letrqstList = null;
	 /*ILIFE-3274 Ends */
	if (t6640ListMap != null) {
		t6640ListMap.clear();
    }
	t6640ListMap = null;
	
	if (tr384ListMap != null) {
		tr384ListMap.clear();
    }
	tr384ListMap = null;
	
	if (t6639ListMap != null) {
		t6639ListMap.clear();
    }
	t6639ListMap = null;
	
	if (t6625ListMap != null) {
		t6625ListMap.clear();
    }
	t6625ListMap = null;
	
	if (t5687Map != null) {
		t5687Map.clear();
    }
	t5687Map = null;
	
	closeFiles910();
	closeBatch920();
	//ILIFE-3274 Starts
	//closeFiles910();
	//closeBatch920();
	//ILIFE-3274 Ends
}

protected void closeFiles910()
	{
		/*   Close the cursor*/
		getAppVars().freeDBConnectionIgnoreErr(sqlcovr1conn, sqlcovr1ps, sqlcovr1rs);
	}

protected void closeBatch920()
	{
		batcdorrec.function.set("CLOSE");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(runparmrec.transcode);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			conerrrec.params.set(batcdorrec.batcdorRec);
			conerrrec.statuz.set(batcdorrec.statuz);
			systemError005();
		}
	}

protected void updateChdrTranno1000()
	{
		try {
			para1010();
			lockChdr1020();
			checkAndUpdateCtrt1030();
			saveFields1040();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para1010()
	{
		if (isEQ(wsaaLastChdrnum, b5018DTO.getChdrnum())) {
			goTo(GotoLabel.exit1090);
		}
	}

	/**
	* <pre>
	* we are processing a new contract number, so lock the contract
	*  header record.
	* </pre>
	*/
protected void lockChdr1020()
	{
		sftlockrec1.function.set("LOCK");
		sftlockrec1.company.set(runparmrec.company);
		sftlockrec1.enttyp.set("CH");
		sftlockrec1.entity.set(b5018DTO.getChdrnum());
		sftlockrec1.transaction.set(runparmrec.transcode);
		sftlockrec1.user.set(runparmrec.user);
		callProgram(Sftlock.class, sftlockrec1.sftlockRec);
		if ((isNE(sftlockrec1.statuz, varcom.oK))
		&& (isNE(sftlockrec1.statuz, "LOCK"))) {
			conerrrec.params.set(sftlockrec1.sftlockRec);
			conerrrec.statuz.set(sftlockrec1.statuz);
			systemError005();
		}
		/* IF SFTL-STATUZ              = 'LOCK'                         */
		/*     MOVE SFTL-SFTLOCK-REC   TO CONR-PARAMS                   */
		/*     MOVE SFTL-STATUZ        TO CONR-STATUZ                   */
		/*     PERFORM 005-SYSTEM-ERROR.                                */
		/* If the contract is already locked by another transaction then   */
		/* Display an error message, increment the control total by 1      */
		/* and continue processing.                                        */
		if (isEQ(sftlockrec1.statuz, "LOCK")) {
			wsaaContractLock.set("Y");
			wsaaLastChdrnum.set(b5018DTO.getChdrnum());
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(b5018DTO.getChdrnum());
			stringVariable1.addExpression(" Header record locked.");
			stringVariable1.setStringInto(conlogrec.message);
			conlogrec.error.set(sftlockrec1.statuz);
			callConlog003();
			countCt03++;
			goTo(GotoLabel.exit1090);
		}
	}

protected void checkAndUpdateCtrt1030()
	{

		if (isNE(b5018DTO.getValidflag(), "1")) {
			databaseError006();
		}
		/*    check contract CURRFROM/CURRTO dates and contract statii*/
		if ((isGTE(p5018DclDate, b5018DTO.getCurrfrom())
		&& isLTE(p5018DclDate, b5018DTO.getCurrto()))) {
			/*        don't need to check statii here becasuse this record was*/
			/*        checked during the SQL retrieve*/
			wsaaProcessingStatus = "Y";
		}
		else {
			/*     Need to read through the old CHDR records for this contract*/
			/*     until we find the correct CURRFROM/CURRTO period - we need*/
			/*     to then check the contract statii to make sure contract was*/
			/*     'IN FORCE' during our Bonus period. - otherwise NO BONUS !!*/
			checkOldChdrs1500();
		}
		if ((isEQ(wsaaProcessingStatus, "N"))) {
			/* Haven't found a valid contract so unlock all records and drop*/
			/*  out and get next COVR from SQL retrieve*/
			wsaaCovrRecStatus = "N";
			sftlockrec1.sftlockRec.set(SPACES);
			sftlockrec1.company.set(runparmrec.company);
			sftlockrec1.entity.set(b5018DTO.getChdrnum());
			sftlockrec1.enttyp.set("CH");
			sftlockrec1.user.set(runparmrec.user);
			sftlockrec1.transaction.set(runparmrec.transcode);
			sftlockrec1.statuz.set(SPACES);
			sftlockrec1.function.set("UNLK");
			callProgram(Sftlock.class, sftlockrec1.sftlockRec);
			if (isNE(sftlockrec1.statuz, varcom.oK)) {
				conerrrec.params.set(sftlockrec1.sftlockRec);
				conerrrec.statuz.set(sftlockrec1.statuz);
				systemError005();
//				chdrlifIO.setFunction(varcom.rlse);
//				chdrlifIO.setFormat(chdrlifrec);
//				SmartFileCode.execute(appVars, chdrlifIO);
//				if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
//					conerrrec.params.set(chdrlifIO.getParams());
//					conerrrec.statuz.set(chdrlifIO.getStatuz());
//					databaseError006();
//					goTo(GotoLabel.exit1090);
//				}
			}
		}
		
		setPrecision(b5018DTO.getTranno(), 0);
		chdrpf = new Chdrpf();
		chdrpf.setTranno(b5018DTO.getTranno() + 1);
		chdrpf.setJobnm(b5018DTO.getJobnm());
		chdrpf.setUsrprf(b5018DTO.getUsrprf());
		chdrpf.setChdrcoy(b5018DTO.getChdrcoy().charAt(0));
		chdrpf.setChdrnum(b5018DTO.getChdrnum());
		chdrpfList.add(chdrpf);

	}

protected void saveFields1040()
	{
		/* store current contract number and reset counter to zeros.*/
		wsaaLastChdrnum.set(b5018DTO.getChdrnum());/* IJTI-1523 */
		count = 0;
		wsaaCovrRecStatus = "Y";
		wsaaCntcurr.set(b5018DTO.getCntcurr());
		wsaaContractTypeInner.wsaaCnttype.set(b5018DTO.getCnttype());
		wsaaTranno = b5018DTO.getTranno() + 1;
		wsaaCowncoy.set(b5018DTO.getCowncoy());
		wsaaCownnum.set(b5018DTO.getCownnum());
		wsaaContractLock.set("N");
	}

protected void checkOldChdrs1500()
	{
		/* Do a BEGN on CHDRMNA file to get most recent Validflag '1'*/
		/*  record ( because LF is defined as LIFO ) then do NEXTR*/
		/*  to start checking the validflag '2' records.*/
		wsaaContractPremStatus.set(t5679T5679RecInner.t5679CnPremStats);
		wsaaContractRiskStatus.set(t5679T5679RecInner.t5679CnRiskStats);
		for(int i = 0;i < b5018DTOList.size();i++){
			if (isNE(b5018DTOList.get(i).getValidflag(), "1")) {
				databaseError006();
			}
			if(i > 0 &&!chdrEndFlag){
				checkChdrStatus1700();
			}
			
		}

	}

protected void checkChdrStatus1700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start1700();
				case continue1750:
					continue1750();
				case exit1790:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1700()
	{
		wsaaChdrmnaStatcode.set(b5018DTO.getStatcode());
		wsaaChdrmnaPstatcode.set(b5018DTO.getPstatcode());
		if (chdrEndFlag) {
			databaseError006();
		}
	
		/*    check contract CURRFROM/CURRTO dates and contract statii*/
		if ((isLT(p5018DclDate, b5018DTO.getCurrfrom())
		&& isGT(p5018DclDate, b5018DTO.getCurrto()))) {
			if (isGT(p5018DclDate, b5018DTO.getCurrto())) {
				/*        dates don't match but there is no CHDR record for our*/
				/*        declaration period so we use 1st record after DCL date*/
				/*        ie the previous record read*/
				goTo(GotoLabel.continue1750);
			}
			/*         dates don't match so go and get next record*/
			goTo(GotoLabel.exit1790);
		}
	}

protected void continue1750()
	{
		/*    now check whether contract statii are valid*/
		wsaaStatCount.set(ZERO);
		wsaaChdrStatusFlag.set("N");
		while ( !(isEQ(wsaaStatCount, 12))) {
			wsaaStatCount.add(1);
			if ((isEQ(wsaaChdrmnaStatcode, wsaaT5679CnriskStat[wsaaStatCount.toInt()]))) {
				wsaaStatCount.set(12);
				wsaaChdrStatusFlag.set("Y");
			}
		}

		if ((isEQ(wsaaChdrStatusFlag, "N"))) {
			/*    invalid contract status so ignore contract/coverage and*/
			/*    we'll go and get next coverage record from SQL*/
			chdrEndFlag = true;
			wsaaProcessingStatus = "N";
			return ;
		}
		/*    get here so contract  status ok - now check PREM status*/
		wsaaChdrStatusFlag.set("N");
		wsaaStatCount.set(ZERO);
		while ( !(isEQ(wsaaStatCount, 12))) {
			wsaaStatCount.add(1);
			if ((isEQ(wsaaChdrmnaPstatcode, wsaaT5679CnpremStat[wsaaStatCount.toInt()]))) {
				wsaaStatCount.set(12);
				wsaaChdrStatusFlag.set("Y");
				wsaaProcessingStatus = "Y";
				chdrEndFlag = true;
				return ;
			}
		}

		wsaaProcessingStatus = "N";
	}

	/**
	* <pre>
	*2000-READ-T6639-T6640-T6634 SECTION.
	* </pre>
	*/
protected void readT6639T6640Tr3842000()
	{
		readT66402010();
		readT66392020();
		readTr3842030();
	}

protected void readT66402010(){
	String keyItemitem = b5018DTO.getCrtable().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t6640ListMap.containsKey(keyItemitem)){	
		itempfList = t6640ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(runparmrec.effdate.toString()) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(runparmrec.effdate.toString()) <= Integer.parseInt(itempf.getItmto().toString())){
					t6640rec.t6640Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					wsaaLcrider.set(t6640rec.lcrider);
					wsaaLcsubr.set(t6640rec.lcsubr);
					wsaaBonusMethod.set(t6640rec.revBonusMeth);				
					itemFound = true;
				}
			}else{
				t6640rec.t6640Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				wsaaLcrider.set(t6640rec.lcrider);
				wsaaLcsubr.set(t6640rec.lcsubr);
				wsaaBonusMethod.set(t6640rec.revBonusMeth);
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		checkWhichTable2100();		
	}	
}

protected void readT66392020(){
	/* MOVE T6640-REV-BONUS-METH   TO WSAA-T6640-BONUS.             */
	wsaaT6640Bonus.set(wsaaBonusMethod);
	wsaaPremStat.set(b5018DTO.getPstatcode());
	
	String keyItemitem = wsaaT6639Key.toString().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t6639ListMap.containsKey(keyItemitem)){	
		itempfList = t6639ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(runparmrec.effdate.toString()) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(runparmrec.effdate.toString()) <= Integer.parseInt(itempf.getItmto().toString())){
					t6639rec.t6639Rec.set(StringUtil.rawToString(itempf.getGenarea()));			
					itemFound = true;
				}
			}else{
				t6639rec.t6639Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		conerrrec.statuz.set(g408);
		databaseError006();		
	}				
}

	/**
	* <pre>
	*2030-READ-T6634.
	* </pre>
	*/
protected void readTr3842030(){
	/* build entry key to T6634 from contract type & transaction code*/
	/* - must find out length of contract type first*/
	if ((isEQ(wsaaContractTypeInner.wsaaSecondChar, SPACES))) {
		/*     MOVE WSAA-CNTTYPE-1     TO WSAA-T6634-CNTLEN1            */
		/*     MOVE PARM-TRANSCODE     TO WSAA-T6634-TCODE-1            */
		wsaaTr384KeyInner.wsaaTr384Cntlen1.set(wsaaContractTypeInner.wsaaCnttype1);
		wsaaTr384KeyInner.wsaaTr384Tcode1.set(runparmrec.transcode);
	}
	else {
		if ((isEQ(wsaaContractTypeInner.wsaaThirdChar, SPACES))) {
			/*     MOVE WSAA-CNTTYPE-2     TO WSAA-T6634-CNTLEN2            */
			/*     MOVE PARM-TRANSCODE     TO WSAA-T6634-TCODE-2            */
			wsaaTr384KeyInner.wsaaTr384Cntlen2.set(wsaaContractTypeInner.wsaaCnttype2);
			wsaaTr384KeyInner.wsaaTr384Tcode2.set(runparmrec.transcode);
		}
		else {
			if ((isEQ(wsaaContractTypeInner.wsaaFourthChar, SPACES))) {
				/*     MOVE WSAA-CNTTYPE-3     TO WSAA-T6634-CNTLEN3            */
				/*     MOVE PARM-TRANSCODE     TO WSAA-T6634-TCODE-3            */
				wsaaTr384KeyInner.wsaaTr384Cntlen3.set(wsaaContractTypeInner.wsaaCnttype3);
				wsaaTr384KeyInner.wsaaTr384Tcode3.set(runparmrec.transcode);
			}
			else {
				/*     MOVE WSAA-CNTTYPE       TO WSAA-T6634-CNTLEN4            */
				/*     MOVE PARM-TRANSCODE     TO WSAA-T6634-TCODE-4            */
				wsaaTr384KeyInner.wsaaTr384Cntlen4.set(wsaaContractTypeInner.wsaaCnttype);
				wsaaTr384KeyInner.wsaaTr384Tcode4.set(runparmrec.transcode);
			}
		}
	}
	
	String keyItemitem = wsaaTr384KeyInner.wsaaTr384Key.trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (tr384ListMap.containsKey(keyItemitem)){	
		itempfList = tr384ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			tr384rec.tr384Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			itemFound = true;					
		}		
	}
	if (!itemFound) {
		conerrrec.statuz.set(g437);
		databaseError006();		
	}
}

protected void checkWhichTable2100(){

	String keyItemitem = b5018DTO.getCrtable().trim();/* IJTI-1523 */
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t6625ListMap.containsKey(keyItemitem)){	
		itempfList = t6625ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(runparmrec.effdate.toString()) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(runparmrec.effdate.toString()) <= Integer.parseInt(itempf.getItmto().toString())){
					t6625rec.t6625Rec.set(itempfList.get(0).getGenarea());
					wsaaLcrider.set(SPACES);
					wsaaLcsubr.set(SPACES);
					wsaaBonusMethod.set(t6625rec.revBonusMeth);			
					itemFound = true;
				}
			}else{
				t6625rec.t6625Rec.set(itempfList.get(0).getGenarea());
				wsaaLcrider.set(SPACES);
				wsaaLcsubr.set(SPACES);
				wsaaBonusMethod.set(t6625rec.revBonusMeth);
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		conerrrec.statuz.set(h149);
		databaseError006();		
	}
}


protected void callBonusSubroutine3000()
	{
		bonusrec.bonusRec.set(SPACES);
		bonusrec.rvBonusSa.set(ZERO);
		bonusrec.trmBonusSa.set(ZERO);
		bonusrec.intBonusSa.set(ZERO);
		bonusrec.extBonusSa.set(ZERO);
		bonusrec.rvBonusBon.set(ZERO);
		bonusrec.trmBonusBon.set(ZERO);
		bonusrec.intBonusBon.set(ZERO);
		bonusrec.rvBalLy.set(ZERO);
		bonusrec.extBonusBon.set(ZERO);
		bonusrec.batckey.set(batcdorrec.batchkey);
		bonusrec.effectiveDate.set(p5018DclDate);
		bonusrec.unitStmtDate.set(b5018DTO.getCbunst());
		bonusrec.language.set(runparmrec.language);
		bonusrec.transcd.set(runparmrec.transcode);
		bonusrec.tranno.set(wsaaTranno);
		bonusrec.cntcurr.set(wsaaCntcurr);
		bonusrec.cnttype.set(wsaaContractTypeInner.wsaaCnttype);
		bonusrec.chdrChdrcoy.set(b5018DTO.getChdrcoy());
		bonusrec.chdrChdrnum.set(b5018DTO.getChdrnum());
		bonusrec.lifeLife.set(b5018DTO.getLife());
		bonusrec.covrCoverage.set(b5018DTO.getCoverage());
		bonusrec.covrRider.set(b5018DTO.getRider());
		bonusrec.plnsfx.set(b5018DTO.getPlnsfx());
		bonusrec.premStatus.set(b5018DTO.getPstatcode());
		bonusrec.crtable.set(b5018DTO.getCrtable());
		bonusrec.sumin.set(b5018DTO.getSumins());
		bonusrec.allocMethod.set("C");
		/* MOVE T6640-REV-BONUS-METH   TO BONS-BONUS-CALC-METH.         */
		bonusrec.bonusCalcMeth.set(wsaaBonusMethod);
		bonusrec.user.set(runparmrec.user);
		bonusrec.unitStmtDate.set(b5018DTO.getCbunst());
		bonusrec.bonusPeriod.set(p5018BonusPeriod);
		/* MOVE T6640-LCRIDER          TO BONS-LOW-COST-RIDER.          */
		bonusrec.lowCostRider.set(wsaaLcrider);
		/* MOVE T6640-LCSUBR           TO BONS-LOW-COST-SUBR.           */
		bonusrec.lowCostSubr.set(wsaaLcsubr);
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(b5018DTO.getCrrcd());
		datcon3rec.intDate2.set(p5018DclDate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, "****")) {
			conerrrec.params.set(datcon3rec.datcon3Rec);
			databaseError006();
		}
		bonusrec.termInForce.set(datcon3rec.freqFactor);
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(b5018DTO.getCrrcd());
		datcon3rec.intDate2.set(b5018DTO.getRcesdte());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, "****")) {
			conerrrec.params.set(datcon3rec.datcon3Rec);
			databaseError006();
		}
		bonusrec.duration.set(datcon3rec.freqFactor);
		callProgram(t6639rec.revBonusProg, bonusrec.bonusRec);
		if (isNE(bonusrec.statuz, varcom.oK)) {
			 conerrrec.params.set(bonusrec.bonusRec);
			 databaseError006();
		}
	}

protected void writePtrn4000(){
    if (ptrnpf!=null && b5018DTO.getChdrnum().equals(ptrnpf.getChdrnum())) {
        return;
    }
	writePtrn4020();
	updateBatchHeader4030();
}


protected void writePtrn4020(){
	ptrnpf = new Ptrnpf();
	ptrnpf.setChdrcoy(b5018DTO.getChdrcoy());
	ptrnpf.setChdrpfx(batcdorrec.prefix.toString());
	ptrnpf.setChdrnum(b5018DTO.getChdrnum());
	ptrnpf.setTranno(wsaaTranno);
	ptrnpf.setTrdt(wsaaTransactionDate.toInt());
	ptrnpf.setTrtm(Integer.parseInt(varcom.vrcmTime.toString()));
	ptrnpf.setPtrneff(Integer.parseInt(runparmrec.effdate.toString()));
	ptrnpf.setUserT(Integer.parseInt(runparmrec.user.toString()));
	ptrnpf.setBatccoy(runparmrec.company.toString());
	ptrnpf.setBatcbrn(runparmrec.batcbranch.toString());
	ptrnpf.setBatcactyr(Integer.parseInt(runparmrec.acctyear.toString()));
	ptrnpf.setBatctrcde(runparmrec.transcode.toString());
	ptrnpf.setBatcactmn(Integer.parseInt(runparmrec.acctmonth.toString()));
	ptrnpf.setBatcbatch(batcdorrec.batch.toString());
	ptrnpf.setDatesub(runparmrec.effdate.toInt());
	if(ptrnpfList == null){
		ptrnpfList = new ArrayList<Ptrnpf>();
	}
	ptrnpfList.add(ptrnpf);
}

protected void updateBatchHeader4030()
	{
		batcuprec.batcupRec.set(SPACES);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.batcpfx.set(batcdorrec.prefix);
		batcuprec.batccoy.set(runparmrec.company);
		batcuprec.batcbrn.set(runparmrec.batcbranch);
		batcuprec.batcactyr.set(runparmrec.acctyear);
		batcuprec.batctrcde.set(runparmrec.transcode);
		batcuprec.batcactmn.set(runparmrec.acctmonth);
		batcuprec.batcbatch.set(batcdorrec.batch);
		batcuprec.function.set("WRITS");
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			conerrrec.params.set(batcuprec.batcupRec);
			conerrrec.statuz.set(batcuprec.statuz);
			systemError005();
		}
	}

protected void writeLetcBons5000(){
		/* first call DATCON2 to get date for when bonus is paid to*/

	datcon2rec.datcon2Rec.set(SPACES);
	datcon2rec.intDate1.set(p5018DclDate);
	datcon2rec.frequency.set("DY");
	datcon2rec.freqFactor.set(-1);
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz, varcom.oK)) {
		conerrrec.params.set(datcon2rec.datcon2Rec);
		databaseError006();
	}

	b5018UpdateDTOList = bonspfDAO.searchBonsPfResult(b5018DTO.getChdrcoy(), b5018DTO.getChdrnum(), b5018DTO.getLife(), b5018DTO.getCoverage()
			, b5018DTO.getRider(), b5018DTO.getPlnsfx());

	if(b5018UpdateDTOList != null && b5018UpdateDTOList.size() > 0){
		bonspf = b5018UpdateDTOList.get(0).getBonspf();
		bonlpf = b5018UpdateDTOList.get(0).getBonlpf();

        for (B5018ForUpdateDTO b5018ForUpdateDTO : b5018UpdateDTOList) {
            if (b5018DTO.getLife().equals(b5018ForUpdateDTO.getBonspf().getLife()) && b5018DTO.getRider().equals(b5018ForUpdateDTO.getBonspf().getRider())
                    && b5018DTO.getPlnsfx() == b5018ForUpdateDTO.getBonspf().getPlanSuffix()
                    && b5018DTO.getCoverage().equals(b5018ForUpdateDTO.getBonspf().getCoverage())) {
        		bonspf = b5018ForUpdateDTO.getBonspf();
        		bonlpf = b5018ForUpdateDTO.getBonlpf();
                rewrt5020(bonspf.getUniqueNumber());
            }
        }
	}else{
		writeRecord5030();
		writeRecord6030(); // ILIFE-7630
		contot5040();
		writeLetcRecord5050();
	}
}

protected void rewrt5020(long unique_number){
	Bonspf bonspfforupdate = new Bonspf();
	bonspfforupdate.setTermid(varcom.vrcmTermid.toString());
	bonspfforupdate.setUser(Integer.parseInt(runparmrec.user.toString()));
	bonspfforupdate.setTransactionDate(Integer.parseInt(varcom.vrcmDate.toString()));
	bonspfforupdate.setTransactionTime(Integer.parseInt(varcom.vrcmTime.toString()));
	bonspfforupdate.setValidflag("2");
	bonspfforupdate.setCurrto(Integer.parseInt(datcon2rec.intDate2.toString()));
	bonspfforupdate.setUniqueNumber(unique_number);
    if(bonsrvbTotalUpdateList==null){
    	bonsrvbTotalUpdateList = new ArrayList<Bonspf>();
    }
	bonsrvbTotalUpdateList.add(bonspf);
	if(ausCtxFlag) {
		writeRecord5030();
		writeRecord6030();
	}
}

protected void writeRecord5030()
	{
	BigDecimal totalBonus = BigDecimal.ZERO;
	if(ausCtxFlag && bonspf != null) {
		totalBonus = bonspf.getTotalBonus();
	}
	bonspf = new Bonspf();
    bonspf.setBonPayLastYr(BigDecimal.ZERO);
    bonspf.setBonPayThisYr(BigDecimal.ZERO);
    bonspf.setTotalBonus(BigDecimal.ZERO);
    bonspf.setChdrcoy(b5018DTO.getChdrcoy());
    bonspf.setChdrnum(b5018DTO.getChdrnum());
    bonspf.setLife(b5018DTO.getLife());
    bonspf.setCoverage(b5018DTO.getCoverage());
    bonspf.setRider(b5018DTO.getRider());
    bonspf.setPlanSuffix(b5018DTO.getPlnsfx());
	bonspf.setValidflag("1");
	bonspf.setCurrfrom(Integer.parseInt(p5018DclDate.toString()));
	bonspf.setCurrto(Integer.parseInt(varcom.vrcmMaxDate.toString()));
	bonspf.setBonPayLastYr(bonspf.getTotalBonus());
	setPrecision(bonspf.getBonPayThisYr(), 2);
	
	bonspf.setBonPayThisYr(bonusrec.rvBonusSa.getbigdata().add(bonusrec.rvBonusBon.getbigdata()));
		/* MOVE BONSRVB-BON-PAY-THIS-YR TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 8000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO BONSRVB-BON-PAY-THIS-YR.     */
	if (isNE(bonspf.getBonPayThisYr(), 0)) {
		zrdecplrec.amountIn.set(bonspf.getBonPayThisYr());
		callRounding8000();
		bonspf.setBonPayThisYr(zrdecplrec.amountOut.getbigdata());
	}
	setPrecision(bonspf.getTotalBonus(), 2);
	if(ausCtxFlag) {
		bonspf.setTotalBonus(totalBonus.add(bonspf.getBonPayThisYr()));
	}
	else {
		bonspf.setTotalBonus(bonspf.getTotalBonus().add(bonspf.getBonPayThisYr()));
	}
	bonspf.setBonusCalcMeth(t6640rec.revBonusMeth.toString());
	bonspf.setTermid(varcom.vrcmTermid.toString());
	bonspf.setUser(Integer.parseInt(runparmrec.user.toString()));
	bonspf.setTransactionDate(Integer.parseInt(varcom.vrcmDate.toString()));
	bonspf.setTransactionTime(Integer.parseInt(varcom.vrcmTime.toString()));
	if(bonsrvbTotalInsertList == null){
		bonsrvbTotalInsertList = new ArrayList<Bonspf>();
	}
	bonsrvbTotalInsertList.add(bonspf);


}

protected void contot5040()
	{
		countCt02 = countCt02.add(bonspf.getBonPayThisYr());
		/* Decide whether component we are processing has changed or not*/
		wsaaCurrentComp.set(SPACES);
		wsaaCurrentCover.set(b5018DTO.getCoverage());
		wsaaCurrentRider.set(b5018DTO.getRider());
		count = 0;
		if ((isNE(wsaaCurrentComp, wsaaPreviousComp))) {
			wsaaPreviousComp.set(wsaaCurrentComp);
			wsaaBonlrvbBcalmeth[count] = bonspf.getBonusCalcMeth();
			/*        get component description to be added to BONLRVB record*/
			getT5687Desc5500();
			count++;
		}
		/* Set up values for Reversionary Bonus Letters*/
		wsaaBonlrvbSumins[count].add(b5018DTO.getSumins());
		wsaaBonlrvbBpayty[count].add(bonspf.getBonPayThisYr());
		wsaaBonlrvbBpayny[count].add(bonspf.getBonPayLastYr());
		wsaaBonlrvbTotbon[count].add(bonspf.getTotalBonus());
	}

	/**
	* <pre>
	* Write LETC record
	* </pre>
	*/
protected void writeLetcRecord5050()
	{
		/* IF LETRQST-OTHER-KEYS       = CHDRNUM                        */
		if (isEQ(letrqstrec.rdocnum, b5018DTO.getChdrnum())) {
			return ;
		}
		letrqstrec = new Letrqstrec();
		letrqstrec.function.set("ADD");
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(b5018DTO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO  LETRQST-LETTER-TYPE.         */
		letrqstrec.letterType.set(tr384rec.letterType);
		/* MOVE P5018-DCL-DATE         TO  LETRQST-LETTER-REQUEST-DATE. */
		letrqstrec.letterRequestDate.set(wsaaToday);
		letrqstrec.clntcoy.set(wsaaCowncoy);
		letrqstrec.clntnum.set(wsaaCownnum);
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(b5018DTO.getChdrcoy());
		letrqstrec.chdrcoy.set(b5018DTO.getChdrcoy());
		letrqstrec.rdocnum.set(b5018DTO.getChdrnum());
		letrqstrec.chdrnum.set(b5018DTO.getChdrnum());
		letrqstrec.tranno.set(wsaaTranno);
		letrqstrec.branch.set(b5018DTO.getCntbranch());
		/* MOVE CHDRNUM                TO  LETRQST-OTHER-KEYS.          */
		/*ILIFE-3274 Starts */
		letrqstList.add(letrqstrec);
		/*ILIFE-3274 Ends */
		
	}

protected void getT5687Desc5500(){

		/* Read table T5687 to get Component description.*/

		
        String descKey = b5018DTO.getCrtable().trim(); 
        if(t5687Map.containsKey(descKey)){
            Descpf descpf = t5687Map.get(descKey);
            wsaaBonlrvbLongdesc[count] = descpf.getLongdesc();/* IJTI-1523 */
            return;
        }
        conerrrec.statuz.set(e402);
        databaseError006();
	}

protected void writeBonlrvbRec6000(){

	if(bonlpf != null){
		rewrt6020(bonlpf.getUniqueNumer());
	}else{
		writeRecord6030();
	}
}

protected void rewrt6020(long unique_number){
		/* Record exists so VALIDFLAG '2' it so we can write a new one*/
	Bonlpf bonlpfforupdate = new Bonlpf();
	bonlpfforupdate.setUniqueNumer(unique_number);
	bonlpfforupdate.setTermid(varcom.vrcmTermid.toString());
	bonlpfforupdate.setUser(Integer.parseInt(runparmrec.user.toString()));
	bonlpfforupdate.setTransactionDate(Integer.parseInt(varcom.vrcmDate.toString()));
	bonlpfforupdate.setTransactionTime(Integer.parseInt(varcom.vrcmTime.toString()));
	bonlpfforupdate.setValidflag("2");
	bonlpfforupdate.setCurrto(Integer.parseInt(datcon2rec.intDate2.toString()));
    if(bonlrvbTotalUpdateList==null){
    	bonlrvbTotalUpdateList = new ArrayList<Bonlpf>();
    }
	bonlrvbTotalUpdateList.add(bonlpfforupdate);
	
}

protected void writeRecord6030(){
	Bonlpf bonlpfforinsert = new Bonlpf();
    bonlpfforinsert.setChdrcoy(bonspf.getChdrcoy());
    bonlpfforinsert.setChdrnum(bonspf.getChdrnum());
	bonlpfforinsert.setValidflag("1");
	bonlpfforinsert.setCurrfrom(Integer.parseInt(p5018DclDate.toString()));
	bonlpfforinsert.setCurrto(Integer.parseInt(varcom.vrcmMaxDate.toString()));
	int wsaaCount = 0;
	count = 0;
    while (wsaaCount != 10 && wsaaCount < wsaaBonlrvbLongdesc.length) {
		wsaaCount++;
		bonlpfforinsert.setLongdesc(wsaaCount, wsaaBonlrvbLongdesc[wsaaCount - 1]);
		bonlpfforinsert.setSumins(wsaaCount, wsaaBonlrvbSumins[wsaaCount - 1]);
		bonlpfforinsert.setBcalmeth(wsaaCount, wsaaBonlrvbBcalmeth[wsaaCount - 1]);
		bonlpfforinsert.setBpayty(wsaaCount,wsaaBonlrvbBpayty[wsaaCount - 1]);
		bonlpfforinsert.setBpayny(wsaaCount, wsaaBonlrvbBpayny[wsaaCount - 1]);
		bonlpfforinsert.setTotbon(wsaaCount, wsaaBonlrvbTotbon[wsaaCount - 1]);
	}
	/* get bonus-to date in text form by calling DATCON6*/
	datcon6rec.datcon6Rec.set(SPACES);
	datcon6rec.language.set(runparmrec.language);
	datcon6rec.company.set(runparmrec.company);
	datcon6rec.intDate1.set(datcon2rec.intDate2);
	callProgram(Datcon6.class, datcon6rec.datcon6Rec);
	if (isNE(datcon6rec.statuz, varcom.oK)) {
		conerrrec.params.set(datcon6rec.datcon6Rec);
		databaseError006();
	}
	/* MOVE DTC6-INT-DATE-2          TO BONLRVB-DATETEXT.           */
	bonlpfforinsert.setDatetexc(datcon6rec.intDate2.toString());
	bonlpfforinsert.setTermid(bonspf.getTermid());
	bonlpfforinsert.setUser(bonspf.getUser());
	bonlpfforinsert.setTransactionDate(bonspf.getTransactionDate());
	bonlpfforinsert.setTransactionTime(bonspf.getTransactionTime());
	bonlpfforinsert.setUserProfile(bonspf.getUserProfile());
    if(bonlrvbTotalInsertList==null){
    	bonlrvbTotalInsertList = new ArrayList<Bonlpf>();
    }
	bonlrvbTotalInsertList.add(bonlpfforinsert);
	

	/* re-initialise all variables ready for next contract*/
	wsaaBonlrvbVars.set(SPACES);
	wsaaPreviousComp.set(SPACES);
	count = 0;
	while ( !(isEQ(count, 10))) {
		wsaaBonlrvbSumins[count] = new BigDecimal(0);
		wsaaBonlrvbBpayty[count] = new BigDecimal(0);
		wsaaBonlrvbBpayny[count] = new BigDecimal(0);
		wsaaBonlrvbTotbon[count] = new BigDecimal(0);
		count++;
	}

	count = 0;
	
	
}

protected void commit3500(){
	
	if(chdrpfList != null && chdrpfList.size() > 0 ){
		chdrDAO.updateChdrTranno(chdrpfList);
		chdrpfList.clear();
		chdrpfList = null;
	}
	
	if(ptrnpfList != null && ptrnpfList.size() > 0 ){
		ptrnpfDAO.insertPtrnPF(ptrnpfList);
		ptrnpfList.clear();
		ptrnpfList = null;
	}
	if(bonsrvbTotalUpdateList != null && bonsrvbTotalUpdateList.size() > 0){
		bonspfDAO.updateBonspf(bonsrvbTotalUpdateList);
		bonsrvbTotalUpdateList.clear();
		bonsrvbTotalUpdateList = null;
	}

	if(bonsrvbTotalInsertList != null && bonsrvbTotalInsertList.size() > 0){
		bonspfDAO.insertBonspf(bonsrvbTotalInsertList);
		bonsrvbTotalInsertList.clear();
		bonsrvbTotalInsertList = null;
	}
	
	if(bonlrvbTotalUpdateList != null && bonlrvbTotalUpdateList.size() > 0 ){
		bonlpfDAO.updateBonlpf(bonlrvbTotalUpdateList);
		bonlrvbTotalUpdateList.clear();
		bonlrvbTotalUpdateList = null;
	}
	
	if(bonlrvbTotalInsertList != null && bonlrvbTotalInsertList.size() > 0){
		bonlpfDAO.insertBonlpf(bonlrvbTotalInsertList);
		bonlrvbTotalInsertList.clear();
		bonlrvbTotalInsertList = null;
	}
	/*ILIFE-3274 Starts */
	for(Letrqstrec letrqstrec : letrqstList){
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			conerrrec.statuz.set(letrqstrec.statuz);
			databaseError006();
		}
	}
	/*ILIFE-3274 Ends */
    if (countCt01 > 0) {
        contotrec.totno.set(ct01);
        contotrec.totval.set(countCt01);
        countCt01 = 0;
        callContot001();
    }
    if (countCt02.compareTo(BigDecimal.ZERO) > 0) {
        contotrec.totno.set(ct02);
        contotrec.totval.set(countCt02);
        countCt02 = BigDecimal.ZERO;
        callContot001();
    }
    if (countCt03 > 0) {
        contotrec.totno.set(ct03);
        contotrec.totval.set(countCt03);
        countCt03 = 0;
        callContot001();
    }
}

protected void unlockSoftlock7000()
	{
		start7010();
	}

protected void start7010()
	{
		sftlockrec1.sftlockRec.set(SPACES);
		sftlockrec1.company.set(wsaaParmCompany);
		sftlockrec1.entity.set(wsaaLastChdrnum);
		sftlockrec1.enttyp.set("CH");
		sftlockrec1.user.set(runparmrec.user);
		sftlockrec1.transaction.set(runparmrec.transcode);
		sftlockrec1.statuz.set(SPACES);
		sftlockrec1.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec1.sftlockRec);
		if (isNE(sftlockrec1.statuz, varcom.oK)) {
			conerrrec.params.set(sftlockrec1.sftlockRec);
			conerrrec.statuz.set(sftlockrec1.statuz);
			systemError005();
		}
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(runparmrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(wsaaCntcurr);
		zrdecplrec.batctrcde.set(runparmrec.transcode);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			conerrrec.statuz.set(zrdecplrec.statuz);
			conerrrec.params.set(zrdecplrec.zrdecplRec);
			systemError005();
		}
		/*EIXT*/
	}

protected void sqlError()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		conerrrec.syserror.set(sqlStatuz);
		systemError005();
		/*EXIT-SQL-ERROR*/
	}
/*
 * Class transformed  from Data Structure WSAA-TR384-KEY--INNER
 */
private static final class WsaaTr384KeyInner {

		/*01  WSAA-T6634-KEY.
		  03  WSAA-T6634-KEY1 REDEFINES WSAA-T6634-ENTRY.
		  03  WSAA-T6634-KEY2 REDEFINES WSAA-T6634-ENTRY.
		  03  WSAA-T6634-KEY3 REDEFINES WSAA-T6634-ENTRY.
		  03  WSAA-T6634-KEY4 REDEFINES WSAA-T6634-ENTRY.                */
	private FixedLengthStringData wsaaTr384Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr384Entry = new FixedLengthStringData(8).isAPartOf(wsaaTr384Key, 0);
	private FixedLengthStringData wsaaTr384Key1 = new FixedLengthStringData(8).isAPartOf(wsaaTr384Entry, 0, REDEFINE);
	private FixedLengthStringData wsaaTr384Cntlen1 = new FixedLengthStringData(1).isAPartOf(wsaaTr384Key1, 0);
	private FixedLengthStringData wsaaTr384Tcode1 = new FixedLengthStringData(7).isAPartOf(wsaaTr384Key1, 1);
	private FixedLengthStringData wsaaTr384Key2 = new FixedLengthStringData(8).isAPartOf(wsaaTr384Entry, 0, REDEFINE);
	private FixedLengthStringData wsaaTr384Cntlen2 = new FixedLengthStringData(2).isAPartOf(wsaaTr384Key2, 0);
	private FixedLengthStringData wsaaTr384Tcode2 = new FixedLengthStringData(6).isAPartOf(wsaaTr384Key2, 2);
	private FixedLengthStringData wsaaTr384Key3 = new FixedLengthStringData(8).isAPartOf(wsaaTr384Entry, 0, REDEFINE);
	private FixedLengthStringData wsaaTr384Cntlen3 = new FixedLengthStringData(3).isAPartOf(wsaaTr384Key3, 0);
	private FixedLengthStringData wsaaTr384Tcode3 = new FixedLengthStringData(5).isAPartOf(wsaaTr384Key3, 3);
	private FixedLengthStringData wsaaTr384Key4 = new FixedLengthStringData(8).isAPartOf(wsaaTr384Entry, 0, REDEFINE);
	private FixedLengthStringData wsaaTr384Cntlen4 = new FixedLengthStringData(4).isAPartOf(wsaaTr384Key4, 0);
	private FixedLengthStringData wsaaTr384Tcode4 = new FixedLengthStringData(4).isAPartOf(wsaaTr384Key4, 4);
}
/*
 * Class transformed  from Data Structure WSAA-CONTRACT-TYPE--INNER
 */
private static final class WsaaContractTypeInner {
		/* WSAA-CONTRACT-TYPE */
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaCtypeSplit = new FixedLengthStringData(4).isAPartOf(wsaaCnttype, 0, REDEFINE);
	private FixedLengthStringData wsaaSecondChar = new FixedLengthStringData(1).isAPartOf(wsaaCtypeSplit, 1);
	private FixedLengthStringData wsaaThirdChar = new FixedLengthStringData(1).isAPartOf(wsaaCtypeSplit, 2);
	private FixedLengthStringData wsaaFourthChar = new FixedLengthStringData(1).isAPartOf(wsaaCtypeSplit, 3);

	private FixedLengthStringData wsaaOneLetter = new FixedLengthStringData(4).isAPartOf(wsaaCnttype, 0, REDEFINE);
	private FixedLengthStringData wsaaCnttype1 = new FixedLengthStringData(1).isAPartOf(wsaaOneLetter, 0);

	private FixedLengthStringData wsaaTwoLetters = new FixedLengthStringData(4).isAPartOf(wsaaCnttype, 0, REDEFINE);
	private FixedLengthStringData wsaaCnttype2 = new FixedLengthStringData(2).isAPartOf(wsaaTwoLetters, 0);

	private FixedLengthStringData wsaaThreeLetters = new FixedLengthStringData(4).isAPartOf(wsaaCnttype, 0, REDEFINE);
	private FixedLengthStringData wsaaCnttype3 = new FixedLengthStringData(3).isAPartOf(wsaaThreeLetters, 0);
}
/*
 * Class transformed  from Data Structure COVRPF--INNER
 */
private static final class CovrpfInner {

		/* COVRPF */
	private FixedLengthStringData covrrec1 = new FixedLengthStringData(61);
	private FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(covrrec1, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(covrrec1, 1);
	private FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(covrrec1, 9);
	private FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(covrrec1, 11);
	private FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(covrrec1, 13);
	private PackedDecimalData plnsfx = new PackedDecimalData(4, 0).isAPartOf(covrrec1, 15);
	private FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(covrrec1, 18);
	private FixedLengthStringData pstatcode = new FixedLengthStringData(2).isAPartOf(covrrec1, 22);
	private FixedLengthStringData statcode = new FixedLengthStringData(2).isAPartOf(covrrec1, 24);
	private PackedDecimalData crrcd = new PackedDecimalData(8, 0).isAPartOf(covrrec1, 26);
	private PackedDecimalData cbunst = new PackedDecimalData(8, 0).isAPartOf(covrrec1, 31);
	private PackedDecimalData rcesdte = new PackedDecimalData(8, 0).isAPartOf(covrrec1, 36);
	private PackedDecimalData sumins = new PackedDecimalData(17, 2).isAPartOf(covrrec1, 41);
	private FixedLengthStringData validflag = new FixedLengthStringData(1).isAPartOf(covrrec1, 50);
	private PackedDecimalData currfrom = new PackedDecimalData(8, 0).isAPartOf(covrrec1, 51);
	private PackedDecimalData currto = new PackedDecimalData(8, 0).isAPartOf(covrrec1, 56);
}
/*
 * Class transformed  from Data Structure T5679-T5679-REC--INNER
 */
private static final class T5679T5679RecInner {

	private FixedLengthStringData t5679T5679Rec = new FixedLengthStringData(500);
	private FixedLengthStringData t5679CnPremStats = new FixedLengthStringData(24).isAPartOf(t5679T5679Rec, 0);
	private FixedLengthStringData t5679CnPremStat01 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 0);
	private FixedLengthStringData t5679CnPremStat02 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 2);
	private FixedLengthStringData t5679CnPremStat03 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 4);
	private FixedLengthStringData t5679CnPremStat04 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 6);
	private FixedLengthStringData t5679CnPremStat05 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 8);
	private FixedLengthStringData t5679CnPremStat06 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 10);
	private FixedLengthStringData t5679CnPremStat07 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 12);
	private FixedLengthStringData t5679CnPremStat08 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 14);
	private FixedLengthStringData t5679CnPremStat09 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 16);
	private FixedLengthStringData t5679CnPremStat10 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 18);
	private FixedLengthStringData t5679CnPremStat11 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 20);
	private FixedLengthStringData t5679CnPremStat12 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 22);
	private FixedLengthStringData t5679CnRiskStats = new FixedLengthStringData(24).isAPartOf(t5679T5679Rec, 24);
	private FixedLengthStringData t5679CnRiskStat01 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 0);
	private FixedLengthStringData t5679CnRiskStat02 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 2);
	private FixedLengthStringData t5679CnRiskStat03 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 4);
	private FixedLengthStringData t5679CnRiskStat04 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 6);
	private FixedLengthStringData t5679CnRiskStat05 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 8);
	private FixedLengthStringData t5679CnRiskStat06 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 10);
	private FixedLengthStringData t5679CnRiskStat07 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 12);
	private FixedLengthStringData t5679CnRiskStat08 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 14);
	private FixedLengthStringData t5679CnRiskStat09 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 16);
	private FixedLengthStringData t5679CnRiskStat10 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 18);
	private FixedLengthStringData t5679CnRiskStat11 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 20);
	private FixedLengthStringData t5679CnRiskStat12 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 22);
	private FixedLengthStringData t5679CovPremStats = new FixedLengthStringData(24).isAPartOf(t5679T5679Rec, 48);
	private FixedLengthStringData t5679CovPremStat01 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 0);
	private FixedLengthStringData t5679CovPremStat02 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 2);
	private FixedLengthStringData t5679CovPremStat03 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 4);
	private FixedLengthStringData t5679CovPremStat04 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 6);
	private FixedLengthStringData t5679CovPremStat05 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 8);
	private FixedLengthStringData t5679CovPremStat06 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 10);
	private FixedLengthStringData t5679CovPremStat07 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 12);
	private FixedLengthStringData t5679CovPremStat08 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 14);
	private FixedLengthStringData t5679CovPremStat09 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 16);
	private FixedLengthStringData t5679CovPremStat10 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 18);
	private FixedLengthStringData t5679CovPremStat11 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 20);
	private FixedLengthStringData t5679CovPremStat12 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 22);
	private FixedLengthStringData t5679CovRiskStats = new FixedLengthStringData(24).isAPartOf(t5679T5679Rec, 72);
	private FixedLengthStringData t5679CovRiskStat01 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 0);
	private FixedLengthStringData t5679CovRiskStat02 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 2);
	private FixedLengthStringData t5679CovRiskStat03 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 4);
	private FixedLengthStringData t5679CovRiskStat04 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 6);
	private FixedLengthStringData t5679CovRiskStat05 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 8);
	private FixedLengthStringData t5679CovRiskStat06 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 10);
	private FixedLengthStringData t5679CovRiskStat07 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 12);
	private FixedLengthStringData t5679CovRiskStat08 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 14);
	private FixedLengthStringData t5679CovRiskStat09 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 16);
	private FixedLengthStringData t5679CovRiskStat10 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 18);
	private FixedLengthStringData t5679CovRiskStat11 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 20);
	private FixedLengthStringData t5679CovRiskStat12 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 22);
	private FixedLengthStringData t5679JlifeStats = new FixedLengthStringData(12).isAPartOf(t5679T5679Rec, 96);
	private FixedLengthStringData t5679JlifeStat01 = new FixedLengthStringData(2).isAPartOf(t5679JlifeStats, 0);
	private FixedLengthStringData t5679JlifeStat02 = new FixedLengthStringData(2).isAPartOf(t5679JlifeStats, 2);
	private FixedLengthStringData t5679JlifeStat03 = new FixedLengthStringData(2).isAPartOf(t5679JlifeStats, 4);
	private FixedLengthStringData t5679JlifeStat04 = new FixedLengthStringData(2).isAPartOf(t5679JlifeStats, 6);
	private FixedLengthStringData t5679JlifeStat05 = new FixedLengthStringData(2).isAPartOf(t5679JlifeStats, 8);
	private FixedLengthStringData t5679JlifeStat06 = new FixedLengthStringData(2).isAPartOf(t5679JlifeStats, 10);
	private FixedLengthStringData t5679LifeStats = new FixedLengthStringData(12).isAPartOf(t5679T5679Rec, 108);
	private FixedLengthStringData t5679LifeStat01 = new FixedLengthStringData(2).isAPartOf(t5679LifeStats, 0);
	private FixedLengthStringData t5679LifeStat02 = new FixedLengthStringData(2).isAPartOf(t5679LifeStats, 2);
	private FixedLengthStringData t5679LifeStat03 = new FixedLengthStringData(2).isAPartOf(t5679LifeStats, 4);
	private FixedLengthStringData t5679LifeStat04 = new FixedLengthStringData(2).isAPartOf(t5679LifeStats, 6);
	private FixedLengthStringData t5679LifeStat05 = new FixedLengthStringData(2).isAPartOf(t5679LifeStats, 8);
	private FixedLengthStringData t5679LifeStat06 = new FixedLengthStringData(2).isAPartOf(t5679LifeStats, 10);
	private FixedLengthStringData t5679RidPremStats = new FixedLengthStringData(24).isAPartOf(t5679T5679Rec, 120);
	private FixedLengthStringData t5679RidPremStat01 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 0);
	private FixedLengthStringData t5679RidPremStat02 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 2);
	private FixedLengthStringData t5679RidPremStat03 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 4);
	private FixedLengthStringData t5679RidPremStat04 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 6);
	private FixedLengthStringData t5679RidPremStat05 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 8);
	private FixedLengthStringData t5679RidPremStat06 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 10);
	private FixedLengthStringData t5679RidPremStat07 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 12);
	private FixedLengthStringData t5679RidPremStat08 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 14);
	private FixedLengthStringData t5679RidPremStat09 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 16);
	private FixedLengthStringData t5679RidPremStat10 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 18);
	private FixedLengthStringData t5679RidPremStat11 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 20);
	private FixedLengthStringData t5679RidPremStat12 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 22);
	private FixedLengthStringData t5679RidRiskStats = new FixedLengthStringData(24).isAPartOf(t5679T5679Rec, 144);
	private FixedLengthStringData t5679RidRiskStat01 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 0);
	private FixedLengthStringData t5679RidRiskStat02 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 2);
	private FixedLengthStringData t5679RidRiskStat03 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 4);
	private FixedLengthStringData t5679RidRiskStat04 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 6);
	private FixedLengthStringData t5679RidRiskStat05 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 8);
	private FixedLengthStringData t5679RidRiskStat06 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 10);
	private FixedLengthStringData t5679RidRiskStat07 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 12);
	private FixedLengthStringData t5679RidRiskStat08 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 14);
	private FixedLengthStringData t5679RidRiskStat09 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 16);
	private FixedLengthStringData t5679RidRiskStat10 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 18);
	private FixedLengthStringData t5679RidRiskStat11 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 20);
	private FixedLengthStringData t5679RidRiskStat12 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 22);
	private FixedLengthStringData t5679SetCnPremStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 168);
	private FixedLengthStringData t5679SetCnRiskStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 170);
	private FixedLengthStringData t5679SetCovPremStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 172);
	private FixedLengthStringData t5679SetCovRiskStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 174);
	private FixedLengthStringData t5679SetJlifeStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 176);
	private FixedLengthStringData t5679SetLifeStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 178);
	private FixedLengthStringData t5679SetRidPremStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 180);
	private FixedLengthStringData t5679SetRidRiskStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 182);
	private FixedLengthStringData t5679SetSngpCnStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 184);
	private FixedLengthStringData t5679SetSngpCovStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 186);
	private FixedLengthStringData t5679SetSngpRidStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 188);
}
}
