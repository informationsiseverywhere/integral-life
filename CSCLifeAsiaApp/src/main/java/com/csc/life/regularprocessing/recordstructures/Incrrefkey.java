package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:59
 * Description:
 * Copybook name: INCRREFKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Incrrefkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData incrrefFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData incrrefKey = new FixedLengthStringData(64).isAPartOf(incrrefFileKey, 0, REDEFINE);
  	public FixedLengthStringData incrrefChdrcoy = new FixedLengthStringData(1).isAPartOf(incrrefKey, 0);
  	public FixedLengthStringData incrrefChdrnum = new FixedLengthStringData(8).isAPartOf(incrrefKey, 1);
  	public FixedLengthStringData incrrefLife = new FixedLengthStringData(2).isAPartOf(incrrefKey, 9);
  	public FixedLengthStringData incrrefCoverage = new FixedLengthStringData(2).isAPartOf(incrrefKey, 11);
  	public FixedLengthStringData incrrefRider = new FixedLengthStringData(2).isAPartOf(incrrefKey, 13);
  	public PackedDecimalData incrrefPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(incrrefKey, 15);
  	public PackedDecimalData incrrefCrrcd = new PackedDecimalData(8, 0).isAPartOf(incrrefKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(41).isAPartOf(incrrefKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(incrrefFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		incrrefFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}