package com.csc.life.regularprocessing.dataaccess.model;

import java.math.BigDecimal;

public class Trwppf {

    private String chdrcoy;
    private String chdrnum;
    private int effdate;
    private int tranno;
    private String validflag;
    private String rdocpfx;
    private String rdoccoy;
    private String rdocnum;
    private String origcurr;
    private BigDecimal origamt;
    private String userProfile;
    private String jobName;
    private String datime;

    public String getChdrcoy() {
        return chdrcoy;
    }

    public String getChdrnum() {
        return chdrnum;
    }

    public int getEffdate() {
        return effdate;
    }

    public int getTranno() {
        return tranno;
    }

    public String getValidflag() {
        return validflag;
    }

    public String getRdocpfx() {
        return rdocpfx;
    }

    public String getRdoccoy() {
        return rdoccoy;
    }

    public String getRdocnum() {
        return rdocnum;
    }

    public String getOrigcurr() {
        return origcurr;
    }

    public BigDecimal getOrigamt() {
        return origamt;
    }

    public String getUserProfile() {
        return userProfile;
    }

    public String getJobName() {
        return jobName;
    }

    public String getDatime() {
        return datime;
    }

    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }

    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }

    public void setEffdate(int effdate) {
        this.effdate = effdate;
    }

    public void setTranno(int tranno) {
        this.tranno = tranno;
    }

    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }

    public void setRdocpfx(String rdocpfx) {
        this.rdocpfx = rdocpfx;
    }

    public void setRdoccoy(String rdoccoy) {
        this.rdoccoy = rdoccoy;
    }

    public void setRdocnum(String rdocnum) {
        this.rdocnum = rdocnum;
    }

    public void setOrigcurr(String origcurr) {
        this.origcurr = origcurr;
    }

    public void setOrigamt(BigDecimal origamt) {
        this.origamt = origamt;
    }

    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public void setDatime(String datime) {
        this.datime = datime;
    }
}