/*
 * File: Br617.java
 * Date: 29 August 2009 22:27:08
 * Author: Quipoz Limited
 * 
 * Class transformed from BR617.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.dataaccess.UstxpfTableDAM;
import com.csc.life.regularprocessing.dataaccess.UtrnrnlTableDAM;
import com.csc.life.regularprocessing.dataaccess.dao.UstxpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Ustxpf;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6659rec;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 2005.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*                   Unit Statement Trigger
*                   ----------------------
*         Rewrite of B5105 with Multi-Thread capability
*
* Overview
* ________
*
* This program is part of the new 'Multi-Threading Batch
* Performance' suite. It runs directly after BR616, which 'splits'
* the CHDRPF according to the number of Unit Statement programs to run.
* All references to the CHDR are via USTXPF - a temporary file
* holding all the CHDR records for this program to process.
*
* BR617 will perform all the processing for the selected contracts
* which are due for unit statement processing. All contracts due
* for such processing are then processed by calling the generic
* subroutine as determined by the method held in T6647.
*
*    Control totals maintained by this program are:
*
*      1. Total CHDRs read
*      2. CHDRs processed
*      3. Payment outstanding
*      4. Unit trans outstanding
*      5. Statements produced
*      6. No. CHDR invalid status
*      7. No. CHDR locked
*      8. No.CHDR not processed
*
* 1000-INITIALISE SECTION
* _______________________
*
*  -  Frequently-referenced tables are stored in working storage
*     arrays to minimize disk IO. These tables include
*     T6647 (Bank codes), T6659 (Billing channels).
*
*  -  Issue an override to read the correct USTXPF for this run.
*     The CRTTMPF process has already created a file specific to
*     the run using the  USTXPF fields and is identified  by
*     concatenating the following:-
*
*     'USTX'
*      BPRD-SYSTEM-PARAM04
*      BSSC-SCHEDULE-NUMBER
*
*      eg USTX2B0001,  for the first run
*         USTX2B0002,  for the second etc.
*
*     The number of threads would have been created by the
*     CRTTMPF process given the parameters of the process
*     definition of the CRTTMPF.
*     To get the correct member of the above physical for BR617
*     to read from, concatenate :-
*
*     'THREAD'
*     BSPR-PROCESS-OCC-NUM
*
* 2000-READ SECTION
* _________________
*
* -  Read the USTX records sequentially incrementing the control
*    total.
*
* -  If end of file move ENDP to WSSP-EDTERROR.
*
* 2500-EDIT SECTION
* _________________
*
* -  Move OK to WSSP-EDTERROR.
*
* -  Read and validate the CHDR risk status and premium status
*    against those obtained from T5679. If the status is invalid
*    add 1 to control total 6 and move SPACES to WSSP-EDTERROR.
*
* -  Check with Transaction Code + Contract Type in T6647 to
*    get the Unit Statement Method.
*
* -  Check with the T6647 Unit Statement Method in T6659 to
*    decide whether the checking of outstanding UTRN is required;
*    whether unit statement is to be produced on anniversary or
*    payment date.
*
* - 'Soft lock' the contract, if it is to be processed.
*    If the contract is already 'locked' increment control
*    total number 7 and  move SPACES to WSSP-EDTERROR.
*
*  3000-UPDATE SECTION
*  ___________________
*
*  - Read & hold the CHDR record using logical file CHDRLIF.
*
*  - Setup the parameters to call the T6659 Subroutine and
*    update the Unit Statement Date in CHDRLIF.
*
*  - Rewrite CHDRLIF
*
*  - Unlock the contract.
*
* 4000-CLOSE SECTION
* __________________
*
*  - Close Files.
*
*  - Delete the Override function for the USTXPF file
*
*   Error Processing:
*
*     Perform the 600-FATAL-ERROR section. The
*     SYSR-SYSERR-TYPE flag does not need to be set in this
*     program, because MAINB takes care of a system errors.
*
*****************************************************************
*
* </pre>
*/
public class Br617 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private UstxpfTableDAM ustxpf = new UstxpfTableDAM();
//	private UstxpfTableDAM ustxpfRec = new UstxpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR617");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		  be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaT5679Sub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaDateFound = new FixedLengthStringData(1).init("N");
	private Validator dateFound = new Validator(wsaaDateFound, "Y");

	private FixedLengthStringData wsaaUstxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaUstxFn, 0, FILLER).init("USTX");
	private FixedLengthStringData wsaaUstxRunid = new FixedLengthStringData(2).isAPartOf(wsaaUstxFn, 4);
	private ZonedDecimalData wsaaUstxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaUstxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private static final String chdrlifrec = "CHDRLIFREC";
		/* ERRORS */
	private static final String h965 = "H965";
	private static final String g029 = "G029";
	private static final String h791 = "H791";
	private static final String ivrm = "IVRM";
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t6647 = "T6647";
	private static final String t6659 = "T6659";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;
	private static final int ct07 = 7;
	private static final int ct08 = 8;


	private FixedLengthStringData wsaaT6647ItemKey = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT6647ItemTranCode = new FixedLengthStringData(4).isAPartOf(wsaaT6647ItemKey, 0);
	private FixedLengthStringData wsaaT6647ItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647ItemKey, 4);

		/* WSAA-T6647-ARRAY */
	//private FixedLengthStringData[] wsaaT6647Rec = FLSInittedArray (50, 16);
	private FixedLengthStringData[] wsaaT6647Rec = FLSInittedArray (2500, 16);	//ILIFE-6753 increased size to 2500
	private FixedLengthStringData[] wsaaT6647Key = FLSDArrayPartOfArrayStructure(7, wsaaT6647Rec, 0);
	private FixedLengthStringData[] wsaaT6647Data = FLSDArrayPartOfArrayStructure(9, wsaaT6647Rec, 7);
	private PackedDecimalData[] wsaaT6647Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT6647Data, 0);
	private FixedLengthStringData[] wsaaT6647UnitStatMethod = FLSDArrayPartOfArrayStructure(4, wsaaT6647Data, 5);
	//private static final int wsaaT6647Size = 50;
	//private static final int wsaaT6647Size = 700;//ILIFE-1985
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT6647Size = 2500;	//ILIFE-6753 increased size to 2500

		/* WSAA-T6659-ARRAY */
	private FixedLengthStringData[] wsaaT6659Rec = FLSInittedArray (1000, 23);
	private FixedLengthStringData[] wsaaT6659Key = FLSDArrayPartOfArrayStructure(4, wsaaT6659Rec, 0);
	private FixedLengthStringData[] wsaaT6659UnitStatMethod = FLSDArrayPartOfArrayStructure(4, wsaaT6659Key, 0);
	private FixedLengthStringData[] wsaaT6659Data = FLSDArrayPartOfArrayStructure(19, wsaaT6659Rec, 4);
	private PackedDecimalData[] wsaaT6659Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT6659Data, 0);
	private FixedLengthStringData[] wsaaT6659AnnOrPayInd = FLSDArrayPartOfArrayStructure(1, wsaaT6659Data, 5);
	private FixedLengthStringData[] wsaaT6659Freq = FLSDArrayPartOfArrayStructure(2, wsaaT6659Data, 6);
	private FixedLengthStringData[] wsaaT6659OsUtrnInd = FLSDArrayPartOfArrayStructure(1, wsaaT6659Data, 8);
	private FixedLengthStringData[] wsaaT6659Subprog = FLSDArrayPartOfArrayStructure(10, wsaaT6659Data, 9);
	//private static final int wsaaT6659Size = 10;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT6659Size = 1000;

	private FixedLengthStringData wsaaValidChdr = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidChdr, "Y");

	private FixedLengthStringData wsaaProceedInd = new FixedLengthStringData(1).init("N");
	private Validator wsaaNotProceed = new Validator(wsaaProceedInd, "N");
	private FixedLengthStringData wsaaUnitStatMethod = new FixedLengthStringData(4).init(SPACES);

	private FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(100);
	private FixedLengthStringData wsysSysparams = new FixedLengthStringData(100).isAPartOf(wsysSystemErrorParams, 0);
	private FixedLengthStringData wsysUstxData = new FixedLengthStringData(100).isAPartOf(wsysSysparams, 0, REDEFINE);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysUstxData, 0);
	private FixedLengthStringData wsysCnttype = new FixedLengthStringData(3).isAPartOf(wsysUstxData, 8);
	private ZonedDecimalData wsysStmdte = new ZonedDecimalData(8, 0).isAPartOf(wsysUstxData, 11).setUnsigned();
	private FixedLengthStringData wsysStatcode = new FixedLengthStringData(2).isAPartOf(wsysUstxData, 19);
	private FixedLengthStringData wsysPstcde = new FixedLengthStringData(2).isAPartOf(wsysUstxData, 21);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaT6647Ix = new IntegerData();
	private IntegerData wsaaT6659Ix = new IntegerData();
	private UtrnrnlTableDAM utrnrnlIO = new UtrnrnlTableDAM();
	private T5679rec t5679rec = new T5679rec();
	private T6647rec t6647rec = new T6647rec();
	private T6659rec t6659rec = new T6659rec();
	private Annprocrec annprocrec = new Annprocrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	
	//fwang3
    private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
    private UstxpfDAO ustxpfDAO=getApplicationContext().getBean("ustxpfDAO", UstxpfDAO.class);
    private ChdrpfDAO chdrpfDAO=getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
    private Iterator<Ustxpf> iter; 
    private Ustxpf ustxpfRec;
    List<Chdrpf> chdrList = new ArrayList<Chdrpf>();
    List<Chdrpf> chdrUpdateList = new ArrayList<Chdrpf>();
    
    private int intBatchID;
    private int intBatchExtractSize;
	private int ct01Val;
	private int ct06Val;
	private int ct08Val;
	private int ct03Val;
	private int ct04Val;
	private int ct07Val;
	private int ct02Val;
	private int ct05Val;
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		outstandUnitTrans2572, 
		continue2578, 
		exit2579
	}

	public Br617() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Restarting of this program is handled by MAINB,*/
		/** using a restart method of '3'.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isNE(bprdIO.getRestartMethod(), "3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*  Point to correct member of USTXPF.*/
		varcom.vrcmTranid.set(batcdorrec.tranid);
		wsaaT6647ItemTranCode.set(bprdIO.getAuthCode());
		wsaaUstxRunid.set(bprdIO.getSystemParam04());
		wsaaUstxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
        if (bprdIO.systemParam01.isNumeric()){
            if (bprdIO.systemParam01.toInt() > 0){
                intBatchExtractSize = bprdIO.systemParam01.toInt();
            }else{
                intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
            }
        }else{
            intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();  
        }
        readChunkRecord();
        
        /* Read T5679*/
		String coy = bsprIO.getCompany().toString();
		String itemitem=bprdIO.getAuthCode().toString();
		
		List<Itempf> items = this.itemDAO.getAllItemitem("IT", coy, t5679, itemitem);
		if (null == items || items.isEmpty()) {
			syserrrec.params.set(this.t5679);
			fatalError600();
		} else {//IJTI-320 START
			t5679rec.t5679Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
		}
		//IJTI-320 END
		/*  Load T6647*/
		wsaaT6647Ix.set(1);
		items = itemDAO.getAllitems("IT", coy, t6647);
		if (null == items || items.isEmpty()) {
			syserrrec.params.set(t6647);
			fatalError600();
		} else {//IJTI-320 START
		if (items.size() > wsaaT6647Size) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(t6647);
			fatalError600();
		}
		for (Itempf item : items) {
			t6647rec.t6647Rec.set(StringUtil.rawToString(item.getGenarea()));
			wsaaT6647Key[wsaaT6647Ix.toInt()].set(item.getItemitem());
			wsaaT6647Itmfrm[wsaaT6647Ix.toInt()].set(item.getItmfrm());
			wsaaT6647UnitStatMethod[wsaaT6647Ix.toInt()].set(t6647rec.unitStatMethod);
			wsaaT6647Ix.add(1);
		}
		}
		//IJTI-320 END
		
		/*  Load T6659*/
		wsaaT6659Ix.set(1);
		items = itemDAO.getAllitems("IT", coy, t6659);
		if (null == items || items.isEmpty()) {
			syserrrec.params.set(t6659);
			fatalError600();
		} else {//IJTI-320 START
		if (isGT(items.size(), wsaaT6659Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(t6659);
			fatalError600();
		}
		for (Itempf item : items) {
			t6659rec.t6659Rec.set(StringUtil.rawToString(item.getGenarea()));
			wsaaT6659Key[wsaaT6659Ix.toInt()].set(item.getItemitem());
			wsaaT6659Itmfrm[wsaaT6659Ix.toInt()].set(item.getItmfrm());
			wsaaT6659AnnOrPayInd[wsaaT6659Ix.toInt()].set(t6659rec.annOrPayInd);
			wsaaT6659Freq[wsaaT6659Ix.toInt()].set(t6659rec.freq);
			wsaaT6659OsUtrnInd[wsaaT6659Ix.toInt()].set(t6659rec.osUtrnInd);
			wsaaT6659Subprog[wsaaT6659Ix.toInt()].set(t6659rec.subprog);
			wsaaT6659Ix.add(1);
		}
		}
		//IJTI-320 END
		
	}

	private void readChunkRecord() {
		List<Ustxpf> ustxpfList = this.ustxpfDAO.searchResult(
				wsaaUstxFn.toString(), wsaaThreadMember.toString(), intBatchExtractSize, intBatchID);
		if (null == ustxpfList || ustxpfList.isEmpty()) {
			wsspEdterror.set(varcom.endp);
			return;
		}
		this.iter = ustxpfList.iterator();
		//load chdrpf
		for (Ustxpf pf : ustxpfList) {
			Chdrpf chdrpf = chdrpfDAO.getchdrRecord(pf.getChdrcoy(), pf.getChdrnum());
			if (null == chdrpf) {
				fatalError600();
				return;
			}
			chdrList.add(chdrpf);
		}
	}


protected void readFile2000()
	{
		readFile2010();
	}

	protected void readFile2010() {
		if (!iter.hasNext()) {
			intBatchID++;
			readChunkRecord();
		}
		
		if (iter.hasNext()) {
			this.ustxpfRec = iter.next();
		
		/* No of CHDR records read */
		ct01Val++;
		/* Set up the key for the SYSR- copybook, should a system error */
		/* for this instalment occur. */
		wsysChdrnum.set(ustxpfRec.getChdrnum());
		wsysCnttype.set(ustxpfRec.getCnttype());
		wsysStmdte.set(ustxpfRec.getStatementDate());
		wsysStatcode.set(ustxpfRec.getStatcode());
		wsysPstcde.set(ustxpfRec.getPstatcode());
		
		}
	}

protected void edit2500()
	{
		read2510();
	}

protected void read2510()
	{
		/* Read the contract header and validate the status against those*/
		/* on T5679. If the status is invalid, add 1 to CT06, which is a*/
		/* log of the Contract Header Records which have invalid statii.*/
		wsspEdterror.set(varcom.oK);
		validateChdr2540();
		if (!validContract.isTrue()) {
			/*  No of CHDRs with invalid statii*/
			ct06Val++;
			wsspEdterror.set(SPACES);
			return ;
		}
		readT66472550();
		readT66592560();
		validate2570();
		if (wsaaNotProceed.isTrue()) {
			wsspEdterror.set(SPACES);
			return ;
		}
		softlock2580();
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			/*  No of CHDRs locked*/
			ct07Val++;
			wsspEdterror.set(SPACES);
		}
	}

protected void validateChdr2540()
	{
		/*START*/
		/* Validate Contract status against T5679.*/
		wsaaValidChdr.set("N");
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)
		|| validContract.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Sub.toInt()], ustxpfRec.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)
				|| validContract.isTrue()); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.cnPremStat[wsaaT5679Sub.toInt()], ustxpfRec.getPstatcode())) {
						wsaaValidChdr.set("Y");
					}
				}
			}
		}
		/*EXIT*/
	}

protected void readT66472550()
	{
		start2550();
	}

protected void start2550()
	{
		wsaaT6647ItemCnttype.set(ustxpfRec.getCnttype());
		wsaaT6647Ix.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaT6647Ix, wsaaT6647Rec.length); wsaaT6647Ix.add(1)){
				if (isEQ(wsaaT6647Key[wsaaT6647Ix.toInt()], wsaaT6647ItemKey)) {
					break searchlabel1;
				}
			}
			syserrrec.statuz.set(h965);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(t6647);
			stringVariable1.addExpression(wsysSystemErrorParams);
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();
		}
		/*  We must find the effective T6647 entry for the contract*/
		/*  (T6647 entries will have been loaded in descending sequence*/
		/*  into the array).*/
		wsaaDateFound.set("N");
		while ( !(isNE(wsaaT6647ItemKey, wsaaT6647Key[wsaaT6647Ix.toInt()])
		|| isGT(wsaaT6647Ix, wsaaT6647Size)
		|| dateFound.isTrue())) {
			if (isGTE(ustxpfRec.getOccdate(), wsaaT6647Itmfrm[wsaaT6647Ix.toInt()])) {
				wsaaDateFound.set("Y");
			}
			else {
				wsaaT6647Ix.add(1);
			}
		}
		
		if (!dateFound.isTrue()) {
			syserrrec.statuz.set(h965);
			syserrrec.params.set(wsysSystemErrorParams);
			fatalError600();
		}
		wsaaUnitStatMethod.set(wsaaT6647UnitStatMethod[wsaaT6647Ix.toInt()]);
	}

protected void readT66592560()
	{
		start2560();
	}

protected void start2560()
	{
		wsaaT6659Ix.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaT6659Ix, wsaaT6659Rec.length); wsaaT6659Ix.add(1)){
				if (isEQ(wsaaT6659Key[wsaaT6659Ix.toInt()], wsaaUnitStatMethod)) {
					break searchlabel1;
				}
			}
			syserrrec.statuz.set(g029);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(t6659);
			stringVariable1.addExpression(wsysSystemErrorParams);
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();
		}
		/*  We must find the effective T6659 entry for the contract*/
		/*  (T6659 entries will have been loaded in descending sequence*/
		/*  into the array).*/
		wsaaDateFound.set("N");
		while ( !(isNE(wsaaUnitStatMethod, wsaaT6659Key[wsaaT6659Ix.toInt()])
		|| isGT(wsaaT6659Ix, wsaaT6659Size)
		|| dateFound.isTrue())) {
			if (isGTE(ustxpfRec.getOccdate(), wsaaT6659Itmfrm[wsaaT6659Ix.toInt()])) {
				wsaaDateFound.set("Y");
			}
			else {
				wsaaT6659Ix.add(1);
			}
		}
		
		if (!dateFound.isTrue()) {
			syserrrec.statuz.set(g029);
			syserrrec.params.set(wsysSystemErrorParams);
			fatalError600();
		}
	}

protected void validate2570()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start2570();
					checkT6659Details2571();
				case outstandUnitTrans2572: 
					outstandUnitTrans2572();
				case continue2578: 
					continue2578();
				case exit2579: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2570()
	{
		wsaaProceedInd.set("N");
		if (isEQ(wsaaT6659Subprog[wsaaT6659Ix.toInt()], SPACES)) {
			/*  No of CHDRs not processed*/
			ct08Val++;
			goTo(GotoLabel.exit2579);
		}
		else {
			t6659rec.subprog.set(wsaaT6659Subprog[wsaaT6659Ix.toInt()]);
		}
	}

protected void checkT6659Details2571()
	{
		/* Check the details and call the generic subroutine from T6659.*/
		if (isNE(wsaaT6659AnnOrPayInd[wsaaT6659Ix.toInt()], "P")) {
			goTo(GotoLabel.outstandUnitTrans2572);
		}
		/* No of CHDR with O/S payments*/
		if (isLT(ustxpfRec.getPtdate(), ustxpfRec.getStatementDate())) {
			ct03Val++;
			goTo(GotoLabel.exit2579);
		}
	}

protected void outstandUnitTrans2572()
	{
		if (isEQ(wsaaT6659OsUtrnInd[wsaaT6659Ix.toInt()], "Y")) {
			goTo(GotoLabel.continue2578);
		}
		utrnrnlIO.setParams(SPACES);
		utrnrnlIO.setChdrnum(ustxpfRec.getChdrnum());
		utrnrnlIO.setChdrcoy(ustxpfRec.getChdrcoy());
		utrnrnlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, utrnrnlIO);
		if ((isNE(utrnrnlIO.getStatuz(), varcom.oK))
		&& (isNE(utrnrnlIO.getStatuz(), varcom.mrnf))) {
			syserrrec.statuz.set(utrnrnlIO.getStatuz());
			syserrrec.params.set(utrnrnlIO.getParams());
			fatalError600();
		}
		if (isEQ(utrnrnlIO.getStatuz(), varcom.oK)
		&& isEQ(utrnrnlIO.getChdrcoy(), ustxpfRec.getChdrcoy())
		&& isEQ(utrnrnlIO.getChdrnum(), ustxpfRec.getChdrnum())) {
			/* No of CHDR with O/S UTRN*/
			ct04Val++;
			goTo(GotoLabel.exit2579);
		}
	}

protected void continue2578()
	{
		wsaaProceedInd.set("Y");
	}

protected void softlock2580()
	{
		start2580();
	}

protected void start2580()
	{
		/* Soft lock the contract, if it is to be processed.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(ustxpfRec.getChdrcoy());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(ustxpfRec.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			wsysSysparams.set(sftlockrec.sftlockRec);
			syserrrec.params.set(wsysSystemErrorParams);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void update3000()
	{
		update3010();
		nearExit3700();
	}

protected void update3010()
	{
		Chdrpf chdrpf = readhChdrlif3100();
		annprocrec.annpllRec.set(SPACES);
		annprocrec.company.set(ustxpfRec.getChdrcoy());
		annprocrec.chdrnum.set(ustxpfRec.getChdrnum());
		annprocrec.life.set(SPACES);
		annprocrec.coverage.set(SPACES);
		annprocrec.rider.set(SPACES);
		annprocrec.planSuffix.set(0);
		annprocrec.effdate.set(bsscIO.getEffectiveDate());
		annprocrec.batctrcde.set(bprdIO.getAuthCode());
		annprocrec.crdate.set(0);
		annprocrec.crtable.set(SPACES);
		annprocrec.user.set(varcom.vrcmUser);
		annprocrec.batcactyr.set(batcdorrec.actyear);
		annprocrec.batcactmn.set(batcdorrec.actmonth);
		annprocrec.batccoy.set(batcdorrec.company);
		annprocrec.batcbrn.set(batcdorrec.branch);
		annprocrec.batcbatch.set(batcdorrec.batch);
		annprocrec.batcpfx.set(batcdorrec.prefix);
		callProgram(t6659rec.subprog, annprocrec.annpllRec);
		if (isNE(annprocrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(annprocrec.statuz);
			syserrrec.params.set(annprocrec.annpllRec);
			fatalError600();
		}
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.frequency.set(wsaaT6659Freq[wsaaT6659Ix.toInt()]);
		datcon2rec.freqFactor.set(1);
		datcon2rec.intDate1.set(chdrpf.getStatdate());
		datcon2rec.intDate2.set(0);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		chdrpf.setStatdate(datcon2rec.intDate2.toInt());
		this.chdrUpdateList.add(chdrpf);
	}

protected void nearExit3700()
	{
		unlockRewriteChdr3800();
		/* No of CHDR recs processed*/
		ct02Val++;
		/* No of unit statement printed*/
		ct05Val++;
		/*EXIT*/
	}

	protected Chdrpf readhChdrlif3100() {
		/* Read and hold the CHDRLIF record. */
		for (Chdrpf pf : this.chdrList) {
			if (isEQ(pf.getChdrcoy(),ustxpfRec.getChdrcoy())
					&& pf.getChdrnum().equals(ustxpfRec.getChdrnum())) {
				return pf;
			}
		}
		fatalError600();
		return null;
	}

protected void unlockRewriteChdr3800()
	{
		para3810();
	}

protected void para3810()
	{
		/* Undone soft lock.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(ustxpfRec.getChdrcoy());
		sftlockrec.entity.set(ustxpfRec.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
	}

protected void commit3500()
	{
		commitControlTotals();
		this.chdrpfDAO.updateChdrLifStatdate(chdrUpdateList);
		chdrUpdateList.clear();
		chdrUpdateList=null;
	}

private void commitControlTotals() {
	contotrec.totno.set(ct01);
	contotrec.totval.set(ct01Val);
	callContot001();
	ct01Val = 0;
	
	contotrec.totno.set(ct02);
	contotrec.totval.set(ct02Val);
	callContot001();
	ct02Val = 0;
	
	contotrec.totno.set(ct03);
	contotrec.totval.set(ct03Val);
	callContot001();
	ct03Val = 0;
	
	contotrec.totno.set(ct04);
	contotrec.totval.set(ct04Val);
	callContot001();
	ct04Val = 0;
	
	contotrec.totno.set(ct05);
	contotrec.totval.set(ct05Val);
	callContot001();
	ct05Val = 0;
	
	contotrec.totno.set(ct06);
	contotrec.totval.set(ct06Val);
	callContot001();
	ct06Val = 0;
	
	contotrec.totno.set(ct07);
	contotrec.totval.set(ct07Val);
	callContot001();
	ct07Val = 0;
	
	contotrec.totno.set(ct08);
	contotrec.totval.set(ct08Val);
	callContot001();
	ct08Val = 0;
}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
	this.chdrList.clear();
	chdrList=null;
	this.iter=null;
	
	lsaaStatuz.set(varcom.oK);
		
	}
}
