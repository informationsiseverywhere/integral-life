package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:32
 * Description:
 * Copybook name: CRTLOANREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Crtloanrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData crtloanRec = new FixedLengthStringData(193);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(crtloanRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(crtloanRec, 5);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(crtloanRec, 9);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(crtloanRec, 10);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(crtloanRec, 18);
  	public PackedDecimalData loanno = new PackedDecimalData(2, 0).isAPartOf(crtloanRec, 21);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(crtloanRec, 23);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(crtloanRec, 26);
  	public FixedLengthStringData billcurr = new FixedLengthStringData(3).isAPartOf(crtloanRec, 29);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(crtloanRec, 32);
  	public PackedDecimalData occdate = new PackedDecimalData(8, 0).isAPartOf(crtloanRec, 37);
  	public FixedLengthStringData authCode = new FixedLengthStringData(4).isAPartOf(crtloanRec, 42);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(crtloanRec, 46);
  	public PackedDecimalData outstamt = new PackedDecimalData(17, 2).isAPartOf(crtloanRec, 47);
  	public PackedDecimalData cbillamt = new PackedDecimalData(17, 2).isAPartOf(crtloanRec, 56);
  	public FixedLengthStringData batchkey = new FixedLengthStringData(21).isAPartOf(crtloanRec, 65);
  	public FixedLengthStringData contkey = new FixedLengthStringData(14).isAPartOf(batchkey, 0);
  	public FixedLengthStringData prefix = new FixedLengthStringData(2).isAPartOf(contkey, 0);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(contkey, 2);
  	public FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(contkey, 3);
  	public PackedDecimalData actyear = new PackedDecimalData(4, 0).isAPartOf(contkey, 5);
  	public PackedDecimalData actmonth = new PackedDecimalData(2, 0).isAPartOf(contkey, 8);
  	public FixedLengthStringData trcde = new FixedLengthStringData(4).isAPartOf(contkey, 10);
  	public FixedLengthStringData batch = new FixedLengthStringData(5).isAPartOf(batchkey, 14);
  	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(batchkey, 19, FILLER);
  	public FixedLengthStringData sacscode01 = new FixedLengthStringData(2).isAPartOf(crtloanRec, 86);
  	public FixedLengthStringData sacscode02 = new FixedLengthStringData(2).isAPartOf(crtloanRec, 88);
  	public FixedLengthStringData sacscode03 = new FixedLengthStringData(2).isAPartOf(crtloanRec, 90);
  	public FixedLengthStringData sacscode04 = new FixedLengthStringData(2).isAPartOf(crtloanRec, 92);
  	public FixedLengthStringData sacstyp01 = new FixedLengthStringData(2).isAPartOf(crtloanRec, 94);
  	public FixedLengthStringData sacstyp02 = new FixedLengthStringData(2).isAPartOf(crtloanRec, 96);
  	public FixedLengthStringData sacstyp03 = new FixedLengthStringData(2).isAPartOf(crtloanRec, 98);
  	public FixedLengthStringData sacstyp04 = new FixedLengthStringData(2).isAPartOf(crtloanRec, 100);
  	public FixedLengthStringData glcode01 = new FixedLengthStringData(14).isAPartOf(crtloanRec, 102);
  	public FixedLengthStringData glcode02 = new FixedLengthStringData(14).isAPartOf(crtloanRec, 116);
  	public FixedLengthStringData glcode03 = new FixedLengthStringData(14).isAPartOf(crtloanRec, 130);
  	public FixedLengthStringData glcode04 = new FixedLengthStringData(14).isAPartOf(crtloanRec, 144);
  	public FixedLengthStringData glsign01 = new FixedLengthStringData(1).isAPartOf(crtloanRec, 158);
  	public FixedLengthStringData glsign02 = new FixedLengthStringData(1).isAPartOf(crtloanRec, 159);
  	public FixedLengthStringData glsign03 = new FixedLengthStringData(1).isAPartOf(crtloanRec, 160);
  	public FixedLengthStringData glsign04 = new FixedLengthStringData(1).isAPartOf(crtloanRec, 161);
  	public FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(crtloanRec, 162);
  	public FixedLengthStringData loantype = new FixedLengthStringData(1).isAPartOf(crtloanRec, 192);


	public void initialize() {
		COBOLFunctions.initialize(crtloanRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		crtloanRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}