package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.BonspfDAO;
import com.csc.life.regularprocessing.dataaccess.model.B5018ForUpdateDTO;
import com.csc.life.regularprocessing.dataaccess.model.Bonlpf;
import com.csc.life.regularprocessing.dataaccess.model.Bonspf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class BonspfDAOImpl extends BaseDAOImpl<Bonspf> implements BonspfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(BonspfDAOImpl.class);

    public Map<String, List<Bonspf>> searchBonspf(String coy, List<String> chdrnumList) {

        StringBuilder sqlBonsSelect1 = new StringBuilder(
                "SELECT UNIQUE_NUMBER, CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,CURRFROM,CURRTO,VALIDFLAG,BCALMETH,BPAYTY,BPAYNY,TOTBON,TERMID,USER_T,TRDT,TRTM ");
        sqlBonsSelect1.append("FROM BONSPF WHERE CHDRCOY=? AND ");
        sqlBonsSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
        sqlBonsSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, UNIQUE_NUMBER ASC ");
        PreparedStatement psBonsSelect = getPrepareStatement(sqlBonsSelect1.toString());
        ResultSet sqlbonspf1rs = null;
        Map<String, List<Bonspf>> bonspfMap = new HashMap<String, List<Bonspf>>();
        try {
            psBonsSelect.setInt(1, Integer.parseInt(coy));
            sqlbonspf1rs = executeQuery(psBonsSelect);

            while (sqlbonspf1rs.next()) {
                Bonspf bonspf = new Bonspf();
                bonspf.setUniqueNumber(sqlbonspf1rs.getLong(1));
                bonspf.setChdrcoy(sqlbonspf1rs.getString(2));
                bonspf.setChdrnum(sqlbonspf1rs.getString(3));
                bonspf.setLife(sqlbonspf1rs.getString(4));
                bonspf.setCoverage(sqlbonspf1rs.getString(5));
                bonspf.setRider(sqlbonspf1rs.getString(6));
                bonspf.setPlanSuffix(sqlbonspf1rs.getInt(7));
                bonspf.setCurrfrom(sqlbonspf1rs.getInt(8));
                bonspf.setCurrto(sqlbonspf1rs.getInt(9));
                bonspf.setValidflag(sqlbonspf1rs.getString(10));
                bonspf.setBonusCalcMeth(sqlbonspf1rs.getString(11));
                bonspf.setBonPayThisYr(sqlbonspf1rs.getBigDecimal(12));
                bonspf.setBonPayLastYr(sqlbonspf1rs.getBigDecimal(13));
                bonspf.setTotalBonus(sqlbonspf1rs.getBigDecimal(14));
                bonspf.setTermid(sqlbonspf1rs.getString(15));
                bonspf.setUser(sqlbonspf1rs.getInt(16));
                bonspf.setTransactionDate(sqlbonspf1rs.getInt(17));
                bonspf.setTransactionTime(sqlbonspf1rs.getInt(18));

                String chdrnum = bonspf.getChdrnum();
                if (bonspfMap.containsKey(chdrnum)) {
                    bonspfMap.get(chdrnum).add(bonspf);
                } else {
                    List<Bonspf> bonspfSearchResult = new LinkedList<Bonspf>();
                    bonspfSearchResult.add(bonspf);
                    bonspfMap.put(chdrnum, bonspfSearchResult);
                }
            }

        } catch (SQLException e) {
            LOGGER.error("searchBonsRecord()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(psBonsSelect, sqlbonspf1rs);
        }
        return bonspfMap;
    }

    public void insertBonspf(List<Bonspf> bonspfList) {

        if (bonspfList != null && bonspfList.size() > 0) {
            String SQL_BONS_INSERT = "INSERT INTO BONSPF(CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,CURRFROM,CURRTO,VALIDFLAG,BCALMETH,BPAYTY,BPAYNY,TOTBON,TERMID,USER_T,TRDT,TRTM,USRPRF,JOBNM,DATIME)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement psBonsInsert = getPrepareStatement(SQL_BONS_INSERT);
            try {
                for (Bonspf c : bonspfList) {
                    psBonsInsert.setString(1, c.getChdrcoy());
                    psBonsInsert.setString(2, c.getChdrnum());
                    psBonsInsert.setString(3, c.getLife());
                    psBonsInsert.setString(4, c.getCoverage());
                    psBonsInsert.setString(5, c.getRider());
                    psBonsInsert.setInt(6, c.getPlanSuffix());
                    psBonsInsert.setInt(7, c.getCurrfrom());
                    psBonsInsert.setInt(8, c.getCurrto());
                    psBonsInsert.setString(9, c.getValidflag());
                    psBonsInsert.setString(10, c.getBonusCalcMeth());
                    psBonsInsert.setBigDecimal(11, c.getBonPayThisYr());
                    psBonsInsert.setBigDecimal(12, c.getBonPayLastYr());
                    psBonsInsert.setBigDecimal(13, c.getTotalBonus());
                    psBonsInsert.setString(14, c.getTermid());
                    psBonsInsert.setInt(15, c.getUser());
                    psBonsInsert.setInt(16, c.getTransactionDate());
                    psBonsInsert.setInt(17, c.getTransactionTime());
                    psBonsInsert.setString(18, getUsrprf());
                    psBonsInsert.setString(19, getJobnm());
                    psBonsInsert.setTimestamp(20, new Timestamp(System.currentTimeMillis()));
                    psBonsInsert.addBatch();
                }
                psBonsInsert.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("insertBonsRecord()", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(psBonsInsert, null);
            }
            bonspfList.clear();
        }

    }

    public void updateBonspf(List<Bonspf> bonspfList) {

        if (bonspfList != null && bonspfList.size() > 0) {
            String SQL_BONS_UPDATE = "UPDATE BONSPF SET TERMID=?,USER_T=?,TRDT=?,TRTM=?,VALIDFLAG=2,CURRTO=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";

            PreparedStatement psBonsUpdate = getPrepareStatement(SQL_BONS_UPDATE);
            try {
                for (Bonspf c : bonspfList) {
                    psBonsUpdate.setString(1, c.getTermid());
                    psBonsUpdate.setInt(2, c.getUser());
                    psBonsUpdate.setInt(3, c.getTransactionDate());
                    psBonsUpdate.setInt(4, c.getTransactionTime());
                    psBonsUpdate.setInt(5, c.getCurrto());
                    psBonsUpdate.setString(6, getJobnm());
                    psBonsUpdate.setString(7, getUsrprf());
                    psBonsUpdate.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
                    psBonsUpdate.setLong(9, c.getUniqueNumber());
                    psBonsUpdate.addBatch();
                }
                psBonsUpdate.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("updateBonsRecord()", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(psBonsUpdate, null);
            }
            bonspfList.clear();
        }

    }
    public List<B5018ForUpdateDTO> searchBonsPfResult(String chdrcoy,String chdrnum,String life,String coverage,String ride,int plnsfx) {
		
		List<B5018ForUpdateDTO> updateList = null;
    	String sql = "select bs.*,bl.* from bonsrvb bs  left join bonlrvb bl on bs.chdrcoy=bl.chdrcoy and bs.chdrnum = bl.chdrnum"
    			+ " where bs.chdrcoy=? and bs.chdrnum=? and bs.life=? and bs.coverage=? and bs.rider=? and bs.plnsfx=?";
    	PreparedStatement ps = getPrepareStatement(sql);
        ResultSet rs = null;
        
    	 try {
 			 ps.setString(1, chdrcoy);
 			 ps.setString(2, chdrnum);
 			 ps.setString(3, life);
			 ps.setString(4, coverage);
			 ps.setString(5, ride);
 			 ps.setInt(6, plnsfx);
    		 rs = executeQuery(ps);
    		 updateList = new ArrayList<B5018ForUpdateDTO>();
			 while(rs.next()){
				 B5018ForUpdateDTO updateDTO = new B5018ForUpdateDTO();
				 Bonspf bonsrvb = new Bonspf();
				 Bonlpf bonlrvb = new Bonlpf();
				 bonsrvb.setUniqueNumber(rs.getLong(1));
				 bonsrvb.setChdrcoy(rs.getString(2));
				 bonsrvb.setChdrnum(rs.getString(3));
				 bonsrvb.setLife(rs.getString(4));
				 bonsrvb.setCoverage(rs.getString(5));
				 bonsrvb.setRider(rs.getString(6));
				 bonsrvb.setPlanSuffix(rs.getInt(7));
				 bonsrvb.setCurrfrom(rs.getInt(8));
				 bonsrvb.setCurrto(rs.getInt(9));
				 bonsrvb.setValidflag(rs.getString(10));
				 bonsrvb.setBonusCalcMeth(rs.getString(11));
				 bonsrvb.setBonPayThisYr(rs.getBigDecimal(12));
				 bonsrvb.setBonPayLastYr(rs.getBigDecimal(13));
				 bonsrvb.setTotalBonus(rs.getBigDecimal(14));
				 bonsrvb.setTermid(rs.getString(15));
				 bonsrvb.setUser(rs.getInt(16));
				 bonsrvb.setTransactionDate(rs.getInt(17));
				 bonsrvb.setTransactionTime(rs.getInt(18));
				 bonsrvb.setUserProfile(rs.getString(19));
				 bonsrvb.setJobName(rs.getString(20));
				 bonsrvb.setDatime(rs.getString(21));
				 updateDTO.setBonspf(bonsrvb);
				 
				 bonlrvb.setUniqueNumer(rs.getLong(22));
				 bonlrvb.setChdrcoy(rs.getString(23));
				 bonlrvb.setChdrnum(rs.getString(24));
				 bonlrvb.setCurrfrom(rs.getInt(25));
				 bonlrvb.setCurrto(rs.getInt(26));
				 bonlrvb.setDatetexc(rs.getString(27));
				 bonlrvb.setValidflag(rs.getString(28));
				 bonlrvb.setLongdesc01(rs.getString(29));
				 bonlrvb.setLongdesc02(rs.getString(30));
				 bonlrvb.setLongdesc03(rs.getString(31));
				 bonlrvb.setLongdesc04(rs.getString(32));
				 bonlrvb.setLongdesc05(rs.getString(33));
				 bonlrvb.setLongdesc06(rs.getString(34));
				 bonlrvb.setLongdesc07(rs.getString(35));
				 bonlrvb.setLongdesc08(rs.getString(36));
				 bonlrvb.setLongdesc09(rs.getString(37));
				 bonlrvb.setLongdesc10(rs.getString(38));
				 bonlrvb.setSumins01(rs.getBigDecimal(39));
				 bonlrvb.setSumins02(rs.getBigDecimal(40));
				 bonlrvb.setSumins03(rs.getBigDecimal(41));
				 bonlrvb.setSumins04(rs.getBigDecimal(42));
				 bonlrvb.setSumins05(rs.getBigDecimal(43));
				 bonlrvb.setSumins06(rs.getBigDecimal(44));
				 bonlrvb.setSumins07(rs.getBigDecimal(45));
				 bonlrvb.setSumins08(rs.getBigDecimal(46));
				 bonlrvb.setSumins09(rs.getBigDecimal(47));
				 bonlrvb.setSumins10(rs.getBigDecimal(48));
				 bonlrvb.setBonusCalcMeth01(rs.getString(49));
				 bonlrvb.setBonusCalcMeth02(rs.getString(50));
				 bonlrvb.setBonusCalcMeth03(rs.getString(51));
				 bonlrvb.setBonusCalcMeth04(rs.getString(52));
				 bonlrvb.setBonusCalcMeth05(rs.getString(53));
				 bonlrvb.setBonusCalcMeth06(rs.getString(54));
				 bonlrvb.setBonusCalcMeth07(rs.getString(55));
				 bonlrvb.setBonusCalcMeth08(rs.getString(56));
				 bonlrvb.setBonusCalcMeth09(rs.getString(57));
				 bonlrvb.setBonusCalcMeth10(rs.getString(58));
				 bonlrvb.setBonPayThisYr01(rs.getBigDecimal(59));
				 bonlrvb.setBonPayThisYr02(rs.getBigDecimal(60));
				 bonlrvb.setBonPayThisYr03(rs.getBigDecimal(61));
				 bonlrvb.setBonPayThisYr04(rs.getBigDecimal(62));
				 bonlrvb.setBonPayThisYr05(rs.getBigDecimal(63));
				 bonlrvb.setBonPayThisYr06(rs.getBigDecimal(64));
				 bonlrvb.setBonPayThisYr07(rs.getBigDecimal(65));
				 bonlrvb.setBonPayThisYr08(rs.getBigDecimal(66));
				 bonlrvb.setBonPayThisYr09(rs.getBigDecimal(67));
				 bonlrvb.setBonPayThisYr10(rs.getBigDecimal(68));
				 bonlrvb.setBonPayLastYr01(rs.getBigDecimal(69));
				 bonlrvb.setBonPayLastYr02(rs.getBigDecimal(70));
				 bonlrvb.setBonPayLastYr03(rs.getBigDecimal(71));
				 bonlrvb.setBonPayLastYr04(rs.getBigDecimal(72));
				 bonlrvb.setBonPayLastYr05(rs.getBigDecimal(73));
				 bonlrvb.setBonPayLastYr06(rs.getBigDecimal(74));
				 bonlrvb.setBonPayLastYr07(rs.getBigDecimal(75));
				 bonlrvb.setBonPayLastYr08(rs.getBigDecimal(76));
				 bonlrvb.setBonPayLastYr09(rs.getBigDecimal(77));
				 bonlrvb.setBonPayLastYr10(rs.getBigDecimal(78));
				 bonlrvb.setTotalBonus01(rs.getBigDecimal(79));
				 bonlrvb.setTotalBonus02(rs.getBigDecimal(80));
				 bonlrvb.setTotalBonus03(rs.getBigDecimal(81));
				 bonlrvb.setTotalBonus04(rs.getBigDecimal(82));
				 bonlrvb.setTotalBonus05(rs.getBigDecimal(83));
				 bonlrvb.setTotalBonus06(rs.getBigDecimal(84));
				 bonlrvb.setTotalBonus07(rs.getBigDecimal(85));
				 bonlrvb.setTotalBonus08(rs.getBigDecimal(86));
				 bonlrvb.setTotalBonus09(rs.getBigDecimal(87));
				 bonlrvb.setTotalBonus10(rs.getBigDecimal(88));
				 bonlrvb.setTermid(rs.getString(89));
				 bonlrvb.setTransactionDate(rs.getInt(90));
				 bonlrvb.setTransactionTime(rs.getInt(91));
				 bonlrvb.setUser(rs.getInt(92));
				 bonlrvb.setUserProfile(rs.getString(93));
				 bonlrvb.setJobName(rs.getString(94));
				 bonlrvb.setDatime(rs.getString(95));
				 updateDTO.setBonlpf(bonlrvb);
				 updateList.add(updateDTO);
			}
		} catch (SQLException e) {
            LOGGER.error("searchBonsPfResult()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
    	 return updateList;
    }
    
    public Bonspf readBonspf(String chdrcoy, String chdrnum, String validflag) {
    	Bonspf bonspf = null;
    	StringBuilder sb  = new StringBuilder();
    	sb.append("SELECT UNIQUE_NUMBER, CHDRCOY,CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, CURRFROM, CURRTO, BPAYTY, BPAYNY, TOTBON FROM BONSPF ");
    	sb.append("WHERE CHDRCOY = ? AND CHDRNUM = ? AND VALIDFLAG = ? AND LIFE='01' AND COVERAGE='01' AND RIDER='00'");
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try {
    		ps = getPrepareStatement(sb.toString());
    		ps.setString(1, chdrcoy);
    		ps.setString(2, chdrnum);
    		ps.setString(3, validflag);
    		rs = ps.executeQuery();
    		while(rs.next()) {
    			bonspf = new Bonspf();
    			bonspf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
    			bonspf.setChdrcoy(rs.getString("CHDRCOY"));
    			bonspf.setChdrnum(rs.getString("CHDRNUM"));
    			bonspf.setLife(rs.getString("LIFE"));
    			bonspf.setCoverage(rs.getString("COVERAGE"));
    			bonspf.setRider(rs.getString("RIDER"));
    			bonspf.setPlanSuffix(rs.getInt("PLNSFX"));
    			bonspf.setCurrfrom(rs.getInt("CURRFROM"));
    			bonspf.setCurrto(rs.getInt("CURRTO"));
    			bonspf.setBonPayThisYr(rs.getBigDecimal("BPAYTY"));
    			bonspf.setBonPayLastYr(rs.getBigDecimal("BPAYNY"));
    			bonspf.setTotalBonus(rs.getBigDecimal("TOTBON"));
    		}
    	}catch (SQLException e) {
    		LOGGER.error("readBonspf()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
    	}
    	finally {
    		close(ps, rs, null);
    	}
    	return bonspf;
    }
}
