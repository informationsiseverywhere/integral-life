package com.csc.life.regularprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CovxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:37
 * Class transformed from COVXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CovxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 75;
	public FixedLengthStringData covxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData covxpfRecord = covxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(covxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(covxrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(covxrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(covxrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(covxrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(covxrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(covxrec);
	public FixedLengthStringData premCurrency = DD.prmcur.copy().isAPartOf(covxrec);
	public PackedDecimalData benBillDate = DD.bbldat.copy().isAPartOf(covxrec);
	public PackedDecimalData sumins = DD.sumins.copy().isAPartOf(covxrec);
	public PackedDecimalData premCessDate = DD.pcesdte.copy().isAPartOf(covxrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(covxrec);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(covxrec);
	public FixedLengthStringData subprog = DD.subprog.copy().isAPartOf(covxrec);
	public FixedLengthStringData unitFreq = DD.ufreq.copy().isAPartOf(covxrec);
	public FixedLengthStringData premmeth = DD.premmeth.copy().isAPartOf(covxrec);
	public FixedLengthStringData jlPremMeth = DD.jlpremeth.copy().isAPartOf(covxrec);
	public FixedLengthStringData svMethod = DD.svmeth.copy().isAPartOf(covxrec);
	public FixedLengthStringData adfeemth = DD.adfeemth.copy().isAPartOf(covxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public CovxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for CovxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public CovxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for CovxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public CovxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for CovxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public CovxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("COVXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"JLIFE, " +
							"PRMCUR, " +
							"BBLDAT, " +
							"SUMINS, " +
							"PCESDTE, " +
							"CRTABLE, " +
							"MORTCLS, " +
							"SUBPROG, " +
							"UFREQ, " +
							"PREMMETH, " +
							"JLPREMETH, " +
							"SVMETH, " +
							"ADFEEMTH, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     jlife,
                                     premCurrency,
                                     benBillDate,
                                     sumins,
                                     premCessDate,
                                     crtable,
                                     mortcls,
                                     subprog,
                                     unitFreq,
                                     premmeth,
                                     jlPremMeth,
                                     svMethod,
                                     adfeemth,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		jlife.clear();
  		premCurrency.clear();
  		benBillDate.clear();
  		sumins.clear();
  		premCessDate.clear();
  		crtable.clear();
  		mortcls.clear();
  		subprog.clear();
  		unitFreq.clear();
  		premmeth.clear();
  		jlPremMeth.clear();
  		svMethod.clear();
  		adfeemth.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getCovxrec() {
  		return covxrec;
	}

	public FixedLengthStringData getCovxpfRecord() {
  		return covxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setCovxrec(what);
	}

	public void setCovxrec(Object what) {
  		this.covxrec.set(what);
	}

	public void setCovxpfRecord(Object what) {
  		this.covxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(covxrec.getLength());
		result.set(covxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}