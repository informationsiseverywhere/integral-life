package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:15
 * Description:
 * Copybook name: ANNPROCREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Annprocrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData annpllRec = new FixedLengthStringData(90);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(annpllRec, 0);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(annpllRec, 4);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(annpllRec, 5);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(annpllRec, 13);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(annpllRec, 15);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(annpllRec, 17);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(annpllRec, 19);
  	public FixedLengthStringData batchkey = new FixedLengthStringData(19).isAPartOf(annpllRec, 22);
  	public FixedLengthStringData contkey = new FixedLengthStringData(14).isAPartOf(batchkey, 0);
  	public FixedLengthStringData batcpfx = new FixedLengthStringData(2).isAPartOf(contkey, 0);
  	public FixedLengthStringData batccoy = new FixedLengthStringData(1).isAPartOf(contkey, 2);
  	public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(contkey, 3);
  	public PackedDecimalData batcactyr = new PackedDecimalData(4, 0).isAPartOf(contkey, 5);
  	public PackedDecimalData batcactmn = new PackedDecimalData(2, 0).isAPartOf(contkey, 8);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(contkey, 10);
  	public FixedLengthStringData batcbatch = new FixedLengthStringData(5).isAPartOf(batchkey, 14);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(annpllRec, 41).setUnsigned();
  	public ZonedDecimalData crdate = new ZonedDecimalData(8, 0).isAPartOf(annpllRec, 49).setUnsigned();
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(annpllRec, 57);
  	public ZonedDecimalData user = new ZonedDecimalData(6, 0).isAPartOf(annpllRec, 61);
  	public ZonedDecimalData initUnitChargeFreq = new ZonedDecimalData(2, 0).isAPartOf(annpllRec, 67);
  	public ZonedDecimalData annchg = new ZonedDecimalData(5, 2).isAPartOf(annpllRec, 69);
  	public FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(annpllRec, 74, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(annpllRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		annpllRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}