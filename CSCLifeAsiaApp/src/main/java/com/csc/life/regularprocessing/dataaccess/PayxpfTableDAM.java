package com.csc.life.regularprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: PayxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:00
 * Class transformed from PAYXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PayxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 28;
	public FixedLengthStringData payxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData payxpfRecord = payxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(payxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(payxrec);
	public PackedDecimalData payrseqno = DD.payrseqno.copy().isAPartOf(payxrec);
	public FixedLengthStringData billsupr = DD.billsupr.copy().isAPartOf(payxrec);
	public PackedDecimalData billspfrom = DD.billspfrom.copy().isAPartOf(payxrec);
	public PackedDecimalData billspto = DD.billspto.copy().isAPartOf(payxrec);
	public FixedLengthStringData billchnl = DD.billchnl.copy().isAPartOf(payxrec);
	public PackedDecimalData billcd = DD.billcd.copy().isAPartOf(payxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public PayxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for PayxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public PayxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for PayxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public PayxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for PayxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public PayxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("PAYXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"PAYRSEQNO, " +
							"BILLSUPR, " +
							"BILLSPFROM, " +
							"BILLSPTO, " +
							"BILLCHNL, " +
							"BILLCD, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     payrseqno,
                                     billsupr,
                                     billspfrom,
                                     billspto,
                                     billchnl,
                                     billcd,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		payrseqno.clear();
  		billsupr.clear();
  		billspfrom.clear();
  		billspto.clear();
  		billchnl.clear();
  		billcd.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getPayxrec() {
  		return payxrec;
	}

	public FixedLengthStringData getPayxpfRecord() {
  		return payxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setPayxrec(what);
	}

	public void setPayxrec(Object what) {
  		this.payxrec.set(what);
	}

	public void setPayxpfRecord(Object what) {
  		this.payxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(payxrec.getLength());
		result.set(payxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}