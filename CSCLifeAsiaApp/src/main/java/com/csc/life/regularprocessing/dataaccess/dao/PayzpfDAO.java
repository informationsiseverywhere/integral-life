package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.Payzpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface PayzpfDAO extends BaseDAO<Payzpf> {

	public List<Payzpf> findResult(String tableName, String memName,
			int batchExtractSize, int batchID);

}