/*
 * File: Br614.java
 * Date: 29 August 2009 22:25:38
 * Author: Quipoz Limited
 * 
 * Class transformed from BR614.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.regularprocessing.dataaccess.WopxpfTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.regularprocessing.tablestructures.T5655rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.procedures.Contot;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 2005.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*  SPLITTER PROGRAM (For WOP Re-Rerate Batch processing)
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* This program is to be run before the Premium Waiver Re-rating
* Batch Program, BR615.
* It is run in single-thread, and writes the selected COVR
* records to multiple files . Each of the members will be read
* by a copy of BR615 run in multi-thread mode.
*
* Embedded SQL statement is used to access the COVRPF.
* The splitter program will extract COVR records that meet the
* following criteria:
*
* 1) Re-rate Date  <>  Zeros
* 2) Re-rate Date   <  Batch Effective Date + Maximum Auto Increase
*                                             Lead Days
* 3) Validflag      =  '1'
* 4) Contract Number between  P6671-chdrnumfrom
*                        and  p6671-chdrnumto
*    order by chdrcoy, chdrnum, life, coverage, rider, plnsfx
*
* Control totals used in this program:
*    01  -  No. of thread members
*    02  -  No. of extracted records
*
***********************************************************************
* </pre>
*/
public class Br614 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private java.sql.ResultSet sqlwopxCursorrs = null;
	private java.sql.PreparedStatement sqlwopxCursorps = null;
	private java.sql.Connection sqlwopxCursorconn = null;
	private DiskFileDAM wopx01 = new DiskFileDAM("WOPX01");
	private DiskFileDAM wopx02 = new DiskFileDAM("WOPX02");
	private DiskFileDAM wopx03 = new DiskFileDAM("WOPX03");
	private DiskFileDAM wopx04 = new DiskFileDAM("WOPX04");
	private DiskFileDAM wopx05 = new DiskFileDAM("WOPX05");
	private DiskFileDAM wopx06 = new DiskFileDAM("WOPX06");
	private DiskFileDAM wopx07 = new DiskFileDAM("WOPX07");
	private DiskFileDAM wopx08 = new DiskFileDAM("WOPX08");
	private DiskFileDAM wopx09 = new DiskFileDAM("WOPX09");
	private DiskFileDAM wopx10 = new DiskFileDAM("WOPX10");
	private DiskFileDAM wopx11 = new DiskFileDAM("WOPX11");
	private DiskFileDAM wopx12 = new DiskFileDAM("WOPX12");
	private DiskFileDAM wopx13 = new DiskFileDAM("WOPX13");
	private DiskFileDAM wopx14 = new DiskFileDAM("WOPX14");
	private DiskFileDAM wopx15 = new DiskFileDAM("WOPX15");
	private DiskFileDAM wopx16 = new DiskFileDAM("WOPX16");
	private DiskFileDAM wopx17 = new DiskFileDAM("WOPX17");
	private DiskFileDAM wopx18 = new DiskFileDAM("WOPX18");
	private DiskFileDAM wopx19 = new DiskFileDAM("WOPX19");
	private DiskFileDAM wopx20 = new DiskFileDAM("WOPX20");
	private WopxpfTableDAM wopxpfData = new WopxpfTableDAM();
		/* Change the record length to that of the temporary file. This
		 can be found by doing a DSPFD of the file being duplicated
		 by the CRTTMPF process.*/
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR614");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

	private FixedLengthStringData wsaaWopxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaWopxFn, 0, FILLER).init("WOPX");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaWopxFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaWopxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThread = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThread, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThread, 6).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaThread, 9, FILLER).init(SPACES);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaWopxData = FLSInittedArray (1000, 18);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaWopxData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaWopxData, 1);
	private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaWopxData, 9);
	private FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaWopxData, 11);
	private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaWopxData, 13);
	private PackedDecimalData[] wsaaPlnsfx = PDArrayPartOfArrayStructure(4, 0, wsaaWopxData, 15);

		/* WSAA-IND-ARRAY */
	private FixedLengthStringData[] wsaaWopxInd = FLSInittedArray (1000, 8);
	private BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(4, 4, 0, wsaaWopxInd, 0);
	private FixedLengthStringData wsaa1 = new FixedLengthStringData(1).init("1");
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaMaxLeadDays = new PackedDecimalData(8, 0).init(ZERO);
	private PackedDecimalData wsaaSqlDate = new PackedDecimalData(8, 0).init(ZERO);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
		/* ERRORS */
	private static final String ivrm = "IVRM";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Datcon2rec datcon2rec = new Datcon2rec();
	private P6671par p6671par = new P6671par();
	private T5655rec t5655rec = new T5655rec();
	
	private int wsaaInd;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Map<String, List<Itempf>> t5655Map = null;
	private int ct02Value = 0;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readT56551020, 
		endT56551030
	}

	public Br614() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** This section is invoked in restart mode. Note section 1100 must*/
		/** clear each temporary file member as the members are not under*/
		/** commitment control. Note also that Splitter programs must have*/
		/** a Restart Method of 1 (re-run). Thus no updating should occur*/
		/** which would result in different records being returned from the*/
		/** primary file in a restart!*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		endT56551030();
	}

protected void initialise1010()
	{
		/*    Check the restart method is compatible with the program.*/
		/*    This program is restartable but will always re-run from*/
		/*    the beginning. This is because the temporary file members*/
		/*    are not under commitment control.*/
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*    Do a ovrdbf for each temporary file member. (Note we have*/
		/*    allowed for a maximum of 20.)*/
		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		/*    Since this program runs from a parameter screen move the*/
		/*    internal parameter area to the original parameter copybook*/
		/*    to retrieve the contract range*/
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum,SPACES)
		&& isEQ(p6671par.chdrnum1,SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		/*    The next thing we must do is construct the name of the*/
		/*    temporary file which we will be working with.*/
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		/*    Do a ovrdbf for each temporary file member. (Note we have*/
		/*    allowed for a maximum of 20.)*/
		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		/*    Log the number of threads being used*/
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		wsaaCompany.set(bsprIO.getCompany());
		/* Get the Maximum Auto Increase Lead Days*/
		wsaaMaxLeadDays.set(ZERO);
		
		t5655Map = itemDAO.loadSmartTable("IT", wsaaCompany.toString(), "T5655");
		readT56551020();
	}

protected void readT56551020()
	{
		if (t5655Map != null) {
			for (List<Itempf> itemList : t5655Map.values()) {
				for (Itempf i : itemList) {
					t5655rec.t5655Rec.set(StringUtil.rawToString(i.getGenarea()));
					if (isGT(t5655rec.leadDays, wsaaMaxLeadDays)) {
						wsaaMaxLeadDays.set(t5655rec.leadDays);
					}
				}
			}
		}
	}

protected void endT56551030()
	{
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(wsaaMaxLeadDays);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		wsaaSqlDate.set(datcon2rec.intDate2);
		String sqlwopxCursor = " SELECT  CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX" +
" FROM   " + getAppVars().getTableNameOverriden("COVRPF") + " " +
" WHERE CHDRCOY = ?" +
" AND CHDRNUM BETWEEN ? AND ?" +
" AND VALIDFLAG = ?" +
" AND RRTDAT <> 0" +
" AND RRTDAT < ?" +
" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX";
		/*    Initialise the array which will hold all the records*/
		/*    returned from each fetch. (INITIALIZE will not work as the*/
		/*    the array is indexed.*/
		iy.set(1);
		for (wsaaInd = 1; !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd++){
			initialize(wsaaWopxData[wsaaInd]);
		}
		wsaaInd = 1;
		try {
			sqlwopxCursorconn = getAppVars().getDBConnectionForTable(new com.csc.life.newbusiness.dataaccess.CovrpfTableDAM());
			sqlwopxCursorps = getAppVars().prepareStatementEmbeded(sqlwopxCursorconn, sqlwopxCursor, "COVRPF");
			getAppVars().setDBString(sqlwopxCursorps, 1, wsaaCompany);
			getAppVars().setDBString(sqlwopxCursorps, 2, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlwopxCursorps, 3, wsaaChdrnumTo);
			getAppVars().setDBString(sqlwopxCursorps, 4, wsaa1);
			getAppVars().setDBNumber(sqlwopxCursorps, 5, wsaaSqlDate);
			sqlwopxCursorrs = getAppVars().executeQuery(sqlwopxCursorps);
		}
		catch (SQLException ex){
			getAppVars().setSqlErrorCode(ex);
		}
	}

protected void openThreadMember1100()
	{
		/*    Clear the member for this thread in case we are in restart*/
		/*    mode in which case it might already contain data.*/
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(),1),iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaWopxFn, wsaaThread, wsaaStatuz);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.syserrType.set("2");
			syserrrec.subrname.set("CLRTMPF");
			syserrrec.statuz.set(wsaaStatuz);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(WOPX");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaWopxFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThread);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*    Open the file.*/
		if (isEQ(iz,1)) {
			wopx01.openOutput();
		}
		if (isEQ(iz,2)) {
			wopx02.openOutput();
		}
		if (isEQ(iz,3)) {
			wopx03.openOutput();
		}
		if (isEQ(iz,4)) {
			wopx04.openOutput();
		}
		if (isEQ(iz,5)) {
			wopx05.openOutput();
		}
		if (isEQ(iz,6)) {
			wopx06.openOutput();
		}
		if (isEQ(iz,7)) {
			wopx07.openOutput();
		}
		if (isEQ(iz,8)) {
			wopx08.openOutput();
		}
		if (isEQ(iz,9)) {
			wopx09.openOutput();
		}
		if (isEQ(iz,10)) {
			wopx10.openOutput();
		}
		if (isEQ(iz,11)) {
			wopx11.openOutput();
		}
		if (isEQ(iz,12)) {
			wopx12.openOutput();
		}
		if (isEQ(iz,13)) {
			wopx13.openOutput();
		}
		if (isEQ(iz,14)) {
			wopx14.openOutput();
		}
		if (isEQ(iz,15)) {
			wopx15.openOutput();
		}
		if (isEQ(iz,16)) {
			wopx16.openOutput();
		}
		if (isEQ(iz,17)) {
			wopx17.openOutput();
		}
		if (isEQ(iz,18)) {
			wopx18.openOutput();
		}
		if (isEQ(iz,19)) {
			wopx19.openOutput();
		}
		if (isEQ(iz,20)) {
			wopx20.openOutput();
		}
	}

protected void readFile2000()
	{
		/*  Now a block of records is fetched into the array.*/
		/*  Also on the first entry into the program we must set up the*/
		/*  WSAA-PREV-CHDRNUM = the present chdrnum.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd,1)) {
			return ;
		}
		try {
			for (int wopxCursorLoopIndex = 1; isLT(wopxCursorLoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlwopxCursorrs); wopxCursorLoopIndex++ ){
				getAppVars().getDBObject(sqlwopxCursorrs, 1, wsaaChdrcoy[wopxCursorLoopIndex], wsaaNullInd[wopxCursorLoopIndex][1]);
				getAppVars().getDBObject(sqlwopxCursorrs, 2, wsaaChdrnum[wopxCursorLoopIndex], wsaaNullInd[wopxCursorLoopIndex][2]);
				getAppVars().getDBObject(sqlwopxCursorrs, 3, wsaaLife[wopxCursorLoopIndex], wsaaNullInd[wopxCursorLoopIndex][3]);
				getAppVars().getDBObject(sqlwopxCursorrs, 4, wsaaCoverage[wopxCursorLoopIndex], wsaaNullInd[wopxCursorLoopIndex][4]);
				getAppVars().getDBObject(sqlwopxCursorrs, 5, wsaaRider[wopxCursorLoopIndex]);
				getAppVars().getDBObject(sqlwopxCursorrs, 6, wsaaPlnsfx[wopxCursorLoopIndex]);
			}
		}
		catch (SQLException ex){
			getAppVars().setSqlErrorCode(ex);
		}
		/*  We must detect :-*/
		/*      a) no rows returned on the first fetch*/
		/*      b) the last row returned (in the block)*/
		/*  IF Either of the above cases occur, then an*/
		/*      SQLCODE = +100 is returned.*/
		/*  The 3000 section is continued for case (b).*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaChdrnum[wsaaInd],SPACES)) {
			wsspEdterror.set(varcom.endp);
		}
		//tom chi modified, bug 1030
		else if(isEQ(wsaaChdrnum[wsaaInd],SPACES)){
			wsspEdterror.set(varcom.endp);
		}
		//end
		else {
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd]);
				wsaaFirstTime.set("N");
			}
		}
	}


protected void edit2500()
	{
		/*READ*/
		/*  Check record is required for processsing but do not do any othe*/
		/*  reads to any other file. The exception to this rule are reads t*/
		/*  ITEM and these should be kept to a mimimum. Do not do any soft*/
		/*  locking in Splitter programs. The soft lock should be done by*/
		/*  the subsequent program which reads the temporary file.*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/* In the update section we write a record to the temporary file*/
		/* member identified by the value of IY. If it is possible to*/
		/* to include the RRN from the primary file we should pass*/
		/* this data as the subsequent program can then use it to do*/
		/* a direct read which is faster than a normal read.*/
		wsaaInd = 1;
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		/*  Re-initialise the block for next fetch and point to first row.*/
		for (wsaaInd = 1; !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd++){
			initialize(wsaaWopxData[wsaaInd]);
		}
		wsaaInd = 1;
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*  If the the CHDRNUM being processed is not equal to the*/
		/*  to the previous we should move to the next output file member.*/
		/*  The condition is here to allow for checking the last chdrnum*/
		/*  of the old block with the first of the new and to move to the*/
		/*  next PAYX member to write to if they have changed.*/
		if (isNE(wsaaChdrnum[wsaaInd],wsaaPrevChdrnum)) {
			wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd]);
			iy.add(1);
		}
		/*  Load from storage all WOPX data for the same contract until*/
		/*  the CHDRNUM on WOPX has changed,*/
		/*    OR*/
		/*  The end of an incomplete block is reached.*/
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| isNE(wsaaChdrnum[wsaaInd],wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		wopxpfData.chdrcoy.set(wsaaChdrcoy[wsaaInd]);
		wopxpfData.chdrnum.set(wsaaChdrnum[wsaaInd]);
		wopxpfData.life.set(wsaaLife[wsaaInd]);
		wopxpfData.coverage.set(wsaaCoverage[wsaaInd]);
		wopxpfData.rider.set(wsaaRider[wsaaInd]);
		wopxpfData.planSuffix.set(wsaaPlnsfx[wsaaInd]);
		if (isGT(iy,bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		/*    Write records to their corresponding temporary file*/
		/*    members, determined by the working storage count, IY.*/
		/*    This spreads the transaction members evenly across*/
		/*    the thread members.*/
		if (isEQ(iy,1)) {
			wopx01.write(wopxpfData);
		}
		if (isEQ(iy,2)) {
			wopx02.write(wopxpfData);
		}
		if (isEQ(iy,3)) {
			wopx03.write(wopxpfData);
		}
		if (isEQ(iy,4)) {
			wopx04.write(wopxpfData);
		}
		if (isEQ(iy,5)) {
			wopx05.write(wopxpfData);
		}
		if (isEQ(iy,6)) {
			wopx06.write(wopxpfData);
		}
		if (isEQ(iy,7)) {
			wopx07.write(wopxpfData);
		}
		if (isEQ(iy,8)) {
			wopx08.write(wopxpfData);
		}
		if (isEQ(iy,9)) {
			wopx09.write(wopxpfData);
		}
		if (isEQ(iy,10)) {
			wopx10.write(wopxpfData);
		}
		if (isEQ(iy,11)) {
			wopx11.write(wopxpfData);
		}
		if (isEQ(iy,12)) {
			wopx12.write(wopxpfData);
		}
		if (isEQ(iy,13)) {
			wopx13.write(wopxpfData);
		}
		if (isEQ(iy,14)) {
			wopx14.write(wopxpfData);
		}
		if (isEQ(iy,15)) {
			wopx15.write(wopxpfData);
		}
		if (isEQ(iy,16)) {
			wopx16.write(wopxpfData);
		}
		if (isEQ(iy,17)) {
			wopx17.write(wopxpfData);
		}
		if (isEQ(iy,18)) {
			wopx18.write(wopxpfData);
		}
		if (isEQ(iy,19)) {
			wopx19.write(wopxpfData);
		}
		if (isEQ(iy,20)) {
			wopx20.write(wopxpfData);
		}
		/*    Log the number of extacted records.*/
		ct02Value++;
		/*  Set up the array for the next block of records.*/
		wsaaInd++;
		/*  Check for an incomplete block retrieved.*/
		if (isLTE(wsaaInd, wsaaRowsInBlock) && isEQ(wsaaChdrnum[wsaaInd], SPACES)) {
			wsaaEofInBlock.set("Y");
		}

	}


protected void commit3500()
	{
		/*COMMIT*/
		contotrec.totno.set(ct02);
		contotrec.totval.set(ct02Value);
		callProgram(Contot.class, contotrec.contotRec);	
		ct02Value = 0;
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		getAppVars().freeDBConnectionIgnoreErr(sqlwopxCursorconn, sqlwopxCursorps, sqlwopxCursorrs);
		/*  Close the open files and remove the overrides.*/
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		if (isEQ(iz,1)) {
			wopx01.close();
		}
		if (isEQ(iz,2)) {
			wopx02.close();
		}
		if (isEQ(iz,3)) {
			wopx03.close();
		}
		if (isEQ(iz,4)) {
			wopx04.close();
		}
		if (isEQ(iz,5)) {
			wopx05.close();
		}
		if (isEQ(iz,6)) {
			wopx06.close();
		}
		if (isEQ(iz,7)) {
			wopx07.close();
		}
		if (isEQ(iz,8)) {
			wopx08.close();
		}
		if (isEQ(iz,9)) {
			wopx09.close();
		}
		if (isEQ(iz,10)) {
			wopx10.close();
		}
		if (isEQ(iz,11)) {
			wopx11.close();
		}
		if (isEQ(iz,12)) {
			wopx12.close();
		}
		if (isEQ(iz,13)) {
			wopx13.close();
		}
		if (isEQ(iz,14)) {
			wopx14.close();
		}
		if (isEQ(iz,15)) {
			wopx15.close();
		}
		if (isEQ(iz,16)) {
			wopx16.close();
		}
		if (isEQ(iz,17)) {
			wopx17.close();
		}
		if (isEQ(iz,18)) {
			wopx18.close();
		}
		if (isEQ(iz,19)) {
			wopx19.close();
		}
		if (isEQ(iz,20)) {
			wopx20.close();
		}
	}
}
