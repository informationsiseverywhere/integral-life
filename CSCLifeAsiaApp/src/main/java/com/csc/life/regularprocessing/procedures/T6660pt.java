/*
 * File: T6660pt.java
 * Date: 30 August 2009 2:29:32
 * Author: Quipoz Limited
 * 
 * Class transformed from T6660PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.regularprocessing.tablestructures.T6660rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6660.
*
*
*****************************************************************
* </pre>
*/
public class T6660pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(44).isAPartOf(wsaaPrtLine001, 32, FILLER).init("Billing Currencies                     S6660");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(76);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init("  Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 46);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(41);
	private FixedLengthStringData filler7 = new FixedLengthStringData(41).isAPartOf(wsaaPrtLine003, 0, FILLER).init("   Contract Currency   Billing Currencies");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(71);
	private FixedLengthStringData filler8 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 10);
	private FixedLengthStringData filler9 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine004, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 23);
	private FixedLengthStringData filler10 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 28);
	private FixedLengthStringData filler11 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 33);
	private FixedLengthStringData filler12 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 38);
	private FixedLengthStringData filler13 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 43);
	private FixedLengthStringData filler14 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 48);
	private FixedLengthStringData filler15 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 53);
	private FixedLengthStringData filler16 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 58);
	private FixedLengthStringData filler17 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 63);
	private FixedLengthStringData filler18 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 66, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 68);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(71);
	private FixedLengthStringData filler19 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 10);
	private FixedLengthStringData filler20 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 23);
	private FixedLengthStringData filler21 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 28);
	private FixedLengthStringData filler22 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 33);
	private FixedLengthStringData filler23 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 38);
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 43);
	private FixedLengthStringData filler25 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 48);
	private FixedLengthStringData filler26 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 53);
	private FixedLengthStringData filler27 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 58);
	private FixedLengthStringData filler28 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 63);
	private FixedLengthStringData filler29 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 66, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 68);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(71);
	private FixedLengthStringData filler30 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 10);
	private FixedLengthStringData filler31 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine006, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 23);
	private FixedLengthStringData filler32 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 28);
	private FixedLengthStringData filler33 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 33);
	private FixedLengthStringData filler34 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 38);
	private FixedLengthStringData filler35 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 43);
	private FixedLengthStringData filler36 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 48);
	private FixedLengthStringData filler37 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 53);
	private FixedLengthStringData filler38 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 58);
	private FixedLengthStringData filler39 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 63);
	private FixedLengthStringData filler40 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 66, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 68);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(71);
	private FixedLengthStringData filler41 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 10);
	private FixedLengthStringData filler42 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 23);
	private FixedLengthStringData filler43 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 28);
	private FixedLengthStringData filler44 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 33);
	private FixedLengthStringData filler45 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 38);
	private FixedLengthStringData filler46 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 43);
	private FixedLengthStringData filler47 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 48);
	private FixedLengthStringData filler48 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 53);
	private FixedLengthStringData filler49 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 58);
	private FixedLengthStringData filler50 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 63);
	private FixedLengthStringData filler51 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 66, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 68);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(71);
	private FixedLengthStringData filler52 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 10);
	private FixedLengthStringData filler53 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine008, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 23);
	private FixedLengthStringData filler54 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 28);
	private FixedLengthStringData filler55 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 33);
	private FixedLengthStringData filler56 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 38);
	private FixedLengthStringData filler57 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo054 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 43);
	private FixedLengthStringData filler58 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 48);
	private FixedLengthStringData filler59 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 53);
	private FixedLengthStringData filler60 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 58);
	private FixedLengthStringData filler61 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 63);
	private FixedLengthStringData filler62 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 66, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo059 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 68);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(71);
	private FixedLengthStringData filler63 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo060 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 10);
	private FixedLengthStringData filler64 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo061 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 23);
	private FixedLengthStringData filler65 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo062 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 28);
	private FixedLengthStringData filler66 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo063 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 33);
	private FixedLengthStringData filler67 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo064 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 38);
	private FixedLengthStringData filler68 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo065 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 43);
	private FixedLengthStringData filler69 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo066 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 48);
	private FixedLengthStringData filler70 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo067 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 53);
	private FixedLengthStringData filler71 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo068 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 58);
	private FixedLengthStringData filler72 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo069 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 63);
	private FixedLengthStringData filler73 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 66, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo070 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 68);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(71);
	private FixedLengthStringData filler74 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo071 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 10);
	private FixedLengthStringData filler75 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine010, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo072 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 23);
	private FixedLengthStringData filler76 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo073 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 28);
	private FixedLengthStringData filler77 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo074 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 33);
	private FixedLengthStringData filler78 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo075 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 38);
	private FixedLengthStringData filler79 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo076 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 43);
	private FixedLengthStringData filler80 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo077 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 48);
	private FixedLengthStringData filler81 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo078 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 53);
	private FixedLengthStringData filler82 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo079 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 58);
	private FixedLengthStringData filler83 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo080 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 63);
	private FixedLengthStringData filler84 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 66, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo081 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 68);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(71);
	private FixedLengthStringData filler85 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo082 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 10);
	private FixedLengthStringData filler86 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo083 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 23);
	private FixedLengthStringData filler87 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo084 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 28);
	private FixedLengthStringData filler88 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo085 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 33);
	private FixedLengthStringData filler89 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo086 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 38);
	private FixedLengthStringData filler90 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo087 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 43);
	private FixedLengthStringData filler91 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo088 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 48);
	private FixedLengthStringData filler92 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo089 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 53);
	private FixedLengthStringData filler93 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo090 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 58);
	private FixedLengthStringData filler94 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo091 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 63);
	private FixedLengthStringData filler95 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 66, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo092 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 68);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(71);
	private FixedLengthStringData filler96 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo093 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 10);
	private FixedLengthStringData filler97 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo094 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 23);
	private FixedLengthStringData filler98 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 26, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo095 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 28);
	private FixedLengthStringData filler99 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo096 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 33);
	private FixedLengthStringData filler100 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 36, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo097 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 38);
	private FixedLengthStringData filler101 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo098 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 43);
	private FixedLengthStringData filler102 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo099 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 48);
	private FixedLengthStringData filler103 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo100 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 53);
	private FixedLengthStringData filler104 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo101 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 58);
	private FixedLengthStringData filler105 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo102 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 63);
	private FixedLengthStringData filler106 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 66, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo103 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 68);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6660rec t6660rec = new T6660rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6660pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6660rec.t6660Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo038.set(t6660rec.cntcurr04);
		fieldNo049.set(t6660rec.cntcurr05);
		fieldNo060.set(t6660rec.cntcurr06);
		fieldNo071.set(t6660rec.cntcurr07);
		fieldNo082.set(t6660rec.cntcurr08);
		fieldNo093.set(t6660rec.cntcurr09);
		fieldNo005.set(t6660rec.cntcurr01);
		fieldNo016.set(t6660rec.cntcurr02);
		fieldNo027.set(t6660rec.cntcurr03);
		fieldNo006.set(t6660rec.acctccy01);
		fieldNo007.set(t6660rec.acctccy02);
		fieldNo008.set(t6660rec.acctccy03);
		fieldNo009.set(t6660rec.acctccy04);
		fieldNo010.set(t6660rec.acctccy05);
		fieldNo011.set(t6660rec.acctccy06);
		fieldNo012.set(t6660rec.acctccy07);
		fieldNo013.set(t6660rec.acctccy08);
		fieldNo014.set(t6660rec.acctccy09);
		fieldNo015.set(t6660rec.acctccy10);
		fieldNo017.set(t6660rec.acctccy11);
		fieldNo018.set(t6660rec.acctccy12);
		fieldNo019.set(t6660rec.acctccy13);
		fieldNo020.set(t6660rec.acctccy14);
		fieldNo021.set(t6660rec.acctccy15);
		fieldNo022.set(t6660rec.acctccy16);
		fieldNo023.set(t6660rec.acctccy17);
		fieldNo024.set(t6660rec.acctccy18);
		fieldNo025.set(t6660rec.acctccy19);
		fieldNo026.set(t6660rec.acctccy20);
		fieldNo028.set(t6660rec.acctccy21);
		fieldNo029.set(t6660rec.acctccy22);
		fieldNo030.set(t6660rec.acctccy23);
		fieldNo031.set(t6660rec.acctccy24);
		fieldNo032.set(t6660rec.acctccy25);
		fieldNo033.set(t6660rec.acctccy26);
		fieldNo034.set(t6660rec.acctccy27);
		fieldNo035.set(t6660rec.acctccy28);
		fieldNo036.set(t6660rec.acctccy29);
		fieldNo037.set(t6660rec.acctccy30);
		fieldNo039.set(t6660rec.acctccy31);
		fieldNo040.set(t6660rec.acctccy32);
		fieldNo041.set(t6660rec.acctccy33);
		fieldNo042.set(t6660rec.acctccy34);
		fieldNo043.set(t6660rec.acctccy35);
		fieldNo044.set(t6660rec.acctccy36);
		fieldNo045.set(t6660rec.acctccy37);
		fieldNo046.set(t6660rec.acctccy38);
		fieldNo047.set(t6660rec.acctccy39);
		fieldNo048.set(t6660rec.acctccy40);
		fieldNo050.set(t6660rec.acctccy41);
		fieldNo051.set(t6660rec.acctccy42);
		fieldNo052.set(t6660rec.acctccy43);
		fieldNo053.set(t6660rec.acctccy44);
		fieldNo054.set(t6660rec.acctccy45);
		fieldNo055.set(t6660rec.acctccy46);
		fieldNo056.set(t6660rec.acctccy47);
		fieldNo057.set(t6660rec.acctccy48);
		fieldNo058.set(t6660rec.acctccy49);
		fieldNo059.set(t6660rec.acctccy50);
		fieldNo061.set(t6660rec.acctccy51);
		fieldNo062.set(t6660rec.acctccy52);
		fieldNo063.set(t6660rec.acctccy53);
		fieldNo064.set(t6660rec.acctccy54);
		fieldNo065.set(t6660rec.acctccy55);
		fieldNo066.set(t6660rec.acctccy56);
		fieldNo067.set(t6660rec.acctccy57);
		fieldNo068.set(t6660rec.acctccy58);
		fieldNo069.set(t6660rec.acctccy59);
		fieldNo070.set(t6660rec.acctccy60);
		fieldNo072.set(t6660rec.acctccy61);
		fieldNo073.set(t6660rec.acctccy62);
		fieldNo074.set(t6660rec.acctccy63);
		fieldNo075.set(t6660rec.acctccy64);
		fieldNo076.set(t6660rec.acctccy65);
		fieldNo077.set(t6660rec.acctccy66);
		fieldNo078.set(t6660rec.acctccy67);
		fieldNo079.set(t6660rec.acctccy68);
		fieldNo080.set(t6660rec.acctccy69);
		fieldNo081.set(t6660rec.acctccy70);
		fieldNo083.set(t6660rec.acctccy71);
		fieldNo084.set(t6660rec.acctccy72);
		fieldNo085.set(t6660rec.acctccy73);
		fieldNo086.set(t6660rec.acctccy74);
		fieldNo087.set(t6660rec.acctccy75);
		fieldNo088.set(t6660rec.acctccy76);
		fieldNo089.set(t6660rec.acctccy77);
		fieldNo090.set(t6660rec.acctccy78);
		fieldNo091.set(t6660rec.acctccy79);
		fieldNo092.set(t6660rec.acctccy80);
		fieldNo094.set(t6660rec.acctccy81);
		fieldNo095.set(t6660rec.acctccy82);
		fieldNo096.set(t6660rec.acctccy83);
		fieldNo097.set(t6660rec.acctccy84);
		fieldNo098.set(t6660rec.acctccy85);
		fieldNo099.set(t6660rec.acctccy86);
		fieldNo100.set(t6660rec.acctccy87);
		fieldNo101.set(t6660rec.acctccy88);
		fieldNo102.set(t6660rec.acctccy89);
		fieldNo103.set(t6660rec.acctccy90);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
