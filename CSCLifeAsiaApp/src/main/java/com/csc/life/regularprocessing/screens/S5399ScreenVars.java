package com.csc.life.regularprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5399
 * @version 1.0 generated on 30/08/09 06:39
 * @author Quipoz
 */
public class S5399ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(972);
	public FixedLengthStringData dataFields = new FixedLengthStringData(140).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData covPremStats = new FixedLengthStringData(24).isAPartOf(dataFields, 1);
	public FixedLengthStringData[] covPremStat = FLSArrayPartOfStructure(12, 2, covPremStats, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(covPremStats, 0, FILLER_REDEFINE);
	public FixedLengthStringData covPremStat01 = DD.crpstat.copy().isAPartOf(filler,0);
	public FixedLengthStringData covPremStat02 = DD.crpstat.copy().isAPartOf(filler,2);
	public FixedLengthStringData covPremStat03 = DD.crpstat.copy().isAPartOf(filler,4);
	public FixedLengthStringData covPremStat04 = DD.crpstat.copy().isAPartOf(filler,6);
	public FixedLengthStringData covPremStat05 = DD.crpstat.copy().isAPartOf(filler,8);
	public FixedLengthStringData covPremStat06 = DD.crpstat.copy().isAPartOf(filler,10);
	public FixedLengthStringData covPremStat07 = DD.crpstat.copy().isAPartOf(filler,12);
	public FixedLengthStringData covPremStat08 = DD.crpstat.copy().isAPartOf(filler,14);
	public FixedLengthStringData covPremStat09 = DD.crpstat.copy().isAPartOf(filler,16);
	public FixedLengthStringData covPremStat10 = DD.crpstat.copy().isAPartOf(filler,18);
	public FixedLengthStringData covPremStat11 = DD.crpstat.copy().isAPartOf(filler,20);
	public FixedLengthStringData covPremStat12 = DD.crpstat.copy().isAPartOf(filler,22);
	public FixedLengthStringData covRiskStats = new FixedLengthStringData(24).isAPartOf(dataFields, 25);
	public FixedLengthStringData[] covRiskStat = FLSArrayPartOfStructure(12, 2, covRiskStats, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(covRiskStats, 0, FILLER_REDEFINE);
	public FixedLengthStringData covRiskStat01 = DD.crrstat.copy().isAPartOf(filler1,0);
	public FixedLengthStringData covRiskStat02 = DD.crrstat.copy().isAPartOf(filler1,2);
	public FixedLengthStringData covRiskStat03 = DD.crrstat.copy().isAPartOf(filler1,4);
	public FixedLengthStringData covRiskStat04 = DD.crrstat.copy().isAPartOf(filler1,6);
	public FixedLengthStringData covRiskStat05 = DD.crrstat.copy().isAPartOf(filler1,8);
	public FixedLengthStringData covRiskStat06 = DD.crrstat.copy().isAPartOf(filler1,10);
	public FixedLengthStringData covRiskStat07 = DD.crrstat.copy().isAPartOf(filler1,12);
	public FixedLengthStringData covRiskStat08 = DD.crrstat.copy().isAPartOf(filler1,14);
	public FixedLengthStringData covRiskStat09 = DD.crrstat.copy().isAPartOf(filler1,16);
	public FixedLengthStringData covRiskStat10 = DD.crrstat.copy().isAPartOf(filler1,18);
	public FixedLengthStringData covRiskStat11 = DD.crrstat.copy().isAPartOf(filler1,20);
	public FixedLengthStringData covRiskStat12 = DD.crrstat.copy().isAPartOf(filler1,22);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData setCnPremStats = new FixedLengthStringData(24).isAPartOf(dataFields, 87);
	public FixedLengthStringData[] setCnPremStat = FLSArrayPartOfStructure(12, 2, setCnPremStats, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(setCnPremStats, 0, FILLER_REDEFINE);
	public FixedLengthStringData setCnPremStat01 = DD.scnpstat.copy().isAPartOf(filler2,0);
	public FixedLengthStringData setCnPremStat02 = DD.scnpstat.copy().isAPartOf(filler2,2);
	public FixedLengthStringData setCnPremStat03 = DD.scnpstat.copy().isAPartOf(filler2,4);
	public FixedLengthStringData setCnPremStat04 = DD.scnpstat.copy().isAPartOf(filler2,6);
	public FixedLengthStringData setCnPremStat05 = DD.scnpstat.copy().isAPartOf(filler2,8);
	public FixedLengthStringData setCnPremStat06 = DD.scnpstat.copy().isAPartOf(filler2,10);
	public FixedLengthStringData setCnPremStat07 = DD.scnpstat.copy().isAPartOf(filler2,12);
	public FixedLengthStringData setCnPremStat08 = DD.scnpstat.copy().isAPartOf(filler2,14);
	public FixedLengthStringData setCnPremStat09 = DD.scnpstat.copy().isAPartOf(filler2,16);
	public FixedLengthStringData setCnPremStat10 = DD.scnpstat.copy().isAPartOf(filler2,18);
	public FixedLengthStringData setCnPremStat11 = DD.scnpstat.copy().isAPartOf(filler2,20);
	public FixedLengthStringData setCnPremStat12 = DD.scnpstat.copy().isAPartOf(filler2,22);
	public FixedLengthStringData setCnRiskStats = new FixedLengthStringData(24).isAPartOf(dataFields, 111);
	public FixedLengthStringData[] setCnRiskStat = FLSArrayPartOfStructure(12, 2, setCnRiskStats, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(24).isAPartOf(setCnRiskStats, 0, FILLER_REDEFINE);
	public FixedLengthStringData setCnRiskStat01 = DD.scnrstat.copy().isAPartOf(filler3,0);
	public FixedLengthStringData setCnRiskStat02 = DD.scnrstat.copy().isAPartOf(filler3,2);
	public FixedLengthStringData setCnRiskStat03 = DD.scnrstat.copy().isAPartOf(filler3,4);
	public FixedLengthStringData setCnRiskStat04 = DD.scnrstat.copy().isAPartOf(filler3,6);
	public FixedLengthStringData setCnRiskStat05 = DD.scnrstat.copy().isAPartOf(filler3,8);
	public FixedLengthStringData setCnRiskStat06 = DD.scnrstat.copy().isAPartOf(filler3,10);
	public FixedLengthStringData setCnRiskStat07 = DD.scnrstat.copy().isAPartOf(filler3,12);
	public FixedLengthStringData setCnRiskStat08 = DD.scnrstat.copy().isAPartOf(filler3,14);
	public FixedLengthStringData setCnRiskStat09 = DD.scnrstat.copy().isAPartOf(filler3,16);
	public FixedLengthStringData setCnRiskStat10 = DD.scnrstat.copy().isAPartOf(filler3,18);
	public FixedLengthStringData setCnRiskStat11 = DD.scnrstat.copy().isAPartOf(filler3,20);
	public FixedLengthStringData setCnRiskStat12 = DD.scnrstat.copy().isAPartOf(filler3,22);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,135);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(208).isAPartOf(dataArea, 140);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData crpstatsErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] crpstatErr = FLSArrayPartOfStructure(12, 4, crpstatsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(48).isAPartOf(crpstatsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData crpstat01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData crpstat02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData crpstat03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData crpstat04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData crpstat05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData crpstat06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData crpstat07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData crpstat08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData crpstat09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData crpstat10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData crpstat11Err = new FixedLengthStringData(4).isAPartOf(filler4, 40);
	public FixedLengthStringData crpstat12Err = new FixedLengthStringData(4).isAPartOf(filler4, 44);
	public FixedLengthStringData crrstatsErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData[] crrstatErr = FLSArrayPartOfStructure(12, 4, crrstatsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(48).isAPartOf(crrstatsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData crrstat01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData crrstat02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData crrstat03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData crrstat04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData crrstat05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData crrstat06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData crrstat07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData crrstat08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData crrstat09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData crrstat10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData crrstat11Err = new FixedLengthStringData(4).isAPartOf(filler5, 40);
	public FixedLengthStringData crrstat12Err = new FixedLengthStringData(4).isAPartOf(filler5, 44);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData scnpstatsErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData[] scnpstatErr = FLSArrayPartOfStructure(12, 4, scnpstatsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(48).isAPartOf(scnpstatsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData scnpstat01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData scnpstat02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData scnpstat03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData scnpstat04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData scnpstat05Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);
	public FixedLengthStringData scnpstat06Err = new FixedLengthStringData(4).isAPartOf(filler6, 20);
	public FixedLengthStringData scnpstat07Err = new FixedLengthStringData(4).isAPartOf(filler6, 24);
	public FixedLengthStringData scnpstat08Err = new FixedLengthStringData(4).isAPartOf(filler6, 28);
	public FixedLengthStringData scnpstat09Err = new FixedLengthStringData(4).isAPartOf(filler6, 32);
	public FixedLengthStringData scnpstat10Err = new FixedLengthStringData(4).isAPartOf(filler6, 36);
	public FixedLengthStringData scnpstat11Err = new FixedLengthStringData(4).isAPartOf(filler6, 40);
	public FixedLengthStringData scnpstat12Err = new FixedLengthStringData(4).isAPartOf(filler6, 44);
	public FixedLengthStringData scnrstatsErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData[] scnrstatErr = FLSArrayPartOfStructure(12, 4, scnrstatsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(48).isAPartOf(scnrstatsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData scnrstat01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData scnrstat02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData scnrstat03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData scnrstat04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData scnrstat05Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);
	public FixedLengthStringData scnrstat06Err = new FixedLengthStringData(4).isAPartOf(filler7, 20);
	public FixedLengthStringData scnrstat07Err = new FixedLengthStringData(4).isAPartOf(filler7, 24);
	public FixedLengthStringData scnrstat08Err = new FixedLengthStringData(4).isAPartOf(filler7, 28);
	public FixedLengthStringData scnrstat09Err = new FixedLengthStringData(4).isAPartOf(filler7, 32);
	public FixedLengthStringData scnrstat10Err = new FixedLengthStringData(4).isAPartOf(filler7, 36);
	public FixedLengthStringData scnrstat11Err = new FixedLengthStringData(4).isAPartOf(filler7, 40);
	public FixedLengthStringData scnrstat12Err = new FixedLengthStringData(4).isAPartOf(filler7, 44);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(624).isAPartOf(dataArea, 348);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData crpstatsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] crpstatOut = FLSArrayPartOfStructure(12, 12, crpstatsOut, 0);
	public FixedLengthStringData[][] crpstatO = FLSDArrayPartOfArrayStructure(12, 1, crpstatOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(144).isAPartOf(crpstatsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] crpstat01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] crpstat02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] crpstat03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] crpstat04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] crpstat05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] crpstat06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] crpstat07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] crpstat08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] crpstat09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] crpstat10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public FixedLengthStringData[] crpstat11Out = FLSArrayPartOfStructure(12, 1, filler8, 120);
	public FixedLengthStringData[] crpstat12Out = FLSArrayPartOfStructure(12, 1, filler8, 132);
	public FixedLengthStringData crrstatsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 156);
	public FixedLengthStringData[] crrstatOut = FLSArrayPartOfStructure(12, 12, crrstatsOut, 0);
	public FixedLengthStringData[][] crrstatO = FLSDArrayPartOfArrayStructure(12, 1, crrstatOut, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(144).isAPartOf(crrstatsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] crrstat01Out = FLSArrayPartOfStructure(12, 1, filler9, 0);
	public FixedLengthStringData[] crrstat02Out = FLSArrayPartOfStructure(12, 1, filler9, 12);
	public FixedLengthStringData[] crrstat03Out = FLSArrayPartOfStructure(12, 1, filler9, 24);
	public FixedLengthStringData[] crrstat04Out = FLSArrayPartOfStructure(12, 1, filler9, 36);
	public FixedLengthStringData[] crrstat05Out = FLSArrayPartOfStructure(12, 1, filler9, 48);
	public FixedLengthStringData[] crrstat06Out = FLSArrayPartOfStructure(12, 1, filler9, 60);
	public FixedLengthStringData[] crrstat07Out = FLSArrayPartOfStructure(12, 1, filler9, 72);
	public FixedLengthStringData[] crrstat08Out = FLSArrayPartOfStructure(12, 1, filler9, 84);
	public FixedLengthStringData[] crrstat09Out = FLSArrayPartOfStructure(12, 1, filler9, 96);
	public FixedLengthStringData[] crrstat10Out = FLSArrayPartOfStructure(12, 1, filler9, 108);
	public FixedLengthStringData[] crrstat11Out = FLSArrayPartOfStructure(12, 1, filler9, 120);
	public FixedLengthStringData[] crrstat12Out = FLSArrayPartOfStructure(12, 1, filler9, 132);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData scnpstatsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 324);
	public FixedLengthStringData[] scnpstatOut = FLSArrayPartOfStructure(12, 12, scnpstatsOut, 0);
	public FixedLengthStringData[][] scnpstatO = FLSDArrayPartOfArrayStructure(12, 1, scnpstatOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(144).isAPartOf(scnpstatsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] scnpstat01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] scnpstat02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] scnpstat03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] scnpstat04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] scnpstat05Out = FLSArrayPartOfStructure(12, 1, filler10, 48);
	public FixedLengthStringData[] scnpstat06Out = FLSArrayPartOfStructure(12, 1, filler10, 60);
	public FixedLengthStringData[] scnpstat07Out = FLSArrayPartOfStructure(12, 1, filler10, 72);
	public FixedLengthStringData[] scnpstat08Out = FLSArrayPartOfStructure(12, 1, filler10, 84);
	public FixedLengthStringData[] scnpstat09Out = FLSArrayPartOfStructure(12, 1, filler10, 96);
	public FixedLengthStringData[] scnpstat10Out = FLSArrayPartOfStructure(12, 1, filler10, 108);
	public FixedLengthStringData[] scnpstat11Out = FLSArrayPartOfStructure(12, 1, filler10, 120);
	public FixedLengthStringData[] scnpstat12Out = FLSArrayPartOfStructure(12, 1, filler10, 132);
	public FixedLengthStringData scnrstatsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 468);
	public FixedLengthStringData[] scnrstatOut = FLSArrayPartOfStructure(12, 12, scnrstatsOut, 0);
	public FixedLengthStringData[][] scnrstatO = FLSDArrayPartOfArrayStructure(12, 1, scnrstatOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(144).isAPartOf(scnrstatsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] scnrstat01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] scnrstat02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] scnrstat03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] scnrstat04Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public FixedLengthStringData[] scnrstat05Out = FLSArrayPartOfStructure(12, 1, filler11, 48);
	public FixedLengthStringData[] scnrstat06Out = FLSArrayPartOfStructure(12, 1, filler11, 60);
	public FixedLengthStringData[] scnrstat07Out = FLSArrayPartOfStructure(12, 1, filler11, 72);
	public FixedLengthStringData[] scnrstat08Out = FLSArrayPartOfStructure(12, 1, filler11, 84);
	public FixedLengthStringData[] scnrstat09Out = FLSArrayPartOfStructure(12, 1, filler11, 96);
	public FixedLengthStringData[] scnrstat10Out = FLSArrayPartOfStructure(12, 1, filler11, 108);
	public FixedLengthStringData[] scnrstat11Out = FLSArrayPartOfStructure(12, 1, filler11, 120);
	public FixedLengthStringData[] scnrstat12Out = FLSArrayPartOfStructure(12, 1, filler11, 132);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5399screenWritten = new LongData(0);
	public LongData S5399protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5399ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, covRiskStat01, covRiskStat02, covRiskStat03, covRiskStat04, covRiskStat05, covRiskStat06, covRiskStat07, covRiskStat08, covRiskStat09, covRiskStat10, covRiskStat11, covRiskStat12, setCnRiskStat01, setCnRiskStat02, setCnRiskStat03, setCnRiskStat04, setCnRiskStat05, setCnRiskStat06, setCnRiskStat07, setCnRiskStat08, setCnRiskStat09, setCnRiskStat10, setCnRiskStat11, setCnRiskStat12, covPremStat01, setCnPremStat01, covPremStat02, setCnPremStat02, covPremStat03, setCnPremStat03, covPremStat04, setCnPremStat04, covPremStat05, setCnPremStat05, covPremStat06, setCnPremStat06, covPremStat07, setCnPremStat07, covPremStat08, setCnPremStat08, covPremStat09, setCnPremStat09, covPremStat10, setCnPremStat10, covPremStat11, setCnPremStat11, covPremStat12, setCnPremStat12};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, crrstat01Out, crrstat02Out, crrstat03Out, crrstat04Out, crrstat05Out, crrstat06Out, crrstat07Out, crrstat08Out, crrstat09Out, crrstat10Out, crrstat11Out, crrstat12Out, scnrstat01Out, scnrstat02Out, scnrstat03Out, scnrstat04Out, scnrstat05Out, scnrstat06Out, scnrstat07Out, scnrstat08Out, scnrstat09Out, scnrstat10Out, scnrstat11Out, scnrstat12Out, crpstat01Out, scnpstat01Out, crpstat02Out, scnpstat02Out, crpstat03Out, scnpstat03Out, crpstat04Out, scnpstat04Out, crpstat05Out, scnpstat05Out, crpstat06Out, scnpstat06Out, crpstat07Out, scnpstat07Out, crpstat08Out, scnpstat08Out, crpstat09Out, scnpstat09Out, crpstat10Out, scnpstat10Out, crpstat11Out, scnpstat11Out, crpstat12Out, scnpstat12Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, crrstat01Err, crrstat02Err, crrstat03Err, crrstat04Err, crrstat05Err, crrstat06Err, crrstat07Err, crrstat08Err, crrstat09Err, crrstat10Err, crrstat11Err, crrstat12Err, scnrstat01Err, scnrstat02Err, scnrstat03Err, scnrstat04Err, scnrstat05Err, scnrstat06Err, scnrstat07Err, scnrstat08Err, scnrstat09Err, scnrstat10Err, scnrstat11Err, scnrstat12Err, crpstat01Err, scnpstat01Err, crpstat02Err, scnpstat02Err, crpstat03Err, scnpstat03Err, crpstat04Err, scnpstat04Err, crpstat05Err, scnpstat05Err, crpstat06Err, scnpstat06Err, crpstat07Err, scnpstat07Err, crpstat08Err, scnpstat08Err, crpstat09Err, scnpstat09Err, crpstat10Err, scnpstat10Err, crpstat11Err, scnpstat11Err, crpstat12Err, scnpstat12Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5399screen.class;
		protectRecord = S5399protect.class;
	}

}
