/*
 * File: T6626pt.java
 * Date: 30 August 2009 2:27:37
 * Author: Quipoz Limited
 * 
 * Class transformed from T6626PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.regularprocessing.tablestructures.T6626rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6626.
*
*
*****************************************************************
* </pre>
*/
public class T6626pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(54).isAPartOf(wsaaPrtLine001, 22, FILLER).init("Miscellaneous Transaction Reversal Stop Codes    S6626");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(53);
	private FixedLengthStringData filler7 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine003, 17, FILLER).init("Billing transaction Code . . .");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine003, 49);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(74);
	private FixedLengthStringData filler9 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(67).isAPartOf(wsaaPrtLine004, 7, FILLER).init("Collection      Issue               Other               Other");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(74);
	private FixedLengthStringData filler11 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(65).isAPartOf(wsaaPrtLine005, 9, FILLER).init("Codes    Codes               Codes               Codes");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(73);
	private FixedLengthStringData filler13 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 9);
	private FixedLengthStringData filler14 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine006, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 29);
	private FixedLengthStringData filler15 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine006, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 49);
	private FixedLengthStringData filler16 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine006, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 69);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(73);
	private FixedLengthStringData filler17 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 9);
	private FixedLengthStringData filler18 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine007, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 29);
	private FixedLengthStringData filler19 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine007, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 49);
	private FixedLengthStringData filler20 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine007, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 69);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(73);
	private FixedLengthStringData filler21 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 9);
	private FixedLengthStringData filler22 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine008, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 29);
	private FixedLengthStringData filler23 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine008, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 49);
	private FixedLengthStringData filler24 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine008, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 69);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(73);
	private FixedLengthStringData filler25 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 9);
	private FixedLengthStringData filler26 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine009, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 29);
	private FixedLengthStringData filler27 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine009, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 49);
	private FixedLengthStringData filler28 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine009, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 69);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(73);
	private FixedLengthStringData filler29 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 9);
	private FixedLengthStringData filler30 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine010, 13, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 29);
	private FixedLengthStringData filler31 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine010, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 49);
	private FixedLengthStringData filler32 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine010, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 69);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6626rec t6626rec = new T6626rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6626pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6626rec.t6626Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo006.set(t6626rec.transcd01);
		fieldNo007.set(t6626rec.transcd06);
		fieldNo011.set(t6626rec.transcd07);
		fieldNo015.set(t6626rec.transcd08);
		fieldNo019.set(t6626rec.transcd09);
		fieldNo023.set(t6626rec.transcd10);
		fieldNo010.set(t6626rec.transcd02);
		fieldNo014.set(t6626rec.transcd03);
		fieldNo018.set(t6626rec.transcd04);
		fieldNo022.set(t6626rec.transcd05);
		fieldNo005.set(t6626rec.trcode);
		fieldNo008.set(t6626rec.trncd01);
		fieldNo012.set(t6626rec.trncd02);
		fieldNo016.set(t6626rec.trncd03);
		fieldNo020.set(t6626rec.trncd04);
		fieldNo024.set(t6626rec.trncd05);
		fieldNo009.set(t6626rec.trncd06);
		fieldNo013.set(t6626rec.trncd07);
		fieldNo017.set(t6626rec.trncd08);
		fieldNo021.set(t6626rec.trncd09);
		fieldNo025.set(t6626rec.trncd10);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
