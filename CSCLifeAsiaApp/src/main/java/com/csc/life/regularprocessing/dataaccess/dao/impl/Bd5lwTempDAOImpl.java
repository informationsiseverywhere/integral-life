package com.csc.life.regularprocessing.dataaccess.dao.impl;


import com.csc.life.regularprocessing.dataaccess.dao.Bd5lwTempDAO;
import com.csc.life.regularprocessing.dataaccess.model.Bd5lwDTO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Bd5lwTempDAOImpl extends BaseDAOImpl<Object> implements Bd5lwTempDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Bd5lwTempDAOImpl.class);

	public boolean deleteBd5lwTempAllRecords(){
			StringBuilder sb = new StringBuilder("DELETE FROM BD5LWDATA");
			LOGGER.info(sb.toString());
			PreparedStatement ps = null;
			ResultSet rs = null;
			try{
				ps = getPrepareStatement(sb.toString());	
	            ps.executeUpdate();
			}catch (SQLException e) {
				LOGGER.error("deleteBd5lwTempAllRecords()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}
			return true;
	}
	
	public boolean insertBd5lwTempRecords(String coy, String chdrnumlow, String chdrnumhigh){
		StringBuilder sb = new StringBuilder("insert into BD5LWDATA ");
		sb.append("select * from ( ");
		sb.append("select AC.RLDGCOY, AC.RLDGACCT, AC.SACSCODE, AC.SACSTYP, AC.SACSCURBAL, CH.CHDRCOY, CH.CHDRNUM, CH.CNTBRANCH, CH.CNTTYPE, ");
		sb.append("CH.STATCODE, CH.OCCDATE, CH.PTDATE, CH.POLSUM, CH.CNTCURR ");
		sb.append("FROM ACBLPF AC ");
		sb.append("INNER JOIN CHDRPF CH ON CH.CHDRCOY = AC.RLDGCOY AND CH.CHDRNUM = SUBSTR(AC.RLDGACCT,1,8) AND CH.VALIDFLAG ='1' AND CH.STATCODE IN ('IF','PU','RH') ");
		sb.append("WHERE CH.CHDRCOY = ? AND CH.CHDRNUM >= ?  AND CH.CHDRNUM <= ? ");
		sb.append(" AND ((AC.SACSCODE = 'LN' AND AC.SACSTYP ='AO') OR (AC.SACSCODE = 'LN' AND AC.SACSTYP ='LO')) AND SACSCURBAL <> 0 ");
		sb.append(") acbl order by CHDRCOY, CHDRNUM desc");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, coy);
			ps.setString(2, chdrnumlow);
			ps.setString(3, chdrnumhigh);
            ps.executeUpdate();
		}catch (SQLException e) {
			LOGGER.error("insertBd5lwTempRecords()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return true;
	}
	
	public List<Bd5lwDTO> loadData(int minRecord, int maxRecord){
		StringBuilder sb = new StringBuilder("SELECT * FROM (");
		sb.append(" SELECT row_number()over(order by CHDRCOY, CHDRNUM desc) ROWNM, RLDGCOY, RLDGACCT, SACSCODE, SACSTYP, SACSCURBAL,");
		sb.append(" CHDRCOY, CHDRNUM, CNTBRANCH, CNTTYPE, STATCODE, OCCDATE, PTDATE, POLSUM, CNTCURR ");
		sb.append(" FROM BD5LWDATA");
		sb.append(") TEMP WHERE ROWNM >=? and ROWNM <? ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		List<Bd5lwDTO> bd5lwList = new ArrayList<Bd5lwDTO>();
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setInt(1, minRecord);
			ps.setInt(2, maxRecord);
			rs = ps.executeQuery();
			while(rs.next()){
				Bd5lwDTO bd5lwDTO = new Bd5lwDTO();
				bd5lwDTO.setRldgcoy(rs.getString("RLDGCOY").trim());
				bd5lwDTO.setRldgacct(rs.getString("RLDGACCT").trim());
				bd5lwDTO.setSacscode(rs.getString("SACSCODE").trim());
				bd5lwDTO.setSacstyp(rs.getString("SACSTYP").trim());
				bd5lwDTO.setSacscurbal(rs.getBigDecimal("SACSCURBAL"));
				bd5lwDTO.setChdrcoy(rs.getString("CHDRCOY").trim());
				bd5lwDTO.setChdrnum(rs.getString("CHDRNUM").trim());
				bd5lwDTO.setCntbranch(rs.getString("CNTBRANCH").trim());
				bd5lwDTO.setCnttype(rs.getString("CNTTYPE").trim());
				bd5lwDTO.setStatcode(rs.getString("STATCODE").trim());
				bd5lwDTO.setOccdate(rs.getInt("OCCDATE"));
				bd5lwDTO.setPtdate(rs.getInt("PTDATE"));
				bd5lwDTO.setPolsum(rs.getObject("POLSUM") != null ? (rs.getShort("POLSUM")) : 0);
				bd5lwDTO.setCntcurr(rs.getString("CNTCURR").trim());
				bd5lwList.add(bd5lwDTO);
			}
		}catch (SQLException e) {
			LOGGER.error("loadDataByEffdate()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return bd5lwList;
	}
	

}
