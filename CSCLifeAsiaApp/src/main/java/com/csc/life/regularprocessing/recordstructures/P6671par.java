package com.csc.life.regularprocessing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:31
 * Description:
 * Copybook name: P6671PAR
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class P6671par extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData parmRecord = new FixedLengthStringData(16);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(parmRecord, 0);
  	public FixedLengthStringData chdrnum1 = new FixedLengthStringData(8).isAPartOf(parmRecord, 8);


	public void initialize() {
		COBOLFunctions.initialize(parmRecord);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRecord.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}