package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.Inczpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface InczpfDAO extends BaseDAO<Inczpf> {
    public List<Inczpf> searchInczRecord(String tableId, String memName, int batchExtractSize, int batchID);
}