package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:53
 * Description:
 * Copybook name: DDSURNLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ddsurnlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ddsurnlFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ddsurnlKey = new FixedLengthStringData(64).isAPartOf(ddsurnlFileKey, 0, REDEFINE);
  	public FixedLengthStringData ddsurnlPayrcoy = new FixedLengthStringData(1).isAPartOf(ddsurnlKey, 0);
  	public FixedLengthStringData ddsurnlPayrnum = new FixedLengthStringData(8).isAPartOf(ddsurnlKey, 1);
  	public FixedLengthStringData ddsurnlMandref = new FixedLengthStringData(5).isAPartOf(ddsurnlKey, 9);
  	public PackedDecimalData ddsurnlBillcd = new PackedDecimalData(8, 0).isAPartOf(ddsurnlKey, 14);
  	public FixedLengthStringData ddsurnlMandstat = new FixedLengthStringData(2).isAPartOf(ddsurnlKey, 19);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(ddsurnlKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ddsurnlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ddsurnlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}