package com.csc.life.regularprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UstxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:51
 * Class transformed from USTXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UstxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 31;
	public FixedLengthStringData ustxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData ustxpfRecord = ustxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(ustxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(ustxrec);
	public PackedDecimalData statementDate = DD.stmdte.copy().isAPartOf(ustxrec);
	public PackedDecimalData ptdate = DD.ptdate.copy().isAPartOf(ustxrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(ustxrec);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(ustxrec);
	public FixedLengthStringData pstatcode = DD.pstcde.copy().isAPartOf(ustxrec);
	public PackedDecimalData occdate = DD.occdate.copy().isAPartOf(ustxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UstxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for UstxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UstxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UstxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UstxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UstxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UstxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("USTXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"STMDTE, " +
							"PTDATE, " +
							"CNTTYPE, " +
							"STATCODE, " +
							"PSTCDE, " +
							"OCCDATE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     statementDate,
                                     ptdate,
                                     cnttype,
                                     statcode,
                                     pstatcode,
                                     occdate,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		statementDate.clear();
  		ptdate.clear();
  		cnttype.clear();
  		statcode.clear();
  		pstatcode.clear();
  		occdate.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUstxrec() {
  		return ustxrec;
	}

	public FixedLengthStringData getUstxpfRecord() {
  		return ustxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUstxrec(what);
	}

	public void setUstxrec(Object what) {
  		this.ustxrec.set(what);
	}

	public void setUstxpfRecord(Object what) {
  		this.ustxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(ustxrec.getLength());
		result.set(ustxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}