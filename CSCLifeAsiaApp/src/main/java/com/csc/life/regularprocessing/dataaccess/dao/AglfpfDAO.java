package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.regularprocessing.dataaccess.model.Aglfpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface AglfpfDAO extends BaseDAO<Aglfpf> {
	public Map<String, Aglfpf> searchAglfRecord(String coy, List<String> agntnumList);
    public Aglfpf searchAglfRecord(String coy, String agntnum);
	public Aglfpf searchAglflnb(String coy, String agntNum);

}