package com.csc.life.regularprocessing.recordstructures;

import java.math.BigDecimal;
import java.util.List;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;



public class MgmFeeRec  extends ExternalData {
	
	private static final long serialVersionUID = 1L;
	
	private String statuz;
	private String function;
 	private String ridrkey;
    private String lifeLife ;
 	private String lifeJlife;
    private String covrCoverage ;
    private String covrRiderId;
 	private String chdrChdrcoy;
 	private String chdrChdrnum;
 	private String covrRider;
	private Integer planSuffix;
    private BigDecimal termdate;
    private String language;
    private Integer user;
	private String batccoy;
	private String batcbrn;
	private Integer batcactyr;
	private Integer batcactmn;
	private String batctrcde;
	private String batch;
	private Integer effdate;
	private String cntcurr;
	private String billfreq;
	private String cnttype;
	private BigDecimal sumins;
	private Integer nrFunds;
	private String premMeth;
	private String jlifePremMeth;
	private Integer premCessDate ;
    private String crtable ;
	private String billchnl ;
	private String mortcls ;
	private String svMethod ;
	private Integer tranno ;
	private String adfeemth ;
	private Integer polsum ;
	private Short polsum2 ;
	private Integer ptdate ;
	private Integer polinc;
	private Short polinc2;
	private BigDecimal singp;
	private String premsubr ;
	private String jpremsubr ;
	private String trandesc ;
	private String svCalcprog ;
	private String comlvlacc ;
	private Integer occdate ;
	private String occdtex ;
	private BigDecimal threadNumber ;
	private String chdrRegister ;
	private Integer polcurr ;
	private String benfunc ;
	private BigDecimal firstPrem;
	private List<String> curUnitBal;
	private List<String>  unitBidPrice;

	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getRidrkey() {
		return ridrkey;
	}
	public void setRidrkey(String ridrkey) {
		this.ridrkey = ridrkey;
	}
	public String getLifeLife() {
		return lifeLife;
	}
	public void setLifeLife(String lifeLife) {
		this.lifeLife = lifeLife;
	}
	public String getLifeJlife() {
		return lifeJlife;
	}
	public void setLifeJlife(String lifeJlife) {
		this.lifeJlife = lifeJlife;
	}
	public String getCovrCoverage() {
		return covrCoverage;
	}
	public void setCovrCoverage(String covrCoverage) {
		this.covrCoverage = covrCoverage;
	}
	public String getCovrRiderId() {
		return covrRiderId;
	}
	public void setCovrRiderId(String covrRiderId) {
		this.covrRiderId = covrRiderId;
	}
	public String getChdrChdrcoy() {
		return chdrChdrcoy;
	}
	public void setChdrChdrcoy(String chdrChdrcoy) {
		this.chdrChdrcoy = chdrChdrcoy;
	}
	public String getChdrChdrnum() {
		return chdrChdrnum;
	}
	public void setChdrChdrnum(String chdrChdrnum) {
		this.chdrChdrnum = chdrChdrnum;
	}
	public String getCovrRider() {
		return covrRider;
	}
	public void setCovrRider(String covrRider) {
		this.covrRider = covrRider;
	}

	public Integer getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Integer planSuffix) {
		this.planSuffix = planSuffix;
	}
	public BigDecimal getTermdate() {
		return termdate;
	}
	public void setTermdate(BigDecimal termdate) {
		this.termdate = termdate;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public Integer getUser() {
		return user;
	}
	public void setUser(Integer user) {
		this.user = user;
	}
	public String getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(String batccoy) {
		this.batccoy = batccoy;
	}
	public String getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(String batcbrn) {
		this.batcbrn = batcbrn;
	}
	public Integer getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Integer batcactyr) {
		this.batcactyr = batcactyr;
	}
	public Integer getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Integer batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Integer getEffdate() {
		return effdate;
	}
	public void setEffdate(Integer effdate) {
		this.effdate = effdate;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}
	public String getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public BigDecimal getSumins() {
		return sumins;
	}
	public void setSumins(BigDecimal sumins) {
		this.sumins = sumins;
	}

	public Integer getNrFunds() {
		return nrFunds;
	}
	public void setNrFunds(Integer nrFunds) {
		this.nrFunds = nrFunds;
	}
	public String getPremMeth() {
		return premMeth;
	}
	public void setPremMeth(String premMeth) {
		this.premMeth = premMeth;
	}
	public String getJlifePremMeth() {
		return jlifePremMeth;
	}
	public void setJlifePremMeth(String jlifePremMeth) {
		this.jlifePremMeth = jlifePremMeth;
	}
	public Integer getPremCessDate() {
		return premCessDate;
	}
	public void setPremCessDate(Integer premCessDate) {
		this.premCessDate = premCessDate;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(String billchnl) {
		this.billchnl = billchnl;
	}
	public String getMortcls() {
		return mortcls;
	}
	public void setMortcls(String mortcls) {
		this.mortcls = mortcls;
	}
	public String getSvMethod() {
		return svMethod;
	}
	public void setSvMethod(String svMethod) {
		this.svMethod = svMethod;
	}
	public Integer getTranno() {
		return tranno;
	}
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}
	public String getAdfeemth() {
		return adfeemth;
	}
	public void setAdfeemth(String adfeemth) {
		this.adfeemth = adfeemth;
	}
	public Integer getPolsum() {
		return polsum;
	}
	public void setPolsum(Integer polsum) {
		this.polsum = polsum;
	}
	
	public Short getPolsum2() {
		return polsum2;
	}
	public void setPolsum2(Short polsum2) {
		this.polsum2 = polsum2;
	}
	public Short getPolinc2() {
		return polinc2;
	}
	public void setPolinc2(Short polinc2) {
		this.polinc2 = polinc2;
	}
	public Integer getPtdate() {
		return ptdate;
	}
	public void setPtdate(Integer ptdate) {
		this.ptdate = ptdate;
	}
	public Integer getPolinc() {
		return polinc;
	}
	public void setPolinc(Integer polinc) {
		this.polinc = polinc;
	}
	public BigDecimal getSingp() {
		return singp;
	}
	public void setSingp(BigDecimal singp) {
		this.singp = singp;
	}
	public String getPremsubr() {
		return premsubr;
	}
	public void setPremsubr(String premsubr) {
		this.premsubr = premsubr;
	}
	public String getJpremsubr() {
		return jpremsubr;
	}
	public void setJpremsubr(String jpremsubr) {
		this.jpremsubr = jpremsubr;
	}
	public String getTrandesc() {
		return trandesc;
	}
	public void setTrandesc(String trandesc) {
		this.trandesc = trandesc;
	}
	public String getSvCalcprog() {
		return svCalcprog;
	}
	public void setSvCalcprog(String svCalcprog) {
		this.svCalcprog = svCalcprog;
	}
	public String getComlvlacc() {
		return comlvlacc;
	}
	public void setComlvlacc(String comlvlacc) {
		this.comlvlacc = comlvlacc;
	}
	public Integer getOccdate() {
		return occdate;
	}
	public void setOccdate(Integer occdate) {
		this.occdate = occdate;
	}
	public String getOccdtex() {
		return occdtex;
	}
	public void setOccdtex(String occdtex) {
		this.occdtex = occdtex;
	}
	public BigDecimal getThreadNumber() {
		return threadNumber;
	}
	public void setThreadNumber(BigDecimal threadNumber) {
		this.threadNumber = threadNumber;
	}
	public String getChdrRegister() {
		return chdrRegister;
	}
	public void setChdrRegister(String chdrRegister) {
		this.chdrRegister = chdrRegister;
	}
	public Integer getPolcurr() {
		return polcurr;
	}
	public void setPolcurr(Integer polcurr) {
		this.polcurr = polcurr;
	}
	public String getBenfunc() {
		return benfunc;
	}
	public void setBenfunc(String benfunc) {
		this.benfunc = benfunc;
	}
	public BigDecimal getFirstPrem() {
		return firstPrem;
	}
	public void setFirstPrem(BigDecimal firstPrem) {
		this.firstPrem = firstPrem;
	}

	public List<String> getCurUnitBal() {
		return curUnitBal;
	}
	public void setCurUnitBal(List<String> curUnitBal) {
		this.curUnitBal = curUnitBal;
	}
	public List<String> getUnitBidPrice() {
		return unitBidPrice;
	}
	public void setUnitBidPrice(List<String> unitBidPrice) {
		this.unitBidPrice = unitBidPrice;
	}
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	@Override
	public void initialize() {
		// TODO Auto-generated method stub
	}

}
