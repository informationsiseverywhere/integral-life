package com.csc.life.regularprocessing.dataaccess.model;

import java.math.BigDecimal;


public class Bonspf {
    private long uniqueNumber;
    private String chdrcoy;
    private String chdrnum;
    private String life;
    private String coverage;
    private String rider;
    private int planSuffix;
    private int currfrom;
    private int currto;
    private String validflag;
    private String bonusCalcMeth;
    private BigDecimal bonPayThisYr;
    private BigDecimal bonPayLastYr;
    private BigDecimal totalBonus;
    private String termid;
    private int user;
    private int transactionDate;
    private int transactionTime;
    private String userProfile;
    private String jobName;
    private String datime;
    
    public long getUniqueNumber() {
        return uniqueNumber;
    }
    public void setUniqueNumber(long uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }
    public String getChdrcoy() {
        return chdrcoy;
    }
    public String getChdrnum() {
        return chdrnum;
    }
    public String getLife() {
        return life;
    }
    public String getCoverage() {
        return coverage;
    }
    public String getRider() {
        return rider;
    }
    public int getPlanSuffix() {
        return planSuffix;
    }
    public int getCurrfrom() {
        return currfrom;
    }
    public int getCurrto() {
        return currto;
    }
    public String getValidflag() {
        return validflag;
    }
    public String getBonusCalcMeth() {
        return bonusCalcMeth;
    }
    public BigDecimal getBonPayThisYr() {
        return bonPayThisYr;
    }
    public BigDecimal getBonPayLastYr() {
        return bonPayLastYr;
    }
    public BigDecimal getTotalBonus() {
        return totalBonus;
    }
    public String getTermid() {
        return termid;
    }
    public int getUser() {
        return user;
    }
    public int getTransactionDate() {
        return transactionDate;
    }
    public int getTransactionTime() {
        return transactionTime;
    }
    public String getUserProfile() {
        return userProfile;
    }
    public String getJobName() {
        return jobName;
    }
    public String getDatime() {
        return datime;
    }
    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }
    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }
    public void setLife(String life) {
        this.life = life;
    }
    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }
    public void setRider(String rider) {
        this.rider = rider;
    }
    public void setPlanSuffix(int planSuffix) {
        this.planSuffix = planSuffix;
    }
    public void setCurrfrom(int currfrom) {
        this.currfrom = currfrom;
    }
    public void setCurrto(int currto) {
        this.currto = currto;
    }
    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }
    public void setBonusCalcMeth(String bonusCalcMeth) {
        this.bonusCalcMeth = bonusCalcMeth;
    }
    public void setBonPayThisYr(BigDecimal bonPayThisYr) {
        this.bonPayThisYr = bonPayThisYr;
    }
    public void setBonPayLastYr(BigDecimal bonPayLastYr) {
        this.bonPayLastYr = bonPayLastYr;
    }
    public void setTotalBonus(BigDecimal totalBonus) {
        this.totalBonus = totalBonus;
    }
    public void setTermid(String termid) {
        this.termid = termid;
    }
    public void setUser(int user) {
        this.user = user;
    }
    public void setTransactionDate(int transactionDate) {
        this.transactionDate = transactionDate;
    }
    public void setTransactionTime(int transactionTime) {
        this.transactionTime = transactionTime;
    }
    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
    public void setDatime(String datime) {
        this.datime = datime;
    }
}