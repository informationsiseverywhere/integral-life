/*
 * File: B5348.java
 * Date: 29 August 2009 21:04:20
 * Author: Quipoz Limited
 * 
 * Class transformed from B5348.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.general.dataaccess.AcagrnlTableDAM;
import com.csc.fsu.general.dataaccess.PayrpfTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Fileprcrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.regularprocessing.dataaccess.PayxpfTableDAM;
import com.csc.life.regularprocessing.dataaccess.dao.B5349DAO;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*  SPLITTER PROGRAM (For BILLING batch processing)
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* This program is to be run before the Billing (B5349) program.
* It is run in single-thread, and writes the selected PAYR
* records to multiple files . Each of these files will be read
* by a copy of B5349 run in multi-thread.
*
* SQL will be used to access the PAYR physical file. The
* splitter program will extract PAYR records that meet the
* following criteria:
*
* i    validflag  =  '1'
* ii   chdrnum    between  <p6671-chdrnumfrom>
*                     and  <p6671-chdrnumto>
* iii  chdroy     =  <run-company>
* iv   billchnl   <> 'N'
*      order by chdrcoy, chdrnum, payrseqno
*
* Control totals used in this program:
*
*    01  -  No. of thread members
*    02  -  No. of extracted records
*
*
* Splitter programs should be simple. They should only deal
* with a single file. If there is insufficient data on the
* primary file to completely identify a transaction then
* further editing should be done in the subsequent process.
* The objective of a splitter is to rapidily isolate potential
* transactions, not process the transaction. The primary concern
* for splitter program design is elasped time speed and this is
* achieved by minimising disk IO.
*
* Before the Splitter process is invoked the, the program CRTTMPF
* should be run. The CRTTMPF (Create Temporary File) will create
* a duplicate of a physical file (created under Smart and defined
* as a Query file) which will have as many members as are defined
* in that process's subsequent threads field. The temporary file
* will have the name XXXXDD9999 where XXXX is the first four
* characters of the file which was duplicated, DD is the first
* two characters of the 4th system parameter and 9999 is the
* schedule run number. Each member added to the temporary file
* will be called THREAD999 where 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read records from a primary file
* and for each record it selects it will write a record to a
* temporary file member. Exactly which temporary file member
* is written to is decided by a working storage count. The
* objective of the count is spread the transaction records
* evenly across the thread members.
*
* The Splitter program should always have a restart method of
* '1' indicating a re-run. In the event of a restart the Splitter
* program will always start with empty file members. Apart from
* writing records to the temporary file members Splitter programs
* should not be doing any further updates.
*
* There should be no non-standard CL commands in the CL Pgm which
* calls this program.
*
* Notes for programs which use Splitter Program Temporary Files.
* --------------------------------------------------------------
*
* The MTBE will be able to submit multiple versions of the program
* which use the file members created from this program. It is
* important that the process which runs the splitter program has
* the same 'number of subsequent threads' as the following
* process has defined in 'number of threads this process'.
*
* The subsequent program need only point itself to one member
* based around the field BSPR-SCHEDULE-THREAD-NO which indicates
* in which thread the copy of the subsequent program is running.
*
* Only the following code should be needed to perform an OVRDBF
* to point to the correct member:
*
*
*****************************************************************
* </pre>
*/
public class B5348 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlpayrCursorrs = null;
	private java.sql.PreparedStatement sqlpayrCursorps = null;
	private java.sql.Connection sqlpayrCursorconn = null;
	private String sqlpayrCursor = "";
	private int payrCursorLoopIndex = 0;
//	private PayxpfTableDAM payxpf = new PayxpfTableDAM();
//	private DiskFileDAM payx01 = new DiskFileDAM("PAYX01");
//	private DiskFileDAM payx02 = new DiskFileDAM("PAYX02");
//	private DiskFileDAM payx03 = new DiskFileDAM("PAYX03");
//	private DiskFileDAM payx04 = new DiskFileDAM("PAYX04");
//	private DiskFileDAM payx05 = new DiskFileDAM("PAYX05");
//	private DiskFileDAM payx06 = new DiskFileDAM("PAYX06");
//	private DiskFileDAM payx07 = new DiskFileDAM("PAYX07");
//	private DiskFileDAM payx08 = new DiskFileDAM("PAYX08");
//	private DiskFileDAM payx09 = new DiskFileDAM("PAYX09");
//	private DiskFileDAM payx10 = new DiskFileDAM("PAYX10");
//	private DiskFileDAM payx11 = new DiskFileDAM("PAYX11");
//	private DiskFileDAM payx12 = new DiskFileDAM("PAYX12");
//	private DiskFileDAM payx13 = new DiskFileDAM("PAYX13");
//	private DiskFileDAM payx14 = new DiskFileDAM("PAYX14");
//	private DiskFileDAM payx15 = new DiskFileDAM("PAYX15");
//	private DiskFileDAM payx16 = new DiskFileDAM("PAYX16");
//	private DiskFileDAM payx17 = new DiskFileDAM("PAYX17");
//	private DiskFileDAM payx18 = new DiskFileDAM("PAYX18");
//	private DiskFileDAM payx19 = new DiskFileDAM("PAYX19");
//	private DiskFileDAM payx20 = new DiskFileDAM("PAYX20");
//	private DiskFileDAM payx21 = new DiskFileDAM("PAYX21");
//	private DiskFileDAM payx22 = new DiskFileDAM("PAYX22");
//	private DiskFileDAM payx23 = new DiskFileDAM("PAYX23");
//	private DiskFileDAM payx24 = new DiskFileDAM("PAYX24");
//	private DiskFileDAM payx25 = new DiskFileDAM("PAYX25");
//	private DiskFileDAM payx26 = new DiskFileDAM("PAYX26");
//	private DiskFileDAM payx27 = new DiskFileDAM("PAYX27");
//	private DiskFileDAM payx28 = new DiskFileDAM("PAYX28");
//	private DiskFileDAM payx29 = new DiskFileDAM("PAYX29");
//	private DiskFileDAM payx30 = new DiskFileDAM("PAYX30");

	private List<DiskFileDAM> payxList;
	
	private PayxpfTableDAM payxpfData = new PayxpfTableDAM();
		/* (Change the record length to that of the temporary file).*/
//	private FixedLengthStringData payx01Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx02Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx03Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx04Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx05Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx06Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx07Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx08Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx09Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx10Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx11Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx12Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx13Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx14Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx15Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx16Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx17Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx18Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx19Rec = new FixedLengthStringData(28);
//	private FixedLengthStringData payx20Rec = new FixedLengthStringData(28);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5348");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaN = new FixedLengthStringData(2).init("N ");
	private FixedLengthStringData wsaa1 = new FixedLengthStringData(1).init("1");
	private PackedDecimalData wsaaBillUpToDate = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaMaxLeadDays = new ZonedDecimalData(3, 0).init(ZERO);

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

		/*  PAYR member parametres.*/
	private FixedLengthStringData wsaaPayxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaPayxFn, 0, FILLER).init("PAYX");
	private FixedLengthStringData wsaaPayxRunid = new FixedLengthStringData(2).isAPartOf(wsaaPayxFn, 4);
	private ZonedDecimalData wsaaPayxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaPayxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
		/*01  PAYXPF-DATA.                                                 
		  COPY DDS-ALL-FORMATS OF PAYXPF.                              
		  Pointers to point to the member to read (IY) and write (IZ).*/
	private int iy = 1;
	private int iz = 1;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
		/*  Define the number of rows in the block to be fetched.*/
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaPayrData = FLSInittedArray (1000, 28);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaPayrData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaPayrData, 1);
	private PackedDecimalData[] wsaaPayrseqno = PDArrayPartOfArrayStructure(1, 0, wsaaPayrData, 9);
	private FixedLengthStringData[] wsaaBillchnl = FLSDArrayPartOfArrayStructure(2, wsaaPayrData, 10);
	private PackedDecimalData[] wsaaBillcd = PDArrayPartOfArrayStructure(8, 0, wsaaPayrData, 12);
	private FixedLengthStringData[] wsaaBillsupr = FLSDArrayPartOfArrayStructure(1, wsaaPayrData, 17);
	private PackedDecimalData[] wsaaBillspfrom = PDArrayPartOfArrayStructure(8, 0, wsaaPayrData, 18);
	private PackedDecimalData[] wsaaBillspto = PDArrayPartOfArrayStructure(8, 0, wsaaPayrData, 23);

		/* WSAA-IND-ARRAY */
	private FixedLengthStringData[] wsaaPayrInd = FLSInittedArray (1000, 8);
	private BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(4, 4, 0, wsaaPayrInd, 0);
		/* WSAA-HOST-VARIABLES */
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";
		/* TABLES */
	private static final String t6654 = "T6654";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
	private AcagrnlTableDAM acagrnlIO = new AcagrnlTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrpfTableDAM payrpfData = new PayrpfTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private T6654rec t6654rec = new T6654rec();
	private P6671par p6671par = new P6671par();
	private Fileprcrec fileprcrec = new Fileprcrec();
	private Boolean japanBilling = false;
	private static final String BTPRO027 = "BTPRO027";
	private ZonedDecimalData monthLastDate = new ZonedDecimalData(8, 0);
	private boolean leaddayFeature=false;
	
	private B5349DAO b5349DAO =  getApplicationContext().getBean("b5349DAO", B5349DAO.class);	

	public B5348() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** The  restart section is being handled in section 1100 .*/
		/** This is because the code is applicable to every run,*/
		/** not just restarts.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/*    Check that the restart method from the process definition*/
		/*    is compatible with the program (of type '1'). In the event*/
		/*    of failure, the program will be re-run from the beginning.*/
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*    Do a ovrdbf for each temporary file member. (Note we have*/
		/*    allowed for a maximum of 20.)*/
//		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
//			bprdIO.setThreadsSubsqntProc(20);
//		}
		/*    Since this program runs from a parameter screen move the*/
		/*    internal parameter area to the original parameter copybook*/
		/*    to retrieve the contract range*/
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES)
		&& isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		/*    The next thing we must do is construct the name of the*/
		/*    temporary file which we will be working with.*/
		wsaaPayxRunid.set(bprdIO.getSystemParam04());
		wsaaPayxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		/*    Open the required number of temporary files, depending*/
		/*    on IZ and determined by the number of threads specified*/
		/*    for the subsequent process.*/
		payxList = new ArrayList<>();
		int threadsNum = bprdIO.getThreadsSubsqntProc().toInt();
		for (iz = 1; iz<= threadsNum; iz++){
			String diskName = "PAYX" + iz;
			DiskFileDAM payxDam = new DiskFileDAM(diskName);
			openThreadMember1100(payxDam);
			payxList.add(payxDam);
		}
		/*    Log the number of threads being used*/
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		wsaaCompany.set(bsprIO.getCompany());
		/*    Read through T6654 to find the maximum number of lead days.  */
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t6654);
		itemIO.setItemitem(SPACES);
		itemIO.setFunction(varcom.begn);
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
			searchT66541200();
		}
		
		/*    Add the maximum number of lead days to the effective date    */
		/*    to find the bill-up-to-date.                                 */
		japanBilling = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), BTPRO027, appVars, "IT");
		boolean billChgFlag = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "CSBIL005", appVars, "IT");//IBPLIFE-4822
		leaddayFeature = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "CSBIL006", appVars, "IT");
		if(billChgFlag) {
			wsaaBillUpToDate.set(bsscIO.getEffectiveDate());
			if(leaddayFeature)
			{
				datcon2rec.freqFactor.set(wsaaMaxLeadDays);
				datcon2rec.frequency.set("DY");
				datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
				
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				if (isNE(datcon2rec.statuz, varcom.oK)) {
					syserrrec.params.set(datcon2rec.datcon2Rec);
					fatalError600();
				}
				wsaaBillUpToDate.set(datcon2rec.intDate2);
			}
		}else {
		  if(japanBilling){
			monthLastDate.set(getMonthEnd(bsscIO.getEffectiveDate().toString()));
			datcon2rec.freqFactor.set(1);
			datcon2rec.frequency.set("01");
			datcon2rec.intDate1.set(monthLastDate);
		  }
		  else{
			datcon2rec.freqFactor.set(wsaaMaxLeadDays);
			datcon2rec.frequency.set("DY");
			datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
		 }
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError600();
		}
		wsaaBillUpToDate.set(datcon2rec.intDate2);
	}
		sqlpayrCursor = " SELECT  CHDRCOY, CHDRNUM, PAYRSEQNO, BILLCHNL, BILLCD, BILLSUPR, BILLSPFROM, BILLSPTO" +
" FROM   " + getAppVars().getTableNameOverriden("PAYRPF") + " " +
" WHERE BILLCHNL <> ?" +
" AND CHDRCOY = ?" +
" AND VALIDFLAG = ?" +
" AND BILLCD <= ?" +
" AND CHDRNUM BETWEEN ? AND ?" +
" ORDER BY CHDRCOY, CHDRNUM, PAYRSEQNO";
		/*    Initialise the array which will hold all the records*/
		/*    returned from each fetch. (INITIALIZE will not work as the*/
		/*    the array is indexed.*/
		iy = 1;
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		sqlerrorflag = false;
		try {
			sqlpayrCursorconn = getAppVars().getDBConnectionForTable(new com.csc.fsu.general.dataaccess.PayrpfTableDAM());
			sqlpayrCursorps = getAppVars().prepareStatementEmbeded(sqlpayrCursorconn, sqlpayrCursor, "PAYRPF");
			getAppVars().setDBString(sqlpayrCursorps, 1, wsaaN);
			getAppVars().setDBString(sqlpayrCursorps, 2, wsaaCompany);
			getAppVars().setDBString(sqlpayrCursorps, 3, wsaa1);
			getAppVars().setDBNumber(sqlpayrCursorps, 4, wsaaBillUpToDate);
			getAppVars().setDBString(sqlpayrCursorps, 5, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlpayrCursorps, 6, wsaaChdrnumTo);
			sqlpayrCursorrs = getAppVars().executeQuery(sqlpayrCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*    Where parameter 3 on the schedule definition is not  not     */
		/*    spaces, clear the ACAG file.                                 */
		if (isNE(bprdIO.getSystemParam03(), SPACES)) {
			clearAcag1300();
		}
	}

protected void openThreadMember1100(DiskFileDAM payxDam)
	{
		/* The following part is used to initialise all members.*/
		/* It is also used in restart mode.*/
		/* Temporary files, previously used are cleared.*/
		/* MOVE IZ                     TO WSAA-THREAD-NUMBER.           */
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(), 1), iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaPayxFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz, varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.params.set("CLRTMPF");
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(PAYX");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaPayxFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*    Open the file.*/
		payxDam.openOutput();
	}

protected void searchT66541200()
	{
		start1210();
	}

protected void start1210()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.endp)
		|| isNE(itemIO.getItemcoy(), bsprIO.getCompany())
		|| isNE(itemIO.getItemtabl(), t6654)) {
			itemIO.setStatuz(varcom.endp);
			return ;
		}
		t6654rec.t6654Rec.set(itemIO.getGenarea());
		if (isGT(t6654rec.leadDays, wsaaMaxLeadDays)) {
			wsaaMaxLeadDays.set(t6654rec.leadDays);
		}
		itemIO.setFunction(varcom.nextr);
	}

protected void clearAcag1300()
	{
		/*START*/
		/*    Check for multi-thread processing. If it is, clear down      */
		/*    the ACAGPF file.                                             */
//		if (isNE(bprdIO.getSystemParam03(), SPACES)) {
//			fileprcrec.function.set("CLRF");
//			fileprcrec.file1.set("ACAGPF");
//			noSource("FILEPROC", fileprcrec.params);
//			if (isNE(fileprcrec.statuz, varcom.oK)) {
//				syserrrec.statuz.set(fileprcrec.statuz);
//				syserrrec.params.set(fileprcrec.params);
//				fatalError600();
//			}
//		}
		/*EXIT*/
		/*START*/
		if (isNE(bprdIO.getSystemParam03(),SPACES)) {
			wsaaQcmdexc.set(SPACES);
			wsaaQcmdexc.set("CLRPFM FILE(ACAGPF)");
			com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		}
		/*EXIT*/
	}

protected void initialiseArray1500()
	{
		/*START*/
		wsaaPayrseqno[wsaaInd.toInt()].set(ZERO);
		wsaaBillspfrom[wsaaInd.toInt()].set(ZERO);
		wsaaBillspto[wsaaInd.toInt()].set(ZERO);
		wsaaBillcd[wsaaInd.toInt()].set(ZERO);
		wsaaChdrcoy[wsaaInd.toInt()].set(SPACES);
		wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaBillchnl[wsaaInd.toInt()].set(SPACES);
		wsaaBillsupr[wsaaInd.toInt()].set(SPACES);
		/*EXIT*/
	}

protected void readFile2000()
	{
		readFile2010();
	}

protected void readFile2010()
	{
		/*  Now a block of records is fetched into the array.*/
		/*  Also on the first entry into the program we must set up the*/
		/*  WSAA-PREV-CHDRNUM = the present chdrnum.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd, 1)) {
			return ;
		}
		sqlerrorflag = false;
		try {//ILIFE-2806
			for (payrCursorLoopIndex = 1; isLTE(payrCursorLoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlpayrCursorrs); payrCursorLoopIndex++ ){
				getAppVars().getDBObject(sqlpayrCursorrs, 1, wsaaChdrcoy[payrCursorLoopIndex], wsaaNullInd[payrCursorLoopIndex][1]);
				getAppVars().getDBObject(sqlpayrCursorrs, 2, wsaaChdrnum[payrCursorLoopIndex], wsaaNullInd[payrCursorLoopIndex][2]);
				getAppVars().getDBObject(sqlpayrCursorrs, 3, wsaaPayrseqno[payrCursorLoopIndex], wsaaNullInd[payrCursorLoopIndex][3]);
				getAppVars().getDBObject(sqlpayrCursorrs, 4, wsaaBillchnl[payrCursorLoopIndex], wsaaNullInd[payrCursorLoopIndex][4]);
				getAppVars().getDBObject(sqlpayrCursorrs, 5, wsaaBillcd[payrCursorLoopIndex]);
				getAppVars().getDBObject(sqlpayrCursorrs, 6, wsaaBillsupr[payrCursorLoopIndex]);
				getAppVars().getDBObject(sqlpayrCursorrs, 7, wsaaBillspfrom[payrCursorLoopIndex]);
				getAppVars().getDBObject(sqlpayrCursorrs, 8, wsaaBillspto[payrCursorLoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*  We must detect :-*/
		/*      a) no rows returned on the first fetch*/
		/*      b) the last row returned (in the block)*/
		/*  IF Either of the above cases occur, then an*/
		/*      SQLCODE = +100 is returned.*/
		/*  The 3000 section is continued for case (b).*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(varcom.endp);
		}
		//tom chi modified, bug 1030
		else if(isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)){
			wsspEdterror.set(varcom.endp);
		}
		//end
		else {
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}
	}

protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		/*  Re-initialise the block for next fetch and point to first row.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*  If the the CHDRNUM being processed is not equal to the*/
		/*  to the previous we should move to the next output file member.*/
		/*  The condition is here to allow for checking the last chdrnum*/
		/*  of the old block with the first of the new and to move to the*/
		/*  next PAYX member to write to if they have changed.*/
		if (isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)) {
			wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
			iy++;
		}
		/*  Load from storage all PAYX data for the same contract until*/
		/*  the CHDRNUM on PAYX has changed,*/
		/*    OR*/
		/*  The end of an incomplete block is reached.*/
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		start3200();
	}

protected void start3200()
	{
		payxpfData.chdrcoy.set(wsaaChdrcoy[wsaaInd.toInt()]);
		payxpfData.chdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
		payxpfData.payrseqno.set(wsaaPayrseqno[wsaaInd.toInt()]);
		payxpfData.billchnl.set(wsaaBillchnl[wsaaInd.toInt()]);
		payxpfData.billcd.set(wsaaBillcd[wsaaInd.toInt()]);
		payxpfData.billsupr.set(wsaaBillsupr[wsaaInd.toInt()]);
		payxpfData.billspfrom.set(wsaaBillspfrom[wsaaInd.toInt()]);
		payxpfData.billspto.set(wsaaBillspto[wsaaInd.toInt()]);
		if (isGT(iy, bprdIO.getThreadsSubsqntProc())) {
			iy = 1;
		}
		/*    Write records to their corresponding temporary file*/
		/*    members, determined by the working storage count, IY.*/
		/*    This spreads the transaction members evenly across*/
		/*    the thread members.*/
		payxList.get(iy - 1).write(payxpfData);

		/*    Log the number of extacted records.*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
		/*  Set up the array for the next block of records.*/
		wsaaInd.add(1);
		/*  Check for an incomplete block retrieved.*/
		if (isLTE(wsaaInd, wsaaRowsInBlock) && isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsaaEofInBlock.set("Y");
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/**    There is no pre-commit processing to be done.*/
		/*EXIT*/
		b5349DAO.initializeB5349Temp();
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close the open files and remove the overrides.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlpayrCursorconn, sqlpayrCursorps, sqlpayrCursorrs);
		int threadsNum = bprdIO.getThreadsSubsqntProc().toInt();
		for (iz = 1; iz <= threadsNum; iz++){
			payxList.get(iz - 1).close();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}

protected String getMonthEnd(String date) {
	int lengthMonth = YearMonth
			.of(Integer.valueOf(date.substring(0, 4).trim()), Integer.valueOf(date.substring(4, 6).trim()))
			.lengthOfMonth();
	return date.substring(0, 6).concat(String.format("%d", lengthMonth));
}

}
