package com.csc.life.regularprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: BonspfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:05
 * Class transformed from BONSPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class BonspfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 125;
	public FixedLengthStringData bonsrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData bonspfRecord = bonsrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(bonsrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(bonsrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(bonsrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(bonsrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(bonsrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(bonsrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(bonsrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(bonsrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(bonsrec);
	public FixedLengthStringData bonusCalcMeth = DD.bcalmeth.copy().isAPartOf(bonsrec);
	public PackedDecimalData bonPayThisYr = DD.bpayty.copy().isAPartOf(bonsrec);
	public PackedDecimalData bonPayLastYr = DD.bpayny.copy().isAPartOf(bonsrec);
	public PackedDecimalData totalBonus = DD.totbon.copy().isAPartOf(bonsrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(bonsrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(bonsrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(bonsrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(bonsrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(bonsrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(bonsrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(bonsrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public BonspfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for BonspfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public BonspfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for BonspfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public BonspfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for BonspfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public BonspfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("BONSPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"CURRFROM, " +
							"CURRTO, " +
							"VALIDFLAG, " +
							"BCALMETH, " +
							"BPAYTY, " +
							"BPAYNY, " +
							"TOTBON, " +
							"TERMID, " +
							"USER_T, " +
							"TRDT, " +
							"TRTM, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     currfrom,
                                     currto,
                                     validflag,
                                     bonusCalcMeth,
                                     bonPayThisYr,
                                     bonPayLastYr,
                                     totalBonus,
                                     termid,
                                     user,
                                     transactionDate,
                                     transactionTime,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		currfrom.clear();
  		currto.clear();
  		validflag.clear();
  		bonusCalcMeth.clear();
  		bonPayThisYr.clear();
  		bonPayLastYr.clear();
  		totalBonus.clear();
  		termid.clear();
  		user.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getBonsrec() {
  		return bonsrec;
	}

	public FixedLengthStringData getBonspfRecord() {
  		return bonspfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setBonsrec(what);
	}

	public void setBonsrec(Object what) {
  		this.bonsrec.set(what);
	}

	public void setBonspfRecord(Object what) {
  		this.bonspfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(bonsrec.getLength());
		result.set(bonsrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}