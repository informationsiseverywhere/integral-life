/*
 * File: Ubblmt3.java
 * Date: 30 August 2009 2:48:24
 * Author: Quipoz Limited
 * 
 * Class transformed from UBBLMT3.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.diary.dataaccess.DacmTableDAM;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.dataaccess.AcagrnlTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.agents.dataaccess.AgcmTableDAM;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.agents.dataaccess.ZctnTableDAM;
import com.csc.life.agents.dataaccess.ZptnTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5644rec;
import com.csc.life.agents.tablestructures.T5647rec;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.contractservicing.dataaccess.AgcmbchTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.procedures.Zorcompy;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.flexiblepremium.dataaccess.AgcmseqTableDAM;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.PcddlnbTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.regularprocessing.dataaccess.LifernlTableDAM;
import com.csc.life.regularprocessing.recordstructures.Ubblallpar;
import com.csc.life.regularprocessing.tablestructures.T5534rec;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrstTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrstnudTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5691rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*REMARKS.
*
* This subroutine is cloned from UBBLMT2 but this subroutine has
* additional features to calculate the commission based on the
* mortality charge.
* If this subroutine is called from issue (FUNCTION = ISSUE), it
* creates an AGCM record and posts the commission else it reads
* the AGCM record to get the commission method and compute the
* commission amount and updates the AGCM record.
*
*
* UBBLMT2 has following features.
*
* This subroutine controls  the  'BENEFIT  BILLING'  processing,
* being called by various parts of the system.
*
* Basically this program  will  use  the  parameters  passed  to
* call a calculation subroutine to work out the  benefit  billed
* amount. This amount  is  calculated  based  on the sum at risk
* which can be calculated as follows:
*
*   Sum at risk = Sum insured - Surrender value of component
*
* Surrender value: if the  Surrender  method  is  not  blank  on
* table T5534, the  value  can  be  calculated  by  calling  the
* subroutine passed in from the linkage area.
*
* The premium to be debited  is  calculated dependent on entries
* (Premium Calculation  Method)  set  up  in  T5534,  T5675  and
* whether there is a joint life or not.
*
* Then the appropriate  subroutine  is  called  to  perform  the
* calculation if the methods are not blank.
*
* The Administration Fee amounts will  be  added  to the premium
* caluculated above. To calculate the amounts, an Administration
* Fee method  will  be  passed  from  table T5534 and, with this
* method  and the Contract Currency, table T5691 will be read to
* get the Initial and Periodic Fee amounts.
*
* Once the premium has been obtained, the corresponding coverage
* debt is then updated with this amount.
*
* This  premium is the Benefit Billed amount for that particular
* coverage or rider.
*
* This subroutine also includes following functionality.
* This will post accouting entries via T5645.
* These should be :
*    - Debit PREMIUM + INITIAL ADM FEES + PERIODIC ADM FEES
*      in 'Coverage Debt'
*    - Credit PREMIUM in 'Coverage Debt'
*    - Credit INITIAL ADM FEES  at 'Initial-Fees'
*    - Credit PERIODIC ADM FEES  at 'Periodic-Fees'
*
*
*    Include administrative charges during
*    Contract Issue & Top Up.
*    The % charge is defined in T5691 & is
*    calculated based on (Admin Charges % *  Single Premium).
*    During TopUp, exclude Mortality Charges &
*    Regular Fees.
*
*    Age to be calculated as the number of
*    years between birthdate and effective
*    date. Call subroutine AGECALC to do this.
*
*    If surrender value of units is more than
*    the sum assured, set CPRM-SUMIN to 0.
*
*    Program checks the Periodic Fee value from
*    T5691 when processing the Initial Fee.
*                                                                     *
*    The Program also writes a ZRST when there
*    is Mortality charge, Policy fee, Initial
*    fee or Admin fee for a contract.
*    This file will be read during statement
*    printing.
*                                                                     *
*    To check for admin fee and do not create
*    accounting movement if zero.
*                                                                     *
*    Surrender values are now calculated in
*    the Fund currency.
*    For postings 'LE IF' 'LE FE',
*    the full coverage key should also be
*    used as RLDGACCT. Also the rider CRTABLE should
*    be used for substitution code.
*    For 'CD' posting an ACMV must be
*    written, even if the amount is zero, as
*    the ACMV record is used to reset Coverage
*    details when Contract Reversal is applied.
*
*    Missing condition to ignore mortality
*    charge during Topup.
*    Readjusted conditions to flow through all
*    types of fees.
*
* Initialise new fields which have been added to the PREMIUMREC
* Copybook.  These fields enable both Basic and Loaded
* Premiums to be calculated.
*
*****************************************************************
* </pre>
*/
public class Ubblmt3 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaCallSubr = new FixedLengthStringData(10).init(SPACES);
	private final String wsaaSubr = "UBBLMT3";
	private FixedLengthStringData wsaaPremMeth = new FixedLengthStringData(4).init(SPACES);

	private FixedLengthStringData wsaaT5691Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5691Adm = new FixedLengthStringData(4).isAPartOf(wsaaT5691Key, 0);
	private FixedLengthStringData wsaaT5691Curr = new FixedLengthStringData(3).isAPartOf(wsaaT5691Key, 4);
	private ZonedDecimalData wsaaCovrTot = new ZonedDecimalData(17, 2).setUnsigned();
	private ZonedDecimalData wsaaAccumSurrenderValue = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaAccumEstimatedValue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(13, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaTrefChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaTranref, 0);
	private FixedLengthStringData wsaaTrefChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaTranref, 1);
	private FixedLengthStringData wsaaTrefLife = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 9);
	private FixedLengthStringData wsaaTrefCoverage = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 11);
	private FixedLengthStringData wsaaTrefRider = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 13);
	private FixedLengthStringData wsaaTrefPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 15);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private FixedLengthStringData wsaaPrevRegister = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaPrevTxcode = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaPrevCrtable = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaPrevCnttype = new FixedLengthStringData(3).init(SPACES);
	private ZonedDecimalData wsaaTotalTax01 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaTotalTax02 = new ZonedDecimalData(17, 2);

	private FixedLengthStringData wsaaBatckey = new FixedLengthStringData(19);
	private FixedLengthStringData wsaaBatccoy = new FixedLengthStringData(1).isAPartOf(wsaaBatckey, 2);
	private FixedLengthStringData wsaaBatcbrn = new FixedLengthStringData(2).isAPartOf(wsaaBatckey, 3);
	private PackedDecimalData wsaaBatcactyr = new PackedDecimalData(4, 0).isAPartOf(wsaaBatckey, 5);
	private PackedDecimalData wsaaBatcactmn = new PackedDecimalData(2, 0).isAPartOf(wsaaBatckey, 8);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaBatckey, 10);
	private FixedLengthStringData wsaaBatcbatch = new FixedLengthStringData(5).isAPartOf(wsaaBatckey, 14);
	private PackedDecimalData wsaaPerFee = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaIniFee = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaAdmChgs = new PackedDecimalData(12, 3);
	private PackedDecimalData wsaaTopupFee = new PackedDecimalData(12, 3);
	private PackedDecimalData wsaaTopupAdmChgs = new PackedDecimalData(12, 3);
	private PackedDecimalData wsaaTotalCd = new PackedDecimalData(15, 2);
	private PackedDecimalData wsaaSeqno = new PackedDecimalData(2, 0);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0);
	private ZonedDecimalData wsaaCommType = new ZonedDecimalData(2, 0);
	private ZonedDecimalData wsaaSubNo = new ZonedDecimalData(2, 0);
	private FixedLengthStringData wsaaBillfreqC = new FixedLengthStringData(2);
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData filler3 = new FixedLengthStringData(2).isAPartOf(wsaaBillfreqC, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaBillfreqN = new ZonedDecimalData(2, 0).isAPartOf(filler3, 0).setUnsigned();
	private ZonedDecimalData wsaaSubRound = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData filler4 = new FixedLengthStringData(2).isAPartOf(wsaaSubRound, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaSubRoundC = new FixedLengthStringData(2).isAPartOf(filler4, 0);
	private ZonedDecimalData wsaaPlanSuffixN = new ZonedDecimalData(4, 0);

	private FixedLengthStringData filler5 = new FixedLengthStringData(4).isAPartOf(wsaaPlanSuffixN, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaPlanSuffixC = new FixedLengthStringData(4).isAPartOf(filler5, 0);
		/* WSAA-AMOUNTS */
	private PackedDecimalData[] wsaaPayamnt = PDInittedArray(3, 17, 2);
	private PackedDecimalData[] wsaaErndamt = PDInittedArray(3, 17, 2);
	private PackedDecimalData wsaaCommDue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCommEarn = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCommPay = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaServDue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaServEarn = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRenlDue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRenlEarn = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaBillfq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfq, 0, REDEFINE).setUnsigned();
	private static final int wsaaAgcmIySize = 500;
	private ZonedDecimalData wsaaAgcmIy = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaAgcmIb = new ZonedDecimalData(3, 0).init(0).setUnsigned();

		/* WSAA-AGCM-SUMMARY */
	private FixedLengthStringData[] wsaaAgcmPremRec = FLSInittedArray (500, 17);
	private ZonedDecimalData[] wsaaAgcmSeqno = ZDArrayPartOfArrayStructure(3, 0, wsaaAgcmPremRec, 0, UNSIGNED_TRUE);
	private PackedDecimalData[] wsaaAgcmEfdate = PDArrayPartOfArrayStructure(8, 0, wsaaAgcmPremRec, 3, UNSIGNED_TRUE);
	private PackedDecimalData[] wsaaAgcmAnnprem = PDArrayPartOfArrayStructure(17, 2, wsaaAgcmPremRec, 8);
	private PackedDecimalData wsaaRegPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRegPremFirst = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRegPremRenewal = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgcmPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaZctnAnnprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaZctnInstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRegIntmDate = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaAgcmFound = new FixedLengthStringData(1);
	private Validator agcmFound = new Validator(wsaaAgcmFound, "Y");

	private FixedLengthStringData wsaaPremType = new FixedLengthStringData(1);
	private Validator singPrem = new Validator(wsaaPremType, "S");
	private Validator firstPrem = new Validator(wsaaPremType, "I");
	private Validator renPrem = new Validator(wsaaPremType, "R");
	private ZonedDecimalData wsaaPtdate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private static final String e308 = "E308";
	private static final String t071 = "T071";
	private static final String f401 = "F401";
	private static final String e103 = "E103";
	private AcagrnlTableDAM acagrnlIO = new AcagrnlTableDAM();
	private AgcmTableDAM agcmIO = new AgcmTableDAM();
	private AgcmbchTableDAM agcmbchIO = new AgcmbchTableDAM();
	private AgcmseqTableDAM agcmseqIO = new AgcmseqTableDAM();
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
	private DacmTableDAM dacmIO = new DacmTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifernlTableDAM lifernlIO = new LifernlTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PcddlnbTableDAM pcddlnbIO = new PcddlnbTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	private ZctnTableDAM zctnIO = new ZctnTableDAM();
	private ZptnTableDAM zptnIO = new ZptnTableDAM();
	private ZrstTableDAM zrstIO = new ZrstTableDAM();
	private ZrstnudTableDAM zrstnudIO = new ZrstnudTableDAM();
	private Itemkey wsaaItemkey = new Itemkey();
	private Varcom varcom = new Varcom();
	private Getdescrec getdescrec = new Getdescrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Freqcpy freqcpy = new Freqcpy();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private T5675rec t5675rec = new T5675rec();
	private T6598rec t6598rec = new T6598rec();
	private T5645rec t5645rec = new T5645rec();
	private T5691rec t5691rec = new T5691rec();
	private T5688rec t5688rec = new T5688rec();
	private T5687rec t5687rec = new T5687rec();
	private Th605rec th605rec = new Th605rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Premiumrec premiumrec = new Premiumrec();
	private T5644rec t5644rec = new T5644rec();
	private T5647rec t5647rec = new T5647rec();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private T5534rec t5534rec = new T5534rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Zorlnkrec zorlnkrec = new Zorlnkrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Ubblallpar ubblallpar = new Ubblallpar();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ExternalisedRules er = new ExternalisedRules();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		skipBonusWorkbench170, 
		skipReadingT56881050, 
		skipReadingT16881060, 
		exit1490, 
		skipReadingT65982150, 
		findPremCalcSubroutine2400, 
		skipReadingT56752450, 
		exit2900, 
		skipTr52d3400, 
		skipTr52e3400, 
		periodicFeeTax3400, 
		issueFeeTax3400, 
		adminChargeTax3400, 
		topupAdminTax3400, 
		topupFeeTax3400, 
		continue3400, 
		exit3400, 
		postPrem4020, 
		postFee4030, 
		perfeeamn4200, 
		postInitFee4050, 
		inifeeamn4300, 
		postAdminCharges4400, 
		callPcddlnbio7020, 
		callCommPay7060, 
		setAgcmValue7070, 
		calcServComm7080, 
		calcRenlComm7090, 
		writeAgcm7100, 
		nextPcddlnb7180, 
		exit7190, 
		postCommEarn7620, 
		postCommPay7630, 
		postServEarn7720, 
		postRenlEarn7820, 
		nextr8120, 
		exit8190, 
		exit8290, 
		c220Call, 
		c280Next, 
		c290Exit, 
		c320Loop, 
		c380Next, 
		c390Exit
	}

	public Ubblmt3() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		ubblallpar.ubblallRec = convertAndSetParam(ubblallpar.ubblallRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start110();
				case skipBonusWorkbench170: 
					skipBonusWorkbench170();
					exit180();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start110()
	{
		initialise1000();
		determinePremCalcMeth1500();
		calculateRiderPremium2000();
		updateCoverage3000();
		wsaaTotalCd.set(ZERO);
		wsaaSeqno.set(ZERO);
		/* INITIALIZE                  ZRST-PARAMS.                     */
		/* MOVE UBBL-CHDR-CHDRCOY      TO ZRST-CHDRCOY.                 */
		/* MOVE UBBL-CHDR-CHDRNUM      TO ZRST-CHDRNUM.                 */
		/* MOVE UBBL-LIFE-LIFE         TO ZRST-LIFE.                    */
		/*    MOVE UBBL-LIFE-JLIFE        TO ZRST-JLIFE.*/
		/* MOVE SPACES                 TO ZRST-COVERAGE                 */
		/*                                ZRST-JLIFE                    */
		/*                                ZRST-RIDER.                   */
		/* MOVE ZRSTREC                TO ZRST-FORMAT.                  */
		/* MOVE BEGN                   TO ZRST-FUNCTION.                */
		/* PERFORM B300-CHECK-ZRST-EXISTS UNTIL ZRST-STATUZ = ENDP.     */
		zrstnudIO.setParams(SPACES);
		zrstnudIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		zrstnudIO.setChdrnum(ubblallpar.chdrChdrnum);
		zrstnudIO.setLife(ubblallpar.lifeLife);
		zrstnudIO.setFormat(formatsInner.zrstnudrec);
		zrstnudIO.setFunction(varcom.begn);
		while ( !(isEQ(zrstnudIO.getStatuz(),varcom.endp))) {
			//performance improvement --  atiwari23 
			zrstnudIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			zrstnudIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");
		

			b300CheckZrstExists();
		}
		
		/* Read table TH605 to see if Bonus Workbench Extraction used      */
		/*    MOVE SPACES                 TO ITEM-PARAMS.          <V73L01>*/
		/*    MOVE 'IT'                   TO ITEM-ITEMPFX.         <V73L01>*/
		/*    MOVE TH605                  TO ITEM-ITEMTABL.        <V73L01>*/
		/*    MOVE UBBL-CHDR-CHDRCOY      TO ITEM-ITEMCOY          <V73L01>*/
		/*                                   ITEM-ITEMITEM.        <V73L01>*/
		/*    MOVE READR                  TO ITEM-FUNCTION.        <V73L01>*/
		/*    MOVE ITEMREC                TO ITEM-FORMAT.          <V73L01>*/
		/*    CALL 'ITEMIO'               USING ITEM-PARAMS.       <V73L01>*/
		/*    IF ITEM-STATUZ          NOT = O-K AND MRNF           <V73L01>*/
		/*       MOVE ITEM-PARAMS         TO SYSR-PARAMS           <V73L01>*/
		/*       MOVE ITEM-STATUZ         TO SYSR-STATUZ           <V73L01>*/
		/*       PERFORM 580-DATABASE-ERROR                        <V73L01>*/
		/*    END-IF.                                              <V73L01>*/
		/*    MOVE ITEM-GENAREA           TO TH605-TH605-REC.      <V73L01>*/
		if (isNE(th605rec.bonusInd,"Y")) {
			goTo(GotoLabel.skipBonusWorkbench170);
		}
		/* Bonus Workbench *                                               */
		if (isNE(premiumrec.calcPrem,ZERO)) {
			if (isEQ(ubblallpar.function,"ISSUE")) {
				c500WriteZptn();
			}
			else {
				c200PremiumHistory();
				c300WriteArrays();
			}
		}
	}

protected void skipBonusWorkbench170()
	{
		processTax3400();
		rewriteCovrRecord3500();
		updateLedgerAccounts4000();
		calcCommission6000();
		/* INITIALIZE                  ZRST-PARAMS.                     */
		/* MOVE UBBL-CHDR-CHDRCOY      TO ZRST-CHDRCOY.                 */
		/* MOVE UBBL-CHDR-CHDRNUM      TO ZRST-CHDRNUM.                 */
		/* MOVE UBBL-LIFE-LIFE         TO ZRST-LIFE.                    */
		/*    MOVE UBBL-LIFE-JLIFE        TO ZRST-JLIFE.*/
		/* MOVE SPACES                 TO ZRST-COVERAGE                 */
		/*                                ZRST-JLIFE                    */
		/*                                ZRST-RIDER.                   */
		/* MOVE ZRSTREC                TO ZRST-FORMAT.                  */
		/*MOVE BEGNH                  TO ZRST-FUNCTION.                */
		/* MOVE BEGN                   TO ZRST-FUNCTION.        <V65L16>*/
		/* PERFORM B400-UPDATE-TOTAL-CD UNTIL ZRST-STATUZ = ENDP.       */
		zrstnudIO.setParams(SPACES);
		zrstnudIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		zrstnudIO.setChdrnum(ubblallpar.chdrChdrnum);
		zrstnudIO.setLife(ubblallpar.lifeLife);
		zrstnudIO.setFormat(formatsInner.zrstnudrec);
		zrstnudIO.setFunction(varcom.begn);
		while ( !(isEQ(zrstnudIO.getStatuz(),varcom.endp))) {
			//performance improvement --  atiwari23 
			zrstnudIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			zrstnudIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");
		
			b400UpdateTotalCd();
		}
		
	}

protected void exit180()
	{
		exitProgram();
	}

protected void stop190()
	{
		stopRun();
	}

protected void systemError570()
	{
			para570();
			exit579();
		}

protected void para570()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit579()
	{
		ubblallpar.statuz.set(varcom.bomb);
		exit180();
	}

protected void databaseError580()
	{
			para580();
			exit589();
		}

protected void para580()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit589()
	{
		ubblallpar.statuz.set(varcom.bomb);
		exit180();
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1010();
				case skipReadingT56881050: 
					skipReadingT56881050();
				case skipReadingT16881060: 
					skipReadingT16881060();
				case exit1490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1010()
	{
		syserrrec.subrname.set(wsaaSubr);
		ubblallpar.statuz.set(varcom.oK);
		wsaaAccumSurrenderValue.set(0);
		wsaaSequenceNo.set(1);
		wsaaCovrTot.set(0);
		varcom.vrcmTime.set(getCobolTime());
		/* Read T5691 for Administration Fees Amounts.*/
		wsaaT5691Curr.set(ubblallpar.cntcurr);
		wsaaT5691Adm.set(ubblallpar.adfeemth);
		wsaaItemkey.set(SPACES);
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itdmIO.setItemtabl(tablesInner.t5691);
		itdmIO.setItmfrm(ubblallpar.effdate);
		itdmIO.setItemitem(wsaaT5691Key);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	

		
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isNE(itdmIO.getItemcoy(),ubblallpar.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5691)
		|| isNE(itdmIO.getItemitem(),wsaaT5691Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(wsaaT5691Key);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(t071);
			databaseError580();
			goTo(GotoLabel.exit1490);
		}
		else {
			t5691rec.t5691Rec.set(itdmIO.getGenarea());
		}
		/* Read T5645 for subaccount details.*/
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(ubblallpar.chdrChdrcoy);
		wsaaItemkey.itemItemtabl.set(tablesInner.t5645);
		wsaaItemkey.itemItemitem.set(wsaaSubr);
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			databaseError580();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Read T5688 to find out if contract type wants Component*/
		/*  level Accounting.*/
		if (isNE(ubblallpar.comlvlacc,SPACES)) {
			t5688rec.comlvlacc.set(ubblallpar.comlvlacc);
			goTo(GotoLabel.skipReadingT56881050);
		}
		wsaaItemkey.set(SPACES);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItmfrm(ubblallpar.effdate);
		itdmIO.setItemitem(ubblallpar.cnttype);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isNE(itdmIO.getItemcoy(),ubblallpar.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(),ubblallpar.cnttype)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(ubblallpar.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			databaseError580();
			goTo(GotoLabel.exit1490);
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}

protected void skipReadingT56881050()
	{
		/* Read T5687 to get the benefit billing type*/
		wsaaItemkey.set(SPACES);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItmfrm(ubblallpar.effdate);
		itdmIO.setItemitem(ubblallpar.crtable);
		itdmIO.setFunction(varcom.begn);
		
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isNE(itdmIO.getItemcoy(),ubblallpar.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(),ubblallpar.crtable)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(ubblallpar.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			databaseError580();
			goTo(GotoLabel.exit1490);
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		/* Get transaction description.*/
		if (isNE(ubblallpar.trandesc,SPACES)) {
			getdescrec.longdesc.set(ubblallpar.trandesc);
			goTo(GotoLabel.skipReadingT16881060);
		}
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(ubblallpar.chdrChdrcoy);
		wsaaItemkey.itemItemtabl.set(tablesInner.t1688);
		wsaaItemkey.itemItemitem.set(ubblallpar.batctrcde);
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set(ubblallpar.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			getdescrec.longdesc.set(SPACES);
		}
	}

protected void skipReadingT16881060()
	{
		/* Read T5534 to get the benefit billing frequency*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tablesInner.t5534);
		itemIO.setItemitem(t5687rec.bbmeth);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError570();
		}
		t5534rec.t5534Rec.set(itemIO.getGenarea());
		/* Get the today date                                              */
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			systemError570();
		}
		wsaaToday.set(datcon1rec.intDate);
		/* Initialize the AGCM arrays                                      */
		c100InitializeArrays();
		wsaaBillfq.set(t5534rec.unitFreq);
		c000ReadPayr();
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.freqFactor.set(1);
		datcon2rec.frequency.set(t5534rec.unitFreq);
		datcon2rec.intDate1.set(ubblallpar.effdate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			systemError570();
		}
		wsaaPtdate.set(datcon2rec.intDate2);
		readTableTh6051400();
	}

protected void readTableTh6051400()
	{
		start1411();
	}

protected void start1411()
	{
		/* Read TH605 to see whether system is "Override based on Agent    */
		/* Details"                                                        */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tablesInner.th605);
		itemIO.setItemitem(ubblallpar.chdrChdrcoy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			systemError570();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
	}

protected void determinePremCalcMeth1500()
	{
			para1505();
			para1510();
		}

	/**
	* <pre>
	* Read LIFERNL to obtain sex & age for prem calculation
	* </pre>
	*/
protected void para1505()
	{
		premiumrec.premiumRec.set(SPACES);
		lifernlIO.setDataArea(SPACES);
		lifernlIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		lifernlIO.setChdrnum(ubblallpar.chdrChdrnum);
		lifernlIO.setLife(ubblallpar.lifeLife);
		lifernlIO.setJlife(ZERO);
		lifernlIO.setFormat(formatsInner.lifernlrec);
		lifernlIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		lifernlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifernlIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");
		SmartFileCode.execute(appVars, lifernlIO);
		if (isNE(lifernlIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifernlIO.getParams());
			databaseError580();
		}
		if ((isNE(lifernlIO.getChdrcoy(),ubblallpar.chdrChdrcoy))
		|| (isNE(lifernlIO.getChdrnum(),ubblallpar.chdrChdrnum))
		|| (isNE(lifernlIO.getLife(),ubblallpar.lifeLife))) {
			lifernlIO.setStatuz(varcom.endp);
			syserrrec.params.set(lifernlIO.getParams());
			databaseError580();
		}
		premiumrec.lsex.set(lifernlIO.getCltsex());
		/* get correct age*/
		/* PERFORM 1700-READ-CONTRACT.                                  */
		calcCorrectAge1800();
	}

protected void para1510()
	{
		/* Read LIFERNL to see if a joint life exists.*/
		/* Which benefit billing to be used is dependent on whether there*/
		/* is a joint life.*/
		lifernlIO.setDataArea(SPACES);
		lifernlIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		lifernlIO.setChdrnum(ubblallpar.chdrChdrnum);
		lifernlIO.setLife(ubblallpar.lifeLife);
		lifernlIO.setJlife("01");
		lifernlIO.setFormat(formatsInner.lifernlrec);
		lifernlIO.setFunction(varcom.begn);
		
		//performance improvement --  atiwari23 
		lifernlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifernlIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");
		SmartFileCode.execute(appVars, lifernlIO);
		if ((isNE(lifernlIO.getStatuz(),varcom.oK))
		&& (isNE(lifernlIO.getStatuz(),varcom.endp))) {
			syserrrec.statuz.set(lifernlIO.getStatuz());
			syserrrec.params.set(lifernlIO.getParams());
			databaseError580();
		}
		/* End of file, hence no joint life.*/
		if ((isEQ(lifernlIO.getStatuz(),varcom.endp))) {
			wsaaPremMeth.set(ubblallpar.premMeth);
			premiumrec.jlage.set(ZERO);
			premiumrec.jlsex.set(SPACES);
			return ;
		}
		/*     GO TO 1900-EXIT.*/
		/* If key breaks, no joint life else joint life exists.*/
		if ((isNE(lifernlIO.getChdrcoy(),ubblallpar.chdrChdrcoy))
		|| (isNE(lifernlIO.getChdrnum(),ubblallpar.chdrChdrnum))
		|| (isNE(lifernlIO.getLife(),ubblallpar.lifeLife))) {
			wsaaPremMeth.set(ubblallpar.premMeth);
			premiumrec.jlage.set(ZERO);
			premiumrec.jlsex.set(SPACES);
		}
		else {
			wsaaPremMeth.set(ubblallpar.jlifePremMeth);
			premiumrec.jlsex.set(lifernlIO.getCltsex());
			calcCorrectAge1800();
		}
	}

	/**
	* <pre>
	*1700-READ-CONTRACT SECTION.                                      
	*1700-START.                                                      
	**** MOVE SPACES                 TO CHDRENQ-PARAMS.               
	**** MOVE UBBL-CHDR-CHDRCOY      TO CHDRENQ-CHDRCOY.              
	**** MOVE UBBL-CHDR-CHDRNUM      TO CHDRENQ-CHDRNUM.              
	**** MOVE CHDRENQREC             TO CHDRENQ-FORMAT.               
	**** MOVE READR                  TO CHDRENQ-FUNCTION.             
	**** CALL 'CHDRENQIO'            USING CHDRENQ-PARAMS.            
	**** IF CHDRENQ-STATUZ           NOT = O-K                        
	****     MOVE CHDRENQ-PARAMS     TO SYSR-PARAMS                   
	****     MOVE CHDRENQ-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 570-SYSTEM-ERROR                                 
	**** END-IF.                                                      
	*1790-EXIT.                                                       
	****  EXIT.                                                       
	* </pre>
	*/
protected void calcCorrectAge1800()
	{
			start1800();
		}

protected void start1800()
	{
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(ubblallpar.language);
		/* MOVE CHDRENQ-CNTTYPE        TO AGEC-CNTTYPE.                 */
		agecalcrec.cnttype.set(ubblallpar.cnttype);
		agecalcrec.intDate1.set(lifernlIO.getCltdob());
		agecalcrec.intDate2.set(ubblallpar.effdate);
		agecalcrec.company.set("9");
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isEQ(agecalcrec.statuz,"IVFD")) {
			syserrrec.statuz.set(f401);
			systemError570();
			return ;
		}
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			systemError570();
		}
		if (isEQ(lifernlIO.getJlife(),"01")) {
			premiumrec.jlage.set(agecalcrec.agerating);
		}
		else {
			premiumrec.lage.set(agecalcrec.agerating);
		}
	}

protected void calculateRiderPremium2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					findSurrenderSubroutine2100();
				case skipReadingT65982150: 
					skipReadingT65982150();
				case findPremCalcSubroutine2400: 
					findPremCalcSubroutine2400();
				case skipReadingT56752450: 
					skipReadingT56752450();
					calculatePremium2500();
				case exit2900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void findSurrenderSubroutine2100()
	{
		/* Use UBBL-SV-METHOD to access T6598 for the surrender value*/
		/* calculation subroutine.*/
		/* Check first if a surrender value exists.*/
		if (isNE(ubblallpar.svCalcprog,SPACES)) {
			t6598rec.calcprog.set(ubblallpar.svCalcprog);
			goTo(GotoLabel.skipReadingT65982150);
		}
		if (isEQ(ubblallpar.svMethod,SPACES)) {
			wsaaAccumSurrenderValue.set(ZERO);
			goTo(GotoLabel.findPremCalcSubroutine2400);
		}
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemitem(ubblallpar.svMethod);
		itemIO.setItemtabl(tablesInner.t6598);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError580();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
	}

protected void skipReadingT65982150()
	{
		if (isEQ(t6598rec.calcprog,SPACES)) {
			wsaaAccumSurrenderValue.set(0);
			goTo(GotoLabel.findPremCalcSubroutine2400);
		}
		/*CALCULATE-SURRENDER-VALUE*/
		/* To be put in!!*/
		/* As of 10/7/92, it has been put it !!*/
		callSurrSubroutine5000();
	}

protected void findPremCalcSubroutine2400()
	{
		/* Use UBBL-PREM-METH or UBBL-JLIFE-PREM-METH T5675 for the*/
		/* premium calculation subroutine.*/
		/* Check if UBBL-PREM-METH or UBBL-JLIFE-PREM-METH exists.*/
		if (isNE(ubblallpar.premsubr,SPACES)
		&& isEQ(premiumrec.jlsex,SPACES)) {
			t5675rec.premsubr.set(ubblallpar.premsubr);
			goTo(GotoLabel.skipReadingT56752450);
		}
		if (isNE(ubblallpar.jpremsubr,SPACES)
		&& isNE(premiumrec.jlsex,SPACES)) {
			t5675rec.premsubr.set(ubblallpar.jpremsubr);
			goTo(GotoLabel.skipReadingT56752450);
		}
		if (isEQ(wsaaPremMeth,SPACES)) {
			premiumrec.calcPrem.set(0);
			premiumrec.calcBasPrem.set(0);
			premiumrec.calcLoaPrem.set(0);
			goTo(GotoLabel.exit2900);
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tablesInner.t5675);
		itemIO.setItemitem(wsaaPremMeth);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError580();
		}
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

protected void skipReadingT56752450()
	{
		if (isNE(t5675rec.premsubr,SPACES)) {
			wsaaCallSubr.set(t5675rec.premsubr);
		}
		else {
			premiumrec.calcPrem.set(0);
			premiumrec.calcBasPrem.set(0);
			premiumrec.calcLoaPrem.set(0);
			goTo(GotoLabel.exit2900);
		}
	}

protected void calculatePremium2500()
	{
		/* Calculation routine is known, so set up parameters and called*/
		/* the subroutine.*/
		premiumrec.effectdt.set(0);
		premiumrec.calcPrem.set(0);
		premiumrec.calcBasPrem.set(0);
		premiumrec.calcLoaPrem.set(0);
		premiumrec.commissionPrem.set(0);//IBPLIFE-5237
		premiumrec.reRateDate.set(0);
		premiumrec.ratingdate.set(0);
		premiumrec.effectdt.set(ubblallpar.effdate);
		premiumrec.reRateDate.set(ubblallpar.effdate);
		premiumrec.ratingdate.set(ubblallpar.effdate);
		premiumrec.function.set(varcom.calc);
		premiumrec.crtable.set(ubblallpar.crtable);
		premiumrec.chdrChdrcoy.set(ubblallpar.chdrChdrcoy);
		premiumrec.chdrChdrnum.set(ubblallpar.chdrChdrnum);
		premiumrec.lifeLife.set(ubblallpar.lifeLife);
		premiumrec.lifeJlife.set(ubblallpar.lifeJlife);
		premiumrec.covrCoverage.set(ubblallpar.covrCoverage);
		premiumrec.covrRider.set(ubblallpar.covrRider);
		premiumrec.termdate.set(ubblallpar.premCessDate);
		premiumrec.billfreq.set(ubblallpar.billfreq);
		premiumrec.plnsfx.set(ubblallpar.planSuffix);
		premiumrec.effectdt.set(ubblallpar.effdate);
		premiumrec.currcode.set(ubblallpar.cntcurr);
		premiumrec.mop.set(ubblallpar.billchnl);
		premiumrec.mortcls.set(ubblallpar.mortcls);
		datcon3rec.function.set(SPACES);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError570();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/* Work out CPRM-SUMIN as the sum at risk.*/
		/* Check if Sum at risk is negative - if so zeroise it.*/
		compute(premiumrec.sumin, 3).setRounded(sub(ubblallpar.sumins,wsaaAccumSurrenderValue));
		if (isLT(premiumrec.sumin,0)) {
			premiumrec.sumin.set(ZERO);
		}
		/* As this is a  Unit Linked Program, there is no*/
		/* need for the Linkage to be set up with ANNY details.*/
		/* Therefore, all fields are initialised before the call*/
		/* to the Subroutine.*/
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
		premiumrec.language.set(ubblallpar.language);
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		//Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(wsaaCallSubr.toString())))
		{
			callProgram(wsaaCallSubr, premiumrec.premiumRec);
		}
		else
		{		

		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
		vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
		Vpxlextrec vpxlextrec = new Vpxlextrec();
		vpxlextrec.function.set("INIT");
		callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
		
		Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
		vpxchdrrec.function.set("INIT");
		callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
		premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
		premiumrec.cnttype.set(ubblallpar.cnttype);
		Vpxacblrec vpxacblrec=new Vpxacblrec();
		callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]		
		premiumrec.premMethod.set(wsaaPremMeth);
		callProgram(wsaaCallSubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		//Ticket #IVE-792 - End
		/*Ticket #ILIFE-2005 - End*/
		if (isNE(premiumrec.statuz,varcom.oK)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			systemError570();
		}
		zrdecplrec.amountIn.set(premiumrec.calcPrem);
		a000CallRounding();
		premiumrec.calcPrem.set(zrdecplrec.amountOut);
	}

protected void updateCoverage3000()
	{
		readCovr3010();
	}

	/**
	* <pre>
	* The amount should add to the CRDEBT field in the COVERAGE
	* record even the benefit billed amount is for a attached rider.
	* </pre>
	*/
protected void readCovr3010()
	{
		covrIO.setDataArea(SPACES);
		covrIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		covrIO.setChdrnum(ubblallpar.chdrChdrnum);
		covrIO.setLife(ubblallpar.lifeLife);
		covrIO.setCoverage(ubblallpar.covrCoverage);
		covrIO.setRider("00");
		covrIO.setPlanSuffix(ubblallpar.planSuffix);
		covrIO.setFunction(varcom.readh);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			databaseError580();
		}
		/* ADD CPRM-CALC-PREM          TO COVR-COVERAGE-DEBT.*/
		/* MOVE CPRM-CALC-PREM         TO WSAA-COVR-TOT.*/
		if (isNE(ubblallpar.function,"TOPUP")) {
			setPrecision(covrIO.getCoverageDebt(), 2);
			covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(),premiumrec.calcPrem));
			wsaaCovrTot.set(premiumrec.calcPrem);
		}
		/* Check if this subroutine is called from the Issue program or*/
		/* from the Renewals program. Add the Administration Fee amount*/
		/* to the coverage debt.*/
		wsaaIniFee.set(ZERO);
		wsaaPerFee.set(ZERO);
		wsaaAdmChgs.set(ZERO);
		wsaaTopupFee.set(ZERO);
		wsaaTopupAdmChgs.set(ZERO);
		if (isEQ(ubblallpar.function,"ISSUE")) {
			/*    We must first correctly proportion the initial Admin fee*/
			/*     across the policies we are processing*/
			if (isNE(ubblallpar.planSuffix,ZERO)) {
				/*            if plan-suffix is not zeros, then we are dealing*/
				/*             with a broken-out policy so to get the fee for*/
				/*             it we just divide the fee by the number of*/
				/*             policies in the plan (POLINC ).*/
				compute(wsaaIniFee, 4).setRounded(div(t5691rec.inifeeamn, ubblallpar.polinc));
				zrdecplrec.amountIn.set(wsaaIniFee);
				a000CallRounding();
				wsaaIniFee.set(zrdecplrec.amountOut);
			}
			if (isEQ(ubblallpar.planSuffix,ZERO)) {
				/*            if we get here, then we either have a fully or*/
				/*            partially summarised record.*/
				/*            - if a full summary, then the no of*/
				/*              summarised policies will be the same as*/
				/*              the total no of policies in the plan...*/
				/*            i.e. POLSUM = POLINC.*/
				/*               Unless there is only 1 policy in the plan, in*/
				/*               which case POLSUM = 0 and thus the whole fee*/
				/*               is applied.*/
				/*            - if a partial summary, then the no of*/
				/*              summarised policies in the plan will be LESS*/
				/*              than the total no of policies in the plan...*/
				/*            i.e. POLSUM < POLINC.*/
				if (isEQ(ubblallpar.polsum,ubblallpar.polinc)
				|| isEQ(ubblallpar.polsum,ZERO)) {
					wsaaIniFee.set(t5691rec.inifeeamn);
				}
				else {
					compute(wsaaIniFee, 4).setRounded(mult((div(t5691rec.inifeeamn, ubblallpar.polinc)), ubblallpar.polsum));
					zrdecplrec.amountIn.set(wsaaIniFee);
					a000CallRounding();
					wsaaIniFee.set(zrdecplrec.amountOut);
				}
			}
			/*       ADD the initial fee to the total Coverage debt*/
			setPrecision(covrIO.getCoverageDebt(), 3);
			covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(),wsaaIniFee));
			wsaaCovrTot.add(wsaaIniFee);
			/* Add the Administrative Charges to the Total Coverage Debt*/
			compute(wsaaAdmChgs, 4).setRounded((div((mult(covrIO.getSingp(), t5691rec.zradmnpc)), 100)));
			zrdecplrec.amountIn.set(wsaaAdmChgs);
			a000CallRounding();
			wsaaAdmChgs.set(zrdecplrec.amountOut);
			setPrecision(covrIO.getCoverageDebt(), 3);
			covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(),wsaaAdmChgs));
			wsaaCovrTot.add(wsaaAdmChgs);
			/*****    ADD T5691-INIFEEAMN      TO COVR-COVERAGE-DEBT*/
			/*****    ADD T5691-INIFEEAMN      TO WSAA-COVR-TOT*/
		}
		/* To calculate the TopUp Fees & Admin Charges.*/
		if (isEQ(ubblallpar.function,"TOPUP")) {
			if (isNE(ubblallpar.planSuffix,ZERO)) {
				compute(wsaaTopupFee, 4).setRounded(div(t5691rec.zrtupfee, ubblallpar.polinc));
				zrdecplrec.amountIn.set(wsaaTopupFee);
				a000CallRounding();
				wsaaTopupFee.set(zrdecplrec.amountOut);
			}
			if (isEQ(ubblallpar.planSuffix,ZERO)) {
				if (isEQ(ubblallpar.polsum,ubblallpar.polinc)
				|| isEQ(ubblallpar.polsum,ZERO)) {
					wsaaTopupFee.set(t5691rec.zrtupfee);
				}
				else {
					compute(wsaaTopupFee, 4).setRounded(mult((div(t5691rec.zrtupfee, ubblallpar.polinc)), ubblallpar.polsum));
					zrdecplrec.amountIn.set(wsaaTopupFee);
					a000CallRounding();
					wsaaTopupFee.set(zrdecplrec.amountOut);
				}
			}
			/* Add the TopUp fee to the total Coverage debt*/
			setPrecision(covrIO.getCoverageDebt(), 3);
			covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(),wsaaTopupFee));
			wsaaCovrTot.add(wsaaTopupFee);
			/* Add the TopUp Administrative Charges to the Total Coverage Debt*/
			compute(wsaaTopupAdmChgs, 4).setRounded((div((mult(t5691rec.zrtupadpc, ubblallpar.singp)), 100)));
			zrdecplrec.amountIn.set(wsaaTopupAdmChgs);
			a000CallRounding();
			wsaaTopupAdmChgs.set(zrdecplrec.amountOut);
			setPrecision(covrIO.getCoverageDebt(), 3);
			covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(),wsaaTopupAdmChgs));
			wsaaCovrTot.add(wsaaTopupAdmChgs);
		}
		/*    We must  proportion the Periodic Admin fee*/
		/*     across the policies we are processing*/
		if (isNE(ubblallpar.planSuffix,ZERO)) {
			/*        if plan-suffix is not zeros, then we are dealing*/
			/*         with a broken-out policy so to get the fee for*/
			/*         it we just divide the fee by the number of*/
			/*         policies in the plan (POLINC ).*/
			compute(wsaaPerFee, 4).setRounded(div(t5691rec.perfeeamn, ubblallpar.polinc));
			zrdecplrec.amountIn.set(wsaaPerFee);
			a000CallRounding();
			wsaaPerFee.set(zrdecplrec.amountOut);
		}
		if (isEQ(ubblallpar.planSuffix,ZERO)) {
			/*        if we get here, then we either have a fully or*/
			/*        partially summarised record.*/
			/*        - if a full summary, then the no of*/
			/*          summarised policies will be the same as*/
			/*          the total no of policies in the plan...*/
			/*        i.e. POLSUM = POLINC.*/
			/*           Unless there is only 1 policy in the plan, in*/
			/*           which case POLSUM = 0 and thus the whole fee*/
			/*           is applied.*/
			/*        - if a partial summary, then the no of*/
			/*          summarised policies in the plan will be LESS*/
			/*          than the total no of policies in the plan...*/
			/*        i.e. POLSUM < POLINC.*/
			if (isEQ(ubblallpar.polsum,ubblallpar.polinc)
			|| isEQ(ubblallpar.polsum,ZERO)) {
				wsaaPerFee.set(t5691rec.perfeeamn);
			}
			else {
				compute(wsaaPerFee, 4).setRounded(mult((div(t5691rec.perfeeamn, ubblallpar.polinc)), ubblallpar.polsum));
				zrdecplrec.amountIn.set(wsaaPerFee);
				a000CallRounding();
				wsaaPerFee.set(zrdecplrec.amountOut);
			}
		}
		/*       ADD the periodic fee to the total Coverage debt*/
		/* For TopUp, there is no periodic fee.*/
		if (isEQ(ubblallpar.function,"TOPUP")) {
			wsaaPerFee.set(ZERO);
		}
		setPrecision(covrIO.getCoverageDebt(), 3);
		covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(),wsaaPerFee));
		wsaaCovrTot.add(wsaaPerFee);
	}

protected void processTax3400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3400();
				case skipTr52d3400: 
					skipTr52d3400();
					readTaxControl3400();
				case skipTr52e3400: 
					skipTr52e3400();
					mortalityChargeTax3400();
				case periodicFeeTax3400: 
					periodicFeeTax3400();
				case issueFeeTax3400: 
					issueFeeTax3400();
				case adminChargeTax3400: 
					adminChargeTax3400();
				case topupAdminTax3400: 
					topupAdminTax3400();
				case topupFeeTax3400: 
					topupFeeTax3400();
				case continue3400: 
					continue3400();
				case exit3400: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3400()
	{
		wsaaTotalTax01.set(0);
		wsaaTotalTax02.set(0);
		if (isEQ(premiumrec.calcPrem, 0)
		&& isEQ(wsaaIniFee, 0)
		&& isEQ(wsaaAdmChgs, 0)
		&& isEQ(wsaaTopupFee, 0)
		&& isEQ(wsaaTopupAdmChgs, 0)
		&& isEQ(wsaaPerFee, 0)) {
			goTo(GotoLabel.exit3400);
		}
		if (isEQ(wsaaPrevRegister, ubblallpar.chdrRegister)) {
			goTo(GotoLabel.skipTr52d3400);
		}
		wsaaPrevRegister.set(ubblallpar.chdrRegister);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(ubblallpar.chdrRegister);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			databaseError580();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				databaseError580();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
	}

protected void skipTr52d3400()
	{
		if (isEQ(tr52drec.txcode, SPACES)) {
			goTo(GotoLabel.exit3400);
		}
	}

protected void readTaxControl3400()
	{
		if (isEQ(wsaaPrevTxcode, tr52drec.txcode)) {
			if (isEQ(wsaaPrevCnttype, ubblallpar.cnttype)
			&& isEQ(wsaaPrevCrtable, ubblallpar.crtable)) {
				goTo(GotoLabel.skipTr52e3400);
			}
		}
		wsaaPrevTxcode.set(tr52drec.txcode);
		wsaaPrevCnttype.set(ubblallpar.cnttype);
		wsaaPrevCrtable.set(ubblallpar.crtable);
		/* Read table TR52E                                                */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tablesInner.tr52e);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(ubblallpar.cnttype);
		wsaaTr52eCrtable.set(covrIO.getCrtable());
		itemIO.setItemitem(wsaaTr52eKey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError580();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
			itemIO.setItemtabl(tablesInner.tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(ubblallpar.cnttype);
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				databaseError580();
			}
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
			itemIO.setItemtabl(tablesInner.tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				databaseError580();
			}
		}
		tr52erec.tr52eRec.set(itemIO.getGenarea());
	}

protected void skipTr52e3400()
	{
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(ubblallpar.chdrChdrcoy);
		txcalcrec.chdrnum.set(ubblallpar.chdrChdrnum);
		txcalcrec.life.set(ubblallpar.lifeLife);
		txcalcrec.coverage.set(ubblallpar.covrCoverage);
		txcalcrec.rider.set(ubblallpar.covrRider);
		txcalcrec.planSuffix.set(ubblallpar.planSuffix);
		txcalcrec.crtable.set(ubblallpar.crtable);
		txcalcrec.cnttype.set(ubblallpar.cnttype);
		txcalcrec.register.set(ubblallpar.chdrRegister);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaCntCurr.set(ubblallpar.cntcurr);
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.txcode.set(tr52drec.txcode);
		wsaaBatccoy.set(ubblallpar.batccoy);
		wsaaBatcbrn.set(ubblallpar.batcbrn);
		wsaaBatcactyr.set(ubblallpar.batcactyr);
		wsaaBatcactmn.set(ubblallpar.batcactmn);
		wsaaBatctrcde.set(ubblallpar.batctrcde);
		wsaaBatcbatch.set(ubblallpar.batch);
		txcalcrec.batckey.set(wsaaBatckey);
		txcalcrec.ccy.set(ubblallpar.cntcurr);
		txcalcrec.effdate.set(ubblallpar.effdate);
		txcalcrec.taxAmt[1].set(0);
		txcalcrec.taxAmt[2].set(0);
		txcalcrec.tranno.set(ubblallpar.tranno);
		txcalcrec.language.set(ubblallpar.language);
		/*  Set the value of TAXD record.                                  */
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		taxdIO.setChdrnum(ubblallpar.chdrChdrnum);
		taxdIO.setLife(ubblallpar.lifeLife);
		taxdIO.setCoverage(ubblallpar.covrCoverage);
		taxdIO.setRider(ubblallpar.covrRider);
		taxdIO.setPlansfx(ubblallpar.planSuffix);
		taxdIO.setEffdate(ubblallpar.effdate);
		taxdIO.setInstfrom(ubblallpar.effdate);
		taxdIO.setInstto(varcom.vrcmMaxDate);
		taxdIO.setBillcd(varcom.vrcmMaxDate);
		taxdIO.setTranno(ubblallpar.tranno);
	}

protected void mortalityChargeTax3400()
	{
		if (isEQ(tr52erec.taxind06, "Y")
		&& isNE(premiumrec.calcPrem, 0)
		&& isNE(ubblallpar.function, "TOPUP")) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.periodicFeeTax3400);
		}
		if (isEQ(tr52erec.zbastyp, "Y")) {
			txcalcrec.amountIn.set(premiumrec.calcBasPrem);
		}
		else {
			txcalcrec.amountIn.set(premiumrec.calcPrem);
		}
		txcalcrec.transType.set("MCHG");
		calcTax3600();
	}

protected void periodicFeeTax3400()
	{
		if (isEQ(tr52erec.taxind07, "Y")
		&& isNE(wsaaPerFee, 0)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.issueFeeTax3400);
		}
		txcalcrec.amountIn.set(wsaaPerFee);
		txcalcrec.transType.set("PERF");
		calcTax3600();
	}

protected void issueFeeTax3400()
	{
		if (isEQ(tr52erec.taxind08, "Y")
		&& isNE(wsaaIniFee, 0)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.adminChargeTax3400);
		}
		txcalcrec.amountIn.set(wsaaIniFee);
		txcalcrec.transType.set("ISSF");
		calcTax3600();
	}

protected void adminChargeTax3400()
	{
		if (isEQ(tr52erec.taxind09, "Y")
		&& isNE(wsaaAdmChgs, 0)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.topupAdminTax3400);
		}
		txcalcrec.amountIn.set(wsaaAdmChgs);
		txcalcrec.transType.set("ADMF");
		calcTax3600();
	}

protected void topupAdminTax3400()
	{
		if (isEQ(tr52erec.taxind10, "Y")
		&& isNE(wsaaTopupAdmChgs, 0)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.topupFeeTax3400);
		}
		txcalcrec.amountIn.set(wsaaTopupAdmChgs);
		txcalcrec.transType.set("TOPA");
		calcTax3600();
	}

protected void topupFeeTax3400()
	{
		if (isEQ(tr52erec.taxind10, "Y")
		&& isNE(wsaaTopupFee, 0)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.continue3400);
		}
		txcalcrec.amountIn.set(wsaaTopupFee);
		txcalcrec.transType.set("TOPF");
		calcTax3600();
	}

protected void continue3400()
	{
		setPrecision(covrIO.getCoverageDebt(), 2);
		covrIO.setCoverageDebt(add(covrIO.getCoverageDebt(), add(wsaaTotalTax01, wsaaTotalTax02)));
		compute(wsaaCovrTot, 2).add(add(wsaaTotalTax01, wsaaTotalTax02));
		if (isNE(wsaaTotalTax01, 0)) {
			lifacmvrec.sacstyp.set(txcalcrec.taxType[1]);
			lifacmvrec.origamt.set(wsaaTotalTax01);
			b200WriteZrst();
		}
		if (isNE(wsaaTotalTax02, 0)) {
			lifacmvrec.sacstyp.set(txcalcrec.taxType[2]);
			lifacmvrec.origamt.set(wsaaTotalTax02);
			b200WriteZrst();
		}
	}

protected void rewriteCovrRecord3500()
	{
		/*START*/
		covrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			databaseError580();
		}
		/*EXIT*/
	}

protected void calcTax3600()
	{
		start3600();
		writeTaxdRecord3600();
	}

protected void start3600()
	{
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.taxAmt[1].set(0);
		txcalcrec.taxAmt[2].set(0);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			systemError570();
		}
	}

protected void writeTaxdRecord3600()
	{
		taxdIO.setTrantype(txcalcrec.transType);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(wsaaTranref);
		taxdIO.setTranref(wsaaTranref);
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(0);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setPostflg(" ");
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			databaseError580();
		}
		if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaTotalTax01.add(txcalcrec.taxAmt[1]);
		}
		if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaTotalTax02.add(txcalcrec.taxAmt[2]);
		}
	}

protected void updateLedgerAccounts4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4010();
				case postPrem4020: 
					postPrem4020();
					calcPrem4100();
				case postFee4030: 
					postFee4030();
				case perfeeamn4200: 
					perfeeamn4200();
				case postInitFee4050: 
					postInitFee4050();
				case inifeeamn4300: 
					inifeeamn4300();
				case postAdminCharges4400: 
					postAdminCharges4400();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4010()
	{
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account*/
		/* movements.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(ubblallpar.chdrChdrnum);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		lifacmvrec.batccoy.set(ubblallpar.chdrChdrcoy);
		lifacmvrec.rldgcoy.set(ubblallpar.chdrChdrcoy);
		lifacmvrec.genlcoy.set(ubblallpar.chdrChdrcoy);
		lifacmvrec.batcactyr.set(ubblallpar.batcactyr);
		lifacmvrec.batctrcde.set(ubblallpar.batctrcde);
		lifacmvrec.batcactmn.set(ubblallpar.batcactmn);
		lifacmvrec.batcbatch.set(ubblallpar.batch);
		lifacmvrec.batcbrn.set(ubblallpar.batcbrn);
		lifacmvrec.threadNumber.set(ZERO);
		if (isEQ(ubblallpar.threadNumber, NUMERIC)) {
			lifacmvrec.threadNumber.set(ubblallpar.threadNumber);
		}
		/* MOVE T5645-SACSCODE-01      TO LIFA-SACSCODE.                */
		/* MOVE T5645-SACSTYPE-01      TO LIFA-SACSTYP.*/
		lifacmvrec.origcurr.set(ubblallpar.cntcurr);
		/* Intead of sending 3 times values to CONT-DEBT, send the total*/
		/* amount (WSAA-COVR-TOT) just once.*/
		/*  MOVE CPRM-CALC-PREM         TO LIFA-ORIGAMT.*/
		lifacmvrec.origamt.set(wsaaCovrTot);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.tranno.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.tranno.set(ubblallpar.tranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.effdate.set(ubblallpar.effdate);
		/* MOVE UBBL-CHDR-CHDRNUM      TO LIFA-TRANREF.*/
		/* Set up TRANREF to contain the Full component key of the*/
		/*  Coverage/Rider that is Creating the debt so that if a*/
		/*  reversal is required at a later date, the correct component*/
		/*  can be identified.*/
		wsaaTranref.set(SPACES);
		wsaaTrefChdrcoy.set(ubblallpar.chdrChdrcoy);
		wsaaTrefChdrnum.set(ubblallpar.chdrChdrnum);
		wsaaPlan.set(ubblallpar.planSuffix);
		wsaaTrefLife.set(ubblallpar.lifeLife);
		wsaaTrefCoverage.set(ubblallpar.covrCoverage);
		wsaaTrefRider.set(ubblallpar.covrRider);
		wsaaTrefPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.tranref.set(wsaaTranref);
		/* MOVE T5645-SIGN-01          TO LIFA-GLSIGN.*/
		/* MOVE T5645-GLMAP-01         TO LIFA-GLCODE.*/
		lifacmvrec.user.set(ubblallpar.user);
		/* MOVE T5645-CNTTOT-01        TO LIFA-CONTOT.*/
		lifacmvrec.trandesc.set(getdescrec.longdesc);
		/* MOVE UBBL-CHDR-CHDRNUM      TO LIFA-RLDGACCT.*/
		/* MOVE UBBL-CHDR-CHDRNUM      TO WSAA-RLDG-CHDRNUM.*/
		/* MOVE UBBL-PLAN-SUFFIX       TO WSAA-PLAN.*/
		/* MOVE UBBL-LIFE-LIFE         TO WSAA-RLDG-LIFE.*/
		/* MOVE UBBL-COVR-COVERAGE     TO WSAA-RLDG-COVERAGE.*/
		/* MOVE UBBL-COVR-RIDER        TO WSAA-RLDG-RIDER.*/
		/* MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX.*/
		/* MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT.*/
		/*        Check for Component level accounting & act accordingly*/
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			lifacmvrec.sacscode.set(t5645rec.sacscode05);
			lifacmvrec.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec.glsign.set(t5645rec.sign05);
			lifacmvrec.glcode.set(t5645rec.glmap05);
			lifacmvrec.contot.set(t5645rec.cnttot05);
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(ubblallpar.chdrChdrnum);
			wsaaPlan.set(ubblallpar.planSuffix);
			wsaaRldgLife.set(ubblallpar.lifeLife);
			wsaaRldgCoverage.set(ubblallpar.covrCoverage);
			/*     MOVE UBBL-COVR-RIDER        TO WSAA-RLDG-RIDER*/
			/*        Post ACMV against the Coverage, not Rider since the*/
			/*         debt is also set against the Coverage*/
			wsaaRldgRider.set("00");
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			/*     MOVE UBBL-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6)*/
			/*        Set correct substitution code*/
			lifacmvrec.substituteCode[6].set(covrIO.getCrtable());
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode01);
			lifacmvrec.sacstyp.set(t5645rec.sacstype01);
			lifacmvrec.glsign.set(t5645rec.sign01);
			lifacmvrec.glcode.set(t5645rec.glmap01);
			lifacmvrec.contot.set(t5645rec.cnttot01);
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(ubblallpar.chdrChdrnum);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		/* MOVE UBBL-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6).*/
		lifacmvrec.substituteCode[1].set(ubblallpar.cnttype);
		lifacmvrec.transactionDate.set(ubblallpar.effdate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.function.set("PSTW");
		/* Write a Coverage/Debt ACMV in all cases. Billing details will*/
		/* not be updated during Contract Reversal if this ACMV is missing*/
		/* IF WSAA-COVR-TOT            = ZERO*/
		/*    GO                       TO 4100-CALC-PREM.*/
		if (isEQ(wsaaCovrTot,ZERO)
		&& isEQ(ubblallpar.function,"TOPUP")) {
			goTo(GotoLabel.postPrem4020);
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
	}

protected void postPrem4020()
	{
		if (isEQ(premiumrec.calcPrem,ZERO)
		|| isEQ(ubblallpar.function,"TOPUP")) {
			goTo(GotoLabel.postFee4030);
		}
	}

protected void calcPrem4100()
	{
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		lifacmvrec.origamt.set(premiumrec.calcPrem);
		/* MOVE UBBL-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6)*/
		/* MOVE T5645-SACSCODE-02      TO LIFA-SACSCODE.*/
		/* MOVE T5645-SACSTYPE-02      TO LIFA-SACSTYP.*/
		/* MOVE T5645-GLMAP-02         TO LIFA-GLCODE.*/
		/* MOVE T5645-SIGN-02          TO LIFA-GLSIGN.*/
		/* MOVE T5645-CNTTOT-02        TO LIFA-CONTOT.*/
		/*        Set correct posting type ( i.e. LE/LC/LP from T5645 )*/
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			wsaaRldgRider.set(ubblallpar.covrRider);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.sacscode.set(t5645rec.sacscode06);
			lifacmvrec.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec.glcode.set(t5645rec.glmap06);
			lifacmvrec.glsign.set(t5645rec.sign06);
			lifacmvrec.contot.set(t5645rec.cnttot06);
			lifacmvrec.substituteCode[6].set(ubblallpar.crtable);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode02);
			lifacmvrec.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec.glcode.set(t5645rec.glmap02);
			lifacmvrec.glsign.set(t5645rec.sign02);
			lifacmvrec.contot.set(t5645rec.cnttot02);
		}
		if (isEQ(premiumrec.calcPrem,ZERO)) {
			if ((isEQ(t5688rec.comlvlacc,"Y"))) {
				wsaaRldgRider.set("00");
				lifacmvrec.rldgacct.set(wsaaRldgacct);
			}
			goTo(GotoLabel.perfeeamn4200);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			wsaaRldgRider.set("00");
			lifacmvrec.rldgacct.set(wsaaRldgacct);
		}
		txcalcrec.transType.set("MCHG");
		b600PostTax();
		b200WriteZrst();
	}

protected void postFee4030()
	{
		if (isEQ(t5691rec.perfeeamn,ZERO)
		|| isEQ(ubblallpar.function,"TOPUP")) {
			goTo(GotoLabel.postInitFee4050);
		}
	}

protected void perfeeamn4200()
	{
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		/* MOVE T5691-PERFEEAMN        TO LIFA-ORIGAMT.*/
		lifacmvrec.origamt.set(wsaaPerFee);
		/*        Post ACMV against the Coverage, not Rider since the*/
		/*         Fee is also set against the Coverage*/
		/* MOVE COVR-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6).*/
		lifacmvrec.substituteCode[6].set(ubblallpar.crtable);
		/* MOVE T5645-SACSCODE-03      TO LIFA-SACSCODE.*/
		/* MOVE T5645-SACSTYPE-03      TO LIFA-SACSTYP.*/
		/* MOVE T5645-GLMAP-03         TO LIFA-GLCODE.*/
		/* MOVE T5645-SIGN-03          TO LIFA-GLSIGN.*/
		/* MOVE T5645-CNTTOT-03        TO LIFA-CONTOT.*/
		/*        Set correct posting type ( i.e. LE/LC/LP from T5645 )*/
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			wsaaRldgRider.set(ubblallpar.covrRider);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.sacscode.set(t5645rec.sacscode07);
			lifacmvrec.sacstyp.set(t5645rec.sacstype07);
			lifacmvrec.glcode.set(t5645rec.glmap07);
			lifacmvrec.glsign.set(t5645rec.sign07);
			lifacmvrec.contot.set(t5645rec.cnttot07);
			lifacmvrec.substituteCode[6].set(ubblallpar.crtable);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode03);
			lifacmvrec.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec.glcode.set(t5645rec.glmap03);
			lifacmvrec.glsign.set(t5645rec.sign03);
			lifacmvrec.contot.set(t5645rec.cnttot03);
		}
		if (isEQ(t5691rec.perfeeamn,ZERO)) {
			goTo(GotoLabel.inifeeamn4300);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
		txcalcrec.transType.set("PERF");
		b600PostTax();
		b200WriteZrst();
	}

protected void postInitFee4050()
	{
		/* IF T5691-PERFEEAMN           = ZERO*/
		if (isEQ(t5691rec.inifeeamn,ZERO)
		&& isEQ(t5691rec.zrtupfee,ZERO)) {
			goTo(GotoLabel.postAdminCharges4400);
			/*****    GO TO 4900-EXIT*/
		}
		if (isNE(ubblallpar.function,"ISSUE")
		&& isNE(ubblallpar.function,"TOPUP")) {
			goTo(GotoLabel.postAdminCharges4400);
			/*****    GO TO 4900-EXIT*/
		}
	}

protected void inifeeamn4300()
	{
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		/* MOVE T5691-INIFEEAMN        TO LIFA-ORIGAMT.*/
		/* MOVE WSAA-INI-FEE           TO LIFA-ORIGAMT.*/
		if (isEQ(ubblallpar.function,"TOPUP")) {
			lifacmvrec.origamt.set(wsaaTopupFee);
		}
		else {
			lifacmvrec.origamt.set(wsaaIniFee);
		}
		/*        Post ACMV against the Coverage, not Rider since the*/
		/*         Fee is also set against the Coverage*/
		/* MOVE COVR-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6).*/
		/* MOVE T5645-SACSCODE-04      TO LIFA-SACSCODE.*/
		/* MOVE T5645-SACSTYPE-04      TO LIFA-SACSTYP.*/
		/* MOVE T5645-GLMAP-04         TO LIFA-GLCODE.*/
		/* MOVE T5645-SIGN-04          TO LIFA-GLSIGN.*/
		/* MOVE T5645-CNTTOT-04        TO LIFA-CONTOT.*/
		/*        Set correct posting type ( i.e. LE/LC/LP from T5645 )*/
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			wsaaRldgRider.set(ubblallpar.covrRider);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.sacscode.set(t5645rec.sacscode08);
			lifacmvrec.sacstyp.set(t5645rec.sacstype08);
			lifacmvrec.glcode.set(t5645rec.glmap08);
			lifacmvrec.glsign.set(t5645rec.sign08);
			lifacmvrec.contot.set(t5645rec.cnttot08);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode04);
			lifacmvrec.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec.glcode.set(t5645rec.glmap04);
			lifacmvrec.glsign.set(t5645rec.sign04);
			lifacmvrec.contot.set(t5645rec.cnttot04);
		}
		/* IF T5691-INIFEEAMN          = ZERO*/
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			/*    GO                       TO 4900-EXIT.*/
			goTo(GotoLabel.postAdminCharges4400);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
		if (isEQ(ubblallpar.function, "TOPUP")) {
			txcalcrec.transType.set("TOPF");
		}
		else {
			txcalcrec.transType.set("ISSF");
		}
		b600PostTax();
		b200WriteZrst();
	}

protected void postAdminCharges4400()
	{
		if (isEQ(t5691rec.zradmnpc,ZERO)
		&& isEQ(t5691rec.zrtupadpc,ZERO)) {
			return ;
		}
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		if (isEQ(ubblallpar.function,"TOPUP")) {
			lifacmvrec.origamt.set(wsaaTopupAdmChgs);
		}
		else {
			lifacmvrec.origamt.set(wsaaAdmChgs);
		}
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			return ;
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaRldgRider.set(ubblallpar.covrRider);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.sacscode.set(t5645rec.sacscode10);
			lifacmvrec.sacstyp.set(t5645rec.sacstype10);
			lifacmvrec.glcode.set(t5645rec.glmap10);
			lifacmvrec.glsign.set(t5645rec.sign10);
			lifacmvrec.contot.set(t5645rec.cnttot10);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode09);
			lifacmvrec.sacstyp.set(t5645rec.sacstype09);
			lifacmvrec.glcode.set(t5645rec.glmap09);
			lifacmvrec.glsign.set(t5645rec.sign09);
			lifacmvrec.contot.set(t5645rec.cnttot09);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
		if (isEQ(ubblallpar.function, "TOPUP")) {
			txcalcrec.transType.set("TOPA");
		}
		else {
			txcalcrec.transType.set("ADMF");
		}
		b600PostTax();
		b200WriteZrst();
	}

protected void b200WriteZrst()
	{
		b210WriteZrst();
	}

protected void b210WriteZrst()
	{
		zrstIO.setParams(SPACES);
		zrstIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		zrstIO.setChdrnum(ubblallpar.chdrChdrnum);
		zrstIO.setLife(ubblallpar.lifeLife);
		/*    MOVE UBBL-LIFE-JLIFE        TO ZRST-JLIFE.*/
		zrstIO.setJlife(SPACES);
		zrstIO.setCoverage(ubblallpar.covrCoverage);
		zrstIO.setRider(ubblallpar.covrRider);
		zrstIO.setBatctrcde(ubblallpar.batctrcde);
		zrstIO.setTranno(ubblallpar.tranno);
		zrstIO.setTrandate(ubblallpar.effdate);
		zrstIO.setZramount01(lifacmvrec.origamt);
		wsaaTotalCd.add(lifacmvrec.origamt);
		wsaaSeqno.add(1);
		zrstIO.setZramount02(ZERO);
		zrstIO.setXtranno(ZERO);
		zrstIO.setSeqno(wsaaSeqno);
		zrstIO.setSacstyp(lifacmvrec.sacstyp);
		zrstIO.setUstmno(ZERO);
		zrstIO.setFormat(formatsInner.zrstrec);
		zrstIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(zrstIO.getStatuz());
			syserrrec.params.set(zrstIO.getParams());
			databaseError580();
		}
	}

protected void b300CheckZrstExists()
	{
			b310Para();
		}

protected void b310Para()
	{
		/* CALL 'ZRSTIO'               USING ZRST-PARAMS.               */
		/* IF  ZRST-STATUZ         NOT = O-K                            */
		/* AND ZRST-STATUZ         NOT = ENDP                           */
		/*     MOVE ZRST-STATUZ        TO SYSR-STATUZ                   */
		/*     MOVE ZRST-PARAMS        TO SYSR-PARAMS                   */
		/*     PERFORM 580-DATABASE-ERROR                               */
		/* END-IF.                                                      */
		/* IF ZRST-CHDRCOY         NOT = UBBL-CHDR-CHDRCOY              */
		/* OR ZRST-CHDRNUM         NOT = UBBL-CHDR-CHDRNUM              */
		/* OR ZRST-LIFE            NOT = UBBL-LIFE-LIFE                 */
		/*    OR ZRST-JLIFE           NOT = UBBL-LIFE-JLIFE*/
		/* OR ZRST-STATUZ              = ENDP                           */
		/*    MOVE ENDP                TO ZRST-STATUZ                   */
		/*    GO TO B390-EXIT                                           */
		/* END-IF.                                                      */
		/* Feedback-ind = spaces ie. Unitdeal is not done.*/
		/*    AND ZRST-SEQNO              = 1*/
		/*    AND ZRST-RIDER              = '00'*/
		/* AND ZRST-COVERAGE           = UBBL-COVR-COVERAGE             */
		/*        ADD ZRST-ZRAMOUNT02     TO WSAA-TOTAL-CD*/
		/*     ADD ZRST-ZRAMOUNT01     TO WSAA-TOTAL-CD                 */
		/* END-IF.                                                      */
		/* MOVE NEXTR                  TO ZRST-FUNCTION.                */
		SmartFileCode.execute(appVars, zrstnudIO);
		if (isNE(zrstnudIO.getStatuz(),varcom.oK)
		&& isNE(zrstnudIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(zrstnudIO.getParams());
			syserrrec.statuz.set(zrstnudIO.getStatuz());
			databaseError580();
		}
		if (isEQ(zrstnudIO.getStatuz(),varcom.endp)
		|| isNE(zrstnudIO.getChdrcoy(),ubblallpar.chdrChdrcoy)
		|| isNE(zrstnudIO.getChdrnum(),ubblallpar.chdrChdrnum)
		|| isNE(zrstnudIO.getLife(),ubblallpar.lifeLife)) {
			zrstnudIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(zrstnudIO.getCoverage(),ubblallpar.covrCoverage)) {
			wsaaTotalCd.add(zrstnudIO.getZramount01());
		}
		zrstnudIO.setFunction(varcom.nextr);
	}

protected void b400UpdateTotalCd()
	{
			b410UpdateTotalCd();
		}

protected void b410UpdateTotalCd()
	{
		/* CALL 'ZRSTIO'               USING ZRST-PARAMS.               */
		/* IF  ZRST-STATUZ         NOT = O-K                            */
		/* AND ZRST-STATUZ         NOT = ENDP                           */
		/*     MOVE ZRST-STATUZ        TO SYSR-STATUZ                   */
		/*     MOVE ZRST-PARAMS        TO SYSR-PARAMS                   */
		/*     PERFORM 580-DATABASE-ERROR                               */
		/* END-IF.                                                      */
		/* IF ZRST-CHDRCOY         NOT = UBBL-CHDR-CHDRCOY              */
		/* OR ZRST-CHDRNUM         NOT = UBBL-CHDR-CHDRNUM              */
		/* OR ZRST-LIFE            NOT = UBBL-LIFE-LIFE                 */
		/*    OR ZRST-JLIFE           NOT = UBBL-LIFE-JLIFE*/
		/* OR ZRST-STATUZ              = ENDP                           */
		/*    MOVE ENDP                TO ZRST-STATUZ                   */
		/*    GO TO B490-EXIT                                           */
		/* END-IF.                                                      */
		/* AND ZRST-COVERAGE           = UBBL-COVR-COVERAGE             */
		/*     MOVE WSAA-TOTAL-CD      TO ZRST-ZRAMOUNT02               */
		/*MOVE REWRT              TO ZRST-FUNCTION                 */
		/*     MOVE WRITD              TO ZRST-FUNCTION         <V65L16>*/
		/*     MOVE ZRSTREC            TO ZRST-FORMAT                   */
		/*     CALL 'ZRSTIO'           USING ZRST-PARAMS                */
		/*     IF ZRST-STATUZ      NOT = O-K                            */
		/*        MOVE ZRST-STATUZ     TO SYSR-STATUZ                   */
		/*        MOVE ZRST-PARAMS     TO SYSR-PARAMS                   */
		/*        PERFORM 580-DATABASE-ERROR                            */
		/*     END-IF                                                   */
		/* END-IF.                                                      */
		/* MOVE NEXTR                  TO ZRST-FUNCTION.                */
		SmartFileCode.execute(appVars, zrstnudIO);
		if (isNE(zrstnudIO.getStatuz(),varcom.oK)
		&& isNE(zrstnudIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(zrstnudIO.getParams());
			syserrrec.statuz.set(zrstnudIO.getStatuz());
			databaseError580();
		}
		if (isEQ(zrstnudIO.getStatuz(),varcom.endp)
		|| isNE(zrstnudIO.getChdrcoy(),ubblallpar.chdrChdrcoy)
		|| isNE(zrstnudIO.getChdrnum(),ubblallpar.chdrChdrnum)
		|| isNE(zrstnudIO.getLife(),ubblallpar.lifeLife)) {
			zrstnudIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(zrstnudIO.getCoverage(),ubblallpar.covrCoverage)) {
			zrstnudIO.setZramount02(wsaaTotalCd);
			zrstnudIO.setFunction(varcom.writd);
			zrstnudIO.setFormat(formatsInner.zrstnudrec);
			SmartFileCode.execute(appVars, zrstnudIO);
			if (isNE(zrstnudIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(zrstnudIO.getParams());
				syserrrec.statuz.set(zrstnudIO.getStatuz());
				databaseError580();
			}
		}
		zrstnudIO.setFunction(varcom.nextr);
	}

protected void b600PostTax()
	{
		b600Start();
	}

protected void b600Start()
	{
		/*  Set the value of TAXD record.                                  */
		if (isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		taxdIO.setChdrnum(ubblallpar.chdrChdrnum);
		taxdIO.setLife(ubblallpar.lifeLife);
		taxdIO.setCoverage(ubblallpar.covrCoverage);
		taxdIO.setRider(ubblallpar.covrRider);
		taxdIO.setPlansfx(ubblallpar.planSuffix);
		taxdIO.setInstfrom(ubblallpar.effdate);
		taxdIO.setTrantype(txcalcrec.transType);
		taxdIO.setStatuz(varcom.oK);
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)
		&& isNE(taxdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			databaseError580();
		}
		if (isEQ(taxdIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		/* Call tax subroutine                                             */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("POST");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(taxdIO.getChdrcoy());
		txcalcrec.chdrnum.set(taxdIO.getChdrnum());
		txcalcrec.life.set(taxdIO.getLife());
		txcalcrec.coverage.set(taxdIO.getCoverage());
		txcalcrec.rider.set(taxdIO.getRider());
		txcalcrec.planSuffix.set(taxdIO.getPlansfx());
		txcalcrec.crtable.set(ubblallpar.crtable);
		txcalcrec.cnttype.set(ubblallpar.cnttype);
		txcalcrec.register.set(ubblallpar.chdrRegister);
		txcalcrec.taxrule.set(subString(taxdIO.getTranref(), 1, 8));
		txcalcrec.rateItem.set(subString(taxdIO.getTranref(), 9, 8));
		txcalcrec.amountIn.set(taxdIO.getBaseamt());
		txcalcrec.transType.set(taxdIO.getTrantype());
		txcalcrec.effdate.set(taxdIO.getEffdate());
		txcalcrec.tranno.set(taxdIO.getTranno());
		txcalcrec.jrnseq.set(wsaaSequenceNo);
		txcalcrec.batckey.set(wsaaBatckey);
		txcalcrec.ccy.set(ubblallpar.cntcurr);
		txcalcrec.taxType[1].set(taxdIO.getTxtype01());
		txcalcrec.taxType[2].set(taxdIO.getTxtype02());
		txcalcrec.taxAmt[1].set(taxdIO.getTaxamt01());
		txcalcrec.taxAmt[2].set(taxdIO.getTaxamt02());
		txcalcrec.taxAbsorb[1].set(taxdIO.getTxabsind01());
		txcalcrec.taxAbsorb[2].set(taxdIO.getTxabsind02());
		txcalcrec.ccy.set(ubblallpar.cntcurr);
		txcalcrec.language.set(ubblallpar.language);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			databaseError580();
		}
		wsaaSequenceNo.set(txcalcrec.jrnseq);
		/* Update TAXD record                                              */
		taxdIO.setPostflg("P");
		taxdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			databaseError580();
		}
	}

protected void callSurrSubroutine5000()
	{
			start5000();
		}

protected void start5000()
	{
		/* Read the Coverage file to get Coverage/Rider details for*/
		/*  call to Surrender value calc. subroutine.*/
		/*  If no valid COVRSUR record found, then exit program.*/
		wsaaAccumSurrenderValue.set(ZERO);
		covrsurIO.setParams(SPACES);
		covrsurIO.setFunction(varcom.readr);
		covrsurIO.setFormat(formatsInner.covrsurrec);
		covrsurIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		covrsurIO.setChdrnum(ubblallpar.chdrChdrnum);
		covrsurIO.setLife(ubblallpar.lifeLife);
		covrsurIO.setCoverage(ubblallpar.covrCoverage);
		covrsurIO.setRider(ubblallpar.covrRider);
		covrsurIO.setPlanSuffix(ubblallpar.planSuffix);
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrsurIO.getParams());
			syserrrec.statuz.set(covrsurIO.getStatuz());
			databaseError580();
		}
		if (isEQ(ubblallpar.function,"ISSUE")) {
			wsaaAccumSurrenderValue.set(covrsurIO.getSingp());
			return ;
		}
		/* We have at found the COVR record.*/
		/* Set up linkage area to Call Surrender value calculation*/
		/*  subroutines.*/
		srcalcpy.surrenderRec.set(SPACES);
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrsurIO.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrsurIO.getChdrnum());
		srcalcpy.planSuffix.set(covrsurIO.getPlanSuffix());
		srcalcpy.polsum.set(ubblallpar.polsum);
		srcalcpy.lifeLife.set(covrsurIO.getLife());
		srcalcpy.lifeJlife.set(covrsurIO.getJlife());
		srcalcpy.covrCoverage.set(covrsurIO.getCoverage());
		srcalcpy.covrRider.set(covrsurIO.getRider());
		srcalcpy.crtable.set(covrsurIO.getCrtable());
		srcalcpy.crrcd.set(covrsurIO.getCrrcd());
		srcalcpy.ptdate.set(ubblallpar.ptdate);
		srcalcpy.effdate.set(ubblallpar.effdate);
		srcalcpy.convUnits.set(covrsurIO.getConvertInitialUnits());
		srcalcpy.language.set(ubblallpar.language);
		srcalcpy.currcode.set(covrsurIO.getPremCurrency());
		srcalcpy.chdrCurr.set(ubblallpar.cntcurr);
		srcalcpy.pstatcode.set(covrsurIO.getPstatcode());
		if (isGT(covrsurIO.getInstprem(),ZERO)) {
			srcalcpy.singp.set(covrsurIO.getInstprem());
		}
		else {
			srcalcpy.singp.set(covrsurIO.getSingp());
		}
		srcalcpy.billfreq.set(ubblallpar.billfreq);
		srcalcpy.status.set(SPACES);
		srcalcpy.type.set("F");
		/* Now call the Surrender value calculation subroutine.*/
		while ( !(isEQ(srcalcpy.status,varcom.endp))) {
			getSurrValue5100();
		}
		
	}

protected void getSurrValue5100()
	{
		start5100();
	}

protected void start5100()
	{
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		/*IVE-796 RUL Product - Partial Surrender Calculation started*/
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		/*ILIFE-7548 start*/
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString()) && er.isExternalized(ubblallpar.cnttype.toString(), srcalcpy.crtable.toString()) )) 
		{
			callProgramX(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		/*ILIFE-7548 end*/
		else
		{
	 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);	
			chdrlnbIO.setChdrcoy(srcalcpy.chdrChdrcoy);
			chdrlnbIO.setChdrnum(srcalcpy.chdrChdrnum);
			chdrlnbIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrlnbIO);

			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrlnbIO);//VPMS call
			
			
			if(isEQ(srcalcpy.type,"L"))
			{
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
		}

		/*IVE-796 RUL Product - Partial Surrender Calculation end*/
		if (isNE(srcalcpy.status,varcom.oK)
		&& isNE(srcalcpy.status,varcom.endp)) {
			syserrrec.params.set(srcalcpy.surrenderRec);
			syserrrec.statuz.set(srcalcpy.status);
			systemError570();
		}
		/* Add values returned to our running Surrender value total*/
		/* The Surrender value of the units is returned in the estimated*/
		/*  variable.....*/
		/* Convert the Estimated value to the fund currency.*/
		zrdecplrec.amountIn.set(srcalcpy.estimatedVal);
		a000CallRounding();
		srcalcpy.estimatedVal.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(srcalcpy.actualVal);
		a000CallRounding();
		srcalcpy.actualVal.set(zrdecplrec.amountOut);
		/* ADD SURC-ESTIMATED-VAL      TO WSAA-ACCUM-SURRENDER-VALUE.*/
		if (isNE(srcalcpy.estimatedVal,ZERO)) {
			/* AND SURC-CURRCODE        NOT = UBBL-CNTCURR*/
			if (isNE(srcalcpy.currcode,ubblallpar.cntcurr)) {
				/*    MOVE SPACES              TO CLNK-CLNK002-REC*/
				/*    MOVE SURC-ESTIMATED-VAL  TO CLNK-AMOUNT-IN*/
				/*    PERFORM 5200-CONVERT-VALUE*/
				/*    ADD CLNK-AMOUNT-OUT TO WSAA-ACCUM-ESTIMATED-VALUE*/
				/*                         WSAA-ACCUM-SURRENDER-VALUE*/
				conlinkrec.clnk002Rec.set(SPACES);
				conlinkrec.amountIn.set(srcalcpy.estimatedVal);
				convertValue5200();
				wsaaAccumSurrenderValue.add(conlinkrec.amountOut);
			}
			else {
				wsaaAccumSurrenderValue.add(srcalcpy.estimatedVal);
			}
		}
		/*  The ACTUAL-VALue returned from the subroutine called is the*/
		/*   PENALTY value - if you want the PENALTY included in the*/
		/*   Surrender value figures,remove the comment on the line below.*/
		/* ADD SURC-ACTUAL-VAL         TO WSAA-ACCUM-SURRENDER-VALUE.*/
		/* As of change number <009>, we do want to include the Penalty*/
		/*  value.*/
		/* Convert the Actual value to the fund currency.*/
		/* ADD SURC-ACTUAL-VAL         TO WSAA-ACCUM-SURRENDER-VALUE.*/
		wsaaAccumSurrenderValue.add(srcalcpy.actualVal);
	}

	/**
	* <pre>
	*     CONVERT THE VALUE TO THE FUND CURRENCY.
	* </pre>
	*/
protected void convertValue5200()
	{
		call5210();
	}

protected void call5210()
	{
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.cashdate.set(ubblallpar.effdate);
		conlinkrec.currIn.set(srcalcpy.currcode);
		conlinkrec.currOut.set(ubblallpar.cntcurr);
		conlinkrec.company.set(ubblallpar.chdrChdrcoy);
		conlinkrec.function.set("REAL");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			systemError570();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void calcCommission6000()
	{
		/*CALC*/
		if (isEQ(ubblallpar.function,"ISSUE")) {
			createAgcm7000();
		}
		else {
			updateAgcmbch8000();
		}
		/*EXIT*/
	}

protected void createAgcm7000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begn7010();
				case callPcddlnbio7020: 
					callPcddlnbio7020();
					readAglf7030();
					initializeLinkage7040();
					calcBasicComm7050();
				case callCommPay7060: 
					callCommPay7060();
				case setAgcmValue7070: 
					setAgcmValue7070();
				case calcServComm7080: 
					calcServComm7080();
				case calcRenlComm7090: 
					calcRenlComm7090();
				case writeAgcm7100: 
					writeAgcm7100();
				case nextPcddlnb7180: 
					nextPcddlnb7180();
				case exit7190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begn7010()
	{
		pcddlnbIO.setDataKey(SPACES);
		pcddlnbIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		pcddlnbIO.setChdrnum(ubblallpar.chdrChdrnum);
		pcddlnbIO.setFormat(formatsInner.pcddlnbrec);
		pcddlnbIO.setFunction(varcom.begn);
	}

protected void callPcddlnbio7020()
	{//performance improvement --  atiwari23 
	pcddlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	pcddlnbIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, pcddlnbIO);
		if (isNE(pcddlnbIO.getStatuz(),varcom.oK)
		&& isNE(pcddlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(pcddlnbIO.getParams());
			syserrrec.statuz.set(pcddlnbIO.getStatuz());
			systemError570();
		}
		if (isEQ(pcddlnbIO.getStatuz(),varcom.endp)
		|| isNE(pcddlnbIO.getChdrcoy(),ubblallpar.chdrChdrcoy)
		|| isNE(pcddlnbIO.getChdrnum(),ubblallpar.chdrChdrnum)) {
			pcddlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit7190);
		}
	}

protected void readAglf7030()
	{
		aglflnbIO.setDataKey(SPACES);
		aglflnbIO.setAgntcoy(pcddlnbIO.getChdrcoy());
		aglflnbIO.setAgntnum(pcddlnbIO.getAgntnum());
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(),varcom.oK)
		&& isNE(aglflnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(aglflnbIO.getParams());
			syserrrec.statuz.set(aglflnbIO.getStatuz());
			systemError570();
		}
		if (isEQ(aglflnbIO.getStatuz(),varcom.mrnf)) {
			aglflnbIO.setDataArea(SPACES);
			aglflnbIO.setOvcpc(ZERO);
		}
	}

protected void initializeLinkage7040()
	{
		comlinkrec.chdrcoy.set(ubblallpar.chdrChdrcoy);
		comlinkrec.chdrnum.set(ubblallpar.chdrChdrnum);
		comlinkrec.life.set(ubblallpar.lifeLife);
		comlinkrec.coverage.set(ubblallpar.covrCoverage);
		comlinkrec.effdate.set(ubblallpar.effdate);
		comlinkrec.currto.set(covrIO.getCurrto());
		comlinkrec.rider.set(ubblallpar.covrRider);
		comlinkrec.planSuffix.set(ubblallpar.planSuffix);
		comlinkrec.agent.set(aglflnbIO.getAgntnum());
		comlinkrec.jlife.set(ubblallpar.lifeJlife);
		comlinkrec.crtable.set(ubblallpar.crtable);
		comlinkrec.language.set(ubblallpar.language);
		comlinkrec.agentClass.set(aglflnbIO.getAgentClass());
		comlinkrec.icommtot.set(ZERO);
		comlinkrec.icommpd.set(ZERO);
		comlinkrec.icommernd.set(ZERO);
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		comlinkrec.instprem.set(ZERO);
		comlinkrec.annprem.set(ZERO);
		comlinkrec.targetPrem.set(ZERO);
		comlinkrec.commprem.set(ZERO);
		agcmIO.setAnnprem(ZERO);
		agcmIO.setInitcom(ZERO);
		agcmIO.setCompay(ZERO);
		agcmIO.setComern(ZERO);
		agcmIO.setScmdue(ZERO);
		agcmIO.setScmearn(ZERO);
		agcmIO.setRnlcdue(ZERO);
		agcmIO.setRnlcearn(ZERO);
		wsaaCommDue.set(ZERO);
		wsaaCommEarn.set(ZERO);
		wsaaCommPay.set(ZERO);
		wsaaServDue.set(ZERO);
		wsaaServEarn.set(ZERO);
		wsaaRenlDue.set(ZERO);
		wsaaRenlEarn.set(ZERO);
		wsaaBillfreqC.set(ubblallpar.billfreq);
		comlinkrec.billfreq.set(ubblallpar.billfreq);
		comlinkrec.seqno.set(1);
		compute(comlinkrec.annprem, 2).set(mult(mult(premiumrec.calcPrem,wsaaBillfreqN),(div(pcddlnbIO.getSplitBcomm(),100))));
		if (isEQ(th605rec.bonusInd,"Y")) {
			compute(wsaaZctnAnnprem, 2).set(mult(premiumrec.calcPrem,wsaaBillfreqN));
		}
		compute(comlinkrec.instprem, 2).set(mult(premiumrec.calcPrem,(div(pcddlnbIO.getSplitBcomm(),100))));
		if (isEQ(th605rec.bonusInd,"Y")) {
			wsaaZctnInstprem.set(premiumrec.calcPrem);
		}
				/* IBPLIFE-5237 starts */
		if (isGT(premiumrec.commissionPrem,ZERO)){
			comlinkrec.commprem.set(premiumrec.commissionPrem);
		}
		/* IBPLIFE-5237 ends */
		
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.frequency.set(t5534rec.unitFreq);
		datcon2rec.freqFactor.set(1);
		datcon2rec.intDate1.set(ubblallpar.effdate);
		datcon2rec.intDate2.set(ZERO);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			systemError570();
		}
		comlinkrec.ptdate.set(datcon2rec.intDate2);
	}

protected void calcBasicComm7050()
	{
		agcmIO.setAnnprem(comlinkrec.annprem);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tablesInner.t5647);
		itemIO.setItemitem(t5687rec.basicCommMeth);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.callCommPay7060);
		}
		t5647rec.t5647Rec.set(itemIO.getGenarea());
		if (isEQ(t5647rec.commsubr,SPACES)) {
			goTo(GotoLabel.callCommPay7060);
		}
		callProgram(t5647rec.commsubr, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			syserrrec.statuz.set(comlinkrec.statuz);
			systemError570();
		}
		zrdecplrec.amountIn.set(comlinkrec.icommtot);
		a000CallRounding();
		comlinkrec.icommtot.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		a000CallRounding();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		a000CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
	}

protected void callCommPay7060()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tablesInner.t5644);
		if (isNE(t5687rec.bascpy,SPACES)) {
			itemIO.setItemitem(t5687rec.bascpy);
		}
		else {
			itemIO.setItemitem(aglflnbIO.getBcmtab());
		}
		if (isEQ(itemIO.getItemitem(),SPACES)) {
			goTo(GotoLabel.setAgcmValue7070);
		}
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.setAgcmValue7070);
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		if (isEQ(t5644rec.compysubr,SPACES)) {
			goTo(GotoLabel.setAgcmValue7070);
		}
		comlinkrec.method.set(itemIO.getItemitem());
		callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			syserrrec.statuz.set(comlinkrec.statuz);
			systemError570();
		}
		zrdecplrec.amountIn.set(comlinkrec.icommtot);
		a000CallRounding();
		comlinkrec.icommtot.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		a000CallRounding();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		a000CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
	}

protected void setAgcmValue7070()
	{
		wsaaCommDue.set(comlinkrec.payamnt);
		wsaaCommEarn.set(comlinkrec.erndamt);
		compute(wsaaCommPay, 2).set(sub(wsaaCommDue,wsaaCommEarn));
		agcmIO.setInitcom(comlinkrec.icommtot);
		agcmIO.setCompay(comlinkrec.payamnt);
		agcmIO.setComern(comlinkrec.erndamt);
		agcmIO.setBasicCommMeth(t5687rec.basicCommMeth);
		agcmIO.setBascpy(t5687rec.bascpy);
		if (isEQ(t5687rec.bascpy,SPACES)) {
			agcmIO.setBascpy(aglflnbIO.getBcmtab());
		}
		if (isNE(th605rec.bonusInd,"Y")) {
			goTo(GotoLabel.calcServComm7080);
		}
		if (isNE(comlinkrec.payamnt,ZERO)) {
			zctnIO.setParams(SPACES);
			zctnIO.setAgntcoy(pcddlnbIO.getChdrcoy());
			zctnIO.setAgntnum(pcddlnbIO.getAgntnum());
			zctnIO.setCommAmt(comlinkrec.payamnt);
			if (isGT(comlinkrec.instprem,comlinkrec.annprem)) {
				zctnIO.setPremium(wsaaZctnAnnprem);
			}
			else {
				zctnIO.setPremium(wsaaZctnInstprem);
			}
			zctnIO.setSplitBcomm(pcddlnbIO.getSplitBcomm());
			zctnIO.setZprflg("I");
			zctnIO.setChdrcoy(comlinkrec.chdrcoy);
			zctnIO.setChdrnum(comlinkrec.chdrnum);
			zctnIO.setLife(comlinkrec.life);
			zctnIO.setCoverage(comlinkrec.coverage);
			zctnIO.setRider(comlinkrec.rider);
			zctnIO.setTranno(ubblallpar.tranno);
			zctnIO.setTransCode(ubblallpar.batctrcde);
			zctnIO.setEffdate(ubblallpar.effdate);
			zctnIO.setTrandate(wsaaToday);
			zctnIO.setFormat(formatsInner.zctnrec);
			zctnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zctnIO);
			if (isNE(zctnIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(zctnIO.getStatuz());
				syserrrec.params.set(zctnIO.getParams());
				systemError570();
			}
		}
	}

protected void calcServComm7080()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tablesInner.t5644);
		if (isNE(t5687rec.srvcpy,SPACES)) {
			itemIO.setItemitem(t5687rec.srvcpy);
		}
		else {
			itemIO.setItemitem(aglflnbIO.getScmtab());
		}
		if (isEQ(itemIO.getItemitem(),SPACES)) {
			goTo(GotoLabel.calcRenlComm7090);
		}
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.calcRenlComm7090);
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		comlinkrec.icommtot.set(ZERO);
		comlinkrec.icommpd.set(ZERO);
		comlinkrec.icommernd.set(ZERO);
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		if (isEQ(t5644rec.compysubr,SPACES)) {
			goTo(GotoLabel.calcRenlComm7090);
		}
		comlinkrec.method.set(itemIO.getItemitem());
		callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			syserrrec.statuz.set(comlinkrec.statuz);
			systemError570();
		}
		zrdecplrec.amountIn.set(comlinkrec.icommtot);
		a000CallRounding();
		comlinkrec.icommtot.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		a000CallRounding();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		a000CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		wsaaServDue.set(comlinkrec.payamnt);
		wsaaServEarn.set(comlinkrec.erndamt);
		agcmIO.setScmdue(comlinkrec.payamnt);
		agcmIO.setScmearn(comlinkrec.erndamt);
		agcmIO.setSrvcpy(t5687rec.srvcpy);
		if (isEQ(t5687rec.srvcpy,SPACES)) {
			agcmIO.setSrvcpy(aglflnbIO.getScmtab());
		}
	}

protected void calcRenlComm7090()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tablesInner.t5644);
		if (isNE(t5687rec.rnwcpy,SPACES)) {
			itemIO.setItemitem(t5687rec.rnwcpy);
		}
		else {
			itemIO.setItemitem(aglflnbIO.getRcmtab());
		}
		if (isEQ(itemIO.getItemitem(),SPACES)) {
			goTo(GotoLabel.writeAgcm7100);
		}
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.writeAgcm7100);
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		if (isEQ(t5644rec.compysubr,SPACES)) {
			goTo(GotoLabel.writeAgcm7100);
		}
		comlinkrec.icommtot.set(ZERO);
		comlinkrec.icommpd.set(ZERO);
		comlinkrec.icommernd.set(ZERO);
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		comlinkrec.method.set(itemIO.getItemitem());
		callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			syserrrec.statuz.set(comlinkrec.statuz);
			systemError570();
		}
		zrdecplrec.amountIn.set(comlinkrec.icommtot);
		a000CallRounding();
		comlinkrec.icommtot.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		a000CallRounding();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		a000CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		wsaaRenlDue.set(comlinkrec.payamnt);
		agcmIO.setRnlcdue(comlinkrec.payamnt);
		wsaaRenlEarn.set(comlinkrec.erndamt);
		agcmIO.setRnlcearn(comlinkrec.erndamt);
		agcmIO.setRnwcpy(t5687rec.rnwcpy);
		if (isEQ(t5687rec.rnwcpy,SPACES)) {
			agcmIO.setRnwcpy(aglflnbIO.getRcmtab());
		}
		if (isNE(th605rec.bonusInd,"Y")) {
			goTo(GotoLabel.writeAgcm7100);
		}
		if (isNE(comlinkrec.payamnt,ZERO)) {
			zctnIO.setParams(SPACES);
			zctnIO.setAgntcoy(pcddlnbIO.getChdrcoy());
			zctnIO.setAgntnum(pcddlnbIO.getAgntnum());
			zctnIO.setCommAmt(comlinkrec.payamnt);
			setPrecision(zctnIO.getPremium(), 3);
			zctnIO.setPremium(sub(wsaaZctnInstprem,wsaaZctnAnnprem), true);
			zctnIO.setSplitBcomm(pcddlnbIO.getSplitBcomm());
			zctnIO.setZprflg("R");
			zctnIO.setChdrcoy(comlinkrec.chdrcoy);
			zctnIO.setChdrnum(comlinkrec.chdrnum);
			zctnIO.setLife(comlinkrec.life);
			zctnIO.setCoverage(comlinkrec.coverage);
			zctnIO.setRider(comlinkrec.rider);
			zctnIO.setTranno(ubblallpar.tranno);
			zctnIO.setTransCode(ubblallpar.batctrcde);
			zctnIO.setEffdate(ubblallpar.effdate);
			zctnIO.setTrandate(wsaaToday);
			zctnIO.setFormat(formatsInner.zctnrec);
			zctnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zctnIO);
			if (isNE(zctnIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(zctnIO.getStatuz());
				syserrrec.params.set(zctnIO.getParams());
				systemError570();
			}
		}
	}

protected void writeAgcm7100()
	{
		if (isEQ(agcmIO.getAnnprem(),ZERO)
		&& isEQ(agcmIO.getInitcom(),ZERO)
		&& isEQ(agcmIO.getCompay(),ZERO)
		&& isEQ(agcmIO.getComern(),ZERO)
		&& isEQ(agcmIO.getScmdue(),ZERO)
		&& isEQ(agcmIO.getScmearn(),ZERO)
		&& isEQ(agcmIO.getRnlcdue(),ZERO)
		&& isEQ(agcmIO.getRnlcearn(),ZERO)) {
			goTo(GotoLabel.nextPcddlnb7180);
		}
		agcmIO.setTransactionDate(ubblallpar.effdate);
		agcmIO.setTransactionTime(varcom.vrcmTime);
		agcmIO.setUser(ubblallpar.user);
		agcmIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		agcmIO.setChdrnum(ubblallpar.chdrChdrnum);
		agcmIO.setAgntnum(aglflnbIO.getAgntnum());
		agcmIO.setAgentClass(aglflnbIO.getAgentClass());
		agcmIO.setLife(ubblallpar.lifeLife);
		agcmIO.setCoverage(ubblallpar.covrCoverage);
		agcmIO.setRider(ubblallpar.covrRider);
		agcmIO.setPlanSuffix(ubblallpar.planSuffix);
		agcmIO.setTranno(ubblallpar.tranno);
		agcmIO.setEfdate(ubblallpar.effdate);
		agcmIO.setCurrfrom(covrIO.getCurrfrom());
		agcmIO.setCurrto(covrIO.getCurrto());
		agcmIO.setOvrdcat("B");
		agcmIO.setCedagent(SPACES);
		agcmIO.setValidflag("1");
		agcmIO.setPtdate(ubblallpar.ptdate);
		agcmIO.setSeqno(1);
		agcmIO.setTermid("1");
		agcmIO.setFormat(formatsInner.agcmrec);
		agcmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, agcmIO);
		if (isNE(agcmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmIO.getParams());
			syserrrec.statuz.set(agcmIO.getStatuz());
			systemError570();
		}
	}

protected void nextPcddlnb7180()
	{
		postingCommission7500();
		pcddlnbIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callPcddlnbio7020);
	}

protected void postingCommission7500()
	{
		setLifacmv7510();
	}

protected void setLifacmv7510()
	{
		lifacmvrec.batccoy.set(ubblallpar.chdrChdrcoy);
		lifacmvrec.rldgcoy.set(ubblallpar.chdrChdrcoy);
		lifacmvrec.genlcoy.set(ubblallpar.chdrChdrcoy);
		lifacmvrec.rdocnum.set(ubblallpar.chdrChdrnum);
		lifacmvrec.batcactyr.set(ubblallpar.batcactyr);
		lifacmvrec.batcactmn.set(ubblallpar.batcactmn);
		lifacmvrec.batcbatch.set(ubblallpar.batch);
		lifacmvrec.batcbrn.set(ubblallpar.batcbrn);
		lifacmvrec.origcurr.set(ubblallpar.cntcurr);
		lifacmvrec.tranno.set(ubblallpar.tranno);
		lifacmvrec.trandesc.set(getdescrec.longdesc);
		lifacmvrec.effdate.set(ubblallpar.effdate);
		lifacmvrec.user.set(ubblallpar.user);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(ubblallpar.cnttype);
		lifacmvrec.substituteCode[6].set(ubblallpar.crtable);
		lifacmvrec.transactionDate.set(ubblallpar.effdate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		postInitialCommission7600();
		postServiceCommission7700();
		postRenewalCommission7800();
	}

protected void postInitialCommission7600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					postCommDue7610();
				case postCommEarn7620: 
					postCommEarn7620();
				case postCommPay7630: 
					postCommPay7630();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void postCommDue7610()
	{
		if (isEQ(wsaaCommDue,ZERO)) {
			goTo(GotoLabel.postCommEarn7620);
		}
		wsaaSubNo.set(11);
		readT5645ExactItem9000();
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub.toInt()]);
		lifacmvrec.rldgacct.set(comlinkrec.agent);
		lifacmvrec.origamt.set(wsaaCommDue);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		wsaaPlanSuffixN.set(ubblallpar.planSuffix);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(ubblallpar.chdrChdrnum);
		stringVariable1.addExpression(ubblallpar.lifeLife);
		stringVariable1.addExpression(ubblallpar.covrCoverage);
		stringVariable1.addExpression(ubblallpar.covrRider);
		stringVariable1.addExpression(wsaaPlanSuffixC);
		stringVariable1.setStringInto(lifacmvrec.tranref);
		if (isEQ(ubblallpar.function,"RENL")) {
			lifacmvrec.function.set("NPSTW");
		}
		if (isEQ(ubblallpar.function,"DRENL")) {
			lifacmvrec.function.set("NPSTD");
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
		if (isEQ(ubblallpar.function,"RENL")) {
			d000WriteAcag();
			lifacmvrec.function.set("PSTW");
		}
		/*                                                         <DRYAP2>*/
		if (isEQ(ubblallpar.function, "DRENL")) {
			d000WriteDacm();
			lifacmvrec.function.set("PSTD");
		}
		/* Override Commission                                             */
		if (isEQ(th605rec.indic, "Y")) {
			if (isEQ(ubblallpar.function, "ISSUE")) {
				zorlnkrec.annprem.set(agcmIO.getAnnprem());
				zorlnkrec.effdate.set(agcmIO.getEfdate());
			}
			else {
				zorlnkrec.annprem.set(agcmbchIO.getAnnprem());
				zorlnkrec.effdate.set(agcmbchIO.getEfdate());
			}
			d500CallZorcompy();
		}
	}

protected void postCommEarn7620()
	{
		if (isEQ(wsaaCommEarn,ZERO)) {
			goTo(GotoLabel.postCommPay7630);
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaSubNo.set(14);
		}
		else {
			wsaaSubNo.set(12);
		}
		readT5645ExactItem9000();
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub.toInt()]);
		lifacmvrec.tranref.set(comlinkrec.agent);
		lifacmvrec.origamt.set(wsaaCommEarn);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		wsaaPlanSuffixN.set(ubblallpar.planSuffix);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(ubblallpar.chdrChdrnum);
		stringVariable1.addExpression(ubblallpar.lifeLife);
		stringVariable1.addExpression(ubblallpar.covrCoverage);
		stringVariable1.addExpression(ubblallpar.covrRider);
		stringVariable1.addExpression(wsaaPlanSuffixC);
		stringVariable1.setStringInto(lifacmvrec.rldgacct);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
	}

protected void postCommPay7630()
	{
		if (isEQ(wsaaCommPay,ZERO)) {
			return ;
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaSub.set(15);
		}
		else {
			wsaaSub.set(13);
		}
		readT5645ExactItem9000();
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub.toInt()]);
		lifacmvrec.rldgacct.set(comlinkrec.agent);
		lifacmvrec.origamt.set(wsaaCommPay);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		wsaaPlanSuffixN.set(ubblallpar.planSuffix);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(ubblallpar.chdrChdrnum);
		stringVariable1.addExpression(ubblallpar.lifeLife);
		stringVariable1.addExpression(ubblallpar.covrCoverage);
		stringVariable1.addExpression(ubblallpar.covrRider);
		stringVariable1.addExpression(wsaaPlanSuffixC);
		stringVariable1.setStringInto(lifacmvrec.tranref);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
	}

protected void postServiceCommission7700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					postServDue7710();
				case postServEarn7720: 
					postServEarn7720();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void postServDue7710()
	{
		if (isEQ(wsaaServDue,ZERO)) {
			goTo(GotoLabel.postServEarn7720);
		}
		wsaaSubNo.set(16);
		readT5645ExactItem9000();
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub.toInt()]);
		lifacmvrec.tranref.set(comlinkrec.agent);
		lifacmvrec.origamt.set(wsaaServDue);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		wsaaPlanSuffixN.set(ubblallpar.planSuffix);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(ubblallpar.chdrChdrnum);
		stringVariable1.addExpression(ubblallpar.lifeLife);
		stringVariable1.addExpression(ubblallpar.covrCoverage);
		stringVariable1.addExpression(ubblallpar.covrRider);
		stringVariable1.addExpression(wsaaPlanSuffixC);
		stringVariable1.setStringInto(lifacmvrec.rldgacct);
		if (isEQ(ubblallpar.function,"RENL")) {
			lifacmvrec.function.set("NPSTW");
		}
		if (isEQ(ubblallpar.function,"DRENL")) {
			lifacmvrec.function.set("NPSTD");
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
		if (isEQ(ubblallpar.function,"RENL")) {
			d000WriteAcag();
			lifacmvrec.function.set("PSTW");
		}
		if (isEQ(ubblallpar.function, "DRENL")) {
			d000WriteDacm();
			lifacmvrec.function.set("PSTD");
		}
		/* Override Commission                                             */
		if (isEQ(th605rec.indic, "Y")) {
			if (isEQ(ubblallpar.function, "ISSUE")) {
				zorlnkrec.annprem.set(agcmIO.getAnnprem());
				zorlnkrec.effdate.set(agcmIO.getEfdate());
			}
			else {
				zorlnkrec.annprem.set(agcmbchIO.getAnnprem());
				zorlnkrec.effdate.set(agcmbchIO.getEfdate());
			}
			d500CallZorcompy();
		}
	}

protected void postServEarn7720()
	{
		if (isEQ(wsaaServEarn,ZERO)) {
			return ;
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaSubNo.set(18);
		}
		else {
			wsaaSubNo.set(17);
		}
		readT5645ExactItem9000();
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub.toInt()]);
		lifacmvrec.tranref.set(comlinkrec.agent);
		lifacmvrec.origamt.set(wsaaServEarn);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		wsaaPlanSuffixN.set(ubblallpar.planSuffix);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(ubblallpar.chdrChdrnum);
		stringVariable1.addExpression(ubblallpar.lifeLife);
		stringVariable1.addExpression(ubblallpar.covrCoverage);
		stringVariable1.addExpression(ubblallpar.covrRider);
		stringVariable1.addExpression(wsaaPlanSuffixC);
		stringVariable1.setStringInto(lifacmvrec.rldgacct);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
	}

protected void postRenewalCommission7800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					postRenlDue7810();
				case postRenlEarn7820: 
					postRenlEarn7820();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void postRenlDue7810()
	{
		if (isEQ(wsaaRenlDue,ZERO)) {
			goTo(GotoLabel.postRenlEarn7820);
		}
		wsaaSubNo.set(19);
		readT5645ExactItem9000();
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub.toInt()]);
		lifacmvrec.rldgacct.set(comlinkrec.agent);
		lifacmvrec.origamt.set(wsaaRenlDue);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		wsaaPlanSuffixN.set(ubblallpar.planSuffix);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(ubblallpar.chdrChdrnum);
		stringVariable1.addExpression(ubblallpar.lifeLife);
		stringVariable1.addExpression(ubblallpar.covrCoverage);
		stringVariable1.addExpression(ubblallpar.covrRider);
		stringVariable1.addExpression(wsaaPlanSuffixC);
		stringVariable1.setStringInto(lifacmvrec.tranref);
		if (isEQ(ubblallpar.function,"RENL")) {
			lifacmvrec.function.set("NPSTW");
		}
		if (isEQ(ubblallpar.function,"DRENL")) {
			lifacmvrec.function.set("NPSTD");
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
		if (isEQ(ubblallpar.function,"RENL")) {
			d000WriteAcag();
			lifacmvrec.function.set("PSTW");
		}
		if (isEQ(ubblallpar.function, "DRENL")) {
			d000WriteDacm();
			lifacmvrec.function.set("PSTD");
		}
		/* Override Commission                                             */
		if (isEQ(th605rec.indic, "Y")) {
			if (isEQ(ubblallpar.function, "ISSUE")) {
				zorlnkrec.annprem.set(agcmIO.getAnnprem());
				zorlnkrec.effdate.set(agcmIO.getEfdate());
			}
			else {
				zorlnkrec.annprem.set(agcmbchIO.getAnnprem());
				zorlnkrec.effdate.set(agcmbchIO.getEfdate());
			}
			d500CallZorcompy();
		}
	}

protected void postRenlEarn7820()
	{
		if (isEQ(wsaaRenlEarn,ZERO)) {
			return ;
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaSubNo.set(21);
		}
		else {
			wsaaSubNo.set(20);
		}
		readT5645ExactItem9000();
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub.toInt()]);
		lifacmvrec.tranref.set(comlinkrec.agent);
		lifacmvrec.origamt.set(wsaaRenlEarn);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		wsaaSequenceNo.add(1);
		wsaaPlanSuffixN.set(ubblallpar.planSuffix);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(ubblallpar.chdrChdrnum);
		stringVariable1.addExpression(ubblallpar.lifeLife);
		stringVariable1.addExpression(ubblallpar.covrCoverage);
		stringVariable1.addExpression(ubblallpar.covrRider);
		stringVariable1.addExpression(wsaaPlanSuffixC);
		stringVariable1.setStringInto(lifacmvrec.rldgacct);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError570();
		}
	}

protected void updateAgcmbch8000()
	{
		begn8010();
	}

protected void begn8010()
	{
		agcmbchIO.setDataKey(SPACES);
		agcmbchIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		agcmbchIO.setChdrnum(ubblallpar.chdrChdrnum);
		agcmbchIO.setLife(ubblallpar.lifeLife);
		agcmbchIO.setCoverage(ubblallpar.covrCoverage);
		agcmbchIO.setRider(ubblallpar.covrRider);
		agcmbchIO.setPlanSuffix(ubblallpar.planSuffix);
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
		agcmbchIO.setFunction(varcom.begn);
		agcmbchIO.setStatuz(varcom.oK);
		while ( !(isEQ(agcmbchIO.getStatuz(),varcom.endp))) {
			//performance improvement --  atiwari23 
			agcmbchIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			agcmbchIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
			processAgcm8100();
		}
		
	}

protected void processAgcm8100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					callAgcmbchio8110();
				case nextr8120: 
					nextr8120();
				case exit8190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void callAgcmbchio8110()
	{
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(),varcom.oK)
		&& isNE(agcmbchIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			systemError570();
		}
		if (isEQ(agcmbchIO.getStatuz(),varcom.endp)
		|| isNE(agcmbchIO.getChdrcoy(),ubblallpar.chdrChdrcoy)
		|| isNE(agcmbchIO.getChdrnum(),ubblallpar.chdrChdrnum)
		|| isNE(agcmbchIO.getLife(),ubblallpar.lifeLife)
		|| isNE(agcmbchIO.getCoverage(),ubblallpar.covrCoverage)
		|| isNE(agcmbchIO.getRider(),ubblallpar.covrRider)
		|| isNE(agcmbchIO.getPlanSuffix(),ubblallpar.planSuffix)) {
			agcmbchIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit8190);
		}
		if (isEQ(agcmbchIO.getDormantFlag(),"Y")
		|| isEQ(agcmbchIO.getPtdate(),ZERO)) {
			goTo(GotoLabel.nextr8120);
		}
		comlinkrec.chdrcoy.set(ubblallpar.chdrChdrcoy);
		comlinkrec.chdrnum.set(ubblallpar.chdrChdrnum);
		comlinkrec.life.set(ubblallpar.lifeLife);
		comlinkrec.coverage.set(ubblallpar.covrCoverage);
		comlinkrec.effdate.set(agcmbchIO.getEfdate());
		comlinkrec.currto.set(covrIO.getCurrto());
		comlinkrec.rider.set(ubblallpar.covrRider);
		comlinkrec.planSuffix.set(ubblallpar.planSuffix);
		comlinkrec.jlife.set(ubblallpar.lifeJlife);
		comlinkrec.crtable.set(ubblallpar.crtable);
		comlinkrec.icommtot.set(ZERO);
		comlinkrec.icommpd.set(ZERO);
		comlinkrec.icommernd.set(ZERO);
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		comlinkrec.instprem.set(ZERO);
		comlinkrec.annprem.set(ZERO);
		comlinkrec.targetPrem.set(ZERO);
		comlinkrec.agent.set(agcmbchIO.getAgntnum());
		comlinkrec.agentClass.set(agcmbchIO.getAgentClass());
		comlinkrec.annprem.set(agcmbchIO.getAnnprem());
		comlinkrec.instprem.set(premiumrec.calcPrem);
		comlinkrec.billfreq.set(ubblallpar.billfreq);
		comlinkrec.seqno.set(1);
		wsaaCommDue.set(ZERO);
		wsaaCommEarn.set(ZERO);
		wsaaCommPay.set(ZERO);
		wsaaServDue.set(ZERO);
		wsaaServEarn.set(ZERO);
		wsaaRenlDue.set(ZERO);
		wsaaRenlEarn.set(ZERO);
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.frequency.set(t5534rec.unitFreq);
		datcon2rec.freqFactor.set(1);
		datcon2rec.intDate1.set(ubblallpar.effdate);
		datcon2rec.intDate2.set(ZERO);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			systemError570();
		}
		comlinkrec.ptdate.set(datcon2rec.intDate2);
		for (wsaaCommType.set(1); !(isGT(wsaaCommType,3)); wsaaCommType.add(1)){
			callSubroutine8200();
		}
		postingCommission7500();
		rewriteAgcm8300();
	}

protected void nextr8120()
	{
		agcmbchIO.setFunction(varcom.nextr);
	}

protected void callSubroutine8200()
	{
		try {
			callSubr8210();
			setUpPostingValues8220();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void callSubr8210()
	{
		initialize(comlinkrec.function);
		initialize(comlinkrec.statuz);
		comlinkrec.icommtot.set(ZERO);
		comlinkrec.icommpd.set(ZERO);
		comlinkrec.icommernd.set(ZERO);
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		wsaaPayamnt[wsaaCommType.toInt()].set(ZERO);
		wsaaErndamt[wsaaCommType.toInt()].set(ZERO);
		itemIO.setDataKey(SPACES);
		if (isEQ(wsaaCommType,1)) {
			if (isEQ(agcmbchIO.getBascpy(),SPACES)
			|| (isEQ(agcmbchIO.getCompay(),agcmbchIO.getInitcom())
			&& isEQ(agcmbchIO.getComern(),agcmbchIO.getInitcom()))) {
				goTo(GotoLabel.exit8290);
			}
			else {
				itemIO.setItemitem(agcmbchIO.getBascpy());
			}
		}
		if (isEQ(wsaaCommType,2)) {
			if (isEQ(agcmbchIO.getRnwcpy(),SPACES)) {
				goTo(GotoLabel.exit8290);
			}
			else {
				itemIO.setItemitem(agcmbchIO.getRnwcpy());
			}
		}
		if (isEQ(wsaaCommType,3)) {
			if (isEQ(agcmbchIO.getSrvcpy(),SPACES)
			|| isGT(agcmbchIO.getSeqno(),1)) {
				goTo(GotoLabel.exit8290);
			}
			else {
				itemIO.setItemitem(agcmbchIO.getSrvcpy());
			}
		}
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ubblallpar.chdrChdrcoy);
		itemIO.setItemtabl(tablesInner.t5644);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError570();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit8290);
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		comlinkrec.method.set(itemIO.getItemitem());
		if (isEQ(wsaaCommType,1)) {
			comlinkrec.icommtot.set(agcmbchIO.getInitcom());
			comlinkrec.icommpd.set(agcmbchIO.getCompay());
			comlinkrec.icommernd.set(agcmbchIO.getComern());
		}
		callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			syserrrec.statuz.set(comlinkrec.statuz);
			systemError570();
		}
		zrdecplrec.amountIn.set(comlinkrec.icommtot);
		a000CallRounding();
		comlinkrec.icommtot.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		a000CallRounding();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		a000CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		wsaaPayamnt[wsaaCommType.toInt()].set(comlinkrec.payamnt);
		wsaaErndamt[wsaaCommType.toInt()].set(comlinkrec.erndamt);
	}

protected void setUpPostingValues8220()
	{
		/*  Basic Commission*/
		if (isEQ(wsaaCommType,1)) {
			/*       IF AGCMBCH-OVRDCAT       = 'O'*/
			wsaaCommDue.set(comlinkrec.payamnt);
			wsaaCommEarn.set(comlinkrec.erndamt);
			compute(wsaaCommPay, 2).set(add(wsaaCommPay,(sub(comlinkrec.payamnt,comlinkrec.erndamt))));
			/**       ELSE*/
			/**          MOVE CLNK-PAYAMNT           TO WSAA-BASCPY-DUE*/
			/**          MOVE CLNK-ERNDAMT           TO WSAA-BASCPY-ERN*/
			/**          COMPUTE WSAA-BASCPY-PAY = WSAA-BASCPY-PAY +*/
			/**                              (CLNK-PAYAMNT - CLNK-ERNDAMT)*/
			/**       END-IF*/
		}
		/*  Service Commission*/
		if (isEQ(wsaaCommType,3)) {
			wsaaServDue.set(comlinkrec.payamnt);
			wsaaServEarn.set(comlinkrec.erndamt);
		}
		/*  Renewal Commission*/
		if (isEQ(wsaaCommType,2)) {
			wsaaRenlDue.set(comlinkrec.payamnt);
			wsaaRenlEarn.set(comlinkrec.erndamt);
		}
		if (isNE(th605rec.bonusInd,"Y")) {
			return ;
		}
		/* Bonus Workbench *                                               */
		/* Skip overriding commission                                      */
		if (isNE(agcmbchIO.getOvrdcat(),"B")) {
			return ;
		}
		if (isEQ(wsaaCommType,1)){
			firstPrem.setTrue();
		}
		else if (isEQ(wsaaCommType,2)){
			renPrem.setTrue();
		}
		else{
			return ;
		}
		if (isNE(comlinkrec.payamnt,ZERO)) {
			zctnIO.setParams(SPACES);
			zctnIO.setAgntcoy(agcmbchIO.getChdrcoy());
			zctnIO.setAgntnum(agcmbchIO.getAgntnum());
			zctnIO.setCommAmt(comlinkrec.payamnt);
			c400LocatePremium();
			setPrecision(zctnIO.getPremium(), 3);
			zctnIO.setPremium(div(wsaaAgcmPremium,wsaaBillfq9), true);
			setPrecision(zctnIO.getSplitBcomm(), 3);
			zctnIO.setSplitBcomm(div(mult(agcmbchIO.getAnnprem(),100),wsaaAgcmPremium), true);
			zctnIO.setZprflg(wsaaPremType);
			zctnIO.setChdrcoy(comlinkrec.chdrcoy);
			zctnIO.setChdrnum(comlinkrec.chdrnum);
			zctnIO.setLife(comlinkrec.life);
			zctnIO.setCoverage(comlinkrec.coverage);
			zctnIO.setRider(comlinkrec.rider);
			zctnIO.setTranno(ubblallpar.tranno);
			zctnIO.setTransCode(ubblallpar.batctrcde);
			zctnIO.setEffdate(ubblallpar.effdate);
			zctnIO.setTrandate(wsaaToday);
			zctnIO.setFormat(formatsInner.zctnrec);
			zctnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zctnIO);
			if (isNE(zctnIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(zctnIO.getStatuz());
				syserrrec.params.set(zctnIO.getParams());
				systemError570();
			}
		}
	}

protected void rewriteAgcm8300()
	{
		rewrite8310();
	}

protected void rewrite8310()
	{
		agcmbchIO.setFunction(varcom.readh);
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			systemError570();
		}
		if (isNE(agcmbchIO.getBascpy(),SPACES)) {
			setPrecision(agcmbchIO.getCompay(), 2);
			agcmbchIO.setCompay(add(agcmbchIO.getCompay(),wsaaPayamnt[1]));
			setPrecision(agcmbchIO.getComern(), 2);
			agcmbchIO.setComern(add(agcmbchIO.getComern(),wsaaErndamt[1]));
		}
		if (isNE(agcmbchIO.getRnwcpy(),SPACES)) {
			setPrecision(agcmbchIO.getRnlcdue(), 2);
			agcmbchIO.setRnlcdue(add(agcmbchIO.getRnlcdue(),wsaaPayamnt[2]));
			setPrecision(agcmbchIO.getRnlcearn(), 2);
			agcmbchIO.setRnlcearn(add(agcmbchIO.getRnlcearn(),wsaaErndamt[2]));
		}
		if (isNE(agcmbchIO.getSrvcpy(),SPACES)) {
			setPrecision(agcmbchIO.getScmdue(), 2);
			agcmbchIO.setScmdue(add(agcmbchIO.getScmdue(),wsaaPayamnt[3]));
			setPrecision(agcmbchIO.getScmearn(), 2);
			agcmbchIO.setScmearn(add(agcmbchIO.getScmearn(),wsaaErndamt[3]));
		}
		agcmbchIO.setPtdate(ubblallpar.ptdate);
		agcmbchIO.setFunction(varcom.rewrt);
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			systemError570();
		}
	}

protected void readT5645ExactItem9000()
	{
		read9010();
	}

protected void read9010()
	{
		wsaaItemkey.itemItemseq.set(SPACES);
		if (isGT(wsaaSubNo,15)) {
			compute(wsaaSubRound, 0).setDivide(wsaaSubNo, (15));
			wsaaSub.setRemainder(wsaaSubRound);
			if (isEQ(wsaaSub,ZERO)) {
				wsaaSub.set(15);
				wsaaSubRound.subtract(1);
			}
			wsaaItemkey.itemItemseq.set(wsaaSubRoundC);
		}
		else {
			wsaaSub.set(wsaaSubNo);
		}
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(ubblallpar.chdrChdrcoy);
		wsaaItemkey.itemItemtabl.set(tablesInner.t5645);
		wsaaItemkey.itemItemitem.set(wsaaSubr);
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			databaseError580();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(ubblallpar.chdrChdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(ubblallpar.cntcurr);
		zrdecplrec.batctrcde.set(ubblallpar.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			databaseError580();
		}
		/*A900-EXIT*/
	}

protected void c000ReadPayr()
	{
		/*C010-READ*/
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		payrIO.setChdrnum(ubblallpar.chdrChdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			systemError570();
		}
		/*C090-EXIT*/
	}

protected void c100InitializeArrays()
	{
		/*C110-INIT*/
		for (wsaaAgcmIy.set(1); !(isGT(wsaaAgcmIy,wsaaAgcmIySize)); wsaaAgcmIy.add(1)){
			wsaaAgcmSeqno[wsaaAgcmIy.toInt()].set(ZERO);
			wsaaAgcmEfdate[wsaaAgcmIy.toInt()].set(ZERO);
			wsaaAgcmAnnprem[wsaaAgcmIy.toInt()].set(ZERO);
		}
		/*C190-EXIT*/
	}

protected void c200PremiumHistory()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					c210Init();
				case c220Call: 
					c220Call();
					c230Summary();
				case c280Next: 
					c280Next();
				case c290Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void c210Init()
	{
		/* Read the AGCM for differentiating the First & Renewal Year      */
		/* Premium                                                         */
		agcmseqIO.setDataArea(SPACES);
		agcmseqIO.setChdrcoy(ubblallpar.chdrChdrcoy);
		agcmseqIO.setChdrnum(ubblallpar.chdrChdrnum);
		agcmseqIO.setLife(ubblallpar.lifeLife);
		agcmseqIO.setCoverage(ubblallpar.covrCoverage);
		agcmseqIO.setRider(ubblallpar.covrRider);
		agcmseqIO.setPlanSuffix(ubblallpar.planSuffix);
		agcmseqIO.setSeqno(ZERO);
		agcmseqIO.setFormat(formatsInner.agcmseqrec);
		agcmseqIO.setFunction(varcom.begn);
	}

protected void c220Call()
	{
	//performance improvement --  atiwari23 
	agcmseqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	agcmseqIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, agcmseqIO);
		if (isNE(agcmseqIO.getStatuz(),varcom.oK)
		&& isNE(agcmseqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(agcmseqIO.getStatuz());
			syserrrec.params.set(agcmseqIO.getParams());
			systemError570();
		}
		if (isEQ(agcmseqIO.getStatuz(),varcom.endp)
		|| isNE(agcmseqIO.getChdrcoy(),ubblallpar.chdrChdrcoy)
		|| isNE(agcmseqIO.getChdrnum(),ubblallpar.chdrChdrnum)
		|| isNE(agcmseqIO.getLife(),ubblallpar.lifeLife)
		|| isNE(agcmseqIO.getCoverage(),ubblallpar.covrCoverage)
		|| isNE(agcmseqIO.getRider(),ubblallpar.covrRider)) {
			agcmseqIO.setStatuz(varcom.endp);
			goTo(GotoLabel.c290Exit);
		}
		/* Skip those irrelevant records                                   */
		if (isNE(agcmseqIO.getOvrdcat(),"B")
		|| isEQ(agcmseqIO.getPtdate(),ZERO)
		|| isEQ(agcmseqIO.getEfdate(),ZERO)
		|| isEQ(agcmseqIO.getEfdate(),varcom.vrcmMaxDate)) {
			goTo(GotoLabel.c280Next);
		}
	}

protected void c230Summary()
	{
		wsaaAgcmFound.set("N");
		for (wsaaAgcmIy.set(1); !(isGT(wsaaAgcmIy,wsaaAgcmIySize)
		|| isEQ(wsaaAgcmEfdate[wsaaAgcmIy.toInt()],ZERO)
		|| agcmFound.isTrue()); wsaaAgcmIy.add(1)){
			if (isEQ(agcmseqIO.getSeqno(),wsaaAgcmSeqno[wsaaAgcmIy.toInt()])) {
				wsaaAgcmAnnprem[wsaaAgcmIy.toInt()].add(agcmseqIO.getAnnprem());
				agcmFound.setTrue();
			}
		}
		if (!agcmFound.isTrue()) {
			if (isGT(wsaaAgcmIy,wsaaAgcmIySize)) {
				syserrrec.statuz.set(e103);
				systemError570();
			}
			wsaaAgcmSeqno[wsaaAgcmIy.toInt()].set(agcmseqIO.getSeqno());
			wsaaAgcmEfdate[wsaaAgcmIy.toInt()].set(agcmseqIO.getEfdate());
			wsaaAgcmAnnprem[wsaaAgcmIy.toInt()].set(agcmseqIO.getAnnprem());
		}
	}

protected void c280Next()
	{
		agcmseqIO.setFunction(varcom.nextr);
		goTo(GotoLabel.c220Call);
	}

protected void c300WriteArrays()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					c310Init();
				case c320Loop: 
					c320Loop();
					c330Writ();
				case c380Next: 
					c380Next();
				case c390Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void c310Init()
	{
		wsaaAgcmIy.set(1);
	}

protected void c320Loop()
	{
		if (isEQ(wsaaAgcmEfdate[wsaaAgcmIy.toInt()],ZERO)) {
			goTo(GotoLabel.c390Exit);
		}
		else {
			if (isGT(wsaaAgcmEfdate[wsaaAgcmIy.toInt()],ubblallpar.effdate)) {
				goTo(GotoLabel.c380Next);
			}
		}
	}

protected void c330Writ()
	{
		initialize(datcon3rec.datcon3Rec);
		datcon3rec.intDate1.set(wsaaAgcmEfdate[wsaaAgcmIy.toInt()]);
		datcon3rec.intDate2.set(ubblallpar.effdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			systemError570();
		}
		if (isGTE(datcon3rec.freqFactor,1)) {
			renPrem.setTrue();
		}
		else {
			firstPrem.setTrue();
		}
		if (isNE(wsaaAgcmAnnprem[wsaaAgcmIy.toInt()],ZERO)) {
			zptnIO.setParams(SPACES);
			zptnIO.setChdrcoy(ubblallpar.chdrChdrcoy);
			zptnIO.setChdrnum(ubblallpar.chdrChdrnum);
			zptnIO.setLife(ubblallpar.lifeLife);
			zptnIO.setCoverage(ubblallpar.covrCoverage);
			zptnIO.setRider(ubblallpar.covrRider);
			zptnIO.setTranno(ubblallpar.tranno);
			setPrecision(zptnIO.getOrigamt(), 3);
			zptnIO.setOrigamt(div(wsaaAgcmAnnprem[wsaaAgcmIy.toInt()],wsaaBillfq9), true);
			zrdecplrec.amountIn.set(zptnIO.getOrigamt());
			a000CallRounding();
			zptnIO.setOrigamt(zrdecplrec.amountOut);
			zptnIO.setTransCode(ubblallpar.batctrcde);
			zptnIO.setEffdate(ubblallpar.effdate);
			zptnIO.setInstfrom(ubblallpar.effdate);
			zptnIO.setBillcd(ubblallpar.effdate);
			zptnIO.setInstto(wsaaPtdate);
			zptnIO.setTrandate(wsaaToday);
			zptnIO.setZprflg(wsaaPremType);
			zptnIO.setFormat(formatsInner.zptnrec);
			zptnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zptnIO);
			if (isNE(zptnIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(zptnIO.getStatuz());
				syserrrec.params.set(zptnIO.getParams());
				systemError570();
			}
		}
	}

protected void c380Next()
	{
		wsaaAgcmIy.add(1);
		goTo(GotoLabel.c320Loop);
	}

protected void c400LocatePremium()
	{
		/*C410-INIT*/
		wsaaAgcmFound.set("N");
		wsaaAgcmPremium.set(ZERO);
		for (wsaaAgcmIb.set(1); !(isGT(wsaaAgcmIb,wsaaAgcmIySize)
		|| agcmFound.isTrue()); wsaaAgcmIb.add(1)){
			if (isEQ(agcmbchIO.getSeqno(),wsaaAgcmSeqno[wsaaAgcmIb.toInt()])) {
				agcmFound.setTrue();
				wsaaAgcmPremium.set(wsaaAgcmAnnprem[wsaaAgcmIb.toInt()]);
			}
		}
		/*C490-EXIT*/
	}

protected void c500WriteZptn()
	{
		c510Writ();
	}

protected void c510Writ()
	{
		compute(wsaaRegPrem, 2).set(mult(premiumrec.calcPrem,wsaaBillfq9));
		if (isGT(premiumrec.calcPrem,wsaaRegPrem)) {
			wsaaRegPremFirst.set(wsaaRegPrem);
			compute(wsaaRegPremRenewal, 2).set(sub(premiumrec.calcPrem,wsaaRegPrem));
			initialize(datcon2rec.datcon2Rec);
			datcon2rec.freqFactor.set(1);
			datcon2rec.frequency.set("01");
			datcon2rec.intDate1.set(ubblallpar.effdate);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.statuz.set(datcon2rec.statuz);
				syserrrec.params.set(datcon2rec.datcon2Rec);
				systemError570();
			}
			wsaaRegIntmDate.set(datcon2rec.intDate2);
		}
		else {
			wsaaRegPremFirst.set(premiumrec.calcPrem);
			wsaaRegPremRenewal.set(ZERO);
			wsaaRegIntmDate.set(wsaaPtdate);
		}
		/* Regular Premium - First Year                                    */
		if (isNE(wsaaRegPremFirst,0)) {
			zptnIO.setParams(SPACES);
			zptnIO.setChdrcoy(ubblallpar.chdrChdrcoy);
			zptnIO.setChdrnum(ubblallpar.chdrChdrnum);
			zptnIO.setLife(ubblallpar.lifeLife);
			zptnIO.setCoverage(ubblallpar.covrCoverage);
			zptnIO.setRider(ubblallpar.covrRider);
			zptnIO.setTranno(ubblallpar.tranno);
			zptnIO.setOrigamt(wsaaRegPremFirst);
			zptnIO.setTransCode(ubblallpar.batctrcde);
			zptnIO.setEffdate(ubblallpar.effdate);
			zptnIO.setBillcd(ubblallpar.effdate);
			zptnIO.setInstfrom(ubblallpar.effdate);
			zptnIO.setInstto(wsaaRegIntmDate);
			zptnIO.setTrandate(wsaaToday);
			zptnIO.setZprflg("I");
			zptnIO.setFormat(formatsInner.zptnrec);
			zptnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zptnIO);
			if (isNE(zptnIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(zptnIO.getStatuz());
				syserrrec.params.set(zptnIO.getParams());
				systemError570();
			}
		}
		/* Regular Premium - Renewal Year                                  */
		if (isNE(wsaaRegPremRenewal,0)) {
			zptnIO.setParams(SPACES);
			zptnIO.setChdrcoy(ubblallpar.chdrChdrcoy);
			zptnIO.setChdrnum(ubblallpar.chdrChdrnum);
			zptnIO.setLife(ubblallpar.lifeLife);
			zptnIO.setCoverage(ubblallpar.covrCoverage);
			zptnIO.setRider(ubblallpar.covrRider);
			zptnIO.setTranno(ubblallpar.tranno);
			zptnIO.setOrigamt(wsaaRegPremRenewal);
			zptnIO.setTransCode(ubblallpar.batctrcde);
			zptnIO.setBillcd(ubblallpar.effdate);
			zptnIO.setEffdate(wsaaRegIntmDate);
			zptnIO.setInstfrom(wsaaRegIntmDate);
			zptnIO.setInstto(wsaaPtdate);
			zptnIO.setTrandate(wsaaToday);
			zptnIO.setZprflg("R");
			zptnIO.setFormat(formatsInner.zptnrec);
			zptnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zptnIO);
			if (isNE(zptnIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(zptnIO.getStatuz());
				syserrrec.params.set(zptnIO.getParams());
				systemError570();
			}
		}
	}

protected void d000WriteAcag()
	{
		d010WriteRecord();
	}

protected void d010WriteRecord()
	{
		acagrnlIO.setRldgcoy(lifacmvrec.rldgcoy);
		acagrnlIO.setSacscode(lifacmvrec.sacscode);
		acagrnlIO.setRldgacct(lifacmvrec.rldgacct);
		acagrnlIO.setOrigcurr(lifacmvrec.origcurr);
		acagrnlIO.setSacstyp(lifacmvrec.sacstyp);
		acagrnlIO.setSacscurbal(lifacmvrec.origamt);
		acagrnlIO.setRdocnum(lifacmvrec.rdocnum);
		acagrnlIO.setGlsign(lifacmvrec.glsign);
		acagrnlIO.setFunction(varcom.writr);
		acagrnlIO.setFormat(formatsInner.acagrnlrec);
		SmartFileCode.execute(appVars, acagrnlIO);
		if (isNE(acagrnlIO.getStatuz(),varcom.oK)
		&& isNE(acagrnlIO.getStatuz(),varcom.dupr)) {
			syserrrec.params.set(acagrnlIO.getParams());
			systemError570();
		}
	}

protected void d000WriteDacm()
	{
		d100Write();
	}

protected void d100Write()
	{
		/* A deferred ACBL update has been found.                          */
		/* Start by completeing the fields on the ACBL data area.          */
		/* Once this is complete, write a new DACM record using the        */
		/* ACBL data area.                                                 */
		acagrnlIO.setParams(SPACES);
		acagrnlIO.setRecKeyData(SPACES);
		acagrnlIO.setRecNonKeyData(SPACES);
		acagrnlIO.setRldgcoy(lifacmvrec.rldgcoy);
		acagrnlIO.setSacscode(lifacmvrec.sacscode);
		acagrnlIO.setRldgacct(lifacmvrec.rldgacct);
		acagrnlIO.setOrigcurr(lifacmvrec.origcurr);
		acagrnlIO.setSacstyp(lifacmvrec.sacstyp);
		acagrnlIO.setSacscurbal(lifacmvrec.origamt);
		acagrnlIO.setRdocnum(lifacmvrec.rdocnum);
		acagrnlIO.setGlsign(lifacmvrec.glsign);
		/* Set up the DACM record, used for deferred processing            */
		/* within the diary system.                                        */
		dacmIO.setParams(SPACES);
		dacmIO.setRecKeyData(SPACES);
		dacmIO.setRecNonKeyData(SPACES);
		dacmIO.setScheduleThreadNo(ubblallpar.threadNumber);
		dacmIO.setCompany(lifacmvrec.batccoy);
		dacmIO.setRecformat(formatsInner.acagrnlrec);
		dacmIO.setDataarea(acagrnlIO.getDataArea());
		dacmIO.setFormat(formatsInner.dacmrec);
		dacmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, dacmIO);
		if (isNE(dacmIO.getStatuz(), varcom.oK)) {
			ubblallpar.statuz.set(dacmIO.getStatuz());
			syserrrec.statuz.set(dacmIO.getStatuz());
			systemError570();
		}
	}

protected void d500CallZorcompy()
	{
		d510Start();
	}

protected void d510Start()
	{
		zorlnkrec.function.set(SPACES);
		zorlnkrec.clawback.set(SPACES);
		zorlnkrec.agent.set(lifacmvrec.rldgacct);
		zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
		zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
		zorlnkrec.crtable.set(lifacmvrec.substituteCode[6]);
		zorlnkrec.ptdate.set(ubblallpar.ptdate);
		zorlnkrec.origcurr.set(lifacmvrec.origcurr);
		zorlnkrec.crate.set(lifacmvrec.crate);
		zorlnkrec.origamt.set(lifacmvrec.origamt);
		zorlnkrec.tranno.set(ubblallpar.tranno);
		zorlnkrec.trandesc.set(lifacmvrec.trandesc);
		zorlnkrec.tranref.set(lifacmvrec.tranref);
		zorlnkrec.genlcur.set(lifacmvrec.genlcur);
		zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
		zorlnkrec.termid.set(lifacmvrec.termid);
		zorlnkrec.cnttype.set(lifacmvrec.substituteCode[1]);
		zorlnkrec.batchKey.set(lifacmvrec.batckey);
		callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
		if (isNE(zorlnkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zorlnkrec.statuz);
			syserrrec.params.set(zorlnkrec.zorlnkRec);
			systemError570();
		}
	}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t5675 = new FixedLengthStringData(5).init("T5675");
	private FixedLengthStringData t1688 = new FixedLengthStringData(5).init("T1688");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t6598 = new FixedLengthStringData(5).init("T6598");
	private FixedLengthStringData t5691 = new FixedLengthStringData(5).init("T5691");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5647 = new FixedLengthStringData(5).init("T5647");
	private FixedLengthStringData t5644 = new FixedLengthStringData(5).init("T5644");
	private FixedLengthStringData t5534 = new FixedLengthStringData(5).init("T5534");
	private FixedLengthStringData th605 = new FixedLengthStringData(6).init("TH605");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData itemrec = new FixedLengthStringData(7).init("ITEMREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(7).init("COVRREC");
	private FixedLengthStringData lifernlrec = new FixedLengthStringData(10).init("LIFERNLREC");
	private FixedLengthStringData covrsurrec = new FixedLengthStringData(10).init("COVRSURREC");
	private FixedLengthStringData zrstrec = new FixedLengthStringData(10).init("ZRSTREC");
	private FixedLengthStringData zrstnudrec = new FixedLengthStringData(10).init("ZRSTNUDREC");
	private FixedLengthStringData agcmbchrec = new FixedLengthStringData(10).init("AGCMBCHREC");
	private FixedLengthStringData pcddlnbrec = new FixedLengthStringData(10).init("PCDDLNBREC");
	private FixedLengthStringData agcmrec = new FixedLengthStringData(10).init("AGCMREC");
	private FixedLengthStringData dacmrec = new FixedLengthStringData(10).init("DACMREC");
	private FixedLengthStringData acagrnlrec = new FixedLengthStringData(10).init("ACAGRNLREC");
	private FixedLengthStringData zptnrec = new FixedLengthStringData(10).init("ZPTNREC");
	private FixedLengthStringData zctnrec = new FixedLengthStringData(10).init("ZCTNREC");
	private FixedLengthStringData agcmseqrec = new FixedLengthStringData(10).init("AGCMSEQREC");
	private FixedLengthStringData taxdrec = new FixedLengthStringData(10).init("TAXDREC");
}
}
