package com.csc.life.regularprocessing.dataaccess.model;

public class Ustxpf {
	private String chdrcoy;
	private String chdrnum;
	private int statementDate;
	private int ptdate;
	private String cnttype;
	private String statcode;
	private String pstatcode;
	private int occdate;
	private long uniqueNumber;

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public int getStatementDate() {
		return statementDate;
	}

	public void setStatementDate(int statementDate) {
		this.statementDate = statementDate;
	}

	public int getPtdate() {
		return ptdate;
	}

	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}

	public String getCnttype() {
		return cnttype;
	}

	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}

	public String getStatcode() {
		return statcode;
	}

	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	public String getPstatcode() {
		return pstatcode;
	}

	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}

	public int getOccdate() {
		return occdate;
	}

	public void setOccdate(int occdate) {
		this.occdate = occdate;
	}

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

}
