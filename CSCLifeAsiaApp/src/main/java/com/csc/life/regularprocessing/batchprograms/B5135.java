/*
 * File: B5135.java
 * Date: 29 August 2009 20:58:15
 * Author: Quipoz Limited
 * 
 * Class transformed from B5135.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.Iterator;
import java.util.List;
import com.csc.life.regularprocessing.dataaccess.dao.AinrpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Ainrpf;
import com.csc.life.regularprocessing.reports.R5135Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*           PENDING AUTOMATIC INCREASES (REPORT).
*           -------------------------------------
*
* Overview
* ________
*
*   This program runs as part of the Automatic Increases multi-
* thread batch suite.  It runs directly after B5134 which is the
* multi-thread process program. B5134 writes records to the AINRPF
* which are then retrieved in single-thread mode by this program.
* All records read are then printed out in an Automatic Increase
* (Pending) Report.
*
* INITIALISE SECTION.
*     - Retrieve and set up standard report headings.
*     - Initialise logical AINR.
*
* READ SECTION.
*     - Read AINR, in coverag order where the record type is
*       equal to system parameter three on the process definition.
*       This will ensure that the AINR records retrieved from
*       the multi-thread process program will be printed in
*       order.
*
* PERFORM UNTIL END OF FILE.....
*     EDIT
*         - Move O-K to WSSP-EDTERROR so the UPDATE section
*           will be performed.
*
*      UPDATE
*         - If new page, write headings.
*         - Write details.
*
*      READ
*         - Read the next record.
*
*
* END PERFORM.
*
* CLOSE.
*     - Close all files.
*
*
* The CONTROL TOTALS maintained within this program are :
*     01  -  Number of records printed.
*     02  -  Number of pages.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class B5135 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R5135Report printerFile = new R5135Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5135");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String descrec = "DESCREC";
		/* TABLES */
	private String t1692 = "T1692";
	private String t1693 = "T1693";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
		/* ERRORS */
	private String ivrm = "IVRM";
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(2).init(SPACES);

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");
	private Validator notNewPageReq = new Validator(wsaaOverflow, "N");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private Validator notEndOfFile = new Validator(wsaaEof, "N");

	private FixedLengthStringData wsaaSysparam03 = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaRecordType = new FixedLengthStringData(4).isAPartOf(wsaaSysparam03, 0);

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData r5135H01 = new FixedLengthStringData(73);
	private FixedLengthStringData r5135h01O = new FixedLengthStringData(73).isAPartOf(r5135H01, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(r5135h01O, 0);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(r5135h01O, 1);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(r5135h01O, 31);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(r5135h01O, 41);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(r5135h01O, 43);

	private FixedLengthStringData r5135D01 = new FixedLengthStringData(99);
	private FixedLengthStringData r5135d01O = new FixedLengthStringData(99).isAPartOf(r5135D01, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(r5135d01O, 0);
	private FixedLengthStringData rd01Life = new FixedLengthStringData(2).isAPartOf(r5135d01O, 8);
	private FixedLengthStringData rd01Coverage = new FixedLengthStringData(2).isAPartOf(r5135d01O, 10);
	private FixedLengthStringData rd01Rider = new FixedLengthStringData(2).isAPartOf(r5135d01O, 12);
	private ZonedDecimalData rd01Plnsfx = new ZonedDecimalData(4, 0).isAPartOf(r5135d01O, 14);
	private FixedLengthStringData rd01Crtable = new FixedLengthStringData(4).isAPartOf(r5135d01O, 18);
	private ZonedDecimalData rd01Oldsum = new ZonedDecimalData(15, 0).isAPartOf(r5135d01O, 22);
	private ZonedDecimalData rd01Newsumi = new ZonedDecimalData(15, 0).isAPartOf(r5135d01O, 37);
	private ZonedDecimalData rd01Oldinst = new ZonedDecimalData(17, 2).isAPartOf(r5135d01O, 52);
	private ZonedDecimalData rd01Newinst = new ZonedDecimalData(17, 2).isAPartOf(r5135d01O, 69);
	private FixedLengthStringData rd01Rcesdte = new FixedLengthStringData(10).isAPartOf(r5135d01O, 86);
	private FixedLengthStringData rd01Currency = new FixedLengthStringData(3).isAPartOf(r5135d01O, 96);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Auto Increase Reporting Logical View*/
//	private AinrTableDAM ainrIO = new AinrTableDAM();
		/*AUTOMATIC INCREASE REPORTING*/
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();

	private int ct01Value = 0;
	private int ct02Value = 0;
	private AinrpfDAO ainrpfDAO = getApplicationContext().getBean("ainrpfDAO", AinrpfDAO.class);
	private Iterator<Ainrpf> iteratorList; 
    private int intBatchID = 0;
    private int intBatchExtractSize;
    private Ainrpf ainrIO = null;
	public B5135() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingCompany1020();
		setUpHeadingBranch1030();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		wsaaCompany.set(bsprIO.getCompany());
		wsaaSysparam03.set(bprdIO.getSystemParam03());
		printerFile.openOutput();
		wsaaOverflow.set("Y");
	}

protected void setUpHeadingCompany1020()
	{
		descIO.setRecKeyData(SPACES);
		descIO.setRecNonKeyData(SPACES);
		descIO.setStatuz(varcom.oK);
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(bsprIO.getCompany());
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch1030()
	{
		descIO.setRecKeyData(SPACES);
		descIO.setRecNonKeyData(SPACES);
		descIO.setStatuz(varcom.oK);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		descIO.setDescitem(bprdIO.getDefaultBranch());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Branch.set(bprdIO.getDefaultBranch());
		rh01Branchnm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		rh01Sdate.set(datcon1rec.extDate);
        if (bprdIO.systemParam01.isNumeric()) {
            if (bprdIO.systemParam01.toInt() > 0) {
                intBatchExtractSize = bprdIO.systemParam01.toInt();
            } else {
                intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
	}
        } else {
            intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
        }
        List<Ainrpf> ainrpfList = ainrpfDAO.searchAinrpfRecord(wsaaCompany.toString(), wsaaSysparam03.toString(),
                intBatchExtractSize, intBatchID);
        iteratorList = ainrpfList.iterator();
    }

protected void readFile2000()
	{
        if (!iteratorList.hasNext()) {
            intBatchID++;
            List<Ainrpf> ainrpfList = ainrpfDAO.searchAinrpfRecord(wsaaCompany.toString(), wsaaSysparam03.toString(),
                    intBatchExtractSize, intBatchID);
            iteratorList = ainrpfList.iterator();
            if (!iteratorList.hasNext()) {
                wsspEdterror.set(varcom.endp);
                return;
		}
		}
        ainrIO = iteratorList.next();
        wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		if (newPageReq.isTrue()) {
            ct02Value++;
			printerFile.printR5135h01(r5135H01, indicArea);
			notNewPageReq.setTrue();
		}
		rd01Chdrnum.set(ainrIO.getChdrnum());
		rd01Life.set(ainrIO.getLife());
		rd01Coverage.set(ainrIO.getCoverage());
		rd01Rider.set(ainrIO.getRider());
		rd01Plnsfx.set(ainrIO.getPlanSuffix());
		rd01Crtable.set(ainrIO.getCrtable());
		rd01Oldsum.set(ainrIO.getRd01Oldsum());
		rd01Newsumi.set(ainrIO.getNewsumi());
		rd01Oldinst.set(ainrIO.getOldinst());
		rd01Newinst.set(ainrIO.getNewinst());
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.intDate.set(ainrIO.getRiskCessDate());
		datcon1rec.function.set("CONV");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.statuz.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		rd01Rcesdte.set(datcon1rec.extDate);
		rd01Currency.set(ainrIO.getCurrency());
		printerFile.printR5135d01(r5135D01, indicArea);
        ct01Value++;
	}

protected void commit3500()
	{
        contotrec.totno.set(ct01);
        contotrec.totval.set(ct01Value);
        callContot001();
        ct01Value = 0;
        contotrec.totno.set(ct02);
        contotrec.totval.set(ct02Value);
        callContot001();
        ct02Value = 0;
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
