package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:21
 * Description:
 * Copybook name: CHDROLDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdroldkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdroldFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdroldKey = new FixedLengthStringData(64).isAPartOf(chdroldFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdroldChdrcoy = new FixedLengthStringData(1).isAPartOf(chdroldKey, 0);
  	public FixedLengthStringData chdroldChdrnum = new FixedLengthStringData(8).isAPartOf(chdroldKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(chdroldKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdroldFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdroldFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}