package com.csc.life.regularprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6626screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 17, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6626ScreenVars sv = (S6626ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6626screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6626ScreenVars screenVars = (S6626ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.transcd01.setClassString("");
		screenVars.transcd06.setClassString("");
		screenVars.transcd07.setClassString("");
		screenVars.transcd08.setClassString("");
		screenVars.transcd09.setClassString("");
		screenVars.transcd10.setClassString("");
		screenVars.transcd02.setClassString("");
		screenVars.transcd03.setClassString("");
		screenVars.transcd04.setClassString("");
		screenVars.transcd05.setClassString("");
		screenVars.trcode.setClassString("");
		screenVars.trncd01.setClassString("");
		screenVars.trncd02.setClassString("");
		screenVars.trncd03.setClassString("");
		screenVars.trncd04.setClassString("");
		screenVars.trncd05.setClassString("");
		screenVars.trncd06.setClassString("");
		screenVars.trncd07.setClassString("");
		screenVars.trncd08.setClassString("");
		screenVars.trncd09.setClassString("");
		screenVars.trncd10.setClassString("");
	}

/**
 * Clear all the variables in S6626screen
 */
	public static void clear(VarModel pv) {
		S6626ScreenVars screenVars = (S6626ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.transcd01.clear();
		screenVars.transcd06.clear();
		screenVars.transcd07.clear();
		screenVars.transcd08.clear();
		screenVars.transcd09.clear();
		screenVars.transcd10.clear();
		screenVars.transcd02.clear();
		screenVars.transcd03.clear();
		screenVars.transcd04.clear();
		screenVars.transcd05.clear();
		screenVars.trcode.clear();
		screenVars.trncd01.clear();
		screenVars.trncd02.clear();
		screenVars.trncd03.clear();
		screenVars.trncd04.clear();
		screenVars.trncd05.clear();
		screenVars.trncd06.clear();
		screenVars.trncd07.clear();
		screenVars.trncd08.clear();
		screenVars.trncd09.clear();
		screenVars.trncd10.clear();
	}
}
