package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:58
 * Description:
 * Copybook name: INCRHSTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Incrhstkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData incrhstFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData incrhstKey = new FixedLengthStringData(64).isAPartOf(incrhstFileKey, 0, REDEFINE);
  	public FixedLengthStringData incrhstChdrcoy = new FixedLengthStringData(1).isAPartOf(incrhstKey, 0);
  	public FixedLengthStringData incrhstChdrnum = new FixedLengthStringData(8).isAPartOf(incrhstKey, 1);
  	public FixedLengthStringData incrhstLife = new FixedLengthStringData(2).isAPartOf(incrhstKey, 9);
  	public FixedLengthStringData incrhstCoverage = new FixedLengthStringData(2).isAPartOf(incrhstKey, 11);
  	public FixedLengthStringData incrhstRider = new FixedLengthStringData(2).isAPartOf(incrhstKey, 13);
  	public PackedDecimalData incrhstPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(incrhstKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(incrhstKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(incrhstFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		incrhstFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}