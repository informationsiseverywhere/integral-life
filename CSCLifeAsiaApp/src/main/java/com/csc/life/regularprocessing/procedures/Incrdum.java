/*
 * File: Incrdum.java
 * Date: 29 August 2009 22:56:58
 * Author: Quipoz Limited
 * 
 * Class transformed from INCRDUM.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import com.csc.life.regularprocessing.recordstructures.Incrsrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*                  DUMMY AUTO INCR SUBROUTINE FOR WOP
*
* This subroutine has been created specifically for WOP component.
* There is change on the value given.
*
*****************************************************************
* </pre>
*/
public class Incrdum extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "INCRDUM";
	private Incrsrec incrsrec = new Incrsrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	public Incrdum() {
		super();
	}

public void mainline(Object... parmArray)
	{
		incrsrec.increaseRec = convertAndSetParam(incrsrec.increaseRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		incrsrec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}
}
