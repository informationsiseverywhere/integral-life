package com.csc.life.regularprocessing.dataaccess.model;

public class Payzpf {
	private String chdrcoy;
	private String chdrnum;
	private int payrseqno;
	private int btdate;
	private int ptdate;
	private String billchnl;
	private String znfopt; 
	private long uniqueNumber;

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public int getPayrseqno() {
		return payrseqno;
	}

	public void setPayrseqno(int payrseqno) {
		this.payrseqno = payrseqno;
	}

	public int getBtdate() {
		return btdate;
	}

	public void setBtdate(int btdate) {
		this.btdate = btdate;
	}

	public int getPtdate() {
		return ptdate;
	}

	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}

	public String getBillchnl() {
		return billchnl;
	}

	public void setBillchnl(String billchnl) {
		this.billchnl = billchnl;
	}
	
	public String getZnfopt() {
		return znfopt;
	}

	public void setZnfopt(String znfopt) {
		this.znfopt = znfopt;
	}

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

}
