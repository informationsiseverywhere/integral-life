package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:01
 * Description:
 * Copybook name: AGCMRNLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agcmrnlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agcmrnlFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData agcmrnlKey = new FixedLengthStringData(256).isAPartOf(agcmrnlFileKey, 0, REDEFINE);
  	public FixedLengthStringData agcmrnlChdrcoy = new FixedLengthStringData(1).isAPartOf(agcmrnlKey, 0);
  	public FixedLengthStringData agcmrnlChdrnum = new FixedLengthStringData(8).isAPartOf(agcmrnlKey, 1);
  	public FixedLengthStringData agcmrnlAgntnum = new FixedLengthStringData(8).isAPartOf(agcmrnlKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(239).isAPartOf(agcmrnlKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agcmrnlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agcmrnlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}