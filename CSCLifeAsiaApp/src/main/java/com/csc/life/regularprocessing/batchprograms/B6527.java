/*
 * File: B6527.java
 * Date: 29 August 2009 21:22:29
 * Author: Quipoz Limited
 * 
 * Class transformed from B6527.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.regularprocessing.dataaccess.CovrbblTableDAM;
import com.csc.life.regularprocessing.dataaccess.CovxpfTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.regularprocessing.tablestructures.T5534rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Contot;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* This program is the splitter program for Benefit Billing.
*
* The efficiency of this program relies on its ability to find
* potential transactions quickly.  It  does this by having its
* own logical file over COVR. This logical, as supplied in the
* base system, may not be ideal for your site.  The logical is
* called COVRBBL. Use of Interactive SQL when Debug  is active
* will  identify if the COVRBBL access path is being used.  If
* not, the COVRBBL logical file should be customised  until it
* is being used. This program is the only program  which  uses
* the COVRBBL logical, not by calling the IO module  directly,
* but  by  utilising  the  access  path which the logical file
* uses.
*
* Batch System Parameters:
*
*    Param 1: Transaction Code to look up T5679. (4 Chars)
*    Param 4: Run Id for temporary file name.   (2 Chars)
*
* Control Totals:
*
*    01  Number of Threads used.
*    02  Number of records returned from SQL Select.
*    03  Number of rejections: Invalid T5679 Statuses.
*    04  Number of rejections: T5687/T5534 Editing.
*    05  Number of COVX records written.
*
* Notes of coding changes for this splitter program.
* -------------------------------------------------
*
* A Splitter Program's purpose is to find pending transactions
* from the database and  create  multiple  extract  files  for
* processing by multiple  copies  of  the  subsequent program,
* each running in its own  thread.  Each  subsequent  program,
* therefore, processes a discrete  portion of the total trans-
* actions.
*
* Splitter programs should be simple. They  should  only  deal
* with a single file. If there  is insufficient  data  on  the
* primary file to completely identify  a transaction, then the
* further editing should be done  in  the  subsequent process.
* The objective of a Splitter is  to rapidly isolate potential
* transactions, not to process the  transaction.  The  primary
* concern for Splitter program  design  is  elapsed time speed
* and this is achieved by minimising disk IO.
*
* Before the Splitter process is invoked, the program CRTTMPF,
* Create Temporary File, should be run.   CRTTMPF will  create
* a duplicate of a physical  file  (created  under  Smart  and
* defined as a Query file) which will have as many members  as
* are defined in that process's   'subsequent threads'  field.
* The temporary file will have the name XXXXDD9999, where XXXX
* is  the   first  four  characters  of  the  file  which  was
* duplicated, DD is the first two characters of the 4th system
* parameter and 9999 is the schedule run number.  Each  member
* added  to  the temporary file will be called THREAD999 where
* 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read  records  from  a primary file
* and, for each record selected, will  write  a  record  to  a
* temporary file member. Exactly which  temporary  file member
* is written to is decided by a  working  storage  count.  The
* objective of the count is to spread  the transaction records
* evenly amongst each thread member.
*
* The Splitter program should always have  a restart method of
* '1' indicating a re-run. In the  event  of  a  restart,  the
* Splitter program will always start with empty file  members.
* Apart from writing records to the  temporary  file  members,
* Splitter programs should not be doing any further updates.
*
*****************************************************************
* </pre>
*/
public class B6527 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlcovrpf1rs = null;
	private java.sql.PreparedStatement sqlcovrpf1ps = null;
	private java.sql.Connection sqlcovrpf1conn = null;
	private DiskFileDAM covx01 = new DiskFileDAM("COVX01");
	private DiskFileDAM covx02 = new DiskFileDAM("COVX02");
	private DiskFileDAM covx03 = new DiskFileDAM("COVX03");
	private DiskFileDAM covx04 = new DiskFileDAM("COVX04");
	private DiskFileDAM covx05 = new DiskFileDAM("COVX05");
	private DiskFileDAM covx06 = new DiskFileDAM("COVX06");
	private DiskFileDAM covx07 = new DiskFileDAM("COVX07");
	private DiskFileDAM covx08 = new DiskFileDAM("COVX08");
	private DiskFileDAM covx09 = new DiskFileDAM("COVX09");
	private DiskFileDAM covx10 = new DiskFileDAM("COVX10");
	private DiskFileDAM covx11 = new DiskFileDAM("COVX11");
	private DiskFileDAM covx12 = new DiskFileDAM("COVX12");
	private DiskFileDAM covx13 = new DiskFileDAM("COVX13");
	private DiskFileDAM covx14 = new DiskFileDAM("COVX14");
	private DiskFileDAM covx15 = new DiskFileDAM("COVX15");
	private DiskFileDAM covx16 = new DiskFileDAM("COVX16");
	private DiskFileDAM covx17 = new DiskFileDAM("COVX17");
	private DiskFileDAM covx18 = new DiskFileDAM("COVX18");
	private DiskFileDAM covx19 = new DiskFileDAM("COVX19");
	private DiskFileDAM covx20 = new DiskFileDAM("COVX20");
	private CovxpfTableDAM covxpfData = new CovxpfTableDAM();
		/* Change the record length to that of the temporary file. This
		 can be found by doing a DSPFD of the file being duplicated
		 by the CRTTMPF process.*/
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B6527");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaCovxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaCovxFn, 0, FILLER).init("COVX");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaCovxFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaCovxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThread = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThread, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThread, 6).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaThread, 9, FILLER).init(SPACES);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private PackedDecimalData iw = new PackedDecimalData(5, 0);
	private PackedDecimalData iy = new PackedDecimalData(5, 0).init(1);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaa1 = new FixedLengthStringData(1).init("1");
	private PackedDecimalData wsaaEffectiveDate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8);
	private FixedLengthStringData lastChdrnum = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1);
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");

	private FixedLengthStringData wsaaGotOne = new FixedLengthStringData(1);
	private Validator gotOne = new Validator(wsaaGotOne, "Y");
	private static final int wsaaMaxIndex = 1000;

		/* WSAA-T5687-T5534-TABLE 
		   03  WSAA-T5687-T5534        OCCURS 256.                      */
	private FixedLengthStringData[] wsaaT5687T5534 = FLSInittedArray (1000, 41);
	private FixedLengthStringData[] wsaaCrtable = FLSDArrayPartOfArrayStructure(4, wsaaT5687T5534, 0, HIVALUE);
	private PackedDecimalData[] wsaaCrrcd = PDArrayPartOfArrayStructure(8, 0, wsaaT5687T5534, 4);
	private FixedLengthStringData[] wsaaBbmeth = FLSDArrayPartOfArrayStructure(4, wsaaT5687T5534, 9);
	private FixedLengthStringData[] wsaaAdfeemth = FLSDArrayPartOfArrayStructure(4, wsaaT5687T5534, 13);
	private FixedLengthStringData[] wsaaJlPremMeth = FLSDArrayPartOfArrayStructure(4, wsaaT5687T5534, 17);
	private FixedLengthStringData[] wsaaPremmeth = FLSDArrayPartOfArrayStructure(4, wsaaT5687T5534, 21);
	private FixedLengthStringData[] wsaaSubprog = FLSDArrayPartOfArrayStructure(10, wsaaT5687T5534, 25);
	private FixedLengthStringData[] wsaaSvMethod = FLSDArrayPartOfArrayStructure(4, wsaaT5687T5534, 35);
	private FixedLengthStringData[] wsaaUnitFreq = FLSDArrayPartOfArrayStructure(2, wsaaT5687T5534, 39);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler4 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler4, 5);
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String h791 = "H791";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
		/* TABLES */
	private static final String t5534 = "T5534";
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private P6671par p6671par = new P6671par();
	private T5534rec t5534rec = new T5534rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private SqlCovrpfInner sqlCovrpfInner = new SqlCovrpfInner();
	
	private int ix;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Map<String, List<Itempf>> t5687Map = null;
	private Map<String, List<Itempf>> t5534Map = null;
	private int ct02Value = 0;
	private int ct03Value = 0;
	private int ct04Value = 0;
	private int ct05Value = 0;
	private static final String  FMC_FEATURE_ID = "BTPRO026";
	private boolean fmcOnFlag = false;
	private static final String FMC = "FMC";
	private ZonedDecimalData wsaaNextdate = new ZonedDecimalData(8, 0).init(0);
	private Datcon4rec datcon4rec = new Datcon4rec();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		eof2080, 
		exit2090, 
		exit2590
	}

	public B6527() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		syserrrec.syserrStatuz.set(sqlStatuz);
		fatalError600();
	}

protected void restart0900()
	{
		/*RESTART*/
		/** This section is invoked in restart mode. Note section 1100 must*/
		/** clear each temporary file member as the members are not under*/
		/** commitment control. Note also that Splitter programs must have*/
		/** a Restart Method of 1 (re-run). Thus no updating should occur*/
		/** which would result in different records being returned from the*/
		/** primary file in a restart!*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*    Check the restart method is compatible with the program.*/
		/*    This program is restartable but will always re-run from*/
		/*    the beginning. This is because the temporary file members*/
		/*    are not under commitment control.*/
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*    The next thing we must do is construct the name of the*/
		/*    temporary file which we will be working with.*/
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		/*    Do a ovrdbf for each temporary file member. (Note we have*/
		/*    allowed for a maximum of 20.)*/
		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		/*    Log the number of threads being used*/
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		/*    Set up the details for the SQL select.*/
		getT56791300();
		wsaaCompany.set(bprdIO.getCompany());
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES)
		&& isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		fmcOnFlag = FeaConfg.isFeatureExist(wsaaCompany.toString().trim(), FMC_FEATURE_ID, appVars, "IT");
		if(fmcOnFlag && isNE(bprdIO.getSystemParam05(),FMC)){
			return;	//IBPLIFE-1380
		}
		if(fmcOnFlag && isEQ(bprdIO.getSystemParam05(),FMC)){	
			datcon4rec.frequency.set("12");
			datcon4rec.freqFactor.set(1);
			datcon4rec.intDate1.set(bsscIO.getEffectiveDate());
			datcon4rec.intDate2.set(0);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			wsaaNextdate.set(datcon4rec.intDate2.toInt());
		}
		StringBuilder sqlcovrpf1 = new StringBuilder(" SELECT  C.CHDRCOY, C.CHDRNUM, C.LIFE, C.COVERAGE, C.RIDER, C.PLNSFX, C.JLIFE, C.PRMCUR, C.BBLDAT, C.CRRCD, C.PCESDTE, C.MORTCLS, C.CRTABLE, C.STATCODE, C.PSTATCODE, C.SUMINS FROM "); 
		sqlcovrpf1.append(getAppVars().getTableNameOverriden("COVRPF"));
		sqlcovrpf1.append(" C ");
		if(fmcOnFlag && isEQ(bprdIO.getSystemParam05(),FMC)){
			sqlcovrpf1.append(" INNER JOIN ");
			sqlcovrpf1.append(getAppVars().getTableNameOverriden("ZFMCPF Z ON Z.CHDRNUM=C.CHDRNUM INNER JOIN PAYRPF P ON Z.CHDRNUM=P.CHDRNUM"));
		}
		sqlcovrpf1.append(" WHERE C.CHDRCOY = ?");
		if(!(fmcOnFlag && isEQ(bprdIO.getSystemParam05(),FMC))){
			sqlcovrpf1.append(" AND C.BCESDTE > C.BBLDAT");
		}
		sqlcovrpf1.append(" AND C.BBLDAT <> 0");
		sqlcovrpf1.append(" AND C.VALIDFLAG = ?");
		sqlcovrpf1.append(" AND C.BBLDAT <= ?");		
		sqlcovrpf1.append(" AND C.CHDRNUM BETWEEN ? AND ? ");
		if(fmcOnFlag && isEQ(bprdIO.getSystemParam05(),FMC)){
			sqlcovrpf1.append(" AND Z.CHDRCOY = ? AND Z.ZFMCDAT <> 0 AND Z.VALIDFLAG = ? AND Z.ZFMCDAT <= ? AND P.BTDATE=P.PTDATE AND P.VALIDFLAG = ? ");
		}
		sqlcovrpf1.append(" ORDER BY C.CHDRCOY, C.CHDRNUM");
	
		sqlerrorflag = false;
		try {
			sqlcovrpf1conn = getAppVars().getDBConnectionForTable(new com.csc.life.newbusiness.dataaccess.CovrpfTableDAM());
			sqlcovrpf1ps = getAppVars().prepareStatementEmbeded(sqlcovrpf1conn, sqlcovrpf1.toString(), "COVRPF");
			getAppVars().setDBString(sqlcovrpf1ps, 1, wsaaCompany);
			getAppVars().setDBString(sqlcovrpf1ps, 2, wsaa1);	
			if(fmcOnFlag && isEQ(bprdIO.getSystemParam05(),FMC)){
				getAppVars().setDBNumber(sqlcovrpf1ps, 3, wsaaNextdate);
			}
			else{
				getAppVars().setDBNumber(sqlcovrpf1ps, 3, wsaaEffectiveDate);
			}
			getAppVars().setDBString(sqlcovrpf1ps, 4, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlcovrpf1ps, 5, wsaaChdrnumTo);
			if(fmcOnFlag && isEQ(bprdIO.getSystemParam05(),FMC)){
				getAppVars().setDBString(sqlcovrpf1ps, 6, wsaaCompany);
				getAppVars().setDBString(sqlcovrpf1ps, 7, wsaa1);
				getAppVars().setDBString(sqlcovrpf1ps, 8, wsaaNextdate);
				getAppVars().setDBString(sqlcovrpf1ps, 9, wsaa1);
			}
			
			sqlcovrpf1rs = getAppVars().executeQuery(sqlcovrpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*    Load T5687 and T5534 into working storage for fast access.*/
		String coy = bsprIO.getCompany().toString();
		t5687Map = itemDAO.loadSmartTable("IT", coy, "T5687");
		t5534Map = itemDAO.loadSmartTable("IT", coy, "T5534");
		ix = 1;
		loadT5687T55341200();
	}


protected void openThreadMember1100()
	{
		/*    Clear the member for this thread in case we are in restart*/
		/*    mode in which case it might already contain data.*/
		/* MOVE IZ                     TO WSAA-THREAD-NUMBER.           */
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(), 1), iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaCovxFn, wsaaThread, wsaaStatuz);
		if (isNE(wsaaStatuz, varcom.oK)) {
			syserrrec.syserrType.set("2");
			syserrrec.subrname.set("CLRTMPF");
			syserrrec.statuz.set(wsaaStatuz);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(COVX");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaCovxFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThread);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*    Open the file.*/
		if (isEQ(iz, 1)) {
			covx01.openOutput();
		}
		if (isEQ(iz, 2)) {
			covx02.openOutput();
		}
		if (isEQ(iz, 3)) {
			covx03.openOutput();
		}
		if (isEQ(iz, 4)) {
			covx04.openOutput();
		}
		if (isEQ(iz, 5)) {
			covx05.openOutput();
		}
		if (isEQ(iz, 6)) {
			covx06.openOutput();
		}
		if (isEQ(iz, 7)) {
			covx07.openOutput();
		}
		if (isEQ(iz, 8)) {
			covx08.openOutput();
		}
		if (isEQ(iz, 9)) {
			covx09.openOutput();
		}
		if (isEQ(iz, 10)) {
			covx10.openOutput();
		}
		if (isEQ(iz, 11)) {
			covx11.openOutput();
		}
		if (isEQ(iz, 12)) {
			covx12.openOutput();
		}
		if (isEQ(iz, 13)) {
			covx13.openOutput();
		}
		if (isEQ(iz, 14)) {
			covx14.openOutput();
		}
		if (isEQ(iz, 15)) {
			covx15.openOutput();
		}
		if (isEQ(iz, 16)) {
			covx16.openOutput();
		}
		if (isEQ(iz, 17)) {
			covx17.openOutput();
		}
		if (isEQ(iz, 18)) {
			covx18.openOutput();
		}
		if (isEQ(iz, 19)) {
			covx19.openOutput();
		}
		if (isEQ(iz, 20)) {
			covx20.openOutput();
		}

	}


protected void loadT5687T55341200()
	{
		/*    Firstly get the T5687 dated item.*/
		/*    Get the T5534 record and see whether there is a*/
		/*    generic subroutine. If so we load the details from both*/
		/*    T5687 and T5534 into the table for fast search access.*/
		if (t5687Map != null && !t5687Map.isEmpty()) {
			for (List<Itempf> t5687items : t5687Map.values()) {
				for (Itempf i : t5687items) {
					t5687rec.t5687Rec.set(StringUtil.rawToString(i.getGenarea()));
					if (t5534Map != null && t5534Map.containsKey(t5687rec.bbmeth)) {
						List<Itempf> itempfList = t5534Map.get(t5687rec.bbmeth);
						t5534rec.t5534Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
						/* IF IX > 256 */
						if (isGT(ix, wsaaMaxIndex)) {
							syserrrec.statuz.set(h791);
							syserrrec.params.set("T5687 & T5534");
							fatalError600();
						}
						wsaaCrtable[ix].set(i.getItemitem());
						wsaaCrrcd[ix].set(i.getItmfrm());
						wsaaAdfeemth[ix].set(t5534rec.adfeemth);
						wsaaJlPremMeth[ix].set(t5534rec.jlPremMeth);
						wsaaPremmeth[ix].set(t5534rec.premmeth);
						wsaaSubprog[ix].set(t5534rec.subprog);
						wsaaSvMethod[ix].set(t5534rec.svMethod);
						wsaaUnitFreq[ix].set(t5534rec.unitFreq);
						ix++;
					} else {
						t5534rec.t5534Rec.set(SPACES);
					}
				}
			}
		}
	}

protected void getT56791300()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getSystemParam01());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFiles2010();
				case eof2080: 
					eof2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFiles2010()
	{
		sqlerrorflag = false;
		try {
			if (sqlcovrpf1rs!=null && getAppVars().fetchNext(sqlcovrpf1rs)) {	//IBPLIFE-1380
				getAppVars().getDBObject(sqlcovrpf1rs, 1, sqlCovrpfInner.sqlChdrcoy);
				getAppVars().getDBObject(sqlcovrpf1rs, 2, sqlCovrpfInner.sqlChdrnum);
				getAppVars().getDBObject(sqlcovrpf1rs, 3, sqlCovrpfInner.sqlLife);
				getAppVars().getDBObject(sqlcovrpf1rs, 4, sqlCovrpfInner.sqlCoverage);
				getAppVars().getDBObject(sqlcovrpf1rs, 5, sqlCovrpfInner.sqlRider);
				getAppVars().getDBObject(sqlcovrpf1rs, 6, sqlCovrpfInner.sqlPlnsfx);
				getAppVars().getDBObject(sqlcovrpf1rs, 7, sqlCovrpfInner.sqlJlife);
				getAppVars().getDBObject(sqlcovrpf1rs, 8, sqlCovrpfInner.sqlPrmcur);
				getAppVars().getDBObject(sqlcovrpf1rs, 9, sqlCovrpfInner.sqlBbldat);
				getAppVars().getDBObject(sqlcovrpf1rs, 10, sqlCovrpfInner.sqlCrrcd);
				getAppVars().getDBObject(sqlcovrpf1rs, 11, sqlCovrpfInner.sqlPcesdte);
				getAppVars().getDBObject(sqlcovrpf1rs, 12, sqlCovrpfInner.sqlMortcls);
				getAppVars().getDBObject(sqlcovrpf1rs, 13, sqlCovrpfInner.sqlCrtable);
				getAppVars().getDBObject(sqlcovrpf1rs, 14, sqlCovrpfInner.sqlStatcode);
				getAppVars().getDBObject(sqlcovrpf1rs, 15, sqlCovrpfInner.sqlPstatcode);
				getAppVars().getDBObject(sqlcovrpf1rs, 16, sqlCovrpfInner.sqlSumins);
			}
			else {
				goTo(GotoLabel.eof2080);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		ct02Value++;
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		try {
			statcodeChecks2520();
			t5687T5534Checks2530();
			gotOne2580();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void statcodeChecks2520()
	{
		/*    If we have a coverage, check we have the correct status*/
		wsaaValidCoverage.set("N");
		if (isNE(sqlCovrpfInner.sqlCoverage, SPACES)
		&& isEQ(sqlCovrpfInner.sqlRider, SPACES)
		|| isEQ(sqlCovrpfInner.sqlRider, "00")) {
			for (ix = 1; !(isGT(ix, 12)); ix++){
				if (isEQ(t5679rec.covRiskStat[ix], sqlCovrpfInner.sqlStatcode)) {
					ix = 13;
					wsaaValidCoverage.set("Y");
				}
			}
		}
		/*    If we have a rider, check we have the correct status*/
		if (isNE(sqlCovrpfInner.sqlCoverage, SPACES)
		&& isNE(sqlCovrpfInner.sqlRider, SPACES)
		|| isNE(sqlCovrpfInner.sqlRider, "00")) {
			for (ix = 1; !(isGT(ix, 12)); ix++){
				if (isEQ(t5679rec.ridRiskStat[ix], sqlCovrpfInner.sqlStatcode)) {
					ix = 13;
					wsaaValidCoverage.set("Y");
				}
			}
		}
		/*    If we do not have a valid coverage or rider, skip to*/
		/*    the next record.*/
		if (!validCoverage.isTrue()) {
			ct03Value++;
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit2590);
		}
	}

protected void t5687T5534Checks2530()
	{
		wsaaGotOne.set("N");
		/* PERFORM VARYING IX FROM 1 BY 1 UNTIL IX > 256                */
		for (ix = 1; !(isGT(ix, wsaaMaxIndex)
		|| isEQ(wsaaCrtable[ix], HIVALUE)
		|| gotOne.isTrue()); ix++){
			if (isEQ(wsaaCrtable[ix], sqlCovrpfInner.sqlCrtable)
			&& isLTE(wsaaCrrcd[ix], sqlCovrpfInner.sqlCrrcd)) {
				if (isNE(wsaaSubprog[ix], SPACES)) {
					iw.set(ix);
					wsaaGotOne.set("Y");
				}
				else {
					/*    MOVE 256        TO IX                            */
					ix = wsaaMaxIndex;
				}
			}
		}
		if (!gotOne.isTrue()) {
			ct04Value++;
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit2590);
		}
	}

protected void gotOne2580()
	{
		wsspEdterror.set(varcom.oK);
	}

protected void update3000()
	{
		covxpfData.chdrcoy.set(sqlCovrpfInner.sqlChdrcoy);
		covxpfData.chdrnum.set(sqlCovrpfInner.sqlChdrnum);
		covxpfData.life.set(sqlCovrpfInner.sqlLife);
		covxpfData.coverage.set(sqlCovrpfInner.sqlCoverage);
		covxpfData.rider.set(sqlCovrpfInner.sqlRider);
		covxpfData.planSuffix.set(sqlCovrpfInner.sqlPlnsfx);
		covxpfData.jlife.set(sqlCovrpfInner.sqlJlife);
		covxpfData.premCurrency.set(sqlCovrpfInner.sqlPrmcur);
		covxpfData.benBillDate.set(sqlCovrpfInner.sqlBbldat);
		covxpfData.sumins.set(sqlCovrpfInner.sqlSumins);
		covxpfData.premCessDate.set(sqlCovrpfInner.sqlPcesdte);
		covxpfData.crtable.set(sqlCovrpfInner.sqlCrtable);
		covxpfData.mortcls.set(sqlCovrpfInner.sqlMortcls);
		covxpfData.subprog.set(wsaaSubprog[iw.toInt()]);
		covxpfData.unitFreq.set(wsaaUnitFreq[iw.toInt()]);
		covxpfData.premmeth.set(wsaaPremmeth[iw.toInt()]);
		covxpfData.jlPremMeth.set(wsaaJlPremMeth[iw.toInt()]);
		covxpfData.svMethod.set(wsaaSvMethod[iw.toInt()]);
		covxpfData.adfeemth.set(wsaaAdfeemth[iw.toInt()]);
		if (isEQ(sqlCovrpfInner.sqlChdrnum, lastChdrnum)) {
			/*NEXT_SENTENCE*/
		}
		else {
			lastChdrnum.set(sqlCovrpfInner.sqlChdrnum);
			iy.add(1);
			if (isGT(iy, bprdIO.getThreadsSubsqntProc())) {
				iy.set(1);
			}
		}
		if (isEQ(iy, 1)) {
			covx01.write(covxpfData);
		}
		if (isEQ(iy, 2)) {
			covx02.write(covxpfData);
		}
		if (isEQ(iy, 3)) {
			covx03.write(covxpfData);
		}
		if (isEQ(iy, 4)) {
			covx04.write(covxpfData);
		}
		if (isEQ(iy, 5)) {
			covx05.write(covxpfData);
		}
		if (isEQ(iy, 6)) {
			covx06.write(covxpfData);
		}
		if (isEQ(iy, 7)) {
			covx07.write(covxpfData);
		}
		if (isEQ(iy, 8)) {
			covx08.write(covxpfData);
		}
		if (isEQ(iy, 9)) {
			covx09.write(covxpfData);
		}
		if (isEQ(iy, 10)) {
			covx10.write(covxpfData);
		}
		if (isEQ(iy, 11)) {
			covx11.write(covxpfData);
		}
		if (isEQ(iy, 12)) {
			covx12.write(covxpfData);
		}
		if (isEQ(iy, 13)) {
			covx13.write(covxpfData);
		}
		if (isEQ(iy, 14)) {
			covx14.write(covxpfData);
		}
		if (isEQ(iy, 15)) {
			covx15.write(covxpfData);
		}
		if (isEQ(iy, 16)) {
			covx16.write(covxpfData);
		}
		if (isEQ(iy, 17)) {
			covx17.write(covxpfData);
		}
		if (isEQ(iy, 18)) {
			covx18.write(covxpfData);
		}
		if (isEQ(iy, 19)) {
			covx19.write(covxpfData);
		}
		if (isEQ(iy, 20)) {
			covx20.write(covxpfData);
		}
		/*    Log the number of extacted records.*/
		ct05Value++;
	}


protected void commit3500()
	{
		contotrec.totno.set(ct02);
		contotrec.totval.set(ct02Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct02Value = 0;
		contotrec.totno.set(ct03);
		contotrec.totval.set(ct03Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct03Value = 0;
		contotrec.totno.set(ct04);
		contotrec.totval.set(ct04Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct04Value = 0;
		contotrec.totno.set(ct05);
		contotrec.totval.set(ct05Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct05Value = 0;
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close the open files and remove the overrides.*/
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		if (isEQ(iz, 1)) {
			covx01.close();
		}
		if (isEQ(iz, 2)) {
			covx02.close();
		}
		if (isEQ(iz, 3)) {
			covx03.close();
		}
		if (isEQ(iz, 4)) {
			covx04.close();
		}
		if (isEQ(iz, 5)) {
			covx05.close();
		}
		if (isEQ(iz, 6)) {
			covx06.close();
		}
		if (isEQ(iz, 7)) {
			covx07.close();
		}
		if (isEQ(iz, 8)) {
			covx08.close();
		}
		if (isEQ(iz, 9)) {
			covx09.close();
		}
		if (isEQ(iz, 10)) {
			covx10.close();
		}
		if (isEQ(iz, 11)) {
			covx11.close();
		}
		if (isEQ(iz, 12)) {
			covx12.close();
		}
		if (isEQ(iz, 13)) {
			covx13.close();
		}
		if (isEQ(iz, 14)) {
			covx14.close();
		}
		if (isEQ(iz, 15)) {
			covx15.close();
		}
		if (isEQ(iz, 16)) {
			covx16.close();
		}
		if (isEQ(iz, 17)) {
			covx17.close();
		}
		if (isEQ(iz, 18)) {
			covx18.close();
		}
		if (isEQ(iz, 19)) {
			covx19.close();
		}
		if (isEQ(iz, 20)) {
			covx20.close();
		}

	}

/*
 * Class transformed  from Data Structure SQL-COVRPF--INNER
 */
private static final class SqlCovrpfInner { 

	private FixedLengthStringData sqlCovrpf = new FixedLengthStringData(56);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlCovrpf, 0);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlCovrpf, 1);
	private FixedLengthStringData sqlLife = new FixedLengthStringData(2).isAPartOf(sqlCovrpf, 9);
	private FixedLengthStringData sqlCoverage = new FixedLengthStringData(2).isAPartOf(sqlCovrpf, 11);
	private FixedLengthStringData sqlRider = new FixedLengthStringData(2).isAPartOf(sqlCovrpf, 13);
	private PackedDecimalData sqlPlnsfx = new PackedDecimalData(4, 0).isAPartOf(sqlCovrpf, 15);
	private FixedLengthStringData sqlJlife = new FixedLengthStringData(2).isAPartOf(sqlCovrpf, 18);
	private FixedLengthStringData sqlPrmcur = new FixedLengthStringData(3).isAPartOf(sqlCovrpf, 20);
	private PackedDecimalData sqlBbldat = new PackedDecimalData(8, 0).isAPartOf(sqlCovrpf, 23);
	private PackedDecimalData sqlCrrcd = new PackedDecimalData(8, 0).isAPartOf(sqlCovrpf, 28);
	private PackedDecimalData sqlPcesdte = new PackedDecimalData(8, 0).isAPartOf(sqlCovrpf, 33);
	private FixedLengthStringData sqlMortcls = new FixedLengthStringData(1).isAPartOf(sqlCovrpf, 38);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlCovrpf, 39);
	private FixedLengthStringData sqlStatcode = new FixedLengthStringData(2).isAPartOf(sqlCovrpf, 43);
	private FixedLengthStringData sqlPstatcode = new FixedLengthStringData(2).isAPartOf(sqlCovrpf, 45);
	private PackedDecimalData sqlSumins = new PackedDecimalData(17, 2).isAPartOf(sqlCovrpf, 47);
}
}