package com.csc.life.regularprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR630
 * @version 1.0 generated on 30/08/09 07:22
 * @author Quipoz
 */
public class Sr630ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());  //210
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0); //82
	public FixedLengthStringData activeInd = DD.actind.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData cmax = DD.cmax.copyToZonedDecimal().isAPartOf(dataFields,1);
	public ZonedDecimalData cmin = DD.cmin.copyToZonedDecimal().isAPartOf(dataFields,18);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,35);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,36);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,74);
	public ZonedDecimalData tdayno = DD.tdayno.copyToZonedDecimal().isAPartOf(dataFields,79);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize()); //32
	public FixedLengthStringData actindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cmaxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData tdaynoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());  //96
	public FixedLengthStringData[] actindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cmaxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] tdaynoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr630screenWritten = new LongData(0);
	public LongData Sr630protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr630ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(actindOut,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tdaynoOut,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cminOut,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cmaxOut,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		
		screenFields = getscreenFields();
		screenOutFields =getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr630screen.class;
		protectRecord = Sr630protect.class;
	}

	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {};
	}
	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {};
	}
	public BaseData[] getscreenDateErrFields()
	{
		return new BaseData[] {};
	}

	public int getDataAreaSize() {
		return 210;
	}
	public int getDataFieldsSize(){
		return 82;
	}
	public int getErrorIndicatorSize(){
		return 32 ; 
	}
	public int getOutputFieldSize(){
		return 96; 
	}
	
	public BaseData[] getscreenFields(){
		return new BaseData[] {company, tabl, item, longdesc, activeInd, tdayno, cmin, cmax};
	}
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, actindOut, tdaynoOut, cminOut, cmaxOut};
	}
	
	public BaseData[] getscreenErrFields(){
		return new BaseData[] {companyErr, tablErr, itemErr, longdescErr, actindErr, tdaynoErr, cminErr, cmaxErr};
		
	}
	
}
