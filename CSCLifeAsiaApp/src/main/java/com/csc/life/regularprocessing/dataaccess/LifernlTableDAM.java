package com.csc.life.regularprocessing.dataaccess;

import com.csc.life.newbusiness.dataaccess.LifepfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: LifernlTableDAM.java
 * Date: Sun, 30 Aug 2009 03:42:39
 * Class transformed from LIFERNL.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LifernlTableDAM extends LifepfTableDAM {

	public LifernlTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("LIFERNL");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", JLIFE";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "JLIFE, " +
		            "VALIDFLAG, " +
		            "STATCODE, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "ANBCCD, " +
		            "CLTSEX, " +
		            "LCDTE, " +
		            "LIFCNUM, " +
		            "LIFEREL, " +
		            "AGEADM, " +
		            "SELECTION, " +
		            "SMOKING, " +
		            "OCCUP, " +
		            "PURSUIT01, " +
		            "PURSUIT02, " +
		            "MARTAL, " +
		            "CLTDOB, " +
		            "SBSTDL, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "JLIFE ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "JLIFE DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               jlife,
                               validflag,
                               statcode,
                               currfrom,
                               currto,
                               anbAtCcd,
                               cltsex,
                               lifeCommDate,
                               lifcnum,
                               liferel,
                               ageadm,
                               selection,
                               smoking,
                               occup,
                               pursuit01,
                               pursuit02,
                               maritalState,
                               cltdob,
                               sbstdl,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(51);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getJlife().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(life.toInternal());
	nonKeyFiller4.setInternal(jlife.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(75);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ getValidflag().toInternal()
					+ getStatcode().toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getAnbAtCcd().toInternal()
					+ getCltsex().toInternal()
					+ getLifeCommDate().toInternal()
					+ getLifcnum().toInternal()
					+ getLiferel().toInternal()
					+ getAgeadm().toInternal()
					+ getSelection().toInternal()
					+ getSmoking().toInternal()
					+ getOccup().toInternal()
					+ getPursuit01().toInternal()
					+ getPursuit02().toInternal()
					+ getMaritalState().toInternal()
					+ getCltdob().toInternal()
					+ getSbstdl().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, anbAtCcd);
			what = ExternalData.chop(what, cltsex);
			what = ExternalData.chop(what, lifeCommDate);
			what = ExternalData.chop(what, lifcnum);
			what = ExternalData.chop(what, liferel);
			what = ExternalData.chop(what, ageadm);
			what = ExternalData.chop(what, selection);
			what = ExternalData.chop(what, smoking);
			what = ExternalData.chop(what, occup);
			what = ExternalData.chop(what, pursuit01);
			what = ExternalData.chop(what, pursuit02);
			what = ExternalData.chop(what, maritalState);
			what = ExternalData.chop(what, cltdob);
			what = ExternalData.chop(what, sbstdl);	
			what = ExternalData.chop(what, datime);	
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public PackedDecimalData getAnbAtCcd() {
		return anbAtCcd;
	}
	public void setAnbAtCcd(Object what) {
		setAnbAtCcd(what, false);
	}
	public void setAnbAtCcd(Object what, boolean rounded) {
		if (rounded)
			anbAtCcd.setRounded(what);
		else
			anbAtCcd.set(what);
	}	
	public FixedLengthStringData getCltsex() {
		return cltsex;
	}
	public void setCltsex(Object what) {
		cltsex.set(what);
	}	
	public PackedDecimalData getLifeCommDate() {
		return lifeCommDate;
	}
	public void setLifeCommDate(Object what) {
		setLifeCommDate(what, false);
	}
	public void setLifeCommDate(Object what, boolean rounded) {
		if (rounded)
			lifeCommDate.setRounded(what);
		else
			lifeCommDate.set(what);
	}	
	public FixedLengthStringData getLifcnum() {
		return lifcnum;
	}
	public void setLifcnum(Object what) {
		lifcnum.set(what);
	}	
	public FixedLengthStringData getLiferel() {
		return liferel;
	}
	public void setLiferel(Object what) {
		liferel.set(what);
	}	
	public FixedLengthStringData getAgeadm() {
		return ageadm;
	}
	public void setAgeadm(Object what) {
		ageadm.set(what);
	}	
	public FixedLengthStringData getSelection() {
		return selection;
	}
	public void setSelection(Object what) {
		selection.set(what);
	}	
	public FixedLengthStringData getSmoking() {
		return smoking;
	}
	public void setSmoking(Object what) {
		smoking.set(what);
	}	
	public FixedLengthStringData getOccup() {
		return occup;
	}
	public void setOccup(Object what) {
		occup.set(what);
	}	
	public FixedLengthStringData getPursuit01() {
		return pursuit01;
	}
	public void setPursuit01(Object what) {
		pursuit01.set(what);
	}	
	public FixedLengthStringData getPursuit02() {
		return pursuit02;
	}
	public void setPursuit02(Object what) {
		pursuit02.set(what);
	}	
	public FixedLengthStringData getMaritalState() {
		return maritalState;
	}
	public void setMaritalState(Object what) {
		maritalState.set(what);
	}	
	public PackedDecimalData getCltdob() {
		return cltdob;
	}
	public void setCltdob(Object what) {
		setCltdob(what, false);
	}
	public void setCltdob(Object what, boolean rounded) {
		if (rounded)
			cltdob.setRounded(what);
		else
			cltdob.set(what);
	}	
	public FixedLengthStringData getSbstdl() {
		return sbstdl;
	}
	public void setSbstdl(Object what) {
		sbstdl.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getPursuits() {
		return new FixedLengthStringData(pursuit01.toInternal()
										+ pursuit02.toInternal());
	}
	public void setPursuits(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getPursuits().getLength()).init(obj);
	
		what = ExternalData.chop(what, pursuit01);
		what = ExternalData.chop(what, pursuit02);
	}
	public FixedLengthStringData getPursuit(BaseData indx) {
		return getPursuit(indx.toInt());
	}
	public FixedLengthStringData getPursuit(int indx) {

		switch (indx) {
			case 1 : return pursuit01;
			case 2 : return pursuit02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setPursuit(BaseData indx, Object what) {
		setPursuit(indx.toInt(), what);
	}
	public void setPursuit(int indx, Object what) {

		switch (indx) {
			case 1 : setPursuit01(what);
					 break;
			case 2 : setPursuit02(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}
	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		jlife.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		validflag.clear();
		statcode.clear();
		currfrom.clear();
		currto.clear();
		anbAtCcd.clear();
		cltsex.clear();
		lifeCommDate.clear();
		lifcnum.clear();
		liferel.clear();
		ageadm.clear();
		selection.clear();
		smoking.clear();
		occup.clear();
		pursuit01.clear();
		pursuit02.clear();
		maritalState.clear();
		cltdob.clear();
		sbstdl.clear();		
		datime.clear();
	}


}