package com.csc.life.regularprocessing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:31
 * Description:
 * Copybook name: P6283PAR
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class P6283par extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData parmRecord = new FixedLengthStringData(26);
  	public FixedLengthStringData effdates = new FixedLengthStringData(10).isAPartOf(parmRecord, 0);
  	public ZonedDecimalData jobnofrom = new ZonedDecimalData(8, 0).isAPartOf(parmRecord, 10);
  	public ZonedDecimalData jobnoto = new ZonedDecimalData(8, 0).isAPartOf(parmRecord, 18);


	public void initialize() {
		COBOLFunctions.initialize(parmRecord);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRecord.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}