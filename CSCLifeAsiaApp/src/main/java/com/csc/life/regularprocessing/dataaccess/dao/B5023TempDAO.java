package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.B5023DTO;
import com.csc.life.regularprocessing.dataaccess.model.T5679T5679Rec;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface B5023TempDAO extends BaseDAO<B5023DTO> {
    public void initTempTable(String wsaaParmCompany, String p5023ChdrnumFrm, String p5023ChdrnumTo,
            int wsaaSqlFrmdate, int wsaaSqlTodate, T5679T5679Rec t5679T5679RecInner);

    public List<B5023DTO> searchRacdResult(int min_record, int max_record);
}