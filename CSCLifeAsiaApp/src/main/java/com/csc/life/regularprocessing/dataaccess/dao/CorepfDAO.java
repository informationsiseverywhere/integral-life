package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.Corepf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface CorepfDAO extends BaseDAO<Corepf> {
	List<Corepf> findResults(String tableId, String memName, int batchExtractSize, int batchID);
}
