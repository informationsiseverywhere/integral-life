/*
 * File: B6688.java
 * Date: 29 August 2009 21:24:22
 * Author: Quipoz Limited
 * 
 * Class transformed from B6688.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.dataaccess.dao.AcagpfDAO;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*                    ACAGPF SPLITTER PROGRAM
*                   -------------------------
*
*
* This program is the Splitter program which will split the ACAG
* file containing all pending updates for any deferred Agent
* postings.
*
* All records from ACAGPF will be read via SQL and ordered
* by RLDGCOY, SACSCODE, RLDGACCT, ORIGCURR and SACSTYP.
*
* Control totals used in this program:
*
*    01  -  No. of ACAG extracted records.
*    02  -  No. of thread members.
*
* The temporary file for this splitter program is ACAXPF with
* many ACAX members. Extracted records will be written to as many
* output members as threads specified in the 'No. of subsequent
* threads' parameter on the process definition for this program.
* Each ACAG record is written to a different ACAX member, unless
* there are several ACAG records for the same agent, in which
* case, these are written to the same ACAX member.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class B6688 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;

	//private DiskFileDAM acax01 = new DiskFileDAM("ACAX01");
	
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B6688");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaClrtmpfError = new FixedLengthStringData(97);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/* ERRORS */
	private String ivrm = "IVRM";
	private String esql = "ESQL";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

	private FixedLengthStringData wsaaAcaxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(wsaaAcaxFn, 0, FILLER).init("ACAX");
	private FixedLengthStringData wsaaAcaxRunid = new FixedLengthStringData(2).isAPartOf(wsaaAcaxFn, 4);
	private ZonedDecimalData wsaaAcaxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaAcaxFn, 6).setUnsigned();
	
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	
	// Starts: ILIFE-4397
	private AcagpfDAO acagpfDAO = getApplicationContext()
			.getBean("acagpfDAO", AcagpfDAO.class);
	// Ends: ILIFE-4397
	

	public B6688() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		wsaaAcaxRunid.set(bprdIO.getSystemParam04());
		wsaaAcaxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		iz.set(1);
		openThreadMember1100();
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
	}

protected void openThreadMember1100()
	{
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(),1),iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaAcaxFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz,varcom.oK)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append("CLRTMPF");
			stringVariable1.append(delimitedExp(bprdIO.getRunLibrary(), SPACES));
			stringVariable1.append(SPACES);
			stringVariable1.append(wsaaAcaxFn.toString());
			stringVariable1.append(SPACES);
			stringVariable1.append(wsaaThreadMember.toString());
			wsaaClrtmpfError.setLeft(stringVariable1.toString());
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.params.set(wsaaClrtmpfError);
			fatalError600();
		}
		//acax01.openOutput();
	}

protected void readFile2000()
	{
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
		}
	}

protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		StringBuilder acaxTempTableName = new StringBuilder("");
		acaxTempTableName.append("ACAX");
		acaxTempTableName.append(bprdIO.getSystemParam04().toString().trim().toUpperCase());
		String scheduleNumber = bsscIO.getScheduleNumber().toString();
		scheduleNumber = scheduleNumber.substring(scheduleNumber.length()-4);
		acaxTempTableName.append(String.format("%04d", Integer.parseInt(scheduleNumber.trim())));
	
		int noOfSubThreads = bprdIO.getThreadsSubsqntProc().toInt();
		int noOfCopiedRec = acagpfDAO.copyDataToTempTable(appVars.getTableNameOverriden("ACAGPF"), 
				acaxTempTableName.toString(), noOfSubThreads);
		contotrec.totval.set(noOfCopiedRec);
		contotrec.totno.set(ct02);
		callContot001();
		
		wsaaEofInBlock.set("Y");
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		//acax01.close();
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
