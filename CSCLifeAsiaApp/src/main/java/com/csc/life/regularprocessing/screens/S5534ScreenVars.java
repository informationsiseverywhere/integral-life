package com.csc.life.regularprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5534
 * @version 1.0 generated on 30/08/09 06:41
 * @author Quipoz
 */
public class S5534ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(232);
	public FixedLengthStringData dataFields = new FixedLengthStringData(72).isAPartOf(dataArea, 0);
	public FixedLengthStringData adfeemth = DD.adfeemth.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,4);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,5);
	public FixedLengthStringData jlPremMeth = DD.jlpremeth.copy().isAPartOf(dataFields,13);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,17);
	public FixedLengthStringData premmeth = DD.premmeth.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData subprog = DD.subprog.copy().isAPartOf(dataFields,51);
	public FixedLengthStringData svMethod = DD.svmeth.copy().isAPartOf(dataFields,61);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData unitFreq = DD.ufreq.copy().isAPartOf(dataFields,70);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 72);
	public FixedLengthStringData adfeemthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData jlpremethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData premmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData subprogErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData svmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData ufreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 112);
	public FixedLengthStringData[] adfeemthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] jlpremethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] premmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] subprogOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] svmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] ufreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5534screenWritten = new LongData(0);
	public LongData S5534protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5534ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(premmethOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jlpremethOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(svmethOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ufreqOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subprogOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, premmeth, jlPremMeth, svMethod, unitFreq, subprog, adfeemth};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, premmethOut, jlpremethOut, svmethOut, ufreqOut, subprogOut, adfeemthOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, premmethErr, jlpremethErr, svmethErr, ufreqErr, subprogErr, adfeemthErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5534screen.class;
		protectRecord = S5534protect.class;
	}

}
