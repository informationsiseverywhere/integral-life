package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:26
 * Description:
 * Copybook name: COVRRNLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrrnlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrrnlFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData covrrnlKey = new FixedLengthStringData(256).isAPartOf(covrrnlFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrrnlChdrcoy = new FixedLengthStringData(1).isAPartOf(covrrnlKey, 0);
  	public FixedLengthStringData covrrnlChdrnum = new FixedLengthStringData(8).isAPartOf(covrrnlKey, 1);
  	public FixedLengthStringData covrrnlLife = new FixedLengthStringData(2).isAPartOf(covrrnlKey, 9);
  	public FixedLengthStringData covrrnlCoverage = new FixedLengthStringData(2).isAPartOf(covrrnlKey, 11);
  	public FixedLengthStringData covrrnlRider = new FixedLengthStringData(2).isAPartOf(covrrnlKey, 13);
  	public PackedDecimalData covrrnlPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrrnlKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(238).isAPartOf(covrrnlKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrrnlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrrnlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}