package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:45
 * Description:
 * Copybook name: DDBTALLREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ddbtallrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData ddbtRec = new FixedLengthStringData(66);
  	public FixedLengthStringData function = new FixedLengthStringData(4).isAPartOf(ddbtRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(ddbtRec, 4);
  	public FixedLengthStringData payrcoy = new FixedLengthStringData(2).isAPartOf(ddbtRec, 8);
  	public FixedLengthStringData mandref = new FixedLengthStringData(5).isAPartOf(ddbtRec, 10);
  	public FixedLengthStringData payrnum = new FixedLengthStringData(8).isAPartOf(ddbtRec, 15);
  	public ZonedDecimalData billamt = new ZonedDecimalData(17, 2).isAPartOf(ddbtRec, 23);
  	public ZonedDecimalData lastUsedDate = new ZonedDecimalData(8, 0).isAPartOf(ddbtRec, 40);
  	public FixedLengthStringData filler = new FixedLengthStringData(18).isAPartOf(ddbtRec, 48, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ddbtRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ddbtRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}