package com.csc.life.regularprocessing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RR631.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:52
 * @author Quipoz
 */
public class Rr631Report extends SMARTReportLayout { 

	private FixedLengthStringData bankcode = new FixedLengthStringData(2);
	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private ZonedDecimalData cmin = new ZonedDecimalData(17, 2);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData currcd = new FixedLengthStringData(3);
	private FixedLengthStringData desi = new FixedLengthStringData(30);
	private FixedLengthStringData longname = new FixedLengthStringData(50);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData repdate = new FixedLengthStringData(10);
	private FixedLengthStringData reqnno = new FixedLengthStringData(9);
	private FixedLengthStringData reqntype = new FixedLengthStringData(1);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private FixedLengthStringData tenline = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData zseqno = new ZonedDecimalData(4, 0);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rr631Report() {
		super();
	}


	/**
	 * Print the XML for Rr631d01
	 */
	public void printRr631d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		zseqno.setFieldName("zseqno");
		zseqno.setInternal(subString(recordData, 1, 4));
		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 5, 8));
		longname.setFieldName("longname");
		longname.setInternal(subString(recordData, 13, 50));
		tenline.setFieldName("tenline");
		tenline.setInternal(subString(recordData, 63, 10));
		cmin.setFieldName("cmin");
		cmin.setInternal(subString(recordData, 73, 17));
		currcd.setFieldName("currcd");
		currcd.setInternal(subString(recordData, 90, 3));
		bankcode.setFieldName("bankcode");
		bankcode.setInternal(subString(recordData, 93, 2));
		reqntype.setFieldName("reqntype");
		reqntype.setInternal(subString(recordData, 95, 1));
		reqnno.setFieldName("reqnno");
		reqnno.setInternal(subString(recordData, 96, 9));
		desi.setFieldName("desi");
		desi.setInternal(subString(recordData, 105, 30));
		printLayout("Rr631d01",			// Record name
			new BaseData[]{			// Fields:
				zseqno,
				chdrnum,
				longname,
				tenline,
				cmin,
				currcd,
				bankcode,
				reqntype,
				reqnno,
				desi
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rr631f01
	 */
	public void printRr631f01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rr631f01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rr631h01
	 */
	public void printRr631h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.set(3);

		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 1, 10));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 11, 10));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 21, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 22, 30));
		time.setFieldName("time");
		time.set(getTime());
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 52, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 54, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rr631h01",			// Record name
			new BaseData[]{			// Fields:
				repdate,
				sdate,
				company,
				companynm,
				time,
				branch,
				branchnm,
				pagnbr
			}
		);

		currentPrintLine.add(4);
	}

	/**
	 * Print the XML for Rr631h02
	 */
	public void printRr631h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rr631h02",			// Record name
			new BaseData[]{			// Fields:
			}
		);

		currentPrintLine.add(2);
	}


}
