package com.csc.life.regularprocessing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:29
 * Description:
 * Copybook name: P5018PAR
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class P5018par extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData parmRecord = new FixedLengthStringData(35);
  	public ZonedDecimalData bonusPeriod = new ZonedDecimalData(11, 5).isAPartOf(parmRecord, 0);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(parmRecord, 11);
  	public FixedLengthStringData chdrnum1 = new FixedLengthStringData(8).isAPartOf(parmRecord, 19);
  	public ZonedDecimalData dclDate = new ZonedDecimalData(8, 0).isAPartOf(parmRecord, 27);


	public void initialize() {
		COBOLFunctions.initialize(parmRecord);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRecord.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}