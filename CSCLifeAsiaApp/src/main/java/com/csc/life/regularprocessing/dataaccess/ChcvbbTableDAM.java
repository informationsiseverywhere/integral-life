package com.csc.life.regularprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ChcvbbTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:20
 * Class transformed from CHCVBB
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ChcvbbTableDAM extends PFAdapterDAM {

	public int pfRecLen = 73;
	public FixedLengthStringData chcvrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData chcvbbRecord = chcvrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(chcvrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(chcvrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(chcvrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(chcvrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(chcvrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(chcvrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(chcvrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(chcvrec);
	public FixedLengthStringData covrValidflag = DD.covrvalid.copy().isAPartOf(chcvrec);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(chcvrec);
	public FixedLengthStringData servunit = DD.servunit.copy().isAPartOf(chcvrec);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(chcvrec);
	public FixedLengthStringData pstatcode = DD.pstcde.copy().isAPartOf(chcvrec);
	public FixedLengthStringData covrStatCode = DD.covrstat.copy().isAPartOf(chcvrec);
	public FixedLengthStringData covrPstatCode = DD.covrpstat.copy().isAPartOf(chcvrec);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(chcvrec);
	public FixedLengthStringData billchnl = DD.billchnl.copy().isAPartOf(chcvrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(chcvrec);
	public PackedDecimalData benCessDate = DD.bcesdte.copy().isAPartOf(chcvrec);
	public PackedDecimalData benBillDate = DD.bbldat.copy().isAPartOf(chcvrec);
	public FixedLengthStringData premCurrency = DD.prmcur.copy().isAPartOf(chcvrec);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(chcvrec);
	public PackedDecimalData premCessDate = DD.pcesdte.copy().isAPartOf(chcvrec);
	public PackedDecimalData sumins = DD.sumins.copy().isAPartOf(chcvrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(chcvrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ChcvbbTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for ChcvbbTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ChcvbbTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ChcvbbTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ChcvbbTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ChcvbbTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ChcvbbTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("CHCVBB");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"VALIDFLAG, " +
							"COVRVALID, " +
							"CNTBRANCH, " +
							"SERVUNIT, " +
							"STATCODE, " +
							"PSTCDE, " +
							"COVRSTAT, " +
							"COVRPSTAT, " +
							"BILLFREQ, " +
							"BILLCHNL, " +
							"CNTTYPE, " +
							"BCESDTE, " +
							"BBLDAT, " +
							"PRMCUR, " +
							"MORTCLS, " +
							"PCESDTE, " +
							"SUMINS, " +
							"CRTABLE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     validflag,
                                     covrValidflag,
                                     cntbranch,
                                     servunit,
                                     statcode,
                                     pstatcode,
                                     covrStatCode,
                                     covrPstatCode,
                                     billfreq,
                                     billchnl,
                                     cnttype,
                                     benCessDate,
                                     benBillDate,
                                     premCurrency,
                                     mortcls,
                                     premCessDate,
                                     sumins,
                                     crtable,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		validflag.clear();
  		covrValidflag.clear();
  		cntbranch.clear();
  		servunit.clear();
  		statcode.clear();
  		pstatcode.clear();
  		covrStatCode.clear();
  		covrPstatCode.clear();
  		billfreq.clear();
  		billchnl.clear();
  		cnttype.clear();
  		benCessDate.clear();
  		benBillDate.clear();
  		premCurrency.clear();
  		mortcls.clear();
  		premCessDate.clear();
  		sumins.clear();
  		crtable.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getChcvrec() {
  		return chcvrec;
	}

	public FixedLengthStringData getChcvbbRecord() {
  		return chcvbbRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setChcvrec(what);
	}

	public void setChcvrec(Object what) {
  		this.chcvrec.set(what);
	}

	public void setChcvbbRecord(Object what) {
  		this.chcvbbRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(chcvrec.getLength());
		result.set(chcvrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}