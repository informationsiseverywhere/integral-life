package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class LinspfSQLDAOImpl extends LinspfDAOImpl {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LinspfSQLDAOImpl.class);
	
	public int copyDataToTempTable(String sourceTableName, String tempTableName, int noOfSubseqThreads,	int wsaaEffdate, String wsaaCompany, String wsaaChdrnumFrom, String wsaaChdrnumTo){
		long startTime = System.currentTimeMillis();
		StringBuilder copyQuery = new StringBuilder(" ");
		copyQuery.append("INSERT INTO " + tempTableName + "(CHDRCOY, CHDRNUM, PAYRSEQNO, INSTFROM, BILLCD, CNTCURR, BILLCURR, BILLCHNL, CBILLAMT, INSTAMT01, INSTAMT02, INSTAMT03, INSTAMT04, INSTAMT05, INSTAMT06, INSTFREQ, PRORATEREC, MEMBER_NAME)" );
		copyQuery.append("SELECT CHDRCOY, CHDRNUM, PAYRSEQNO, INSTFROM, BILLCD, CNTCURR, BILLCURR, BILLCHNL, CBILLAMT, INSTAMT01, INSTAMT02, INSTAMT03, INSTAMT04, INSTAMT05, INSTAMT06, INSTFREQ, PRORATEREC ");
		copyQuery.append(", CONCAT('THREAD', CONCAT (REPLICATE('0', 3 - LEN((ROW_NUMBER()OVER(  ORDER BY CHDRCOY, CHDRNUM, INSTFROM)-1) % ? + 1)),  (ROW_NUMBER()OVER(  ORDER BY CHDRCOY, CHDRNUM, INSTFROM)-1) % ? + 1)) MEMBER_NAME FROM " + sourceTableName );
		copyQuery.append(" WHERE UNIQUE_NUMBER IN ( ");
		//ILIFE -7909 start by dpuhawan
		/*
		copyQuery.append(" SELECT MAX(UNIQUE_NUMBER) FROM ");
		copyQuery.append(sourceTableName);
		copyQuery.append(" WHERE PAYFLAG <> 'P' AND BILLCHNL <> 'N' AND BILLCD <= ? AND CHDRCOY = ? AND VALIDFLAG = '1' AND CHDRNUM >= ? AND CHDRNUM <= ? GROUP BY CHDRNUM, INSTFROM ");
		*/
		copyQuery.append(" SELECT UNIQUE_NUMBER FROM ");
		copyQuery.append(sourceTableName);
		copyQuery.append(" WHERE PAYFLAG <> 'P' AND BILLCHNL <> 'N' AND BILLCD <= ? AND CHDRCOY = ? AND VALIDFLAG = '1' AND CHDRNUM >= ? AND CHDRNUM <= ? ");	
		//ILIFE -7909 end
		copyQuery.append(" ) ORDER BY CHDRCOY, CHDRNUM, INSTFROM ");
		int copiedRecordCount = 0;		
		try(PreparedStatement ps = getPrepareStatement(copyQuery.toString())) {
			ps.setInt(1,noOfSubseqThreads);
			ps.setInt(2,noOfSubseqThreads);
			ps.setInt(3, wsaaEffdate);
			ps.setString(4, wsaaCompany);
			ps.setString(5, wsaaChdrnumFrom);
			ps.setString(6, wsaaChdrnumTo);
			copiedRecordCount = ps.executeUpdate();
			LOGGER.info("{} record(s) copied in {} ms", copiedRecordCount, (System.currentTimeMillis() - startTime));//IJTI-1561
			LOGGER.info("from table {} to table {}", sourceTableName, tempTableName);//IJTI-1561
		} catch (SQLException e) {
			LOGGER.error("copyDataToTable() ", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		}
		return copiedRecordCount;
	}	
}
