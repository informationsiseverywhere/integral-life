package com.csc.life.regularprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: LinspfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:44
 * Class transformed from LINSPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LinspfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 196;
	public FixedLengthStringData linsrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData linspfRecord = linsrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(linsrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(linsrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(linsrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(linsrec);
	public FixedLengthStringData branch = DD.branch.copy().isAPartOf(linsrec);
	public PackedDecimalData instfrom = DD.instfrom.copy().isAPartOf(linsrec);
	public PackedDecimalData instto = DD.instto.copy().isAPartOf(linsrec);
	public PackedDecimalData instamt01 = DD.instamt.copy().isAPartOf(linsrec);
	public PackedDecimalData instamt02 = DD.instamt.copy().isAPartOf(linsrec);
	public PackedDecimalData instamt03 = DD.instamt.copy().isAPartOf(linsrec);
	public PackedDecimalData instamt04 = DD.instamt.copy().isAPartOf(linsrec);
	public PackedDecimalData instamt05 = DD.instamt.copy().isAPartOf(linsrec);
	public PackedDecimalData instamt06 = DD.instamt.copy().isAPartOf(linsrec);
	public FixedLengthStringData instfreq = DD.instfreq.copy().isAPartOf(linsrec);
	public FixedLengthStringData instjctl = DD.instjctl.copy().isAPartOf(linsrec);
	public FixedLengthStringData billchnl = DD.billchnl.copy().isAPartOf(linsrec);
	public FixedLengthStringData payflag = DD.payflag.copy().isAPartOf(linsrec);
	public FixedLengthStringData dueflg = DD.dueflg.copy().isAPartOf(linsrec);
	public FixedLengthStringData transcode = DD.transcode.copy().isAPartOf(linsrec);
	public PackedDecimalData cbillamt = DD.cbillamt.copy().isAPartOf(linsrec);
	public FixedLengthStringData billcurr = DD.billcurr.copy().isAPartOf(linsrec);
	public FixedLengthStringData mandref = DD.mandref.copy().isAPartOf(linsrec);
	public PackedDecimalData billcd = DD.billcd.copy().isAPartOf(linsrec);
	public PackedDecimalData payrseqno = DD.payrseqno.copy().isAPartOf(linsrec);
	public FixedLengthStringData taxrelmth = DD.taxrelmth.copy().isAPartOf(linsrec);
	public FixedLengthStringData acctmeth = DD.acctmeth.copy().isAPartOf(linsrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(linsrec);
	public FixedLengthStringData jobname = DD.jobname.copy().isAPartOf(linsrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(linsrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(linsrec);
	public FixedLengthStringData proraterec = new FixedLengthStringData(1).isAPartOf(linsrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public LinspfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for LinspfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public LinspfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for LinspfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public LinspfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for LinspfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public LinspfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("LINSPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"CNTCURR, " +
							"VALIDFLAG, " +
							"BRANCH, " +
							"INSTFROM, " +
							"INSTTO, " +
							"INSTAMT01, " +
							"INSTAMT02, " +
							"INSTAMT03, " +
							"INSTAMT04, " +
							"INSTAMT05, " +
							"INSTAMT06, " +
							"INSTFREQ, " +
							"INSTJCTL, " +
							"BILLCHNL, " +
							"PAYFLAG, " +
							"DUEFLG, " +
							"TRANSCODE, " +
							"CBILLAMT, " +
							"BILLCURR, " +
							"MANDREF, " +
							"BILLCD, " +
							"PAYRSEQNO, " +
							"TAXRELMTH, " +
							"ACCTMETH, " +
							"USRPRF, " +
							"JOBNAME, " +
							"DATIME, " +
							"JOBNM, " +
							"PRORATEREC, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     cntcurr,
                                     validflag,
                                     branch,
                                     instfrom,
                                     instto,
                                     instamt01,
                                     instamt02,
                                     instamt03,
                                     instamt04,
                                     instamt05,
                                     instamt06,
                                     instfreq,
                                     instjctl,
                                     billchnl,
                                     payflag,
                                     dueflg,
                                     transcode,
                                     cbillamt,
                                     billcurr,
                                     mandref,
                                     billcd,
                                     payrseqno,
                                     taxrelmth,
                                     acctmeth,
                                     userProfile,
                                     jobname,
                                     datime,
                                     jobName,
                                     proraterec,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		cntcurr.clear();
  		validflag.clear();
  		branch.clear();
  		instfrom.clear();
  		instto.clear();
  		instamt01.clear();
  		instamt02.clear();
  		instamt03.clear();
  		instamt04.clear();
  		instamt05.clear();
  		instamt06.clear();
  		instfreq.clear();
  		instjctl.clear();
  		billchnl.clear();
  		payflag.clear();
  		dueflg.clear();
  		transcode.clear();
  		cbillamt.clear();
  		billcurr.clear();
  		mandref.clear();
  		billcd.clear();
  		payrseqno.clear();
  		taxrelmth.clear();
  		acctmeth.clear();
  		userProfile.clear();
  		jobname.clear();
  		datime.clear();
  		jobName.clear();
  		proraterec.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getLinsrec() {
  		return linsrec;
	}

	public FixedLengthStringData getLinspfRecord() {
  		return linspfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setLinsrec(what);
	}

	public void setLinsrec(Object what) {
  		this.linsrec.set(what);
	}

	public void setLinspfRecord(Object what) {
  		this.linspfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(linsrec.getLength());
		result.set(linsrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}