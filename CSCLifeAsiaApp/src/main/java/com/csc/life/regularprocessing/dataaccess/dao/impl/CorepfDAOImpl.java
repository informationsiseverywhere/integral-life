package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.CorepfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Corepf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class CorepfDAOImpl extends BaseDAOImpl<Corepf> implements CorepfDAO {
	   private static final Logger LOGGER = LoggerFactory.getLogger(CorepfDAOImpl.class);

		@Override
		public List<Corepf> findResults(String tableId, String memName,
				int batchExtractSize, int batchID) {
			StringBuilder sqlStr = new StringBuilder();
//			sqlStr.append(" SELECT * FROM ( ");
//			sqlStr.append(" SELECT CHDRCOY,CHDRNUM,STMDTE,PTDATE,CNTTYPE,STATCODE,PSTCDE,OCCDATE,UNIQUE_NUMBER ");
//			sqlStr.append("   ,FLOOR((ROW_NUMBER()OVER( ORDER BY UNIQUE_NUMBER DESC)-1)/?) BATCHNUM ");
//			sqlStr.append("  FROM ");
//			sqlStr.append(tableId);
//			sqlStr.append("   WHERE MEMBER_NAME = ? ");
//			sqlStr.append(" ) MAIN WHERE BATCHNUM = ? ");
			
	        sqlStr.append("SELECT * FROM (");
	        sqlStr.append("SELECT FLOOR((ROW_NUMBER()OVER(ORDER BY TE.UNIQUE_NUMBER DESC)-1)/?) ROWNM, TE.* FROM ");
	        sqlStr.append(tableId);
	        sqlStr.append(" TE");
	        sqlStr.append(" WHERE MEMBER_NAME = ? )cc WHERE cc.ROWNM = ?");
			PreparedStatement ps = getPrepareStatement(sqlStr.toString());
			ResultSet rs = null;
			List<Corepf> pfList = new ArrayList<>();
			try {
				ps.setInt(1, batchExtractSize);
				ps.setString(2, memName);
				ps.setInt(3, batchID);
				rs = executeQuery(ps);
				while (rs.next()) {
					Corepf pf = new Corepf();
					pf.setChdrcoy(rs.getString("CHDRCOY"));
					pf.setChdrnum(rs.getString("CHDRNUM"));
					pf.setLife(rs.getString("LIFE"));
					pf.setCoverage(rs.getString("COVERAGE"));
					pf.setRider(rs.getString("RIDER"));
					pf.setPlanSuffix(rs.getInt("PLNSFX"));
					pf.setPstatcode(rs.getString("PSTATCODE"));
					pf.setStatcode(rs.getString("STATCODE"));
					pf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
					pfList.add(pf);
				}
			} catch (SQLException e) {
				LOGGER.error("findResults()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);
			}
			return pfList;
		}
}
