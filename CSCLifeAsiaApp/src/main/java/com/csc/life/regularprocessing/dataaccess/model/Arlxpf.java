package com.csc.life.regularprocessing.dataaccess.model;

public class Arlxpf {
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int planSuffix;
	private int npaydate;
	private String validflag;
	private int currfrom;
	private int currto;
	private String crtable;
	private String cownnum;
	private long uniqueNumber;

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public int getPlanSuffix() {
		return planSuffix;
	}

	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}

	public int getNpaydate() {
		return npaydate;
	}

	public void setNpaydate(int npaydate) {
		this.npaydate = npaydate;
	}

	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	
	public int getCurrfrom() {
		return currfrom;
	}

	public void setCurrfrom(int currfrom) {
		this.currfrom = currfrom;
	}

	public int getCurrto() {
		return currto;
	}

	public void setCurrto(int currto) {
		this.currto = currto;
	}

	public String getCrtable() {
		return crtable;
	}

	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	public String getCownnum() {
		return cownnum;
	}

	public void setCownnum(String cownnum) {
		this.cownnum = cownnum;
	}

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

}
