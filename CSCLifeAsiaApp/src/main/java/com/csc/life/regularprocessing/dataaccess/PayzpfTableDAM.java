package com.csc.life.regularprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: PayzpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:00
 * Class transformed from PAYZPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PayzpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 25;
	public FixedLengthStringData payzrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData payzpfRecord = payzrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(payzrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(payzrec);
	public PackedDecimalData payrseqno = DD.payrseqno.copy().isAPartOf(payzrec);
	public PackedDecimalData btdate = DD.btdate.copy().isAPartOf(payzrec);
	public PackedDecimalData ptdate = DD.ptdate.copy().isAPartOf(payzrec);
	public FixedLengthStringData billchnl = DD.billchnl.copy().isAPartOf(payzrec);
	public FixedLengthStringData znfopt = DD.znfopt.copy().isAPartOf(payzrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public PayzpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for PayzpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public PayzpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for PayzpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public PayzpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for PayzpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public PayzpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("PAYZPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"PAYRSEQNO, " +
							"BTDATE, " +
							"PTDATE, " +
							"BILLCHNL, " +
							"ZNFOPT, "+
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     payrseqno,
                                     btdate,
                                     ptdate,
                                     billchnl,
                                     znfopt,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		payrseqno.clear();
  		btdate.clear();
  		ptdate.clear();
  		billchnl.clear();
  		znfopt.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getPayzrec() {
  		return payzrec;
	}

	public FixedLengthStringData getPayzpfRecord() {
  		return payzpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setPayzrec(what);
	}

	public void setPayzrec(Object what) {
  		this.payzrec.set(what);
	}

	public void setPayzpfRecord(Object what) {
  		this.payzpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(payzrec.getLength());
		result.set(payzrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}