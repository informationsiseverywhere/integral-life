package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:26
 * Description:
 * Copybook name: HBXTPNTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hbxtpntkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hbxtpntFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData hbxtpntKey = new FixedLengthStringData(256).isAPartOf(hbxtpntFileKey, 0, REDEFINE);
  	public FixedLengthStringData hbxtpntChdrcoy = new FixedLengthStringData(1).isAPartOf(hbxtpntKey, 0);
  	public FixedLengthStringData hbxtpntChdrnum = new FixedLengthStringData(8).isAPartOf(hbxtpntKey, 1);
  	public PackedDecimalData hbxtpntInstfrom = new PackedDecimalData(8, 0).isAPartOf(hbxtpntKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(242).isAPartOf(hbxtpntKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hbxtpntFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hbxtpntFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}