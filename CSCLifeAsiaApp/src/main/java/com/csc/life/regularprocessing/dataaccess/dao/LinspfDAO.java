package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;
import java.util.Map;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface LinspfDAO extends BaseDAO<Linspf> {
    public void updateLinspf(List<Linspf> linspfList);
    public boolean insertLinsPF(List<? extends Linspf> linsList);
    
    // Ticket#ILIFE-4404
    public Map<String, List<Linspf>> searchLinsrnlRecord(List<String> chdrnumList);
    public void updateLinspfDueflg(List<Linspf> linspfList);

    public Linspf getTotBilAmt(Linspf linspf);//ILIFE-5975

	  //Ticket #ILIFE-5362
    public Linspf getLinsRecordByCoyAndNumAndflag(String chdrcoy, String chdrnum, String validflag);
    public List<Linspf> searchLinscfiRecord(String coy,String chdrnum);
    public void deleteLinspfRecord(List<Linspf> linspfList);
    public Map<String,Integer> searchTdayBillCDContractNos(List<String> chdrnumList,String chdrcoy);
    public int copyDataToTempTable(String sourceTableName, String tempTableName, int noOfSubseqThreads,	int wsaaEffdate, String wsaaCompany, String wsaaChdrnumFrom, String wsaaChdrnumTo);
    
    //IJTI-1727 starts
	/**
	 * Gets Linspf records for collection process in policy renewal.
	 * 
	 * @param linspf - Input Linspf
	 * @return - list of Linspf records
	 */
	public List<Linspf> getLinspfForCollection(Linspf linspfInput);
	//IJTI-1727 ends
	//Ticket #PINNACLE - 1363 starts
	List<Linspf> getPaidLinspfRecordList(String chdrnum);
	public Linspf getProratedRecord(String coy,String chdrnum,String proraterec);
	public Linspf getProratedRecordDishonored(String coy, String chdrnum, String proraterec, int instfrom, int instto);
}