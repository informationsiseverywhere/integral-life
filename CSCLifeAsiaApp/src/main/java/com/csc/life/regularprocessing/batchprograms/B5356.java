/*
 * File: B5356.java
 * Date: 29 August 2009 21:10:14
 * Author: Quipoz Limited
 * 
 * Class transformed from B5356.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.regularprocessing.dataaccess.LinsTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* B5356 - LINS Initialisation Program.
*
* Overview.
* =========
* This program is part of the new Multi-thread Batch Environment
* Life Renewals suite. It is run once to initialise all records
* from Revenue Accounted contracts on the system prior to the
* installation of the new Billing program.
*
* Processing.
* ===========
* For each LINS:
*
* Get the contract from CHDR for the LINS. Because the LINS is
* keyed on CHDRCOY and CHDRNUM, we do not need to read the CHDR
* for every LINS on a contract.
*
* If the CHDR has ACCTMETH = 'R', move the acctmeth to the
* LINS-ACCTMETH and rewrite the LINS.
*
* Continue until all LINS records have been processed.
*
* Control Totals.
* ===============
* 01 : Number of LINS records read
* 02 : Number of LINS records updated to 'R'
* 03 : Number of LINS records updated to other than 'R'
*****************************************************************
* </pre>
*/
public class B5356 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5356");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPrevChdrcoy = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
	private String chdrlifrec = "CHDRLIFREC";
	private String linsrec = "LINSREC";
		/* ERRORS */
	private String ivrm = "IVRM";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Contract Header Life Fields*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
		/*Life instalments billed details*/
	private LinsTableDAM linsIO = new LinsTableDAM();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		exit2590
	}

	public B5356() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		if (isNE(bprdIO.getRestartMethod(),"2")) {
			syserrrec.syserrStatuz.set(ivrm);
			fatalError600();
		}
		wsspEdterror.set(varcom.oK);
		linsIO.setParams(SPACES);
		linsIO.setFormat(linsrec);
		linsIO.setFunction(varcom.begnh);
		/*EXIT*/
	}

protected void readFile2000()
	{
		try {
			readFile2010();
		}
		catch (GOTOException e){
		}
	}

protected void readFile2010()
	{
		SmartFileCode.execute(appVars, linsIO);
		if (isNE(linsIO.getStatuz(),varcom.oK)
		&& isNE(linsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(linsIO.getParams());
			fatalError600();
		}
		if (isEQ(linsIO.getStatuz(),varcom.endp)) {
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
	}

protected void edit2500()
	{
		try {
			edit2510();
		}
		catch (GOTOException e){
		}
	}

protected void edit2510()
	{
		wsspEdterror.set(varcom.oK);
		if (isEQ(linsIO.getChdrcoy(),wsaaPrevChdrcoy)
		&& isEQ(linsIO.getChdrnum(),wsaaPrevChdrnum)) {
			goTo(GotoLabel.exit2590);
		}
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(linsIO.getChdrcoy());
		chdrlifIO.setChdrnum(linsIO.getChdrnum());
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		wsaaPrevChdrcoy.set(chdrlifIO.getChdrcoy());
		wsaaPrevChdrnum.set(chdrlifIO.getChdrnum());
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		linsIO.setAcctmeth(chdrlifIO.getAcctmeth());
		linsIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, linsIO);
		if (isNE(linsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(linsIO.getParams());
			fatalError600();
		}
		linsIO.setFunction(varcom.nextr);
		if (isEQ(chdrlifIO.getAcctmeth(),"R")) {
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
		}
		else {
			contotrec.totno.set(ct03);
			contotrec.totval.set(1);
			callContot001();
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
