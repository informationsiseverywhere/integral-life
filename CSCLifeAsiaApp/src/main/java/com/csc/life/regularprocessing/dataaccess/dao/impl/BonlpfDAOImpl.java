package com.csc.life.regularprocessing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.regularprocessing.dataaccess.dao.BonlpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Bonlpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class BonlpfDAOImpl extends BaseDAOImpl<Bonlpf> implements BonlpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(BonlpfDAOImpl.class);

    public void insertBonlpf(List<Bonlpf> bonlpfList) {

        if (bonlpfList != null && bonlpfList.size() > 0) {

            String SQL_BONL_INSERT = "INSERT INTO BONLPF(CHDRCOY,CHDRNUM,CURRFROM,CURRTO,DATETEXC,VALIDFLAG,LONGDESC01,LONGDESC02,LONGDESC03,LONGDESC04,LONGDESC05,LONGDESC06,LONGDESC07,LONGDESC08,LONGDESC09,LONGDESC10,SUMINS01,SUMINS02,SUMINS03,SUMINS04,SUMINS05,SUMINS06,SUMINS07,SUMINS08,SUMINS09,SUMINS10,BCALMETH01,BCALMETH02,BCALMETH03,BCALMETH04,BCALMETH05,BCALMETH06,BCALMETH07,BCALMETH08,BCALMETH09,BCALMETH10,BPAYTY01,BPAYTY02,BPAYTY03,BPAYTY04,BPAYTY05,BPAYTY06,BPAYTY07,BPAYTY08,BPAYTY09,BPAYTY10,BPAYNY01,BPAYNY02,BPAYNY03,BPAYNY04,BPAYNY05,BPAYNY06,BPAYNY07,BPAYNY08,BPAYNY09,BPAYNY10,TOTBON01,TOTBON02,TOTBON03,TOTBON04,TOTBON05,TOTBON06,TOTBON07,TOTBON08,TOTBON09,TOTBON10,TERMID,USER_T,TRDT,TRTM,JOBNM,USRPRF,DATIME)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement psBonlInsert = getPrepareStatement(SQL_BONL_INSERT);
            try {
                for (Bonlpf c : bonlpfList) {
                    psBonlInsert.setString(1, c.getChdrcoy());
                    psBonlInsert.setString(2, c.getChdrnum());
                    psBonlInsert.setInt(3, c.getCurrfrom());
                    psBonlInsert.setInt(4, c.getCurrto());
                    psBonlInsert.setString(5, c.getDatetexc());
                    psBonlInsert.setString(6, c.getValidflag());
                    psBonlInsert.setString(7, c.getLongdesc01());
                    psBonlInsert.setString(8, c.getLongdesc02());
                    psBonlInsert.setString(9, c.getLongdesc03());
                    psBonlInsert.setString(10, c.getLongdesc04());
                    psBonlInsert.setString(11, c.getLongdesc05());
                    psBonlInsert.setString(12, c.getLongdesc06());
                    psBonlInsert.setString(13, c.getLongdesc07());
                    psBonlInsert.setString(14, c.getLongdesc08());
                    psBonlInsert.setString(15, c.getLongdesc09());
                    psBonlInsert.setString(16, c.getLongdesc10());
                    psBonlInsert.setBigDecimal(17, c.getSumins01());
                    psBonlInsert.setBigDecimal(18, c.getSumins02());
                    psBonlInsert.setBigDecimal(19, c.getSumins03());
                    psBonlInsert.setBigDecimal(20, c.getSumins04());
                    psBonlInsert.setBigDecimal(21, c.getSumins05());
                    psBonlInsert.setBigDecimal(22, c.getSumins06());
                    psBonlInsert.setBigDecimal(23, c.getSumins07());
                    psBonlInsert.setBigDecimal(24, c.getSumins08());
                    psBonlInsert.setBigDecimal(25, c.getSumins09());
                    psBonlInsert.setBigDecimal(26, c.getSumins10());
                    psBonlInsert.setString(27, c.getBonusCalcMeth01());
                    psBonlInsert.setString(28, c.getBonusCalcMeth02());
                    psBonlInsert.setString(29, c.getBonusCalcMeth03());
                    psBonlInsert.setString(30, c.getBonusCalcMeth04());
                    psBonlInsert.setString(31, c.getBonusCalcMeth05());
                    psBonlInsert.setString(32, c.getBonusCalcMeth06());
                    psBonlInsert.setString(33, c.getBonusCalcMeth07());
                    psBonlInsert.setString(34, c.getBonusCalcMeth08());
                    psBonlInsert.setString(35, c.getBonusCalcMeth09());
                    psBonlInsert.setString(36, c.getBonusCalcMeth10());
                    psBonlInsert.setBigDecimal(37, c.getBonPayThisYr01());
                    psBonlInsert.setBigDecimal(38, c.getBonPayThisYr02());
                    psBonlInsert.setBigDecimal(39, c.getBonPayThisYr03());
                    psBonlInsert.setBigDecimal(40, c.getBonPayThisYr04());
                    psBonlInsert.setBigDecimal(41, c.getBonPayThisYr05());
                    psBonlInsert.setBigDecimal(42, c.getBonPayThisYr06());
                    psBonlInsert.setBigDecimal(43, c.getBonPayThisYr07());
                    psBonlInsert.setBigDecimal(44, c.getBonPayThisYr08());
                    psBonlInsert.setBigDecimal(45, c.getBonPayThisYr09());
                    psBonlInsert.setBigDecimal(46, c.getBonPayThisYr10());
                    psBonlInsert.setBigDecimal(47, c.getBonPayLastYr01());
                    psBonlInsert.setBigDecimal(48, c.getBonPayLastYr02());
                    psBonlInsert.setBigDecimal(49, c.getBonPayLastYr03());
                    psBonlInsert.setBigDecimal(50, c.getBonPayLastYr04());
                    psBonlInsert.setBigDecimal(51, c.getBonPayLastYr05());
                    psBonlInsert.setBigDecimal(52, c.getBonPayLastYr06());
                    psBonlInsert.setBigDecimal(53, c.getBonPayLastYr07());
                    psBonlInsert.setBigDecimal(54, c.getBonPayLastYr08());
                    psBonlInsert.setBigDecimal(55, c.getBonPayLastYr09());
                    psBonlInsert.setBigDecimal(56, c.getBonPayLastYr10());
                    psBonlInsert.setBigDecimal(57, c.getTotalBonus01());
                    psBonlInsert.setBigDecimal(58, c.getTotalBonus02());
                    psBonlInsert.setBigDecimal(59, c.getTotalBonus03());
                    psBonlInsert.setBigDecimal(60, c.getTotalBonus04());
                    psBonlInsert.setBigDecimal(61, c.getTotalBonus05());
                    psBonlInsert.setBigDecimal(62, c.getTotalBonus06());
                    psBonlInsert.setBigDecimal(63, c.getTotalBonus07());
                    psBonlInsert.setBigDecimal(64, c.getTotalBonus08());
                    psBonlInsert.setBigDecimal(65, c.getTotalBonus09());
                    psBonlInsert.setBigDecimal(66, c.getTotalBonus10());
                    psBonlInsert.setString(67, c.getTermid());
                    psBonlInsert.setInt(68, c.getUser());
                    psBonlInsert.setInt(69, c.getTransactionDate());
                    psBonlInsert.setInt(70, c.getTransactionTime());
                    psBonlInsert.setString(71, getJobnm());
                    psBonlInsert.setString(72, getUsrprf());
                    psBonlInsert.setTimestamp(73, new Timestamp(System.currentTimeMillis()));
                    psBonlInsert.addBatch();
                }
                psBonlInsert.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("insertBonlRecord()", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(psBonlInsert, null);
            }
            bonlpfList.clear();
        }

    }

    public void updateBonlpf(List<Bonlpf> bonlpfList) {
        if (bonlpfList != null && bonlpfList.size() > 0) {
            String SQL_BONL_UPDATE = "UPDATE BONLPF SET TERMID=?,USER_T=?,TRDT=?,TRTM=?,VALIDFLAG=2,CURRTO=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";

            PreparedStatement psBonlUpdate = getPrepareStatement(SQL_BONL_UPDATE);
            try {
                for (Bonlpf c : bonlpfList) {
                    psBonlUpdate.setString(1, c.getTermid());
                    psBonlUpdate.setInt(2, c.getUser());
                    psBonlUpdate.setInt(3, c.getTransactionDate());
                    psBonlUpdate.setInt(4, c.getTransactionTime());
                    psBonlUpdate.setInt(5, c.getCurrto());
                    psBonlUpdate.setString(6, getJobnm());
                    psBonlUpdate.setString(7, getUsrprf());
                    psBonlUpdate.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
                    psBonlUpdate.setLong(9, c.getUniqueNumer());
                    psBonlUpdate.addBatch();
                }
                psBonlUpdate.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("updateBonlRecord()", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(psBonlUpdate, null);
            }
            bonlpfList.clear();
        }
    }

    public Map<String, List<Bonlpf>> searchBonlpf(String coy, List<String> chdrnumList) {

        StringBuilder sqlBonlSelect1 = new StringBuilder(
                "SELECT CHDRCOY,CHDRNUM,CURRFROM,CURRTO,DATETEXC,VALIDFLAG,LONGDESC01,LONGDESC02,LONGDESC03,LONGDESC04,LONGDESC05,LONGDESC06,LONGDESC07,LONGDESC08,LONGDESC09,LONGDESC10,SUMINS01,SUMINS02,SUMINS03,SUMINS04,SUMINS05,SUMINS06,SUMINS07,SUMINS08,SUMINS09,SUMINS10,BCALMETH01,BCALMETH02,BCALMETH03,BCALMETH04,BCALMETH05,BCALMETH06,BCALMETH07,BCALMETH08,BCALMETH09,BCALMETH10,BPAYTY01,BPAYTY02,BPAYTY03,BPAYTY04,BPAYTY05,BPAYTY06,BPAYTY07,BPAYTY08,BPAYTY09,BPAYTY10,BPAYNY01,BPAYNY02,BPAYNY03,BPAYNY04,BPAYNY05,BPAYNY06,BPAYNY07,BPAYNY08,BPAYNY09,BPAYNY10,TOTBON01,TOTBON02,TOTBON03,TOTBON04,TOTBON05,TOTBON06,TOTBON07,TOTBON08,TOTBON09,TOTBON10,TERMID,USER_T,TRDT,TRTM,UNIQUE_NUMBER ");
        sqlBonlSelect1.append("FROM BONLPF WHERE CHDRCOY=? AND ");
        sqlBonlSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
        sqlBonlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, UNIQUE_NUMBER ASC ");
        PreparedStatement psBonlSelect = getPrepareStatement(sqlBonlSelect1.toString());
        ResultSet sqlbonlpf1rs = null;
        Map<String, List<Bonlpf>> bonlpfMap = new HashMap<String, List<Bonlpf>>();
        try {
            psBonlSelect.setInt(1, Integer.parseInt(coy));
            sqlbonlpf1rs = executeQuery(psBonlSelect);

            while (sqlbonlpf1rs.next()) {
                Bonlpf bonlpf = new Bonlpf();
                bonlpf.setChdrcoy(sqlbonlpf1rs.getString(1));
                bonlpf.setChdrnum(sqlbonlpf1rs.getString(2));
                bonlpf.setCurrfrom(sqlbonlpf1rs.getInt(3));
                bonlpf.setCurrto(sqlbonlpf1rs.getInt(4));
                bonlpf.setDatetexc(sqlbonlpf1rs.getString(5));
                bonlpf.setValidflag(sqlbonlpf1rs.getString(6));
                bonlpf.setLongdesc01(sqlbonlpf1rs.getString(7));
                bonlpf.setLongdesc02(sqlbonlpf1rs.getString(8));
                bonlpf.setLongdesc03(sqlbonlpf1rs.getString(9));
                bonlpf.setLongdesc04(sqlbonlpf1rs.getString(10));
                bonlpf.setLongdesc05(sqlbonlpf1rs.getString(11));
                bonlpf.setLongdesc06(sqlbonlpf1rs.getString(12));
                bonlpf.setLongdesc07(sqlbonlpf1rs.getString(13));
                bonlpf.setLongdesc08(sqlbonlpf1rs.getString(14));
                bonlpf.setLongdesc09(sqlbonlpf1rs.getString(15));
                bonlpf.setLongdesc10(sqlbonlpf1rs.getString(16));
                bonlpf.setSumins01(sqlbonlpf1rs.getBigDecimal(17));
                bonlpf.setSumins02(sqlbonlpf1rs.getBigDecimal(18));
                bonlpf.setSumins03(sqlbonlpf1rs.getBigDecimal(19));
                bonlpf.setSumins04(sqlbonlpf1rs.getBigDecimal(20));
                bonlpf.setSumins05(sqlbonlpf1rs.getBigDecimal(21));
                bonlpf.setSumins06(sqlbonlpf1rs.getBigDecimal(22));
                bonlpf.setSumins07(sqlbonlpf1rs.getBigDecimal(23));
                bonlpf.setSumins08(sqlbonlpf1rs.getBigDecimal(24));
                bonlpf.setSumins09(sqlbonlpf1rs.getBigDecimal(25));
                bonlpf.setSumins10(sqlbonlpf1rs.getBigDecimal(26));
                bonlpf.setBonusCalcMeth01(sqlbonlpf1rs.getString(27));
                bonlpf.setBonusCalcMeth02(sqlbonlpf1rs.getString(28));
                bonlpf.setBonusCalcMeth03(sqlbonlpf1rs.getString(29));
                bonlpf.setBonusCalcMeth04(sqlbonlpf1rs.getString(30));
                bonlpf.setBonusCalcMeth05(sqlbonlpf1rs.getString(31));
                bonlpf.setBonusCalcMeth06(sqlbonlpf1rs.getString(32));
                bonlpf.setBonusCalcMeth07(sqlbonlpf1rs.getString(33));
                bonlpf.setBonusCalcMeth08(sqlbonlpf1rs.getString(34));
                bonlpf.setBonusCalcMeth09(sqlbonlpf1rs.getString(35));
                bonlpf.setBonusCalcMeth10(sqlbonlpf1rs.getString(36));
                bonlpf.setBonPayThisYr01(sqlbonlpf1rs.getBigDecimal(37));
                bonlpf.setBonPayThisYr02(sqlbonlpf1rs.getBigDecimal(38));
                bonlpf.setBonPayThisYr03(sqlbonlpf1rs.getBigDecimal(39));
                bonlpf.setBonPayThisYr04(sqlbonlpf1rs.getBigDecimal(40));
                bonlpf.setBonPayThisYr05(sqlbonlpf1rs.getBigDecimal(41));
                bonlpf.setBonPayThisYr06(sqlbonlpf1rs.getBigDecimal(42));
                bonlpf.setBonPayThisYr07(sqlbonlpf1rs.getBigDecimal(43));
                bonlpf.setBonPayThisYr08(sqlbonlpf1rs.getBigDecimal(44));
                bonlpf.setBonPayThisYr09(sqlbonlpf1rs.getBigDecimal(45));
                bonlpf.setBonPayThisYr10(sqlbonlpf1rs.getBigDecimal(46));
                bonlpf.setBonPayLastYr01(sqlbonlpf1rs.getBigDecimal(47));
                bonlpf.setBonPayLastYr02(sqlbonlpf1rs.getBigDecimal(48));
                bonlpf.setBonPayLastYr03(sqlbonlpf1rs.getBigDecimal(49));
                bonlpf.setBonPayLastYr04(sqlbonlpf1rs.getBigDecimal(50));
                bonlpf.setBonPayLastYr05(sqlbonlpf1rs.getBigDecimal(51));
                bonlpf.setBonPayLastYr06(sqlbonlpf1rs.getBigDecimal(52));
                bonlpf.setBonPayLastYr07(sqlbonlpf1rs.getBigDecimal(53));
                bonlpf.setBonPayLastYr08(sqlbonlpf1rs.getBigDecimal(54));
                bonlpf.setBonPayLastYr09(sqlbonlpf1rs.getBigDecimal(55));
                bonlpf.setBonPayLastYr10(sqlbonlpf1rs.getBigDecimal(56));
                bonlpf.setTotalBonus01(sqlbonlpf1rs.getBigDecimal(57));
                bonlpf.setTotalBonus02(sqlbonlpf1rs.getBigDecimal(58));
                bonlpf.setTotalBonus03(sqlbonlpf1rs.getBigDecimal(59));
                bonlpf.setTotalBonus04(sqlbonlpf1rs.getBigDecimal(60));
                bonlpf.setTotalBonus05(sqlbonlpf1rs.getBigDecimal(61));
                bonlpf.setTotalBonus06(sqlbonlpf1rs.getBigDecimal(62));
                bonlpf.setTotalBonus07(sqlbonlpf1rs.getBigDecimal(63));
                bonlpf.setTotalBonus08(sqlbonlpf1rs.getBigDecimal(64));
                bonlpf.setTotalBonus09(sqlbonlpf1rs.getBigDecimal(65));
                bonlpf.setTotalBonus10(sqlbonlpf1rs.getBigDecimal(66));
                bonlpf.setTermid(sqlbonlpf1rs.getString(67));
                bonlpf.setUser(sqlbonlpf1rs.getInt(68));
                bonlpf.setTransactionDate(sqlbonlpf1rs.getInt(69));
                bonlpf.setTransactionTime(sqlbonlpf1rs.getInt(70));
                bonlpf.setUniqueNumer(sqlbonlpf1rs.getInt(71));
                String chdrnum = bonlpf.getChdrnum();
                if (bonlpfMap.containsKey(chdrnum)) {
                    bonlpfMap.get(chdrnum).add(bonlpf);
                } else {
                    List<Bonlpf> bonlpfSearchResult = new LinkedList<Bonlpf>();
                    bonlpfSearchResult.add(bonlpf);
                    bonlpfMap.put(chdrnum, bonlpfSearchResult);
                }
            }

        } catch (SQLException e) {
            LOGGER.error("searchBonlRecord()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(psBonlSelect, sqlbonlpf1rs);
        }
        return bonlpfMap;
    }
}