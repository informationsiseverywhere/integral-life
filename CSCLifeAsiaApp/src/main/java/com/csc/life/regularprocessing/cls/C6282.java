/*
 * File: C6282.java
 * Date: 30 August 2009 2:58:55
 * Author: $Id$
 * 
 * Class transformed from C6282.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.cls;

import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.directdebit.batchprograms.B6282;
import com.csc.smart.procedures.Passparm;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DshnpfDAO;
import com.csc.smart400framework.dataaccess.model.Dshnpf;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class C6282 extends COBOLConvCodeModel {
    
    	private static final Logger LOGGER = LoggerFactory.getLogger(C6282.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData params = new FixedLengthStringData(300);
	private FixedLengthStringData conj = new FixedLengthStringData(860);
	private boolean isAiaAusDirectDebit,isAiaAusPaymentGateway ;

	public C6282() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
        	/* Pinnacle-98 Get Feature config for AIA */
        	isAiaAusDirectDebit = FeaConfg.isFeatureExist("2", "BTPRO029", appVars, "IT");
        	isAiaAusPaymentGateway = FeaConfg.isFeatureExist("2", "BTPRO030", appVars, "IT");
		params = convertAndSetParam(params, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			try {
				switch (qState) {
				case QS_START: {
					callProgram(Passparm.class, new Object[] {params, conj});
					
					Character parmCom = subString(params, 6, 1).toString().toCharArray()[0];
					Integer parmJobnofr = subString(conj, 11, 8).toInt();
					Integer parmJobnoto = subString(conj, 19, 8).toInt();
					
					//DshnpfDAO dshnpfDAO = DAOFactory.getDAO(DshnpfDAO.class);
					DshnpfDAO dshnpfDAO = DAOFactory.getDshnpfDAO();
					Iterator<Dshnpf> dshnpfIterator;
					LOGGER.info("Going to get dshnpf records:{}",isAiaAusDirectDebit);
					/*Ticket #:Pinnacle-98 for austrailia localization use sql query method */
					if(isAiaAusDirectDebit || isAiaAusPaymentGateway) {
					    dshnpfIterator = dshnpfDAO.findWithServUnitLP(parmCom).iterator();
					}else {
					    dshnpfIterator = dshnpfDAO.findByCriteriaWithServUnitLP(parmCom, parmJobnofr, parmJobnoto).iterator();
					}
					
					callProgram(B6282.class, new Object[] {params, dshnpfIterator});
				}
				case returnVar: {
					return ;
				}
				case error: {
					appVars.sendMessageToQueue("Unexpected errors occurred", "*");
					params.setSub1String(1, 4, "ENDP");
					qState = returnVar;
					break;
				}
				default:{
					qState = QS_END;
				}
				}
			}
			catch (ExtMsgException ex){
				if (ex.messageMatches("CPF0000")
				|| ex.messageMatches("CBE0000")) {
					qState = error;
				}
				else {
					throw ex;
				}
			}
		}
		
	}
}
