package com.csc.life.regularprocessing.dataaccess.model;

import java.math.BigDecimal;

public class Inczpf {

	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private int planSuffix;
	private int crrcd;
	private String crtable;
	private String validflag;
	private String statcode;
	private String pstatcode;
	private String anniversaryMethod;
	private String basicCommMeth;
	private String bascpy;
	private String rnwcpy;
	private String srvcpy;
	private BigDecimal newinst;
	private BigDecimal lastInst;
	private BigDecimal newsum;
	private BigDecimal zbnewinst;
	private BigDecimal zblastinst;
	private BigDecimal zlnewinst;
	private BigDecimal zllastinst;
    public long getUniqueNumber() {
        return uniqueNumber;
    }
    public void setUniqueNumber(long uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }
    public String getChdrcoy() {
        return chdrcoy;
    }
    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }
    public String getChdrnum() {
        return chdrnum;
    }
    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }
    public String getLife() {
        return life;
    }
    public void setLife(String life) {
        this.life = life;
    }
    public String getJlife() {
        return jlife;
    }
    public void setJlife(String jlife) {
        this.jlife = jlife;
    }
    public String getCoverage() {
        return coverage;
    }
    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }
    public String getRider() {
        return rider;
    }
    public void setRider(String rider) {
        this.rider = rider;
    }
    public int getPlanSuffix() {
        return planSuffix;
    }
    public void setPlanSuffix(int planSuffix) {
        this.planSuffix = planSuffix;
    }
    public int getCrrcd() {
        return crrcd;
    }
    public void setCrrcd(int crrcd) {
        this.crrcd = crrcd;
    }
    public String getCrtable() {
        return crtable;
    }
    public void setCrtable(String crtable) {
        this.crtable = crtable;
    }
    public String getValidflag() {
        return validflag;
    }
    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }
    public String getStatcode() {
        return statcode;
    }
    public void setStatcode(String statcode) {
        this.statcode = statcode;
    }
    public String getPstatcode() {
        return pstatcode;
    }
    public void setPstatcode(String pstatcode) {
        this.pstatcode = pstatcode;
    }
    public String getAnniversaryMethod() {
        return anniversaryMethod;
    }
    public void setAnniversaryMethod(String anniversaryMethod) {
        this.anniversaryMethod = anniversaryMethod;
    }
    public String getBasicCommMeth() {
        return basicCommMeth;
    }
    public void setBasicCommMeth(String basicCommMeth) {
        this.basicCommMeth = basicCommMeth;
    }
    public String getBascpy() {
        return bascpy;
    }
    public void setBascpy(String bascpy) {
        this.bascpy = bascpy;
    }
    public String getRnwcpy() {
        return rnwcpy;
    }
    public void setRnwcpy(String rnwcpy) {
        this.rnwcpy = rnwcpy;
    }
    public String getSrvcpy() {
        return srvcpy;
    }
    public void setSrvcpy(String srvcpy) {
        this.srvcpy = srvcpy;
    }
    public BigDecimal getNewinst() {
        return newinst;
    }
    public void setNewinst(BigDecimal newinst) {
        this.newinst = newinst;
    }
    public BigDecimal getLastInst() {
        return lastInst;
    }
    public void setLastInst(BigDecimal lastInst) {
        this.lastInst = lastInst;
    }
    public BigDecimal getNewsum() {
        return newsum;
    }
    public void setNewsum(BigDecimal newsum) {
        this.newsum = newsum;
    }
    public BigDecimal getZbnewinst() {
        return zbnewinst;
    }
    public void setZbnewinst(BigDecimal zbnewinst) {
        this.zbnewinst = zbnewinst;
    }
    public BigDecimal getZblastinst() {
        return zblastinst;
    }
    public void setZblastinst(BigDecimal zblastinst) {
        this.zblastinst = zblastinst;
    }
    public BigDecimal getZlnewinst() {
        return zlnewinst;
    }
    public void setZlnewinst(BigDecimal zlnewinst) {
        this.zlnewinst = zlnewinst;
    }
    public BigDecimal getZllastinst() {
        return zllastinst;
    }
    public void setZllastinst(BigDecimal zllastinst) {
        this.zllastinst = zllastinst;
    }

}