package com.csc.life.regularprocessing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:51
 * Description:
 * Copybook name: BONUSREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Bonusrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData bonusRec = new FixedLengthStringData(223);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(bonusRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(bonusRec, 5);
  	public FixedLengthStringData chdrChdrcoy = new FixedLengthStringData(1).isAPartOf(bonusRec, 9);
  	public FixedLengthStringData chdrChdrnum = new FixedLengthStringData(8).isAPartOf(bonusRec, 10);
  	public FixedLengthStringData lifeLife = new FixedLengthStringData(2).isAPartOf(bonusRec, 18);
  	public FixedLengthStringData covrCoverage = new FixedLengthStringData(2).isAPartOf(bonusRec, 20);
  	public FixedLengthStringData covrRider = new FixedLengthStringData(2).isAPartOf(bonusRec, 22);
  	public PackedDecimalData plnsfx = new PackedDecimalData(4, 0).isAPartOf(bonusRec, 24);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(bonusRec, 27);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(4).isAPartOf(bonusRec, 30);
  	public FixedLengthStringData type = new FixedLengthStringData(1).isAPartOf(bonusRec, 34);
  	public FixedLengthStringData transcd = new FixedLengthStringData(4).isAPartOf(bonusRec, 35);
  	public FixedLengthStringData premStatus = new FixedLengthStringData(2).isAPartOf(bonusRec, 39);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(bonusRec, 41);
  	public PackedDecimalData sumin = new PackedDecimalData(17, 2).isAPartOf(bonusRec, 45);
  	public PackedDecimalData rvBonusSa = new PackedDecimalData(17, 2).isAPartOf(bonusRec, 54);
  	public PackedDecimalData trmBonusSa = new PackedDecimalData(17, 2).isAPartOf(bonusRec, 63);
  	public PackedDecimalData intBonusSa = new PackedDecimalData(17, 2).isAPartOf(bonusRec, 72);
  	public PackedDecimalData extBonusSa = new PackedDecimalData(17, 2).isAPartOf(bonusRec, 81);
  	public PackedDecimalData rvBonusBon = new PackedDecimalData(17, 2).isAPartOf(bonusRec, 90);
  	public PackedDecimalData trmBonusBon = new PackedDecimalData(17, 2).isAPartOf(bonusRec, 99);
  	public PackedDecimalData intBonusBon = new PackedDecimalData(17, 2).isAPartOf(bonusRec, 108);
  	public PackedDecimalData extBonusBon = new PackedDecimalData(17, 2).isAPartOf(bonusRec, 117);
  	public PackedDecimalData effectiveDate = new PackedDecimalData(8, 0).isAPartOf(bonusRec, 126);
  	public PackedDecimalData termInForce = new PackedDecimalData(11, 5).isAPartOf(bonusRec, 131);
  	public FixedLengthStringData allocMethod = new FixedLengthStringData(1).isAPartOf(bonusRec, 137);
  	public FixedLengthStringData deferred = new FixedLengthStringData(1).isAPartOf(bonusRec, 138);
  	public PackedDecimalData duration = new PackedDecimalData(11, 5).isAPartOf(bonusRec, 139);
  	public FixedLengthStringData lowCostRider = new FixedLengthStringData(4).isAPartOf(bonusRec, 145);
  	public FixedLengthStringData lowCostSubr = new FixedLengthStringData(8).isAPartOf(bonusRec, 149);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(bonusRec, 157);
  	public FixedLengthStringData batckey = new FixedLengthStringData(22).isAPartOf(bonusRec, 160);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(bonusRec, 182);
  	public PackedDecimalData rvBalLy = new PackedDecimalData(17, 2).isAPartOf(bonusRec, 183);
  	public PackedDecimalData unitStmtDate = new PackedDecimalData(8, 0).isAPartOf(bonusRec, 192);
  	public FixedLengthStringData bonusCalcMeth = new FixedLengthStringData(4).isAPartOf(bonusRec, 197);
  	public ZonedDecimalData user = new ZonedDecimalData(6, 0).isAPartOf(bonusRec, 201).setUnsigned();
  	public PackedDecimalData bonusPeriod = new PackedDecimalData(11, 5).isAPartOf(bonusRec, 207);
  	public FixedLengthStringData description = new FixedLengthStringData(10).isAPartOf(bonusRec, 213);


	public void initialize() {
		COBOLFunctions.initialize(bonusRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bonusRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}