/*
 * File: B5351.java
 * Date: 29 August 2009 21:06:27
 * Author: Quipoz Limited
 *
 * Class transformed from B5351.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.dao.TaxdpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Taxdpf;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.dataaccess.dao.LinrpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Linrpf;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Contot;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS
*                 REVENUE (DUE-DATE) ACCOUNTING.
*                 -----------------------------
*     The system must be able to handle both Cash and Due Date
* Accounting of contracts. Cash accounting requires that the premi ms
* are only accounted for when the money is available. This is
* for in B5353 - Collection.
*
*     Revenue requires for the premiums to be accounted for as
* soon as they are due, the contra entry for the Revenue Account
* postings being made to a 'PREMIUMS DUE' account.
*
* (Collection (B5353) debits the Suspense account and updates the
* control account as soon as the  money is available).
*
* PROCESSING.
* ----------
*
*     The LINR records have been selected from the LINSPF in
* B5350 - the Revenue Accounting splitter program.
*
* - All tables except for T5688 are read once for their
*   respective revenue accounting entries.
*
* - All the contract types from T5688 are loaded into an array.
*
* - For each LINR record, increment the 'records read' count
*   and set up the WSYS- error fields.
*
* - We must softlock the contract first before a READH on the
*   contract file.  If it is locked, the next LINR is read.
*
* - Read the CHDR and validate the statii against the T5679. If
*   it is invalid read the next LINR.  Increment the TRANNO.
*
* ( The  validating of the contract does not have to be done
*   if there are several LINRs for the same contract.  The READH,
*   of the CHDRLIF, however does, as the rewrite of the contract
*   at the end of the LINR processing releases the hold.)
*
* - Post the total premium (INSTAMT06) to the Premiums due.
*
* - Post all other INSTAMTS to their appropriate Revenue Accounts
*
* - REWRT the contract header with the new TRANNO and LINR
*   INSTFROM date, and update the DUEFLG on the LINSRNL file.
*
* - Write a PTRN for this transaction.
*
* Control totals used in this program:
*
*    01  -  No. of LINS  records read
*    02  -  Gross Prem Comp level acct
*    03  -  No. LINS recs produced.
*    04  -  Total Net Premium Due
*    05  -  Gross Prems Contract lvl
*    06  -  Total amount of fees
*    07  -  Total amount waived
*    08  -  No. of LINS CHDR's locked
*    09  -  No Invalid LINS CHDR's
*    10  -  No of COVR's not posted
*    11  -  No of COVR's posted
*
*****************************************************************
* </pre>
*/
public class B5351 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5351");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/*  LINR parameters*/
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaLinrFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaLinrFn, 0, FILLER).init("LINR");
	private FixedLengthStringData wsaaLinrRunid = new FixedLengthStringData(2).isAPartOf(wsaaLinrFn, 4);
	private ZonedDecimalData wsaaLinrJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaLinrFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
		/* WSAA-MISC-SUBSCRIPTS */
	protected ZonedDecimalData wsaaGlSub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaT5679Sub = new ZonedDecimalData(2, 0).setUnsigned();

		/*  Status indicators*/
	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");

	private FixedLengthStringData wsaaDateFound = new FixedLengthStringData(1).init("N");
	private Validator dateFound = new Validator(wsaaDateFound, "Y");
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuff = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/*  Storage for T5688 table items.*/
	//private static final int wsaaT5688Size = 60;
	//ILIFE-2628 fixed--Array size increased 
	private static final int wsaaT5688Size = 1000;

		/* WSAA-T5688-ARRAY */
	private FixedLengthStringData[] wsaaT5688Rec = FLSInittedArray (1000, 10);
	private FixedLengthStringData[] wsaaT5688Key = FLSDArrayPartOfArrayStructure(3, wsaaT5688Rec, 0);
	private FixedLengthStringData[] wsaaT5688Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5688Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5688Data = FLSDArrayPartOfArrayStructure(7, wsaaT5688Rec, 3);
	private PackedDecimalData[] wsaaT5688Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5688Data, 0);
	private FixedLengthStringData[] wsaaT5688Comlvlacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 5);
	private FixedLengthStringData[] wsaaT5688Revacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 6);

	private FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(97);
	private FixedLengthStringData wsysLinrkey = new FixedLengthStringData(20).isAPartOf(wsysSystemErrorParams, 0);
	private FixedLengthStringData wsysChdrcoy = new FixedLengthStringData(2).isAPartOf(wsysLinrkey, 0);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysLinrkey, 3);
	private ZonedDecimalData wsysBillcd = new ZonedDecimalData(8, 0).isAPartOf(wsysLinrkey, 12).setUnsigned();
	private FixedLengthStringData wsysSysparams = new FixedLengthStringData(77).isAPartOf(wsysSystemErrorParams, 20);
		/* ERRORS */
	private static final String h791 = "H791";
	private static final String f781 = "F781";
	private static final String ivrm = "IVRM";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5679 = "T5679";
	private static final String t5688 = "T5688";
	private static final String t5645 = "T5645";
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	protected Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private T5679rec t5679rec = new T5679rec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();

	private Map<String, List<Itempf>> t5688Map = null;
	private Map<String, List<Itempf>> tr52eMap = null;
	private Map<String, List<Itempf>> tr52dMap = null;
	private int ct01Value = 0;
	private BigDecimal ct02Value = BigDecimal.ZERO;
	private int ct03Value = 0;
	private BigDecimal ct04Value = BigDecimal.ZERO;
	private BigDecimal ct05Value = BigDecimal.ZERO;
	private BigDecimal ct06Value = BigDecimal.ZERO;
	private BigDecimal ct07Value = BigDecimal.ZERO;
	private int ct08Value = 0;
	private int ct09Value = 0;
	private int ct10Value = 0;
	private int ct11Value = 0;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private LinrpfDAO linrpfDAO = getApplicationContext().getBean("linrpfDAO", LinrpfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private LinspfDAO linspfDAO = getApplicationContext().getBean("linspfDAO", LinspfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private TaxdpfDAO taxdpfDAO = getApplicationContext().getBean("taxdpfDAO", TaxdpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	
	private String payrCurr = null;
	protected Linspf linsrnlIO;
	private Iterator<Linrpf> iteratorList;
	private Linrpf linrpfRec;
	protected Chdrpf chdrlifIO = null;
	private int intBatchID = 0;
	private int intBatchExtractSize;
	private int wsaaT5688Ix = 1;
	private Map<String, List<Chdrpf>> chdrLifMap;
	private Map<String, List<Payrpf>> payrMap;
	private Map<String, List<Linspf>> linsrnlMap;
	private Map<String, List<Covrpf>> covrenqMap;
	private Map<String, List<Taxdpf>> taxdbilMap;
	
	private List<Chdrpf> updateChdrlifList = null;
	private List<Ptrnpf> insertPtrnpfList = null;
	private List<Linspf> updateLinsrnlList = null;
	private List<Taxdpf> updateTaxdList = null;
	private Boolean japanBilling = false;
	private static final String BTPRO027 = "BTPRO027";
	
	public B5351() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(), 3)) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*  Point to correct member of LINRPF.*/
		varcom.vrcmDate.set(getCobolDate());
		wsaaLinrRunid.set(bprdIO.getSystemParam04());
		wsaaLinrJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		linrpfRec = new Linrpf();
		linrpfRec.setChdrnum("");
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				intBatchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
		readChunkRecord();
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.batccoy.set(batcdorrec.company);
		lifacmvrec.rldgcoy.set(batcdorrec.company);
		lifacmvrec.genlcoy.set(batcdorrec.company);
		lifacmvrec.batcactyr.set(batcdorrec.actyear);
		lifacmvrec.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec.batcbrn.set(batcdorrec.branch);
		lifacmvrec.batctrcde.set(bprdIO.getAuthCode());
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.tranref.set(linrpfRec.getChdrnum());
		lifacmvrec.user.set(0);
		descIO.setStatuz(varcom.oK);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(batcdorrec.company);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(bprdIO.getAuthCode());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		/*  Look up the valid contract and coverage statuii.*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(batcdorrec.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFormat("ITEMREC");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* Read T5645 for ITEMSEQ = ' ' ONLY - since we only need the*/
		/* first 7 entries.*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(batcdorrec.company);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/*  Load contract type details.*/
		t5688Map = itemDAO.loadSmartTable("IT", batcdorrec.company.toString(), "T5688");
		wsaaT5688Ix = 1;
		loadT56881100();
		japanBilling = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), BTPRO027, appVars, "IT");
		tr52eMap = itemDAO.loadSmartTable("IT", batcdorrec.company.toString(), "TR52E");
		
		tr52dMap = itemDAO.loadSmartTable("IT", batcdorrec.company.toString(), "TR52D");
	}

protected void loadT56881100()
	{
		if (t5688Map != null) {
			for (List<Itempf> itemList : t5688Map.values()) {
				for (Itempf i : itemList) {
					t5688rec.t5688Rec.set(StringUtil.rawToString(i.getGenarea()));
					wsaaT5688Itmfrm[wsaaT5688Ix].set(i.getItmfrm());
					wsaaT5688Cnttype[wsaaT5688Ix].set(i.getItemitem());
					wsaaT5688Comlvlacc[wsaaT5688Ix].set(t5688rec.comlvlacc);
					wsaaT5688Revacc[wsaaT5688Ix].set(t5688rec.revacc);
					wsaaT5688Ix++;
				}
				if (isGT(wsaaT5688Ix, wsaaT5688Size)) {
					syserrrec.params.set(t5688);
					syserrrec.statuz.set(h791);
					fatalError600();
				}
			}
		}
	}

protected void readFile2000()
	{
		if (iteratorList != null && iteratorList.hasNext()) {
			linrpfRec = iteratorList.next();
			ct01Value++;
			wsysChdrcoy.set(linrpfRec.getChdrcoy());
			wsysChdrnum.set(linrpfRec.getChdrnum());
		} else {
			intBatchID++;
			clearList2100();
			readChunkRecord();
			if (iteratorList != null && iteratorList.hasNext()) {
				linrpfRec = iteratorList.next();			
				ct01Value++;
				wsysChdrcoy.set(linrpfRec.getChdrcoy());
				wsysChdrnum.set(linrpfRec.getChdrnum());
			} else {
				wsspEdterror.set(varcom.endp);
				return ;
			}
		}
	}

	private void clearList2100() {
		if (chdrLifMap != null) {
			chdrLifMap.clear();
		}
		if (payrMap != null) {
			payrMap.clear();
		}
		if (linsrnlMap != null) {
			linsrnlMap.clear();
		}
		if (covrenqMap != null) {
			covrenqMap.clear();
		}
		if (taxdbilMap != null) {
			taxdbilMap.clear();
		}
	}

	private void readChunkRecord() {
		List<Linrpf> linrpfList = linrpfDAO.searchLinrpfRecord(wsaaLinrFn.toString(), wsaaThreadMember.toString(),
				intBatchExtractSize, intBatchID);
		iteratorList = linrpfList.iterator();
		if (linrpfList != null && !linrpfList.isEmpty()) {
			List<String> chdrnumSet = new ArrayList<>();
			for (Linrpf l : linrpfList) {
				chdrnumSet.add(l.getChdrnum());
			}
			chdrLifMap = chdrpfDAO.searchChdrRecordByChdrnum(chdrnumSet);
			payrMap = payrpfDAO.searchPayrRecordByChdrnumChdrcoy(bsprIO.getCompany().trim(),chdrnumSet);
			//linsrnlMap = linspfDAO.searchLinsrnlRecord(chdrnumSet);
			linsrnlMap = searchLinsrnlRecord(chdrnumSet);
			covrenqMap = covrpfDAO.searchValidCovrByChdrnum(chdrnumSet);
			taxdbilMap = taxdpfDAO.searchTaxdbilRecord(chdrnumSet);
		}
	}
	
	protected Map<String, List<Linspf>> searchLinsrnlRecord(List<String> chdrnumSet ){
		return linspfDAO.searchLinsrnlRecord(chdrnumSet);
	}


protected void edit2500()
	{
		wsspEdterror.set(varcom.oK);
		softlockContract2100();
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			conlogrec.error.set(sftlockrec.statuz);
			conlogrec.params.set(sftlockrec.sftlockRec);
			callConlog003();
			ct08Value++;
			wsspEdterror.set(SPACES);
			return ;
		}
		readChdr2200();
		if(japanBilling && (isEQ(chdrlifIO.getPtdate(),ZERO) || isLTE(chdrlifIO.getPtdate(),chdrlifIO.getOccdate()))) {
			ct08Value++;
	        wsspEdterror.set(SPACES);
			return ;
		}
		
		if (isNE(linrpfRec.getChdrnum(), wsaaPrevChdrnum)) {
			validateContract2300();
			if (!validContract.isTrue()) {
				ct09Value++;
				wsspEdterror.set(SPACES);
				return ;
			}
		}
		readPayr2400();
		
	}


protected void softlockContract2100()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.enttyp.set("CH");
		sftlockrec.company.set(batcdorrec.company);
		sftlockrec.user.set(0);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.function.set("LOCK");
		sftlockrec.entity.set(linrpfRec.getChdrnum());
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			wsysSysparams.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(wsysSystemErrorParams);
			fatalError600();
		}
	}


protected void readChdr2200()
	{
		/*START*/
		if(chdrLifMap!=null &&chdrLifMap.containsKey(linrpfRec.getChdrnum())){
			for(Chdrpf c:chdrLifMap.get(linrpfRec.getChdrnum())){
				if(c.getChdrcoy().toString().equals(linrpfRec.getChdrcoy())){
					chdrlifIO = c;
					break;
				}
			}
		}
		if(chdrlifIO == null){
			syserrrec.params.set(linrpfRec.getChdrnum());
			fatalError600();
		}
		wsysBillcd.set(chdrlifIO.getBillcd());
		chdrlifIO.setTranno(chdrlifIO.getTranno()+1);
		/*EXIT1*/
	}

protected void validateContract2300()
	{
		wsaaT5688Ix = 1;
		 searchlabel1:
		{
			for (; isLT(wsaaT5688Ix, wsaaT5688Rec.length); wsaaT5688Ix++){
				if (isEQ(wsaaT5688Key[wsaaT5688Ix], chdrlifIO.getCnttype())) {
					break searchlabel1;
				}
			}
			syserrrec.statuz.set(f781);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(t5688);
			stringVariable1.addExpression(wsysSystemErrorParams);
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();
		}
		/*  We must find the effective T5688 entry for the contract*/
		/*  (T5688 entries will have been loaded in descending sequence*/
		/*  into the array).*/
		while ( !(isNE(chdrlifIO.getCnttype(), wsaaT5688Cnttype[wsaaT5688Ix])
		|| isGT(wsaaT5688Ix, wsaaT5688Size)
		|| dateFound.isTrue())) {
			if (isGTE(chdrlifIO.getOccdate(), wsaaT5688Itmfrm[wsaaT5688Ix])) {
				wsaaDateFound.set("Y");
			}
			else {
				wsaaT5688Ix++;
			}
		}
	
		if (!dateFound.isTrue()) {
			syserrrec.params.set(wsysSystemErrorParams);
			fatalError600();
		}
		/*  Validate the statii of the contract*/
		wsaaValidContract.set("N");
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Sub.toInt()], chdrlifIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.cnPremStat[wsaaT5679Sub.toInt()], chdrlifIO.getPstcde())) {
						wsaaT5679Sub.set("13");
						wsaaValidContract.set("Y");
					}
				}
			}
		}
		/*  We can now store this contract so that any subsequent LINRs*/
		/*  for the same contract need not have the contract locked, read*/
		/*  and re-validated.*/
		wsaaPrevChdrnum.set(linrpfRec.getChdrnum());
	}

protected void readPayr2400()
 {
		if (payrMap != null && payrMap.containsKey(linrpfRec.getChdrnum())) {
			for (Payrpf p : payrMap.get(linrpfRec.getChdrnum())) {
				if (p.getChdrcoy().equals(linrpfRec.getChdrcoy())) {
					payrCurr = p.getCntcurr();
					break;
				}
			}
		}
	}


protected void update3000()
	{
		/*UPDATE*/
		readLins3100();
		
		acmvPostings3200();
		rewrtChdrLins3300();
		a000PostTax();
		writePtrn3400();
		unlkChdr3500();
		/*EXIT*/
	}

protected void readLins3100()
	{
		if(linsrnlMap!=null&&linsrnlMap.containsKey(linrpfRec.getChdrnum())){
			for(Linspf l:linsrnlMap.get(linrpfRec.getChdrnum())){
				if(linrpfRec.getChdrcoy().equals(l.getChdrcoy())&&linrpfRec.getInstfrom() == l.getInstfrom()){
					linsrnlIO = l;
					rdLins3100CustomerSpecific(l);
					break;
				}
			}
		}
	}
protected void rdLins3100CustomerSpecific(Linspf l){
	
}

protected void acmvPostings3200()
	{
		/*  LIFA-RLDGACCT must be re-initialised in case the last entry*/
		/*  was for component level accounting.*/
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rldgacct.set(linrpfRec.getChdrnum());
		/*  Post the Premium-due.*/
		if (BigDecimal.ZERO.compareTo(linsrnlIO.getInstamt06()) != 0) {
			lifacmvrec.origamt.set(linsrnlIO.getInstamt06());
			wsaaGlSub.set(1);
			callLifacmv5000();
			ct04Value = ct04Value.add(linsrnlIO.getInstamt06());
		}
		/*  Post the Premium-Income.*/
		if (linsrnlIO.getInstamt01().compareTo(BigDecimal.ZERO)>0
			//MIBT-266 STARTS
			//&& isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix], "N"))
			//MIBT-266 ENDS
			&& isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix], " ")){
			lifacmvrec.origamt.set(linsrnlIO.getInstamt01());
			wsaaGlSub.set(2);
			callLifacmv5000();
			ct05Value = ct05Value.add(linsrnlIO.getInstamt01());
		}
		/*  Post the Fees.*/
		
		/*  Post the Tolerance.*/
		postCustomerSpecific3210();
		/*  Post the Stamp-Duty.*/
		if (linsrnlIO.getInstamt04().compareTo(BigDecimal.ZERO)>0) {
			lifacmvrec.origamt.set(linsrnlIO.getInstamt04());
			wsaaGlSub.set(5);
		}
		/*  Post the Waiver-holding.*/
		if (BigDecimal.ZERO.compareTo(linsrnlIO.getInstamt05())!=0) {
			lifacmvrec.origamt.set(linsrnlIO.getInstamt05());
			wsaaGlSub.set(6);
			callLifacmv5000();
			/*  Log total amount of waiver*/
			ct07Value = ct07Value.add(linsrnlIO.getInstamt05());
		}
		/*  Create a posting for every COVR-INSTPREMs for component*/
		/*  level accounted contracts only.*/
		if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix], "Y")) {
			if(covrenqMap!=null&&covrenqMap.containsKey(linrpfRec.getChdrnum())){
				for(Covrpf c:covrenqMap.get(linrpfRec.getChdrnum())){
					if(linrpfRec.getChdrcoy().equals(c.getChdrcoy())){
						covrAcmvs3250(c);
					}
				}
			}
		}
		postFeeChrgCustomerSpecific();

	}
protected void postCustomerSpecific3210(){
	if (linsrnlIO.getInstamt02().compareTo(BigDecimal.ZERO)>0) {
		lifacmvrec.origamt.set(linsrnlIO.getInstamt02());
		wsaaGlSub.set(3);
		callLifacmv5000();
		/*  Log total amount of fees*/
		ct06Value = ct06Value.add(linsrnlIO.getInstamt02());
	}
	
	if (linsrnlIO.getInstamt03().compareTo(BigDecimal.ZERO)>0) {
		lifacmvrec.origamt.set(linsrnlIO.getInstamt03());
		wsaaGlSub.set(4);
		callLifacmv5000();
	}
}
protected void covrAcmvs3250(Covrpf covrenqIO)
	{
		wsaaValidCoverage.set("N");
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covrenqIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], covrenqIO.getPstatcode())) {
						wsaaT5679Sub.set("13");
						wsaaValidCoverage.set("Y");
					}
				}
			}
		}
		if (validCoverage.isTrue()
		&& BigDecimal.ZERO.compareTo(covrenqIO.getInstprem())!=0) {
			lifacmvrec.origamt.set(covrenqIO.getInstprem());
			wsaaRldgChdrnum.set(covrenqIO.getChdrnum());
			wsaaRldgLife.set(covrenqIO.getLife());
			wsaaRldgCoverage.set(covrenqIO.getCoverage());
			wsaaRldgRider.set(covrenqIO.getRider());
			wsaaPlan.set(covrenqIO.getPlanSuffix());
			lifacmvrec.substituteCode[6].set(covrenqIO.getCrtable());
			wsaaRldgPlanSuff.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			wsaaGlSub.set(7);
			callLifacmv5000();
			/*  Log gross premiums - - comp level*/
			ct02Value = ct02Value.add(covrenqIO.getInstprem());
			/*  Log no. of COVRS posted*/
			ct11Value++;
		}
		else {
			/*  Log no. of COVRS not posted*/
			ct10Value++;
		}
	}

protected void callLifacmv5000()
	{
		/* These LIFACMV parameters are the same for both instalment*/
		/* and coverage posting.  The correct T5645 entries are pointed*/
		/* to by the value in the subscript WSAA-GL-SUB.*/
		lifacmvrec.rdocnum.set(linrpfRec.getChdrnum());
		lifacmvrec.tranref.set(linrpfRec.getChdrnum());
		lifacmvrec.origcurr.set(payrCurr);
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.jrnseq.add(1);
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaGlSub.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaGlSub.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaGlSub.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaGlSub.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaGlSub.toInt()]);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}

	}

protected void rewrtChdrLins3300()
	{
		if(updateChdrlifList == null){
			updateChdrlifList = new ArrayList<>();
		}
		Chdrpf c = new Chdrpf(chdrlifIO);
		c.setUniqueNumber(chdrlifIO.getUniqueNumber());
		updateChdrlifList.add(c);
		
		/* Update the instalment DUEFLG.*/
		if(updateLinsrnlList == null){
			updateLinsrnlList = new ArrayList<>();
		}
		linsrnlIO.setDueflg("Y");
		updateLinsrnlList.add(linsrnlIO);
		/*  Log no. LINS record processed*/
		ct03Value++;
	}


protected void writePtrn3400()
	{
		Ptrnpf ptrnIO = new Ptrnpf();
		ptrnIO.setChdrcoy(linrpfRec.getChdrcoy());
		ptrnIO.setChdrnum(linrpfRec.getChdrnum());
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setTrtm(varcom.vrcmTime.toInt());
		ptrnIO.setPtrneff(linsrnlIO.getInstfrom());
		/* MOVE BSSC-EFFECTIVE-DATE     TO PTRN-TRANSACTION-DATE.       */
		ptrnIO.setTrdt(varcom.vrcmDate.toInt());
		ptrnIO.setUserT(0);
		ptrnIO.setBatccoy(batcdorrec.company.toString());
		ptrnIO.setBatcbrn(batcdorrec.branch.toString());
		ptrnIO.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnIO.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnIO.setBatctrcde(bprdIO.getAuthCode().toString());
		ptrnIO.setBatcbatch(batcdorrec.batch.toString());
		ptrnIO.setBatcpfx(batcdorrec.prefix.toString());
		ptrnIO.setValidflag("1");				   
		ptrnIO.setDatesub(bsscIO.getEffectiveDate().toInt());
		if(insertPtrnpfList == null){
			insertPtrnpfList = new ArrayList<>();
		}
		insertPtrnpfList.add(ptrnIO);
	}


protected void unlkChdr3500()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.enttyp.set("CH");
		sftlockrec.company.set(batcdorrec.company);
		sftlockrec.user.set(0);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.entity.set(linrpfRec.getChdrnum());
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			wsysSysparams.set(sftlockrec.sftlockRec);
			syserrrec.params.set(wsysSystemErrorParams);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void commit3500()
 {
		/* COMMIT */
		/** Place any additional commitment processing in here. */
		commitControlTotals();
		if (updateChdrlifList != null && !updateChdrlifList.isEmpty()) {
			chdrpfDAO.updateChdrTrannoByUniqueNo(updateChdrlifList);
			updateChdrlifList.clear();
		}
		if (insertPtrnpfList != null && !insertPtrnpfList.isEmpty()) {
			ptrnpfDAO.insertPtrnPF(insertPtrnpfList);
			insertPtrnpfList.clear();
		}
		if (updateLinsrnlList != null && !updateLinsrnlList.isEmpty()) {
			linspfDAO.updateLinspfDueflg(updateLinsrnlList);
			updateLinsrnlList.clear();
		}
		if (updateTaxdList != null && !updateTaxdList.isEmpty()) {
			taxdpfDAO.updateTaxdpf(updateTaxdList);
			updateTaxdList.clear();
		}
		/* EXIT */
	}
private void commitControlTotals(){
	contotrec.totno.set(1);
	contotrec.totval.set(ct01Value);
	callProgram(Contot.class, contotrec.contotRec);		
	ct01Value = 0;

	contotrec.totno.set(2);
	contotrec.totval.set(ct02Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct02Value = BigDecimal.ZERO;
	
	contotrec.totno.set(3);
	contotrec.totval.set(ct03Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct03Value = 0;
	
	contotrec.totno.set(4);
	contotrec.totval.set(ct04Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct04Value = BigDecimal.ZERO;
	
	contotrec.totno.set(5);
	contotrec.totval.set(ct05Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct05Value = BigDecimal.ZERO;
	
	contotrec.totno.set(6);
	contotrec.totval.set(ct06Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct06Value = BigDecimal.ZERO;
	
	contotrec.totno.set(7);
	contotrec.totval.set(ct07Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct07Value = BigDecimal.ZERO;
	
	contotrec.totno.set(8);
	contotrec.totval.set(ct08Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct08Value = 0;
	
	contotrec.totno.set(9);
	contotrec.totval.set(ct09Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct09Value = 0;
	
	contotrec.totno.set(10);
	contotrec.totval.set(ct10Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct10Value = 0;
	
	contotrec.totno.set(11);
	contotrec.totval.set(ct11Value);
	callProgram(Contot.class, contotrec.contotRec);	
	ct11Value = 0;
}
protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*START*/
		wsaaQcmdexc.set("DLTOVR FILE(LINRPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void a000PostTax()
 {
		/* Read table TR52D */
		Itempf item = null;
		if (tr52dMap != null) {
			if (tr52dMap.containsKey(chdrlifIO.getReg())) {
				item = tr52dMap.get(chdrlifIO.getReg()).get(0);
			} else if (tr52dMap.containsKey("***")) {
				item = tr52dMap.get("***").get(0);
			}
		}
		if (item == null) {
			syserrrec.params.set("tr52d:" + chdrlifIO.getReg());
			fatalError600();
		} else {//IJTI-320 START
			tr52drec.tr52dRec.set(StringUtil.rawToString(item.getGenarea()));
		}
		//IJTI-320 END
		/* Read TAXD records */
		if (taxdbilMap != null && taxdbilMap.containsKey(linrpfRec.getChdrnum())) {
			for (Taxdpf t : taxdbilMap.get(linrpfRec.getChdrnum())) {
				if (linrpfRec.getChdrcoy().equals(t.getChdrcoy()) && linrpfRec.getInstfrom() == t.getInstfrom()) {
					a100ProcessTax(t);
				}
			}
		}
	}

protected void a100ProcessTax(Taxdpf taxdbilIO)
	{
		if (isNE(taxdbilIO.getPostflg(), SPACES)) {
			return ;
		}
		/* Read table TR52E                                                */
		String tranref = subString(taxdbilIO.getTranref(), 1, 8).toString();
		Itempf tr52eItem = null;
		if(tr52eMap != null && tr52eMap.containsKey(tranref)){
			for(Itempf item:tr52eMap.get(tranref)){
				if (item.getItmfrm().compareTo(new BigDecimal(taxdbilIO.getEffdate())) <= 0) {
					tr52eItem = item;
					break;
				}
			}
		}else{
			syserrrec.params.set("TR52E:"+tranref);
			fatalError600();
		}
		if(tr52eItem == null){
			syserrrec.params.set("TR52E:"+tranref);
			fatalError600();
		}
		tr52erec.tr52eRec.set(StringUtil.rawToString(tr52eItem.getGenarea()));
		
		/* Call tax subroutine                                             */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("POST");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(taxdbilIO.getChdrcoy());
		txcalcrec.chdrnum.set(taxdbilIO.getChdrnum());
		txcalcrec.life.set(taxdbilIO.getLife());
		txcalcrec.coverage.set(taxdbilIO.getCoverage());
		txcalcrec.rider.set(taxdbilIO.getRider());
		txcalcrec.planSuffix.set(taxdbilIO.getPlansfx());
		/* get the component detail.                                      */
		Covrpf covrenqIO = null;
		if (isNE(txcalcrec.coverage, SPACES)) {
			if (covrenqMap != null
					&& covrenqMap.containsKey(linrpfRec.getChdrnum())) {
				for (Covrpf c : covrenqMap.get(taxdbilIO.getChdrnum())) {
					if (c.getChdrcoy().equals(taxdbilIO.getChdrcoy())
							&& c.getLife().equals(taxdbilIO.getLife())
							&& c.getCoverage().equals(taxdbilIO.getCoverage())
							&& c.getRider().equals(taxdbilIO.getRider())
							&& 0 == c.getPlanSuffix()) {
						covrenqIO = c;
						break;
					}
				}
			}
			if (covrenqIO == null) {
				syserrrec.params.set(taxdbilIO.getChdrnum());
				fatalError600();
			}
		}
		if (isEQ(taxdbilIO.getCoverage(), SPACES)) {
			txcalcrec.crtable.set(SPACES);
			txcalcrec.cntTaxInd.set("Y");
		}
		else {
			//IJTI-320 START
			if(covrenqIO != null ){
				txcalcrec.crtable.set(covrenqIO.getCrtable());
			}
			//IJTI-320 END
			txcalcrec.cntTaxInd.set(" ");
		}
		txcalcrec.cnttype.set(chdrlifIO.getCnttype());
		txcalcrec.register.set(chdrlifIO.getReg());
		txcalcrec.taxrule.set(subString(taxdbilIO.getTranref(), 1, 8));
		txcalcrec.rateItem.set(subString(taxdbilIO.getTranref(), 9, 8));
		txcalcrec.amountIn.set(taxdbilIO.getBaseamt());
		txcalcrec.effdate.set(taxdbilIO.getEffdate());
		txcalcrec.transType.set(taxdbilIO.getTrantype());
		txcalcrec.taxType[1].set(taxdbilIO.getTxtype01());
		txcalcrec.taxType[2].set(taxdbilIO.getTxtype02());
		txcalcrec.taxAmt[1].set(taxdbilIO.getTaxamt01());
		txcalcrec.taxAmt[2].set(taxdbilIO.getTaxamt02());
		txcalcrec.taxAbsorb[1].set(taxdbilIO.getTxabsind01());
		txcalcrec.taxAbsorb[2].set(taxdbilIO.getTxabsind02());
		txcalcrec.batckey.set(batcdorrec.batchkey);
		txcalcrec.ccy.set(chdrlifIO.getCntcurr());
		txcalcrec.language.set(bsscIO.getLanguage());
		txcalcrec.tranno.set(chdrlifIO.getTranno());
		txcalcrec.jrnseq.set(0);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		/* Update TAXD record                                              */
		/*                                                         <LA4758>*/
		/*                                                         <LA4758>*/
		/*        PERFORM 600-FATAL-ERROR                          <LA4758>*/
		/*     END-IF.                                             <LA4758>*/
		if(updateTaxdList == null){
			updateTaxdList = new ArrayList<>();
		}
		Taxdpf t = new Taxdpf();
		t.setUnique_number(taxdbilIO.getUnique_number());
		t.setPostflg("P");
		updateTaxdList.add(t);
	}

protected void postFeeChrgCustomerSpecific(){
	
}
}
