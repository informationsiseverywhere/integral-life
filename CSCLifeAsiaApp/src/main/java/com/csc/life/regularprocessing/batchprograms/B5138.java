/*
 * File: B5138.java
 * Date: 29 August 2009 20:59:38
 * Author: Quipoz Limited
 * 
 * Class transformed from B5138.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.Iterator;
import java.util.List;

import com.csc.life.regularprocessing.dataaccess.dao.AinrpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Ainrpf;
import com.csc.life.regularprocessing.reports.R5138Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*          FACULTATIVE REASSURANCE REPORT
*          -----------------------------
*
* This batch program is part of the Automatic Increases multi-
* thread batch suite.  It runs directly after the Actual Automatic
* Increases batch program (B5137) which has written details of the
* Facultative Reassurance Records which were automatically
* increased to the AINRPF file. This program produces a report on
* these records.
*
* The program reads all the records from the AINRPF file where the
* record type equals system parameter 3 on the process definition
* and the record company is equal to the batch run company.
* System parameter 3 should be set accordingly to ensure that only
* actual increase records are processed.
*
*       The following fields are extracted;
*       CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLAN-SUFFIX
*       CRTABLE, REASSURANCE CESSION COMMENCE,
*       REASSURANCE ACCOUNT NUMBER, ARRANGEMNENT CODE FOR
*       HIERARCHY, REASSURANCE AMOUNT
*
*       The selected records will be in contract and
*       coverage order.
*
*   Control totals:
*     01  -  Number of pages printed
*     02  -  Number of records printed
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class B5138 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R5138Report printerFile = new R5138Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5138");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String descrec = "DESCREC";
	private String ainrrec = "AINRREC";
		/* TABLES */
	private String t1692 = "T1692";
	private String t1693 = "T1693";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
		/* ERRORS */
	private String ivrm = "IVRM";

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);

	private FixedLengthStringData wsDetailWritten = new FixedLengthStringData(1);
	private Validator detailWritten = new Validator(wsDetailWritten, "Y");
	private Validator detailNotWritten = new Validator(wsDetailWritten, "N");
	private PackedDecimalData wsaaTransTime = new PackedDecimalData(6, 0);

	private FixedLengthStringData r5138H01 = new FixedLengthStringData(73);
	private FixedLengthStringData r5138h01O = new FixedLengthStringData(73).isAPartOf(r5138H01, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5138h01O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5138h01O, 1);
	private FixedLengthStringData sdate = new FixedLengthStringData(10).isAPartOf(r5138h01O, 31);
	private FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(r5138h01O, 41);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30).isAPartOf(r5138h01O, 43);

	private FixedLengthStringData r5138D01 = new FixedLengthStringData(61);
	private FixedLengthStringData r5138d01O = new FixedLengthStringData(61).isAPartOf(r5138D01, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5138d01O, 0);
	private FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(r5138d01O, 8);
	private FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(r5138d01O, 12);
	private FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(r5138d01O, 14);
	private FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(r5138d01O, 16);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0).isAPartOf(r5138d01O, 18);
	private FixedLengthStringData cmdate = new FixedLengthStringData(10).isAPartOf(r5138d01O, 22);
	private FixedLengthStringData rasnum = new FixedLengthStringData(8).isAPartOf(r5138d01O, 32);
	private FixedLengthStringData rngmnt = new FixedLengthStringData(4).isAPartOf(r5138d01O, 40);
	private ZonedDecimalData raamount = new ZonedDecimalData(17, 2).isAPartOf(r5138d01O, 44);

	private FixedLengthStringData r5138M01 = new FixedLengthStringData(2);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Auto Increase Reporting Logical View*/
//	private AinrTableDAM ainrIO = new AinrTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	   
	private AinrpfDAO ainrpfDAO = getApplicationContext().getBean("ainrpfDAO", AinrpfDAO.class);
    private int ct01Value = 0;
    private int ct02Value = 0;

    private Iterator<Ainrpf> iteratorList; 
    private int intBatchID = 0;
    private int intBatchExtractSize;
    private Ainrpf ainrIO = null;
    
	public B5138() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingCompany1020();
		setUpHeadingBranch1030();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		wsaaTransTime.set(getCobolTime());
		wsaaOverflow.set("Y");
		detailNotWritten.setTrue();
	}

protected void setUpHeadingCompany1020()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		company.set(bsprIO.getCompany());
		companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch1030()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		descIO.setDescitem(bprdIO.getDefaultBranch());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		branch.set(bprdIO.getDefaultBranch());
		branchnm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		sdate.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(bsscIO.getEffectiveDate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		sdate.set(datcon1rec.extDate);
        if (bprdIO.systemParam01.isNumeric()) {
            if (bprdIO.systemParam01.toInt() > 0) {
                intBatchExtractSize = bprdIO.systemParam01.toInt();
            } else {
                intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
            }
        } else {
            intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
        }
        List<Ainrpf> ainrpfList = ainrpfDAO.searchAinrpfRecord(bsprIO.getCompany().toString(), bprdIO.getSystemParam03().toString(),
                intBatchExtractSize, intBatchID);
        iteratorList = ainrpfList.iterator();
	}

protected void readFile2000()
 {
        if (!iteratorList.hasNext()) {
            intBatchID++;
            List<Ainrpf> ainrpfList = ainrpfDAO.searchAinrpfRecord(wsaaCompany.toString(), bprdIO.getSystemParam03().toString(),
                    intBatchExtractSize, intBatchID);
            iteratorList = ainrpfList.iterator();
            if (!iteratorList.hasNext()) {
                wsspEdterror.set(varcom.endp);
                return;
            }
        }
        ainrIO = iteratorList.next();
    }

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		if (newPageReq.isTrue()) {
			ct01Value++;
			printerFile.printR5138h01(r5138H01, indicArea);
			wsaaOverflow.set("N");
		}
		writeDetail3200();
		/*EXIT*/
	}

protected void writeDetail3200()
	{
        datcon1rec.function.set("CONV");
        datcon1rec.intDate.set(ainrIO.getCmdate());
        Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
        if (isNE(datcon1rec.statuz,varcom.oK)) {
            syserrrec.statuz.set(datcon1rec.statuz);
            syserrrec.params.set(datcon1rec.datcon1Rec);
            fatalError600();
        }
        cmdate.set(datcon1rec.extDate);
        crtable.set(ainrIO.getCrtable());
        chdrnum.set(ainrIO.getChdrnum());
        life.set(ainrIO.getLife());
        coverage.set(ainrIO.getCoverage());
        rider.set(ainrIO.getRider());
        plnsfx.set(ainrIO.getPlanSuffix());
        rasnum.set(ainrIO.getRasnum());
        rngmnt.set(ainrIO.getRngmnt());
        raamount.set(ainrIO.getRaAmount());
        ct02Value++;
        printerFile.printR5138d01(r5138D01, indicArea);
        detailWritten.setTrue();

	}

protected void commit3500()
	{
        contotrec.totno.set(ct01);
        contotrec.totval.set(ct01Value);
        callContot001();
        ct01Value = 0;
        contotrec.totno.set(ct02);
        contotrec.totval.set(ct02Value);
        callContot001();
        ct02Value = 0;
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		if (detailWritten.isTrue()) {
			printerFile.printR5138m01(r5138M01, indicArea);
		}
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
