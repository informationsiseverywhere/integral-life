/*
 * File: Br613.java
 * Date: 29 August 2009 22:24:46
 * Author: Quipoz Limited
 *
 * Class transformed from BR613.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.AgecalcPojo;
import com.csc.fsu.general.procedures.AgecalcUtils;
import com.csc.fsu.general.procedures.Datcon2Pojo;
import com.csc.fsu.general.procedures.Datcon2Utils;
import com.csc.fsu.general.procedures.Datcon3Pojo;
import com.csc.fsu.general.procedures.Datcon3Utils;
import com.csc.fsu.general.procedures.ZrdecplcPojo;
import com.csc.fsu.general.procedures.ZrdecplcUtils;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.dao.impl.CovrpfDAOImp;
import com.csc.life.enquiries.procedures.Crtundwrt;
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.linkage.procedures.LinkageInfoService;
import com.csc.life.newbusiness.dataaccess.dao.AgcmpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Agcmpf;
import com.csc.life.newbusiness.dataaccess.model.Pcddpf;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.productdefinition.dataaccess.dao.AnnypfDAO;
import com.csc.life.productdefinition.dataaccess.dao.Br613TempDAO;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.MbnspfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Annypf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Mbnspf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.VpxchdrPojo;
import com.csc.life.productdefinition.procedures.VpxchdrUtil;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.regularprocessing.dataaccess.dao.CorepfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.RertpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Corepf;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.regularprocessing.dataaccess.model.Rertpf;
import com.csc.life.regularprocessing.tablestructures.T5655rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.smart.procedures.SftlockUtil;
import com.csc.smart.recordstructures.SftlockRecBean;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 2005.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*                     Component Rerate
*                    ------------------
*
*      This program will process all of the contract header
* records selected based on the following criteria:
*
* Contract Header File
*                      -  Valid Flag           =  1
*                      -  Company              = Batch Run Company
*                      -  Branch               = Batch Run Branch
*                      -  Service Unit         =  LP
*                      -  Billing Channel  not =  N
*                      -  Contract Header number between range on
*                         parameter prompt screen (if specified).
*
*      The coverage file criteria must be met for at least one of
* the coverages attached to the contract header.
*
* Coverage File
*                      -  Valid Flag           =  1
*                      -  Rerate Date      not =  0
*                      -  Rerate Date          < Batch Run date
*                                              + highest number
*                                              of re-rate lead
*                                              days on T5655.
*
*      Firstly, the contract header will be validated for risk
* and premium status codes from T5679.  Any which do not have the
* valid risk and premium status codes will be rejected.
*      The program then reads through all of the COVR records
* linked to the contract header until it arrives at the first COVR
* record fitting the selection criteria.
*      Further validation will be performed upon the COVR record
* for risk and premium status codes and those without valid risk
* and premium status codes will be rejected.  Records will also be
* rejected when the rerate date is less than the effective date
* of the batch run plus the lead days on T5655.
*        (COVR-RRTDAT < PARM-EFFDATE + T5655-LEAD-DAYS).
*      All COVR records linked to the contract will be checked to
* see if the premium cessation date is earlier than the rerate
* date.  If this is the case, the contract header premium status
* is updated when the contract header file is rewritten.
*      Assuming that the rerate date on the COVR does not equal
* the premium cessation date, the premium will be rerated by
* calling the premium calculation subroutine on T5675 if one
* exists.
*      The COVR record is then held for updating.  If the premium
* cessation date equals the rerate date, update the premium status
* code, add 1 to Control Total 4 and leave the section. Otherwise,
* the rerate date - 1 day is moved to the current to date and the
* record is updated validflag '2'.  The LEXTBRR file is then read
* until one of the following conditions is met:
*
*    (1) - End of file or wrong record
*    (2) - Cessation date on the LEXTBRR is not equal to the
*          rerate from date on the COVR incremented by the
*          premium recalculation frequency on T5687.
*
*     When (1),
*              If premium recalculation freq. on T5687 = zeroes,
*                 move COVR premium cessation date to
*                      COVR rerate date
*              Else
*                 move incremented rerate from date to
*                      COVR rerate and rerate from dates.
*     When (2),
*              If incremented rerate from date < LEXTBRR cessation
*              date,
*                   If COVR rerate date > LEXTBBR cessation date
*                      move LEXTBBR cessation date to
*                           COVR rerate date
*                   Else
*                      continue the loop
*              Else
*                 move incremented rerate from date to
*                      COVR rerate and rerate from dates.
*
*      Write a new COVR record, updating the COVR installment
* premium if a premium method exists on T5687, moving the rerate
* date to the current from date, 'maxdate' to the current to date
* and updating the transaction number.  Add 1 to Control Total 2
* and perform the statistics section.
*      Once all of the coverages for the contract have been
* processed, update the contract header file with a valid flag '2'
* record, moving the rerate date - 1 day to the current to date.
*      Write a new valid flag '1' record with the rerate date as
* the current from date, 'maxdate' as the current to date, a new
* transaction number and update standing instalment amounts 1 and
* 6.  If all coverages (and riders) attached to the contract have
* expired, update the premium status code from T5679 and remove
* the contract fee from the contract header (subtract from
* standing instalment amounts 2 and 6).
*      Write a PTRN record for the transaction, increment Control
* Total 1 and add the difference in premiums to Control Total 3.
*      Update the PAYR file with a valid flag '2' record and a new
* PAYR record with the new premium amounts.
*
*   These remarks must be replaced by what the program actually
*     does.
*
***********************************************************************
* </pre>
*/
public class Br613 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR613");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
//	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
//	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaCoreFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaCoreFn, 0, FILLER).init("CORE");
	private FixedLengthStringData wsaaCoreRunid = new FixedLengthStringData(2).isAPartOf(wsaaCoreFn, 4);
	private ZonedDecimalData wsaaCoreJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaCoreFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCedagent = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaAgntFound = new FixedLengthStringData(1);
	private Validator agentFound = new Validator(wsaaAgntFound, "Y");
	private ZonedDecimalData wsaaInstPrem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaZbinstprem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaZlinstprem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaCommPrem = new ZonedDecimalData(17, 2);
	protected String wsaaFullyPaid = "";
	private FixedLengthStringData wsaaBillfreqx = new FixedLengthStringData(2);

	private FixedLengthStringData filler4 = new FixedLengthStringData(2).isAPartOf(wsaaBillfreqx, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaBillfreqn = new ZonedDecimalData(2, 0).isAPartOf(filler4, 0).setUnsigned();
	protected ZonedDecimalData wsaaNewTranno = new ZonedDecimalData(5, 0);

	protected FixedLengthStringData wsaaRerateType = new FixedLengthStringData(1);
	private Validator trueRerate = new Validator(wsaaRerateType, "1");
	private Validator lextRerate = new Validator(wsaaRerateType, "2");

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");
	protected String wsaaCovrUpdated = "";
	protected String wsaaCovrsLive = "";
	protected ZonedDecimalData wsaaPremDiff = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaBillDate = new ZonedDecimalData(8, 0);
	protected FixedLengthStringData wsaaZsredtrm = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	//private static final int wsaaT5655Size = 100;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5655Size = 1000;
	private PackedDecimalData wsaaT5655IxMax = new PackedDecimalData(5, 0);
	

	private FixedLengthStringData wsaaT5655Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5655Item, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT5655Item, 4);
	private String wsaaChdrValid = "";
	private String wsaaCovrValid = "";
	private ZonedDecimalData wsaaLeadDays = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaDurationInt = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaDurationRem = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaDuration = new ZonedDecimalData(2, 0).setUnsigned();
	protected PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2);
		/* WSAA-TRANID */
	private static final String wsaaTermid = "";
	private int wsaaTransactionDate = varcom.vrcmDate.toInt();//IJS-371
	private int wsaaTransactionTime = varcom.vrcmTime.toInt();
	private static final int wsaaUser = 0;

	private FixedLengthStringData wsaaT5671Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemTranCode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 0);
	private FixedLengthStringData wsaaItemCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 4);
	protected ZonedDecimalData wsaaLastRrtDate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0);
	protected ZonedDecimalData wsaaRateFrom = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaRerateDate = new ZonedDecimalData(8, 0);
	protected PackedDecimalData wsaaRerateStore = new PackedDecimalData(8, 0);

		/* WSAA-T5655-ARRAY */
	private FixedLengthStringData[] wsaaT5655Rec = FLSInittedArray (1000, 20);//ILIFE-2628 fixed--Array size increased
	private FixedLengthStringData[] wsaaT5655Key = FLSDArrayPartOfArrayStructure(7, wsaaT5655Rec, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT5655Trcde = FLSDArrayPartOfArrayStructure(4, wsaaT5655Key, 0);
	private FixedLengthStringData[] wsaaT5655Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5655Key, 4);
	private FixedLengthStringData[] wsaaT5655Data = FLSDArrayPartOfArrayStructure(13, wsaaT5655Rec, 7);
	private PackedDecimalData[] wsaaT5655Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5655Data, 0);
	private PackedDecimalData[] wsaaT5655Itmto = PDArrayPartOfArrayStructure(8, 0, wsaaT5655Data, 5);
	private ZonedDecimalData[] wsaaT5655LeadDays = ZDArrayPartOfArrayStructure(3, 0, wsaaT5655Data, 10);
	private ZonedDecimalData wsaaMaxLeadDays = new ZonedDecimalData(3, 0);

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");
		/* ERRORS */
	private static final String ivrm = "IVRM";

		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaT5655Ix = new IntegerData();

	private T5655rec t5655rec = new T5655rec();
	private T5671rec t5671rec = new T5671rec();
	protected T5687rec t5687rec = new T5687rec();
	private T5675rec t5675rec = new T5675rec();
	private Premiumrec premiumrec = new Premiumrec();
	private T5679rec t5679rec = new T5679rec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Isuallrec isuallrec = new Isuallrec();
	private Crtundwrec crtundwrec = new Crtundwrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();

	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();
	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
    private CorepfDAO corepfDAO = getApplicationContext().getBean("corepfDAO", CorepfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
    protected PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
    private IncrpfDAO incrpfDAO = getApplicationContext().getBean("incrpfDAO", IncrpfDAO.class);
    private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
    private Clntpf clntpf= new Clntpf();//ILIFE-8502
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);//ILIFE-8502
	private Iterator<Corepf> iter;
	protected int ct01Val, ct02Val, ct04Val, ct05Val;
	private ZonedDecimalData ct03Val = new ZonedDecimalData(17, 2);
	private Corepf corepfRec = new Corepf();
	protected Chdrpf chdrlifIO = new Chdrpf();
	protected Payrpf payrIO = new Payrpf();
	private com.csc.smart400framework.dataaccess.model.Clntpf clnt;//ILIFE-8502
	private com.csc.smart400framework.dataaccess.dao.ClntpfDAO clntDao;	//ILIFE-8502
	private int batchID;
    private int batchExtractSize;
    
	private Map<String, List<Chdrpf>> chdrpfMap = new HashMap<String, List<Chdrpf>>();
//  private List<Covrpf> covrpfList;
	private List<Covrpf> covrpfListInst = new ArrayList<Covrpf>();
	protected List<Covrpf> covrpfListUpdt = new ArrayList<Covrpf>();
	private Map<String, List<Payrpf>> payrpfMap = new HashMap<String, List<Payrpf>>();
	private Map<String, List<Incrpf>> incrpfMap = new HashMap<String, List<Incrpf>>();
	
	private List<Ptrnpf> ptrnpfList = new ArrayList<Ptrnpf>();
	private List<Chdrpf> chdrpfListUpdt = new ArrayList<Chdrpf>();
	private List<Chdrpf> chdrpfListInst = new ArrayList<Chdrpf>();
	private List<Payrpf> payrpfListUpdt = new ArrayList<Payrpf>();
	private List<Payrpf> payrpfListInst = new ArrayList<Payrpf>();
	private List<Incrpf> incrpfList = new ArrayList<Incrpf>();
	private List<Agcmpf> agcmpfListUpdt = new ArrayList<Agcmpf>();
	private List<Agcmpf> agcmppfListInst = new ArrayList<Agcmpf>();
	//ILIFE-6982 : Start
	private Rcvdpf rcvdPFObject= new Rcvdpf();
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	private boolean dialdownFlag = false;
	private boolean incomeProtectionflag = false;
	private boolean premiumflag = false;
	//ILIFE-6982 : End
	
	//add by wli31 ILIFE-7308
	private MbnspfDAO mbnsDAO = getApplicationContext().getBean("mbnspfDAO",MbnspfDAO.class);
	protected LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	protected Br613TempDAO br613TempDAO = getApplicationContext().getBean("br613TempDAO", Br613TempDAO.class);
	private LextpfDAO lextpfDAO =  getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
	private AgcmpfDAO agcmpfDAO = getApplicationContext().getBean("agcmpfDAO1", AgcmpfDAO.class);
	private AnnypfDAO annypfDAO = getApplicationContext().getBean("annypfDAO", AnnypfDAO.class);
	protected Lifepf lifepf = null;
	private FixedLengthStringData wsaaT5675Item =new FixedLengthStringData(8);
	private Map<String, List<Itempf>> t5655ListMap; 
	private Map<String, List<Itempf>> t5671ListMap;
	private Map<String, List<Itempf>> t5687ListMap;
	private Map<String, List<Itempf>> t5675ListMap;
	private Map<String, List<Itempf>> tr517ListMap;
    private Map<String, List<Covrpf>> covrMap = new HashMap<String,  List<Covrpf>>();
    private Map<String, List<Agcmpf>> agcmMap = new HashMap<String,  List<Agcmpf>>();
    private Map<String, List<Pcddpf>> pcddpfMap = new HashMap<String,  List<Pcddpf>>();
    protected Map<String, List<Lextpf>> lextpfMap = new HashMap<String,  List<Lextpf>>();
    private Map<String, List<Annypf>> annyMap = new HashMap<String,  List<Annypf>>();
	private PackedDecimalData wsaaAnnprem = new PackedDecimalData(17, 2);
	
	// Add by yy
	private Datcon3Utils datcon3Utils = getApplicationContext().getBean("datcon3Utils", Datcon3Utils.class);
	protected Datcon2Utils datcon2Utils = getApplicationContext().getBean("datcon2Utils", Datcon2Utils.class);
	private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
	private AgecalcUtils agecalcUtils = getApplicationContext().getBean("agecalcUtils", AgecalcUtils.class);
	private ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
	//ILIFE-7577	
	private boolean ausRerate;
	private String prevChdrnum;
	private FixedLengthStringData isUpdated = new FixedLengthStringData(1).init("N");
	private boolean riskPremflag = false; //ILIFE-7845 
	private static final String  RISKPREM_FEATURE_ID="NBPRP094";//ILIFE-7845 
	private SftlockRecBean sftlockRecBean = new SftlockRecBean();
	private SftlockUtil sftlockUtil = new SftlockUtil();
	private VpxchdrUtil vpxchdrUitl = new VpxchdrUtil();
	private VpxchdrPojo vpxchdrPojo = new VpxchdrPojo();
	private boolean stampDutyflag = false;
	/*ILIFE-8248 start*/
  	private boolean lnkgFlag = false;
	/*ILIFE-8248 end*/
	private boolean prmhldtrad = false;//ILIFE-8509
	private Map<String, Itempf> ta524Map;
	private Covrpf covrpfInsert ;
	private Rertpf rertpf ;
	private RertpfDAO rertpfDAO = getApplicationContext().getBean("rertpfDAO", RertpfDAO.class);
	private List<Rertpf> rertpfListInst = new ArrayList<Rertpf>();
	private List<Rertpf> rertpfUpdateList = new ArrayList<>();
	private List<Rertpf> rertpfList = new ArrayList<>();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		payr3256,
		singlePrem3295,
		continue3296
	}

	public Br613() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(),3)) {
			syserrrec.statuz.set("IVRM");
			fatalError600();
		}
		this.loadTables();
		varcom.vrcmDate.set(getCobolDate());
		wsaaCoreRunid.set(bprdIO.getSystemParam04());
		wsaaCoreJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		
		//PINNACLE-3190
		wsaaTransactionDate = varcom.vrcmDate.toInt();
	    wsaaTransactionTime = varcom.vrcmTime.toInt();
		
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				batchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				batchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			batchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
        readChunkRecord();
	}


	private void readChunkRecord() {
		List<Corepf> pfList = this.corepfDAO.findResults(wsaaCoreFn.toString(),wsaaThreadMember.toString(), batchExtractSize, batchID);
		if (null == pfList || pfList.isEmpty()) {
			wsspEdterror.set(varcom.endp);
			return;
		}
		this.iter = pfList.iterator();
		Map<String, List<String>> chdrInfor = new HashMap<String, List<String>>();
		for (Corepf pf : pfList) {
			if (chdrInfor.containsKey(pf.getChdrcoy())) {
				chdrInfor.get(pf.getChdrcoy()).add(pf.getChdrnum());
			} else {
				List<String> chdrnumList = new ArrayList<>();
				chdrnumList.add(pf.getChdrnum());
				chdrInfor.put(pf.getChdrcoy(), chdrnumList);
			}
		}
		agcmMap.clear();
		covrMap.clear();
		chdrpfMap.clear();
		payrpfMap.clear();
		incrpfMap.clear();
		pcddpfMap.clear();
		annyMap.clear();
		for (String coyKey : chdrInfor.keySet()) {
			chdrpfMap = chdrpfDAO.searchChdrpf(coyKey, chdrInfor.get(coyKey));
			covrMap = br613TempDAO.searchCovrRecord(coyKey,chdrInfor.get(coyKey));
			payrpfMap = payrpfDAO.getPayrLifMap(coyKey, chdrInfor.get(coyKey));
			incrpfMap = incrpfDAO.searchIncrRecordByChdrnum(chdrInfor.get(coyKey));
			agcmMap = br613TempDAO.getAgcmRecords(coyKey, chdrInfor.get(coyKey));
			pcddpfMap = br613TempDAO.getPcddpfRecords(coyKey,chdrInfor.get(coyKey));
			lextpfMap = lextpfDAO.searchLextpfMap(coyKey, chdrInfor.get(coyKey));
			annyMap = annypfDAO.searchAnnyRecordByChdrnum(chdrInfor.get(coyKey));
		}
	}

	private void loadTables() {
		String itemcoy = bsprIO.getCompany().toString();
		String itemitem= bprdIO.getAuthCode().toString();
		String ausRerateItem = "BTPRO014";	//ILIFE-7577
		String stampdutyItem = "NBPROP01";
		String linkageItem ="NBPRP055";//ILIFE-8248
		String prmhldtradItem = "CSOTH010";//ILIFE-8509
		
		List<Itempf> items = itemDAO.getAllItemitem(smtpfxcpy.item.toString(), itemcoy, "T5679", itemitem);
		if (null == items || items.isEmpty()) {
			syserrrec.params.set("T5679");
			fatalError600();
		} else {//IJTI-320 START
			t5679rec.t5679Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
		}
		//IJTI-320 END
		t5687ListMap = itemDAO.loadSmartTable(smtpfxcpy.item.toString(), itemcoy, "T5687");
		t5675ListMap = itemDAO.loadSmartTable(smtpfxcpy.item.toString(), itemcoy, "T5675");
		tr517ListMap = itemDAO.loadSmartTable(smtpfxcpy.item.toString(), itemcoy, "TR517");
		t5655ListMap = itemDAO.loadSmartTable(smtpfxcpy.item.toString(), itemcoy, "T5655");
		t5671ListMap = itemDAO.loadSmartTable(smtpfxcpy.item.toString(), itemcoy, "T5671");
		
		wsaaT5655Ix.set(1);
		if(t5655ListMap==null|| t5655ListMap.isEmpty()){
			return ;
		}
		
		for(String key:t5655ListMap.keySet()){
			if(key.startsWith(itemitem)){
				for(Itempf item:t5655ListMap.get(key)){
					if(item.getItmfrm().compareTo(varcom.vrcmMaxDate.getbigdata())<=0){
						if (isGT(wsaaT5655Ix, wsaaT5655Size)) {
							syserrrec.statuz.set("H791");
							syserrrec.params.set("T5655");
							fatalError600();
						}
						t5655rec.t5655Rec.set(StringUtil.rawToString(item.getGenarea()));
						wsaaT5655Key[wsaaT5655Ix.toInt()].set(item.getItemitem());
						wsaaT5655Itmfrm[wsaaT5655Ix.toInt()].set(item.getItmfrm());
						wsaaT5655Itmto[wsaaT5655Ix.toInt()].set(item.getItmto());
						wsaaT5655LeadDays[wsaaT5655Ix.toInt()].set(t5655rec.leadDays);
						if (isGT(t5655rec.leadDays, wsaaMaxLeadDays)) {
							wsaaMaxLeadDays.set(t5655rec.leadDays);
						}
						wsaaT5655IxMax.set(wsaaT5655Ix);
						wsaaT5655Ix.add(1);
					}
				}
			}
		}
		ausRerate = FeaConfg.isFeatureExist(itemcoy, ausRerateItem, appVars, "IT");	//ILIFE-7577
		stampDutyflag = FeaConfg.isFeatureExist(itemcoy, stampdutyItem, appVars, "IT");
		/* ILIFE-8248 start */
		lnkgFlag = FeaConfg.isFeatureExist(itemcoy, linkageItem, appVars, "IT");
		/* ILIFE-8248 start */
		prmhldtrad = FeaConfg.isFeatureExist(itemcoy, prmhldtradItem, appVars, "IT");//ILIFE-8509
		if(prmhldtrad)
			ta524Map = itemDAO.getItemMap("IT", itemcoy, "TA524");
	}
	private void clearList2100() {
		if (agcmMap != null) {
			agcmMap.clear();
		}
		if (covrMap != null) {
			covrMap.clear();
		}
		if (chdrpfMap != null) {
			chdrpfMap.clear();
		}
		if (payrpfMap != null) {
			payrpfMap.clear();
		}
		if (incrpfMap != null) {
			incrpfMap.clear();
		}
		if (pcddpfMap != null) {
			pcddpfMap.clear();
		}
		if (annyMap != null) {
			annyMap.clear();
		}
		
	}
	protected void readFile2000() {
		if (!iter.hasNext()) {
			batchID++;
			clearList2100();
			readChunkRecord();
			if(wsspEdterror.equals(varcom.endp)){
				return;
			}
		}
		this.corepfRec = iter.next();
		if(ausRerate && prevChdrnum != null && !prevChdrnum.equals(corepfRec.getChdrnum()))	//ILIFE-7577
			isUpdated.set("N");	//ILIFE-7577
		this.ct01Val++;
	}

protected void edit2500()
	{
		/*EDIT*/
		wsaaChdrValid = "Y";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)); wsaaIndex.add(1)) {
			validateChdrStatus2520();
		}
		pendingIncrease2700();
		if (isEQ(wsaaChdrValid, "N")) {
			wsspEdterror.set(SPACES);
			this.ct05Val++;
		} else {
			readChdr2600();
			wsspEdterror.set(varcom.oK);
		}
		/*EXIT*/

	}

protected void validateChdrStatus2520()
	{
		/*CHECK*/
		if (isEQ(t5679rec.cnRiskStat[wsaaIndex.toInt()],corepfRec.getStatcode())) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
				validateChdrPremStatus2550();
			}
		}
		/*EXIT*/
	}

protected void validateChdrPremStatus2550()
	{
		/*PARA*/
		if (isEQ(t5679rec.cnPremStat[wsaaIndex.toInt()],corepfRec.getPstatcode())) {
			wsaaIndex.set(13);
			wsaaChdrValid = "Y";
		}
		/*EXIT*/
	}

	protected void readChdr2600() {
		if (chdrpfMap != null && chdrpfMap.containsKey(corepfRec.getChdrnum())) {
			chdrlifIO = chdrpfMap.get(corepfRec.getChdrnum()).get(0);
		} else {
			syserrrec.params.set(corepfRec.getChdrnum());
			fatalError600();
		}
		/* The program now uses T5655 to obtain the lead number of */
		/* days. */
		/* Search for the number of lead days for this contract type */
		/* within the working storage array. */
		for (wsaaT5655Ix.set(wsaaT5655IxMax); 
				!(isEQ(wsaaT5655Ix, 0) || (isEQ(chdrlifIO.getCnttype(), wsaaT5655Cnttype[wsaaT5655Ix.toInt()])
				&& isGTE(chdrlifIO.getOccdate(), wsaaT5655Itmfrm[wsaaT5655Ix.toInt()]) && isLTE(chdrlifIO.getOccdate(), wsaaT5655Itmto[wsaaT5655Ix.toInt()]))); 
				wsaaT5655Ix.add(-1)) {
			/* CONTINUE_STMT */
		}
		/* If the contract type specific entry is not found, use the */
		/* default entry of the transaction code plus '***'. */
		if (isEQ(wsaaT5655Ix, 0)) {
			for (wsaaT5655Ix.set(wsaaT5655IxMax); !(isEQ(wsaaT5655Ix, 0) || isEQ(wsaaT5655Cnttype[wsaaT5655Ix.toInt()], "***")); wsaaT5655Ix.add(-1)) {
				/* CONTINUE_STMT */
			}
		}
		if (isEQ(wsaaT5655Ix, 0)) {
			fatalError600();
		}
		List<Itempf> itempfList = new ArrayList<Itempf>();
		List<List<Itempf>> itempfListList = new ArrayList<>(t5655ListMap.values());
		Iterator<List<Itempf>> iteratorList = itempfListList.iterator();
		boolean isNotFund = true;
		while (iteratorList.hasNext() && isNotFund) {
			itempfList = iteratorList.next();
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext() && isNotFund) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if ((isEQ(chdrlifIO.getCnttype(), itempf.getItemitem().substring(4).trim())
						&& isGTE(chdrlifIO.getOccdate(), itempf.getItmfrm())
						&& isLTE(chdrlifIO.getOccdate(), itempf.getItmto()))) {
					isNotFund = false;
					t5655rec.t5655Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				}
			}
		}

		if (isNotFund) {
			Iterator<List<Itempf>> iteratorList2 = itempfListList.iterator();
			while (iteratorList2.hasNext()) {
				itempfList = iteratorList2.next();
				Iterator<Itempf> iterator = itempfList.iterator();
				while (iterator.hasNext() && isNotFund) {
					Itempf itempf = new Itempf();
					itempf = iterator.next();
					if (isEQ(chdrlifIO.getCnttype(), "***")) {
						isNotFund = false;
						t5655rec.t5655Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					}
				}
			}
		}
		wsaaLeadDays.set(wsaaT5655LeadDays[wsaaT5655Ix.toInt()]);
//		datcon2rec.freqFactor.set(wsaaT5655LeadDays[wsaaT5655Ix.toInt()]);
//		datcon2rec.frequency.set("DY");
//		datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
//		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
//		if (isNE(datcon2rec.statuz, varcom.oK)) {
//			syserrrec.statuz.set(datcon2rec.statuz);
//			syserrrec.params.set(datcon2rec.datcon2Rec);
//			fatalError600();
//		}
		Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
		datcon2Pojo.setFreqFactor(wsaaT5655LeadDays[wsaaT5655Ix.toInt()].toInt());
		datcon2Pojo.setIntDate1(bsscIO.getEffectiveDate().toString());
		datcon2Pojo.setFrequency("DY");
		datcon2Utils.calDatcon2(datcon2Pojo);
	
		if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
			syserrrec.statuz.set(datcon2Pojo.getStatuz());
			syserrrec.params.set(datcon2Pojo.toString());
			fatalError600();
		}
		wsaaBillDate.set(datcon2Pojo.getIntDate2());
	}

	protected void pendingIncrease2700() {
		if(!prmhldtrad || ta524Map.get(chdrpfMap.get(corepfRec.getChdrnum()).get(0).getCnttype()) == null) {//ILIFE-8509
			for(Covrpf covr: covrMap.get(corepfRec.getChdrnum())) {
				if(covr.getReinstated() == null || !(("Y").equals(covr.getReinstated()))) {
					if (incrpfMap != null && incrpfMap.containsKey(corepfRec.getChdrnum())) {
						for (Incrpf c : incrpfMap.get(corepfRec.getChdrnum())) {
							if(c.getValidflag().equals("1")){
								conlogrec.error.set("F374");
								callConlog003();
								wsaaChdrValid = "N";
								break;
							}
						}
					}
				}
			}
		}
	}

protected void update3000()
	{
	sftlockRecBean.setFunction("LOCK");
	sftlockRecBean.setCompany(batcdorrec.company.trim());
	sftlockRecBean.setEnttyp("CH");
	sftlockRecBean.setEntity(chdrlifIO.getChdrnum().trim());
	sftlockRecBean.setTransaction(bprdIO.getAuthCode().toString());
	sftlockRecBean.setUser("999999");
	sftlockUtil.process(sftlockRecBean, appVars);
	if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())
		&& !"LOCK".equals(sftlockRecBean.getStatuz())) {
			syserrrec.statuz.set(sftlockRecBean.getStatuz());
			fatalError600();
	}

		if (sftlockRecBean.getStatuz().equals("LOCK")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrlifIO.getChdrnum());
			stringVariable1.addExpression(" HEADER RECORD LOCKED");
			stringVariable1.setStringInto(conlogrec.message);
			conlogrec.error.set(sftlockRecBean.getStatuz());
			callConlog003();
			return ;
		}
		/*3010-UPDATE.                                                     */
		/* Perform a begin on the COVRRNL and then enter the COVR loop.*/
		/* The Wsaa-covrs-live indicator is set to N initially. This*/
		/* is set to Y if a valid cover/rider is found, on the contract*/
		/* which is alive. This indicator is then checked in section*/
		/* 4000 to determine whether the whole contract has matured or*/
		/* whether there are valid covers/riders existing.*/
		List<Covrpf> covrList = covrMap.get(corepfRec.getChdrnum());
		List<Agcmpf> agcmList = agcmMap.get(corepfRec.getChdrnum());
		rertpfList = rertpfDAO.getRertpfList(corepfRec.getChdrcoy(),corepfRec.getChdrnum(),"1",0);
		wsaaCovrsLive = "N";
		wsaaCovrUpdated = "N";
		wsaaPremDiff.set(ZERO);
		//START OF ILIFE-7577
		if(ausRerate){ 
				if(isEQ(isUpdated, "N")) {
				for (Covrpf covrpf : covrList) {
					prevChdrnum = corepfRec.getChdrnum();
					validateCovr3100(covrpf);
					if (isEQ(wsaaCovrValid,"Y")) {
						updateCovr3200(covrpf);
						if (agcmList != null && agcmList.size() > 0) {
							updateAgcm3700(covrpf, agcmList);
						}
						genericProcessing3900(covrpf);
					}	
				}
			
				if (isEQ(wsaaCovrUpdated,"Y")) {
					updateChdrPtrn4100();
					updatePayr4200();//
					isUpdated.set("Y");
				}
			}
		}
		else {
			for (Covrpf covrpf : covrList) {
				validateCovr3100(covrpf);
				if (isEQ(wsaaCovrValid,"Y")) {
					updateCovr3200(covrpf);
					if (agcmList != null && agcmList.size() > 0) {
						updateAgcm3700(covrpf, agcmList);
					}
					genericProcessing3900(covrpf);
				}
			}
	
			if (isEQ(wsaaCovrUpdated,"Y")) {
				updateChdrPtrn4100();
				updatePayr4200();//
			}
		}
		//END OF ILIFE-7577
		
		sftlockRecBean.setFunction("UNLK");
		sftlockRecBean.setCompany(batcdorrec.company.trim());
		sftlockRecBean.setEnttyp("CH");
		sftlockRecBean.setEntity(chdrlifIO.getChdrnum());
		sftlockRecBean.setTransaction(bprdIO.getAuthCode().toString());
		sftlockRecBean.setUser("0");
		sftlockUtil.process(sftlockRecBean, appVars);
		if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())) {
				syserrrec.statuz.set(sftlockRecBean.getStatuz());
				fatalError600();
		}
	}



protected void validateCovr3100(Covrpf covrpf)
	{
		/* Check that the rerate date is not equal to zeroes*/
		/* and less than both the billing effective date + 30 days and*/
		/* the billing effective date + the number of lead days on*/
		/* T5655.*/
		/* If a valid cover/rider has a premium cessation date beyond*/
		/* the billing date then it is still 'live' and hence set*/
		/* set indicator accordingly.*/
		/* Validate the coverage risk and premium statii.*/
		wsaaCovrValid = "N";
		/* Check if COVR is the WOP component, ignore it!               */
		/*    MOVE SPACES                  TO ITDM-PARAMS.                 */
		/*    MOVE SMTP-ITEM               TO ITDM-ITEMPFX.                */
		/*    MOVE COVRRNL-CHDRCOY         TO ITDM-ITEMCOY.                */
		/*    MOVE TR517                   TO ITDM-ITEMTABL.               */
		/*    MOVE COVRRNL-CRTABLE         TO ITDM-ITEMITEM.               */
		/*    MOVE COVRRNL-CRRCD           TO ITDM-ITMFRM.                 */
		/*    MOVE BEGN                    TO ITDM-FUNCTION.               */
		/*    CALL 'ITDMIO'           USING  ITDM-PARAMS.                  */
		/*    IF  ITDM-STATUZ             NOT = O-K                        */
		/*    AND                         NOT = ENDP                       */
		/*        MOVE ITDM-STATUZ         TO SYSR-STATUZ                  */
		/*        MOVE ITDM-PARAMS         TO SYSR-PARAMS                  */
		/*        PERFORM 600-FATAL-ERROR                                  */
		/*    END-IF.                                                      */
		/*    IF  ITDM-STATUZ                 = ENDP                       */
		/*    OR  ITDM-ITEMCOY            NOT = COVRRNL-CHDRCOY            */
		/*    OR  ITDM-ITEMTABL           NOT = TR517                      */
		/*    OR  ITDM-ITEMITEM           NOT = COVRRNL-CRTABLE            */
		/*        NEXT SENTENCE                                            */
		/*    ELSE                                                         */
		/*        GO TO 3149-EXIT                                          */
		/*    END-IF.                                                      */
		if (isNE(covrpf.getRerateDate(),ZERO)
		&& isEQ(covrpf.getValidflag(),"1")) {
			if (isGTE(covrpf.getPremCessDate(),wsaaBillDate)) {
				wsaaCovrsLive = "Y";
			}
			if (isLTE(covrpf.getRerateDate(),wsaaBillDate)) {
				for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
					validateCovrStatus3150(covrpf);
				}
			}
		}
		if (isEQ(wsaaCovrsLive,"N")) {
			return ;
		}
		String keyItemitem = covrpf.getCrtable().trim();
		List<Itempf> tr517List = tr517ListMap.get(keyItemitem);
		if (tr517List != null && tr517List.size() > 0) {
			wsaaCovrValid = "N";
		}
	}

protected void validateCovrStatus3150(Covrpf covrpf)
	{
		/*VALIDATE-RISK-STATUS*/
		if (isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()],covrpf.getStatcode())) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
				validateCovrPremStatus3170(covrpf);
			}
		}
		/*EXIT*/
	}

protected void validateCovrPremStatus3170(Covrpf covrpf)
	{
		/*PARA*/
		if (isEQ(t5679rec.covPremStat[wsaaIndex.toInt()],covrpf.getPstatcode())) {
			wsaaIndex.set(13);
			wsaaCovrValid = "Y";
		}
		/*EXIT*/
	}

protected void updateCovr3200(Covrpf covrpf)
	{
		wsaaCovrUpdated = "Y";
		/*  Store the rerate date.*/
		wsaaRerateStore.set(covrpf.getRerateDate());
		/*  Default to a TRUE rerate.*/
		wsaaRerateType.set("1");
		wsaaInstPrem.set(ZERO);
		wsaaZbinstprem.set(ZERO);
		wsaaZlinstprem.set(ZERO);
		wsaaCommPrem.set(ZERO);
		/* Read the COVR record before rewriting the validflag '2'*/
		/* record using a different logical view to avoid picking up the*/
		/* new COVR record when reading the next record.*/
		lifepf = lifepfDAO.getLifeEnqRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "00");/* IJTI-1386 */
		if(lifepf == null){
			syserrrec.statuz.set("lifepf MRNF");
			fatalError600();
		}
		if (isEQ(covrpf.getPremCessDate(),covrpf.getRerateDate())) {
			wsaaFullyPaid = "Y";
			pastCeaseDate3290(covrpf);
			return ;
		}
		
		wsaaFullyPaid = "N";
		if(!prmhldtrad || (covrpf.getReinstated()==null || !"Y".equals(covrpf.getReinstated())))//ILIFE-8509
			calculatePremium3250(covrpf); 
		else if(prmhldtrad && "Y".equals(covrpf.getReinstated()))
			readT56873255(covrpf);
		Covrpf covrpfUpdate = new Covrpf(covrpf);
		covrpfUpdate.setCurrto(wsaaRerateStore.toInt());
		covrpfUpdate.setValidflag("2");
		this.covrpfListUpdt.add(covrpfUpdate);
		checkLexts3300(covrpfUpdate);
		statistics5000();
	}

protected void calculatePremium3250(Covrpf covrpf)
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
		try {
				switch (nextMethod) {
				case DEFAULT:
					readT56873255(covrpf);
				case payr3256:
					payr3256(covrpf);
				}
				break;
		}
		catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readT56873255(Covrpf covrpf){
	List<Itempf> itempfList = new ArrayList<Itempf>();
	String keyItemitem = covrpf.getCrtable().trim();/* IJTI-1386 */
	String effDate = Integer.toString(covrpf.getCrrcd());
	boolean itemFound = false;
	if (t5687ListMap.containsKey(keyItemitem)) {
		itempfList = t5687ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if (Integer.parseInt(itempf.getItmfrm().toString()) > 0) {
				if ((Integer.parseInt(effDate) >= Integer.parseInt(itempf.getItmfrm().toString()))
						&& Integer.parseInt(effDate) <= Integer.parseInt(itempf.getItmto().toString())) {
					t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					wsaaZsredtrm.set(t5687rec.zsredtrm);
					itemFound = true;
				}
			} else {
				t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				wsaaZsredtrm.set(t5687rec.zsredtrm);
				itemFound = true;
			}
		}
	}
	if (!itemFound) {
		syserrrec.params.set(t5687rec.t5687Rec);
		syserrrec.params.set("T5687" + keyItemitem);
		fatalError600();
	}
	
	
	if (isEQ(t5687rec.premmeth,SPACES)) {
		/*        GO TO 3259-EXIT                                          */
		goTo(GotoLabel.payr3256);
	}

	setRerateType3410(covrpf);
	premiumrec.premiumRec.set(SPACES);
	initialize(premiumrec.premiumRec);

	premiumrec.zstpduty01.set(ZERO);	//IBPLIFE-4907

	premiumrec.lsex.set(lifepf.getCltsex());
	calculateAnb3260(lifepf);
	premiumrec.lage.set(wsaaAnb);
	/* Read T5675 for premium calculation subroutine call.*/

	checkJointLife3270(covrpf);
	List<Itempf> t5675List = t5675ListMap.get(wsaaT5675Item);
	if(t5675List == null || t5675List.size() == 0){
		syserrrec.params.set("T5675"+ wsaaT5675Item);
		syserrrec.statuz.set("MRNF");
		fatalError600();
	}
	/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
	if(AppVars.getInstance().getAppConfig().isVpmsEnable())
	{
		premiumrec.premMethod.set(wsaaT5675Item);
	}
	/* ILIFE-3142 End*/

	t5675rec.t5675Rec.set(StringUtil.rawToString(t5675List.get(0).getGenarea()));
}

protected void payr3256(Covrpf covrpf)
	{
		/*  The PAYR file must be read to get the billing frequency.*/
		getPayrpf(); //ILIFE-4323 by dpuhawan
		payrIO = null;
		if (payrpfMap != null && payrpfMap.containsKey(chdrlifIO.getChdrnum())) {
			for (Payrpf c : payrpfMap.get(chdrlifIO.getChdrnum())) {
				if (c.getPayrseqno() == 1 && "1".equals(c.getValidflag())) {
					payrIO = c;
					break;
				}
			}
		}
		
		if (payrIO == null) {
			syserrrec.params.set(chdrlifIO.getChdrnum());
			fatalError600();
		}
		
		wsaaBillfreqx.set(payrIO.getBillfreq());
		if (isEQ(t5687rec.premmeth, SPACES)) {
			return ;
		}
		premiumrec.crtable.set(covrpf.getCrtable());
		premiumrec.chdrChdrcoy.set(covrpf.getChdrcoy());
		premiumrec.chdrChdrnum.set(covrpf.getChdrnum());
		premiumrec.lifeLife.set(covrpf.getLife());
		premiumrec.lifeJlife.set(covrpf.getJlife());
		premiumrec.covrRider.set(covrpf.getRider());
		premiumrec.covrCoverage.set(covrpf.getCoverage());
		premiumrec.mop.set(payrIO.getBillchnl());
		premiumrec.billfreq.set(payrIO.getBillfreq());
		premiumrec.effectdt.set(wsaaLastRrtDate);
		premiumrec.mortcls.set(covrpf.getMortcls());
		premiumrec.currcode.set(covrpf.getPremCurrency());
		premiumrec.termdate.set(covrpf.getPremCessDate());
		/*  Calculate the remaining term. This is re-rate type*/
		/*  dependent. For TRUE re-rates this is the re-rate date.*/
		/*  For LEXT re-rates this is the LAST TRUE RERATE DATE.*/
		/*  Either way the correct result is stored as*/
		/*  WSAA-LAST-RRT-DATE.*/
//		datcon3rec.function.set(SPACES);
//		datcon3rec.intDate1.set(premiumrec.effectdt);
//		datcon3rec.intDate2.set(premiumrec.termdate);
//		datcon3rec.frequency.set("01");
//		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
//		if (isNE(datcon3rec.statuz,varcom.oK)) {
//			syserrrec.statuz.set(datcon3rec.statuz);
//			syserrrec.params.set(datcon3rec.datcon3Rec);
//			fatalError600();
//		}
//		compute(premiumrec.duration, 5).set(add(datcon3rec.freqFactor,.99999));
		// changed by yy
		Datcon3Pojo datcon3Pojo = new Datcon3Pojo();
		datcon3Pojo.setIntDate1(premiumrec.effectdt.toString());
		datcon3Pojo.setIntDate2(premiumrec.termdate.toString());
		datcon3Pojo.setFrequency("01");
		datcon3Utils.calDatcon3(datcon3Pojo);
		String statuz = datcon3Pojo.getStatuz();
		if (isNE(statuz,varcom.oK)) {
			syserrrec.statuz.set(statuz);
			syserrrec.params.set(datcon3Pojo.toString());
			fatalError600();
		}
		compute(premiumrec.duration, 5).set(add(datcon3Pojo.getFreqFactor(),.99999));
		premiumrec.ratingdate.set(covrpf.getRerateFromDate());
		premiumrec.reRateDate.set(covrpf.getRerateDate());
		premiumrec.calcPrem.set(covrpf.getInstprem());
		premiumrec.calcBasPrem.set(covrpf.getZbinstprem());
		premiumrec.calcLoaPrem.set(covrpf.getZlinstprem());
		if (isEQ(covrpf.getPlanSuffix(),ZERO)
		&& isNE(chdrlifIO.getPolinc(),1)) {
			compute(premiumrec.sumin, 3).setRounded(div(covrpf.getSumins(),chdrlifIO.getPolsum()));
		}
		else {
			premiumrec.sumin.set(covrpf.getSumins());
		}
		compute(premiumrec.sumin, 3).setRounded(mult(premiumrec.sumin,chdrlifIO.getPolinc()));
		premiumrec.riskPrem.set(BigDecimal.ZERO); //ILIFE-9223
		if (isEQ(wsaaZsredtrm,"Y")) {
			/*        MOVE COVR-INSTPREM      TO CPRM-BASE-PREM*/
			return ;
		}
		premiumrec.function.set("CALC");
		/* Get any Annuity values required for the calculation.  If the*/
		/* coverage is not an annuity, initialise these values.*/
		getAnny3280(covrpf);
		premiumrec.language.set(bsscIO.getLanguage());
		//ILIFE-8248
		if (("PMEX".equals(t5675rec.premsubr.toString().trim())) && (lnkgFlag == true)) {

			if (null == covrpf.getLnkgsubrefno() || covrpf.getLnkgsubrefno().trim().isEmpty()) {
				premiumrec.lnkgSubRefNo.set(SPACE);
			} else {
				premiumrec.lnkgSubRefNo.set(covrpf.getLnkgsubrefno().trim());/* IJTI-1386 */
			}

			if (null == covrpf.getLnkgno() || covrpf.getLnkgno().trim().isEmpty()) {
				premiumrec.linkcov.set(SPACE);
			} else {
					LinkageInfoService linkgService = new LinkageInfoService();
					FixedLengthStringData linkgCov = new FixedLengthStringData(
							linkgService.getLinkageInfo(covrpf.getLnkgno().trim()));/* IJTI-1386 */
					premiumrec.linkcov.set(linkgCov);
			}
		}
		//ILIFE-8248
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]	
		//ILIFE-8502
		premiumrec.commTaxInd.set("Y");
		List<Incrpf> incrpfList=incrpfDAO.getIncrpfList(covrpf.getChdrcoy(),covrpf.getChdrnum());
		if(incrpfList !=null && !incrpfList.isEmpty()) {
		if(incrpfList.get(0).getLastSum().intValue() !=0) {
		premiumrec.prevSumIns.set(incrpfList.get(0).getLastSum().intValue());
		}else {
			premiumrec.prevSumIns.set(ZERO);
		}
	}else {
		premiumrec.prevSumIns.set(ZERO);
	}
		clntpf = clntpfDAO.searchClntRecord("CN",bsprIO.getFsuco().toString(),lifepf.getLifcnum());
		clntDao = DAOFactory.getClntpfDAO();
		clnt=clntDao.getClientByClntnum(lifepf.getLifcnum());
		if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
			premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
		}else{
			premiumrec.stateAtIncep.set(clntpf.getClntStateCd());/* IJTI-1386 */
		}
		
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && (er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))) //ILIFE-7584
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else{
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			vpxchdrPojo = vpxchdrUitl.callInit(vpmcalcrec);
			premiumrec.rstaflag.set(vpxchdrPojo.getRstaflag());
			premiumrec.cnttype.set(chdrlifIO.getCnttype());
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-7123 : Start						
			boolean dialdownFlag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPRP012", appVars, "IT");/* IJTI-1386 */
			boolean incomeProtectionflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP02", appVars, "IT");/* IJTI-1386 */
			boolean premiumflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP03", appVars, "IT");/* IJTI-1386 */			
			/* IJTI-1386 START*/
			if(incomeProtectionflag || premiumflag || dialdownFlag){
				RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
				Rcvdpf rcvdPFObject= new Rcvdpf();
				rcvdPFObject.setChdrcoy(covrpf.getChdrcoy());
				rcvdPFObject.setChdrnum(covrpf.getChdrnum());
				rcvdPFObject.setLife(covrpf.getLife());
				rcvdPFObject.setCoverage(covrpf.getCoverage());
				rcvdPFObject.setRider(covrpf.getRider());
				rcvdPFObject.setCrtable(covrpf.getCrtable());
				rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);		
				/* IJTI-1386 END*/
				premiumrec.bentrm.set(rcvdPFObject.getBentrm());				
				if(rcvdPFObject.getPrmbasis() != null && isEQ("S",rcvdPFObject.getPrmbasis())){
					premiumrec.prmbasis.set("Y");
				}else{
					premiumrec.prmbasis.set("");
				}
				premiumrec.poltyp.set(rcvdPFObject.getPoltyp());
				premiumrec.occpcode.set("");
				premiumrec.dialdownoption.set(rcvdPFObject.getDialdownoption());
				premiumrec.waitperiod.set(rcvdPFObject.getWaitperiod());
			}						
			//ILIFE-7123 : End
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			//START OF ILIFE-7584
			premiumrec.cownnum.set(SPACES);
			premiumrec.occdate.set(ZERO);
			if(t5675rec.premsubr.toString().trim().equals("PMEX")){
				premiumrec.setPmexCall.set("Y");
				premiumrec.batcBatctrcde.set(bprdIO.getAuthCode());
				premiumrec.inputPrevPrem.set(ZERO);//ILIFE-8537
				premiumrec.updateRequired.set("N");
				premiumrec.validflag.set("Y");
				premiumrec.cownnum.set(chdrlifIO.getCownnum());
				premiumrec.occdate.set(chdrlifIO.getOccdate());
				
			}
			//END OF ILIFE-7584
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End		*/

		/*Ticket #ILIFE-2005 - End		*/
		//ILIFE-7845 
		premiumrec.riskPrem.set(BigDecimal.ZERO); //ILIFE-7922 by dpuhawan
		riskPremflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), RISKPREM_FEATURE_ID, appVars, "IT");/* IJTI-1386 */
		if(riskPremflag)
		{
			//premiumrec.riskPrem.set(ZERO);  //ILIFE-7922 by dpuhawan
			premiumrec.cnttype.set(chdrlifIO.getCnttype());
			premiumrec.crtable.set(covrpf.getCrtable());
			premiumrec.calcTotPrem.set(add(premiumrec.calcPrem,premiumrec.zstpduty01));
			callProgram("RISKPREMIUM", premiumrec.premiumRec);
		
	}
		//ILIFE-7845  end
		if (isNE(premiumrec.statuz,varcom.oK)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
	}


private void getPayrpf() {
	payrIO = null;
	if (payrpfMap != null && payrpfMap.containsKey(chdrlifIO.getChdrnum())) {
		for (Payrpf c : payrpfMap.get(chdrlifIO.getChdrnum())) {
			if (c.getPayrseqno() == 1 && "1".equals(c.getValidflag())) {
				payrIO = c;
				break;
			}
		}
	}
	
	if (payrIO == null) {
		syserrrec.params.set(chdrlifIO.getChdrnum());
		fatalError600();
	}
}

protected void calculateAnb3260(Lifepf lifelnb)
	{
	wsaaAnb.set(ZERO);
	AgecalcPojo agecalcPojo = new AgecalcPojo();
	agecalcPojo.setFunction("CALCP");
	agecalcPojo.setLanguage(bsscIO.getLanguage().toString());
	agecalcPojo.setCnttype(chdrlifIO.getCnttype());
	agecalcPojo.setIntDate1(Integer.toString(lifelnb.getCltdob()));
	agecalcPojo.setIntDate2(wsaaLastRrtDate.toString());
	agecalcPojo.setCompany(bsprIO.getFsuco().toString());
	agecalcUtils.calcAge(agecalcPojo);
	if (isNE(agecalcPojo.getStatuz(), varcom.oK)) {
		syserrrec.params.set(agecalcPojo.toString());
		syserrrec.statuz.set(agecalcPojo.getStatuz());
		fatalError600();
	}
	wsaaAnb.set(agecalcPojo.getAgerating());
	}

	protected void checkJointLife3270(Covrpf covrpf) {
		Lifepf lifelnb = lifepfDAO.getLifelnbRecord(covrpf.getChdrcoy(),covrpf.getChdrnum(), covrpf.getLife(),"01");/* IJTI-1386 */
		if (lifelnb != null) {
			wsaaT5675Item.set(t5687rec.jlPremMeth);
			premiumrec.jlsex.set(lifelnb.getCltsex());
			calculateAnb3260(lifelnb);
			premiumrec.jlage.set(wsaaAnb);
		} else {
			wsaaT5675Item.set(t5687rec.premmeth);
			premiumrec.jlsex.set(SPACES);
			premiumrec.jlage.set(ZERO);
		}
	}

	protected void getAnny3280(Covrpf covrpf) {
		Annypf annyIO = null;
		if (annyMap != null && annyMap.containsKey(covrpf.getChdrnum())) {
			for (Annypf a : annyMap.get(covrpf.getChdrnum())) {
				if (a.getChdrcoy().equals(covrpf.getChdrcoy())
						&& a.getLife().equals(covrpf.getLife())
						&& a.getCoverage().equals(covrpf.getCoverage())
						&& a.getRider().equals(covrpf.getRider())
						&& a.getPlanSuffix() == covrpf.getPlanSuffix()) {
					annyIO = a;
					break;
				}
			}
		}
		if (annyIO != null) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		} else {
			premiumrec.advance.set(SPACES);
			premiumrec.arrears.set(SPACES);
			premiumrec.freqann.set(SPACES);
			premiumrec.withprop.set(SPACES);
			premiumrec.withoprop.set(SPACES);
			premiumrec.ppind.set(SPACES);
			premiumrec.nomlife.set(SPACES);
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
	}

protected void pastCeaseDate3290(Covrpf covrpf)
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					calc3295(covrpf);
				case singlePrem3295:
					singlePrem3295(covrpf);
				case continue3296:
					continue3296(covrpf);
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void calc3295(Covrpf covrpf){
	Covrpf covrpfUpdate = new Covrpf(covrpf);
	covrpfUpdate.setCurrto(wsaaRerateStore.toInt());
	covrpfUpdate.setValidflag("2");
	this.covrpfListUpdt.add(covrpfUpdate);
		/* Write a new COVR with an incremented transaction number, a*/
		/* new premium status code, and new current from and current to*/
		/* dates.*/
	compute(wsaaPremDiff, 3).setRounded(add(wsaaPremDiff,(mult(covrpf.getInstprem(),-1))));
	if (isEQ(chdrlifIO.getBillfreq(),"00")) {
		goTo(GotoLabel.singlePrem3295);
	}
	if (isEQ(covrpf.getCoverage(), "01") && isEQ(covrpf.getLife(), "01") && isEQ(covrpf.getRider(), "00")) {
		if (isNE(t5679rec.setCovPremStat,SPACES)) {
			covrpf.setPstatcode(t5679rec.setCovPremStat.toString());
		}
	}
	else {
		if (isNE(t5679rec.setRidPremStat,SPACES)) {
			covrpf.setPstatcode(t5679rec.setRidPremStat.toString());
		}
	}
	goTo(GotoLabel.continue3296);
}

protected void singlePrem3295(Covrpf covrpf)
	{
		if (isEQ(covrpf.getRider(),"00")
		|| isEQ(covrpf.getRider(), "  ")) {
			if (isNE(t5679rec.setSngpCovStat,SPACES)) {
				covrpf.setPstatcode(t5679rec.setSngpCovStat.toString());
			}
		}
		else {
			if (isNE(t5679rec.setSngpRidStat,SPACES)) {
				covrpf.setPstatcode(t5679rec.setSngpRidStat.toString());
			}
		}
	}

protected void continue3296(Covrpf covrpf)
	{
		 covrpfInsert = new Covrpf(covrpf);
		compute(wsaaNewTranno, 0).set(add(chdrlifIO.getTranno(),1));
		covrpfInsert.setTranno(wsaaNewTranno.toInt());
		//covrpfInsert.setCurrfrom(wsaaRerateStore.toInt());
		covrpfInsert.setCurrto(varcom.vrcmMaxDate.toInt());
		covrpfInsert.setValidflag("1");
		covrpfInsert.setRiskprem(premiumrec.riskPrem.getbigdata()); //ILIFE-7845
		covrpfListInst.add(covrpfInsert);
		
		this.ct04Val++;
	}

protected void checkLexts3300(Covrpf covrpf)
	{
		 covrpfInsert = new Covrpf(covrpf);
		if (isNE(t5687rec.rtrnwfreq,NUMERIC)
		|| isEQ(t5687rec.rtrnwfreq,0)) {
			t5687rec.rtrnwfreq.set(ZERO);
			wsaaRateFrom.set(covrpf.getPremCessDate());
		}
		else {
//			datcon2rec.frequency.set("01");
//			datcon2rec.freqFactor.set(t5687rec.rtrnwfreq);
//			datcon2rec.intDate1.set(wsaaLastRrtDate);
//			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
//			if (isNE(datcon2rec.statuz,varcom.oK)) {
//				syserrrec.statuz.set(datcon2rec.statuz);
//				syserrrec.params.set(datcon2rec.datcon2Rec);
//				fatalError600();
//			}
//			wsaaRateFrom.set(datcon2rec.intDate2);
			// changed by yy
			Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
			datcon2Pojo.setFreqFactor(t5687rec.rtrnwfreq.toInt());
			datcon2Pojo.setIntDate1(wsaaLastRrtDate.toString());
			datcon2Pojo.setFrequency("01");
			datcon2Utils.calDatcon2(datcon2Pojo);
		
			if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
				syserrrec.statuz.set(datcon2Pojo.getStatuz());
				syserrrec.params.set(datcon2Pojo.toString());
				fatalError600();
			}
			wsaaRateFrom.set(datcon2Pojo.getIntDate2());
		}

		if (lextpfMap != null && lextpfMap.containsKey(covrpf.getChdrnum())) {
			for (Lextpf c : lextpfMap.get(covrpf.getChdrnum())) {
				if (isNE(c.getChdrcoy(),covrpf.getChdrcoy())
				|| isNE(c.getChdrnum(),covrpf.getChdrnum())
				|| isNE(c.getRider(),covrpf.getRider())
				|| isNE(c.getCoverage(),covrpf.getCoverage())
				|| isNE(c.getLife(),covrpf.getLife())) {
					//return;
					break;	//ILIFE-7577
				}
				if (isLT(c.getExtCessDate(),wsaaRateFrom)
				&& isGT(c.getExtCessDate(),covrpf.getRerateDate())) {
					wsaaRateFrom.set(c.getExtCessDate());
				}
			}
		}
		covrpfInsert.setRerateDate(wsaaRateFrom.toInt());
		covrpfInsert.setRerateFromDate(covrpf.getRerateDate());
		checkMgp3360(covrpfInsert);
		if (isNE(t5687rec.premmeth,SPACES)) {
			if(!prmhldtrad || (covrpf.getReinstated()==null || !"Y".equals(covrpf.getReinstated()))) {//ILIFE-8509
				if(!stampDutyflag)
					compute(wsaaInstPrem, 3).setRounded(div(premiumrec.calcPrem,chdrlifIO.getPolinc()));
				else
					compute(wsaaInstPrem, 3).setRounded(div(add(premiumrec.calcPrem, premiumrec.zstpduty01),chdrlifIO.getPolinc()));
				compute(wsaaZbinstprem, 3).setRounded(div(premiumrec.calcBasPrem,chdrlifIO.getPolinc()));
				compute(wsaaZlinstprem, 3).setRounded(div(premiumrec.calcLoaPrem,chdrlifIO.getPolinc()));
				compute(wsaaCommPrem, 3).setRounded(div(premiumrec.commissionPrem,chdrlifIO.getPolinc()));
				if (isEQ(covrpf.getPlanSuffix(),0)
				&& isNE(chdrlifIO.getPolinc(),1)) {
					compute(wsaaInstPrem, 3).setRounded(mult(wsaaInstPrem,chdrlifIO.getPolsum()));
					compute(wsaaZbinstprem, 3).setRounded(mult(wsaaZbinstprem,chdrlifIO.getPolsum()));
					compute(wsaaZlinstprem, 3).setRounded(mult(wsaaZlinstprem,chdrlifIO.getPolsum()));
					compute(wsaaCommPrem, 3).setRounded(mult(wsaaCommPrem,chdrlifIO.getPolsum()));
				}
				if (isNE(wsaaInstPrem, 0)) {
	//				zrdecplrec.amountIn.set(wsaaInstPrem);
					zrdecplcPojo.setAmountIn(wsaaInstPrem.getbigdata());
					callRounding6000();
	//				wsaaInstPrem.set(zrdecplrec.amountOut);
					wsaaInstPrem.set(zrdecplcPojo.getAmountOut());
				}
				if (isNE(wsaaZbinstprem, 0)) {
	//				zrdecplrec.amountIn.set(wsaaZbinstprem);
					zrdecplcPojo.setAmountIn(wsaaZbinstprem.getbigdata());
					callRounding6000();
	//				wsaaZbinstprem.set(zrdecplrec.amountOut);
					wsaaZbinstprem.set(zrdecplcPojo.getAmountOut());
				}
				if (isNE(wsaaZlinstprem, 0)) {
	//				zrdecplrec.amountIn.set(wsaaZlinstprem);
					zrdecplcPojo.setAmountIn(wsaaZlinstprem.getbigdata());
					callRounding6000();
	//				wsaaZlinstprem.set(zrdecplrec.amountOut);
					wsaaZlinstprem.set(zrdecplcPojo.getAmountOut());
				}
				if (isNE(wsaaCommPrem, 0)) {
	//				zrdecplrec.amountIn.set(wsaaCommPrem);
					zrdecplcPojo.setAmountIn(wsaaCommPrem.getbigdata());
					callRounding6000();
	//				wsaaCommPrem.set(zrdecplrec.amountOut);
					wsaaCommPrem.set(zrdecplcPojo.getAmountOut());
				}
				compute(wsaaPremDiff, 3).setRounded(add(wsaaPremDiff,(sub(wsaaInstPrem,covrpf.getInstprem()))));
				rertpf = getrertObj(covrpfInsert);
				rertpf.setNewinst(wsaaInstPrem.getbigdata());
				rertpf.setZbnewinst(wsaaZbinstprem.getbigdata());
				rertpf.setZlnewinst(wsaaZlinstprem.getbigdata());
				rertpf.setCommprem(wsaaCommPrem.getbigdata());
				rertpf.setEffdate(wsaaRerateStore.toInt());
				checkExistingRertRecord(rertpf);
			}
		
		checkForIncrease3800(covrpfInsert);
		if (isGT(covrpfInsert.getRerateDate(),covrpfInsert.getPremCessDate())) {
			covrpfInsert.setRerateDate(covrpfInsert.getPremCessDate());
		}
		if (isGT(covrpfInsert.getRerateFromDate(),covrpfInsert.getPremCessDate())) {
			covrpfInsert.setRerateFromDate(covrpfInsert.getPremCessDate());
		}
		if (isEQ(wsaaZsredtrm,"Y")) {
			m100CheckReducing(covrpfInsert);
			m200UpdateUndr(covrpfInsert);
			covrpfInsert.setSumins(wsaaSumins.getbigdata());
		}
		covrpfInsert.setValidflag("1");
		compute(wsaaNewTranno, 0).set(add(chdrlifIO.getTranno(),1));
		rertpf.setTranno(wsaaNewTranno.toInt());
		covrpfInsert.setTranno(wsaaNewTranno.toInt());
		covrpfInsert.setCurrfrom(wsaaRerateStore.toInt());
		covrpfInsert.setCurrto(varcom.vrcmMaxDate.toInt());
		if(!prmhldtrad || (covrpf.getReinstated()==null || !"Y".equals(covrpf.getReinstated()))) {//ILIFE-8509
			covrpfInsert.setRiskprem(premiumrec.riskPrem.getbigdata()); //ILIFE-7845
			if(stampDutyflag) {
				rertpf.setNewzstpduty(premiumrec.zstpduty01.getbigdata());
				covrpfInsert.setZclstate(premiumrec.rstate01.toString());
			}
		}
		else if(prmhldtrad && "Y".equals(covrpf.getReinstated()))//ILIFE-8509
			covrpfInsert.setRiskprem(BigDecimal.ZERO);
		covrpfListInst.add(covrpfInsert);
		rertpfListInst.add(rertpf);
		this.ct02Val++;
	}
}

protected void checkExistingRertRecord(Rertpf rertpf) {
	if(!rertpfList.isEmpty()) {
		for(Rertpf rert : rertpfList) {
			if(rertpf.getLife().equals(rert.getLife()) && rertpf.getCoverage().equals(rert.getCoverage())
					&& rertpf.getRider().equals(rert.getRider()) && rertpf.getPlnsfx().equals(rert.getPlnsfx())
					&& rertpf.getEffdate()==rert.getEffdate()) {
				Rertpf r = new Rertpf(rert);
				r.setValidflag("2");
				rertpfUpdateList.add(r);
			}
		}
	}
}

protected Rertpf getrertObj(Covrpf covrpf){
	rertpf = new Rertpf();
	rertpf.setChdrcoy(covrpf.getChdrcoy());
	rertpf.setChdrnum(covrpf.getChdrnum());
	rertpf.setCoverage(covrpf.getCoverage());
	rertpf.setJlife(covrpf.getJlife());
	rertpf.setLife(covrpf.getLife());
	rertpf.setRider(covrpf.getRider());
	rertpf.setPlnsfx(covrpf.getPlanSuffix());
	rertpf.setValidflag("1");
	rertpf.setLastInst(covrpf.getInstprem());
	rertpf.setLastzstpduty(covrpf.getZstpduty01());
	rertpf.setZblastinst(covrpf.getZbinstprem());
	rertpf.setZllastinst(covrpf.getZlinstprem());
	return rertpf;
}

protected void checkMgp3360(Covrpf covrpf){
		if (isEQ(t5687rec.premGuarPeriod,0)) {
			covrpf.setRerateFromDate(covrpf.getRerateDate());
		}else {
//			datcon2rec.intDate1.set(covrpf.getCrrcd());
//			datcon2rec.frequency.set("01");
//			datcon2rec.freqFactor.set(t5687rec.premGuarPeriod);
//			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
//			if (isNE(datcon2rec.statuz,varcom.oK)) {
//				syserrrec.params.set(datcon2rec.datcon2Rec);
//				syserrrec.statuz.set(datcon2rec.statuz);
//				fatalError600();
//			}
//			if (isLT(datcon2rec.intDate2,covrpf.getRerateDate())) {
//				covrpf.setRerateFromDate(covrpf.getRerateDate());
//			}
//			else {
//				covrpf.setRerateFromDate(covrpf.getCrrcd());
//			}
			// changed by yy
			Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
			datcon2Pojo.setFreqFactor(t5687rec.premGuarPeriod.toInt());
			datcon2Pojo.setIntDate1(Integer.toString(covrpf.getCrrcd()));
			datcon2Pojo.setFrequency("01");
			datcon2Utils.calDatcon2(datcon2Pojo);
		
			if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
				syserrrec.statuz.set(datcon2Pojo.getStatuz());
				syserrrec.params.set(datcon2Pojo.toString());
				fatalError600();
			}
			
			if (isLT(datcon2Pojo.getIntDate2(), covrpf.getRerateDate())) {
				covrpf.setRerateFromDate(covrpf.getRerateDate());
			}
			else {
				covrpf.setRerateFromDate(covrpf.getCrrcd());
			}
		}
		
	}


protected void setRerateType3410(Covrpf covrpf)
	{
		wsaaDurationInt.set(ZERO);
		if (isNE(t5687rec.rtrnwfreq,NUMERIC)
		|| isEQ(t5687rec.rtrnwfreq,0)) {
			wsaaRerateType.set("2");
		}
		else {
//			datcon3rec.function.set(SPACES);
//			datcon3rec.intDate1.set(covrpf.getCrrcd());
//			datcon3rec.intDate2.set(covrpf.getRerateDate());
//			datcon3rec.frequency.set("01");
//			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
//			if (isNE(datcon3rec.statuz,varcom.oK)) {
//				syserrrec.statuz.set(datcon3rec.statuz);
//				syserrrec.params.set(datcon3rec.datcon3Rec);
//				fatalError600();
//			}
			// changed by yy
			Datcon3Pojo datcon3Pojo = new Datcon3Pojo();
			datcon3Pojo.setIntDate1(Integer.toString(covrpf.getCrrcd()));
			datcon3Pojo.setIntDate2(Integer.toString(covrpf.getRerateDate()));
			datcon3Pojo.setFrequency("01");
			datcon3Utils.calDatcon3(datcon3Pojo);
			String statuz = datcon3Pojo.getStatuz();
			if (isNE(statuz,varcom.oK)) {
				syserrrec.statuz.set(statuz);
				syserrrec.params.set(datcon3Pojo.toString());
				fatalError600();
			}
			
			compute(wsaaDurationInt, 5).setDivide(datcon3Pojo.getFreqFactor().intValue(), (t5687rec.rtrnwfreq));
			wsaaDurationRem.setRemainder(wsaaDurationInt);
			if (isNE(wsaaDurationRem,0)) {
				checkLextRerate3420(covrpf);
			}
		}
		if (lextRerate.isTrue()) {
//			compute(datcon2rec.freqFactor, 0).set((mult(t5687rec.rtrnwfreq,wsaaDurationInt)));
//			datcon2rec.intDate1.set(covrpf.getCrrcd());
//			datcon2rec.frequency.set("01");
//			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
//			if (isNE(datcon2rec.statuz,varcom.oK)) {
//				syserrrec.statuz.set(datcon2rec.statuz);
//				syserrrec.params.set(datcon2rec.datcon2Rec);
//				fatalError600();
//			}
//			wsaaLastRrtDate.set(datcon2rec.intDate2);
			// changed by yy
			Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
			datcon2Pojo.setFreqFactor((mult(t5687rec.rtrnwfreq,wsaaDurationInt)).toInt());
			datcon2Pojo.setIntDate1(Integer.toString(covrpf.getCrrcd()));
			datcon2Pojo.setFrequency("01");
			datcon2Utils.calDatcon2(datcon2Pojo);
		
			if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
				syserrrec.statuz.set(datcon2Pojo.getStatuz());
				syserrrec.params.set(datcon2Pojo.toString());
				fatalError600();
			}
			wsaaLastRrtDate.set(datcon2Pojo.getIntDate2());
		}
		else {
			wsaaLastRrtDate.set(covrpf.getRerateDate());
		}
	}
	
	protected void checkLextRerate3420(Covrpf covrpf) {
		Lextpf lextbrr = new Lextpf();
		/* IJTI-1386 START*/
		lextbrr.setChdrcoy(covrpf.getChdrcoy());
		lextbrr.setChdrnum(covrpf.getChdrnum());
		lextbrr.setLife(covrpf.getLife());
		lextbrr.setCoverage(covrpf.getCoverage());
		lextbrr.setRider(covrpf.getRider());
		/* IJTI-1386 END*/
		lextbrr.setExtCessDate(covrpf.getRerateDate());
		lextbrr = lextpfDAO.getLextbrrRecord(lextbrr);
		if (lextbrr != null) {
			wsaaRerateType.set("2");
		}
	}
	
protected void m100CheckReducing(Covrpf covrpf){

//		datcon3rec.function.set(SPACES);
//		datcon3rec.intDate1.set(chdrlifIO.getOccdate());
//		datcon3rec.intDate2.set(covrpf.getRerateDate());
//		datcon3rec.frequency.set("01");
//		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
//		if (isNE(datcon3rec.statuz,varcom.oK)) {
//			syserrrec.statuz.set(datcon3rec.statuz);
//			syserrrec.params.set(datcon3rec.datcon3Rec);
//			fatalError600();
//		}
//		compute(wsaaDuration, 5).set(add(datcon3rec.freqFactor,.99999));
// changed by yy
	Datcon3Pojo datcon3Pojo = new Datcon3Pojo();
	datcon3Pojo.setIntDate1(chdrlifIO.getOccdate().toString());
	datcon3Pojo.setIntDate2(Integer.toString(covrpf.getRerateDate()));
	datcon3Pojo.setFrequency("01");
		datcon3Utils.calDatcon3(datcon3Pojo);
		String statuz = datcon3Pojo.getStatuz();
		if (isNE(statuz,varcom.oK)) {
			syserrrec.statuz.set(statuz);
			syserrrec.params.set(datcon3Pojo.toString());
			fatalError600();
		}
		compute(wsaaDuration, 5).set(add(datcon3Pojo.getFreqFactor(),.99999));
	Mbnspf mbnspf=mbnsDAO.getmbnsRecord(covrpf.getChdrcoy().trim(),covrpf.getChdrnum().trim(),covrpf.getLife().trim(),
			covrpf.getCoverage().trim(),covrpf.getRider().trim(),wsaaDuration.toInt());/* IJTI-1386 */
	
	if (mbnspf != null) {
		wsaaSumins.set(mbnspf.getSumins());
	}
}

protected void m200UpdateUndr(Covrpf covrpf)
	{
		initialize(crtundwrec.parmRec);
//		a900ReadLife();
		crtundwrec.clntnum.set(lifepf.getLifcnum());
		crtundwrec.coy.set(covrpf.getChdrcoy());
		crtundwrec.currcode.set(chdrlifIO.getCntcurr());
		crtundwrec.chdrnum.set(covrpf.getChdrnum());
		crtundwrec.crtable.set(covrpf.getCrtable());
		crtundwrec.cnttyp.set(chdrlifIO.getCnttype());
		crtundwrec.function.set("DEL");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status,varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			fatalError600();
		}
		initialize(crtundwrec.parmRec);
//		a900ReadLife();
		crtundwrec.clntnum.set(lifepf.getLifcnum());
		crtundwrec.coy.set(covrpf.getChdrcoy());
		crtundwrec.chdrnum.set(covrpf.getChdrnum());
		crtundwrec.life.set(covrpf.getLife());
		crtundwrec.crtable.set(covrpf.getCrtable());
		crtundwrec.batctrcde.set(bprdIO.getAuthCode());
		crtundwrec.sumins.set(wsaaSumins);
		crtundwrec.cnttyp.set(chdrlifIO.getCnttype());
		crtundwrec.currcode.set(chdrlifIO.getCntcurr());
		crtundwrec.function.set("ADD");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status,varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			fatalError600();
		}
	}

	protected void commit3500() {
		this.commitControlTotals();
		
		chdrpfDAO.updateInvalidChdrRecord(this.chdrpfListUpdt);
		chdrpfListUpdt.clear();
		chdrpfDAO.insertChdrAmt(this.chdrpfListInst);
		chdrpfListInst.clear();
		
		payrpfDAO.updatePayrRecord(this.payrpfListUpdt);
		payrpfListUpdt.clear();
		payrpfDAO.insertPayrpfList(this.payrpfListInst);
		updatePayrCustomerSpecific(this.payrpfListInst);
		payrpfListInst.clear();
		
		incrpfDAO.insertIncrList(this.incrpfList);
		incrpfList.clear();
		insertCovrCustomerspecific();
		
		
		ptrnpfDAO.insertPtrnPF(this.ptrnpfList);
		ptrnpfList.clear();
		
		br613TempDAO.updateAgcmRecord(this.agcmpfListUpdt);
		agcmpfListUpdt.clear();
		br613TempDAO.insertAgcmRecord(this.agcmppfListInst);
		agcmppfListInst.clear();
		
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void updateAgcm3700(Covrpf covrpf,List<Agcmpf> agcmList){
	for(Agcmpf agcmpf: agcmList){
		if(agcmpf.getChdrnum().equals(covrpf.getChdrnum())
				&& agcmpf.getLife().equals(covrpf.getLife())
				&& agcmpf.getRider().equals(covrpf.getRider())
				&& agcmpf.getCoverage().equals(covrpf.getCoverage())
				&& agcmpf.getPlanSuffix() == covrpf.getPlanSuffix()){
			agcmUpdateLoop3710(covrpf,agcmpf);
			return;
		}
	}
}

protected void agcmUpdateLoop3710(Covrpf covrpf,Agcmpf agcmpf)
	{
		if (isNE(agcmpf.getDormantFlag(),"Y")) {
			Agcmpf agcmpfUpdate = new Agcmpf(agcmpf);
			agcmpfUpdate.setCurrto(wsaaRerateStore.toInt());
			agcmpfUpdate.setValidflag("2");
			this.agcmpfListUpdt.add(agcmpfUpdate);
//			if(!isSuccessful){
//				syserrrec.params.set("AGCMPF UPDATE FAIL");
//				syserrrec.statuz.set(Varcom.bomb);
//				fatalError600();
//			}
			writeNewAgcm3720(covrpf,agcmpf);
		}
	}

	protected void writeNewAgcm3720(Covrpf covrpf, Agcmpf agcmpf) {
		Agcmpf agcmpfInsert = new Agcmpf(agcmpf);
		agcmpfInsert.setTranno(wsaaNewTranno.toInt());
		agcmpfInsert.setCurrfrom(wsaaRerateStore.toInt());
		agcmpfInsert.setCurrto(varcom.vrcmMaxDate.toInt());
		agcmpfInsert.setValidflag("1");
		if (isEQ(covrpf.getPremCessDate(), covrpf.getRerateDate())) {
			/* CONTINUE_STMT */
		} else {
			agcmpfInsert.setAnnprem(agentAnnprem3730(covrpf, agcmpfInsert));
		}
		this.agcmppfListInst.add(agcmpfInsert);

	}

protected BigDecimal agentAnnprem3730(Covrpf covrpf, Agcmpf agcmpf)
	{
		wsaaAgntnum.set(agcmpf.getAgntnum());
		if (isEQ(agcmpf.getOvrdcat(),"O")) {
			wsaaAgntFound.set("N");
			wsaaCedagent.set(agcmpf.getCedagent());
			while ( !(agentFound.isTrue())) {
				findBasicAgent3740(agcmpf);
			}

		}
		List<Pcddpf> pcddpfList = pcddpfMap.get(corepfRec.getChdrnum());
		boolean isPcddFound =false;
		for (Pcddpf pcddpf : pcddpfList) {
			if (pcddpf.getAgntNum().equals(wsaaAgntnum.toString())) {
				isPcddFound =true;
				wsaaAnnprem.set(ZERO);
				wsaaAnnprem.set(agcmpf.getAnnprem());
				if (isNE(t5687rec.zrrcombas, SPACES)) {
					setPrecision(wsaaAnnprem, 3);
					wsaaAnnprem.setRounded(
							mult((mult(covrpfInsert.getZbinstprem(), wsaaBillfreqn)), (div(pcddpf.getSplitC(), 100))));
				} else {
					setPrecision(wsaaAnnprem, 3);
					wsaaAnnprem.setRounded(
							mult((mult(covrpfInsert.getInstprem(), wsaaBillfreqn)), (div(pcddpf.getSplitC(), 100))));
				}
				/* MOVE AGCMBCH-ANNPREM TO ZRDP-AMOUNT-IN. */
				/* PERFORM 6000-CALL-ROUNDING. */
				/* MOVE ZRDP-AMOUNT-OUT TO AGCMBCH-ANNPREM. */
				if (isNE(wsaaAnnprem, 0)) {
					zrdecplcPojo.setAmountIn(wsaaAnnprem.getbigdata());
					callRounding6000();
					wsaaAnnprem.set(zrdecplcPojo.getAmountIn());
				}
				return wsaaAnnprem.getbigdata();
			}
		}
		if(!isPcddFound){
			syserrrec.params.set("PCDDPF AGNT" + wsaaAgntnum.toString());
			syserrrec.statuz.set(Varcom.mrnf);
			fatalError600();
		}
		if (pcddpfList == null || pcddpfList.size() == 0) {
			syserrrec.params.set("PCDDPF");
			syserrrec.statuz.set(Varcom.mrnf);
			fatalError600();
		}
		return null;
	}

protected void findBasicAgent3740(Agcmpf agcmpf)
	{
	agcmpf.setChdrcoy(agcmpf.getChdrcoy());
	agcmpf.setChdrnum(agcmpf.getChdrnum());
	agcmpf.setLife(agcmpf.getLife());
	agcmpf.setCoverage(agcmpf.getCoverage());
	agcmpf.setRider(agcmpf.getRider());
	agcmpf.setPlanSuffix(agcmpf.getPlanSuffix());
	agcmpf.setAgntnum(wsaaCedagent.toString());
	Agcmpf agcm = agcmpfDAO.searchAgcmRecord(agcmpf);
	if (agcm == null) {
		fatalError600();
	}
	if (isEQ(agcm.getOvrdcat(),"B")) {
		wsaaAgntFound.set("Y");
		wsaaAgntnum.set(agcm.getAgntnum());
	}
	else {
		wsaaCedagent.set(agcm.getCedagent());
	}
}

protected void checkForIncrease3800(Covrpf covrpf)
	{
		Incrpf incrhstIO = null;
		if (incrpfMap != null && incrpfMap.containsKey(covrpf.getChdrnum())) {
			for (Incrpf c : incrpfMap.get(covrpf.getChdrnum())) {
				/* IJTI-1386 START*/
				if (covrpf.getChdrcoy().equals(c.getChdrcoy())
						&& covrpf.getLife().equals(c.getLife()) 
						&& covrpf.getCoverage().equals(c.getCoverage())
						&& covrpf.getRider().equals(c.getRider())
						/* IJTI-1386 END*/
						&& covrpf.getPlanSuffix() == c.getPlnsfx()
						&& "2".equals(c.getValidflag())
						&& "".equals(c.getRefusalFlag().trim())) {
					incrhstIO = c;
					break;
				}
			}
		}
		
		if (incrhstIO == null) {
			return;
		}

		/* Write a new historical INCR record based on the details of*/
		/* the last record.  The main changes from the previous record*/
		/* are that the LASTINST comes from the NEWINST on the previous*/
		/* record and that the increase in premium caused by the*/
		/* rerate is added to the NEWINST to give the 'new' NEWINST.*/
		/* The TRANNO and date/time fields are also updated.*/
		incrhstIO.setLastInst(incrhstIO.getNewinst());
		incrhstIO.setZblastinst(incrhstIO.getZbnewinst());
		incrhstIO.setZllastinst(incrhstIO.getZlnewinst());
		setPrecision(incrhstIO.getNewinst(), 2);
		incrhstIO.setNewinst(add(incrhstIO.getNewinst(),(sub(covrpf.getInstprem(),covrpf.getInstprem()))).getbigdata());
		setPrecision(incrhstIO.getZbnewinst(), 2);
		incrhstIO.setZbnewinst(add(incrhstIO.getZbnewinst(),(sub(covrpf.getZbinstprem(),covrpf.getZbinstprem()))).getbigdata());
		setPrecision(incrhstIO.getZlnewinst(), 2);
		incrhstIO.setZlnewinst(add(incrhstIO.getZlnewinst(),(sub(covrpf.getZlinstprem(),covrpf.getZlinstprem()))).getbigdata());
		setPrecision(incrhstIO.getTranno(), 0);
		incrhstIO.setTranno(add(chdrlifIO.getTranno(),1).toInt());
		incrhstIO.setStatcode(covrpf.getStatcode());/* IJTI-1386 */
		incrhstIO.setPstatcode(covrpf.getPstatcode());/* IJTI-1386 */
		incrhstIO.setCrrcd(bsscIO.getEffectiveDate().toInt());
		incrhstIO.setTransactionDate(wsaaTransactionDate);
		incrhstIO.setTransactionTime(wsaaTransactionTime);
		incrhstIO.setTermid(wsaaTermid);
		incrhstIO.setUser(wsaaUser);
		setPrecision(incrhstIO.getZstpduty01(), 2);
		incrhstIO.setZstpduty01(add(incrhstIO.getNewinst(),(sub(covrpf.getZstpduty01(),covrpf.getZstpduty01()))).getbigdata());
		this.incrpfList.add(incrhstIO);
	}

protected void genericProcessing3900(Covrpf covrpf)
	{
		wsaaT5671Item.set(SPACES);
		wsaaItemTranCode.set(batcdorrec.trcde);
		wsaaItemCrtable.set(covrpf.getCrtable());

		List<Itempf> t5671List = t5671ListMap.get(wsaaT5671Item);
		if (t5671List != null && wsaaT5671Item.length() !=8 ) {
			syserrrec.params.set("T5671" + wsaaT5671Item);
			fatalError600();
		}
		if(t5671List == null || t5671List.size() ==0){
			t5671rec.t5671Rec.set(SPACES);
		}
		else{
			t5671rec.t5671Rec.set(StringUtil.rawToString(t5671List.get(0).getGenarea()));
		}
		isuallrec.isuallRec.set(SPACES);
		isuallrec.function.set("ACTIN");
		isuallrec.company.set(covrpf.getChdrcoy());
		isuallrec.chdrnum.set(covrpf.getChdrnum());
		isuallrec.life.set(covrpf.getLife());
		isuallrec.coverage.set(covrpf.getCoverage());
		isuallrec.rider.set(covrpf.getRider());
		isuallrec.planSuffix.set(covrpf.getPlanSuffix());
		isuallrec.oldcovr.set(covrpf.getCoverage());
		isuallrec.oldrider.set(covrpf.getRider());
		if (isEQ(wsaaFullyPaid,"Y")) {
			isuallrec.freqFactor.set(0);
		}
		else {
			isuallrec.freqFactor.set(1);
		}
		isuallrec.batchkey.set(batcdorrec.batchkey);
		isuallrec.transactionDate.set(wsaaTransactionDate);
		isuallrec.transactionTime.set(wsaaTransactionTime);
		isuallrec.user.set(wsaaUser);
		isuallrec.termid.set(wsaaTermid);
		isuallrec.effdate.set(covrpf.getCrrcd());
		isuallrec.newTranno.set(covrpf.getTranno());
		isuallrec.covrSingp.set(ZERO);
		isuallrec.covrInstprem.set(covrpf.getInstprem());
		isuallrec.language.set(bsscIO.getLanguage());
		if (isGT(chdrlifIO.getPolinc(),1)) {
			isuallrec.convertUnlt.set("Y");
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub,4)); wsaaSub.add(1)){
			subroutineCall3950();
		}
	}

protected void subroutineCall3950()
	{
		/*CALL*/
		isuallrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.subprog[wsaaSub.toInt()],SPACES)) {
			callProgram(t5671rec.subprog[wsaaSub.toInt()], isuallrec.isuallRec);
		}
		if (isNE(isuallrec.statuz,varcom.oK)) {
			syserrrec.params.set(isuallrec.isuallRec);
			syserrrec.statuz.set(isuallrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	protected void close4000() {
		chdrpfMap.clear();
		chdrpfMap = null;
		payrpfMap.clear();
		payrpfMap = null;
		incrpfMap.clear();
		incrpfMap = null;
		ptrnpfList.clear();
		ptrnpfList = null;
		chdrpfListUpdt.clear();
		chdrpfListUpdt = null;
		chdrpfListInst.clear();
		chdrpfListInst = null;
		payrpfListUpdt.clear();
		payrpfListUpdt = null;
		payrpfListInst.clear();
		payrpfListInst = null;
		incrpfList.clear();
		incrpfList = null;
		iter = null;
		agcmpfListUpdt.clear();
		agcmpfListUpdt = null;
		
		agcmppfListInst.clear();
		agcmppfListInst = null;
		lsaaStatuz.set(varcom.oK);
	}

protected void updateChdrPtrn4100()
	{
		Ptrnpf ptrnIO = new Ptrnpf();
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy().toString());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTranno(wsaaNewTranno.toInt());
		ptrnIO.setPtrneff(wsaaRerateStore.toInt());
		ptrnIO.setTrdt(wsaaTransactionDate);
		ptrnIO.setTrtm(wsaaTransactionTime);
		ptrnIO.setUserT(wsaaUser);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setBatcpfx(batcdorrec.prefix.toString());
		ptrnIO.setBatccoy(batcdorrec.company.toString());
		ptrnIO.setBatcbrn(batcdorrec.branch.toString());
		ptrnIO.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnIO.setBatctrcde(bprdIO.getAuthCode().toString());
		ptrnIO.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnIO.setBatcbatch(batcdorrec.batch.toString());
		ptrnIO.setDatesub(bsscIO.getEffectiveDate().toInt());
		this.ptrnpfList.add(ptrnIO);
		Chdrpf chdrpf = new Chdrpf(chdrlifIO);
		chdrpf.setValidflag('2');
		//chdrpf.setCurrto(bsscIO.getEffectiveDate().toInt());
		chdrpf.setCurrto(wsaaRerateStore.toInt());
		chdrpf.setUniqueNumber(chdrlifIO.getUniqueNumber());
		this.chdrpfListUpdt.add(chdrpf);
		chdrlifIO.setValidflag('1');
		chdrlifIO.setCurrto(varcom.vrcmMaxDate.toInt());
		chdrlifIO.setCurrfrom(wsaaRerateStore.toInt());
		chdrlifIO.setTranno(wsaaNewTranno.toInt());
		//ILIFE-8763 ILIFE-9402
	/*	if (isNE(t5679rec.setCnPremStat,SPACES)) {
			chdrlifIO.setPstcde(t5679rec.setCnPremStat.toString());
		}*/
		//ILIFE-8763  ILIFE-9402
		setChdrCustomerSpecific();
		this.chdrpfListInst.add(chdrlifIO);
		this.ct03Val.add(wsaaPremDiff);
	}

protected void allCovrsExpired4150()
	{
		/*START*/
		compute(wsaaPremDiff, 2).set(sub(wsaaPremDiff,chdrlifIO.getSinstamt02()));
		chdrlifIO.setSinstamt02(BigDecimal.ZERO);
		if (isNE(t5679rec.setCnPremStat,SPACES)) {
			chdrlifIO.setPstcde(t5679rec.setCnPremStat.toString());
		}
		/*EXIT*/
	}

protected void updatePayr4200()
	{
		getPayrpf();  //ILIFE-4323 by dpuhawan
		Payrpf payrpf = new Payrpf(payrIO);
		payrpf.setValidflag("2");
		this.payrpfListUpdt.add(payrpf);
		payrIO.setValidflag("1");
		payrIO.setEffdate(wsaaRerateStore.toInt());
		payrIO.setTranno(wsaaNewTranno.toInt());
		
		setPayrCustomerSpecific();
//		datcon2rec.intDate1.set(chdrlifIO.getBillcd());
//		datcon2rec.frequency.set("DY");
//		compute(datcon2rec.freqFactor, 0).set(mult(wsaaLeadDays,-1));
//		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
//		if (isNE(datcon2rec.statuz,varcom.oK)) {
//			syserrrec.statuz.set(datcon2rec.statuz);
//			syserrrec.params.set(datcon2rec.datcon2Rec);
//			fatalError600();
//		}
//		payrIO.setNextdate(datcon2rec.intDate2.toInt());
		// changed by yy
		Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
		datcon2Pojo.setFreqFactor(mult(wsaaLeadDays,-1).toInt());
		datcon2Pojo.setIntDate1(chdrlifIO.getBillcd().toString());
		datcon2Pojo.setFrequency("DY");
		datcon2Utils.calDatcon2(datcon2Pojo);
	
		if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
			syserrrec.statuz.set(datcon2Pojo.getStatuz());
			syserrrec.params.set(datcon2Pojo.toString());
			fatalError600();
		}
		payrIO.setNextdate(Integer.parseInt(datcon2Pojo.getIntDate2()));
		this.payrpfListInst.add(payrIO);
	}

protected void unlockChdr4300()
	{
	sftlockRecBean.setFunction("UNLK");
	sftlockRecBean.setCompany(batcdorrec.company.toString());
	sftlockRecBean.setEnttyp("CH");
	sftlockRecBean.setEntity(chdrlifIO.getChdrnum());
	sftlockRecBean.setTransaction(bprdIO.getAuthCode().toString());
	sftlockRecBean.setUser("0");
	sftlockUtil.process(sftlockRecBean, appVars);
	if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())) {
			syserrrec.statuz.set(sftlockRecBean.getStatuz());
			fatalError600();
	}
 }

protected void statistics5000()
	{
		lifsttrrec.batccoy.set(batcdorrec.company);
		lifsttrrec.batcbrn.set(batcdorrec.branch);
		lifsttrrec.batcactyr.set(batcdorrec.actyear);
		lifsttrrec.batcactmn.set(batcdorrec.actmonth);
		lifsttrrec.batctrcde.set(bprdIO.getAuthCode());
		lifsttrrec.batcbatch.set(batcdorrec.batch);
		lifsttrrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrlifIO.getChdrnum());
		lifsttrrec.tranno.set(wsaaNewTranno);
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			fatalError600();
		}
	}

	protected void callRounding6000() {
		/* CALL */
		zrdecplcPojo.setFunction(SPACES.toString());
		zrdecplcPojo.setStatuz(Varcom.oK.toString());
		zrdecplcPojo.setCompany(bsprIO.getCompany().toString());
		zrdecplcPojo.setCurrency(chdrlifIO.getCntcurr());/* IJTI-1386 */
		zrdecplcPojo.setBatctrcde(bprdIO.getAuthCode().toString());
		zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
		if (isNE(zrdecplcPojo.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zrdecplcPojo.getStatuz());
			syserrrec.params.set(zrdecplcPojo.toString());
			fatalError600();
		}
		/* EXIT */
	}

	private void commitControlTotals() {
		contotrec.totno.set(ct01);
		contotrec.totval.set(ct01Val);
		callContot001();
		ct01Val = 0;

		contotrec.totno.set(ct05);
		contotrec.totval.set(ct05Val);
		callContot001();
		ct05Val = 0;
		
		contotrec.totno.set(ct04);
		contotrec.totval.set(ct04Val);
		callContot001();
		ct04Val=0;
				
		contotrec.totno.set(ct02);
		contotrec.totval.set(ct02Val);
		callContot001();
		ct02Val = 0;
		
		contotrec.totno.set(ct03);
		contotrec.totval.set(wsaaPremDiff);
		callContot001();
		ct03Val.set(ZERO);
	}	

	protected void insertCovrCustomerspecific(){
		br613TempDAO.updateCovrRecord(this.covrpfListUpdt);
		covrpfListUpdt.clear();
		br613TempDAO.insertCovrList(this.covrpfListInst);
		covrpfListInst.clear();
		rertpfDAO.insertRertList(rertpfListInst);
		if(!rertpfUpdateList.isEmpty()) {
			rertpfDAO.updateRertList(rertpfUpdateList);
			rertpfUpdateList.clear();
		}
		rertpfListInst.clear();
	}
	protected void setChdrCustomerSpecific(){
		setPrecision(chdrlifIO.getSinstamt01(), 2);
//		chdrlifIO.setSinstamt01(add(chdrlifIO.getSinstamt01(),wsaaPremDiff).getbigdata());
		if (isEQ(wsaaCovrsLive,"N")) {
			allCovrsExpired4150();
		}
		setPrecision(chdrlifIO.getSinstamt06(), 2);
//		chdrlifIO.setSinstamt06(add(chdrlifIO.getSinstamt06(),wsaaPremDiff).getbigdata());
		
	}
	protected void updatePayrCustomerSpecific(List<Payrpf> payrpfListInst){
		
	}
	protected void setPayrCustomerSpecific(){
		setPrecision(payrIO.getSinstamt06(), 2);
//		payrIO.setSinstamt06(add(payrIO.getSinstamt06(),wsaaPremDiff).getbigdata());
		if (isEQ(wsaaCovrsLive,"N")) {
			payrIO.setSinstamt02(BigDecimal.ZERO);
			payrIO.setSinstamt01(BigDecimal.ZERO);
		}
		else {
			setPrecision(payrIO.getSinstamt01(), 2);
//			payrIO.setSinstamt01(add(payrIO.getSinstamt01(),wsaaPremDiff).getbigdata());
		}
	}
}