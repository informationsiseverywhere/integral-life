package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.Zptnpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ZptnpfDAO extends BaseDAO<Zptnpf> {
    public void insertZptnpf(List<Zptnpf> zptnpfList);
    public List<Zptnpf> searchZptnrevRecord(String coy, String chdrnum);
}