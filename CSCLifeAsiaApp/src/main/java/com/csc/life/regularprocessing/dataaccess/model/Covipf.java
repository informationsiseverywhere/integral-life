package com.csc.life.regularprocessing.dataaccess.model;

import java.math.BigDecimal;

public class Covipf{
    private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private int planSuffix;
	private String validflag;
	private String statcode;
	private String pstatcode;
	private int crrcd;
	private String premCurrency;
	private String crtable;
	private BigDecimal singp;
	private int riskCessDate;
	private BigDecimal sumins;
	private String mortcls;
	private String indexationInd;
	private BigDecimal instprem;
	private BigDecimal zbinstprem;
	private BigDecimal zlinstprem;
	private int cpiDate;
    public long getUniqueNumber() {
        return uniqueNumber;
    }
    public void setUniqueNumber(long uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }
    public String getChdrcoy() {
        return chdrcoy;
    }
    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }
    public String getChdrnum() {
        return chdrnum;
    }
    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }
    public String getLife() {
        return life;
    }
    public void setLife(String life) {
        this.life = life;
    }
    public String getJlife() {
        return jlife;
    }
    public void setJlife(String jlife) {
        this.jlife = jlife;
    }
    public String getCoverage() {
        return coverage;
    }
    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }
    public String getRider() {
        return rider;
    }
    public void setRider(String rider) {
        this.rider = rider;
    }
    public int getPlanSuffix() {
        return planSuffix;
    }
    public void setPlanSuffix(int planSuffix) {
        this.planSuffix = planSuffix;
    }
    public String getValidflag() {
        return validflag;
    }
    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }
    public String getStatcode() {
        return statcode;
    }
    public void setStatcode(String statcode) {
        this.statcode = statcode;
    }
    public String getPstatcode() {
        return pstatcode;
    }
    public void setPstatcode(String pstatcode) {
        this.pstatcode = pstatcode;
    }
    public int getCrrcd() {
        return crrcd;
    }
    public void setCrrcd(int crrcd) {
        this.crrcd = crrcd;
    }
    public String getPremCurrency() {
        return premCurrency;
    }
    public void setPremCurrency(String premCurrency) {
        this.premCurrency = premCurrency;
    }
    public String getCrtable() {
        return crtable;
    }
    public void setCrtable(String crtable) {
        this.crtable = crtable;
    }
    public BigDecimal getSingp() {
        return singp;
    }
    public void setSingp(BigDecimal singp) {
        this.singp = singp;
    }
    public int getRiskCessDate() {
        return riskCessDate;
    }
    public void setRiskCessDate(int riskCessDate) {
        this.riskCessDate = riskCessDate;
    }
    public BigDecimal getSumins() {
        return sumins;
    }
    public void setSumins(BigDecimal sumins) {
        this.sumins = sumins;
    }
    public String getMortcls() {
        return mortcls;
    }
    public void setMortcls(String mortcls) {
        this.mortcls = mortcls;
    }
    public String getIndexationInd() {
        return indexationInd;
    }
    public void setIndexationInd(String indexationInd) {
        this.indexationInd = indexationInd;
    }
    public BigDecimal getInstprem() {
        return instprem;
    }
    public void setInstprem(BigDecimal instprem) {
        this.instprem = instprem;
    }
    public BigDecimal getZbinstprem() {
        return zbinstprem;
    }
    public void setZbinstprem(BigDecimal zbinstprem) {
        this.zbinstprem = zbinstprem;
    }
    public BigDecimal getZlinstprem() {
        return zlinstprem;
    }
    public void setZlinstprem(BigDecimal zlinstprem) {
        this.zlinstprem = zlinstprem;
    }
    public int getCpiDate() {
        return cpiDate;
    }
    public void setCpiDate(int cpiDate) {
        this.cpiDate = cpiDate;
    }
}