package com.csc.life.regularprocessing.dataaccess.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Incrpf implements Serializable {

	private static final long serialVersionUID = 2998401309503064542L;

	@Id
	@Column(name = "UNIQUE_NUMBER")
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer plnsfx;//IJTI-1726
	private int effdate;
	private int tranno;
	private String validflag;
	private String statcode;
	private String pstatcode;
	private int crrcd;
	private String crtable;
	private BigDecimal origInst;
	private BigDecimal lastInst;
	private BigDecimal newinst;
	private BigDecimal origSum;
	private BigDecimal lastSum;
	private BigDecimal newsum;
	private String annvry;
	private String bascmeth;
	private int pctinc;
	private String refflag;
	private int trdt;
	private int trtm;
	private int user;
	private String termid;
	private String bascpy;
	private String ceaseInd;
	private String rnwcpy;
	private String srvcpy;
	private BigDecimal zboriginst;
	private BigDecimal zblastinst;
	private BigDecimal zbnewinst;
	private BigDecimal zloriginst;
	private BigDecimal zllastinst;
	private BigDecimal zlnewinst;
	private BigDecimal zstpduty01;
	private String usrprf;
	private String jobnm;
	private Date datime;
	
	public Incrpf() {
		super();
	}
	public Incrpf(Incrpf incrpf) {//ILIFE-8509
		this.uniqueNumber=incrpf.uniqueNumber;
		this.chdrcoy=incrpf.chdrcoy;
		this.chdrnum=incrpf.chdrnum;
		this.life=incrpf.life;
		this.jlife=incrpf.jlife;
		this.coverage=incrpf.coverage;
		this.rider=incrpf.rider;
		this.plnsfx=incrpf.plnsfx;
		this.effdate=incrpf.effdate;
		this.tranno=incrpf.tranno;
		this.validflag=incrpf.validflag;
		this.statcode=incrpf.statcode;
		this.pstatcode=incrpf.pstatcode;
		this.crrcd=incrpf.crrcd;
		this.crtable=incrpf.crtable;
		this.origInst=incrpf.origInst;
		this.lastInst=incrpf.lastInst;
		this.newinst=incrpf.newinst;
		this.origSum=incrpf.origSum;
		this.lastSum=incrpf.lastSum;
		this.newsum=incrpf.newsum;
		this.annvry=incrpf.annvry;
		this.bascmeth=incrpf.bascmeth;
		this.pctinc=incrpf.pctinc;
		this.refflag=incrpf.refflag;
		this.trdt=incrpf.trdt;
		this.trtm=incrpf.trtm;
		this.user=incrpf.user;
		this.termid=incrpf.termid;
		this.bascpy=incrpf.bascpy;
		this.ceaseInd=incrpf.ceaseInd;
		this.rnwcpy=incrpf.rnwcpy;
		this.srvcpy=incrpf.srvcpy;
		this.zboriginst=incrpf.zboriginst;
		this.zblastinst=incrpf.zblastinst;
		this.zbnewinst=incrpf.zbnewinst;
		this.zloriginst=incrpf.zloriginst;
		this.zllastinst=incrpf.zllastinst;
		this.zlnewinst=incrpf.zlnewinst;
		this.zstpduty01=incrpf.zstpduty01;
		this.usrprf=incrpf.usrprf;
		this.jobnm=incrpf.jobnm;
		this.datime=incrpf.datime;
	}
	public long getUniqueNumber() {
        return uniqueNumber;
    }
    public void setUniqueNumber(long uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }
	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getJlife() {
		return jlife;
	}

	public void setJlife(String jlife) {
		this.jlife = jlife;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public Integer getPlnsfx() { //IJTI-1726
		return plnsfx;
	}

	public void setPlnsfx(Integer plnsfx) { //IJTI-1726
		this.plnsfx = plnsfx;
	}

	public int getEffdate() {
		return effdate;
	}

	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public String getStatcode() {
		return statcode;
	}

	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	public String getPstatcode() {
		return pstatcode;
	}

	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}

	public int getCrrcd() {
		return crrcd;
	}

	public void setCrrcd(int crrcd) {
		this.crrcd = crrcd;
	}

	public String getCrtable() {
		return crtable;
	}

	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	public BigDecimal getOrigInst() {
		return origInst;
	}

	public void setOrigInst(BigDecimal origInst) {
		this.origInst = origInst;
	}

	public BigDecimal getLastInst() {
		return lastInst;
	}

	public void setLastInst(BigDecimal lastInst) {
		this.lastInst = lastInst;
	}

	public BigDecimal getNewinst() {
		return newinst;
	}

	public void setNewinst(BigDecimal newinst) {
		this.newinst = newinst;
	}

	public BigDecimal getOrigSum() {
		return origSum;
	}

	public void setOrigSum(BigDecimal origSum) {
		this.origSum = origSum;
	}

	public BigDecimal getLastSum() {
		return lastSum;
	}

	public void setLastSum(BigDecimal lastSum) {
		this.lastSum = lastSum;
	}

	public BigDecimal getNewsum() {
		return newsum;
	}

	public void setNewsum(BigDecimal newsum) {
		this.newsum = newsum;
	}

	public String getAnniversaryMethod() {
		return annvry;
	}

	public void setAnniversaryMethod(String annvry) {
		this.annvry = annvry;
	}

	public String getBasicCommMeth() {
		return bascmeth;
	}

	public void setBasicCommMeth(String bascmeth) {
		this.bascmeth = bascmeth;
	}

	public int getPctinc() {
		return pctinc;
	}

	public void setPctinc(int pctinc) {
		this.pctinc = pctinc;
	}

	public String getRefusalFlag() {
		return refflag;
	}

	public void setRefusalFlag(String refflag) {
		this.refflag = refflag;
	}

	public int getTransactionDate() {
		return trdt;
	}

	public void setTransactionDate(int transactionDate) {
		this.trdt = transactionDate;
	}

	public int getTransactionTime() {
		return trtm;
	}

	public void setTransactionTime(int transactionTime) {
		this.trtm = transactionTime;
	}

	public int getUser() {
		return user;
	}

	public void setUser(int user) {
		this.user = user;
	}

	public String getTermid() {
		return termid;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}

	public String getBascpy() {
		return bascpy;
	}

	public void setBascpy(String bascpy) {
		this.bascpy = bascpy;
	}

	public String getCeaseInd() {
		return ceaseInd;
	}

	public void setCeaseInd(String ceaseInd) {
		this.ceaseInd = ceaseInd;
	}

	public String getRnwcpy() {
		return rnwcpy;
	}

	public void setRnwcpy(String rnwcpy) {
		this.rnwcpy = rnwcpy;
	}

	public String getSrvcpy() {
		return srvcpy;
	}

	public void setSrvcpy(String srvcpy) {
		this.srvcpy = srvcpy;
	}

	public BigDecimal getZboriginst() {
		return zboriginst;
	}

	public void setZboriginst(BigDecimal zboriginst) {
		this.zboriginst = zboriginst;
	}

	public BigDecimal getZblastinst() {
		return zblastinst;
	}

	public void setZblastinst(BigDecimal zblastinst) {
		this.zblastinst = zblastinst;
	}

	public BigDecimal getZbnewinst() {
		return zbnewinst;
	}

	public void setZbnewinst(BigDecimal zbnewinst) {
		this.zbnewinst = zbnewinst;
	}

	public BigDecimal getZloriginst() {
		return zloriginst;
	}

	public void setZloriginst(BigDecimal zloriginst) {
		this.zloriginst = zloriginst;
	}

	public BigDecimal getZllastinst() {
		return zllastinst;
	}

	public void setZllastinst(BigDecimal zllastinst) {
		this.zllastinst = zllastinst;
	}

	public BigDecimal getZlnewinst() {
		return zlnewinst;
	}

	public void setZlnewinst(BigDecimal zlnewinst) {
		this.zlnewinst = zlnewinst;
	}

	public BigDecimal getZstpduty01() {
		return zstpduty01;
	}
	
	public void setZstpduty01(BigDecimal zstpduty01) {
		this.zstpduty01 = zstpduty01;
	}
	public String getUserProfile() {
		return usrprf;
	}

	public void setUserProfile(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobName() {
		return jobnm;
	}

	public void setJobName(String jobName) {
		this.jobnm = jobName;
	}

	public Date getDatime() {
		return new Date(datime.getTime());//IJTI-316
	}

	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());//IJTI-314
	}
	
}
