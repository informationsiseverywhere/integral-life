package com.csc.life.regularprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:33
 * Description:
 * Copybook name: T5654REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5654rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5654Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData indxflg = new FixedLengthStringData(1).isAPartOf(t5654Rec, 0);
	public FixedLengthStringData cpidef = new FixedLengthStringData(1).isAPartOf(t5654Rec, 1);
	public FixedLengthStringData cpiallwd = new FixedLengthStringData(1).isAPartOf(t5654Rec, 2);
	public FixedLengthStringData predef = new FixedLengthStringData(1).isAPartOf(t5654Rec, 3);
	public FixedLengthStringData predefallwd = new FixedLengthStringData(1).isAPartOf(t5654Rec, 4);
  	public FixedLengthStringData filler = new FixedLengthStringData(495).isAPartOf(t5654Rec, 5, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5654Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5654Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}