package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.regularprocessing.dataaccess.model.Rertpf;

public interface RertpfDAO {

	public List<Rertpf> getRertpfList( String chdrcoy,String chdrnum, String validflag, int tranno);
	public boolean insertRertList(List<Rertpf> rertpfList);
	public Map<String, List<Rertpf>> searchRertRecordByChdrnum(String chdrcoy, List<String> chdrnumList);
    public void updateRertList(List<Rertpf> rertpfUpdateList);
    public List<Rertpf> getRertpfList(String chdrcoy, String chdrnum, String life, String jlife, String coverage, String rider, int plnsfx);
    public void deleteRertpf(List<Rertpf> rertList);
}
