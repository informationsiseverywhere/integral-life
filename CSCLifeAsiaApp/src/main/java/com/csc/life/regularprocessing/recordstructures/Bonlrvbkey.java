package com.csc.life.regularprocessing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:50
 * Description:
 * Copybook name: BONLRVBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Bonlrvbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData bonlrvbFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData bonlrvbKey = new FixedLengthStringData(64).isAPartOf(bonlrvbFileKey, 0, REDEFINE);
  	public FixedLengthStringData bonlrvbChdrcoy = new FixedLengthStringData(1).isAPartOf(bonlrvbKey, 0);
  	public FixedLengthStringData bonlrvbChdrnum = new FixedLengthStringData(8).isAPartOf(bonlrvbKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(bonlrvbKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(bonlrvbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bonlrvbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}