package com.csc.life.regularprocessing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5399screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5399ScreenVars sv = (S5399ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5399screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5399ScreenVars screenVars = (S5399ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.covRiskStat01.setClassString("");
		screenVars.covRiskStat02.setClassString("");
		screenVars.covRiskStat03.setClassString("");
		screenVars.covRiskStat04.setClassString("");
		screenVars.covRiskStat05.setClassString("");
		screenVars.covRiskStat06.setClassString("");
		screenVars.covRiskStat07.setClassString("");
		screenVars.covRiskStat08.setClassString("");
		screenVars.covRiskStat09.setClassString("");
		screenVars.covRiskStat10.setClassString("");
		screenVars.covRiskStat11.setClassString("");
		screenVars.covRiskStat12.setClassString("");
		screenVars.setCnRiskStat01.setClassString("");
		screenVars.setCnRiskStat02.setClassString("");
		screenVars.setCnRiskStat03.setClassString("");
		screenVars.setCnRiskStat04.setClassString("");
		screenVars.setCnRiskStat05.setClassString("");
		screenVars.setCnRiskStat06.setClassString("");
		screenVars.setCnRiskStat07.setClassString("");
		screenVars.setCnRiskStat08.setClassString("");
		screenVars.setCnRiskStat09.setClassString("");
		screenVars.setCnRiskStat10.setClassString("");
		screenVars.setCnRiskStat11.setClassString("");
		screenVars.setCnRiskStat12.setClassString("");
		screenVars.covPremStat01.setClassString("");
		screenVars.setCnPremStat01.setClassString("");
		screenVars.covPremStat02.setClassString("");
		screenVars.setCnPremStat02.setClassString("");
		screenVars.covPremStat03.setClassString("");
		screenVars.setCnPremStat03.setClassString("");
		screenVars.covPremStat04.setClassString("");
		screenVars.setCnPremStat04.setClassString("");
		screenVars.covPremStat05.setClassString("");
		screenVars.setCnPremStat05.setClassString("");
		screenVars.covPremStat06.setClassString("");
		screenVars.setCnPremStat06.setClassString("");
		screenVars.covPremStat07.setClassString("");
		screenVars.setCnPremStat07.setClassString("");
		screenVars.covPremStat08.setClassString("");
		screenVars.setCnPremStat08.setClassString("");
		screenVars.covPremStat09.setClassString("");
		screenVars.setCnPremStat09.setClassString("");
		screenVars.covPremStat10.setClassString("");
		screenVars.setCnPremStat10.setClassString("");
		screenVars.covPremStat11.setClassString("");
		screenVars.setCnPremStat11.setClassString("");
		screenVars.covPremStat12.setClassString("");
		screenVars.setCnPremStat12.setClassString("");
	}

/**
 * Clear all the variables in S5399screen
 */
	public static void clear(VarModel pv) {
		S5399ScreenVars screenVars = (S5399ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.covRiskStat01.clear();
		screenVars.covRiskStat02.clear();
		screenVars.covRiskStat03.clear();
		screenVars.covRiskStat04.clear();
		screenVars.covRiskStat05.clear();
		screenVars.covRiskStat06.clear();
		screenVars.covRiskStat07.clear();
		screenVars.covRiskStat08.clear();
		screenVars.covRiskStat09.clear();
		screenVars.covRiskStat10.clear();
		screenVars.covRiskStat11.clear();
		screenVars.covRiskStat12.clear();
		screenVars.setCnRiskStat01.clear();
		screenVars.setCnRiskStat02.clear();
		screenVars.setCnRiskStat03.clear();
		screenVars.setCnRiskStat04.clear();
		screenVars.setCnRiskStat05.clear();
		screenVars.setCnRiskStat06.clear();
		screenVars.setCnRiskStat07.clear();
		screenVars.setCnRiskStat08.clear();
		screenVars.setCnRiskStat09.clear();
		screenVars.setCnRiskStat10.clear();
		screenVars.setCnRiskStat11.clear();
		screenVars.setCnRiskStat12.clear();
		screenVars.covPremStat01.clear();
		screenVars.setCnPremStat01.clear();
		screenVars.covPremStat02.clear();
		screenVars.setCnPremStat02.clear();
		screenVars.covPremStat03.clear();
		screenVars.setCnPremStat03.clear();
		screenVars.covPremStat04.clear();
		screenVars.setCnPremStat04.clear();
		screenVars.covPremStat05.clear();
		screenVars.setCnPremStat05.clear();
		screenVars.covPremStat06.clear();
		screenVars.setCnPremStat06.clear();
		screenVars.covPremStat07.clear();
		screenVars.setCnPremStat07.clear();
		screenVars.covPremStat08.clear();
		screenVars.setCnPremStat08.clear();
		screenVars.covPremStat09.clear();
		screenVars.setCnPremStat09.clear();
		screenVars.covPremStat10.clear();
		screenVars.setCnPremStat10.clear();
		screenVars.covPremStat11.clear();
		screenVars.setCnPremStat11.clear();
		screenVars.covPremStat12.clear();
		screenVars.setCnPremStat12.clear();
	}
}
