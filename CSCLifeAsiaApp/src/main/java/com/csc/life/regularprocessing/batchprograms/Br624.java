/*
 * File: Br624.java
 * Date: 29 August 2009 22:29:17
 * Author: Quipoz Limited
 * 
 * Class transformed from BR624.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.general.recordstructures.Fileprcrec;
import com.csc.life.agents.dataaccess.MapreffTableDAM;
import com.csc.life.statistics.dataaccess.AgprTableDAM;
import com.csc.life.statistics.dataaccess.AgpxpfTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*                DEFERRED AGENT PRODUCTION UPDATE
*                --------------------------------
*
* This program updates the MAPR balances for agents if this
* processing has been deferred from Revenue Accounting and
* collections.
*
* Due to the way Agent Production works, if multi-thread processing
* is used, it is possible for the processing to abort if one of
* the threads tries to access an agent production (MAPR) record
* which is already held for update by another thread.
* To avoid this situation, it is now possible to defer the
* updates to the MAPR to allow the update records to be sorted
* and split in such a way as not to cause a clash.
*
* This program reads through the ACAX splitter file, accumulates
* the balance and on change of Key, updates the MAPRPF.
*
* Control Totals used in this program:
*
*    CT01 - Number of Agent records updated.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Br624 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private AgpxpfTableDAM agpxpf = new AgpxpfTableDAM();
	private AgpxpfTableDAM agpxpfRec = new AgpxpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR624");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaAgpxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaAgpxFn, 0, FILLER).init("AGPX");
	private FixedLengthStringData wsaaAgpxRunid = new FixedLengthStringData(2).isAPartOf(wsaaAgpxFn, 4);
	private ZonedDecimalData wsaaAgpxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaAgpxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

	private FixedLengthStringData wsaaFirstTimeIn = new FixedLengthStringData(1).init("Y");
	private Validator firstTimeIn = new Validator(wsaaFirstTimeIn, "Y");
	private ZonedDecimalData wsaaI = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaAgpxKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaAgpxAgntcoy = new FixedLengthStringData(1).isAPartOf(wsaaAgpxKey, 0);
	private FixedLengthStringData wsaaAgpxAgntnum = new FixedLengthStringData(8).isAPartOf(wsaaAgpxKey, 1);
	private PackedDecimalData wsaaAgpxEffdate = new PackedDecimalData(8, 0).isAPartOf(wsaaAgpxKey, 9);
	private PackedDecimalData wsaaAgpxAcctyr = new PackedDecimalData(4, 0).isAPartOf(wsaaAgpxKey, 14);
	private PackedDecimalData wsaaAgpxMnth = new PackedDecimalData(2, 0).isAPartOf(wsaaAgpxKey, 17);
	private FixedLengthStringData wsaaAgpxCnttype = new FixedLengthStringData(3).isAPartOf(wsaaAgpxKey, 19);
		/* FORMATS */
	private static final String mapreffrec = "MAPREFFREC";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private AgprTableDAM agprIO = new AgprTableDAM();
	private MapreffTableDAM mapreffIO = new MapreffTableDAM();
	private AgpxpfTableDAM agpxpfData = new AgpxpfTableDAM();
	private Fileprcrec fileprcrec = new Fileprcrec();
	private WsaaAccumDataInner wsaaAccumDataInner = new WsaaAccumDataInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		accumTotals3040, 
		exit3090
	}

	public Br624() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/***** No additional restart processing.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		/* Point to correct member of AGPXPF.*/
		wsaaAgpxRunid.set(bprdIO.getSystemParam04());
		wsaaAgpxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		/* Initialise working storage*/
		for (wsaaI.set(1); !(isGT(wsaaI, 10)); wsaaI.add(1)){
			wsaaAccumDataInner.wsaaMlperpp[wsaaI.toInt()].set(ZERO);
			wsaaAccumDataInner.wsaaMlperpc[wsaaI.toInt()].set(ZERO);
			wsaaAccumDataInner.wsaaMlgrppp[wsaaI.toInt()].set(ZERO);
			wsaaAccumDataInner.wsaaMlgrppc[wsaaI.toInt()].set(ZERO);
			wsaaAccumDataInner.wsaaMldirpp[wsaaI.toInt()].set(ZERO);
			wsaaAccumDataInner.wsaaMldirpc[wsaaI.toInt()].set(ZERO);
		}
		wsaaAccumDataInner.wsaaCntcount.set(ZERO);
		wsaaAccumDataInner.wsaaSumins.set(ZERO);
		/* Do the override*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(AGPXPF) TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaAgpxFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")  SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		agpxpf.openInput();
		/*EXIT*/
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		agpxpf.read(agpxpfRec);
		if (agpxpf.isAtEnd()) {
			wsaaEdterror.set(varcom.endp);
			return ;
		}
		/*EXIT*/
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					update3010();
					readMapreff3020();
					setWriteFunction3030();
				case accumTotals3040: 
					accumTotals3040();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void update3010()
	{
		/* Below, the working storage AGPX key is set. This is compared*/
		/* with the MAPREFF key to determine if there has been key chang*/
		/* Once EOF has been reached, the program needs to complete*/
		/* the 3000-UPDATE section once more to update the final MAPREFF*/
		/* record. Obviously, there is no AGPX key so in this circumstan*/
		/* the program will set this key to spaces. This will force a ch*/
		/* of key and so cause the final record to be updated.*/
		if (isEQ(wsaaEdterror, varcom.endp)) {
			wsaaAgpxKey.set(SPACES);
		}
		else {
			wsaaAgpxAgntcoy.set(agpxpfRec.agntcoy);
			wsaaAgpxAgntnum.set(agpxpfRec.agntnum);
			wsaaAgpxAcctyr.set(agpxpfRec.acctyr);
			wsaaAgpxMnth.set(agpxpfRec.mnth);
			wsaaAgpxCnttype.set(agpxpfRec.cnttype);
			wsaaAgpxEffdate.set(agpxpfRec.effdate);
		}
		/* By-pass processing if first time through.*/
		/* If there is no change in key, recalculate the current*/
		/* balance. Otherwise, update the MAPREFF record.*/
		if (firstTimeIn.isTrue()) {
			wsaaFirstTimeIn.set("N");
		}
		else {
			if (isEQ(wsaaAgpxKey, mapreffIO.getDataKey())) {
				goTo(GotoLabel.accumTotals3040);
			}
			else {
				for (wsaaI.set(1); !(isGT(wsaaI, 10)); wsaaI.add(1)){
					setPrecision(mapreffIO.getMlperpp(wsaaI), 0);
					mapreffIO.setMlperpp(wsaaI, add(mapreffIO.getMlperpp(wsaaI), wsaaAccumDataInner.wsaaMlperpp[wsaaI.toInt()]));
					setPrecision(mapreffIO.getMlperpc(wsaaI), 0);
					mapreffIO.setMlperpc(wsaaI, add(mapreffIO.getMlperpc(wsaaI), wsaaAccumDataInner.wsaaMlperpc[wsaaI.toInt()]));
					setPrecision(mapreffIO.getMlgrppp(wsaaI), 0);
					mapreffIO.setMlgrppp(wsaaI, add(mapreffIO.getMlgrppp(wsaaI), wsaaAccumDataInner.wsaaMlgrppp[wsaaI.toInt()]));
					setPrecision(mapreffIO.getMlgrppc(wsaaI), 0);
					mapreffIO.setMlgrppc(wsaaI, add(mapreffIO.getMlgrppc(wsaaI), wsaaAccumDataInner.wsaaMlgrppc[wsaaI.toInt()]));
					setPrecision(mapreffIO.getMldirpp(wsaaI), 0);
					mapreffIO.setMldirpp(wsaaI, add(mapreffIO.getMldirpp(wsaaI), wsaaAccumDataInner.wsaaMldirpp[wsaaI.toInt()]));
					setPrecision(mapreffIO.getMldirpc(wsaaI), 0);
					mapreffIO.setMldirpc(wsaaI, add(mapreffIO.getMldirpc(wsaaI), wsaaAccumDataInner.wsaaMldirpc[wsaaI.toInt()]));
				}
				setPrecision(mapreffIO.getCntcount(), 0);
				mapreffIO.setCntcount(add(mapreffIO.getCntcount(), wsaaAccumDataInner.wsaaCntcount));
				setPrecision(mapreffIO.getSumins(), 2);
				mapreffIO.setSumins(add(mapreffIO.getSumins(), wsaaAccumDataInner.wsaaSumins));
				SmartFileCode.execute(appVars, mapreffIO);
				if (isNE(mapreffIO.getStatuz(), varcom.oK)) {
					syserrrec.statuz.set(mapreffIO.getStatuz());
					syserrrec.params.set(mapreffIO.getParams());
					fatalError600();
				}
				for (wsaaI.set(1); !(isGT(wsaaI, 10)); wsaaI.add(1)){
					wsaaAccumDataInner.wsaaMlperpp[wsaaI.toInt()].set(ZERO);
					wsaaAccumDataInner.wsaaMlperpc[wsaaI.toInt()].set(ZERO);
					wsaaAccumDataInner.wsaaMlgrppp[wsaaI.toInt()].set(ZERO);
					wsaaAccumDataInner.wsaaMlgrppc[wsaaI.toInt()].set(ZERO);
					wsaaAccumDataInner.wsaaMldirpp[wsaaI.toInt()].set(ZERO);
					wsaaAccumDataInner.wsaaMldirpc[wsaaI.toInt()].set(ZERO);
				}
				wsaaAccumDataInner.wsaaCntcount.set(ZERO);
				wsaaAccumDataInner.wsaaSumins.set(ZERO);
				contotrec.totno.set(ct01);
				contotrec.totval.set(1);
				callContot001();
			}
		}
		if (isEQ(wsaaEdterror, varcom.endp)) {
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit3090);
		}
	}

protected void readMapreff3020()
	{
		/* Read MAPREFF key.*/
		mapreffIO.setDataKey(wsaaAgpxKey);
		mapreffIO.setFormat(mapreffrec);
		mapreffIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mapreffIO);
		if (isNE(mapreffIO.getStatuz(), varcom.oK)
		&& isNE(mapreffIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(mapreffIO.getStatuz());
			syserrrec.params.set(mapreffIO.getParams());
			fatalError600();
		}
	}

protected void setWriteFunction3030()
	{
		/* Determine MAPREFF function, either REWRT or WRITR.*/
		if (isEQ(mapreffIO.getStatuz(), varcom.oK)) {
			mapreffIO.setFunction(varcom.writd);
		}
		else {
			mapreffIO.setFunction(varcom.writr);
			for (wsaaI.set(1); !(isGT(wsaaI, 10)); wsaaI.add(1)){
				mapreffIO.setMlperpp(wsaaI, ZERO);
				mapreffIO.setMlperpc(wsaaI, ZERO);
				mapreffIO.setMlgrppp(wsaaI, ZERO);
				mapreffIO.setMlgrppc(wsaaI, ZERO);
				mapreffIO.setMldirpp(wsaaI, ZERO);
				mapreffIO.setMldirpc(wsaaI, ZERO);
			}
			mapreffIO.setCntcount(ZERO);
			mapreffIO.setSumins(ZERO);
		}
	}

protected void accumTotals3040()
	{
		/* Recalculate balance of incoming AGPX records.*/
		wsaaAccumDataInner.wsaaMlperpp01.add(agpxpfRec.mlperpp01);
		wsaaAccumDataInner.wsaaMlperpp02.add(agpxpfRec.mlperpp02);
		wsaaAccumDataInner.wsaaMlperpp03.add(agpxpfRec.mlperpp03);
		wsaaAccumDataInner.wsaaMlperpp04.add(agpxpfRec.mlperpp04);
		wsaaAccumDataInner.wsaaMlperpp05.add(agpxpfRec.mlperpp05);
		wsaaAccumDataInner.wsaaMlperpp06.add(agpxpfRec.mlperpp06);
		wsaaAccumDataInner.wsaaMlperpp07.add(agpxpfRec.mlperpp07);
		wsaaAccumDataInner.wsaaMlperpp08.add(agpxpfRec.mlperpp08);
		wsaaAccumDataInner.wsaaMlperpp09.add(agpxpfRec.mlperpp09);
		wsaaAccumDataInner.wsaaMlperpp10.add(agpxpfRec.mlperpp10);
		wsaaAccumDataInner.wsaaMlperpc01.add(agpxpfRec.mlperpc01);
		wsaaAccumDataInner.wsaaMlperpc02.add(agpxpfRec.mlperpc02);
		wsaaAccumDataInner.wsaaMlperpc03.add(agpxpfRec.mlperpc03);
		wsaaAccumDataInner.wsaaMlperpc04.add(agpxpfRec.mlperpc04);
		wsaaAccumDataInner.wsaaMlperpc05.add(agpxpfRec.mlperpc05);
		wsaaAccumDataInner.wsaaMlperpc06.add(agpxpfRec.mlperpc06);
		wsaaAccumDataInner.wsaaMlperpc07.add(agpxpfRec.mlperpc07);
		wsaaAccumDataInner.wsaaMlperpc08.add(agpxpfRec.mlperpc08);
		wsaaAccumDataInner.wsaaMlperpc09.add(agpxpfRec.mlperpc09);
		wsaaAccumDataInner.wsaaMlperpc10.add(agpxpfRec.mlperpc10);
		wsaaAccumDataInner.wsaaMldirpp01.add(agpxpfRec.mldirpp01);
		wsaaAccumDataInner.wsaaMldirpp02.add(agpxpfRec.mldirpp02);
		wsaaAccumDataInner.wsaaMldirpp03.add(agpxpfRec.mldirpp03);
		wsaaAccumDataInner.wsaaMldirpp04.add(agpxpfRec.mldirpp04);
		wsaaAccumDataInner.wsaaMldirpp05.add(agpxpfRec.mldirpp05);
		wsaaAccumDataInner.wsaaMldirpp06.add(agpxpfRec.mldirpp06);
		wsaaAccumDataInner.wsaaMldirpp07.add(agpxpfRec.mldirpp07);
		wsaaAccumDataInner.wsaaMldirpp08.add(agpxpfRec.mldirpp08);
		wsaaAccumDataInner.wsaaMldirpp09.add(agpxpfRec.mldirpp09);
		wsaaAccumDataInner.wsaaMldirpp10.add(agpxpfRec.mldirpp10);
		wsaaAccumDataInner.wsaaMldirpc01.add(agpxpfRec.mldirpc01);
		wsaaAccumDataInner.wsaaMldirpc02.add(agpxpfRec.mldirpc02);
		wsaaAccumDataInner.wsaaMldirpc03.add(agpxpfRec.mldirpc03);
		wsaaAccumDataInner.wsaaMldirpc04.add(agpxpfRec.mldirpc04);
		wsaaAccumDataInner.wsaaMldirpc05.add(agpxpfRec.mldirpc05);
		wsaaAccumDataInner.wsaaMldirpc06.add(agpxpfRec.mldirpc06);
		wsaaAccumDataInner.wsaaMldirpc07.add(agpxpfRec.mldirpc07);
		wsaaAccumDataInner.wsaaMldirpc08.add(agpxpfRec.mldirpc08);
		wsaaAccumDataInner.wsaaMldirpc09.add(agpxpfRec.mldirpc09);
		wsaaAccumDataInner.wsaaMldirpc10.add(agpxpfRec.mldirpc10);
		wsaaAccumDataInner.wsaaMlgrppp01.add(agpxpfRec.mlgrppp01);
		wsaaAccumDataInner.wsaaMlgrppp02.add(agpxpfRec.mlgrppp02);
		wsaaAccumDataInner.wsaaMlgrppp03.add(agpxpfRec.mlgrppp03);
		wsaaAccumDataInner.wsaaMlgrppp04.add(agpxpfRec.mlgrppp04);
		wsaaAccumDataInner.wsaaMlgrppp05.add(agpxpfRec.mlgrppp05);
		wsaaAccumDataInner.wsaaMlgrppp06.add(agpxpfRec.mlgrppp06);
		wsaaAccumDataInner.wsaaMlgrppp07.add(agpxpfRec.mlgrppp07);
		wsaaAccumDataInner.wsaaMlgrppp08.add(agpxpfRec.mlgrppp08);
		wsaaAccumDataInner.wsaaMlgrppp09.add(agpxpfRec.mlgrppp09);
		wsaaAccumDataInner.wsaaMlgrppp10.add(agpxpfRec.mlgrppp10);
		wsaaAccumDataInner.wsaaMlgrppc01.add(agpxpfRec.mlgrppc01);
		wsaaAccumDataInner.wsaaMlgrppc02.add(agpxpfRec.mlgrppc02);
		wsaaAccumDataInner.wsaaMlgrppc03.add(agpxpfRec.mlgrppc03);
		wsaaAccumDataInner.wsaaMlgrppc04.add(agpxpfRec.mlgrppc04);
		wsaaAccumDataInner.wsaaMlgrppc05.add(agpxpfRec.mlgrppc05);
		wsaaAccumDataInner.wsaaMlgrppc06.add(agpxpfRec.mlgrppc06);
		wsaaAccumDataInner.wsaaMlgrppc07.add(agpxpfRec.mlgrppc07);
		wsaaAccumDataInner.wsaaMlgrppc08.add(agpxpfRec.mlgrppc08);
		wsaaAccumDataInner.wsaaMlgrppc09.add(agpxpfRec.mlgrppc09);
		wsaaAccumDataInner.wsaaMlgrppc10.add(agpxpfRec.mlgrppc10);
		wsaaAccumDataInner.wsaaCntcount.add(agpxpfRec.cntcount);
		wsaaAccumDataInner.wsaaSumins.add(agpxpfRec.sumins);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/***** No additional commitment processing.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/***** No additional rollback processing.*/
		/*EXIT*/
	}

protected void close4000()
	{
		closeFiles4010();
	}

protected void closeFiles4010()
	{
//		/* Close AGPXPF and delete override.*/
//		agpxpf.close();
//		wsaaQcmdexc.set("DLTOVR FILE(AGPXPF)");
//		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
//		/* Clear down the AGPRPF file.*/
//		fileprcrec.function.set("CLRF");
//		fileprcrec.file1.set("AGPRPF");
//		noSource("FILEPROC", fileprcrec.params);
//		if (isNE(fileprcrec.statuz, varcom.oK)) {
//			syserrrec.statuz.set(fileprcrec.statuz);
//			syserrrec.params.set(fileprcrec.params);
//			fatalError600();
//		}
//		lsaaStatuz.set(varcom.oK);
		/*CLOSE-FILES*/
		agpxpf.close();
		wsaaQcmdexc.set("DLTOVR FILE(AGPXPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		wsaaQcmdexc.set(SPACES);
		wsaaQcmdexc.set("CLRPFM FILE(AGPRPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-ACCUM-DATA--INNER
 */
private static final class WsaaAccumDataInner { 

		/* WSAA-ACCUM-DATA */
	private FixedLengthStringData wsaaMlperpps = new FixedLengthStringData(90);
	private PackedDecimalData[] wsaaMlperpp = PDArrayPartOfStructure(10, 17, 2, wsaaMlperpps, 0);

	private FixedLengthStringData filler = new FixedLengthStringData(90).isAPartOf(wsaaMlperpps, 0, FILLER_REDEFINE);
	private PackedDecimalData wsaaMlperpp01 = new PackedDecimalData(17, 2).isAPartOf(filler, 0);
	private PackedDecimalData wsaaMlperpp02 = new PackedDecimalData(17, 2).isAPartOf(filler, 9);
	private PackedDecimalData wsaaMlperpp03 = new PackedDecimalData(17, 2).isAPartOf(filler, 18);
	private PackedDecimalData wsaaMlperpp04 = new PackedDecimalData(17, 2).isAPartOf(filler, 27);
	private PackedDecimalData wsaaMlperpp05 = new PackedDecimalData(17, 2).isAPartOf(filler, 36);
	private PackedDecimalData wsaaMlperpp06 = new PackedDecimalData(17, 2).isAPartOf(filler, 45);
	private PackedDecimalData wsaaMlperpp07 = new PackedDecimalData(17, 2).isAPartOf(filler, 54);
	private PackedDecimalData wsaaMlperpp08 = new PackedDecimalData(17, 2).isAPartOf(filler, 63);
	private PackedDecimalData wsaaMlperpp09 = new PackedDecimalData(17, 2).isAPartOf(filler, 72);
	private PackedDecimalData wsaaMlperpp10 = new PackedDecimalData(17, 2).isAPartOf(filler, 81);

	private FixedLengthStringData wsaaMlperpcs = new FixedLengthStringData(90);
	private PackedDecimalData[] wsaaMlperpc = PDArrayPartOfStructure(10, 17, 2, wsaaMlperpcs, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(90).isAPartOf(wsaaMlperpcs, 0, FILLER_REDEFINE);
	private PackedDecimalData wsaaMlperpc01 = new PackedDecimalData(17, 2).isAPartOf(filler1, 0);
	private PackedDecimalData wsaaMlperpc02 = new PackedDecimalData(17, 2).isAPartOf(filler1, 9);
	private PackedDecimalData wsaaMlperpc03 = new PackedDecimalData(17, 2).isAPartOf(filler1, 18);
	private PackedDecimalData wsaaMlperpc04 = new PackedDecimalData(17, 2).isAPartOf(filler1, 27);
	private PackedDecimalData wsaaMlperpc05 = new PackedDecimalData(17, 2).isAPartOf(filler1, 36);
	private PackedDecimalData wsaaMlperpc06 = new PackedDecimalData(17, 2).isAPartOf(filler1, 45);
	private PackedDecimalData wsaaMlperpc07 = new PackedDecimalData(17, 2).isAPartOf(filler1, 54);
	private PackedDecimalData wsaaMlperpc08 = new PackedDecimalData(17, 2).isAPartOf(filler1, 63);
	private PackedDecimalData wsaaMlperpc09 = new PackedDecimalData(17, 2).isAPartOf(filler1, 72);
	private PackedDecimalData wsaaMlperpc10 = new PackedDecimalData(17, 2).isAPartOf(filler1, 81);

	private FixedLengthStringData wsaaMlgrppps = new FixedLengthStringData(90);
	private PackedDecimalData[] wsaaMlgrppp = PDArrayPartOfStructure(10, 17, 2, wsaaMlgrppps, 0);

	private FixedLengthStringData filler2 = new FixedLengthStringData(90).isAPartOf(wsaaMlgrppps, 0, FILLER_REDEFINE);
	private PackedDecimalData wsaaMlgrppp01 = new PackedDecimalData(17, 2).isAPartOf(filler2, 0);
	private PackedDecimalData wsaaMlgrppp02 = new PackedDecimalData(17, 2).isAPartOf(filler2, 9);
	private PackedDecimalData wsaaMlgrppp03 = new PackedDecimalData(17, 2).isAPartOf(filler2, 18);
	private PackedDecimalData wsaaMlgrppp04 = new PackedDecimalData(17, 2).isAPartOf(filler2, 27);
	private PackedDecimalData wsaaMlgrppp05 = new PackedDecimalData(17, 2).isAPartOf(filler2, 36);
	private PackedDecimalData wsaaMlgrppp06 = new PackedDecimalData(17, 2).isAPartOf(filler2, 45);
	private PackedDecimalData wsaaMlgrppp07 = new PackedDecimalData(17, 2).isAPartOf(filler2, 54);
	private PackedDecimalData wsaaMlgrppp08 = new PackedDecimalData(17, 2).isAPartOf(filler2, 63);
	private PackedDecimalData wsaaMlgrppp09 = new PackedDecimalData(17, 2).isAPartOf(filler2, 72);
	private PackedDecimalData wsaaMlgrppp10 = new PackedDecimalData(17, 2).isAPartOf(filler2, 81);

	private FixedLengthStringData wsaaMlgrppcs = new FixedLengthStringData(90);
	private PackedDecimalData[] wsaaMlgrppc = PDArrayPartOfStructure(10, 17, 2, wsaaMlgrppcs, 0);

	private FixedLengthStringData filler3 = new FixedLengthStringData(90).isAPartOf(wsaaMlgrppcs, 0, FILLER_REDEFINE);
	private PackedDecimalData wsaaMlgrppc01 = new PackedDecimalData(17, 2).isAPartOf(filler3, 0);
	private PackedDecimalData wsaaMlgrppc02 = new PackedDecimalData(17, 2).isAPartOf(filler3, 9);
	private PackedDecimalData wsaaMlgrppc03 = new PackedDecimalData(17, 2).isAPartOf(filler3, 18);
	private PackedDecimalData wsaaMlgrppc04 = new PackedDecimalData(17, 2).isAPartOf(filler3, 27);
	private PackedDecimalData wsaaMlgrppc05 = new PackedDecimalData(17, 2).isAPartOf(filler3, 36);
	private PackedDecimalData wsaaMlgrppc06 = new PackedDecimalData(17, 2).isAPartOf(filler3, 45);
	private PackedDecimalData wsaaMlgrppc07 = new PackedDecimalData(17, 2).isAPartOf(filler3, 54);
	private PackedDecimalData wsaaMlgrppc08 = new PackedDecimalData(17, 2).isAPartOf(filler3, 63);
	private PackedDecimalData wsaaMlgrppc09 = new PackedDecimalData(17, 2).isAPartOf(filler3, 72);
	private PackedDecimalData wsaaMlgrppc10 = new PackedDecimalData(17, 2).isAPartOf(filler3, 81);

	private FixedLengthStringData wsaaMldirpps = new FixedLengthStringData(90);
	private PackedDecimalData[] wsaaMldirpp = PDArrayPartOfStructure(10, 17, 2, wsaaMldirpps, 0);

	private FixedLengthStringData filler4 = new FixedLengthStringData(90).isAPartOf(wsaaMldirpps, 0, FILLER_REDEFINE);
	private PackedDecimalData wsaaMldirpp01 = new PackedDecimalData(17, 2).isAPartOf(filler4, 0);
	private PackedDecimalData wsaaMldirpp02 = new PackedDecimalData(17, 2).isAPartOf(filler4, 9);
	private PackedDecimalData wsaaMldirpp03 = new PackedDecimalData(17, 2).isAPartOf(filler4, 18);
	private PackedDecimalData wsaaMldirpp04 = new PackedDecimalData(17, 2).isAPartOf(filler4, 27);
	private PackedDecimalData wsaaMldirpp05 = new PackedDecimalData(17, 2).isAPartOf(filler4, 36);
	private PackedDecimalData wsaaMldirpp06 = new PackedDecimalData(17, 2).isAPartOf(filler4, 45);
	private PackedDecimalData wsaaMldirpp07 = new PackedDecimalData(17, 2).isAPartOf(filler4, 54);
	private PackedDecimalData wsaaMldirpp08 = new PackedDecimalData(17, 2).isAPartOf(filler4, 63);
	private PackedDecimalData wsaaMldirpp09 = new PackedDecimalData(17, 2).isAPartOf(filler4, 72);
	private PackedDecimalData wsaaMldirpp10 = new PackedDecimalData(17, 2).isAPartOf(filler4, 81);

	private FixedLengthStringData wsaaMldirpcs = new FixedLengthStringData(90);
	private PackedDecimalData[] wsaaMldirpc = PDArrayPartOfStructure(10, 17, 2, wsaaMldirpcs, 0);

	private FixedLengthStringData filler5 = new FixedLengthStringData(90).isAPartOf(wsaaMldirpcs, 0, FILLER_REDEFINE);
	private PackedDecimalData wsaaMldirpc01 = new PackedDecimalData(17, 2).isAPartOf(filler5, 0);
	private PackedDecimalData wsaaMldirpc02 = new PackedDecimalData(17, 2).isAPartOf(filler5, 9);
	private PackedDecimalData wsaaMldirpc03 = new PackedDecimalData(17, 2).isAPartOf(filler5, 18);
	private PackedDecimalData wsaaMldirpc04 = new PackedDecimalData(17, 2).isAPartOf(filler5, 27);
	private PackedDecimalData wsaaMldirpc05 = new PackedDecimalData(17, 2).isAPartOf(filler5, 36);
	private PackedDecimalData wsaaMldirpc06 = new PackedDecimalData(17, 2).isAPartOf(filler5, 45);
	private PackedDecimalData wsaaMldirpc07 = new PackedDecimalData(17, 2).isAPartOf(filler5, 54);
	private PackedDecimalData wsaaMldirpc08 = new PackedDecimalData(17, 2).isAPartOf(filler5, 63);
	private PackedDecimalData wsaaMldirpc09 = new PackedDecimalData(17, 2).isAPartOf(filler5, 72);
	private PackedDecimalData wsaaMldirpc10 = new PackedDecimalData(17, 2).isAPartOf(filler5, 81);
	private PackedDecimalData wsaaCntcount = new PackedDecimalData(6, 0);
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2);
}
}
