/*
 * File: Intbonus.java
 * Date: 29 August 2009 22:57:14
 * Author: Quipoz Limited
 * 
 * Class transformed from INTBONUS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.regularprocessing.recordstructures.Bonusrec;
import com.csc.life.regularprocessing.tablestructures.T6637rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*              INTERIM BONUS
*              -------------
*
* This program is part of the Traditional Business Bonus suite.
* It is called to calculate the Interim Bonus for a specified
*  contract component.
* The Interim Bonus bonus rates are stored in Table T6637 and
*  the key to this table is Bonus Method & Premium Status ( - FP
*   ..Fully Paid, PP ..Premium Paying, SP ..Single Premium).
* This is a dated table that will be added to each year bonuses
*  are declared.
*                From Date 1991/01/01     To Date 1991/12/31
*                From Date 1990/01/01     To Date 1990/12/31
*                Etc.
*
*  The table is stored so that you can have the possibility of
*   Sum Assured based bonuses only, Bonus on Bonus or both.
*  E.g. Sum Ass Rate is 4.0 and Bonus Rate is 0
*      Calc = Component Sum Assured * 4/100 = Bonus
*      or
*      Sum Ass Rate is 4.0 and Bonus Rate is 5.0
*      Calc = Component Sum Assured * 4/100
*             + Component Bonuses * 5/100
*
*  The Sum assured and Bonus bonus values are calculated and
*   returned to the calling program. The T6637-rate-per-risk-
*   unit is obtained from T6637 by reference to the actual
*   length of time that the component has been in force.
*
*  CALCULATIONS.
*  -------------
*
*  Sum Assured bonus =
*                       Term of component from date of last
*                       Reversionary Bonus declaration up to
*                       the passed effective date (expressed
*                       in years and fractions of a year) *
*                       BONS-SUMIN *
*                       T6637-rate-per-risk-unit
*
*  Bonus on bonus    =
*                       ACBL Balance per T5645 entry *
*                       T6637-rate-per-risk-unit
*
* where T6637-rate-per-risk-unit is =
*                                   T6637-rate / T6637-risk-unit
*
* and the ACBL field used is = ACBL-SACSCURBAL
*
* NOTE.
* -----
*      The 1st entry in T5645 is used to provide part of the
*        key for the read of the ACBL (account balance) file.
*      The ACBL read is the one that holds the Reversionary
*       bonus value for the component we are processing.
*
*
*****************************************************************
* </pre>
*/
public class Intbonus extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "INTBONUS";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaUnitDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaUnitYr = new ZonedDecimalData(4, 0).isAPartOf(wsaaUnitDate, 0).setUnsigned();
	private ZonedDecimalData wsaaUnitMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaUnitDate, 4).setUnsigned();
	private ZonedDecimalData wsaaUnitDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaUnitDate, 6).setUnsigned();

	private FixedLengthStringData wsaaTdayDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaTdayYr = new ZonedDecimalData(4, 0).isAPartOf(wsaaTdayDate, 0).setUnsigned();
	private ZonedDecimalData wsaaTdayMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaTdayDate, 4).setUnsigned();
	private ZonedDecimalData wsaaTdayDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaTdayDate, 6).setUnsigned();

	private FixedLengthStringData wsaaIntDateR = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaIntYrR = new ZonedDecimalData(4, 0).isAPartOf(wsaaIntDateR, 0).setUnsigned();
	private ZonedDecimalData wsaaIntMthR = new ZonedDecimalData(2, 0).isAPartOf(wsaaIntDateR, 4).setUnsigned();
	private ZonedDecimalData wsaaIntDayR = new ZonedDecimalData(2, 0).isAPartOf(wsaaIntDateR, 6).setUnsigned();

	private FixedLengthStringData wsaaYear = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaAcctyear = new ZonedDecimalData(2, 0).isAPartOf(wsaaYear, 2).setUnsigned();
	private ZonedDecimalData wsaaSumassRate = new ZonedDecimalData(7, 2).setUnsigned();
	private ZonedDecimalData wsaaBonusRate = new ZonedDecimalData(7, 2).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaCurbal = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaBonusMethod = new FixedLengthStringData(4).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaPremStatus = new FixedLengthStringData(2).isAPartOf(wsaaItem, 4);
	private ZonedDecimalData wsaaSplit = new ZonedDecimalData(11, 5);
		/* ERRORS */
	private String g406 = "G406";
	private String g549 = "G549";
	private String g814 = "G814";
		/* FORMATS */
	private String acblrec = "ACBLREC";
	private String itemrec = "ITEMREC";
	private String itdmrec = "ITEMREC";
		/* TABLES */
	private String t5645 = "T5645";
	private String t6637 = "T6637";
		/*Subsidiary account balance*/
	private AcblTableDAM acblIO = new AcblTableDAM();
	private Bonusrec bonusrec = new Bonusrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T6637rec t6637rec = new T6637rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit390, 
		exit490, 
		exit870, 
		exit970
	}

	public Intbonus() {
		super();
	}

public void mainline(Object... parmArray)
	{
		bonusrec.bonusRec = convertAndSetParam(bonusrec.bonusRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		initialise200();
		readBonusLinkage300();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		syserrrec.subrname.set(wsaaSubr);
		bonusrec.statuz.set(varcom.oK);
		bonusrec.extBonusSa.set(ZERO);
		bonusrec.extBonusBon.set(ZERO);
		wsaaSumassRate.set(ZERO);
		wsaaBonusRate.set(ZERO);
		wsaaCurbal.set(ZERO);
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			databaseError800();
		}
	}

protected void readBonusLinkage300()
	{
		try {
			beginReading310();
		}
		catch (GOTOException e){
		}
	}

protected void beginReading310()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(bonusrec.chdrChdrcoy);
		itdmIO.setItmfrm(bonusrec.effectiveDate);
		itdmIO.setItemtabl(t6637);
		wsaaBonusMethod.set(bonusrec.bonusCalcMeth);
		wsaaPremStatus.set(bonusrec.premStatus);
		itdmIO.setItemitem(wsaaItem);
		itdmIO.setFormat(itdmrec);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError800();
		}
		if ((isNE(itdmIO.getItemitem(),wsaaItem))
		|| (isNE(itdmIO.getItemcoy(),bonusrec.chdrChdrcoy))
		|| (isNE(itdmIO.getItemtabl(),t6637))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemitem(wsaaItem);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g406);
			databaseError800();
		}
		t6637rec.t6637Rec.set(itdmIO.getGenarea());
		if (isEQ(t6637rec.riskunit01,ZERO)
		|| isEQ(t6637rec.riskunit02,ZERO)) {
			itdmIO.setItemitem(wsaaItem);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g549);
			databaseError800();
		}
		descIO.setParams(SPACES);
		descIO.setDescitem(wsaaItem);
		descIO.setDesctabl(t6637);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bonusrec.chdrChdrcoy);
		descIO.setLanguage(bonusrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			databaseError800();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			bonusrec.description.set(descIO.getShortdesc());
		}
		else {
			bonusrec.description.fill("?");
		}
		wsaaSub1.set(1);
		checkYearsInForce400();
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bonusrec.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError800();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		acblIO.setParams(SPACES);
		wsaaRldgChdrnum.set(bonusrec.chdrChdrnum);
		wsaaRldgLife.set(bonusrec.lifeLife);
		wsaaRldgCoverage.set(bonusrec.covrCoverage);
		wsaaRldgRider.set(bonusrec.covrRider);
		wsaaPlan.set(bonusrec.plnsfx);
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		acblIO.setRldgacct(wsaaRldgacct);
		acblIO.setRldgcoy(bonusrec.chdrChdrcoy);
		acblIO.setOrigcurr(bonusrec.cntcurr);
		acblIO.setSacscode(t5645rec.sacscode01);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if ((isNE(acblIO.getStatuz(),varcom.oK))
		&& (isNE(acblIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			databaseError800();
			goTo(GotoLabel.exit390);
		}
		if (isEQ(acblIO.getStatuz(),varcom.mrnf)) {
			wsaaCurbal.set(ZERO);
		}
		else {
			wsaaCurbal.set(acblIO.getSacscurbal());
		}
		bonusCalculations500();
	}

protected void checkYearsInForce400()
	{
		try {
			start410();
		}
		catch (GOTOException e){
		}
	}

protected void start410()
	{
		while ( !(isGT(wsaaSub1,10))) {
			if ((isGT(bonusrec.termInForce,t6637rec.yrsinf[wsaaSub1.toInt()]))) {
				wsaaSub1.add(1);
			}
			else {
				wsaaSumassRate.set(t6637rec.sumass[wsaaSub1.toInt()]);
				wsaaBonusRate.set(t6637rec.bonus[wsaaSub1.toInt()]);
				goTo(GotoLabel.exit490);
			}
		}
		
		/*CHECK*/
		itdmIO.setItemitem(wsaaItem);
		syserrrec.params.set(itdmIO.getParams());
		syserrrec.statuz.set(g814);
		databaseError800();
	}

protected void bonusCalculations500()
	{
		start510();
	}

protected void start510()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(bonusrec.unitStmtDate);
		datcon3rec.intDate2.set(bonusrec.effectiveDate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,"****")) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			databaseError800();
		}
		wsaaSplit.set(datcon3rec.freqFactor);
		compute(bonusrec.intBonusSa, 6).setRounded(mult((mult((div(bonusrec.sumin,t6637rec.riskunit01)),wsaaSumassRate)),wsaaSplit));
		compute(bonusrec.intBonusBon, 6).setRounded(mult((mult((div(wsaaCurbal,t6637rec.riskunit02)),wsaaBonusRate)),wsaaSplit));
	}

protected void databaseError800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start810();
				}
				case exit870: {
					exit870();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start810()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit870);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit870()
	{
		bonusrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void systemError900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start910();
				}
				case exit970: {
					exit970();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start910()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit970);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit970()
	{
		bonusrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
