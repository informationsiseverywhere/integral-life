/*
 * File: Br631.java
 * Date: 29 August 2009 22:30:12
 * Author: Quipoz Limited
 *
 * Class transformed from BR631.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.ALPHANUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;
import com.csc.fsuframework.core.FeaConfg;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.financials.procedures.Payreq;
import com.csc.fsu.financials.recordstructures.Payreqrec;
import com.csc.fsu.financials.tablestructures.Tr630rec;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.dataaccess.PtrnenqTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.enquiries.dataaccess.RtrnsacTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.FlupTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.PyoupfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Pyoupf;
import com.csc.life.terminationclaims.dataaccess.dao.impl.PyoupfDAOImpl;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.regularprocessing.reports.Rr631Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.Dbcstrcpy2;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.IntegralDBProperties;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* COMPILE-OPTIONS-SQL   CSRSQLCSR(*ENDJOB) COMMIT(*NONE) <Do Not Delete>
*
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   This is a skeleton for a batch mainline program.
*
*   The basic procedure division logic is for reading via SQL and
*     printing a simple input primary file. The overall structure
*     is as follows:
*
*   Initialise
*     - retrieve and set up standard report headings.
*
*   Read
*     - read first primary file record
*
*   Perform     Until End of File
*
*      Edit
*       - Check if the primary file record is required
*       - Softlock it if the record is to be updated
*
*      Update
*       - update database files
*       - write details to report while not primary file EOF
*       - look up referred to records for output details
*       - if new page, write headings
*       - write details
*
*      Read next primary file record
*
*    End Perform
*
*   Control totals:
*     01  -  Number of pages printed
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*   These remarks must be replaced by what the program actually
*     does.
*
***********************************************************************
* </pre>
*/
public class Br631 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected boolean sqlerrorflag;
	protected java.sql.ResultSet sqlacblCursorrs = null;
	protected java.sql.PreparedStatement sqlacblCursorps = null;
	protected java.sql.Connection sqlacblCursorconn = null;
	protected String sqlacblCursor = "";
	private Rr631Report printerFile = new Rr631Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(500);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR631");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	protected FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	protected FixedLengthStringData wsaaCompany = new FixedLengthStringData(2);
	protected FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	protected FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);
	protected FixedLengthStringData wsaaSacstype2 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstype3 = new FixedLengthStringData(2);
	protected PackedDecimalData wsaaSacscurbal = new PackedDecimalData(17, 2);
	protected FixedLengthStringData wsaaChdrnumFr = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaTr630Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr630Key, 0);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaTr630Key, 3);
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2).isAPartOf(wsaaTr630Key, 6);
	private ZonedDecimalData wsaaRtrnsacEffdate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaAmount = new ZonedDecimalData(17, 2);
	private static final String wsaaCashNotBanked = "T204";
	private static final String wsaaCashBanked = "T205";
	private static final int wsaaT5661Size = 3000;

		/* WSAA-T5661-ARRAY */
	private FixedLengthStringData[] wsaaT5661Rec = FLSInittedArray (3000, 14);
	private FixedLengthStringData[] wsaaT5661Key = FLSDArrayPartOfArrayStructure(4, wsaaT5661Rec, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT5661Lang = FLSDArrayPartOfArrayStructure(1, wsaaT5661Key, 0);
	private FixedLengthStringData[] wsaaT5661Fupcode = FLSDArrayPartOfArrayStructure(3, wsaaT5661Key, 1);
	private FixedLengthStringData[] wsaaT5661Data = FLSDArrayPartOfArrayStructure(10, wsaaT5661Rec, 4);
	private FixedLengthStringData[] wsaaT5661Fuposss = FLSDArrayPartOfArrayStructure(10, wsaaT5661Data, 0);

	private FixedLengthStringData wsaaT5661FuposssRec = new FixedLengthStringData(10);
	private FixedLengthStringData[] wsaaT5661Fuposs = FLSArrayPartOfStructure(10, 1, wsaaT5661FuposssRec, 0);
	protected ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaPendFlup = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaTrandesc = new FixedLengthStringData(30);
	protected ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(4, 0);

	private FixedLengthStringData wsaaDatimeInit = new FixedLengthStringData(26);
	private FixedLengthStringData wsaaDatimeDate = new FixedLengthStringData(10).isAPartOf(wsaaDatimeInit, 0);
	private FixedLengthStringData wsaaDatimeTime = new FixedLengthStringData(10).isAPartOf(wsaaDatimeInit, 10);
	private FixedLengthStringData wsaaUser = new FixedLengthStringData(6).isAPartOf(wsaaDatimeInit, 20);
	protected static final String wsaaAutoCheque = "1";
	private FixedLengthStringData wsaaFlgPrint = new FixedLengthStringData(1).init(SPACES);
	protected ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	protected static final String wsaaOk = "OK";
	protected static final String wsaaFailed = "Failed";
	protected static final String wsaaOwnerErr = "Policy Owner is not active";
	private ZonedDecimalData wsaaOvrprtfNumChar = new ZonedDecimalData(8, 5).init(73);
	private PackedDecimalData wsaaOvrprtfLen = new PackedDecimalData(15, 5);
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t3629 = "T3629";
	private static final String t5645 = "T5645";
	private static final String tr630 = "TR630";
	private static final String t5661 = "T5661";
	private static final String t1688 = "T1688";
		/* ERRORS */
	private static final String hl63 = "HL63";
	private static final String g641 = "G641";
	private static final String e101 = "E101";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;
	private static final int ct07 = 7;

		/* SQL-ACBLPF */
	private FixedLengthStringData sqlAcblrec = new FixedLengthStringData(35);
	private FixedLengthStringData sqlRldgcoy = new FixedLengthStringData(1).isAPartOf(sqlAcblrec, 0);
	private FixedLengthStringData sqlSacscode = new FixedLengthStringData(2).isAPartOf(sqlAcblrec, 1);

	protected FixedLengthStringData sqlRldgacct = new FixedLengthStringData(16).isAPartOf(sqlAcblrec, 3);
	protected FixedLengthStringData sqlOrigcurr = new FixedLengthStringData(3).isAPartOf(sqlAcblrec, 19);
	private FixedLengthStringData sqlSacstyp = new FixedLengthStringData(2).isAPartOf(sqlAcblrec, 22);
	private FixedLengthStringData sqlRldgpfx = new FixedLengthStringData(2).isAPartOf(sqlAcblrec, 24);
	protected PackedDecimalData sqlSacscurbal = new PackedDecimalData(17, 2).isAPartOf(sqlAcblrec, 26);

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rr631H01 = new FixedLengthStringData(83);
	private FixedLengthStringData rr631h01O = new FixedLengthStringData(83).isAPartOf(rr631H01, 0);
	private FixedLengthStringData rh01Repdate = new FixedLengthStringData(10).isAPartOf(rr631h01O, 0);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rr631h01O, 10);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rr631h01O, 20);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rr631h01O, 21);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(rr631h01O, 51);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(rr631h01O, 53);

	private FixedLengthStringData rr631H02 = new FixedLengthStringData(1);

	private FixedLengthStringData rr631F01 = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaT5661Ix = new IntegerData();
		/*Subsidiary account balance*/
	private AcblTableDAM acblIO = new AcblTableDAM();
		/*Contract header - life new business*/
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Client logical file*/
	private ClntTableDAM clntIO = new ClntTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
		/*Logical File for follow ups*/
	private FlupTableDAM flupIO = new FlupTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private PtrnenqTableDAM ptrnenqIO = new PtrnenqTableDAM();/*BRD-489*/
		/*Receipt Details - Contract Enquiry.*/
	private RtrnsacTableDAM rtrnsacIO = new RtrnsacTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	protected P6671par p6671par = new P6671par();
	protected T5645rec t5645rec = new T5645rec();
	private Tr630rec tr630rec = new Tr630rec();
	private Freqcpy freqcpy = new Freqcpy();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private T5661rec t5661rec = new T5661rec();
	protected Lifacmvrec lifacmvrec = new Lifacmvrec();
	protected Payreqrec payreqrec = new Payreqrec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Dbcstrcpy2 dbcstrcpy2 = new Dbcstrcpy2();
	protected T3629rec t3629rec = new T3629rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Getdescrec getdescrec = new Getdescrec();
	private FormatsInner formatsInner = new FormatsInner();
	protected Rr631D01Inner rr631D01Inner = new Rr631D01Inner();
	private WsaaOvrprtfRecInner wsaaOvrprtfRecInner = new WsaaOvrprtfRecInner();
	protected PyoupfDAO pyoupfDAO = getApplicationContext().getBean("pyoupfDAO",PyoupfDAO.class);
	protected List<Pyoupf> pyoupfList;
	protected Pyoupf pyoupf = null;
	/* BRD-489 STARTS */

	protected String paymentTransaction="";
	protected boolean susur002Permission = false;  
	/* BRD-489 ENDS*/

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		endOfFile2080,
		exit2090,
		validateDays2680,
		exit12690,
		exit2790
	}

	public Br631() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingCompany1020();
		setUpHeadingBranch1030();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		susur002Permission = FeaConfg.isFeatureExist("2", "SUSUR002", appVars, "IT"); 
		wsaaOvrprtfLen.set(wsaaOvrprtfNumChar);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaOvrprtfRecInner.wsaaOvrprtfRec, wsaaOvrprtfLen);
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		wsaaCompany.set(bsprIO.getCompany());
		wsaaDatimeInit.set(bsscIO.getDatimeInit());
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsaaCompany);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		readT56611200();
		prepareLifacmv1400();
		if(susur002Permission)
		{
			preparePayreqCHN();
		}
		else
		{
		preparePayreq1500();
		}
	}

protected void setUpHeadingCompany1020()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(bsprIO.getCompany());
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch1030()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		descIO.setDescitem(bprdIO.getDefaultBranch());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Branch.set(bprdIO.getDefaultBranch());
		rh01Branchnm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(bsscIO.getEffectiveDate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Repdate.set(datcon1rec.extDate);
		if(susur002Permission)
		{
		defineCursorCHN();
		}
		else
		{
		defineCursor1600();
		}
		
		/*EXIT*/
	}

protected void readT56611200()
	{
		start1200();
		nextT56611250();
	}

protected void start1200()
	{
		wsaaT5661Ix.set(1);
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5661);
		wsaaT5661Lang[wsaaT5661Ix.toInt()].set(bsscIO.getLanguage());
		wsaaT5661Fupcode[wsaaT5661Ix.toInt()].set(SPACES);
		itemIO.setItemitem(wsaaT5661Key[wsaaT5661Ix.toInt()]);
		/*    MOVE SPACES                 TO ITEM-ITEMITEM.                */
		itemIO.setFunction(varcom.begn);
	}

protected void nextT56611250()
	{
	  //performance improvement --  atiwari23
	itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	itemIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMPFX");
	SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.endp)
		|| isNE(itemIO.getItempfx(),smtpfxcpy.item)
		|| isNE(itemIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itemIO.getItemtabl(),t5661)) {
			return ;
		}
		t5661rec.t5661Rec.set(itemIO.getGenarea());
		/*    MOVE ITEM-ITEMITEM          TO WSAA-T5661-FUPCODE            */
		/*                                   (WSAA-T5661-IX).              */
		wsaaT5661Key[wsaaT5661Ix.toInt()].set(itemIO.getItemitem());
		wsaaT5661Fuposss[wsaaT5661Ix.toInt()].set(t5661rec.fuposss);
		wsaaT5661Ix.add(1);
		itemIO.setFunction(varcom.nextr);
		nextT56611250();
		return ;
	}

protected void prepareLifacmv1400()
	{
		start1400();
	}

protected void start1400()
	{
		ArrayList<Object> initializeReplaceType1 = new ArrayList<Object>();
		ArrayList<Object> initializeReplaceBy1 = new ArrayList<Object>();
		initializeReplaceType1.add(NUMERIC);
		initializeReplaceBy1.add(ZERO);
		initializeReplaceType1.add(ALPHANUMERIC);
		initializeReplaceBy1.add(SPACES);
		initialize(lifacmvrec.lifacmvRec, initializeReplaceType1, initializeReplaceBy1);
		lifacmvrec.batccoy.set(batcdorrec.company);
		lifacmvrec.batcbrn.set(batcdorrec.branch);
		lifacmvrec.batcactyr.set(batcdorrec.actyear);
		lifacmvrec.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec.batctrcde.set(batcdorrec.trcde);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.rldgcoy.set(batcdorrec.company);
		lifacmvrec.genlcoy.set(batcdorrec.company);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.origamt.set(ZERO);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.transactionDate.set(bsscIO.getEffectiveDate());
		lifacmvrec.user.set(ZERO);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t1688);
		itemIO.setItemitem(bprdIO.getAuthCode());
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			lifacmvrec.trandesc.set(SPACES);
			wsaaTrandesc.set(SPACES);
		}
		else {
			lifacmvrec.trandesc.set(getdescrec.longdesc);
			wsaaTrandesc.set(getdescrec.longdesc);
		}
	}



protected void preparePayreqCHN()
{
	
	ArrayList<Object> initializeReplaceType1 = new ArrayList<Object>();
	ArrayList<Object> initializeReplaceBy1 = new ArrayList<Object>();
	initializeReplaceType1.add(NUMERIC);
	initializeReplaceBy1.add(ZERO);
	initializeReplaceType1.add(ALPHANUMERIC);
	initializeReplaceBy1.add(SPACES);
	
	initialize(payreqrec.rec, initializeReplaceType1, initializeReplaceBy1);
	payreqrec.effdate.set(bsscIO.getEffectiveDate());
	payreqrec.sacscode.set(t5645rec.sacscode03);
	payreqrec.sacstype.set(t5645rec.sacstype03);
	
	payreqrec.glcode.set(t5645rec.glmap03);
	payreqrec.sign.set(t5645rec.sign03);
	payreqrec.cnttot.set(t5645rec.cnttot03);
	payreqrec.trandesc.set(wsaaTrandesc);
	
	payreqrec.termid.set(varcom.vrcmTermid);
	payreqrec.user.set(wsaaUser);
	payreqrec.batctrcde.set(batcdorrec.trcde);
	payreqrec.batckey.set(batcdorrec.batchkey);
	payreqrec.tranref.set(bsscIO.getJobName());
	payreqrec.language.set(bsscIO.getLanguage());
	payreqrec.function.set("REQN");
	
	
}


protected void preparePayreq1500()
	{
		start1500();
	}

protected void start1500()
	{
		ArrayList<Object> initializeReplaceType1 = new ArrayList<Object>();
		ArrayList<Object> initializeReplaceBy1 = new ArrayList<Object>();
		initializeReplaceType1.add(NUMERIC);
		initializeReplaceBy1.add(ZERO);
		initializeReplaceType1.add(ALPHANUMERIC);
		initializeReplaceBy1.add(SPACES);
		initialize(payreqrec.rec, initializeReplaceType1, initializeReplaceBy1);
		payreqrec.effdate.set(bsscIO.getEffectiveDate());
		payreqrec.sacscode.set(t5645rec.sacscode03);
		payreqrec.sacstype.set(t5645rec.sacstype03);
		payreqrec.glcode.set(t5645rec.glmap03);
		payreqrec.sign.set(t5645rec.sign03);
		payreqrec.cnttot.set(t5645rec.cnttot03);
		payreqrec.trandesc.set(wsaaTrandesc);
		payreqrec.bankkey.set(SPACES);
		payreqrec.bankacckey.set(SPACES);
		payreqrec.reqntype.set(wsaaAutoCheque);
		payreqrec.termid.set(varcom.vrcmTermid);
		payreqrec.user.set(wsaaUser);
		payreqrec.batctrcde.set(batcdorrec.trcde);
		payreqrec.batckey.set(batcdorrec.batchkey);
		payreqrec.tranref.set(bsscIO.getJobName());
		payreqrec.language.set(bsscIO.getLanguage());
		payreqrec.function.set("REQN");
		
		
	}


protected void defineCursor1600()
	{
		/*START*/
		if (isEQ(p6671par.chdrnum,SPACES)
		&& isEQ(p6671par.chdrnum1,SPACES)) {
			wsaaChdrnumFr.set("0");
			wsaaChdrnumTo.set("99999999");
		}
		else {
			wsaaChdrnumFr.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		wsaaSacscode.set(t5645rec.sacscode01);
		wsaaSacstype.set(t5645rec.sacstype01);
		wsaaSacstype2.set(t5645rec.sacstype04);
		/*  Define the query required by declaring a cursor*/
		sqlacblCursor = " SELECT  RLDGCOY, SACSCODE, RLDGACCT, ORIGCURR, SACSTYP, RLDGPFX, SACSCURBAL" +
" FROM   " + getAppVars().getTableNameOverriden("ACBLPF") + " " +
" WHERE RLDGCOY = ?" +
" AND SACSCODE = ?" +
" AND (SACSTYP = ?" +
" OR SACSTYP = ?)" +
" AND SACSCURBAL < 0" +
" AND RLDGACCT BETWEEN ? AND ?" +
" ORDER BY RLDGCOY, RLDGACCT";
		/*   Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlacblCursorconn = getAppVars().getDBConnectionForTable(new com.csc.fsu.accounting.dataaccess.AcblpfTableDAM());
			sqlacblCursorps = getAppVars().prepareStatementEmbeded(sqlacblCursorconn, sqlacblCursor, "ACBLPF");
			//MIBT-35 START
			//ILIFE-1820 starts
			/* START OF ILIFE-6645 Oracle and MSSQL parameters are separated by checking databaseType. */
			if(IntegralDBProperties.getType().equals("0")) {//IJTI-449
				sqlacblCursorps.setString(1, wsaaCompany.toString().trim());
				sqlacblCursorps.setString(2, wsaaSacscode.toString().trim());
				sqlacblCursorps.setString(3, wsaaSacstype.toString());
				sqlacblCursorps.setString(4, wsaaSacstype2.toString().trim());
				getAppVars().setDBInt(sqlacblCursorps, 5, wsaaChdrnumFr.toInt());
				getAppVars().setDBInt(sqlacblCursorps, 6, wsaaChdrnumTo.toInt());
			}
			else{
				getAppVars().setDBString(sqlacblCursorps, 1, wsaaCompany);
				getAppVars().setDBString(sqlacblCursorps, 2, wsaaSacscode);
				getAppVars().setDBString(sqlacblCursorps, 3, wsaaSacstype);
				getAppVars().setDBString(sqlacblCursorps, 4, wsaaSacstype2);
				getAppVars().setDBString(sqlacblCursorps, 5, wsaaChdrnumFr);
				getAppVars().setDBString(sqlacblCursorps, 6, wsaaChdrnumTo);
		}
		/* END OF ILIFE-6645*/
		//ILIFE-1820 ENDS
		//MIBT-35 END
		sqlacblCursorrs = getAppVars().executeQuery(sqlacblCursorps);
	}
	catch (SQLException ex){
		sqlerrorflag = true;
		getAppVars().setSqlErrorCode(ex);
	}
	/*EXIT*/
}
protected void defineCursorCHN()
	{
	/*START*/
	if (isEQ(p6671par.chdrnum,SPACES)
	&& isEQ(p6671par.chdrnum1,SPACES)) {
		wsaaChdrnumFr.set("0");
		wsaaChdrnumTo.set("99999999");
	}
	else {
		wsaaChdrnumFr.set(p6671par.chdrnum);
		wsaaChdrnumTo.set(p6671par.chdrnum1);
	}
	wsaaSacscode.set(t5645rec.sacscode01);
	wsaaSacstype.set(t5645rec.sacstype02);
	wsaaSacstype2.set(t5645rec.sacstype04);
	/*  Define the query required by declaring a cursor*/
	sqlacblCursor = " SELECT  RLDGCOY, SACSCODE, RLDGACCT, ORIGCURR, SACSTYP, RLDGPFX, SACSCURBAL" +
" FROM   " + getAppVars().getTableNameOverriden("ACBLPF") + " " +
" WHERE RLDGCOY = ?" +
" AND SACSCODE = ?" +
" AND (SACSTYP = ?" +
" OR SACSTYP = ?)" +
" AND SACSCURBAL < 0" +
" AND RLDGACCT BETWEEN ? AND ?" +
" ORDER BY RLDGCOY, RLDGACCT";
	/*   Open the cursor (this runs the query)*/
	sqlerrorflag = false;
	try {
		sqlacblCursorconn = getAppVars().getDBConnectionForTable(new com.csc.fsu.accounting.dataaccess.AcblpfTableDAM());
		sqlacblCursorps = getAppVars().prepareStatementEmbeded(sqlacblCursorconn, sqlacblCursor, "ACBLPF");
		//MIBT-35 START
		//ILIFE-1820 starts
		/* START OF ILIFE-6645 Oracle and MSSQL parameters are separated by checking databaseType. */
		if(IntegralDBProperties.getType().equals("0")) {//IJTI-449
			sqlacblCursorps.setString(1, wsaaCompany.toString().trim());
			sqlacblCursorps.setString(2, wsaaSacscode.toString().trim());
			sqlacblCursorps.setString(3, wsaaSacstype.toString());
			sqlacblCursorps.setString(4, wsaaSacstype2.toString().trim());
			getAppVars().setDBInt(sqlacblCursorps, 5, wsaaChdrnumFr.toInt());
			getAppVars().setDBInt(sqlacblCursorps, 6, wsaaChdrnumTo.toInt());
		}
		else{
			getAppVars().setDBString(sqlacblCursorps, 1, wsaaCompany);
			getAppVars().setDBString(sqlacblCursorps, 2, wsaaSacscode);
			getAppVars().setDBString(sqlacblCursorps, 3, wsaaSacstype);
			getAppVars().setDBString(sqlacblCursorps, 4, wsaaSacstype2);
			getAppVars().setDBString(sqlacblCursorps, 5, wsaaChdrnumFr);
			getAppVars().setDBString(sqlacblCursorps, 6, wsaaChdrnumTo);
	}
	/* END OF ILIFE-6645*/
	//ILIFE-1820 ENDS
	//MIBT-35 END
	sqlacblCursorrs = getAppVars().executeQuery(sqlacblCursorps);
}
catch (SQLException ex){
	sqlerrorflag = true;
	getAppVars().setSqlErrorCode(ex);
}
/*EXIT*/
}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readFile2010();
				case endOfFile2080:
					endOfFile2080();
				case exit2090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		wsspEdterror.set(varcom.oK);
		/*   Fetch record*/
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlacblCursorrs)) {
				getAppVars().getDBObject(sqlacblCursorrs, 1, sqlRldgcoy);
				getAppVars().getDBObject(sqlacblCursorrs, 2, sqlSacscode);
				getAppVars().getDBObject(sqlacblCursorrs, 3, sqlRldgacct);
				getAppVars().getDBObject(sqlacblCursorrs, 4, sqlOrigcurr);
				getAppVars().getDBObject(sqlacblCursorrs, 5, sqlSacstyp);
				getAppVars().getDBObject(sqlacblCursorrs, 6, sqlRldgpfx);
				getAppVars().getDBObject(sqlacblCursorrs, 7, sqlSacscurbal);
			}
			else {
				goTo(GotoLabel.endOfFile2080);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		goTo(GotoLabel.exit2090);
	}

protected void endOfFile2080()
	{
		if (isEQ(wsaaFlgPrint,"Y")) {
			printerFile.printRr631f01(rr631F01, indicArea);
		}
		wsspEdterror.set(varcom.endp);
		if (isEQ(wsaaEof,"Y")) {
			wsspEdterror.set(varcom.endp);
		}
	}

protected void edit2500()
	{
		edit2510();
	}

protected void edit2510()
	{
		wsspEdterror.set(varcom.oK);
		readChdrlnb2550();
		readTr6302600();
		if (isEQ(wsspEdterror,varcom.oK)) {
			checkLatestSuspDate2650();
		}
		if (isEQ(wsspEdterror,varcom.oK)) {
			checkPendingFlup2700();
		}
		if (isEQ(wsspEdterror,varcom.oK)) {
			validPolicyOwner2800();
		}
		if (isEQ(wsspEdterror,varcom.oK)) {
			lockContract2900();
		}
		/* BRD-489 STARTS */
				if(isEQ(wsspEdterror,varcom.oK)){
					readPtrn29500();
				}
			/* BRD-489 ENDS*/

	}

protected void readChdrlnb2550()
	{
		/*START*/
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(wsaaCompany);
		chdrlnbIO.setChdrnum(sqlRldgacct);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readTr6302600()
	{
		start2600();
		nextTr6302610();
	}

protected void start2600()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsaaCompany);
		itemIO.setItemtabl(tr630);
		wsaaCnttype.set(chdrlnbIO.getCnttype());
		wsaaCntcurr.set(chdrlnbIO.getCntcurr());
		wsaaStatcode.set(chdrlnbIO.getStatcode());
	}

protected void nextTr6302610()
	{
		itemIO.setItemitem(wsaaTr630Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			if (isNE(wsaaCnttype,"***")) {
				wsaaCnttype.set("***");
				nextTr6302610();
				return ;
			}
			else {
				contotrec.totno.set(ct02);
				contotrec.totval.set(1);
				callContot001();
				wsspEdterror.set(SPACES);
				return ;
			}
		}
		tr630rec.tr630Rec.set(itemIO.getGenarea());
		compute(wsaaSacscurbal, 2).set(mult(sqlSacscurbal,(-1)));
		if (isLT(wsaaSacscurbal,tr630rec.cmin)
		|| isGT(wsaaSacscurbal,tr630rec.cmax)) {
			contotrec.totno.set(ct03);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
		}
	}

protected void checkLatestSuspDate2650()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start2650();
					nextRtrnsac2660();
				case validateDays2680:
					validateDays2680();
				case exit12690:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2650()
	{
		if (isEQ(tr630rec.tdayno,NUMERIC)
		&& isGT(tr630rec.tdayno,ZERO)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.exit12690);
		}
		/* Get latest premium suspense date.*/
		wsaaRtrnsacEffdate.set(varcom.mindate);
		rtrnsacIO.setRldgcoy(sqlRldgcoy);
		rtrnsacIO.setSacscode(t5645rec.sacscode01);
		rtrnsacIO.setSacstyp(t5645rec.sacstype01);
		rtrnsacIO.setRldgacct(sqlRldgacct);
		rtrnsacIO.setOrigccy(SPACES);
		rtrnsacIO.setFormat(formatsInner.rtrnsacrec);
		rtrnsacIO.setFunction(varcom.begn);
	}

protected void nextRtrnsac2660()
	{
	  //performance improvement --  atiwari23
	rtrnsacIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	rtrnsacIO.setFitKeysSearch("RLDGCOY","SACSCODE","SACSTYP","RLDGACCT");
		SmartFileCode.execute(appVars, rtrnsacIO);
		if (isNE(rtrnsacIO.getStatuz(),varcom.oK)
		&& isNE(rtrnsacIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(rtrnsacIO.getParams());
			syserrrec.statuz.set(rtrnsacIO.getStatuz());
			fatalError600();
		}
		if (isEQ(rtrnsacIO.getStatuz(),varcom.endp)
		|| isNE(rtrnsacIO.getRldgcoy(),sqlRldgcoy)
		|| isNE(rtrnsacIO.getSacscode(),t5645rec.sacscode01)
		|| isNE(rtrnsacIO.getSacstyp(),t5645rec.sacstype01)
		|| isNE(rtrnsacIO.getRldgacct(),sqlRldgacct)) {
			goTo(GotoLabel.validateDays2680);
		}
		if (isEQ(rtrnsacIO.getGlsign(),"-")) {
			compute(wsaaAmount, 2).set(mult(rtrnsacIO.getOrigamt(),(-1)));
		}
		else {
			wsaaAmount.set(rtrnsacIO.getOrigamt());
		}
		if ((isEQ(rtrnsacIO.getBatctrcde(),wsaaCashNotBanked)
		|| isEQ(rtrnsacIO.getBatctrcde(),wsaaCashBanked))
		&& isLT(wsaaAmount,ZERO)
		&& isGT(rtrnsacIO.getEffdate(),wsaaRtrnsacEffdate)) {
			wsaaRtrnsacEffdate.set(rtrnsacIO.getEffdate());
		}
		rtrnsacIO.setFunction(varcom.nextr);
		nextRtrnsac2660();
		return ;
	}

	/**
	* <pre>
	* Calculate days from latest premium suspense date from
	* BSSC-EFFECTIVE-DATE.
	* </pre>
	*/
protected void validateDays2680()
	{
		if (isEQ(wsaaRtrnsacEffdate,varcom.mindate)) {
			contotrec.totno.set(ct04);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			return ;
		}
		datcon3rec.intDate1.set(wsaaRtrnsacEffdate);
		datcon3rec.intDate2.set(bsscIO.getEffectiveDate());
		datcon3rec.freqFactor.set(1);
		datcon3rec.frequency.set(freqcpy.daily);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		if (isLT(datcon3rec.freqFactor,tr630rec.tdayno)) {
			contotrec.totno.set(ct04);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
		}
	}

protected void checkPendingFlup2700()
	{
		try {
			start2700();
			nextFlup2750();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start2700()
	{
		if (isEQ(tr630rec.activeInd,"Y")) {
			goTo(GotoLabel.exit2790);
		}
		flupIO.setParams(SPACES);
		flupIO.setChdrcoy(bsprIO.getCompany());
		flupIO.setChdrnum(sqlRldgacct);
		flupIO.setFupno(ZERO);
		flupIO.setFormat(formatsInner.fluprec);
		flupIO.setFunction(varcom.begn);
	}

protected void nextFlup2750()
	{  //performance improvement --  atiwari23
	flupIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	flupIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, flupIO);
		if (isNE(flupIO.getStatuz(),varcom.oK)
		&& isNE(flupIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(flupIO.getStatuz());
			syserrrec.params.set(flupIO.getParams());
			fatalError600();
		}
		if (isEQ(flupIO.getStatuz(),varcom.endp)
		|| isNE(flupIO.getChdrcoy(),bsprIO.getCompany())
		|| isNE(flupIO.getChdrnum(),sqlRldgacct)) {
			return ;
		}
		for (wsaaT5661Ix.set(1); !(isGT(wsaaT5661Ix,wsaaT5661Size)
		|| (isEQ(bsscIO.getLanguage(),wsaaT5661Lang[wsaaT5661Ix.toInt()])
		&& isEQ(flupIO.getFupcode(),wsaaT5661Fupcode[wsaaT5661Ix.toInt()]))); wsaaT5661Ix.add(1)){
			wsaaT5661FuposssRec.set(wsaaT5661Fuposss[wsaaT5661Ix.toInt()]);
		}
		if (isGT(wsaaT5661Ix,wsaaT5661Size)) {
			wsspEdterror.set(SPACES);
			contotrec.totno.set(ct05);
			contotrec.totval.set(1);
			callContot001();
			return ;
		}
		wsaaPendFlup.set(SPACES);
		for (wsaaSub.set(1); !(isGT(wsaaSub,10)); wsaaSub.add(1)){
			if (isEQ(flupIO.getFupstat(),wsaaT5661Fuposs[wsaaSub.toInt()])) {
				wsaaSub.set(30);
				wsaaPendFlup.set("N");
			}
		}
		/*    IF WSAA-PEND-FLUP       NOT = SPACE                  <LA4479>*/
		if (isEQ(wsaaPendFlup, SPACES)) {
			contotrec.totno.set(ct06);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			return ;
		}
		flupIO.setFunction(varcom.nextr);
		nextFlup2750();
		return ;
	}

protected void validPolicyOwner2800()
	{
		start2800();
	}

protected void start2800()
	{
		clntIO.setParams(SPACES);
		clntIO.setClntpfx(chdrlnbIO.getCownpfx());
		clntIO.setClntcoy(chdrlnbIO.getCowncoy());
		clntIO.setClntnum(chdrlnbIO.getCownnum());
		clntIO.setFormat(formatsInner.clntrec);
		clntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(clntIO.getStatuz());
			syserrrec.params.set(clntIO.getParams());
			fatalError600();
		}
		if (isNE(clntIO.getCltstat(),"AC")
		&& isNE(clntIO.getCltdod(),varcom.vrcmMaxDate)) {
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			printReport3350();
		}
	}

protected void lockContract2900()
	{
		start2900();
	}

protected void start2900()
	{
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(chdrlnbIO.getChdrcoy());
		sftlockrec.enttyp.set(chdrlnbIO.getChdrpfx());
		sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			conlogrec.error.set(e101);
			conlogrec.params.set(sftlockrec.sftlockRec);
			callConlog003();
			wsspEdterror.set(SPACES);
			/*CONTINUE_STMT*/
		}
	}
protected void readPtrn29500()
{
	start2950();
}

protected void start2950()
{
	ptrnenqIO.setParams(SPACES);
	ptrnenqIO.setChdrcoy(chdrlnbIO.getChdrcoy());
	ptrnenqIO.setChdrnum(chdrlnbIO.getChdrnum());
	ptrnenqIO.setTranno(chdrlnbIO.getTranno());
	ptrnenqIO.setFormat(formatsInner.ptrnenqrec);
	ptrnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	ptrnenqIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");
	ptrnenqIO.setFunction(varcom.begn);
	SmartFileCode.execute(appVars, ptrnenqIO);
	if (isNE(ptrnenqIO.getStatuz(),varcom.oK)) {
		syserrrec.statuz.set(ptrnenqIO.getStatuz());
		syserrrec.params.set(ptrnenqIO.getParams());
		fatalError600();
	}
	else{
		paymentTransaction=ptrnenqIO.getBatctrcde().toString();
	}
}


protected void update3000()
	{
		/*UPDATE*/
		writeContract3100();
		writePtrn3150();
		callLifacmv3200();
		if(susur002Permission)
		{
			callPayreqCHN();
		}
		else
		{
		callPayreq3250();
		}
		releaseSftlock3330();
		printReport3350();
		/*EXIT*/
	}

protected void writeContract3100()
	{
		start3100();
	}

protected void start3100()
	{
		chdrlnbIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		setPrecision(chdrlnbIO.getTranno(), 0);
		chdrlnbIO.setTranno(add(chdrlnbIO.getTranno(),1));
		chdrlnbIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}

protected void writePtrn3150()
	{
		start3150();
	}

protected void start3150()
	{
		ptrnIO.setParams(SPACES);
		ptrnIO.setBatcpfx(batcdorrec.prefix);
		ptrnIO.setBatccoy(batcdorrec.company);
		ptrnIO.setBatcbrn(batcdorrec.branch);
		ptrnIO.setBatcactyr(batcdorrec.actyear);
		ptrnIO.setBatcactmn(batcdorrec.actmonth);
		ptrnIO.setBatctrcde(batcdorrec.trcde);
		ptrnIO.setBatcbatch(batcdorrec.batch);
		ptrnIO.setChdrpfx(chdrlnbIO.getChdrpfx());
		ptrnIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlnbIO.getChdrnum());
		ptrnIO.setTranno(chdrlnbIO.getTranno());
		ptrnIO.setValidflag("1");
		ptrnIO.setTransactionDate(getCobolDate());
		ptrnIO.setTransactionTime(getCobolTime());
		ptrnIO.setPtrneff(bsscIO.getEffectiveDate());
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(subString(bsscIO.getDatimeInit(), 21, 6));
		ptrnIO.setDatesub(bsscIO.getEffectiveDate());
		ptrnIO.setCrtuser(bsscIO.getUserName());
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ptrnIO.getStatuz());
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void callLifacmv3200()
	{
		/*START*/
		wsaaJrnseq.set(1);
		wsaaSub.set(1);
		lifacmvrec.origamt.set(wsaaSacscurbal);
		a1000CallLifacmv();
		wsaaSub.set(2);
		lifacmvrec.origamt.set(wsaaSacscurbal);
		a1000CallLifacmv();
		/*EXIT*/
	}

protected void callPayreqCHN()
{
	pyoupfList=pyoupfDAO.getAllItemitem(chdrlnbIO.getChdrnum().toString()); 
	
	 if (pyoupfList.size() >= 1 )
	{
		 for(Pyoupf pyoupf : pyoupfList)
			{
			 if(isEQ(pyoupf.getPaymentflag(),SPACE)){
					
					if(isEQ(pyoupf.getBankkey(),SPACES) && isEQ(pyoupf.getBankacckey(),SPACES))
				{
					payreqrec.bankkey.set(SPACES);
					payreqrec.bankacckey.set(SPACES);
					payreqrec.reqntype.set(wsaaAutoCheque);
				}
				else
				{
					payreqrec.bankkey.set(pyoupf.getBankkey());
					payreqrec.bankacckey.set(pyoupf.getBankacckey());
					payreqrec.reqntype.set(pyoupf.getReqntype());
				}
					payreqrec.paycurr.set(sqlOrigcurr);
					/*    MOVE SQL-SACSCURBAL         TO PAYREQ-PYMT.                  */
					payreqrec.pymt.set(wsaaSacscurbal);
					payreqrec.frmRldgacct.set(sqlRldgacct);
					payreqrec.trandesc.set(descIO.getLongdesc());
					payreqrec.clntcoy.set(chdrlnbIO.getCowncoy());
					payreqrec.clntnum.set(pyoupf.getPayrnum());
					readT36293260();
					payreqrec.bankcode.set(t3629rec.bankcode);
					payreqrec.tranref.set(sqlRldgacct);
					payreqrec.tranno.set(chdrlnbIO.getTranno());
					/* BRD-489 STARTS */
							payreqrec.zbatctrcde.set(paymentTransaction);
					/* BRD-489 ENDS*/
					payreqrec.function.set("REQN");
					callProgram(Payreq.class, payreqrec.rec);
					if (isNE(payreqrec.statuz,varcom.oK)) {
						syserrrec.statuz.set(payreqrec.statuz);
						syserrrec.params.set(payreqrec.rec);
						fatalError600();
						
					}
					
					pyoupf.setPaymentflag("Y");
					pyoupf.setReqnno(payreqrec.reqnno.toString());
					
					try{
						pyoupfDAO.updatePayflg(pyoupf);
			    	}catch(Exception e){
			    		//syserrrec.params.set(covtlnbIO.getParams());	
			    		e.printStackTrace();
			    		fatalError600();
			    	}
				}
			}
	}
	
	
	else
	{   
		payreqrec.bankkey.set(SPACES);
		payreqrec.bankacckey.set(SPACES);
		payreqrec.reqntype.set(wsaaAutoCheque);
		payreqrec.paycurr.set(sqlOrigcurr);
		/*    MOVE SQL-SACSCURBAL         TO PAYREQ-PYMT.                  */
		payreqrec.pymt.set(wsaaSacscurbal);
		payreqrec.frmRldgacct.set(sqlRldgacct);
		payreqrec.trandesc.set(descIO.getLongdesc());
		payreqrec.clntcoy.set(chdrlnbIO.getCowncoy());
		payreqrec.clntnum.set(chdrlnbIO.getCownnum());
		readT36293260();
		payreqrec.bankcode.set(t3629rec.bankcode);
		payreqrec.tranref.set(sqlRldgacct);
		payreqrec.tranno.set(chdrlnbIO.getTranno());
		/* BRD-489 STARTS */
				payreqrec.zbatctrcde.set(paymentTransaction);
		/* BRD-489 ENDS*/
		payreqrec.function.set("REQN");
		callProgram(Payreq.class, payreqrec.rec);
		if (isNE(payreqrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(payreqrec.statuz);
			syserrrec.params.set(payreqrec.rec);
			fatalError600();
		}
	}
	
}

protected void callPayreq3250()
	{
		start3250();
	}

protected void start3250()
	{
		payreqrec.paycurr.set(sqlOrigcurr);
		/*    MOVE SQL-SACSCURBAL         TO PAYREQ-PYMT.                  */
		payreqrec.pymt.set(wsaaSacscurbal);
		payreqrec.frmRldgacct.set(sqlRldgacct);
		payreqrec.trandesc.set(descIO.getLongdesc());
		payreqrec.clntcoy.set(chdrlnbIO.getCowncoy());
		payreqrec.clntnum.set(chdrlnbIO.getCownnum());
		readT36293260();
		payreqrec.bankcode.set(t3629rec.bankcode);
		payreqrec.tranref.set(sqlRldgacct);
		payreqrec.tranno.set(chdrlnbIO.getTranno());
		/* BRD-489 STARTS */
				payreqrec.zbatctrcde.set(paymentTransaction);
		/* BRD-489 ENDS*/
		payreqrec.function.set("REQN");
		callProgram(Payreq.class, payreqrec.rec);
		if (isNE(payreqrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(payreqrec.statuz);
			syserrrec.params.set(payreqrec.rec);
			fatalError600();
		}
		
	}

protected void readT36293260()
	{
		start3260();
	}

protected void start3260()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(sqlOrigcurr);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			itemIO.setStatuz(g641);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		else {
			t3629rec.t3629Rec.set(itemIO.getGenarea());
			if (isEQ(t3629rec.bankcode,SPACES)) {
				itemIO.setStatuz(hl63);
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
	}

protected void releaseSftlock3330()
	{
		start3330();
	}

protected void start3330()
	{
		sftlockrec.function.set("UNLK");
		sftlockrec.company.set(chdrlnbIO.getChdrcoy());
		sftlockrec.enttyp.set(chdrlnbIO.getChdrpfx());
		sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
	}

protected void printReport3350()
	{
		start3350();
	}

protected void start3350()
	{
		if (newPageReq.isTrue()) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			printerFile.printRr631h01(rr631H01, indicArea);
			wsaaOverflow.set("N");
			printerFile.printRr631h02(rr631H02, indicArea);
			wsaaOverflow.set("N");
		}
		moveDetail3360();
		/*  Write detail, checking for page overflow*/
		printerFile.printRr631d01(rr631D01Inner.rr631D01, indicArea);
		wsaaFlgPrint.set("Y");
	}

protected void moveDetail3360()
	{
		start3360();
	}

protected void start3360()
	{
		initialize(rr631D01Inner.rr631D01);
		wsaaCount.add(1);
		rr631D01Inner.rd01Zseqno.set(wsaaCount);
		rr631D01Inner.rd01Chdrnum.set(chdrlnbIO.getChdrnum());
		formatPayeeName3370();
		rr631D01Inner.rd01Cmin.set(sqlSacscurbal);
		rr631D01Inner.rd01Currcd.set(sqlOrigcurr);
		if (isEQ(wsspEdterror,varcom.oK)) {
			rr631D01Inner.rd01Tenline.set(wsaaOk);
			rr631D01Inner.rd01Bankcode.set(payreqrec.bankcode);
			rr631D01Inner.rd01Reqntype.set(payreqrec.reqntype);
			rr631D01Inner.rd01Reqnno.set(payreqrec.reqnno);
		}
		else {
			rr631D01Inner.rd01Tenline.set(wsaaFailed);
			rr631D01Inner.rd01Bankcode.set(SPACES);
			rr631D01Inner.rd01Reqntype.set(SPACES);
			rr631D01Inner.rd01Reqnno.set(SPACES);
			rr631D01Inner.rd01Desi.set(wsaaOwnerErr);
		}
	}

protected void formatPayeeName3370()
	{
		start3370();
	}

protected void start3370()
	{
		initialize(namadrsrec.namadrsRec);
		namadrsrec.clntNumber.set(chdrlnbIO.getCownnum());
		namadrsrec.clntPrefix.set(chdrlnbIO.getCownpfx());
		namadrsrec.clntCompany.set(bsprIO.getFsuco());
		namadrsrec.language.set(bsscIO.getLanguage());
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)
		&& isNE(namadrsrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		if (isEQ(namadrsrec.statuz,varcom.mrnf)) {
			rr631D01Inner.rd01Longname.fill("?");
		}
		else {
			dbcstrcpy2.dbcsInputString.set(namadrsrec.name);
			dbcstrcpy2.dbcsOutputLength.set(30);
			a2000CallDbcstrnc();
			rr631D01Inner.rd01Longname.set(dbcstrcpy2.dbcsOutputString);
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*   Close the cursor*/
		getAppVars().freeDBConnectionIgnoreErr(sqlacblCursorconn, sqlacblCursorps, sqlacblCursorrs);
		/*  Close any open files.*/
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void a1000CallLifacmv()
	{
			a1000Start();
		}

protected void a1000Start()
	{
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			return ;
		}
		lifacmvrec.function.set("PSTW");
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.origcurr.set(sqlOrigcurr);
		lifacmvrec.rldgacct.set(sqlRldgacct);
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSub.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSub.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaSub.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaSub.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaSub.toInt()]);
		lifacmvrec.rdocnum.set(chdrlnbIO.getChdrnum());
		lifacmvrec.tranno.set(chdrlnbIO.getTranno());
		lifacmvrec.tranref.set(chdrlnbIO.getTranno());
		lifacmvrec.effdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
		lifacmvrec.substituteCode[2].set(SPACES);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError600();
		}
		wsaaJrnseq.add(1);
	}

protected void a2000CallDbcstrnc()
	{
		/*A2000-START*/
		dbcstrcpy2.dbcsStatuz.set(SPACES);
		dbcsTrnc2(dbcstrcpy2.rec);
		if (isNE(dbcstrcpy2.dbcsStatuz,varcom.oK)) {
			dbcstrcpy2.dbcsOutputString.set(SPACES);
		}
		/*A2090-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-OVRPRTF-REC--INNER
 */
private static final class WsaaOvrprtfRecInner {

	private FixedLengthStringData wsaaOvrprtfRec = new FixedLengthStringData(73);
	private FixedLengthStringData filler = new FixedLengthStringData(12).isAPartOf(wsaaOvrprtfRec, 0, FILLER).init("OVRPRTF FILE");
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaOvrprtfRec, 12, FILLER).init("(");
	private FixedLengthStringData wsaaOvrpPrtf = new FixedLengthStringData(8).isAPartOf(wsaaOvrprtfRec, 13).init("RR631");
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaOvrprtfRec, 21, FILLER).init(")");
	private FixedLengthStringData filler3 = new FixedLengthStringData(10).isAPartOf(wsaaOvrprtfRec, 22, FILLER).init(" PAGESIZE(");
	private ZonedDecimalData wsaaOvrpPagelength = new ZonedDecimalData(2, 0).isAPartOf(wsaaOvrprtfRec, 32).init(60).setUnsigned();
	private FixedLengthStringData filler4 = new FixedLengthStringData(1).isAPartOf(wsaaOvrprtfRec, 34, FILLER).init(" ");
	private ZonedDecimalData wsaaOvrpPagewidth = new ZonedDecimalData(3, 0).isAPartOf(wsaaOvrprtfRec, 35).init(198).setUnsigned();
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaOvrprtfRec, 38, FILLER).init(" *ROWCOL)");
	private FixedLengthStringData filler6 = new FixedLengthStringData(7).isAPartOf(wsaaOvrprtfRec, 47, FILLER).init(" LPI(6)");
	private FixedLengthStringData filler7 = new FixedLengthStringData(8).isAPartOf(wsaaOvrprtfRec, 54, FILLER).init(" CPI(15)");
	private FixedLengthStringData filler8 = new FixedLengthStringData(11).isAPartOf(wsaaOvrprtfRec, 62, FILLER).init(" OVRFLW(60)");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData rtrnsacrec = new FixedLengthStringData(10).init("RTRNREC");
	private FixedLengthStringData fluprec = new FixedLengthStringData(10).init("FLUPREC");
	private FixedLengthStringData clntrec = new FixedLengthStringData(10).init("CLNTREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	/* BRD-489 STARTS */
		private FixedLengthStringData ptrnenqrec = new FixedLengthStringData(10).init("PTRNENQREC");
	/* BRD-489 ENDS*/

}
/*
 * Class transformed  from Data Structure RR631-D01--INNER
 */
public static final class Rr631D01Inner {

	public FixedLengthStringData rr631D01 = new FixedLengthStringData(134);
	private FixedLengthStringData rr631d01O = new FixedLengthStringData(134).isAPartOf(rr631D01, 0);
	public ZonedDecimalData rd01Zseqno = new ZonedDecimalData(4, 0).isAPartOf(rr631d01O, 0);
	public FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(rr631d01O, 4);
	private FixedLengthStringData rd01Longname = new FixedLengthStringData(50).isAPartOf(rr631d01O, 12);
	public FixedLengthStringData rd01Tenline = new FixedLengthStringData(10).isAPartOf(rr631d01O, 62);
	public ZonedDecimalData rd01Cmin = new ZonedDecimalData(17, 2).isAPartOf(rr631d01O, 72);
	public FixedLengthStringData rd01Currcd = new FixedLengthStringData(3).isAPartOf(rr631d01O, 89);
	public FixedLengthStringData rd01Bankcode = new FixedLengthStringData(2).isAPartOf(rr631d01O, 92);
	public FixedLengthStringData rd01Reqntype = new FixedLengthStringData(1).isAPartOf(rr631d01O, 94);
	public FixedLengthStringData rd01Reqnno = new FixedLengthStringData(9).isAPartOf(rr631d01O, 95);
	public FixedLengthStringData rd01Desi = new FixedLengthStringData(30).isAPartOf(rr631d01O, 104);
}
}
