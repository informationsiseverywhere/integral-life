package com.csc.life.regularprocessing.dataaccess.model;

import com.quipoz.framework.datatype.FixedLengthStringData;

/*
 * Class transformed  from Data Structure T5679-T5679-REC--INNER
 */
public class T5679T5679Rec {
    private FixedLengthStringData t5679T5679Rec = new FixedLengthStringData(500);
    private FixedLengthStringData t5679CnPremStats = new FixedLengthStringData(24).isAPartOf(t5679T5679Rec, 0);
    private FixedLengthStringData t5679CnPremStat01 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 0);
    private FixedLengthStringData t5679CnPremStat02 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 2);
    private FixedLengthStringData t5679CnPremStat03 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 4);
    private FixedLengthStringData t5679CnPremStat04 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 6);
    private FixedLengthStringData t5679CnPremStat05 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 8);
    private FixedLengthStringData t5679CnPremStat06 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 10);
    private FixedLengthStringData t5679CnPremStat07 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 12);
    private FixedLengthStringData t5679CnPremStat08 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 14);
    private FixedLengthStringData t5679CnPremStat09 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 16);
    private FixedLengthStringData t5679CnPremStat10 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 18);
    private FixedLengthStringData t5679CnPremStat11 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 20);
    private FixedLengthStringData t5679CnPremStat12 = new FixedLengthStringData(2).isAPartOf(t5679CnPremStats, 22);
    private FixedLengthStringData t5679CnRiskStats = new FixedLengthStringData(24).isAPartOf(t5679T5679Rec, 24);
    private FixedLengthStringData t5679CnRiskStat01 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 0);
    private FixedLengthStringData t5679CnRiskStat02 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 2);
    private FixedLengthStringData t5679CnRiskStat03 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 4);
    private FixedLengthStringData t5679CnRiskStat04 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 6);
    private FixedLengthStringData t5679CnRiskStat05 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 8);
    private FixedLengthStringData t5679CnRiskStat06 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 10);
    private FixedLengthStringData t5679CnRiskStat07 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 12);
    private FixedLengthStringData t5679CnRiskStat08 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 14);
    private FixedLengthStringData t5679CnRiskStat09 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 16);
    private FixedLengthStringData t5679CnRiskStat10 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 18);
    private FixedLengthStringData t5679CnRiskStat11 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 20);
    private FixedLengthStringData t5679CnRiskStat12 = new FixedLengthStringData(2).isAPartOf(t5679CnRiskStats, 22);
    private FixedLengthStringData t5679CovPremStats = new FixedLengthStringData(24).isAPartOf(t5679T5679Rec, 48);
    private FixedLengthStringData t5679CovPremStat01 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 0);
    private FixedLengthStringData t5679CovPremStat02 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 2);
    private FixedLengthStringData t5679CovPremStat03 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 4);
    private FixedLengthStringData t5679CovPremStat04 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 6);
    private FixedLengthStringData t5679CovPremStat05 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 8);
    private FixedLengthStringData t5679CovPremStat06 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 10);
    private FixedLengthStringData t5679CovPremStat07 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 12);
    private FixedLengthStringData t5679CovPremStat08 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 14);
    private FixedLengthStringData t5679CovPremStat09 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 16);
    private FixedLengthStringData t5679CovPremStat10 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 18);
    private FixedLengthStringData t5679CovPremStat11 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 20);
    private FixedLengthStringData t5679CovPremStat12 = new FixedLengthStringData(2).isAPartOf(t5679CovPremStats, 22);
    private FixedLengthStringData t5679CovRiskStats = new FixedLengthStringData(24).isAPartOf(t5679T5679Rec, 72);
    private FixedLengthStringData t5679CovRiskStat01 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 0);
    private FixedLengthStringData t5679CovRiskStat02 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 2);
    private FixedLengthStringData t5679CovRiskStat03 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 4);
    private FixedLengthStringData t5679CovRiskStat04 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 6);
    private FixedLengthStringData t5679CovRiskStat05 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 8);
    private FixedLengthStringData t5679CovRiskStat06 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 10);
    private FixedLengthStringData t5679CovRiskStat07 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 12);
    private FixedLengthStringData t5679CovRiskStat08 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 14);
    private FixedLengthStringData t5679CovRiskStat09 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 16);
    private FixedLengthStringData t5679CovRiskStat10 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 18);
    private FixedLengthStringData t5679CovRiskStat11 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 20);
    private FixedLengthStringData t5679CovRiskStat12 = new FixedLengthStringData(2).isAPartOf(t5679CovRiskStats, 22);
    private FixedLengthStringData t5679JlifeStats = new FixedLengthStringData(12).isAPartOf(t5679T5679Rec, 96);
    private FixedLengthStringData t5679JlifeStat01 = new FixedLengthStringData(2).isAPartOf(t5679JlifeStats, 0);
    private FixedLengthStringData t5679JlifeStat02 = new FixedLengthStringData(2).isAPartOf(t5679JlifeStats, 2);
    private FixedLengthStringData t5679JlifeStat03 = new FixedLengthStringData(2).isAPartOf(t5679JlifeStats, 4);
    private FixedLengthStringData t5679JlifeStat04 = new FixedLengthStringData(2).isAPartOf(t5679JlifeStats, 6);
    private FixedLengthStringData t5679JlifeStat05 = new FixedLengthStringData(2).isAPartOf(t5679JlifeStats, 8);
    private FixedLengthStringData t5679JlifeStat06 = new FixedLengthStringData(2).isAPartOf(t5679JlifeStats, 10);
    private FixedLengthStringData t5679LifeStats = new FixedLengthStringData(12).isAPartOf(t5679T5679Rec, 108);
    private FixedLengthStringData t5679LifeStat01 = new FixedLengthStringData(2).isAPartOf(t5679LifeStats, 0);
    private FixedLengthStringData t5679LifeStat02 = new FixedLengthStringData(2).isAPartOf(t5679LifeStats, 2);
    private FixedLengthStringData t5679LifeStat03 = new FixedLengthStringData(2).isAPartOf(t5679LifeStats, 4);
    private FixedLengthStringData t5679LifeStat04 = new FixedLengthStringData(2).isAPartOf(t5679LifeStats, 6);
    private FixedLengthStringData t5679LifeStat05 = new FixedLengthStringData(2).isAPartOf(t5679LifeStats, 8);
    private FixedLengthStringData t5679LifeStat06 = new FixedLengthStringData(2).isAPartOf(t5679LifeStats, 10);
    private FixedLengthStringData t5679RidPremStats = new FixedLengthStringData(24).isAPartOf(t5679T5679Rec, 120);
    private FixedLengthStringData t5679RidPremStat01 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 0);
    private FixedLengthStringData t5679RidPremStat02 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 2);
    private FixedLengthStringData t5679RidPremStat03 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 4);
    private FixedLengthStringData t5679RidPremStat04 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 6);
    private FixedLengthStringData t5679RidPremStat05 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 8);
    private FixedLengthStringData t5679RidPremStat06 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 10);
    private FixedLengthStringData t5679RidPremStat07 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 12);
    private FixedLengthStringData t5679RidPremStat08 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 14);
    private FixedLengthStringData t5679RidPremStat09 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 16);
    private FixedLengthStringData t5679RidPremStat10 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 18);
    private FixedLengthStringData t5679RidPremStat11 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 20);
    private FixedLengthStringData t5679RidPremStat12 = new FixedLengthStringData(2).isAPartOf(t5679RidPremStats, 22);
    private FixedLengthStringData t5679RidRiskStats = new FixedLengthStringData(24).isAPartOf(t5679T5679Rec, 144);
    private FixedLengthStringData t5679RidRiskStat01 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 0);
    private FixedLengthStringData t5679RidRiskStat02 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 2);
    private FixedLengthStringData t5679RidRiskStat03 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 4);
    private FixedLengthStringData t5679RidRiskStat04 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 6);
    private FixedLengthStringData t5679RidRiskStat05 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 8);
    private FixedLengthStringData t5679RidRiskStat06 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 10);
    private FixedLengthStringData t5679RidRiskStat07 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 12);
    private FixedLengthStringData t5679RidRiskStat08 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 14);
    private FixedLengthStringData t5679RidRiskStat09 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 16);
    private FixedLengthStringData t5679RidRiskStat10 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 18);
    private FixedLengthStringData t5679RidRiskStat11 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 20);
    private FixedLengthStringData t5679RidRiskStat12 = new FixedLengthStringData(2).isAPartOf(t5679RidRiskStats, 22);
    private FixedLengthStringData t5679SetCnPremStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 168);
    private FixedLengthStringData t5679SetCnRiskStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 170);
    private FixedLengthStringData t5679SetCovPremStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 172);
    private FixedLengthStringData t5679SetCovRiskStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 174);
    private FixedLengthStringData t5679SetJlifeStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 176);
    private FixedLengthStringData t5679SetLifeStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 178);
    private FixedLengthStringData t5679SetRidPremStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 180);
    private FixedLengthStringData t5679SetRidRiskStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 182);
    private FixedLengthStringData t5679SetSngpCnStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 184);
    private FixedLengthStringData t5679SetSngpCovStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 186);
    private FixedLengthStringData t5679SetSngpRidStat = new FixedLengthStringData(2).isAPartOf(t5679T5679Rec, 188);
    public FixedLengthStringData getT5679T5679Rec() {
        return t5679T5679Rec;
    }
    public String getT5679CnPremStats() {
        return t5679CnPremStats.toString();
    }
    public String getT5679CnPremStat01() {
        return t5679CnPremStat01.toString();
    }
    public String getT5679CnPremStat02() {
        return t5679CnPremStat02.toString();
    }
    public String getT5679CnPremStat03() {
        return t5679CnPremStat03.toString();
    }
    public String getT5679CnPremStat04() {
        return t5679CnPremStat04.toString();
    }
    public String getT5679CnPremStat05() {
        return t5679CnPremStat05.toString();
    }
    public String getT5679CnPremStat06() {
        return t5679CnPremStat06.toString();
    }
    public String getT5679CnPremStat07() {
        return t5679CnPremStat07.toString();
    }
    public String getT5679CnPremStat08() {
        return t5679CnPremStat08.toString();
    }
    public String getT5679CnPremStat09() {
        return t5679CnPremStat09.toString();
    }
    public String getT5679CnPremStat10() {
        return t5679CnPremStat10.toString();
    }
    public String getT5679CnPremStat11() {
        return t5679CnPremStat11.toString();
    }
    public String getT5679CnPremStat12() {
        return t5679CnPremStat12.toString();
    }
    public String getT5679CnRiskStats() {
        return t5679CnRiskStats.toString();
    }
    public String getT5679CnRiskStat01() {
        return t5679CnRiskStat01.toString();
    }
    public String getT5679CnRiskStat02() {
        return t5679CnRiskStat02.toString();
    }
    public String getT5679CnRiskStat03() {
        return t5679CnRiskStat03.toString();
    }
    public String getT5679CnRiskStat04() {
        return t5679CnRiskStat04.toString();
    }
    public String getT5679CnRiskStat05() {
        return t5679CnRiskStat05.toString();
    }
    public String getT5679CnRiskStat06() {
        return t5679CnRiskStat06.toString();
    }
    public String getT5679CnRiskStat07() {
        return t5679CnRiskStat07.toString();
    }
    public String getT5679CnRiskStat08() {
        return t5679CnRiskStat08.toString();
    }
    public String getT5679CnRiskStat09() {
        return t5679CnRiskStat09.toString();
    }
    public String getT5679CnRiskStat10() {
        return t5679CnRiskStat10.toString();
    }
    public String getT5679CnRiskStat11() {
        return t5679CnRiskStat11.toString();
    }
    public String getT5679CnRiskStat12() {
        return t5679CnRiskStat12.toString();
    }
    public String getT5679CovPremStats() {
        return t5679CovPremStats.toString();
    }
    public String getT5679CovPremStat01() {
        return t5679CovPremStat01.toString();
    }
    public String getT5679CovPremStat02() {
        return t5679CovPremStat02.toString();
    }
    public String getT5679CovPremStat03() {
        return t5679CovPremStat03.toString();
    }
    public String getT5679CovPremStat04() {
        return t5679CovPremStat04.toString();
    }
    public String getT5679CovPremStat05() {
        return t5679CovPremStat05.toString();
    }
    public String getT5679CovPremStat06() {
        return t5679CovPremStat06.toString();
    }
    public String getT5679CovPremStat07() {
        return t5679CovPremStat07.toString();
    }
    public String getT5679CovPremStat08() {
        return t5679CovPremStat08.toString();
    }
    public String getT5679CovPremStat09() {
        return t5679CovPremStat09.toString();
    }
    public String getT5679CovPremStat10() {
        return t5679CovPremStat10.toString();
    }
    public String getT5679CovPremStat11() {
        return t5679CovPremStat11.toString();
    }
    public String getT5679CovPremStat12() {
        return t5679CovPremStat12.toString();
    }
    public String getT5679CovRiskStats() {
        return t5679CovRiskStats.toString();
    }
    public String getT5679CovRiskStat01() {
        return t5679CovRiskStat01.toString();
    }
    public String getT5679CovRiskStat02() {
        return t5679CovRiskStat02.toString();
    }
    public String getT5679CovRiskStat03() {
        return t5679CovRiskStat03.toString();
    }
    public String getT5679CovRiskStat04() {
        return t5679CovRiskStat04.toString();
    }
    public String getT5679CovRiskStat05() {
        return t5679CovRiskStat05.toString();
    }
    public String getT5679CovRiskStat06() {
        return t5679CovRiskStat06.toString();
    }
    public String getT5679CovRiskStat07() {
        return t5679CovRiskStat07.toString();
    }
    public String getT5679CovRiskStat08() {
        return t5679CovRiskStat08.toString();
    }
    public String getT5679CovRiskStat09() {
        return t5679CovRiskStat09.toString();
    }
    public String getT5679CovRiskStat10() {
        return t5679CovRiskStat10.toString();
    }
    public String getT5679CovRiskStat11() {
        return t5679CovRiskStat11.toString();
    }
    public String getT5679CovRiskStat12() {
        return t5679CovRiskStat12.toString();
    }
    public String getT5679JlifeStats() {
        return t5679JlifeStats.toString();
    }
    public String getT5679JlifeStat01() {
        return t5679JlifeStat01.toString();
    }
    public String getT5679JlifeStat02() {
        return t5679JlifeStat02.toString();
    }
    public String getT5679JlifeStat03() {
        return t5679JlifeStat03.toString();
    }
    public String getT5679JlifeStat04() {
        return t5679JlifeStat04.toString();
    }
    public String getT5679JlifeStat05() {
        return t5679JlifeStat05.toString();
    }
    public String getT5679JlifeStat06() {
        return t5679JlifeStat06.toString();
    }
    public String getT5679LifeStats() {
        return t5679LifeStats.toString();
    }
    public String getT5679LifeStat01() {
        return t5679LifeStat01.toString();
    }
    public String getT5679LifeStat02() {
        return t5679LifeStat02.toString();
    }
    public String getT5679LifeStat03() {
        return t5679LifeStat03.toString();
    }
    public String getT5679LifeStat04() {
        return t5679LifeStat04.toString();
    }
    public String getT5679LifeStat05() {
        return t5679LifeStat05.toString();
    }
    public String getT5679LifeStat06() {
        return t5679LifeStat06.toString();
    }
    public String getT5679RidPremStats() {
        return t5679RidPremStats.toString();
    }
    public String getT5679RidPremStat01() {
        return t5679RidPremStat01.toString();
    }
    public String getT5679RidPremStat02() {
        return t5679RidPremStat02.toString();
    }
    public String getT5679RidPremStat03() {
        return t5679RidPremStat03.toString();
    }
    public String getT5679RidPremStat04() {
        return t5679RidPremStat04.toString();
    }
    public String getT5679RidPremStat05() {
        return t5679RidPremStat05.toString();
    }
    public String getT5679RidPremStat06() {
        return t5679RidPremStat06.toString();
    }
    public String getT5679RidPremStat07() {
        return t5679RidPremStat07.toString();
    }
    public String getT5679RidPremStat08() {
        return t5679RidPremStat08.toString();
    }
    public String getT5679RidPremStat09() {
        return t5679RidPremStat09.toString();
    }
    public String getT5679RidPremStat10() {
        return t5679RidPremStat10.toString();
    }
    public String getT5679RidPremStat11() {
        return t5679RidPremStat11.toString();
    }
    public String getT5679RidPremStat12() {
        return t5679RidPremStat12.toString();
    }
    public String getT5679RidRiskStats() {
        return t5679RidRiskStats.toString();
    }
    public String getT5679RidRiskStat01() {
        return t5679RidRiskStat01.toString();
    }
    public String getT5679RidRiskStat02() {
        return t5679RidRiskStat02.toString();
    }
    public String getT5679RidRiskStat03() {
        return t5679RidRiskStat03.toString();
    }
    public String getT5679RidRiskStat04() {
        return t5679RidRiskStat04.toString();
    }
    public String getT5679RidRiskStat05() {
        return t5679RidRiskStat05.toString();
    }
    public String getT5679RidRiskStat06() {
        return t5679RidRiskStat06.toString();
    }
    public String getT5679RidRiskStat07() {
        return t5679RidRiskStat07.toString();
    }
    public String getT5679RidRiskStat08() {
        return t5679RidRiskStat08.toString();
    }
    public String getT5679RidRiskStat09() {
        return t5679RidRiskStat09.toString();
    }
    public String getT5679RidRiskStat10() {
        return t5679RidRiskStat10.toString();
    }
    public String getT5679RidRiskStat11() {
        return t5679RidRiskStat11.toString();
    }
    public String getT5679RidRiskStat12() {
        return t5679RidRiskStat12.toString();
    }
    public String getT5679SetCnPremStat() {
        return t5679SetCnPremStat.toString();
    }
    public String getT5679SetCnRiskStat() {
        return t5679SetCnRiskStat.toString();
    }
    public String getT5679SetCovPremStat() {
        return t5679SetCovPremStat.toString();
    }
    public String getT5679SetCovRiskStat() {
        return t5679SetCovRiskStat.toString();
    }
    public String getT5679SetJlifeStat() {
        return t5679SetJlifeStat.toString();
    }
    public String getT5679SetLifeStat() {
        return t5679SetLifeStat.toString();
    }
    public String getT5679SetRidPremStat() {
        return t5679SetRidPremStat.toString();
    }
    public String getT5679SetRidRiskStat() {
        return t5679SetRidRiskStat.toString();
    }
    public String getT5679SetSngpCnStat() {
        return t5679SetSngpCnStat.toString();
    }
    public String getT5679SetSngpCovStat() {
        return t5679SetSngpCovStat.toString();
    }
    public String getT5679SetSngpRidStat() {
        return t5679SetSngpRidStat.toString();
    }
    public void setT5679T5679Rec(FixedLengthStringData t5679t5679Rec) {
        t5679T5679Rec = t5679t5679Rec;
    }
    public void setT5679CnPremStats(FixedLengthStringData t5679CnPremStats) {
        this.t5679CnPremStats = t5679CnPremStats;
    }
    public void setT5679CnPremStat01(FixedLengthStringData t5679CnPremStat01) {
        this.t5679CnPremStat01 = t5679CnPremStat01;
    }
    public void setT5679CnPremStat02(FixedLengthStringData t5679CnPremStat02) {
        this.t5679CnPremStat02 = t5679CnPremStat02;
    }
    public void setT5679CnPremStat03(FixedLengthStringData t5679CnPremStat03) {
        this.t5679CnPremStat03 = t5679CnPremStat03;
    }
    public void setT5679CnPremStat04(FixedLengthStringData t5679CnPremStat04) {
        this.t5679CnPremStat04 = t5679CnPremStat04;
    }
    public void setT5679CnPremStat05(FixedLengthStringData t5679CnPremStat05) {
        this.t5679CnPremStat05 = t5679CnPremStat05;
    }
    public void setT5679CnPremStat06(FixedLengthStringData t5679CnPremStat06) {
        this.t5679CnPremStat06 = t5679CnPremStat06;
    }
    public void setT5679CnPremStat07(FixedLengthStringData t5679CnPremStat07) {
        this.t5679CnPremStat07 = t5679CnPremStat07;
    }
    public void setT5679CnPremStat08(FixedLengthStringData t5679CnPremStat08) {
        this.t5679CnPremStat08 = t5679CnPremStat08;
    }
    public void setT5679CnPremStat09(FixedLengthStringData t5679CnPremStat09) {
        this.t5679CnPremStat09 = t5679CnPremStat09;
    }
    public void setT5679CnPremStat10(FixedLengthStringData t5679CnPremStat10) {
        this.t5679CnPremStat10 = t5679CnPremStat10;
    }
    public void setT5679CnPremStat11(FixedLengthStringData t5679CnPremStat11) {
        this.t5679CnPremStat11 = t5679CnPremStat11;
    }
    public void setT5679CnPremStat12(FixedLengthStringData t5679CnPremStat12) {
        this.t5679CnPremStat12 = t5679CnPremStat12;
    }
    public void setT5679CnRiskStats(FixedLengthStringData t5679CnRiskStats) {
        this.t5679CnRiskStats = t5679CnRiskStats;
    }
    public void setT5679CnRiskStat01(FixedLengthStringData t5679CnRiskStat01) {
        this.t5679CnRiskStat01 = t5679CnRiskStat01;
    }
    public void setT5679CnRiskStat02(FixedLengthStringData t5679CnRiskStat02) {
        this.t5679CnRiskStat02 = t5679CnRiskStat02;
    }
    public void setT5679CnRiskStat03(FixedLengthStringData t5679CnRiskStat03) {
        this.t5679CnRiskStat03 = t5679CnRiskStat03;
    }
    public void setT5679CnRiskStat04(FixedLengthStringData t5679CnRiskStat04) {
        this.t5679CnRiskStat04 = t5679CnRiskStat04;
    }
    public void setT5679CnRiskStat05(FixedLengthStringData t5679CnRiskStat05) {
        this.t5679CnRiskStat05 = t5679CnRiskStat05;
    }
    public void setT5679CnRiskStat06(FixedLengthStringData t5679CnRiskStat06) {
        this.t5679CnRiskStat06 = t5679CnRiskStat06;
    }
    public void setT5679CnRiskStat07(FixedLengthStringData t5679CnRiskStat07) {
        this.t5679CnRiskStat07 = t5679CnRiskStat07;
    }
    public void setT5679CnRiskStat08(FixedLengthStringData t5679CnRiskStat08) {
        this.t5679CnRiskStat08 = t5679CnRiskStat08;
    }
    public void setT5679CnRiskStat09(FixedLengthStringData t5679CnRiskStat09) {
        this.t5679CnRiskStat09 = t5679CnRiskStat09;
    }
    public void setT5679CnRiskStat10(FixedLengthStringData t5679CnRiskStat10) {
        this.t5679CnRiskStat10 = t5679CnRiskStat10;
    }
    public void setT5679CnRiskStat11(FixedLengthStringData t5679CnRiskStat11) {
        this.t5679CnRiskStat11 = t5679CnRiskStat11;
    }
    public void setT5679CnRiskStat12(FixedLengthStringData t5679CnRiskStat12) {
        this.t5679CnRiskStat12 = t5679CnRiskStat12;
    }
    public void setT5679CovPremStats(FixedLengthStringData t5679CovPremStats) {
        this.t5679CovPremStats = t5679CovPremStats;
    }
    public void setT5679CovPremStat01(FixedLengthStringData t5679CovPremStat01) {
        this.t5679CovPremStat01 = t5679CovPremStat01;
    }
    public void setT5679CovPremStat02(FixedLengthStringData t5679CovPremStat02) {
        this.t5679CovPremStat02 = t5679CovPremStat02;
    }
    public void setT5679CovPremStat03(FixedLengthStringData t5679CovPremStat03) {
        this.t5679CovPremStat03 = t5679CovPremStat03;
    }
    public void setT5679CovPremStat04(FixedLengthStringData t5679CovPremStat04) {
        this.t5679CovPremStat04 = t5679CovPremStat04;
    }
    public void setT5679CovPremStat05(FixedLengthStringData t5679CovPremStat05) {
        this.t5679CovPremStat05 = t5679CovPremStat05;
    }
    public void setT5679CovPremStat06(FixedLengthStringData t5679CovPremStat06) {
        this.t5679CovPremStat06 = t5679CovPremStat06;
    }
    public void setT5679CovPremStat07(FixedLengthStringData t5679CovPremStat07) {
        this.t5679CovPremStat07 = t5679CovPremStat07;
    }
    public void setT5679CovPremStat08(FixedLengthStringData t5679CovPremStat08) {
        this.t5679CovPremStat08 = t5679CovPremStat08;
    }
    public void setT5679CovPremStat09(FixedLengthStringData t5679CovPremStat09) {
        this.t5679CovPremStat09 = t5679CovPremStat09;
    }
    public void setT5679CovPremStat10(FixedLengthStringData t5679CovPremStat10) {
        this.t5679CovPremStat10 = t5679CovPremStat10;
    }
    public void setT5679CovPremStat11(FixedLengthStringData t5679CovPremStat11) {
        this.t5679CovPremStat11 = t5679CovPremStat11;
    }
    public void setT5679CovPremStat12(FixedLengthStringData t5679CovPremStat12) {
        this.t5679CovPremStat12 = t5679CovPremStat12;
    }
    public void setT5679CovRiskStats(FixedLengthStringData t5679CovRiskStats) {
        this.t5679CovRiskStats = t5679CovRiskStats;
    }
    public void setT5679CovRiskStat01(FixedLengthStringData t5679CovRiskStat01) {
        this.t5679CovRiskStat01 = t5679CovRiskStat01;
    }
    public void setT5679CovRiskStat02(FixedLengthStringData t5679CovRiskStat02) {
        this.t5679CovRiskStat02 = t5679CovRiskStat02;
    }
    public void setT5679CovRiskStat03(FixedLengthStringData t5679CovRiskStat03) {
        this.t5679CovRiskStat03 = t5679CovRiskStat03;
    }
    public void setT5679CovRiskStat04(FixedLengthStringData t5679CovRiskStat04) {
        this.t5679CovRiskStat04 = t5679CovRiskStat04;
    }
    public void setT5679CovRiskStat05(FixedLengthStringData t5679CovRiskStat05) {
        this.t5679CovRiskStat05 = t5679CovRiskStat05;
    }
    public void setT5679CovRiskStat06(FixedLengthStringData t5679CovRiskStat06) {
        this.t5679CovRiskStat06 = t5679CovRiskStat06;
    }
    public void setT5679CovRiskStat07(FixedLengthStringData t5679CovRiskStat07) {
        this.t5679CovRiskStat07 = t5679CovRiskStat07;
    }
    public void setT5679CovRiskStat08(FixedLengthStringData t5679CovRiskStat08) {
        this.t5679CovRiskStat08 = t5679CovRiskStat08;
    }
    public void setT5679CovRiskStat09(FixedLengthStringData t5679CovRiskStat09) {
        this.t5679CovRiskStat09 = t5679CovRiskStat09;
    }
    public void setT5679CovRiskStat10(FixedLengthStringData t5679CovRiskStat10) {
        this.t5679CovRiskStat10 = t5679CovRiskStat10;
    }
    public void setT5679CovRiskStat11(FixedLengthStringData t5679CovRiskStat11) {
        this.t5679CovRiskStat11 = t5679CovRiskStat11;
    }
    public void setT5679CovRiskStat12(FixedLengthStringData t5679CovRiskStat12) {
        this.t5679CovRiskStat12 = t5679CovRiskStat12;
    }
    public void setT5679JlifeStats(FixedLengthStringData t5679JlifeStats) {
        this.t5679JlifeStats = t5679JlifeStats;
    }
    public void setT5679JlifeStat01(FixedLengthStringData t5679JlifeStat01) {
        this.t5679JlifeStat01 = t5679JlifeStat01;
    }
    public void setT5679JlifeStat02(FixedLengthStringData t5679JlifeStat02) {
        this.t5679JlifeStat02 = t5679JlifeStat02;
    }
    public void setT5679JlifeStat03(FixedLengthStringData t5679JlifeStat03) {
        this.t5679JlifeStat03 = t5679JlifeStat03;
    }
    public void setT5679JlifeStat04(FixedLengthStringData t5679JlifeStat04) {
        this.t5679JlifeStat04 = t5679JlifeStat04;
    }
    public void setT5679JlifeStat05(FixedLengthStringData t5679JlifeStat05) {
        this.t5679JlifeStat05 = t5679JlifeStat05;
    }
    public void setT5679JlifeStat06(FixedLengthStringData t5679JlifeStat06) {
        this.t5679JlifeStat06 = t5679JlifeStat06;
    }
    public void setT5679LifeStats(FixedLengthStringData t5679LifeStats) {
        this.t5679LifeStats = t5679LifeStats;
    }
    public void setT5679LifeStat01(FixedLengthStringData t5679LifeStat01) {
        this.t5679LifeStat01 = t5679LifeStat01;
    }
    public void setT5679LifeStat02(FixedLengthStringData t5679LifeStat02) {
        this.t5679LifeStat02 = t5679LifeStat02;
    }
    public void setT5679LifeStat03(FixedLengthStringData t5679LifeStat03) {
        this.t5679LifeStat03 = t5679LifeStat03;
    }
    public void setT5679LifeStat04(FixedLengthStringData t5679LifeStat04) {
        this.t5679LifeStat04 = t5679LifeStat04;
    }
    public void setT5679LifeStat05(FixedLengthStringData t5679LifeStat05) {
        this.t5679LifeStat05 = t5679LifeStat05;
    }
    public void setT5679LifeStat06(FixedLengthStringData t5679LifeStat06) {
        this.t5679LifeStat06 = t5679LifeStat06;
    }
    public void setT5679RidPremStats(FixedLengthStringData t5679RidPremStats) {
        this.t5679RidPremStats = t5679RidPremStats;
    }
    public void setT5679RidPremStat01(FixedLengthStringData t5679RidPremStat01) {
        this.t5679RidPremStat01 = t5679RidPremStat01;
    }
    public void setT5679RidPremStat02(FixedLengthStringData t5679RidPremStat02) {
        this.t5679RidPremStat02 = t5679RidPremStat02;
    }
    public void setT5679RidPremStat03(FixedLengthStringData t5679RidPremStat03) {
        this.t5679RidPremStat03 = t5679RidPremStat03;
    }
    public void setT5679RidPremStat04(FixedLengthStringData t5679RidPremStat04) {
        this.t5679RidPremStat04 = t5679RidPremStat04;
    }
    public void setT5679RidPremStat05(FixedLengthStringData t5679RidPremStat05) {
        this.t5679RidPremStat05 = t5679RidPremStat05;
    }
    public void setT5679RidPremStat06(FixedLengthStringData t5679RidPremStat06) {
        this.t5679RidPremStat06 = t5679RidPremStat06;
    }
    public void setT5679RidPremStat07(FixedLengthStringData t5679RidPremStat07) {
        this.t5679RidPremStat07 = t5679RidPremStat07;
    }
    public void setT5679RidPremStat08(FixedLengthStringData t5679RidPremStat08) {
        this.t5679RidPremStat08 = t5679RidPremStat08;
    }
    public void setT5679RidPremStat09(FixedLengthStringData t5679RidPremStat09) {
        this.t5679RidPremStat09 = t5679RidPremStat09;
    }
    public void setT5679RidPremStat10(FixedLengthStringData t5679RidPremStat10) {
        this.t5679RidPremStat10 = t5679RidPremStat10;
    }
    public void setT5679RidPremStat11(FixedLengthStringData t5679RidPremStat11) {
        this.t5679RidPremStat11 = t5679RidPremStat11;
    }
    public void setT5679RidPremStat12(FixedLengthStringData t5679RidPremStat12) {
        this.t5679RidPremStat12 = t5679RidPremStat12;
    }
    public void setT5679RidRiskStats(FixedLengthStringData t5679RidRiskStats) {
        this.t5679RidRiskStats = t5679RidRiskStats;
    }
    public void setT5679RidRiskStat01(FixedLengthStringData t5679RidRiskStat01) {
        this.t5679RidRiskStat01 = t5679RidRiskStat01;
    }
    public void setT5679RidRiskStat02(FixedLengthStringData t5679RidRiskStat02) {
        this.t5679RidRiskStat02 = t5679RidRiskStat02;
    }
    public void setT5679RidRiskStat03(FixedLengthStringData t5679RidRiskStat03) {
        this.t5679RidRiskStat03 = t5679RidRiskStat03;
    }
    public void setT5679RidRiskStat04(FixedLengthStringData t5679RidRiskStat04) {
        this.t5679RidRiskStat04 = t5679RidRiskStat04;
    }
    public void setT5679RidRiskStat05(FixedLengthStringData t5679RidRiskStat05) {
        this.t5679RidRiskStat05 = t5679RidRiskStat05;
    }
    public void setT5679RidRiskStat06(FixedLengthStringData t5679RidRiskStat06) {
        this.t5679RidRiskStat06 = t5679RidRiskStat06;
    }
    public void setT5679RidRiskStat07(FixedLengthStringData t5679RidRiskStat07) {
        this.t5679RidRiskStat07 = t5679RidRiskStat07;
    }
    public void setT5679RidRiskStat08(FixedLengthStringData t5679RidRiskStat08) {
        this.t5679RidRiskStat08 = t5679RidRiskStat08;
    }
    public void setT5679RidRiskStat09(FixedLengthStringData t5679RidRiskStat09) {
        this.t5679RidRiskStat09 = t5679RidRiskStat09;
    }
    public void setT5679RidRiskStat10(FixedLengthStringData t5679RidRiskStat10) {
        this.t5679RidRiskStat10 = t5679RidRiskStat10;
    }
    public void setT5679RidRiskStat11(FixedLengthStringData t5679RidRiskStat11) {
        this.t5679RidRiskStat11 = t5679RidRiskStat11;
    }
    public void setT5679RidRiskStat12(FixedLengthStringData t5679RidRiskStat12) {
        this.t5679RidRiskStat12 = t5679RidRiskStat12;
    }
    public void setT5679SetCnPremStat(FixedLengthStringData t5679SetCnPremStat) {
        this.t5679SetCnPremStat = t5679SetCnPremStat;
    }
    public void setT5679SetCnRiskStat(FixedLengthStringData t5679SetCnRiskStat) {
        this.t5679SetCnRiskStat = t5679SetCnRiskStat;
    }
    public void setT5679SetCovPremStat(FixedLengthStringData t5679SetCovPremStat) {
        this.t5679SetCovPremStat = t5679SetCovPremStat;
    }
    public void setT5679SetCovRiskStat(FixedLengthStringData t5679SetCovRiskStat) {
        this.t5679SetCovRiskStat = t5679SetCovRiskStat;
    }
    public void setT5679SetJlifeStat(FixedLengthStringData t5679SetJlifeStat) {
        this.t5679SetJlifeStat = t5679SetJlifeStat;
    }
    public void setT5679SetLifeStat(FixedLengthStringData t5679SetLifeStat) {
        this.t5679SetLifeStat = t5679SetLifeStat;
    }
    public void setT5679SetRidPremStat(FixedLengthStringData t5679SetRidPremStat) {
        this.t5679SetRidPremStat = t5679SetRidPremStat;
    }
    public void setT5679SetRidRiskStat(FixedLengthStringData t5679SetRidRiskStat) {
        this.t5679SetRidRiskStat = t5679SetRidRiskStat;
    }
    public void setT5679SetSngpCnStat(FixedLengthStringData t5679SetSngpCnStat) {
        this.t5679SetSngpCnStat = t5679SetSngpCnStat;
    }
    public void setT5679SetSngpCovStat(FixedLengthStringData t5679SetSngpCovStat) {
        this.t5679SetSngpCovStat = t5679SetSngpCovStat;
    }
    public void setT5679SetSngpRidStat(FixedLengthStringData t5679SetSngpRidStat) {
        this.t5679SetSngpRidStat = t5679SetSngpRidStat;
    }
}
