/* ********************   */
/*Author  :tsaxena3		  */
/*Purpose :Util of Crtloan*/
/*Date    :2018.10.04	  */
package com.csc.life.regularprocessing.procedures;

public interface CrtloanUtils {

	public void calCrtloan(CrtloanPojo crtloanPojo);
}
