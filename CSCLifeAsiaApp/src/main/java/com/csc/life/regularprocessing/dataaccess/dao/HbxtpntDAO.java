package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.newbusiness.dataaccess.model.Payrpf;
import com.csc.life.regularprocessing.dataaccess.model.Hbxtpnt;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface HbxtpntDAO extends BaseDAO<Hbxtpnt> {

	public List<Hbxtpnt> readHbxtpnt(String chdrcoy,String chdrnum,int instfrom);
	public boolean delete(Hbxtpnt hbxtpnt);
	public boolean update(String flag,Hbxtpnt hbxtpnt);
	public List<Payrpf> readPayrpfTranno(int seqno,Hbxtpnt hbxtpnt);
}
