package com.csc.life.regularprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: InczpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:39
 * Class transformed from INCZPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class InczpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 117;
	public FixedLengthStringData inczrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData inczpfRecord = inczrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(inczrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(inczrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(inczrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(inczrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(inczrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(inczrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(inczrec);
	public PackedDecimalData crrcd = DD.crrcd.copy().isAPartOf(inczrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(inczrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(inczrec);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(inczrec);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(inczrec);
	public FixedLengthStringData anniversaryMethod = DD.annvry.copy().isAPartOf(inczrec);
	public FixedLengthStringData basicCommMeth = DD.bascmeth.copy().isAPartOf(inczrec);
	public FixedLengthStringData bascpy = DD.bascpy.copy().isAPartOf(inczrec);
	public FixedLengthStringData rnwcpy = DD.rnwcpy.copy().isAPartOf(inczrec);
	public FixedLengthStringData srvcpy = DD.srvcpy.copy().isAPartOf(inczrec);
	public PackedDecimalData newinst = DD.newinst.copy().isAPartOf(inczrec);
	public PackedDecimalData lastInst = DD.lastinst.copy().isAPartOf(inczrec);
	public PackedDecimalData newsum = DD.newsum.copy().isAPartOf(inczrec);
	public PackedDecimalData zbnewinst = DD.zbnewinst.copy().isAPartOf(inczrec);
	public PackedDecimalData zblastinst = DD.zblastinst.copy().isAPartOf(inczrec);
	public PackedDecimalData zlnewinst = DD.zlnewinst.copy().isAPartOf(inczrec);
	public PackedDecimalData zllastinst = DD.zllastinst.copy().isAPartOf(inczrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public InczpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for InczpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public InczpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for InczpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public InczpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for InczpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public InczpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("INCZPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"CRRCD, " +
							"CRTABLE, " +
							"VALIDFLAG, " +
							"STATCODE, " +
							"PSTATCODE, " +
							"ANNVRY, " +
							"BASCMETH, " +
							"BASCPY, " +
							"RNWCPY, " +
							"SRVCPY, " +
							"NEWINST, " +
							"LASTINST, " +
							"NEWSUM, " +
							"ZBNEWINST, " +
							"ZBLASTINST, " +
							"ZLNEWINST, " +
							"ZLLASTINST, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     crrcd,
                                     crtable,
                                     validflag,
                                     statcode,
                                     pstatcode,
                                     anniversaryMethod,
                                     basicCommMeth,
                                     bascpy,
                                     rnwcpy,
                                     srvcpy,
                                     newinst,
                                     lastInst,
                                     newsum,
                                     zbnewinst,
                                     zblastinst,
                                     zlnewinst,
                                     zllastinst,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		crrcd.clear();
  		crtable.clear();
  		validflag.clear();
  		statcode.clear();
  		pstatcode.clear();
  		anniversaryMethod.clear();
  		basicCommMeth.clear();
  		bascpy.clear();
  		rnwcpy.clear();
  		srvcpy.clear();
  		newinst.clear();
  		lastInst.clear();
  		newsum.clear();
  		zbnewinst.clear();
  		zblastinst.clear();
  		zlnewinst.clear();
  		zllastinst.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getInczrec() {
  		return inczrec;
	}

	public FixedLengthStringData getInczpfRecord() {
  		return inczpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setInczrec(what);
	}

	public void setInczrec(Object what) {
  		this.inczrec.set(what);
	}

	public void setInczpfRecord(Object what) {
  		this.inczpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(inczrec.getLength());
		result.set(inczrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}