package com.csc.life.regularprocessing.dataaccess.dao;

import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.Covxpf;

public interface CovxpfDAO {
	public List<Covxpf> searchCovxRecord(String tableId, String memName, int batchExtractSize, int batchID);
}

