package com.csc.life.regularprocessing.dataaccess.dao;

import com.csc.life.regularprocessing.dataaccess.model.Bd5lwDTO;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

import java.util.List;

public interface Bd5lwTempDAO extends BaseDAO<Object> {
	public boolean deleteBd5lwTempAllRecords();
	public boolean insertBd5lwTempRecords(String chdrcoy, String chdrnumlow, String chdrnumhigh);
	public List<Bd5lwDTO> loadData(int minRecord, int maxRecord);
}
