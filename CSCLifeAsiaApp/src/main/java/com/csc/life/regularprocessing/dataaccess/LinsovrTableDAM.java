package com.csc.life.regularprocessing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: LinsovrTableDAM.java
 * Date: Sun, 30 Aug 2009 03:42:44
 * Class transformed from LINSOVR.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LinsovrTableDAM extends LinspfTableDAM {

	public LinsovrTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("LINSOVR");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", INSTTO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "CNTCURR, " +
		            "BILLCURR, " +
		            "VALIDFLAG, " +
		            "BRANCH, " +
		            "INSTFROM, " +
		            "INSTTO, " +
		            "INSTAMT01, " +
		            "INSTAMT02, " +
		            "INSTAMT03, " +
		            "INSTAMT04, " +
		            "INSTAMT05, " +
		            "INSTAMT06, " +
		            "INSTFREQ, " +
		            "INSTJCTL, " +
		            "CBILLAMT, " +
		            "BILLCHNL, " +
		            "PAYFLAG, " +
		            "DUEFLG, " +
		            "TRANSCODE, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "PRORATEREC, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "INSTTO DESC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "INSTTO ASC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               cntcurr,
                               billcurr,
                               validflag,
                               branch,
                               instfrom,
                               instto,
                               instamt01,
                               instamt02,
                               instamt03,
                               instamt04,
                               instamt05,
                               instamt06,
                               instfreq,
                               instjctl,
                               cbillamt,
                               billchnl,
                               payflag,
                               dueflg,
                               transcode,
                               userProfile,
                               jobName,
                               datime,
                               proraterec,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(50);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getInstto().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, instto);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller8 = new FixedLengthStringData(5);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller8.setInternal(instto.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(172);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ getCntcurr().toInternal()
					+ getBillcurr().toInternal()
					+ getValidflag().toInternal()
					+ getBranch().toInternal()
					+ getInstfrom().toInternal()
					+ nonKeyFiller8.toInternal()
					+ getInstamt01().toInternal()
					+ getInstamt02().toInternal()
					+ getInstamt03().toInternal()
					+ getInstamt04().toInternal()
					+ getInstamt05().toInternal()
					+ getInstamt06().toInternal()
					+ getInstfreq().toInternal()
					+ getInstjctl().toInternal()
					+ getCbillamt().toInternal()
					+ getBillchnl().toInternal()
					+ getPayflag().toInternal()
					+ getDueflg().toInternal()
					+ getTranscode().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()
					+ getProraterec().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, billcurr);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, branch);
			what = ExternalData.chop(what, instfrom);
			what = ExternalData.chop(what, nonKeyFiller8);
			what = ExternalData.chop(what, instamt01);
			what = ExternalData.chop(what, instamt02);
			what = ExternalData.chop(what, instamt03);
			what = ExternalData.chop(what, instamt04);
			what = ExternalData.chop(what, instamt05);
			what = ExternalData.chop(what, instamt06);
			what = ExternalData.chop(what, instfreq);
			what = ExternalData.chop(what, instjctl);
			what = ExternalData.chop(what, cbillamt);
			what = ExternalData.chop(what, billchnl);
			what = ExternalData.chop(what, payflag);
			what = ExternalData.chop(what, dueflg);
			what = ExternalData.chop(what, transcode);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);
			what = ExternalData.chop(what, proraterec);
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getInstto() {
		return instto;
	}
	public void setInstto(Object what) {
		setInstto(what, false);
	}
	public void setInstto(Object what, boolean rounded) {
		if (rounded)
			instto.setRounded(what);
		else
			instto.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public FixedLengthStringData getBillcurr() {
		return billcurr;
	}
	public void setBillcurr(Object what) {
		billcurr.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getBranch() {
		return branch;
	}
	public void setBranch(Object what) {
		branch.set(what);
	}	
	public PackedDecimalData getInstfrom() {
		return instfrom;
	}
	public void setInstfrom(Object what) {
		setInstfrom(what, false);
	}
	public void setInstfrom(Object what, boolean rounded) {
		if (rounded)
			instfrom.setRounded(what);
		else
			instfrom.set(what);
	}	
	public PackedDecimalData getInstamt01() {
		return instamt01;
	}
	public void setInstamt01(Object what) {
		setInstamt01(what, false);
	}
	public void setInstamt01(Object what, boolean rounded) {
		if (rounded)
			instamt01.setRounded(what);
		else
			instamt01.set(what);
	}	
	public PackedDecimalData getInstamt02() {
		return instamt02;
	}
	public void setInstamt02(Object what) {
		setInstamt02(what, false);
	}
	public void setInstamt02(Object what, boolean rounded) {
		if (rounded)
			instamt02.setRounded(what);
		else
			instamt02.set(what);
	}	
	public PackedDecimalData getInstamt03() {
		return instamt03;
	}
	public void setInstamt03(Object what) {
		setInstamt03(what, false);
	}
	public void setInstamt03(Object what, boolean rounded) {
		if (rounded)
			instamt03.setRounded(what);
		else
			instamt03.set(what);
	}	
	public PackedDecimalData getInstamt04() {
		return instamt04;
	}
	public void setInstamt04(Object what) {
		setInstamt04(what, false);
	}
	public void setInstamt04(Object what, boolean rounded) {
		if (rounded)
			instamt04.setRounded(what);
		else
			instamt04.set(what);
	}	
	public PackedDecimalData getInstamt05() {
		return instamt05;
	}
	public void setInstamt05(Object what) {
		setInstamt05(what, false);
	}
	public void setInstamt05(Object what, boolean rounded) {
		if (rounded)
			instamt05.setRounded(what);
		else
			instamt05.set(what);
	}	
	public PackedDecimalData getInstamt06() {
		return instamt06;
	}
	public void setInstamt06(Object what) {
		setInstamt06(what, false);
	}
	public void setInstamt06(Object what, boolean rounded) {
		if (rounded)
			instamt06.setRounded(what);
		else
			instamt06.set(what);
	}	
	public FixedLengthStringData getInstfreq() {
		return instfreq;
	}
	public void setInstfreq(Object what) {
		instfreq.set(what);
	}	
	public FixedLengthStringData getInstjctl() {
		return instjctl;
	}
	public void setInstjctl(Object what) {
		instjctl.set(what);
	}	
	public PackedDecimalData getCbillamt() {
		return cbillamt;
	}
	public void setCbillamt(Object what) {
		setCbillamt(what, false);
	}
	public void setCbillamt(Object what, boolean rounded) {
		if (rounded)
			cbillamt.setRounded(what);
		else
			cbillamt.set(what);
	}	
	public FixedLengthStringData getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(Object what) {
		billchnl.set(what);
	}	
	public FixedLengthStringData getPayflag() {
		return payflag;
	}
	public void setPayflag(Object what) {
		payflag.set(what);
	}	
	public FixedLengthStringData getDueflg() {
		return dueflg;
	}
	public void setDueflg(Object what) {
		dueflg.set(what);
	}	
	public FixedLengthStringData getTranscode() {
		return transcode;
	}
	public void setTranscode(Object what) {
		transcode.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	public FixedLengthStringData getProraterec() {
		return proraterec;
	}
	public void setProraterec(Object what) {
		proraterec.set(what);
	}

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getInstamts() {
		return new FixedLengthStringData(instamt01.toInternal()
										+ instamt02.toInternal()
										+ instamt03.toInternal()
										+ instamt04.toInternal()
										+ instamt05.toInternal()
										+ instamt06.toInternal());
	}
	public void setInstamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getInstamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, instamt01);
		what = ExternalData.chop(what, instamt02);
		what = ExternalData.chop(what, instamt03);
		what = ExternalData.chop(what, instamt04);
		what = ExternalData.chop(what, instamt05);
		what = ExternalData.chop(what, instamt06);
	}
	public PackedDecimalData getInstamt(BaseData indx) {
		return getInstamt(indx.toInt());
	}
	public PackedDecimalData getInstamt(int indx) {

		switch (indx) {
			case 1 : return instamt01;
			case 2 : return instamt02;
			case 3 : return instamt03;
			case 4 : return instamt04;
			case 5 : return instamt05;
			case 6 : return instamt06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setInstamt(BaseData indx, Object what) {
		setInstamt(indx, what, false);
	}
	public void setInstamt(BaseData indx, Object what, boolean rounded) {
		setInstamt(indx.toInt(), what, rounded);
	}
	public void setInstamt(int indx, Object what) {
		setInstamt(indx, what, false);
	}
	public void setInstamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInstamt01(what, rounded);
					 break;
			case 2 : setInstamt02(what, rounded);
					 break;
			case 3 : setInstamt03(what, rounded);
					 break;
			case 4 : setInstamt04(what, rounded);
					 break;
			case 5 : setInstamt05(what, rounded);
					 break;
			case 6 : setInstamt06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		instto.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		cntcurr.clear();
		billcurr.clear();
		validflag.clear();
		branch.clear();
		instfrom.clear();
		nonKeyFiller8.clear();
		instamt01.clear();
		instamt02.clear();
		instamt03.clear();
		instamt04.clear();
		instamt05.clear();
		instamt06.clear();
		instfreq.clear();
		instjctl.clear();
		cbillamt.clear();
		billchnl.clear();
		payflag.clear();
		dueflg.clear();
		transcode.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();
		proraterec.clear();
	}


}