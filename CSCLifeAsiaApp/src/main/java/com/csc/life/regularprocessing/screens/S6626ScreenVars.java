package com.csc.life.regularprocessing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6626
 * @version 1.0 generated on 30/08/09 06:53
 * @author Quipoz
 */
public class S6626ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(528);
	public FixedLengthStringData dataFields = new FixedLengthStringData(128).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData transcds = new FixedLengthStringData(40).isAPartOf(dataFields, 44);
	public FixedLengthStringData[] transcd = FLSArrayPartOfStructure(10, 4, transcds, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(transcds, 0, FILLER_REDEFINE);
	public FixedLengthStringData transcd01 = DD.transcd.copy().isAPartOf(filler,0);
	public FixedLengthStringData transcd02 = DD.transcd.copy().isAPartOf(filler,4);
	public FixedLengthStringData transcd03 = DD.transcd.copy().isAPartOf(filler,8);
	public FixedLengthStringData transcd04 = DD.transcd.copy().isAPartOf(filler,12);
	public FixedLengthStringData transcd05 = DD.transcd.copy().isAPartOf(filler,16);
	public FixedLengthStringData transcd06 = DD.transcd.copy().isAPartOf(filler,20);
	public FixedLengthStringData transcd07 = DD.transcd.copy().isAPartOf(filler,24);
	public FixedLengthStringData transcd08 = DD.transcd.copy().isAPartOf(filler,28);
	public FixedLengthStringData transcd09 = DD.transcd.copy().isAPartOf(filler,32);
	public FixedLengthStringData transcd10 = DD.transcd.copy().isAPartOf(filler,36);
	public FixedLengthStringData trcode = DD.trcode.copy().isAPartOf(dataFields,84);
	public FixedLengthStringData trncds = new FixedLengthStringData(40).isAPartOf(dataFields, 88);
	public FixedLengthStringData[] trncd = FLSArrayPartOfStructure(10, 4, trncds, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(40).isAPartOf(trncds, 0, FILLER_REDEFINE);
	public FixedLengthStringData trncd01 = DD.trncd.copy().isAPartOf(filler1,0);
	public FixedLengthStringData trncd02 = DD.trncd.copy().isAPartOf(filler1,4);
	public FixedLengthStringData trncd03 = DD.trncd.copy().isAPartOf(filler1,8);
	public FixedLengthStringData trncd04 = DD.trncd.copy().isAPartOf(filler1,12);
	public FixedLengthStringData trncd05 = DD.trncd.copy().isAPartOf(filler1,16);
	public FixedLengthStringData trncd06 = DD.trncd.copy().isAPartOf(filler1,20);
	public FixedLengthStringData trncd07 = DD.trncd.copy().isAPartOf(filler1,24);
	public FixedLengthStringData trncd08 = DD.trncd.copy().isAPartOf(filler1,28);
	public FixedLengthStringData trncd09 = DD.trncd.copy().isAPartOf(filler1,32);
	public FixedLengthStringData trncd10 = DD.trncd.copy().isAPartOf(filler1,36);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(100).isAPartOf(dataArea, 128);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData transcdsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData[] transcdErr = FLSArrayPartOfStructure(10, 4, transcdsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(transcdsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData transcd01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData transcd02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData transcd03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData transcd04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData transcd05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData transcd06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData transcd07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData transcd08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData transcd09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData transcd10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData trcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData trncdsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData[] trncdErr = FLSArrayPartOfStructure(10, 4, trncdsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(trncdsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData trncd01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData trncd02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData trncd03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData trncd04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData trncd05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData trncd06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData trncd07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData trncd08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData trncd09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData trncd10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(300).isAPartOf(dataArea, 228);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData transcdsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] transcdOut = FLSArrayPartOfStructure(10, 12, transcdsOut, 0);
	public FixedLengthStringData[][] transcdO = FLSDArrayPartOfArrayStructure(12, 1, transcdOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(120).isAPartOf(transcdsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] transcd01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] transcd02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] transcd03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] transcd04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] transcd05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] transcd06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] transcd07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] transcd08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] transcd09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] transcd10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData[] trcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData trncdsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 180);
	public FixedLengthStringData[] trncdOut = FLSArrayPartOfStructure(10, 12, trncdsOut, 0);
	public FixedLengthStringData[][] trncdO = FLSDArrayPartOfArrayStructure(12, 1, trncdOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(120).isAPartOf(trncdsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] trncd01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] trncd02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] trncd03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] trncd04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] trncd05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] trncd06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] trncd07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] trncd08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] trncd09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] trncd10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6626screenWritten = new LongData(0);
	public LongData S6626protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6626ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(transcd01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(transcd06Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(transcd07Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(transcd08Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(transcd09Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(transcd10Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(transcd02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(transcd03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(transcd04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(transcd05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trcodeOut,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trncd01Out,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trncd02Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trncd03Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trncd04Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trncd05Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trncd06Out,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trncd07Out,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trncd08Out,new String[] {"32",null, "-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trncd09Out,new String[] {"33",null, "-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trncd10Out,new String[] {"34",null, "-34",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, transcd01, transcd06, transcd07, transcd08, transcd09, transcd10, transcd02, transcd03, transcd04, transcd05, trcode, trncd01, trncd02, trncd03, trncd04, trncd05, trncd06, trncd07, trncd08, trncd09, trncd10};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, transcd01Out, transcd06Out, transcd07Out, transcd08Out, transcd09Out, transcd10Out, transcd02Out, transcd03Out, transcd04Out, transcd05Out, trcodeOut, trncd01Out, trncd02Out, trncd03Out, trncd04Out, trncd05Out, trncd06Out, trncd07Out, trncd08Out, trncd09Out, trncd10Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, transcd01Err, transcd06Err, transcd07Err, transcd08Err, transcd09Err, transcd10Err, transcd02Err, transcd03Err, transcd04Err, transcd05Err, trcodeErr, trncd01Err, trncd02Err, trncd03Err, trncd04Err, trncd05Err, trncd06Err, trncd07Err, trncd08Err, trncd09Err, trncd10Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6626screen.class;
		protectRecord = S6626protect.class;
	}

}
