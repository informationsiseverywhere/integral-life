/*
 * File: Nfbbill.java
 * Date: 29 August 2009 23:00:08
 * Author: Quipoz Limited
 * 
 * Class transformed from NFBBILL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*           NON-FORFEITURE ROUTINE FOR BENEFIT BILLED RIDERS
*
*         This subroutine has been created specifically for use
*         by benefit billed riders.  It decides whether to lapse
*         the rider according to the status of its "owning"
*         coverage.
*
*         This subroutine will read COVR file of its owning
*         coverage, using parameters passed in the linkage
*         OVRDUEREC.  It will check to see if that coverage
*         is lapsed.  If it is lapsed, it will return an
*         OVRD-STATUZ of O-K else OVRD-STATUZ will be OMIT.
*         According to the statuz returned, the calling program
*         will lapse or leave unchanged the status of the rider.
*
****************************************************************
* </pre>
*/
public class Nfbbill extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "NFBBILL";
		/* TABLES */
	private String t5679 = "T5679";
	private String itemrec = "ITEMREC   ";
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Ovrduerec ovrduerec = new Ovrduerec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5679rec t5679rec = new T5679rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit190
	}

	public Nfbbill() {
		super();
	}

public void mainline(Object... parmArray)
	{
		ovrduerec.ovrdueRec = convertAndSetParam(ovrduerec.ovrdueRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		try {
			readCovr110();
		}
		catch (GOTOException e){
		}
		finally{
			exit190();
		}
	}

protected void readCovr110()
	{
		syserrrec.subrname.set(wsaaSubr);
		ovrduerec.statuz.set(varcom.oK);
		covrmjaIO.setStatuz(SPACES);
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(ovrduerec.chdrcoy);
		covrmjaIO.setChdrnum(ovrduerec.chdrnum);
		covrmjaIO.setLife(ovrduerec.life);
		covrmjaIO.setCoverage(ovrduerec.coverage);
		covrmjaIO.setRider("00");
		covrmjaIO.setPlanSuffix(ovrduerec.planSuffix);
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			ovrduerec.statuz.set(varcom.bomb);
			goTo(GotoLabel.exit190);
		}
		readT56791000();
		if (isEQ(covrmjaIO.getPstatcode(),t5679rec.setCovRiskStat)) {
			ovrduerec.statuz.set(varcom.oK);
		}
		else {
			ovrduerec.statuz.set("OMIT");
		}
	}

protected void exit190()
	{
		exitProgram();
	}

protected void readT56791000()
	{
		start1010();
	}

protected void start1010()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ovrduerec.chdrcoy);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(ovrduerec.trancode);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			ovrduerec.statuz.set(varcom.bomb);
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}
}
