package com.csc.life.regularprocessing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:26
 * Description:
 * Copybook name: T6626REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6626rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6626Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData transcds = new FixedLengthStringData(40).isAPartOf(t6626Rec, 0);
  	public FixedLengthStringData[] transcd = FLSArrayPartOfStructure(10, 4, transcds, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(transcds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData transcd01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData transcd02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData transcd03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData transcd04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData transcd05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData transcd06 = new FixedLengthStringData(4).isAPartOf(filler, 20);
  	public FixedLengthStringData transcd07 = new FixedLengthStringData(4).isAPartOf(filler, 24);
  	public FixedLengthStringData transcd08 = new FixedLengthStringData(4).isAPartOf(filler, 28);
  	public FixedLengthStringData transcd09 = new FixedLengthStringData(4).isAPartOf(filler, 32);
  	public FixedLengthStringData transcd10 = new FixedLengthStringData(4).isAPartOf(filler, 36);
  	public FixedLengthStringData trcode = new FixedLengthStringData(4).isAPartOf(t6626Rec, 40);
  	public FixedLengthStringData trncds = new FixedLengthStringData(40).isAPartOf(t6626Rec, 44);
  	public FixedLengthStringData[] trncd = FLSArrayPartOfStructure(10, 4, trncds, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(40).isAPartOf(trncds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData trncd01 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
  	public FixedLengthStringData trncd02 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
  	public FixedLengthStringData trncd03 = new FixedLengthStringData(4).isAPartOf(filler1, 8);
  	public FixedLengthStringData trncd04 = new FixedLengthStringData(4).isAPartOf(filler1, 12);
  	public FixedLengthStringData trncd05 = new FixedLengthStringData(4).isAPartOf(filler1, 16);
  	public FixedLengthStringData trncd06 = new FixedLengthStringData(4).isAPartOf(filler1, 20);
  	public FixedLengthStringData trncd07 = new FixedLengthStringData(4).isAPartOf(filler1, 24);
  	public FixedLengthStringData trncd08 = new FixedLengthStringData(4).isAPartOf(filler1, 28);
  	public FixedLengthStringData trncd09 = new FixedLengthStringData(4).isAPartOf(filler1, 32);
  	public FixedLengthStringData trncd10 = new FixedLengthStringData(4).isAPartOf(filler1, 36);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(416).isAPartOf(t6626Rec, 84, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6626Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6626Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}