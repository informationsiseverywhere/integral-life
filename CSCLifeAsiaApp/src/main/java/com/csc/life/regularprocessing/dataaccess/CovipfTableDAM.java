package com.csc.life.regularprocessing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CovipfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:33
 * Class transformed from COVIPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CovipfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 94;
	public FixedLengthStringData covirec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData covipfRecord = covirec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(covirec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(covirec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(covirec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(covirec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(covirec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(covirec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(covirec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(covirec);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(covirec);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(covirec);
	public PackedDecimalData crrcd = DD.crrcd.copy().isAPartOf(covirec);
	public FixedLengthStringData premCurrency = DD.prmcur.copy().isAPartOf(covirec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(covirec);
	public PackedDecimalData singp = DD.singp.copy().isAPartOf(covirec);
	public PackedDecimalData riskCessDate = DD.rcesdte.copy().isAPartOf(covirec);
	public PackedDecimalData sumins = DD.sumins.copy().isAPartOf(covirec);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(covirec);
	public FixedLengthStringData indexationInd = DD.indxin.copy().isAPartOf(covirec);
	public PackedDecimalData instprem = DD.instprem.copy().isAPartOf(covirec);
	public PackedDecimalData zbinstprem = DD.zbinstprem.copy().isAPartOf(covirec);
	public PackedDecimalData zlinstprem = DD.zlinstprem.copy().isAPartOf(covirec);
	public PackedDecimalData cpiDate = DD.cpidte.copy().isAPartOf(covirec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public CovipfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for CovipfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public CovipfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for CovipfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public CovipfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for CovipfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public CovipfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("COVIPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"VALIDFLAG, " +
							"STATCODE, " +
							"PSTATCODE, " +
							"CRRCD, " +
							"PRMCUR, " +
							"CRTABLE, " +
							"SINGP, " +
							"RCESDTE, " +
							"SUMINS, " +
							"MORTCLS, " +
							"INDXIN, " +
							"INSTPREM, " +
							"ZBINSTPREM, " +
							"ZLINSTPREM, " +
							"CPIDTE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     validflag,
                                     statcode,
                                     pstatcode,
                                     crrcd,
                                     premCurrency,
                                     crtable,
                                     singp,
                                     riskCessDate,
                                     sumins,
                                     mortcls,
                                     indexationInd,
                                     instprem,
                                     zbinstprem,
                                     zlinstprem,
                                     cpiDate,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		validflag.clear();
  		statcode.clear();
  		pstatcode.clear();
  		crrcd.clear();
  		premCurrency.clear();
  		crtable.clear();
  		singp.clear();
  		riskCessDate.clear();
  		sumins.clear();
  		mortcls.clear();
  		indexationInd.clear();
  		instprem.clear();
  		zbinstprem.clear();
  		zlinstprem.clear();
  		cpiDate.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getCovirec() {
  		return covirec;
	}

	public FixedLengthStringData getCovipfRecord() {
  		return covipfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setCovirec(what);
	}

	public void setCovirec(Object what) {
  		this.covirec.set(what);
	}

	public void setCovipfRecord(Object what) {
  		this.covipfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(covirec.getLength());
		result.set(covirec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}