/*
 * File: B6528.java
 * Date: 29 August 2009 21:22:48
 * Author: Quipoz Limited
 *
 * Class transformed from B6528.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.regularprocessing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.dao.ZfmcpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.interestbearing.dataaccess.dao.HitspfDAO;
import com.csc.life.interestbearing.dataaccess.model.Hitspf;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.HitrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.dao.VprcpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Hitrpf;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.life.productdefinition.dataaccess.model.Vprcpf;
import com.csc.life.productdefinition.procedures.Vpuubbl;
import com.csc.life.productdefinition.procedures.Vpxubbl;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.dataaccess.dao.CovxpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Covxpf;
import com.csc.life.regularprocessing.recordstructures.MgmFeeRec;
import com.csc.life.regularprocessing.recordstructures.Ubblallpar;
import com.csc.life.terminationclaims.recordstructures.Vpxubblrec;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZrstpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zrstpf;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Contot;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.model.Zfmcpf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 *
 * REMARKS.
 * 
 *  Benefit Billing Process Program.
 *  --------------------------------
 * 
 *  This program is the second  half  of  the  Life/400  Benefit
 *  Billing Batch Subsystem.
 * 
 *  The  first  program  is  the  Splitter  program  B6527.  The
 *  Splitter will have searched through the database looking for
 *  components (Coverages or Riders (COVRPF)) which are eligible
 *  for Benefit Billing. The COVRs which satisfy  the  selection
 *  criteria are written to the COVX temporary file.
 * 
 *  This  program   will  read  a  COVX  record  and perform the
 *  required processing.
 * 
 *    Step 1: Read a COVX.
 *    Step 2: Softlock the contract. If locked - go to Step 1.
 *    Step 3: Get a transaction number by reading  the  contract
 *            header (CHDR), adding 1 to the transaction  number
 *            and rewriting the updated record.
 *    Step 4: Create a PTRN for the transaction.
 *    Step 5: Call the generic subroutine found by the  Splitter
 *            program from T5534.
 *    Step 6: Read the respective component record (COVR).
 *    Step 7: Calculate the next benefit billing date.
 *    Step 8: Rewrite the component record.
 *    Step 9: If  the  new  BB  date is less than the  effective
 *            date, go to Step 5, otherwise go to Step 1.
 * 
 * 
 *  Control Totals:
 * 
 *  01  Number of COVX Temporary File Records Read
 *  02  Number of COVXs rejected due to Softlocked Policies
 *  03  Number of COVXs processed
 *  04  Contracts processed
 *  05  Contracts rejected - Statuses not on T5679
 *
 *
 *****************************************************************
 * </pre>
 */
public class B6528 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B6528");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	/* COVX parameters */

	private FixedLengthStringData wsaaCovxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaCovxFn, 0, FILLER).init("COVX");
	private FixedLengthStringData wsaaCovxRunid = new FixedLengthStringData(2).isAPartOf(wsaaCovxFn, 4);
	private ZonedDecimalData wsaaCovxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaCovxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init(
			"THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaLastLocked = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData lastChdrnum = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1);
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");

	private FixedLengthStringData wsaaJustCommited = new FixedLengthStringData(1).init("N");
	private Validator justCommited = new Validator(wsaaJustCommited, "Y");
	private PackedDecimalData ix = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).setUnsigned();

	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4);
	private PackedDecimalData wsaaTrandate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaMinfundWrnlt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaMinfundInsuflt = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaIsufLetFlg = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaReadyToTrig = new FixedLengthStringData(1);
	/* V65L16 variables */
	private ZonedDecimalData wsaaADate = new ZonedDecimalData(8, 0).setUnsigned();
	/* NEW-ERRORS */
	private static final String f026 = "F026";
	private static final String e044 = "E044";
	private static final String h021 = "H021";
	private static final String e308 = "E308";
	private static final String h979 = "H979";
	private static final String h791 = "H791";
	/* Storage for T5675 table items. */
	// private static final int wsaaT5675Size = 70;
	// ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5675Size = 1000;

	/* WSAA-T5675-ARRAY */
	private FixedLengthStringData[] wsaaT5675Rec = FLSInittedArray(1000, 11);
	private FixedLengthStringData[] wsaaT5675Key = FLSDArrayPartOfArrayStructure(4, wsaaT5675Rec, 0);
	private FixedLengthStringData[] wsaaT5675Premmeth = FLSDArrayPartOfArrayStructure(4, wsaaT5675Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5675Data = FLSDArrayPartOfArrayStructure(7, wsaaT5675Rec, 4);
	private FixedLengthStringData[] wsaaT5675Premsubr = FLSDArrayPartOfArrayStructure(7, wsaaT5675Data, 0);
	/* Storage for T1688 table items. */
	private static final int wsaaT1688Size = 1000;

	/* WSAA-T1688-ARRAY */
	private FixedLengthStringData[] wsaaT1688Rec = FLSInittedArray(1000, 34);
	private FixedLengthStringData[] wsaaT1688Key = FLSDArrayPartOfArrayStructure(4, wsaaT1688Rec, 0);
	private FixedLengthStringData[] wsaaT1688Trcde = FLSDArrayPartOfArrayStructure(4, wsaaT1688Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT1688Data = FLSDArrayPartOfArrayStructure(30, wsaaT1688Rec, 4);
	private FixedLengthStringData[] wsaaT1688Longdesc = FLSDArrayPartOfArrayStructure(30, wsaaT1688Data, 0);
	/* Storage for T6598 table items. */
	// private static final int wsaaT6598Size = 200;
	// ILIFE-2628 fixed--Array size increased
	private static final int wsaaT6598Size = 1000;

	/* WSAA-T6598-ARRAY */
	private FixedLengthStringData[] wsaaT6598Rec = FLSInittedArray(1000, 11);
	private FixedLengthStringData[] wsaaT6598Key = FLSDArrayPartOfArrayStructure(4, wsaaT6598Rec, 0);
	private FixedLengthStringData[] wsaaT6598Calcmeth = FLSDArrayPartOfArrayStructure(4, wsaaT6598Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT6598Data = FLSDArrayPartOfArrayStructure(7, wsaaT6598Rec, 4);
	private FixedLengthStringData[] wsaaT6598Calcprog = FLSDArrayPartOfArrayStructure(7, wsaaT6598Data, 0);

	/* Storage for T5688 table items. */
	private FixedLengthStringData wsaaDateFound = new FixedLengthStringData(2).init("N");
	private Validator dateFound = new Validator(wsaaDateFound, "Y");
	// private static final int wsaaT5688Size = 70;
	// ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5688Size = 1000;

	/* WSAA-T5688-ARRAY */
	private FixedLengthStringData[] wsaaT5688Rec = FLSInittedArray(1000, 10);
	private FixedLengthStringData[] wsaaT5688Key = FLSDArrayPartOfArrayStructure(3, wsaaT5688Rec, 0);
	private FixedLengthStringData[] wsaaT5688Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5688Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5688Data = FLSDArrayPartOfArrayStructure(7, wsaaT5688Rec, 3);
	private PackedDecimalData[] wsaaT5688Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5688Data, 0);
	private FixedLengthStringData[] wsaaT5688Comlvlacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 5);
	private FixedLengthStringData[] wsaaT5688Revacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 6);
	private ZonedDecimalData wsaaOccdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(wsaaOccdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaOccMm = new ZonedDecimalData(2, 0).isAPartOf(filler3, 4).setUnsigned();
	private ZonedDecimalData wsaaOccDd = new ZonedDecimalData(2, 0).isAPartOf(filler3, 6).setUnsigned();
	/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String e101 = "E101";
	/* TABLES */
	private static final String t5679 = "T5679";
	private FixedLengthStringData t5675 = new FixedLengthStringData(6).init("T5675");
	private FixedLengthStringData t1688 = new FixedLengthStringData(6).init("T1688");
	private FixedLengthStringData t6598 = new FixedLengthStringData(6).init("T6598");
	private FixedLengthStringData t5688 = new FixedLengthStringData(6).init("T5688");
	/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);

	private ItemTableDAM itemIO = new ItemTableDAM();

	private Datcon4rec datcon4rec = new Datcon4rec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private T5679rec t5679rec = new T5679rec();
	private T5675rec t5675rec = new T5675rec();
	private T6598rec t6598rec = new T6598rec();
	private T5688rec t5688rec = new T5688rec();
	private Th506rec th506rec = new Th506rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Ubblallpar ubblallpar = new Ubblallpar();
	private MgmFeeRec mgmFeeRec =new MgmFeeRec();
	private FormatsInner formatsInner = new FormatsInner();
	private ExternalisedRules er = new ExternalisedRules();

	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private CovxpfDAO covxpfDAO = getApplicationContext().getBean("covxpfDAO", CovxpfDAO.class);
	private UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO", UtrspfDAO.class);
	private VprcpfDAO vprcpfDAO = getApplicationContext().getBean("vprcpfDAO", VprcpfDAO.class);
	private HitspfDAO hitspfDAO = getApplicationContext().getBean("hitspfDAO", HitspfDAO.class);
	private ZrstpfDAO zrstpfDAO = getApplicationContext().getBean("zrstpfDAO", ZrstpfDAO.class);
	private UtrnpfDAO utrnpfDAO = getApplicationContext().getBean("utrnpfDAO", UtrnpfDAO.class);
	private HitrpfDAO hitrpfDAO = getApplicationContext().getBean("hitrpfDAO", HitrpfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	
	private BigDecimal wsaaLastcoi = BigDecimal.ZERO;
	private BigDecimal wsaaFundval = BigDecimal.ZERO;
	private BigDecimal wsaaTempval = BigDecimal.ZERO;
	private Covxpf covxpfRec;
/*	private Covrpf covrIO;*/
	private Covrpf covrIO = new Covrpf();  /*ILIFE-4733*/
	private int intBatchID = 0;
	private int intBatchExtractSize;
	private int wsaaT5675Ix = 1;
	private int wsaaT1688Ix = 1;
	private int wsaaT6598Ix = 1;
	private int wsaaT5688Ix = 1;
	private Iterator<Covxpf> iteratorList;
	private Map<String, List<Itempf>> t5675Map = null;
	private Map<String, Descpf> t1688DescMap = null;
	private Map<String, List<Itempf>> t6598Map = null;
	private Map<String, List<Itempf>> t5688Map = null;
	private Map<String, List<Itempf>> th506Map = null;
	private Map<String, List<Itempf>> tr384Map = null;
	private Map<String, List<Utrspf>> utrsMap = null;
	private Map<String, List<Vprcpf>> vprnudlMap = null;
	private Map<String, List<Hitspf>> hitsMap = null;
	private Map<String, List<Zrstpf>> zrstMap = null;
	private Map<String, List<Zrstpf>> zrsttraMap = null;
	private Map<String, List<Chdrpf>> chdrlifMap;
	private Map<String, List<Covrpf>> covrMap;
	private List<String> utrnrnlList = null;
	private List<String> hitrrnlList = null;
	private List<Covrpf> covrpfList = null;
	private Chdrpf chdrlifIO;
	private List<Chdrpf> updateChdrpfList;
	private List<Ptrnpf> insertPtrnpfList;
	private List<Covrpf> updateCovrpfList;
	private int ct01Value = 0;
	private int ct02Value = 0;
	private int ct03Value = 0;
	private int ct04Value = 0;
	private int ct05Value = 0;
	private Boolean flag = Boolean.FALSE;//ILIFE-6288
	private List<Utrspf> utrspf=null;
	private static final String  MGMFEE_ID="CSULK003";
	private boolean mgmFeeFlag=false;
	private boolean advisorFeeCalc=false;
	private Map<String, Integer> chdrBtDateMap;
	protected FixedLengthStringData wsaaEdterror = new FixedLengthStringData(4);
	private static final String  FMC_FLAG="BTPRO026";
	private boolean fmcFlag=false;
	private ZfmcpfDAO zfmcpfDAO = getApplicationContext().getBean("zfmcpfDAO", ZfmcpfDAO.class);
	private Zfmcpf zfmcpf = new Zfmcpf();
	private String wsaaPtrnWritten="N";
	private static final String FMC = "FMC";
	private List<Hitspf> hitspfList = new ArrayList<Hitspf>();	//IBPLIFE-1937
	private Map<String, List<Utrnpf>> utrnuddMap = null;
	private Map<String, List<Hitrpf>> hitraloMap = null;
	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, readT56751120, readT16881220, readT65981320, readT56881420, eof2080, exit2090, setCtrlBreak3080, exit3090, exit4190, read5520, read5120, read5220, continue15230, read25240, exit5290, continue6250, exit6290, continue6550, exit6590
	}

	public B6528() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}

	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}

	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray) {
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void restart0900() {
		/* RESTART */
		/** This program may deal with many COVX records for the same */
		/** contract. Thus to avoid repetitive processing of the contract */
		/** level details we use a control break based upon contact number. */
		/** As we do not need to store any running totals this does not */
		/** cause too many problems. */
		/** Should this program fail, it will leave the last contract we */
		/** were attempting to process in a locked state. Thus when we */
		/** restart, the contract will be skipped and we will start */
		/** on the next available one. */
		/* EXIT */
	}

	protected void initialise1000() {
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(), "3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		insertPtrnpfList = new ArrayList<Ptrnpf>();
		/* Point to correct member of COVXPF. */
		wsaaCovxRunid.set(bprdIO.getSystemParam04());
		wsaaCovxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		varcom.vrcmDate.set(getCobolDate());

		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				intBatchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
		advisorFeeCalc = FeaConfg.isFeatureExist(bprdIO.getCompany().toString(), "CSULK004", appVars, "IT");
		fmcFlag = FeaConfg.isFeatureExist(bprdIO.getCompany().toString(), FMC_FLAG, appVars, "IT");
		readChunkRecord();

		/* Read T5679 for valid statii. */
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bprdIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());

		String coy = bsprIO.getCompany().toString();

		t5675Map = itemDAO.loadSmartTable("IT", coy, "T5675");

		loadT56751100();

		t1688DescMap = descDAO.getItems("IT", coy, "T1688", bsscIO.getLanguage().toString());
		loadT16881200();

		t6598Map = itemDAO.loadSmartTable("IT", coy, "T6598");
		loadT65981300();

		t5688Map = itemDAO.loadSmartTable("IT", coy, "T5688");
		loadT56881400();

		th506Map = itemDAO.loadSmartTable("IT", coy, "TH506");
		tr384Map = itemDAO.loadSmartTable("IT", coy, "TR384");
	}

	private void readChunkRecord() {
		List<Covxpf> covxpfList = covxpfDAO.searchCovxRecord(wsaaCovxFn.toString(), wsaaThreadMember.toString(),
				intBatchExtractSize, intBatchID);
		iteratorList = covxpfList.iterator();
		if (!covxpfList.isEmpty()) {
			List<String> chdrnumSet = new ArrayList<>();
			for (Covxpf h : covxpfList) {
				chdrnumSet.add(h.getChdrnum());
			}
			String coy = bsprIO.getCompany().toString();
			if(!chdrnumSet.isEmpty()){
				utrsMap = utrspfDAO.searchUtrsRecord(coy, chdrnumSet); }
			if (utrsMap != null && !utrsMap.isEmpty()) {
				List<String> vrtfndSet = new ArrayList<>();
				for (List<Utrspf> usList : utrsMap.values()) {
					for (Utrspf us : usList) {
						vrtfndSet.add(us.getUnitVirtualFund());
					}
				}
				vprnudlMap = vprcpfDAO.searchVprnudlRecord(bsscIO.getEffectiveDate().toInt(), vrtfndSet, coy);
			}
			if(!chdrnumSet.isEmpty()){
				zrstMap = zrstpfDAO.searchZrstRecordByChdrnum(chdrnumSet, coy);
				hitsMap = hitspfDAO.searchHitsRecordByChdrcoy(coy, chdrnumSet);
				zrsttraMap = zrstpfDAO.searchZrsttraRecordByChdrnum(chdrnumSet, coy);
				utrnrnlList = utrnpfDAO.checkUtrnrnlRecordByChdrnum(coy, chdrnumSet);
				hitrrnlList = hitrpfDAO.checkHitrrnlRecordByChdrnum(coy, chdrnumSet);

				chdrlifMap = chdrpfDAO.searchChdrRecordByChdrnum(chdrnumSet);
				covrMap = covrpfDAO.searchCovrBenbilldate(coy,chdrnumSet);
				Covrpf tempcovrpf = new Covrpf();
				tempcovrpf.setChdrcoy(coy);
				tempcovrpf.setChdrnum(covxpfList.get(0).getChdrnum());
				covrpfList = covrpfDAO.searchCovrRecordForContract(tempcovrpf);
				if(advisorFeeCalc)
				{					
					chdrBtDateMap = getApplicationContext().getBean("linspfDAO",LinspfDAO.class).searchTdayBillCDContractNos(chdrnumSet,coy);
				}
				if(fmcFlag && isEQ(bprdIO.getSystemParam05(),FMC)){
					utrnuddMap = utrnpfDAO.searchUtrnuddRecord(chdrnumSet);
					hitraloMap = hitrpfDAO.searchHitrRecordByChdrnum(chdrnumSet);
				}
			}
		}
	}

	protected void loadT56751100() {
		wsaaT5675Ix = 1;
		if (t5675Map != null && !t5675Map.isEmpty()) {
			for (List<Itempf> itempfList : t5675Map.values()) {
				for (Itempf i : itempfList) {
					if (isGT(wsaaT5675Ix, wsaaT5675Size)) {
						syserrrec.params.set(t5675);
						syserrrec.statuz.set(h791);
						fatalError600();
					}
					t5675rec.t5675Rec.set(StringUtil.rawToString(i.getGenarea()));
					wsaaT5675Premmeth[wsaaT5675Ix].set(i.getItemitem());
					wsaaT5675Premsubr[wsaaT5675Ix].set(t5675rec.premsubr);
					wsaaT5675Ix++;
				}
			}
		}
	}

	protected void loadT16881200() {
		wsaaT1688Ix = 1;
		if (t1688DescMap != null && !t1688DescMap.isEmpty()) {
			for (Descpf desc : t1688DescMap.values()) {
				if (isGT(wsaaT1688Ix, wsaaT1688Size)) {
					syserrrec.params.set(t1688);
					syserrrec.statuz.set(h791);
					fatalError600();
				}
				wsaaT1688Trcde[wsaaT1688Ix].set(desc.getDescitem());
				wsaaT1688Longdesc[wsaaT1688Ix].set(desc.getLongdesc());
				wsaaT1688Ix++;
			}
		}
	}

	protected void loadT65981300() {
		wsaaT6598Ix = 1;
		if (t6598Map != null && !t6598Map.isEmpty()) {
			for (List<Itempf> itempfList : t6598Map.values()) {
				for (Itempf i : itempfList) {
					if (isGT(wsaaT6598Ix, wsaaT6598Size)) {
						syserrrec.params.set(t6598);
						syserrrec.statuz.set(h791);
						fatalError600();
					}
					t6598rec.t6598Rec.set(StringUtil.rawToString(i.getGenarea()));
					wsaaT6598Calcmeth[wsaaT6598Ix].set(i.getItemitem());
					wsaaT6598Calcprog[wsaaT6598Ix].set(t6598rec.calcprog);
					wsaaT6598Ix++;
				}
			}
		}
	}

	protected void loadT56881400() {
		wsaaT5688Ix = 1;
		if (t5688Map != null && !t5688Map.isEmpty()) {
			for (List<Itempf> itempfList : t5688Map.values()) {
				for (Itempf i : itempfList) {
					if (isGT(wsaaT5688Ix, wsaaT5688Size)) {
						syserrrec.params.set(t5688);
						syserrrec.statuz.set(h791);
						fatalError600();
					}
					t5688rec.t5688Rec.set(StringUtil.rawToString(i.getGenarea()));
					wsaaT5688Itmfrm[wsaaT5688Ix].set(i.getItmfrm());
					wsaaT5688Cnttype[wsaaT5688Ix].set(i.getItemitem());
					wsaaT5688Comlvlacc[wsaaT5688Ix].set(t5688rec.comlvlacc);
					wsaaT5688Revacc[wsaaT5688Ix].set(t5688rec.revacc);
					wsaaT5688Ix++;
				}
			}
		}
	}

	protected void readFile2000() {
		if (iteratorList != null && iteratorList.hasNext()) {
			covxpfRec = iteratorList.next();
			ct01Value++;
		} else {
			intBatchID++;
			clearList2100();
			readChunkRecord();
			if (iteratorList.hasNext()) {
				covxpfRec = iteratorList.next();
				ct01Value++;
			} else {
				wsspEdterror.set(varcom.endp);
				eof2080();
			}
		}
	}

	private void clearList2100() {
		if (utrsMap != null)
			utrsMap.clear();
		if (vprnudlMap != null)
			vprnudlMap.clear();
		if (zrstMap != null)
			zrstMap.clear();
		if (hitsMap != null)
			hitsMap.clear();
		if (zrsttraMap != null)
			zrsttraMap.clear();
		if (utrnrnlList != null)
			utrnrnlList.clear();
		if (hitrrnlList != null)
			hitrrnlList.clear();
		if (chdrlifMap != null)
			chdrlifMap.clear();
		if (covrMap != null)
			covrMap.clear();
		if(chdrBtDateMap != null)
			chdrBtDateMap.clear();
		iteratorList = null;
	}

	protected void eof2080() {
		if (validStatus.isTrue()) {
			triggerAllLetter4100();
		}
	}

	protected void edit2500() {
		wsspEdterror.set(varcom.oK);
		if(fmcFlag && isEQ(bprdIO.getSystemParam05(),FMC)){
			if (utrnuddMap != null && utrnuddMap.containsKey(covxpfRec.getChdrnum())) {
				List<Utrnpf> utrnpfList = utrnuddMap.get(covxpfRec.getChdrnum());
				for (Utrnpf u : utrnpfList) {
					if (u.getChdrcoy().equals(covxpfRec.getChdrcoy()) && u.getLife().equals(covxpfRec.getLife())
							&& u.getCoverage().equals(covxpfRec.getCoverage()) && u.getRider().equals(covxpfRec.getRider())
							&& u.getPlanSuffix() == covxpfRec.getPlanSuffix()) {
						wsspEdterror.set(SPACES);
						return;
					}
				}
			}
			if (hitraloMap != null && hitraloMap.containsKey(covxpfRec.getChdrnum())) {
				List<Hitrpf> hitrList = hitraloMap.get(covxpfRec.getChdrnum());
				for (Hitrpf h : hitrList) {
					if (h.getChdrcoy().equals(covxpfRec.getChdrcoy()) && h.getChdrnum().equals(covxpfRec.getChdrnum())
							&& h.getLife().equals(covxpfRec.getLife()) && h.getCoverage().equals(covxpfRec.getCoverage())
							&& h.getRider().equals(covxpfRec.getRider()) && h.getPlanSuffix() == covxpfRec.getPlanSuffix()) {
						wsspEdterror.set(SPACES);
						return;
					}
				}
			}
		}
		if (isNE(covxpfRec.getChdrnum(), lastChdrnum)) {
			softlockPolicy2600();
			if (isEQ(sftlockrec.statuz, "LOCK")) {
				if (isNE(covxpfRec.getChdrnum(), wsaaLastLocked)) {
					wsaaPlnsfx.set(covxpfRec.getPlanSuffix());
					StringUtil stringVariable1 = new StringUtil();
					stringVariable1.addExpression(covxpfRec.getChdrcoy());
					stringVariable1.addExpression("|");
					stringVariable1.addExpression(covxpfRec.getChdrnum());
					stringVariable1.addExpression("|");
					stringVariable1.addExpression(covxpfRec.getLife());
					stringVariable1.addExpression("|");
					stringVariable1.addExpression(covxpfRec.getCoverage());
					stringVariable1.addExpression("|");
					stringVariable1.addExpression(covxpfRec.getRider());
					stringVariable1.addExpression("|");
					stringVariable1.addExpression(wsaaPlnsfx);
					stringVariable1.setStringInto(conlogrec.params);
					/* MOVE SFTL-STATUZ TO CONL-ERROR */
					conlogrec.error.set(e101);
					callConlog003();
				}
				wsaaLastLocked.set(covxpfRec.getChdrnum());
				ct02Value++;
				wsspEdterror.set(SPACES);
				return;
			}
		}
	}

	protected void softlockPolicy2600() {
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(covxpfRec.getChdrcoy());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(covxpfRec.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(ZERO);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK) && isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, varcom.oK)) {//ILIFE-6288
			flag = Boolean.TRUE;
		}									     //ILIFE-6288
	}

	protected void update3000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					update3010();
				case setCtrlBreak3080:
					setCtrlBreak3080();
				case exit3090:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void update3010() {
		/* The splitter program may have extracted many COVRs for the */
		/* same contract. To minimise the repetivite processing this */
		/* program uses a control break on CHDRNUM. A chdrnum of spaces */
		/* means we are on the first record. */
		if (isNE(covxpfRec.getChdrnum(), lastChdrnum)) {
			if (isNE(lastChdrnum, SPACES)) {
				if (validStatus.isTrue()) {
					triggerAllLetter4100();
				}
				//unlockLastContract3900();  //ILIFE-6288
			}
			readhChdr3100();
			chdrStatusCheck3200();
			if(fmcFlag) {
				if(isNE(bprdIO.getSystemParam05(),FMC))
					return;
				else{
					List<Utrspf> utrspfList = utrspfDAO.searchUtrsRecord(covxpfRec.getChdrcoy().toString().trim(),covxpfRec.getChdrnum().toString().trim());
					if (hitsMap != null && hitsMap.containsKey(covxpfRec.getChdrnum().toString())) {
						hitspfList = hitsMap.get(covxpfRec.getChdrnum().toString());	//IBPLIFE-1937
					}
					if(!utrspfList.isEmpty() || !hitspfList.isEmpty()){
						checkFMCDone6800();
						if (isEQ(wsaaReadyToTrig, "Y")) {
							if (validStatus.isTrue() && isNE(wsaaPtrnWritten, "Y")) {
									rewriteChdr3150();
									writePtrn3400();
									wsaaPtrnWritten = "Y";
								}
							} else {
								goTo(GotoLabel.setCtrlBreak3080);
							}
					} 
					else{
						goTo(GotoLabel.setCtrlBreak3080);
					}
				}
			}
			else{
				if (validStatus.isTrue()) {
					rewriteChdr3150();
					writePtrn3400();
				} else {
					goTo(GotoLabel.setCtrlBreak3080);
				}
			}
		}
		if (!validStatus.isTrue()) {
			goTo(GotoLabel.exit3090);
		}
		ct03Value++;
//		covrIO.setBenBillDate(covxpfRec.getBenBillDate());
		while (!(isGT(covxpfRec.getBenBillDate(), bsscIO.getEffectiveDate()))) {
				if(fmcFlag && isEQ(wsaaReadyToTrig, "N") && isEQ(bprdIO.getSystemParam05(),FMC)) {
					break;
				}
				callT5534Subprog3300();
				readhCovr3600();
				calcNewBbDate3700();
				rewriteCovr3800();
			}
	}
	
	protected void checkFMCDone6800() {
		wsaaReadyToTrig.set("N");
		readZfmcchd6900();
		if (isLT(covxpfRec.getBenBillDate(), zfmcpf.getZfmcdat())) {
			wsaaReadyToTrig.set("Y");
		}
	}
	
	protected void readZfmcchd6900() {
		zfmcpf = zfmcpfDAO.getZfmcRecord(covxpfRec.getChdrcoy(),covxpfRec.getChdrnum());
		if (zfmcpf == null) {
			syserrrec.params.set(covxpfRec.getChdrcoy() + covxpfRec.getChdrnum());
			fatalError600();
		}
	}

	protected void setCtrlBreak3080() {
		lastChdrnum.set(covxpfRec.getChdrnum());
	}

	protected void readhChdr3100() {
		/* READH-CHDR */
		if (chdrlifMap != null && chdrlifMap.containsKey(covxpfRec.getChdrnum())) {
			List<Chdrpf> chdrList = chdrlifMap.get(covxpfRec.getChdrnum());
			for (Chdrpf c : chdrList) {
				if (c.getChdrcoy().toString().equals(covxpfRec.getChdrcoy())) {
					wsaaOccdate.set(c.getOccdate());
					chdrlifIO = c;
					break;
				}
			}
		}
		if (chdrlifIO == null) {
			syserrrec.params.set(covxpfRec.getChdrnum());
			fatalError600();
		}
		/* EXIT */
	}

	protected void rewriteChdr3150() {
		/* REWRITE-CHDR */
		chdrlifIO.setTranno(chdrlifIO.getTranno() + 1);
		if (updateChdrpfList == null) {
			updateChdrpfList = new ArrayList<Chdrpf>();
		}
		updateChdrpfList.add(chdrlifIO);
		/* EXIT */
	}

	protected void chdrStatusCheck3200() {
		/* CHDR-STATUS-CHECK */
		wsaaValidStatus.set("N");
		for (ix.set(1); !(isGT(ix, 12)); ix.add(1)) {
			if (isEQ(t5679rec.cnRiskStat[ix.toInt()], chdrlifIO.getStatcode())) {
				ix.set(13);
				wsaaValidStatus.set("Y");
			}
		}
		if (!validStatus.isTrue()) {
			ct05Value++;
		}
		/* EXIT */
	}

	protected void callT5534Subprog3300() {

		/* Firstly, set up the param record with the values found */
		/* in the splitter program. (B6527) */
		ubblallpar.ubblallRec.set(SPACES);
		ubblallpar.chdrChdrcoy.set(covxpfRec.getChdrcoy());
		ubblallpar.chdrChdrnum.set(covxpfRec.getChdrnum());
		ubblallpar.lifeLife.set(covxpfRec.getLife());
		ubblallpar.lifeJlife.set(covxpfRec.getJlife());
		ubblallpar.covrCoverage.set(covxpfRec.getCoverage());
		ubblallpar.covrRider.set(covxpfRec.getRider());
		ubblallpar.planSuffix.set(covxpfRec.getPlanSuffix());
		ubblallpar.billfreq.set(covxpfRec.getUnitFreq());
		ubblallpar.cntcurr.set(covxpfRec.getPremCurrency());
		/* MOVE BBLDAT TO UBBL-EFFDATE. */
		ubblallpar.effdate.set(covxpfRec.getBenBillDate());
		ubblallpar.premMeth.set(covxpfRec.getPremmeth());
		ubblallpar.jlifePremMeth.set(covxpfRec.getJlPremMeth());
		ubblallpar.sumins.set(covxpfRec.getSumins());
		ubblallpar.premCessDate.set(covxpfRec.getPremCessDate());
		ubblallpar.crtable.set(covxpfRec.getCrtable());
		ubblallpar.mortcls.set(covxpfRec.getMortcls());
		ubblallpar.svMethod.set(covxpfRec.getSvMethod());
		ubblallpar.adfeemth.set(covxpfRec.getAdfeemth());
		/* Other values either available or found in this program. */
		ubblallpar.batccoy.set(batcdorrec.company);
		ubblallpar.batcbrn.set(batcdorrec.branch);
		ubblallpar.batcactmn.set(batcdorrec.actmonth);
		ubblallpar.batcactyr.set(batcdorrec.actyear);
		ubblallpar.batctrcde.set(batcdorrec.trcde);
		ubblallpar.batch.set(batcdorrec.batch);
		ubblallpar.billchnl.set(chdrlifIO.getBillchnl());
		ubblallpar.cnttype.set(chdrlifIO.getCnttype());
		ubblallpar.tranno.set(chdrlifIO.getTranno());
		ubblallpar.polsum.set(chdrlifIO.getPolsum());
		ubblallpar.ptdate.set(chdrlifIO.getPtdate());
		ubblallpar.polinc.set(chdrlifIO.getPolinc());
		ubblallpar.singp.set(ZERO);
		ubblallpar.chdrRegister.set(chdrlifIO.getReg());
		ubblallpar.language.set(bsscIO.getLanguage());
		ubblallpar.user.set(ZERO);
		ubblallpar.function.set("RENL");
		setupNewUbblFields5000();
		setAdvisorFeesRequired();
		if(fmcFlag && isEQ(bprdIO.getSystemParam05(),FMC)){
			ubblallpar.znofdue.set(2);
			ubblallpar.batchrundate.set(bsscIO.getEffectiveDate());
		}
		/* IVE-795 RUL Product - Benefit Billing Calculation started */

		// callProgram(covxpfRec.getSubprog(), ubblallpar.ubblallRec);
		/*ILIFE-7548 Start*/
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(covxpfRec.getSubprog())  && er.isExternalized(ubblallpar.cnttype.toString(), ubblallpar.crtable.toString())))/* IJTI-1523 */
		{
			callProgramX(covxpfRec.getSubprog(), ubblallpar.ubblallRec);//
		}
		/*ILIFE-7548 end*/
		else {
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxubblrec vpxubblrec = new Vpxubblrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(ubblallpar.ubblallRec);
			// ubblallpar.sumins.format(ubblallpar.sumins.toString());
			vpxubblrec.function.set("INIT");
			callProgram(Vpxubbl.class, vpmcalcrec.vpmcalcRec, vpxubblrec);
			ubblallpar.ubblallRec.set(vpmcalcrec.linkageArea);
			vpmfmtrec.initialize();
			vpmfmtrec.date01.set(ubblallpar.effdate);
			vpmfmtrec.previousSum.set(ZERO);
			for(Covrpf c : covrpfList) {
				if(c.getCrtable().equals(covxpfRec.getCrtable())) {
					vpmfmtrec.state.set(c.getZclstate());
					vpmfmtrec.riskComDate.set(c.getCurrfrom());
					}
			}
			mgmFeeFlag = FeaConfg.isFeatureExist(covxpfRec.getChdrcoy().trim(),MGMFEE_ID, appVars, "IT");
			if(mgmFeeFlag){
				initMgmFeeRec();
				callProgram(covxpfRec.getSubprog(), vpmfmtrec, mgmFeeRec, vpxubblrec);
			}
			else{
			callProgram(covxpfRec.getSubprog(), vpmfmtrec, ubblallpar.ubblallRec, vpxubblrec);
			}
			callProgram(Vpuubbl.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);

			if (isEQ(ubblallpar.statuz, SPACES))
				ubblallpar.statuz.set(Varcom.oK);
		}

		/* IVE-795 RUL Product - Benefit Billing Calculation end */
		if (isNE(ubblallpar.statuz, varcom.oK)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression("Sub:", SPACES);
			stringVariable1.addExpression(covxpfRec.getSubprog(), SPACES);
			stringVariable1.addExpression(" Parms: ");
			stringVariable1.addExpression(ubblallpar.ubblallRec);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(ubblallpar.statuz);
			fatalError600();
		}
	
	}

	protected void writePtrn3400() {
		Ptrnpf ptrnIO = new Ptrnpf();
		ptrnIO.setChdrcoy(covxpfRec.getChdrcoy());
		ptrnIO.setChdrnum(covxpfRec.getChdrnum());
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setPtrneff(bsscIO.getEffectiveDate().toInt());
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatccoy(batcdorrec.company.toString());
		ptrnIO.setBatcbrn(batcdorrec.branch.toString());
		ptrnIO.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnIO.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnIO.setBatctrcde(batcdorrec.trcde.toString());
		ptrnIO.setBatcbatch(batcdorrec.batch.toString());
		/*varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		ptrnIO.setTrtm(varcom.vrcmTime.toInt());
		 MOVE BSSC-EFFECTIVE-DATE TO PTRN-TRANSACTION-DATE. 
		ptrnIO.setTrdt(varcom.vrcmDate.toInt());
		*/
		ptrnIO.setTrtm(varcom.vrcmTime.toInt());
		ptrnIO.setTrdt(varcom.vrcmDate.toInt());
		ptrnIO.setUserT(0);
		ptrnIO.setDatesub(bsscIO.getEffectiveDate().toInt());
		ct04Value++;
		insertPtrnpfList.add(ptrnIO);
	}
	//Added by Dennis
	protected void commitPtrnBulkInsert(){	 
		boolean isInsertPtrnPF = ptrnpfDAO.insertPtrnPF(insertPtrnpfList);
		if (!isInsertPtrnPF) fatalError600();
		else insertPtrnpfList.clear();
	}	

	protected void commit3500() {
		/* COMMIT */
		wsaaJustCommited.set("Y");
		commitControlTotals();
		if (updateChdrpfList != null) {
			chdrpfDAO.updateChdrTrannoByUniqueNo(updateChdrpfList);
			updateChdrpfList.clear();
		}
		//Added by Dennis
		commitPtrnBulkInsert();
		if(updateCovrpfList!=null){
			covrpfDAO.updateCovrBillbendateRecord(updateCovrpfList);
			updateCovrpfList.clear();
		}
		if(flag){//ILIFE-6288
			unlockLastContract3900();	
			flag = Boolean.FALSE;
		}       //ILIFE-6288
		/* EXIT */
	}
	
	private void commitControlTotals(){
		contotrec.totno.set(ct01);
		contotrec.totval.set(ct01Value);
		callProgram(Contot.class, contotrec.contotRec);		
		ct01Value = 0;

		contotrec.totno.set(ct02);
		contotrec.totval.set(ct02Value);
		callProgram(Contot.class, contotrec.contotRec);	
		ct02Value = 0;
		
		contotrec.totno.set(ct03);
		contotrec.totval.set(ct03Value);
		callProgram(Contot.class, contotrec.contotRec);	
		ct03Value = 0;
		
		contotrec.totno.set(ct04);
		contotrec.totval.set(ct04Value);
		callProgram(Contot.class, contotrec.contotRec);	
		ct04Value = 0;
		
		contotrec.totno.set(ct05);
		contotrec.totval.set(ct05Value);
		callProgram(Contot.class, contotrec.contotRec);	
		ct05Value = 0;
	}

	protected void rollback3600() {
		/* ROLLBACK */
		/** This section should never be invoked by this program as */
		/** the only valid WSSP-EDTERROR statii are SPACES, O-K, ENDP. */
		/* EXIT */
	}

	protected void readhCovr3600() {
		/* We need to read the Coverage/Rider after the call to the */
		/* generic subroutine as the subroutine will have updated */
		/* the coverage details. */
		if (covrMap != null && covrMap.containsKey(covxpfRec.getChdrnum())) {
			List<Covrpf> covrList = covrMap.get(covxpfRec.getChdrnum());
			for (Covrpf c : covrList) {
				if (c.getChdrcoy().equals(covxpfRec.getChdrcoy()) && c.getLife().equals(covxpfRec.getLife())
						&& c.getCoverage().equals(covxpfRec.getCoverage()) && c.getRider().equals(covxpfRec.getRider())
						&& c.getPlanSuffix() == covxpfRec.getPlanSuffix()) {
					covrIO = c;
				}
			}
		}
		if (covrIO == null) {
			syserrrec.params.set(covxpfRec.getChdrnum());
			fatalError600();
		}
	}


	protected void calcNewBbDate3700() {

		/* Calculate COVR-BEN-BILL-DATE as COVR-BEN-BILL-DATE incremente */
		/* by the deduction frequency from T5534. */
		if (isNE(covxpfRec.getUnitFreq(), NUMERIC)) {
			covxpfRec.setUnitFreq("00");
		}
		/* MOVE UFREQ TO DTC2-FREQUENCY. */
		/* MOVE 1 TO DTC2-FREQ-FACTOR. */
		/* MOVE COVR-BEN-BILL-DATE TO DTC2-INT-DATE-1. */
		/* MOVE 0 TO DTC2-INT-DATE-2. */
		/* CALL 'DATCON2' USING DTC2-DATCON2-REC. */
		/* IF DTC2-STATUZ NOT = O-K */
		/* MOVE DTC2-DATCON2-REC TO SYSR-PARAMS */
		/* MOVE DTC2-STATUZ TO SYSR-STATUZ */
		/* PERFORM 600-FATAL-ERROR. */
		/* MOVE DTC2-INT-DATE-2 TO COVR-BEN-BILL-DATE. */
		datcon4rec.frequency.set(covxpfRec.getUnitFreq());
		datcon4rec.freqFactor.set(1);
		datcon4rec.intDate1.set(covxpfRec.getBenBillDate());
		datcon4rec.intDate2.set(0);
		datcon4rec.billdayNum.set(wsaaOccDd);
		datcon4rec.billmonthNum.set(wsaaOccMm);
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			fatalError600();
		}
		covrIO.setBenBillDate(datcon4rec.intDate2.toInt());
		covxpfRec.setBenBillDate(datcon4rec.intDate2.toInt());
	}

	protected void rewriteCovr3800() {
		/* REWRITE-COVR */
		if (updateCovrpfList == null) {
			updateCovrpfList = new ArrayList<>();
		}
		updateCovrpfList.add(covrIO);
		/* EXIT */
	}

	protected void unlockLastContract3900() {
		/* UNLOCK-LAST-CONTRACT */
		/* The control break logic operates accross commit points. Luckily */
		/* only for softlocks. When MAINB issues a commit, SFTLOCK is */
		/* called with a UALL function which will remove all softlocks */
		/* for the program. Thus when we recieve a MRNF from SFTLOCK we */
		/* check whether MAINB has just issued a COMMIT. If so we ignore */
		/* the error. */
		sftlockrec.entity.set(lastChdrnum);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (justCommited.isTrue() && isEQ(sftlockrec.statuz, varcom.mrnf)) {
			wsaaJustCommited.set("N");
		} else {
			if (isNE(sftlockrec.statuz, varcom.oK)) {
				syserrrec.params.set(sftlockrec.sftlockRec);
				syserrrec.statuz.set(sftlockrec.statuz);
				fatalError600();
			}
		}
		/* EXIT */
	}

	protected void close4000() {
		/* CLOSE-FILES */
		lsaaStatuz.set(varcom.oK);
		/* EXIT */
	}

	protected void triggerAllLetter4100() {
		try {
			start4110();
			triggerInsuffLet4120();
			triggerWarnLet4130();
		} catch (GOTOException e) {
			/* Expected exception for control flow purposes. */
		}
	}

	protected void start4110() {
		wsaaFundval = BigDecimal.ZERO;
		wsaaReadyToTrig.set(SPACES);
		utrsFundval5500();
		hitsFundval5100();
		getLastcoi5200();
		readUtrn6600();
		readHitr6700();
	}

	protected void triggerInsuffLet4120() {
		if (isEQ(wsaaReadyToTrig, "N")) {
			goTo(GotoLabel.exit4190);
		}
		readTh5066000();
		if (isEQ(th506rec.nofbbwrn, ZERO) && isEQ(th506rec.nofbbisf, ZERO)) {
			goTo(GotoLabel.exit4190);
		}
		wsaaMinfundWrnlt.set(ZERO);
		wsaaMinfundInsuflt.set(ZERO);
		if (isNE(th506rec.nofbbisf, ZERO)) {
			insufLetProc6400();
			if (isEQ(wsaaIsufLetFlg, "Y")) {
				goTo(GotoLabel.exit4190);
			}
		}
	}

	protected void triggerWarnLet4130() {
		if (isNE(th506rec.nofbbwrn, ZERO)) {
			warnLetProc6100();
		}
	}

	protected void setupNewUbblFields5000() {

		ubblallpar.occdate.set(chdrlifIO.getOccdate());
		/* Setup T5675 fields */
		wsaaT5675Ix = 1;
		searchlabel1: {
			for (; isLT(wsaaT5675Ix, wsaaT5675Rec.length); wsaaT5675Ix++) {
				if (isEQ(wsaaT5675Key[wsaaT5675Ix], ubblallpar.premMeth)) {
					ubblallpar.premsubr.set(wsaaT5675Premsubr[wsaaT5675Ix]);
					break searchlabel1;
				}
			}
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(covxpfRec.getChdrcoy());
			stringVariable1.addExpression(covxpfRec.getChdrnum());
			stringVariable1.addExpression(covxpfRec.getCrtable());
			stringVariable1.addExpression("-");
			stringVariable1.addExpression(t5675);
			stringVariable1.addExpression(":");
			stringVariable1.addExpression(ubblallpar.premMeth);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(f026);
			fatalError600();
		}
		/* Now search for Join Life */
		wsaaT5675Ix = 1;
		searchlabel2: {
			for (; isLT(wsaaT5675Ix, wsaaT5675Rec.length); wsaaT5675Ix++) {
				if (isEQ(wsaaT5675Key[wsaaT5675Ix], ubblallpar.jlifePremMeth)) {
					ubblallpar.jpremsubr.set(wsaaT5675Premsubr[wsaaT5675Ix]);
					break searchlabel2;
				}
			}
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(covxpfRec.getChdrcoy());
			stringVariable2.addExpression(covxpfRec.getChdrnum());
			stringVariable2.addExpression(covxpfRec.getCrtable());
			stringVariable2.addExpression("-");
			stringVariable2.addExpression(t5675);
			stringVariable2.addExpression(":");
			stringVariable2.addExpression(ubblallpar.jlifePremMeth);
			stringVariable2.setStringInto(syserrrec.params);
			syserrrec.statuz.set(f026);
			fatalError600();
		}
		/* Setup T1688 field */
		wsaaT1688Ix = 1;
		searchlabel3: {
			for (; isLT(wsaaT1688Ix, wsaaT1688Rec.length); wsaaT1688Ix++) {
				if (isEQ(wsaaT1688Key[wsaaT1688Ix], ubblallpar.batctrcde)) {
					ubblallpar.trandesc.set(wsaaT1688Longdesc[wsaaT1688Ix]);
					break searchlabel3;
				}
			}
			StringUtil stringVariable3 = new StringUtil();
			stringVariable3.addExpression(t1688);
			stringVariable3.addExpression(":");
			stringVariable3.addExpression(ubblallpar.batctrcde);
			stringVariable3.setStringInto(syserrrec.params);
			syserrrec.statuz.set(e044);
			fatalError600();
		}
		/* Setup T6598 field */
		wsaaT6598Ix = 1;
		searchlabel4: {
			for (; isLT(wsaaT6598Ix, wsaaT6598Rec.length); wsaaT6598Ix++) {
				if (isEQ(wsaaT6598Key[wsaaT6598Ix], ubblallpar.svMethod)) {
					ubblallpar.svCalcprog.set(wsaaT6598Calcprog[wsaaT6598Ix]);
					break searchlabel4;
				}
			}
			StringUtil stringVariable4 = new StringUtil();
			stringVariable4.addExpression(covxpfRec.getChdrcoy());
			stringVariable4.addExpression(covxpfRec.getChdrnum());
			stringVariable4.addExpression(covxpfRec.getCrtable());
			stringVariable4.addExpression("-");
			stringVariable4.addExpression(t6598);
			stringVariable4.addExpression(":");
			stringVariable4.addExpression(ubblallpar.svMethod);
			stringVariable4.setStringInto(syserrrec.params);
			syserrrec.statuz.set(h021);
			fatalError600();
		}
		/* Setup T5688 field */
		wsaaT5688Ix = 1;
		searchlabel5: {
			for (; isLT(wsaaT5688Ix, wsaaT5688Rec.length); wsaaT5688Ix++) {
				if (isEQ(wsaaT5688Key[wsaaT5688Ix], ubblallpar.cnttype)) {
					break searchlabel5;
				}
			}
			wsaaADate.set(ubblallpar.effdate);
			StringUtil stringVariable5 = new StringUtil();
			stringVariable5.addExpression(covxpfRec.getChdrcoy());
			stringVariable5.addExpression(covxpfRec.getChdrnum());
			stringVariable5.addExpression("-");
			stringVariable5.addExpression(t5688);
			stringVariable5.addExpression(":");
			stringVariable5.addExpression(ubblallpar.cnttype);
			stringVariable5.addExpression("-");
			stringVariable5.addExpression(wsaaADate);
			stringVariable5.setStringInto(syserrrec.params);
			syserrrec.statuz.set(e308);
			fatalError600();
		}
		wsaaDateFound.set("N");
		while (!(isNE(chdrlifIO.getCnttype(), wsaaT5688Cnttype[wsaaT5688Ix]) || isGT(wsaaT5688Ix, wsaaT5688Size) || dateFound
				.isTrue())) {
			if (isGTE(ubblallpar.effdate, wsaaT5688Itmfrm[wsaaT5688Ix])) {
				wsaaDateFound.set("Y");
				ubblallpar.comlvlacc.set(wsaaT5688Comlvlacc[wsaaT5688Ix]);
			} else {
				wsaaT5688Ix++;
			}
		}

		if (!dateFound.isTrue()) {
			StringUtil stringVariable6 = new StringUtil();
			stringVariable6.addExpression(covxpfRec.getChdrcoy());
			stringVariable6.addExpression(covxpfRec.getChdrnum());
			stringVariable6.addExpression("-");
			stringVariable6.addExpression(t5688);
			stringVariable6.addExpression(":");
			stringVariable6.addExpression(ubblallpar.cnttype);
			stringVariable6.addExpression("-");
			stringVariable6.addExpression(wsaaADate);
			stringVariable6.setStringInto(syserrrec.params);
			syserrrec.statuz.set(h979);
			fatalError600();
		}
	}

	protected void utrsFundval5500() {
		if (utrsMap != null && utrsMap.containsKey(lastChdrnum)) {
			List<Utrspf> utList = utrsMap.get(lastChdrnum);
			for (Utrspf u : utList) {
				if (u.getChdrcoy().equals(bsprIO.getCompany().toString())) {
					BigDecimal unitBidPrice = BigDecimal.ZERO;
					if (vprnudlMap != null && vprnudlMap.containsKey(u.getUnitVirtualFund())) {
						List<Vprcpf> vprcpfList = vprnudlMap.get(u.getUnitVirtualFund());
						for (Vprcpf v : vprcpfList) {
							if (v.getUnitType().endsWith(u.getUnitType())) {
								unitBidPrice = v.getUnitBidPrice();
								break;
							}
						}
					}
					wsaaTempval = u.getCurrentUnitBal().multiply(unitBidPrice);
					wsaaFundval = wsaaFundval.add(wsaaTempval);
				}
			}
		}
	}

	protected void hitsFundval5100() {
		if (hitsMap != null && hitsMap.containsKey(lastChdrnum)) {
			List<Hitspf> hsList = hitsMap.get(lastChdrnum);
			for (Hitspf h : hsList) {
				wsaaFundval = wsaaFundval.add(h.getZcurprmbal());
			}
		}
	}

	protected void getLastcoi5200() {
		wsaaLastcoi = BigDecimal.ZERO;
		wsaaTranno.set(ZERO);
		wsaaBatctrcde.set(SPACES);
		wsaaTrandate.set(ZERO);
		if (zrstMap != null && zrstMap.containsKey(lastChdrnum)) {
			for (Zrstpf z : zrstMap.get(lastChdrnum)) {
				if (isEQ(z.getBatctrcde(), bprdIO.getAuthCode())) {
					wsaaTranno.set(z.getTranno());
					wsaaBatctrcde.set(z.getBatctrcde());
					wsaaTrandate.set(z.getTrandate());
					continue15230();
				}
			}
		}
	}

	protected void continue15230() {
		if (zrsttraMap != null && zrsttraMap.containsKey(lastChdrnum)) {
			for (Zrstpf z : zrsttraMap.get(lastChdrnum)) {
				if (z.getTranno() == wsaaTranno.toInt() && z.getBatctrcde().equals(wsaaBatctrcde.toString())
						&& z.getTrandate() == wsaaTrandate.toInt()) {
					wsaaLastcoi = wsaaLastcoi.add(z.getZramount01());
				}
			}
		}
	}

	protected void readTh5066000() {
		if (th506Map != null) {
			if (th506Map.containsKey(chdrlifIO.getCnttype())) {
				th506rec.th506Rec.set(StringUtil.rawToString(th506Map.get(chdrlifIO.getCnttype()).get(0).getGenarea()));
				return;
			} else if (th506Map.containsKey("***")) {
				th506rec.th506Rec.set(StringUtil.rawToString(th506Map.get("***").get(0).getGenarea()));
				return;
			}
		}
		syserrrec.params.set("th506:" + chdrlifIO.getCnttype());
		fatalError600();
	}

	protected void warnLetProc6100() {
		/* START */
		compute(wsaaMinfundWrnlt, 2).set(mult(wsaaLastcoi, th506rec.nofbbwrn));
		if (isLT(wsaaFundval, wsaaMinfundWrnlt)) {
			letcokcpy.isNoBb.set(th506rec.nofbbwrn);
			genWarnLetc6200();
		}
		/* EXIT */
	}

	protected void genWarnLetc6200() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start6210();
				case continue6250:
					continue6250();
				case exit6290:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void start6210() {
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrlifIO.getCnttype());
		stringVariable1.addExpression("WLET");
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression("***");
		stringVariable2.addExpression("WLET");
		if (tr384Map != null) {
			if (tr384Map.containsKey(stringVariable1.toString())) {
				tr384rec.tr384Rec.set(StringUtil.rawToString(tr384Map.get(stringVariable1.toString()).get(0)
						.getGenarea()));
				goTo(GotoLabel.continue6250);
			} else if (tr384Map.containsKey(stringVariable2.toString())) {
				tr384rec.tr384Rec.set(StringUtil.rawToString(tr384Map.get(stringVariable2.toString()).get(0)
						.getGenarea()));
				return;
			}
		}
		goTo(GotoLabel.exit6290);
	}

	protected void continue6250() {
		if (isEQ(tr384rec.letterType, SPACES)) {
			return;
		}
		getLetc6300();
	}

	protected void getLetc6300() {
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrlifIO.getChdrcoy());
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(bsscIO.getEffectiveDate());
		letrqstrec.rdocpfx.set(chdrlifIO.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrlifIO.getChdrnum());
		letcokcpy.isLanguage.set(bsscIO.getLanguage());
		letrqstrec.clntcoy.set(chdrlifIO.getCowncoy());
		letrqstrec.clntnum.set(chdrlifIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrlifIO.getChdrnum());
		letrqstrec.tranno.set(chdrlifIO.getTranno());
		letrqstrec.branch.set(chdrlifIO.getCntbranch());
		letrqstrec.despnum.set(chdrlifIO.getDespnum());
		letrqstrec.trcde.set(bprdIO.getAuthCode());
		letcokcpy.isFndamt.set(wsaaFundval);
		letcokcpy.isBbamt.set(wsaaLastcoi);
		letrqstrec.otherKeys.set(letcokcpy.saveOtherKeys);
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
	}

	protected void insufLetProc6400() {
		/* START */
		wsaaIsufLetFlg.set(SPACES);
		compute(wsaaMinfundInsuflt, 2).set(mult(wsaaLastcoi, th506rec.nofbbisf));
		if (isLT(wsaaFundval, wsaaMinfundInsuflt)) {
			wsaaIsufLetFlg.set("Y");
			letcokcpy.isNoBb.set(th506rec.nofbbisf);
			genInsufLetc6500();
		}
		/* EXIT */
	}

	protected void genInsufLetc6500() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start6510();
				case continue6550:
					continue6550();
				case exit6590:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void start6510() {
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrlifIO.getCnttype());
		stringVariable1.addExpression("ILET");
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression("***");
		stringVariable2.addExpression("ILET");
		if (tr384Map != null) {
			if (tr384Map.containsKey(stringVariable1.toString())) {
				tr384rec.tr384Rec.set(StringUtil.rawToString(tr384Map.get(stringVariable1.toString()).get(0)
						.getGenarea()));
				goTo(GotoLabel.continue6550);
			} else if (tr384Map.containsKey(stringVariable2.toString())) {
				tr384rec.tr384Rec.set(StringUtil.rawToString(tr384Map.get(stringVariable2.toString()).get(0)
						.getGenarea()));
				return;
			}
		}
		goTo(GotoLabel.exit6590);
	}

	protected void continue6550() {
		if (isEQ(tr384rec.letterType, SPACES)) {
			return;
		}
		getLetc6300();
	}

	protected void readUtrn6600() {
		/* Check that the contract has no unprocessed URNRs pending. */
		if (utrnrnlList != null) {
			if (utrnrnlList.contains(lastChdrnum)) {
				wsaaReadyToTrig.set("N");
			}
		}
	}

	protected void readHitr6700() {
		/* Check that the contract has no unprocessed URNRs pending. */
		if (hitrrnlList != null && hitrrnlList.contains(lastChdrnum)) {
			wsaaReadyToTrig.set("N");
		}
	}
	//ALS-4706-start
		private List<String> getCurrUnitBal(String coy) {
			List<String> addToList=new ArrayList<String>();
			utrspf = utrspfDAO.getUtrsRecord(coy,
					covxpfRec.getChdrnum(),
					covxpfRec.getLife(),
					covxpfRec.getCoverage(),
					covxpfRec.getRider());
			if (!(utrspf == null ||utrspf.isEmpty())) {
				for (Utrspf utlist : utrspf) {
					if(utlist !=null && utlist.getCurrentUnitBal() !=null){
						addToList.add(utlist.getCurrentUnitBal().toString());
					
					}
			}
		}
			return addToList;
		}

		private List<String> getUnitBidPrice(String coy, int effdate) {
			List<String> addToList = null;
			if (!(utrspf == null ||utrspf.isEmpty())) {
				addToList=new ArrayList<String>();
				for (Utrspf utlist : utrspf) {
					if(utlist !=null && utlist.getCurrentUnitBal() !=null){
					addToList.add(vprcpfDAO.getUnitBidPrice(coy,
							utlist.getUnitVirtualFund(), effdate).toString());
						
					}
				}
			}
			return addToList;
		}
		private void initMgmFeeRec(){
			
			mgmFeeRec.setFunction("RENL");
			mgmFeeRec.setChdrChdrcoy(covxpfRec.getChdrcoy());
			mgmFeeRec.setChdrChdrnum(covxpfRec.getChdrnum());
			mgmFeeRec.setLifeLife(covxpfRec.getLife());
			mgmFeeRec.setLifeJlife(covxpfRec.getJlife());
			mgmFeeRec.setCovrCoverage(covxpfRec.getCoverage());
			mgmFeeRec.setCovrRider(covxpfRec.getRider());
			mgmFeeRec.setPlanSuffix(covxpfRec.getPlanSuffix());
			mgmFeeRec.setBillfreq(covxpfRec.getUnitFreq());
			mgmFeeRec.setCntcurr(covxpfRec.getPremCurrency());
			mgmFeeRec.setEffdate(covxpfRec.getBenBillDate());//ILIFE-8029
			mgmFeeRec.setPremMeth(covxpfRec.getPremmeth());
			mgmFeeRec.setJlifePremMeth(covxpfRec.getJlPremMeth());
			mgmFeeRec.setSumins(covxpfRec.getSumins());
			mgmFeeRec.setPremCessDate(covxpfRec.getPremCessDate());
			mgmFeeRec.setCrtable(covxpfRec.getCrtable());
			mgmFeeRec.setMortcls(covxpfRec.getMortcls());
			mgmFeeRec.setSvMethod(covxpfRec.getSvMethod());
			mgmFeeRec.setAdfeemth(covxpfRec.getAdfeemth());
			mgmFeeRec.setBatccoy(batcdorrec.company.toString());
			mgmFeeRec.setBatcbrn(batcdorrec.branch.toString());
			mgmFeeRec.setBatcactmn(batcdorrec.actmonth.toInt());
			mgmFeeRec.setBatcactyr(batcdorrec.actyear.toInt());
			mgmFeeRec.setBatctrcde(batcdorrec.trcde.toString());
			mgmFeeRec.setBatch(batcdorrec.batch.toString());
			mgmFeeRec.setBillchnl(chdrlifIO.getBillchnl());
			mgmFeeRec.setCnttype(chdrlifIO.getCnttype());
			mgmFeeRec.setTranno(chdrlifIO.getTranno());
			mgmFeeRec.setPolsum2(chdrlifIO.getPolsum());
			mgmFeeRec.setPtdate(chdrlifIO.getPtdate());
			mgmFeeRec.setPolinc2(chdrlifIO.getPolinc());
			mgmFeeRec.setSingp(BigDecimal.ZERO);
			mgmFeeRec.setChdrRegister(chdrlifIO.getReg());
			mgmFeeRec.setLanguage(bsscIO.getLanguage().toString());
			mgmFeeRec.setUser(0);
			mgmFeeRec.setPolcurr(0);
			mgmFeeRec.setBenfunc("0");
			mgmFeeRec.setFirstPrem(BigDecimal.ZERO);
			List<String> currUnitbal=getCurrUnitBal(covxpfRec.getChdrcoy());/* IJTI-1523 */
			mgmFeeRec.setCurUnitBal(currUnitbal);
			mgmFeeRec.setUnitBidPrice(getUnitBidPrice(covxpfRec.getChdrcoy(),covxpfRec.getBenBillDate()));/* IJTI-1523 */  
			mgmFeeRec.setNrFunds(currUnitbal.size());      
		} //ALS-4706-end

	/*
	 * Class transformed from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner {
		private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	}
	
	protected void setAdvisorFeesRequired() {
		if (advisorFeeCalc) {
			Integer date = chdrBtDateMap.get(covxpfRec.getChdrnum());/* IJTI-1523 */
			if (date != null && date != 0) {
				if (isEQ(date, covxpfRec.getBenBillDate())) { // ILIFE-8082
					int i = 0;
				
						List<Utrspf> data = utrsMap.get(covxpfRec.getChdrnum());
						if(data != null){
							for (Utrspf utrspf : data) {
								if (covxpfRec.getCoverage().equals(utrspf.getCoverage())
										&& covxpfRec.getLife().equals(utrspf.getLife())
										&& covxpfRec.getRider().equals(utrspf.getRider())) {
									if (vprnudlMap.containsKey(utrspf.getUnitVirtualFund())) {
										List<Vprcpf> vprcpfList = vprnudlMap.get(utrspf.getUnitVirtualFund());

										for (Vprcpf v : vprcpfList) {
											if (v.getUnitType().endsWith(utrspf.getUnitType())) {
												++i;

												ubblallpar.curdunitprice[i].set(v.getUnitBidPrice());
												ubblallpar.curdunitbal[i].set(utrspf.getCurrentUnitBal());
												break;
											}
										}
									}
								}
							}
						}
					ubblallpar.nrFunds.set(i);				
					ubblallpar.cntAdvisorFessReq.set("Y");
				} else {
					ubblallpar.cntAdvisorFessReq.set("N");
				}
			}
		} else {
			ubblallpar.cntAdvisorFessReq.set("N");
		}
	}
	
}