package com.csc.life.flexiblepremium.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:13
 * Description:
 * Copybook name: T5728REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5728rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5728Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData commAsEarned = new FixedLengthStringData(1).isAPartOf(t5728Rec, 0);
  	public FixedLengthStringData commEarns = new FixedLengthStringData(42).isAPartOf(t5728Rec, 1);
  	public ZonedDecimalData[] commEarn = ZDArrayPartOfStructure(14, 3, 0, commEarns, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(commEarns, 0, FILLER_REDEFINE);
  	public ZonedDecimalData commEarn01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData commEarn02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData commEarn03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData commEarn04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public ZonedDecimalData commEarn05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData commEarn06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData commEarn07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData commEarn08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
  	public ZonedDecimalData commEarn09 = new ZonedDecimalData(3, 0).isAPartOf(filler, 24);
  	public ZonedDecimalData commEarn10 = new ZonedDecimalData(3, 0).isAPartOf(filler, 27);
  	public ZonedDecimalData commEarn11 = new ZonedDecimalData(3, 0).isAPartOf(filler, 30);
  	public ZonedDecimalData commEarn12 = new ZonedDecimalData(3, 0).isAPartOf(filler, 33);
  	public ZonedDecimalData commEarn13 = new ZonedDecimalData(3, 0).isAPartOf(filler, 36);
  	public ZonedDecimalData commEarn14 = new ZonedDecimalData(3, 0).isAPartOf(filler, 39);
  	public FixedLengthStringData commRels = new FixedLengthStringData(42).isAPartOf(t5728Rec, 43);
  	public ZonedDecimalData[] commRel = ZDArrayPartOfStructure(14, 3, 0, commRels, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(42).isAPartOf(commRels, 0, FILLER_REDEFINE);
  	public ZonedDecimalData commRel01 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData commRel02 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 3);
  	public ZonedDecimalData commRel03 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 6);
  	public ZonedDecimalData commRel04 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 9);
  	public ZonedDecimalData commRel05 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData commRel06 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 15);
  	public ZonedDecimalData commRel07 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 18);
  	public ZonedDecimalData commRel08 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 21);
  	public ZonedDecimalData commRel09 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 24);
  	public ZonedDecimalData commRel10 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 27);
  	public ZonedDecimalData commRel11 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 30);
  	public ZonedDecimalData commRel12 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 33);
  	public ZonedDecimalData commRel13 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 36);
  	public ZonedDecimalData commRel14 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 39);
  	public FixedLengthStringData tgtRcvdEarns = new FixedLengthStringData(42).isAPartOf(t5728Rec, 85);
  	public ZonedDecimalData[] tgtRcvdEarn = ZDArrayPartOfStructure(14, 3, 0, tgtRcvdEarns, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(42).isAPartOf(tgtRcvdEarns, 0, FILLER_REDEFINE);
  	public ZonedDecimalData tgtRcvdEarn01 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData tgtRcvdEarn02 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 3);
  	public ZonedDecimalData tgtRcvdEarn03 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 6);
  	public ZonedDecimalData tgtRcvdEarn04 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 9);
  	public ZonedDecimalData tgtRcvdEarn05 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 12);
  	public ZonedDecimalData tgtRcvdEarn06 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 15);
  	public ZonedDecimalData tgtRcvdEarn07 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 18);
  	public ZonedDecimalData tgtRcvdEarn08 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 21);
  	public ZonedDecimalData tgtRcvdEarn09 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 24);
  	public ZonedDecimalData tgtRcvdEarn10 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 27);
  	public ZonedDecimalData tgtRcvdEarn11 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 30);
  	public ZonedDecimalData tgtRcvdEarn12 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 33);
  	public ZonedDecimalData tgtRcvdEarn13 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 36);
  	public ZonedDecimalData tgtRcvdEarn14 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 39);
  	public FixedLengthStringData tgtRcvdPays = new FixedLengthStringData(42).isAPartOf(t5728Rec, 127);
  	public ZonedDecimalData[] tgtRcvdPay = ZDArrayPartOfStructure(14, 3, 0, tgtRcvdPays, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(42).isAPartOf(tgtRcvdPays, 0, FILLER_REDEFINE);
  	public ZonedDecimalData tgtRcvdPay01 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 0);
  	public ZonedDecimalData tgtRcvdPay02 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 3);
  	public ZonedDecimalData tgtRcvdPay03 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 6);
  	public ZonedDecimalData tgtRcvdPay04 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 9);
  	public ZonedDecimalData tgtRcvdPay05 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 12);
  	public ZonedDecimalData tgtRcvdPay06 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 15);
  	public ZonedDecimalData tgtRcvdPay07 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 18);
  	public ZonedDecimalData tgtRcvdPay08 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 21);
  	public ZonedDecimalData tgtRcvdPay09 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 24);
  	public ZonedDecimalData tgtRcvdPay10 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 27);
  	public ZonedDecimalData tgtRcvdPay11 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 30);
  	public ZonedDecimalData tgtRcvdPay12 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 33);
  	public ZonedDecimalData tgtRcvdPay13 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 36);
  	public ZonedDecimalData tgtRcvdPay14 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 39);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(331).isAPartOf(t5728Rec, 169, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5728Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5728Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}