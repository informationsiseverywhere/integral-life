package com.csc.life.flexiblepremium.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5140
 * @version 1.0 generated on 30/08/09 06:36
 * @author Quipoz
 */
public class S5140ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(422);
	public FixedLengthStringData dataFields = new FixedLengthStringData(182).isAPartOf(dataArea, 0);
	public ZonedDecimalData anntarprm = DD.anntarprm.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,17);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,19);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,27);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,35);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,45);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,48);
	public ZonedDecimalData instalment = DD.instalment.copyToZonedDecimal().isAPartOf(dataFields,51);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,68);
	public ZonedDecimalData minPrmReqd = DD.minreqd.copyToZonedDecimal().isAPartOf(dataFields,98);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,116);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,126);
	public ZonedDecimalData totalBilled = DD.totbill.copyToZonedDecimal().isAPartOf(dataFields,129);
	public ZonedDecimalData totalRecd = DD.totrecd.copyToZonedDecimal().isAPartOf(dataFields,147);
	public ZonedDecimalData varnce = DD.varnce.copyToZonedDecimal().isAPartOf(dataFields,165);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 182);
	public FixedLengthStringData anntarprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData instalmentErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData minreqdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData totbillErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData totrecdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData varnceErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(180).isAPartOf(dataArea, 242);
	public FixedLengthStringData[] anntarprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] instalmentOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] minreqdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] totbillOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] totrecdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] varnceOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);

	public LongData S5140screenWritten = new LongData(0);
	public LongData S5140protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5140ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {chdrnum, cnttype, longdesc, cntcurr, chdrstatus, premstatus, register, anntarprm, instalment, billfreq, btdate, minPrmReqd, totalBilled, totalRecd, varnce};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, longdescOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, anntarprmOut, instalmentOut, billfreqOut, btdateOut, minreqdOut, totbillOut, totrecdOut, varnceOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, longdescErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, anntarprmErr, instalmentErr, billfreqErr, btdateErr, minreqdErr, totbillErr, totrecdErr, varnceErr};
		screenDateFields = new BaseData[] {btdate};
		screenDateErrFields = new BaseData[] {btdateErr};
		screenDateDispFields = new BaseData[] {btdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5140screen.class;
		protectRecord = S5140protect.class;
	}

}
