package com.csc.life.flexiblepremium.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S5728screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5728ScreenVars sv = (S5728ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5728screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5728ScreenVars screenVars = (S5728ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.commAsEarned.setClassString("");
		screenVars.tgtRcvdPay01.setClassString("");
		screenVars.tgtRcvdPay02.setClassString("");
		screenVars.tgtRcvdPay03.setClassString("");
		screenVars.tgtRcvdPay04.setClassString("");
		screenVars.tgtRcvdPay05.setClassString("");
		screenVars.tgtRcvdPay06.setClassString("");
		screenVars.tgtRcvdPay07.setClassString("");
		screenVars.tgtRcvdPay08.setClassString("");
		screenVars.tgtRcvdPay09.setClassString("");
		screenVars.tgtRcvdPay10.setClassString("");
		screenVars.tgtRcvdPay11.setClassString("");
		screenVars.tgtRcvdPay12.setClassString("");
		screenVars.tgtRcvdPay13.setClassString("");
		screenVars.tgtRcvdPay14.setClassString("");
		screenVars.commRel01.setClassString("");
		screenVars.commRel02.setClassString("");
		screenVars.commRel03.setClassString("");
		screenVars.commRel04.setClassString("");
		screenVars.commRel05.setClassString("");
		screenVars.commRel06.setClassString("");
		screenVars.commRel07.setClassString("");
		screenVars.commRel08.setClassString("");
		screenVars.commRel09.setClassString("");
		screenVars.commRel10.setClassString("");
		screenVars.commRel11.setClassString("");
		screenVars.commRel12.setClassString("");
		screenVars.commRel13.setClassString("");
		screenVars.commRel14.setClassString("");
		screenVars.tgtRcvdEarn01.setClassString("");
		screenVars.tgtRcvdEarn02.setClassString("");
		screenVars.tgtRcvdEarn03.setClassString("");
		screenVars.tgtRcvdEarn04.setClassString("");
		screenVars.tgtRcvdEarn05.setClassString("");
		screenVars.tgtRcvdEarn06.setClassString("");
		screenVars.tgtRcvdEarn07.setClassString("");
		screenVars.tgtRcvdEarn08.setClassString("");
		screenVars.tgtRcvdEarn09.setClassString("");
		screenVars.tgtRcvdEarn10.setClassString("");
		screenVars.tgtRcvdEarn11.setClassString("");
		screenVars.tgtRcvdEarn12.setClassString("");
		screenVars.tgtRcvdEarn13.setClassString("");
		screenVars.tgtRcvdEarn14.setClassString("");
		screenVars.commEarn01.setClassString("");
		screenVars.commEarn02.setClassString("");
		screenVars.commEarn03.setClassString("");
		screenVars.commEarn04.setClassString("");
		screenVars.commEarn05.setClassString("");
		screenVars.commEarn06.setClassString("");
		screenVars.commEarn07.setClassString("");
		screenVars.commEarn08.setClassString("");
		screenVars.commEarn09.setClassString("");
		screenVars.commEarn10.setClassString("");
		screenVars.commEarn11.setClassString("");
		screenVars.commEarn12.setClassString("");
		screenVars.commEarn13.setClassString("");
		screenVars.commEarn14.setClassString("");
	}

/**
 * Clear all the variables in S5728screen
 */
	public static void clear(VarModel pv) {
		S5728ScreenVars screenVars = (S5728ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.commAsEarned.clear();
		screenVars.tgtRcvdPay01.clear();
		screenVars.tgtRcvdPay02.clear();
		screenVars.tgtRcvdPay03.clear();
		screenVars.tgtRcvdPay04.clear();
		screenVars.tgtRcvdPay05.clear();
		screenVars.tgtRcvdPay06.clear();
		screenVars.tgtRcvdPay07.clear();
		screenVars.tgtRcvdPay08.clear();
		screenVars.tgtRcvdPay09.clear();
		screenVars.tgtRcvdPay10.clear();
		screenVars.tgtRcvdPay11.clear();
		screenVars.tgtRcvdPay12.clear();
		screenVars.tgtRcvdPay13.clear();
		screenVars.tgtRcvdPay14.clear();
		screenVars.commRel01.clear();
		screenVars.commRel02.clear();
		screenVars.commRel03.clear();
		screenVars.commRel04.clear();
		screenVars.commRel05.clear();
		screenVars.commRel06.clear();
		screenVars.commRel07.clear();
		screenVars.commRel08.clear();
		screenVars.commRel09.clear();
		screenVars.commRel10.clear();
		screenVars.commRel11.clear();
		screenVars.commRel12.clear();
		screenVars.commRel13.clear();
		screenVars.commRel14.clear();
		screenVars.tgtRcvdEarn01.clear();
		screenVars.tgtRcvdEarn02.clear();
		screenVars.tgtRcvdEarn03.clear();
		screenVars.tgtRcvdEarn04.clear();
		screenVars.tgtRcvdEarn05.clear();
		screenVars.tgtRcvdEarn06.clear();
		screenVars.tgtRcvdEarn07.clear();
		screenVars.tgtRcvdEarn08.clear();
		screenVars.tgtRcvdEarn09.clear();
		screenVars.tgtRcvdEarn10.clear();
		screenVars.tgtRcvdEarn11.clear();
		screenVars.tgtRcvdEarn12.clear();
		screenVars.tgtRcvdEarn13.clear();
		screenVars.tgtRcvdEarn14.clear();
		screenVars.commEarn01.clear();
		screenVars.commEarn02.clear();
		screenVars.commEarn03.clear();
		screenVars.commEarn04.clear();
		screenVars.commEarn05.clear();
		screenVars.commEarn06.clear();
		screenVars.commEarn07.clear();
		screenVars.commEarn08.clear();
		screenVars.commEarn09.clear();
		screenVars.commEarn10.clear();
		screenVars.commEarn11.clear();
		screenVars.commEarn12.clear();
		screenVars.commEarn13.clear();
		screenVars.commEarn14.clear();
	}
}
