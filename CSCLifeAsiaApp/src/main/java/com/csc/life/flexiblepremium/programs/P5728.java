/*
 * File: P5728.java
 * Date: 30 August 2009 0:35:12
 * Author: Quipoz Limited
 * 
 * Class transformed from P5728.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.flexiblepremium.procedures.T5728pt;
import com.csc.life.flexiblepremium.screens.S5728ScreenVars;
import com.csc.life.flexiblepremium.tablestructures.T5728rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P5728 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5728");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private T5728rec t5728rec = new T5728rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5728ScreenVars sv = ScreenProgram.getScreenVars( S5728ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public P5728() {
		super();
		screenVars = sv;
		new ScreenModel("S5728", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5728rec.t5728Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t5728rec.commEarn01.set(ZERO);
		t5728rec.commEarn02.set(ZERO);
		t5728rec.commEarn03.set(ZERO);
		t5728rec.commEarn04.set(ZERO);
		t5728rec.commEarn05.set(ZERO);
		t5728rec.commEarn06.set(ZERO);
		t5728rec.commEarn07.set(ZERO);
		t5728rec.commEarn08.set(ZERO);
		t5728rec.commEarn09.set(ZERO);
		t5728rec.commEarn10.set(ZERO);
		t5728rec.commEarn11.set(ZERO);
		t5728rec.commEarn12.set(ZERO);
		t5728rec.commEarn13.set(ZERO);
		t5728rec.commEarn14.set(ZERO);
		t5728rec.commRel01.set(ZERO);
		t5728rec.commRel02.set(ZERO);
		t5728rec.commRel03.set(ZERO);
		t5728rec.commRel04.set(ZERO);
		t5728rec.commRel05.set(ZERO);
		t5728rec.commRel06.set(ZERO);
		t5728rec.commRel07.set(ZERO);
		t5728rec.commRel08.set(ZERO);
		t5728rec.commRel09.set(ZERO);
		t5728rec.commRel10.set(ZERO);
		t5728rec.commRel11.set(ZERO);
		t5728rec.commRel12.set(ZERO);
		t5728rec.commRel13.set(ZERO);
		t5728rec.commRel14.set(ZERO);
		t5728rec.tgtRcvdEarn01.set(ZERO);
		t5728rec.tgtRcvdEarn02.set(ZERO);
		t5728rec.tgtRcvdEarn03.set(ZERO);
		t5728rec.tgtRcvdEarn04.set(ZERO);
		t5728rec.tgtRcvdEarn05.set(ZERO);
		t5728rec.tgtRcvdEarn06.set(ZERO);
		t5728rec.tgtRcvdEarn07.set(ZERO);
		t5728rec.tgtRcvdEarn08.set(ZERO);
		t5728rec.tgtRcvdEarn09.set(ZERO);
		t5728rec.tgtRcvdEarn10.set(ZERO);
		t5728rec.tgtRcvdEarn11.set(ZERO);
		t5728rec.tgtRcvdEarn12.set(ZERO);
		t5728rec.tgtRcvdEarn13.set(ZERO);
		t5728rec.tgtRcvdEarn14.set(ZERO);
		t5728rec.tgtRcvdPay01.set(ZERO);
		t5728rec.tgtRcvdPay02.set(ZERO);
		t5728rec.tgtRcvdPay03.set(ZERO);
		t5728rec.tgtRcvdPay04.set(ZERO);
		t5728rec.tgtRcvdPay05.set(ZERO);
		t5728rec.tgtRcvdPay06.set(ZERO);
		t5728rec.tgtRcvdPay07.set(ZERO);
		t5728rec.tgtRcvdPay08.set(ZERO);
		t5728rec.tgtRcvdPay09.set(ZERO);
		t5728rec.tgtRcvdPay10.set(ZERO);
		t5728rec.tgtRcvdPay11.set(ZERO);
		t5728rec.tgtRcvdPay12.set(ZERO);
		t5728rec.tgtRcvdPay13.set(ZERO);
		t5728rec.tgtRcvdPay14.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.commAsEarned.set(t5728rec.commAsEarned);
		sv.commEarns.set(t5728rec.commEarns);
		sv.commRels.set(t5728rec.commRels);
		sv.tgtRcvdEarns.set(t5728rec.tgtRcvdEarns);
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.tgtRcvdPays.set(t5728rec.tgtRcvdPays);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(t5728rec.t5728Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.commAsEarned,t5728rec.commAsEarned)) {
			t5728rec.commAsEarned.set(sv.commAsEarned);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.commEarns,t5728rec.commEarns)) {
			t5728rec.commEarns.set(sv.commEarns);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.commRels,t5728rec.commRels)) {
			t5728rec.commRels.set(sv.commRels);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.tgtRcvdEarns,t5728rec.tgtRcvdEarns)) {
			t5728rec.tgtRcvdEarns.set(sv.tgtRcvdEarns);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.tgtRcvdPays,t5728rec.tgtRcvdPays)) {
			t5728rec.tgtRcvdPays.set(sv.tgtRcvdPays);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T5728pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
