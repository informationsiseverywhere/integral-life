package com.csc.life.flexiblepremium.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5140screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 18, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5140ScreenVars sv = (S5140ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5140screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5140ScreenVars screenVars = (S5140ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.anntarprm.setClassString("");
		screenVars.instalment.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.minPrmReqd.setClassString("");
		screenVars.totalBilled.setClassString("");
		screenVars.totalRecd.setClassString("");
		screenVars.varnce.setClassString("");
	}

/**
 * Clear all the variables in S5140screen
 */
	public static void clear(VarModel pv) {
		S5140ScreenVars screenVars = (S5140ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.longdesc.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.anntarprm.clear();
		screenVars.instalment.clear();
		screenVars.billfreq.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.minPrmReqd.clear();
		screenVars.totalBilled.clear();
		screenVars.totalRecd.clear();
		screenVars.varnce.clear();
	}
}
