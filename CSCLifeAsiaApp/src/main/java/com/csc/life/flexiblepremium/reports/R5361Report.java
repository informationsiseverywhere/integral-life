package com.csc.life.flexiblepremium.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGDateData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5361.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5361Report extends SMARTReportLayout { 

	private ZonedDecimalData billedp = new ZonedDecimalData(17, 2);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData coverage = new FixedLengthStringData(2);
	private FixedLengthStringData currfrom = new FixedLengthStringData(10);
	private FixedLengthStringData currto = new FixedLengthStringData(10);
	private RPGDateData dateReportVariable = new RPGDateData();
	private FixedLengthStringData life = new FixedLengthStringData(2);
	private ZonedDecimalData ovrminreq = new ZonedDecimalData(17, 2);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0);
	private ZonedDecimalData prmper = new ZonedDecimalData(17, 2);
	private ZonedDecimalData prmrcdp = new ZonedDecimalData(17, 2);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5361Report() {
		super();
	}


	/**
	 * Print the XML for R5361d01
	 */
	public void printR5361d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		life.setFieldName("life");
		life.setInternal(subString(recordData, 9, 2));
		coverage.setFieldName("coverage");
		coverage.setInternal(subString(recordData, 11, 2));
		plnsfx.setFieldName("plnsfx");
		plnsfx.setInternal(subString(recordData, 13, 4));
		currfrom.setFieldName("currfrom");
		currfrom.setInternal(subString(recordData, 17, 10));
		currto.setFieldName("currto");
		currto.setInternal(subString(recordData, 27, 10));
		prmper.setFieldName("prmper");
		prmper.setInternal(subString(recordData, 37, 17));
		prmrcdp.setFieldName("prmrcdp");
		prmrcdp.setInternal(subString(recordData, 54, 17));
		billedp.setFieldName("billedp");
		billedp.setInternal(subString(recordData, 71, 17));
		ovrminreq.setFieldName("ovrminreq");
		ovrminreq.setInternal(subString(recordData, 88, 17));
		printLayout("R5361d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				life,
				coverage,
				plnsfx,
				currfrom,
				currto,
				prmper,
				prmrcdp,
				billedp,
				ovrminreq
			}
		);

	}

	/**
	 * Print the XML for R5361h01
	 */
	public void printR5361h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.set(getDate());
		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("R5361h01",			// Record name
			new BaseData[]{			// Fields:
				dateReportVariable,
				company,
				companynm,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(13);
	}

	/**
	 * Print the XML for R5361t01
	 */
	public void printR5361t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		printLayout("R5361t01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

		currentPrintLine.add(1);
	}


}
