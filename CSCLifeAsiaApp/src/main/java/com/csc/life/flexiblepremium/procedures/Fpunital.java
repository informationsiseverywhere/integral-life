/*
 * File: Fpunital.java
 * Date: 29 August 2009 22:48:30
 * Author: Quipoz Limited
 * 
 * Class transformed from FPUNITAL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.recordstructures.Rnlallrec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrunlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.InciTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkrnlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrstTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5519rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5539rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6646rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*AUTHOR.                          PAXUS FINANCIAL SYSTEMS.
*REMARKS.
*
*
* This generic subroutine allocates units (premium) to a component
* following receipt of premium.
*
* This subroutine is called only for flexible premium contracts
* for premium received up to target.
* (Over target unit allocation is performed in a different
* subroutine 'OVERALOC').
*
* It will allocate premium to the oldest seqno INCI until
* the annualised premium on the INCI is met. Once the annualised
* premium for a target year has been received, premium is added
* to the subsequent INCI (this is where premium changes have been
* made).
*
*    The module has to establish premium allocations which
* may be split between initial units, accumulation units and non-
* invested amounts. This process also applies to any unit enhance-
* ment which may be relevant.
*
*    Once the net  allocation  to  units  is  established, the  un t
* apportionment file (ULNKPF)  is  accessed  to find out which fun s
* are to be  invested in. Finally the unit transaction file (UTRNP )
* is updated with  a  space  in  the 'feedback indicator' which wi l
* trigger later processing by the unit dealing batch run.
*
*
* 1.) Establish premium allocation.
*
* For flexible premiums, the amount passed in the COVER-
* INSTPREM is the total amount of up to target premium received
* in this transaction for the particular target year.
*
* Again, passed in linkage, is the total premium received to date
* for the target year PRIOR to the addition of this premium.
*
* The total premium to date is moved to WSAA-TOT-RECD.
* The COVR-INSTPREM is moved to WSAA-AMT-REM.
*
* Each INCI record is read in ASCENDING SEQNO order (ie) oldest
* first and until WSAA-AMT-REM = zero or there are no more INCIS
* for the Coverage :
*
*     If the annualised INCI-CURR-PREM is < WSAA-TOT-RECD        c l
*     read the next INCI.
*
*     Calculate the difference between WSAA-TOT-RECD and
*     the annualised INCI-CURR-PREM giving WSAA-PREM-AVAIL
*
*     If WSAA-AMT-REM > or = WSAA-PREM-AVAIL
*        allocate all the premium received to this INCI
*        (move WSAA-AMT-REM to WSAA-INCI-PREM)
*
*     If WSAA-AMT-REM < WSAA-PREM-AVAIL
*        Allocate only WSAA-PREM-AVAIL to this INCI
*        (move WSAA-PREM-AVAIL to WSAA-INCI-PREM)
*        subtract WSAA-PREM-AVAIL from WSAA-AMT-REM
*        Add WSAA-PREM-AVAIL to WSAA-TOT-RECD.
*
*
*     Once the WSAA-INCI-PREM has been determnied,
*        read through  each occurrence of INCI-PREM-CURR
*        until WSAA-INCI-PREM has been fully allocated :         t l
*
*        If this occurrence of INCI-PREM-CURR is zero ignore it an 
*          look up the next occurrence.
*        If the premium we are allocating is greater than this
*          occurrence of INCI-PREM-CURR perform Calculate Split
*          using INCI-PREM-CURR as the amount.
*        If INCI-PREM-CURR > premium to be allocated perform
*          Calculate Split using the premium to be allocated as th 
*          amount.
*
* Calculate Split.
*
*        Compute the non-invested premium as:
*
*     Amount to be allocated * (100 - INCI-PCUNIT)
*     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*                          100
*
*        Compute the allocation to initial units as:
*
*     (Amount to be allocated - non invested prem) * INCI-UNIT-SPL T
*     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
*                          100
*
*        Compute the allocation to accumulation units as:
*
*     Amount to be allocated - (non invested prem + initial units)
*
*        Accumulate these three amounts in working storage as ther 
*          may be more to be added to them if the premium falls
*          within two or more bands of allocation.
*        Update the INCIPF record by subtracting the amount of
*          allocation (for this occurrence) from the relevant
*          INCI-PREM-CURR field.
*        If the total of the amounts allocated in working storage  
*          INCI-CURR-PREM (i.e. the premium has been allocated)
*          rewrite the INCIPF record back to the database
*        Else
*        Calculate the amount to be allocated for next time round
*          the loop as:
*             INCI-CURR-PREM - total of working storage amounts
*
*    After processing all  INCIPF  records  the total amount held  n
* the 3 working  storage  areas  should  equal the amount of premi m
* originally passed in linkage - if not this is a fatal error.
*
*
* 2.) Establish Fund allocation split.
*
*    Calculate the initial unit discount factor. First look up
* T5540 for the discount basis, then use this basis to access
* T5519. We use the information in T5519 to decide whether to
* look up T5539 or T6646 for the actual discount figure. Both
* T5539 and T6646 are accessed by the discount factor in T5540.
*
*    Each  coverage has a  specific  apportionment  for  units  in o
* different funds. This should not  be confused with unit allocati n
* which   determines   how    premium    is    distributed   betwe n
* non-investment, initial and accumulation units. This fund split  s
* held in the ULNKPF file.
*
*    Read  the  ULNKPF  file using the logical view ULNKRNL with t e
* component key.  Write  a  UTRN  for  each  of the fund allocatio s
* (initial or accumulation) in working storage which are not 0. Th y
* should be  apportioned  between  the  funds as held on ULNKPF. T e
* UTRNPF record should be set up as follows:
*
*          - Component key from COVRPF
*          - Virtual Fund from ULNKPF
*          - Unit type dependent on working storage type being
*                                                  processed.
*          - Tran-no from linkage
*          - Term ID and user from run parameters
*          - Transaction date and time from system parameters
*          - Batch key as set up in this program
*          - Unit sub account 'INIT' or 'ACUM' dependent on type
*                                           being processed.
*          - Now/Deferred indicator from T6647 using transaction/
*            contract type
*            If this is an off movement (negative amount) look up
*            the deallocation indicator - if this is now (look at
*            copybook CBNOWDEF) move 'N' to this field else move ' '.
*            If this is an on  movement (positive amount) look up
*            the allocation indicator - if this is now (look at
*            copybook CBNOWDEF) move 'N' to this field else move ' '.
*          - Monies date as bill date from linkage
*          - CRTABLE from COVRPF
*          - Contract currency from linkage
*          - Contract amount = allocation amount
*          - Fund currency from T5515 (key = fund)
*          - SACS code and type and GL code from linkage
*          - Contract type from linkage
*          - Processing sequence number from T6647
*          - SVP = 1
*          - CR comm date - from COVRPF
*
*    Write  the  record  to  the database.
*
*
* 3.) Write the uninvested record.
*
*
*    After  dealing  with  the  allocation  a  single UTRNPF must  e
* created  for the non investment allocation. Create a UTRNPF reco d
* as follows:
*
*          - Initialise all fields
*          - Component key from COVRPF
*          - Tran-no from linkage
*          - Term ID and user from run parameters
*          - Transaction date and time from system parameters
*          - Batch key as set up in this program
*          - Unit sub account 'NVST'
*          - Monies date as bill date from linkage
*          - CRTABLE from COVRPF
*          - Contract currency from linkage
*          - Contract amount = uninvested amount
*          - SACS code and type and GL code from linkage
*          - Contract type from linkage
*          - CR comm date from COVRPF
*
*    Write the record to the database.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Fpunital extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "FPUNITAL";
	private ZonedDecimalData wsaaMoniesDate = new ZonedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaTref = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaTrefChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaTref, 0);
	private FixedLengthStringData wsaaTrefChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaTref, 1);
	private FixedLengthStringData wsaaTrefLife = new FixedLengthStringData(2).isAPartOf(wsaaTref, 9);
	private FixedLengthStringData wsaaTrefCoverage = new FixedLengthStringData(2).isAPartOf(wsaaTref, 11);
	private FixedLengthStringData wsaaTrefRider = new FixedLengthStringData(2).isAPartOf(wsaaTref, 13);
	private FixedLengthStringData wsaaTrefPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaTref, 15);
		/* WSAA-BILLFREQ-VAR */
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);

	private FixedLengthStringData filler1 = new FixedLengthStringData(2).isAPartOf(wsaaBillfreq, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaBillfreqNum = new ZonedDecimalData(2, 0).isAPartOf(filler1, 0).setUnsigned();

	private FixedLengthStringData wsaaT6647Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT6647Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Item, 0);
	private FixedLengthStringData wsaaT6647Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Item, 4);
	private ZonedDecimalData wsaaFact = new ZonedDecimalData(3, 0);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);

	private FixedLengthStringData wsaaTaxs = new FixedLengthStringData(34);
	private ZonedDecimalData[] wsaaTax = ZDArrayPartOfStructure(2, 17, 2, wsaaTaxs, 0);
	private PackedDecimalData wsaaTotCd = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
	private ZonedDecimalData wsaaSeqno = new ZonedDecimalData(2, 0);
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String g027 = "G027";
	private static final String t744 = "T744";
		/* FORMATS */
	private static final String incirec = "INCIREC";
	private static final String utrnrec = "UTRNREC   ";
	private static final String itemrec = "ITEMREC   ";
	private static final String ulnkrnlrec = "ULNKRNLREC";
	private static final String hitrrec = "HITRREC   ";
	private static final String taxdrec = "TAXDREC";
	private static final String zrstrec = "ZRSTREC";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CovrunlTableDAM covrunlIO = new CovrunlTableDAM();
	private HitrTableDAM hitrIO = new HitrTableDAM();
	private InciTableDAM inciIO = new InciTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	private UlnkrnlTableDAM ulnkrnlIO = new UlnkrnlTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private ZrstTableDAM zrstIO = new ZrstTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Itemkey wsaaItemkey = new Itemkey();
	private T5515rec t5515rec = new T5515rec();
	private T6647rec t6647rec = new T6647rec();
	private T5540rec t5540rec = new T5540rec();
	private T6646rec t6646rec = new T6646rec();
	private T5539rec t5539rec = new T5539rec();
	private T5519rec t5519rec = new T5519rec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Getdescrec getdescrec = new Getdescrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Rnlallrec rnlallrec = new Rnlallrec();
	private TablesInner tablesInner = new TablesInner();
	private WsaaAmountHoldersInner wsaaAmountHoldersInner = new WsaaAmountHoldersInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readInci690, 
		readT5540220, 
		next4150, 
		accumUnit4200, 
		next4250, 
		nearExit4800, 
		comlvlacc5105, 
		cont5110, 
		writrUtrn5800, 
		writrHitr5250, 
		fixedTerm6200, 
		wholeOfLife6300, 
		exit6900, 
		b100CreateZrst, 
		b100Exit, 
		b300WriteTax2
	}

	public Fpunital() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		rnlallrec.rnlallRec = convertAndSetParam(rnlallrec.rnlallRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		para100();
		exit190();
	}

protected void para100()
	{
		initialisation200();
		/* Read contract header details.                                   */
		chdrlnbIO.setDataKey(SPACES);
		chdrlnbIO.setChdrcoy(rnlallrec.company);
		chdrlnbIO.setChdrnum(rnlallrec.chdrnum);
		/*    Need to Hold record as we will have to update the Unit       */
		/*    Statement Date.                                              */
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			databaseError580();
		}
		/*  Read and hold coverage/rider record.                           */
		/*  The COVR Record may be updated by the other programs that      */
		/*  this subroutine calls. As such, initial reading of the COVR    */
		/*  should be using READR.                                         */
		covrunlIO.setChdrcoy(rnlallrec.company);
		covrunlIO.setChdrnum(rnlallrec.chdrnum);
		covrunlIO.setLife(rnlallrec.life);
		covrunlIO.setCoverage(rnlallrec.coverage);
		covrunlIO.setRider(rnlallrec.rider);
		covrunlIO.setPlanSuffix(rnlallrec.planSuffix);
		covrunlIO.setFunction(varcom.readr);
		covrunlIO.setFormat("COVRUNLREC");
		SmartFileCode.execute(appVars, covrunlIO);
		if (isNE(covrunlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrunlIO.getParams());
			syserrrec.statuz.set(covrunlIO.getStatuz());
			databaseError580();
		}
		/* Read ULNKRNL to get the fund split for the coverage/rider.*/
		ulnkrnlIO.setDataKey(SPACES);
		ulnkrnlIO.setChdrcoy(rnlallrec.company);
		ulnkrnlIO.setChdrnum(rnlallrec.chdrnum);
		ulnkrnlIO.setLife(rnlallrec.life);
		ulnkrnlIO.setCoverage(rnlallrec.coverage);
		ulnkrnlIO.setRider(rnlallrec.rider);
		ulnkrnlIO.setPlanSuffix(rnlallrec.planSuffix);
		ulnkrnlIO.setFunction(varcom.readr);
		ulnkrnlIO.setFormat(ulnkrnlrec);
		SmartFileCode.execute(appVars, ulnkrnlIO);
		if (isNE(ulnkrnlIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(ulnkrnlIO.getStatuz());
			syserrrec.params.set(ulnkrnlIO.getParams());
			databaseError580();
		}
		wsaaTaxs.fill("0");
		wsaaAmountHoldersInner.wsaaTotRecd.set(rnlallrec.totrecd);
		wsaaAmountHoldersInner.wsaaAmtRem.set(rnlallrec.covrInstprem);
		wsaaAmountHoldersInner.wsaaPremAvail.set(0);
		wsaaAmountHoldersInner.wsaaTotAnnPrem.set(0);
		wsaaBillfreq.set(rnlallrec.billfreq);
		/* Read the first INCI record for the component.*/
		/* Only unit linked contract will have a corresponding INCI record.*/
		/* There could be more than one INCI reocrd for each coverage/rider*/
		inciIO.setDataKey(SPACES);
		inciIO.setChdrcoy(rnlallrec.company);
		inciIO.setChdrnum(rnlallrec.chdrnum);
		inciIO.setLife(rnlallrec.life);
		inciIO.setCoverage(rnlallrec.coverage);
		inciIO.setRider(rnlallrec.rider);
		inciIO.setPlanSuffix(rnlallrec.planSuffix);
		inciIO.setSeqno(ZERO);
		inciIO.setInciNum(ZERO);
		/* MOVE BEGNH                  TO INCI-FUNCTION.                */
		inciIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		inciIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		inciIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		inciIO.setFormat(incirec);
		SmartFileCode.execute(appVars, inciIO);
		if ((isNE(inciIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(inciIO.getStatuz());
			syserrrec.params.set(inciIO.getParams());
			databaseError580();
		}
		if ((isNE(inciIO.getChdrcoy(), rnlallrec.company))
		|| (isNE(inciIO.getChdrnum(), rnlallrec.chdrnum))
		|| (isNE(inciIO.getLife(), rnlallrec.life))
		|| (isNE(inciIO.getCoverage(), rnlallrec.coverage))
		|| (isNE(inciIO.getRider(), rnlallrec.rider))
		|| (isNE(inciIO.getPlanSuffix(), rnlallrec.planSuffix))) {
			syserrrec.statuz.set(inciIO.getStatuz());
			syserrrec.params.set(inciIO.getParams());
			databaseError580();
		}
		while ( !(isEQ(wsaaAmountHoldersInner.wsaaAmtRem, 0)
		|| isEQ(inciIO.getStatuz(), varcom.endp))) {
			processInci600();
		}
		
		if (isNE(wsaaAmountHoldersInner.wsaaAmtRem, ZERO)) {
			syserrrec.params.set(wsaaAmountHoldersInner.wsaaAmtRem);
			syserrrec.statuz.set(t744);
			systemError570();
		}
		/* Perform to determine the tax.                                   */
		b100ProcessTax();
		/* Update COVRUNL.                                                 */
		covrunlIO.setChdrcoy(rnlallrec.company);
		covrunlIO.setChdrnum(rnlallrec.chdrnum);
		covrunlIO.setLife(rnlallrec.life);
		covrunlIO.setCoverage(rnlallrec.coverage);
		covrunlIO.setRider(rnlallrec.rider);
		covrunlIO.setPlanSuffix(rnlallrec.planSuffix);
		covrunlIO.setFunction(varcom.readh);
		covrunlIO.setFormat("COVRUNLREC");
		SmartFileCode.execute(appVars, covrunlIO);
		if (isNE(covrunlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrunlIO.getParams());
			syserrrec.statuz.set(covrunlIO.getStatuz());
			databaseError580();
		}
		setPrecision(covrunlIO.getCoverageDebt(), 2);
		covrunlIO.setCoverageDebt(add(covrunlIO.getCoverageDebt(), add(wsaaTax[1], wsaaTax[2])));
		covrunlIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrunlIO);
		if (isNE(covrunlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrunlIO.getParams());
			syserrrec.statuz.set(covrunlIO.getStatuz());
			databaseError580();
		}
	}

protected void exit190()
	{
		exitProgram();
	}

protected void processInci600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para601();
					fundAllocation180();
				case readInci690: 
					readInci690();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para601()
	{
		/* BEGNH is replaced by BEGN,so record is not locked,           */
		/* REWRT is not necessary.                                      */
		if (isEQ(inciIO.getDormantFlag(), "Y")) {
			/*     MOVE REWRT       TO INCI-FUNCTION                        */
			/*     CALL 'INCIIO' USING INCI-PARAMS                          */
			/*     IF (INCI-STATUZ NOT = O-K)                               */
			/*         MOVE INCI-STATUZ TO SYSR-STATUZ                      */
			/*         MOVE INCI-PARAMS TO SYSR-PARAMS                      */
			/*         PERFORM 580-DATABASE-ERROR                           */
			/*     END-IF                                                   */
			goTo(GotoLabel.readInci690);
		}
		if (isEQ(inciIO.getCurrPrem(), 0)) {
			/*     MOVE REWRT       TO INCI-FUNCTION                        */
			/*     CALL 'INCIIO' USING INCI-PARAMS                          */
			/*     IF (INCI-STATUZ NOT = O-K)                               */
			/*         MOVE INCI-STATUZ TO SYSR-STATUZ                      */
			/*         MOVE INCI-PARAMS TO SYSR-PARAMS                      */
			/*         PERFORM 580-DATABASE-ERROR                           */
			/*     END-IF                                                   */
			goTo(GotoLabel.readInci690);
		}
		/* Calculate annualised INCI-CURR-PREM*/
		compute(wsaaAmountHoldersInner.wsaaAnnCurrPrem, 2).set(mult(inciIO.getCurrPrem(), wsaaBillfreqNum));
		wsaaAmountHoldersInner.wsaaTotAnnPrem.add(wsaaAmountHoldersInner.wsaaAnnCurrPrem);
		/* If the amount already allocated exceeds the annualised*/
		/* premium, read next INCI*/
		if (isGT(wsaaAmountHoldersInner.wsaaTotRecd, wsaaAmountHoldersInner.wsaaTotAnnPrem)
		|| isEQ(wsaaAmountHoldersInner.wsaaTotRecd, wsaaAmountHoldersInner.wsaaTotAnnPrem)) {
			goTo(GotoLabel.readInci690);
		}
		/* If we are here, the total premium recd so far must be less*/
		/* than the annual premium on the INCI*/
		/* We will allocate enough to this INCI to reach the Annual*/
		/* premium, and if there is any surplus premium left, there must*/
		/* be at least one other INCI on which we allocate the remainder*/
		compute(wsaaAmountHoldersInner.wsaaPremAvail, 0).set(sub(wsaaAmountHoldersInner.wsaaTotAnnPrem, wsaaAmountHoldersInner.wsaaTotRecd));
		if (isLT(wsaaAmountHoldersInner.wsaaAmtRem, wsaaAmountHoldersInner.wsaaPremAvail)
		|| isEQ(wsaaAmountHoldersInner.wsaaAmtRem, wsaaAmountHoldersInner.wsaaPremAvail)) {
			wsaaAmountHoldersInner.wsaaInciPrem.set(wsaaAmountHoldersInner.wsaaAmtRem);
			wsaaAmountHoldersInner.wsaaAmtRem.set(ZERO);
		}
		else {
			wsaaAmountHoldersInner.wsaaInciPrem.set(wsaaAmountHoldersInner.wsaaPremAvail);
			compute(wsaaAmountHoldersInner.wsaaAmtRem, 0).set(sub(wsaaAmountHoldersInner.wsaaAmtRem, wsaaAmountHoldersInner.wsaaPremAvail));
		}
		wsaaAmountHoldersInner.wsaaTotRecd.add(wsaaAmountHoldersInner.wsaaInciPrem);
		wsaaAmountHoldersInner.wsaaInciPerd[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciPerd[2].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciInit[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciInit[2].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciAlloc[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciAlloc[2].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciTot[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciTot[2].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciNInvs[1].set(ZERO);
		wsaaAmountHoldersInner.wsaaInciNInvs[2].set(ZERO);
		/* For each INCI record read, split the instalment into*/
		/* 3 different portions accordingly.*/
		wsaaAmountHoldersInner.wsaaNonInvestPremTot.set(0);
		wsaaAmountHoldersInner.wsaaTotUlSplit.set(0);
		wsaaAmountHoldersInner.wsaaTotIbSplit.set(0);
		wsaaAmountHoldersInner.wsaaAllocInitTot.set(0);
		wsaaAmountHoldersInner.wsaaAllocAccumTot.set(0);
		wsaaAmountHoldersInner.wsaaIndex.set(1);
		while ( !((isGT(wsaaAmountHoldersInner.wsaaIndex, 4))
		|| (isEQ(wsaaAmountHoldersInner.wsaaInciPrem, 0)))) {
			premiumAllocation1000();
		}
		
		/* Rewrite the INCI record once the amount to allocate to this*/
		/* INCI has been gobbled up.*/
		/* MOVE REWRT                  TO INCI-FUNCTION.                */
		inciIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, inciIO);
		if ((isNE(inciIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(inciIO.getStatuz());
			syserrrec.params.set(inciIO.getParams());
			databaseError580();
		}
	}

protected void fundAllocation180()
	{
		/* Calculate initial unit discount factor.*/
		/* Write a UTRN record for each fund split.*/
		initUnitDiscDetails6000();
		wsaaAmountHoldersInner.wsaaIndex2.set(1);
		wsaaAmountHoldersInner.wsaaRunInitTot.set(0);
		wsaaAmountHoldersInner.wsaaRunAccumTot.set(0);
		while ( !((isGT(wsaaAmountHoldersInner.wsaaIndex2, 10))
		|| (isEQ(ulnkrnlIO.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2), SPACES)))) {
			splitAllocation4000();
		}
		
		/* Write a UTRN record for the uninvested premium.*/
		/* IF WSAA-NON-INVEST-PREM-TOT NOT = 0                          */
		/*     MOVE SPACE              TO UTRN-PARAMS                   */
		/*     MOVE 'NVST'             TO UTRN-UNIT-SUB-ACCOUNT         */
		/*     MOVE WSAA-NON-INVEST-PREM-TOT TO UTRN-CONTRACT-AMOUNT    */
		/*     MOVE 0                  TO UTRN-DISCOUNT-FACTOR          */
		/*     PERFORM 5000-SET-UP-WRITE-UTRN.                          */
		if (isNE(wsaaAmountHoldersInner.wsaaNonInvestPremTot, 0)) {
			if (isGT(wsaaAmountHoldersInner.wsaaTotUlSplit, 0)) {
				utrnIO.setParams(SPACES);
				utrnIO.setUnitSubAccount("NVST");
				setPrecision(utrnIO.getContractAmount(), 0);
				utrnIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaNonInvestPremTot, wsaaAmountHoldersInner.wsaaTotUlSplit), 100));
				zrdecplrec.amountIn.set(utrnIO.getContractAmount());
				callRounding8000();
				utrnIO.setContractAmount(zrdecplrec.amountOut);
				utrnIO.setDiscountFactor(0);
				setUpWriteUtrn5000();
				if (isGT(wsaaAmountHoldersInner.wsaaTotIbSplit, 0)) {
					hitrIO.setParams(SPACES);
					setPrecision(hitrIO.getContractAmount(), 2);
					hitrIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaNonInvestPremTot, utrnIO.getContractAmount()));
					setUpWriteHitr5200();
				}
			}
			else {
				hitrIO.setParams(SPACES);
				hitrIO.setContractAmount(wsaaAmountHoldersInner.wsaaNonInvestPremTot);
				setUpWriteHitr5200();
			}
		}
	}

protected void readInci690()
	{
		inciIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, inciIO);
		if ((isNE(inciIO.getStatuz(), varcom.oK))
		&& (isNE(inciIO.getStatuz(), varcom.endp))) {
			syserrrec.statuz.set(inciIO.getStatuz());
			syserrrec.params.set(inciIO.getParams());
			databaseError580();
		}
		if (isEQ(inciIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* BEGNH is replaced by BEGN,so record is not locked,           */
		/* REWRT is not necessary.                                      */
		if ((isNE(inciIO.getChdrcoy(), rnlallrec.company))
		|| (isNE(inciIO.getChdrnum(), rnlallrec.chdrnum))
		|| (isNE(inciIO.getLife(), rnlallrec.life))
		|| (isNE(inciIO.getCoverage(), rnlallrec.coverage))
		|| (isNE(inciIO.getRider(), rnlallrec.rider))
		|| (isNE(inciIO.getPlanSuffix(), rnlallrec.planSuffix))) {
			/*     MOVE REWRT       TO INCI-FUNCTION                        */
			/*     CALL 'INCIIO' USING INCI-PARAMS                          */
			/*     IF (INCI-STATUZ NOT = O-K)                               */
			/*         MOVE INCI-STATUZ TO SYSR-STATUZ                      */
			/*         MOVE INCI-PARAMS TO SYSR-PARAMS                      */
			/*         PERFORM 580-DATABASE-ERROR                           */
			/*     END-IF                                                   */
			inciIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void initialisation200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para201();
					readT6647210();
				case readT5540220: 
					readT5540220();
					readT5645240();
					readT5688250();
					readT5519260();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para201()
	{
		syserrrec.subrname.set(wsaaSubr);
		rnlallrec.statuz.set(varcom.oK);
		varcom.vrcmTime.set(getCobolTime());
	}

protected void readT6647210()
	{
		/* Read T6647 for the relevant details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(tablesInner.t6647);
		itdmIO.setItmfrm(rnlallrec.effdate);
		wsaaT6647Batctrcde.set(rnlallrec.batctrcde);
		wsaaT6647Cnttype.set(rnlallrec.cnttype);
		itdmIO.setItemitem(wsaaT6647Item);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.endp))
		&& (isNE(itdmIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			t6647rec.t6647Rec.set(SPACES);
			t6647rec.procSeqNo.set(0);
			goTo(GotoLabel.readT5540220);
		}
		if ((isNE(itdmIO.getItemcoy(), rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t6647))
		|| (isNE(itdmIO.getItemitem(), wsaaT6647Item))) {
			t6647rec.t6647Rec.set(SPACES);
			t6647rec.procSeqNo.set(0);
			goTo(GotoLabel.readT5540220);
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
	}

protected void readT5540220()
	{
		/* Read T5540 for the general unit linked details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(tablesInner.t5540);
		itdmIO.setItmfrm(rnlallrec.effdate);
		itdmIO.setItemitem(rnlallrec.crtable);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.endp))
		&& (isNE(itdmIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		if ((isNE(itdmIO.getItemcoy(), rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5540))
		|| (isNE(itdmIO.getItemitem(), rnlallrec.crtable))) {
			itdmIO.setGenarea(SPACES);
		}
		t5540rec.t5540Rec.set(itdmIO.getGenarea());
	}

protected void readT5645240()
	{
		/* Read T5645 for the SUB ACC details.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(rnlallrec.company);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			databaseError580();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void readT5688250()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItmfrm(rnlallrec.moniesDate);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(rnlallrec.cnttype);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError580();
		}
		if (isNE(itdmIO.getItemcoy(), rnlallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(), rnlallrec.cnttype)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(rnlallrec.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			systemError570();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void readT5519260()
	{
		/* Read T5519 for initial unit discount details.*/
		if (isEQ(t5540rec.iuDiscBasis, SPACES)) {
			return ;
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(tablesInner.t5519);
		itdmIO.setItmfrm(rnlallrec.effdate);
		itdmIO.setItemitem(t5540rec.iuDiscBasis);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if ((isNE(itdmIO.getItemcoy(), rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5519))
		|| (isNE(itdmIO.getItemitem(), t5540rec.iuDiscBasis))) {
			itdmIO.setItemitem(t5540rec.iuDiscBasis);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g027);
			systemError570();
		}
		t5519rec.t5519Rec.set(itdmIO.getGenarea());
	}

protected void getT5539300()
	{
		para310();
	}

protected void para310()
	{
		/*  Read T5539 for initial unit discount factor for term.*/
		if (isEQ(t5540rec.iuDiscFact, SPACES)) {
			return ;
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(rnlallrec.company);
		itemIO.setItemtabl(tablesInner.t5539);
		itemIO.setItemitem(t5540rec.iuDiscFact);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			databaseError580();
		}
		t5539rec.t5539Rec.set(itemIO.getGenarea());
	}

protected void getT6646400()
	{
		para410();
	}

protected void para410()
	{
		/* Read T5546 for initial unit discount factor for whole of life.*/
		if (isEQ(t5540rec.wholeIuDiscFact, SPACES)) {
			return ;
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(rnlallrec.company);
		itemIO.setItemtabl(tablesInner.t6646);
		itemIO.setItemitem(t5540rec.wholeIuDiscFact);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			databaseError580();
		}
		t6646rec.t6646Rec.set(itemIO.getGenarea());
	}

protected void systemError570()
	{
		para570();
		exit579();
	}

protected void para570()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit579()
	{
		rnlallrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError580()
	{
		para580();
		exit589();
	}

protected void para580()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit589()
	{
		rnlallrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void premiumAllocation1000()
	{
		para1010();
		nearExit1800();
	}

protected void para1010()
	{
		/* WSAA-ALLOC-AMT is the amount to be allocated to buy units.*/
		/* It is not necessary to be the same as the premium.(Depends*/
		/* on INCI details.)*/
		if (isEQ(inciIO.getPremcurr(wsaaAmountHoldersInner.wsaaIndex), 0)) {
			return ;
		}
		if (isGTE(wsaaAmountHoldersInner.wsaaInciPrem, inciIO.getPremcurr(wsaaAmountHoldersInner.wsaaIndex))) {
			wsaaAmountHoldersInner.wsaaAllocAmt.set(inciIO.getPremcurr(wsaaAmountHoldersInner.wsaaIndex));
		}
		if (isGT(inciIO.getPremcurr(wsaaAmountHoldersInner.wsaaIndex), wsaaAmountHoldersInner.wsaaInciPrem)) {
			wsaaAmountHoldersInner.wsaaAllocAmt.set(wsaaAmountHoldersInner.wsaaInciPrem);
		}
		calculateSplit2000();
	}

protected void nearExit1800()
	{
		wsaaAmountHoldersInner.wsaaIndex.add(1);
		/*EXIT*/
	}

protected void calculateSplit2000()
	{
		para2010();
	}

protected void para2010()
	{
		/* Calculate the uninvested allocation.*/
		compute(wsaaAmountHoldersInner.wsaaNonInvestPrem, 1).setRounded(div((mult(wsaaAmountHoldersInner.wsaaAllocAmt, (sub(100, inciIO.getPcunit(wsaaAmountHoldersInner.wsaaIndex))))), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		callRounding8000();
		wsaaAmountHoldersInner.wsaaNonInvestPrem.set(zrdecplrec.amountOut);
		/* Calculate the allocation for buying initial units.*/
		compute(wsaaAmountHoldersInner.wsaaAllocInit, 1).setRounded(div(mult((sub(wsaaAmountHoldersInner.wsaaAllocAmt, wsaaAmountHoldersInner.wsaaNonInvestPrem)), inciIO.getUsplitpc(wsaaAmountHoldersInner.wsaaIndex)), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocInit);
		callRounding8000();
		wsaaAmountHoldersInner.wsaaAllocInit.set(zrdecplrec.amountOut);
		/* Calculate the allocation for buying accumulation units.*/
		compute(wsaaAmountHoldersInner.wsaaAllocAccum, 1).setRounded(sub(wsaaAmountHoldersInner.wsaaAllocAmt, (add(wsaaAmountHoldersInner.wsaaNonInvestPrem, wsaaAmountHoldersInner.wsaaAllocInit))));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocAccum);
		callRounding8000();
		wsaaAmountHoldersInner.wsaaAllocAccum.set(zrdecplrec.amountOut);
		/* Accumulate totals.*/
		wsaaAmountHoldersInner.wsaaNonInvestPremTot.add(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		wsaaAmountHoldersInner.wsaaAllocInitTot.add(wsaaAmountHoldersInner.wsaaAllocInit);
		wsaaAmountHoldersInner.wsaaAllocAccumTot.add(wsaaAmountHoldersInner.wsaaAllocAccum);
		/* Update the premium left to pay for that period.*/
		setPrecision(inciIO.getPremcurr(wsaaAmountHoldersInner.wsaaIndex), 1);
		inciIO.setPremcurr(wsaaAmountHoldersInner.wsaaIndex, sub(inciIO.getPremcurr(wsaaAmountHoldersInner.wsaaIndex), wsaaAmountHoldersInner.wsaaAllocAmt), true);
		/* Store the INCI amounts in working storage.*/
		if (isEQ(wsaaAmountHoldersInner.wsaaInciPerd[1], ZERO)) {
			wsaaAmountHoldersInner.wsaaInciPerd[1].set(wsaaAmountHoldersInner.wsaaIndex);
			wsaaAmountHoldersInner.wsaaInciInit[1].add(wsaaAmountHoldersInner.wsaaAllocInit);
			wsaaAmountHoldersInner.wsaaInciAlloc[1].add(wsaaAmountHoldersInner.wsaaAllocAccum);
			wsaaAmountHoldersInner.wsaaInciNInvs[1].add(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		}
		else {
			wsaaAmountHoldersInner.wsaaInciPerd[2].set(wsaaAmountHoldersInner.wsaaIndex);
			wsaaAmountHoldersInner.wsaaInciInit[2].add(wsaaAmountHoldersInner.wsaaAllocInit);
			wsaaAmountHoldersInner.wsaaInciAlloc[2].add(wsaaAmountHoldersInner.wsaaAllocAccum);
			wsaaAmountHoldersInner.wsaaInciNInvs[2].add(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		}
		/* If the calculated amount of premium for this INCI has been*/
		/* fully allocated, rewrite the INCI record. Otherwise, reduce*/
		/* WSAA-INCI-PREM by the allocated amount and go round the loop*/
		/* again.*/
		compute(wsaaAmountHoldersInner.wsaaInciPrem, 0).set(sub(wsaaAmountHoldersInner.wsaaInciPrem, wsaaAmountHoldersInner.wsaaAllocAmt));
	}

protected void splitAllocation4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initUnit4100();
				case next4150: 
					next4150();
				case accumUnit4200: 
					accumUnit4200();
				case next4250: 
					next4250();
				case nearExit4800: 
					nearExit4800();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initUnit4100()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItmfrm(rnlallrec.effdate);
		itdmIO.setItemitem(ulnkrnlIO.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2));
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.endp)
		&& isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError580();
		}
		if (isNE(itdmIO.getItemcoy(), rnlallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5515)
		|| isNE(itdmIO.getItemitem(), ulnkrnlIO.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2))
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isEQ(t5515rec.zfundtyp, "D")) {
			compute(wsaaAmountHoldersInner.wsaaAllocTot, 0).set(add(wsaaAmountHoldersInner.wsaaAllocInitTot, wsaaAmountHoldersInner.wsaaAllocAccumTot));
			wsaaAmountHoldersInner.wsaaTotIbSplit.add(ulnkrnlIO.getUalprc(wsaaAmountHoldersInner.wsaaIndex2));
			if (isNE(wsaaAmountHoldersInner.wsaaAllocTot, 0)) {
				calcHitrAmount5100();
				setUpWriteHitr5200();
			}
			goTo(GotoLabel.nearExit4800);
		}
		wsaaAmountHoldersInner.wsaaTotUlSplit.add(ulnkrnlIO.getUalprc(wsaaAmountHoldersInner.wsaaIndex2));
		/* To avoid rounding error, use the remainder(total investment -*/
		/* invested so far) to invest on the last fund.*/
		/* Invest the initial units premium in the funds specified in*/
		/* the ULNKRNL record. Write a UTRN record for each fund.*/
		if (isEQ(wsaaAmountHoldersInner.wsaaAllocInitTot, 0)) {
			goTo(GotoLabel.accumUnit4200);
		}
		utrnIO.setParams(SPACES);
		if (isEQ(wsaaAmountHoldersInner.wsaaIndex2, 10)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocInitTot, wsaaAmountHoldersInner.wsaaRunInitTot));
			goTo(GotoLabel.next4150);
		}
		if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaAmountHoldersInner.wsaaIndex2, 1)), SPACES)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocInitTot, wsaaAmountHoldersInner.wsaaRunInitTot));
		}
		else {
			setPrecision(utrnIO.getContractAmount(), 1);
			utrnIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocInitTot, ulnkrnlIO.getUalprc(wsaaAmountHoldersInner.wsaaIndex2)), 100), true);
		}
	}

protected void next4150()
	{
		zrdecplrec.amountIn.set(utrnIO.getContractAmount());
		callRounding8000();
		utrnIO.setContractAmount(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaRunInitTot.add(utrnIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaRunTot.add(utrnIO.getContractAmount());
		utrnIO.setUnitVirtualFund(ulnkrnlIO.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2));
		utrnIO.setUnitType("I");
		utrnIO.setUnitSubAccount("INIT");
		utrnIO.setDiscountFactor(wsaaAmountHoldersInner.wsaaInitUnitDiscFact);
		setUpWriteUtrn5000();
	}

	/**
	* <pre>
	* Invest the accumulation units premium in the funds specified in
	* the ULNKRNL record. Write a UTRN record for each fund.
	* </pre>
	*/
protected void accumUnit4200()
	{
		if (isEQ(wsaaAmountHoldersInner.wsaaAllocAccumTot, 0)) {
			goTo(GotoLabel.nearExit4800);
		}
		utrnIO.setParams(SPACES);
		if (isEQ(wsaaAmountHoldersInner.wsaaIndex2, 10)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocAccumTot, wsaaAmountHoldersInner.wsaaRunAccumTot));
			goTo(GotoLabel.next4250);
		}
		if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaAmountHoldersInner.wsaaIndex2, 1)), SPACES)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocAccumTot, wsaaAmountHoldersInner.wsaaRunAccumTot));
		}
		else {
			setPrecision(utrnIO.getContractAmount(), 1);
			utrnIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocAccumTot, ulnkrnlIO.getUalprc(wsaaAmountHoldersInner.wsaaIndex2)), 100), true);
		}
	}

protected void next4250()
	{
		wsaaAmountHoldersInner.wsaaRunAccumTot.add(utrnIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaRunTot.add(utrnIO.getContractAmount());
		utrnIO.setUnitVirtualFund(ulnkrnlIO.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2));
		utrnIO.setUnitType("A");
		utrnIO.setUnitSubAccount("ACUM");
		utrnIO.setDiscountFactor(0);
		setUpWriteUtrn5000();
	}

protected void nearExit4800()
	{
		wsaaAmountHoldersInner.wsaaIndex2.add(1);
		/*EXIT*/
	}

protected void setUpWriteUtrn5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para5100();
				case comlvlacc5105: 
					comlvlacc5105();
				case cont5110: 
					cont5110();
				case writrUtrn5800: 
					writrUtrn5800();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para5100()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItmfrm(rnlallrec.effdate);
		itdmIO.setItemitem(utrnIO.getUnitVirtualFund());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.endp))
		&& (isNE(itdmIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			databaseError580();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		if ((isNE(itdmIO.getItemcoy(), rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5515))
		|| (isNE(itdmIO.getItemitem(), utrnIO.getUnitVirtualFund()))) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		utrnIO.setChdrcoy(rnlallrec.company);
		utrnIO.setChdrnum(rnlallrec.chdrnum);
		utrnIO.setCoverage(rnlallrec.coverage);
		utrnIO.setLife(rnlallrec.life);
		utrnIO.setRider(rnlallrec.rider);
		utrnIO.setPlanSuffix(rnlallrec.planSuffix);
		utrnIO.setTranno(rnlallrec.tranno);
		utrnIO.setTransactionTime(varcom.vrcmTime);
		utrnIO.setTransactionDate(rnlallrec.effdate);
		utrnIO.setUser(rnlallrec.user);
		utrnIO.setBatccoy(rnlallrec.batccoy);
		utrnIO.setBatcbrn(rnlallrec.batcbrn);
		utrnIO.setBatcactyr(rnlallrec.batcactyr);
		utrnIO.setBatcactmn(rnlallrec.batcactmn);
		utrnIO.setBatctrcde(rnlallrec.batctrcde);
		utrnIO.setBatcbatch(rnlallrec.batcbatch);
		utrnIO.setCrtable(rnlallrec.crtable);
		utrnIO.setCntcurr(rnlallrec.cntcurr);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			goTo(GotoLabel.comlvlacc5105);
		}
		if (isEQ(utrnIO.getUnitSubAccount(), "NVST")) {
			utrnIO.setSacscode(t5645rec.sacscode02);
			utrnIO.setSacstyp(t5645rec.sacstype02);
			utrnIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode01);
			utrnIO.setSacstyp(t5645rec.sacstype01);
			utrnIO.setGenlcde(t5645rec.glmap01);
		}
		goTo(GotoLabel.cont5110);
	}

protected void comlvlacc5105()
	{
		if (isEQ(utrnIO.getUnitSubAccount(), "NVST")) {
			utrnIO.setSacscode(t5645rec.sacscode04);
			utrnIO.setSacstyp(t5645rec.sacstype04);
			utrnIO.setGenlcde(t5645rec.glmap04);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode03);
			utrnIO.setSacstyp(t5645rec.sacstype03);
			utrnIO.setGenlcde(t5645rec.glmap03);
		}
	}

protected void cont5110()
	{
		utrnIO.setContractType(rnlallrec.cnttype);
		utrnIO.setSvp(1);
		utrnIO.setCrComDate(rnlallrec.crdate);
		utrnIO.setFundCurrency(t5515rec.currcode);
		if (isLT(utrnIO.getContractAmount(), 0)) {
			utrnIO.setNowDeferInd(t6647rec.dealin);
		}
		else {
			utrnIO.setNowDeferInd(t6647rec.aloind);
		}
		/* Calculate the monies date.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaMoniesDate.set(datcon1rec.intDate);
		if (isEQ(t6647rec.efdcode, "BD")) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(t6647rec.efdcode, "DD")) {
				wsaaMoniesDate.set(rnlallrec.duedate);
			}
			else {
				if (isEQ(t6647rec.efdcode, "RD")) {
					wsaaMoniesDate.set(rnlallrec.moniesDate);
				}
				else {
					if (isEQ(t6647rec.efdcode, "LO")) {
						if (isGT(rnlallrec.duedate, wsaaMoniesDate)) {
							wsaaMoniesDate.set(rnlallrec.duedate);
						}
					}
				}
			}
		}
		utrnIO.setMoniesDate(wsaaMoniesDate);
		utrnIO.setProcSeqNo(t6647rec.procSeqNo);
		utrnIO.setJobnoPrice(0);
		utrnIO.setFundRate(0);
		utrnIO.setStrpdate(0);
		utrnIO.setNofUnits(0);
		utrnIO.setNofDunits(0);
		utrnIO.setPriceDateUsed(0);
		utrnIO.setPriceUsed(0);
		utrnIO.setUnitBarePrice(0);
		utrnIO.setFundAmount(0);
		utrnIO.setSurrenderPercent(0);
		utrnIO.setUstmno(0);
		utrnIO.setInciprm01(0);
		utrnIO.setInciprm02(0);
		utrnIO.setInciNum(inciIO.getSeqno());
		utrnIO.setInciPerd01(wsaaAmountHoldersInner.wsaaInciPerd[1]);
		utrnIO.setInciPerd02(wsaaAmountHoldersInner.wsaaInciPerd[2]);
		/* Use the amounts previously stored for each allocation*/
		/* period rather than the  whole contract amounts.*/
		if (isEQ(utrnIO.getUnitSubAccount(), "NVST")) {
			utrnIO.setInciprm01(wsaaAmountHoldersInner.wsaaInciNInvs[1]);
			utrnIO.setInciprm02(wsaaAmountHoldersInner.wsaaInciNInvs[2]);
			goTo(GotoLabel.writrUtrn5800);
		}
		if (isEQ(utrnIO.getUnitSubAccount(), "INIT")) {
			compute(wsaaAmountHoldersInner.wsaaFundPc, 3).setRounded(mult((div(utrnIO.getContractAmount(), wsaaAmountHoldersInner.wsaaAllocInitTot)), 100));
			if (isGT(wsaaAmountHoldersInner.wsaaInciInit[1], ZERO)) {
				setPrecision(utrnIO.getInciprm01(), 1);
				utrnIO.setInciprm01(div((mult(wsaaAmountHoldersInner.wsaaInciInit[1], wsaaAmountHoldersInner.wsaaFundPc)), 100), true);
			}
			zrdecplrec.amountIn.set(utrnIO.getInciprm01());
			callRounding8000();
			utrnIO.setInciprm01(zrdecplrec.amountOut);
			if (isGT(wsaaAmountHoldersInner.wsaaInciInit[2], ZERO)) {
				setPrecision(utrnIO.getInciprm02(), 1);
				utrnIO.setInciprm02(div((mult(wsaaAmountHoldersInner.wsaaInciInit[2], wsaaAmountHoldersInner.wsaaFundPc)), 100), true);
				zrdecplrec.amountIn.set(utrnIO.getInciprm02());
				callRounding8000();
				utrnIO.setInciprm02(zrdecplrec.amountOut);
			}
			goTo(GotoLabel.writrUtrn5800);
		}
		if (isEQ(utrnIO.getUnitSubAccount(), "ACUM")) {
			compute(wsaaAmountHoldersInner.wsaaFundPc, 3).setRounded(mult((div(utrnIO.getContractAmount(), wsaaAmountHoldersInner.wsaaAllocAccumTot)), 100));
			if (isGT(wsaaAmountHoldersInner.wsaaInciAlloc[1], ZERO)) {
				setPrecision(utrnIO.getInciprm01(), 1);
				utrnIO.setInciprm01(div((mult(wsaaAmountHoldersInner.wsaaInciAlloc[1], wsaaAmountHoldersInner.wsaaFundPc)), 100), true);
				zrdecplrec.amountIn.set(utrnIO.getInciprm01());
				callRounding8000();
				utrnIO.setInciprm01(zrdecplrec.amountOut);
			}
			if (isGT(wsaaAmountHoldersInner.wsaaInciAlloc[2], ZERO)) {
				setPrecision(utrnIO.getInciprm02(), 1);
				utrnIO.setInciprm02(div((mult(wsaaAmountHoldersInner.wsaaInciAlloc[2], wsaaAmountHoldersInner.wsaaFundPc)), 100), true);
				zrdecplrec.amountIn.set(utrnIO.getInciprm02());
				callRounding8000();
				utrnIO.setInciprm02(zrdecplrec.amountOut);
			}
		}
	}

protected void writrUtrn5800()
	{
		utrnIO.setFunction(varcom.writr);
		utrnIO.setFormat(utrnrec);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(utrnIO.getStatuz());
			syserrrec.params.set(utrnIO.getParams());
			databaseError580();
		}
		/*EXIT*/
	}

protected void calcHitrAmount5100()
	{
		calc5110();
	}

protected void calc5110()
	{
		hitrIO.setParams(SPACES);
		if (isEQ(wsaaAmountHoldersInner.wsaaIndex2, 10)) {
			setPrecision(hitrIO.getContractAmount(), 0);
			hitrIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocTot, wsaaAmountHoldersInner.wsaaRunTot));
		}
		else {
			if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaAmountHoldersInner.wsaaIndex2, 1)), SPACES)) {
				setPrecision(hitrIO.getContractAmount(), 0);
				hitrIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocTot, wsaaAmountHoldersInner.wsaaRunTot));
			}
			else {
				setPrecision(hitrIO.getContractAmount(), 1);
				hitrIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocTot, ulnkrnlIO.getUalprc(wsaaAmountHoldersInner.wsaaIndex2)), 100), true);
				compute(wsaaAmountHoldersInner.wsaaInitAmount, 1).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocInitTot, ulnkrnlIO.getUalprc(wsaaAmountHoldersInner.wsaaIndex2)), 100));
				compute(wsaaAmountHoldersInner.wsaaAccumAmount, 1).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocAccumTot, ulnkrnlIO.getUalprc(wsaaAmountHoldersInner.wsaaIndex2)), 100));
			}
		}
		wsaaAmountHoldersInner.wsaaRunTot.add(hitrIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaRunInitTot.add(wsaaAmountHoldersInner.wsaaInitAmount);
		wsaaAmountHoldersInner.wsaaRunAccumTot.add(wsaaAmountHoldersInner.wsaaAccumAmount);
		hitrIO.setZintbfnd(ulnkrnlIO.getUalfnd(wsaaAmountHoldersInner.wsaaIndex2));
	}

protected void setUpWriteHitr5200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para5210();
				case writrHitr5250: 
					writrHitr5250();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para5210()
	{
		hitrIO.setChdrcoy(rnlallrec.company);
		hitrIO.setChdrnum(rnlallrec.chdrnum);
		hitrIO.setCoverage(rnlallrec.coverage);
		hitrIO.setLife(rnlallrec.life);
		hitrIO.setRider("00");
		hitrIO.setPlanSuffix(rnlallrec.planSuffix);
		hitrIO.setTranno(rnlallrec.tranno);
		hitrIO.setBatccoy(rnlallrec.batccoy);
		hitrIO.setBatcbrn(rnlallrec.batcbrn);
		hitrIO.setBatcactyr(rnlallrec.batcactyr);
		hitrIO.setBatcactmn(rnlallrec.batcactmn);
		hitrIO.setBatctrcde(rnlallrec.batctrcde);
		hitrIO.setBatcbatch(rnlallrec.batcbatch);
		hitrIO.setCrtable(rnlallrec.crtable);
		hitrIO.setCntcurr(rnlallrec.cntcurr);
		if (isEQ(hitrIO.getZintbfnd(), SPACES)) {
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				hitrIO.setSacscode(t5645rec.sacscode08);
				hitrIO.setSacstyp(t5645rec.sacstype08);
				hitrIO.setGenlcde(t5645rec.glmap08);
			}
			else {
				hitrIO.setSacscode(t5645rec.sacscode06);
				hitrIO.setSacstyp(t5645rec.sacstype06);
				hitrIO.setGenlcde(t5645rec.glmap06);
			}
		}
		else {
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				hitrIO.setSacscode(t5645rec.sacscode07);
				hitrIO.setSacstyp(t5645rec.sacstype07);
				hitrIO.setGenlcde(t5645rec.glmap07);
			}
			else {
				hitrIO.setSacscode(t5645rec.sacscode05);
				hitrIO.setSacstyp(t5645rec.sacstype05);
				hitrIO.setGenlcde(t5645rec.glmap05);
			}
		}
		hitrIO.setCnttyp(rnlallrec.cnttype);
		hitrIO.setSvp(1);
		hitrIO.setFundCurrency(t5515rec.currcode);
		hitrIO.setProcSeqNo(t6647rec.procSeqNo);
		hitrIO.setFundAmount(ZERO);
		hitrIO.setSurrenderPercent(ZERO);
		hitrIO.setUstmno(ZERO);
		hitrIO.setInciprm01(ZERO);
		hitrIO.setInciprm02(ZERO);
		hitrIO.setFundRate(ZERO);
		hitrIO.setInciNum(inciIO.getSeqno());
		hitrIO.setInciPerd01(wsaaAmountHoldersInner.wsaaInciPerd[1]);
		hitrIO.setInciPerd02(wsaaAmountHoldersInner.wsaaInciPerd[2]);
		/* Use the amounts previously stored for each allocation           */
		/* period rather than the  whole contract amounts.                 */
		if (isEQ(hitrIO.getZintbfnd(), SPACES)) {
			hitrIO.setInciprm01(wsaaAmountHoldersInner.wsaaInciNInvs[1]);
			hitrIO.setInciprm02(wsaaAmountHoldersInner.wsaaInciNInvs[2]);
			goTo(GotoLabel.writrHitr5250);
		}
		compute(wsaaAmountHoldersInner.wsaaInciTot[1], 0).set(add(wsaaAmountHoldersInner.wsaaInciInit[1], wsaaAmountHoldersInner.wsaaInciAlloc[1]));
		compute(wsaaAmountHoldersInner.wsaaInciTot[2], 0).set(add(wsaaAmountHoldersInner.wsaaInciInit[2], wsaaAmountHoldersInner.wsaaInciAlloc[2]));
		compute(wsaaAmountHoldersInner.wsaaFundPc, 3).setRounded(mult(div(hitrIO.getContractAmount(), wsaaAmountHoldersInner.wsaaAllocTot), 100));
		if (isGT(wsaaAmountHoldersInner.wsaaInciTot[1], ZERO)) {
			setPrecision(hitrIO.getInciprm01(), 1);
			hitrIO.setInciprm01(div(mult(wsaaAmountHoldersInner.wsaaInciTot[1], wsaaAmountHoldersInner.wsaaFundPc), 100), true);
			zrdecplrec.amountIn.set(hitrIO.getInciprm01());
			callRounding8000();
			hitrIO.setInciprm01(zrdecplrec.amountOut);
		}
		if (isGT(wsaaAmountHoldersInner.wsaaInciTot[2], ZERO)) {
			setPrecision(hitrIO.getInciprm02(), 1);
			hitrIO.setInciprm02(div(mult(wsaaAmountHoldersInner.wsaaInciTot[2], wsaaAmountHoldersInner.wsaaFundPc), 100), true);
			zrdecplrec.amountIn.set(hitrIO.getInciprm02());
			callRounding8000();
			hitrIO.setInciprm02(zrdecplrec.amountOut);
		}
	}

protected void writrHitr5250()
	{
		if (isEQ(hitrIO.getZintbfnd(), SPACES)) {
			hitrIO.setZrectyp("N");
		}
		else {
			hitrIO.setZrectyp("P");
		}
		hitrIO.setEffdate(rnlallrec.effdate);
		hitrIO.setZintappind(SPACES);
		hitrIO.setZlstintdte(varcom.vrcmMaxDate);
		hitrIO.setZinteffdt(varcom.vrcmMaxDate);
		hitrIO.setZintrate(ZERO);
		hitrIO.setFunction(varcom.writr);
		hitrIO.setFormat(hitrrec);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrIO.getParams());
			syserrrec.statuz.set(hitrIO.getStatuz());
			databaseError580();
		}
	}

protected void initUnitDiscDetails6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para6010();
					termToRun6100();
				case fixedTerm6200: 
					fixedTerm6200();
				case wholeOfLife6300: 
					wholeOfLife6300();
				case exit6900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para6010()
	{
		wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(1);
		if (((isEQ(t5540rec.iuDiscBasis, SPACES))
		|| (isEQ(t5540rec.iuDiscFact, SPACES)))
		&& (isEQ(t5540rec.wholeIuDiscFact, SPACES))) {
			goTo(GotoLabel.exit6900);
		}
	}

	/**
	* <pre>
	* There are 3 methods to obtain the discount factor, which one to
	* use is dependent on the field entries in T5519 & T5540 which
	* has been read in initialisation section.(Only one method
	* allowed.)
	* </pre>
	*/
protected void termToRun6100()
	{
		/* If whole of life discount factor(T5540-WHOLE-IU-DISC-FACT) is*/
		/* blank,(i.e. term discount basis not blank) and if fixed term*/
		/* (T5519-FIXTRM) is zero, use this the term left to run to read*/
		/* off T5539 to obtain the discount factor.*/
		if (isNE(t5540rec.wholeIuDiscFact, SPACES)) {
			goTo(GotoLabel.wholeOfLife6300);
		}
		if (isNE(t5519rec.fixdtrm, 0)) {
			goTo(GotoLabel.fixedTerm6200);
		}
		if (isEQ(rnlallrec.termLeftToRun, 0)) {
			goTo(GotoLabel.exit6900);
		}
		getT5539300();
		/*    MOVE T5539-DFACT(RNLA-TERM-LEFT-TO-RUN) TO                   */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (isLT(rnlallrec.termLeftToRun, 100)) {
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.dfact[rnlallrec.termLeftToRun.toInt()]);
		}
		else {
			compute(wsaaFact, 0).set(sub(rnlallrec.termLeftToRun, 99));
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.fact[wsaaFact.toInt()]);
		}
		compute(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 1).setRounded(div(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 100000));
		goTo(GotoLabel.exit6900);
	}

protected void fixedTerm6200()
	{
		/* Use this fixed term to read off T5539 to obtain the*/
		/* discount factor.*/
		getT5539300();
		/*    MOVE T5539-DFACT(T5519-FIXDTRM) TO                           */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (isLT(t5519rec.fixdtrm, 100)) {
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.dfact[t5519rec.fixdtrm.toInt()]);
		}
		else {
			compute(wsaaFact, 0).set(sub(t5519rec.fixdtrm, 99));
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.fact[wsaaFact.toInt()]);
		}
		compute(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 1).setRounded(div(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 100000));
		goTo(GotoLabel.exit6900);
	}

protected void wholeOfLife6300()
	{
		/* Use the age at next birthday to read off T6646 to obtain*/
		/* the discount factor.*/
		if (isEQ(rnlallrec.anbAtCcd, 0)) {
			return ;
		}
		getT6646400();
		/*    MOVE T6646-DFACT(RNLA-ANB-AT-CCD) TO                         */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (isLT(rnlallrec.anbAtCcd, 100)) {
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t6646rec.dfact[rnlallrec.anbAtCcd.toInt()]);
		}
		else {
			compute(wsaaFact, 0).set(sub(rnlallrec.anbAtCcd, 99));
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t6646rec.fact[wsaaFact.toInt()]);
		}
		compute(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 1).setRounded(div(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 100000));
		return ;
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(rnlallrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(rnlallrec.cntcurr);
		zrdecplrec.batctrcde.set(rnlallrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			databaseError580();
		}
		/*EXIT*/
	}

protected void b100ProcessTax()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					b100Start();
					b100CreateTaxd();
				case b100CreateZrst: 
					b100CreateZrst();
				case b100Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b100Start()
	{
		if (isEQ(wsaaAmountHoldersInner.wsaaNonInvestPrem, 0)) {
			goTo(GotoLabel.b100Exit);
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(rnlallrec.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrlnbIO.getRegister());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError580();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(rnlallrec.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				databaseError580();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isEQ(tr52drec.txcode, SPACES)) {
			goTo(GotoLabel.b100Exit);
		}
		/* Read table TR52E                                                */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(rnlallrec.company);
		itemIO.setItemtabl(tablesInner.tr52e);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set(covrunlIO.getCrtable());
		itemIO.setItemitem(wsaaTr52eKey);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError580();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(rnlallrec.company);
			itemIO.setItemtabl(tablesInner.tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				databaseError580();
			}
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(rnlallrec.company);
			itemIO.setItemtabl(tablesInner.tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				databaseError580();
			}
		}
		tr52erec.tr52eRec.set(itemIO.getGenarea());
		if (isNE(tr52erec.taxind05, "Y")) {
			goTo(GotoLabel.b100Exit);
		}
		txcalcrec.function.set("CPST");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		txcalcrec.life.set(covrunlIO.getLife());
		txcalcrec.coverage.set(covrunlIO.getCoverage());
		txcalcrec.rider.set(covrunlIO.getRider());
		txcalcrec.planSuffix.set(covrunlIO.getPlanSuffix());
		txcalcrec.crtable.set(covrunlIO.getCrtable());
		txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
		txcalcrec.register.set(chdrlnbIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.batckey.set(rnlallrec.batchkey);
		txcalcrec.tranno.set(chdrlnbIO.getTranno());
		txcalcrec.language.set(rnlallrec.language);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
		wsaaCntCurr.set(chdrlnbIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.transType.set("NINV");
		txcalcrec.amountIn.set(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		txcalcrec.effdate.set(rnlallrec.effdate);
		txcalcrec.tranno.set(chdrlnbIO.getTranno());
		txcalcrec.jrnseq.set(0);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			databaseError580();
		}
		if (isEQ(txcalcrec.taxAmt[1], 0)
		&& isEQ(txcalcrec.taxAmt[2], 0)) {
			goTo(GotoLabel.b100Exit);
		}
	}

protected void b100CreateTaxd()
	{
		/*   Tax on non invested premium may be calcuated on both reg     */
		/*   premium and singple premium in case where there is lump sum  */
		/*   at issue.  We need to add the tax on single premium to       */
		/*   the reg prem non invest tax if the records has alread been   */
		/*   created.                                                     */
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(covrunlIO.getChdrcoy());
		taxdIO.setChdrnum(covrunlIO.getChdrnum());
		taxdIO.setLife(covrunlIO.getLife());
		taxdIO.setCoverage(covrunlIO.getCoverage());
		taxdIO.setRider(covrunlIO.getRider());
		taxdIO.setPlansfx(ZERO);
		taxdIO.setInstfrom(rnlallrec.effdate);
		taxdIO.setTrantype("NINV");
		taxdIO.setFunction(varcom.readh);
		taxdIO.setFormat(taxdrec);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)
		&& isNE(taxdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			databaseError580();
		}
		if (isEQ(taxdIO.getStatuz(), varcom.oK)) {
			setPrecision(taxdIO.getBaseamt(), 2);
			taxdIO.setBaseamt(add(taxdIO.getBaseamt(), txcalcrec.amountIn));
			setPrecision(taxdIO.getTaxamt01(), 2);
			taxdIO.setTaxamt01(add(taxdIO.getTaxamt01(), txcalcrec.taxAmt[1]));
			setPrecision(taxdIO.getTaxamt02(), 2);
			taxdIO.setTaxamt02(add(taxdIO.getTaxamt02(), txcalcrec.taxAmt[2]));
			taxdIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, taxdIO);
			if (isNE(taxdIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(taxdIO.getParams());
				syserrrec.statuz.set(taxdIO.getStatuz());
				databaseError580();
			}
			goTo(GotoLabel.b100CreateZrst);
		}
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(covrunlIO.getChdrcoy());
		taxdIO.setChdrnum(covrunlIO.getChdrnum());
		taxdIO.setLife(covrunlIO.getLife());
		taxdIO.setCoverage(covrunlIO.getCoverage());
		taxdIO.setRider(covrunlIO.getRider());
		taxdIO.setPlansfx(ZERO);
		taxdIO.setEffdate(rnlallrec.effdate);
		taxdIO.setInstfrom(rnlallrec.effdate);
		taxdIO.setInstto(varcom.vrcmMaxDate);
		wsaaTranref.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(wsaaTranref);
		taxdIO.setTranref(wsaaTranref);
		taxdIO.setTranno(chdrlnbIO.getTranno());
		taxdIO.setTrantype("NINV");
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(ZERO);
		taxdIO.setBillcd(chdrlnbIO.getBillcd());
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxabsind03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setPostflg("P");
		taxdIO.setFormat(taxdrec);
		taxdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			databaseError580();
		}
	}

protected void b100CreateZrst()
	{
		if (isEQ(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaTax[1].set(0);
		}
		else {
			wsaaTax[1].set(txcalcrec.taxAmt[1]);
		}
		if (isEQ(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaTax[2].set(0);
		}
		else {
			wsaaTax[2].set(txcalcrec.taxAmt[2]);
		}
		zrstIO.setDataArea(SPACES);
		zrstIO.setChdrcoy(covrunlIO.getChdrcoy());
		zrstIO.setChdrnum(covrunlIO.getChdrnum());
		zrstIO.setLife(covrunlIO.getLife());
		zrstIO.setCoverage(SPACES);
		zrstIO.setRider(SPACES);
		zrstIO.setFormat(zrstrec);
		zrstIO.setFunction(varcom.begn);
		while ( !(isEQ(zrstIO.getStatuz(), varcom.endp))) {
			b200ReadZrst();
		}
		
		compute(wsaaTotCd, 2).add(add(wsaaTax[1], wsaaTax[2]));
		b300WriteZrst();
		if (isEQ(wsaaTax[1], ZERO)
		&& isEQ(wsaaTax[2], ZERO)) {
			return ;
		}
		zrstIO.setDataArea(SPACES);
		zrstIO.setChdrcoy(covrunlIO.getChdrcoy());
		zrstIO.setChdrnum(covrunlIO.getChdrnum());
		zrstIO.setLife(covrunlIO.getLife());
		zrstIO.setCoverage(SPACES);
		zrstIO.setRider(SPACES);
		zrstIO.setFormat(zrstrec);
		zrstIO.setFunction(varcom.begn);
		while ( !(isEQ(zrstIO.getStatuz(), varcom.endp))) {
			b400UpdateZrst();
		}
		
		/* Post coverage debt                                              */
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(rnlallrec.company);
		wsaaItemkey.itemItemtabl.set(tablesInner.t1688);
		wsaaItemkey.itemItemitem.set(rnlallrec.batctrcde);
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set(rnlallrec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			getdescrec.longdesc.set(SPACES);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account    */
		/* movements.                                                      */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(rnlallrec.chdrnum);
		lifacmvrec.jrnseq.set(txcalcrec.jrnseq);
		lifacmvrec.batccoy.set(rnlallrec.company);
		lifacmvrec.rldgcoy.set(rnlallrec.company);
		lifacmvrec.genlcoy.set(rnlallrec.company);
		lifacmvrec.batcactyr.set(rnlallrec.batcactyr);
		lifacmvrec.batctrcde.set(rnlallrec.batctrcde);
		lifacmvrec.batcactmn.set(rnlallrec.batcactmn);
		lifacmvrec.batcbatch.set(rnlallrec.batcbatch);
		lifacmvrec.batcbrn.set(rnlallrec.batcbrn);
		lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
		compute(lifacmvrec.origamt, 2).set(add(wsaaTax[1], wsaaTax[2]));
		lifacmvrec.rcamt.set(0);
		lifacmvrec.tranno.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.tranno.set(chdrlnbIO.getTranno());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.effdate.set(rnlallrec.effdate);
		/* Set up TRANREF to contain the Full component key of the         */
		/* Coverage/Rider that is Creating the debt so that if a           */
		/* reversal is required at a later date, the correct component     */
		/* can be identified.                                              */
		wsaaTref.set(SPACES);
		wsaaTrefChdrcoy.set(rnlallrec.company);
		wsaaTrefChdrnum.set(rnlallrec.chdrnum);
		wsaaPlan.set(rnlallrec.planSuffix);
		wsaaTrefLife.set(rnlallrec.life);
		wsaaTrefCoverage.set(rnlallrec.coverage);
		wsaaTrefRider.set(rnlallrec.rider);
		wsaaTrefPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.tranref.set(wsaaTref);
		lifacmvrec.user.set(rnlallrec.user);
		lifacmvrec.trandesc.set(getdescrec.longdesc);
		/* Check for Component Level accounting and act accordingly        */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode10);
			lifacmvrec.sacstyp.set(t5645rec.sacstype10);
			lifacmvrec.glsign.set(t5645rec.sign10);
			lifacmvrec.glcode.set(t5645rec.glmap10);
			lifacmvrec.contot.set(t5645rec.cnttot10);
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(rnlallrec.chdrnum);
			wsaaPlan.set(rnlallrec.planSuffix);
			wsaaRldgLife.set(rnlallrec.life);
			wsaaRldgCoverage.set(rnlallrec.coverage);
			/* Post ACMV against the Coverage, not Rider since the debt is also*/
			/* set against Coverage.                                           */
			wsaaRldgRider.set("00");
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			/* Set correct substitution code                                   */
			lifacmvrec.substituteCode[6].set(covrunlIO.getCrtable());
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode09);
			lifacmvrec.sacstyp.set(t5645rec.sacstype09);
			lifacmvrec.glsign.set(t5645rec.sign09);
			lifacmvrec.glcode.set(t5645rec.glmap09);
			lifacmvrec.contot.set(t5645rec.cnttot09);
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(rnlallrec.chdrnum);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
		lifacmvrec.transactionDate.set(rnlallrec.effdate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			databaseError580();
		}
	}

protected void b200ReadZrst()
	{
		b200Start();
	}

protected void b200Start()
	{
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(), varcom.oK)
		&& isNE(zrstIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(zrstIO.getParams());
			syserrrec.statuz.set(zrstIO.getStatuz());
			databaseError580();
		}
		if (isNE(zrstIO.getChdrcoy(), covrunlIO.getChdrcoy())
		|| isNE(zrstIO.getChdrnum(), covrunlIO.getChdrnum())
		|| isNE(zrstIO.getLife(), covrunlIO.getLife())
		|| isEQ(zrstIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(zrstIO.getParams());
			zrstIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(zrstIO.getFeedbackInd(), SPACES)) {
			wsaaTotCd.add(zrstIO.getZramount01());
		}
		zrstIO.setFunction(varcom.nextr);
	}

protected void b300WriteZrst()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					b310WriteZrst();
				case b300WriteTax2: 
					b300WriteTax2();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b310WriteZrst()
	{
		zrstIO.setParams(SPACES);
		zrstIO.setZramount01(0);
		zrstIO.setZramount02(0);
		zrstIO.setChdrcoy(covrunlIO.getChdrcoy());
		zrstIO.setChdrnum(covrunlIO.getChdrnum());
		zrstIO.setLife(covrunlIO.getLife());
		zrstIO.setJlife(SPACES);
		zrstIO.setCoverage(covrunlIO.getCoverage());
		zrstIO.setRider(covrunlIO.getRider());
		zrstIO.setBatctrcde(rnlallrec.batctrcde);
		zrstIO.setTranno(rnlallrec.tranno);
		zrstIO.setTrandate(rnlallrec.effdate);
		wsaaSeqno.add(1);
		zrstIO.setXtranno(ZERO);
		zrstIO.setSeqno(wsaaSeqno);
		zrstIO.setUstmno(ZERO);
		zrstIO.setFormat(zrstrec);
		if (isEQ(wsaaTax[1], 0)) {
			goTo(GotoLabel.b300WriteTax2);
		}
		zrstIO.setZramount01(wsaaTax[1]);
		zrstIO.setZramount02(wsaaTotCd);
		/*    MOVE TXCL-TAX-TYPE (1)      TO ZRST-SACSTYP.         <LA4895>*/
		zrstIO.setSacstyp(txcalcrec.taxSacstyp[1]);
		zrstIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zrstIO.getParams());
			syserrrec.statuz.set(zrstIO.getStatuz());
			databaseError580();
		}
	}

protected void b300WriteTax2()
	{
		if (isEQ(wsaaTax[2], 0)) {
			return ;
		}
		/*    MOVE WSAA-TAX(1)            TO ZRST-ZRAMOUNT01       <LA4895>*/
		zrstIO.setZramount01(wsaaTax[2]);
		zrstIO.setZramount02(wsaaTotCd);
		/*    MOVE TXCL-TAX-TYPE (1)      TO ZRST-SACSTYP.         <LA4895>*/
		zrstIO.setSacstyp(txcalcrec.taxSacstyp[2]);
		zrstIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zrstIO.getParams());
			syserrrec.statuz.set(zrstIO.getStatuz());
			databaseError580();
		}
	}

protected void b400UpdateZrst()
	{
		b410UpdateZrst();
	}

protected void b410UpdateZrst()
	{
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(), varcom.oK)
		&& isNE(zrstIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(zrstIO.getParams());
			syserrrec.statuz.set(zrstIO.getStatuz());
			databaseError580();
		}
		if (isNE(zrstIO.getChdrcoy(), covrunlIO.getChdrcoy())
		|| isNE(zrstIO.getChdrnum(), covrunlIO.getChdrnum())
		|| isNE(zrstIO.getLife(), covrunlIO.getLife())
		|| isEQ(zrstIO.getStatuz(), varcom.endp)) {
			zrstIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(zrstIO.getFeedbackInd(), SPACES)) {
			zrstIO.setZramount02(wsaaTotCd);
			zrstIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, zrstIO);
			if (isNE(zrstIO.getStatuz(), varcom.oK)
			&& isNE(zrstIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(zrstIO.getParams());
				syserrrec.statuz.set(zrstIO.getStatuz());
				databaseError580();
			}
		}
		zrstIO.setFunction(varcom.nextr);
	}
/*
 * Class transformed  from Data Structure WSAA-AMOUNT-HOLDERS--INNER
 */
private static final class WsaaAmountHoldersInner { 
		/* WSAA-AMOUNT-HOLDERS */
	private ZonedDecimalData wsaaNonInvestPrem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaAllocAmt = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaNonInvestPremTot = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaAllocInit = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaAllocInitTot = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaRunInitTot = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaAllocAccum = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaAllocAccumTot = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaRunAccumTot = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaAllocTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaRunTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaInitAmount = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaAccumAmount = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaInciPrem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaIndex2 = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaInitUnitDiscFact = new ZonedDecimalData(10, 5).init(0);
	private PackedDecimalData[] wsaaInciPerd = PDInittedArray(2, 1, 0);
	private PackedDecimalData[] wsaaInciprm = PDInittedArray(2, 17, 2);
	private PackedDecimalData[] wsaaInciInit = PDInittedArray(2, 17, 2);
	private PackedDecimalData[] wsaaInciAlloc = PDInittedArray(2, 17, 2);
	private PackedDecimalData[] wsaaInciTot = PDInittedArray(2, 17, 2);
	private PackedDecimalData[] wsaaInciNInvs = PDInittedArray(2, 17, 2);
	private PackedDecimalData wsaaContractAmount = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaFundPc = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotRecd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAmtRem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPremAvail = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAnnCurrPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotAnnPrem = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaTotUlSplit = new ZonedDecimalData(5, 2).init(0);
	private ZonedDecimalData wsaaTotIbSplit = new ZonedDecimalData(5, 2).init(0);
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t5515 = new FixedLengthStringData(5).init("T5515");
	private FixedLengthStringData t6647 = new FixedLengthStringData(5).init("T6647");
	private FixedLengthStringData t6646 = new FixedLengthStringData(5).init("T6646");
	private FixedLengthStringData t5540 = new FixedLengthStringData(5).init("T5540");
	private FixedLengthStringData t5519 = new FixedLengthStringData(5).init("T5519");
	private FixedLengthStringData t5539 = new FixedLengthStringData(5).init("T5539");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData t1688 = new FixedLengthStringData(5).init("T1688");
}
}
