package com.csc.life.flexiblepremium.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGDateData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5359.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5359Report extends SMARTReportLayout { 

	private FixedLengthStringData btdate = new FixedLengthStringData(10);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private RPGDateData dateReportVariable = new RPGDateData();
	private ZonedDecimalData linstamt = new ZonedDecimalData(11, 2);
	private ZonedDecimalData maxprem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData susamt = new ZonedDecimalData(17, 2);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData toporg = new ZonedDecimalData(18, 2);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5359Report() {
		super();
	}


	/**
	 * Print the XML for R5359d01
	 */
	public void printR5359d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		btdate.setFieldName("btdate");
		btdate.setInternal(subString(recordData, 9, 10));
		linstamt.setFieldName("linstamt");
		linstamt.setInternal(subString(recordData, 19, 11));
		maxprem.setFieldName("maxprem");
		maxprem.setInternal(subString(recordData, 30, 17));
		susamt.setFieldName("susamt");
		susamt.setInternal(subString(recordData, 47, 17));
		printLayout("R5359d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				btdate,
				linstamt,
				maxprem,
				susamt
			}
		);

	}

	/**
	 * Print the XML for R5359h01
	 */
	public void printR5359h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.set(getDate());
		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("R5359h01",			// Record name
			new BaseData[]{			// Fields:
				dateReportVariable,
				company,
				companynm,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(11);
	}

	/**
	 * Print the XML for R5359t01
	 */
	public void printR5359t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		toporg.setFieldName("toporg");
		toporg.setInternal(subString(recordData, 1, 18));
		printLayout("R5359t01",			// Record name
			new BaseData[]{			// Fields:
				toporg
			}
		);

		currentPrintLine.add(3);
	}


}
