package com.csc.life.flexiblepremium.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S5729screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5729ScreenVars sv = (S5729ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5729screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5729ScreenVars screenVars = (S5729ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.frqcy01.setClassString("");
		screenVars.frqcy02.setClassString("");
		screenVars.frqcy03.setClassString("");
		screenVars.durationa01.setClassString("");
		screenVars.targetMina01.setClassString("");
		screenVars.targetMaxa01.setClassString("");
		screenVars.overdueMina01.setClassString("");
		screenVars.durationb01.setClassString("");
		screenVars.targetMinb01.setClassString("");
		screenVars.targetMaxb01.setClassString("");
		screenVars.overdueMinb01.setClassString("");
		screenVars.durationc01.setClassString("");
		screenVars.targetMinc01.setClassString("");
		screenVars.targetMaxc01.setClassString("");
		screenVars.overdueMinc01.setClassString("");
		screenVars.durationa02.setClassString("");
		screenVars.targetMina02.setClassString("");
		screenVars.targetMaxa02.setClassString("");
		screenVars.overdueMina02.setClassString("");
		screenVars.durationb02.setClassString("");
		screenVars.targetMinb02.setClassString("");
		screenVars.targetMaxb02.setClassString("");
		screenVars.overdueMinb02.setClassString("");
		screenVars.durationc02.setClassString("");
		screenVars.targetMinc02.setClassString("");
		screenVars.targetMaxc02.setClassString("");
		screenVars.overdueMinc02.setClassString("");
		screenVars.durationa03.setClassString("");
		screenVars.targetMina03.setClassString("");
		screenVars.targetMaxa03.setClassString("");
		screenVars.overdueMina03.setClassString("");
		screenVars.durationb03.setClassString("");
		screenVars.targetMinb03.setClassString("");
		screenVars.targetMaxb03.setClassString("");
		screenVars.overdueMinb03.setClassString("");
		screenVars.durationc03.setClassString("");
		screenVars.targetMinc03.setClassString("");
		screenVars.targetMaxc03.setClassString("");
		screenVars.overdueMinc03.setClassString("");
		screenVars.durationa04.setClassString("");
		screenVars.targetMina04.setClassString("");
		screenVars.targetMaxa04.setClassString("");
		screenVars.overdueMina04.setClassString("");
		screenVars.durationb04.setClassString("");
		screenVars.targetMinb04.setClassString("");
		screenVars.targetMaxb04.setClassString("");
		screenVars.overdueMinb04.setClassString("");
		screenVars.durationc04.setClassString("");
		screenVars.targetMinc04.setClassString("");
		screenVars.targetMaxc04.setClassString("");
		screenVars.overdueMinc04.setClassString("");
		screenVars.frqcy04.setClassString("");
		screenVars.frqcy05.setClassString("");
		screenVars.frqcy06.setClassString("");
		screenVars.durationd01.setClassString("");
		screenVars.targetMind01.setClassString("");
		screenVars.targetMaxd01.setClassString("");
		screenVars.overdueMind01.setClassString("");
		screenVars.duratione01.setClassString("");
		screenVars.targetMine01.setClassString("");
		screenVars.targetMaxe01.setClassString("");
		screenVars.overdueMine01.setClassString("");
		screenVars.durationf01.setClassString("");
		screenVars.targetMinf01.setClassString("");
		screenVars.targetMaxf01.setClassString("");
		screenVars.overdueMinf01.setClassString("");
		screenVars.durationd02.setClassString("");
		screenVars.targetMind02.setClassString("");
		screenVars.targetMaxd02.setClassString("");
		screenVars.overdueMind02.setClassString("");
		screenVars.duratione02.setClassString("");
		screenVars.targetMine02.setClassString("");
		screenVars.targetMaxe02.setClassString("");
		screenVars.overdueMine02.setClassString("");
		screenVars.durationf02.setClassString("");
		screenVars.targetMinf02.setClassString("");
		screenVars.targetMaxf02.setClassString("");
		screenVars.overdueMinf02.setClassString("");
		screenVars.durationd03.setClassString("");
		screenVars.targetMind03.setClassString("");
		screenVars.targetMaxd03.setClassString("");
		screenVars.overdueMind03.setClassString("");
		screenVars.duratione03.setClassString("");
		screenVars.targetMine03.setClassString("");
		screenVars.targetMaxe03.setClassString("");
		screenVars.overdueMine03.setClassString("");
		screenVars.durationf03.setClassString("");
		screenVars.targetMinf03.setClassString("");
		screenVars.targetMaxf03.setClassString("");
		screenVars.overdueMinf03.setClassString("");
		screenVars.durationd04.setClassString("");
		screenVars.targetMind04.setClassString("");
		screenVars.targetMaxd04.setClassString("");
		screenVars.overdueMind04.setClassString("");
		screenVars.duratione04.setClassString("");
		screenVars.targetMine04.setClassString("");
		screenVars.targetMaxe04.setClassString("");
		screenVars.overdueMine04.setClassString("");
		screenVars.durationf04.setClassString("");
		screenVars.targetMinf04.setClassString("");
		screenVars.targetMaxf04.setClassString("");
		screenVars.overdueMinf04.setClassString("");
	}

/**
 * Clear all the variables in S5729screen
 */
	public static void clear(VarModel pv) {
		S5729ScreenVars screenVars = (S5729ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.frqcy01.clear();
		screenVars.frqcy02.clear();
		screenVars.frqcy03.clear();
		screenVars.durationa01.clear();
		screenVars.targetMina01.clear();
		screenVars.targetMaxa01.clear();
		screenVars.overdueMina01.clear();
		screenVars.durationb01.clear();
		screenVars.targetMinb01.clear();
		screenVars.targetMaxb01.clear();
		screenVars.overdueMinb01.clear();
		screenVars.durationc01.clear();
		screenVars.targetMinc01.clear();
		screenVars.targetMaxc01.clear();
		screenVars.overdueMinc01.clear();
		screenVars.durationa02.clear();
		screenVars.targetMina02.clear();
		screenVars.targetMaxa02.clear();
		screenVars.overdueMina02.clear();
		screenVars.durationb02.clear();
		screenVars.targetMinb02.clear();
		screenVars.targetMaxb02.clear();
		screenVars.overdueMinb02.clear();
		screenVars.durationc02.clear();
		screenVars.targetMinc02.clear();
		screenVars.targetMaxc02.clear();
		screenVars.overdueMinc02.clear();
		screenVars.durationa03.clear();
		screenVars.targetMina03.clear();
		screenVars.targetMaxa03.clear();
		screenVars.overdueMina03.clear();
		screenVars.durationb03.clear();
		screenVars.targetMinb03.clear();
		screenVars.targetMaxb03.clear();
		screenVars.overdueMinb03.clear();
		screenVars.durationc03.clear();
		screenVars.targetMinc03.clear();
		screenVars.targetMaxc03.clear();
		screenVars.overdueMinc03.clear();
		screenVars.durationa04.clear();
		screenVars.targetMina04.clear();
		screenVars.targetMaxa04.clear();
		screenVars.overdueMina04.clear();
		screenVars.durationb04.clear();
		screenVars.targetMinb04.clear();
		screenVars.targetMaxb04.clear();
		screenVars.overdueMinb04.clear();
		screenVars.durationc04.clear();
		screenVars.targetMinc04.clear();
		screenVars.targetMaxc04.clear();
		screenVars.overdueMinc04.clear();
		screenVars.frqcy04.clear();
		screenVars.frqcy05.clear();
		screenVars.frqcy06.clear();
		screenVars.durationd01.clear();
		screenVars.targetMind01.clear();
		screenVars.targetMaxd01.clear();
		screenVars.overdueMind01.clear();
		screenVars.duratione01.clear();
		screenVars.targetMine01.clear();
		screenVars.targetMaxe01.clear();
		screenVars.overdueMine01.clear();
		screenVars.durationf01.clear();
		screenVars.targetMinf01.clear();
		screenVars.targetMaxf01.clear();
		screenVars.overdueMinf01.clear();
		screenVars.durationd02.clear();
		screenVars.targetMind02.clear();
		screenVars.targetMaxd02.clear();
		screenVars.overdueMind02.clear();
		screenVars.duratione02.clear();
		screenVars.targetMine02.clear();
		screenVars.targetMaxe02.clear();
		screenVars.overdueMine02.clear();
		screenVars.durationf02.clear();
		screenVars.targetMinf02.clear();
		screenVars.targetMaxf02.clear();
		screenVars.overdueMinf02.clear();
		screenVars.durationd03.clear();
		screenVars.targetMind03.clear();
		screenVars.targetMaxd03.clear();
		screenVars.overdueMind03.clear();
		screenVars.duratione03.clear();
		screenVars.targetMine03.clear();
		screenVars.targetMaxe03.clear();
		screenVars.overdueMine03.clear();
		screenVars.durationf03.clear();
		screenVars.targetMinf03.clear();
		screenVars.targetMaxf03.clear();
		screenVars.overdueMinf03.clear();
		screenVars.durationd04.clear();
		screenVars.targetMind04.clear();
		screenVars.targetMaxd04.clear();
		screenVars.overdueMind04.clear();
		screenVars.duratione04.clear();
		screenVars.targetMine04.clear();
		screenVars.targetMaxe04.clear();
		screenVars.overdueMine04.clear();
		screenVars.durationf04.clear();
		screenVars.targetMinf04.clear();
		screenVars.targetMaxf04.clear();
		screenVars.overdueMinf04.clear();
	}
}
