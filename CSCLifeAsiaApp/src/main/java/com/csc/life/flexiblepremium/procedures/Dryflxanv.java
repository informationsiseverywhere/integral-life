/*
 * File: Dryflxanv.java
 * Date: February 4, 2015 11:28:55 AM ICT
 * Author: CSC
 * 
 * Class transformed from DRYFLXANV.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsuframework.core.FeaConfg;
import com.csc.diary.procedures.Drylog;
import com.csc.diary.recordstructures.Drylogrec;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.diary.dataaccess.FpcodryTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This subroutine is used by the Diary system to set
* up the trigger record for flexible premium anniversary
* processing.
*
* The trigger date for anniversary processing will not be
* the exact anniversary date as found on the COVR-CPI-DATE.
*
* T6654 is used to retrieve the lead days. The trigger date
* (DRYP-NXTPRCDATE) will be set to the earliest valid Anniversary
* Processing Date on the Flexible Premium Coverage records for
* the contract, less the number of lead days on T6654.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dryflxanv extends Maind {//ILPI-61

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(9).init("DRYFLXANV");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(2, 0).setUnsigned();
	private final int wsaaT5679Max = 12;

	private FixedLengthStringData wsaaT5679Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5679Trcde = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init("N");
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");
	private Validator invalidStatus = new Validator(wsaaValidStatus, "N");

	private FixedLengthStringData wsaaT6654Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemBillchnl = new FixedLengthStringData(1).isAPartOf(wsaaT6654Item, 0);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Item, 1);
	private FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(wsaaT6654Item, 4, FILLER);
	private ZonedDecimalData wsaaLeadDays = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaAnnivProcDate = new ZonedDecimalData(8, 0);
		/* ERRORS */
	private static final String f321 = "F321";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String fpcodryrec = "FPCODRYREC";
	private static final String covrenqrec = "COVRENQREC";
	private static final String chdrlifrec = "CHDRLIFREC";
		/* TABLES */
	private static final String t6654 = "T6654";
	private static final String t5679 = "T5679";
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private FpcodryTableDAM fpcodryIO = new FpcodryTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T6654rec t6654rec = new T6654rec();
	private T5679rec t5679rec = new T5679rec();
	private Varcom varcom = new Varcom();
	private Freqcpy freqcpy = new Freqcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Drylogrec drylogrec = new Drylogrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected Itempf itempf = null;

	public Dryflxanv() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainLine000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdYes.setTrue();
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		/* Validate the contract status using T5679.*/
		validate100();
		if (invalidStatus.isTrue()) {
			drypDryprcRecInner.dtrdNo.setTrue();
			return ;
		}
		/* Find the FPCO record for the contract with the earliest*/
		/* Anniversary Date which has not already been processed.*/
		wsaaAnnivProcDate.set(varcom.vrcmMaxDate);
		fpcodryIO.setRecKeyData(SPACES);
		fpcodryIO.setRecNonKeyData(SPACES);
		fpcodryIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		fpcodryIO.setChdrnum(drypDryprcRecInner.drypEntity);
		fpcodryIO.setFormat(fpcodryrec);
		fpcodryIO.setFunction(varcom.begn);
		fpcodryIO.setStatuz(varcom.oK);
		while ( !(isEQ(fpcodryIO.getStatuz(), varcom.endp))) {
			readFpcos400();
		}
		
		/* If no record has been found, do not create a DTRD record and*/
		/* do no further processing.*/
		if (isEQ(wsaaAnnivProcDate, varcom.vrcmMaxDate)) {
			drypDryprcRecInner.dtrdNo.setTrue();
			return ;
		}
		/* Find the number of lead days for the billing channel*/
		readChdr();
		String key;
		if(BTPRO028Permission) {
			key = drypDryprcRecInner.drypBillchnl.toString().trim() + drypDryprcRecInner.drypCnttype.toString().trim() + chdrlifIO.getBillfreq().toString().trim();
			if(!readT6654(key)) {
				key = drypDryprcRecInner.drypBillchnl.toString().trim().concat(chdrlifIO.getCnttype().toString().trim()).concat("**");
				if(!readT6654(key)) {
					key = drypDryprcRecInner.drypBillchnl.toString().trim().concat("*****");
					if(!readT6654(key)) {
						drylogrec.statuz.set(itemIO.getStatuz());
						drylogrec.params.set(itemIO.getParams());
						drylogrec.dryDatabaseError.setTrue();
						a000FatalError();
					}
				}
			}
		}
		else {
		itemIO.setParams(SPACES);
		itemIO.setRecKeyData(SPACES);
		itemIO.setRecNonKeyData(SPACES);
		initialize(wsaaT6654Item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemtabl(t6654);
		wsaaItemBillchnl.set(drypDryprcRecInner.drypBillchnl);
		wsaaItemCnttype.set(drypDryprcRecInner.drypCnttype);
		itemIO.setItemitem(wsaaT6654Item);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaItemBillchnl.set(drypDryprcRecInner.drypBillchnl);
			wsaaItemCnttype.set("***");
			itemIO.setItemitem(wsaaT6654Item);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				drylogrec.params.set(itemIO.getParams());
				drylogrec.statuz.set(itemIO.getStatuz());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
		}
		t6654rec.t6654Rec.set(itemIO.getGenarea());
		}
		/* Subtract the number of lead days from the anniversary*/
		/* date to trigger the next anniversary processing.*/
		/* E.g. if the lead days is 10 days, then the next process*/
		/* date will be set to 10 days before the anniversary date.*/
		compute(wsaaLeadDays, 0).set(mult(t6654rec.leadDays, -1));
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(wsaaAnnivProcDate);
		datcon2rec.intDate2.set(0);
		datcon2rec.frequency.set(freqcpy.daily);
		datcon2rec.freqFactor.set(wsaaLeadDays);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* Finally set the next processing date to the DATCON2*/
		/* result.*/
		drypDryprcRecInner.drypNxtprcdate.set(datcon2rec.intDate2);
		drypDryprcRecInner.drypNxtprctime.set(0);
	}

protected boolean readT6654(String key) {
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(drypDryprcRecInner.drypCompany.toString());
	itempf.setItemtabl(t6654);
	itempf.setItemitem(key);
	itempf = itempfDAO.getItempfRecord(itempf);
	if (null != itempf) {
		t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		return true;
	}
	return false;
}

protected void readChdr() {
	chdrlifIO.setChdrcoy(fpcodryIO.getChdrcoy());
	chdrlifIO.setChdrnum(fpcodryIO.getChdrnum());
	chdrlifIO.setFunction(varcom.readr);
	chdrlifIO.setFormat(chdrlifrec);
	SmartFileCode.execute(appVars, chdrlifIO);
	if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
		drylogrec.statuz.set(chdrlifIO.getStatuz());
		drylogrec.params.set(chdrlifIO.getParams());
		drylogrec.dryDatabaseError.setTrue();
		a000FatalError();
	}
}

protected void exit090()
	{
		exitProgram();
	}

protected void validate100()
	{
		/*VALD*/
		/* Validate the Contract Risk and Premium status against T5679.*/
		readT5679200();
		validateStatus300();
		/*EXIT*/
	}

protected void readT5679200()
	{
		t5679210();
	}

protected void t5679210()
	{
		wsaaT5679Trcde.set(drypDryprcRecInner.drypSystParm[1]);
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaT5679Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(f321);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void validateStatus300()
	{
		/*VALIDATE*/
		invalidStatus.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
		|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(drypDryprcRecInner.drypStatcode, t5679rec.cnRiskStat[wsaaT5679Sub.toInt()])) {
				validStatus.setTrue();
			}
		}
		if (validStatus.isTrue()) {
			invalidStatus.setTrue();
			for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
			|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
				if (isEQ(drypDryprcRecInner.drypPstatcode, t5679rec.cnPremStat[wsaaT5679Sub.toInt()])) {
					validStatus.setTrue();
				}
			}
		}
		/*EXIT*/
	}

protected void readFpcos400()
	{
		call410();
	}

protected void call410()
	{
		SmartFileCode.execute(appVars, fpcodryIO);
		if (isNE(fpcodryIO.getStatuz(), varcom.oK)
		&& isNE(fpcodryIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(fpcodryIO.getStatuz());
			drylogrec.params.set(fpcodryIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(fpcodryIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(fpcodryIO.getChdrnum(), drypDryprcRecInner.drypEntity)) {
			fpcodryIO.setStatuz(varcom.endp);
		}
		if (isEQ(fpcodryIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* If the Anniversary Processing Date of the record is earlier*/
		/* than the one stored, check that the associated coverage is*/
		/* valid and store the earlier date if it is.*/
		if (isLT(fpcodryIO.getAnnivProcDate(), wsaaAnnivProcDate)) {
			validateCoverage500();
			if (validStatus.isTrue()) {
				wsaaAnnivProcDate.set(fpcodryIO.getAnnivProcDate());
			}
		}
		fpcodryIO.setFunction(varcom.nextr);
	}

protected void validateCoverage500()
	{
		validate510();
	}

protected void validate510()
	{
		/* Read the associated coverage record for the FPCO record.*/
		covrenqIO.setRecKeyData(SPACES);
		covrenqIO.setRecNonKeyData(SPACES);
		covrenqIO.setChdrcoy(fpcodryIO.getChdrcoy());
		covrenqIO.setChdrnum(fpcodryIO.getChdrnum());
		covrenqIO.setLife(fpcodryIO.getLife());
		covrenqIO.setCoverage(fpcodryIO.getCoverage());
		covrenqIO.setRider(fpcodryIO.getRider());
		covrenqIO.setPlanSuffix(fpcodryIO.getPlanSuffix());
		covrenqIO.setFormat(covrenqrec);
		covrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(covrenqIO.getStatuz());
			drylogrec.params.set(covrenqIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		invalidStatus.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
		|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(covrenqIO.getStatcode(), t5679rec.covRiskStat[wsaaT5679Sub.toInt()])) {
				validStatus.setTrue();
			}
		}
		if (validStatus.isTrue()) {
			invalidStatus.setTrue();
			for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
			|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
				if (isEQ(covrenqIO.getPstatcode(), t5679rec.covPremStat[wsaaT5679Sub.toInt()])) {
					validStatus.setTrue();
				}
			}
		}
	}

//ILPI-61
//protected void a000FatalError()
//	{
//		/*A010-FATAL*/
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
//		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
//		if (drypDryprcRecInner.onlineMode.isTrue()) {
//			drylogrec.runNumber.set(ZERO);
//		}
//		else {
//			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
//		}
//		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
//		drypDryprcRecInner.fatalError.setTrue();
//		callProgram(Drylog.class, drylogrec.drylogRec);
//		/*A090-EXIT*/
//		exitProgram();
//	}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 

	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
}

/**
 * ILPI-61
 */
@Override
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
}

/**
 * ILPI-61
 */
@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
}
}
