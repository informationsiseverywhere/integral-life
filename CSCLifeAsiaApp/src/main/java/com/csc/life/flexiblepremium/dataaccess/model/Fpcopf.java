/******************************************************************************
 * File Name 		: Fpcopf.java
 * Author			: smalchi2
 * Creation Date	: 29 December 2016
 * Project			: Integral Life
 * Description		: The Model Class for FPCOPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.flexiblepremium.dataaccess.model;

import java.math.BigDecimal;
import java.sql.Date;
import java.io.Serializable;

public class Fpcopf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	// Constant for database table name
	public static final String TABLE_NAME = "FPCOPF";

	//member variables for columns
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	private String validflag;
	private Integer currfrom;
	private Integer currto;
	private BigDecimal prmper;
	private BigDecimal prmrcdp;
	private Integer tranno;
	private String usrprf;
	private String jobnm;
	private Date datime;
	private String actind;
	private BigDecimal billedp;
	private BigDecimal ovrminreq;
	private Integer minovrpro;
	private String anproind;
	private Integer targfrom;
	private Integer targto;
	private Integer effdate;
	private Integer cbanpr;

	// Constructor
	public Fpcopf ( ) {};

	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public String getChdrcoy(){
		return this.chdrcoy;
	}
	public String getChdrnum(){
		return this.chdrnum;
	}
	public String getLife(){
		return this.life;
	}
	public String getJlife(){
		return this.jlife;
	}
	public String getCoverage(){
		return this.coverage;
	}
	public String getRider(){
		return this.rider;
	}
	public Integer getPlnsfx(){
		return this.plnsfx;
	}
	public String getValidflag(){
		return this.validflag;
	}
	public Integer getCurrfrom(){
		return this.currfrom;
	}
	public Integer getCurrto(){
		return this.currto;
	}
	public BigDecimal getPrmper(){
		return this.prmper;
	}
	public BigDecimal getPrmrcdp(){
		return this.prmrcdp;
	}
	public Integer getTranno(){
		return this.tranno;
	}
	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public Date getDatime(){
		return new Date(this.datime.getTime());//IJTI-316
	}
	public String getActind(){
		return this.actind;
	}
	public BigDecimal getBilledp(){
		return this.billedp;
	}
	public BigDecimal getOvrminreq(){
		return this.ovrminreq;
	}
	public Integer getMinovrpro(){
		return this.minovrpro;
	}
	public String getAnproind(){
		return this.anproind;
	}
	public Integer getTargfrom(){
		return this.targfrom;
	}
	public Integer getTargto(){
		return this.targto;
	}
	public Integer getEffdate(){
		return this.effdate;
	}
	public Integer getCbanpr(){
		return this.cbanpr;
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setChdrcoy( String chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setChdrnum( String chdrnum ){
		 this.chdrnum = chdrnum;
	}
	public void setLife( String life ){
		 this.life = life;
	}
	public void setJlife( String jlife ){
		 this.jlife = jlife;
	}
	public void setCoverage( String coverage ){
		 this.coverage = coverage;
	}
	public void setRider( String rider ){
		 this.rider = rider;
	}
	public void setPlnsfx( Integer plnsfx ){
		 this.plnsfx = plnsfx;
	}
	public void setValidflag( String validflag ){
		 this.validflag = validflag;
	}
	public void setCurrfrom( Integer currfrom ){
		 this.currfrom = currfrom;
	}
	public void setCurrto( Integer currto ){
		 this.currto = currto;
	}
	public void setPrmper( BigDecimal prmper ){
		 this.prmper = prmper;
	}
	public void setPrmrcdp( BigDecimal prmrcdp ){
		 this.prmrcdp = prmrcdp;
	}
	public void setTranno( Integer tranno ){
		 this.tranno = tranno;
	}
	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime( Date datime ){
		this.datime = new Date(datime.getTime());//IJTI-314
	}
	public void setActind( String actind ){
		 this.actind = actind;
	}
	public void setBilledp( BigDecimal billedp ){
		 this.billedp = billedp;
	}
	public void setOvrminreq( BigDecimal ovrminreq ){
		 this.ovrminreq = ovrminreq;
	}
	public void setMinovrpro( Integer minovrpro ){
		 this.minovrpro = minovrpro;
	}
	public void setAnproind( String anproind ){
		 this.anproind = anproind;
	}
	public void setTargfrom( Integer targfrom ){
		 this.targfrom = targfrom;
	}
	public void setTargto( Integer targto ){
		 this.targto = targto;
	}
	public void setEffdate( Integer effdate ){
		 this.effdate = effdate;
	}
	public void setCbanpr( Integer cbanpr ){
		 this.cbanpr = cbanpr;
	}

	// ToString method
	public String toString(){

		StringBuilder output = new StringBuilder();

		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("CHDRCOY:		");
		output.append(getChdrcoy());
		output.append("\r\n");
		output.append("CHDRNUM:		");
		output.append(getChdrnum());
		output.append("\r\n");
		output.append("LIFE:		");
		output.append(getLife());
		output.append("\r\n");
		output.append("JLIFE:		");
		output.append(getJlife());
		output.append("\r\n");
		output.append("COVERAGE:		");
		output.append(getCoverage());
		output.append("\r\n");
		output.append("RIDER:		");
		output.append(getRider());
		output.append("\r\n");
		output.append("PLNSFX:		");
		output.append(getPlnsfx());
		output.append("\r\n");
		output.append("VALIDFLAG:		");
		output.append(getValidflag());
		output.append("\r\n");
		output.append("CURRFROM:		");
		output.append(getCurrfrom());
		output.append("\r\n");
		output.append("CURRTO:		");
		output.append(getCurrto());
		output.append("\r\n");
		output.append("PRMPER:		");
		output.append(getPrmper());
		output.append("\r\n");
		output.append("PRMRCDP:		");
		output.append(getPrmrcdp());
		output.append("\r\n");
		output.append("TRANNO:		");
		output.append(getTranno());
		output.append("\r\n");
		output.append("USRPRF:		");
		output.append(getUsrprf());
		output.append("\r\n");
		output.append("JOBNM:		");
		output.append(getJobnm());
		output.append("\r\n");
		output.append("DATIME:		");
		output.append(getDatime());
		output.append("\r\n");
		output.append("ACTIND:		");
		output.append(getActind());
		output.append("\r\n");
		output.append("BILLEDP:		");
		output.append(getBilledp());
		output.append("\r\n");
		output.append("OVRMINREQ:		");
		output.append(getOvrminreq());
		output.append("\r\n");
		output.append("MINOVRPRO:		");
		output.append(getMinovrpro());
		output.append("\r\n");
		output.append("ANPROIND:		");
		output.append(getAnproind());
		output.append("\r\n");
		output.append("TARGFROM:		");
		output.append(getTargfrom());
		output.append("\r\n");
		output.append("TARGTO:		");
		output.append(getTargto());
		output.append("\r\n");
		output.append("EFFDATE:		");
		output.append(getEffdate());
		output.append("\r\n");
		output.append("CBANPR:		");
		output.append(getCbanpr());
		output.append("\r\n");

		return output.toString();
	}

}
