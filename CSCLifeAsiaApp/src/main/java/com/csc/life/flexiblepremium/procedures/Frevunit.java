/*
 * File: Frevunit.java
 * Date: 29 August 2009 22:48:40
 * Author: Quipoz Limited
 *
 * Class transformed from FREVUNIT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.contractservicing.dataaccess.IncigrvTableDAM;
import com.csc.life.contractservicing.dataaccess.UtrncfiTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.interestbearing.dataaccess.HitrcfiTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*   Reversal of UTRNs and INCIs for Flexible Premiums
*   ---------------------------------------------------
*
************  THIS IS CLONED FROM GREVUNIT.          ************
*
****   CHECK IF ANY CHANGES MADE TO THIS APPLY TO GREVUNIT  *****
************                                         ************
*
*   This subroutine is normally called by REVFPCOL reversal subroutine
*   via Table T5671. The parameters passed to this subroutine are
*   contained in the GREVERSREC copybook.
*
*   UTRN and INCI records are reversed by this process.
*
*   Read all the INCIs for the contract passed and for the transaction
*   being reversed read the relevant UTRNs.
*
*   For each UTRN to be reversed, the INCI amount is reduced by the
*   amount on the UTRN unless the UTRN is for an over-target amount.
*   An over-target premium will have none of the period fields set on
*   the UTRN and is ignored.
*
*   If the UTRN-INCI-NUM is not = to the INCI-SEQNO (the UTRN
*   INCI-NUM is set to the appropriate INCI-SEQNO), read the next
*   UTRN.
*   Note : Where the INCINUM is not set, we will process the
*   UTRNS when the seqno on the INCI is 1. This overcomes the
*   problem of processing the UTRNS twice.
*
*   The INCIs are updated from the most recent period first.  This will
*   be in the period 2 field on the UTRN.
*
*   The value of the current premium (CURR-PREM) for each UTRN record
*   processed is added to the current amount of premium required for the
*   most recent period able to absorb the premium (CURR-PREM0n).
*   If only part of the premium can be absorbed by a given period, the
*   remainder is applied to the next most recent period. (This will
*   also be in the period 1 field on the UTRN0.
*
*   If any premium is outstanding at the end of this process, the program
*   ends with an error.
*
*   The processing for UTRN records depends on whether they have been
*   processed by UNITDEAL or not.
*
*   If the Feedback Indicator is set, a new negative copy of the UTRN
*   is written, otherwise the UTRN record is deleted.
*
*****************************************************************
* </pre>
*/
public class Frevunit extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
		/* FORMATS */
	private String utrncfirec = "UTRNCFIREC";
	private String hitrcfirec = "HITRCFIREC";
	private String t744 = "T744";
		/* WSAA-INCI-PERIODS */
	private PackedDecimalData wsaaInciPerd01 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaInciPerd02 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaInciPerd03 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaInciPerd04 = new PackedDecimalData(18, 2);
	private ZonedDecimalData wsaaInciSub = new ZonedDecimalData(1, 0);
	private ZonedDecimalData wsaaStoredValueToAdd = new ZonedDecimalData(13, 2).init(ZERO);
	private ZonedDecimalData wsaaStoredDifference = new ZonedDecimalData(13, 2).init(ZERO);
	private Greversrec greversrec = new Greversrec();
		/*Interest Bearing Transaction Details*/
	private HitrcfiTableDAM hitrcfiIO = new HitrcfiTableDAM();
		/*Logical File Layout - Version 9305*/
	private IncigrvTableDAM incigrvIO = new IncigrvTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
		/*CFI/AFI view of UTRN file*/
	private UtrncfiTableDAM utrncfiIO = new UtrncfiTableDAM();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		pass370,
		exit399,
		exit499,
		a270Pass,
		a290Exit,
		end950
	}

	public Frevunit() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			control000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void control000()
	{
		start000();
		exit000();
	}

protected void start000()
	{
		incigrvIO.setDataArea(SPACES);
		incigrvIO.setPlanSuffix(ZERO);
		incigrvIO.setInciNum(ZERO);
		incigrvIO.setSeqno(ZERO);
		incigrvIO.setChdrcoy(greversrec.chdrcoy);
		incigrvIO.setChdrnum(greversrec.chdrnum);
		incigrvIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		//incigrvIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//incigrvIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, incigrvIO);
		if (isNE(incigrvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incigrvIO.getParams());
			syserrrec.statuz.set(incigrvIO.getStatuz());
			fatalError900();
		}
		if ((isNE(incigrvIO.getChdrcoy(),greversrec.chdrcoy))
		|| (isNE(incigrvIO.getChdrnum(),greversrec.chdrnum))) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(greversrec.chdrcoy.toString());
			stringVariable1.append(greversrec.chdrnum.toString());
			syserrrec.params.setLeft(stringVariable1.toString());
			fatalError900();
		}
		while ( !(isEQ(incigrvIO.getStatuz(),varcom.endp))) {
			readIncis100();
		}

	}

protected void exit000()
	{
		exitProgram();
	}

protected void readIncis100()
	{
		start110();
	}

protected void start110()
	{
		wsaaInciPerd01.set(incigrvIO.getPremCurr01());
		wsaaInciPerd02.set(incigrvIO.getPremCurr02());
		wsaaInciPerd03.set(incigrvIO.getPremCurr03());
		wsaaInciPerd04.set(incigrvIO.getPremCurr04());
		utrncfiIO.setParams(SPACES);
		utrncfiIO.setChdrcoy(greversrec.chdrcoy);
		utrncfiIO.setChdrnum(greversrec.chdrnum);
		utrncfiIO.setLife(incigrvIO.getLife());
		utrncfiIO.setCoverage(incigrvIO.getCoverage());
		utrncfiIO.setRider(incigrvIO.getRider());
		utrncfiIO.setPlanSuffix(incigrvIO.getPlanSuffix());
		utrncfiIO.setTranno(greversrec.tranno);
		utrncfiIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		utrncfiIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		while ( !(isEQ(utrncfiIO.getStatuz(),varcom.endp))) {
			readUtrns200();
		}

		hitrcfiIO.setParams(SPACES);
		hitrcfiIO.setChdrcoy(greversrec.chdrcoy);
		hitrcfiIO.setChdrnum(greversrec.chdrnum);
		hitrcfiIO.setLife(incigrvIO.getLife());
		hitrcfiIO.setCoverage(incigrvIO.getCoverage());
		hitrcfiIO.setRider(incigrvIO.getRider());
		hitrcfiIO.setPlanSuffix(incigrvIO.getPlanSuffix());
		hitrcfiIO.setTranno(greversrec.tranno);
		hitrcfiIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hitrcfiIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		while ( !(isEQ(hitrcfiIO.getStatuz(),varcom.endp))) {
			a100ReadHitrs();
		}

		incigrvIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, incigrvIO);
		if (isNE(incigrvIO.getStatuz(),varcom.oK)
		&& isNE(incigrvIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incigrvIO.getParams());
			syserrrec.statuz.set(incigrvIO.getStatuz());
			fatalError900();
		}
		if ((isNE(incigrvIO.getChdrcoy(),greversrec.chdrcoy))
		|| (isNE(incigrvIO.getChdrnum(),greversrec.chdrnum))) {
			incigrvIO.setStatuz(varcom.endp);
		}
	}

protected void readUtrns200()
	{
		/*START*/
		SmartFileCode.execute(appVars, utrncfiIO);
		if ((isNE(utrncfiIO.getStatuz(),varcom.oK))
		&& (isNE(utrncfiIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(utrncfiIO.getParams());
			syserrrec.statuz.set(utrncfiIO.getStatuz());
			fatalError900();
		}
		if (isNE(greversrec.chdrcoy,utrncfiIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,utrncfiIO.getChdrnum())
		|| isNE(incigrvIO.getLife(),utrncfiIO.getLife())
		|| isNE(incigrvIO.getCoverage(),utrncfiIO.getCoverage())
		|| isNE(incigrvIO.getRider(),utrncfiIO.getRider())
		|| isNE(incigrvIO.getPlanSuffix(),utrncfiIO.getPlanSuffix())
		|| isNE(greversrec.tranno,utrncfiIO.getTranno())
		|| isEQ(utrncfiIO.getStatuz(),varcom.endp)) {
			utrncfiIO.setStatuz(varcom.endp);
			rewrtInci700();
		}
		else {
			reverseUtrns300();
		}
		/*EXIT*/
	}

protected void reverseUtrns300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start310();
				}
				case pass370: {
					pass370();
				}
				case exit399: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start310()
	{
		if (isEQ(utrncfiIO.getInciNum(),0)
		&& isEQ(incigrvIO.getSeqno(),1)) {
			goTo(GotoLabel.pass370);
		}
		if (isNE(utrncfiIO.getInciNum(),incigrvIO.getSeqno())) {
			utrncfiIO.setFunction(varcom.nextr);
			goTo(GotoLabel.exit399);
		}
	}

protected void pass370()
	{
		if (isNE(utrncfiIO.getInciPerd01(),0)) {
			reverseInci400();
		}
		if (isEQ(utrncfiIO.getFeedbackInd(),"N")
		|| isEQ(utrncfiIO.getFeedbackInd(),SPACES)) {
			deleteUtrn500();
		}
		else {
			writeNegativeUtrn600();
		}
		utrncfiIO.setFunction(varcom.nextr);
	}

protected void reverseInci400()
	{
		/*START*/
		wsaaStoredValueToAdd.set(utrncfiIO.getContractAmount());
		if (isNE(utrncfiIO.getInciPerd01(),0)
		&& isNE(utrncfiIO.getInciPerd02(),0)) {
			wsaaInciSub.set(utrncfiIO.getInciPerd02());
		}
		else {
			wsaaInciSub.set(utrncfiIO.getInciPerd01());
		}
		updateInciCurrPrem450();
		/*EXIT*/
	}

protected void updateInciCurrPrem450()
	{
		try {
			start451();
		}
		catch (GOTOException e){
		}
	}

protected void start451()
	{
		if (isEQ(wsaaInciSub,4)) {
			if (isNE(incigrvIO.getPremCurr04(),incigrvIO.getPremStart04())) {
				compute(wsaaStoredDifference, 2).set(sub(incigrvIO.getPremStart04(),incigrvIO.getPremCurr04()));
				if (isGTE(wsaaStoredDifference,wsaaStoredValueToAdd)) {
					wsaaInciPerd04.add(wsaaStoredValueToAdd);
					wsaaStoredValueToAdd.set(ZERO);
					goTo(GotoLabel.exit499);
				}
				else {
					wsaaInciPerd04.set(incigrvIO.getPremStart04());
					wsaaStoredValueToAdd.subtract(wsaaStoredDifference);
					wsaaInciSub.subtract(1);
				}
			}
		}
		if (isEQ(wsaaInciSub,3)) {
			if (isNE(incigrvIO.getPremCurr03(),incigrvIO.getPremStart03())) {
				compute(wsaaStoredDifference, 2).set(sub(incigrvIO.getPremStart03(),incigrvIO.getPremCurr03()));
				if (isGTE(wsaaStoredDifference,wsaaStoredValueToAdd)) {
					wsaaInciPerd03.add(wsaaStoredValueToAdd);
					wsaaStoredValueToAdd.set(ZERO);
					goTo(GotoLabel.exit499);
				}
				else {
					wsaaInciPerd03.set(incigrvIO.getPremStart03());
					wsaaStoredValueToAdd.subtract(wsaaStoredDifference);
					wsaaInciSub.subtract(1);
				}
			}
		}
		if (isEQ(wsaaInciSub,2)) {
			if (isNE(incigrvIO.getPremCurr02(),incigrvIO.getPremStart02())) {
				compute(wsaaStoredDifference, 2).set(sub(incigrvIO.getPremStart02(),incigrvIO.getPremCurr02()));
				if (isGTE(wsaaStoredDifference,wsaaStoredValueToAdd)) {
					wsaaInciPerd02.add(wsaaStoredValueToAdd);
					wsaaStoredValueToAdd.set(ZERO);
					goTo(GotoLabel.exit499);
				}
				else {
					wsaaInciPerd02.set(incigrvIO.getPremStart02());
					wsaaStoredValueToAdd.subtract(wsaaStoredDifference);
					wsaaInciSub.subtract(1);
				}
			}
		}
		if (isEQ(wsaaInciSub,1)) {
			if (isNE(incigrvIO.getPremCurr01(),incigrvIO.getPremStart01())) {
				compute(wsaaStoredDifference, 2).set(sub(incigrvIO.getPremStart01(),incigrvIO.getPremCurr01()));
				if (isGTE(wsaaStoredDifference,wsaaStoredValueToAdd)) {
					wsaaInciPerd01.add(wsaaStoredValueToAdd);
					wsaaStoredValueToAdd.set(ZERO);
					goTo(GotoLabel.exit499);
				}
				else {
					wsaaInciPerd01.set(incigrvIO.getPremStart01());
					wsaaStoredValueToAdd.subtract(wsaaStoredDifference);
				}
			}
		}
		if (isNE(wsaaStoredValueToAdd,ZERO)) {
			syserrrec.statuz.set(t744);
			fatalError900();
		}
	}

protected void deleteUtrn500()
	{
		start510();
	}

protected void start510()
	{
		utrncfiIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, utrncfiIO);
		if (isNE(utrncfiIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrncfiIO.getParams());
			syserrrec.statuz.set(utrncfiIO.getStatuz());
			fatalError900();
		}
		utrncfiIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, utrncfiIO);
		if (isNE(utrncfiIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrncfiIO.getParams());
			syserrrec.statuz.set(utrncfiIO.getStatuz());
			fatalError900();
		}
	}

protected void writeNegativeUtrn600()
	{
		start610();
	}

protected void start610()
	{
		utrncfiIO.setFormat(utrncfirec);
		utrncfiIO.setFeedbackInd(SPACES);
		utrncfiIO.setSwitchIndicator(SPACES);
		utrncfiIO.setSurrenderPercent(ZERO);
		setPrecision(utrncfiIO.getProcSeqNo(), 0);
		utrncfiIO.setProcSeqNo(mult(utrncfiIO.getProcSeqNo(),-1));
		setPrecision(utrncfiIO.getContractAmount(), 2);
		utrncfiIO.setContractAmount(mult(utrncfiIO.getContractAmount(),-1));
		setPrecision(utrncfiIO.getFundAmount(), 2);
		utrncfiIO.setFundAmount(mult(utrncfiIO.getFundAmount(),-1));
		setPrecision(utrncfiIO.getNofDunits(), 5);
		utrncfiIO.setNofDunits(mult(utrncfiIO.getNofDunits(),-1));
		setPrecision(utrncfiIO.getNofUnits(), 5);
		utrncfiIO.setNofUnits(mult(utrncfiIO.getNofUnits(),-1));
		setPrecision(utrncfiIO.getInciprm01(), 2);
		utrncfiIO.setInciprm01(mult(utrncfiIO.getInciprm01(),-1));
		setPrecision(utrncfiIO.getInciprm02(), 2);
		utrncfiIO.setInciprm02(mult(utrncfiIO.getInciprm02(),-1));
		utrncfiIO.setTransactionDate(greversrec.transDate);
		utrncfiIO.setTransactionTime(greversrec.transTime);
		utrncfiIO.setTranno(greversrec.newTranno);
		utrncfiIO.setTermid(greversrec.termid);
		utrncfiIO.setUser(greversrec.user);
		wsaaBatckey.set(greversrec.batckey);
		utrncfiIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		utrncfiIO.setBatccoy(wsaaBatckey.batcBatccoy);
		utrncfiIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		utrncfiIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		utrncfiIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		utrncfiIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		utrncfiIO.setUstmno(ZERO);
		utrncfiIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, utrncfiIO);
		if (isNE(utrncfiIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrncfiIO.getParams());
			syserrrec.statuz.set(utrncfiIO.getStatuz());
			fatalError900();
		}
	}

protected void rewrtInci700()
	{
		start710();
	}

protected void start710()
	{
		incigrvIO.setPremCurr01(wsaaInciPerd01);
		incigrvIO.setPremCurr02(wsaaInciPerd02);
		incigrvIO.setPremCurr03(wsaaInciPerd03);
		incigrvIO.setPremCurr04(wsaaInciPerd04);
		incigrvIO.setTransactionDate(greversrec.transDate);
		incigrvIO.setTransactionTime(greversrec.transTime);
		incigrvIO.setUser(greversrec.user);
		incigrvIO.setTermid(greversrec.termid);
		incigrvIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, incigrvIO);
		if (isNE(incigrvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incigrvIO.getParams());
			syserrrec.statuz.set(incigrvIO.getStatuz());
			fatalError900();
		}
	}

protected void a100ReadHitrs()
	{
		/*A110-START*/
		SmartFileCode.execute(appVars, hitrcfiIO);
		if ((isNE(hitrcfiIO.getStatuz(),varcom.oK))
		&& (isNE(hitrcfiIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(hitrcfiIO.getParams());
			syserrrec.statuz.set(hitrcfiIO.getStatuz());
			fatalError900();
		}
		if (isNE(greversrec.chdrcoy,hitrcfiIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,hitrcfiIO.getChdrnum())
		|| isNE(incigrvIO.getLife(),hitrcfiIO.getLife())
		|| isNE(incigrvIO.getCoverage(),hitrcfiIO.getCoverage())
		|| isNE(incigrvIO.getRider(),hitrcfiIO.getRider())
		|| isNE(incigrvIO.getPlanSuffix(),hitrcfiIO.getPlanSuffix())
		|| isNE(greversrec.tranno,hitrcfiIO.getTranno())
		|| isEQ(hitrcfiIO.getStatuz(),varcom.endp)) {
			hitrcfiIO.setStatuz(varcom.endp);
			rewrtInci700();
		}
		else {
			a200ReverseHitrs();
		}
		/*A190-EXIT*/
	}

protected void a200ReverseHitrs()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					a210Start();
				}
				case a270Pass: {
					a270Pass();
				}
				case a290Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a210Start()
	{
		if (isEQ(hitrcfiIO.getInciNum(),0)
		&& isEQ(incigrvIO.getSeqno(),1)) {
			goTo(GotoLabel.a270Pass);
		}
		if (isNE(hitrcfiIO.getInciNum(),incigrvIO.getSeqno())) {
			hitrcfiIO.setFunction(varcom.nextr);
			goTo(GotoLabel.a290Exit);
		}
	}

protected void a270Pass()
	{
		if (isNE(hitrcfiIO.getInciPerd01(),0)) {
			a300ReverseInci();
		}
		if (isEQ(hitrcfiIO.getFeedbackInd(),"N")) {
			a400DeleteHitr();
		}
		else {
			a500WriteNegativeHitr();
		}
		hitrcfiIO.setFunction(varcom.nextr);
	}

protected void a300ReverseInci()
	{
		/*A310-START*/
		wsaaStoredValueToAdd.set(hitrcfiIO.getContractAmount());
		if (isNE(hitrcfiIO.getInciPerd01(),0)
		&& isNE(hitrcfiIO.getInciPerd02(),0)) {
			wsaaInciSub.set(hitrcfiIO.getInciPerd02());
		}
		else {
			wsaaInciSub.set(hitrcfiIO.getInciPerd01());
		}
		updateInciCurrPrem450();
		/*A390-EXIT*/
	}

protected void a400DeleteHitr()
	{
		a410Start();
	}

protected void a410Start()
	{
		hitrcfiIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hitrcfiIO);
		if (isNE(hitrcfiIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrcfiIO.getParams());
			syserrrec.statuz.set(hitrcfiIO.getStatuz());
			fatalError900();
		}
		hitrcfiIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, hitrcfiIO);
		if (isNE(hitrcfiIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrcfiIO.getParams());
			syserrrec.statuz.set(hitrcfiIO.getStatuz());
			fatalError900();
		}
	}

protected void a500WriteNegativeHitr()
	{
		a510Start();
	}

protected void a510Start()
	{
		hitrcfiIO.setFormat(hitrcfirec);
		hitrcfiIO.setFeedbackInd(SPACES);
		hitrcfiIO.setSwitchIndicator(SPACES);
		hitrcfiIO.setSurrenderPercent(ZERO);
		setPrecision(hitrcfiIO.getProcSeqNo(), 0);
		hitrcfiIO.setProcSeqNo(mult(hitrcfiIO.getProcSeqNo(),-1));
		setPrecision(hitrcfiIO.getContractAmount(), 2);
		hitrcfiIO.setContractAmount(mult(hitrcfiIO.getContractAmount(),-1));
		setPrecision(hitrcfiIO.getFundAmount(), 2);
		hitrcfiIO.setFundAmount(mult(hitrcfiIO.getFundAmount(),-1));
		setPrecision(hitrcfiIO.getInciprm01(), 2);
		hitrcfiIO.setInciprm01(mult(hitrcfiIO.getInciprm01(),-1));
		setPrecision(hitrcfiIO.getInciprm02(), 2);
		hitrcfiIO.setInciprm02(mult(hitrcfiIO.getInciprm02(),-1));
		hitrcfiIO.setTranno(greversrec.newTranno);
		wsaaBatckey.set(greversrec.batckey);
		hitrcfiIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		hitrcfiIO.setBatccoy(wsaaBatckey.batcBatccoy);
		hitrcfiIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		hitrcfiIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		hitrcfiIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		hitrcfiIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		hitrcfiIO.setUstmno(ZERO);
		hitrcfiIO.setZintappind(SPACES);
		hitrcfiIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hitrcfiIO);
		if (isNE(hitrcfiIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrcfiIO.getParams());
			syserrrec.statuz.set(hitrcfiIO.getStatuz());
			fatalError900();
		}
	}

protected void fatalError900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start900();
				}
				case end950: {
					end950();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start900()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.end950);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void end950()
	{
		greversrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
