/*
 * File: Cmpy003.java
 * Date: 29 August 2009 22:41:21
 * Author: Quipoz Limited
 *
 * Class transformed from CMPY003.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5564rec;
import com.csc.life.flexiblepremium.dataaccess.AgcmseqTableDAM;
import com.csc.life.flexiblepremium.dataaccess.Fpcolf1TableDAM;
import com.csc.life.flexiblepremium.tablestructures.T5728rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*           COMMISSION RELEASE FOR FLEXIBLE PREMIUM
*
* A) Commision Payment.
*
* Obtain the  commission  payment  rules  table  item  key from
* T5564, keyed  by  Payment  pattern,  Coverage/rider  code and
* effective date (hence use ITDM).
*
* If an increase has occurred, there may be more than one AGCM
* for the component/Agent
*
* Therefore, we only calculate and pay initial commission
* on AGCM's for seqno's > 1 where the amount received in
* the target period is > the total of the annual premium
* for the previous seqno's.
*
* We read all the AGCM records for the current seqno and
* store the total annual premium.
* (ignoring single premium and override records)
*
* If this total is = to the target premium, we know that
* there are no other AGCMs with different seqnos.
*
* Otherwise, if the sequence number is > 1,
* we read all the previous AGCM records for the earlier
* seqno's and total the Annual premium.
* (again ignoring single premium and override records)
*
* Once we have determined the current and previous
* total annual premiums, we read the FPCO records
* and for each one found, move either the total recd
* to working storage if it is less than the target, otherwise
* store the target.
* This is because initial commission is only paid on up to
* target amounts.
*
* The applicable amount recd for the seqno is :
*     amount recd (up to target) - ann prem for prev seq no.
*
* If the result is > the AGCM ann prem, we use the Ann Prem
* as the applicable amount.
*
* At the end of the FPCO's, if the applicable amount is = zero,
* no commission is to be calculated for this AGCM.
*
* If the amount applicable is > zero, commission must be
* calculated based on the % recd.
*
* The % of the annual premium recd is calculated as :
*
* total annual prem for AGCM seqno
* --------------------------------
* derived amount applicable.
*
* Once we have the %, we read T5728 to determine the % of the
* ICOMMTOT that is payable.
*
* Read T5728 using the key from T5564
* -------------------------------------
*
* The Commission Payable and the amount earned are calculated
* differently acccording to the 'as earned commission' flag.
* If the flag is 'y' then the amount of commission to be paid
* is equivalent to the percentage of the premium that has been
* paid.
*
* If the flag is spaces then the bands on the table are
* used to calculate the amount paid.
* The bands are read until one is found where the derived %
* received is > the T5728 recd %. The previous entry is then
* sought.
* The corresponding % to pay is extracted and the ICOMMTOT
* multiplied by this figure giving the payment amount.
*
* The 'earnings' are calculated in a similar way but the
* correct earning recd band is the first one where the % is >
* or = the % recd. No previous band is read.
*
* Once the commission earned and payable have been calculated,
* the previous paid or earned as passed in linkage is subtracted
* from this total.
*
* The amounts to return to the calling program are these values.
*
* B) Commission claw back.
*
* This routine can also be used for reversals. The PAYAMNT is
* a negative value which will cancel out the payments which
* need to be reversed, and the ERNDAMT is returned to the
* value it was at before the premiums concerned were received.
*
*****************************************************************
*****************************************************************
* DATE.... VSN/MOD  WORK UNIT    BY....
*
* 02/02/96  01/01   D9604        Bev Chapman                          *
*           New commission subroutine to be used to calculate
*           values for Flexible Premium Products.
*                                                                     *
* 25/04/96  01/01   D96NUM       Rob Yates                            *
*           Recompile for Monetary fields project                     *
*                                                                     *
* 27/07/04  01/01   SMU002       Annie Ma, CSC Hong Kong              *
*           Tidy REMARKS for Scribe.                                  *
*                                                                     *
**DD/MM/YY*************************************************************
/
* </pre>
*/
public class Cmpy003 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "CMPY003";

	private FixedLengthStringData wsaaT5564Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaMethod = new FixedLengthStringData(4).isAPartOf(wsaaT5564Key, 0).init(SPACES);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5564Key, 4).init(SPACES);
	private FixedLengthStringData wsaaT5728Key = new FixedLengthStringData(8).init(SPACES);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaPercReceived = new PackedDecimalData(10, 7);
	private PackedDecimalData wsaaTotPercRec = new PackedDecimalData(10, 7);
	private PackedDecimalData wsaaProRataRate = new PackedDecimalData(9, 7);
	private PackedDecimalData wsaaProRataPercent = new PackedDecimalData(10, 7);
	private ZonedDecimalData wsaaBand = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaComEarnedBand = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaPremRecd = new PackedDecimalData(16, 7);
	private PackedDecimalData wsaaSeqAnnPrem = new PackedDecimalData(16, 7);
	private PackedDecimalData wsaaPrevAnnPrem = new PackedDecimalData(16, 7);
	private PackedDecimalData wsaaPremApplic = new PackedDecimalData(16, 7);
		/* ERRORS */
	private String h027 = "H027";
	private String d039 = "D039";
	private String fpcolf1rec = "FPCOLF1REC";
	private String agcmseqrec = "AGCMSEQREC";
		/* TABLES */
	private String t5564 = "T5564";
	private String t5728 = "T5728";
		/*AGCMs in sequence number order*/
	private AgcmseqTableDAM agcmseqIO = new AgcmseqTableDAM();
	private Comlinkrec comlinkrec = new Comlinkrec();
		/*Flexible Premium Coverage*/
	private Fpcolf1TableDAM fpcolf1IO = new Fpcolf1TableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T5564rec t5564rec = new T5564rec();
	private T5728rec t5728rec = new T5728rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		pass2050,
		readNextAgcm2150,
		readNextAgcm2250,
		exit2390,
		exit3090,
		exit3290,
		exit3390,
		seExit8090,
		dbExit8190
	}

	public Cmpy003() {
		super();
	}

public void mainline(Object... parmArray)
	{
		comlinkrec.clnkallRec = convertAndSetParam(comlinkrec.clnkallRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		comlinkrec.statuz.set(varcom.oK);
		obtainTables1000();
		calcCommamntPayable3000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void obtainTables1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1000();
					para2000();
				}
				case pass2050: {
					pass2050();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1000()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(comlinkrec.chdrcoy);
		itdmIO.setItemtabl(t5564);
		wsaaMethod.set(comlinkrec.method);
		wsaaCrtable.set(comlinkrec.crtable);
		itdmIO.setItemitem(wsaaT5564Key);
		itdmIO.setItmfrm(comlinkrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5564)
		|| isNE(itdmIO.getItemitem(),wsaaT5564Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(wsaaT5564Key);
			syserrrec.statuz.set(h027);
			dbError8100();
		}
		t5564rec.t5564Rec.set(itdmIO.getGenarea());
		wsaaT5728Key.set(SPACES);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
			if (isEQ(t5564rec.freq[wsaaIndex.toInt()],comlinkrec.billfreq)) {
				wsaaT5728Key.set(t5564rec.itmkey[wsaaIndex.toInt()]);
				wsaaIndex.set(13);
			}
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(comlinkrec.chdrcoy);
		itdmIO.setItemtabl(t5728);
		itdmIO.setItemitem(wsaaT5728Key);
		itdmIO.setItmfrm(comlinkrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5728)
		|| isNE(itdmIO.getItemitem(),wsaaT5728Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(wsaaT5728Key);
			syserrrec.statuz.set(d039);
			dbError8100();
		}
		else {
			t5728rec.t5728Rec.set(itdmIO.getGenarea());
		}
	}

protected void para2000()
	{
		wsaaPremRecd.set(ZERO);
		wsaaPremApplic.set(ZERO);
		wsaaSeqAnnPrem.set(ZERO);
		wsaaPrevAnnPrem.set(ZERO);
		if (isEQ(comlinkrec.annprem,comlinkrec.targetPrem)) {
			wsaaSeqAnnPrem.set(comlinkrec.annprem);
			goTo(GotoLabel.pass2050);
		}
		if (isEQ(comlinkrec.currto,comlinkrec.effdate)) {
			wsaaSeqAnnPrem.set(comlinkrec.targetPrem);
			goTo(GotoLabel.pass2050);
		}
		agcmseqIO.setChdrcoy(comlinkrec.chdrcoy);
		agcmseqIO.setChdrnum(comlinkrec.chdrnum);
		agcmseqIO.setLife(comlinkrec.life);
		agcmseqIO.setCoverage(comlinkrec.coverage);
		agcmseqIO.setRider(comlinkrec.rider);
		agcmseqIO.setPlanSuffix(comlinkrec.planSuffix);
		agcmseqIO.setSeqno(comlinkrec.seqno);
		agcmseqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmseqIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		agcmseqIO.setFormat(agcmseqrec);
		agcmseqIO.setStatuz(varcom.oK);
		SmartFileCode.execute(appVars, agcmseqIO);
		if (isNE(agcmseqIO.getStatuz(),varcom.oK)
		&& isNE(agcmseqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(agcmseqIO.getStatuz());
			syserrrec.params.set(agcmseqIO.getParams());
		}
		if (isNE(agcmseqIO.getChdrcoy(),comlinkrec.chdrcoy)
		|| isNE(agcmseqIO.getChdrnum(),comlinkrec.chdrnum)
		|| isNE(agcmseqIO.getLife(),comlinkrec.life)
		|| isNE(agcmseqIO.getCoverage(),comlinkrec.coverage)
		|| isNE(agcmseqIO.getRider(),comlinkrec.rider)
		|| isNE(agcmseqIO.getPlanSuffix(),comlinkrec.planSuffix)
		|| isNE(agcmseqIO.getSeqno(),comlinkrec.seqno)) {
			agcmseqIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(agcmseqIO.getStatuz(),varcom.endp))) {
			processAgcm2100();
		}

		if (isEQ(wsaaSeqAnnPrem,comlinkrec.targetPrem)
		|| isEQ(comlinkrec.seqno,1)) {
			goTo(GotoLabel.pass2050);
		}
		if (isEQ(wsaaSeqAnnPrem,ZERO)) {
			wsaaSeqAnnPrem.set(comlinkrec.targetPrem);
			goTo(GotoLabel.pass2050);
		}
		agcmseqIO.setChdrcoy(comlinkrec.chdrcoy);
		agcmseqIO.setChdrnum(comlinkrec.chdrnum);
		agcmseqIO.setLife(comlinkrec.life);
		agcmseqIO.setCoverage(comlinkrec.coverage);
		agcmseqIO.setRider(comlinkrec.rider);
		agcmseqIO.setPlanSuffix(comlinkrec.planSuffix);
		agcmseqIO.setSeqno(0);
		agcmseqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmseqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmseqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		agcmseqIO.setFormat(agcmseqrec);
		agcmseqIO.setStatuz(varcom.oK);
		SmartFileCode.execute(appVars, agcmseqIO);
		if (isNE(agcmseqIO.getStatuz(),varcom.oK)
		&& isNE(agcmseqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(agcmseqIO.getStatuz());
			syserrrec.params.set(agcmseqIO.getParams());
		}
		if (isNE(agcmseqIO.getChdrcoy(),comlinkrec.chdrcoy)
		|| isNE(agcmseqIO.getChdrnum(),comlinkrec.chdrnum)
		|| isNE(agcmseqIO.getLife(),comlinkrec.life)
		|| isNE(agcmseqIO.getCoverage(),comlinkrec.coverage)
		|| isNE(agcmseqIO.getRider(),comlinkrec.rider)
		|| isNE(agcmseqIO.getPlanSuffix(),comlinkrec.planSuffix)
		|| isGTE(agcmseqIO.getSeqno(),comlinkrec.seqno)) {
			agcmseqIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(agcmseqIO.getStatuz(),varcom.endp))) {
			prevSeq2200();
		}

	}

protected void pass2050()
	{
		calcFpcoTotal2300();
		/*EXIT*/
	}

protected void processAgcm2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2101();
				}
				case readNextAgcm2150: {
					readNextAgcm2150();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2101()
	{
		if (isEQ(agcmseqIO.getOvrdcat(),"O")
		|| isEQ(agcmseqIO.getPtdate(),0)) {
			goTo(GotoLabel.readNextAgcm2150);
		}
		wsaaSeqAnnPrem.add(agcmseqIO.getAnnprem());
	}

protected void readNextAgcm2150()
	{
		agcmseqIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmseqIO);
		if (isNE(agcmseqIO.getStatuz(),varcom.oK)
		&& isNE(agcmseqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(agcmseqIO.getStatuz());
			syserrrec.params.set(agcmseqIO.getParams());
		}
		if (isNE(agcmseqIO.getChdrcoy(),comlinkrec.chdrcoy)
		|| isNE(agcmseqIO.getChdrnum(),comlinkrec.chdrnum)
		|| isNE(agcmseqIO.getLife(),comlinkrec.life)
		|| isNE(agcmseqIO.getCoverage(),comlinkrec.coverage)
		|| isNE(agcmseqIO.getRider(),comlinkrec.rider)
		|| isNE(agcmseqIO.getPlanSuffix(),comlinkrec.planSuffix)
		|| isNE(agcmseqIO.getSeqno(),comlinkrec.seqno)) {
			agcmseqIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void prevSeq2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2201();
				}
				case readNextAgcm2250: {
					readNextAgcm2250();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2201()
	{
		if (isEQ(agcmseqIO.getOvrdcat(),"O")
		|| isEQ(agcmseqIO.getPtdate(),0)) {
			goTo(GotoLabel.readNextAgcm2250);
		}
		wsaaPrevAnnPrem.add(agcmseqIO.getAnnprem());
	}

protected void readNextAgcm2250()
	{
		agcmseqIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmseqIO);
		if (isNE(agcmseqIO.getStatuz(),varcom.oK)
		&& isNE(agcmseqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(agcmseqIO.getStatuz());
			syserrrec.params.set(agcmseqIO.getParams());
		}
		if (isNE(agcmseqIO.getChdrcoy(),comlinkrec.chdrcoy)
		|| isNE(agcmseqIO.getChdrnum(),comlinkrec.chdrnum)
		|| isNE(agcmseqIO.getLife(),comlinkrec.life)
		|| isNE(agcmseqIO.getCoverage(),comlinkrec.coverage)
		|| isNE(agcmseqIO.getRider(),comlinkrec.rider)
		|| isNE(agcmseqIO.getPlanSuffix(),comlinkrec.planSuffix)
		|| isEQ(agcmseqIO.getSeqno(),comlinkrec.seqno)) {
			agcmseqIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void calcFpcoTotal2300()
	{
		try {
			start2301();
		}
		catch (GOTOException e){
		}
	}

protected void start2301()
	{
		fpcolf1IO.setChdrcoy(comlinkrec.chdrcoy);
		fpcolf1IO.setChdrnum(comlinkrec.chdrnum);
		fpcolf1IO.setLife(comlinkrec.life);
		fpcolf1IO.setCoverage(comlinkrec.coverage);
		fpcolf1IO.setRider(comlinkrec.rider);
		fpcolf1IO.setPlanSuffix(comlinkrec.planSuffix);
		fpcolf1IO.setTargfrom(comlinkrec.effdate);
		fpcolf1IO.setActiveInd(SPACES);
		fpcolf1IO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fpcolf1IO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fpcolf1IO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		fpcolf1IO.setFormat(fpcolf1rec);
		fpcolf1IO.setStatuz(varcom.oK);
		while ( !(isEQ(fpcolf1IO.getStatuz(),varcom.endp))) {
			SmartFileCode.execute(appVars, fpcolf1IO);
			if (isNE(fpcolf1IO.getStatuz(),varcom.oK)
			&& isNE(fpcolf1IO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(fpcolf1IO.getParams());
				dbError8100();
			}
			if (isNE(fpcolf1IO.getChdrcoy(),comlinkrec.chdrcoy)
			|| isNE(fpcolf1IO.getChdrnum(),comlinkrec.chdrnum)
			|| isNE(fpcolf1IO.getLife(),comlinkrec.life)
			|| isNE(fpcolf1IO.getCoverage(),comlinkrec.coverage)
			|| isNE(fpcolf1IO.getRider(),comlinkrec.rider)
			|| isNE(fpcolf1IO.getPlanSuffix(),comlinkrec.planSuffix)
			|| isEQ(fpcolf1IO.getStatuz(),varcom.endp)) {
				fpcolf1IO.setStatuz(varcom.endp);
				goTo(GotoLabel.exit2390);
			}
			if (isEQ(fpcolf1IO.getTargto(),comlinkrec.currto)) {
				setPrecision(fpcolf1IO.getPremRecPer(), 2);
				fpcolf1IO.setPremRecPer(sub(fpcolf1IO.getPremRecPer(),comlinkrec.instprem));
			}
			if (isGT(fpcolf1IO.getPremRecPer(),fpcolf1IO.getTargetPremium())) {
				wsaaPremRecd.set(fpcolf1IO.getTargetPremium());
			}
			else {
				wsaaPremRecd.set(fpcolf1IO.getPremRecPer());
			}
			compute(wsaaPremRecd, 7).set(sub(wsaaPremRecd,wsaaPrevAnnPrem));
			if (isGTE(wsaaPremRecd,wsaaSeqAnnPrem)) {
				wsaaPremApplic.add(wsaaSeqAnnPrem);
			}
			else {
				wsaaPremApplic.add(wsaaPremRecd);
			}
			fpcolf1IO.setFunction(varcom.nextr);
		}

	}

protected void calcCommamntPayable3000()
	{
		try {
			para3000();
		}
		catch (GOTOException e){
		}
	}

protected void para3000()
	{
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		if (isLTE(wsaaPremApplic,ZERO)) {
			compute(comlinkrec.payamnt, 2).set(mult(comlinkrec.icommpd,-1));
			compute(comlinkrec.erndamt, 2).set(mult(comlinkrec.icommernd,-1));
			goTo(GotoLabel.exit3090);
		}
		if (isNE(t5728rec.commAsEarned,SPACES)) {
			commAsEarned3100();
		}
		else {
			notEarned3200();
		}
		earnedCommission3300();
	}

protected void commAsEarned3100()
	{
		/*PARA*/
		compute(wsaaPercReceived, 8).setRounded(mult(div(wsaaPremApplic,wsaaSeqAnnPrem),100));
		if (isGT(wsaaPercReceived,100)) {
			wsaaPercReceived.set(100);
		}
		compute(comlinkrec.payamnt, 8).setRounded(sub((mult(div(wsaaPercReceived,100),comlinkrec.icommtot)),comlinkrec.icommpd));
		/*EXIT*/
	}

protected void notEarned3200()
	{
		try {
			para3200();
		}
		catch (GOTOException e){
		}
	}

protected void para3200()
	{
		compute(wsaaTotPercRec, 7).set(mult(div(wsaaPremApplic,wsaaSeqAnnPrem),100));
		wsaaBand.set(ZERO);
		for (ix.set(1); !(isGT(ix,14)); ix.add(1)){
			if (isGT(t5728rec.tgtRcvdPay[ix.toInt()],wsaaTotPercRec)
			|| isEQ(t5728rec.tgtRcvdPay[ix.toInt()],ZERO)) {
				compute(ix, 0).set(sub(ix,1));
				wsaaBand.set(ix);
				ix.set(15);
			}
			else {
				if (isEQ(t5728rec.tgtRcvdPay[ix.toInt()],wsaaTotPercRec)) {
					wsaaBand.set(ix);
					ix.set(15);
				}
			}
		}
		if (isEQ(wsaaBand,ZERO)) {
			if (isGT(comlinkrec.icommpd,ZERO)) {
				compute(comlinkrec.payamnt, 2).set(mult(comlinkrec.icommpd,-1));
			}
			else {
				comlinkrec.payamnt.set(ZERO);
			}
			goTo(GotoLabel.exit3290);
		}
		compute(comlinkrec.payamnt, 3).setRounded(sub(mult(div(t5728rec.commRel[wsaaBand.toInt()],100),comlinkrec.icommtot),comlinkrec.icommpd));
	}

protected void earnedCommission3300()
	{
		try {
			start3300();
		}
		catch (GOTOException e){
		}
	}

protected void start3300()
	{
		compute(wsaaTotPercRec, 8).setRounded(mult(div(wsaaPremApplic,wsaaSeqAnnPrem),100));
		for (ix.set(1); !(isGT(ix,14)); ix.add(1)){
			if (isGTE(t5728rec.tgtRcvdEarn[ix.toInt()],wsaaTotPercRec)) {
				wsaaComEarnedBand.set(ix);
				ix.set(15);
			}
			else {
				if (isEQ(t5728rec.tgtRcvdEarn[ix.toInt()],ZERO)) {
					wsaaComEarnedBand.set(99);
					ix.set(15);
				}
			}
		}
		if (isEQ(wsaaComEarnedBand,99)) {
			compute(comlinkrec.erndamt, 2).set(sub(comlinkrec.icommtot,comlinkrec.icommernd));
			goTo(GotoLabel.exit3390);
		}
		if (isEQ(t5728rec.commEarn[wsaaComEarnedBand.toInt()],0)) {
			comlinkrec.erndamt.set(0);
			goTo(GotoLabel.exit3390);
		}
		compute(wsaaProRataRate, 7).set(div(t5728rec.tgtRcvdEarn[wsaaComEarnedBand.toInt()],t5728rec.commEarn[wsaaComEarnedBand.toInt()]));
		compute(wsaaProRataPercent, 7).set(div(wsaaTotPercRec,wsaaProRataRate));
		compute(comlinkrec.erndamt, 8).setRounded(sub((mult(div(wsaaProRataPercent,100),comlinkrec.icommtot)),comlinkrec.icommernd));
	}

protected void systemError8000()
	{
		try {
			se8000();
		}
		catch (GOTOException e){
		}
		finally{
			seExit8090();
		}
	}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit8090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		comlinkrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
		try {
			db8100();
		}
		catch (GOTOException e){
		}
		finally{
			dbExit8190();
		}
	}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit8190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		comlinkrec.statuz.set(varcom.bomb);
		exit090();
	}
}
