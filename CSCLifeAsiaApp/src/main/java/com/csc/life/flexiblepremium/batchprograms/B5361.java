/*
 * File: B5361.java
 * Date: 29 August 2009 21:12:07
 * Author: Quipoz Limited
 *
 * Class transformed from B5361.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;

import java.util.List;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.flexiblepremium.dataaccess.FpcoTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FpcxpfTableDAM;
import com.csc.life.flexiblepremium.reports.R5361Report;
import com.csc.life.flexiblepremium.tablestructures.T5729rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.ArraySearch;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*
*REMARKS
*            FLEXIBLE PREMIUM ANNIVERSARY PROCESSING
*            ---------------------------------------
*
* Flexible Premium Anniversary Processing processes the Flexible
* Premium Coverage File (FPCOPF) which are selected by B5360.
*
* Processing.
* ----------
* B5361 runs directly after B5360 which 'splits' the FPCOPF
* according to the number of Anniversary programs to run.  All
* references to the FPCO are via FPCXPF - a temporary file
* holding all the FPCO records for this program to process.
*
*  1000-INITIALISE.
*  ---------------
*  -  Frequently-referenced tables are stored in working storage
*     arrays to minimize disk IO.
*
*  -  Issue an override to read the correct FPCXPF for this run.
*     The CRTTMPF process has already created a file specific to
*     the run using the  FPCXPF format and is identified  by
*     concatenating the following:-
*
*     'FPCX'
*      BPRD-SYSTEM-PARAM04
*      BSSC-SCHEDULE-NUMBER
*
*      eg FPCXFP0001,  for the first run
*         FPCXFP0002,  for the second etc.
*
*     The number of threads would have been created by the
*     CRTTMPF process given the parameters of the process
*     definition of the CRTTMPF.
*     To get the correct member of the above physical for B5353
*     to read from, concatenate :-
*
*     'THREAD'
*     BSPR-PROCESS-OCC-NUM
*
*     eg. THREAD001,   for the first member (thread 1)
*         THREAD002    for the second etc.  (thread 2)
*
*  -  Initialise the static values of the LIFACMV copybook.
*
*  2000-READ
*  ---------
*
*  -  Read the FPCX records sequentially keeping the count of
*     the number read and storing the present FPCO details for
*     the WSYS- error record.
*
*
*  2500-EDIT
*  ---------
*
*     For Each FPCX  record:
*
*  -  Search T6654 for number of lead days for contract
*
*  -  If the current to date on the FPCX is greater than the
*     effective date plus the number of lead days, ignore this
*     FPCX record and read the next
*
*  -  Validate the contract header details, if not valid read
*     next record
*
*  -  Read the FPCO record and validate the coverage
*
*  -  If this is the first FPCX read for the contract add 1 to
*     the transaction number on the CHDR and write a PTRN
*
*  3000-UPDATE
*  -----------
*
*  -  If the premium received in the period on the FPCO is less
*     than the target premium, write an entry to the report
*
*     If the target premium received is > or = the target premium
*     move 'N' to the FPCO active indicator
*
*  -  Move 'Y' to the Anniversary Process Indicator, update the
*     transaction number and rewrite the FPCO
*
*  -  Write a new FPCO with an Anniversary Process Indicator of
*     spaces and current from date = previous current to date and
*     current to date = current to date + 1 year
*
* Control totals used in this program:
*
*    01  -  No. of FPCO records read
*    02  -  No. of FPCO records written
*    03  -  No. report entries written
*    04  -  No. of PTRN records created
*    05  -  No. of contracts softlocked
*    06  -  FPCOs softlocked
*    07  -  Anniversary date not reached
*
*     (BATC processing is handled in MAINB)
*
***********************************************************************
* </pre>
*/
public class B5361 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FpcxpfTableDAM fpcxpf = new FpcxpfTableDAM();
	private R5361Report printerFile = new R5361Report();
	private FpcxpfTableDAM fpcxpfRec = new FpcxpfTableDAM();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
		/* ERRORS */
	private static final String h791 = "H791";
	private static final String i086 = "I086";
	private static final String f035 = "F035";
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5361");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/*  FPCX parameters*/
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaFpcxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaFpcxFn, 0, FILLER).init("FPCX");
	private FixedLengthStringData wsaaFpcxRunid = new FixedLengthStringData(2).isAPartOf(wsaaFpcxFn, 4);
	private ZonedDecimalData wsaaFpcxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaFpcxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaStoredChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaALockedChdrnum = new FixedLengthStringData(8).init(SPACES);
	private ZonedDecimalData wsaaOldCurrto = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaEffdatePlusCntlead = new PackedDecimalData(8, 0).init(0);
		/*  Subscripts*/
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaT5729Sub = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaTest = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaNumPeriod = new ZonedDecimalData(11, 5).init(0);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTempDate = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaFormattedDate = new FixedLengthStringData(10).init(SPACES);

		/*  Status indicators*/
	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("N");
	private Validator firstTimeIn = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaReportWritten = new FixedLengthStringData(1).init("N");
	private Validator reportWritten = new Validator(wsaaReportWritten, "Y");

	private FixedLengthStringData wsaaDateFound = new FixedLengthStringData(1).init("N");
	private Validator dateFound = new Validator(wsaaDateFound, "Y");
	private Validator dateNotFound = new Validator(wsaaDateFound, "N");

	private FixedLengthStringData wsaaFreqFound = new FixedLengthStringData(1).init("N");
	private Validator freqFound = new Validator(wsaaFreqFound, "Y");

	private FixedLengthStringData wsaaDurationFound = new FixedLengthStringData(1).init("N");
	private Validator durationFound = new Validator(wsaaDurationFound, "Y");
	private Validator durationNotFound = new Validator(wsaaDurationFound, "N");
		/* Arrays to store regularly-referenced data
		  Storage for T6654 items*/
	//private static final int wsaaT6654Size = 500;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT6654Size = 1000;

	private FixedLengthStringData wsaaT6654Key2 = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBillchnl2 = new FixedLengthStringData(1).isAPartOf(wsaaT6654Key2, 0);
	private FixedLengthStringData wsaaCnttype2 = new FixedLengthStringData(3).isAPartOf(wsaaT6654Key2, 1);

		/* WSAA-T6654-ARRAY
		 03  WSAA-T6654-REC OCCURS 180                                */
	private FixedLengthStringData[] wsaaT6654Rec = FLSInittedArray (1000, 15);
	private FixedLengthStringData[] wsaaT6654Key = FLSDArrayPartOfArrayStructure(4, wsaaT6654Rec, 0);
	private FixedLengthStringData[] wsaaT6654Billchnl = FLSDArrayPartOfArrayStructure(1, wsaaT6654Key, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT6654Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT6654Key, 1, HIVALUE);
	private FixedLengthStringData[] wsaaT6654Data = FLSDArrayPartOfArrayStructure(11, wsaaT6654Rec, 4);
	private FixedLengthStringData[] wsaaT6654Collsub = FLSDArrayPartOfArrayStructure(8, wsaaT6654Data, 0);
	private ZonedDecimalData[] wsaaT6654Leadday = ZDArrayPartOfArrayStructure(3, 0, wsaaT6654Data, 8);

		/*  Storage for T5729 table items.*/
	private FixedLengthStringData wsaaDurations = new FixedLengthStringData(16);
	private ZonedDecimalData[] wsaaDuration = ZDArrayPartOfStructure(4, 4, 0, wsaaDurations, 0);

	private FixedLengthStringData wsaaOverdueMins = new FixedLengthStringData(12);
	private ZonedDecimalData[] wsaaOverdueMin = ZDArrayPartOfStructure(4, 3, 0, wsaaOverdueMins, 0);
	private FixedLengthStringData wsaaT5729Item = new FixedLengthStringData(4);
	//private static final int wsaaT5729Size = 60;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5729Size = 1000;

	private FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(146);
	private FixedLengthStringData wsysFpcokey = new FixedLengthStringData(26).isAPartOf(wsysSystemErrorParams, 0);
	private FixedLengthStringData wsysChdrcoy = new FixedLengthStringData(2).isAPartOf(wsysFpcokey, 0);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysFpcokey, 2);
	private FixedLengthStringData wsysCoverage = new FixedLengthStringData(2).isAPartOf(wsysFpcokey, 10);
	private FixedLengthStringData wsysLife = new FixedLengthStringData(2).isAPartOf(wsysFpcokey, 12);
	private FixedLengthStringData wsysRider = new FixedLengthStringData(2).isAPartOf(wsysFpcokey, 14);
	private ZonedDecimalData wsysPlnsfx = new ZonedDecimalData(2, 0).isAPartOf(wsysFpcokey, 16).setUnsigned();
	private ZonedDecimalData wsysTargfrom = new ZonedDecimalData(8, 0).isAPartOf(wsysFpcokey, 18).setUnsigned();
	private FixedLengthStringData wsysSysparams = new FixedLengthStringData(120).isAPartOf(wsysSystemErrorParams, 26);
		/* TABLES */
	private static final String t5679 = "T5679";
	private FixedLengthStringData t6654 = new FixedLengthStringData(6).init("T6654");
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
	private static final String t1693 = "T1693";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;
	private static final int ct07 = 7;
	private static final int ct08 = 8;
	private static final int ct09 = 9;
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String ptrnrec = "PTRNREC";
	private static final String covrrec = "COVRREC";
	private static final String fpcorec = "FPCOREC";
	private static final String BTPRO028 = "BTPRO028";
	private boolean BTPRO028Permission = false;

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData r5361h01Record = new FixedLengthStringData(31);
	private FixedLengthStringData r5361h01O = new FixedLengthStringData(31).isAPartOf(r5361h01Record, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(r5361h01O, 0);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(r5361h01O, 1);

	private FixedLengthStringData r5361t01Record = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaT6654Ix = new IntegerData();
	private IntegerData wsaaT5729Ix = new IntegerData();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FpcoTableDAM fpcoIO = new FpcoTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private T5679rec t5679rec = new T5679rec();
	private T6654rec t6654rec = new T6654rec();
	private T5729rec t5729rec = new T5729rec();
	private R5361d01RecordInner r5361d01RecordInner = new R5361d01RecordInner();
	private WsaaT5729ArrayInner wsaaT5729ArrayInner = new WsaaT5729ArrayInner();
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected Itempf itempf = null;
	
	public B5361() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
	    BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		firstTimeIn.setTrue();
		wsaaReportWritten.set("N");
		/*  Point to correct member of FPCXPF.*/
		wsaaFpcxRunid.set(bprdIO.getSystemParam04());
		wsaaFpcxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(FPCXPF) TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaFpcxFn);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		fpcxpf.openInput();
		printerFile.openOutput();
		/*  Initialise common LIFA, LIFR, RNLA and PTRN fields*/
		varcom.vrcmDate.set(getCobolDate());
		ptrnIO.setParams(SPACES);
		ptrnIO.setUser(0);
		ptrnIO.setPtrneff(bsscIO.getEffectiveDate());
		ptrnIO.setBatccoy(batcdorrec.company);
		ptrnIO.setBatcactyr(batcdorrec.actyear);
		ptrnIO.setBatcactmn(batcdorrec.actmonth);
		ptrnIO.setBatctrcde(batcdorrec.trcde);
		ptrnIO.setBatcbatch(batcdorrec.batch);
		ptrnIO.setBatcbrn(batcdorrec.branch);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		/*  Load table T6654*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t6654);
		itemIO.setItemitem(SPACES);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		wsaaT6654Ix.set(1);
		while ( !(isEQ(itemIO.getStatuz(),varcom.endp))) {
			loadT66541500();
		}

		/*  Load Table T5729*/
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(t5729);
		itdmIO.setItemitem(SPACES);
		itdmIO.setItmto(0);
		itdmIO.setItmfrm(0);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL");
		wsaaT5729Ix.set(1);
		wsaaT5729Sub.set(1);
		while ( !(isEQ(itdmIO.getStatuz(),varcom.endp))) {
			loadT57291600();
		}

		/*  Load Contract statii.*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void loadT66541500()
	{
			start1510();
		}

protected void start1510()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.endp)
		|| isNE(itemIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itemIO.getItemtabl(),t6654)) {
			itemIO.setStatuz(varcom.endp);
			return ;
		}
		if (isGT(wsaaT6654Ix,wsaaT6654Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(t6654);
			fatalError600();
		}
		t6654rec.t6654Rec.set(itemIO.getGenarea());
		wsaaT6654Key[wsaaT6654Ix.toInt()].set(itemIO.getItemitem());
		wsaaT6654Collsub[wsaaT6654Ix.toInt()].set(t6654rec.collectsub);
		wsaaT6654Leadday[wsaaT6654Ix.toInt()].set(t6654rec.leadDays);
		itemIO.setFunction(varcom.nextr);
		wsaaT6654Ix.add(1);
	}

protected void loadT57291600()
	{
			start1610();
		}

protected void start1610()
	{
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),bprdIO.getCompany())
		|| isNE(itdmIO.getItemtabl(),t5729)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setStatuz(varcom.endp);
			return ;
		}
		t5729rec.t5729Rec.set(itdmIO.getGenarea());
		if (isGT(wsaaT5729Ix,wsaaT5729Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(t5729);
			fatalError600();
		}
		wsaaT5729ArrayInner.wsaaT5729Key[wsaaT5729Ix.toInt()].set(itdmIO.getItemitem());
		wsaaT5729ArrayInner.wsaaT5729Itmfrm[wsaaT5729Ix.toInt()].set(itdmIO.getItmfrm());
		for (wsaaT5729Sub.set(1); !(isGT(wsaaT5729Sub,6)); wsaaT5729Sub.add(1)){
			wsaaT5729ArrayInner.wsaaT5729Frqcys[wsaaT5729Ix.toInt()].set(t5729rec.frqcys);
		}
		wsaaT5729Sub.set(1);
		for (wsaaT5729Sub.set(1); !(isGT(wsaaT5729Sub,4)); wsaaT5729Sub.add(1)){
			wsaaT5729ArrayInner.wsaaT5729Durationas[wsaaT5729Ix.toInt()].set(t5729rec.durationas);
			wsaaT5729ArrayInner.wsaaT5729Durationbs[wsaaT5729Ix.toInt()].set(t5729rec.durationbs);
			wsaaT5729ArrayInner.wsaaT5729Durationcs[wsaaT5729Ix.toInt()].set(t5729rec.durationcs);
			wsaaT5729ArrayInner.wsaaT5729Durationds[wsaaT5729Ix.toInt()].set(t5729rec.durationds);
			wsaaT5729ArrayInner.wsaaT5729Durationes[wsaaT5729Ix.toInt()].set(t5729rec.durationes);
			wsaaT5729ArrayInner.wsaaT5729Durationfs[wsaaT5729Ix.toInt()].set(t5729rec.durationfs);
			wsaaT5729ArrayInner.wsaaT5729OverdueMinas[wsaaT5729Ix.toInt()].set(t5729rec.overdueMinas);
			wsaaT5729ArrayInner.wsaaT5729OverdueMinbs[wsaaT5729Ix.toInt()].set(t5729rec.overdueMinbs);
			wsaaT5729ArrayInner.wsaaT5729OverdueMincs[wsaaT5729Ix.toInt()].set(t5729rec.overdueMincs);
			wsaaT5729ArrayInner.wsaaT5729OverdueMinds[wsaaT5729Ix.toInt()].set(t5729rec.overdueMinds);
			wsaaT5729ArrayInner.wsaaT5729OverdueMines[wsaaT5729Ix.toInt()].set(t5729rec.overdueMines);
			wsaaT5729ArrayInner.wsaaT5729OverdueMinfs[wsaaT5729Ix.toInt()].set(t5729rec.overdueMinfs);
			wsaaT5729ArrayInner.wsaaT5729TargetMaxas[wsaaT5729Ix.toInt()].set(t5729rec.targetMaxas);
			wsaaT5729ArrayInner.wsaaT5729TargetMaxbs[wsaaT5729Ix.toInt()].set(t5729rec.targetMaxbs);
			wsaaT5729ArrayInner.wsaaT5729TargetMaxcs[wsaaT5729Ix.toInt()].set(t5729rec.targetMaxcs);
			wsaaT5729ArrayInner.wsaaT5729TargetMaxds[wsaaT5729Ix.toInt()].set(t5729rec.targetMaxds);
			wsaaT5729ArrayInner.wsaaT5729TargetMaxes[wsaaT5729Ix.toInt()].set(t5729rec.targetMaxes);
			wsaaT5729ArrayInner.wsaaT5729TargetMaxfs[wsaaT5729Ix.toInt()].set(t5729rec.targetMaxfs);
			wsaaT5729ArrayInner.wsaaT5729TargetMinas[wsaaT5729Ix.toInt()].set(t5729rec.targetMinas);
			wsaaT5729ArrayInner.wsaaT5729TargetMinbs[wsaaT5729Ix.toInt()].set(t5729rec.targetMinbs);
			wsaaT5729ArrayInner.wsaaT5729TargetMincs[wsaaT5729Ix.toInt()].set(t5729rec.targetMincs);
			wsaaT5729ArrayInner.wsaaT5729TargetMinds[wsaaT5729Ix.toInt()].set(t5729rec.targetMinds);
			wsaaT5729ArrayInner.wsaaT5729TargetMines[wsaaT5729Ix.toInt()].set(t5729rec.targetMines);
			wsaaT5729ArrayInner.wsaaT5729TargetMinfs[wsaaT5729Ix.toInt()].set(t5729rec.targetMinfs);
		}
		wsaaT5729Ix.add(1);
		itdmIO.setFunction(varcom.nextr);
	}

protected void readFile2000()
	{
			readFile2010();
		}

protected void readFile2010()
	{
		/*  Read the records from the temporary FPCO file.*/
		fpcxpf.read(fpcxpfRec);
		if (fpcxpf.isAtEnd()) {
			if (reportWritten.isTrue()) {
				printerFile.printR5361t01(r5361t01Record, indicArea);
			}
			wsspEdterror.set(varcom.endp);
			return ;
		}
		/*  Set up the key for the SYSR- copybook should a system error*/
		/*  for this instalment occur.*/
		wsysChdrcoy.set(fpcxpfRec.chdrcoy);
		wsysChdrnum.set(fpcxpfRec.chdrnum);
		wsysLife.set(fpcxpfRec.life);
		wsysCoverage.set(fpcxpfRec.coverage);
		wsysRider.set(fpcxpfRec.rider);
		wsysPlnsfx.set(fpcxpfRec.planSuffix);
		wsysTargfrom.set(fpcxpfRec.targfrom);
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
	}

protected void edit2500()
	{
			edit2510();
		}

protected void edit2510()
	{
		wsspEdterror.set(varcom.oK);
		if (!firstTimeIn.isTrue()) {
			if (isNE(fpcxpfRec.chdrnum,wsaaStoredChdrnum)) {
				firstTimeIn.setTrue();
				/**          PERFORM 2300-UNLOCK-POLICY                             */
			}
		}
		calcLeadDays2600();
		/* IF TARGTO                  > WSAA-EFFDATE-PLUS-CNTLEAD<D9604>*/
		if (isGT(fpcxpfRec.annivProcDate,wsaaEffdatePlusCntlead)) {
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			return ;
		}
		/*  Here we must validate the CHDR details.*/
		validateContract2700();
		if (!validContract.isTrue()) {
			contotrec.totno.set(ct08);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			return ;
		}
		validateCoverage2800();
		if (!validCoverage.isTrue()) {
			contotrec.totno.set(ct09);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			return ;
		}
		if (isEQ(fpcxpfRec.chdrnum,wsaaALockedChdrnum)) {
			contotrec.totno.set(ct06);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			return ;
		}
		/*    IF  CHDRNUM NOT = WSAA-STORED-CHDRNUM                        */
		/*        PERFORM 2400-SOFTLOCK-POLICY                             */
		/*        IF  SFTL-STATUZ             = 'LOCK'                     */
		/*            MOVE CHDRNUM TO WSAA-A-LOCKED-CHDRNUM                */
		/*            MOVE SPACES         TO WSSP-EDTERROR         <D9604> */
		/*            GO TO 2590-EXIT                                      */
		/*        ELSE                                                     */
		/*            MOVE SPACES             TO WSAA-A-LOCKED-CHDRNUM     */
		/*        END-IF                                                   */
		/*    END-IF.                                                      */
		/*  Read FPCO record*/
		fpcoIO.setChdrcoy(fpcxpfRec.chdrcoy);
		fpcoIO.setChdrnum(fpcxpfRec.chdrnum);
		fpcoIO.setLife(fpcxpfRec.life);
		fpcoIO.setCoverage(fpcxpfRec.coverage);
		fpcoIO.setRider(fpcxpfRec.rider);
		fpcoIO.setPlanSuffix(fpcxpfRec.planSuffix);
		fpcoIO.setTargfrom(fpcxpfRec.targfrom);
		fpcoIO.setFormat(fpcorec);
		fpcoIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fpcoIO.getParams());
			fatalError600();
		}
		/*only write a ptrn for each change of contract*/
		if (firstTimeIn.isTrue()) {
			wsaaTranno.set(chdrlifIO.getTranno());
			wsaaTranno.add(1);
			writePtrn2900();
			wsaaFirstTime.set("N");
			wsaaStoredChdrnum.set(fpcxpfRec.chdrnum);
		}
	}

protected void unlockPolicy2300()
	{
		start2310();
	}

protected void start2310()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.enttyp.set("CH");
		sftlockrec.company.set(bsprIO.getCompany());
		sftlockrec.user.set(99999);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.entity.set(wsaaStoredChdrnum);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			wsysSysparams.set(sftlockrec.sftlockRec);
			syserrrec.params.set(wsysSystemErrorParams);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void softlockPolicy2400()
	{
		softlockPolicy2410();
	}

protected void softlockPolicy2410()
	{
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(fpcxpfRec.chdrcoy);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(fpcxpfRec.chdrnum);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(fpcxpfRec.chdrcoy);
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(fpcxpfRec.chdrnum);
			stringVariable1.setStringInto(conlogrec.params);
			conlogrec.error.set(sftlockrec.statuz);
			callConlog003();
			contotrec.totno.set(ct05);
			contotrec.totval.set(1);
			callContot001();
			contotrec.totno.set(ct06);
			contotrec.totval.set(1);
			callContot001();
		}
		else {
			if (isNE(sftlockrec.statuz,varcom.oK)) {
				syserrrec.params.set(sftlockrec.sftlockRec);
				syserrrec.statuz.set(sftlockrec.statuz);
				fatalError600();
			}
		}
	}

protected void calcLeadDays2600()
	{
		start2610();
	}

protected boolean readT6654(String key) {
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(chdrlifIO.getChdrcoy().toString());
	itempf.setItemtabl(t6654.toString());
	itempf.setItemitem(key);
	itempf = itempfDAO.getItempfRecord(itempf);
	if (null != itempf) {
		t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		return true;
	}
	return false;
}

protected void start2610()
	{
		/* We should read the contract header record*/
		chdrlifIO.setChdrcoy(fpcxpfRec.chdrcoy);
		chdrlifIO.setChdrnum(fpcxpfRec.chdrnum);
		chdrlifIO.setFunction(varcom.readh);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		/*  Search the array to find the subroutine for the contract*/
		/*  required. If this is not found then the contract type (CNTTYPE)*/
		/*  is replaced by '***'.*/
		String key;
		if(BTPRO028Permission) {
			key = chdrlifIO.getBillchnl().toString().trim() + chdrlifIO.getCnttype().toString().trim() + chdrlifIO.getBillfreq().toString().trim();
			if(!readT6654(key)) {
				key = chdrlifIO.getBillchnl().toString().trim().concat(chdrlifIO.getCnttype().toString().trim()).concat("**");
				if(!readT6654(key)) {
					key = chdrlifIO.getBillchnl().toString().trim().concat("*****");
					if(!readT6654(key)) {
						syserrrec.statuz.set(f035);
						StringUtil stringVariable1 = new StringUtil();
						stringVariable1.addExpression(t6654);
						stringVariable1.addExpression(wsaaT6654Key2);
						stringVariable1.setStringInto(wsysSysparams);
						syserrrec.params.set(wsysSystemErrorParams);
						fatalError600();
					}
				}
			}
			datcon2rec.freqFactor.set(t6654rec.leadDays);
		}
		else {
			wsaaBillchnl2.set(chdrlifIO.getBillchnl());
			wsaaCnttype2.set(chdrlifIO.getCnttype());
			ArraySearch as1 = ArraySearch.getInstance(wsaaT6654Rec);
			as1.setIndices(wsaaT6654Ix);
			as1.addSearchKey(wsaaT6654Key, wsaaT6654Key2, true);
			if (as1.binarySearch()) {
				/*CONTINUE_STMT*/
			}
			else {
				wsaaCnttype2.set("***");
				ArraySearch as3 = ArraySearch.getInstance(wsaaT6654Rec);
				as3.setIndices(wsaaT6654Ix);
				as3.addSearchKey(wsaaT6654Key, wsaaT6654Key2, true);
				if (as3.binarySearch()) {
					/*CONTINUE_STMT*/
				}
				else {
					syserrrec.statuz.set(f035);
					StringUtil stringVariable1 = new StringUtil();
					stringVariable1.addExpression(t6654);
					stringVariable1.addExpression(wsaaT6654Key2);
					stringVariable1.setStringInto(wsysSysparams);
					syserrrec.params.set(wsysSystemErrorParams);
					fatalError600();
				}
			}
			datcon2rec.freqFactor.set(wsaaT6654Leadday[wsaaT6654Ix.toInt()]);
		}


		/* Call 'DATCON2' to increment the effective date by the T6654*/
		/* LEAD DAYS.*/
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError600();
		}
		wsaaEffdatePlusCntlead.set(datcon2rec.intDate2);
	}

protected void validateContract2700()
	{
		/*START*/
		/*  Validate the statii of the contract*/
		wsaaValidContract.set("N");
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub,12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Sub.toInt()],chdrlifIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub,12)); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.cnPremStat[wsaaT5679Sub.toInt()],chdrlifIO.getPstatcode())) {
						wsaaT5679Sub.set(13);
						wsaaValidContract.set("Y");
					}
				}
			}
		}
		/*EXIT*/
	}

protected void validateCoverage2800()
	{
		start2810();
	}

protected void start2810()
	{
		/* Check to see if coverage is of a valid status*/
		covrIO.setChdrcoy(fpcxpfRec.chdrcoy);
		covrIO.setChdrnum(fpcxpfRec.chdrnum);
		covrIO.setLife(fpcxpfRec.life);
		covrIO.setCoverage(fpcxpfRec.coverage);
		covrIO.setRider(fpcxpfRec.rider);
		covrIO.setPlanSuffix(fpcxpfRec.planSuffix);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		wsaaValidCoverage.set("N");
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub,12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()],covrIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub,12)); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()],covrIO.getPstatcode())) {
						wsaaT5679Sub.set(13);
						wsaaValidCoverage.set("Y");
					}
				}
			}
		}
	}

protected void writePtrn2900()
	{
		start2910();
	}

protected void start2910()
	{
		ptrnIO.setTranno(wsaaTranno);
		ptrnIO.setChdrcoy(fpcxpfRec.chdrcoy);
		ptrnIO.setChdrnum(fpcxpfRec.chdrnum);
		ptrnIO.setBatcpfx(batcdorrec.prefix);
		/* MOVE TARGTO                  TO PTRN-PTRNEFF.         <D9604>*/
		ptrnIO.setPtrneff(fpcxpfRec.annivProcDate);
		ptrnIO.setDatesub(bsscIO.getEffectiveDate());
		ptrnIO.setCrtuser(bsscIO.getUserName().toString());   //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
		contotrec.totno.set(ct04);
		contotrec.totval.set(1);
		callContot001();
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		softlockPolicy2400();
		/*                                                         <D9604>*/
		/* If all target has been billed and at least the target ha<D9604>*/
		/* been recd, deactivate the FPCO record. Otherwise, if all<D9604>*/
		/* the target has not been recd, write an entry to the repo<D9604>*/
		/*                                                         <D9604>*/
		if (isLT(fpcoIO.getPremRecPer(),fpcoIO.getTargetPremium())) {
			writeReport3100();
		}
		else {
			if (isGTE(fpcoIO.getBilledInPeriod(),fpcoIO.getTargetPremium())) {
				fpcoIO.setActiveInd("N");
			}
		}
		fpcoIO.setAnnProcessInd("Y");
		fpcoIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fpcoIO.getParams());
			fatalError600();
		}
		writeNewFpco3200();
		updateChdr3700();
		unlockPolicy2300();
	}

protected void writeReport3100()
	{
		start3110();
	}

protected void start3110()
	{
		if (newPageReq.isTrue()) {
			writeHeader3150();
		}
		wsaaTempDate.set(fpcoIO.getTargfrom());
		callDatcon13400();
		r5361d01RecordInner.rd01Currfrom.set(wsaaFormattedDate);
		wsaaTempDate.set(fpcoIO.getTargto());
		callDatcon13400();
		r5361d01RecordInner.rd01Currto.set(wsaaFormattedDate);
		r5361d01RecordInner.rd01Chdrnum.set(fpcoIO.getChdrnum());
		r5361d01RecordInner.rd01Life.set(fpcoIO.getLife());
		r5361d01RecordInner.rd01Coverage.set(fpcoIO.getCoverage());
		r5361d01RecordInner.rd01Plnsfx.set(fpcoIO.getPlanSuffix());
		r5361d01RecordInner.rd01Tgtprem.set(fpcoIO.getTargetPremium());
		r5361d01RecordInner.rd01Prmrecd.set(fpcoIO.getPremRecPer());
		r5361d01RecordInner.rd01Billamt.set(fpcoIO.getBilledInPeriod());
		r5361d01RecordInner.rd01Minreqd.set(fpcoIO.getOverdueMin());
		printerFile.printR5361d01(r5361d01RecordInner.r5361d01Record, indicArea);
		reportWritten.setTrue();
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
	}

protected void writeHeader3150()
	{
		start3151();
	}

protected void start3151()
	{
		/* Get company description for report header.*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		rh01Company.set(bsprIO.getCompany());
		rh01Companynm.set(descIO.getLongdesc());
		printerFile.printR5361h01(r5361h01Record, indicArea);
		wsaaOverflow.set("N");
	}

protected void writeNewFpco3200()
	{
		start3210();
	}

protected void start3210()
	{
		wsaaOldCurrto.set(fpcoIO.getTargto());
		/* Calculate TARGTO as old TARGTO + 1 Year                 <D9604>*/
		datcon2rec.frequency.set("01");
		datcon2rec.intDate1.set(fpcoIO.getTargto());
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		fpcoIO.setTargto(datcon2rec.intDate2);
		/* Calculate CBANPR as old CBANPR + 1 Year                         */
		datcon2rec.frequency.set("01");
		datcon2rec.intDate1.set(fpcoIO.getAnnivProcDate());
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		/* Set up the new FPCODRY details..                                */
		fpcoIO.setAnnivProcDate(datcon2rec.intDate2);
		fpcoIO.setTranno(wsaaTranno);
		fpcoIO.setTargfrom(wsaaOldCurrto);
		fpcoIO.setCurrfrom(bsscIO.getEffectiveDate());
		fpcoIO.setEffdate(bsscIO.getEffectiveDate());
		fpcoIO.setEffdate(fpcxpfRec.annivProcDate);
		fpcoIO.setCurrto(varcom.vrcmMaxDate);
		fpcoIO.setAnnProcessInd(SPACES);
		fpcoIO.setActiveInd("Y");
		fpcoIO.setPremRecPer(ZERO);
		fpcoIO.setOverdueMin(ZERO);
		fpcoIO.setBilledInPeriod(ZERO);
		findMinPercentage3300();
		fpcoIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fpcoIO.getParams());
			fatalError600();
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
	}

protected void findMinPercentage3300()
	{
		start3310();
	}

protected void start3310()
	{
		wsaaT5729Ix.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaT5729Ix, wsaaT5729ArrayInner.wsaaT5729Rec.length); wsaaT5729Ix.add(1)){
				if (isEQ(wsaaT5729ArrayInner.wsaaT5729Key[wsaaT5729Ix.toInt()], chdrlifIO.getCnttype())) {
					break searchlabel1;
				}
			}
			syserrrec.statuz.set(i086);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(t5729);
			stringVariable1.addExpression(wsysSystemErrorParams);
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();
		}
		/*  We must find the effective T5729 entry for the contract*/
		/*  (T5729 entries will have been loaded in descending sequence*/
		/*  into the array).*/
		wsaaDateFound.set("N");
		while ( !(isNE(chdrlifIO.getCnttype(), wsaaT5729ArrayInner.wsaaT5729Key[wsaaT5729Ix.toInt()])
		|| isGT(wsaaT5729Ix,wsaaT5729Size)
		|| dateFound.isTrue())) {
			if (isGTE(chdrlifIO.getOccdate(), wsaaT5729ArrayInner.wsaaT5729Itmfrm[wsaaT5729Ix.toInt()])) {
				wsaaDateFound.set("Y");
			}
			else {
				wsaaT5729Ix.add(1);
			}
		}

		if (dateNotFound.isTrue()) {
			syserrrec.statuz.set(i086);
			syserrrec.params.set(wsysSystemErrorParams);
			fatalError600();
		}
		/*  Load the values from table T5729 which match the frequency*/
		/*  on the CHDR record*/
		wsaaFreqFound.set("N");
		wsaaT5729Sub.set(1);
		for (wsaaT5729Sub.set(1); !(isGT(wsaaT5729Sub,6)
		|| freqFound.isTrue()); wsaaT5729Sub.add(1)){
			if (isEQ(wsaaT5729ArrayInner.wsaaT5729Frqcy[wsaaT5729Ix.toInt()][wsaaT5729Sub.toInt()], chdrlifIO.getBillfreq())) {
				wsaaTest.set(wsaaT5729Sub);
				wsaaFreqFound.set("Y");
			}
		}
		if (isGT(wsaaT5729Sub,6)) {
			syserrrec.statuz.set(i086);
			syserrrec.params.set(wsysSystemErrorParams);
			fatalError600();
		}
		if (isEQ(wsaaTest,1)){
			wsaaDurations.set(wsaaT5729ArrayInner.wsaaT5729Durationas[wsaaT5729Ix.toInt()]);
			wsaaOverdueMins.set(wsaaT5729ArrayInner.wsaaT5729OverdueMinas[wsaaT5729Ix.toInt()]);
		}
		else if (isEQ(wsaaTest,2)){
			wsaaDurations.set(wsaaT5729ArrayInner.wsaaT5729Durationbs[wsaaT5729Ix.toInt()]);
			wsaaOverdueMins.set(wsaaT5729ArrayInner.wsaaT5729OverdueMinbs[wsaaT5729Ix.toInt()]);
		}
		else if (isEQ(wsaaTest,3)){
			wsaaDurations.set(wsaaT5729ArrayInner.wsaaT5729Durationcs[wsaaT5729Ix.toInt()]);
			wsaaOverdueMins.set(wsaaT5729ArrayInner.wsaaT5729OverdueMincs[wsaaT5729Ix.toInt()]);
		}
		else if (isEQ(wsaaTest,4)){
			wsaaDurations.set(wsaaT5729ArrayInner.wsaaT5729Durationds[wsaaT5729Ix.toInt()]);
			wsaaOverdueMins.set(wsaaT5729ArrayInner.wsaaT5729OverdueMinds[wsaaT5729Ix.toInt()]);
		}
		else if (isEQ(wsaaTest,5)){
			wsaaDurations.set(wsaaT5729ArrayInner.wsaaT5729Durationes[wsaaT5729Ix.toInt()]);
			wsaaOverdueMins.set(wsaaT5729ArrayInner.wsaaT5729OverdueMines[wsaaT5729Ix.toInt()]);
		}
		else if (isEQ(wsaaTest,6)){
			wsaaDurations.set(wsaaT5729ArrayInner.wsaaT5729Durationfs[wsaaT5729Ix.toInt()]);
			wsaaOverdueMins.set(wsaaT5729ArrayInner.wsaaT5729OverdueMinfs[wsaaT5729Ix.toInt()]);
		}
		else{
			syserrrec.params.set(wsysSystemErrorParams);
			fatalError600();
		}
		/*  Determine the current minimum % from T5729 based on the*/
		/*  difference between the Occdate and the effective date of this*/
		/*  job.*/
		/*  Use the DATCON3 subroutine to calculate the freq factor*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrIO.getCrrcd());
		datcon3rec.intDate2.set(fpcoIO.getTargto());
		datcon3rec.frequency.set(chdrlifIO.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		wsaaNumPeriod.set(datcon3rec.freqFactor);
		wsaaSub2.set(0);
		durationNotFound.setTrue();
		for (wsaaSub.set(1); !(isGT(wsaaSub,4)
		|| isEQ(wsaaDuration[wsaaSub.toInt()],SPACES)
		|| durationFound.isTrue()); wsaaSub.add(1)){
			if (isLTE(wsaaNumPeriod,wsaaDuration[wsaaSub.toInt()])) {
				wsaaSub2.set(wsaaSub);
				durationFound.setTrue();
			}
		}
		if (durationNotFound.isTrue()) {
			syserrrec.statuz.set(i086);
			syserrrec.params.set(wsysSystemErrorParams);
			fatalError600();
		}
		fpcoIO.setMinOverduePer(wsaaOverdueMin[wsaaSub2.toInt()]);
	}

protected void callDatcon13400()
	{
		/*START*/
		wsaaFormattedDate.set(SPACES);
		datcon1rec.intDate.set(wsaaTempDate);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaFormattedDate.set(datcon1rec.extDate);
		/*EXIT*/
	}

protected void updateChdr3700()
	{
		/*START*/
		/*    ADD 1                       TO CHDRLIF-TRANNO                */
		chdrlifIO.setTranno(wsaaTranno);
		chdrlifIO.setFunction(varcom.rewrt);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*START*/
		fpcxpf.close();
		printerFile.close();
		wsaaQcmdexc.set("DLTOVR FILE(FPCXPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-T5729-ARRAY--INNER
 */
private static final class WsaaT5729ArrayInner {

		/* WSAA-T5729-ARRAY */
	private FixedLengthStringData[] wsaaT5729Rec = FLSInittedArray (60, 380);
	private FixedLengthStringData[] wsaaT5729Key = FLSDArrayPartOfArrayStructure(3, wsaaT5729Rec, 0);
	private FixedLengthStringData[] wsaaT5729Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5729Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5729Data = FLSDArrayPartOfArrayStructure(377, wsaaT5729Rec, 3);
	private PackedDecimalData[] wsaaT5729Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5729Data, 0);
	private FixedLengthStringData[] wsaaT5729Frqcys = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 5);
	private FixedLengthStringData[][] wsaaT5729Frqcy = FLSDArrayPartOfArrayStructure(6, 2, wsaaT5729Frqcys, 0);
	private FixedLengthStringData[] wsaaT5729Durationas = FLSDArrayPartOfArrayStructure(16, wsaaT5729Data, 17);
	private ZonedDecimalData[][] wsaaT5729Durationa = ZDArrayPartOfArrayStructure(4, 4, 0, wsaaT5729Durationas, 0);
	private FixedLengthStringData[] wsaaT5729Durationbs = FLSDArrayPartOfArrayStructure(16, wsaaT5729Data, 33);
	private ZonedDecimalData[][] wsaaT5729Durationb = ZDArrayPartOfArrayStructure(4, 4, 0, wsaaT5729Durationbs, 0);
	private FixedLengthStringData[] wsaaT5729Durationcs = FLSDArrayPartOfArrayStructure(16, wsaaT5729Data, 49);
	private ZonedDecimalData[][] wsaaT5729Durationc = ZDArrayPartOfArrayStructure(4, 4, 0, wsaaT5729Durationcs, 0);
	private FixedLengthStringData[] wsaaT5729Durationds = FLSDArrayPartOfArrayStructure(16, wsaaT5729Data, 65);
	private ZonedDecimalData[][] wsaaT5729Durationd = ZDArrayPartOfArrayStructure(4, 4, 0, wsaaT5729Durationds, 0);
	private FixedLengthStringData[] wsaaT5729Durationes = FLSDArrayPartOfArrayStructure(16, wsaaT5729Data, 81);
	private ZonedDecimalData[][] wsaaT5729Duratione = ZDArrayPartOfArrayStructure(4, 4, 0, wsaaT5729Durationes, 0);
	private FixedLengthStringData[] wsaaT5729Durationfs = FLSDArrayPartOfArrayStructure(16, wsaaT5729Data, 97);
	private ZonedDecimalData[][] wsaaT5729Durationf = ZDArrayPartOfArrayStructure(4, 4, 0, wsaaT5729Durationfs, 0);
	private FixedLengthStringData[] wsaaT5729TargetMinas = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 113);
	private ZonedDecimalData[][] wsaaT5729TargetMina = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729TargetMinas, 0);
	private FixedLengthStringData[] wsaaT5729TargetMinbs = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 125);
	private ZonedDecimalData[][] wsaaT5729TargetMinb = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729TargetMinbs, 0);
	private FixedLengthStringData[] wsaaT5729TargetMincs = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 137);
	private ZonedDecimalData[][] wsaaT5729TargetMinc = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729TargetMincs, 0);
	private FixedLengthStringData[] wsaaT5729TargetMinds = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 149);
	private ZonedDecimalData[][] wsaaT5729TargetMind = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729TargetMinds, 0);
	private FixedLengthStringData[] wsaaT5729TargetMines = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 161);
	private ZonedDecimalData[][] wsaaT5729TargetMine = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729TargetMines, 0);
	private FixedLengthStringData[] wsaaT5729TargetMinfs = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 173);
	private ZonedDecimalData[][] wsaaT5729TargetMinf = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729TargetMinfs, 0);
	private FixedLengthStringData[] wsaaT5729TargetMaxas = FLSDArrayPartOfArrayStructure(20, wsaaT5729Data, 185);
	private ZonedDecimalData[][] wsaaT5729TargetMaxa = ZDArrayPartOfArrayStructure(4, 5, 0, wsaaT5729TargetMaxas, 0);
	private FixedLengthStringData[] wsaaT5729TargetMaxbs = FLSDArrayPartOfArrayStructure(20, wsaaT5729Data, 205);
	private ZonedDecimalData[][] wsaaT5729TargetMaxb = ZDArrayPartOfArrayStructure(4, 5, 0, wsaaT5729TargetMaxbs, 0);
	private FixedLengthStringData[] wsaaT5729TargetMaxcs = FLSDArrayPartOfArrayStructure(20, wsaaT5729Data, 225);
	private ZonedDecimalData[][] wsaaT5729TargetMaxc = ZDArrayPartOfArrayStructure(4, 5, 0, wsaaT5729TargetMaxcs, 0);
	private FixedLengthStringData[] wsaaT5729TargetMaxds = FLSDArrayPartOfArrayStructure(20, wsaaT5729Data, 245);
	private ZonedDecimalData[][] wsaaT5729TargetMaxd = ZDArrayPartOfArrayStructure(4, 5, 0, wsaaT5729TargetMaxds, 0);
	private FixedLengthStringData[] wsaaT5729TargetMaxes = FLSDArrayPartOfArrayStructure(20, wsaaT5729Data, 265);
	private ZonedDecimalData[][] wsaaT5729TargetMaxe = ZDArrayPartOfArrayStructure(4, 5, 0, wsaaT5729TargetMaxes, 0);
	private FixedLengthStringData[] wsaaT5729TargetMaxfs = FLSDArrayPartOfArrayStructure(20, wsaaT5729Data, 285);
	private ZonedDecimalData[][] wsaaT5729TargetMaxf = ZDArrayPartOfArrayStructure(4, 5, 0, wsaaT5729TargetMaxfs, 0);
	private FixedLengthStringData[] wsaaT5729OverdueMinas = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 305);
	private ZonedDecimalData[][] wsaaT5729OverdueMina = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729OverdueMinas, 0);
	private FixedLengthStringData[] wsaaT5729OverdueMinbs = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 317);
	private ZonedDecimalData[][] wsaaT5729OverdueMinb = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729OverdueMinbs, 0);
	private FixedLengthStringData[] wsaaT5729OverdueMincs = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 329);
	private ZonedDecimalData[][] wsaaT5729OverdueMinc = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729OverdueMincs, 0);
	private FixedLengthStringData[] wsaaT5729OverdueMinds = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 341);
	private ZonedDecimalData[][] wsaaT5729OverdueMind = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729OverdueMinds, 0);
	private FixedLengthStringData[] wsaaT5729OverdueMines = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 353);
	private ZonedDecimalData[][] wsaaT5729OverdueMine = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729OverdueMines, 0);
	private FixedLengthStringData[] wsaaT5729OverdueMinfs = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 365);
	private ZonedDecimalData[][] wsaaT5729OverdueMinf = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729OverdueMinfs, 0);
}
/*
 * Class transformed  from Data Structure R5361D01-RECORD--INNER
 */
private static final class R5361d01RecordInner {

	private FixedLengthStringData r5361d01Record = new FixedLengthStringData(104);
	private FixedLengthStringData r5361d01O = new FixedLengthStringData(104).isAPartOf(r5361d01Record, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(r5361d01O, 0);
	private FixedLengthStringData rd01Life = new FixedLengthStringData(2).isAPartOf(r5361d01O, 8);
	private FixedLengthStringData rd01Coverage = new FixedLengthStringData(2).isAPartOf(r5361d01O, 10);
	private ZonedDecimalData rd01Plnsfx = new ZonedDecimalData(4, 0).isAPartOf(r5361d01O, 12);
	private FixedLengthStringData rd01Currfrom = new FixedLengthStringData(10).isAPartOf(r5361d01O, 16);
	private FixedLengthStringData rd01Currto = new FixedLengthStringData(10).isAPartOf(r5361d01O, 26);
	private ZonedDecimalData rd01Tgtprem = new ZonedDecimalData(17, 2).isAPartOf(r5361d01O, 36);
	private ZonedDecimalData rd01Prmrecd = new ZonedDecimalData(17, 2).isAPartOf(r5361d01O, 53);
	private ZonedDecimalData rd01Billamt = new ZonedDecimalData(17, 2).isAPartOf(r5361d01O, 70);
	private ZonedDecimalData rd01Minreqd = new ZonedDecimalData(17, 2).isAPartOf(r5361d01O, 87);
}
}
