/*
 * File: B5358.java
 * Date: 29 August 2009 21:10:27
 * Author: Quipoz Limited
 * 
 * Class transformed from B5358.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsu.general.dataaccess.AcagrnlTableDAM;
import com.csc.fsu.general.recordstructures.Fileprcrec;
import com.csc.life.flexiblepremium.dataaccess.FprmpfTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FprxpfTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* This program is the Premium Mop-Up Splitter program for
* Flexible Premium Contracts to be run before the Premium Mop-
* Up batch job in multi-thread.
*
* The FPRMPF will be read via SQL with the following criteria:-
* i   Validflag    = '1'
* ii  Currfrom    <= BSSC-EFFECTIVE-DATE
* iii Currto      >= BSSC-EFFECTIVE-DATE
*
* Control totals used in this program:
*
*    01  -  No. of FPRM extracted records
*    02  -  No. of thread members
*
* Splitter programs should be simple. They should only deal with
* a single file. If there is insufficient data on the primary file
* to completely identify a transaction then the further editing
* should be done in the subsequent process. The objective of a
* Splitter is to rapidly isolate potential transactions, not
* to process the transaction.   The primary concern for Splitter
* program design is to minimise elapsed time and this is achieved
* by minimising disk IO.
*
* Before the Splitter process is invoked, the program CRTTMPF
* should be run.  CRTTMPF (Create Temporary File) will create
* a duplicate of a physical file (created under Smart and defined
* as a Query file) which will have as many members as are defined
* in that process's 'subsequent threads' field. The temporary file
* will have the name XXXXDD9999 where XXXX is the first four
* characters of the file which was duplicated, DD is the first two
* characters of the 4th system parameter and 9999 is the schedule
* run number. Each member added to the temporary file will be
* called THREAD999 where 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read records from a primary file
* and, for each record selected,  it will write a record to a
* temporary file member. Exactly which temporary file member
* is written to is decided by a working storage count. The
* objective of the count is to spread the transaction records
* evenly amongst each thread member.
*
* The Splitter program should always have a restart method of
* '1' indicating a re-run. In the event of a restart, the Splitter
* program will always start with empty file members. Apart from
* writing records to the temporary file members, Splitter programs
* should not be doing any further updates.
*
*
* Notes for programs which use Splitter Program Temporary Files.
* --------------------------------------------------------------
*
* The Multi Thread Batch Environment will be able to submit multip e
* versions of the program which use the file members created from
* this program. It is important that the process which runs the
* splitter program has the same 'number of subsequent threads' as
* the following process has defined in 'number of threads this
* process'.
*
* The subsequent program need only point itself to one member
* based around the field BSPR-SCHEDULE-THREAD-NO which indicates
* in which thread the copy of the subsequent program is running.
*
****************************************************************** ****
* </pre>
*/
public class B5358 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlfprmCursorrs = null;
	private java.sql.PreparedStatement sqlfprmCursorps = null;
	private java.sql.Connection sqlfprmCursorconn = null;
	private String sqlfprmCursor = "";
	private int fprmCursorLoopIndex = 0;
	private FprxpfTableDAM fprxpf = new FprxpfTableDAM();
	private DiskFileDAM fprx01 = new DiskFileDAM("FPRX01");
	private DiskFileDAM fprx02 = new DiskFileDAM("FPRX02");
	private DiskFileDAM fprx03 = new DiskFileDAM("FPRX03");
	private DiskFileDAM fprx04 = new DiskFileDAM("FPRX04");
	private DiskFileDAM fprx05 = new DiskFileDAM("FPRX05");
	private DiskFileDAM fprx06 = new DiskFileDAM("FPRX06");
	private DiskFileDAM fprx07 = new DiskFileDAM("FPRX07");
	private DiskFileDAM fprx08 = new DiskFileDAM("FPRX08");
	private DiskFileDAM fprx09 = new DiskFileDAM("FPRX09");
	private DiskFileDAM fprx10 = new DiskFileDAM("FPRX10");
	private DiskFileDAM fprx11 = new DiskFileDAM("FPRX11");
	private DiskFileDAM fprx12 = new DiskFileDAM("FPRX12");
	private DiskFileDAM fprx13 = new DiskFileDAM("FPRX13");
	private DiskFileDAM fprx14 = new DiskFileDAM("FPRX14");
	private DiskFileDAM fprx15 = new DiskFileDAM("FPRX15");
	private DiskFileDAM fprx16 = new DiskFileDAM("FPRX16");
	private DiskFileDAM fprx17 = new DiskFileDAM("FPRX17");
	private DiskFileDAM fprx18 = new DiskFileDAM("FPRX18");
	private DiskFileDAM fprx19 = new DiskFileDAM("FPRX19");
	private DiskFileDAM fprx20 = new DiskFileDAM("FPRX20");
	private FprxpfTableDAM fprxpfData = new FprxpfTableDAM();
	private FixedLengthStringData fprx01Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx02Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx03Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx04Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx05Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx06Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx07Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx08Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx09Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx10Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx11Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx12Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx13Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx14Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx15Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx16Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx17Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx18Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx19Rec = new FixedLengthStringData(79);
	private FixedLengthStringData fprx20Rec = new FixedLengthStringData(79);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5358");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

		/*  FPRX member parametres.*/
	private FixedLengthStringData wsaaFprxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaFprxFn, 0, FILLER).init("FPRX");
	private FixedLengthStringData wsaaFprxRunid = new FixedLengthStringData(2).isAPartOf(wsaaFprxFn, 4);
	private ZonedDecimalData wsaaFprxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaFprxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaa1 = new FixedLengthStringData(1).init("1");

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");
	private FixedLengthStringData wsaaClrtmpfError = new FixedLengthStringData(97);
		/*  Pointers to point to the member to read (IY) and write (IZ).*/
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();

		/*  SQL error message formatting.*/
	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
		/*  Define the number of rows in the block to be fetched.*/
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaFprmData = FLSInittedArray (1000, 41);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaFprmData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaFprmData, 1);
	private PackedDecimalData[] wsaaPayrseqno = PDArrayPartOfArrayStructure(1, 0, wsaaFprmData, 9);
	private PackedDecimalData[] wsaaTotrecd = PDArrayPartOfArrayStructure(18, 2, wsaaFprmData, 10);
	private PackedDecimalData[] wsaaTotbill = PDArrayPartOfArrayStructure(18, 2, wsaaFprmData, 20);
	private PackedDecimalData[] wsaaCurrfrom = PDArrayPartOfArrayStructure(8, 0, wsaaFprmData, 30, 0.0);
	private PackedDecimalData[] wsaaCurrto = PDArrayPartOfArrayStructure(8, 0, wsaaFprmData, 35, 0.0);
	private FixedLengthStringData[] wsaaValidflag = FLSDArrayPartOfArrayStructure(1, wsaaFprmData, 40);

		/* WSAA-IND-ARRAY */
	private FixedLengthStringData[] wsaaFprmInd = FLSInittedArray (1000, 12);
	private BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(6, 4, 0, wsaaFprmInd, 0);
		/* WSAA-HOST-VARIABLES */
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).init(0);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
	private AcagrnlTableDAM acagrnlIO = new AcagrnlTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FprmpfTableDAM fprmpfData = new FprmpfTableDAM();
	private P6671par p6671par = new P6671par();
	private Fileprcrec fileprcrec = new Fileprcrec();

	public B5358() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** This section is invoked in restart mode. Note section 1100 must*/
		/** clear each temporary file member as the members are not under*/
		/** commitment control. Note also that Splitter programs must have*/
		/** a Restart Method of 1 (re-run). Thus no updating should occur*/
		/** which would result in different records being returned from the*/
		/** primary file in a restart!*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/*    Check the restart method is compatible with the program.*/
		/*    This program is restartable but will always re-run from*/
		/*    the beginning. This is because the temporary file members*/
		/*    are not under commitment control.*/
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*    Do a ovrdbf for each temporary file member. (Note we have*/
		/*    allowed for a maximum of 20.)*/
		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		wsaaFprxRunid.set(bprdIO.getSystemParam04());
		wsaaFprxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		wsaaCompany.set(bsprIO.getCompany());
		/*    Since this program runs from a parameter screen move the*/
		/*    internal parameter area to the original parameter copybook*/
		/*    to retrieve the contract range*/
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES)
		&& isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		/*    Log the number of threads being used*/
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		/*    Prepare the host effective date from the parameter screen*/
		wsaaEffdate.set(bsscIO.getEffectiveDate());
		wsaaCompany.set(bsprIO.getCompany());
		iy.set(1);
		sqlfprmCursor = " SELECT  CHDRCOY, CHDRNUM, PAYRSEQNO, TOTRECD, TOTBILL, CURRFROM, CURRTO, VALIDFLAG" +
" FROM   " + getAppVars().getTableNameOverriden("FPRMPF") + " " +
" WHERE VALIDFLAG = ?" +
" AND CHDRCOY = ?" +
" AND CURRFROM <= ?" +
" AND CURRTO >= ?" +
" AND CHDRNUM >= ?" +
" AND CHDRNUM <= ?" +
" ORDER BY CHDRNUM, PAYRSEQNO";
		/*    Initialise the array which will hold all the records*/
		/*    returned from each fetch. (INITIALIZE will not work as the*/
		/*    the array is indexed.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		sqlerrorflag = false;
		try {
			sqlfprmCursorconn = getAppVars().getDBConnectionForTable(new com.csc.life.flexiblepremium.dataaccess.FprmpfTableDAM());
			sqlfprmCursorps = getAppVars().prepareStatementEmbeded(sqlfprmCursorconn, sqlfprmCursor, "FPRMPF");
			getAppVars().setDBString(sqlfprmCursorps, 1, wsaa1);
			getAppVars().setDBString(sqlfprmCursorps, 2, wsaaCompany);
			getAppVars().setDBNumber(sqlfprmCursorps, 3, wsaaEffdate);
			getAppVars().setDBNumber(sqlfprmCursorps, 4, wsaaEffdate);
			getAppVars().setDBString(sqlfprmCursorps, 5, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlfprmCursorps, 6, wsaaChdrnumTo);
			sqlfprmCursorrs = getAppVars().executeQuery(sqlfprmCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/* Where parameter 3 on the schedule definition is not          */
		/* spaces, clear the ACAG file.                                 */
		if (isNE(bprdIO.getSystemParam03(), SPACES)) {
			clearAcag1200();
		}
	}

protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

protected void openThreadMember1110()
	{
		/*    The next thing we must do is construct the name of the*/
		/*    temporary file which we will be working with.*/
		/* MOVE IZ                     TO WSAA-THREAD-NUMBER.           */
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(), 1), iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaFprxFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz, varcom.oK)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression("CLRTMPF", SPACES);
			stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
			stringVariable1.addExpression(SPACES);
			stringVariable1.addExpression(wsaaFprxFn);
			stringVariable1.addExpression(SPACES);
			stringVariable1.addExpression(wsaaThreadMember);
			stringVariable1.setStringInto(wsaaClrtmpfError);
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.params.set(wsaaClrtmpfError);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression("OVRDBF FILE(FPRX");
		stringVariable2.addExpression(iz);
		stringVariable2.addExpression(") TOFILE(");
		stringVariable2.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable2.addExpression("/");
		stringVariable2.addExpression(wsaaFprxFn);
		stringVariable2.addExpression(") MBR(");
		stringVariable2.addExpression(wsaaThreadMember);
		stringVariable2.addExpression(")");
		stringVariable2.addExpression(" SEQONLY(*YES 1000)");
		stringVariable2.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*    Open the file.*/
		if (isEQ(iz, 1)) {
			fprx01.openOutput();
		}
		if (isEQ(iz, 2)) {
			fprx02.openOutput();
		}
		if (isEQ(iz, 3)) {
			fprx03.openOutput();
		}
		if (isEQ(iz, 4)) {
			fprx04.openOutput();
		}
		if (isEQ(iz, 5)) {
			fprx05.openOutput();
		}
		if (isEQ(iz, 6)) {
			fprx06.openOutput();
		}
		if (isEQ(iz, 7)) {
			fprx07.openOutput();
		}
		if (isEQ(iz, 8)) {
			fprx08.openOutput();
		}
		if (isEQ(iz, 9)) {
			fprx09.openOutput();
		}
		if (isEQ(iz, 10)) {
			fprx10.openOutput();
		}
		if (isEQ(iz, 11)) {
			fprx11.openOutput();
		}
		if (isEQ(iz, 12)) {
			fprx12.openOutput();
		}
		if (isEQ(iz, 13)) {
			fprx13.openOutput();
		}
		if (isEQ(iz, 14)) {
			fprx14.openOutput();
		}
		if (isEQ(iz,15)) {
			fprx15.openOutput();
		}
		if (isEQ(iz, 16)) {
			fprx16.openOutput();
		}
		if (isEQ(iz, 17)) {
			fprx17.openOutput();
		}
		if (isEQ(iz, 18)) {
			fprx18.openOutput();
		}
		if (isEQ(iz, 19)) {
			fprx19.openOutput();
		}
		if (isEQ(iz, 20)) {
			fprx20.openOutput();
		}
	}

protected void clearAcag1200()
	{
		/*START*/
		/* Check if multi-thread processing.  If it is, clear down      */
		/* the ACAGPF file                                              */
		if (isNE(bprdIO.getSystemParam03(), SPACES)) {
			fileprcrec.function.set("CLRF");
			fileprcrec.file1.set("ACAGPF");
			noSource("FILEPROC", fileprcrec.params);
			if (isNE(fileprcrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(fileprcrec.statuz);
				syserrrec.params.set(fileprcrec.params);
				fatalError600();
			}
		}
		/*EXIT*/
	}

protected void initialiseArray1500()
	{
		/*START*/
		wsaaCurrfrom[wsaaInd.toInt()].set(ZERO);
		wsaaCurrto[wsaaInd.toInt()].set(ZERO);
		wsaaTotbill[wsaaInd.toInt()].set(ZERO);
		wsaaTotrecd[wsaaInd.toInt()].set(ZERO);
		wsaaPayrseqno[wsaaInd.toInt()].set(ZERO);
		/*                   WSAA-VALIDFLAG (WSAA-IND).                 */
		wsaaChdrcoy[wsaaInd.toInt()].set(SPACES);
		wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaValidflag[wsaaInd.toInt()].set(SPACES);
		/*EXIT*/
	}

protected void readFile2000()
	{
		/*READ-FILES*/
		/*  We only need to FETCH records when a full block has been*/
		/*  processed or if the end of file was retrieved in the last*/
		/*  block.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd, 1)) {
			return ;
		}
		sqlerrorflag = false;
		try {
			for (fprmCursorLoopIndex = 1; isLT(fprmCursorLoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlfprmCursorrs); fprmCursorLoopIndex++ ){
				getAppVars().getDBObject(sqlfprmCursorrs, 1, wsaaChdrcoy[fprmCursorLoopIndex], wsaaNullInd[fprmCursorLoopIndex][1]);
				getAppVars().getDBObject(sqlfprmCursorrs, 2, wsaaChdrnum[fprmCursorLoopIndex], wsaaNullInd[fprmCursorLoopIndex][2]);
				getAppVars().getDBObject(sqlfprmCursorrs, 3, wsaaPayrseqno[fprmCursorLoopIndex], wsaaNullInd[fprmCursorLoopIndex][3]);
				getAppVars().getDBObject(sqlfprmCursorrs, 4, wsaaTotrecd[fprmCursorLoopIndex], wsaaNullInd[fprmCursorLoopIndex][4]);
				getAppVars().getDBObject(sqlfprmCursorrs, 5, wsaaTotbill[fprmCursorLoopIndex], wsaaNullInd[fprmCursorLoopIndex][5]);
				getAppVars().getDBObject(sqlfprmCursorrs, 6, wsaaCurrfrom[fprmCursorLoopIndex], wsaaNullInd[fprmCursorLoopIndex][6]);
				getAppVars().getDBObject(sqlfprmCursorrs, 7, wsaaCurrto[fprmCursorLoopIndex]);
				getAppVars().getDBObject(sqlfprmCursorrs, 8, wsaaValidflag[fprmCursorLoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*  An SQLCODE = +100 is returned when :-*/
		/*      a) no rows are returned on the first fetch*/
		/*      b) the last row of the cursor is either in the block or is*/
		/*         the last row of the block.*/
		/*  We must continue processing to the 3000- section for case b)*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(varcom.endp);
		}
		//tom chi modified, bug 1030
		else if(isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)){
			wsspEdterror.set(varcom.endp);
		}
		//end
		/*EXIT*/
	}

protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3200();
		}
		
		/*  Re-initialise the block for next fetch and point to first row.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3200()
	{
		start3200();
	}

protected void start3200()
	{
		fprxpfData.chdrcoy.set(wsaaChdrcoy[wsaaInd.toInt()]);
		fprxpfData.chdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
		fprxpfData.payrseqno.set(wsaaPayrseqno[wsaaInd.toInt()]);
		fprxpfData.totalRecd.set(wsaaTotrecd[wsaaInd.toInt()]);
		fprxpfData.totalBilled.set(wsaaTotbill[wsaaInd.toInt()]);
		fprxpfData.currfrom.set(wsaaCurrfrom[wsaaInd.toInt()]);
		fprxpfData.currto.set(wsaaCurrto[wsaaInd.toInt()]);
		fprxpfData.validflag.set(wsaaValidflag[wsaaInd.toInt()]);
		if (isGT(iy, bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		if (isEQ(iy, 1)) {
			fprx01.write(fprxpfData);
		}
		if (isEQ(iy, 2)) {
			fprx02.write(fprxpfData);
		}
		if (isEQ(iy, 3)) {
			fprx03.write(fprxpfData);
		}
		if (isEQ(iy, 4)) {
			fprx04.write(fprxpfData);
		}
		if (isEQ(iy, 5)) {
			fprx05.write(fprxpfData);
		}
		if (isEQ(iy, 6)) {
			fprx06.write(fprxpfData);
		}
		if (isEQ(iy, 7)) {
			fprx07.write(fprxpfData);
		}
		if (isEQ(iy, 8)) {
			fprx08.write(fprxpfData);
		}
		if (isEQ(iy, 9)) {
			fprx09.write(fprxpfData);
		}
		if (isEQ(iy, 10)) {
			fprx10.write(fprxpfData);
		}
		if (isEQ(iy, 11)) {
			fprx11.write(fprxpfData);
		}
		if (isEQ(iy, 12)) {
			fprx12.write(fprxpfData);
		}
		if (isEQ(iy, 13)) {
			fprx13.write(fprxpfData);
		}
		if (isEQ(iy, 14)) {
			fprx14.write(fprxpfData);
		}
		if (isEQ(iy, 15)) {
			fprx15.write(fprxpfData);
		}
		if (isEQ(iy, 16)) {
			fprx16.write(fprxpfData);
		}
		if (isEQ(iy, 17)) {
			fprx17.write(fprxpfData);
		}
		if (isEQ(iy, 18)) {
			fprx18.write(fprxpfData);
		}
		if (isEQ(iy, 19)) {
			fprx19.write(fprxpfData);
		}
		if (isEQ(iy, 20)) {
			fprx20.write(fprxpfData);
		}
		/*    Log the number of extracted records.*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
		/*  Set up the array for the next block of records.*/
		/*  Otherwise process the next record in the array.*/
		wsaaInd.add(1);
		/*  Check for an incomplete block retrieved.*/		
		if ( isLTE(wsaaInd,wsaaRowsInBlock) && isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)) {
			wsaaEofInBlock.set("Y");
		}
		iy.add(1);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/**    There is no pre-commit processing to be done*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close the open files and remove the overrides.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlfprmCursorconn, sqlfprmCursorps, sqlfprmCursorrs);
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		close4110();
	}

protected void close4110()
	{
		if (isEQ(iz, 1)) {
			fprx01.close();
		}
		if (isEQ(iz, 2)) {
			fprx02.close();
		}
		if (isEQ(iz, 3)) {
			fprx03.close();
		}
		if (isEQ(iz, 4)) {
			fprx04.close();
		}
		if (isEQ(iz, 5)) {
			fprx05.close();
		}
		if (isEQ(iz, 6)) {
			fprx06.close();
		}
		if (isEQ(iz, 7)) {
			fprx07.close();
		}
		if (isEQ(iz, 8)) {
			fprx08.close();
		}
		if (isEQ(iz, 9)) {
			fprx09.close();
		}
		if (isEQ(iz, 10)) {
			fprx10.close();
		}
		if (isEQ(iz, 11)) {
			fprx11.close();
		}
		if (isEQ(iz, 12)) {
			fprx12.close();
		}
		if (isEQ(iz, 13)) {
			fprx13.close();
		}
		if (isEQ(iz, 14)) {
			fprx14.close();
		}
		if (isEQ(iz, 15)) {
			fprx15.close();
		}
		if (isEQ(iz, 16)) {
			fprx16.close();
		}
		if (isEQ(iz, 17)) {
			fprx17.close();
		}
		if (isEQ(iz, 18)) {
			fprx18.close();
		}
		if (isEQ(iz, 19)) {
			fprx19.close();
		}
		if (isEQ(iz, 20)) {
			fprx20.close();
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
