package com.csc.life.flexiblepremium.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:41
 * Description:
 * Copybook name: FPCOKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Fpcokey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData fpcoFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData fpcoKey = new FixedLengthStringData(64).isAPartOf(fpcoFileKey, 0, REDEFINE);
  	public FixedLengthStringData fpcoChdrcoy = new FixedLengthStringData(1).isAPartOf(fpcoKey, 0);
  	public FixedLengthStringData fpcoChdrnum = new FixedLengthStringData(8).isAPartOf(fpcoKey, 1);
  	public FixedLengthStringData fpcoLife = new FixedLengthStringData(2).isAPartOf(fpcoKey, 9);
  	public FixedLengthStringData fpcoCoverage = new FixedLengthStringData(2).isAPartOf(fpcoKey, 11);
  	public FixedLengthStringData fpcoRider = new FixedLengthStringData(2).isAPartOf(fpcoKey, 13);
  	public PackedDecimalData fpcoPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(fpcoKey, 15);
  	public PackedDecimalData fpcoTargfrom = new PackedDecimalData(8, 0).isAPartOf(fpcoKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(41).isAPartOf(fpcoKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(fpcoFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		fpcoFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}