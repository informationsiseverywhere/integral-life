package com.csc.life.flexiblepremium.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.flexiblepremium.dataaccess.dao.FprmpfDAO;
import com.csc.life.flexiblepremium.dataaccess.model.Fprmpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class FprmpfDAOImpl extends BaseDAOImpl<Fprmpf>implements FprmpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(FprmpfDAOImpl.class);	
	
    public Map<String, List<Fprmpf>> getFprmMap(String coy, List<String> chdrnumList) {
        StringBuilder sb = new StringBuilder("");
        sb.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, PAYRSEQNO, VALIDFLAG, CURRFROM, CURRTO, TRANNO, TOTRECD, TOTBILL, MINREQD, USRPRF, JOBNM, DATIME  ");
        sb.append("FROM VM1DTA.FPRMPF WHERE VALIDFLAG = '1' AND CHDRCOY=? AND ");
        sb.append(getSqlInStr("CHDRNUM", chdrnumList));
        sb.append(" ORDER BY CHDRCOY, CHDRNUM, PAYRSEQNO, UNIQUE_NUMBER DESC ");        
        LOGGER.info(sb.toString());	
        PreparedStatement ps = getPrepareStatement(sb.toString());
        ResultSet rs = null;
        Map<String, List<Fprmpf>> fprmMap = new HashMap<String, List<Fprmpf>>();
        try {
            ps.setInt(1, Integer.parseInt(coy));
            rs = executeQuery(ps);

            while (rs.next()) {
            	Fprmpf fprmpf = new Fprmpf();
                fprmpf.setUnique_number(rs.getLong("UNIQUE_NUMBER"));
                fprmpf.setChdrcoy(rs.getString("CHDRCOY"));
                fprmpf.setChdrnum(rs.getString("CHDRNUM"));
                fprmpf.setPayrseqno(rs.getInt("PAYRSEQNO"));
                fprmpf.setValidflag(rs.getString("VALIDFLAG"));
                fprmpf.setCurrfrom(rs.getInt("CURRFROM"));
                fprmpf.setCurrto(rs.getInt("CURRTO"));
                fprmpf.setTranno(rs.getInt("TRANNO"));
                fprmpf.setTotrecd(rs.getBigDecimal("TOTRECD"));
                fprmpf.setTotbill(rs.getBigDecimal("TOTBILL"));
                fprmpf.setMinreqd(rs.getBigDecimal("MINREQD"));
                fprmpf.setUsrprf(rs.getString("USRPRF"));
                fprmpf.setJobnm(rs.getString("JOBNM"));                
                fprmpf.setDatime(rs.getDate("DATIME"));                 

                if (fprmMap.containsKey(fprmpf.getChdrnum())) {
                	fprmMap.get(fprmpf.getChdrnum()).add(fprmpf);
                } else {
                    List<Fprmpf> fprmList = new ArrayList<Fprmpf>();
                    fprmList.add(fprmpf);
                    fprmMap.put(fprmpf.getChdrnum(), fprmList);
                }
            }

        } catch (SQLException e) {
            LOGGER.error("getFprmMap()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return fprmMap;

    }	
	
	public boolean updateFprmPF(List<Fprmpf> fprmList){
		StringBuilder sb = new StringBuilder(" UPDATE FPRMPF SET TOTBILL= ?, MINREQD=?, JOBNM=?,USRPRF=?,DATIME=? ");
		sb.append("WHERE UNIQUE_NUMBER=?");     
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean isUpdated = false;
		try{
			ps = getPrepareStatement(sb.toString());
			int ctr;
			for (Fprmpf fprm : fprmList) {		
				ctr=0;
				ps.setBigDecimal(++ctr, fprm.getTotbill());
				ps.setBigDecimal(++ctr, fprm.getMinreqd());
				ps.setString(++ctr, getJobnm());
                ps.setString(++ctr, getUsrprf());
                ps.setTimestamp(++ctr, new Timestamp(System.currentTimeMillis()));	
			    ps.setLong(++ctr, fprm.getUnique_number());
			    ps.addBatch();	
			}			
			ps.executeBatch();
			isUpdated = true;
		}catch (SQLException e) {
			LOGGER.error("updateFprmPF()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
		return isUpdated;
	}
}
