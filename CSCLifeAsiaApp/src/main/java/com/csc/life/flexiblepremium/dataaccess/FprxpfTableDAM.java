package com.csc.life.flexiblepremium.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: FprxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:55
 * Class transformed from FPRXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class FprxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 41;
	public FixedLengthStringData fprxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData fprxpfRecord = fprxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(fprxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(fprxrec);
	public PackedDecimalData payrseqno = DD.payrseqno.copy().isAPartOf(fprxrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(fprxrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(fprxrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(fprxrec);
	public PackedDecimalData totalRecd = DD.totrecd.copy().isAPartOf(fprxrec);
	public PackedDecimalData totalBilled = DD.totbill.copy().isAPartOf(fprxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public FprxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for FprxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public FprxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for FprxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public FprxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for FprxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public FprxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("FPRXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"PAYRSEQNO, " +
							"CURRFROM, " +
							"CURRTO, " +
							"VALIDFLAG, " +
							"TOTRECD, " +
							"TOTBILL, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     payrseqno,
                                     currfrom,
                                     currto,
                                     validflag,
                                     totalRecd,
                                     totalBilled,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		payrseqno.clear();
  		currfrom.clear();
  		currto.clear();
  		validflag.clear();
  		totalRecd.clear();
  		totalBilled.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getFprxrec() {
  		return fprxrec;
	}

	public FixedLengthStringData getFprxpfRecord() {
  		return fprxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setFprxrec(what);
	}

	public void setFprxrec(Object what) {
  		this.fprxrec.set(what);
	}

	public void setFprxpfRecord(Object what) {
  		this.fprxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(fprxrec.getLength());
		result.set(fprxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}