/*
 * File: B6685.java
 * Date: 29 August 2009 21:23:47
 * Author: Quipoz Limited
 * 
 * Class transformed from B6685.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;

import com.csc.life.flexiblepremium.reports.R5358Report;
import com.csc.life.flexiblepremium.reports.R5359Report;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* COMPILE-OPTIONS-SQL   CSRSQLCSR(*ENDJOB)        <Do not delete>
*      *
*
*   This program produces the maximum and minimum variance reports
*   for flexible premiums.
*
*   (Reporting previously carried out in B5359 (Collection)).
*
*    Extract those records to be reported on from the FREP file
*    written in B5359.
*
*    CONTROL TOTALS
*
*    01  -  No. of FREP records read.
*    02  -  No. of records over maximum LP S
*    03  -  Tot. amount of LP S over maximum
*    04  -  No. of records under minimum LP S
*    05  -  Tot. amount of LP S under minimum
*
****************************************************************** ****
* </pre>
*/
public class B6685 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlfrepCursorrs = null;
	private java.sql.PreparedStatement sqlfrepCursorps = null;
	private java.sql.Connection sqlfrepCursorconn = null;
	private String sqlfrepCursor = "";
	private R5358Report printerFile1 = new R5358Report();
	private R5359Report printerFile2 = new R5359Report();
	private FixedLengthStringData printerRec1 = new FixedLengthStringData(132);
	private FixedLengthStringData printerRec2 = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B6685");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaContractStart = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaContractEnd = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaProcflg = new FixedLengthStringData(1).init("C");
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private PackedDecimalData wsaaTotMaxFail = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotMinFail = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsaaOverflow1 = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq1 = new Validator(wsaaOverflow1, "Y");

	private FixedLengthStringData wsaaOverflow2 = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq2 = new Validator(wsaaOverflow2, "Y");

	private FixedLengthStringData wsaaMinReportWritten = new FixedLengthStringData(1).init("N");
	private Validator minReportWritten = new Validator(wsaaMinReportWritten, "Y");

	private FixedLengthStringData wsaaMaxReportWritten = new FixedLengthStringData(1).init("N");
	private Validator maxReportWritten = new Validator(wsaaMaxReportWritten, "Y");

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";
	private static final String t1693 = "T1693";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

	private FixedLengthStringData sqlFreppf = new FixedLengthStringData(38);
	private FixedLengthStringData sqlCompany = new FixedLengthStringData(1).isAPartOf(sqlFreppf, 0);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlFreppf, 1);
	private PackedDecimalData sqlBtdate = new PackedDecimalData(8, 0).isAPartOf(sqlFreppf, 9);
	private PackedDecimalData sqlLinstamt = new PackedDecimalData(11, 2).isAPartOf(sqlFreppf, 14);
	private PackedDecimalData sqlMinprem = new PackedDecimalData(11, 2).isAPartOf(sqlFreppf, 20);
	private PackedDecimalData sqlMaxprem = new PackedDecimalData(11, 2).isAPartOf(sqlFreppf, 26);
	private PackedDecimalData sqlSusamt = new PackedDecimalData(11, 2).isAPartOf(sqlFreppf, 32);

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData r5358h01Record = new FixedLengthStringData(31);
	private FixedLengthStringData r5358h01O = new FixedLengthStringData(31).isAPartOf(r5358h01Record, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(r5358h01O, 0);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(r5358h01O, 1);

	private FixedLengthStringData r5358d01Record = new FixedLengthStringData(63);
	private FixedLengthStringData r5358d01O = new FixedLengthStringData(63).isAPartOf(r5358d01Record, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(r5358d01O, 0);
	private FixedLengthStringData rd01Btdate = new FixedLengthStringData(10).isAPartOf(r5358d01O, 8);
	private ZonedDecimalData rd01Instprem = new ZonedDecimalData(11, 2).isAPartOf(r5358d01O, 18);
	private ZonedDecimalData rd01MinAmount = new ZonedDecimalData(17, 2).isAPartOf(r5358d01O, 29);
	private ZonedDecimalData rd01AmtRecd = new ZonedDecimalData(17, 2).isAPartOf(r5358d01O, 46);

	private FixedLengthStringData r5358t01Record = new FixedLengthStringData(18);
	private FixedLengthStringData r5358t01O = new FixedLengthStringData(18).isAPartOf(r5358t01Record, 0);
	private ZonedDecimalData rt01TotRecd = new ZonedDecimalData(18, 2).isAPartOf(r5358t01O, 0);

	private FixedLengthStringData r5359h01Record = new FixedLengthStringData(31);
	private FixedLengthStringData r5359h01O = new FixedLengthStringData(31).isAPartOf(r5359h01Record, 0);
	private FixedLengthStringData rh01Company2 = new FixedLengthStringData(1).isAPartOf(r5359h01O, 0);
	private FixedLengthStringData rh01Companynm2 = new FixedLengthStringData(30).isAPartOf(r5359h01O, 1);

	private FixedLengthStringData r5359d01Record = new FixedLengthStringData(63);
	private FixedLengthStringData r5359d01O = new FixedLengthStringData(63).isAPartOf(r5359d01Record, 0);
	private FixedLengthStringData rd01Chdrnum2 = new FixedLengthStringData(8).isAPartOf(r5359d01O, 0);
	private FixedLengthStringData rd01Btdate2 = new FixedLengthStringData(10).isAPartOf(r5359d01O, 8);
	private ZonedDecimalData rd01Instprem2 = new ZonedDecimalData(11, 2).isAPartOf(r5359d01O, 18);
	private ZonedDecimalData rd01MaxAmount2 = new ZonedDecimalData(17, 2).isAPartOf(r5359d01O, 29);
	private ZonedDecimalData rd01AmtRecd2 = new ZonedDecimalData(17, 2).isAPartOf(r5359d01O, 46);

	private FixedLengthStringData r5359t01Record = new FixedLengthStringData(18);
	private FixedLengthStringData r5359t01O = new FixedLengthStringData(18).isAPartOf(r5359t01Record, 0);
	private ZonedDecimalData rt01TotRecd2 = new ZonedDecimalData(18, 2).isAPartOf(r5359t01O, 0);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private DescTableDAM descIO = new DescTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private P6671par p6671par = new P6671par();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endOfCursor2080, 
		exit2090
	}

	public B6685() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		wsspEdterror.set(varcom.oK);
		printerFile1.openOutput();
		printerFile2.openOutput();
		/* Get company description for report header.*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		rh01Companynm2.set(descIO.getLongdesc());
		rh01Companynm.set(descIO.getLongdesc());
		rh01Company2.set(bsprIO.getCompany());
		rh01Company.set(bsprIO.getCompany());
		wsaaCompany.set(bsprIO.getCompany());
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES)) {
			wsaaContractStart.set(LOVALUE);
		}
		else {
			wsaaContractStart.set(p6671par.chdrnum);
		}
		if (isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaContractEnd.set(HIVALUE);
		}
		else {
			wsaaContractEnd.set(p6671par.chdrnum1);
		}
		sqlfrepCursor = " SELECT  COMPANY, CHDRNUM, BTDATE, LINSTAMT, MINPREM, MAXPREM, SUSAMT, PROCFLAG, PAYRSEQNO" +
" FROM   " + getAppVars().getTableNameOverriden("FREPPF") + " " +
" WHERE COMPANY = ?" +
" AND CHDRNUM >= ?" +
" AND CHDRNUM <= ?" +
" AND PROCFLAG = ?" +
" ORDER BY COMPANY, CHDRNUM, PAYRSEQNO";
		sqlerrorflag = false;
		try {
			sqlfrepCursorconn = getAppVars().getDBConnectionForTable(new com.csc.life.flexiblepremium.dataaccess.FreppfTableDAM());
			sqlfrepCursorps = getAppVars().prepareStatementEmbeded(sqlfrepCursorconn, sqlfrepCursor, "FREPPF");
			getAppVars().setDBString(sqlfrepCursorps, 1, wsaaCompany);
			getAppVars().setDBString(sqlfrepCursorps, 2, wsaaContractStart);
			getAppVars().setDBString(sqlfrepCursorps, 3, wsaaContractEnd);
			getAppVars().setDBString(sqlfrepCursorps, 4, wsaaProcflg);
			sqlfrepCursorrs = getAppVars().executeQuery(sqlfrepCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case endOfCursor2080: 
					endOfCursor2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		wsspEdterror.set(varcom.oK);
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlfrepCursorrs)) {
				getAppVars().getDBObject(sqlfrepCursorrs, 1, sqlCompany);
				getAppVars().getDBObject(sqlfrepCursorrs, 2, sqlChdrnum);
				getAppVars().getDBObject(sqlfrepCursorrs, 3, sqlBtdate);
				getAppVars().getDBObject(sqlfrepCursorrs, 4, sqlLinstamt);
				getAppVars().getDBObject(sqlfrepCursorrs, 5, sqlMinprem);
				getAppVars().getDBObject(sqlfrepCursorrs, 6, sqlMaxprem);
				getAppVars().getDBObject(sqlfrepCursorrs, 7, sqlSusamt);
			}
			else {
				goTo(GotoLabel.endOfCursor2080);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		goTo(GotoLabel.exit2090);
	}

protected void endOfCursor2080()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		if (isGT(sqlSusamt, sqlMaxprem)
		&& isNE(sqlMaxprem, 0)) {
			if (newPageReq2.isTrue()) {
				writeMaxHeader5000();
			}
			writeMaximumReport5100();
		}
		if (isLT(sqlSusamt, sqlMinprem)) {
			if (newPageReq1.isTrue()) {
				writeMinHeader6000();
			}
			writeMinimumReport6100();
		}
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void writeMaxHeader5000()
	{
		/*START*/
		printerRec2.set(SPACES);
		printerFile2.printR5359h01(r5359h01Record, indicArea);
		wsaaOverflow2.set("N");
		/*EXIT*/
	}

protected void writeMaximumReport5100()
	{
		start5100();
	}

protected void start5100()
	{
		printerRec2.set(SPACES);
		rd01Chdrnum2.set(sqlChdrnum);
		datcon1rec.intDate.set(sqlBtdate);
		callDatcon17000();
		rd01Btdate2.set(datcon1rec.extDate);
		rd01Instprem2.set(sqlLinstamt);
		rd01MaxAmount2.set(sqlMaxprem);
		rd01AmtRecd2.set(sqlSusamt);
		wsaaTotMaxFail.add(sqlSusamt);
		printerFile2.printR5359d01(r5359d01Record, indicArea);
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct03);
		contotrec.totval.set(sqlSusamt);
		callContot001();
		maxReportWritten.setTrue();
	}

protected void writeMaxTotalsReport5200()
	{
		/*PARA*/
		printerRec2.set(SPACES);
		rt01TotRecd2.set(wsaaTotMaxFail);
		printerFile2.printR5359t01(r5359t01Record, indicArea);
		wsaaOverflow2.set("Y");
		/*EXIT*/
	}

protected void writeMinHeader6000()
	{
		/*PARA*/
		printerRec1.set(SPACES);
		printerFile1.printR5358h01(r5358h01Record, indicArea);
		wsaaOverflow1.set("N");
		/*EXIT*/
	}

protected void writeMinimumReport6100()
	{
		start6100();
	}

protected void start6100()
	{
		printerRec1.set(SPACES);
		rd01Chdrnum.set(sqlChdrnum);
		datcon1rec.intDate.set(sqlBtdate);
		callDatcon17000();
		rd01Btdate.set(datcon1rec.extDate);
		rd01Instprem.set(sqlLinstamt);
		rd01MinAmount.set(sqlMinprem);
		rd01AmtRecd.set(sqlSusamt);
		wsaaTotMinFail.add(sqlSusamt);
		printerFile1.printR5358d01(r5358d01Record, indicArea);
		contotrec.totno.set(ct04);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct05);
		contotrec.totval.set(sqlSusamt);
		callContot001();
		minReportWritten.setTrue();
	}

protected void writeMinTotalsReport6200()
	{
		/*PARA*/
		printerRec1.set(SPACES);
		rt01TotRecd.set(wsaaTotMinFail);
		printerFile1.printR5358t01(r5358t01Record, indicArea);
		wsaaOverflow1.set("Y");
		/*EXIT*/
	}

protected void callDatcon17000()
	{
		/*START*/
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void close4000()
	{
		/*PARA*/
		if (maxReportWritten.isTrue()) {
			writeMaxTotalsReport5200();
		}
		if (minReportWritten.isTrue()) {
			writeMinTotalsReport6200();
		}
		getAppVars().freeDBConnectionIgnoreErr(sqlfrepCursorconn, sqlfrepCursorps, sqlfrepCursorrs);
		printerFile1.close();
		printerFile2.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
