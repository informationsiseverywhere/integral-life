/******************************************************************************
 * File Name 		: AgcmpfDAOImpl.java
 * Author			: smalchi2
 * Creation Date	: 10 January 2017
 * Project			: Integral Life
 * Description		: The DAO Implementation Class for AGCMPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.flexiblepremium.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.flexiblepremium.dataaccess.dao.AgcmpfDAO;
import com.csc.life.flexiblepremium.dataaccess.model.Agcmpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AgcmpfDAOImpl extends BaseDAOImpl<Agcmpf> implements AgcmpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(AgcmpfDAOImpl.class);

	public List<Agcmpf> searchAgcmpfRecord(Agcmpf agcmpf) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, AGNTNUM, LIFE, JLIFE, ");
		sqlSelect.append("COVERAGE, RIDER, PLNSFX, TRANNO, EFDATE, ");
		sqlSelect.append("ANNPREM, BASCMETH, INITCOM, BASCPY, COMPAY, ");
		sqlSelect.append("COMERN, SRVCPY, SCMDUE, SCMEARN, RNWCPY, ");
		sqlSelect.append("RNLCDUE, RNLCEARN, AGCLS, TERMID, TRDT, ");
		sqlSelect.append("TRTM, USER_T, CRTABLE, CURRFROM, CURRTO, ");
		sqlSelect.append("VALIDFLAG, SEQNO, PTDATE, CEDAGENT, OVRDCAT, ");
		sqlSelect.append("DORMFLAG, USRPRF, JOBNM, DATIME ");
		sqlSelect.append("FROM AGCMPF WHERE CHDRCOY = ? AND CHDRNUM = ? ");
		if(agcmpf.getLife() != null && agcmpf.getCoverage() != null && agcmpf.getRider() != null
				&& agcmpf.getPlnsfx() >=0 && agcmpf.getSeqno() >0)
		{
			sqlSelect.append(" AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ? AND SEQNO = ? ");
			sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, SEQNO ASC, UNIQUE_NUMBER DESC ");
		}
		//ILIFE-8786 start
		else {
			if(agcmpf.getLife() != null && agcmpf.getCoverage() != null && agcmpf.getRider() != null
					&& agcmpf.getPlnsfx() >=0 && agcmpf.getSeqno()==0) {
			sqlSelect.append(" AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ? ");
			sqlSelect.append(" order by CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, SEQNO DESC, DORMFLAG ASC, UNIQUE_NUMBER ASC ");
			}
		}
		//ILIFE-8786 end
		PreparedStatement psAgcmpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlAgcmpfRs = null;
		List<Agcmpf> outputList = new ArrayList<Agcmpf>();

		try {

			psAgcmpfSelect.setString(1, agcmpf.getChdrcoy());
			psAgcmpfSelect.setString(2, agcmpf.getChdrnum());
			if(agcmpf.getLife() != null && agcmpf.getCoverage() != null && agcmpf.getRider() != null
					&& agcmpf.getPlnsfx() >=0 && agcmpf.getSeqno() >0)
			{
				psAgcmpfSelect.setString(3, agcmpf.getLife());
				psAgcmpfSelect.setString(4, agcmpf.getCoverage());
				psAgcmpfSelect.setString(5, agcmpf.getRider());
				psAgcmpfSelect.setInt(6, agcmpf.getPlnsfx());
				psAgcmpfSelect.setInt(7, agcmpf.getSeqno());
			}
			//ILIFE-8786 start
			else {
				if(agcmpf.getLife() != null && agcmpf.getCoverage() != null && agcmpf.getRider() != null
						&& agcmpf.getPlnsfx() >=0 && agcmpf.getSeqno() ==0)
				{
					psAgcmpfSelect.setString(3, agcmpf.getLife());
					psAgcmpfSelect.setString(4, agcmpf.getCoverage());
					psAgcmpfSelect.setString(5, agcmpf.getRider());
					psAgcmpfSelect.setInt(6, agcmpf.getPlnsfx());
				}
			}
			//ILIFE-8786 end
			sqlAgcmpfRs = executeQuery(psAgcmpfSelect);

			while (sqlAgcmpfRs.next()) {

				agcmpf = new Agcmpf();

				agcmpf.setUniqueNumber(sqlAgcmpfRs.getInt("UNIQUE_NUMBER"));
				agcmpf.setChdrcoy(sqlAgcmpfRs.getString("CHDRCOY"));
				agcmpf.setChdrnum(sqlAgcmpfRs.getString("CHDRNUM"));
				agcmpf.setAgntnum(sqlAgcmpfRs.getString("AGNTNUM"));
				agcmpf.setLife(sqlAgcmpfRs.getString("LIFE"));
				agcmpf.setJlife(sqlAgcmpfRs.getString("JLIFE"));
				agcmpf.setCoverage(sqlAgcmpfRs.getString("COVERAGE"));
				agcmpf.setRider(sqlAgcmpfRs.getString("RIDER"));
				agcmpf.setPlnsfx(sqlAgcmpfRs.getInt("PLNSFX"));
				agcmpf.setTranno(sqlAgcmpfRs.getInt("TRANNO"));
				agcmpf.setEfdate(sqlAgcmpfRs.getInt("EFDATE"));
				agcmpf.setAnnprem(sqlAgcmpfRs.getBigDecimal("ANNPREM"));
				agcmpf.setBascmeth(sqlAgcmpfRs.getString("BASCMETH"));
				agcmpf.setInitcom(sqlAgcmpfRs.getBigDecimal("INITCOM"));
				agcmpf.setBascpy(sqlAgcmpfRs.getString("BASCPY"));
				agcmpf.setCompay(sqlAgcmpfRs.getBigDecimal("COMPAY"));
				agcmpf.setComern(sqlAgcmpfRs.getBigDecimal("COMERN"));
				agcmpf.setSrvcpy(sqlAgcmpfRs.getString("SRVCPY"));
				agcmpf.setScmdue(sqlAgcmpfRs.getBigDecimal("SCMDUE"));
				agcmpf.setScmearn(sqlAgcmpfRs.getBigDecimal("SCMEARN"));
				agcmpf.setRnwcpy(sqlAgcmpfRs.getString("RNWCPY"));
				agcmpf.setRnlcdue(sqlAgcmpfRs.getBigDecimal("RNLCDUE"));
				agcmpf.setRnlcearn(sqlAgcmpfRs.getBigDecimal("RNLCEARN"));
				agcmpf.setAgcls(sqlAgcmpfRs.getString("AGCLS"));
				agcmpf.setTermid(sqlAgcmpfRs.getString("TERMID"));
				agcmpf.setTrdt(sqlAgcmpfRs.getInt("TRDT"));
				agcmpf.setTrtm(sqlAgcmpfRs.getInt("TRTM"));
				agcmpf.setUserT(sqlAgcmpfRs.getInt("USER_T"));
				agcmpf.setCrtable(sqlAgcmpfRs.getString("CRTABLE"));
				agcmpf.setCurrfrom(sqlAgcmpfRs.getInt("CURRFROM"));
				agcmpf.setCurrto(sqlAgcmpfRs.getInt("CURRTO"));
				agcmpf.setValidflag(sqlAgcmpfRs.getString("VALIDFLAG"));
				agcmpf.setSeqno(sqlAgcmpfRs.getInt("SEQNO"));
				agcmpf.setPtdate(sqlAgcmpfRs.getInt("PTDATE"));
				agcmpf.setCedagent(sqlAgcmpfRs.getString("CEDAGENT"));
				agcmpf.setOvrdcat(sqlAgcmpfRs.getString("OVRDCAT"));
				agcmpf.setDormflag(sqlAgcmpfRs.getString("DORMFLAG"));
				agcmpf.setUsrprf(sqlAgcmpfRs.getString("USRPRF"));
				agcmpf.setJobnm(sqlAgcmpfRs.getString("JOBNM"));
				agcmpf.setDatime(sqlAgcmpfRs.getDate("DATIME"));

				outputList.add(agcmpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchAgcmpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psAgcmpfSelect, sqlAgcmpfRs);
		}

		return outputList;
	}
	public void insertIntoAgcmpf(Agcmpf agcmpf) throws SQLRuntimeException{

		StringBuilder sqlInsert = new StringBuilder();

		sqlInsert.append("INSERT INTO AGCMPF (");
		
		sqlInsert.append("COMPAY, COMERN, SCMDUE, SCMEARN, ");
		sqlInsert.append("RNLCDUE, RNLCEARN, PTDATE ");
		sqlInsert.append(") VALUES ( ?, ?, ?, ?, ?, ?, ?)");

		PreparedStatement psAgcmpfInsert = getPrepareStatement(sqlInsert.toString());
		int index = 1;

		try {

			
			psAgcmpfInsert.setBigDecimal(index++, agcmpf.getCompay());
			psAgcmpfInsert.setBigDecimal(index++, agcmpf.getComern());
			
			psAgcmpfInsert.setBigDecimal(index++, agcmpf.getScmdue());
			psAgcmpfInsert.setBigDecimal(index++, agcmpf.getScmearn());
			
			psAgcmpfInsert.setBigDecimal(index++, agcmpf.getRnlcdue());
			psAgcmpfInsert.setBigDecimal(index++, agcmpf.getRnlcearn());
			
			psAgcmpfInsert.setInt(index++, agcmpf.getPtdate());
			
		

			int affectedCount = executeUpdate(psAgcmpfInsert);

			LOGGER.debug("insertIntoAgcmpf {} rows inserted.", affectedCount);//IJTI-1561

		} catch (SQLException e) {
			LOGGER.error("insertIntoAgcmpf()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psAgcmpfInsert, null);
		}
	}
	

	  public Map<String, List<Agcmpf>> searchAgcmRecordByChdrnum(List<String> chdrnumList){
	    	
			StringBuilder sqlSelect1 = new StringBuilder();
			sqlSelect1.append("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,AGNTNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,TRANNO,EFDATE,ANNPREM,BASCMETH,INITCOM,BASCPY,COMPAY,COMERN,SRVCPY,SCMDUE,SCMEARN,RNWCPY,RNLCDUE,RNLCEARN,AGCLS,TERMID,TRDT,TRTM,CRTABLE,CURRFROM,CURRTO,VALIDFLAG,SEQNO,PTDATE,CEDAGENT,OVRDCAT,DORMFLAG ");
		    sqlSelect1.append("FROM AGCMPF WHERE  ");
		    sqlSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		    sqlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, AGNTNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC ");

		    PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
		    ResultSet sqlrs = null;
		    Map<String, List<Agcmpf>> resultMap = new HashMap<>();
		    
		    try {
		    	sqlrs = executeQuery(psSelect);
		
		        while (sqlrs.next()) {
					Agcmpf agcmpf = new Agcmpf();
					agcmpf.setUniqueNumber(sqlrs.getLong("Unique_number"));
					agcmpf.setChdrcoy(sqlrs.getString("chdrcoy"));
					agcmpf.setChdrnum(sqlrs.getString("chdrnum"));
					agcmpf.setAgntnum(sqlrs.getString("agntnum"));
					agcmpf.setLife(sqlrs.getString("life"));
					agcmpf.setJlife(sqlrs.getString("jlife"));
					agcmpf.setCoverage(sqlrs.getString("coverage"));
					agcmpf.setRider(sqlrs.getString("rider"));
					agcmpf.setPlnsfx(sqlrs.getInt("plnsfx"));
					agcmpf.setTranno(sqlrs.getInt("tranno"));
					agcmpf.setEfdate(sqlrs.getInt("efdate"));
					agcmpf.setAnnprem(sqlrs.getBigDecimal("annprem"));
					agcmpf.setBascmeth(sqlrs.getString("bascmeth"));
					agcmpf.setInitcom(sqlrs.getBigDecimal("initcom"));
					agcmpf.setBascpy(sqlrs.getString("bascpy"));
					agcmpf.setCompay(sqlrs.getBigDecimal("compay"));
					agcmpf.setComern(sqlrs.getBigDecimal("comern"));
					agcmpf.setSrvcpy(sqlrs.getString("srvcpy"));
					agcmpf.setScmdue(sqlrs.getBigDecimal("scmdue"));
					agcmpf.setScmearn(sqlrs.getBigDecimal("scmearn"));
					agcmpf.setRnwcpy(sqlrs.getString("rnwcpy"));
					agcmpf.setRnlcdue(sqlrs.getBigDecimal("rnlcdue"));
					agcmpf.setRnlcearn(sqlrs.getBigDecimal("rnlcearn"));
					agcmpf.setAgcls(sqlrs.getString("agcls"));
					agcmpf.setTermid(sqlrs.getString("termid"));
					agcmpf.setTrdt(sqlrs.getInt("trdt"));
					agcmpf.setTrtm(sqlrs.getInt("trtm"));
					agcmpf.setCrtable(sqlrs.getString("crtable"));
					agcmpf.setCurrfrom(sqlrs.getInt("currfrom"));
					agcmpf.setCurrto(sqlrs.getInt("currto"));
					agcmpf.setValidflag(sqlrs.getString("validflag"));
					agcmpf.setSeqno(sqlrs.getInt("seqno"));
					agcmpf.setPtdate(sqlrs.getInt("ptdate"));
					agcmpf.setCedagent(sqlrs.getString("cedagent"));
					agcmpf.setOvrdcat(sqlrs.getString("ovrdcat"));
					agcmpf.setDormflag(sqlrs.getString("dormflag"));
					
					if (resultMap.containsKey(agcmpf.getChdrnum())) {
						resultMap.get(agcmpf.getChdrnum()).add(agcmpf);
					} else {
						List<Agcmpf> agcmpfList = new ArrayList<>();
						agcmpfList.add(agcmpf);
						resultMap.put(agcmpf.getChdrnum(), agcmpfList);
					}
		        }
		    } catch (SQLException e) {
		        LOGGER.error("searchAgcmRecordByChdrnum()", e);//IJTI-1561
		        throw new SQLRuntimeException(e);
		    } finally {
		        close(psSelect, sqlrs);
		    }
			return resultMap;
		}
	
	public void insertAgcmpfList(List<Agcmpf> agcmpfList) {
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO AGCMPF(CHDRCOY,CHDRNUM,AGNTNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,TRANNO,EFDATE,ANNPREM,BASCMETH,INITCOM,BASCPY,COMPAY,COMERN,SRVCPY,SCMDUE,SCMEARN,RNWCPY,RNLCDUE,RNLCEARN,AGCLS,TERMID,TRDT,TRTM,CRTABLE,CURRFROM,CURRTO,VALIDFLAG,SEQNO,PTDATE,CEDAGENT,OVRDCAT,DORMFLAG, USER_T, USRPRF, JOBNM, DATIME,INITCOMMGST,RNWLCOMMGST) ");
		sb.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for (Agcmpf agcmpf : agcmpfList) {
				ps.setString(1, agcmpf.getChdrcoy());
				ps.setString(2, agcmpf.getChdrnum());
				ps.setString(3, agcmpf.getAgntnum());
				ps.setString(4, agcmpf.getLife());
				ps.setString(5, agcmpf.getJlife());
				ps.setString(6, agcmpf.getCoverage());
				ps.setString(7, agcmpf.getRider());
				ps.setInt(8, agcmpf.getPlnsfx());
				ps.setInt(9, agcmpf.getTranno());
				ps.setInt(10, agcmpf.getEfdate());
				ps.setBigDecimal(11, agcmpf.getAnnprem());
				ps.setString(12, agcmpf.getBascmeth());
				ps.setBigDecimal(13, agcmpf.getInitcom());
				ps.setString(14, agcmpf.getBascpy());
				ps.setBigDecimal(15, agcmpf.getCompay());
				ps.setBigDecimal(16, agcmpf.getComern());
				ps.setString(17, agcmpf.getSrvcpy());
				ps.setBigDecimal(18, agcmpf.getScmdue());
				ps.setBigDecimal(19, agcmpf.getScmearn());
				ps.setString(20, agcmpf.getRnwcpy());
				ps.setBigDecimal(21, agcmpf.getRnlcdue());
				ps.setBigDecimal(22, agcmpf.getRnlcearn());
				ps.setString(23, agcmpf.getAgcls());
				ps.setString(24, agcmpf.getTermid());
				ps.setInt(25, agcmpf.getTrdt());
				ps.setInt(26, agcmpf.getTrtm());
				ps.setString(27, agcmpf.getCrtable());
				ps.setInt(28, agcmpf.getCurrfrom());
				ps.setInt(29, agcmpf.getCurrto());
				ps.setString(30, agcmpf.getValidflag());
				ps.setInt(31, agcmpf.getSeqno());
				ps.setInt(32, agcmpf.getPtdate());
				ps.setString(33, agcmpf.getCedagent());
				ps.setString(34, agcmpf.getOvrdcat());
				ps.setString(35, agcmpf.getDormflag());
				ps.setInt(36, (agcmpf.getUserT() == null ? 0 : agcmpf.getUserT()));
				ps.setString(37, this.getUsrprf());
				ps.setString(38, this.getJobnm());
				ps.setTimestamp(39, getDatime());
				ps.setBigDecimal(40, agcmpf.getInitcommgst()==null ? BigDecimal.ZERO:agcmpf.getInitcommgst());
				ps.setBigDecimal(41, agcmpf.getRnwlcommgst()==null ? BigDecimal.ZERO:agcmpf.getRnwlcommgst());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertAgcmpfList()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
    public void updateInvalidRecord(List<Agcmpf> agcmpfList) {
        if (agcmpfList != null && !agcmpfList.isEmpty()) {
            String SQL_UPDATE = "UPDATE AGCMPF SET VALIDFLAG=?,CURRTO=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";

            PreparedStatement psUpdate = getPrepareStatement(SQL_UPDATE);
            try {
                for (Agcmpf c : agcmpfList) {
                	psUpdate.setString(1, c.getValidflag());
                	psUpdate.setInt(2, c.getCurrto());
                    psUpdate.setString(3, getJobnm());
                    psUpdate.setString(4, getUsrprf());
                    psUpdate.setTimestamp(5, getDatime());
                    psUpdate.setLong(6, c.getUniqueNumber());					
                    psUpdate.addBatch();
                }
                psUpdate.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("updateInvalidRecord()", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(psUpdate, null);
            }
        }
    }
    
    public void updateAgcmRecord(List<Agcmpf> agcmpfList) {
    	 if (agcmpfList != null && !agcmpfList.isEmpty()) {
			String SQL_UPDATE = "UPDATE AGCMPF SET ANNPREM=? WHERE UNIQUE_NUMBER = ?  ";
	
			PreparedStatement psUpdate = getPrepareStatement(SQL_UPDATE);
			try {
				{
					  for (Agcmpf agcmpf : agcmpfList) {
						psUpdate.setBigDecimal(1, agcmpf.getAnnprem());
						psUpdate.setLong(2, agcmpf.getUniqueNumber());
						psUpdate.addBatch(); 
					  }
				}
				psUpdate.executeBatch(); 
			} catch (SQLException e) {
				LOGGER.error("updateAgcmRecord()", e);
				throw new SQLRuntimeException(e);
			} finally {
				close(psUpdate, null);
			}
    	 }
	}
    public void updateAgcmrevdRecord(List<Agcmpf> agcmpfList) {
        if (agcmpfList != null && !agcmpfList.isEmpty()) {
            String SQL_UPDATE = "UPDATE AGCMPF SET COMPAY=?,COMERN=?,RNLCDUE=?,RNLCEARN=?,SCMDUE=?,SCMEARN=?,PTDATE=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";

            PreparedStatement psUpdate = getPrepareStatement(SQL_UPDATE);
            try {
                for (Agcmpf c : agcmpfList) {
                	psUpdate.setBigDecimal(1, c.getCompay());
                	psUpdate.setBigDecimal(2, c.getComern());
                	psUpdate.setBigDecimal(3, c.getRnlcdue());
                	psUpdate.setBigDecimal(4, c.getRnlcearn());
                	psUpdate.setBigDecimal(5, c.getScmdue());
                	psUpdate.setBigDecimal(6, c.getScmearn());
                	psUpdate.setInt(7, c.getPtdate());
                    psUpdate.setString(8, getJobnm());
                    psUpdate.setString(9, getUsrprf());
                    psUpdate.setTimestamp(10, getDatime());
                    psUpdate.setLong(11, c.getUniqueNumber());					
                    psUpdate.addBatch();
                }
                psUpdate.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("updateAgcmrevdRecord()", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(psUpdate, null);
            }
        }
    }
    
    public List<Agcmpf> searchAgcmstRecord(String chdrcoy,String chdrnum, String life, String coverage, String rider, int plnsfx, String validflag) throws SQLRuntimeException{

        StringBuilder sqlSelect = new StringBuilder();
        sqlSelect.append("SELECT * FROM AGCMPF WHERE CHDRCOY = ? AND CHDRNUM = ?  AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ? AND VALIDFLAG = ? ");
        sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, AGNTNUM ASC, UNIQUE_NUMBER DESC ");
        PreparedStatement psAgcmpfSelect = getPrepareStatement(sqlSelect.toString());
        ResultSet sqlAgcmpfRs = null;
        List<Agcmpf> outputList = new ArrayList<Agcmpf>();

        try {

            psAgcmpfSelect.setString(1, chdrcoy);
            psAgcmpfSelect.setString(2, chdrnum);
            psAgcmpfSelect.setString(3, life);
            psAgcmpfSelect.setString(4, coverage);
            psAgcmpfSelect.setString(5, rider);
            psAgcmpfSelect.setInt(6, plnsfx);
            psAgcmpfSelect.setString(7, validflag);

            sqlAgcmpfRs = executeQuery(psAgcmpfSelect);

            while (sqlAgcmpfRs.next()) {

                Agcmpf agcmpf = new Agcmpf();

                agcmpf.setUniqueNumber(sqlAgcmpfRs.getInt("UNIQUE_NUMBER"));
                agcmpf.setChdrcoy(sqlAgcmpfRs.getString("CHDRCOY"));
                agcmpf.setChdrnum(sqlAgcmpfRs.getString("CHDRNUM"));
                agcmpf.setAgntnum(sqlAgcmpfRs.getString("AGNTNUM"));
                agcmpf.setLife(sqlAgcmpfRs.getString("LIFE"));
                agcmpf.setJlife(sqlAgcmpfRs.getString("JLIFE"));
                agcmpf.setCoverage(sqlAgcmpfRs.getString("COVERAGE"));
                agcmpf.setRider(sqlAgcmpfRs.getString("RIDER"));
                agcmpf.setPlnsfx(sqlAgcmpfRs.getInt("PLNSFX"));
                agcmpf.setTranno(sqlAgcmpfRs.getInt("TRANNO"));
                agcmpf.setEfdate(sqlAgcmpfRs.getInt("EFDATE"));
                agcmpf.setAnnprem(sqlAgcmpfRs.getBigDecimal("ANNPREM"));
                agcmpf.setBascmeth(sqlAgcmpfRs.getString("BASCMETH"));
                agcmpf.setInitcom(sqlAgcmpfRs.getBigDecimal("INITCOM"));
                agcmpf.setBascpy(sqlAgcmpfRs.getString("BASCPY"));
                agcmpf.setCompay(sqlAgcmpfRs.getBigDecimal("COMPAY"));
                agcmpf.setComern(sqlAgcmpfRs.getBigDecimal("COMERN"));
                agcmpf.setSrvcpy(sqlAgcmpfRs.getString("SRVCPY"));
                agcmpf.setScmdue(sqlAgcmpfRs.getBigDecimal("SCMDUE"));
                agcmpf.setScmearn(sqlAgcmpfRs.getBigDecimal("SCMEARN"));
                agcmpf.setRnwcpy(sqlAgcmpfRs.getString("RNWCPY"));
                agcmpf.setRnlcdue(sqlAgcmpfRs.getBigDecimal("RNLCDUE"));
                agcmpf.setRnlcearn(sqlAgcmpfRs.getBigDecimal("RNLCEARN"));
                agcmpf.setAgcls(sqlAgcmpfRs.getString("AGCLS"));
                agcmpf.setTermid(sqlAgcmpfRs.getString("TERMID"));
                agcmpf.setTrdt(sqlAgcmpfRs.getInt("TRDT"));
                agcmpf.setTrtm(sqlAgcmpfRs.getInt("TRTM"));
                agcmpf.setUserT(sqlAgcmpfRs.getInt("USER_T"));
                agcmpf.setCrtable(sqlAgcmpfRs.getString("CRTABLE"));
                agcmpf.setCurrfrom(sqlAgcmpfRs.getInt("CURRFROM"));
                agcmpf.setCurrto(sqlAgcmpfRs.getInt("CURRTO"));
                agcmpf.setValidflag(sqlAgcmpfRs.getString("VALIDFLAG"));
                agcmpf.setSeqno(sqlAgcmpfRs.getInt("SEQNO"));
                agcmpf.setPtdate(sqlAgcmpfRs.getInt("PTDATE"));
                agcmpf.setCedagent(sqlAgcmpfRs.getString("CEDAGENT"));
                agcmpf.setOvrdcat(sqlAgcmpfRs.getString("OVRDCAT"));
                agcmpf.setDormflag(sqlAgcmpfRs.getString("DORMFLAG"));
                agcmpf.setInitcommgst(sqlAgcmpfRs.getBigDecimal("INITCOMMGST"));
                agcmpf.setRnwlcommgst(sqlAgcmpfRs.getBigDecimal("RNWLCOMMGST"));

                outputList.add(agcmpf);
            }

        } catch (SQLException e) {
            LOGGER.error("searchAgcmstRecord()", e);
            throw new SQLRuntimeException(e);
        } finally {
            close(psAgcmpfSelect, sqlAgcmpfRs);
        }

        return outputList;
    }
}
