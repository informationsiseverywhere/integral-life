/*
 * File: Cmpy004.java
 * Date: 29 August 2009 22:41:27
 * Author: Quipoz Limited
 *
 * Class transformed from CMPY004.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.agents.dataaccess.ChdrcmcTableDAM;
import com.csc.life.agents.dataaccess.LifecmcTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5692rec;
import com.csc.life.agents.tablestructures.T5694rec;
import com.csc.life.flexiblepremium.dataaccess.AgcmseqTableDAM;
import com.csc.life.flexiblepremium.dataaccess.Fpcolf1TableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*                  INSTALMENT RATE COMMISSION PAYMENT
*
* Obtain the  commission  payment  calculation  rules  (T5694),
* keyed by Coverage/rider  table code and effective date (hence
* use ITDM).
*
* Calculate 'To year'
*
*      The entries in this  table  are referenced by 'To year'.
*           To obtain the  year, calculate the year as follows,
*           use CURRTO Date passed from the FPCO record
*
*      Use DATCON3  with a  frequency  of  1  and  passing  the
*           commencement date (commission/risk) and  the CURRTO
*           date.  This  will  return  the number  of years
*           in existence and this value will be used to look up
*           the table entries in T5694, i.e.  the 'To year'.
*
*           Note : To read T5694, determine the coverage premium
*           term based on RCD till cessation date. Read T5694
*           based on the itemkey = Renewal Premium Method +
*           Coverage Premium Term. If the item is not found,
*           replace the coverage premium term with catch-all i.e.
*           '***' and read table T5694.
*
* Calculate the total ann prem for the seq on passed
*
*     Read all the AGCM records for the seq no
*     Accumulating the Ann prem.
*
* Check if Ann prem accumulated = target passed
*     If the total accumulated is =
*     there are no other seq no AGCMs around, so commission
*     is to be added to the record
*
* Ann prem for seq < target passed
*     Read T5730 using the comm method.
*     If not found, must be renewal
*     Else, servicing
*     o/t commission is only to be added to the first
*     AGCM seq no records so dont read other AGCMs or FPCO.
*
*     Otherwise,
*
*     Read all AGCMs for prev seq no's and accumulate the ann
*     prem.
*
*     After these have been read, read the current FPCO
*     for the target year and check the amount of prem recd
*
*     If the amt recd is > the target prem, move
*        target to WSAA-PREM-RECD
*     Else
*        Move recd to WSAA-PREM-RECD.
*
*     Then subtract the previous seq no AGCM ann prem from
*     the total.
*
*     If this result is > the Ann prem for the seqno, move
*     the Ann prem for the seqno to WSAA-PREM-RECD.
*
*     If WSAA-PREM-RECD = zero
*        NO COMMISSION IS PAYABLE ON THIS AGCM.
*
* If the result is > zero, calc commission by applying the
* enhancements.
*
*      Firstly, access the enhancement rules table T5692, Keyed
*           on coverage/rider code  and  agent  class (READR on
*           AGLFCMC).  Based on the  age  of the life (READR on
*           LIFECMC with the key passed) access the appropriate
*           section in  the  commission  enhancement  table and
*           obtain the enhancement % of premium for the Renewal
*           Commission.  Apply this  enhancement  to the % rate
*           from T5694, also accessed by age band.
*
*      Secondly,  apply  the   above   adjusted   rate  to  the
*           instalment  premium  passed  in  the  linkage area.
*           This gives the commission amount.
*
*      Thirdly, apply  the  commission  enhancement  percentage
*           for renewal commission from T5692 to the commission
*           amount calculated  above.  This  gives the enhanced
*           renewal commission.
*
* To calculate the amount payable, if the CLNK-INSTPREM is <
* WSAA-PREM-RECD, move CLNK-INSTPREM to WSAA-PREM-RECD.
*
* Then, find the % of this AGCM Annprem over the Ann Prem
* for the whole seqno.
*
* Apply this % to WSAA-PREM-RECD and multiply by the T5694
* relevant % then apply any enhancement.
*
* The renewal commission should  now be returned in the linkage
* area as both the commission paid and earned.
/
***********************************************************************
*                                                                     *
* </pre>
*/
public class Cmpy004 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "CMPY004";
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT5692Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaAgentClass = new FixedLengthStringData(4).isAPartOf(wsaaT5692Key, 0).init(SPACES);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5692Key, 4).init(SPACES);
	private PackedDecimalData wsaaReprempc = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaRwcmrate = new PackedDecimalData(5, 2);
	private ZonedDecimalData wsaaT5694 = new ZonedDecimalData(5, 2).init(ZERO);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaRenComm = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaOtComm = new FixedLengthStringData(1).init("N");
	private Validator otComm = new Validator(wsaaOtComm, "Y");

	private FixedLengthStringData wsaaIssueTran = new FixedLengthStringData(1).init("N");
	private Validator issueTran = new Validator(wsaaIssueTran, "Y");
		/* WSAA-STORED-AMTS */
	private PackedDecimalData wsaaPercentTarget = new PackedDecimalData(18, 5);
	private PackedDecimalData wsaaPremRecd = new PackedDecimalData(18, 5);
	private PackedDecimalData wsaaAnnprem = new PackedDecimalData(18, 5);
	private PackedDecimalData wsaaSeqAnnPrem = new PackedDecimalData(18, 5);
	private PackedDecimalData wsaaPrevAnnPrem = new PackedDecimalData(18, 5);

	private FixedLengthStringData wsaaPremApplic = new FixedLengthStringData(1).init("N");
	private Validator premApplic = new Validator(wsaaPremApplic, "Y");
	private PackedDecimalData wsaaPremAtStart = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaTotReqd = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaRemaining = new PackedDecimalData(16, 5);

	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaMethod = new FixedLengthStringData(4).isAPartOf(wsaaItemitem, 0).init(SPACES);
	private FixedLengthStringData wsaaSrcebus = new FixedLengthStringData(2).isAPartOf(wsaaItemitem, 4).init(SPACES);
		/* ERRORS */
	private String h028 = "H028";
	private String fpcolf1rec = "FPCOLF1REC";
	private String agcmseqrec = "AGCMSEQREC";
		/* TABLES */
	private String t5692 = "T5692";
	private String t5694 = "T5694";
	private String t5730 = "T5730";
		/*AGCMs in sequence number order*/
	private AgcmseqTableDAM agcmseqIO = new AgcmseqTableDAM();
		/*Contract Header Commission Calcs.*/
	private ChdrcmcTableDAM chdrcmcIO = new ChdrcmcTableDAM();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Flexible Premium Coverage*/
	private Fpcolf1TableDAM fpcolf1IO = new Fpcolf1TableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*LIFE record Commission Calculations*/
	private LifecmcTableDAM lifecmcIO = new LifecmcTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T5692rec t5692rec = new T5692rec();
	private T5694rec t5694rec = new T5694rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit090,
		next1050,
		bypassDatcon31080,
		fpco2080,
		exit2090,
		readNextAgcm2150,
		readNextAgcm2250,
		exit2390,
		afterT56923000,
		exit3590,
		seExit8090,
		dbExit8190
	}

	public Cmpy004() {
		super();
	}

public void mainline(Object... parmArray)
	{
		comlinkrec.clnkallRec = convertAndSetParam(comlinkrec.clnkallRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			para010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		comlinkrec.statuz.set(varcom.oK);
		comlinkrec.payamnt.set(0);
		comlinkrec.erndamt.set(0);
		obtainPaymntRules1000();
		if (isEQ(wsaaT5694,ZERO)) {
			goTo(GotoLabel.exit090);
		}
		calcPremamt2000();
		if (!premApplic.isTrue()) {
			goTo(GotoLabel.exit090);
		}
		calcRnwlCommission3000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void obtainPaymntRules1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1000();
				}
				case next1050: {
					next1050();
				}
				case bypassDatcon31080: {
					bypassDatcon31080();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1000()
	{
		if (isEQ(comlinkrec.billfreq,"00")) {
			wsaaTerm.set(0);
			goTo(GotoLabel.bypassDatcon31080);
		}
		datcon3rec.intDate2.set(comlinkrec.currto);
		datcon3rec.intDate1.set(comlinkrec.effdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError8000();
		}
		compute(wsaaTerm, 5).set(add(datcon3rec.freqFactor,0.99999));
		chdrcmcIO.setChdrcoy(comlinkrec.chdrcoy);
		chdrcmcIO.setChdrnum(comlinkrec.chdrnum);
		chdrcmcIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrcmcIO);
		if (isNE(chdrcmcIO.getStatuz(),varcom.oK)
		&& isNE(chdrcmcIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrcmcIO.getParams());
			syserrrec.statuz.set(chdrcmcIO.getStatuz());
			dbError8100();
		}
		wsaaItemitem.set(SPACES);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(comlinkrec.chdrcoy);
		itdmIO.setItemtabl(t5694);
		wsaaMethod.set(comlinkrec.method);
		wsaaSrcebus.set(chdrcmcIO.getSrcebus());
		itdmIO.setItemitem(wsaaItemitem);
		itdmIO.setItmfrm(comlinkrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
	}

protected void next1050()
	{
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5694)
		|| isNE(itdmIO.getItemitem(),wsaaItemitem)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			if (isNE(itdmIO.getItemitem(),wsaaItemitem)
			&& isNE(wsaaSrcebus,"**")) {
				itdmIO.setDataKey(SPACES);
				itdmIO.setItemcoy(comlinkrec.chdrcoy);
				itdmIO.setItemtabl(t5694);
				wsaaMethod.set(comlinkrec.method);
				wsaaSrcebus.set("**");
				itdmIO.setItemitem(wsaaItemitem);
				itdmIO.setItmfrm(comlinkrec.effdate);
				itdmIO.setFunction(varcom.begn);
				goTo(GotoLabel.next1050);
			}
			syserrrec.params.set(SPACES);
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(comlinkrec.method.toString());
			FixedLengthStringData groupTEMP = itdmIO.getParams();
			stringVariable1.append(groupTEMP.toString());
			itdmIO.setParams(groupTEMP);
			syserrrec.params.setLeft(stringVariable1.toString());
			syserrrec.statuz.set(h028);
			dbError8100();
		}
		else {
			t5694rec.t5694Rec.set(itdmIO.getGenarea());
		}
	}

protected void bypassDatcon31080()
	{
		wsaaT5694.set(0);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,11)); wsaaIndex.add(1)){
			getT56944000();
		}
		/*EXIT*/
	}

protected void calcPremamt2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para2000();
					checkAnnPrem2050();
					allAgcmsForSeq2060();
					allAgcmsForPrevSeqnos2070();
				}
				case fpco2080: {
					fpco2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2000()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(comlinkrec.chdrcoy);
		itemIO.setItemtabl(t5730);
		itemIO.setItemitem(comlinkrec.method);
		itdmIO.setItmfrm(comlinkrec.effdate);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			wsaaOtComm.set("N");
		}
		else {
			wsaaOtComm.set("Y");
		}
	}

protected void checkAnnPrem2050()
	{
		wsaaPremRecd.set(ZERO);
		wsaaSeqAnnPrem.set(ZERO);
		wsaaPrevAnnPrem.set(ZERO);
		wsaaPremApplic.set("N");
		wsaaIssueTran.set("N");
		if (isEQ(comlinkrec.currto,comlinkrec.effdate)) {
			wsaaSeqAnnPrem.set(comlinkrec.annprem);
			wsaaPremRecd.set(comlinkrec.instprem);
			wsaaPremApplic.set("Y");
			wsaaIssueTran.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void allAgcmsForSeq2060()
	{
		agcmseqIO.setParams(SPACES);
		agcmseqIO.setChdrcoy(comlinkrec.chdrcoy);
		agcmseqIO.setChdrnum(comlinkrec.chdrnum);
		agcmseqIO.setLife(comlinkrec.life);
		agcmseqIO.setCoverage(comlinkrec.coverage);
		agcmseqIO.setRider(comlinkrec.rider);
		agcmseqIO.setPlanSuffix(comlinkrec.planSuffix);
		agcmseqIO.setSeqno(comlinkrec.seqno);
		agcmseqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmseqIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		agcmseqIO.setFormat(agcmseqrec);
		agcmseqIO.setStatuz(varcom.oK);
		SmartFileCode.execute(appVars, agcmseqIO);
		if (isNE(agcmseqIO.getStatuz(),varcom.oK)
		&& isNE(agcmseqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(agcmseqIO.getStatuz());
			syserrrec.params.set(agcmseqIO.getParams());
			dbError8100();
		}
		if (isNE(agcmseqIO.getChdrcoy(),comlinkrec.chdrcoy)
		|| isNE(agcmseqIO.getChdrnum(),comlinkrec.chdrnum)
		|| isNE(agcmseqIO.getLife(),comlinkrec.life)
		|| isNE(agcmseqIO.getCoverage(),comlinkrec.coverage)
		|| isNE(agcmseqIO.getRider(),comlinkrec.rider)
		|| isNE(agcmseqIO.getPlanSuffix(),comlinkrec.planSuffix)
		|| isNE(agcmseqIO.getSeqno(),comlinkrec.seqno)) {
			agcmseqIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(agcmseqIO.getStatuz(),varcom.endp))) {
			processAgcm2100();
		}

	}

protected void allAgcmsForPrevSeqnos2070()
	{
		if (otComm.isTrue()
		|| isEQ(wsaaSeqAnnPrem,comlinkrec.targetPrem)) {
			wsaaPremRecd.set(comlinkrec.instprem);
			wsaaPremApplic.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(comlinkrec.seqno,1)) {
			goTo(GotoLabel.fpco2080);
		}
		agcmseqIO.setChdrcoy(comlinkrec.chdrcoy);
		agcmseqIO.setChdrnum(comlinkrec.chdrnum);
		agcmseqIO.setLife(comlinkrec.life);
		agcmseqIO.setCoverage(comlinkrec.coverage);
		agcmseqIO.setRider(comlinkrec.rider);
		agcmseqIO.setPlanSuffix(comlinkrec.planSuffix);
		agcmseqIO.setSeqno(0);
		agcmseqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmseqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmseqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		agcmseqIO.setFormat(agcmseqrec);
		agcmseqIO.setStatuz(varcom.oK);
		SmartFileCode.execute(appVars, agcmseqIO);
		if (isNE(agcmseqIO.getStatuz(),varcom.oK)
		&& isNE(agcmseqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(agcmseqIO.getStatuz());
			syserrrec.params.set(agcmseqIO.getParams());
		}
		if (isNE(agcmseqIO.getChdrcoy(),comlinkrec.chdrcoy)
		|| isNE(agcmseqIO.getChdrnum(),comlinkrec.chdrnum)
		|| isNE(agcmseqIO.getLife(),comlinkrec.life)
		|| isNE(agcmseqIO.getCoverage(),comlinkrec.coverage)
		|| isNE(agcmseqIO.getRider(),comlinkrec.rider)
		|| isNE(agcmseqIO.getPlanSuffix(),comlinkrec.planSuffix)
		|| isGTE(agcmseqIO.getSeqno(),comlinkrec.seqno)) {
			agcmseqIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(agcmseqIO.getStatuz(),varcom.endp))) {
			prevSeq2200();
		}

	}

protected void fpco2080()
	{
		calcFpcoTotal2300();
	}

protected void processAgcm2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2101();
				}
				case readNextAgcm2150: {
					readNextAgcm2150();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2101()
	{
		if (isEQ(agcmseqIO.getOvrdcat(),"O")
		|| isEQ(agcmseqIO.getPtdate(),0)) {
			goTo(GotoLabel.readNextAgcm2150);
		}
		wsaaSeqAnnPrem.add(agcmseqIO.getAnnprem());
	}

protected void readNextAgcm2150()
	{
		agcmseqIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmseqIO);
		if (isNE(agcmseqIO.getStatuz(),varcom.oK)
		&& isNE(agcmseqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(agcmseqIO.getStatuz());
			syserrrec.params.set(agcmseqIO.getParams());
		}
		if (isNE(agcmseqIO.getChdrcoy(),comlinkrec.chdrcoy)
		|| isNE(agcmseqIO.getChdrnum(),comlinkrec.chdrnum)
		|| isNE(agcmseqIO.getLife(),comlinkrec.life)
		|| isNE(agcmseqIO.getCoverage(),comlinkrec.coverage)
		|| isNE(agcmseqIO.getRider(),comlinkrec.rider)
		|| isNE(agcmseqIO.getPlanSuffix(),comlinkrec.planSuffix)
		|| isNE(agcmseqIO.getSeqno(),comlinkrec.seqno)) {
			agcmseqIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void prevSeq2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2201();
				}
				case readNextAgcm2250: {
					readNextAgcm2250();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2201()
	{
		if (isEQ(agcmseqIO.getOvrdcat(),"O")
		|| isEQ(agcmseqIO.getPtdate(),0)) {
			goTo(GotoLabel.readNextAgcm2250);
		}
		wsaaPrevAnnPrem.add(agcmseqIO.getAnnprem());
	}

protected void readNextAgcm2250()
	{
		agcmseqIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmseqIO);
		if (isNE(agcmseqIO.getStatuz(),varcom.oK)
		&& isNE(agcmseqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(agcmseqIO.getStatuz());
			syserrrec.params.set(agcmseqIO.getParams());
		}
		if (isNE(agcmseqIO.getChdrcoy(),comlinkrec.chdrcoy)
		|| isNE(agcmseqIO.getChdrnum(),comlinkrec.chdrnum)
		|| isNE(agcmseqIO.getLife(),comlinkrec.life)
		|| isNE(agcmseqIO.getCoverage(),comlinkrec.coverage)
		|| isNE(agcmseqIO.getRider(),comlinkrec.rider)
		|| isNE(agcmseqIO.getPlanSuffix(),comlinkrec.planSuffix)
		|| isEQ(agcmseqIO.getSeqno(),comlinkrec.seqno)) {
			agcmseqIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void calcFpcoTotal2300()
	{
		try {
			start2301();
		}
		catch (GOTOException e){
		}
	}

protected void start2301()
	{
		fpcolf1IO.setChdrcoy(comlinkrec.chdrcoy);
		fpcolf1IO.setChdrnum(comlinkrec.chdrnum);
		fpcolf1IO.setLife(comlinkrec.life);
		fpcolf1IO.setCoverage(comlinkrec.coverage);
		fpcolf1IO.setRider(comlinkrec.rider);
		fpcolf1IO.setPlanSuffix(comlinkrec.planSuffix);
		datcon2rec.frequency.set("01");
		datcon2rec.intDate1.set(comlinkrec.currto);
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			dbError8100();
		}
		fpcolf1IO.setTargfrom(datcon2rec.intDate2);
		fpcolf1IO.setActiveInd(SPACES);
		fpcolf1IO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fpcolf1IO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fpcolf1IO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX", "TARGFROM");
		fpcolf1IO.setFormat(fpcolf1rec);
		fpcolf1IO.setStatuz(varcom.oK);
		SmartFileCode.execute(appVars, fpcolf1IO);
		if (isNE(fpcolf1IO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(fpcolf1IO.getStatuz());
			syserrrec.params.set(fpcolf1IO.getParams());
			dbError8100();
		}
		if (isNE(fpcolf1IO.getChdrcoy(),comlinkrec.chdrcoy)
		|| isNE(fpcolf1IO.getChdrnum(),comlinkrec.chdrnum)
		|| isNE(fpcolf1IO.getLife(),comlinkrec.life)
		|| isNE(fpcolf1IO.getCoverage(),comlinkrec.coverage)
		|| isNE(fpcolf1IO.getRider(),comlinkrec.rider)
		|| isNE(fpcolf1IO.getPlanSuffix(),comlinkrec.planSuffix)
		|| isNE(fpcolf1IO.getTargfrom(),datcon2rec.intDate2)
		|| isEQ(fpcolf1IO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(fpcolf1IO.getParams());
			dbError8100();
		}
		if (isGT(fpcolf1IO.getPremRecPer(),fpcolf1IO.getTargetPremium())) {
			wsaaPremRecd.set(fpcolf1IO.getTargetPremium());
		}
		else {
			wsaaPremRecd.set(fpcolf1IO.getPremRecPer());
		}
		if (isLTE(wsaaPremRecd,wsaaPrevAnnPrem)) {
			wsaaPremApplic.set("N");
			goTo(GotoLabel.exit2390);
		}
		compute(wsaaPremAtStart, 5).set(sub(wsaaPremRecd,comlinkrec.instprem));
		compute(wsaaTotReqd, 5).set(add(wsaaSeqAnnPrem,wsaaPrevAnnPrem));
		if (isGTE(wsaaPremAtStart,wsaaTotReqd)) {
			wsaaPremApplic.set("N");
			goTo(GotoLabel.exit2390);
		}
		compute(wsaaRemaining, 5).set(sub(wsaaPremRecd,wsaaPrevAnnPrem));
		if (isLT(wsaaRemaining,wsaaSeqAnnPrem)) {
			wsaaPremRecd.set(wsaaRemaining);
		}
		else {
			wsaaPremRecd.set(comlinkrec.instprem);
		}
		if (isGT(wsaaPremRecd,ZERO)) {
			wsaaPremApplic.set("Y");
		}
		else {
			wsaaPremApplic.set("N");
		}
	}

protected void calcRnwlCommission3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para3000();
				}
				case afterT56923000: {
					afterT56923000();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3000()
	{
		itdmIO.setDataKey(SPACES);
		wsaaCrtable.set(comlinkrec.crtable);
		wsaaAgentClass.set(comlinkrec.agentClass);
		itdmIO.setItemcoy(comlinkrec.chdrcoy);
		itdmIO.setItemtabl(t5692);
		itdmIO.setItemitem(wsaaT5692Key);
		itdmIO.setItmfrm(comlinkrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5692)
		|| isNE(itdmIO.getItemitem(),wsaaT5692Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemtabl(t5692);
			wsaaCrtable.set("****");
			itdmIO.setItemitem(wsaaT5692Key);
			itdmIO.setItmfrm(comlinkrec.effdate);
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				dbError8100();
			}
			if (isNE(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
			|| isNE(itdmIO.getItemtabl(),t5692)
			|| isNE(itdmIO.getItemitem(),wsaaT5692Key)
			|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
				t5692rec.age01.set(ZERO);
				t5692rec.age02.set(ZERO);
				t5692rec.age03.set(ZERO);
				t5692rec.age04.set(ZERO);
				goTo(GotoLabel.afterT56923000);
			}
		}
		t5692rec.t5692Rec.set(itdmIO.getGenarea());
	}

protected void afterT56923000()
	{
		lifecmcIO.setFunction(varcom.readr);
		lifecmcIO.setChdrcoy(comlinkrec.chdrcoy);
		lifecmcIO.setChdrnum(comlinkrec.chdrnum);
		lifecmcIO.setLife(comlinkrec.life);
		if (isEQ(comlinkrec.jlife,SPACES)) {
			lifecmcIO.setJlife(ZERO);
		}
		else {
			lifecmcIO.setJlife(comlinkrec.jlife);
		}
		SmartFileCode.execute(appVars, lifecmcIO);
		if (isNE(lifecmcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifecmcIO.getParams());
			syserrrec.statuz.set(lifecmcIO.getStatuz());
			dbError8100();
		}
		getT5692Value3500();
		if (isLT(comlinkrec.instprem,wsaaPremRecd)) {
			wsaaPremRecd.set(comlinkrec.instprem);
		}
		wsaaAnnprem.set(comlinkrec.annprem);
		compute(wsaaPercentTarget, 5).set(div(wsaaAnnprem,wsaaSeqAnnPrem));
		if (!issueTran.isTrue()) {
			compute(wsaaPremRecd, 5).set(mult(wsaaPremRecd,wsaaPercentTarget));
		}
		compute(wsaaRenComm, 6).setRounded(div(mult(div(mult(div(mult(wsaaPremRecd,wsaaReprempc),100),wsaaRwcmrate),100),wsaaT5694),100));
		comlinkrec.payamnt.set(wsaaRenComm);
		comlinkrec.erndamt.set(wsaaRenComm);
	}

protected void getT5692Value3500()
	{
		try {
			para3500();
		}
		catch (GOTOException e){
		}
	}

protected void para3500()
	{
		wsaaReprempc.set(100);
		wsaaRwcmrate.set(100);
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5692rec.age01)) {
			wsaaRwcmrate.set(t5692rec.rwcmrate01);
			wsaaReprempc.set(t5692rec.reprempc01);
			goTo(GotoLabel.exit3590);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5692rec.age02)) {
			wsaaRwcmrate.set(t5692rec.rwcmrate02);
			wsaaReprempc.set(t5692rec.reprempc02);
			goTo(GotoLabel.exit3590);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5692rec.age03)) {
			wsaaRwcmrate.set(t5692rec.rwcmrate03);
			wsaaReprempc.set(t5692rec.reprempc03);
			goTo(GotoLabel.exit3590);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5692rec.age04)) {
			wsaaRwcmrate.set(t5692rec.rwcmrate04);
			wsaaReprempc.set(t5692rec.reprempc04);
			goTo(GotoLabel.exit3590);
		}
	}

protected void getT56944000()
	{
		/*PARA*/
		if (isLTE(wsaaTerm,t5694rec.toYear[wsaaIndex.toInt()])) {
			wsaaT5694.set(t5694rec.instalpc[wsaaIndex.toInt()]);
			wsaaIndex.set(13);
		}
		/*EXIT*/
	}

protected void systemError8000()
	{
		try {
			se8000();
		}
		catch (GOTOException e){
		}
		finally{
			seExit8090();
		}
	}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit8090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		comlinkrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
		try {
			db8100();
		}
		catch (GOTOException e){
		}
		finally{
			dbExit8190();
		}
	}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit8190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		comlinkrec.statuz.set(varcom.bomb);
		exit090();
	}
}
