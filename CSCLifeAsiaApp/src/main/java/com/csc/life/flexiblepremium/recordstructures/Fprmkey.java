package com.csc.life.flexiblepremium.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:43
 * Description:
 * Copybook name: FPRMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Fprmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData fprmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData fprmKey = new FixedLengthStringData(64).isAPartOf(fprmFileKey, 0, REDEFINE);
  	public FixedLengthStringData fprmChdrcoy = new FixedLengthStringData(1).isAPartOf(fprmKey, 0);
  	public FixedLengthStringData fprmChdrnum = new FixedLengthStringData(8).isAPartOf(fprmKey, 1);
  	public PackedDecimalData fprmPayrseqno = new PackedDecimalData(1, 0).isAPartOf(fprmKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(54).isAPartOf(fprmKey, 10, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(fprmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		fprmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}