/*
 * File: B6684.java
 * Date: 29 August 2009 21:23:36
 * Author: Quipoz Limited
 * 
 * Class transformed from B6684.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.flexiblepremium.dataaccess.FrepTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*  FREP DELETION PRIOR TO COLLECTIONS/ANNIVERSARY/OVERDUE
*  ======================================================
*
*  OVERVIEW.
*
*  This program must run before the Anniversary extract
*  processing program, ie. at the beginning of the
*  Renewals batch suite.
*
*  It will delete all the FREPs for the contract range input.
*  If no range is entered, all records will be deleted.
*
*  Control totals used in this program:
*
*  01 - No. of FREP recs read
*  02 - No. of FREP recs deleted
*
*****************************************************************
* </pre>
*/
public class B6684 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B6684");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/* ERRORS */
	private static final String ivrm = "IVRM";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private FrepTableDAM frepIO = new FrepTableDAM();
	private P6671par p6671par = new P6671par();

	public B6684() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		frepIO.setDataArea(SPACES);
		frepIO.setStatuz(varcom.oK);
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		frepIO.setCompany(bsprIO.getCompany());
		if (isEQ(p6671par.chdrnum, SPACES)) {
			p6671par.chdrnum.set(LOVALUE);
		}
		if (isEQ(p6671par.chdrnum1, SPACES)) {
			p6671par.chdrnum1.set(HIVALUE);
		}
		frepIO.setChdrnum(p6671par.chdrnum);
		frepIO.setPlanSuffix(ZERO);
		frepIO.setFunction(varcom.begnh);
	}

protected void readFile2000()
	{
		readFile2010();
	}

protected void readFile2010()
	{
		SmartFileCode.execute(appVars, frepIO);
		if (isNE(frepIO.getStatuz(), varcom.oK)
		&& isNE(frepIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(frepIO.getParams());
			fatalError600();
		}
		if (isNE(frepIO.getCompany(), bsprIO.getCompany())
		|| isGT(frepIO.getChdrnum(), p6671par.chdrnum1)) {
			frepIO.setFunction("REWRT");
			SmartFileCode.execute(appVars, frepIO);
			if (isNE(frepIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(frepIO.getParams());
				fatalError600();
			}
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isEQ(frepIO.getStatuz(), varcom.endp)) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		/*    Log number of FREP records read.*/
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		frepIO.setStatuz(varcom.oK);
		frepIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, frepIO);
		if (isNE(frepIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(frepIO.getParams());
			fatalError600();
		}
		/*    Log number of FREP records deleted*/
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		frepIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLL*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
