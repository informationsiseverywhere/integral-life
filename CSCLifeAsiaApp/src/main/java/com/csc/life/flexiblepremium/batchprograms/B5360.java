/*
 * File: B5360.java
 * Date: 29 August 2009 21:11:49
 * Author: Quipoz Limited
 * 
 * Class transformed from B5360.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.flexiblepremium.dataaccess.FpcopfTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FpcxpfTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* This program is the Splitter program for Anniversary Processing
* of Flexible Premium Contracts to be run before the Anniversary
* Processing batch job in multi-thread.
*
* The FPCOPF will be read via SQL with the following criteria:-
* i   Validflag       = '1'
* ii  Ann-process-ind not = 'Y'
* iii Targto         <= BSSC-EFFECTIVE-DATE + max lead days from
*                                                           T6654
*
* Control totals used in this program:
*
*    01  -  No. of thread members
*    02  -  No. of FPCO extracted records
*
* Splitter programs should be simple. They should only deal with
* a single file. If there is insufficient data on the primary file
* to completely identify a transaction then the further editing
* should be done in the subsequent process. The objective of a
* Splitter is to rapidly isolate potential transactions, not
* to process the transaction.   The primary concern for Splitter
* program design is to minimise elapsed time and this is achieved
* by minimising disk IO.
*
* Before the Splitter process is invoked, the program CRTTMPF
* should be run.  CRTTMPF (Create Temporary File) will create
* a duplicate of a physical file (created under Smart and defined
* as a Query file) which will have as many members as are defined
* in that process's 'subsequent threads' field. The temporary file
* will have the name XXXXDD9999 where XXXX is the first four
* characters of the file which was duplicated, DD is the first two
* characters of the 4th system parameter and 9999 is the schedule
* run number. Each member added to the temporary file will be
* called THREAD999 where 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read records from a primary file
* and, for each record selected,  it will write a record to a
* temporary file member. Exactly which temporary file member
* is written to is decided by a working storage count. The
* objective of the count is to spread the transaction records
* evenly amongst each thread member.
*
* The Splitter program should always have a restart method of
* '1' indicating a re-run. In the event of a restart, the Splitter
* program will always start with empty file members. Apart from
* writing records to the temporary file members, Splitter programs
* should not be doing any further updates.
*
*
* Notes for programs which use Splitter Program Temporary Files.
* --------------------------------------------------------------
*
* The Multi Thread Batch Environment will be able to submit multiple
* versions of the program which use the file members created from
* this program. It is important that the process which runs the
* splitter program has the same 'number of subsequent threads' as
* the following process has defined in 'number of threads this
* process'.
*
* The subsequent program need only point itself to one member
* based around the field BSPR-SCHEDULE-THREAD-NO which indicates
* in which thread the copy of the subsequent program is running.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class B5360 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlfpcoCursorrs = null;
	private java.sql.PreparedStatement sqlfpcoCursorps = null;
	private java.sql.Connection sqlfpcoCursorconn = null;
	private String sqlfpcoCursor = "";
	private int fpcoCursorLoopIndex = 0;
	private FpcxpfTableDAM fpcxpf = new FpcxpfTableDAM();
	private DiskFileDAM fpcx01 = new DiskFileDAM("FPCX01");
	private DiskFileDAM fpcx02 = new DiskFileDAM("FPCX02");
	private DiskFileDAM fpcx03 = new DiskFileDAM("FPCX03");
	private DiskFileDAM fpcx04 = new DiskFileDAM("FPCX04");
	private DiskFileDAM fpcx05 = new DiskFileDAM("FPCX05");
	private DiskFileDAM fpcx06 = new DiskFileDAM("FPCX06");
	private DiskFileDAM fpcx07 = new DiskFileDAM("FPCX07");
	private DiskFileDAM fpcx08 = new DiskFileDAM("FPCX08");
	private DiskFileDAM fpcx09 = new DiskFileDAM("FPCX09");
	private DiskFileDAM fpcx10 = new DiskFileDAM("FPCX10");
	private DiskFileDAM fpcx11 = new DiskFileDAM("FPCX11");
	private DiskFileDAM fpcx12 = new DiskFileDAM("FPCX12");
	private DiskFileDAM fpcx13 = new DiskFileDAM("FPCX13");
	private DiskFileDAM fpcx14 = new DiskFileDAM("FPCX14");
	private DiskFileDAM fpcx15 = new DiskFileDAM("FPCX15");
	private DiskFileDAM fpcx16 = new DiskFileDAM("FPCX16");
	private DiskFileDAM fpcx17 = new DiskFileDAM("FPCX17");
	private DiskFileDAM fpcx18 = new DiskFileDAM("FPCX18");
	private DiskFileDAM fpcx19 = new DiskFileDAM("FPCX19");
	private DiskFileDAM fpcx20 = new DiskFileDAM("FPCX20");
	private FpcxpfTableDAM fpcxpfData = new FpcxpfTableDAM();
	private FixedLengthStringData fpcx01Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx02Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx03Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx04Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx05Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx06Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx07Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx08Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx09Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx10Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx11Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx12Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx13Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx14Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx15Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx16Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx17Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx18Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx19Rec = new FixedLengthStringData(86);
	private FixedLengthStringData fpcx20Rec = new FixedLengthStringData(86);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5360");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFpcxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaFpcxFn, 0, FILLER).init("FPCX");
	private FixedLengthStringData wsaaFpcxRunid = new FixedLengthStringData(2).isAPartOf(wsaaFpcxFn, 4);
	private ZonedDecimalData wsaaFpcxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaFpcxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaa1 = new FixedLengthStringData(1).init("1");
	private FixedLengthStringData wsaaY = new FixedLengthStringData(1).init("Y");
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
	private PackedDecimalData wsaaBillUpToDate = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaMaxLeadDays = new ZonedDecimalData(3, 0).init(ZERO);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");
	private FixedLengthStringData wsaaClrtmpfError = new FixedLengthStringData(97);
		/*  Pointers to point to the member to read (IY) and write (IZ).*/
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();

		/*  SQL error message formatting.*/
	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
		/*  Define the number of rows in the block to be fetched.*/
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);

		/* WSAA-IND-ARRAY */
	private FixedLengthStringData[] wsaaFpcoInd = FLSInittedArray (1000, 12);
	private BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(6, 4, 0, wsaaFpcoInd, 0);
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";
		/* TABLES */
	private static final String t6654 = "T6654";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
		/*Contract Header Life Fields*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private FpcopfTableDAM fpcopfData = new FpcopfTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private T6654rec t6654rec = new T6654rec();
	private P6671par p6671par = new P6671par();
	private WsaaFetchArrayInner wsaaFetchArrayInner = new WsaaFetchArrayInner();

	public B5360() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		wsaaFpcxRunid.set(bprdIO.getSystemParam04());
		wsaaCompany.set(bprdIO.getCompany());
		wsaaFpcxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum,SPACES)
		&& isEQ(p6671par.chdrnum1,SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t6654);
		itemIO.setItemitem(SPACES);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		while ( !(isEQ(itemIO.getStatuz(),varcom.endp))) {
			searchT66541200();
		}
		
		datcon2rec.freqFactor.set(wsaaMaxLeadDays);
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError600();
		}
		wsaaBillUpToDate.set(datcon2rec.intDate2);
		iy.set(1);
		sqlfpcoCursor = " SELECT  CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, VALIDFLAG, CURRFROM, CURRTO, PRMPER, PRMRCDP, ACTIND, BILLEDP, OVRMINREQ, MINOVRPRO, ANPROIND, TARGFROM, TARGTO, CBANPR" +
" FROM   " + getAppVars().getTableNameOverriden("FPCOPF") + " " +
" WHERE VALIDFLAG = ?" +
" AND ANPROIND <> ?" +
" AND CBANPR <= ?" +
" AND CHDRNUM >= ?" +
" AND CHDRNUM <= ?" +
" AND CHDRCOY = ?" +
" ORDER BY CHDRNUM, COVERAGE";
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		sqlerrorflag = false;
		try {
			sqlfpcoCursorconn = getAppVars().getDBConnectionForTable(new com.csc.life.flexiblepremium.dataaccess.FpcopfTableDAM());
			sqlfpcoCursorps = getAppVars().prepareStatementEmbeded(sqlfpcoCursorconn, sqlfpcoCursor, "FPCOPF");
			getAppVars().setDBString(sqlfpcoCursorps, 1, wsaa1);
			getAppVars().setDBString(sqlfpcoCursorps, 2, wsaaY);
			getAppVars().setDBNumber(sqlfpcoCursorps, 3, wsaaBillUpToDate);
			getAppVars().setDBString(sqlfpcoCursorps, 4, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlfpcoCursorps, 5, wsaaChdrnumTo);
			getAppVars().setDBString(sqlfpcoCursorps, 6, wsaaCompany);
			sqlfpcoCursorrs = getAppVars().executeQuery(sqlfpcoCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

protected void openThreadMember1110()
	{
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(),1),iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaFpcxFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz,varcom.oK)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression("CLRTMPF", SPACES);
			stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
			stringVariable1.addExpression(SPACES);
			stringVariable1.addExpression(wsaaFpcxFn);
			stringVariable1.addExpression(SPACES);
			stringVariable1.addExpression(wsaaThreadMember);
			stringVariable1.setStringInto(wsaaClrtmpfError);
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.params.set(wsaaClrtmpfError);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression("OVRDBF FILE(FPCX");
		stringVariable2.addExpression(iz);
		stringVariable2.addExpression(") TOFILE(");
		stringVariable2.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable2.addExpression("/");
		stringVariable2.addExpression(wsaaFpcxFn);
		stringVariable2.addExpression(") MBR(");
		stringVariable2.addExpression(wsaaThreadMember);
		stringVariable2.addExpression(")");
		stringVariable2.addExpression(" SEQONLY(*YES 1000)");
		stringVariable2.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		if (isEQ(iz,1)) {
			fpcx01.openOutput();
		}
		if (isEQ(iz,2)) {
			fpcx02.openOutput();
		}
		if (isEQ(iz,3)) {
			fpcx03.openOutput();
		}
		if (isEQ(iz,4)) {
			fpcx04.openOutput();
		}
		if (isEQ(iz,5)) {
			fpcx05.openOutput();
		}
		if (isEQ(iz,6)) {
			fpcx06.openOutput();
		}
		if (isEQ(iz,7)) {
			fpcx07.openOutput();
		}
		if (isEQ(iz,8)) {
			fpcx08.openOutput();
		}
		if (isEQ(iz,9)) {
			fpcx09.openOutput();
		}
		if (isEQ(iz,10)) {
			fpcx10.openOutput();
		}
		if (isEQ(iz,11)) {
			fpcx11.openOutput();
		}
		if (isEQ(iz,12)) {
			fpcx12.openOutput();
		}
		if (isEQ(iz,13)) {
			fpcx13.openOutput();
		}
		if (isEQ(iz,14)) {
			fpcx14.openOutput();
		}
		if (isEQ(iz,15)) {
			fpcx15.openOutput();
		}
		if (isEQ(iz,16)) {
			fpcx16.openOutput();
		}
		if (isEQ(iz,17)) {
			fpcx17.openOutput();
		}
		if (isEQ(iz,18)) {
			fpcx18.openOutput();
		}
		if (isEQ(iz,19)) {
			fpcx19.openOutput();
		}
		if (isEQ(iz,20)) {
			fpcx20.openOutput();
		}
	}

protected void searchT66541200()
	{
			start1210();
		}

protected void start1210()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.endp)
		|| isNE(itemIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itemIO.getItemtabl(),t6654)) {
			itemIO.setStatuz(varcom.endp);
			return ;
		}
		t6654rec.t6654Rec.set(itemIO.getGenarea());
		if (isGT(t6654rec.leadDays,wsaaMaxLeadDays)) {
			wsaaMaxLeadDays.set(t6654rec.leadDays);
		}
		itemIO.setFunction(varcom.nextr);
	}

protected void initialiseArray1500()
	{
		/*START*/
		wsaaFetchArrayInner.wsaaCurrfrom[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaCurrto[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaPrmper[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaPlnsfx[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaPrmrcdp[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaValidflag[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaBilledp[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaOvrminreq[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaMinovrpro[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaTargfrom[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaCbanpr[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaTargto[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaChdrcoy[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaLife[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaJlife[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaCoverage[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaRider[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaActind[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaAnproind[wsaaInd.toInt()].set(SPACES);
		/*EXIT*/
	}

protected void readFile2000()
	{
			readFiles2010();
		}

protected void readFiles2010()
	{
		/*  We only need to FETCH records when a full block has been*/
		/*  processed or if the end of file was retrieved in the last*/
		/*  block.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd,1)) {
			return ;
		}
		sqlerrorflag = false;
		try {
			for (fpcoCursorLoopIndex = 1; isLT(fpcoCursorLoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlfpcoCursorrs); fpcoCursorLoopIndex++ ){
				getAppVars().getDBObject(sqlfpcoCursorrs, 1, wsaaFetchArrayInner.wsaaChdrcoy[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 2, wsaaFetchArrayInner.wsaaChdrnum[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 3, wsaaFetchArrayInner.wsaaLife[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 4, wsaaFetchArrayInner.wsaaJlife[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 5, wsaaFetchArrayInner.wsaaCoverage[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 6, wsaaFetchArrayInner.wsaaRider[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 7, wsaaFetchArrayInner.wsaaPlnsfx[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 8, wsaaFetchArrayInner.wsaaValidflag[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 9, wsaaFetchArrayInner.wsaaCurrfrom[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 10, wsaaFetchArrayInner.wsaaCurrto[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 11, wsaaFetchArrayInner.wsaaPrmper[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 12, wsaaFetchArrayInner.wsaaPrmrcdp[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 13, wsaaFetchArrayInner.wsaaActind[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 14, wsaaFetchArrayInner.wsaaBilledp[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 15, wsaaFetchArrayInner.wsaaOvrminreq[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 16, wsaaFetchArrayInner.wsaaMinovrpro[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 17, wsaaFetchArrayInner.wsaaAnproind[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 18, wsaaFetchArrayInner.wsaaTargfrom[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 19, wsaaFetchArrayInner.wsaaTargto[fpcoCursorLoopIndex]);
				getAppVars().getDBObject(sqlfpcoCursorrs, 20, wsaaFetchArrayInner.wsaaCbanpr[fpcoCursorLoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*  An SQLCODE = +100 is returned when :-*/
		/*      a) no rows are returned on the first fetch*/
		/*      b) the last row of the cursor is either in the block or is*/
		/*         the last row of the block.*/
		/*  We must continue processing to the 3000- section for case b)*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(varcom.endp);
		} else if (isEQ(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], SPACES)){
			wsspEdterror.set(varcom.endp);
		}
		else {
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}
	}

protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		wsaaInd.set(1);
		/*   PERFORM 3200-LOAD-THREADS                                    */
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		/*  Re-initialise the block for next fetch and point to first row.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*  If the the CHDRNUM being processed is not equal to the         */
		/*  to the previous we should move to the next output file member. */
		/*  The condition is here to allow for checking the last chdrnum   */
		/*  of the old block with the first of the new and to move to the  */
		/*  next file member to write to if they have changed.             */
		if (isNE(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)) {
			wsaaPrevChdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
			iy.add(1);
		}
		/*  Load from storage all FPCX data for the same contract until    */
		/*  the CHDRNUM on FPCX has changed,                               */
		/*    OR                                                           */
		/*  The end of an incomplete block is reached.                     */
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| isNE(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}

	/**
	* <pre>
	*3200-LOAD-THREADS SECTION.                                       
	* </pre>
	*/
protected void loadSameContracts3200()
	{
		start3200();
	}

protected void start3200()
	{
		fpcxpfData.chdrcoy.set(wsaaFetchArrayInner.wsaaChdrcoy[wsaaInd.toInt()]);
		fpcxpfData.chdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
		fpcxpfData.life.set(wsaaFetchArrayInner.wsaaLife[wsaaInd.toInt()]);
		fpcxpfData.jlife.set(wsaaFetchArrayInner.wsaaJlife[wsaaInd.toInt()]);
		fpcxpfData.coverage.set(wsaaFetchArrayInner.wsaaCoverage[wsaaInd.toInt()]);
		fpcxpfData.rider.set(wsaaFetchArrayInner.wsaaRider[wsaaInd.toInt()]);
		fpcxpfData.planSuffix.set(wsaaFetchArrayInner.wsaaPlnsfx[wsaaInd.toInt()]);
		fpcxpfData.validflag.set(wsaaFetchArrayInner.wsaaValidflag[wsaaInd.toInt()]);
		fpcxpfData.currfrom.set(wsaaFetchArrayInner.wsaaCurrfrom[wsaaInd.toInt()]);
		fpcxpfData.currto.set(wsaaFetchArrayInner.wsaaCurrto[wsaaInd.toInt()]);
		fpcxpfData.targetPremium.set(wsaaFetchArrayInner.wsaaPrmper[wsaaInd.toInt()]);
		fpcxpfData.premRecPer.set(wsaaFetchArrayInner.wsaaPrmrcdp[wsaaInd.toInt()]);
		fpcxpfData.activeInd.set(wsaaFetchArrayInner.wsaaActind[wsaaInd.toInt()]);
		fpcxpfData.billedInPeriod.set(wsaaFetchArrayInner.wsaaBilledp[wsaaInd.toInt()]);
		fpcxpfData.overdueMin.set(wsaaFetchArrayInner.wsaaOvrminreq[wsaaInd.toInt()]);
		fpcxpfData.minOverduePer.set(wsaaFetchArrayInner.wsaaMinovrpro[wsaaInd.toInt()]);
		fpcxpfData.annProcessInd.set(wsaaFetchArrayInner.wsaaAnproind[wsaaInd.toInt()]);
		fpcxpfData.targfrom.set(wsaaFetchArrayInner.wsaaTargfrom[wsaaInd.toInt()]);
		fpcxpfData.targto.set(wsaaFetchArrayInner.wsaaTargto[wsaaInd.toInt()]);
		fpcxpfData.annivProcDate.set(wsaaFetchArrayInner.wsaaCbanpr[wsaaInd.toInt()]);
		if (isGT(iy,bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		if (isEQ(iy,1)) {
			fpcx01.write(fpcxpfData);
		}
		if (isEQ(iy,2)) {
			fpcx02.write(fpcxpfData);
		}
		if (isEQ(iy,3)) {
			fpcx03.write(fpcxpfData);
		}
		if (isEQ(iy,4)) {
			fpcx04.write(fpcxpfData);
		}
		if (isEQ(iy,5)) {
			fpcx05.write(fpcxpfData);
		}
		if (isEQ(iy,6)) {
			fpcx06.write(fpcxpfData);
		}
		if (isEQ(iy,7)) {
			fpcx07.write(fpcxpfData);
		}
		if (isEQ(iy,8)) {
			fpcx08.write(fpcxpfData);
		}
		if (isEQ(iy,9)) {
			fpcx09.write(fpcxpfData);
		}
		if (isEQ(iy,10)) {
			fpcx10.write(fpcxpfData);
		}
		if (isEQ(iy,11)) {
			fpcx11.write(fpcxpfData);
		}
		if (isEQ(iy,12)) {
			fpcx12.write(fpcxpfData);
		}
		if (isEQ(iy,13)) {
			fpcx13.write(fpcxpfData);
		}
		if (isEQ(iy,14)) {
			fpcx14.write(fpcxpfData);
		}
		if (isEQ(iy,15)) {
			fpcx15.write(fpcxpfData);
		}
		if (isEQ(iy,16)) {
			fpcx16.write(fpcxpfData);
		}
		if (isEQ(iy,17)) {
			fpcx17.write(fpcxpfData);
		}
		if (isEQ(iy,18)) {
			fpcx18.write(fpcxpfData);
		}
		if (isEQ(iy,19)) {
			fpcx19.write(fpcxpfData);
		}
		if (isEQ(iy,20)) {
			fpcx20.write(fpcxpfData);
		}
		/*    Log the number of extracted records.*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
		/*  Set up the array for the next block of records.*/
		/*  Otherwise process the next record in the array.*/
		wsaaInd.add(1);
		/*  Check for an incomplete block retrieved.*/	
		if (isLTE(wsaaInd,wsaaRowsInBlock) && isEQ(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()],SPACES)) {
			wsaaEofInBlock.set("Y");
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/**    There is no pre-commit processing to be done*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close the open files and remove the overrides.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlfpcoCursorconn, sqlfpcoCursorps, sqlfpcoCursorrs);
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		close4110();
	}

protected void close4110()
	{
		if (isEQ(iz,1)) {
			fpcx01.close();
		}
		if (isEQ(iz,2)) {
			fpcx02.close();
		}
		if (isEQ(iz,3)) {
			fpcx03.close();
		}
		if (isEQ(iz,4)) {
			fpcx04.close();
		}
		if (isEQ(iz,5)) {
			fpcx05.close();
		}
		if (isEQ(iz,6)) {
			fpcx06.close();
		}
		if (isEQ(iz,7)) {
			fpcx07.close();
		}
		if (isEQ(iz,8)) {
			fpcx08.close();
		}
		if (isEQ(iz,9)) {
			fpcx09.close();
		}
		if (isEQ(iz,10)) {
			fpcx10.close();
		}
		if (isEQ(iz,11)) {
			fpcx11.close();
		}
		if (isEQ(iz,12)) {
			fpcx12.close();
		}
		if (isEQ(iz,13)) {
			fpcx13.close();
		}
		if (isEQ(iz,14)) {
			fpcx14.close();
		}
		if (isEQ(iz,15)) {
			fpcx15.close();
		}
		if (isEQ(iz,16)) {
			fpcx16.close();
		}
		if (isEQ(iz,17)) {
			fpcx17.close();
		}
		if (isEQ(iz,18)) {
			fpcx18.close();
		}
		if (isEQ(iz,19)) {
			fpcx19.close();
		}
		if (isEQ(iz,20)) {
			fpcx20.close();
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure WSAA-FETCH-ARRAY--INNER
 */
private static final class WsaaFetchArrayInner { 

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaFpcoData = FLSInittedArray (1000, 86);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaFpcoData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaFpcoData, 1);
	private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaFpcoData, 9);
	private FixedLengthStringData[] wsaaJlife = FLSDArrayPartOfArrayStructure(2, wsaaFpcoData, 11);
	private FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaFpcoData, 13);
	private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaFpcoData, 15);
	private PackedDecimalData[] wsaaPlnsfx = PDArrayPartOfArrayStructure(4, 0, wsaaFpcoData, 17);
	private FixedLengthStringData[] wsaaValidflag = FLSDArrayPartOfArrayStructure(1, wsaaFpcoData, 20);
	private PackedDecimalData[] wsaaCurrfrom = PDArrayPartOfArrayStructure(8, 0, wsaaFpcoData, 21);
	private PackedDecimalData[] wsaaCurrto = PDArrayPartOfArrayStructure(8, 0, wsaaFpcoData, 26);
	private PackedDecimalData[] wsaaPrmper = PDArrayPartOfArrayStructure(17, 2, wsaaFpcoData, 31);
	private PackedDecimalData[] wsaaPrmrcdp = PDArrayPartOfArrayStructure(17, 2, wsaaFpcoData, 40);
	private FixedLengthStringData[] wsaaActind = FLSDArrayPartOfArrayStructure(1, wsaaFpcoData, 49);
	private PackedDecimalData[] wsaaBilledp = PDArrayPartOfArrayStructure(17, 2, wsaaFpcoData, 50);
	private PackedDecimalData[] wsaaOvrminreq = PDArrayPartOfArrayStructure(17, 2, wsaaFpcoData, 59);
	private PackedDecimalData[] wsaaMinovrpro = PDArrayPartOfArrayStructure(3, 0, wsaaFpcoData, 68);
	private FixedLengthStringData[] wsaaAnproind = FLSDArrayPartOfArrayStructure(1, wsaaFpcoData, 70);
	private PackedDecimalData[] wsaaTargfrom = PDArrayPartOfArrayStructure(8, 0, wsaaFpcoData, 71);
	private PackedDecimalData[] wsaaTargto = PDArrayPartOfArrayStructure(8, 0, wsaaFpcoData, 76);
	private PackedDecimalData[] wsaaCbanpr = PDArrayPartOfArrayStructure(8, 0, wsaaFpcoData, 81);
}
}
