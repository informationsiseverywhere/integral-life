/******************************************************************************
 * File Name 		: FpcopfDAOImpl.java
 * Author			: smalchi2
 * Creation Date	: 29 December 2016
 * Project			: Integral Life
 * Description		: The DAO Implementation Class for FPCOPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.flexiblepremium.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.csc.life.flexiblepremium.dataaccess.dao.FpcopfDAO;
import com.csc.life.flexiblepremium.dataaccess.model.Fpcopf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class FpcopfDAOImpl extends BaseDAOImpl<Fpcopf> implements FpcopfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(FpcopfDAOImpl.class);

	public List<Fpcopf> searchFpcopfRecord(Fpcopf fpcopf) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlSelect.append("RIDER, PLNSFX, VALIDFLAG, CURRFROM, CURRTO, ");
		sqlSelect.append("PRMPER, PRMRCDP, BILLEDP, OVRMINREQ, MINOVRPRO, ");
		sqlSelect.append("TRANNO, ACTIND, ANPROIND, TARGFROM, TARGTO, ");
		sqlSelect.append("EFFDATE, USRPRF, JOBNM, DATIME, CBANPR ");		
		sqlSelect.append("FROM FPCOPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? ");
		sqlSelect.append("AND PLNSFX = ? ");//IJTI-1726
		sqlSelect.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, TARGFROM ASC, UNIQUE_NUMBER DESC ");


		PreparedStatement psFpcopfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlFpcopfRs = null;
		List<Fpcopf> outputList = new ArrayList<>();//IJTI-1726

		try {

			psFpcopfSelect.setString(1, fpcopf.getChdrcoy());
			psFpcopfSelect.setString(2, fpcopf.getChdrnum());
			psFpcopfSelect.setString(3, fpcopf.getLife());
			psFpcopfSelect.setString(4, fpcopf.getCoverage());
			psFpcopfSelect.setString(5, fpcopf.getRider());
			psFpcopfSelect.setInt(6, fpcopf.getPlnsfx());

			sqlFpcopfRs = executeQuery(psFpcopfSelect);

			while (sqlFpcopfRs.next()) {

				Fpcopf fpcopfnew = new Fpcopf();

				fpcopfnew.setUniqueNumber(sqlFpcopfRs.getInt("UNIQUE_NUMBER"));
				fpcopfnew.setChdrcoy(sqlFpcopfRs.getString("CHDRCOY"));
				fpcopfnew.setChdrnum(sqlFpcopfRs.getString("CHDRNUM"));
				fpcopfnew.setLife(sqlFpcopfRs.getString("LIFE"));
				fpcopfnew.setJlife(sqlFpcopfRs.getString("JLIFE"));
				fpcopfnew.setCoverage(sqlFpcopfRs.getString("COVERAGE"));
				fpcopfnew.setRider(sqlFpcopfRs.getString("RIDER"));
				fpcopfnew.setPlnsfx(sqlFpcopfRs.getInt("PLNSFX"));
				
				fpcopfnew.setValidflag(sqlFpcopfRs.getString("VALIDFLAG"));
				fpcopfnew.setCurrfrom(sqlFpcopfRs.getInt("CURRFROM"));
				fpcopfnew.setCurrto(sqlFpcopfRs.getInt("CURRTO"));
				fpcopfnew.setPrmper(sqlFpcopfRs.getBigDecimal("PRMPER"));
				fpcopfnew.setPrmrcdp(sqlFpcopfRs.getBigDecimal("PRMRCDP"));
				fpcopfnew.setBilledp(sqlFpcopfRs.getBigDecimal("BILLEDP"));
				fpcopfnew.setOvrminreq(sqlFpcopfRs.getBigDecimal("OVRMINREQ"));
				fpcopfnew.setMinovrpro(sqlFpcopfRs.getInt("MINOVRPRO"));
				fpcopfnew.setTranno(sqlFpcopfRs.getInt("TRANNO"));				
				fpcopfnew.setActind(sqlFpcopfRs.getString("ACTIND"));
				fpcopfnew.setAnproind(sqlFpcopfRs.getString("ANPROIND"));
				fpcopfnew.setTargfrom(sqlFpcopfRs.getInt("TARGFROM"));
				fpcopfnew.setTargto(sqlFpcopfRs.getInt("TARGTO"));
				fpcopfnew.setEffdate(sqlFpcopfRs.getInt("EFFDATE"));
				
				fpcopf.setUsrprf(sqlFpcopfRs.getString("USRPRF"));
				fpcopf.setJobnm(sqlFpcopfRs.getString("JOBNM"));
				fpcopf.setDatime(sqlFpcopfRs.getDate("DATIME"));
				
				fpcopfnew.setCbanpr(sqlFpcopfRs.getInt("CBANPR"));			

				outputList.add(fpcopfnew);
			}

		} catch (SQLException e) {
			LOGGER.error("searchFpcopfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psFpcopfSelect, sqlFpcopfRs);
		}

		return outputList;
	}
	
	public void InsertFpcopfRecord(Fpcopf fpcopf) throws SQLRuntimeException
	{
		StringBuilder sqlInsert = new StringBuilder();
		sqlInsert.append("INSERT INTO FPCOPF (");		
		sqlInsert.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlInsert.append("RIDER, PLNSFX, VALIDFLAG, CURRFROM, CURRTO, ");
		sqlInsert.append("PRMPER, PRMRCDP, BILLEDP, OVRMINREQ, MINOVRPRO, ");
		sqlInsert.append("TRANNO, ACTIND, ANPROIND, TARGFROM, TARGTO, ");
		sqlInsert.append("EFFDATE, USRPRF, JOBNM, DATIME, CBANPR ");
		sqlInsert.append(") VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		
		PreparedStatement psFpcopfInsert = getPrepareStatement(sqlInsert.toString());
		int index = 0;
		try {

			psFpcopfInsert.setString(index++, fpcopf.getChdrcoy());
			psFpcopfInsert.setString(index++, fpcopf.getChdrnum());
			psFpcopfInsert.setString(index++, fpcopf.getLife());
			psFpcopfInsert.setString(index++, fpcopf.getJlife());
			psFpcopfInsert.setString(index++, fpcopf.getCoverage());
			psFpcopfInsert.setString(index++, fpcopf.getRider());
			psFpcopfInsert.setInt(index++, fpcopf.getPlnsfx());
			
			psFpcopfInsert.setString(index++, fpcopf.getValidflag());
			psFpcopfInsert.setInt(index++, fpcopf.getCurrfrom());
			psFpcopfInsert.setInt(index++, fpcopf.getCurrto());
			psFpcopfInsert.setBigDecimal(index++, fpcopf.getPrmper());
			psFpcopfInsert.setBigDecimal(index++, fpcopf.getPrmrcdp());
			psFpcopfInsert.setBigDecimal(index++, fpcopf.getBilledp());
			psFpcopfInsert.setBigDecimal(index++, fpcopf.getOvrminreq());
			psFpcopfInsert.setInt(index++, fpcopf.getMinovrpro());
			
			psFpcopfInsert.setInt(index++, fpcopf.getTranno());
			psFpcopfInsert.setString(index++, fpcopf.getActind());
			psFpcopfInsert.setString(index++, fpcopf.getAnproind());
			psFpcopfInsert.setInt(index++, fpcopf.getTargfrom());
			psFpcopfInsert.setInt(index++, fpcopf.getTargto());
			psFpcopfInsert.setInt(index++, fpcopf.getEffdate());
			psFpcopfInsert.setString(index++, fpcopf.getUsrprf());
			psFpcopfInsert.setString(index++, fpcopf.getJobnm());
			psFpcopfInsert.setDate(index++, fpcopf.getDatime());
			psFpcopfInsert.setInt(index++, fpcopf.getCbanpr());
			
			int affectedCount = executeUpdate(psFpcopfInsert);

			LOGGER.debug("insertIntoFpcopf {} rows inserted.", affectedCount);//IJTI-1561
		}
		catch (SQLException e) {
			LOGGER.error("insertIntoFpcopf()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psFpcopfInsert, null);
		}
		
	}

    public Map<String, List<Fpcopf>> getFpcoMap(String coy, List<String> chdrnumList) {
        StringBuilder sb = new StringBuilder("");
        sb.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, VALIDFLAG, CURRFROM, CURRTO, PRMPER, PRMRCDP, TRANNO, USRPRF, JOBNM, DATIME, ACTIND, BILLEDP, OVRMINREQ, MINOVRPRO, ANPROIND, TARGFROM, TARGTO, EFFDATE, CBANPR   ");
        sb.append("FROM VM1DTA.FPCOPF WHERE VALIDFLAG = '1' AND ACTIND = 'Y' AND CHDRCOY=? AND ");
        sb.append(getSqlInStr("CHDRNUM", chdrnumList));
        sb.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, TARGFROM, UNIQUE_NUMBER DESC ");        
        LOGGER.info(sb.toString());	
        PreparedStatement ps = getPrepareStatement(sb.toString());
        ResultSet rs = null;
        Map<String, List<Fpcopf>> fpcoMap = new HashMap<String, List<Fpcopf>>();
        try {
            ps.setInt(1, Integer.parseInt(coy));
            rs = executeQuery(ps);

            while (rs.next()) {
            	Fpcopf fpcopf = new Fpcopf();
                fpcopf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
                fpcopf.setChdrcoy(rs.getString("CHDRCOY"));
                fpcopf.setChdrnum(rs.getString("CHDRNUM"));
                fpcopf.setLife(rs.getString("LIFE"));
                fpcopf.setJlife(rs.getString("JLIFE"));                
                fpcopf.setCoverage(rs.getString("COVERAGE"));
                fpcopf.setRider(rs.getString("RIDER"));               
                fpcopf.setPlnsfx(rs.getInt("PLNSFX"));
                fpcopf.setValidflag(rs.getString("VALIDFLAG"));
                fpcopf.setCurrfrom(rs.getInt("CURRFROM"));
                fpcopf.setCurrto(rs.getInt("CURRTO"));
                fpcopf.setPrmper(rs.getBigDecimal("PRMPER"));                
                fpcopf.setPrmrcdp(rs.getBigDecimal("PRMRCDP"));     
                fpcopf.setTranno(rs.getInt("TRANNO"));
                fpcopf.setUsrprf(rs.getString("USRPRF"));
                fpcopf.setJobnm(rs.getString("JOBNM"));                
                fpcopf.setDatime(rs.getDate("DATIME")); 
                fpcopf.setActind(rs.getString("ACTIND"));
                fpcopf.setBilledp(rs.getBigDecimal("BILLEDP"));
                fpcopf.setOvrminreq(rs.getBigDecimal("OVRMINREQ"));
                fpcopf.setMinovrpro(rs.getInt("MINOVRPRO"));
                fpcopf.setAnproind(rs.getString("ANPROIND"));
                fpcopf.setTargfrom(rs.getInt("TARGFROM"));
                fpcopf.setTargto(rs.getInt("TARGTO"));                
                fpcopf.setEffdate(rs.getInt("EFFDATE"));         
                fpcopf.setCbanpr(rs.getInt("CBANPR"));

                if (fpcoMap.containsKey(fpcopf.getChdrnum())) {
                	fpcoMap.get(fpcopf.getChdrnum()).add(fpcopf);
                } else {
                    List<Fpcopf> fpcoList = new ArrayList<Fpcopf>();
                    fpcoList.add(fpcopf);
                    fpcoMap.put(fpcopf.getChdrnum(), fpcoList);
                }
            }

        } catch (SQLException e) {
            LOGGER.error("getFpcoMap()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return fpcoMap;

    }
    
	public boolean updateFpcoPF(List<Fpcopf> fpcoList){
		StringBuilder sb = new StringBuilder(" UPDATE FPCOPF SET BILLEDP= ?, OVRMINREQ=?, JOBNM=?,USRPRF=?,DATIME=? ");
		sb.append("WHERE UNIQUE_NUMBER=?");     
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean isUpdated = false;
		try{
			ps = getPrepareStatement(sb.toString());
			int ctr;
			for (Fpcopf fpco : fpcoList) {		
				ctr = 0;
				ps.setBigDecimal(++ctr, fpco.getBilledp());
				ps.setBigDecimal(++ctr, fpco.getOvrminreq());
				ps.setString(++ctr, getJobnm());
                ps.setString(++ctr, getUsrprf());
                ps.setTimestamp(++ctr, new Timestamp(System.currentTimeMillis()));	
			    ps.setLong(++ctr, fpco.getUniqueNumber());
			    ps.addBatch();	
			}			
			ps.executeBatch();
			isUpdated = true;
		}catch (SQLException e) {
			LOGGER.error("updateFpcoPF()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
		return isUpdated;
	}     
	
	public List<Fpcopf> searchFpcoRecord(String chdrcoy, String chdrnum){

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlSelect.append("RIDER, PLNSFX, VALIDFLAG, CURRFROM, CURRTO, ");
		sqlSelect.append("PRMPER, PRMRCDP, BILLEDP, OVRMINREQ, MINOVRPRO, ");
		sqlSelect.append("TRANNO, ACTIND, ANPROIND, TARGFROM, TARGTO, ");
		sqlSelect.append("EFFDATE, USRPRF, JOBNM, DATIME, CBANPR ");		
		sqlSelect.append("FROM FPCO WHERE CHDRCOY = ? AND CHDRNUM = ? ");
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, TARGFROM ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement psFpcopfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlFpcopfRs = null;
		List<Fpcopf> outputList = new ArrayList<Fpcopf>();

		try {

			psFpcopfSelect.setString(1, chdrcoy);
			psFpcopfSelect.setString(2, chdrnum);

			sqlFpcopfRs = executeQuery(psFpcopfSelect);

			while (sqlFpcopfRs.next()) {

				Fpcopf fpcopfnew = new Fpcopf();

				fpcopfnew.setUniqueNumber(sqlFpcopfRs.getInt("UNIQUE_NUMBER"));
				fpcopfnew.setChdrcoy(sqlFpcopfRs.getString("CHDRCOY"));
				fpcopfnew.setChdrnum(sqlFpcopfRs.getString("CHDRNUM"));
				fpcopfnew.setLife(sqlFpcopfRs.getString("LIFE"));
				fpcopfnew.setJlife(sqlFpcopfRs.getString("JLIFE"));
				fpcopfnew.setCoverage(sqlFpcopfRs.getString("COVERAGE"));
				fpcopfnew.setRider(sqlFpcopfRs.getString("RIDER"));
				fpcopfnew.setPlnsfx(sqlFpcopfRs.getInt("PLNSFX"));
				fpcopfnew.setValidflag(sqlFpcopfRs.getString("VALIDFLAG"));
				fpcopfnew.setCurrfrom(sqlFpcopfRs.getInt("CURRFROM"));
				fpcopfnew.setCurrto(sqlFpcopfRs.getInt("CURRTO"));
				fpcopfnew.setPrmper(sqlFpcopfRs.getBigDecimal("PRMPER"));
				fpcopfnew.setPrmrcdp(sqlFpcopfRs.getBigDecimal("PRMRCDP"));
				fpcopfnew.setBilledp(sqlFpcopfRs.getBigDecimal("BILLEDP"));
				fpcopfnew.setOvrminreq(sqlFpcopfRs.getBigDecimal("OVRMINREQ"));
				fpcopfnew.setMinovrpro(sqlFpcopfRs.getInt("MINOVRPRO"));
				fpcopfnew.setTranno(sqlFpcopfRs.getInt("TRANNO"));				
				fpcopfnew.setActind(sqlFpcopfRs.getString("ACTIND"));
				fpcopfnew.setAnproind(sqlFpcopfRs.getString("ANPROIND"));
				fpcopfnew.setTargfrom(sqlFpcopfRs.getInt("TARGFROM"));
				fpcopfnew.setTargto(sqlFpcopfRs.getInt("TARGTO"));
				fpcopfnew.setEffdate(sqlFpcopfRs.getInt("EFFDATE"));
				fpcopfnew.setCbanpr(sqlFpcopfRs.getInt("CBANPR"));			

				outputList.add(fpcopfnew);
			}

		} catch (SQLException e) {
			LOGGER.error("searchFpcoRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psFpcopfSelect, sqlFpcopfRs);
		}

		return outputList;
	}
	
	public void insertFpcoRecord(List<Fpcopf> fpcopfList) {

		if (fpcopfList == null || fpcopfList.isEmpty()) {
			return;
		}
		StringBuilder sqlInsert = new StringBuilder();
		sqlInsert.append("INSERT INTO FPCOPF (");
		sqlInsert.append("CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, ");
		sqlInsert.append("RIDER, PLNSFX, VALIDFLAG, CURRFROM, CURRTO, ");
		sqlInsert.append("PRMPER, PRMRCDP, BILLEDP, OVRMINREQ, MINOVRPRO, ");
		sqlInsert.append("TRANNO, ACTIND, ANPROIND, TARGFROM, TARGTO, ");
		sqlInsert.append("EFFDATE, USRPRF, JOBNM, DATIME, CBANPR ");
		sqlInsert.append(") VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

		PreparedStatement psFpcopfInsert = getPrepareStatement(sqlInsert.toString());
		int index = 1;
		try {

			for (Fpcopf fpcopf : fpcopfList) {
				psFpcopfInsert.setString(index++, fpcopf.getChdrcoy());
				psFpcopfInsert.setString(index++, fpcopf.getChdrnum());
				psFpcopfInsert.setString(index++, fpcopf.getLife());
				psFpcopfInsert.setString(index++, fpcopf.getJlife());
				psFpcopfInsert.setString(index++, fpcopf.getCoverage());
				psFpcopfInsert.setString(index++, fpcopf.getRider());
				psFpcopfInsert.setInt(index++, fpcopf.getPlnsfx());

				psFpcopfInsert.setString(index++, fpcopf.getValidflag());
				psFpcopfInsert.setInt(index++, fpcopf.getCurrfrom());
				psFpcopfInsert.setInt(index++, fpcopf.getCurrto());
				psFpcopfInsert.setBigDecimal(index++, fpcopf.getPrmper());
				psFpcopfInsert.setBigDecimal(index++, fpcopf.getPrmrcdp());
				psFpcopfInsert.setBigDecimal(index++, fpcopf.getBilledp());
				psFpcopfInsert.setBigDecimal(index++, fpcopf.getOvrminreq());
				psFpcopfInsert.setInt(index++, fpcopf.getMinovrpro());

				psFpcopfInsert.setInt(index++, fpcopf.getTranno());
				psFpcopfInsert.setString(index++, fpcopf.getActind());
				psFpcopfInsert.setString(index++, fpcopf.getAnproind());
				psFpcopfInsert.setInt(index++, fpcopf.getTargfrom());
				psFpcopfInsert.setInt(index++, fpcopf.getTargto());
				psFpcopfInsert.setInt(index++, fpcopf.getEffdate());
				psFpcopfInsert.setString(index++, fpcopf.getUsrprf());
				psFpcopfInsert.setString(index++, fpcopf.getJobnm());
				psFpcopfInsert.setDate(index++, fpcopf.getDatime());
				psFpcopfInsert.setInt(index++, fpcopf.getCbanpr());
				psFpcopfInsert.addBatch();
				index = 0;
			}
			psFpcopfInsert.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertFpcoRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psFpcopfInsert, null);
		}
	}
	
    
	public void updateInvalidFpcoRecord(List<Fpcopf> fpcoList){
		String sb = " UPDATE FPCOPF SET VALIDFLAG= ?, CURRTO=?, JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";
		PreparedStatement ps = getPrepareStatement(sb);
		ResultSet rs = null;
		try{
			int ctr;
			for (Fpcopf fpco : fpcoList) {		
				ctr = 0;
				ps.setString(++ctr, fpco.getValidflag());
				ps.setInt(++ctr, fpco.getCurrto());
				ps.setString(++ctr, getJobnm());
                ps.setString(++ctr, getUsrprf());
                ps.setTimestamp(++ctr, new Timestamp(System.currentTimeMillis()));	
			    ps.setLong(++ctr, fpco.getUniqueNumber());
			    ps.addBatch();	
			}			
			ps.executeBatch();
		}catch (SQLException e) {
			LOGGER.error("updateInvalidFpcoRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
	}    
}
