/*
 * File: T5729pt.java
 * Date: 30 August 2009 2:27:04
 * Author: Quipoz Limited
 * 
 * Class transformed from T5729PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.flexiblepremium.tablestructures.T5729rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5729.
*
*
*****************************************************************
* </pre>
*/
public class T5729pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(49).isAPartOf(wsaaPrtLine001, 27, FILLER).init("Flexible Premium Variances                  S5729");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(67);
	private FixedLengthStringData filler9 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Frequency :");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 13);
	private FixedLengthStringData filler10 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine004, 15, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine004, 27, FILLER).init("Frequency :");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 39);
	private FixedLengthStringData filler12 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine004, 41, FILLER).init(SPACES);
	private FixedLengthStringData filler13 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine004, 53, FILLER).init("Frequency :");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 65);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(77);
	private FixedLengthStringData filler14 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler15 = new FixedLengthStringData(71).isAPartOf(wsaaPrtLine005, 6, FILLER).init("% of Target Overdue       % of Target Overdue       % of Target Overdue");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(76);
	private FixedLengthStringData filler16 = new FixedLengthStringData(76).isAPartOf(wsaaPrtLine006, 0, FILLER).init(" To    Min   Max   Min %   To    Min   Max   Min %   To    Min   Max   Min %");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(75);
	private FixedLengthStringData filler17 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine007, 1).setPattern("ZZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 5, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 7).setPattern("ZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 10, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 12).setPattern("ZZZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 20).setPattern("ZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine007, 27).setPattern("ZZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 33).setPattern("ZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 38).setPattern("ZZZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 46).setPattern("ZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine007, 53).setPattern("ZZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 59).setPattern("ZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine007, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 72).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(75);
	private FixedLengthStringData filler29 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine008, 1).setPattern("ZZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 5, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 7).setPattern("ZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 10, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 12).setPattern("ZZZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 20).setPattern("ZZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine008, 27).setPattern("ZZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 33).setPattern("ZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 38).setPattern("ZZZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 46).setPattern("ZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine008, 53).setPattern("ZZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 59).setPattern("ZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine008, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 72).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(75);
	private FixedLengthStringData filler41 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine009, 1).setPattern("ZZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 5, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 7).setPattern("ZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 10, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 12).setPattern("ZZZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 20).setPattern("ZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine009, 27).setPattern("ZZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 33).setPattern("ZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 38).setPattern("ZZZZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 46).setPattern("ZZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine009, 53).setPattern("ZZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 59).setPattern("ZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine009, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 72).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(75);
	private FixedLengthStringData filler53 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine010, 1).setPattern("ZZZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 5, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 7).setPattern("ZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 10, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 12).setPattern("ZZZZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 20).setPattern("ZZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine010, 27).setPattern("ZZZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 33).setPattern("ZZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 38).setPattern("ZZZZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 46).setPattern("ZZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine010, 53).setPattern("ZZZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 59).setPattern("ZZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine010, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 72).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(67);
	private FixedLengthStringData filler65 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine011, 0, FILLER).init(" Frequency :");
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 13);
	private FixedLengthStringData filler66 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine011, 15, FILLER).init(SPACES);
	private FixedLengthStringData filler67 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine011, 27, FILLER).init("Frequency :");
	private FixedLengthStringData fieldNo059 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 39);
	private FixedLengthStringData filler68 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine011, 41, FILLER).init(SPACES);
	private FixedLengthStringData filler69 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine011, 53, FILLER).init("Frequency :");
	private FixedLengthStringData fieldNo060 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 65);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(77);
	private FixedLengthStringData filler70 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler71 = new FixedLengthStringData(71).isAPartOf(wsaaPrtLine012, 6, FILLER).init("% of Target Overdue       % of Target Overdue       % of Target Overdue");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(76);
	private FixedLengthStringData filler72 = new FixedLengthStringData(76).isAPartOf(wsaaPrtLine013, 0, FILLER).init(" To    Min   Max   Min %   To    Min   Max   Min %   To    Min   Max   Min %");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(75);
	private FixedLengthStringData filler73 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine014, 1).setPattern("ZZZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 5, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 7).setPattern("ZZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 10, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine014, 12).setPattern("ZZZZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 20).setPattern("ZZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine014, 27).setPattern("ZZZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 33).setPattern("ZZZ");
	private FixedLengthStringData filler79 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine014, 38).setPattern("ZZZZZ");
	private FixedLengthStringData filler80 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 46).setPattern("ZZZ");
	private FixedLengthStringData filler81 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine014, 53).setPattern("ZZZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 59).setPattern("ZZZ");
	private FixedLengthStringData filler83 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine014, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler84 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 72).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(75);
	private FixedLengthStringData filler85 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine015, 1).setPattern("ZZZZ");
	private FixedLengthStringData filler86 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 5, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 7).setPattern("ZZZ");
	private FixedLengthStringData filler87 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 10, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine015, 12).setPattern("ZZZZZ");
	private FixedLengthStringData filler88 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo076 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 20).setPattern("ZZZ");
	private FixedLengthStringData filler89 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine015, 27).setPattern("ZZZZ");
	private FixedLengthStringData filler90 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 33).setPattern("ZZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo079 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine015, 38).setPattern("ZZZZZ");
	private FixedLengthStringData filler92 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo080 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 46).setPattern("ZZZ");
	private FixedLengthStringData filler93 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine015, 53).setPattern("ZZZZ");
	private FixedLengthStringData filler94 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo082 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 59).setPattern("ZZZ");
	private FixedLengthStringData filler95 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine015, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler96 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 72).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(75);
	private FixedLengthStringData filler97 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo085 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine016, 1).setPattern("ZZZZ");
	private FixedLengthStringData filler98 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 5, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo086 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 7).setPattern("ZZZ");
	private FixedLengthStringData filler99 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 10, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo087 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine016, 12).setPattern("ZZZZZ");
	private FixedLengthStringData filler100 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo088 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 20).setPattern("ZZZ");
	private FixedLengthStringData filler101 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo089 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine016, 27).setPattern("ZZZZ");
	private FixedLengthStringData filler102 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo090 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 33).setPattern("ZZZ");
	private FixedLengthStringData filler103 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo091 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine016, 38).setPattern("ZZZZZ");
	private FixedLengthStringData filler104 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo092 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 46).setPattern("ZZZ");
	private FixedLengthStringData filler105 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine016, 53).setPattern("ZZZZ");
	private FixedLengthStringData filler106 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo094 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 59).setPattern("ZZZ");
	private FixedLengthStringData filler107 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo095 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine016, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler108 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo096 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 72).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(75);
	private FixedLengthStringData filler109 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo097 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine017, 1).setPattern("ZZZZ");
	private FixedLengthStringData filler110 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 5, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo098 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine017, 7).setPattern("ZZZ");
	private FixedLengthStringData filler111 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 10, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo099 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine017, 12).setPattern("ZZZZZ");
	private FixedLengthStringData filler112 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine017, 17, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo100 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine017, 20).setPattern("ZZZ");
	private FixedLengthStringData filler113 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine017, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo101 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine017, 27).setPattern("ZZZZ");
	private FixedLengthStringData filler114 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo102 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine017, 33).setPattern("ZZZ");
	private FixedLengthStringData filler115 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo103 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine017, 38).setPattern("ZZZZZ");
	private FixedLengthStringData filler116 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine017, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo104 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine017, 46).setPattern("ZZZ");
	private FixedLengthStringData filler117 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine017, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo105 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine017, 53).setPattern("ZZZZ");
	private FixedLengthStringData filler118 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo106 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine017, 59).setPattern("ZZZ");
	private FixedLengthStringData filler119 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo107 = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrtLine017, 64).setPattern("ZZZZZ");
	private FixedLengthStringData filler120 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine017, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo108 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine017, 72).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(28);
	private FixedLengthStringData filler121 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine018, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5729rec t5729rec = new T5729rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5729pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5729rec.t5729Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5729rec.frqcy01);
		fieldNo008.set(t5729rec.frqcy02);
		fieldNo009.set(t5729rec.frqcy03);
		fieldNo010.set(t5729rec.durationa01);
		fieldNo011.set(t5729rec.targetMina01);
		fieldNo012.set(t5729rec.targetMaxa01);
		fieldNo013.set(t5729rec.overdueMina01);
		fieldNo014.set(t5729rec.durationb01);
		fieldNo015.set(t5729rec.targetMinb01);
		fieldNo016.set(t5729rec.targetMaxb01);
		fieldNo017.set(t5729rec.overdueMinb01);
		fieldNo018.set(t5729rec.durationc01);
		fieldNo019.set(t5729rec.targetMinc01);
		fieldNo020.set(t5729rec.targetMaxc01);
		fieldNo021.set(t5729rec.overdueMinc01);
		fieldNo022.set(t5729rec.durationa02);
		fieldNo023.set(t5729rec.targetMina02);
		fieldNo024.set(t5729rec.targetMaxa02);
		fieldNo025.set(t5729rec.overdueMina02);
		fieldNo026.set(t5729rec.durationb02);
		fieldNo027.set(t5729rec.targetMinb02);
		fieldNo028.set(t5729rec.targetMaxb02);
		fieldNo029.set(t5729rec.overdueMinb02);
		fieldNo030.set(t5729rec.durationc02);
		fieldNo031.set(t5729rec.targetMinc02);
		fieldNo032.set(t5729rec.targetMaxc02);
		fieldNo033.set(t5729rec.overdueMinc02);
		fieldNo034.set(t5729rec.durationa03);
		fieldNo035.set(t5729rec.targetMina03);
		fieldNo036.set(t5729rec.targetMaxa03);
		fieldNo037.set(t5729rec.overdueMina03);
		fieldNo038.set(t5729rec.durationb03);
		fieldNo039.set(t5729rec.targetMinb03);
		fieldNo040.set(t5729rec.targetMaxb03);
		fieldNo041.set(t5729rec.overdueMinb03);
		fieldNo042.set(t5729rec.durationc03);
		fieldNo043.set(t5729rec.targetMinc03);
		fieldNo044.set(t5729rec.targetMaxc03);
		fieldNo045.set(t5729rec.overdueMinc03);
		fieldNo046.set(t5729rec.durationa04);
		fieldNo047.set(t5729rec.targetMina04);
		fieldNo048.set(t5729rec.targetMaxa04);
		fieldNo049.set(t5729rec.overdueMina04);
		fieldNo050.set(t5729rec.durationb04);
		fieldNo051.set(t5729rec.targetMinb04);
		fieldNo052.set(t5729rec.targetMaxb04);
		fieldNo053.set(t5729rec.overdueMinb04);
		fieldNo054.set(t5729rec.durationc04);
		fieldNo055.set(t5729rec.targetMinc04);
		fieldNo056.set(t5729rec.targetMaxc04);
		fieldNo057.set(t5729rec.overdueMinc04);
		fieldNo058.set(t5729rec.frqcy04);
		fieldNo059.set(t5729rec.frqcy05);
		fieldNo060.set(t5729rec.frqcy06);
		fieldNo061.set(t5729rec.durationd01);
		fieldNo062.set(t5729rec.targetMind01);
		fieldNo063.set(t5729rec.targetMaxd01);
		fieldNo064.set(t5729rec.overdueMind01);
		fieldNo065.set(t5729rec.duratione01);
		fieldNo066.set(t5729rec.targetMine01);
		fieldNo067.set(t5729rec.targetMaxe01);
		fieldNo068.set(t5729rec.overdueMine01);
		fieldNo069.set(t5729rec.durationf01);
		fieldNo070.set(t5729rec.targetMinf01);
		fieldNo071.set(t5729rec.targetMaxf01);
		fieldNo072.set(t5729rec.overdueMinf01);
		fieldNo073.set(t5729rec.durationd02);
		fieldNo074.set(t5729rec.targetMind02);
		fieldNo075.set(t5729rec.targetMaxd02);
		fieldNo076.set(t5729rec.overdueMind02);
		fieldNo077.set(t5729rec.duratione02);
		fieldNo078.set(t5729rec.targetMine02);
		fieldNo079.set(t5729rec.targetMaxe02);
		fieldNo080.set(t5729rec.overdueMine02);
		fieldNo081.set(t5729rec.durationf02);
		fieldNo082.set(t5729rec.targetMinf02);
		fieldNo083.set(t5729rec.targetMaxf02);
		fieldNo084.set(t5729rec.overdueMinf02);
		fieldNo085.set(t5729rec.durationd03);
		fieldNo086.set(t5729rec.targetMind03);
		fieldNo087.set(t5729rec.targetMaxd03);
		fieldNo088.set(t5729rec.overdueMind03);
		fieldNo089.set(t5729rec.duratione03);
		fieldNo090.set(t5729rec.targetMine03);
		fieldNo091.set(t5729rec.targetMaxe03);
		fieldNo092.set(t5729rec.overdueMine03);
		fieldNo093.set(t5729rec.durationf03);
		fieldNo094.set(t5729rec.targetMinf03);
		fieldNo095.set(t5729rec.targetMaxf03);
		fieldNo096.set(t5729rec.overdueMinf03);
		fieldNo097.set(t5729rec.durationd04);
		fieldNo098.set(t5729rec.targetMind04);
		fieldNo099.set(t5729rec.targetMaxd04);
		fieldNo100.set(t5729rec.overdueMind04);
		fieldNo101.set(t5729rec.duratione04);
		fieldNo102.set(t5729rec.targetMine04);
		fieldNo103.set(t5729rec.targetMaxe04);
		fieldNo104.set(t5729rec.overdueMine04);
		fieldNo105.set(t5729rec.durationf04);
		fieldNo106.set(t5729rec.targetMinf04);
		fieldNo107.set(t5729rec.targetMaxf04);
		fieldNo108.set(t5729rec.overdueMinf04);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
