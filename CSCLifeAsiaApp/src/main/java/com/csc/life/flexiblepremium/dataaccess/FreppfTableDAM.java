package com.csc.life.flexiblepremium.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: FreppfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:55
 * Class transformed from FREPPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class FreppfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 158;
	public FixedLengthStringData freprec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData freppfRecord = freprec;
	
	public FixedLengthStringData company = DD.company.copy().isAPartOf(freprec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(freprec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(freprec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(freprec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(freprec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(freprec);
	public PackedDecimalData btdate = DD.btdate.copy().isAPartOf(freprec);
	public PackedDecimalData linstamt = DD.linstamt.copy().isAPartOf(freprec);
	public PackedDecimalData minprem = DD.minprem.copy().isAPartOf(freprec);
	public PackedDecimalData maxprem = DD.maxprem.copy().isAPartOf(freprec);
	public PackedDecimalData susamt = DD.susamt.copy().isAPartOf(freprec);
	public PackedDecimalData targetPremium = DD.prmper.copy().isAPartOf(freprec);
	public PackedDecimalData premRecPer = DD.prmrcdp.copy().isAPartOf(freprec);
	public PackedDecimalData billedInPeriod = DD.billedp.copy().isAPartOf(freprec);
	public PackedDecimalData overdueMin = DD.ovrminreq.copy().isAPartOf(freprec);
	public PackedDecimalData targfrom = DD.targfrom.copy().isAPartOf(freprec);
	public PackedDecimalData targto = DD.targto.copy().isAPartOf(freprec);
	public PackedDecimalData payrseqno = DD.payrseqno.copy().isAPartOf(freprec);
	public FixedLengthStringData procflag = DD.procflag.copy().isAPartOf(freprec);
	public PackedDecimalData minOverduePer = DD.minovrpro.copy().isAPartOf(freprec);
	public PackedDecimalData annivProcDate = DD.cbanpr.copy().isAPartOf(freprec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(freprec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(freprec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(freprec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public FreppfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for FreppfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public FreppfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for FreppfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public FreppfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for FreppfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public FreppfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("FREPPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"COMPANY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"BTDATE, " +
							"LINSTAMT, " +
							"MINPREM, " +
							"MAXPREM, " +
							"SUSAMT, " +
							"PRMPER, " +
							"PRMRCDP, " +
							"BILLEDP, " +
							"OVRMINREQ, " +
							"TARGFROM, " +
							"TARGTO, " +
							"PAYRSEQNO, " +
							"PROCFLAG, " +
							"MINOVRPRO, " +
							"CBANPR, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     company,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     btdate,
                                     linstamt,
                                     minprem,
                                     maxprem,
                                     susamt,
                                     targetPremium,
                                     premRecPer,
                                     billedInPeriod,
                                     overdueMin,
                                     targfrom,
                                     targto,
                                     payrseqno,
                                     procflag,
                                     minOverduePer,
                                     annivProcDate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		company.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		btdate.clear();
  		linstamt.clear();
  		minprem.clear();
  		maxprem.clear();
  		susamt.clear();
  		targetPremium.clear();
  		premRecPer.clear();
  		billedInPeriod.clear();
  		overdueMin.clear();
  		targfrom.clear();
  		targto.clear();
  		payrseqno.clear();
  		procflag.clear();
  		minOverduePer.clear();
  		annivProcDate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getFreprec() {
  		return freprec;
	}

	public FixedLengthStringData getFreppfRecord() {
  		return freppfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setFreprec(what);
	}

	public void setFreprec(Object what) {
  		this.freprec.set(what);
	}

	public void setFreppfRecord(Object what) {
  		this.freppfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(freprec.getLength());
		result.set(freprec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}