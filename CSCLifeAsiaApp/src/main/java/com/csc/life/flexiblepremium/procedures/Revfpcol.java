/*
 * File: Revfpcol.java
 * Date: 30 August 2009 2:07:35
 * Author: Quipoz Limited
 * 
 * Class transformed from REVFPCOL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.PDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.agents.dataaccess.ZctnTableDAM;
import com.csc.life.agents.dataaccess.ZctnrevTableDAM;
import com.csc.life.agents.dataaccess.ZptnTableDAM;
import com.csc.life.agents.dataaccess.ZptnrevTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5644rec;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmbchTableDAM;
import com.csc.life.contractservicing.dataaccess.RtrnrevTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.flexiblepremium.dataaccess.FpcorevTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FprmrevTableDAM;
import com.csc.life.flexiblepremium.dataaccess.LinsfprTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* REVFPCOLL
* ~~~~~~~
* The parameters passed to this program are set up in the copybook
* REVESEREC.
*
* Delete the Instalment for a contract:-
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*     Read the life instalments file for this contract (LINSPF) with
*     the logical view LINSFPR using the company/contract number and
*     eff date of the PTRN in the LINS billcd. Only paid LINS are
*     created for flexible premium contracts with the billcd = to the
*     effective date of the collections run. Only one will exist per
*     FP coll run being reversed. When found, the instfrom and instto
*     dates will be used to update the paidto and billed to dates on
*     the Payr and Contract header.
*
*     The amount on this LINS will represent the actual amount
*     collected which will be used later to calculate the amounts
*     allocated per coverage.
*
* Update CHDR, PAYR and FPRM records :-
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*     Read the contract header record (CHDRPF) using the logical view
*     CHDR.
*     Set the paid to date = the LINS instfrom date.
*     Rewrite the record.
*
*     Read the Payr record (PAYRPF) using the logical view PAYR
*     Set the paid to date = the LINS instfrom date.
*     Rewrite the record.
*
*     Read the FPRM record using the logical view FPRM.
*     Subtract the LINSFPR-CBILLAMT from the FPRM-TOTRECD.
*     Rewrite the record.
*
* Put receipt amount back into suspense:-
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*     Read the receipt file (RTRNPF) using the logical view RTRNREV
*     with a key of contract number/transaction number from linkage.
*
*     For each record found:
*         Overwrite the accounting month/year with the batch
*         accounting month/year from linkage.
*         Overwrite the tranno with the NEW tranno from linkage.
*         Overwrite the batch key with the new batch key from linkage.
*         Reverse the posting by changing the GL sign :
*         ie when '+' set to '-' and vice versa.
*         Overwrite the transaction reference with the NEW tranno from
*         linkage.
*         Overwrite the transaction description with the long
*         description of the new tranno from T1688.
*         Overwrite the effective date with todays date.
*         Write the new record to the database by calling the
*         subroutine 'LIFRTRN'.
*
* Reverse the account movements from the ledger:-
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*     Read the Account movements file (ACMVPF) using the logical view
*     ACMVREV with a key of contract number/transaction number from
*     linkage.
*
*     For each record found:
*         Overwrite the accounting month/year with the batch
*         accounting month/year from linkage.
*         Overwrite the tranno with the NEW tranno from linkage.
*         Overwrite the batch key with the new batch key from linkage.
*         Reverse the posting by changing the GL sign :
*         ie when '+' set to '-' and vice versa.
*         Overwrite the transaction reference with the NEW tranno from
*         linkage.
*         Overwrite the transaction description with the long
*         description of the new tranno from T1688.
*         Overwrite the effective date with todays date.
*         Write the new record to the database by calling the
*         subroutine 'LIFACMV'.
*
*
* Calculate the amount assigned to each coverage
* ----------------------------------------------
*
* Read each validflag 1 coverage for the contract.
* Validate the status for each.
* For each one of a valid status, calculate the amount assigned
* to this coverage as (coverage-instprem / payr-instamt06) *
* LINSFPR-CBILLAMT.
* This is necessary for multi-coverage or breakout processing.
*
* For the first coverage only, store the CRTABLE.
* It will be used later on to read T5671 for the UTRN reversal
* subroutine.
* This subroutine, although called at Coverage level will reverse
* all UTRNS for the contract in one call.
*
* Update each applicable FPCO per coverage
* ----------------------------------------
*
* When the amount assigned per coverage has been calculated,
* each FPCO for the covage is read in reverse date order using
* the FPCOREV logical file.
* This will ensure that the last one updated will be reversed
* first.
* If the received amount on the FPCO is = zero, the next one is
* read.
* Where the amount received on the FPCO is < the amount to
* reverse, the FPCO received is zeroised and the amount to
* reverse is reduced by the received amount on the FPCO.
* After processing the current FPCO, including the AGCM updating,
* the next FPCO is read and the same comparison on the amount to
* reverse over the FPCO received amount is performed, but this time it
* is on the reversal amount after it has been reduced by the received
* amount on the previous FPCO.
*
* Should the FPCO received amount be > or = the amount to
* reverse, no more FPCO records need to be read.
*
* Once the amount to reverse on the FPCO record has been
* determined, we have to ascertain whether the amount
* being reduced is less than, = to or > the target amount on the
* FPCO record.
* If it is < or =, all premium reduced is WSAA-UP-TO-TARGET.
*
* If it is >, and when the premium to reverse is deducted
* the FPCO received is still > or = the target, all premium
* reduced is WSAA-OVER-TARGET.
*
* If it is >, and when the premium to reverse is deducted, the
* FPCO is now < the target, the amount is split between WSAA-
* UP-TO-TARGET and WSAA-OVER-TARGET.
*
* WSAA-OVER-TARGET is calculated as the FPCO amount received
* over the target premium.
* WSAA-UP-TO-TARGET is the remainder of the amount to reverse
* for this FPCO.
*
* Once WSAA-UP-TO-TARGET and WSAA-OVER-TARGET have been
* calculated, and the AGCM commission
* calculations have been performed, the FPCO is updated.
*
* The FPCO is updated prior to reading the next FPCO
* or Coverage.
*
* Update Agent commission records for each FPCO updated
* -----------------------------------------------------
*     Read the Agents commission file (AGCMPF) with the logical
*     view AGCMBCH using the contract number as the key.
*
*     For each record found carry out the following processing:-
*         If the effective date of the record >
*         the new paid to date (effective date 1 from linkage) read the
*         next record
*
*         Otherwise :
*
*         Set up the details required in the COMLINKREC copybook for
*         each of the three types of commission we may be reversing.
*         For each 'type' call the relevant reversal subroutine. This
*         is obtained from T5644 using the method from the AGCM record
*         for this type of commission.
*
*         Determine the Initial commission to reverse
*         -------------------------------------------
*         If comern or compay = zero
*            skip initial commission calculation.
*         If the initial commission method is = spaces
*            skip initial commission calculation.
*
*         Pass WSAA-PREM-RECD to CLNK-INSTPREM
*         Add set up Initial commission fields for calling the
*         initial commission reversal program.
*         Then call the commission payment reversal subroutine
*         The routine will calculate the amount to reduce COMERN
*         and COMPAY by.
*
*         Determine the Renewal commission to reverse
*         -------------------------------------------
*         If RNWPAY = spaces
*            skip renewal commission calculation.
*         If RNLCDUE = zero or RNLCEARN = zero
*            skip renewal commission calculation.
*         If WSAA-UP-TO-TARGET = zero
*            skip renewal commission calculation.
*
*         Move WSAA-UP-TO-TARGET to CLNK-INSTPREM
*         Then call the commission payment reversal subroutine
*         The routine will calculate the amount to reduce RNLCDUE
*         and RNLCERN by.
*
*         Determine the Over target commission to reverse
*         -------------------------------------------
*         If SRVPAY = spaces
*            skip over target commission calculation.
*         If SCMDUE = zero or SCMEARN = zero
*            skip over target commission calculation.
*         If WSAA-OVER-TARGET = zero
*            skip over target commission calculation.
*
*         Move WSAA-OVER-TARGET to CLNK-INSTPREM
*
*         Then call the commission payment reversal subroutine
*         The routine will calculate the amount to reduce SCMDUE
*         and SCMEARN by.
*
* Once all the AGCM records have been updated, the the FPCO is
* updated.
*
*
* Call UTRN reversal subroutine
* -----------------------------
*
* Once all the Coverages have been processed, T5671 is accessed
* using the Coverage type stored and the reversal transaction code.
* The UTRN reversal subroutine is then called.
* As previously mentioned, although the T5671 entry is at coverage
* level, the subroutine reverses at contract level which is why it
* is called here and not per coverage.
*
/
***********************************************************************
*                                                                     *
* </pre>
*/
public class Revfpcol extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVFPCOL";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreq, 0).setUnsigned();

	private FixedLengthStringData wsaaT5671 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batc = new FixedLengthStringData(4).isAPartOf(wsaaT5671, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671, 4);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaPostyearX = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaPostyear = new ZonedDecimalData(4, 0).isAPartOf(wsaaPostyearX, 0).setUnsigned();

	private FixedLengthStringData wsaaPostmonthX = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaPostmonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaPostmonthX, 0).setUnsigned();
	private ZonedDecimalData wsaaCommType = new ZonedDecimalData(1, 0).setUnsigned();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");
		/* WSAA-SAVED-AMOUNTS */
	private PackedDecimalData[] wsaaPayamnt = PDInittedArray(3, 17, 2);
	private PackedDecimalData[] wsaaErndamt = PDInittedArray(3, 17, 2);

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";

	private FixedLengthStringData wsaaFpcoAmts = new FixedLengthStringData(78);
	private PackedDecimalData wsaaTotAllocated = new PackedDecimalData(17, 2).isAPartOf(wsaaFpcoAmts, 0).init(0);
	private PackedDecimalData wsaaTotInstprem = new PackedDecimalData(17, 2).isAPartOf(wsaaFpcoAmts, 9).init(0);
	private PackedDecimalData wsaaPolamt = new PackedDecimalData(17, 2).isAPartOf(wsaaFpcoAmts, 18).init(0);
	private PackedDecimalData wsaaFpcoOverTgt = new PackedDecimalData(17, 2).isAPartOf(wsaaFpcoAmts, 27).init(0);
	private PackedDecimalData wsaaUpToTarget = new PackedDecimalData(17, 5).isAPartOf(wsaaFpcoAmts, 36).init(0);
	private PackedDecimalData wsaaOverTarget = new PackedDecimalData(17, 5).isAPartOf(wsaaFpcoAmts, 45).init(0);
	private PackedDecimalData wsaaFpcoAlloc = new PackedDecimalData(17, 2).isAPartOf(wsaaFpcoAmts, 54).init(0);
	private PackedDecimalData wsaaAllocRemain = new PackedDecimalData(17, 2).isAPartOf(wsaaFpcoAmts, 63).init(0);
	private PackedDecimalData wsaaProportion = new PackedDecimalData(10, 7).isAPartOf(wsaaFpcoAmts, 72).init(0);
	private FixedLengthStringData wsaaStoredCrtable = new FixedLengthStringData(4);
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5644 = "T5644";
	private static final String t5671 = "T5671";
	private static final String t5679 = "T5679";
	private static final String th605 = "TH605";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private AgcmbchTableDAM agcmbchIO = new AgcmbchTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FpcorevTableDAM fpcorevIO = new FpcorevTableDAM();
	private FprmrevTableDAM fprmrevIO = new FprmrevTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LinsfprTableDAM linsfprIO = new LinsfprTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private RtrnrevTableDAM rtrnrevIO = new RtrnrevTableDAM();
	private TaxdrevTableDAM taxdrevIO = new TaxdrevTableDAM();
	private ZctnTableDAM zctnIO = new ZctnTableDAM();
	private ZctnrevTableDAM zctnrevIO = new ZctnrevTableDAM();
	private ZptnTableDAM zptnIO = new ZptnTableDAM();
	private ZptnrevTableDAM zptnrevIO = new ZptnrevTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Batckey wsaaBatckey = new Batckey();
	private T5644rec t5644rec = new T5644rec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private Th605rec th605rec = new Th605rec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private Greversrec greversrec = new Greversrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextFpco2250, 
		exit2290, 
		call4020, 
		exit4090, 
		call5020, 
		exit5090, 
		nextAcmv10020
	}

	public Revfpcol() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		wsaaBatckey.set(reverserec.batchkey);
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		/* get today's date*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		readTables600();
		processLins700();
		if (isEQ(linsfprIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		updateContractHeader800();
		updatePayrFile900();
		updateFprmFile1000();
		readReceiptFile1200();
		reverseAcctMovements1400();
		processAcmvOptical1600();
		processFpcoAgcm2000();
		reverseUnits3000();
		deleteLins3200();
		/* Read table TH605 to see if Bonus Workbench Extraction used      */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(th605);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemitem(reverserec.company);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError8000();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
		if (isNE(th605rec.bonusInd, "Y")) {
			return ;
		}
		/* Bonus Workbench  *                                              */
		processZptn4000();
		processZctn5000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readTables600()
	{
		para600();
	}

protected void para600()
	{
		/* Obtain transaction description from T1688*/
		descIO.setDescpfx("IT");
		descIO.setDesctabl(t1688);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setDesccoy(reverserec.company);
		descIO.setLanguage(reverserec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK))
		&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			systemError8000();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			wsaaTransDesc.fill("?");
		}
		else {
			wsaaTransDesc.set(descIO.getLongdesc());
		}
		/*  Load Contract statii.*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(reverserec.oldBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			systemError8000();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	*   Read paid LINS for a contract
	* </pre>
	*/
protected void processLins700()
	{
		/*PARA*/
		linsfprIO.setParams(SPACES);
		linsfprIO.setChdrcoy(reverserec.company);
		linsfprIO.setChdrnum(reverserec.chdrnum);
		linsfprIO.setBillcd(reverserec.ptrneff);
		linsfprIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, linsfprIO);
		if (isNE(linsfprIO.getStatuz(), varcom.oK)
		&& isNE(linsfprIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(linsfprIO.getParams());
			dbError8100();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*   Read contract header
	* </pre>
	*/
protected void updateContractHeader800()
	{
		para800();
	}

protected void para800()
	{
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			dbError8100();
		}
		/*  Now update contract*/
		chdrlifIO.setPtdate(linsfprIO.getInstfrom());
		chdrlifIO.setBtdate(linsfprIO.getInstto());
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		chdrlifIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			dbError8100();
		}
	}

protected void updatePayrFile900()
	{
		para910();
	}

protected void para910()
	{
		/* Read the PAYR file with a function READH - at the moment*/
		/* we assume that there is only one PAYR record per contract.*/
		/* This code will be changed in the future to cater for*/
		/* multiple PAYR records*/
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(reverserec.company);
		payrIO.setChdrnum(chdrlifIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			dbError8100();
		}
		/* Update the PAYR record with the billed to and paid to*/
		/* dates.*/
		payrIO.setPtdate(linsfprIO.getInstfrom());
		payrIO.setBtdate(linsfprIO.getInstto());
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			dbError8100();
		}
	}

protected void updateFprmFile1000()
	{
		para1000();
	}

protected void para1000()
	{
		/* Read and update the FPRM file. This file holds*/
		/* details of the premium received. The amount on the LINS is*/
		/* the amount to be deducted from the record.*/
		fprmrevIO.setDataArea(SPACES);
		fprmrevIO.setChdrcoy(reverserec.company);
		fprmrevIO.setChdrnum(chdrlifIO.getChdrnum());
		fprmrevIO.setPayrseqno(payrIO.getPayrseqno());
		fprmrevIO.setFormat(formatsInner.fprmrevrec);
		fprmrevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fprmrevIO);
		if (isNE(fprmrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fprmrevIO.getParams());
			dbError8100();
		}
		/* Update the FPRM record by reducing the received by the LINS*/
		/* amount*/
		setPrecision(fprmrevIO.getTotalRecd(), 2);
		fprmrevIO.setTotalRecd(sub(fprmrevIO.getTotalRecd(), linsfprIO.getCbillamt()));
		fprmrevIO.setFormat(formatsInner.fprmrevrec);
		fprmrevIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fprmrevIO);
		if (isNE(fprmrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fprmrevIO.getParams());
			dbError8100();
		}
	}

protected void readReceiptFile1200()
	{
		para1200();
	}

protected void para1200()
	{
		rtrnrevIO.setParams(SPACES);
		rtrnrevIO.setRldgcoy(reverserec.company);
		rtrnrevIO.setRdocnum(reverserec.chdrnum);
		rtrnrevIO.setTranno(reverserec.tranno);
		rtrnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		rtrnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, rtrnrevIO);
		if ((isNE(rtrnrevIO.getStatuz(), varcom.oK))
		&& (isNE(rtrnrevIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(rtrnrevIO.getParams());
			dbError8100();
		}
		if (isNE(reverserec.chdrnum, rtrnrevIO.getRdocnum())
		|| isNE(reverserec.company, rtrnrevIO.getRldgcoy())
		|| isNE(reverserec.tranno, rtrnrevIO.getTranno())
		|| isNE(reverserec.oldBatctrcde, rtrnrevIO.getBatctrcde())) {
			rtrnrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(rtrnrevIO.getStatuz(), varcom.endp))) {
			overwriteReceipts1300();
		}
		
	}

protected void overwriteReceipts1300()
	{
		para1301();
	}

protected void para1301()
	{
		/*     Write a new record to the database by calling subroutine*/
		/*     LIFRTRN*/
		lifrtrnrec.function.set("PSTW");
		lifrtrnrec.batckey.set(reverserec.batchkey);
		lifrtrnrec.rdocnum.set(chdrlifIO.getChdrnum());
		lifrtrnrec.rldgacct.set(chdrlifIO.getChdrnum());
		lifrtrnrec.tranno.set(reverserec.newTranno);
		lifrtrnrec.jrnseq.set(rtrnrevIO.getTranseq());
		lifrtrnrec.rldgcoy.set(chdrlifIO.getChdrcoy());
		lifrtrnrec.sacscode.set(rtrnrevIO.getSacscode());
		lifrtrnrec.sacstyp.set(rtrnrevIO.getSacstyp());
		lifrtrnrec.trandesc.set(wsaaTransDesc);
		lifrtrnrec.crate.set(rtrnrevIO.getCrate());
		lifrtrnrec.transactionDate.set(rtrnrevIO.getTrandate());
		lifrtrnrec.transactionTime.set(rtrnrevIO.getTrantime());
		compute(lifrtrnrec.acctamt, 2).set(mult(rtrnrevIO.getAcctamt(), -1));
		compute(lifrtrnrec.origamt, 2).set(mult(rtrnrevIO.getOrigamt(), -1));
		lifrtrnrec.genlcur.set(rtrnrevIO.getGenlcur());
		lifrtrnrec.origcurr.set(rtrnrevIO.getOrigccy());
		lifrtrnrec.genlcoy.set(rtrnrevIO.getGenlcoy());
		lifrtrnrec.glcode.set(rtrnrevIO.getGlcode());
		lifrtrnrec.glsign.set(rtrnrevIO.getGlsign());
		lifrtrnrec.postyear.set(rtrnrevIO.getPostyear());
		lifrtrnrec.postmonth.set(rtrnrevIO.getPostmonth());
		lifrtrnrec.effdate.set(wsaaToday);
		lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
		lifrtrnrec.rcamt.set(0);
		lifrtrnrec.contot.set(1);
		lifrtrnrec.substituteCode[1].set(chdrlifIO.getCnttype());
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifrtrnrec.lifrtrnRec);
			systemError8000();
		}
		rtrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, rtrnrevIO);
		if ((isNE(rtrnrevIO.getStatuz(), varcom.oK))
		&& (isNE(rtrnrevIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(rtrnrevIO.getParams());
			dbError8100();
		}
		if (isNE(reverserec.chdrnum, rtrnrevIO.getRdocnum())
		|| isNE(reverserec.company, rtrnrevIO.getRldgcoy())
		|| isNE(reverserec.tranno, rtrnrevIO.getTranno())
		|| isNE(reverserec.oldBatctrcde, rtrnrevIO.getBatctrcde())) {
			rtrnrevIO.setStatuz(varcom.endp);
		}
	}

	/**
	* <pre>
	*  Reverse the account movements from the ledger
	* </pre>
	*/
protected void reverseAcctMovements1400()
	{
		para1400();
	}

protected void para1400()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setBatctrcde(SPACES);
		while ( !((isEQ(acmvrevIO.getStatuz(), varcom.endp))
		|| (isEQ(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)))) {
			SmartFileCode.execute(appVars, acmvrevIO);
			if ((isNE(acmvrevIO.getStatuz(), varcom.oK))
			&& (isNE(acmvrevIO.getStatuz(), varcom.endp))) {
				syserrrec.params.set(acmvrevIO.getParams());
				dbError8100();
			}
			if ((isNE(acmvrevIO.getRldgcoy(), reverserec.company))
			|| (isNE(acmvrevIO.getRdocnum(), reverserec.chdrnum))
			|| (isNE(acmvrevIO.getTranno(), reverserec.tranno))) {
				acmvrevIO.setStatuz(varcom.endp);
			}
			acmvrevIO.setFunction(varcom.nextr);
		}
		
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			readAcmvs10000();
		}
		
	}

protected void processAcmvOptical1600()
	{
		start1610();
	}

protected void start1610()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			dbError8100();
		}
		/* If the PTRN being processed has an Accounting Period Greater*/
		/* than the last period Archived then there is no need to*/
		/* attempt to read the Optical Device.*/
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			dbError8100();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			readAcmvs10000();
		}
		
		/* If the next policy is found then we must call the COLD API*/
		/* again with a function of CLOSE.*/
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				dbError8100();
			}
		}
	}

protected void processFpcoAgcm2000()
	{
		go2001();
	}

protected void go2001()
	{
		initialize(wsaaFpcoAmts);
		covrlnbIO.setParams(SPACES);
		covrlnbIO.setChdrnum(reverserec.chdrnum);
		covrlnbIO.setChdrcoy(reverserec.company);
		covrlnbIO.setPlanSuffix(ZERO);
		covrlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(), varcom.endp)
		&& isNE(covrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrlnbIO.getParams());
			dbError8100();
		}
		if (isNE(reverserec.chdrnum, covrlnbIO.getChdrnum())
		|| isNE(reverserec.company, covrlnbIO.getChdrcoy())) {
			covrlnbIO.setStatuz(varcom.endp);
		}
		/* store 1st Coverage type for unit processing later*/
		if (isNE(covrlnbIO.getStatuz(), varcom.endp)) {
			wsaaStoredCrtable.set(covrlnbIO.getCrtable());
		}
		while ( !(isEQ(covrlnbIO.getStatuz(), varcom.endp))) {
			proRateCovr2100();
		}
		
		/* check that the total reversed = the amount received*/
		if (isNE(wsaaTotAllocated, linsfprIO.getCbillamt())) {
			syserrrec.params.set(wsaaTotAllocated);
			systemError8000();
		}
	}

protected void proRateCovr2100()
	{
		go2101();
		readNextCovr2150();
	}

protected void go2101()
	{
		/* Check to see if coverage is of a valid status*/
		wsaaValidCoverage.set("N");
		if (isNE(covrlnbIO.getValidflag(), "1")) {
			return ;
		}
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covrlnbIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], covrlnbIO.getPstatcode())) {
						wsaaT5679Sub.set(13);
						wsaaValidCoverage.set("Y");
					}
				}
			}
		}
		/*  If the coverage is not of a valid status read the next covr*/
		/*  for the contract*/
		if (!validCoverage.isTrue()) {
			return ;
		}
		if (isEQ(covrlnbIO.getInstprem(), 0)) {
			return ;
		}
		/*  Coverage found. Calculate the proportion of the LINS that*/
		/*  was assigned to this coverage, then add to the total.*/
		wsaaTotInstprem.add(covrlnbIO.getInstprem());
		if (isEQ(wsaaTotInstprem, payrIO.getSinstamt06())) {
			compute(wsaaPolamt, 2).set(sub(linsfprIO.getCbillamt(), wsaaTotAllocated));
		}
		else {
			compute(wsaaProportion, 7).set((div(covrlnbIO.getInstprem(), payrIO.getSinstamt06())));
			compute(wsaaPolamt, 8).setRounded(mult(wsaaProportion, linsfprIO.getCbillamt()));
		}
		zrdecplrec.amountIn.set(wsaaPolamt);
		callRounding9000();
		wsaaPolamt.set(zrdecplrec.amountOut);
		wsaaTotAllocated.add(wsaaPolamt);
		fpcorevIO.setParams(SPACES);
		fpcorevIO.setChdrcoy(covrlnbIO.getChdrcoy());
		fpcorevIO.setChdrnum(covrlnbIO.getChdrnum());
		fpcorevIO.setCoverage(covrlnbIO.getCoverage());
		fpcorevIO.setLife(covrlnbIO.getLife());
		fpcorevIO.setRider(covrlnbIO.getRider());
		fpcorevIO.setPlanSuffix(covrlnbIO.getPlanSuffix());
		fpcorevIO.setTargfrom(varcom.vrcmMaxDate);
		/* MOVE BEGNH                   TO FPCOREV-FUNCTION.            */
		fpcorevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fpcorevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fpcorevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			syserrrec.params.set(covrlnbIO.getParams());
			dbError8100();
		}
		if (isNE(covrlnbIO.getChdrcoy(), fpcorevIO.getChdrcoy())
		|| isNE(covrlnbIO.getChdrnum(), fpcorevIO.getChdrnum())
		|| isNE(covrlnbIO.getCoverage(), fpcorevIO.getCoverage())
		|| isNE(covrlnbIO.getLife(), fpcorevIO.getLife())
		|| isNE(covrlnbIO.getRider(), fpcorevIO.getRider())
		|| isNE(covrlnbIO.getPlanSuffix(), fpcorevIO.getPlanSuffix())) {
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			syserrrec.params.set(fpcorevIO.getParams());
			dbError8100();
		}
		/* Process all FPCO records*/
		wsaaFpcoAlloc.set(0);
		wsaaAllocRemain.set(wsaaPolamt);
		while ( !(isEQ(fpcorevIO.getStatuz(), varcom.endp))) {
			processFpco2200();
		}
		
	}

protected void readNextCovr2150()
	{
		covrlnbIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(), varcom.endp)
		&& isNE(covrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrlnbIO.getParams());
			dbError8100();
		}
		if (isNE(reverserec.chdrnum, covrlnbIO.getChdrnum())
		|| isNE(reverserec.company, covrlnbIO.getChdrcoy())) {
			covrlnbIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processFpco2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					go2201();
				case nextFpco2250: 
					nextFpco2250();
				case exit2290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go2201()
	{
		/* Check that premium has been received on this FPCO,*/
		/* otherwise read next FPCO (after we release the lock)*/
		/* REWRT is not necessary,as BEGNH is replaced by BEGN,record      */
		/* is not locked.                                                  */
		if (isEQ(fpcorevIO.getPremRecPer(), ZERO)) {
			/*    MOVE REWRT               TO FPCOREV-FUNCTION              */
			/*    CALL 'FPCOREVIO'         USING FPCOREV-PARAMS             */
			/*    IF FPCOREV-STATUZ            NOT = O-K                    */
			/*       MOVE FPCOREV-PARAMS     TO SYSR-PARAMS                 */
			/*    END-IF                                                    */
			goTo(GotoLabel.nextFpco2250);
		}
		/* If we have received more premium on the FPCO than the*/
		/* coverage pro-rata'd prem being reversed, all prem can come*/
		/* off this record. Otherwise, the premium applicable to reverse*/
		/* off this FPCO is the total recd so far on the record.*/
		/* Any premium not reversed will have to come from an earlier*/
		/* period FPCO for the same coverage.*/
		if (isGTE(fpcorevIO.getPremRecPer(), wsaaAllocRemain)) {
			wsaaFpcoAlloc.set(wsaaAllocRemain);
			wsaaAllocRemain.set(ZERO);
		}
		else {
			compute(wsaaAllocRemain, 2).set(sub(wsaaAllocRemain, fpcorevIO.getPremRecPer()));
			wsaaFpcoAlloc.set(fpcorevIO.getPremRecPer());
		}
		/* Now determine whether all the amount to reverse is*/
		/* > the target or not. If it is > the target, the amount*/
		/* being reversed that exceeds the target is WSAA-OVER-TARGET.*/
		/* In all other cases it is WSAA-UP-TO-TARGET.*/
		if (isGT(fpcorevIO.getPremRecPer(), fpcorevIO.getTargetPremium())) {
			compute(wsaaFpcoOverTgt, 2).set(sub(fpcorevIO.getPremRecPer(), fpcorevIO.getTargetPremium()));
		}
		else {
			wsaaFpcoOverTgt.set(ZERO);
		}
		if (isGT(wsaaFpcoOverTgt, wsaaFpcoAlloc)) {
			wsaaOverTarget.set(wsaaFpcoAlloc);
		}
		else {
			wsaaOverTarget.set(wsaaFpcoOverTgt);
		}
		compute(wsaaUpToTarget, 5).set(sub(wsaaFpcoAlloc, wsaaOverTarget));
		/*  Now we have the premium to reverse from the period,*/
		/*  read the AGCM records and reverse commission.*/
		agcmbchIO.setParams(SPACES);
		agcmbchIO.setChdrnum(fpcorevIO.getChdrnum());
		agcmbchIO.setChdrcoy(fpcorevIO.getChdrcoy());
		agcmbchIO.setCoverage(fpcorevIO.getCoverage());
		agcmbchIO.setLife(fpcorevIO.getLife());
		agcmbchIO.setRider(fpcorevIO.getRider());
		agcmbchIO.setPlanSuffix(fpcorevIO.getPlanSuffix());
		/* MOVE BEGNH                   TO AGCMBCH-FUNCTION.            */
		agcmbchIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmbchIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmbchIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), varcom.endp)
		&& isNE(agcmbchIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			dbError8100();
		}
		/* REWRT is not necessary,as BEGNH is replaced by BEGN,record      */
		/* is not locked.                                                  */
		if (isNE(agcmbchIO.getChdrnum(), fpcorevIO.getChdrnum())
		|| isNE(agcmbchIO.getChdrcoy(), fpcorevIO.getChdrcoy())
		|| isNE(agcmbchIO.getCoverage(), fpcorevIO.getCoverage())
		|| isNE(agcmbchIO.getLife(), fpcorevIO.getLife())
		|| isNE(agcmbchIO.getRider(), fpcorevIO.getRider())
		|| isNE(agcmbchIO.getPlanSuffix(), fpcorevIO.getPlanSuffix())) {
			/*    MOVE REWRT               TO AGCMBCH-FUNCTION              */
			/*    CALL 'AGCMBCHIO'         USING AGCMBCH-PARAMS             */
			/*    IF AGCMBCH-STATUZ            NOT = O-K                    */
			/*       MOVE AGCMBCH-PARAMS     TO SYSR-PARAMS                 */
			/*    END-IF                                                    */
			agcmbchIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(agcmbchIO.getStatuz(), varcom.endp))) {
			processCommission2300();
		}
		
		/* Update the FPCO record before reading the next FPCO record      */
		/* This is updated by reducing the premium recd and resetting      */
		/* the active indicator if we have now not recd enough to satisfy  */
		/* the target.                                                     */
		setPrecision(fpcorevIO.getPremRecPer(), 2);
		fpcorevIO.setPremRecPer(sub(fpcorevIO.getPremRecPer(), wsaaFpcoAlloc));
		if (isEQ(fpcorevIO.getActiveInd(), "N")
		&& isLT(fpcorevIO.getPremRecPer(), fpcorevIO.getTargetPremium())) {
			fpcorevIO.setActiveInd("Y");
		}
		/* MOVE REWRT               TO FPCOREV-FUNCTION.        <LA3993>*/
		fpcorevIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			dbError8100();
		}
		/* Once all the premium has been processed on the FPCO*/
		/* check to see if we have any left to reverse*/
		if (isEQ(wsaaAllocRemain, 0)) {
			fpcorevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2290);
		}
	}

protected void nextFpco2250()
	{
		fpcorevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(), varcom.endp)
		&& isNE(fpcorevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			dbError8100();
		}
		/* RLSE is not necessary,as BEGNH is replaced by BEGN,record       */
		/* is not locked.                                                  */
		if (isNE(fpcorevIO.getChdrnum(), covrlnbIO.getChdrnum())
		|| isNE(fpcorevIO.getChdrcoy(), covrlnbIO.getChdrcoy())
		|| isNE(fpcorevIO.getCoverage(), covrlnbIO.getCoverage())
		|| isNE(fpcorevIO.getLife(), covrlnbIO.getLife())
		|| isNE(fpcorevIO.getRider(), covrlnbIO.getRider())
		|| isNE(fpcorevIO.getPlanSuffix(), covrlnbIO.getPlanSuffix())) {
			/*    MOVE RLSE                  TO FPCOREV-FUNCTION            */
			/*    CALL 'FPCOREVIO'           USING FPCOREV-PARAMS           */
			/*    IF FPCOREV-STATUZ          NOT = O-K                      */
			/*       MOVE FPCOREV-PARAMS     TO SYSR-PARAMS                 */
			/*    END-IF                                                    */
			fpcorevIO.setStatuz(varcom.endp);
		}
	}

protected void processCommission2300()
	{
		go2301();
		nextAgcm2320();
	}

protected void go2301()
	{
		/* When a dormant AGCM record is encountered, do not process*/
		/* the record but rewrite it to release the record lock and go*/
		/* on to the next record.*/
		/* Also, donot reverse sp top up AGCMs (PT date = 0)    <D9604>*/
		/* REWRT is not necessary,as BEGNH is replaced by BEGN,record      */
		/* is not locked.                                                  */
		if (isEQ(agcmbchIO.getDormantFlag(), "Y")
		|| isEQ(agcmbchIO.getPtdate(), 0)) {
			/*     MOVE REWRT              TO AGCMBCH-FUNCTION              */
			/*     MOVE AGCMBCHREC         TO AGCMBCH-FORMAT                */
			/*     CALL 'AGCMBCHIO'        USING AGCMBCH-PARAMS             */
			/*     IF AGCMBCH-STATUZ    NOT = O-K                           */
			/*         MOVE AGCMBCH-STATUZ TO SYSR-STATUZ                   */
			/*         MOVE AGCMBCH-PARAMS TO SYSR-PARAMS                   */
			/*     END-IF                                                   */
			return ;
		}
		if (isGT(agcmbchIO.getEfdate(), reverserec.effdate1)
		|| isLT(agcmbchIO.getCurrto(), reverserec.effdate1)) {
			/*     MOVE REWRT              TO AGCMBCH-FUNCTION              */
			/*     MOVE AGCMBCHREC         TO AGCMBCH-FORMAT                */
			/*     CALL 'AGCMBCHIO'        USING AGCMBCH-PARAMS             */
			/*     IF AGCMBCH-STATUZ    NOT = O-K                           */
			/*         MOVE AGCMBCH-STATUZ TO SYSR-STATUZ                   */
			/*         MOVE AGCMBCH-PARAMS TO SYSR-PARAMS                   */
			/*     END-IF                                                   */
			return ;
		}
		/*                                                         <D9604>*/
		/* Donot process AGCM any further if the amount we are     <D9604>*/
		/* reversing is all over target and the seqno is > 1 as we <D9604>*/
		/* all over target to the first AGCM seqno.                <D9604>*/
		/*                                                         <D9604>*/
		if (isEQ(wsaaUpToTarget, 0)
		&& isGT(agcmbchIO.getSeqno(), 1)) {
			/*     MOVE REWRT              TO AGCMBCH-FUNCTION      <LA3993>*/
			/*     MOVE AGCMBCHREC         TO AGCMBCH-FORMAT        <LA3993>*/
			/*     CALL 'AGCMBCHIO'        USING AGCMBCH-PARAMS     <LA3993>*/
			/*     IF AGCMBCH-STATUZ    NOT = O-K                   <LA3993>*/
			/*         MOVE AGCMBCH-STATUZ TO SYSR-STATUZ           <LA3993>*/
			/*         MOVE AGCMBCH-PARAMS TO SYSR-PARAMS           <LA3993>*/
			/*     END-IF                                           <LA3993>*/
			return ;
		}
		/* set up common linkage for the commission reversal subroutine*/
		comlinkrec.agent.set(agcmbchIO.getAgntnum());
		comlinkrec.chdrcoy.set(reverserec.company);
		comlinkrec.chdrnum.set(reverserec.chdrnum);
		comlinkrec.life.set(agcmbchIO.getLife());
		comlinkrec.coverage.set(agcmbchIO.getCoverage());
		comlinkrec.rider.set(agcmbchIO.getRider());
		comlinkrec.planSuffix.set(agcmbchIO.getPlanSuffix());
		comlinkrec.crtable.set(covrlnbIO.getCrtable());
		comlinkrec.jlife.set(covrlnbIO.getJlife());
		comlinkrec.agentClass.set(agcmbchIO.getAgentClass());
		comlinkrec.effdate.set(agcmbchIO.getEfdate());
		comlinkrec.billfreq.set(payrIO.getBillfreq());
		comlinkrec.annprem.set(agcmbchIO.getAnnprem());
		comlinkrec.targetPrem.set(fpcorevIO.getTargetPremium());
		comlinkrec.currto.set(fpcorevIO.getTargto());
		comlinkrec.seqno.set(agcmbchIO.getSeqno());
		comlinkrec.ptdate.set(0);
		comlinkrec.language.set(reverserec.language);
		for (wsaaCommType.set(1); !(isEQ(wsaaCommType, 4)); wsaaCommType.add(1)){
			callSubroutine2500();
		}
		rewriteAgcm2800();
	}

protected void nextAgcm2320()
	{
		agcmbchIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), varcom.endp)
		&& isNE(agcmbchIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			dbError8100();
		}
		if (isNE(agcmbchIO.getChdrnum(), fpcorevIO.getChdrnum())
		|| isNE(agcmbchIO.getChdrcoy(), fpcorevIO.getChdrcoy())
		|| isNE(agcmbchIO.getCoverage(), fpcorevIO.getCoverage())
		|| isNE(agcmbchIO.getLife(), fpcorevIO.getLife())
		|| isNE(agcmbchIO.getRider(), fpcorevIO.getRider())
		|| isNE(agcmbchIO.getPlanSuffix(), fpcorevIO.getPlanSuffix())) {
			/*    MOVE REWRT                TO AGCMBCH-FUNCTION             */
			/*    CALL 'AGCMBCHIO'          USING AGCMBCH-PARAMS            */
			/*    IF AGCMBCH-STATUZ         NOT = O-K                       */
			/*       MOVE AGCMBCH-PARAMS    TO SYSR-PARAMS                  */
			/*    END-IF                                                    */
			agcmbchIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void callSubroutine2500()
	{
		go2501();
	}

protected void go2501()
	{
		wsaaPayamnt[wsaaCommType.toInt()].set(ZERO);
		wsaaErndamt[wsaaCommType.toInt()].set(ZERO);
		/* set up linkage for the commission reversal subroutine*/
		comlinkrec.function.set(SPACES);
		comlinkrec.statuz.set(SPACES);
		comlinkrec.icommtot.set(ZERO);
		comlinkrec.icommpd.set(ZERO);
		comlinkrec.icommernd.set(ZERO);
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		comlinkrec.instprem.set(ZERO);
		if (isEQ(wsaaCommType, 1)
		&& isEQ(wsaaUpToTarget, ZERO)) {
			return ;
		}
		if (isEQ(wsaaCommType, 1)) {
			if (isEQ(agcmbchIO.getBascpy(), SPACES)
			|| (isEQ(agcmbchIO.getCompay(), ZERO)
			&& isEQ(agcmbchIO.getComern(), ZERO))) {
				return ;
			}
			else {
				itemIO.setItemitem(agcmbchIO.getBascpy());
			}
		}
		if (isEQ(wsaaCommType, 2)) {
			if (isEQ(agcmbchIO.getRnwcpy(), SPACES)
			|| isEQ(agcmbchIO.getRnlcdue(), ZERO)
			|| isEQ(wsaaUpToTarget, ZERO)) {
				return ;
			}
			else {
				itemIO.setItemitem(agcmbchIO.getRnwcpy());
			}
		}
		if (isEQ(wsaaCommType, 3)) {
			if (isEQ(agcmbchIO.getSrvcpy(), SPACES)
			|| isEQ(agcmbchIO.getScmdue(), ZERO)
			|| isEQ(wsaaOverTarget, ZERO)
			|| isGT(agcmbchIO.getSeqno(), 1)) {
				return ;
			}
			else {
				itemIO.setItemitem(agcmbchIO.getSrvcpy());
			}
		}
		comlinkrec.method.set(itemIO.getItemitem());
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t5644);
		itemIO.setItempfx("IT");
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), "****")) {
			syserrrec.params.set(itemIO.getParams());
			systemError8000();
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		if (isEQ(t5644rec.subrev, SPACES)) {
			return ;
		}
		if (isEQ(wsaaCommType, 1)) {
			comlinkrec.instprem.set(wsaaUpToTarget);
			comlinkrec.icommtot.set(agcmbchIO.getInitcom());
			comlinkrec.icommpd.set(agcmbchIO.getCompay());
			comlinkrec.icommernd.set(agcmbchIO.getComern());
		}
		else {
			if (isEQ(wsaaCommType, 2)) {
				comlinkrec.instprem.set(wsaaUpToTarget);
			}
			else {
				comlinkrec.instprem.set(wsaaOverTarget);
			}
		}
		callProgram(t5644rec.subrev, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			systemError8000();
		}
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		callRounding9000();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		callRounding9000();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		wsaaPayamnt[wsaaCommType.toInt()].set(comlinkrec.payamnt);
		wsaaErndamt[wsaaCommType.toInt()].set(comlinkrec.erndamt);
	}

protected void rewriteAgcm2800()
	{
		go2801();
	}

protected void go2801()
	{
		if (isNE(agcmbchIO.getBascpy(), SPACES)) {
			setPrecision(agcmbchIO.getCompay(), 2);
			agcmbchIO.setCompay(add(agcmbchIO.getCompay(), wsaaPayamnt[1]));
			setPrecision(agcmbchIO.getComern(), 2);
			agcmbchIO.setComern(add(agcmbchIO.getComern(), wsaaErndamt[1]));
		}
		if (isNE(agcmbchIO.getRnwcpy(), SPACES)) {
			setPrecision(agcmbchIO.getRnlcdue(), 2);
			agcmbchIO.setRnlcdue(sub(agcmbchIO.getRnlcdue(), wsaaPayamnt[2]));
			setPrecision(agcmbchIO.getRnlcearn(), 2);
			agcmbchIO.setRnlcearn(sub(agcmbchIO.getRnlcearn(), wsaaErndamt[2]));
		}
		if (isNE(agcmbchIO.getSrvcpy(), SPACES)) {
			setPrecision(agcmbchIO.getScmdue(), 2);
			agcmbchIO.setScmdue(sub(agcmbchIO.getScmdue(), wsaaPayamnt[3]));
			setPrecision(agcmbchIO.getScmearn(), 2);
			agcmbchIO.setScmearn(sub(agcmbchIO.getScmearn(), wsaaErndamt[3]));
		}
		agcmbchIO.setPtdate(payrIO.getPtdate());
		/* MOVE REWRT                  TO AGCMBCH-FUNCTION.             */
		agcmbchIO.setFunction(varcom.writd);
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			dbError8100();
		}
	}

protected void reverseUnits3000()
	{
		start3000();
	}

protected void start3000()
	{
		/* Call unit reversal subroutine using first coverage only -*/
		/* the subroutine reverses all UTRNS for the contract in one*/
		/* pass.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t5671);
		wsaaT5671Batc.set(reverserec.oldBatctrcde);
		wsaaT5671Crtable.set(wsaaStoredCrtable);
		itemIO.setItemitem(wsaaT5671);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError8000();
		}
		if (isEQ(itemIO.getStatuz(), SPACES)) {
			t5671rec.t5671Rec.set(SPACES);
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		initialize(greversrec.reverseRec);
		greversrec.effdate.set(varcom.vrcmMaxDate);
		greversrec.transDate.set(varcom.vrcmMaxDate);
		greversrec.chdrcoy.set(reverserec.company);
		greversrec.chdrnum.set(reverserec.chdrnum);
		greversrec.tranno.set(reverserec.tranno);
		greversrec.planSuffix.set(reverserec.planSuffix);
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.coverage.set(reverserec.coverage);
		greversrec.rider.set(reverserec.rider);
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.effdate.set(reverserec.ptrneff);
		greversrec.language.set(reverserec.language);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
			callSubprog3100();
		}
	}

protected void callSubprog3100()
	{
		/*CALL*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()], SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(greversrec.statuz);
				systemError8000();
			}
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*   Read paid LINS for a contract
	* </pre>
	*/
protected void deleteLins3200()
	{
		para3201();
	}

protected void para3201()
	{
		linsfprIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, linsfprIO);
		if (isNE(linsfprIO.getStatuz(), varcom.oK)
		&& isNE(linsfprIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(linsfprIO.getParams());
			dbError8100();
		}
		/* Delete TAXDREV                                                  */
		taxdrevIO.setParams(SPACES);
		taxdrevIO.setChdrcoy(reverserec.company);
		taxdrevIO.setChdrnum(reverserec.chdrnum);
		taxdrevIO.setEffdate(ZERO);
		taxdrevIO.setFunction(varcom.begn);
		while ( !(isEQ(taxdrevIO.getStatuz(), varcom.endp))) {
			deleteTaxdrev3300();
		}
		
	}

protected void deleteTaxdrev3300()
	{
		start3310();
	}

protected void start3310()
	{
		SmartFileCode.execute(appVars, taxdrevIO);
		if (isNE(taxdrevIO.getStatuz(), varcom.oK)
		&& isNE(taxdrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(taxdrevIO.getParams());
			syserrrec.statuz.set(taxdrevIO.getStatuz());
			dbError8100();
		}
		if (isNE(reverserec.company, taxdrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, taxdrevIO.getChdrnum())
		|| isEQ(taxdrevIO.getStatuz(), varcom.endp)) {
			taxdrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(reverserec.company, taxdrevIO.getChdrcoy())
		&& isEQ(reverserec.chdrnum, taxdrevIO.getChdrnum())
		&& isEQ(linsfprIO.getInstfrom(), taxdrevIO.getInstfrom())
		&& isEQ(linsfprIO.getInstto(), taxdrevIO.getInstto())
		&& isEQ(taxdrevIO.getPostflg(), "P")) {
			taxdrevIO.setStatuz(varcom.oK);
			taxdrevIO.setFunction(varcom.deltd);
			SmartFileCode.execute(appVars, taxdrevIO);
			if (isNE(taxdrevIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(taxdrevIO.getParams());
				syserrrec.statuz.set(taxdrevIO.getStatuz());
				dbError8100();
			}
		}
		taxdrevIO.setFunction(varcom.nextr);
	}

protected void processZptn4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					proc4010();
				case call4020: 
					call4020();
					writ4030();
					nextr4080();
				case exit4090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void proc4010()
	{
		zptnrevIO.setDataArea(SPACES);
		zptnrevIO.setChdrcoy(reverserec.company);
		zptnrevIO.setChdrnum(reverserec.chdrnum);
		zptnrevIO.setTranno(reverserec.tranno);
		zptnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zptnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		zptnrevIO.setFormat(formatsInner.zptnrevrec);
	}

protected void call4020()
	{
		SmartFileCode.execute(appVars, zptnrevIO);
		if (isNE(zptnrevIO.getStatuz(), varcom.oK)
		&& isNE(zptnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zptnrevIO.getStatuz());
			syserrrec.params.set(zptnrevIO.getParams());
			dbError8100();
		}
		if (isEQ(zptnrevIO.getStatuz(), varcom.endp)
		|| isNE(zptnrevIO.getChdrcoy(), reverserec.company)
		|| isNE(zptnrevIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(zptnrevIO.getTranno(), reverserec.tranno)) {
			zptnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit4090);
		}
	}

protected void writ4030()
	{
		zptnIO.setDataArea(SPACES);
		zptnIO.setChdrcoy(zptnrevIO.getChdrcoy());
		zptnIO.setChdrnum(zptnrevIO.getChdrnum());
		zptnIO.setLife(zptnrevIO.getLife());
		zptnIO.setCoverage(zptnrevIO.getCoverage());
		zptnIO.setRider(zptnrevIO.getRider());
		zptnIO.setTranno(reverserec.newTranno);
		setPrecision(zptnIO.getOrigamt(), 2);
		zptnIO.setOrigamt(mult(zptnrevIO.getOrigamt(), -1));
		zptnIO.setTransCode(reverserec.batctrcde);
		zptnIO.setEffdate(zptnrevIO.getEffdate());
		zptnIO.setBillcd(zptnrevIO.getBillcd());
		zptnIO.setInstfrom(zptnrevIO.getInstfrom());
		zptnIO.setInstto(zptnrevIO.getInstto());
		zptnIO.setTrandate(wsaaToday);
		zptnIO.setZprflg(zptnrevIO.getZprflg());
		zptnIO.setFormat(formatsInner.zptnrec);
		zptnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zptnIO);
		if (isNE(zptnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zptnIO.getStatuz());
			syserrrec.params.set(zptnIO.getParams());
			dbError8100();
		}
	}

protected void nextr4080()
	{
		zptnrevIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call4020);
	}

protected void processZctn5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					proc5010();
				case call5020: 
					call5020();
					writ5030();
					nextr5080();
				case exit5090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void proc5010()
	{
		zctnrevIO.setDataArea(SPACES);
		zctnrevIO.setChdrcoy(reverserec.company);
		zctnrevIO.setChdrnum(reverserec.chdrnum);
		zctnrevIO.setTranno(reverserec.tranno);
		zctnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zctnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		zctnrevIO.setFormat(formatsInner.zctnrevrec);
	}

protected void call5020()
	{
		SmartFileCode.execute(appVars, zctnrevIO);
		if (isNE(zctnrevIO.getStatuz(), varcom.oK)
		&& isNE(zctnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zctnrevIO.getStatuz());
			syserrrec.params.set(zctnrevIO.getParams());
			dbError8100();
		}
		if (isEQ(zctnrevIO.getStatuz(), varcom.endp)
		|| isNE(zctnrevIO.getChdrcoy(), reverserec.company)
		|| isNE(zctnrevIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(zctnrevIO.getTranno(), reverserec.tranno)) {
			zctnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit5090);
		}
	}

protected void writ5030()
	{
		zctnIO.setDataArea(SPACES);
		zctnIO.setAgntcoy(zctnrevIO.getAgntcoy());
		zctnIO.setAgntnum(zctnrevIO.getAgntnum());
		zctnIO.setChdrcoy(zctnrevIO.getChdrcoy());
		zctnIO.setChdrnum(zctnrevIO.getChdrnum());
		zctnIO.setLife(zctnrevIO.getLife());
		zctnIO.setCoverage(zctnrevIO.getCoverage());
		zctnIO.setRider(zctnrevIO.getRider());
		zctnIO.setTranno(reverserec.newTranno);
		setPrecision(zctnIO.getCommAmt(), 2);
		zctnIO.setCommAmt(mult(zctnrevIO.getCommAmt(), -1));
		setPrecision(zctnIO.getPremium(), 2);
		zctnIO.setPremium(mult(zctnrevIO.getPremium(), -1));
		zctnIO.setSplitBcomm(zctnrevIO.getSplitBcomm());
		zctnIO.setTransCode(reverserec.batctrcde);
		zctnIO.setEffdate(zctnrevIO.getEffdate());
		zctnIO.setTrandate(wsaaToday);
		zctnIO.setZprflg(zctnrevIO.getZprflg());
		zctnIO.setFormat(formatsInner.zctnrec);
		zctnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zctnIO);
		if (isNE(zctnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zctnIO.getStatuz());
			syserrrec.params.set(zctnIO.getParams());
			dbError8100();
		}
	}

protected void nextr5080()
	{
		zctnrevIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call5020);
	}

protected void readAcmvs10000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					go10001();
				case nextAcmv10020: 
					nextAcmv10020();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go10001()
	{
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.batcactmn.set(reverserec.batcactmn);
		lifacmvrec.batcactyr.set(reverserec.batcactyr);
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), -1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), -1));
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.termid.set(acmvrevIO.getTermid());
		lifacmvrec.user.set(acmvrevIO.getUser());
		lifacmvrec.transactionTime.set(acmvrevIO.getTransactionTime());
		lifacmvrec.transactionDate.set(acmvrevIO.getTransactionDate());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		wsaaPostyear.set(reverserec.batcactyr);
		wsaaPostmonth.set(reverserec.batcactmn);
		lifacmvrec.postyear.set(wsaaPostyearX);
		lifacmvrec.postmonth.set(wsaaPostmonthX);
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError8000();
		}
	}

protected void nextAcmv10020()
	{
		/*  read the next acmv record.*/
		acmvrevIO.setFunction(varcom.nextr);
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			dbError8100();
		}
		if (isNE(acmvrevIO.getRldgcoy(), reverserec.company)
		|| isNE(acmvrevIO.getRdocnum(), reverserec.chdrnum)
		|| isNE(acmvrevIO.getTranno(), reverserec.tranno)
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			goTo(GotoLabel.nextAcmv10020);
		}
	}

protected void systemError8000()
	{
		se8000();
		seExit8090();
	}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		reverserec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
		db8100();
		dbExit8190();
	}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		reverserec.statuz.set(varcom.bomb);
		exit090();
	}

protected void callRounding9000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(reverserec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(acmvrevIO.getOrigcurr());
		zrdecplrec.batctrcde.set(reverserec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError8000();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData agcmbchrec = new FixedLengthStringData(10).init("AGCMBCHREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
	private FixedLengthStringData fprmrevrec = new FixedLengthStringData(10).init("FPRMREVREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData zptnrec = new FixedLengthStringData(10).init("ZPTNREC");
	private FixedLengthStringData zctnrec = new FixedLengthStringData(10).init("ZCTNREC");
	private FixedLengthStringData zptnrevrec = new FixedLengthStringData(10).init("ZPTNREVREC");
	private FixedLengthStringData zctnrevrec = new FixedLengthStringData(10).init("ZCTNREVREC");
}
}
