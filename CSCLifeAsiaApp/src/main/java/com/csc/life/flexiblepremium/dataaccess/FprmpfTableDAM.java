package com.csc.life.flexiblepremium.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: FprmpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:54
 * Class transformed from FPRMPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class FprmpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 100;
	public FixedLengthStringData fprmrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData fprmpfRecord = fprmrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(fprmrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(fprmrec);
	public PackedDecimalData payrseqno = DD.payrseqno.copy().isAPartOf(fprmrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(fprmrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(fprmrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(fprmrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(fprmrec);
	public PackedDecimalData totalRecd = DD.totrecd.copy().isAPartOf(fprmrec);
	public PackedDecimalData totalBilled = DD.totbill.copy().isAPartOf(fprmrec);
	public PackedDecimalData minPrmReqd = DD.minreqd.copy().isAPartOf(fprmrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(fprmrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(fprmrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(fprmrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public FprmpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for FprmpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public FprmpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for FprmpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public FprmpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for FprmpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public FprmpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("FPRMPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"PAYRSEQNO, " +
							"VALIDFLAG, " +
							"CURRFROM, " +
							"CURRTO, " +
							"TRANNO, " +
							"TOTRECD, " +
							"TOTBILL, " +
							"MINREQD, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     payrseqno,
                                     validflag,
                                     currfrom,
                                     currto,
                                     tranno,
                                     totalRecd,
                                     totalBilled,
                                     minPrmReqd,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		payrseqno.clear();
  		validflag.clear();
  		currfrom.clear();
  		currto.clear();
  		tranno.clear();
  		totalRecd.clear();
  		totalBilled.clear();
  		minPrmReqd.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getFprmrec() {
  		return fprmrec;
	}

	public FixedLengthStringData getFprmpfRecord() {
  		return fprmpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setFprmrec(what);
	}

	public void setFprmrec(Object what) {
  		this.fprmrec.set(what);
	}

	public void setFprmpfRecord(Object what) {
  		this.fprmpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(fprmrec.getLength());
		result.set(fprmrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}