package com.csc.life.flexiblepremium.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: FpcopfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:54
 * Class transformed from FPCOPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class FpcopfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 140;
	public FixedLengthStringData fpcorec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData fpcopfRecord = fpcorec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(fpcorec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(fpcorec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(fpcorec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(fpcorec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(fpcorec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(fpcorec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(fpcorec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(fpcorec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(fpcorec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(fpcorec);
	public PackedDecimalData targetPremium = DD.prmper.copy().isAPartOf(fpcorec);
	public PackedDecimalData premRecPer = DD.prmrcdp.copy().isAPartOf(fpcorec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(fpcorec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(fpcorec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(fpcorec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(fpcorec);
	public FixedLengthStringData activeInd = DD.actind.copy().isAPartOf(fpcorec);
	public PackedDecimalData billedInPeriod = DD.billedp.copy().isAPartOf(fpcorec);
	public PackedDecimalData overdueMin = DD.ovrminreq.copy().isAPartOf(fpcorec);
	public PackedDecimalData minOverduePer = DD.minovrpro.copy().isAPartOf(fpcorec);
	public FixedLengthStringData annProcessInd = DD.anproind.copy().isAPartOf(fpcorec);
	public PackedDecimalData targfrom = DD.targfrom.copy().isAPartOf(fpcorec);
	public PackedDecimalData targto = DD.targto.copy().isAPartOf(fpcorec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(fpcorec);
	public PackedDecimalData annivProcDate = DD.cbanpr.copy().isAPartOf(fpcorec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public FpcopfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for FpcopfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public FpcopfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for FpcopfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public FpcopfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for FpcopfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public FpcopfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("FPCOPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"VALIDFLAG, " +
							"CURRFROM, " +
							"CURRTO, " +
							"PRMPER, " +
							"PRMRCDP, " +
							"TRANNO, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"ACTIND, " +
							"BILLEDP, " +
							"OVRMINREQ, " +
							"MINOVRPRO, " +
							"ANPROIND, " +
							"TARGFROM, " +
							"TARGTO, " +
							"EFFDATE, " +
							"CBANPR, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     validflag,
                                     currfrom,
                                     currto,
                                     targetPremium,
                                     premRecPer,
                                     tranno,
                                     userProfile,
                                     jobName,
                                     datime,
                                     activeInd,
                                     billedInPeriod,
                                     overdueMin,
                                     minOverduePer,
                                     annProcessInd,
                                     targfrom,
                                     targto,
                                     effdate,
                                     annivProcDate,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		validflag.clear();
  		currfrom.clear();
  		currto.clear();
  		targetPremium.clear();
  		premRecPer.clear();
  		tranno.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		activeInd.clear();
  		billedInPeriod.clear();
  		overdueMin.clear();
  		minOverduePer.clear();
  		annProcessInd.clear();
  		targfrom.clear();
  		targto.clear();
  		effdate.clear();
  		annivProcDate.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getFpcorec() {
  		return fpcorec;
	}

	public FixedLengthStringData getFpcopfRecord() {
  		return fpcopfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setFpcorec(what);
	}

	public void setFpcorec(Object what) {
  		this.fpcorec.set(what);
	}

	public void setFpcopfRecord(Object what) {
  		this.fpcopfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(fpcorec.getLength());
		result.set(fpcorec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}