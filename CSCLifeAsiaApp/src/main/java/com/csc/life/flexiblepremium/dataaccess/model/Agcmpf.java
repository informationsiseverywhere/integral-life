/******************************************************************************
 * File Name 		: Agcmpf.java
 * Author			: smalchi2
 * Creation Date	: 10 January 2017
 * Project			: Integral Life
 * Description		: The Model Class for AGCMPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.flexiblepremium.dataaccess.model;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;

public class Agcmpf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	// Constant for database table name
	public static final String TABLE_NAME = "AGCMPF";

	//member variables for columns
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String agntnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	private Integer tranno;
	private Integer efdate;
	private BigDecimal annprem;
	private String bascmeth;
	private BigDecimal initcom;
	private String bascpy;
	private BigDecimal compay;
	private BigDecimal comern;
	private String srvcpy;
	private BigDecimal scmdue;
	private BigDecimal scmearn;
	private String rnwcpy;
	private BigDecimal rnlcdue;
	private BigDecimal rnlcearn;
	private String agcls;
	private String termid;
	private Integer trdt;
	private Integer trtm;
	private Integer userT;
	private String crtable;
	private Integer currfrom;
	private Integer currto;
	private String validflag;
	private Integer seqno;
	private Integer ptdate;
	private String cedagent;
	private String ovrdcat;
	private String dormflag;
	private String usrprf;
	private String jobnm;
	private Date datime;
	private BigDecimal initcommgst;
	private BigDecimal rnwlcommgst;

	// Constructor
	public Agcmpf ( ) {};

	public Agcmpf(Agcmpf a) {
		super();
		this.uniqueNumber = a.uniqueNumber;
		this.chdrcoy = a.chdrcoy;
		this.chdrnum = a.chdrnum;
		this.agntnum = a.agntnum;
		this.life = a.life;
		this.jlife = a.jlife;
		this.coverage = a.coverage;
		this.rider = a.rider;
		this.plnsfx = a.plnsfx;
		this.tranno = a.tranno;
		this.efdate = a.efdate;
		this.annprem = a.annprem;
		this.bascmeth = a.bascmeth;
		this.initcom = a.initcom;
		this.bascpy = a.bascpy;
		this.compay = a.compay;
		this.comern = a.comern;
		this.srvcpy = a.srvcpy;
		this.scmdue = a.scmdue;
		this.scmearn = a.scmearn;
		this.rnwcpy = a.rnwcpy;
		this.rnlcdue = a.rnlcdue;
		this.rnlcearn = a.rnlcearn;
		this.agcls = a.agcls;
		this.termid = a.termid;
		this.trdt = a.trdt;
		this.trtm = a.trtm;
		this.userT = a.userT;
		this.crtable = a.crtable;
		this.currfrom = a.currfrom;
		this.currto = a.currto;
		this.validflag = a.validflag;
		this.seqno = a.seqno;
		this.ptdate = a.ptdate;
		this.cedagent = a.cedagent;
		this.ovrdcat = a.ovrdcat;
		this.dormflag = a.dormflag;
		this.usrprf = a.usrprf;
		this.jobnm = a.jobnm;
		this.datime = a.datime;
		this.initcommgst = a.initcommgst;
		this.rnwlcommgst = a.rnwlcommgst;
	}

	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public String getChdrcoy(){
		return this.chdrcoy;
	}
	public String getChdrnum(){
		return this.chdrnum;
	}
	public String getAgntnum(){
		return this.agntnum;
	}
	public String getLife(){
		return this.life;
	}
	public String getJlife(){
		return this.jlife;
	}
	public String getCoverage(){
		return this.coverage;
	}
	public String getRider(){
		return this.rider;
	}
	public Integer getPlnsfx(){
		return this.plnsfx;
	}
	public Integer getTranno(){
		return this.tranno;
	}
	public Integer getEfdate(){
		return this.efdate;
	}
	public BigDecimal getAnnprem(){
		return this.annprem;
	}
	public String getBascmeth(){
		return this.bascmeth;
	}
	public BigDecimal getInitcom(){
		return this.initcom;
	}
	public String getBascpy(){
		return this.bascpy;
	}
	public BigDecimal getCompay(){
		return this.compay;
	}
	public BigDecimal getComern(){
		return this.comern;
	}
	public String getSrvcpy(){
		return this.srvcpy;
	}
	public BigDecimal getScmdue(){
		return this.scmdue;
	}
	public BigDecimal getScmearn(){
		return this.scmearn;
	}
	public String getRnwcpy(){
		return this.rnwcpy;
	}
	public BigDecimal getRnlcdue(){
		return this.rnlcdue;
	}
	public BigDecimal getRnlcearn(){
		return this.rnlcearn;
	}
	public String getAgcls(){
		return this.agcls;
	}
	public String getTermid(){
		return this.termid;
	}
	public Integer getTrdt(){
		return this.trdt;
	}
	public Integer getTrtm(){
		return this.trtm;
	}
	public Integer getUserT(){
		return this.userT;
	}
	public String getCrtable(){
		return this.crtable;
	}
	public Integer getCurrfrom(){
		return this.currfrom;
	}
	public Integer getCurrto(){
		return this.currto;
	}
	public String getValidflag(){
		return this.validflag;
	}
	public Integer getSeqno(){
		return this.seqno;
	}
	public Integer getPtdate(){
		return this.ptdate;
	}
	public String getCedagent(){
		return this.cedagent;
	}
	public String getOvrdcat(){
		return this.ovrdcat;
	}
	public String getDormflag(){
		return this.dormflag;
	}
	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public Date getDatime(){
		return new Date(this.datime.getTime());//IJTI-316
	}
	public BigDecimal getInitcommgst(){
		return this.initcommgst;
	}
	public BigDecimal getRnwlcommgst(){
		return this.rnwlcommgst;
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setChdrcoy( String chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setChdrnum( String chdrnum ){
		 this.chdrnum = chdrnum;
	}
	public void setAgntnum( String agntnum ){
		 this.agntnum = agntnum;
	}
	public void setLife( String life ){
		 this.life = life;
	}
	public void setJlife( String jlife ){
		 this.jlife = jlife;
	}
	public void setCoverage( String coverage ){
		 this.coverage = coverage;
	}
	public void setRider( String rider ){
		 this.rider = rider;
	}
	public void setPlnsfx( Integer plnsfx ){
		 this.plnsfx = plnsfx;
	}
	public void setTranno( Integer tranno ){
		 this.tranno = tranno;
	}
	public void setEfdate( Integer efdate ){
		 this.efdate = efdate;
	}
	public void setAnnprem( BigDecimal annprem ){
		 this.annprem = annprem;
	}
	public void setBascmeth( String bascmeth ){
		 this.bascmeth = bascmeth;
	}
	public void setInitcom( BigDecimal initcom ){
		 this.initcom = initcom;
	}
	public void setBascpy( String bascpy ){
		 this.bascpy = bascpy;
	}
	public void setCompay( BigDecimal compay ){
		 this.compay = compay;
	}
	public void setComern( BigDecimal comern ){
		 this.comern = comern;
	}
	public void setSrvcpy( String srvcpy ){
		 this.srvcpy = srvcpy;
	}
	public void setScmdue( BigDecimal scmdue ){
		 this.scmdue = scmdue;
	}
	public void setScmearn( BigDecimal scmearn ){
		 this.scmearn = scmearn;
	}
	public void setRnwcpy( String rnwcpy ){
		 this.rnwcpy = rnwcpy;
	}
	public void setRnlcdue( BigDecimal rnlcdue ){
		 this.rnlcdue = rnlcdue;
	}
	public void setRnlcearn( BigDecimal rnlcearn ){
		 this.rnlcearn = rnlcearn;
	}
	public void setAgcls( String agcls ){
		 this.agcls = agcls;
	}
	public void setTermid( String termid ){
		 this.termid = termid;
	}
	public void setTrdt( Integer trdt ){
		 this.trdt = trdt;
	}
	public void setTrtm( Integer trtm ){
		 this.trtm = trtm;
	}
	public void setUserT( Integer userT ){
		 this.userT = userT;
	}
	public void setCrtable( String crtable ){
		 this.crtable = crtable;
	}
	public void setCurrfrom( Integer currfrom ){
		 this.currfrom = currfrom;
	}
	public void setCurrto( Integer currto ){
		 this.currto = currto;
	}
	public void setValidflag( String validflag ){
		 this.validflag = validflag;
	}
	public void setSeqno( Integer seqno ){
		 this.seqno = seqno;
	}
	public void setPtdate( Integer ptdate ){
		 this.ptdate = ptdate;
	}
	public void setCedagent( String cedagent ){
		 this.cedagent = cedagent;
	}
	public void setOvrdcat( String ovrdcat ){
		 this.ovrdcat = ovrdcat;
	}
	public void setDormflag( String dormflag ){
		 this.dormflag = dormflag;
	}
	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime( Date datime ){
		this.datime = new Date(datime.getTime());//IJTI-314
	}
	public void setInitcommgst( BigDecimal initcommgst ){
		 this.initcommgst = initcommgst;
	}
	public void setRnwlcommgst( BigDecimal rnwlcommgst ){
		 this.rnwlcommgst = rnwlcommgst;
	}

	// ToString method
	public String toString(){

		StringBuilder output = new StringBuilder();

		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("CHDRCOY:		");
		output.append(getChdrcoy());
		output.append("\r\n");
		output.append("CHDRNUM:		");
		output.append(getChdrnum());
		output.append("\r\n");
		output.append("AGNTNUM:		");
		output.append(getAgntnum());
		output.append("\r\n");
		output.append("LIFE:		");
		output.append(getLife());
		output.append("\r\n");
		output.append("JLIFE:		");
		output.append(getJlife());
		output.append("\r\n");
		output.append("COVERAGE:		");
		output.append(getCoverage());
		output.append("\r\n");
		output.append("RIDER:		");
		output.append(getRider());
		output.append("\r\n");
		output.append("PLNSFX:		");
		output.append(getPlnsfx());
		output.append("\r\n");
		output.append("TRANNO:		");
		output.append(getTranno());
		output.append("\r\n");
		output.append("EFDATE:		");
		output.append(getEfdate());
		output.append("\r\n");
		output.append("ANNPREM:		");
		output.append(getAnnprem());
		output.append("\r\n");
		output.append("BASCMETH:		");
		output.append(getBascmeth());
		output.append("\r\n");
		output.append("INITCOM:		");
		output.append(getInitcom());
		output.append("\r\n");
		output.append("BASCPY:		");
		output.append(getBascpy());
		output.append("\r\n");
		output.append("COMPAY:		");
		output.append(getCompay());
		output.append("\r\n");
		output.append("COMERN:		");
		output.append(getComern());
		output.append("\r\n");
		output.append("SRVCPY:		");
		output.append(getSrvcpy());
		output.append("\r\n");
		output.append("SCMDUE:		");
		output.append(getScmdue());
		output.append("\r\n");
		output.append("SCMEARN:		");
		output.append(getScmearn());
		output.append("\r\n");
		output.append("RNWCPY:		");
		output.append(getRnwcpy());
		output.append("\r\n");
		output.append("RNLCDUE:		");
		output.append(getRnlcdue());
		output.append("\r\n");
		output.append("RNLCEARN:		");
		output.append(getRnlcearn());
		output.append("\r\n");
		output.append("AGCLS:		");
		output.append(getAgcls());
		output.append("\r\n");
		output.append("TERMID:		");
		output.append(getTermid());
		output.append("\r\n");
		output.append("TRDT:		");
		output.append(getTrdt());
		output.append("\r\n");
		output.append("TRTM:		");
		output.append(getTrtm());
		output.append("\r\n");
		output.append("USER_T:		");
		output.append(getUserT());
		output.append("\r\n");
		output.append("CRTABLE:		");
		output.append(getCrtable());
		output.append("\r\n");
		output.append("CURRFROM:		");
		output.append(getCurrfrom());
		output.append("\r\n");
		output.append("CURRTO:		");
		output.append(getCurrto());
		output.append("\r\n");
		output.append("VALIDFLAG:		");
		output.append(getValidflag());
		output.append("\r\n");
		output.append("SEQNO:		");
		output.append(getSeqno());
		output.append("\r\n");
		output.append("PTDATE:		");
		output.append(getPtdate());
		output.append("\r\n");
		output.append("CEDAGENT:		");
		output.append(getCedagent());
		output.append("\r\n");
		output.append("OVRDCAT:		");
		output.append(getOvrdcat());
		output.append("\r\n");
		output.append("DORMFLAG:		");
		output.append(getDormflag());
		output.append("\r\n");
		output.append("USRPRF:		");
		output.append(getUsrprf());
		output.append("\r\n");
		output.append("JOBNM:		");
		output.append(getJobnm());
		output.append("\r\n");
		output.append("DATIME:		");
		output.append(getDatime());
		output.append("\r\n");
		output.append(getInitcommgst());
		output.append("\r\n");
		output.append(getRnwlcommgst());
		output.append("\r\n");

		return output.toString();
	}

}
