/*
 * File: B6687.java
 * Date: 29 August 2009 21:24:10
 * Author: Quipoz Limited
 * 
 * Class transformed from B6687.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.life.flexiblepremium.reports.R5363Report;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* COMPILE-OPTIONS-SQL   CSRSQLCSR(*ENDJOB)        <Do not delete>
*
*
*    Report on Undertarget Flexible Premium.
*
*   (Reporting previously carried out in B5363 (OVERDUE)).
*
*    Extract those records to be reported on from the FREP file
*    written in B5363.
*
*    CONTROL TOTALS
*
*    01  -  No. of records read.
*    02  -  No. report entries written.
*
***********************************************************************
* </pre>
*/
public class B6687 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlfrepCursorrs = null;
	private java.sql.PreparedStatement sqlfrepCursorps = null;
	private java.sql.Connection sqlfrepCursorconn = null;
	private String sqlfrepCursor = "";
	private R5363Report printerFile = new R5363Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B6687");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaContractStart = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaContractEnd = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaProcflg = new FixedLengthStringData(1).init("O");
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaKeyChange = new FixedLengthStringData(1).init("N");
	private Validator keyChange = new Validator(wsaaKeyChange, "Y");
	private PackedDecimalData wsaaTotReqd = new PackedDecimalData(18, 2).init(ZERO);

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);

	private FixedLengthStringData r5363H01 = new FixedLengthStringData(31);
	private FixedLengthStringData r5363h01O = new FixedLengthStringData(31).isAPartOf(r5363H01, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(r5363h01O, 0);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(r5363h01O, 1);

	private FixedLengthStringData r5363D02 = new FixedLengthStringData(18);
	private FixedLengthStringData r5363d02O = new FixedLengthStringData(18).isAPartOf(r5363D02, 0);
	private ZonedDecimalData rd02Minreqd = new ZonedDecimalData(18, 2).isAPartOf(r5363d02O, 0);

	private FixedLengthStringData r5363T01 = new FixedLengthStringData(1);
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";
	private static final String t1693 = "T1693";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler3, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler3, 8);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private DescTableDAM descIO = new DescTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private P6671par p6671par = new P6671par();
	private R5363D01Inner r5363D01Inner = new R5363D01Inner();
	private SqlFreppfInner sqlFreppfInner = new SqlFreppfInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endOfCursor2080, 
		exit2090
	}

	public B6687() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		wsspEdterror.set(varcom.oK);
		printerFile.openOutput();
		rh01Company.set(SPACES);
		r5363D01Inner.rd01Chdrnum.set(SPACES);
		r5363D01Inner.rd01Life.set(SPACES);
		r5363D01Inner.rd01Coverage.set(SPACES);
		r5363D01Inner.rd01Rider.set(SPACES);
		r5363D01Inner.rd01Plnsfx.set(ZERO);
		/* Get company description for report header.*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		rh01Companynm.set(descIO.getLongdesc());
		rh01Company.set(bsprIO.getCompany());
		wsaaCompany.set(bsprIO.getCompany());
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum,SPACES)) {
			wsaaContractStart.set(LOVALUE);
		}
		else {
			wsaaContractStart.set(p6671par.chdrnum);
		}
		if (isEQ(p6671par.chdrnum1,SPACES)) {
			wsaaContractEnd.set(HIVALUE);
		}
		else {
			wsaaContractEnd.set(p6671par.chdrnum1);
		}
		sqlfrepCursor = " SELECT  COMPANY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, TARGTO, PRMPER, BILLEDP, PRMRCDP, OVRMINREQ, MINOVRPRO, PROCFLAG, TARGFROM" +
" FROM   " + getAppVars().getTableNameOverriden("FREPPF") + " " +
" WHERE COMPANY = ?" +
" AND CHDRNUM >= ?" +
" AND CHDRNUM <= ?" +
" AND PROCFLAG = ?" +
" ORDER BY COMPANY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, TARGFROM";
		sqlerrorflag = false;
		try {
			sqlfrepCursorconn = getAppVars().getDBConnectionForTable(new com.csc.life.flexiblepremium.dataaccess.FreppfTableDAM());
			sqlfrepCursorps = getAppVars().prepareStatementEmbeded(sqlfrepCursorconn, sqlfrepCursor, "FREPPF");
			getAppVars().setDBString(sqlfrepCursorps, 1, wsaaCompany);
			getAppVars().setDBString(sqlfrepCursorps, 2, wsaaContractStart);
			getAppVars().setDBString(sqlfrepCursorps, 3, wsaaContractEnd);
			getAppVars().setDBString(sqlfrepCursorps, 4, wsaaProcflg);
			sqlfrepCursorrs = getAppVars().executeQuery(sqlfrepCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case endOfCursor2080: 
					endOfCursor2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		wsspEdterror.set(varcom.oK);
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlfrepCursorrs)) {
				getAppVars().getDBObject(sqlfrepCursorrs, 1, sqlFreppfInner.sqlCompany);
				getAppVars().getDBObject(sqlfrepCursorrs, 2, sqlFreppfInner.sqlChdrnum);
				getAppVars().getDBObject(sqlfrepCursorrs, 3, sqlFreppfInner.sqlLife);
				getAppVars().getDBObject(sqlfrepCursorrs, 4, sqlFreppfInner.sqlCoverage);
				getAppVars().getDBObject(sqlfrepCursorrs, 5, sqlFreppfInner.sqlRider);
				getAppVars().getDBObject(sqlfrepCursorrs, 6, sqlFreppfInner.sqlPlnsfx);
				getAppVars().getDBObject(sqlfrepCursorrs, 7, sqlFreppfInner.sqlTargto);
				getAppVars().getDBObject(sqlfrepCursorrs, 8, sqlFreppfInner.sqlPrmper);
				getAppVars().getDBObject(sqlfrepCursorrs, 9, sqlFreppfInner.sqlBilledp);
				getAppVars().getDBObject(sqlfrepCursorrs, 10, sqlFreppfInner.sqlPrmrcdp);
				getAppVars().getDBObject(sqlfrepCursorrs, 11, sqlFreppfInner.sqlOvrminreq);
				getAppVars().getDBObject(sqlfrepCursorrs, 12, sqlFreppfInner.sqlMinovrpro);
			}
			else {
				goTo(GotoLabel.endOfCursor2080);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		goTo(GotoLabel.exit2090);
	}

protected void endOfCursor2080()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		if (isEQ(sqlFreppfInner.sqlCompany, rh01Company)
		&& isEQ(sqlFreppfInner.sqlChdrnum, r5363D01Inner.rd01Chdrnum)
		&& isEQ(sqlFreppfInner.sqlLife, r5363D01Inner.rd01Life)
		&& isEQ(sqlFreppfInner.sqlCoverage, r5363D01Inner.rd01Coverage)
		&& isEQ(sqlFreppfInner.sqlRider, r5363D01Inner.rd01Rider)
		&& isEQ(sqlFreppfInner.sqlPlnsfx, r5363D01Inner.rd01Plnsfx)) {
			wsaaKeyChange.set("N");
		}
		else {
			wsaaKeyChange.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		if (newPageReq.isTrue()) {
			writeHeader3100();
		}
		if (keyChange.isTrue()
		&& isNE(wsaaTotReqd,ZERO)) {
			writeTotal3200();
		}
		writeDetail3300();
		/*EXIT*/
	}

protected void writeHeader3100()
	{
		/*START*/
		printerRec.set(SPACES);
		printerFile.printR5363h01(r5363H01, indicArea);
		wsaaOverflow.set("N");
		/*EXIT*/
	}

protected void writeTotal3200()
	{
		/*START*/
		printerRec.set(SPACES);
		rd02Minreqd.set(wsaaTotReqd);
		wsaaTotReqd.set(ZERO);
		printerFile.printR5363d02(r5363D02, indicArea);
		/*EXIT*/
	}

protected void writeDetail3300()
	{
		start3300();
	}

protected void start3300()
	{
		printerRec.set(SPACES);
		r5363D01Inner.rd01Chdrnum.set(sqlFreppfInner.sqlChdrnum);
		r5363D01Inner.rd01Life.set(sqlFreppfInner.sqlLife);
		r5363D01Inner.rd01Coverage.set(sqlFreppfInner.sqlCoverage);
		r5363D01Inner.rd01Rider.set(sqlFreppfInner.sqlRider);
		r5363D01Inner.rd01Plnsfx.set(sqlFreppfInner.sqlPlnsfx);
		r5363D01Inner.rd01Prmper.set(sqlFreppfInner.sqlPrmper);
		r5363D01Inner.rd01Billedp.set(sqlFreppfInner.sqlBilledp);
		r5363D01Inner.rd01Prmrcdp.set(sqlFreppfInner.sqlPrmrcdp);
		r5363D01Inner.rd01Minovrpro.set(sqlFreppfInner.sqlMinovrpro);
		compute(r5363D01Inner.rd01Ovrminreq, 0).set(sub(sqlFreppfInner.sqlOvrminreq, sqlFreppfInner.sqlPrmrcdp));
		wsaaTotReqd.add(r5363D01Inner.rd01Ovrminreq);
		datcon1rec.intDate.set(sqlFreppfInner.sqlTargto);
		callDatcon13310();
		r5363D01Inner.rd01Targto.set(datcon1rec.extDate);
		printerFile.printR5363d01(r5363D01Inner.r5363D01, indicArea);
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
	}

protected void callDatcon13310()
	{
		/*START*/
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		para4010();
	}

protected void para4010()
	{
		if (newPageReq.isTrue()) {
			writeHeader3100();
		}
		if (isNE(wsaaTotReqd,ZERO)) {
			rd02Minreqd.set(wsaaTotReqd);
			wsaaTotReqd.set(ZERO);
			printerRec.set(SPACES);
			printerFile.printR5363d02(r5363D02);
		}
		printerRec.set(SPACES);
		printerFile.printR5363t01(r5363T01);
		getAppVars().freeDBConnectionIgnoreErr(sqlfrepCursorconn, sqlfrepCursorps, sqlfrepCursorrs);
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure R5363-D01--INNER
 */
private static final class R5363D01Inner { 

	private FixedLengthStringData r5363D01 = new FixedLengthStringData(99);
	private FixedLengthStringData r5363d01O = new FixedLengthStringData(99).isAPartOf(r5363D01, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(r5363d01O, 0);
	private FixedLengthStringData rd01Life = new FixedLengthStringData(2).isAPartOf(r5363d01O, 8);
	private FixedLengthStringData rd01Coverage = new FixedLengthStringData(2).isAPartOf(r5363d01O, 10);
	private FixedLengthStringData rd01Rider = new FixedLengthStringData(2).isAPartOf(r5363d01O, 12);
	private ZonedDecimalData rd01Plnsfx = new ZonedDecimalData(4, 0).isAPartOf(r5363d01O, 14);
	private FixedLengthStringData rd01Targto = new FixedLengthStringData(10).isAPartOf(r5363d01O, 18);
	private ZonedDecimalData rd01Prmper = new ZonedDecimalData(17, 2).isAPartOf(r5363d01O, 28);
	private ZonedDecimalData rd01Billedp = new ZonedDecimalData(17, 2).isAPartOf(r5363d01O, 45);
	private ZonedDecimalData rd01Prmrcdp = new ZonedDecimalData(17, 2).isAPartOf(r5363d01O, 62);
	private ZonedDecimalData rd01Ovrminreq = new ZonedDecimalData(17, 2).isAPartOf(r5363d01O, 79);
	private ZonedDecimalData rd01Minovrpro = new ZonedDecimalData(3, 0).isAPartOf(r5363d01O, 96);
}
/*
 * Class transformed  from Data Structure SQL-FREPPF--INNER
 */
private static final class SqlFreppfInner { 

	private FixedLengthStringData sqlFreppf = new FixedLengthStringData(61);
	private FixedLengthStringData sqlCompany = new FixedLengthStringData(1).isAPartOf(sqlFreppf, 0);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlFreppf, 1);
	private FixedLengthStringData sqlLife = new FixedLengthStringData(2).isAPartOf(sqlFreppf, 9);
	private FixedLengthStringData sqlCoverage = new FixedLengthStringData(2).isAPartOf(sqlFreppf, 11);
	private FixedLengthStringData sqlRider = new FixedLengthStringData(2).isAPartOf(sqlFreppf, 13);
	private PackedDecimalData sqlPlnsfx = new PackedDecimalData(4, 0).isAPartOf(sqlFreppf, 15);
	private PackedDecimalData sqlTargto = new PackedDecimalData(8, 0).isAPartOf(sqlFreppf, 18);
	private PackedDecimalData sqlPrmper = new PackedDecimalData(17, 2).isAPartOf(sqlFreppf, 23);
	private PackedDecimalData sqlBilledp = new PackedDecimalData(17, 2).isAPartOf(sqlFreppf, 32);
	private PackedDecimalData sqlPrmrcdp = new PackedDecimalData(17, 2).isAPartOf(sqlFreppf, 41);
	private PackedDecimalData sqlOvrminreq = new PackedDecimalData(17, 2).isAPartOf(sqlFreppf, 50);
	private PackedDecimalData sqlMinovrpro = new PackedDecimalData(3, 0).isAPartOf(sqlFreppf, 59);
}
}
