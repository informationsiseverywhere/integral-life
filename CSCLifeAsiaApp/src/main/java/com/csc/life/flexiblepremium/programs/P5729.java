/*
 * File: P5729.java
 * Date: 30 August 2009 0:35:18
 * Author: Quipoz Limited
 * 
 * Class transformed from P5729.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.flexiblepremium.procedures.T5729pt;
import com.csc.life.flexiblepremium.screens.S5729ScreenVars;
import com.csc.life.flexiblepremium.tablestructures.T5729rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P5729 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5729");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private T5729rec t5729rec = new T5729rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5729ScreenVars sv = ScreenProgram.getScreenVars( S5729ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public P5729() {
		super();
		screenVars = sv;
		new ScreenModel("S5729", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5729rec.t5729Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t5729rec.durationa01.set(ZERO);
		t5729rec.durationa02.set(ZERO);
		t5729rec.durationa03.set(ZERO);
		t5729rec.durationa04.set(ZERO);
		t5729rec.durationb01.set(ZERO);
		t5729rec.durationb02.set(ZERO);
		t5729rec.durationb03.set(ZERO);
		t5729rec.durationb04.set(ZERO);
		t5729rec.durationc01.set(ZERO);
		t5729rec.durationc02.set(ZERO);
		t5729rec.durationc03.set(ZERO);
		t5729rec.durationc04.set(ZERO);
		t5729rec.durationd01.set(ZERO);
		t5729rec.durationd02.set(ZERO);
		t5729rec.durationd03.set(ZERO);
		t5729rec.durationd04.set(ZERO);
		t5729rec.duratione01.set(ZERO);
		t5729rec.duratione02.set(ZERO);
		t5729rec.duratione03.set(ZERO);
		t5729rec.duratione04.set(ZERO);
		t5729rec.durationf01.set(ZERO);
		t5729rec.durationf02.set(ZERO);
		t5729rec.durationf03.set(ZERO);
		t5729rec.durationf04.set(ZERO);
		t5729rec.overdueMina01.set(ZERO);
		t5729rec.overdueMina02.set(ZERO);
		t5729rec.overdueMina03.set(ZERO);
		t5729rec.overdueMina04.set(ZERO);
		t5729rec.overdueMinb01.set(ZERO);
		t5729rec.overdueMinb02.set(ZERO);
		t5729rec.overdueMinb03.set(ZERO);
		t5729rec.overdueMinb04.set(ZERO);
		t5729rec.overdueMinc01.set(ZERO);
		t5729rec.overdueMinc02.set(ZERO);
		t5729rec.overdueMinc03.set(ZERO);
		t5729rec.overdueMinc04.set(ZERO);
		t5729rec.overdueMind01.set(ZERO);
		t5729rec.overdueMind02.set(ZERO);
		t5729rec.overdueMind03.set(ZERO);
		t5729rec.overdueMind04.set(ZERO);
		t5729rec.overdueMine01.set(ZERO);
		t5729rec.overdueMine02.set(ZERO);
		t5729rec.overdueMine03.set(ZERO);
		t5729rec.overdueMine04.set(ZERO);
		t5729rec.overdueMinf01.set(ZERO);
		t5729rec.overdueMinf02.set(ZERO);
		t5729rec.overdueMinf03.set(ZERO);
		t5729rec.overdueMinf04.set(ZERO);
		t5729rec.targetMaxa01.set(ZERO);
		t5729rec.targetMaxa02.set(ZERO);
		t5729rec.targetMaxa03.set(ZERO);
		t5729rec.targetMaxa04.set(ZERO);
		t5729rec.targetMaxb01.set(ZERO);
		t5729rec.targetMaxb02.set(ZERO);
		t5729rec.targetMaxb03.set(ZERO);
		t5729rec.targetMaxb04.set(ZERO);
		t5729rec.targetMaxc01.set(ZERO);
		t5729rec.targetMaxc02.set(ZERO);
		t5729rec.targetMaxc03.set(ZERO);
		t5729rec.targetMaxc04.set(ZERO);
		t5729rec.targetMaxd01.set(ZERO);
		t5729rec.targetMaxd02.set(ZERO);
		t5729rec.targetMaxd03.set(ZERO);
		t5729rec.targetMaxd04.set(ZERO);
		t5729rec.targetMaxe01.set(ZERO);
		t5729rec.targetMaxe02.set(ZERO);
		t5729rec.targetMaxe03.set(ZERO);
		t5729rec.targetMaxe04.set(ZERO);
		t5729rec.targetMaxf01.set(ZERO);
		t5729rec.targetMaxf02.set(ZERO);
		t5729rec.targetMaxf03.set(ZERO);
		t5729rec.targetMaxf04.set(ZERO);
		t5729rec.targetMina01.set(ZERO);
		t5729rec.targetMina02.set(ZERO);
		t5729rec.targetMina03.set(ZERO);
		t5729rec.targetMina04.set(ZERO);
		t5729rec.targetMinb01.set(ZERO);
		t5729rec.targetMinb02.set(ZERO);
		t5729rec.targetMinb03.set(ZERO);
		t5729rec.targetMinb04.set(ZERO);
		t5729rec.targetMinc01.set(ZERO);
		t5729rec.targetMinc02.set(ZERO);
		t5729rec.targetMinc03.set(ZERO);
		t5729rec.targetMinc04.set(ZERO);
		t5729rec.targetMind01.set(ZERO);
		t5729rec.targetMind02.set(ZERO);
		t5729rec.targetMind03.set(ZERO);
		t5729rec.targetMind04.set(ZERO);
		t5729rec.targetMine01.set(ZERO);
		t5729rec.targetMine02.set(ZERO);
		t5729rec.targetMine03.set(ZERO);
		t5729rec.targetMine04.set(ZERO);
		t5729rec.targetMinf01.set(ZERO);
		t5729rec.targetMinf02.set(ZERO);
		t5729rec.targetMinf03.set(ZERO);
		t5729rec.targetMinf04.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.durationas.set(t5729rec.durationas);
		sv.durationbs.set(t5729rec.durationbs);
		sv.durationcs.set(t5729rec.durationcs);
		sv.durationds.set(t5729rec.durationds);
		sv.durationes.set(t5729rec.durationes);
		sv.durationfs.set(t5729rec.durationfs);
		sv.frqcys.set(t5729rec.frqcys);
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.overdueMinas.set(t5729rec.overdueMinas);
		sv.overdueMinbs.set(t5729rec.overdueMinbs);
		sv.overdueMincs.set(t5729rec.overdueMincs);
		sv.overdueMinds.set(t5729rec.overdueMinds);
		sv.overdueMines.set(t5729rec.overdueMines);
		sv.overdueMinfs.set(t5729rec.overdueMinfs);
		sv.targetMaxas.set(t5729rec.targetMaxas);
		sv.targetMaxbs.set(t5729rec.targetMaxbs);
		sv.targetMaxcs.set(t5729rec.targetMaxcs);
		sv.targetMaxds.set(t5729rec.targetMaxds);
		sv.targetMaxes.set(t5729rec.targetMaxes);
		sv.targetMaxfs.set(t5729rec.targetMaxfs);
		sv.targetMinas.set(t5729rec.targetMinas);
		sv.targetMinbs.set(t5729rec.targetMinbs);
		sv.targetMincs.set(t5729rec.targetMincs);
		sv.targetMinds.set(t5729rec.targetMinds);
		sv.targetMines.set(t5729rec.targetMines);
		sv.targetMinfs.set(t5729rec.targetMinfs);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(t5729rec.t5729Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.durationas,t5729rec.durationas)) {
			t5729rec.durationas.set(sv.durationas);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.durationbs,t5729rec.durationbs)) {
			t5729rec.durationbs.set(sv.durationbs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.durationcs,t5729rec.durationcs)) {
			t5729rec.durationcs.set(sv.durationcs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.durationds,t5729rec.durationds)) {
			t5729rec.durationds.set(sv.durationds);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.durationes,t5729rec.durationes)) {
			t5729rec.durationes.set(sv.durationes);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.durationfs,t5729rec.durationfs)) {
			t5729rec.durationfs.set(sv.durationfs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.frqcys,t5729rec.frqcys)) {
			t5729rec.frqcys.set(sv.frqcys);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.overdueMinas,t5729rec.overdueMinas)) {
			t5729rec.overdueMinas.set(sv.overdueMinas);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.overdueMinbs,t5729rec.overdueMinbs)) {
			t5729rec.overdueMinbs.set(sv.overdueMinbs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.overdueMincs,t5729rec.overdueMincs)) {
			t5729rec.overdueMincs.set(sv.overdueMincs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.overdueMinds,t5729rec.overdueMinds)) {
			t5729rec.overdueMinds.set(sv.overdueMinds);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.overdueMines,t5729rec.overdueMines)) {
			t5729rec.overdueMines.set(sv.overdueMines);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.overdueMinfs,t5729rec.overdueMinfs)) {
			t5729rec.overdueMinfs.set(sv.overdueMinfs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.targetMaxas,t5729rec.targetMaxas)) {
			t5729rec.targetMaxas.set(sv.targetMaxas);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.targetMaxbs,t5729rec.targetMaxbs)) {
			t5729rec.targetMaxbs.set(sv.targetMaxbs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.targetMaxcs,t5729rec.targetMaxcs)) {
			t5729rec.targetMaxcs.set(sv.targetMaxcs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.targetMaxds,t5729rec.targetMaxds)) {
			t5729rec.targetMaxds.set(sv.targetMaxds);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.targetMaxes,t5729rec.targetMaxes)) {
			t5729rec.targetMaxes.set(sv.targetMaxes);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.targetMaxfs,t5729rec.targetMaxfs)) {
			t5729rec.targetMaxfs.set(sv.targetMaxfs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.targetMinas,t5729rec.targetMinas)) {
			t5729rec.targetMinas.set(sv.targetMinas);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.targetMinbs,t5729rec.targetMinbs)) {
			t5729rec.targetMinbs.set(sv.targetMinbs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.targetMincs,t5729rec.targetMincs)) {
			t5729rec.targetMincs.set(sv.targetMincs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.targetMinds,t5729rec.targetMinds)) {
			t5729rec.targetMinds.set(sv.targetMinds);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.targetMines,t5729rec.targetMines)) {
			t5729rec.targetMines.set(sv.targetMines);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.targetMinfs,t5729rec.targetMinfs)) {
			t5729rec.targetMinfs.set(sv.targetMinfs);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T5729pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
