/******************************************************************************
 * File Name 		: FpcopfDAO.java
 * Author			: smalchi2
 * Creation Date	: 29 December 2016
 * Project			: Integral Life
 * Description		: The DAO Interface for FPCOPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.flexiblepremium.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.life.flexiblepremium.dataaccess.model.Fpcopf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface FpcopfDAO extends BaseDAO<Fpcopf> {

	public List<Fpcopf> searchFpcopfRecord(Fpcopf fpcopf) throws SQLRuntimeException;

	public void InsertFpcopfRecord(Fpcopf fpcopf) throws SQLRuntimeException;
	public Map<String, List<Fpcopf>> getFpcoMap(String coy, List<String> chdrnumList);
	public boolean updateFpcoPF(List<Fpcopf> fpcoList);
	
	public List<Fpcopf> searchFpcoRecord(String chdrcoy, String chdrnum);
	public void insertFpcoRecord(List<Fpcopf> fpcopfList);
	public void updateInvalidFpcoRecord(List<Fpcopf> fpcoList);
}
