/*
 * File: B6686.java
 * Date: 29 August 2009 21:23:59
 * Author: Quipoz Limited
 * 
 * Class transformed from B6686.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;

import com.csc.life.flexiblepremium.reports.R5361Report;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* COMPILE-OPTIONS-SQL   CSRSQLCSR(*ENDJOB)        <Do not delete>
*      *
*
*    This program produces the Anniversary Processing Report where
*    the target premium has not been reached in the period.
*
*   (Reporting previously carried out in B5361 (Anniversary)).
*
*    Extract those records to be reported on from the FREP file
*    written in B5361.
*
*    CONTROL TOTALS
*
*    01  -  No. of records read.
*    02  -  No. of records reported on.
*
***********************************************************************
* </pre>
*/
public class B6686 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlfrepCursorrs = null;
	private java.sql.PreparedStatement sqlfrepCursorps = null;
	private java.sql.Connection sqlfrepCursorconn = null;
	private String sqlfrepCursor = "";
	private R5361Report printerFile = new R5361Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B6686");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaContractStart = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaContractEnd = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaProcflg = new FixedLengthStringData(1).init("A");
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaReportWritten = new FixedLengthStringData(1).init("N");
	private Validator reportWritten = new Validator(wsaaReportWritten, "Y");

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";
	private static final String t1693 = "T1693";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 1;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData r5361h01Record = new FixedLengthStringData(31);
	private FixedLengthStringData r5361h01O = new FixedLengthStringData(31).isAPartOf(r5361h01Record, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(r5361h01O, 0);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(r5361h01O, 1);

	private FixedLengthStringData r5361t01Record = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private DescTableDAM descIO = new DescTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private P6671par p6671par = new P6671par();
	private R5361d01RecordInner r5361d01RecordInner = new R5361d01RecordInner();
	private SqlFreppfInner sqlFreppfInner = new SqlFreppfInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endOfCursor2080, 
		exit2090
	}

	public B6686() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		wsspEdterror.set(varcom.oK);
		printerFile.openOutput();
		/* Get company description for report header.*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		rh01Companynm.set(descIO.getLongdesc());
		rh01Company.set(bsprIO.getCompany());
		wsaaCompany.set(bsprIO.getCompany());
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES)) {
			wsaaContractStart.set(LOVALUE);
		}
		else {
			wsaaContractStart.set(p6671par.chdrnum);
		}
		if (isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaContractEnd.set(HIVALUE);
		}
		else {
			wsaaContractEnd.set(p6671par.chdrnum1);
		}
		sqlfrepCursor = " SELECT  COMPANY, CHDRNUM, LIFE, COVERAGE, PLNSFX, TARGFROM, CBANPR, PRMPER, PRMRCDP, BILLEDP, OVRMINREQ, PROCFLAG" +
" FROM   " + getAppVars().getTableNameOverriden("FREPPF") + " " +
" WHERE COMPANY = ?" +
" AND CHDRNUM >= ?" +
" AND CHDRNUM <= ?" +
" AND PROCFLAG = ?" +
" ORDER BY COMPANY, CHDRNUM, COVERAGE";
		sqlerrorflag = false;
		try {
			sqlfrepCursorconn = getAppVars().getDBConnectionForTable(new com.csc.life.flexiblepremium.dataaccess.FreppfTableDAM());
			sqlfrepCursorps = getAppVars().prepareStatementEmbeded(sqlfrepCursorconn, sqlfrepCursor, "FREPPF");
			getAppVars().setDBString(sqlfrepCursorps, 1, wsaaCompany);
			getAppVars().setDBString(sqlfrepCursorps, 2, wsaaContractStart);
			getAppVars().setDBString(sqlfrepCursorps, 3, wsaaContractEnd);
			getAppVars().setDBString(sqlfrepCursorps, 4, wsaaProcflg);
			sqlfrepCursorrs = getAppVars().executeQuery(sqlfrepCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case endOfCursor2080: 
					endOfCursor2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		wsspEdterror.set(varcom.oK);
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlfrepCursorrs)) {
				getAppVars().getDBObject(sqlfrepCursorrs, 1, sqlFreppfInner.sqlCompany);
				getAppVars().getDBObject(sqlfrepCursorrs, 2, sqlFreppfInner.sqlChdrnum);
				getAppVars().getDBObject(sqlfrepCursorrs, 3, sqlFreppfInner.sqlLife);
				getAppVars().getDBObject(sqlfrepCursorrs, 4, sqlFreppfInner.sqlCoverage);
				getAppVars().getDBObject(sqlfrepCursorrs, 5, sqlFreppfInner.sqlPlnsfx);
				getAppVars().getDBObject(sqlfrepCursorrs, 6, sqlFreppfInner.sqlTargfrom);
				getAppVars().getDBObject(sqlfrepCursorrs, 7, sqlFreppfInner.sqlCbanpr);
				getAppVars().getDBObject(sqlfrepCursorrs, 8, sqlFreppfInner.sqlPrmper);
				getAppVars().getDBObject(sqlfrepCursorrs, 9, sqlFreppfInner.sqlPrmrcdp);
				getAppVars().getDBObject(sqlfrepCursorrs, 10, sqlFreppfInner.sqlBilledp);
				getAppVars().getDBObject(sqlfrepCursorrs, 11, sqlFreppfInner.sqlOvrminreq);
			}
			else {
				goTo(GotoLabel.endOfCursor2080);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		goTo(GotoLabel.exit2090);
	}

protected void endOfCursor2080()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		if (newPageReq.isTrue()) {
			writeHeader3100();
		}
		datcon1rec.intDate.set(sqlFreppfInner.sqlTargfrom);
		callDatcon13200();
		r5361d01RecordInner.rd01Currfrom.set(datcon1rec.extDate);
		/*MOVE SQL-TARGTO             TO DTC1-INT-DATE.*/
		datcon1rec.intDate.set(sqlFreppfInner.sqlCbanpr);
		callDatcon13200();
		r5361d01RecordInner.rd01Currto.set(datcon1rec.extDate);
		r5361d01RecordInner.rd01Chdrnum.set(sqlFreppfInner.sqlChdrnum);
		r5361d01RecordInner.rd01Life.set(sqlFreppfInner.sqlLife);
		r5361d01RecordInner.rd01Coverage.set(sqlFreppfInner.sqlCoverage);
		r5361d01RecordInner.rd01Plnsfx.set(sqlFreppfInner.sqlPlnsfx);
		r5361d01RecordInner.rd01Tgtprem.set(sqlFreppfInner.sqlPrmper);
		r5361d01RecordInner.rd01Prmrecd.set(sqlFreppfInner.sqlPrmrcdp);
		r5361d01RecordInner.rd01Billamt.set(sqlFreppfInner.sqlBilledp);
		r5361d01RecordInner.rd01Minreqd.set(sqlFreppfInner.sqlOvrminreq);
		printerRec.set(SPACES);
		printerFile.printR5361d01(r5361d01RecordInner.r5361d01Record, indicArea);
		reportWritten.setTrue();
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
	}

protected void writeHeader3100()
	{
		/*START*/
		printerRec.set(SPACES);
		printerFile.printR5361h01(r5361h01Record, indicArea);
		wsaaOverflow.set("N");
		/*EXIT*/
	}

protected void callDatcon13200()
	{
		/*START*/
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*PARA*/
		if (newPageReq.isTrue()
		&& reportWritten.isTrue()) {
			writeHeader3100();
		}
		if (reportWritten.isTrue()) {
			printerRec.set(SPACES);
			printerFile.printR5361t01(r5361t01Record, indicArea);
		}
		getAppVars().freeDBConnectionIgnoreErr(sqlfrepCursorconn, sqlfrepCursorps, sqlfrepCursorrs);
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure SQL-FREPPF--INNER
 */
private static final class SqlFreppfInner { 

	private FixedLengthStringData sqlFreppf = new FixedLengthStringData(62);
	private FixedLengthStringData sqlCompany = new FixedLengthStringData(1).isAPartOf(sqlFreppf, 0);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlFreppf, 1);
	private FixedLengthStringData sqlLife = new FixedLengthStringData(2).isAPartOf(sqlFreppf, 9);
	private FixedLengthStringData sqlCoverage = new FixedLengthStringData(2).isAPartOf(sqlFreppf, 11);
	private PackedDecimalData sqlPlnsfx = new PackedDecimalData(4, 0).isAPartOf(sqlFreppf, 13);
	private PackedDecimalData sqlTargfrom = new PackedDecimalData(8, 0).isAPartOf(sqlFreppf, 16);
	private PackedDecimalData sqlCbanpr = new PackedDecimalData(8, 0).isAPartOf(sqlFreppf, 21);
	private PackedDecimalData sqlPrmper = new PackedDecimalData(17, 2).isAPartOf(sqlFreppf, 26);
	private PackedDecimalData sqlPrmrcdp = new PackedDecimalData(17, 2).isAPartOf(sqlFreppf, 35);
	private PackedDecimalData sqlBilledp = new PackedDecimalData(17, 2).isAPartOf(sqlFreppf, 44);
	private PackedDecimalData sqlOvrminreq = new PackedDecimalData(17, 2).isAPartOf(sqlFreppf, 53);
}
/*
 * Class transformed  from Data Structure R5361D01-RECORD--INNER
 */
private static final class R5361d01RecordInner { 

	private FixedLengthStringData r5361d01Record = new FixedLengthStringData(104);
	private FixedLengthStringData r5361d01O = new FixedLengthStringData(104).isAPartOf(r5361d01Record, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(r5361d01O, 0);
	private FixedLengthStringData rd01Life = new FixedLengthStringData(2).isAPartOf(r5361d01O, 8);
	private FixedLengthStringData rd01Coverage = new FixedLengthStringData(2).isAPartOf(r5361d01O, 10);
	private ZonedDecimalData rd01Plnsfx = new ZonedDecimalData(4, 0).isAPartOf(r5361d01O, 12);
	private FixedLengthStringData rd01Currfrom = new FixedLengthStringData(10).isAPartOf(r5361d01O, 16);
	private FixedLengthStringData rd01Currto = new FixedLengthStringData(10).isAPartOf(r5361d01O, 26);
	private ZonedDecimalData rd01Tgtprem = new ZonedDecimalData(17, 2).isAPartOf(r5361d01O, 36);
	private ZonedDecimalData rd01Prmrecd = new ZonedDecimalData(17, 2).isAPartOf(r5361d01O, 53);
	private ZonedDecimalData rd01Billamt = new ZonedDecimalData(17, 2).isAPartOf(r5361d01O, 70);
	private ZonedDecimalData rd01Minreqd = new ZonedDecimalData(17, 2).isAPartOf(r5361d01O, 87);
}
}
