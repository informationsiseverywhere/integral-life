/*
 * File: P5140.java
 * Date: 30 August 2009 0:13:10
 * Author: Quipoz Limited
 * 
 * Class transformed from P5140.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FprmTableDAM;
import com.csc.life.flexiblepremium.screens.S5140ScreenVars;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* This program is called from Contract Enquiries Extra Details.
* Details are only shown for Flexible Premium contracts.
*
* The screen will show details of prem billed, received and the
* minimum amount required (of the premium billed) before overdue
* processing is activated.
*
* Details are cumulative as they are from the FPRM record not
* the FPCO (the FPCO is held at coverage/target year level).
*                                                                     *
*****************************************************************
* </pre>
*/
public class P5140 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5140");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaBillfreq = new ZonedDecimalData(2, 0).setUnsigned();
		/* TABLES */
	private String t5688 = "T5688";
	private String t3588 = "T3588";
	private String t3623 = "T3623";
		/* FORMATS */
	private String payrrec = "PAYRREC";
	private String fprmrec = "FPRMREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Flexible Premium Logical File*/
	private FprmTableDAM fprmIO = new FprmTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Payor Details Logical File*/
	private PayrTableDAM payrIO = new PayrTableDAM();
	private S5140ScreenVars sv = ScreenProgram.getScreenVars( S5140ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit
	}

	public P5140() {
		super();
		screenVars = sv;
		new ScreenModel("S5140", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.anntarprm.set(ZERO);
		sv.btdate.set(varcom.maxdate);
		sv.instalment.set(ZERO);
		sv.minPrmReqd.set(ZERO);
		sv.totalBilled.set(ZERO);
		sv.totalRecd.set(ZERO);
		sv.varnce.set(ZERO);
		chdrenqIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrenqIO.getChdrnum());
		sv.cnttype.set(chdrenqIO.getCnttype());
		sv.cntcurr.set(chdrenqIO.getCntcurr());
		sv.register.set(chdrenqIO.getRegister());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrenqIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.fill("?");
		}
		else {
			sv.longdesc.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrenqIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrenqIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		payrIO.setDataKey(SPACES);
		payrIO.setChdrcoy(wsspcomn.chdrChdrcoy);
		payrIO.setChdrnum(wsspcomn.chdrChdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		fprmIO.setDataKey(SPACES);
		fprmIO.setChdrcoy(payrIO.getChdrcoy());
		fprmIO.setChdrnum(payrIO.getChdrnum());
		fprmIO.setPayrseqno(1);
		fprmIO.setFormat(fprmrec);
		fprmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(fprmIO.getStatuz());
			syserrrec.params.set(fprmIO.getParams());
			fatalError600();
		}
		wsaaBillfreq.set(payrIO.getBillfreq());
		compute(sv.anntarprm, 2).set(mult(payrIO.getSinstamt06(),wsaaBillfreq));
		sv.instalment.set(payrIO.getSinstamt06());
		sv.billfreq.set(payrIO.getBillfreq());
		sv.btdate.set(payrIO.getBtdate());
		if (isGTE(fprmIO.getTotalRecd(),fprmIO.getMinPrmReqd())) {
			sv.minPrmReqd.set(ZERO);
		}
		else {
			compute(sv.minPrmReqd, 2).set(sub(fprmIO.getMinPrmReqd(),fprmIO.getTotalRecd()));
		}
		sv.totalBilled.set(fprmIO.getTotalBilled());
		sv.totalRecd.set(fprmIO.getTotalRecd());
		compute(sv.varnce, 2).set(sub(fprmIO.getTotalRecd(),fprmIO.getTotalBilled()));
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
