package com.csc.life.flexiblepremium.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:17
 * Description:
 * Copybook name: T5729REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5729rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5729Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData durationas = new FixedLengthStringData(16).isAPartOf(t5729Rec, 0);
  	public ZonedDecimalData[] durationa = ZDArrayPartOfStructure(4, 4, 0, durationas, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(durationas, 0, FILLER_REDEFINE);
  	public ZonedDecimalData durationa01 = new ZonedDecimalData(4, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData durationa02 = new ZonedDecimalData(4, 0).isAPartOf(filler, 4);
  	public ZonedDecimalData durationa03 = new ZonedDecimalData(4, 0).isAPartOf(filler, 8);
  	public ZonedDecimalData durationa04 = new ZonedDecimalData(4, 0).isAPartOf(filler, 12);
  	public FixedLengthStringData durationbs = new FixedLengthStringData(16).isAPartOf(t5729Rec, 16);
  	public ZonedDecimalData[] durationb = ZDArrayPartOfStructure(4, 4, 0, durationbs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(16).isAPartOf(durationbs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData durationb01 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData durationb02 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 4);
  	public ZonedDecimalData durationb03 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 8);
  	public ZonedDecimalData durationb04 = new ZonedDecimalData(4, 0).isAPartOf(filler1, 12);
  	public FixedLengthStringData durationcs = new FixedLengthStringData(16).isAPartOf(t5729Rec, 32);
  	public ZonedDecimalData[] durationc = ZDArrayPartOfStructure(4, 4, 0, durationcs, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(16).isAPartOf(durationcs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData durationc01 = new ZonedDecimalData(4, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData durationc02 = new ZonedDecimalData(4, 0).isAPartOf(filler2, 4);
  	public ZonedDecimalData durationc03 = new ZonedDecimalData(4, 0).isAPartOf(filler2, 8);
  	public ZonedDecimalData durationc04 = new ZonedDecimalData(4, 0).isAPartOf(filler2, 12);
  	public FixedLengthStringData durationds = new FixedLengthStringData(16).isAPartOf(t5729Rec, 48);
  	public ZonedDecimalData[] durationd = ZDArrayPartOfStructure(4, 4, 0, durationds, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(durationds, 0, FILLER_REDEFINE);
  	public ZonedDecimalData durationd01 = new ZonedDecimalData(4, 0).isAPartOf(filler3, 0);
  	public ZonedDecimalData durationd02 = new ZonedDecimalData(4, 0).isAPartOf(filler3, 4);
  	public ZonedDecimalData durationd03 = new ZonedDecimalData(4, 0).isAPartOf(filler3, 8);
  	public ZonedDecimalData durationd04 = new ZonedDecimalData(4, 0).isAPartOf(filler3, 12);
  	public FixedLengthStringData durationes = new FixedLengthStringData(16).isAPartOf(t5729Rec, 64);
  	public ZonedDecimalData[] duratione = ZDArrayPartOfStructure(4, 4, 0, durationes, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(16).isAPartOf(durationes, 0, FILLER_REDEFINE);
  	public ZonedDecimalData duratione01 = new ZonedDecimalData(4, 0).isAPartOf(filler4, 0);
  	public ZonedDecimalData duratione02 = new ZonedDecimalData(4, 0).isAPartOf(filler4, 4);
  	public ZonedDecimalData duratione03 = new ZonedDecimalData(4, 0).isAPartOf(filler4, 8);
  	public ZonedDecimalData duratione04 = new ZonedDecimalData(4, 0).isAPartOf(filler4, 12);
  	public FixedLengthStringData durationfs = new FixedLengthStringData(16).isAPartOf(t5729Rec, 80);
  	public ZonedDecimalData[] durationf = ZDArrayPartOfStructure(4, 4, 0, durationfs, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(16).isAPartOf(durationfs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData durationf01 = new ZonedDecimalData(4, 0).isAPartOf(filler5, 0);
  	public ZonedDecimalData durationf02 = new ZonedDecimalData(4, 0).isAPartOf(filler5, 4);
  	public ZonedDecimalData durationf03 = new ZonedDecimalData(4, 0).isAPartOf(filler5, 8);
  	public ZonedDecimalData durationf04 = new ZonedDecimalData(4, 0).isAPartOf(filler5, 12);
  	public FixedLengthStringData frqcys = new FixedLengthStringData(12).isAPartOf(t5729Rec, 96);
  	public FixedLengthStringData[] frqcy = FLSArrayPartOfStructure(6, 2, frqcys, 0);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(12).isAPartOf(frqcys, 0, FILLER_REDEFINE);
  	public FixedLengthStringData frqcy01 = new FixedLengthStringData(2).isAPartOf(filler6, 0);
  	public FixedLengthStringData frqcy02 = new FixedLengthStringData(2).isAPartOf(filler6, 2);
  	public FixedLengthStringData frqcy03 = new FixedLengthStringData(2).isAPartOf(filler6, 4);
  	public FixedLengthStringData frqcy04 = new FixedLengthStringData(2).isAPartOf(filler6, 6);
  	public FixedLengthStringData frqcy05 = new FixedLengthStringData(2).isAPartOf(filler6, 8);
  	public FixedLengthStringData frqcy06 = new FixedLengthStringData(2).isAPartOf(filler6, 10);
  	public FixedLengthStringData overdueMinas = new FixedLengthStringData(12).isAPartOf(t5729Rec, 108);
  	public ZonedDecimalData[] overdueMina = ZDArrayPartOfStructure(4, 3, 0, overdueMinas, 0);
  	public FixedLengthStringData filler7 = new FixedLengthStringData(12).isAPartOf(overdueMinas, 0, FILLER_REDEFINE);
  	public ZonedDecimalData overdueMina01 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 0);
  	public ZonedDecimalData overdueMina02 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 3);
  	public ZonedDecimalData overdueMina03 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 6);
  	public ZonedDecimalData overdueMina04 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 9);
  	public FixedLengthStringData overdueMinbs = new FixedLengthStringData(12).isAPartOf(t5729Rec, 120);
  	public ZonedDecimalData[] overdueMinb = ZDArrayPartOfStructure(4, 3, 0, overdueMinbs, 0);
  	public FixedLengthStringData filler8 = new FixedLengthStringData(12).isAPartOf(overdueMinbs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData overdueMinb01 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 0);
  	public ZonedDecimalData overdueMinb02 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 3);
  	public ZonedDecimalData overdueMinb03 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 6);
  	public ZonedDecimalData overdueMinb04 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 9);
  	public FixedLengthStringData overdueMincs = new FixedLengthStringData(12).isAPartOf(t5729Rec, 132);
  	public ZonedDecimalData[] overdueMinc = ZDArrayPartOfStructure(4, 3, 0, overdueMincs, 0);
  	public FixedLengthStringData filler9 = new FixedLengthStringData(12).isAPartOf(overdueMincs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData overdueMinc01 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 0);
  	public ZonedDecimalData overdueMinc02 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 3);
  	public ZonedDecimalData overdueMinc03 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 6);
  	public ZonedDecimalData overdueMinc04 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 9);
  	public FixedLengthStringData overdueMinds = new FixedLengthStringData(12).isAPartOf(t5729Rec, 144);
  	public ZonedDecimalData[] overdueMind = ZDArrayPartOfStructure(4, 3, 0, overdueMinds, 0);
  	public FixedLengthStringData filler10 = new FixedLengthStringData(12).isAPartOf(overdueMinds, 0, FILLER_REDEFINE);
  	public ZonedDecimalData overdueMind01 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 0);
  	public ZonedDecimalData overdueMind02 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 3);
  	public ZonedDecimalData overdueMind03 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 6);
  	public ZonedDecimalData overdueMind04 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 9);
  	public FixedLengthStringData overdueMines = new FixedLengthStringData(12).isAPartOf(t5729Rec, 156);
  	public ZonedDecimalData[] overdueMine = ZDArrayPartOfStructure(4, 3, 0, overdueMines, 0);
  	public FixedLengthStringData filler11 = new FixedLengthStringData(12).isAPartOf(overdueMines, 0, FILLER_REDEFINE);
  	public ZonedDecimalData overdueMine01 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 0);
  	public ZonedDecimalData overdueMine02 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 3);
  	public ZonedDecimalData overdueMine03 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 6);
  	public ZonedDecimalData overdueMine04 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 9);
  	public FixedLengthStringData overdueMinfs = new FixedLengthStringData(12).isAPartOf(t5729Rec, 168);
  	public ZonedDecimalData[] overdueMinf = ZDArrayPartOfStructure(4, 3, 0, overdueMinfs, 0);
  	public FixedLengthStringData filler12 = new FixedLengthStringData(12).isAPartOf(overdueMinfs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData overdueMinf01 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 0);
  	public ZonedDecimalData overdueMinf02 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 3);
  	public ZonedDecimalData overdueMinf03 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 6);
  	public ZonedDecimalData overdueMinf04 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 9);
  	public FixedLengthStringData targetMaxas = new FixedLengthStringData(20).isAPartOf(t5729Rec, 180);
  	public ZonedDecimalData[] targetMaxa = ZDArrayPartOfStructure(4, 5, 0, targetMaxas, 0);
  	public FixedLengthStringData filler13 = new FixedLengthStringData(20).isAPartOf(targetMaxas, 0, FILLER_REDEFINE);
  	public ZonedDecimalData targetMaxa01 = new ZonedDecimalData(5, 0).isAPartOf(filler13, 0);
  	public ZonedDecimalData targetMaxa02 = new ZonedDecimalData(5, 0).isAPartOf(filler13, 5);
  	public ZonedDecimalData targetMaxa03 = new ZonedDecimalData(5, 0).isAPartOf(filler13, 10);
  	public ZonedDecimalData targetMaxa04 = new ZonedDecimalData(5, 0).isAPartOf(filler13, 15);
  	public FixedLengthStringData targetMaxbs = new FixedLengthStringData(20).isAPartOf(t5729Rec, 200);
  	public ZonedDecimalData[] targetMaxb = ZDArrayPartOfStructure(4, 5, 0, targetMaxbs, 0);
  	public FixedLengthStringData filler14 = new FixedLengthStringData(20).isAPartOf(targetMaxbs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData targetMaxb01 = new ZonedDecimalData(5, 0).isAPartOf(filler14, 0);
  	public ZonedDecimalData targetMaxb02 = new ZonedDecimalData(5, 0).isAPartOf(filler14, 5);
  	public ZonedDecimalData targetMaxb03 = new ZonedDecimalData(5, 0).isAPartOf(filler14, 10);
  	public ZonedDecimalData targetMaxb04 = new ZonedDecimalData(5, 0).isAPartOf(filler14, 15);
  	public FixedLengthStringData targetMaxcs = new FixedLengthStringData(20).isAPartOf(t5729Rec, 220);
  	public ZonedDecimalData[] targetMaxc = ZDArrayPartOfStructure(4, 5, 0, targetMaxcs, 0);
  	public FixedLengthStringData filler15 = new FixedLengthStringData(20).isAPartOf(targetMaxcs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData targetMaxc01 = new ZonedDecimalData(5, 0).isAPartOf(filler15, 0);
  	public ZonedDecimalData targetMaxc02 = new ZonedDecimalData(5, 0).isAPartOf(filler15, 5);
  	public ZonedDecimalData targetMaxc03 = new ZonedDecimalData(5, 0).isAPartOf(filler15, 10);
  	public ZonedDecimalData targetMaxc04 = new ZonedDecimalData(5, 0).isAPartOf(filler15, 15);
  	public FixedLengthStringData targetMaxds = new FixedLengthStringData(20).isAPartOf(t5729Rec, 240);
  	public ZonedDecimalData[] targetMaxd = ZDArrayPartOfStructure(4, 5, 0, targetMaxds, 0);
  	public FixedLengthStringData filler16 = new FixedLengthStringData(20).isAPartOf(targetMaxds, 0, FILLER_REDEFINE);
  	public ZonedDecimalData targetMaxd01 = new ZonedDecimalData(5, 0).isAPartOf(filler16, 0);
  	public ZonedDecimalData targetMaxd02 = new ZonedDecimalData(5, 0).isAPartOf(filler16, 5);
  	public ZonedDecimalData targetMaxd03 = new ZonedDecimalData(5, 0).isAPartOf(filler16, 10);
  	public ZonedDecimalData targetMaxd04 = new ZonedDecimalData(5, 0).isAPartOf(filler16, 15);
  	public FixedLengthStringData targetMaxes = new FixedLengthStringData(20).isAPartOf(t5729Rec, 260);
  	public ZonedDecimalData[] targetMaxe = ZDArrayPartOfStructure(4, 5, 0, targetMaxes, 0);
  	public FixedLengthStringData filler17 = new FixedLengthStringData(20).isAPartOf(targetMaxes, 0, FILLER_REDEFINE);
  	public ZonedDecimalData targetMaxe01 = new ZonedDecimalData(5, 0).isAPartOf(filler17, 0);
  	public ZonedDecimalData targetMaxe02 = new ZonedDecimalData(5, 0).isAPartOf(filler17, 5);
  	public ZonedDecimalData targetMaxe03 = new ZonedDecimalData(5, 0).isAPartOf(filler17, 10);
  	public ZonedDecimalData targetMaxe04 = new ZonedDecimalData(5, 0).isAPartOf(filler17, 15);
  	public FixedLengthStringData targetMaxfs = new FixedLengthStringData(20).isAPartOf(t5729Rec, 280);
  	public ZonedDecimalData[] targetMaxf = ZDArrayPartOfStructure(4, 5, 0, targetMaxfs, 0);
  	public FixedLengthStringData filler18 = new FixedLengthStringData(20).isAPartOf(targetMaxfs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData targetMaxf01 = new ZonedDecimalData(5, 0).isAPartOf(filler18, 0);
  	public ZonedDecimalData targetMaxf02 = new ZonedDecimalData(5, 0).isAPartOf(filler18, 5);
  	public ZonedDecimalData targetMaxf03 = new ZonedDecimalData(5, 0).isAPartOf(filler18, 10);
  	public ZonedDecimalData targetMaxf04 = new ZonedDecimalData(5, 0).isAPartOf(filler18, 15);
  	public FixedLengthStringData targetMinas = new FixedLengthStringData(12).isAPartOf(t5729Rec, 300);
  	public ZonedDecimalData[] targetMina = ZDArrayPartOfStructure(4, 3, 0, targetMinas, 0);
  	public FixedLengthStringData filler19 = new FixedLengthStringData(12).isAPartOf(targetMinas, 0, FILLER_REDEFINE);
  	public ZonedDecimalData targetMina01 = new ZonedDecimalData(3, 0).isAPartOf(filler19, 0);
  	public ZonedDecimalData targetMina02 = new ZonedDecimalData(3, 0).isAPartOf(filler19, 3);
  	public ZonedDecimalData targetMina03 = new ZonedDecimalData(3, 0).isAPartOf(filler19, 6);
  	public ZonedDecimalData targetMina04 = new ZonedDecimalData(3, 0).isAPartOf(filler19, 9);
  	public FixedLengthStringData targetMinbs = new FixedLengthStringData(12).isAPartOf(t5729Rec, 312);
  	public ZonedDecimalData[] targetMinb = ZDArrayPartOfStructure(4, 3, 0, targetMinbs, 0);
  	public FixedLengthStringData filler20 = new FixedLengthStringData(12).isAPartOf(targetMinbs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData targetMinb01 = new ZonedDecimalData(3, 0).isAPartOf(filler20, 0);
  	public ZonedDecimalData targetMinb02 = new ZonedDecimalData(3, 0).isAPartOf(filler20, 3);
  	public ZonedDecimalData targetMinb03 = new ZonedDecimalData(3, 0).isAPartOf(filler20, 6);
  	public ZonedDecimalData targetMinb04 = new ZonedDecimalData(3, 0).isAPartOf(filler20, 9);
  	public FixedLengthStringData targetMincs = new FixedLengthStringData(12).isAPartOf(t5729Rec, 324);
  	public ZonedDecimalData[] targetMinc = ZDArrayPartOfStructure(4, 3, 0, targetMincs, 0);
  	public FixedLengthStringData filler21 = new FixedLengthStringData(12).isAPartOf(targetMincs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData targetMinc01 = new ZonedDecimalData(3, 0).isAPartOf(filler21, 0);
  	public ZonedDecimalData targetMinc02 = new ZonedDecimalData(3, 0).isAPartOf(filler21, 3);
  	public ZonedDecimalData targetMinc03 = new ZonedDecimalData(3, 0).isAPartOf(filler21, 6);
  	public ZonedDecimalData targetMinc04 = new ZonedDecimalData(3, 0).isAPartOf(filler21, 9);
  	public FixedLengthStringData targetMinds = new FixedLengthStringData(12).isAPartOf(t5729Rec, 336);
  	public ZonedDecimalData[] targetMind = ZDArrayPartOfStructure(4, 3, 0, targetMinds, 0);
  	public FixedLengthStringData filler22 = new FixedLengthStringData(12).isAPartOf(targetMinds, 0, FILLER_REDEFINE);
  	public ZonedDecimalData targetMind01 = new ZonedDecimalData(3, 0).isAPartOf(filler22, 0);
  	public ZonedDecimalData targetMind02 = new ZonedDecimalData(3, 0).isAPartOf(filler22, 3);
  	public ZonedDecimalData targetMind03 = new ZonedDecimalData(3, 0).isAPartOf(filler22, 6);
  	public ZonedDecimalData targetMind04 = new ZonedDecimalData(3, 0).isAPartOf(filler22, 9);
  	public FixedLengthStringData targetMines = new FixedLengthStringData(12).isAPartOf(t5729Rec, 348);
  	public ZonedDecimalData[] targetMine = ZDArrayPartOfStructure(4, 3, 0, targetMines, 0);
  	public FixedLengthStringData filler23 = new FixedLengthStringData(12).isAPartOf(targetMines, 0, FILLER_REDEFINE);
  	public ZonedDecimalData targetMine01 = new ZonedDecimalData(3, 0).isAPartOf(filler23, 0);
  	public ZonedDecimalData targetMine02 = new ZonedDecimalData(3, 0).isAPartOf(filler23, 3);
  	public ZonedDecimalData targetMine03 = new ZonedDecimalData(3, 0).isAPartOf(filler23, 6);
  	public ZonedDecimalData targetMine04 = new ZonedDecimalData(3, 0).isAPartOf(filler23, 9);
  	public FixedLengthStringData targetMinfs = new FixedLengthStringData(12).isAPartOf(t5729Rec, 360);
  	public ZonedDecimalData[] targetMinf = ZDArrayPartOfStructure(4, 3, 0, targetMinfs, 0);
  	public FixedLengthStringData filler24 = new FixedLengthStringData(12).isAPartOf(targetMinfs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData targetMinf01 = new ZonedDecimalData(3, 0).isAPartOf(filler24, 0);
  	public ZonedDecimalData targetMinf02 = new ZonedDecimalData(3, 0).isAPartOf(filler24, 3);
  	public ZonedDecimalData targetMinf03 = new ZonedDecimalData(3, 0).isAPartOf(filler24, 6);
  	public ZonedDecimalData targetMinf04 = new ZonedDecimalData(3, 0).isAPartOf(filler24, 9);
  	public FixedLengthStringData filler25 = new FixedLengthStringData(128).isAPartOf(t5729Rec, 372, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5729Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5729Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}