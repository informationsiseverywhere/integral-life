package com.csc.life.flexiblepremium.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.flexiblepremium.dataaccess.model.Fprmpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface FprmpfDAO extends BaseDAO<Fprmpf> {
	
	public Map<String, List<Fprmpf>> getFprmMap(String coy, List<String> chdrnumList);
	public boolean updateFprmPF(List<Fprmpf> fprmList);

}
