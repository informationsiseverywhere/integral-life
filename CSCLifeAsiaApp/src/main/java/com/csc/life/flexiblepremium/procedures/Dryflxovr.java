/*
 * File: Dryflxovr.java
 * Date: February 11, 2015 5:23:46 PM ICT
 * Author: CSC
 * 
 * Class transformed from DRYFLXOVR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.procedures;

import com.quipoz.framework.datatype.*;
import com.quipoz.framework.exception.ServerException;
import com.quipoz.framework.exception.TransferProgramException;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.sessionfacade.JTAWrapper;
import com.quipoz.framework.util.*;
import com.quipoz.COBOLFramework.util.*;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import static com.quipoz.COBOLFramework.util.CLFunctions.*;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.ExitSection;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import java.util.ArrayList;
import com.quipoz.framework.util.log.QPLogger;

import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FpcoTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.diary.recordstructures.Drylogrec;




import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.diary.procedures.Drylog;
import com.csc.diaryframework.parent.Maind;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This subroutine is used by the diary system to set
* up the trigger record for flexible premium overdue
* processing.
*
* A record needs to be set if the Active Indicator on the FPCO
* file is 'Y' and the minimum for overdue processing is greater
* than the premium received.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dryflxovr extends Maind {//ILPI-61

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(9).init("DRYFLXOVR");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(2, 0).setUnsigned();
	private final int wsaaT5679Max = 12;

	private FixedLengthStringData wsaaT5679Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5679Trcde = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init("N");
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");
	private Validator invalidStatus = new Validator(wsaaValidStatus, "N");

	private FixedLengthStringData wsaaOverdueInd = new FixedLengthStringData(1);
	private Validator overdueRequired = new Validator(wsaaOverdueInd, "Y");
	private Validator noOverdueRequired = new Validator(wsaaOverdueInd, "N");
		/* ERRORS */
	private static final String f321 = "F321";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String fpcorec = "FPCOREC";
	private static final String covrenqrec = "COVRENQREC";
	private static final String t5679 = "T5679";
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private FpcoTableDAM fpcoIO = new FpcoTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5679rec t5679rec = new T5679rec();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Drylogrec drylogrec = new Drylogrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

	public Dryflxovr() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainLine000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdYes.setTrue();
		noOverdueRequired.setTrue();
		/* Validate the contract status using T5679.*/
		validate100();
		if (invalidStatus.isTrue()) {
			drypDryprcRecInner.dtrdNo.setTrue();
			return ;
		}
		/* Find a FPCO record with the active indicator set to 'Y'*/
		/* and premium received less than minimum set for overdue.*/
		fpcoIO.setRecKeyData(SPACES);
		fpcoIO.setRecNonKeyData(SPACES);
		fpcoIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		fpcoIO.setChdrnum(drypDryprcRecInner.drypEntity);
		fpcoIO.setFormat(fpcorec);
		fpcoIO.setFunction(varcom.begn);
		fpcoIO.setStatuz(varcom.oK);
		while ( !(isEQ(fpcoIO.getStatuz(), varcom.endp))) {
			readFpcos400();
		}
		
		/* If no record has been found, do not create a DTRD record and*/
		/* do no further processing.*/
		/*    IF  OVERDUE-REQUIRED                                         */
		if (noOverdueRequired.isTrue()) {
			drypDryprcRecInner.dtrdNo.setTrue();
			return ;
		}
		else {
			drypDryprcRecInner.drypNxtprcdate.set(drypDryprcRecInner.drypRunDate);
			drypDryprcRecInner.drypNxtprctime.set(ZERO);
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void validate100()
	{
		/*VALD*/
		/* Validate the Contract Risk and Premium status against T5679.*/
		readT5679200();
		validateStatus300();
		/*EXIT*/
	}

protected void readT5679200()
	{
		t5679210();
	}

protected void t5679210()
	{
		wsaaT5679Trcde.set(drypDryprcRecInner.drypSystParm[1]);
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaT5679Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(f321);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void validateStatus300()
	{
		/*VALIDATE*/
		invalidStatus.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
		|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(drypDryprcRecInner.drypStatcode, t5679rec.cnRiskStat[wsaaT5679Sub.toInt()])) {
				validStatus.setTrue();
			}
		}
		if (validStatus.isTrue()) {
			invalidStatus.setTrue();
			for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
			|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
				if (isEQ(drypDryprcRecInner.drypPstatcode, t5679rec.cnPremStat[wsaaT5679Sub.toInt()])) {
					validStatus.setTrue();
				}
			}
		}
		/*EXIT*/
	}

protected void readFpcos400()
	{
		call410();
	}

protected void call410()
	{
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)
		&& isNE(fpcoIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(fpcoIO.getStatuz());
			drylogrec.params.set(fpcoIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(fpcoIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(fpcoIO.getChdrnum(), drypDryprcRecInner.drypEntity)
		|| isEQ(fpcoIO.getStatuz(), varcom.endp)) {
			fpcoIO.setStatuz(varcom.endp);
			return ;
		}
		/* If the Active Indicator is set and the premium received is less*/
		/* than the minimum for overdue set the OVERDUE-REQUIRED indicator*/
		if (isEQ(fpcoIO.getActiveInd(), "Y")
		&& isLT(fpcoIO.getPremRecPer(), fpcoIO.getOverdueMin())) {
			validateCoverage500();
			if (validStatus.isTrue()) {
				overdueRequired.setTrue();
				fpcoIO.setStatuz(varcom.endp);
				return ;
			}
		}
		fpcoIO.setFunction(varcom.nextr);
	}

protected void validateCoverage500()
	{
		validate510();
	}

protected void validate510()
	{
		/* Read the associated coverage record for the FPCO record.*/
		covrenqIO.setRecKeyData(SPACES);
		covrenqIO.setRecNonKeyData(SPACES);
		covrenqIO.setChdrcoy(fpcoIO.getChdrcoy());
		covrenqIO.setChdrnum(fpcoIO.getChdrnum());
		covrenqIO.setLife(fpcoIO.getLife());
		covrenqIO.setCoverage(fpcoIO.getCoverage());
		covrenqIO.setRider(fpcoIO.getRider());
		covrenqIO.setPlanSuffix(fpcoIO.getPlanSuffix());
		covrenqIO.setFormat(covrenqrec);
		covrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(covrenqIO.getStatuz());
			drylogrec.params.set(covrenqIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		invalidStatus.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
		|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(covrenqIO.getStatcode(), t5679rec.covRiskStat[wsaaT5679Sub.toInt()])) {
				validStatus.setTrue();
			}
		}
		if (validStatus.isTrue()) {
			invalidStatus.setTrue();
			for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
			|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
				if (isEQ(covrenqIO.getPstatcode(), t5679rec.covPremStat[wsaaT5679Sub.toInt()])) {
					validStatus.setTrue();
				}
			}
		}
	}

//ILPI-61
//protected void a000FatalError()
//	{
//		/*A010-FATAL*/
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
//		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
//		if (drypDryprcRecInner.onlineMode.isTrue()) {
//			drylogrec.runNumber.set(ZERO);
//		}
//		else {
//			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
//		}
//		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
//		drypDryprcRecInner.fatalError.setTrue();
//		callProgram(Drylog.class, drylogrec.drylogRec);
//		/*A090-EXIT*/
//		exitProgram();
//	}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 

	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
}

/**
 * ILPI-61
 */
@Override
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
}

/**
 * ILPI-61
 */
@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
}
}
