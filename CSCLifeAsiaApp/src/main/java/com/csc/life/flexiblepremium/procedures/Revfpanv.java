/*
 * File: Revfpanv.java
 * Date: 30 August 2009 2:07:32
 * Author: Quipoz Limited
 * 
 * Class transformed from REVFPANV.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.flexiblepremium.dataaccess.FpcoanvTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FpcorevTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* REVFPANV
* ~~~~~~~
* The parameters passed to this program are set up in the copybook
* REVESEREC.
*
* Update each applicable FPCO per coverage
* ----------------------------------------
*
* Read the FPCO using the logical view FPCOANV.
* This selects FPCO records in contract and descending target
* from order. The PTRN effective date on the Anniversary PTRN
* being reversed is set to the TARGFROM date of the FPCO so
* we match these dates and the contract number in order to
* identify all FPCO records written in anniversary processing.
* For each FPCO found we set up the key to the FPCOREV record
* from the FPCOANV, delete the FPCOANV then read the latest
* FPCOREV record.
* The latest is read by moving the FPCOANV TARGFROM date to
* the FPCOREV TARGFROM date. As the TARGFROM date is in
* descending date order, the first one we will find is the
* one updated in anniversary processing.
* Once we find this record, we set the anniversary process ind
* back to 'N', and the active ind to 'Y'.
*
/
***********************************************************************
*                                                                     *
* </pre>
*/
public class Revfpanv extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVFPANV";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
		/* FORMATS */
	private static final String fpcoanvrec = "FPCOANVREC";
	private static final String fpcorevrec = "FPCOREVREC";
	private FpcoanvTableDAM fpcoanvIO = new FpcoanvTableDAM();
	private FpcorevTableDAM fpcorevIO = new FpcorevTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Batckey wsaaBatckey = new Batckey();
	private Reverserec reverserec = new Reverserec();

	public Revfpanv() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		wsaaBatckey.set(reverserec.batchkey);
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		/* get today's date*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		readFirstFpco600();
		while ( !(isEQ(fpcoanvIO.getStatuz(), varcom.endp))) {
			reverseFpcoProcessing700();
		}
		
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readFirstFpco600()
	{
		/*PARA*/
		fpcoanvIO.setChdrcoy(reverserec.company);
		fpcoanvIO.setChdrnum(reverserec.chdrnum);
		/* MOVE REVE-PTRNEFF           TO FPCOANV-TARGFROM.     <LA3997>*/
		/* MOVE REVE-PTRNEFF         TO FPCOANV-ANNIV-PROC-DATE.<LA4626>*/
		fpcoanvIO.setAnnivProcDate(varcom.vrcmMaxDate);
		/* MOVE BEGNH                  TO FPCOANV-FUNCTION.             */
		fpcoanvIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fpcoanvIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fpcoanvIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		fpcoanvIO.setFormat(fpcoanvrec);
		SmartFileCode.execute(appVars, fpcoanvIO);
		if (isNE(fpcoanvIO.getStatuz(), varcom.oK)
		|| isNE(fpcoanvIO.getChdrcoy(), reverserec.company)
		|| isNE(fpcoanvIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(fpcoanvIO.getEffdate(), reverserec.ptrneff)) {
			syserrrec.params.set(fpcoanvIO.getParams());
			dbError8100();
		}
		/*EXIT*/
	}

protected void reverseFpcoProcessing700()
	{
		para700();
	}

protected void para700()
	{
		fpcorevIO.setChdrcoy(fpcoanvIO.getChdrcoy());
		fpcorevIO.setChdrnum(fpcoanvIO.getChdrnum());
		fpcorevIO.setLife(fpcoanvIO.getLife());
		fpcorevIO.setCoverage(fpcoanvIO.getCoverage());
		fpcorevIO.setRider(fpcoanvIO.getRider());
		fpcorevIO.setPlanSuffix(fpcoanvIO.getPlanSuffix());
		fpcorevIO.setTargfrom(fpcoanvIO.getTargfrom());
		deleteFpco800();
		fpcorevIO.setFormat(fpcorevrec);
		/* MOVE BEGNH                  TO FPCOREV-FUNCTION.             */
		fpcorevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fpcorevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fpcorevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			dbError8100();
		}
		if (isNE(fpcorevIO.getChdrcoy(), fpcoanvIO.getChdrcoy())
		|| isNE(fpcorevIO.getChdrnum(), fpcoanvIO.getChdrnum())
		|| isNE(fpcorevIO.getCoverage(), fpcoanvIO.getCoverage())
		|| isNE(fpcorevIO.getLife(), fpcoanvIO.getLife())
		|| isNE(fpcorevIO.getRider(), fpcoanvIO.getRider())
		|| isNE(fpcorevIO.getPlanSuffix(), fpcoanvIO.getPlanSuffix())) {
			syserrrec.params.set(fpcoanvIO.getParams());
			dbError8100();
		}
		fpcorevIO.setAnnProcessInd("N");
		if (isEQ(fpcorevIO.getActiveInd(), "N")) {
			fpcorevIO.setActiveInd("Y");
		}
		/* MOVE REWRT                  TO FPCOREV-FUNCTION.             */
		fpcorevIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			dbError8100();
		}
		readNextFpco900();
	}

protected void deleteFpco800()
	{
		/*PARA*/
		/* MOVE DELET                  TO FPCOANV-FUNCTION              */
		fpcoanvIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, fpcoanvIO);
		if (isNE(fpcoanvIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcoanvIO.getParams());
			dbError8100();
		}
		/*EXIT*/
	}

protected void readNextFpco900()
	{
		/*PARA*/
		fpcoanvIO.setFunction(varcom.nextr);
		fpcoanvIO.setFormat(fpcoanvrec);
		SmartFileCode.execute(appVars, fpcoanvIO);
		if (isNE(fpcoanvIO.getStatuz(), varcom.oK)
		&& isNE(fpcoanvIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(fpcoanvIO.getParams());
			dbError8100();
		}
		if (isNE(fpcoanvIO.getChdrcoy(), reverserec.company)
		|| isNE(fpcoanvIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(fpcoanvIO.getEffdate(), reverserec.ptrneff)) {
			/*    MOVE REWRT               TO FPCOANV-FUNCTION              */
			/*    CALL 'FPCOANVIO'         USING FPCOANV-PARAMS             */
			/*    IF FPCOANV-STATUZ        NOT = O-K                        */
			/*       MOVE FPCOANV-PARAMS   TO SYSR-PARAMS                   */
			/*    END-IF                                                    */
			fpcoanvIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void systemError8000()
	{
		se8000();
		seExit8090();
	}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		reverserec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
		db8100();
		dbExit8190();
	}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		reverserec.statuz.set(varcom.bomb);
		exit090();
	}
}
