package com.csc.life.flexiblepremium.dataaccess.model;

import java.math.BigDecimal;
import java.util.Date;

public class Fprmpf {
	private long unique_number;
	private String chdrcoy;
	private String chdrnum;
	private int payrseqno;
	private String validflag;
	private int currfrom;
	private int currto;
	private int tranno;
	private BigDecimal totrecd;
	private BigDecimal totbill;
	private BigDecimal minreqd;
	private String usrprf;
	private String jobnm;
	private Date datime;
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public int getPayrseqno() {
		return payrseqno;
	}
	public void setPayrseqno(int payrseqno) {
		this.payrseqno = payrseqno;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public int getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(int currfrom) {
		this.currfrom = currfrom;
	}
	public int getCurrto() {
		return currto;
	}
	public void setCurrto(int currto) {
		this.currto = currto;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public BigDecimal getTotrecd() {
		return totrecd;
	}
	public void setTotrecd(BigDecimal totrecd) {
		this.totrecd = totrecd;
	}
	public BigDecimal getTotbill() {
		return totbill;
	}
	public void setTotbill(BigDecimal totbill) {
		this.totbill = totbill;
	}
	public BigDecimal getMinreqd() {
		return minreqd;
	}
	public void setMinreqd(BigDecimal minreqd) {
		this.minreqd = minreqd;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Date getDatime() {
		return new Date(datime.getTime());//IJTI-316
	}
	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());//IJTI-314
	}

}
