/*
 * File: B5363.java
 * Date: 29 August 2009 21:13:00
 * Author: Quipoz Limited
 * 
 * Class transformed from B5363.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.flexiblepremium.dataaccess.FpcoTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FpcxpfTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FrepTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrudlTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*
*REMARKS
*            FLEXIBLE PREMIUM OVERDUE PROCESSING
*            -----------------------------------
*
* Flexible Premium overdue Processing processes the Flexible
* Premium Coverage File (FPCOPF) which are selected by B5363.
*
* Processing.
* ----------
*
* B5363 runs directly after B5362 which 'splits' the FPCOPF
* according to the number of Overdue programs to run.  All
* references to the FPCO are via FPCXPF - a temporary file
* holding all the FPCO records for this program to process.
*
*  1000-INITIALISE.
*  ---------------
*  -  Frequently-referenced tables are stored in working storage
*     arrays to minimize disk IO.
*
*  -  Issue an override to read the correct FPCXPF for this run.
*     The CRTTMPF process has already created a file specific to
*     the run using the  FPCXPF format and is identified  by
*     concatenating the following:-
*
*     'FPCX'
*      BPRD-SYSTEM-PARAM04
*      BSSC-SCHEDULE-NUMBER
*
*      eg FPCXOV0001,  for the first run
*         FPCXOV0002,  for the second etc.
*
*     The number of threads would have been created by the
*     CRTTMPF process given the parameters of the process
*     definition of the CRTTMPF.
*     To get the correct member of the above physical for B5353
*     to read from, concatenate :-
*
*     'THREAD'
*     BSPR-PROCESS-OCC-NUM
*
*     eg. THREAD001,   for the first member (thread 1)
*         THREAD002    for the second etc.  (thread 2)
*
*  -  Initialise the static values of the LIFACMV copybook.
*
*  2000-READ
*  ---------
*
*  -  Read the FPCX records sequentially keeping the count of
*     the number read and storing the present FPCX details for
*     the WSYS- error record.
*
*
*  2500-EDIT
*  ---------
*
*     For Each FPCX  record:
*
*  -  Read the coverage and validate. If not valid read
*     next record
*
*
*  3000-UPDATE
*  -----------
*
*  -  If the premium received in the period on the FPCO is less
*     than the minimum required write an entry to the report
*
*    No file updates are required.
*
* Control totals used in this program:
*
*    01  -  No. of FPCO records read
*    02  -  No. report entries written
*    03  -  No. of records of an invalid status
*
*     (BATC processing is handled in MAINB)
*
***********************************************************************
* </pre>
*/
public class B5363 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FpcxpfTableDAM fpcxpf = new FpcxpfTableDAM();
	private FpcxpfTableDAM fpcxpfRec = new FpcxpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5363");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private static final String itemrec = "ITEMREC";
	private static final String covrudlrec = "COVRUDLREC";
	private static final String freprec = "FREPREC";
	private static final String t1688 = "T1688";
	private static final String t5679 = "T5679";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
		/*88  KEY-CHANGE                            VALUE 'Y'.     <A06923>
		                                          VALUE ZERO.    <A06923>
		01  INDIC-AREA.                                                  
		        88  IND-OFF  VALUE B'0'.                                 
		        88  IND-ON   VALUE B'1'.                                 
		  FPCX parameters*/
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaFpcxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaFpcxFn, 0, FILLER).init("FPCX");
	private FixedLengthStringData wsaaFpcxRunid = new FixedLengthStringData(2).isAPartOf(wsaaFpcxFn, 4);
	private ZonedDecimalData wsaaFpcxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaFpcxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

		/* WSYS-SYSTEM-ERROR-PARAMS */
	private FixedLengthStringData wsysFpcokey = new FixedLengthStringData(28);
	private FixedLengthStringData wsysChdrcoy = new FixedLengthStringData(2).isAPartOf(wsysFpcokey, 0);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysFpcokey, 2);
	private FixedLengthStringData wsysCoverage = new FixedLengthStringData(2).isAPartOf(wsysFpcokey, 10);
	private FixedLengthStringData wsysLife = new FixedLengthStringData(2).isAPartOf(wsysFpcokey, 12);
	private FixedLengthStringData wsysRider = new FixedLengthStringData(2).isAPartOf(wsysFpcokey, 14);
	private ZonedDecimalData wsysPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsysFpcokey, 16).setUnsigned();
	private ZonedDecimalData wsysTargfrom = new ZonedDecimalData(8, 0).isAPartOf(wsysFpcokey, 20).setUnsigned();
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*component file view for dealing (debt on*/
	private CovrudlTableDAM covrudlIO = new CovrudlTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Flexible Premiums Coverage Logical*/
	private FpcoTableDAM fpcoIO = new FpcoTableDAM();
		/*Flexible Premium Reporting*/
	private FrepTableDAM frepIO = new FrepTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private P6671par p6671par = new P6671par();
	private T5679rec t5679rec = new T5679rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		pass2050, 
		exit2090
	}

	public B5363() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		p6671par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		wsaaFpcxRunid.set(bprdIO.getSystemParam04());
		wsaaFpcxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(FPCXPF) TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaFpcxFn);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		fpcxpf.openInput();
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1688);
		descIO.setDescitem(bprdIO.getAuthCode());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		covrudlIO.setDataKey(SPACES);
		covrudlIO.setPlanSuffix(ZERO);
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case pass2050: 
					pass2050();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		fpcxpf.read(fpcxpfRec);
		if (fpcxpf.isAtEnd()) {
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		if (isNE(wsspEdterror,varcom.endp)) {
			goTo(GotoLabel.pass2050);
		}
	}

protected void pass2050()
	{
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
	}

protected void edit2500()
	{
					edit2510();
					pass2550();
				}

protected void edit2510()
	{
		wsspEdterror.set(varcom.oK);
		/* Read the COVR file to get the statii.*/
		if (isEQ(fpcxpfRec.chdrcoy,covrudlIO.getChdrcoy())
		&& isEQ(fpcxpfRec.chdrnum,covrudlIO.getChdrnum())
		&& isEQ(fpcxpfRec.life,covrudlIO.getLife())
		&& isEQ(fpcxpfRec.rider,covrudlIO.getRider())
		&& isEQ(fpcxpfRec.coverage,covrudlIO.getCoverage())
		&& isEQ(fpcxpfRec.planSuffix,covrudlIO.getPlanSuffix())) {
			/*     MOVE 'N'                 TO WSAA-KEY-CHANGE       <A06923>*/
			return ;
		}
		covrudlIO.setChdrcoy(fpcxpfRec.chdrcoy);
		covrudlIO.setChdrnum(fpcxpfRec.chdrnum);
		covrudlIO.setLife(fpcxpfRec.life);
		covrudlIO.setRider(fpcxpfRec.rider);
		covrudlIO.setCoverage(fpcxpfRec.coverage);
		covrudlIO.setPlanSuffix(fpcxpfRec.planSuffix);
		covrudlIO.setFunction(varcom.readr);
		covrudlIO.setFormat(covrudlrec);
		SmartFileCode.execute(appVars, covrudlIO);
		if (isNE(covrudlIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrudlIO.getParams());
			fatalError600();
		}
		/*  Validate the statii of the coverage.*/
		wsaaValidCoverage.set("N");
		/*  MOVE 'Y'                    TO WSAA-KEY-CHANGE.      <A06923>*/
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub,12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()],covrudlIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub,12)); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()],covrudlIO.getPstatcode())) {
						wsaaT5679Sub.set(13);
						wsaaValidCoverage.set("Y");
					}
				}
			}
		}
	}

protected void pass2550()
	{
		if (!validCoverage.isTrue()) {
			wsspEdterror.set(SPACES);
			contotrec.totno.set(ct03);
			contotrec.totval.set(1);
			callContot001();
		}
		else {
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		/* Update database records.*/
		/* If first page/overflow - write standard headings*/
		/*   IF NEW-PAGE-REQ                                              */
		/*      WRITE PRINTER-REC      FROM R5363-H01                     */
		/*                           FORMAT 'R5363H01'                    */
		/*                       INDICATORS INDIC-AREA                    */
		/*      MOVE 'N'                 TO WSAA-OVERFLOW                 */
		/*   END-IF.                                                      */
		/*   IF KEY-CHANGE AND                                     <D9604>*/
		/*      WSAA-TOT-REQD NOT = ZERO                           <D9604>*/
		/*      MOVE WSAA-TOT-REQD       TO RD02-MINREQD           <D9604>*/
		/*      MOVE ZERO                TO WSAA-TOT-REQD          <D9604>*/
		/*      WRITE PRINTER-REC         FROM R5363-D02           <D9604>*/
		/*                                FORMAT 'R5363D02'        <D9604>*/
		/*                       INDICATORS INDIC-AREA             <D9604>*/
		/*      EOP MOVE 'Y'             TO WSAA-OVERFLOW          <D9604>*/
		/*   END-IF                                                <D9604>*/
		/*  Write detail, checking for page overflow*/
		/*   MOVE SPACES                 TO PRINTER-REC.           <D9604>*/
		/*                                                         <D9604>*/
		/*   MOVE CHDRNUM                TO RD01-CHDRNUM.                 */
		/*   MOVE LIFE                   TO RD01-LIFE.                    */
		/*   MOVE COVERAGE               TO RD01-COVERAGE.                */
		/*   MOVE RIDER                  TO RD01-RIDER.            <D9604>*/
		/*   MOVE PLNSFX                 TO RD01-PLNSFX.           <D9604>*/
		/*   MOVE PRMPER                 TO RD01-PRMPER.                  */
		/*   MOVE BILLEDP                TO RD01-BILLEDP.                 */
		/*   MOVE MINOVRPRO              TO RD01-MINOVRPRO.        <D9604>*/
		/*   MOVE PRMRCDP                TO RD01-PRMRCDP.                 */
		/*   COMPUTE RD01-OVRMINREQ  = OVRMINREQ - PRMRCDP.        <D9604>*/
		/*   ADD  RD01-OVRMINREQ         TO WSAA-TOT-REQD.         <D9604>*/
		/*Convert dates to external format.                               */
		/*   MOVE TARGTO                 TO DTC1-INT-DATE.                */
		/*   MOVE CONV                   TO DTC1-FUNCTION.                */
		/*   CALL 'DATCON1'              USING DTC1-DATCON1-REC.          */
		/*   MOVE DTC1-EXT-DATE          TO RD01-TARGTO.           <D9604>*/
		/*   WRITE PRINTER-REC         FROM R5363-D01              <D9604>*/
		/*                           FORMAT 'R5363D01'             <D9604>*/
		/*                       INDICATORS INDIC-AREA             <D9604>*/
		/*      EOP MOVE 'Y'             TO WSAA-OVERFLOW.         <D9604>*/
		frepIO.setParams(SPACES);
		frepIO.setBtdate(varcom.maxdate);
		frepIO.setAnnivProcDate(varcom.maxdate);
		frepIO.setLinstamt(ZERO);
		frepIO.setMinprem(ZERO);
		frepIO.setMaxprem(ZERO);
		frepIO.setSusamt(ZERO);
		frepIO.setPayrseqno(ZERO);
		frepIO.setCompany(fpcxpfRec.chdrcoy);
		frepIO.setChdrnum(fpcxpfRec.chdrnum);
		frepIO.setLife(fpcxpfRec.life);
		frepIO.setCoverage(fpcxpfRec.coverage);
		frepIO.setRider(fpcxpfRec.rider);
		frepIO.setPlanSuffix(fpcxpfRec.planSuffix);
		frepIO.setTargto(fpcxpfRec.targto);
		frepIO.setTargfrom(fpcxpfRec.targfrom);
		frepIO.setTargetPremium(fpcxpfRec.targetPremium);
		frepIO.setBilledInPeriod(fpcxpfRec.billedInPeriod);
		frepIO.setPremRecPer(fpcxpfRec.premRecPer);
		frepIO.setMinOverduePer(fpcxpfRec.minOverduePer);
		frepIO.setOverdueMin(fpcxpfRec.overdueMin);
		frepIO.setProcflag("O");
		frepIO.setFormat(freprec);
		frepIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, frepIO);
		if (isNE(frepIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(frepIO.getParams());
			fatalError600();
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close any open files.*/
		fpcxpf.close();
		/*   CLOSE PRINTER-FILE.                                          */
		wsaaQcmdexc.set("DLTOVR FILE(FPCXPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
