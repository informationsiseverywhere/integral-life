/*
 * File: Overaloc.java
 * Date: 29 August 2009 23:01:53
 * Author: Quipoz Limited
 * 
 * Class transformed from OVERALOC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.recordstructures.Rnlallrec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrunlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkrnlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UnltunlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5536rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5537rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5545rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*          OVER-TARGET ALLOCATION FOR FLEXIBLE PREMIUMS
*          --------------------------------------------
*
*  This routine allocates units for flexible premiums contracts wh se
*  last paid premium has partially or fully exceeded the target fo 
*  this period.
*
*  After reading the necessary tables, the processing follows:-
*
*  - From the first relevant entry on T5536 use the over-allocatio 
*    key to read the table again.  Find the relevant billfreq,
*    calculate the number of frequencies between the contract
*    commencement and business dates to find the correct percentag 
*    entry.
*
*  - Calculate initial, accumulation and non-invested as follows:
*
*         Non-Invest = RNLA instprem * (100-T5536 % )
*                                      --------------
*                                          100
*
*            Initial = (RNLA instprem - Non Invest) * T5536 %
*                                                     -------
*                                                     100
*
*      Accumulation  = RNLA instprem - (Non Invest + initial)
*
*  - Next calculate any enhancement on the above figures.
*
*  - Read ULNK record for fund proportions.
*
*  - Write UTRN for each fund.
*
*  - If Non Invest amount > 0 write a UTRN for that.
*
****************************************************************** ****
* </pre>
*/
public class Overaloc extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "OVERALOC";
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT5537Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBasisSelect = new FixedLengthStringData(3).isAPartOf(wsaaT5537Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5537Sex = new FixedLengthStringData(1).isAPartOf(wsaaT5537Key, 3).init(SPACES);
	private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaT5537Key, 4).init(SPACES);

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrCde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaContract = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 4);
	private FixedLengthStringData wsaaLifeCltsex = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaLifeAnbAtCcd = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaJlifeCltsex = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaJlifeAnbAtCcd = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaAllocBasis = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaY = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaZ = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaMoniesDate = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaEnhancePerc = new PackedDecimalData(5, 2).init(ZERO);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT5545Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5545Enhall = new FixedLengthStringData(4).isAPartOf(wsaaT5545Item, 0);
	private FixedLengthStringData wsaaT5545Cntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT5545Item, 4);

		/* WSAA-T5536-VALUES */
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(100); //58
	private ZonedDecimalData wsaaBillfreqNum = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreq, 0).setUnsigned();
	private FixedLengthStringData wsaaMaxPeriods = new FixedLengthStringData(28).isAPartOf(wsaaBillfreq, 2);  //16
	private ZonedDecimalData[] wsaaMaxPeriod = ZDArrayPartOfStructure(7, 4, 0, wsaaMaxPeriods, 0);
	private FixedLengthStringData wsaaPcUnits = new FixedLengthStringData(35).isAPartOf(wsaaBillfreq, 30);  //18
	private ZonedDecimalData[] wsaaPcUnit = ZDArrayPartOfStructure(7, 5, 2, wsaaPcUnits, 0);
	private FixedLengthStringData wsaaPcInitUnits = new FixedLengthStringData(35).isAPartOf(wsaaBillfreq, 65); //38
	private ZonedDecimalData[] wsaaPcInitUnit = ZDArrayPartOfStructure(7, 5, 2, wsaaPcInitUnits, 0);
	private PackedDecimalData wsaaRiskCessTerm = new PackedDecimalData(3, 0);
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String e616 = "E616";
	private static final String e706 = "E706";
	private static final String e707 = "E707";
	private static final String utrnrec = "UTRNREC";
	private static final String ulnkrnlrec = "ULNKRNLREC";
	private static final String itemrec = "ITEMREC";
	private static final String hitrrec = "HITRREC";
	private static final String t5540 = "T5540";
	private static final String t6647 = "T6647";
	private static final String t5537 = "T5537";
	private static final String t5536 = "T5536";
	private static final String t5545 = "T5545";
	private static final String t5645 = "T5645";
	private static final String t5515 = "T5515";
	private static final String t5688 = "T5688";
	private CovrunlTableDAM covrunlIO = new CovrunlTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitrTableDAM hitrIO = new HitrTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
	private UlnkrnlTableDAM ulnkrnlIO = new UlnkrnlTableDAM();
	private UnltunlTableDAM unltunlIO = new UnltunlTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T5540rec t5540rec = new T5540rec();
	private T6647rec t6647rec = new T6647rec();
	private T5688rec t5688rec = new T5688rec();
	private T5537rec t5537rec = new T5537rec();
	private T5536rec t5536rec = new T5536rec();
	private T5545rec t5545rec = new T5545rec();
	private T5645rec t5645rec = new T5645rec();
	private T5515rec t5515rec = new T5515rec();
	private Rnlallrec rnlallrec = new Rnlallrec();
	private WsaaAmountHoldersInner wsaaAmountHoldersInner = new WsaaAmountHoldersInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		cont145, 
		exit199, 
		readItem615, 
		cont620, 
		yOrdinate640, 
		increment660, 
		xOrdinate680, 
		increment685, 
		exit4999, 
		enhanb4520, 
		enhanc4530, 
		enhand4540, 
		enhane4550, 
		enhanf4560, 
		exit4599
	}

	public Overaloc() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		rnlallrec.rnlallRec = convertAndSetParam(rnlallrec.rnlallRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		rnlallrec.statuz.set(varcom.oK);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		readTables1000();
		loadPercents2000();
		calcPrem3000();
		calcEnhance4000();
		setUpUtrn5000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readTables1000()
	{
		/*PARA*/
		varcom.vrcmTime.set(getCobolTime());
		getT6647100();
		getT5545200();
		getT5540300();
		readLife400();
		readChdrCovr500();
		getT5537600();
		getT5536700();
		getT5688800();
		getT5645900();
		/*EXIT*/
	}

protected void getT6647100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start110();
				case cont145: 
					cont145();
				case exit199: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start110()
	{
		/* Read T6647 for unit linked contract details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(t6647);
		wsaaTrCde.set(rnlallrec.batctrcde);
		wsaaContract.set(rnlallrec.cnttype);
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(rnlallrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(), rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), t6647))
		|| (isNE(itdmIO.getItemitem(), wsaaT6647Key))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setItemcoy(rnlallrec.company);
			itdmIO.setItemtabl(t6647);
			itdmIO.setItemitem(wsaaT6647Key);
			itdmIO.setItmfrm(rnlallrec.effdate);
			syserrrec.params.set(itdmIO.getParams());
			goTo(GotoLabel.cont145);
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
			goTo(GotoLabel.exit199);
		}
	}

protected void cont145()
	{
		wsaaTrCde.set(rnlallrec.batctrcde);
		wsaaContract.set("***");
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setItemcoy(rnlallrec.company);
			wsaaTrCde.set(rnlallrec.batctrcde);
			wsaaContract.set(rnlallrec.cnttype);
			itdmIO.setItemitem(wsaaT6647Key);
			itdmIO.setItmfrm(rnlallrec.effdate);
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(), rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), t6647))
		|| (isNE(itdmIO.getItemitem(), wsaaT6647Key))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			dbError8100();
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT5545200()
	{
		start210();
	}

protected void start210()
	{
		/* Read T5545 to get the enhancement percentage.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(t5545);
		itdmIO.setItmfrm(rnlallrec.effdate);
		wsaaT5545Cntcurr.set(rnlallrec.cntcurr);
		wsaaT5545Enhall.set(t6647rec.enhall);
		itdmIO.setItemitem(wsaaT5545Item);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.endp))
		&& (isNE(itdmIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		if ((isNE(itdmIO.getItemcoy(), rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), t5545))
		|| (isNE(itdmIO.getItemitem(), wsaaT5545Item))) {
			itdmIO.setGenarea(SPACES);
		}
		t5545rec.t5545Rec.set(itdmIO.getGenarea());
	}

protected void getT5540300()
	{
		start310();
	}

protected void start310()
	{
		/*  Read T5540 for general unit linked details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(t5540);
		itdmIO.setItemitem(rnlallrec.crtable);
		itdmIO.setItmfrm(rnlallrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(), rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), t5540))
		|| (isNE(itdmIO.getItemitem(), rnlallrec.crtable))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setItemcoy(rnlallrec.company);
			itdmIO.setItemtabl(t5540);
			itdmIO.setItemitem(rnlallrec.crtable);
			itdmIO.setItmfrm(rnlallrec.effdate);
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		else {
			t5540rec.t5540Rec.set(itdmIO.getGenarea());
		}
	}

protected void readLife400()
	{
		start410();
	}

protected void start410()
	{
		lifelnbIO.setDataKey(SPACES);
		lifelnbIO.setChdrcoy(rnlallrec.company);
		lifelnbIO.setChdrnum(rnlallrec.chdrnum);
		lifelnbIO.setLife(rnlallrec.life);
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifelnbIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		|| (isNE(lifelnbIO.getChdrcoy(), rnlallrec.company)
		|| isNE(lifelnbIO.getChdrnum(), rnlallrec.chdrnum)
		|| isNE(lifelnbIO.getLife(), rnlallrec.life)
		|| isNE(lifelnbIO.getJlife(), "00"))) {
			syserrrec.params.set(lifelnbIO.getParams());
			dbError8100();
		}
		/* Need to see if a joint life exists. If so, use the younger of*/
		/* the two lives to find the unit allocation basis.*/
		wsaaLifeCltsex.set(lifelnbIO.getCltsex());
		wsaaLifeAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		lifelnbIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if ((isNE(lifelnbIO.getStatuz(), varcom.oK))
		&& (isNE(lifelnbIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(lifelnbIO.getParams());
			dbError8100();
		}
		if (isEQ(rnlallrec.company, lifelnbIO.getChdrcoy())
		&& isEQ(rnlallrec.chdrnum, lifelnbIO.getChdrnum())
		&& isEQ(rnlallrec.life, lifelnbIO.getLife())
		&& isNE(lifelnbIO.getStatuz(), varcom.endp)) {
			wsaaJlifeAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		}
		/* LIFELNB-CLTSEX and LIFELNB-ANB-AT-CCD are used to access*/
		/* T5536 and T5537, so we have to make sure that they have the*/
		/* correct informations in them.*/
		if (isGTE(wsaaJlifeAnbAtCcd, wsaaLifeAnbAtCcd)) {
			lifelnbIO.setCltsex(wsaaLifeCltsex);
			lifelnbIO.setAnbAtCcd(wsaaLifeAnbAtCcd);
		}
	}

protected void readChdrCovr500()
	{
		start510();
	}

protected void start510()
	{
		covrunlIO.setChdrcoy(rnlallrec.company);
		covrunlIO.setChdrnum(rnlallrec.chdrnum);
		covrunlIO.setLife(rnlallrec.life);
		covrunlIO.setCoverage(rnlallrec.coverage);
		covrunlIO.setRider(rnlallrec.rider);
		covrunlIO.setPlanSuffix(rnlallrec.planSuffix);
		covrunlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrunlIO);
		if (isNE(covrunlIO.getStatuz(), varcom.oK)) {
			covrunlIO.setParams(covrunlIO.getParams());
			dbError8100();
		}
	}

protected void getT5537600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start610();
				case readItem615: 
					readItem615();
				case cont620: 
					cont620();
				case yOrdinate640: 
					yOrdinate640();
				case increment660: 
					increment660();
				case xOrdinate680: 
					xOrdinate680();
				case increment685: 
					increment685();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start610()
	{
		/* Read T5537 for unit allocation basis.*/
		wsaaBasisSelect.set(t5540rec.allbas);
		wsaaTranCode.set(rnlallrec.batctrcde);
		wsaaT5537Sex.set(lifelnbIO.getCltsex());
	}

protected void readItem615()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(rnlallrec.company);
		itemIO.setItemtabl(t5537);
		itemIO.setItemitem(wsaaT5537Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.cont620);
		}
		/*    If NO record found then try first with the basis, * for*/
		/*    Sex and Transaction code.*/
		wsaaT5537Sex.set("*");
		itemIO.setItemitem(wsaaT5537Key);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.cont620);
		}
		/*    If NO record found then try secondly with the basis, Sex*/
		/*    and '****'  Transaction code.*/
		wsaaT5537Sex.set(lifelnbIO.getCltsex());
		wsaaTranCode.fill("*");
		itemIO.setItemitem(wsaaT5537Key);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.cont620);
		}
		/* If no match is found, try again with the global transaction*/
		/* code.*/
		wsaaTranCode.fill("*");
		/* If no match is found, try again with the global sex code*/
		wsaaT5537Sex.set("*");
		/*    When doing a global change and the global data is being*/
		/*    moved to the wsaa-t5537-key make sure that you move the*/
		/*    wsaa-t5537-key to the ITEM-ITEMITEM.*/
		itemIO.setItemitem(wsaaT5537Key);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.cont620);
		}
		/* And again if no match is found to prevent system falling over*/
		/* read T5537 with global key.*/
		itemIO.setItemitem("********");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
		}
	}

protected void cont620()
	{
		t5537rec.t5537Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	*  Work out the array's Y index based on ANB-AT-CCD.
	* </pre>
	*/
protected void yOrdinate640()
	{
		wsaaZ.set(0);
	}

protected void increment660()
	{
		wsaaZ.add(1);
		/* Check the continuation field for a key to re-read table.*/
		if (isGT(wsaaZ, 10)
		&& isNE(t5537rec.agecont, SPACES)) {
			readAgecont650();
			goTo(GotoLabel.yOrdinate640);
		}
		else {
			if (isGT(wsaaZ, 10)
			&& isNE(t5537rec.agecont, SPACES)) {
				syserrrec.params.set(itemIO.getDataArea());
				syserrrec.statuz.set(e706);
				dbError8100();
			}
		}
		if (isLTE(wsaaZ, 10)) {
			/*        IF  LIFELNB-ANB-AT-CCD           > T5537-TOAGE(WSAA-Z)   */
			if (isGT(lifelnbIO.getAnbAtCcd(), t5537rec.ageIssageTo[wsaaZ.toInt()])) {
				goTo(GotoLabel.increment660);
			}
			else {
				wsaaY.set(wsaaZ);
			}
		}
		else {
			wsaaT5537Key.set(t5537rec.agecont);
			goTo(GotoLabel.readItem615);
		}
	}

	/**
	* <pre>
	*  Work out the array's X index based on TERM.
	* </pre>
	*/
protected void xOrdinate680()
	{
		wsaaZ.set(0);
	}

protected void increment685()
	{
		wsaaZ.add(1);
		/* Check the term continuation field for a key to re-read table.*/
		if (isGT(wsaaZ, 9)
		&& isNE(t5537rec.trmcont, SPACES)) {
			readTermcont660();
			goTo(GotoLabel.xOrdinate680);
		}
		else {
			if (isGT(wsaaZ, 9)
			&& isEQ(t5537rec.trmcont, SPACES)) {
				syserrrec.params.set(itemIO.getDataArea());
				syserrrec.statuz.set(e707);
				dbError8100();
			}
		}
		/*    Always calculate the term.*/
		calculateTerm620();
		wsaaTerm.set(wsaaRiskCessTerm);
		if (isLTE(wsaaZ, 9)) {
			if (isGT(wsaaTerm, t5537rec.toterm[wsaaZ.toInt()])) {
				goTo(GotoLabel.increment685);
			}
			else {
				wsaaX.set(wsaaZ);
			}
		}
		else {
			wsaaT5537Key.set(t5537rec.trmcont);
			goTo(GotoLabel.readItem615);
		}
		/* Now we have the correct position(WSAA-X, WSAA-Y) of the basis.*/
		/* All we have to do now is to work out the corresponding position*/
		/* in the copybook by using these co-ordinates.(The copybook is*/
		/* defined as one-dimensional.)*/
		wsaaY.subtract(1);
		compute(wsaaIndex, 0).set(mult(wsaaY, 9));
		wsaaIndex.add(wsaaX);
		wsaaAllocBasis.set(t5537rec.actAllocBasis[wsaaIndex.toInt()]);
	}

protected void calculateTerm620()
	{
		/*START*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrunlIO.getCrrcd());
		datcon3rec.intDate2.set(covrunlIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			datcon3rec.freqFactor.set(0);
		}
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		if (isGT(datcon3rec.freqFactor, wsaaRiskCessTerm)) {
			wsaaRiskCessTerm.add(1);
		}
		/*EXIT*/
	}

protected void readAgecont650()
	{
		/*READ-AGECONT*/
		/* Read Age continuation Item.*/
		itemIO.setItemitem(t5537rec.agecont);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getDataArea());
			dbError8100();
		}
		t5537rec.t5537Rec.set(itemIO.getGenarea());
		wsaaIndex.subtract(99);
		/*EXIT*/
	}

protected void readTermcont660()
	{
		/*READ-TERMCONT*/
		/* Read Term continuation Item.*/
		itemIO.setItemitem(t5537rec.trmcont);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getDataArea());
			dbError8100();
		}
		t5537rec.t5537Rec.set(itemIO.getGenarea());
		/*EXIT*/
	}

protected void getT5536700()
	{
		start710();
	}

protected void start710()
	{
		/* Read T5536 for unit allocation basis details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(t5536);
		itdmIO.setItemitem(wsaaAllocBasis);
		itdmIO.setItmfrm(rnlallrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(), rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), t5536))
		|| (isNE(itdmIO.getItemitem(), wsaaAllocBasis))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setItemcoy(rnlallrec.company);
			itdmIO.setItemtabl(t5536);
			itdmIO.setItemitem(wsaaAllocBasis);
			itdmIO.setItmfrm(rnlallrec.effdate);
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		else {
			t5536rec.t5536Rec.set(itdmIO.getGenarea());
		}
		/*  Over-target key should be on this table too.*/
		if (isEQ(t5536rec.yotalmth, SPACES)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		itdmIO.setItemitem(t5536rec.yotalmth);
		wsaaAllocBasis.set(t5536rec.yotalmth);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(), rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), t5536))
		|| (isNE(itdmIO.getItemitem(), wsaaAllocBasis))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setItemcoy(rnlallrec.company);
			itdmIO.setItemtabl(t5536);
			itdmIO.setItemitem(wsaaAllocBasis);
			itdmIO.setItmfrm(rnlallrec.effdate);
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		else {
			t5536rec.t5536Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT5688800()
	{
		start810();
	}

protected void start810()
	{
		itdmIO.setItemcoy(covrunlIO.getChdrcoy());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(rnlallrec.cnttype);
		itdmIO.setItmfrm(rnlallrec.effdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(), covrunlIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), rnlallrec.cnttype)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(rnlallrec.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			dbError8100();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void getT5645900()
	{
		start910();
	}

protected void start910()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(rnlallrec.company);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void loadPercents2000()
	{
		start2010();
	}

protected void start2010()
	{
		/* There are 6 sets of details in the table, each set is led by*/
		/* the billing frequency. So to find the correct set of details,*/
		/* match each of these frequencies with the billing frequency*/
		/* specified in linkage.*/
		for (wsaaZ.set(1); !(isEQ(t5536rec.billfreq[wsaaZ.toInt()], rnlallrec.billfreq)
		|| isGT(wsaaZ, 6)); wsaaZ.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isEQ(wsaaZ, 1)){
			wsaaBillfreq.set(t5536rec.billfreq01);
			wsaaMaxPeriods.set(t5536rec.maxPeriodas);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsas);
			wsaaPcUnits.set(t5536rec.pcUnitsas);
		}
		else if (isEQ(wsaaZ, 2)){
			wsaaBillfreq.set(t5536rec.billfreq02);
			wsaaMaxPeriods.set(t5536rec.maxPeriodbs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsbs);
			wsaaPcUnits.set(t5536rec.pcUnitsbs);
		}
		else if (isEQ(wsaaZ, 3)){
			wsaaBillfreq.set(t5536rec.billfreq03);
			wsaaMaxPeriods.set(t5536rec.maxPeriodcs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitscs);
			wsaaPcUnits.set(t5536rec.pcUnitscs);
		}
		else if (isEQ(wsaaZ, 4)){
			wsaaBillfreq.set(t5536rec.billfreq04);
			wsaaMaxPeriods.set(t5536rec.maxPeriodds);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsds);
			wsaaPcUnits.set(t5536rec.pcUnitsds);
		}
		else if (isEQ(wsaaZ, 5)){
			wsaaBillfreq.set(t5536rec.billfreq05);
			wsaaMaxPeriods.set(t5536rec.maxPeriodes);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitses);
			wsaaPcUnits.set(t5536rec.pcUnitses);
		}
		else if (isEQ(wsaaZ, 6)){
			wsaaBillfreq.set(t5536rec.billfreq06);
			wsaaMaxPeriods.set(t5536rec.maxPeriodfs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsfs);
			wsaaPcUnits.set(t5536rec.pcUnitsfs);
		}
		else{
			syserrrec.statuz.set(e616);
			systemError8000();
		}
	}

protected void calcPrem3000()
	{
		start3010();
	}

protected void start3010()
	{
		datcon3rec.intDate1.set(covrunlIO.getCrrcd());
		datcon3rec.intDate2.set(datcon1rec.intDate);
		datcon3rec.frequency.set(rnlallrec.billfreq);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			systemError8000();
		}
		wsaaZ.set(1);
		while ( !(isGT(wsaaZ, 4)
		|| isGTE(wsaaMaxPeriod[wsaaZ.toInt()], datcon3rec.freqFactor))) {
			if (isLT(wsaaMaxPeriod[wsaaZ.toInt()], datcon3rec.freqFactor)) {
				wsaaZ.add(1);
			}
		}
		
		if (isGT(wsaaZ, 4)) {
			syserrrec.params.set(t5536rec.t5536Rec);
			dbError8100();
		}
		/*  Calculate the uninvested amount.*/
		compute(wsaaAmountHoldersInner.wsaaNonInvestPrem, 3).setRounded(div((mult(rnlallrec.covrInstprem, (sub(100, wsaaPcUnit[wsaaZ.toInt()])))), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaNonInvestPrem.set(zrdecplrec.amountOut);
		/*  Calculate the initial amount.*/
		compute(wsaaAmountHoldersInner.wsaaAllocInit, 3).setRounded(div(mult((sub(rnlallrec.covrInstprem, wsaaAmountHoldersInner.wsaaNonInvestPrem)), wsaaPcInitUnit[wsaaZ.toInt()]), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocInit);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaAllocInit.set(zrdecplrec.amountOut);
		/*  Calculate the accumulative amount.*/
		compute(wsaaAmountHoldersInner.wsaaAllocAccum, 3).setRounded(sub(rnlallrec.covrInstprem, (add(wsaaAmountHoldersInner.wsaaNonInvestPrem, wsaaAmountHoldersInner.wsaaAllocInit))));
	}

protected void calcEnhance4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start4010();
				case exit4999: 
					start4510();
				case enhanb4520: 
					enhanb4520();
				case enhanc4530: 
					enhanc4530();
				case enhand4540: 
					enhand4540();
				case enhane4550: 
					enhane4550();
				case enhanf4560: 
					enhanf4560();
				case exit4599: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start4010()
	{
		if (isEQ(t6647rec.enhall, SPACES)
		|| isEQ(t5545rec.t5545Rec, SPACES)) {
			goTo(GotoLabel.exit4999);
		}
		wsaaEnhancePerc.set(0);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 5)
		|| (isNE(wsaaEnhancePerc, 0))); wsaaIndex.add(1))
{
			/* No processing required. */
		}
		if (isEQ(wsaaEnhancePerc, 0)) {
			goTo(GotoLabel.exit4999);
		}
		/* Calculate the enhanced premiums.*/
		wsaaAmountHoldersInner.wsaaOldAllocInit.set(wsaaAmountHoldersInner.wsaaAllocInit);
		compute(wsaaAmountHoldersInner.wsaaAllocInit, 3).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocInit, wsaaEnhancePerc), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocInit);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaAllocInit.set(zrdecplrec.amountOut);
		compute(wsaaAmountHoldersInner.wsaaEnhanceAlloc, 0).set(sub(wsaaAmountHoldersInner.wsaaOldAllocInit, wsaaAmountHoldersInner.wsaaAllocInit));
		wsaaAmountHoldersInner.wsaaOldAllocAccum.set(wsaaAmountHoldersInner.wsaaAllocAccum);
		compute(wsaaAmountHoldersInner.wsaaAllocAccum, 3).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocAccum, wsaaEnhancePerc), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocAccum);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaAllocAccum.set(zrdecplrec.amountOut);
		compute(wsaaAmountHoldersInner.wsaaEnhanceAlloc, 0).set(add(wsaaAmountHoldersInner.wsaaEnhanceAlloc, (sub(wsaaAmountHoldersInner.wsaaOldAllocAccum, wsaaAmountHoldersInner.wsaaAllocAccum))));
	}

protected void start4510()
	{
		/* First match the billing frequency.*/
		if (isEQ(t5545rec.billfreq[wsaaIndex.toInt()], SPACES)) {
			goTo(GotoLabel.exit4599);
		}
		/* Once the billing frequency is matched, match the premium.*/
		/* There are 5 premiums in each billing frequency set, so*/
		/* match each premium individually.*/
		if (isNE(wsaaIndex, 1)) {
			goTo(GotoLabel.enhanb4520);
		}
		if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhanaPrm05))
		&& (isNE(t5545rec.enhanaPrm05, 0))) {
			wsaaEnhancePerc.set(t5545rec.enhanaPc05);
		}
		else {
			if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhanaPrm04))
			&& (isNE(t5545rec.enhanaPrm04, 0))) {
				wsaaEnhancePerc.set(t5545rec.enhanaPc04);
			}
			else {
				if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhanaPrm03))
				&& (isNE(t5545rec.enhanaPrm03, 0))) {
					wsaaEnhancePerc.set(t5545rec.enhanaPc03);
				}
				else {
					if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhanaPrm02))
					&& (isNE(t5545rec.enhanaPrm02, 0))) {
						wsaaEnhancePerc.set(t5545rec.enhanaPc02);
					}
					else {
						wsaaEnhancePerc.set(t5545rec.enhanaPc01);
					}
				}
			}
		}
		goTo(GotoLabel.exit4599);
	}

protected void enhanb4520()
	{
		if (isNE(wsaaIndex, 2)) {
			goTo(GotoLabel.enhanc4530);
		}
		if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhanbPrm05))
		&& (isNE(t5545rec.enhanbPrm05, 0))) {
			wsaaEnhancePerc.set(t5545rec.enhanbPc05);
		}
		else {
			if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhanbPrm04))
			&& (isNE(t5545rec.enhanbPrm04, 0))) {
				wsaaEnhancePerc.set(t5545rec.enhanbPc04);
			}
			else {
				if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhanbPrm03))
				&& (isNE(t5545rec.enhanbPrm03, 0))) {
					wsaaEnhancePerc.set(t5545rec.enhanbPc03);
				}
				else {
					if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhanbPrm02))
					&& (isNE(t5545rec.enhanbPrm02, 0))) {
						wsaaEnhancePerc.set(t5545rec.enhanbPc02);
					}
					else {
						wsaaEnhancePerc.set(t5545rec.enhanbPc01);
					}
				}
			}
		}
		goTo(GotoLabel.exit4599);
	}

protected void enhanc4530()
	{
		if (isNE(wsaaIndex, 3)) {
			goTo(GotoLabel.enhand4540);
		}
		if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhancPrm05))
		&& (isNE(t5545rec.enhancPrm05, 0))) {
			wsaaEnhancePerc.set(t5545rec.enhancPc05);
		}
		else {
			if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhancPrm04))
			&& (isNE(t5545rec.enhancPrm04, 0))) {
				wsaaEnhancePerc.set(t5545rec.enhancPc04);
			}
			else {
				if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhancPrm03))
				&& (isNE(t5545rec.enhancPrm03, 0))) {
					wsaaEnhancePerc.set(t5545rec.enhancPc03);
				}
				else {
					if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhancPrm02))
					&& (isNE(t5545rec.enhancPrm02, 0))) {
						wsaaEnhancePerc.set(t5545rec.enhancPc02);
					}
					else {
						wsaaEnhancePerc.set(t5545rec.enhancPc01);
					}
				}
			}
		}
		goTo(GotoLabel.exit4599);
	}

protected void enhand4540()
	{
		if (isNE(wsaaIndex, 4)) {
			goTo(GotoLabel.enhane4550);
		}
		if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhandPrm05))
		&& (isNE(t5545rec.enhandPrm05, 0))) {
			wsaaEnhancePerc.set(t5545rec.enhandPc05);
		}
		else {
			if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhandPrm04))
			&& (isNE(t5545rec.enhandPrm04, 0))) {
				wsaaEnhancePerc.set(t5545rec.enhandPc04);
			}
			else {
				if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhandPrm03))
				&& (isNE(t5545rec.enhandPrm03, 0))) {
					wsaaEnhancePerc.set(t5545rec.enhandPc03);
				}
				else {
					if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhandPrm02))
					&& (isNE(t5545rec.enhandPrm02, 0))) {
						wsaaEnhancePerc.set(t5545rec.enhandPc02);
					}
					else {
						wsaaEnhancePerc.set(t5545rec.enhandPc01);
					}
				}
			}
		}
		goTo(GotoLabel.exit4599);
	}

protected void enhane4550()
	{
		if (isNE(wsaaIndex, 5)) {
			goTo(GotoLabel.enhanf4560);
		}
		if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhanePrm05))
		&& (isNE(t5545rec.enhanePrm05, 0))) {
			wsaaEnhancePerc.set(t5545rec.enhanePc05);
		}
		else {
			if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhanePrm04))
			&& (isNE(t5545rec.enhanePrm04, 0))) {
				wsaaEnhancePerc.set(t5545rec.enhanePc04);
			}
			else {
				if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhanePrm03))
				&& (isNE(t5545rec.enhanePrm03, 0))) {
					wsaaEnhancePerc.set(t5545rec.enhanePc03);
				}
				else {
					if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhanePrm02))
					&& (isNE(t5545rec.enhanePrm02, 0))) {
						wsaaEnhancePerc.set(t5545rec.enhanePc02);
					}
					else {
						wsaaEnhancePerc.set(t5545rec.enhanePc01);
					}
				}
			}
		}
		goTo(GotoLabel.exit4599);
	}

protected void enhanf4560()
	{
		if (isNE(wsaaIndex, 6)) {
			return ;
		}
		if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhanfPrm05))
		&& (isNE(t5545rec.enhanfPrm05, 0))) {
			wsaaEnhancePerc.set(t5545rec.enhanfPc05);
		}
		else {
			if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhanfPrm04))
			&& (isNE(t5545rec.enhanfPrm04, 0))) {
				wsaaEnhancePerc.set(t5545rec.enhanfPc04);
			}
			else {
				if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhanfPrm03))
				&& (isNE(t5545rec.enhanfPrm03, 0))) {
					wsaaEnhancePerc.set(t5545rec.enhanfPc03);
				}
				else {
					if ((isGTE(rnlallrec.covrInstprem, t5545rec.enhanfPrm02))
					&& (isNE(t5545rec.enhanfPrm02, 0))) {
						wsaaEnhancePerc.set(t5545rec.enhanfPc02);
					}
					else {
						wsaaEnhancePerc.set(t5545rec.enhanfPc01);
					}
				}
			}
		}
	}

protected void setUpUtrn5000()
	{
		start5100();
	}

protected void start5100()
	{
		ulnkrnlIO.setDataKey(SPACES);
		ulnkrnlIO.setChdrcoy(rnlallrec.company);
		ulnkrnlIO.setChdrnum(rnlallrec.chdrnum);
		ulnkrnlIO.setLife(rnlallrec.life);
		ulnkrnlIO.setCoverage(rnlallrec.coverage);
		ulnkrnlIO.setRider(rnlallrec.rider);
		ulnkrnlIO.setPlanSuffix(rnlallrec.planSuffix);
		ulnkrnlIO.setFunction(varcom.readr);
		ulnkrnlIO.setFormat(ulnkrnlrec);
		SmartFileCode.execute(appVars, ulnkrnlIO);
		if (isNE(ulnkrnlIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(ulnkrnlIO.getStatuz());
			syserrrec.params.set(ulnkrnlIO.getParams());
			dbError8100();
		}
		if (isNE(covrunlIO.getReserveUnitsDate(), ZERO)
		&& isNE(covrunlIO.getReserveUnitsDate(), varcom.vrcmMaxDate)) {
			wsaaMoniesDate.set(covrunlIO.getReserveUnitsDate());
		}
		else {
			wsaaMoniesDate.set(rnlallrec.moniesDate);
		}
		wsaaAmountHoldersInner.wsaaRunInitTot.set(0);
		wsaaAmountHoldersInner.wsaaRunAccumTot.set(0);
		wsaaAmountHoldersInner.wsaaTotIbSplit.set(0);
		wsaaAmountHoldersInner.wsaaTotUlSplit.set(0);
		for (wsaaIndex.set(1); !((isGT(wsaaIndex, 10))
		|| (isEQ(ulnkrnlIO.getUalfnd(wsaaIndex), SPACES))); wsaaIndex.add(1)){
			fundSplitAllocation5500();
		}
		/* Write a UTRN record for the uninvested premium.*/
		compute(wsaaAmountHoldersInner.wsaaNonInvestPrem, 0).set(add(wsaaAmountHoldersInner.wsaaNonInvestPrem, wsaaAmountHoldersInner.wsaaEnhanceAlloc));
		if (isEQ(wsaaAmountHoldersInner.wsaaNonInvestPrem, 0)) {
			return ;
		}
		utrnIO.setUnitSubAccount("NVST");
		utrnIO.setUnitVirtualFund(SPACES);
		utrnIO.setUnitType(SPACES);
		/* MOVE WSAA-NON-INVEST-PREM TO UTRN-CONTRACT-AMOUNT            */
		if (isEQ(wsaaAmountHoldersInner.wsaaTotIbSplit, 0)) {
			utrnIO.setContractAmount(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		}
		else {
			setPrecision(utrnIO.getContractAmount(), 1);
			utrnIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaNonInvestPrem, wsaaAmountHoldersInner.wsaaTotUlSplit), 100), true);
		}
		zrdecplrec.amountIn.set(utrnIO.getContractAmount());
		a000CallRounding();
		utrnIO.setContractAmount(zrdecplrec.amountOut);
		utrnIO.setDiscountFactor(0);
		if (isNE(utrnIO.getContractAmount(), 0)) {
			writeUtrn6000();
		}
		hitrIO.setZintbfnd(SPACES);
		if (isEQ(wsaaAmountHoldersInner.wsaaTotUlSplit, 0)) {
			hitrIO.setContractAmount(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		}
		else {
			setPrecision(hitrIO.getContractAmount(), 2);
			hitrIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaNonInvestPrem, utrnIO.getContractAmount()));
		}
		if (isNE(hitrIO.getContractAmount(), 0)) {
			writeHitr7000();
		}
	}

protected void writeUtrn6000()
	{
		start6010();
	}

protected void start6010()
	{
		/* PERFORM 5550-READ-T5515.                                     */
		utrnIO.setChdrcoy(rnlallrec.company);
		utrnIO.setChdrnum(rnlallrec.chdrnum);
		utrnIO.setCoverage(rnlallrec.coverage);
		utrnIO.setLife(rnlallrec.life);
		utrnIO.setRider(rnlallrec.rider);
		utrnIO.setPlanSuffix(rnlallrec.planSuffix);
		utrnIO.setTranno(rnlallrec.tranno);
		utrnIO.setTransactionTime(varcom.vrcmTime);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		utrnIO.setTransactionDate(datcon1rec.intDate);
		utrnIO.setUser(rnlallrec.user);
		utrnIO.setBatccoy(rnlallrec.batccoy);
		utrnIO.setBatcbrn(rnlallrec.batcbrn);
		utrnIO.setBatcactyr(rnlallrec.batcactyr);
		utrnIO.setBatcactmn(rnlallrec.batcactmn);
		utrnIO.setBatctrcde(rnlallrec.batctrcde);
		utrnIO.setBatcbatch(rnlallrec.batcbatch);
		utrnIO.setCrtable(rnlallrec.crtable);
		utrnIO.setCntcurr(rnlallrec.cntcurr);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			if (isEQ(utrnIO.getUnitSubAccount(), "NVST")) {
				utrnIO.setSacscode(t5645rec.sacscode04);
				utrnIO.setSacstyp(t5645rec.sacstype04);
				utrnIO.setGenlcde(t5645rec.glmap04);
			}
			else {
				utrnIO.setSacscode(t5645rec.sacscode03);
				utrnIO.setSacstyp(t5645rec.sacstype03);
				utrnIO.setGenlcde(t5645rec.glmap03);
			}
		}
		else {
			if (isEQ(utrnIO.getUnitSubAccount(), "NVST")) {
				utrnIO.setSacscode(t5645rec.sacscode02);
				utrnIO.setSacstyp(t5645rec.sacstype02);
				utrnIO.setGenlcde(t5645rec.glmap02);
			}
			else {
				utrnIO.setSacscode(t5645rec.sacscode01);
				utrnIO.setSacstyp(t5645rec.sacstype01);
				utrnIO.setGenlcde(t5645rec.glmap01);
			}
		}
		utrnIO.setContractType(rnlallrec.cnttype);
		utrnIO.setSvp(1);
		utrnIO.setCrComDate(covrunlIO.getCrrcd());
		utrnIO.setFundCurrency(t5515rec.currcode);
		if (isLT(utrnIO.getContractAmount(), 0)) {
			utrnIO.setNowDeferInd(t6647rec.dealin);
		}
		else {
			utrnIO.setNowDeferInd(t6647rec.aloind);
		}
		utrnIO.setMoniesDate(wsaaMoniesDate);
		utrnIO.setProcSeqNo(t6647rec.procSeqNo);
		utrnIO.setJobnoPrice(0);
		utrnIO.setFundRate(0);
		utrnIO.setStrpdate(0);
		utrnIO.setUstmno(0);
		utrnIO.setNofUnits(0);
		utrnIO.setNofDunits(0);
		utrnIO.setPriceDateUsed(0);
		utrnIO.setPriceUsed(0);
		utrnIO.setUnitBarePrice(0);
		utrnIO.setInciPerd01(0);
		utrnIO.setInciPerd02(0);
		utrnIO.setInciprm01(0);
		utrnIO.setInciprm02(0);
		utrnIO.setFundAmount(0);
		utrnIO.setSurrenderPercent(0);
		utrnIO.setInciNum(0);
		utrnIO.setFunction(varcom.writr);
		utrnIO.setFormat(utrnrec);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(utrnIO.getStatuz());
			syserrrec.params.set(utrnIO.getParams());
			dbError8100();
		}
	}

protected void writeHitr7000()
	{
		para7010();
	}

	/**
	* <pre>
	*************************                                 <INTBR> 
	* </pre>
	*/
protected void para7010()
	{
		hitrIO.setChdrcoy(rnlallrec.company);
		hitrIO.setChdrnum(rnlallrec.chdrnum);
		hitrIO.setCoverage(rnlallrec.coverage);
		hitrIO.setLife(rnlallrec.life);
		hitrIO.setRider(rnlallrec.rider);
		hitrIO.setPlanSuffix(rnlallrec.planSuffix);
		hitrIO.setTranno(rnlallrec.tranno);
		hitrIO.setBatccoy(rnlallrec.batccoy);
		hitrIO.setBatcbrn(rnlallrec.batcbrn);
		hitrIO.setBatcactyr(rnlallrec.batcactyr);
		hitrIO.setBatcactmn(rnlallrec.batcactmn);
		hitrIO.setBatctrcde(rnlallrec.batctrcde);
		hitrIO.setBatcbatch(rnlallrec.batcbatch);
		hitrIO.setCrtable(rnlallrec.crtable);
		hitrIO.setCntcurr(rnlallrec.cntcurr);
		if (isEQ(hitrIO.getZintbfnd(), SPACES)) {
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				hitrIO.setSacscode(t5645rec.sacscode08);
				hitrIO.setSacstyp(t5645rec.sacstype08);
				hitrIO.setGenlcde(t5645rec.glmap08);
			}
			else {
				hitrIO.setSacscode(t5645rec.sacscode06);
				hitrIO.setSacstyp(t5645rec.sacstype06);
				hitrIO.setGenlcde(t5645rec.glmap06);
			}
		}
		else {
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				hitrIO.setSacscode(t5645rec.sacscode07);
				hitrIO.setSacstyp(t5645rec.sacstype07);
				hitrIO.setGenlcde(t5645rec.glmap07);
			}
			else {
				hitrIO.setSacscode(t5645rec.sacscode05);
				hitrIO.setSacstyp(t5645rec.sacstype05);
				hitrIO.setGenlcde(t5645rec.glmap05);
			}
		}
		hitrIO.setCnttyp(rnlallrec.cnttype);
		hitrIO.setSvp(1);
		hitrIO.setFundCurrency(t5515rec.currcode);
		hitrIO.setProcSeqNo(t6647rec.procSeqNo);
		hitrIO.setFundAmount(ZERO);
		hitrIO.setSurrenderPercent(ZERO);
		hitrIO.setUstmno(ZERO);
		hitrIO.setInciprm01(ZERO);
		hitrIO.setInciprm02(ZERO);
		hitrIO.setFundRate(ZERO);
		hitrIO.setInciNum(ZERO);
		hitrIO.setInciPerd01(ZERO);
		hitrIO.setInciPerd02(ZERO);
		hitrIO.setZintrate(ZERO);
		if (isEQ(hitrIO.getZintbfnd(), SPACES)) {
			hitrIO.setZrectyp("N");
		}
		else {
			hitrIO.setZrectyp("P");
		}
		hitrIO.setEffdate(rnlallrec.effdate);
		hitrIO.setZintappind(SPACES);
		hitrIO.setZlstintdte(varcom.vrcmMaxDate);
		hitrIO.setZinteffdt(varcom.vrcmMaxDate);
		hitrIO.setFunction(varcom.writr);
		hitrIO.setFormat(hitrrec);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrIO.getParams());
			dbError8100();
		}
	}

protected void readT55155550()
	{
		start5551();
	}

protected void start5551()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rnlallrec.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItmfrm(rnlallrec.effdate);
		/* MOVE UTRN-UNIT-VIRTUAL-FUND TO ITDM-ITEMITEM.        <INTBR> */
		itdmIO.setItemitem(ulnkrnlIO.getUalfnd(wsaaIndex));
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.endp))
		&& (isNE(itdmIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		if ((isNE(itdmIO.getItemcoy(), rnlallrec.company))
		|| (isNE(itdmIO.getItemtabl(), t5515))
		|| (isNE(itdmIO.getItemitem(), ulnkrnlIO.getUalfnd(wsaaIndex)))) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
	}

protected void fundSplitAllocation5500()
	{
		start5510();
	}

protected void start5510()
	{
		readT55155550();
		if (isEQ(t5515rec.zfundtyp, "D")) {
			compute(wsaaAmountHoldersInner.wsaaAllocTot, 0).set(add(wsaaAmountHoldersInner.wsaaAllocInit, wsaaAmountHoldersInner.wsaaAllocAccum));
			if (isNE(wsaaAmountHoldersInner.wsaaAllocTot, 0)) {
				splitInterestBearing5570();
			}
			return ;
		}
		wsaaAmountHoldersInner.wsaaTotUlSplit.add(ulnkrnlIO.getUalprc(wsaaIndex));
		/* Invest the initial units premium in the funds specified in*/
		/* the ULNKRNL record. Write a UTRN record for each fund.*/
		if (isNE(wsaaAmountHoldersInner.wsaaAllocInit, 0)) {
			splitInitial5550();
		}
		/* Invest the accumulation units premium in the funds specified in*/
		/* the ULNKRNL record. Write a UTRN record for each fund.*/
		if (isNE(wsaaAmountHoldersInner.wsaaAllocAccum, 0)) {
			splitAccum5560();
		}
	}

protected void splitInitial5550()
	{
		start15551();
	}

protected void start15551()
	{
		utrnIO.setParams(SPACES);
		/* To avoid rounding error, use the remainder(total investment -*/
		/* invested so far) to invest on the last fund.*/
		if (isEQ(wsaaIndex, 10)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocInit, wsaaAmountHoldersInner.wsaaRunInitTot));
		}
		else {
			if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaIndex, 1)), SPACES)) {
				setPrecision(utrnIO.getContractAmount(), 0);
				utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocInit, wsaaAmountHoldersInner.wsaaRunInitTot));
			}
			else {
				setPrecision(utrnIO.getContractAmount(), 1);
				utrnIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocInit, ulnkrnlIO.getUalprc(wsaaIndex)), 100), true);
			}
		}
		wsaaAmountHoldersInner.wsaaRunInitTot.add(utrnIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaRunTot.add(utrnIO.getContractAmount());
		utrnIO.setUnitVirtualFund(ulnkrnlIO.getUalfnd(wsaaIndex));
		utrnIO.setUnitType("I");
		utrnIO.setUnitSubAccount("INIT");
		utrnIO.setDiscountFactor(wsaaAmountHoldersInner.wsaaInitUnitDiscFact);
		writeUtrn6000();
	}

protected void splitAccum5560()
	{
		start5561();
	}

protected void start5561()
	{
		utrnIO.setParams(SPACES);
		/* To avoid rounding error, use the remainder(total investment -*/
		/* invested so far) to invest on the last fund.*/
		if (isEQ(wsaaIndex, 10)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocAccum, wsaaAmountHoldersInner.wsaaRunAccumTot));
		}
		else {
			if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaIndex, 1)), SPACES)) {
				setPrecision(utrnIO.getContractAmount(), 0);
				utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocAccum, wsaaAmountHoldersInner.wsaaRunAccumTot));
			}
			else {
				setPrecision(utrnIO.getContractAmount(), 1);
				utrnIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocAccum, ulnkrnlIO.getUalprc(wsaaIndex)), 100), true);
			}
		}
		wsaaAmountHoldersInner.wsaaRunAccumTot.add(utrnIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaRunTot.add(utrnIO.getContractAmount());
		utrnIO.setUnitVirtualFund(ulnkrnlIO.getUalfnd(wsaaIndex));
		utrnIO.setUnitType("A");
		utrnIO.setUnitSubAccount("ACUM");
		utrnIO.setDiscountFactor(0);
		writeUtrn6000();
	}

protected void splitInterestBearing5570()
	{
		start5571();
	}

	/**
	* <pre>
	*************************************                     <INTBR> 
	* </pre>
	*/
protected void start5571()
	{
		hitrIO.setParams(SPACES);
		/* To avoid rounding error, use the remainder(total investment -   */
		/* invested so far) to invest on the last fund.            <INTBR> */
		if (isEQ(wsaaIndex, 10)) {
			setPrecision(hitrIO.getContractAmount(), 0);
			hitrIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocTot, wsaaAmountHoldersInner.wsaaRunTot));
		}
		else {
			if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaIndex, 1)), SPACES)) {
				setPrecision(hitrIO.getContractAmount(), 0);
				hitrIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocTot, wsaaAmountHoldersInner.wsaaRunTot));
			}
			else {
				setPrecision(hitrIO.getContractAmount(), 1);
				hitrIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocTot, ulnkrnlIO.getUalprc(wsaaIndex)), 100), true);
				compute(wsaaAmountHoldersInner.wsaaInitAmount, 1).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocInit, ulnkrnlIO.getUalprc(wsaaIndex)), 100));
				compute(wsaaAmountHoldersInner.wsaaAccumAmount, 1).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocAccum, ulnkrnlIO.getUalprc(wsaaIndex)), 100));
			}
		}
		zrdecplrec.amountIn.set(hitrIO.getContractAmount());
		a000CallRounding();
		hitrIO.setContractAmount(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaInitAmount);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaInitAmount.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAccumAmount);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaAccumAmount.set(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaTotIbSplit.add(ulnkrnlIO.getUalprc(wsaaIndex));
		wsaaAmountHoldersInner.wsaaRunTot.add(hitrIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaRunInitTot.add(wsaaAmountHoldersInner.wsaaInitAmount);
		wsaaAmountHoldersInner.wsaaRunAccumTot.add(wsaaAmountHoldersInner.wsaaAccumAmount);
		hitrIO.setZintbfnd(ulnkrnlIO.getUalfnd(wsaaIndex));
		writeHitr7000();
	}

protected void systemError8000()
	{
		start8010();
		exit8999();
	}

protected void start8010()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit8999()
	{
		rnlallrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
		start8110();
		exit8199();
	}

protected void start8110()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit8199()
	{
		rnlallrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void a000CallRounding()
	{
		/*A010-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(rnlallrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(rnlallrec.cntcurr);
		zrdecplrec.batctrcde.set(rnlallrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError8000();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-AMOUNT-HOLDERS--INNER
 */
private static final class WsaaAmountHoldersInner { 
		/* WSAA-AMOUNT-HOLDERS */
	private ZonedDecimalData wsaaNonInvestPrem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaAllocInit = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaRunInitTot = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaAllocAccum = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaRunAccumTot = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaInitUnitDiscFact = new ZonedDecimalData(10, 5).init(0);
	private ZonedDecimalData wsaaOldAllocAccum = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaOldAllocInit = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaEnhanceAlloc = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaTotUlSplit = new ZonedDecimalData(5, 2).init(0);
	private ZonedDecimalData wsaaTotIbSplit = new ZonedDecimalData(5, 2).init(0);
	private ZonedDecimalData wsaaAllocTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaRunTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaInitAmount = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaAccumAmount = new ZonedDecimalData(18, 2).init(0);
}
}
