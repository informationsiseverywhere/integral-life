package com.csc.life.flexiblepremium.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:41
 * Description:
 * Copybook name: FPCOANVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Fpcoanvkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData fpcoanvFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData fpcoanvKey = new FixedLengthStringData(256).isAPartOf(fpcoanvFileKey, 0, REDEFINE);
  	public FixedLengthStringData fpcoanvChdrcoy = new FixedLengthStringData(1).isAPartOf(fpcoanvKey, 0);
  	public FixedLengthStringData fpcoanvChdrnum = new FixedLengthStringData(8).isAPartOf(fpcoanvKey, 1);
  	public PackedDecimalData fpcoanvAnnivProcDate = new PackedDecimalData(8, 0).isAPartOf(fpcoanvKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(242).isAPartOf(fpcoanvKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(fpcoanvFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		fpcoanvFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}