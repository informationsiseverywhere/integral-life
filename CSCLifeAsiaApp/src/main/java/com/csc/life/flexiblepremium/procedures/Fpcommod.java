/*
 * File: Fpcommod.java
 * Date: 29 August 2009 22:48:26
 * Author: Quipoz Limited
 *
 * Class transformed from FPCOMMOD.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5564rec;
import com.csc.life.flexiblepremium.dataaccess.Fpcolf1TableDAM;
import com.csc.life.flexiblepremium.tablestructures.T5728rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*           COMMISSION MODIFY  FOR FLEXIBLE PREMIUM
* A) Commision Payment.
* Obtain the  commission  payment  rules  table  item  key from
* T5564, keyed  by  Payment  pattern,  Coverage/rider  code and
* effective date (hence use ITDM).
*
* We read all the FPCO records for the previous target periods
* excluding the one for the currto passed and add the AGCM
* Ann prem for each one found.
*
* At the end of the FPCO's, if the applicable amount is = zero,
* no commission is to be calculated for this AGCM.
*
* If the amount applicable is > zero, commission must be
* calculated based on the % recd.
*
* The % of the annual premium recd is calculated as :
*
* Annual premium on AGCM
* --------------------------------
* derived recd amount
*
* Once we have the %, we read T5728 to determine the % of the
* ICOMMTOT that has been earned.
*
* Read T5728 using the key from T5564
* -------------------------------------
*
* The 'earnings' are calculated using the first entry on
* T5728 where the % is >
* or = the % recd. No previous band is read.
*
* Once the commission earned has been calculated,
* It is returned to the calling program.
*
*****************************************************************
*****************************************************************
* DATE.... VSN/MOD  WORK UNIT    BY....
*
* 02/02/96  01/01   D9604        Bev Chapman                          *
*           New commission subroutine to be used to calculate
*           values for dormant AGCMs on a premium decrease.
*                                                                     *
**DD/MM/YY*************************************************************
/
* </pre>
*/
public class Fpcommod extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "FPCOMMOD";

	private FixedLengthStringData wsaaT5564Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaMethod = new FixedLengthStringData(4).isAPartOf(wsaaT5564Key, 0).init(SPACES);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5564Key, 4).init(SPACES);
	private FixedLengthStringData wsaaT5728Key = new FixedLengthStringData(8).init(SPACES);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaTotPercRec = new PackedDecimalData(10, 7);
	private PackedDecimalData wsaaProRataRate = new PackedDecimalData(9, 7);
	private PackedDecimalData wsaaProRataPercent = new PackedDecimalData(10, 7);
	private ZonedDecimalData wsaaComEarnedBand = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaPremRecd = new PackedDecimalData(16, 7);
		/* ERRORS */
	private String h027 = "H027";
	private String d039 = "D039";
	private String fpcolf1rec = "FPCOLF1REC";
		/* TABLES */
	private String t5564 = "T5564";
	private String t5728 = "T5728";
	private Comlinkrec comlinkrec = new Comlinkrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
		/*Flexible Premium Coverage*/
	private Fpcolf1TableDAM fpcolf1IO = new Fpcolf1TableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T5564rec t5564rec = new T5564rec();
	private T5728rec t5728rec = new T5728rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit2390,
		exit3090,
		exit3390,
		seExit8090,
		dbExit8190
	}

	public Fpcommod() {
		super();
	}

public void mainline(Object... parmArray)
	{
		comlinkrec.clnkallRec = convertAndSetParam(comlinkrec.clnkallRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		comlinkrec.statuz.set(varcom.oK);
		obtainTables1000();
		calcCommamntPayable3000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void obtainTables1000()
	{
		para1000();
		para2000();
	}

protected void para1000()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(comlinkrec.chdrcoy);
		itdmIO.setItemtabl(t5564);
		wsaaMethod.set(comlinkrec.method);
		wsaaCrtable.set(comlinkrec.crtable);
		itdmIO.setItemitem(wsaaT5564Key);
		itdmIO.setItmfrm(comlinkrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5564)
		|| isNE(itdmIO.getItemitem(),wsaaT5564Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(wsaaT5564Key);
			syserrrec.statuz.set(h027);
			dbError8100();
		}
		t5564rec.t5564Rec.set(itdmIO.getGenarea());
		wsaaT5728Key.set(SPACES);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
			if (isEQ(t5564rec.freq[wsaaIndex.toInt()],comlinkrec.billfreq)) {
				wsaaT5728Key.set(t5564rec.itmkey[wsaaIndex.toInt()]);
				wsaaIndex.set(13);
			}
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(comlinkrec.chdrcoy);
		itdmIO.setItemtabl(t5728);
		itdmIO.setItemitem(wsaaT5728Key);
		itdmIO.setItmfrm(comlinkrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5728)
		|| isNE(itdmIO.getItemitem(),wsaaT5728Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(wsaaT5728Key);
			syserrrec.statuz.set(d039);
			dbError8100();
		}
		else {
			t5728rec.t5728Rec.set(itdmIO.getGenarea());
		}
	}

protected void para2000()
	{
		wsaaPremRecd.set(ZERO);
		datcon2rec.frequency.set("01");
		datcon2rec.intDate1.set(comlinkrec.currto);
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			dbError8100();
		}
		calcFpcoTotal2300();
		/*EXIT*/
	}

protected void calcFpcoTotal2300()
	{
		try {
			start2301();
		}
		catch (GOTOException e){
		}
	}

protected void start2301()
	{
		fpcolf1IO.setChdrcoy(comlinkrec.chdrcoy);
		fpcolf1IO.setChdrnum(comlinkrec.chdrnum);
		fpcolf1IO.setLife(comlinkrec.life);
		fpcolf1IO.setCoverage(comlinkrec.coverage);
		fpcolf1IO.setRider(comlinkrec.rider);
		fpcolf1IO.setPlanSuffix(comlinkrec.planSuffix);
		fpcolf1IO.setTargfrom(comlinkrec.effdate);
		fpcolf1IO.setActiveInd(SPACES);
		fpcolf1IO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fpcolf1IO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fpcolf1IO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		fpcolf1IO.setFormat(fpcolf1rec);
		fpcolf1IO.setStatuz(varcom.oK);
		while ( !(isEQ(fpcolf1IO.getStatuz(),varcom.endp))) {
			SmartFileCode.execute(appVars, fpcolf1IO);
			if (isNE(fpcolf1IO.getStatuz(),varcom.oK)
			&& isNE(fpcolf1IO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(fpcolf1IO.getParams());
				dbError8100();
			}
			if (isNE(fpcolf1IO.getChdrcoy(),comlinkrec.chdrcoy)
			|| isNE(fpcolf1IO.getChdrnum(),comlinkrec.chdrnum)
			|| isNE(fpcolf1IO.getLife(),comlinkrec.life)
			|| isNE(fpcolf1IO.getCoverage(),comlinkrec.coverage)
			|| isNE(fpcolf1IO.getRider(),comlinkrec.rider)
			|| isNE(fpcolf1IO.getPlanSuffix(),comlinkrec.planSuffix)
			|| isGTE(fpcolf1IO.getTargfrom(),datcon2rec.intDate2)
			|| isEQ(fpcolf1IO.getStatuz(),varcom.endp)) {
				fpcolf1IO.setStatuz(varcom.endp);
				goTo(GotoLabel.exit2390);
			}
			compute(wsaaPremRecd, 7).set(add(wsaaPremRecd,comlinkrec.annprem));
			fpcolf1IO.setFunction(varcom.nextr);
		}

	}

protected void calcCommamntPayable3000()
	{
		try {
			para3000();
		}
		catch (GOTOException e){
		}
	}

protected void para3000()
	{
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		if (isEQ(wsaaPremRecd,ZERO)) {
			goTo(GotoLabel.exit3090);
		}
		earnedCommission3300();
	}

protected void earnedCommission3300()
	{
		try {
			start3300();
		}
		catch (GOTOException e){
		}
	}

protected void start3300()
	{
		compute(wsaaTotPercRec, 8).setRounded(mult(div(wsaaPremRecd,comlinkrec.annprem),100));
		for (ix.set(1); !(isGT(ix,14)); ix.add(1)){
			if (isGTE(t5728rec.tgtRcvdEarn[ix.toInt()],wsaaTotPercRec)) {
				wsaaComEarnedBand.set(ix);
				ix.set(15);
			}
			else {
				if (isEQ(t5728rec.tgtRcvdEarn[ix.toInt()],ZERO)) {
					wsaaComEarnedBand.set(99);
					ix.set(15);
				}
			}
		}
		if (isEQ(wsaaComEarnedBand,99)) {
			compute(comlinkrec.erndamt, 2).set(sub(comlinkrec.icommtot,comlinkrec.icommernd));
			goTo(GotoLabel.exit3390);
		}
		if (isEQ(t5728rec.commEarn[wsaaComEarnedBand.toInt()],0)) {
			comlinkrec.erndamt.set(0);
			goTo(GotoLabel.exit3390);
		}
		compute(wsaaProRataRate, 7).set(div(t5728rec.tgtRcvdEarn[wsaaComEarnedBand.toInt()],t5728rec.commEarn[wsaaComEarnedBand.toInt()]));
		compute(wsaaProRataPercent, 7).set(div(wsaaTotPercRec,wsaaProRataRate));
		compute(comlinkrec.erndamt, 8).setRounded(sub((mult(div(wsaaProRataPercent,100),comlinkrec.icommtot)),comlinkrec.icommernd));
	}

protected void systemError8000()
	{
		try {
			se8000();
		}
		catch (GOTOException e){
		}
		finally{
			seExit8090();
		}
	}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit8090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		comlinkrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
		try {
			db8100();
		}
		catch (GOTOException e){
		}
		finally{
			dbExit8190();
		}
	}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit8190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		comlinkrec.statuz.set(varcom.bomb);
		exit090();
	}
}
