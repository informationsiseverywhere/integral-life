package com.csc.life.flexiblepremium.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:43
 * Description:
 * Copybook name: FREPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Frepkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData frepFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData frepKey = new FixedLengthStringData(64).isAPartOf(frepFileKey, 0, REDEFINE);
  	public FixedLengthStringData frepCompany = new FixedLengthStringData(1).isAPartOf(frepKey, 0);
  	public FixedLengthStringData frepChdrnum = new FixedLengthStringData(8).isAPartOf(frepKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(frepKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(frepFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		frepFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}