package com.csc.life.flexiblepremium.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: FpcxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:54
 * Class transformed from FPCXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class FpcxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 86;
	public FixedLengthStringData fpcxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData fpcxpfRecord = fpcxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(fpcxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(fpcxrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(fpcxrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(fpcxrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(fpcxrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(fpcxrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(fpcxrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(fpcxrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(fpcxrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(fpcxrec);
	public PackedDecimalData targetPremium = DD.prmper.copy().isAPartOf(fpcxrec);
	public PackedDecimalData premRecPer = DD.prmrcdp.copy().isAPartOf(fpcxrec);
	public FixedLengthStringData activeInd = DD.actind.copy().isAPartOf(fpcxrec);
	public PackedDecimalData billedInPeriod = DD.billedp.copy().isAPartOf(fpcxrec);
	public PackedDecimalData overdueMin = DD.ovrminreq.copy().isAPartOf(fpcxrec);
	public PackedDecimalData minOverduePer = DD.minovrpro.copy().isAPartOf(fpcxrec);
	public FixedLengthStringData annProcessInd = DD.anproind.copy().isAPartOf(fpcxrec);
	public PackedDecimalData targfrom = DD.targfrom.copy().isAPartOf(fpcxrec);
	public PackedDecimalData targto = DD.targto.copy().isAPartOf(fpcxrec);
	public PackedDecimalData annivProcDate = DD.cbanpr.copy().isAPartOf(fpcxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public FpcxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for FpcxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public FpcxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for FpcxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public FpcxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for FpcxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public FpcxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("FPCXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"VALIDFLAG, " +
							"CURRFROM, " +
							"CURRTO, " +
							"PRMPER, " +
							"PRMRCDP, " +
							"ACTIND, " +
							"BILLEDP, " +
							"OVRMINREQ, " +
							"MINOVRPRO, " +
							"ANPROIND, " +
							"TARGFROM, " +
							"TARGTO, " +
							"CBANPR, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     validflag,
                                     currfrom,
                                     currto,
                                     targetPremium,
                                     premRecPer,
                                     activeInd,
                                     billedInPeriod,
                                     overdueMin,
                                     minOverduePer,
                                     annProcessInd,
                                     targfrom,
                                     targto,
                                     annivProcDate,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		validflag.clear();
  		currfrom.clear();
  		currto.clear();
  		targetPremium.clear();
  		premRecPer.clear();
  		activeInd.clear();
  		billedInPeriod.clear();
  		overdueMin.clear();
  		minOverduePer.clear();
  		annProcessInd.clear();
  		targfrom.clear();
  		targto.clear();
  		annivProcDate.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getFpcxrec() {
  		return fpcxrec;
	}

	public FixedLengthStringData getFpcxpfRecord() {
  		return fpcxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setFpcxrec(what);
	}

	public void setFpcxrec(Object what) {
  		this.fpcxrec.set(what);
	}

	public void setFpcxpfRecord(Object what) {
  		this.fpcxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(fpcxrec.getLength());
		result.set(fpcxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}