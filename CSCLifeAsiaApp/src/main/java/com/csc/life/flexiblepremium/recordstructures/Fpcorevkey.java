package com.csc.life.flexiblepremium.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:42
 * Description:
 * Copybook name: FPCOREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Fpcorevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData fpcorevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData fpcorevKey = new FixedLengthStringData(64).isAPartOf(fpcorevFileKey, 0, REDEFINE);
  	public FixedLengthStringData fpcorevChdrcoy = new FixedLengthStringData(1).isAPartOf(fpcorevKey, 0);
  	public FixedLengthStringData fpcorevChdrnum = new FixedLengthStringData(8).isAPartOf(fpcorevKey, 1);
  	public FixedLengthStringData fpcorevLife = new FixedLengthStringData(2).isAPartOf(fpcorevKey, 9);
  	public FixedLengthStringData fpcorevCoverage = new FixedLengthStringData(2).isAPartOf(fpcorevKey, 11);
  	public FixedLengthStringData fpcorevRider = new FixedLengthStringData(2).isAPartOf(fpcorevKey, 13);
  	public PackedDecimalData fpcorevPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(fpcorevKey, 15);
  	public PackedDecimalData fpcorevTargfrom = new PackedDecimalData(8, 0).isAPartOf(fpcorevKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(41).isAPartOf(fpcorevKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(fpcorevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		fpcorevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}