/*
 * File: Flexout.java
 * Date: 29 August 2009 22:48:19
 * Author: Quipoz Limited
 * 
 * Class transformed from FLEXOUT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.recordstructures.Genoutrec;
import com.csc.life.flexiblepremium.dataaccess.FpcoTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FpcobrkTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  GENERIC SUB-ROUTINE  (BREAKOUT)
*
*
* This Unit Linked generic subroutine is called by the Breakout
* subroutine (which in turn  was  called by an "AT" module), in
* order to breakout the  summarised  FPCO records.
*
* BREAKOUT processing occurs when the policy selected from the
* calling program belongs to the summerised policy record which
* is denoted by PLAN-SUFFIX '00'. For whatever reason this policy
* must become a record in its own right for further processing.
*
* PROCESSING
* -----------
* Get the summerised record (plan suffix 00).
* Store the values of the summerised record in an array
* (WSAA-AMOUNT-IN).
* Write a new record until the last policy that requires to be
* broken out is reached. The values on the new record are
* calculated and stored in an array (WSAA-AMOUNT-OUT) as follows -
*
*    COMPUTE WSAA-AMOUNT-OUT (WSAA-SUB) ROUNDED =
*                 WSAA-AMOUNT-IN (WSAA-SUB) / BRK-OLD-SUMMARY.
*
* These new values are totalled in an array;
*
*    COMPUTE WSAD-AMOUNT-TOT (WSAA-SUB) =
*                 WSAD-AMOUNT-TOT (WSAA-SUB) +
*                 WSAA-AMOUNT-OUT (WSAA-SUB).
*
* When the last policy that requires to be broken out is reached
* calculate the values on the record as the following example
*
*    COMPUTE FPCOBRK-VALUE =
*                 WSAA-AMOUNT-IN (4) - WSAD-AMOUNT-TOT (4).
*
* These values are either written as a new record or rewritten on
* an old record depending on the following;
*
*  1.The policy to be broken out is the last policy of the plan.
*    In this case it is necessary to delete the summerised record
*    (plan-suffix 00) and write a new recordusing the calculation
*    above, with a plan suffix of 1.
*
*  2.The policy to be broken out is not the last policy on the
*    plan so calculate the new values as above and rewrite the
*    summerised record (plan-suffix 00).
*
****************************************************************** ****
/
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Flexout extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "FLEXOUT";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private static final String fpcobrkrec = "FPCOBRKREC";
	private static final String fpcorec = "FPCOREC   ";

		/* WSAA-AMOUNT-TABLE */
	private FixedLengthStringData[] wsaaAmountsIn = FLSInittedArray (20, 13);
	private ZonedDecimalData[] wsaaAmountIn = ZDArrayPartOfArrayStructure(13, 2, wsaaAmountsIn, 0);

	private FixedLengthStringData[] wsaaAmountsOut = FLSInittedArray (20, 13);
	private ZonedDecimalData[] wsaaAmountOut = ZDArrayPartOfArrayStructure(13, 2, wsaaAmountsOut, 0);
		/* WSAD-AMOUNT-TOT-TABLE */
	private ZonedDecimalData[] wsadAmountTot = ZDInittedArray(20, 13, 2);
	private ZonedDecimalData wsaaFpcoNoRecs = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaSub = new PackedDecimalData(4, 0).init(ZERO);
	private FpcoTableDAM fpcoIO = new FpcoTableDAM();
	private FpcobrkTableDAM fpcobrkIO = new FpcobrkTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Genoutrec genoutrec = new Genoutrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr202, 
		exit209
	}

	public Flexout() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		genoutrec.outRec = convertAndSetParam(genoutrec.outRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		/*PARA*/
		initialize100();
		/*  process the FPCO file*/
		while ( !(isEQ(fpcobrkIO.getStatuz(), varcom.endp))) {
			mainProcessingFpcobrk200();
		}
		
		/*EXIT*/
		exitProgram();
	}

protected void initialize100()
	{
		para101();
	}

protected void para101()
	{
		genoutrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaFpcoNoRecs.set(ZERO);
		/*   FIRST read of the FPCOBRK file*/
		fpcobrkIO.setDataArea(SPACES);
		fpcobrkIO.setChdrcoy(genoutrec.chdrcoy);
		fpcobrkIO.setChdrnum(genoutrec.chdrnum);
		fpcobrkIO.setLife(ZERO);
		fpcobrkIO.setCoverage(ZERO);
		fpcobrkIO.setRider(ZERO);
		fpcobrkIO.setPlanSuffix(ZERO);
		fpcobrkIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, fpcobrkIO);
		if (isNE(fpcobrkIO.getStatuz(), varcom.oK)
		&& isNE(fpcobrkIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(fpcobrkIO.getParams());
			fatalError9000();
		}
		if (isNE(fpcobrkIO.getChdrnum(), genoutrec.chdrnum)
		|| isNE(fpcobrkIO.getChdrcoy(), genoutrec.chdrcoy)
		|| isNE(fpcobrkIO.getPlanSuffix(), ZERO)
		|| isEQ(fpcobrkIO.getStatuz(), varcom.endp)) {
			wsaaFpcoNoRecs.set(1);
		}
		/*   initialize the  working storage table used in the*/
		/*   breakout calculation*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 20)); wsaaSub.add(1)){
			wsaaAmountIn[wsaaSub.toInt()].set(ZERO);
			wsaaAmountOut[wsaaSub.toInt()].set(ZERO);
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub, 20)); wsaaSub.add(1)){
			wsadAmountTot[wsaaSub.toInt()].set(ZERO);
		}
	}

protected void mainProcessingFpcobrk200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para201();
				case nextr202: 
					nextr202();
				case exit209: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para201()
	{
		if (isEQ(wsaaFpcoNoRecs, 1)) {
			fpcobrkIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit209);
		}
		/*  move all the numeric fields from the FPCO summary record in*/
		/*  order to loop through them and unsummarize each one*/
		moveFpcoValsToTable210();
		for (wsaaSub.set(1); !(isGT(wsaaSub, 20)); wsaaSub.add(1)){
			wsadAmountTot[wsaaSub.toInt()].set(ZERO);
		}
		for (wsaaPlanSuffix.set(genoutrec.oldSummary); !(isEQ(wsaaPlanSuffix, genoutrec.newSummary)); wsaaPlanSuffix.add(-1)){
			fpcoCalcs220();
			moveFpcoValsFromTable230();
			writeBrkFpco240();
		}
		/* calculate the new summary record - passed to us in linkage*/
		calcNewSummaryRecFpco250();
	}

	/**
	* <pre>
	*  read the next FPCO record and process as above until EOF
	* </pre>
	*/
protected void nextr202()
	{
		fpcobrkIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, fpcobrkIO);
		if (isNE(fpcobrkIO.getStatuz(), varcom.oK)
		&& isNE(fpcobrkIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(fpcobrkIO.getParams());
			fatalError9000();
		}
		if (isEQ(fpcobrkIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(fpcobrkIO.getChdrnum(), genoutrec.chdrnum)
		|| isNE(fpcobrkIO.getChdrcoy(), genoutrec.chdrcoy)) {
			fpcobrkIO.setFunction(varcom.rewrt);
			fpcobrkIO.setFormat(fpcobrkrec);
			SmartFileCode.execute(appVars, fpcobrkIO);
			if (isNE(fpcobrkIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(fpcobrkIO.getParams());
				fatalError9000();
			}
			else {
				fpcobrkIO.setStatuz(varcom.endp);
				return ;
			}
		}
		/*   In the absence of a proper release function rewrite the*/
		/*   record at end of file.  This will enable the calling program*/
		/*   to update the record if required*/
		if (isNE(fpcobrkIO.getChdrnum(), genoutrec.chdrnum)
		|| isNE(fpcobrkIO.getChdrcoy(), genoutrec.chdrcoy)
		|| isNE(fpcobrkIO.getPlanSuffix(), ZERO)) {
			fpcobrkIO.setFunction(varcom.rewrt);
			fpcobrkIO.setFormat(fpcobrkrec);
			SmartFileCode.execute(appVars, fpcobrkIO);
			if (isNE(fpcobrkIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(fpcobrkIO.getParams());
				fatalError9000();
			}
			else {
				goTo(GotoLabel.nextr202);
			}
		}
	}

protected void moveFpcoValsToTable210()
	{
		/*READ*/
		wsaaAmountIn[1].set(fpcobrkIO.getTargetPremium());
		wsaaAmountIn[2].set(fpcobrkIO.getPremRecPer());
		wsaaAmountIn[3].set(fpcobrkIO.getBilledInPeriod());
		wsaaAmountIn[4].set(fpcobrkIO.getOverdueMin());
		/*EXIT*/
	}

protected void fpcoCalcs220()
	{
		/*READ*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 10)); wsaaSub.add(1)){
			calcRemainBreakout7000();
		}
		/*EXIT*/
	}

protected void moveFpcoValsFromTable230()
	{
		/*GO*/
		fpcobrkIO.setTargetPremium(wsaaAmountOut[1]);
		fpcobrkIO.setPremRecPer(wsaaAmountOut[2]);
		fpcobrkIO.setBilledInPeriod(wsaaAmountOut[3]);
		fpcobrkIO.setOverdueMin(wsaaAmountOut[4]);
		/*EXIT*/
	}

protected void writeBrkFpco240()
	{
		go241();
	}

protected void go241()
	{
		/* Move the FPCOBRK values to the FPCO logical view in order*/
		/* to write the broken out FPCO records.*/
		fpcoIO.setChdrcoy(fpcobrkIO.getChdrcoy());
		fpcoIO.setChdrnum(fpcobrkIO.getChdrnum());
		fpcoIO.setLife(fpcobrkIO.getLife());
		fpcoIO.setCoverage(fpcobrkIO.getCoverage());
		fpcoIO.setRider(fpcobrkIO.getRider());
		fpcoIO.setPlanSuffix(wsaaPlanSuffix);
		fpcoIO.setTargfrom(fpcobrkIO.getTargfrom());
		fpcoIO.setNonKey(SPACES);
		fpcoIO.setJlife(fpcobrkIO.getJlife());
		fpcoIO.setValidflag(fpcobrkIO.getValidflag());
		fpcoIO.setTargto(fpcobrkIO.getTargto());
		fpcoIO.setCurrto(fpcobrkIO.getCurrto());
		fpcoIO.setCurrfrom(fpcobrkIO.getCurrfrom());
		fpcoIO.setEffdate(fpcobrkIO.getEffdate());
		fpcoIO.setTargetPremium(fpcobrkIO.getTargetPremium());
		fpcoIO.setPremRecPer(fpcobrkIO.getPremRecPer());
		fpcoIO.setBilledInPeriod(fpcobrkIO.getBilledInPeriod());
		fpcoIO.setOverdueMin(fpcobrkIO.getOverdueMin());
		fpcoIO.setMinOverduePer(fpcobrkIO.getMinOverduePer());
		fpcoIO.setAnnProcessInd(fpcobrkIO.getAnnProcessInd());
		fpcoIO.setActiveInd(fpcobrkIO.getActiveInd());
		fpcoIO.setTranno(fpcobrkIO.getTranno());
		fpcoIO.setAnnivProcDate(fpcobrkIO.getAnnivProcDate());
		fpcoIO.setFormat(fpcorec);
		fpcoIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcoIO.getParams());
			fatalError9000();
		}
	}

protected void calcNewSummaryRecFpco250()
	{
		go251();
	}

protected void go251()
	{
		/* if the old summary record has not been rewritten (i.e) there*/
		/* are now no summary records delete the old summary record.*/
		if (isLT(genoutrec.newSummary, 2)) {
			deleteSummaryFpco260();
		}
		/* calculate the new summary record by using the new number of*/
		/* summary records - passed in linkage*/
		setPrecision(fpcobrkIO.getTargetPremium(), 2);
		fpcobrkIO.setTargetPremium(sub(wsaaAmountIn[1], wsadAmountTot[1]));
		setPrecision(fpcobrkIO.getPremRecPer(), 2);
		fpcobrkIO.setPremRecPer(sub(wsaaAmountIn[2], wsadAmountTot[2]));
		setPrecision(fpcobrkIO.getBilledInPeriod(), 2);
		fpcobrkIO.setBilledInPeriod(sub(wsaaAmountIn[3], wsadAmountTot[3]));
		setPrecision(fpcobrkIO.getOverdueMin(), 2);
		fpcobrkIO.setOverdueMin(sub(wsaaAmountIn[4], wsadAmountTot[4]));
		if (isLT(genoutrec.newSummary, 2)) {
			wsaaPlanSuffix.set(1);
			writeBrkFpco240();
			return ;
		}
		/* rewrite the new summarised record*/
		fpcobrkIO.setFormat(fpcobrkrec);
		fpcobrkIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fpcobrkIO);
		if (isNE(fpcobrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcobrkIO.getParams());
			fatalError9000();
		}
	}

protected void deleteSummaryFpco260()
	{
		/*GO*/
		fpcobrkIO.setFunction(varcom.delet);
		fpcobrkIO.setFormat(fpcobrkrec);
		SmartFileCode.execute(appVars, fpcobrkIO);
		if (isNE(fpcobrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcobrkIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*       S E C T I O N S    C O M M O N  T O        *
	*    M O R E    T H A N   O N E   S E C T I O N    *
	* </pre>
	*/
protected void calcRemainBreakout7000()
	{
		/*GO*/
		if (isEQ(wsaaAmountIn[wsaaSub.toInt()], ZERO)) {
			wsaaAmountOut[wsaaSub.toInt()].set(ZERO);
			return ;
		}
		compute(wsaaAmountOut[wsaaSub.toInt()], 3).setRounded(div(wsaaAmountIn[wsaaSub.toInt()], genoutrec.oldSummary));
		zrdecplrec.amountIn.set(wsaaAmountOut[wsaaSub.toInt()]);
		callRounding8000();
		wsaaAmountOut[wsaaSub.toInt()].set(zrdecplrec.amountOut);
		compute(wsadAmountTot[wsaaSub.toInt()], 2).set(add(wsadAmountTot[wsaaSub.toInt()], wsaaAmountOut[wsaaSub.toInt()]));
		/*EXIT*/
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(genoutrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(genoutrec.cntcurr);
		zrdecplrec.batctrcde.set(genoutrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError9000();
		}
		/*EXIT*/
	}

protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz, "BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		genoutrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
