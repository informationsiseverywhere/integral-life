package com.csc.life.flexiblepremium.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:43
 * Description:
 * Copybook name: FPRMREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Fprmrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData fprmrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData fprmrevKey = new FixedLengthStringData(64).isAPartOf(fprmrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData fprmrevChdrcoy = new FixedLengthStringData(1).isAPartOf(fprmrevKey, 0);
  	public FixedLengthStringData fprmrevChdrnum = new FixedLengthStringData(8).isAPartOf(fprmrevKey, 1);
  	public PackedDecimalData fprmrevPayrseqno = new PackedDecimalData(1, 0).isAPartOf(fprmrevKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(54).isAPartOf(fprmrevKey, 10, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(fprmrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		fprmrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}