package com.csc.life.flexiblepremium.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:42
 * Description:
 * Copybook name: FPCORV1KEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Fpcorv1key extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData fpcorv1FileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData fpcorv1Key = new FixedLengthStringData(64).isAPartOf(fpcorv1FileKey, 0, REDEFINE);
  	public FixedLengthStringData fpcorv1Chdrcoy = new FixedLengthStringData(1).isAPartOf(fpcorv1Key, 0);
  	public FixedLengthStringData fpcorv1Chdrnum = new FixedLengthStringData(8).isAPartOf(fpcorv1Key, 1);
  	public PackedDecimalData fpcorv1Effdate = new PackedDecimalData(8, 0).isAPartOf(fpcorv1Key, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(fpcorv1Key, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(fpcorv1FileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		fpcorv1FileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}