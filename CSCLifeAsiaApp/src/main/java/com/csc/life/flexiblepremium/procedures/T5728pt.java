/*
 * File: T5728pt.java
 * Date: 30 August 2009 2:26:57
 * Author: Quipoz Limited
 * 
 * Class transformed from T5728PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.flexiblepremium.tablestructures.T5728rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5728.
*
*
*****************************************************************
* </pre>
*/
public class T5728pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(54).isAPartOf(wsaaPrtLine001, 22, FILLER).init("Premium Received Commission Release              S5728");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(49);
	private FixedLengthStringData filler9 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine004, 31, FILLER).init("Commission Payment");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(28);
	private FixedLengthStringData filler11 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine005, 0, FILLER).init(" As Earned Commission ?");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 27);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(39);
	private FixedLengthStringData filler12 = new FixedLengthStringData(39).isAPartOf(wsaaPrtLine006, 0, FILLER).init(" Indemnity Commission Payment Pattern :");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(48);
	private FixedLengthStringData filler13 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine007, 31, FILLER).init("% Target Received");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(79);
	private FixedLengthStringData filler15 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 11).setPattern("ZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 14, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 16).setPattern("ZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 21).setPattern("ZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 26).setPattern("ZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 31).setPattern("ZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 36).setPattern("ZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 41).setPattern("ZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 46).setPattern("ZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 51).setPattern("ZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 54, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 56).setPattern("ZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 61).setPattern("ZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 66).setPattern("ZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 71).setPattern("ZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 74, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 76).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(6);
	private FixedLengthStringData filler29 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 5, FILLER).init("%");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(79);
	private FixedLengthStringData filler31 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine010, 0, FILLER).init(" Released");
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 11).setPattern("ZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 14, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 16).setPattern("ZZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 21).setPattern("ZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 26).setPattern("ZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 31).setPattern("ZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 36).setPattern("ZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 41).setPattern("ZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 46).setPattern("ZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 51).setPattern("ZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 54, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 56).setPattern("ZZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 61).setPattern("ZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 66).setPattern("ZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 71).setPattern("ZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 74, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 76).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(48);
	private FixedLengthStringData filler45 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler46 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine011, 31, FILLER).init("Commission Earned");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(52);
	private FixedLengthStringData filler47 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler48 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine012, 27, FILLER).init("% Target Received (up to)");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(79);
	private FixedLengthStringData filler49 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 11).setPattern("ZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 14, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 16).setPattern("ZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 21).setPattern("ZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 26).setPattern("ZZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 31).setPattern("ZZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 36).setPattern("ZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 41).setPattern("ZZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 46).setPattern("ZZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 51).setPattern("ZZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 54, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 56).setPattern("ZZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 61).setPattern("ZZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 66).setPattern("ZZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 71).setPattern("ZZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 74, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 76).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(6);
	private FixedLengthStringData filler63 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler64 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 5, FILLER).init("%");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(79);
	private FixedLengthStringData filler65 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine015, 0, FILLER).init(" Earned");
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 11).setPattern("ZZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 14, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 16).setPattern("ZZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 21).setPattern("ZZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 26).setPattern("ZZZ");
	private FixedLengthStringData filler69 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 31).setPattern("ZZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 36).setPattern("ZZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 41).setPattern("ZZZ");
	private FixedLengthStringData filler72 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 46).setPattern("ZZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 51).setPattern("ZZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 54, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 56).setPattern("ZZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 61).setPattern("ZZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 66).setPattern("ZZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 71).setPattern("ZZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 74, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 76).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(28);
	private FixedLengthStringData filler79 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine016, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5728rec t5728rec = new T5728rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5728pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5728rec.t5728Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5728rec.commAsEarned);
		fieldNo008.set(t5728rec.tgtRcvdPay01);
		fieldNo009.set(t5728rec.tgtRcvdPay02);
		fieldNo010.set(t5728rec.tgtRcvdPay03);
		fieldNo011.set(t5728rec.tgtRcvdPay04);
		fieldNo012.set(t5728rec.tgtRcvdPay05);
		fieldNo013.set(t5728rec.tgtRcvdPay06);
		fieldNo014.set(t5728rec.tgtRcvdPay07);
		fieldNo015.set(t5728rec.tgtRcvdPay08);
		fieldNo016.set(t5728rec.tgtRcvdPay09);
		fieldNo017.set(t5728rec.tgtRcvdPay10);
		fieldNo018.set(t5728rec.tgtRcvdPay11);
		fieldNo019.set(t5728rec.tgtRcvdPay12);
		fieldNo020.set(t5728rec.tgtRcvdPay13);
		fieldNo021.set(t5728rec.tgtRcvdPay14);
		fieldNo022.set(t5728rec.commRel01);
		fieldNo023.set(t5728rec.commRel02);
		fieldNo024.set(t5728rec.commRel03);
		fieldNo025.set(t5728rec.commRel04);
		fieldNo026.set(t5728rec.commRel05);
		fieldNo027.set(t5728rec.commRel06);
		fieldNo028.set(t5728rec.commRel07);
		fieldNo029.set(t5728rec.commRel08);
		fieldNo030.set(t5728rec.commRel09);
		fieldNo031.set(t5728rec.commRel10);
		fieldNo032.set(t5728rec.commRel11);
		fieldNo033.set(t5728rec.commRel12);
		fieldNo034.set(t5728rec.commRel13);
		fieldNo035.set(t5728rec.commRel14);
		fieldNo036.set(t5728rec.tgtRcvdEarn01);
		fieldNo037.set(t5728rec.tgtRcvdEarn02);
		fieldNo038.set(t5728rec.tgtRcvdEarn03);
		fieldNo039.set(t5728rec.tgtRcvdEarn04);
		fieldNo040.set(t5728rec.tgtRcvdEarn05);
		fieldNo041.set(t5728rec.tgtRcvdEarn06);
		fieldNo042.set(t5728rec.tgtRcvdEarn07);
		fieldNo043.set(t5728rec.tgtRcvdEarn08);
		fieldNo044.set(t5728rec.tgtRcvdEarn09);
		fieldNo045.set(t5728rec.tgtRcvdEarn10);
		fieldNo046.set(t5728rec.tgtRcvdEarn11);
		fieldNo047.set(t5728rec.tgtRcvdEarn12);
		fieldNo048.set(t5728rec.tgtRcvdEarn13);
		fieldNo049.set(t5728rec.tgtRcvdEarn14);
		fieldNo050.set(t5728rec.commEarn01);
		fieldNo051.set(t5728rec.commEarn02);
		fieldNo052.set(t5728rec.commEarn03);
		fieldNo053.set(t5728rec.commEarn04);
		fieldNo054.set(t5728rec.commEarn05);
		fieldNo055.set(t5728rec.commEarn06);
		fieldNo056.set(t5728rec.commEarn07);
		fieldNo057.set(t5728rec.commEarn08);
		fieldNo058.set(t5728rec.commEarn09);
		fieldNo059.set(t5728rec.commEarn10);
		fieldNo060.set(t5728rec.commEarn11);
		fieldNo061.set(t5728rec.commEarn12);
		fieldNo062.set(t5728rec.commEarn13);
		fieldNo063.set(t5728rec.commEarn14);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
