/******************************************************************************
 * File Name 		: AgcmpfDAO.java
 * Author			: smalchi2
 * Creation Date	: 10 January 2017
 * Project			: Integral Life
 * Description		: The DAO Interface for AGCMPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.flexiblepremium.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.life.flexiblepremium.dataaccess.model.Agcmpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface AgcmpfDAO extends BaseDAO<Agcmpf> {

	public List<Agcmpf> searchAgcmpfRecord(Agcmpf agcmpf) throws SQLRuntimeException;

	public void insertIntoAgcmpf(Agcmpf agcmpf) throws SQLRuntimeException;

	//public void deleteAgcmpf(Agcmpf agcmpf) throws SQLRuntimeException;
	public Map<String, List<Agcmpf>> searchAgcmRecordByChdrnum(List<String> chdrnumList);
	public void insertAgcmpfList(List<Agcmpf> agcmpfList);
	
	 public void updateInvalidRecord(List<Agcmpf> agcmpfList);
	 
	 public void updateAgcmrevdRecord(List<Agcmpf> agcmpfList);
	 
	 public List<Agcmpf> searchAgcmstRecord(String chdrcoy,String chdrnum, String life, String coverage, String rider, int plnsfx, String validflag);
	 
	 public void updateAgcmRecord(List<Agcmpf> agcmpfList) ;
}
