package com.csc.life.flexiblepremium.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGDateData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5363.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5363Report extends SMARTReportLayout { 

	private ZonedDecimalData billedp = new ZonedDecimalData(17, 2);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData coverage = new FixedLengthStringData(2);
	private RPGDateData dateReportVariable = new RPGDateData();
	private FixedLengthStringData life = new FixedLengthStringData(2);
	private ZonedDecimalData minovrpro = new ZonedDecimalData(3, 0);
	private ZonedDecimalData minreqd = new ZonedDecimalData(18, 2);
	private ZonedDecimalData ovrminreq = new ZonedDecimalData(17, 2);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0);
	private ZonedDecimalData prmper = new ZonedDecimalData(17, 2);
	private ZonedDecimalData prmrcdp = new ZonedDecimalData(17, 2);
	private FixedLengthStringData rider = new FixedLengthStringData(2);
	private FixedLengthStringData targto = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5363Report() {
		super();
	}


	/**
	 * Print the XML for R5363d01
	 */
	public void printR5363d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		life.setFieldName("life");
		life.setInternal(subString(recordData, 9, 2));
		coverage.setFieldName("coverage");
		coverage.setInternal(subString(recordData, 11, 2));
		rider.setFieldName("rider");
		rider.setInternal(subString(recordData, 13, 2));
		plnsfx.setFieldName("plnsfx");
		plnsfx.setInternal(subString(recordData, 15, 4));
		targto.setFieldName("targto");
		targto.setInternal(subString(recordData, 19, 10));
		prmper.setFieldName("prmper");
		prmper.setInternal(subString(recordData, 29, 17));
		billedp.setFieldName("billedp");
		billedp.setInternal(subString(recordData, 46, 17));
		prmrcdp.setFieldName("prmrcdp");
		prmrcdp.setInternal(subString(recordData, 63, 17));
		ovrminreq.setFieldName("ovrminreq");
		ovrminreq.setInternal(subString(recordData, 80, 17));
		minovrpro.setFieldName("minovrpro");
		minovrpro.setInternal(subString(recordData, 97, 3));
		printLayout("R5363d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				life,
				coverage,
				rider,
				plnsfx,
				targto,
				prmper,
				billedp,
				prmrcdp,
				ovrminreq,
				minovrpro
			}
		);

	}

	/**
	 * Print the XML for R5363d02
	 */
	public void printR5363d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		minreqd.setFieldName("minreqd");
		minreqd.setInternal(subString(recordData, 1, 18));
		printLayout("R5363d02",			// Record name
			new BaseData[]{			// Fields:
				minreqd
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for R5363h01
	 */
	public void printR5363h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.set(getDate());
		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("R5363h01",			// Record name
			new BaseData[]{			// Fields:
				dateReportVariable,
				company,
				companynm,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(11);
	}

	/**
	 * Print the XML for R5363t01
	 */
	public void printR5363t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		printLayout("R5363t01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

		currentPrintLine.add(1);
	}


}
