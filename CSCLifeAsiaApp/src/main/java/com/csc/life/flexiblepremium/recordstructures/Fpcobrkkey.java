package com.csc.life.flexiblepremium.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:41
 * Description:
 * Copybook name: FPCOBRKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Fpcobrkkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData fpcobrkFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData fpcobrkKey = new FixedLengthStringData(64).isAPartOf(fpcobrkFileKey, 0, REDEFINE);
  	public FixedLengthStringData fpcobrkChdrcoy = new FixedLengthStringData(1).isAPartOf(fpcobrkKey, 0);
  	public FixedLengthStringData fpcobrkChdrnum = new FixedLengthStringData(8).isAPartOf(fpcobrkKey, 1);
  	public FixedLengthStringData fpcobrkLife = new FixedLengthStringData(2).isAPartOf(fpcobrkKey, 9);
  	public FixedLengthStringData fpcobrkCoverage = new FixedLengthStringData(2).isAPartOf(fpcobrkKey, 11);
  	public FixedLengthStringData fpcobrkRider = new FixedLengthStringData(2).isAPartOf(fpcobrkKey, 13);
  	public PackedDecimalData fpcobrkPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(fpcobrkKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(fpcobrkKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(fpcobrkFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		fpcobrkFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}