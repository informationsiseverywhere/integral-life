package com.csc.life.flexiblepremium.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:11
 * Description:
 * Copybook name: LINSFPRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Linsfprkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData linsfprFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData linsfprKey = new FixedLengthStringData(64).isAPartOf(linsfprFileKey, 0, REDEFINE);
  	public FixedLengthStringData linsfprChdrcoy = new FixedLengthStringData(1).isAPartOf(linsfprKey, 0);
  	public FixedLengthStringData linsfprChdrnum = new FixedLengthStringData(8).isAPartOf(linsfprKey, 1);
  	public PackedDecimalData linsfprBillcd = new PackedDecimalData(8, 0).isAPartOf(linsfprKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(linsfprKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(linsfprFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		linsfprFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}