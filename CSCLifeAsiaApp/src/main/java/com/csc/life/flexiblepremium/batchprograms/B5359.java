/*
 * File: B5359.java
 * Date: 29 August 2009 21:10:41
 * Author: Quipoz Limited
 *
 * Class transformed from B5359.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.flexiblepremium.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceAll;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.general.dataaccess.AcagrnlTableDAM;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.dataaccess.Dddelf2TableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3620rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.ZctnTableDAM;
import com.csc.life.agents.dataaccess.ZptnTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5644rec;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.contractservicing.dataaccess.AgcmbchTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.procedures.Zorcompy;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.flexiblepremium.dataaccess.AgcmseqTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FpcoTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FprmTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FprxpfTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FrepTableDAM;
import com.csc.life.flexiblepremium.procedures.Overaloc;
import com.csc.life.flexiblepremium.reports.R5358Report;
import com.csc.life.flexiblepremium.reports.R5359Report;
import com.csc.life.flexiblepremium.tablestructures.T5729rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.regularprocessing.dataaccess.LinsrnlTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.regularprocessing.recordstructures.Rnlallrec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Gettim;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.ArraySearch;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*
*REMARKS
*                   FLEXIBLE PREMIUM COLLECTIONS
*                   ----------------------------
*
* This program processes premium received for Flexible Premium
* Contracts. All premium in suspense will be allocated to the
* contract unless there are billed direct debit premiums where
* due date has not been reached or the amount exceeds the maximum
* allowed or is below the minimum.
*
* This program will run in multi-thread within the Renewal suite.
*
* In  addition,  the  allocation of the amount in suspense can
* generate generic processing such as commission payment and
* unit   allocation   which  is  driven  by table parameters.
*
* Flexible Premium Collections processes the Flexible Premium
* File (FPRMPF) which are selected by B5358.
*
* Processing.
* ----------
* B5359 runs directly after B5358 which 'splits' the FPRMPF
* according to the number of collections programs to run.  All
* references to the FPRM are via FPRMPF - a temporary file
* holding all the FPRM records for this program to process.
*
*
*  1000-INITIALISE.
*  ---------------
*  -  Frequently-referenced tables are stored in working storage
*     arrays to minimize disk IO.
*
*  -  Issue an override to read the correct FPRXPF for this run.
*     The CRTTMPF process has already created a file specific to
*     the run using the  FPRXPF format and is identified  by
*     concatenating the following:-
*
*     'FPRX'
*      BPRD-SYSTEM-PARAM04
*      BSSC-SCHEDULE-NUMBER
*
*      eg FPRX2C0001,  for the first run
*         FPRX2C0002,  for the second etc.
*
*     The number of threads would have been created by the
*     CRTTMPF process given the parameters of the process
*     definition of the CRTTMPF.
*     To get the correct member of the above physical for B5359
*     to read from, concatonate :-
*
*     'THREAD'
*     BSPR-PROCESS-OCC-NUM
*
*     eg. THREAD001,   for the first member (thread 1)
*         THREAD002    for the second etc.  (thread 2)
*
*  -  Initialise the static values of the LIFACMV copybook.
*
*  2000-READ
*  ---------
*
*  -  Read the FPRX records sequentially keeping the count of
*     the number read and storing the present FPRM details for
*     the WSYS- error record.
*
*  -  Write the total lines for any minimum and maximum variance
*     reports produced.
*
*  2500-EDIT
*  ---------
*
*     For Each FPRX  record:
*
*  -  Read the PAYR and ACBL details.
*
*  -  Move the LP S balance into working storage as a positive
*     value. If the LP S balance is 0, do not process the FPRX
*     and read the next record.
*
*  -  Read and validate the CHDR record. If the record is not
*     valid read the next FPRX.
*
*  -  Softlock the CHDR and increment the control total if locked
*
*  -  Read the CLRF details.
*
*  -  Search T3620 for the billing channel of the contract. If
*     the billing channel is direct debit an attempt is made to
*     read any DDDE records for the contract where the due date
*     has not yet been reached. If any exists the amount on these
*     DDDE records is subtracted from the amount in suspense and
*     the remainder allocated. If there is no remainder the next
*     FPRX is read.
*
*  -  A search is made of T5729 for the premium variances for the
*     contract type and frequency.
*
*  -  The total amount billed and the total amount received in
*     period are compared and if the amount billed has not been
*     received the difference is allocated and the remainder
*     tested to ensure that it does not exceed the maximum
*     allowed.
*
*  -  If the amount in suspense after any unpaid premiums are
*     allocated exceeds the maximum allowed, this amount is not
*     allocated and a report is written for this amount.
*
*  -  The amount in suspense is then tested to ensure it is
*     greater than the minimum amount allowed. If it does not,
*     a report is generated for this amount and the next FPRX is
*     read.
*
*  3000-UPDATE
*  -----------
*
*  -  Create a LINS record for the allocated amount with a status
*     of paid in order to complete the audit trail and ensure
*     reversals can function correctly.
*
*  -  Update the CHDR record adding 1 to the tranno.
*
*  -  Update the FPRM record adding the amount allocated to the
*     total premium received.
*
*  -  Update the PAYR record with the paid-to-date equal to the
*     billed to date from the previous record if there are no
*     outstanding DDDE records for the contract.
*
*  -  Call LIFRTRN to debit the suspense account.
*
*  -  Read all the coverages for the contract.  For each
*     coverage of a valid status with an instalment premium
*     greater than 0 (i.e. not a coverage) work out the amount
*     to be allocated proportionately for the coverage.
*
*  -  Process component level accounting for each coverage.
*
*  -  Read the first active FPCO record for the coverage and
*     determine whether this is the record for the current period
*     or for a previous period.
*
*  -  If the current to date on the FPCO record is less than the
*     billed to date on the PAYR record, we are looking at the
*     record for the previous period.
*
*  -  The pro-rata amount for the policy is less than the amount
*     left to pay in the period we should add the amount to the
*     premium received field on the FPCO and add this amount to
*     the working storage total for up to target premium
*
*  -  If the pro-rata amount is greater or equal to the amount
*     left to pay in the period we should 'pay up' the premium
*     received field on the FPCO for this period, set the active
*     indicator to 'N' and add this amount to the over target
*     working storage.
*
*  -  If we are looking at the current FPCO record we should pay
*     the whole amount on to the current record.
*
*  -  If the pro-rata amount is less than or equal to the amount
*     left to pay, the whole amount should be added into up to
*     target working storage.
*
*  -  If the pro-rata amount is greater than the amount left
*     to pay we add the amount left to pay into the up to target
*     working  storage and the remainder into over target
*     working storage.
*
*     Commission
*     ----------
*
*        Set up the linkage and call the commission payment
*        subroutine held on T5644.
*
*  -  Set up linkage and call the unit allocation subroutine
*     For each FPCO updated
*
*  -  Write a PTRN for the transaction.
*
*  -  Release the softlock on the CHDR.
*
* Control totals used in this program:
*
*    01  -  No. of FPRM  records read
*    02  -  No. of FPRM with no LP S
*    03  -  Tot. LP S read
*    04  -  No. of FPRM records softlocked
*    05  -  No. of FPRM with no remaining LP S
*    06  -  Tot. amount of LP S allocated
*    07  -  No. of outstanding DDDE records
*    08  -  Tot. amount outstanding on DDDE records
*    09  -  No. of records under minimum LP S
*    10  -  No. of records over maximum LP S
*    11  -  Tot. amount of LP S under minimum
*    12  -  Tot. amount of LP S over maximum
*    13  -  No. of LINS records created
*    14  -  No. of PTRN records created
*
*     (BATC processing is handled in MAINB)
*
****************************************************************** ****
* </pre>
*/
public class B5359 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FprxpfTableDAM fprxpf = new FprxpfTableDAM();
	private FprxpfTableDAM fprxpfRec = new FprxpfTableDAM();
		/* ERRORS */
	private static final String h791 = "H791";
	private static final String h979 = "H979";
	private static final String e308 = "E308";
	private static final String i086 = "I086";
	private static final String e103 = "E103";
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5359");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/*  FPRX parameters*/
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaFprxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaFprxFn, 0, FILLER).init("FPRX");
	private FixedLengthStringData wsaaFprxRunid = new FixedLengthStringData(2).isAPartOf(wsaaFprxFn, 4);
	private ZonedDecimalData wsaaFprxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaFprxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
		/*  Calculated amounts*/
	private ZonedDecimalData wsaaSuspAvail = new ZonedDecimalData(18, 2);
	private FixedLengthStringData wsaaSuspRed = new FixedLengthStringData(18).isAPartOf(wsaaSuspAvail, 0, REDEFINE);
	private ZonedDecimalData wsaaBascpyDue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaBascpyErn = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaBascpyPay = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaSrvcpyDue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaSrvcpyErn = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaRnwcpyErn = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaRnwcpyDue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOvrdBascpyDue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOvrdBascpyErn = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOvrdBascpyPay = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaT5645Offset = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private PackedDecimalData wsaaDifference = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaMaxAllowed = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaMinAllowed = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotPols = new PackedDecimalData(17, 2).init(0);
	//private PackedDecimalData wsaaTotAllocated = new PackedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaTotAllocated = new ZonedDecimalData(18, 2).init(0);
	private FixedLengthStringData wsaaTotRed = new FixedLengthStringData(18).isAPartOf(wsaaTotAllocated, 0, REDEFINE);
	private PackedDecimalData wsaaPolamnt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotamt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTgtDiff = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaUpToTgt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaOvrTgt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaOvrTarget = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaPolAmt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaBilled = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaAfterPremPaid = new PackedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaCommType = new ZonedDecimalData(1, 0).setUnsigned();
	private PackedDecimalData wsaaProportion = new PackedDecimalData(10, 7);
		/* WSAA-SAVED-AMOUNTS */
	private PackedDecimalData[] wsaaPayamnt = PDInittedArray(3, 13, 2);
	private PackedDecimalData[] wsaaErndamt = PDInittedArray(3, 13, 2);
	private PackedDecimalData wsaaGlSub = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaT5645Sub = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaT5729Sub = new PackedDecimalData(5, 0);
	private int wsaaT3620Sub = 0;
	private PackedDecimalData wsaaTest = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaNumPeriod = new ZonedDecimalData(11, 5).init(0);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).init(0).setUnsigned();
		/* WSAA-DATES */
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(11, 5);

	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaTerm, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaTermLeftInteger = new ZonedDecimalData(6, 0).isAPartOf(filler3, 0).setUnsigned();
	private ZonedDecimalData wsaaTermLeftRemain = new ZonedDecimalData(5, 0).isAPartOf(filler3, 6).setUnsigned();
	private FixedLengthStringData wsaaPayrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPayrcoy = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
		/* WSAA-BILLFREQ-VAR */
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuff = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);

	private FixedLengthStringData wsaaRldgagnt = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaAgntChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgagnt, 0);
	private FixedLengthStringData wsaaAgntLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 8);
	private FixedLengthStringData wsaaAgntCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 10);
	private FixedLengthStringData wsaaAgntRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 12);
	private FixedLengthStringData wsaaAgntPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 14);

		/*  Status indicators*/
	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");

	private FixedLengthStringData wsaaValidCovr = new FixedLengthStringData(1).init("N");
	private Validator validCovr = new Validator(wsaaValidCovr, "Y");

	private FixedLengthStringData wsaaDateFound = new FixedLengthStringData(1).init("N");
	private Validator dateFound = new Validator(wsaaDateFound, "Y");
	private Validator dateNotFound = new Validator(wsaaDateFound, "N");

	private FixedLengthStringData wsaaFreqFound = new FixedLengthStringData(1).init("N");
	private Validator freqFound = new Validator(wsaaFreqFound, "Y");

	private FixedLengthStringData wsaaDurationFound = new FixedLengthStringData(1).init("N");
	private Validator durationFound = new Validator(wsaaDurationFound, "Y");
	private Validator durationNotFound = new Validator(wsaaDurationFound, "N");

	private FixedLengthStringData wsaaMaxFail = new FixedLengthStringData(1).init("N");
	private Validator failMaximumTest = new Validator(wsaaMaxFail, "Y");

	private FixedLengthStringData wsaaMinFail = new FixedLengthStringData(1).init("N");
	private Validator failMinimumTest = new Validator(wsaaMinFail, "Y");

	private FixedLengthStringData wsaaFpcoFound = new FixedLengthStringData(1).init("N");
	private Validator fpcoRecordFound = new Validator(wsaaFpcoFound, "Y");

	private FixedLengthStringData wsaaAllocAmt = new FixedLengthStringData(1).init("N");
	private Validator allocAmtFound = new Validator(wsaaAllocAmt, "Y");

	private FixedLengthStringData wsaaAllAllocated = new FixedLengthStringData(1).init("N");
	private Validator allAllocated = new Validator(wsaaAllAllocated, "Y");

	private FixedLengthStringData wsaaOutstandingDdde = new FixedLengthStringData(1).init("N");
	private Validator outstandingDdde = new Validator(wsaaOutstandingDdde, "Y");
	private Validator noOutstandingDdde = new Validator(wsaaOutstandingDdde, "N");
		/*  Storage for T5644 table items.*/
	//private static final int wsaaT5644Size = 50;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5644Size = 1000;

		/* WSAA-T5644-ARRAY
		 03  WSAA-T5644-REC  OCCURS 30                                */
	private FixedLengthStringData[] wsaaT5644Rec = FLSInittedArray (1000, 11);
	private FixedLengthStringData[] wsaaT5644Key = FLSDArrayPartOfArrayStructure(4, wsaaT5644Rec, 0);
	private FixedLengthStringData[] wsaaT5644Commth = FLSDArrayPartOfArrayStructure(4, wsaaT5644Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT5644Data = FLSDArrayPartOfArrayStructure(7, wsaaT5644Rec, 4);
	private FixedLengthStringData[] wsaaT5644Comsub = FLSDArrayPartOfArrayStructure(7, wsaaT5644Data, 0);
		/*  Storage for T5645 table items.*/
	//private static final int wsaaT5645Size = 45;
	//private static final int wsaaT5645Size = 300;//ILIFE-1985
	//ILIFE-2628 fixed--Array size increased
	  private static final int wsaaT5645Size = 1000;

		/* WSAA-T5645-ARRAY */
	//private FixedLengthStringData[] wsaaT5645Data = FLSInittedArray (45, 21);
	  private FixedLengthStringData[] wsaaT5645Data = FLSInittedArray (1000, 21);//ILIFE-1985
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaT5645Data, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaT5645Data, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaT5645Data, 20);

	private FixedLengthStringData wsaaDurations = new FixedLengthStringData(16);
	private ZonedDecimalData[] wsaaDuration = ZDArrayPartOfStructure(4, 4, 0, wsaaDurations, 0);

	private FixedLengthStringData wsaaOverdueMins = new FixedLengthStringData(12);
	private ZonedDecimalData[] wsaaOverdueMin = ZDArrayPartOfStructure(4, 3, 0, wsaaOverdueMins, 0);

	private FixedLengthStringData wsaaTargetMaxs = new FixedLengthStringData(20);
	private ZonedDecimalData[] wsaaTargetMax = ZDArrayPartOfStructure(4, 5, 0, wsaaTargetMaxs, 0);

	private FixedLengthStringData wsaaTargetMins = new FixedLengthStringData(12);
	private ZonedDecimalData[] wsaaTargetMin = ZDArrayPartOfStructure(4, 3, 0, wsaaTargetMins, 0);
	private FixedLengthStringData wsaaT5729Item = new FixedLengthStringData(4);
	//private static final int wsaaT5729Size = 60;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5729Size = 1000;
		/*  Storage for T3620 table items.*/
	//private static final int wsaaT3620Size = 10;
	//	private static final int wsaaT3620Size = 100;//ILIFE-1985
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT3620Size = 1000;
	private FixedLengthStringData wsaaT3620Item = new FixedLengthStringData(4);

		/* WSAA-T3620-ARRAY */
	//private FixedLengthStringData[] wsaaT3620Rec = FLSInittedArray (10, 2);
	private FixedLengthStringData[] wsaaT3620Rec = FLSInittedArray (1000, 2);//ILIFE-1985
	private FixedLengthStringData[] wsaaT3620Key = FLSDArrayPartOfArrayStructure(1, wsaaT3620Rec, 0);
	private FixedLengthStringData[] wsaaT3620Billchnl = FLSDArrayPartOfArrayStructure(1, wsaaT3620Key, 0);
	private FixedLengthStringData[] wsaaT3620Data = FLSDArrayPartOfArrayStructure(1, wsaaT3620Rec, 1);
	private FixedLengthStringData[] wsaaT3620Ddind = FLSDArrayPartOfArrayStructure(1, wsaaT3620Data, 0);
		/*  Storage for T5671 table items.*/
	private FixedLengthStringData wsaaT5671Item = new FixedLengthStringData(4);

	//private static final int wsaaT5671Size = 250;
	private static final int wsaaT5671Size = 3000;//ILIFE-1985

		/* WSAA-T5671-ARRAY
		 03  WSAA-T5671-REC OCCURS 50
		 03  WSAA-T5671-REC OCCURS 250                        <V65L21>*/
	//private FixedLengthStringData[] wsaaT5671Rec = FLSInittedArray (500, 48);
	private FixedLengthStringData[] wsaaT5671Rec = FLSInittedArray (3000, 48);//ILIFE-1985
	private FixedLengthStringData[] wsaaT5671Key = FLSDArrayPartOfArrayStructure(8, wsaaT5671Rec, 0);
	private FixedLengthStringData[] wsaaT5671Trcde = FLSDArrayPartOfArrayStructure(4, wsaaT5671Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT5671Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5671Key, 4, HIVALUES);
	private FixedLengthStringData[] wsaaT5671Data = FLSDArrayPartOfArrayStructure(40, wsaaT5671Rec, 8);
	private FixedLengthStringData[] wsaaT5671Subprogs = FLSDArrayPartOfArrayStructure(40, wsaaT5671Data, 0);
	private FixedLengthStringData[][] wsaaT5671Subprog = FLSDArrayPartOfArrayStructure(4, 10, wsaaT5671Subprogs, 0);

	private FixedLengthStringData wsaaTrcdeCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaAuthCode = new FixedLengthStringData(4).isAPartOf(wsaaTrcdeCrtable, 0);
	private FixedLengthStringData wsaaCovrlnbCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTrcdeCrtable, 4);
		/*  Storage for T5688 table items.*/
	//private static final int wsaaT5688Size = 60;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5688Size = 1000;

		/* WSAA-T5688-ARRAY */
	private FixedLengthStringData[] wsaaT5688Rec = FLSInittedArray (1000, 10);
	private FixedLengthStringData[] wsaaT5688Key = FLSDArrayPartOfArrayStructure(3, wsaaT5688Rec, 0);
	private FixedLengthStringData[] wsaaT5688Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5688Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5688Data = FLSDArrayPartOfArrayStructure(7, wsaaT5688Rec, 3);
	private PackedDecimalData[] wsaaT5688Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5688Data, 0);
	private FixedLengthStringData[] wsaaT5688Comlvlacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 5);
	private FixedLengthStringData[] wsaaT5688Revacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 6);
		/*  Storage for T6654 table items.*/
	//private static final int wsaaT6654Size = 500;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT6654Size = 1000;

		/* WSAA-T6654-ARRAY
		 03  WSAA-T6654-REC OCCURS 80                                 */
	private FixedLengthStringData[] wsaaT6654Rec = FLSInittedArray (1000, 12);
	private FixedLengthStringData[] wsaaT6654Key = FLSDArrayPartOfArrayStructure(4, wsaaT6654Rec, 0);
	private FixedLengthStringData[] wsaaT6654Billchnl = FLSDArrayPartOfArrayStructure(1, wsaaT6654Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT6654Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT6654Key, 1, HIVALUES);
	private FixedLengthStringData[] wsaaT6654Data = FLSDArrayPartOfArrayStructure(8, wsaaT6654Rec, 4);
	private FixedLengthStringData[] wsaaT6654Collsub = FLSDArrayPartOfArrayStructure(8, wsaaT6654Data, 0);

	private FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(97);
	private FixedLengthStringData wsysFprmkey = new FixedLengthStringData(11).isAPartOf(wsysSystemErrorParams, 0);
	private FixedLengthStringData wsysChdrcoy = new FixedLengthStringData(2).isAPartOf(wsysFprmkey, 0);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysFprmkey, 2);
	private FixedLengthStringData wsysPayrseqno = new FixedLengthStringData(1).isAPartOf(wsysFprmkey, 10);
	private FixedLengthStringData wsysSysparams = new FixedLengthStringData(86).isAPartOf(wsysSystemErrorParams, 11);
	private FixedLengthStringData wsaaBillfq = new FixedLengthStringData(2);
	private static final int wsaaAgcmIxSize = 100;
	private ZonedDecimalData wsaaAgcmIx = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaAgcmIa = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private static final int wsaaAgcmIySize = 500;
	private ZonedDecimalData wsaaAgcmIy = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaAgcmIb = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaAgcmPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgcmAlcprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgcmPremLeft = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaReceivedPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaOutstandingPremium = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaAgcmFound = new FixedLengthStringData(1);
	private Validator agcmFound = new Validator(wsaaAgcmFound, "Y");

	private FixedLengthStringData wsaaPremType = new FixedLengthStringData(1);
	private Validator singPrem = new Validator(wsaaPremType, "S");
	private Validator firstPrem = new Validator(wsaaPremType, "I");
	private Validator renPrem = new Validator(wsaaPremType, "R");

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);

	private FixedLengthStringData wsaaEndPost = new FixedLengthStringData(1).init("N");
	private Validator endPost = new Validator(wsaaEndPost, "Y");
	private static final String zptnrec = "ZPTNREC";
	private static final String zctnrec = "ZCTNREC";
	private static final String wsaaSubroutineA="LBONCALA";
	private static final String wsaaSubroutineB="LBONCALB";
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaT5644Ix = new IntegerData();
	private IntegerData wsaaT5729Ix = new IntegerData();
	private IntegerData wsaaT3620Ix = new IntegerData();
	private IntegerData wsaaT5671Ix = new IntegerData();
	private IntegerData wsaaSubprogIx = new IntegerData();
	private IntegerData wsaaT5688Ix = new IntegerData();
	private IntegerData wsaaT6654Ix = new IntegerData();
	private AcagrnlTableDAM acagrnlIO = new AcagrnlTableDAM();
	private AcblTableDAM acblIO = new AcblTableDAM();
	private AgcmbchTableDAM agcmbchIO = new AgcmbchTableDAM();
	private AgcmseqTableDAM agcmseqIO = new AgcmseqTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private Dddelf2TableDAM dddelf2IO = new Dddelf2TableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FpcoTableDAM fpcoIO = new FpcoTableDAM();
	private FprmTableDAM fprmIO = new FprmTableDAM();
	private FrepTableDAM frepIO = new FrepTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LinsrnlTableDAM linsrnlIO = new LinsrnlTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	private ZctnTableDAM zctnIO = new ZctnTableDAM();
	private ZptnTableDAM zptnIO = new ZptnTableDAM();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private Rnlallrec rnlallrec = new Rnlallrec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private P6671par p6671par = new P6671par();
	private Zorlnkrec zorlnkrec = new Zorlnkrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private T3695rec t3695rec = new T3695rec();
	private T5644rec t5644rec = new T5644rec();
	private T5645rec t5645rec = new T5645rec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private T6654rec t6654rec = new T6654rec();
	private T5729rec t5729rec = new T5729rec();
	private T3620rec t3620rec = new T3620rec();
	private Th605rec th605rec = new Th605rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private ControlTotalsInner controlTotalsInner = new ControlTotalsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaAgcmSummaryInner wsaaAgcmSummaryInner = new WsaaAgcmSummaryInner();
	private WsaaT5729ArrayInner wsaaT5729ArrayInner = new WsaaT5729ArrayInner();
	private WsaaVariablesInner wsaaVariablesInner = new WsaaVariablesInner();
	
	//Ticket#1296 starts by vhukumagrawa
	private R5358Report minVarReport = new R5358Report();
	private R5359Report maxVarReport = new R5359Report();
	
	private FixedLengthStringData r5358h01Record = new FixedLengthStringData(49);
	private FixedLengthStringData r5358h01O = new FixedLengthStringData(49).isAPartOf(r5358h01Record, 0);
	private FixedLengthStringData r5358h01Company = new FixedLengthStringData(1).isAPartOf(r5358h01O, 0);
	private FixedLengthStringData r5358h01Companynm = new FixedLengthStringData(30).isAPartOf(r5358h01O, 1);
	private FixedLengthStringData r5358h01Sdate = new FixedLengthStringData(10).isAPartOf(r5358h01O, 31);
	private FixedLengthStringData r5358h01Stime = new FixedLengthStringData(8).isAPartOf(r5358h01O, 41);

	private FixedLengthStringData r5358t01Record = new FixedLengthStringData(18);
	private FixedLengthStringData r5358t01O = new FixedLengthStringData(18).isAPartOf(r5358t01Record, 0);	
	private ZonedDecimalData r5358h01TotReceived = new ZonedDecimalData(18,2).isAPartOf(r5358t01O, 0);
	
	private FixedLengthStringData r5359h01Record = new FixedLengthStringData(49);
	private FixedLengthStringData r5359h01O = new FixedLengthStringData(49).isAPartOf(r5359h01Record, 0);
	private FixedLengthStringData r5359h01Company = new FixedLengthStringData(1).isAPartOf(r5359h01O, 0);
	private FixedLengthStringData r5359h01Companynm = new FixedLengthStringData(30).isAPartOf(r5359h01O, 1);
	private FixedLengthStringData r5359h01Sdate = new FixedLengthStringData(10).isAPartOf(r5358h01O, 31);
	private FixedLengthStringData r5359h01Stime = new FixedLengthStringData(8).isAPartOf(r5358h01O, 41);
	
	private FixedLengthStringData r5359t01Record = new FixedLengthStringData(18);
	private FixedLengthStringData r5359t01O = new FixedLengthStringData(18).isAPartOf(r5359t01Record, 0);	
	private ZonedDecimalData r5359h01TotReceived = new ZonedDecimalData(18,2).isAPartOf(r5359t01O, 0);
	
	private R5358d01RecordInner r5358printRecordInner = new R5358d01RecordInner();
	private R5359d01RecordInner r5359printRecordInner = new R5359d01RecordInner();
	
	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	
	private ZonedDecimalData wsaaLinecnt5358 = new ZonedDecimalData(4, 0).init(60).setUnsigned();
	private ZonedDecimalData wsaaLinecnt5359 = new ZonedDecimalData(4, 0).init(60).setUnsigned();
	private String wsaaPageChange5358 = "Y";
	private String wsaaPageChange5359 = "Y";
	private FixedLengthStringData wsaaFrepCreated = new FixedLengthStringData(1).init("N");
	private Validator frepCreated = new Validator(wsaaFrepCreated, "Y");
	private static final int wsaaMaxLines = 66;
	private Datcon1rec datcon1rec = new Datcon1rec();
	private ZonedDecimalData wsaaInTime = new ZonedDecimalData(6, 0).setUnsigned();
	private PackedDecimalData wsaaMinTotReqd = new PackedDecimalData(18, 2).init(ZERO);
	private PackedDecimalData wsaaMaxTotReqd = new PackedDecimalData(18, 2).init(ZERO);
	private static final String t1693 = "T1693";
	private FixedLengthStringData wsaaFirst  = new FixedLengthStringData(1);
	private T5540rec t5540rec = new T5540rec();
	private FixedLengthStringData wsaaLoyaltyApp = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaOverdueDays = new ZonedDecimalData(4, 0).init(0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private WsaaT6654ArrayInner wsaaT6654ArrayInner = new WsaaT6654ArrayInner();
	private Map<String, List<Itempf>> t6654ListMap; 
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private IntegerData wsaaT6654SubrIx = new IntegerData();
	private static final String t6654 ="T6654";
	private IntegerData wsaaT6654ExpyIx = new IntegerData();
	private String wsaaLoyalty = "Y";
	//Ticket#1296 ends by vhukumagrawa
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		next3515,
		skipBonusWorkbench3515,
		readNextCovr3517,
		skipBonusWorkbench3572,
		post3632,
		exit3639,
		b220Call,
		b270Next,
		b290Exit,
		b320Locate,
		b330Loop,
		b390Exit
	}

	public B5359() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/*  Move the parameter screen data from P6671 into its original*/
		/*  field map.*/
		t6654ListMap = new HashMap<String, List<Itempf>>(); 
		wsaaLoyaltyApp.set("N");
		wsaaFirst.set("Y");
		p6671par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		/*  Point to correct member of FPRXPF.*/
		wsaaFprxRunid.set(bprdIO.getSystemParam04());
		wsaaFprxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(FPRXPF) TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaFprxFn);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		fprxpf.openInput();
		//ILIFE-1296 STARTS
		minVarReport.openOutput();
		maxVarReport.openOutput();
		r5358h01Company.set(SPACES);
		r5358h01Companynm.set(SPACES);

		r5358printRecordInner.rd01Chdrnum.set(SPACES);
		r5359printRecordInner.rd01Chdrnum.set(SPACES);		
		wsaaLinecnt5358.set(65);
		//ILIFE-1296 ENDS
		/*   OPEN OUTPUT PRINTER-FILE1.                                   */
		/*   OPEN OUTPUT PRINTER-FILE2.                                   */
		/* Get transaction description.*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(tablesInner.t1688);
		descIO.setDescitem(bprdIO.getAuthCode());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		/*  Initialise common LIFA, LIFR, RNLA and PTRN fields*/
		varcom.vrcmDate.set(getCobolDate());
		ptrnIO.setParams(SPACES);
		ptrnIO.setUser(0);
		lifrtrnrec.lifrtrnRec.set(SPACES);
		lifacmvrec.lifacmvRec.set(SPACES);
		rnlallrec.rnlallRec.set(SPACES);
		rnlallrec.effdate.set(bsscIO.getEffectiveDate());
		rnlallrec.moniesDate.set(bsscIO.getEffectiveDate());
		ptrnIO.setPtrneff(bsscIO.getEffectiveDate());
		ptrnIO.setTransactionDate(varcom.vrcmDate.toInt());
		lifrtrnrec.batccoy.set(batcdorrec.company);
		lifrtrnrec.rldgcoy.set(batcdorrec.company);
		lifrtrnrec.genlcoy.set(batcdorrec.company);
		lifacmvrec.batccoy.set(batcdorrec.company);
		lifacmvrec.rldgcoy.set(batcdorrec.company);
		lifacmvrec.genlcoy.set(batcdorrec.company);
		rnlallrec.batccoy.set(batcdorrec.company);
		ptrnIO.setBatccoy(batcdorrec.company);
		rnlallrec.language.set(bsscIO.getLanguage());
		lifrtrnrec.batcactyr.set(batcdorrec.actyear);
		lifacmvrec.batcactyr.set(batcdorrec.actyear);
		rnlallrec.batcactyr.set(batcdorrec.actyear);
		ptrnIO.setBatcactyr(batcdorrec.actyear);
		lifrtrnrec.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec.batcactmn.set(batcdorrec.actmonth);
		rnlallrec.batcactmn.set(batcdorrec.actmonth);
		ptrnIO.setBatcactmn(batcdorrec.actmonth);
		lifrtrnrec.batctrcde.set(batcdorrec.trcde);
		lifacmvrec.batctrcde.set(batcdorrec.trcde);
		rnlallrec.batctrcde.set(batcdorrec.trcde);
		ptrnIO.setBatctrcde(batcdorrec.trcde);
		lifrtrnrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		rnlallrec.batcbatch.set(batcdorrec.batch);
		ptrnIO.setBatcbatch(batcdorrec.batch);
		lifrtrnrec.batcbrn.set(batcdorrec.branch);
		lifacmvrec.batcbrn.set(batcdorrec.branch);
		rnlallrec.batcbrn.set(batcdorrec.branch);
		ptrnIO.setBatcbrn(batcdorrec.branch);
		lifrtrnrec.rcamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifrtrnrec.crate.set(0);
		lifacmvrec.crate.set(0);
		lifrtrnrec.acctamt.set(0);
		lifacmvrec.acctamt.set(0);
		lifrtrnrec.user.set(0);
		lifacmvrec.user.set(0);
		rnlallrec.user.set(0);
		lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifrtrnrec.transactionTime.set(varcom.vrcmTime);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		lifrtrnrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifrtrnrec.transactionDate.set(bsscIO.getEffectiveDate());
		wsaaVariablesInner.wsaaTotTaxCharged.set(ZERO);
		wsaaVariablesInner.wsaaPremNet.set(ZERO);
		wsaaVariablesInner.wsaaOrigSusp.set(ZERO);
		wsaaVariablesInner.wsaaTaxPercent.set(ZERO);
		wsaaVariablesInner.wsaaDiff.set(ZERO);
		wsaaVariablesInner.wsaaTaxProp.set(ZERO);
		/* Initialise tax array                                            */
		for (wsaaVariablesInner.wsaaIx.set(1); !(isGT(wsaaVariablesInner.wsaaIx, 98)); wsaaVariablesInner.wsaaIx.add(1)){
			wsaaVariablesInner.wsaaLife[wsaaVariablesInner.wsaaIx.toInt()].set(SPACES);
			wsaaVariablesInner.wsaaCoverage[wsaaVariablesInner.wsaaIx.toInt()].set(SPACES);
			wsaaVariablesInner.wsaaRider[wsaaVariablesInner.wsaaIx.toInt()].set(SPACES);
			wsaaVariablesInner.wsaaTaxAbs[wsaaVariablesInner.wsaaIx.toInt()][1].set(SPACES);
			wsaaVariablesInner.wsaaTaxAbs[wsaaVariablesInner.wsaaIx.toInt()][2].set(SPACES);
			wsaaVariablesInner.wsaaTaxType[wsaaVariablesInner.wsaaIx.toInt()][1].set(SPACES);
			wsaaVariablesInner.wsaaTaxType[wsaaVariablesInner.wsaaIx.toInt()][2].set(SPACES);
			wsaaVariablesInner.wsaaTaxRule[wsaaVariablesInner.wsaaIx.toInt()].set(SPACES);
			wsaaVariablesInner.wsaaTaxItem[wsaaVariablesInner.wsaaIx.toInt()].set(SPACES);
			wsaaVariablesInner.wsaaPlanSuffix[wsaaVariablesInner.wsaaIx.toInt()].set(ZERO);
			wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][1].set(ZERO);
			wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][2].set(ZERO);
		}
		/* Load account codes*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		wsaaT5645Sub.set(1);
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
			loadT56451100();
		}

		/* Retrieve the suspense account sign*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tablesInner.t3695);
		itemIO.setItemitem(wsaaT5645Sacstype[1]);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		/*  Load contract type details.*/
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(SPACES);
		itdmIO.setItmfrm(0);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL");
		wsaaT5688Ix.set(1);
		while ( !(isEQ(itdmIO.getStatuz(), varcom.endp))) {
			loadT56881400();
		}

		/*  Load Contract statii.*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/*  Load the commission methods.*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemitem(SPACES);
		itemIO.setItemtabl(tablesInner.t5644);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		wsaaT5644Ix.set(1);
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
			loadT56441600();
		}

		/*  Load the Collection routines*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tablesInner.t6654);
		itemIO.setItemitem(SPACES);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMTABL");
		wsaaT6654Ix.set(1);
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
			loadT66541700();
		}

		/*  Load the coverage switching details.*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bprdIO.getCompany());
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setItemtabl(tablesInner.t5671);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		itemIO.setStatuz(varcom.oK);
		wsaaT5671Ix.set(1);
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
			loadT56711900();
		}

		/*  Load the premium variances.*/
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(tablesInner.t5729);
		itdmIO.setItemitem(SPACES);
		itdmIO.setItmto(0);
		itdmIO.setItmfrm(0);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL");
		wsaaT5729Ix.set(1);
		wsaaT5729Sub.set(1);
		while ( !(isEQ(itdmIO.getStatuz(), varcom.endp))) {
			loadT57291950();
		}

		/*  Load the Direct Debit Indicators*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bprdIO.getCompany());
		itemIO.setItemitem(SPACES);
		itemIO.setItemtabl(tablesInner.t3620);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		itemIO.setStatuz(varcom.oK);
		wsaaT3620Ix.set(1);
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
			loadT36201800();
		}

		/*  Load Company Defaults                                          */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tablesInner.th605);
		itemIO.setItemitem(bsprIO.getCompany());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		/*    IF ITEM-STATUZ           NOT = O-K AND MRNF          <V73L01>*/
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
	}

protected void loadT56451100()
	{
		start1110();
	}

protected void start1110()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(bsprIO.getCompany(), itemIO.getItemcoy())
		|| isNE(itemIO.getItemtabl(), tablesInner.t5645)
		|| isNE(itemIO.getItemitem(), wsaaProg)
		|| isNE(itemIO.getStatuz(), varcom.oK)) {
			if (isEQ(itemIO.getFunction(), varcom.begn)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			else {
				itemIO.setStatuz(varcom.endp);
				return ;
			}
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		wsaaT5645Sub.set(1);
		if (isGT(wsaaT5645Sub, wsaaT5645Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(tablesInner.t5645);
			fatalError600();
		}
		while ( !(isGT(wsaaT5645Sub, 15))) {
			wsaaT5645Cnttot[wsaaT5645Offset.toInt()].set(t5645rec.cnttot[wsaaT5645Sub.toInt()]);
			wsaaT5645Glmap[wsaaT5645Offset.toInt()].set(t5645rec.glmap[wsaaT5645Sub.toInt()]);
			wsaaT5645Sacscode[wsaaT5645Offset.toInt()].set(t5645rec.sacscode[wsaaT5645Sub.toInt()]);
			wsaaT5645Sacstype[wsaaT5645Offset.toInt()].set(t5645rec.sacstype[wsaaT5645Sub.toInt()]);
			wsaaT5645Sign[wsaaT5645Offset.toInt()].set(t5645rec.sign[wsaaT5645Sub.toInt()]);
			wsaaT5645Sub.add(1);
			wsaaT5645Offset.add(1);
		}

		itemIO.setFunction(varcom.nextr);
	}

protected void loadT56881400()
	{
		start1410();
	}

protected void start1410()
	{
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), bprdIO.getCompany())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)) {
			if (isEQ(itdmIO.getFunction(), varcom.begn)) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			else {
				itdmIO.setStatuz(varcom.endp);
				return ;
			}
		}
		if (isGT(wsaaT5688Ix, wsaaT5688Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(tablesInner.t5688);
			fatalError600();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		wsaaT5688Itmfrm[wsaaT5688Ix.toInt()].set(itdmIO.getItmfrm());
		wsaaT5688Cnttype[wsaaT5688Ix.toInt()].set(itdmIO.getItemitem());
		wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()].set(t5688rec.comlvlacc);
		wsaaT5688Revacc[wsaaT5688Ix.toInt()].set(t5688rec.revacc);
		itdmIO.setFunction(varcom.nextr);
		wsaaT5688Ix.add(1);
	}

protected void loadT56441600()
	{
		start1610();
	}

protected void start1610()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemcoy(), bsprIO.getCompany())
		|| isNE(itemIO.getItemtabl(), tablesInner.t5644)
		|| isNE(itemIO.getStatuz(), varcom.oK)) {
			itemIO.setStatuz(varcom.endp);
			return ;
		}
		if (isGT(wsaaT5644Ix, wsaaT5644Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(tablesInner.t5644);
			fatalError600();
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		wsaaT5644Commth[wsaaT5644Ix.toInt()].set(itemIO.getItemitem());
		wsaaT5644Comsub[wsaaT5644Ix.toInt()].set(t5644rec.compysubr);
		itemIO.setFunction(varcom.nextr);
		wsaaT5644Ix.add(1);
	}

protected void loadT66541700()
	{
		start1710();
	}

protected void start1710()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.endp)
		|| isNE(itemIO.getItemcoy(), bprdIO.getCompany())
		|| isNE(itemIO.getItemtabl(), tablesInner.t6654)) {
			itemIO.setStatuz(varcom.endp);
			return ;
		}
		if (isGT(wsaaT6654Ix, wsaaT6654Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(tablesInner.t6654);
			fatalError600();
		}
		t6654rec.t6654Rec.set(itemIO.getGenarea());
		itemIO.setFunction(varcom.nextr);
		wsaaT6654Ix.add(1);
	}

protected void loadT36201800()
	{
		start1810();
	}

protected void start1810()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getItemcoy(), bprdIO.getCompany())
		|| isNE(itemIO.getItemtabl(), tablesInner.t3620)
		|| isEQ(itemIO.getStatuz(), varcom.endp)) {
			itemIO.setStatuz(varcom.endp);
			return ;
		}
		t3620rec.t3620Rec.set(itemIO.getGenarea());
		if (isGT(wsaaT3620Ix, wsaaT3620Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(tablesInner.t3620);
			fatalError600();
		}
		wsaaT3620Key[wsaaT3620Ix.toInt()].set(itemIO.getItemitem());
		wsaaT3620Ddind[wsaaT3620Ix.toInt()].set(t3620rec.ddind);
		wsaaT3620Ix.add(1);
		itemIO.setFunction(varcom.nextr);
	}

protected void loadT56711900()
	{
		start1910();
	}

protected void start1910()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isGT(wsaaT5671Ix, wsaaT5671Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(tablesInner.t5671);
			fatalError600();
		}
		wsaaT5671Item.set(itemIO.getItemitem());
		if (isEQ(itemIO.getStatuz(), varcom.endp)
		|| isNE(itemIO.getItemcoy(), bprdIO.getCompany())
		|| isNE(itemIO.getItemtabl(), tablesInner.t5671)
		|| isNE(wsaaT5671Item, bprdIO.getAuthCode())) {
			itemIO.setStatuz(varcom.endp);
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		wsaaT5671Key[wsaaT5671Ix.toInt()].set(itemIO.getItemitem());
		wsaaT5671Subprogs[wsaaT5671Ix.toInt()].set(t5671rec.subprogs);
		itemIO.setFunction(varcom.nextr);
		wsaaT5671Ix.add(1);
	}

protected void loadT57291950()
	{
		start1951();
	}

protected void start1951()
	{
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), bprdIO.getCompany())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5729)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setStatuz(varcom.endp);
			return ;
		}
		t5729rec.t5729Rec.set(itdmIO.getGenarea());
		if (isGT(wsaaT5729Ix, wsaaT5729Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(tablesInner.t5729);
			fatalError600();
		}
		wsaaT5729ArrayInner.wsaaT5729Key[wsaaT5729Ix.toInt()].set(itdmIO.getItemitem());
		wsaaT5729ArrayInner.wsaaT5729Itmfrm[wsaaT5729Ix.toInt()].set(itdmIO.getItmfrm());
		for (wsaaT5729Sub.set(1); !(isGT(wsaaT5729Sub, 6)); wsaaT5729Sub.add(1)){
			wsaaT5729ArrayInner.wsaaT5729Frqcys[wsaaT5729Ix.toInt()].set(t5729rec.frqcys);
		}
		wsaaT5729Sub.set(1);
		for (wsaaT5729Sub.set(1); !(isGT(wsaaT5729Sub, 4)); wsaaT5729Sub.add(1)){
			wsaaT5729ArrayInner.wsaaT5729Durationas[wsaaT5729Ix.toInt()].set(t5729rec.durationas);
			wsaaT5729ArrayInner.wsaaT5729Durationbs[wsaaT5729Ix.toInt()].set(t5729rec.durationbs);
			wsaaT5729ArrayInner.wsaaT5729Durationcs[wsaaT5729Ix.toInt()].set(t5729rec.durationcs);
			wsaaT5729ArrayInner.wsaaT5729Durationds[wsaaT5729Ix.toInt()].set(t5729rec.durationds);
			wsaaT5729ArrayInner.wsaaT5729Durationes[wsaaT5729Ix.toInt()].set(t5729rec.durationes);
			wsaaT5729ArrayInner.wsaaT5729Durationfs[wsaaT5729Ix.toInt()].set(t5729rec.durationfs);
			wsaaT5729ArrayInner.wsaaT5729OverdueMinas[wsaaT5729Ix.toInt()].set(t5729rec.overdueMinas);
			wsaaT5729ArrayInner.wsaaT5729OverdueMinbs[wsaaT5729Ix.toInt()].set(t5729rec.overdueMinbs);
			wsaaT5729ArrayInner.wsaaT5729OverdueMincs[wsaaT5729Ix.toInt()].set(t5729rec.overdueMincs);
			wsaaT5729ArrayInner.wsaaT5729OverdueMinds[wsaaT5729Ix.toInt()].set(t5729rec.overdueMinds);
			wsaaT5729ArrayInner.wsaaT5729OverdueMines[wsaaT5729Ix.toInt()].set(t5729rec.overdueMines);
			wsaaT5729ArrayInner.wsaaT5729OverdueMinfs[wsaaT5729Ix.toInt()].set(t5729rec.overdueMinfs);
			wsaaT5729ArrayInner.wsaaT5729TargetMaxas[wsaaT5729Ix.toInt()].set(t5729rec.targetMaxas);
			wsaaT5729ArrayInner.wsaaT5729TargetMaxbs[wsaaT5729Ix.toInt()].set(t5729rec.targetMaxbs);
			wsaaT5729ArrayInner.wsaaT5729TargetMaxcs[wsaaT5729Ix.toInt()].set(t5729rec.targetMaxcs);
			wsaaT5729ArrayInner.wsaaT5729TargetMaxds[wsaaT5729Ix.toInt()].set(t5729rec.targetMaxds);
			wsaaT5729ArrayInner.wsaaT5729TargetMaxes[wsaaT5729Ix.toInt()].set(t5729rec.targetMaxes);
			wsaaT5729ArrayInner.wsaaT5729TargetMaxfs[wsaaT5729Ix.toInt()].set(t5729rec.targetMaxfs);
			wsaaT5729ArrayInner.wsaaT5729TargetMinas[wsaaT5729Ix.toInt()].set(t5729rec.targetMinas);
			wsaaT5729ArrayInner.wsaaT5729TargetMinbs[wsaaT5729Ix.toInt()].set(t5729rec.targetMinbs);
			wsaaT5729ArrayInner.wsaaT5729TargetMincs[wsaaT5729Ix.toInt()].set(t5729rec.targetMincs);
			wsaaT5729ArrayInner.wsaaT5729TargetMinds[wsaaT5729Ix.toInt()].set(t5729rec.targetMinds);
			wsaaT5729ArrayInner.wsaaT5729TargetMines[wsaaT5729Ix.toInt()].set(t5729rec.targetMines);
			wsaaT5729ArrayInner.wsaaT5729TargetMinfs[wsaaT5729Ix.toInt()].set(t5729rec.targetMinfs);
		}
		wsaaT5729Ix.add(1);
		itdmIO.setFunction(varcom.nextr);
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		/*  Read the records from the temporary FPRM file.*/
		fprxpf.read(fprxpfRec);
		if (fprxpf.isAtEnd()) {
			//ILIFE-1296 STARTS
			if (isNE(wsaaMinTotReqd,ZERO)) {				
				writeMinTotal3200();
			}
			if (isNE(wsaaMaxTotReqd,ZERO)) {				
				writeMaxTotal3500();
			}
			//ILIFE-1296 ENDS
			wsspEdterror.set(varcom.endp);
			/*      IF MIN-REPORT-WRITTEN                                     */
			/*         PERFORM 11600-WRITE-MIN-TOTALS-REPORT                  */
			/*      END-IF                                                    */
			/*      IF MAX-REPORT-WRITTEN                                     */
			/*         PERFORM 11500-WRITE-MAX-TOTALS-REPORT                  */
			/*      END-IF                                                    */
			return ;
		}
		/*  Set up the key for the SYSR- copybook should a system error*/
		/*  for this instalment occur.*/
		wsysChdrcoy.set(fprxpfRec.chdrcoy);
		wsysChdrnum.set(fprxpfRec.chdrnum);
		wsysPayrseqno.set(fprxpfRec.payrseqno);
		wsaaFrepCreated.set("N");
		contotrec.totno.set(controlTotalsInner.ct01);
		contotrec.totval.set(1);
		callContot001();
		/*EXIT*/
	}

protected void edit2500()
	{
		edit2510();
	}

protected void edit2510()
	{
		wsspEdterror.set(varcom.oK);
		/*  Read PAYR record*/
		payrIO.setDataKey(SPACES);
		payrIO.setChdrcoy(fprxpfRec.chdrcoy);
		payrIO.setChdrnum(fprxpfRec.chdrnum);
		payrIO.setPayrseqno(fprxpfRec.payrseqno);
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/* Now we should read the contract header record                   */
		readChdr2100();
		/*  Here we must validate the CHDR details.                        */
		validateContract2200();
		if (!validContract.isTrue()) {
			wsspEdterror.set(SPACES);
			return ;
		}
		/*  Read table TR52D.                                      <V74L01>*/
		b800ReadTr52d();
		softlockContract2300();
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			conlogrec.error.set(sftlockrec.statuz);
			conlogrec.params.set(sftlockrec.sftlockRec);
			callConlog003();
			contotrec.totno.set(controlTotalsInner.ct04);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			return ;
		}
		/*  Read ACBL record*/
		acblIO.setRldgacct(fprxpfRec.chdrnum);
		acblIO.setRldgcoy(fprxpfRec.chdrcoy);
		acblIO.setOrigcurr(payrIO.getBillcurr());
		acblIO.setSacscode(wsaaT5645Sacscode[1]);
		acblIO.setSacstyp(wsaaT5645Sacstype[1]);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(formatsInner.acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(), varcom.oK)
		&& isNE(acblIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			fatalError600();
		}
		/*  Move LP S balance to working storage as a positive balance*/
		if (isEQ(acblIO.getStatuz(), varcom.mrnf)) {
			wsspEdterror.set(SPACES);
			return ;
		}
		else {
			if (isEQ(t3695rec.sign, "-")) {
				compute(wsaaSuspAvail, 2).set(sub(0, acblIO.getSacscurbal()));
			}
			else {
				wsaaSuspAvail.set(acblIO.getSacscurbal());
			}
		}
		if (isGTE(acblIO.getSacscurbal(), 0)) {
			contotrec.totno.set(controlTotalsInner.ct02);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			return ;
		}
		/* Dont increment the control total for the suspense               */
		/* avail until after the contract has been soflocked               */
		/*    MOVE CT03                      TO CONT-TOTNO.                */
		/*    MOVE WSAA-SUSP-AVAIL           TO CONT-TOTVAL.               */
		/*    PERFORM 001-CALL-CONTOT.                                     */
		/*                                                       <LIF2.1>*/
		/*Now we should read the contract header record          <LIF2.1>*/
		/*                                                       <LIF2.1>*/
		/*  PERFORM 2100-READ-CHDR.                              <LIF2.1>*/
		/*                                                       <LIF2.1>*/
		/*Here we must validate the CHDR details.                <LIF2.1>*/
		/*                                                       <LIF2.1>*/
		/*  PERFORM 2200-VALIDATE-CONTRACT.                      <LIF2.1>*/
		/*                                                       <LIF2.1>*/
		/*  IF NOT VALID-CONTRACT                                <LIF2.1>*/
		/*     MOVE SPACES              TO WSSP-EDTERROR         <LIF2.1>*/
		/*     GO TO 2590-EXIT                                   <LIF2.1>*/
		/*  END-IF.                                              <LIF2.1>*/
		/*                                                       <LIF2.1>*/
		/*  PERFORM 2300-SOFTLOCK-CONTRACT.                      <LIF2.1>*/
		/*                                                       <LIF2.1>*/
		/*  IF SFTL-STATUZ        = 'LOCK'                       <LIF2.1>*/
		/*                                                       <LIF2.1>*/
		/*     MOVE SFTL-STATUZ         TO CONL-ERROR            <LIF2.1>*/
		/*     MOVE SFTL-SFTLOCK-REC    TO CONL-PARAMS           <LIF2.1>*/
		/*     PERFORM 003-CALL-CONLOG                           <LIF2.1>*/
		/*                                                       <LIF2.1>*/
		/*     MOVE CT04                TO CONT-TOTNO            <LIF2.1>*/
		/*     MOVE 1                   TO CONT-TOTVAL           <LIF2.1>*/
		/*     PERFORM 001-CALL-CONTOT                           <LIF2.1>*/
		/*                                                       <LIF2.1>*/
		/*     MOVE SPACES              TO WSSP-EDTERROR         <LIF2.1>*/
		/*     GO TO 2590-EXIT                                   <LIF2.1>*/
		/*  END-IF.                                              <LIF2.1>*/
		/* Increment CT03 now - the record has been checked                */
		/* to see if it is softlocked                                      */
		contotrec.totno.set(controlTotalsInner.ct03);
		contotrec.totval.set(wsaaSuspAvail);
		callContot001();
		readClrf2400();
		wsaaBilled.set(fprxpfRec.totalBilled);
		/*  Search table T3620 for the billing channel indicator for the*/
		/*  contract to establish whether this is a Direct Debit case or no*/
		wsaaT3620Ix.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaT3620Ix, wsaaT3620Rec.length); wsaaT3620Ix.add(1)){
				if (isEQ(wsaaT3620Key[wsaaT3620Ix.toInt()], payrIO.getBillchnl())) {
					break searchlabel1;
				}
			}
			syserrrec.statuz.set(i086);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(tablesInner.t3620);
			stringVariable1.addExpression(wsysSystemErrorParams);
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();
		}
		/*  Check to see if the DDIND on table T3620 is not spaces*/
		/*  for the billing channel on the PAYR record*/
		wsaaOutstandingDdde.set("N");
		/*  If the DDIND is not spaces the DDDE record should be read*/
		if (isNE(wsaaT3620Data[wsaaT3620Ix.toInt()], SPACES)) {
			readDdde2450();
		}
		if (isLTE(wsaaSuspAvail, 0)) {
			contotrec.totno.set(controlTotalsInner.ct05);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			return ;
		}
		/*  Check the premium variances on table T5729*/
		/*  We must search table T5729 to find the record which matches*/
		/*  the product type of the contract.*/
		wsaaT5729Ix.set(1);
		 searchlabel2:
		{
			for (; isLT(wsaaT5729Ix, wsaaT5729ArrayInner.wsaaT5729Rec.length); wsaaT5729Ix.add(1)){
				if (isEQ(wsaaT5729ArrayInner.wsaaT5729Key[wsaaT5729Ix.toInt()], chdrlifIO.getCnttype())) {
					break searchlabel2;
				}
			}
			syserrrec.statuz.set(i086);
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(tablesInner.t5729);
			stringVariable2.addExpression(wsysSystemErrorParams);
			stringVariable2.setStringInto(syserrrec.params);
			fatalError600();
		}
		/*  We must find the effective T5729 entry for the contract*/
		/*  (T5729 entries will have been loaded in descending sequence*/
		/*  into the array).*/
		wsaaDateFound.set("N");
		while ( !(isNE(chdrlifIO.getCnttype(), wsaaT5729ArrayInner.wsaaT5729Key[wsaaT5729Ix.toInt()])
		|| isGT(wsaaT5729Ix, wsaaT5729Size)
		|| dateFound.isTrue())) {
			if (isGTE(chdrlifIO.getOccdate(), wsaaT5729ArrayInner.wsaaT5729Itmfrm[wsaaT5729Ix.toInt()])) {
				wsaaDateFound.set("Y");
			}
			else {
				wsaaT5729Ix.add(1);
			}
		}

		if (dateNotFound.isTrue()) {
			syserrrec.statuz.set(i086);
			syserrrec.params.set(wsysSystemErrorParams);
			fatalError600();
		}
		/*  Load the values from table T5729 which match the frequency*/
		/*  on the PAYR record*/
		wsaaFreqFound.set("N");
		wsaaT5729Sub.set(1);
		for (wsaaT5729Sub.set(1); !(isGT(wsaaT5729Sub, 6)
		|| freqFound.isTrue()); wsaaT5729Sub.add(1)){
			if (isEQ(wsaaT5729ArrayInner.wsaaT5729Frqcy[wsaaT5729Ix.toInt()][wsaaT5729Sub.toInt()], payrIO.getBillfreq())) {
				wsaaTest.set(wsaaT5729Sub);
				wsaaFreqFound.set("Y");
			}
		}
		if (isGT(wsaaT5729Sub, 6)) {
			syserrrec.statuz.set(i086);
			syserrrec.params.set(wsysSystemErrorParams);
			fatalError600();
		}
		/* Frequency found *************/
		if (isEQ(wsaaTest, 1)){
			wsaaDurations.set(wsaaT5729ArrayInner.wsaaT5729Durationas[wsaaT5729Ix.toInt()]);
			wsaaTargetMaxs.set(wsaaT5729ArrayInner.wsaaT5729TargetMaxas[wsaaT5729Ix.toInt()]);
			wsaaTargetMins.set(wsaaT5729ArrayInner.wsaaT5729TargetMinas[wsaaT5729Ix.toInt()]);
		}
		else if (isEQ(wsaaTest, 2)){
			wsaaDurations.set(wsaaT5729ArrayInner.wsaaT5729Durationbs[wsaaT5729Ix.toInt()]);
			wsaaTargetMaxs.set(wsaaT5729ArrayInner.wsaaT5729TargetMaxbs[wsaaT5729Ix.toInt()]);
			wsaaTargetMins.set(wsaaT5729ArrayInner.wsaaT5729TargetMinbs[wsaaT5729Ix.toInt()]);
		}
		else if (isEQ(wsaaTest, 3)){
			wsaaDurations.set(wsaaT5729ArrayInner.wsaaT5729Durationcs[wsaaT5729Ix.toInt()]);
			wsaaTargetMaxs.set(wsaaT5729ArrayInner.wsaaT5729TargetMaxcs[wsaaT5729Ix.toInt()]);
			wsaaTargetMins.set(wsaaT5729ArrayInner.wsaaT5729TargetMincs[wsaaT5729Ix.toInt()]);
		}
		else if (isEQ(wsaaTest, 4)){
			wsaaDurations.set(wsaaT5729ArrayInner.wsaaT5729Durationds[wsaaT5729Ix.toInt()]);
			wsaaTargetMaxs.set(wsaaT5729ArrayInner.wsaaT5729TargetMaxds[wsaaT5729Ix.toInt()]);
			wsaaTargetMins.set(wsaaT5729ArrayInner.wsaaT5729TargetMinds[wsaaT5729Ix.toInt()]);
		}
		else if (isEQ(wsaaTest, 5)){
			wsaaDurations.set(wsaaT5729ArrayInner.wsaaT5729Durationes[wsaaT5729Ix.toInt()]);
			wsaaTargetMaxs.set(wsaaT5729ArrayInner.wsaaT5729TargetMaxes[wsaaT5729Ix.toInt()]);
			wsaaTargetMins.set(wsaaT5729ArrayInner.wsaaT5729TargetMines[wsaaT5729Ix.toInt()]);
		}
		else if (isEQ(wsaaTest, 6)){
			wsaaDurations.set(wsaaT5729ArrayInner.wsaaT5729Durationfs[wsaaT5729Ix.toInt()]);
			wsaaTargetMaxs.set(wsaaT5729ArrayInner.wsaaT5729TargetMaxfs[wsaaT5729Ix.toInt()]);
			wsaaTargetMins.set(wsaaT5729ArrayInner.wsaaT5729TargetMinfs[wsaaT5729Ix.toInt()]);
		}
		/*  Determine the current variances from T5729 based on the*/
		/*  difference between the Occdate and the effective date of this*/
		/*  job.*/
		/*  Use the DATCON3 subroutine to calculate the freq factor*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(chdrlifIO.getOccdate());
		datcon3rec.intDate2.set(bsscIO.getEffectiveDate());
		datcon3rec.frequency.set(payrIO.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		wsaaNumPeriod.set(datcon3rec.freqFactor);
		wsaaSub2.set(0);
		durationNotFound.setTrue();
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)
		|| isEQ(wsaaDuration[wsaaSub.toInt()], SPACES)
		|| durationFound.isTrue()); wsaaSub.add(1)){
			if (isLT(wsaaNumPeriod, wsaaDuration[wsaaSub.toInt()])) {
				wsaaSub2.set(wsaaSub);
				durationFound.setTrue();
			}
		}
		if (durationNotFound.isTrue()) {
			syserrrec.statuz.set(i086);
			syserrrec.params.set(wsysSystemErrorParams);
			fatalError600();
		}
		/*  Calculate tax                                                  */
		wsaaVariablesInner.wsaaTotTaxCharged.set(ZERO);
		wsaaVariablesInner.wsaaPremNet.set(ZERO);
		wsaaVariablesInner.wsaaOrigSusp.set(ZERO);
		wsaaVariablesInner.wsaaTaxPercent.set(ZERO);
		wsaaVariablesInner.wsaaDiff.set(ZERO);
		wsaaVariablesInner.wsaaTaxProp.set(ZERO);
		calcTax2800();
		wsaaVariablesInner.wsaaOrigSusp.set(wsaaSuspAvail);
		wsaaVariablesInner.wsaaPremNet.set(wsaaSuspAvail);
		if (isGT(wsaaVariablesInner.wsaaTotTaxCharged, 0)) {
			compute(wsaaVariablesInner.wsaaTaxPercent, 3).setRounded(add((div(wsaaVariablesInner.wsaaTotTaxCharged, payrIO.getSinstamt01())), 1));
			compute(wsaaVariablesInner.wsaaPremNet, 3).setRounded(div(wsaaSuspAvail, wsaaVariablesInner.wsaaTaxPercent));
			compute(wsaaVariablesInner.wsaaTaxProp, 3).setRounded(sub(wsaaSuspAvail, wsaaVariablesInner.wsaaPremNet));
			compute(wsaaSuspAvail, 3).setRounded(sub(wsaaSuspAvail, wsaaVariablesInner.wsaaTaxProp));
		}
		/*  If the total amount billed is greater than the total amount*/
		/*  received the suspense balance should be reduced by the*/
		/*  difference between the total billed and the total received*/
		/*  to allow for payment of outstanding premiums before the*/
		/*  maximum variances are checked*/
		if (isGT(wsaaBilled, fprxpfRec.totalRecd)) {
			compute(wsaaDifference, 2).set(sub(wsaaBilled, fprxpfRec.totalRecd));
		}
		else {
			wsaaDifference.set(0);
		}
		wsaaMaxFail.set("N");
		if (isGT(wsaaSuspAvail, wsaaDifference)) {
			compute(wsaaAfterPremPaid, 2).set(sub(wsaaSuspAvail, wsaaDifference));
			maximumTest2600();
		}
		//ILIFE-1296 STARTS
		if (failMaximumTest.isTrue()) {
			printMaxVariance3020();
		} 			
		//ILIFE-1296 ENDS
		if (isLTE(wsaaSuspAvail, 0)) {
			wsspEdterror.set(SPACES);
			return ;
		}
		wsaaMaxFail.set("N");
		minimumTest2700();
		//ILIFE-1296 STARTS
		if (failMinimumTest.isTrue()) {
			printMinVariance3010();
		}
		//ILIFE-1296 ENDS
		if (failMinimumTest.isTrue()) {
			wsspEdterror.set(SPACES);
			return ;
		}
	}

protected void readChdr2100()
	{
		/*START*/
		chdrlifIO.setChdrcoy(fprxpfRec.chdrcoy);
		chdrlifIO.setChdrnum(fprxpfRec.chdrnum);
		chdrlifIO.setFunction(varcom.readh);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void validateContract2200()
	{
		start2210();
	}

protected void start2210()
	{
		/*  For each contract retrieved we must look up the contract type*/
		/*  details on T5688*/
		wsaaT5688Ix.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaT5688Ix, wsaaT5688Rec.length); wsaaT5688Ix.add(1)){
				if (isEQ(wsaaT5688Key[wsaaT5688Ix.toInt()], chdrlifIO.getCnttype())) {
					break searchlabel1;
				}
			}
			syserrrec.statuz.set(e308);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(tablesInner.t5688);
			stringVariable1.addExpression(wsysSystemErrorParams);
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();
		}
		/*  We must find the effective T5688 entry for the contract*/
		/*  (T5688 entries will have been loaded in descending sequence*/
		/*  into the array).*/
		while ( !(isNE(chdrlifIO.getCnttype(), wsaaT5688Cnttype[wsaaT5688Ix.toInt()])
		|| isGT(wsaaT5688Ix, wsaaT5688Size)
		|| dateFound.isTrue())) {
			if (isGTE(chdrlifIO.getOccdate(), wsaaT5688Itmfrm[wsaaT5688Ix.toInt()])) {
				wsaaDateFound.set("Y");
			}
			else {
				wsaaT5688Ix.add(1);
			}
		}

		if (!dateFound.isTrue()) {
			syserrrec.statuz.set(h979);
			syserrrec.params.set(wsysSystemErrorParams);
			fatalError600();
		}
		/*  Validate the statii of the contract*/
		wsaaValidContract.set("N");
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Sub.toInt()], chdrlifIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.cnPremStat[wsaaT5679Sub.toInt()], chdrlifIO.getPstatcode())) {
						wsaaT5679Sub.set(13);
						wsaaValidContract.set("Y");
					}
				}
			}
		}
	}

protected void softlockContract2300()
	{
		start2310();
	}

protected void start2310()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.enttyp.set("CH");
		sftlockrec.company.set(bsprIO.getCompany());
		sftlockrec.user.set(99999);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.function.set("LOCK");
		sftlockrec.entity.set(fprxpfRec.chdrnum);
		sftlockrec.statuz.set(varcom.oK);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			wsysSysparams.set(sftlockrec.sftlockRec);
			syserrrec.params.set(wsysSystemErrorParams);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void readClrf2400()
	{
		start2410();
	}

protected void start2410()
	{
		clrfIO.setForepfx(chdrlifIO.getChdrpfx());
		clrfIO.setForecoy(fprxpfRec.chdrcoy);
		wsaaChdrnum.set(fprxpfRec.chdrnum);
		wsaaPayrseqno.set(fprxpfRec.payrseqno);
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
		wsaaPayrnum.set(clrfIO.getClntnum());
	}

protected void readDdde2450()
	{
		start2451();
	}

protected void start2451()
	{
		/*  Set up key to read any DDDE records for the contract where*/
		/*  the billing date is greater than the effective date*/
		dddelf2IO.setChdrcoy(chdrlifIO.getChdrcoy());
		dddelf2IO.setChdrnum(chdrlifIO.getChdrnum());
		dddelf2IO.setPayrnum(wsaaPayrnum);
		dddelf2IO.setBilldate(bsscIO.getEffectiveDate());
		/*  Start reading DDDELF2 by using BEGN*/
		dddelf2IO.setFunction(varcom.begn);
		dddelf2IO.setFormat(formatsInner.dddelf2rec);
		SmartFileCode.execute(appVars, dddelf2IO);
		if ((isNE(dddelf2IO.getStatuz(), varcom.oK))
		&& (isNE(dddelf2IO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(dddelf2IO.getParams());
			fatalError600();
		}
		/* If the status is endp or the key has changed we should go*/
		/* to exit. If the record matches the key we should compare*/
		/* the amount remaining in suspense with the instalment amount.*/
		/* If the amount in suspense is greater than the instalment amount*/
		/* we subtract the outstanding instalment from suspense and from*/
		/* the total billed.*/
		while ( !(isEQ(dddelf2IO.getStatuz(), varcom.endp))) {
			if (isEQ(dddelf2IO.getStatuz(), varcom.endp)
			|| (isNE(dddelf2IO.getChdrcoy(), chdrlifIO.getChdrcoy()))
			|| (isNE(dddelf2IO.getChdrnum(), chdrlifIO.getChdrnum()))
			|| (isNE(dddelf2IO.getPayrnum(), wsaaPayrnum))) {
				dddelf2IO.setStatuz(varcom.endp);
				return ;
			}
			if (isGT(dddelf2IO.getBilldate(), bsscIO.getEffectiveDate())) {
				if (isGTE(wsaaSuspAvail, dddelf2IO.getInstamt06())) {
					compute(wsaaSuspAvail, 2).set(sub(wsaaSuspAvail, dddelf2IO.getInstamt06()));
					compute(wsaaBilled, 2).set(sub(wsaaBilled, dddelf2IO.getInstamt01()));
					outstandingDdde.setTrue();
					contotrec.totno.set(controlTotalsInner.ct07);
					contotrec.totval.set(1);
					callContot001();
					contotrec.totno.set(controlTotalsInner.ct08);
					contotrec.totval.set(dddelf2IO.getInstamt06());
					callContot001();
				}
			}
			dddelf2IO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, dddelf2IO);
		}

	}

protected void maximumTest2600()
	{
		start2610();
	}

protected void start2610()
	{
		/*  Check premium to be allocated is not greater than the maximum*/
		/*  allowed by table T5729*/
		wsaaMaxFail.set("N");
		compute(wsaaMaxAllowed, 3).setRounded(mult(payrIO.getSinstamt06(), (div(wsaaTargetMax[wsaaSub2.toInt()], 100))));
		/* MOVE WSAA-MAX-ALLOWED       TO ZRDP-AMOUNT-IN.               */
		/* PERFORM A000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO WSAA-MAX-ALLOWED.             */
		if (isNE(wsaaMaxAllowed, 0)) {
			zrdecplrec.amountIn.set(wsaaMaxAllowed);
			zrdecplrec.currency.set(payrIO.getCntcurr());
			a000CallRounding();
			wsaaMaxAllowed.set(zrdecplrec.amountOut);
		}
		if (isGT(wsaaAfterPremPaid, wsaaMaxAllowed)) {
			failMaximumTest.setTrue();
			compute(wsaaSuspAvail, 2).set(sub(wsaaSuspAvail, wsaaAfterPremPaid));
			/*      PERFORM 11000-WRITE-VARIANCE-REPORT                       */
			writeFrepRecord12000();
		}
	}

protected void minimumTest2700()
	{
		/*START*/
		wsaaMinFail.set("N");
		compute(wsaaMinAllowed, 3).setRounded(mult(payrIO.getSinstamt06(), (div(wsaaTargetMin[wsaaSub2.toInt()], 100))));
		/* MOVE WSAA-MIN-ALLOWED       TO ZRDP-AMOUNT-IN.               */
		/* PERFORM A000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO WSAA-MIN-ALLOWED.             */
		if (isNE(wsaaMinAllowed, 0)) {
			zrdecplrec.amountIn.set(wsaaMinAllowed);
			zrdecplrec.currency.set(payrIO.getCntcurr());
			a000CallRounding();
			wsaaMinAllowed.set(zrdecplrec.amountOut);
		}
		if (isLT(wsaaSuspAvail, wsaaMinAllowed)) {
			failMinimumTest.setTrue();
			/*      PERFORM 11000-WRITE-VARIANCE-REPORT                       */
			writeFrepRecord12000();
		}
		/*EXIT*/
	}

protected void calcTax2800()
	{
		start2810();
	}

protected void start2810()
	{
		wsaaVariablesInner.wsaaIx.set(ZERO);
		if (isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}
		/* Read COVRLNB                                                    */
		covrlnbIO.setStatuz(varcom.oK);
		covrlnbIO.setDataKey(SPACES);
		covrlnbIO.setChdrcoy(fprxpfRec.chdrcoy);
		covrlnbIO.setChdrnum(fprxpfRec.chdrnum);
		covrlnbIO.setPlanSuffix(ZERO);
		covrlnbIO.setFormat(formatsInner.covrlnbrec);
		covrlnbIO.setFunction(varcom.begn);
		while ( !(isEQ(covrlnbIO.getStatuz(), varcom.endp))) {
			validateCovr2900();
		}

	}

protected void validateCovr2900()
	{
		start2910();
	}

protected void start2910()
	{
		wsaaValidCovr.set("N");
		SmartFileCode.execute(appVars, covrlnbIO);
		if ((isNE(covrlnbIO.getStatuz(), varcom.oK))
		&& (isNE(covrlnbIO.getStatuz(), varcom.endp))) {
			syserrrec.statuz.set(covrlnbIO.getStatuz());
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError600();
		}
		if ((isNE(covrlnbIO.getChdrcoy(), fprxpfRec.chdrcoy))
		|| (isNE(covrlnbIO.getChdrnum(), fprxpfRec.chdrnum))
		|| isEQ(covrlnbIO.getStatuz(), varcom.endp)) {
			covrlnbIO.setStatuz(varcom.endp);
			return ;
		}
		/* Validate conditions for COVR                                    */
		/* CURRTO > BSSC-EFFECTIVE-DATE                                    */
		if (isLTE(covrlnbIO.getCurrto(), bsscIO.getEffectiveDate())) {
			covrlnbIO.setFunction(varcom.nextr);
			return ;
		}
		/* Validflag should be '1'                                         */
		if (isNE(covrlnbIO.getValidflag(), "1")) {
			covrlnbIO.setFunction(varcom.nextr);
			return ;
		}
		/* Statcode and Pstatcode exists in T5679 item                     */
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covrlnbIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], covrlnbIO.getPstatcode())) {
						wsaaT5679Sub.set(13);
						wsaaValidCovr.set("Y");
					}
				}
			}
		}
		if (!validCovr.isTrue()) {
			covrlnbIO.setFunction(varcom.nextr);
			return ;
		}
		/* Installment premium not = 0                                     */
		if (isEQ(covrlnbIO.getInstprem(), 0)) {
			covrlnbIO.setFunction(varcom.nextr);
			return ;
		}
		wsaaCrtable.set(covrlnbIO.getCrtable());
		/* Read table TR52E.                                               */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlifIO.getCnttype());
		wsaaTr52eCrtable.set(covrlnbIO.getCrtable());
		b900ReadTr52e();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlifIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			b900ReadTr52e();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			b900ReadTr52e();
		}
		if (isNE(tr52erec.taxind01, "Y")) {
			covrlnbIO.setFunction(varcom.nextr);
			return ;
		}
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.transType.set("PREM");
		txcalcrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlifIO.getChdrnum());
		txcalcrec.life.set(covrlnbIO.getLife());
		txcalcrec.coverage.set(covrlnbIO.getCoverage());
		txcalcrec.rider.set(covrlnbIO.getRider());
		txcalcrec.planSuffix.set(covrlnbIO.getPlanSuffix());
		txcalcrec.crtable.set(covrlnbIO.getCrtable());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		wsaaCntCurr.set(chdrlifIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.effdate.set(bsscIO.getEffectiveDate());
		if (isEQ(tr52erec.zbastyp, "Y")) {
			txcalcrec.amountIn.set(covrlnbIO.getZbinstprem());
		}
		else {
			txcalcrec.amountIn.set(covrlnbIO.getInstprem());
		}
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		/* Move variables to working storage array.                        */
		wsaaVariablesInner.wsaaIx.add(1);
		wsaaVariablesInner.wsaaLife[wsaaVariablesInner.wsaaIx.toInt()].set(txcalcrec.life);
		wsaaVariablesInner.wsaaCoverage[wsaaVariablesInner.wsaaIx.toInt()].set(txcalcrec.coverage);
		wsaaVariablesInner.wsaaRider[wsaaVariablesInner.wsaaIx.toInt()].set(txcalcrec.rider);
		wsaaVariablesInner.wsaaPlanSuffix[wsaaVariablesInner.wsaaIx.toInt()].set(txcalcrec.planSuffix);
		wsaaVariablesInner.wsaaTaxRule[wsaaVariablesInner.wsaaIx.toInt()].set(txcalcrec.taxrule);
		wsaaVariablesInner.wsaaTaxItem[wsaaVariablesInner.wsaaIx.toInt()].set(txcalcrec.rateItem);
		wsaaVariablesInner.wsaaTaxType[wsaaVariablesInner.wsaaIx.toInt()][1].set(txcalcrec.taxType[1]);
		wsaaVariablesInner.wsaaTaxType[wsaaVariablesInner.wsaaIx.toInt()][2].set(txcalcrec.taxType[2]);
		wsaaVariablesInner.wsaaTaxAbs[wsaaVariablesInner.wsaaIx.toInt()][1].set(txcalcrec.taxAbsorb[1]);
		wsaaVariablesInner.wsaaTaxAbs[wsaaVariablesInner.wsaaIx.toInt()][2].set(txcalcrec.taxAbsorb[2]);
		if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaVariablesInner.wsaaTotTaxCharged.add(txcalcrec.taxAmt[1]);
			wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][1].set(txcalcrec.taxAmt[1]);
		}
		else {
			wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][1].set(txcalcrec.taxAmt[1]);
		}
		if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaVariablesInner.wsaaTotTaxCharged.add(txcalcrec.taxAmt[2]);
			wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][2].set(txcalcrec.taxAmt[2]);
		}
		else {
			wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][2].set(txcalcrec.taxAmt[2]);
		}
		/* Read next component                                             */
		covrlnbIO.setFunction(varcom.nextr);
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		/*  Read and hold the FPRM record for updating later*/
		fprmIO.setChdrcoy(fprxpfRec.chdrcoy);
		fprmIO.setChdrnum(fprxpfRec.chdrnum);
		fprmIO.setPayrseqno(fprxpfRec.payrseqno);
		fprmIO.setFormat(formatsInner.fprmrec);
		fprmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fprmIO.getParams());
			fatalError600();
		}
		checkOverdueprocess();
		writeLins3100();
		updateChdrFprmPayr3200();
		updateSuspense3300();
		individualLedger3400();
		covrsAcmvsPtrn3500();
		writePtrn3800();
		unlkChdr3900();
	}
//ILIFE-1296 STARTS
protected void printMinVariance3010()
{
	if (isGT(wsaaLinecnt5358, wsaaMaxLines)) {
		wsaaPageChange5358 = "Y";
	}
	if (isEQ(wsaaPageChange5358, "Y")) {
		writeMinHeader3100();
	}
	writeMinDetail3300();	
}

protected void writeMinHeader3100()
{
	wsaaPageChange5358 = "N";
	wsaaLinecnt5358.set(12);
	r5358h01Company.set(bsprIO.getCompany());
	getCompanyName3110();
	r5358h01Companynm.set(descIO.getLongdesc());
	datcon1rec.datcon1Rec.set(SPACES);
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	wsaaVariablesInner.wsaaDatex.set(datcon1rec.extDate);
	callProgram(Gettim.class, wsaaInTime);
	wsaaVariablesInner.wsaaTime.set(wsaaInTime);
	wsaaVariablesInner.wsaaTimex.set(inspectReplaceAll(wsaaVariablesInner.wsaaTimex, "/", ":"));
	r5358h01Sdate.set(wsaaVariablesInner.wsaaDatex);
	r5358h01Stime.set(wsaaVariablesInner.wsaaTimex);
	minVarReport.printR5358h01(r5358h01Record, indicArea);
	//wsaaOverflow.set("N");
	/*EXIT*/
}
protected void getCompanyName3110()
{
	descIO.setDescpfx("IT");
	descIO.setDesccoy("0");
	descIO.setDesctabl(t1693);
	descIO.setDescitem(bsprIO.getCompany());
	descIO.setLanguage(bsscIO.getLanguage());
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(),varcom.oK)) {
		descIO.setLongdesc(SPACES);
	}
}
protected void writeMinTotal3200()
{
	r5358h01TotReceived.set(wsaaMinTotReqd);
	wsaaMinTotReqd.set(ZERO);
	minVarReport.printR5358t01(r5358t01Record, indicArea);
	/*EXIT*/
}

protected void writeMinDetail3300()
{
	start3300();
}

protected void start3300()
{
	wsaaMinTotReqd.add(r5358printRecordInner.rd01SusAmt);	
	minVarReport.printR5358d01(r5358printRecordInner.r5358d01Record, indicArea);
	wsaaLinecnt5358.add(2);
	
}

protected void printMaxVariance3020()
{
	if (isGT(wsaaLinecnt5359, wsaaMaxLines)) {
		wsaaPageChange5359 = "Y";
	}
	if (isEQ(wsaaPageChange5359, "Y")) {
		writeMaxHeader3400();
	}
	writeMaxDetail3600();
	
}

protected void writeMaxHeader3400()
{
	wsaaPageChange5359 = "N";
	wsaaLinecnt5359.set(12);
	r5359h01Company.set(bsprIO.getCompany());
	getCompanyName3110();
	r5359h01Companynm.set(descIO.getLongdesc());
	datcon1rec.datcon1Rec.set(SPACES);
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	wsaaVariablesInner.wsaaDatex.set(datcon1rec.extDate);
	callProgram(Gettim.class, wsaaInTime);
	wsaaVariablesInner.wsaaTime.set(wsaaInTime);
	wsaaVariablesInner.wsaaTimex.set(inspectReplaceAll(wsaaVariablesInner.wsaaTimex, "/", ":"));
	r5359h01Sdate.set(wsaaVariablesInner.wsaaDatex);
	r5359h01Stime.set(wsaaVariablesInner.wsaaTimex);
	maxVarReport.printR5359h01(r5359h01Record, indicArea);	
	/*EXIT*/
}

protected void writeMaxTotal3500()
{
	r5359h01TotReceived.set(wsaaMaxTotReqd);
	wsaaMaxTotReqd.set(ZERO);	
	maxVarReport.printR5359t01(r5359t01Record, indicArea);
	/*EXIT*/
}

protected void writeMaxDetail3600()
{
	start3600();
}

protected void start3600()
{
	wsaaMaxTotReqd.add(r5359printRecordInner.rd01SusAmt);
	maxVarReport.printR5359d01(r5359printRecordInner.r5359d01Record, indicArea);
	wsaaLinecnt5359.add(2);
	
}
//ILIFE-1296 ENDS 
protected void writeLins3100()
	{
		start3110();
	}

protected void start3110()
	{
		/*  A LINS record is created for the assigned amount with a status*/
		/*  of paid to complete the audit trail and ensure reversals can*/
		/*  function correctly*/
		linsrnlIO.setInstjctl(SPACES);
		linsrnlIO.setDueflg(SPACES);
		linsrnlIO.setInstamt02(ZERO);
		linsrnlIO.setInstamt03(ZERO);
		linsrnlIO.setInstamt04(ZERO);
		linsrnlIO.setInstamt05(ZERO);
		linsrnlIO.setInstamt06(wsaaSuspAvail);
		linsrnlIO.setInstamt01(wsaaSuspAvail);
		linsrnlIO.setCbillamt(wsaaSuspAvail);
		setPrecision(linsrnlIO.getInstamt06(), 2);
		linsrnlIO.setInstamt06(add(linsrnlIO.getInstamt06(), wsaaVariablesInner.wsaaTaxProp));
		setPrecision(linsrnlIO.getCbillamt(), 2);
		linsrnlIO.setCbillamt(add(linsrnlIO.getCbillamt(), wsaaVariablesInner.wsaaTaxProp));
		linsrnlIO.setChdrnum(fprxpfRec.chdrnum);
		linsrnlIO.setChdrcoy(fprxpfRec.chdrcoy);
		linsrnlIO.setBillchnl(payrIO.getBillchnl());
		linsrnlIO.setPayrseqno(fprxpfRec.payrseqno);
		linsrnlIO.setBranch(batcdorrec.branch);
		linsrnlIO.setTranscode(bprdIO.getAuthCode());
		linsrnlIO.setInstfreq(payrIO.getBillfreq());
		linsrnlIO.setCntcurr(payrIO.getCntcurr());
		linsrnlIO.setBillcurr(payrIO.getBillcurr());
		linsrnlIO.setInstto(payrIO.getBtdate());
		linsrnlIO.setMandref(payrIO.getMandref());
		linsrnlIO.setInstfrom(payrIO.getPtdate());
		linsrnlIO.setBillcd(bsscIO.getEffectiveDate());
		linsrnlIO.setAcctmeth(chdrlifIO.getAcctmeth());
		linsrnlIO.setPayflag("P");
		linsrnlIO.setValidflag("1");
		linsrnlIO.setFunction(varcom.writr);
		linsrnlIO.setFormat(formatsInner.linsrnlrec);
		SmartFileCode.execute(appVars, linsrnlIO);
		if (isNE(linsrnlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(linsrnlIO.getParams());
			fatalError600();
		}
		/*  Log number of LINS written*/
		contotrec.totno.set(controlTotalsInner.ct13);
		contotrec.totval.set(1);
		callContot001();
	}

protected void updateChdrFprmPayr3200()
	{
		start3210();
	}

protected void start3210()
	{
		chdrlifIO.setInstjctl(SPACES);
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(add(chdrlifIO.getTranno(), 1));
		if (noOutstandingDdde.isTrue()) {
			chdrlifIO.setPtdate(chdrlifIO.getBtdate());
		}
		chdrlifIO.setFunction(varcom.rewrt);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		setPrecision(fprmIO.getTotalRecd(), 2);
		fprmIO.setTotalRecd(add(fprmIO.getTotalRecd(), wsaaSuspAvail));
		fprmIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fprmIO.getParams());
			fatalError600();
		}
		payrIO.setTranno(chdrlifIO.getTranno());
		if (noOutstandingDdde.isTrue()) {
			payrIO.setPtdate(payrIO.getBtdate());
		}
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
	}

protected void updateSuspense3300()
	{
		start3310();
	}

protected void start3310()
	{
		lifrtrnrec.origamt.set(wsaaSuspAvail);
		lifrtrnrec.origamt.add(wsaaVariablesInner.wsaaTaxProp);
		lifrtrnrec.rdocnum.set(fprxpfRec.chdrnum);
		lifrtrnrec.tranref.set(fprxpfRec.chdrnum);
		lifrtrnrec.jrnseq.set(0);
		lifrtrnrec.contot.set(wsaaT5645Cnttot[1]);
		lifrtrnrec.sacscode.set(wsaaT5645Sacscode[1]);
		lifrtrnrec.sacstyp.set(wsaaT5645Sacstype[1]);
		lifrtrnrec.glsign.set(wsaaT5645Sign[1]);
		lifrtrnrec.glcode.set(wsaaT5645Glmap[1]);
		lifrtrnrec.tranno.set(chdrlifIO.getTranno());
		lifrtrnrec.origcurr.set(payrIO.getBillcurr());
		lifrtrnrec.effdate.set(bsscIO.getEffectiveDate());
		lifrtrnrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifrtrnrec.rldgacct.set(fprxpfRec.chdrnum);
		lifrtrnrec.function.set("PSTW");
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, varcom.oK)) {
			wsysSysparams.set(lifrtrnrec.lifrtrnRec);
			syserrrec.params.set(wsysSystemErrorParams);
			syserrrec.statuz.set(lifrtrnrec.statuz);
			fatalError600();
		}
		if (isEQ(chdrlifIO.getCntcurr(), chdrlifIO.getBillcurr())) {
			return ;
		}
	}

protected void individualLedger3400()
	{
		/*START*/
		lifacmvrec.effdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.tranref.set(fprxpfRec.chdrnum);
		lifacmvrec.rldgacct.set(fprxpfRec.chdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.origcurr.set(payrIO.getCntcurr());
		/*  Post the Premium-Income.*/
		if (isGT(wsaaSuspAvail, 0)
		&& (isNE(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")
		&& isNE(wsaaT5688Revacc[wsaaT5688Ix.toInt()], "Y"))) {
			lifacmvrec.origamt.set(wsaaSuspAvail);
			wsaaGlSub.set(3);
			callLifacmv6000();
		}
		/*EXIT*/
	}

protected void covrsAcmvsPtrn3500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start3501();
				case next3515:
					next3515();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3501()
	{
		initialize(wsaaTotAllocated);
		initialize(wsaaTotamt);
		initialize(wsaaPolamnt);
		if (isNE(th605rec.bonusInd, "Y")) {
			goTo(GotoLabel.next3515);
		}
		/* Initialize the AGCM arrays                                      */
		b100InitializeArrays();
		wsaaBillfq.set(payrIO.getBillfreq());
		wsaaAgcmIx.set(0);
	}

protected void next3515()
	{
		/*  Set up key to read COVR records for contract.*/
		covrlnbIO.setStatuz(varcom.oK);
		covrlnbIO.setDataKey(SPACES);
		covrlnbIO.setPlanSuffix(0);
		covrlnbIO.setChdrnum(fprxpfRec.chdrnum);
		covrlnbIO.setChdrcoy(fprxpfRec.chdrcoy);
		covrlnbIO.setFormat(formatsInner.covrlnbrec);
		covrlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrlnbIO);
		if ((isNE(covrlnbIO.getStatuz(), varcom.oK))
		&& (isNE(covrlnbIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError600();
		}
		if ((isNE(covrlnbIO.getChdrcoy(), fprxpfRec.chdrcoy))
		|| (isNE(covrlnbIO.getChdrnum(), fprxpfRec.chdrnum))
		|| isEQ(covrlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError600();
		}
		
		//ILIFE-1055 - adding contract fees to amount
		wsaaTotamt.add(payrIO.getSinstamt02());
		while ( !(isEQ(covrlnbIO.getStatuz(), varcom.endp))) {
			/* Validate suspense available against total allocated*/
			processCovrs3510();
		}
		
		if (isNE(wsaaSuspAvail, wsaaTotAllocated)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(wsaaSuspRed);
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(wsaaTotRed);
			stringVariable1.setStringInto(wsysSystemErrorParams);
			syserrrec.params.set(wsysSystemErrorParams);
			fatalError600();
		}
		contotrec.totno.set(controlTotalsInner.ct06);
		contotrec.totval.set(wsaaTotAllocated);
		callContot001();
	}

protected void processCovrs3510()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start3511();
				case skipBonusWorkbench3515:
					skipBonusWorkbench3515();
				case readNextCovr3517:
					readNextCovr3517();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3511()
	{
		/* Check to see if coverage is of a valid status*/
		wsaaValidCoverage.set("N");
		if (isGT(bsscIO.getEffectiveDate(), covrlnbIO.getCurrto())) {
			goTo(GotoLabel.readNextCovr3517);
		}
		if (isNE(covrlnbIO.getValidflag(), "1")) {
			goTo(GotoLabel.readNextCovr3517);
		}
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covrlnbIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], covrlnbIO.getPstatcode())) {
						wsaaT5679Sub.set(13);
						wsaaValidCoverage.set("Y");
					}
				}
			}
		}
		/*  If the coverage is not of a valid status read the next covr*/
		/*  for the contract*/
		if (!validCoverage.isTrue()) {
			goTo(GotoLabel.readNextCovr3517);
		}
		if (isEQ(covrlnbIO.getInstprem(), 0)) {
			goTo(GotoLabel.readNextCovr3517);
		}
		wsaaTotamt.add(covrlnbIO.getInstprem());
		if (isLT(wsaaTotamt, payrIO.getSinstamt06())) {
			compute(wsaaProportion, 7).set(div(covrlnbIO.getInstprem(), payrIO.getSinstamt06()));
			wsaaProportion.set(1);
			compute(wsaaPolamnt, 8).setRounded(mult(wsaaProportion, wsaaSuspAvail));
			if (isNE(wsaaPolamnt, 0)) {
				zrdecplrec.amountIn.set(wsaaPolamnt);
				zrdecplrec.currency.set(acblIO.getOrigcurr());
				a000CallRounding();
				wsaaPolamnt.set(zrdecplrec.amountOut);
			}
			wsaaTotAllocated.add(wsaaPolamnt);
		}
		else {
			compute(wsaaPolamnt, 2).set(sub(wsaaSuspAvail, wsaaTotAllocated));
			if (isNE(wsaaPolamnt, 0)) {
				zrdecplrec.amountIn.set(wsaaPolamnt);
				zrdecplrec.currency.set(acblIO.getOrigcurr());
				a000CallRounding();
				wsaaPolamnt.set(zrdecplrec.amountOut);
			}
			wsaaTotAllocated.add(wsaaPolamnt);
		}
		if (isNE(th605rec.bonusInd, "Y")) {
			goTo(GotoLabel.skipBonusWorkbench3515);
		}
		/* Bonus Workbench *                                               */
		wsaaAgcmIx.add(1);
		wsaaAgcmSummaryInner.wsaaAgcmChdrcoy[wsaaAgcmIx.toInt()].set(covrlnbIO.getChdrcoy());
		wsaaAgcmSummaryInner.wsaaAgcmChdrnum[wsaaAgcmIx.toInt()].set(covrlnbIO.getChdrnum());
		wsaaAgcmSummaryInner.wsaaAgcmLife[wsaaAgcmIx.toInt()].set(covrlnbIO.getLife());
		wsaaAgcmSummaryInner.wsaaAgcmCoverage[wsaaAgcmIx.toInt()].set(covrlnbIO.getCoverage());
		wsaaAgcmSummaryInner.wsaaAgcmRider[wsaaAgcmIx.toInt()].set(covrlnbIO.getRider());
		b200PremiumHistory();
	}

protected void skipBonusWorkbench3515()
	{
		wsaaVariablesInner.wsaaTotTaxCharged.set(0);
		if (isEQ(tr52drec.txcode, SPACES)) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaEndPost.set("N");
			for (wsaaVariablesInner.wsaaIx.set(1); !(isGT(wsaaVariablesInner.wsaaIx, 98)
			|| endPost.isTrue()); wsaaVariablesInner.wsaaIx.add(1)){
				b700PropTax();
			}
			wsaaEndPost.set("N");
			for (wsaaVariablesInner.wsaaIx.set(1); !(isGT(wsaaVariablesInner.wsaaIx, 98)
			|| endPost.isTrue()); wsaaVariablesInner.wsaaIx.add(1)){
				b750PostTax();
			}
		}
		componentAcc3530();
		readFpco3550();
	}

protected void readNextCovr3517()
	{
		covrlnbIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrlnbIO);
		if ((isNE(covrlnbIO.getStatuz(), varcom.oK))
		&& (isNE(covrlnbIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError600();
		}
		if ((isNE(covrlnbIO.getChdrcoy(), fprxpfRec.chdrcoy))
		|| (isNE(covrlnbIO.getChdrnum(), fprxpfRec.chdrnum))
		|| isEQ(covrlnbIO.getStatuz(), varcom.endp)) {
			covrlnbIO.setStatuz(varcom.endp);
			return ;
		}
		/*EXIT*/
	}

protected void componentAcc3530()
	{
		start3531();
	}

protected void start3531()
	{
		/*  Do component level accounting for coverage*/
		if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")
		&& isNE(wsaaT5688Revacc[wsaaT5688Ix.toInt()], "Y")) {
			/* AND COVRLNB-RIDER        NOT = ZEROES OR SPACES              */
			wsaaGlSub.set(22);
			wsaaRldgChdrnum.set(covrlnbIO.getChdrnum());
			wsaaRldgLife.set(covrlnbIO.getLife());
			wsaaRldgCoverage.set(covrlnbIO.getCoverage());
			wsaaRldgRider.set(covrlnbIO.getRider());
			wsaaPlan.set(covrlnbIO.getPlanSuffix());
			wsaaRldgPlanSuff.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.origamt.set(wsaaPolamnt);
			//lifacmvrec.origamt.set(covrlnbIO.getInstprem());//ILIFE-4941
			lifacmvrec.jrnseq.set(0);
			lifacmvrec.origcurr.set(covrlnbIO.getPremCurrency());
			lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
			lifacmvrec.effdate.set(bsscIO.getEffectiveDate());
			callLifacmv6000();
		}
	}

protected void readFpco3550()
	{
		start3551();
	}

protected void start3551()
	{
		/*  Read the first FPCO record for the coverage*/
		wsaaPolAmt.set(wsaaPolamnt);
		fpcoIO.setChdrcoy(covrlnbIO.getChdrcoy());
		fpcoIO.setChdrnum(covrlnbIO.getChdrnum());
		fpcoIO.setLife(covrlnbIO.getLife());
		fpcoIO.setCoverage(covrlnbIO.getCoverage());
		fpcoIO.setRider(covrlnbIO.getRider());
		fpcoIO.setPlanSuffix(covrlnbIO.getPlanSuffix());
		fpcoIO.setTargfrom(0);
		fpcoIO.setFunction(varcom.begnh);
		fpcoIO.setFormat(formatsInner.fpcorec);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcoIO.getParams());
			fatalError600();
		}
		if (isNE(fpcoIO.getChdrcoy(), covrlnbIO.getChdrcoy())
		|| isNE(fpcoIO.getChdrnum(), covrlnbIO.getChdrnum())
		|| isNE(fpcoIO.getLife(), covrlnbIO.getLife())
		|| isNE(fpcoIO.getCoverage(), covrlnbIO.getCoverage())
		|| isNE(fpcoIO.getRider(), covrlnbIO.getRider())
		|| isNE(fpcoIO.getPlanSuffix(), covrlnbIO.getPlanSuffix())) {
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError600();
		}
		while ( !(isEQ(fpcoIO.getStatuz(), varcom.endp))) {
			processFpco3570();
		}

	}

protected void processFpco3570()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start3571();
				case skipBonusWorkbench3572:
					skipBonusWorkbench3572();
					readNextFpco3575();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3571()
	{
		/*  Work out the difference between the target premium and the*/
		/*  premium received in the period so far*/
		/*  If the current to date on the FPCO record is less than the*/
		/*  billed to date on the PAYR record, we are looking at the*/
		/*  record for the previous period.*/
		/*  If the pro-rata amount for the policy is less than the amount*/
		/*  left to pay in the period we should add the amount to the prem*/
		/*  received field on the FPCO and add this amount to the working*/
		/*  storage total for up to target premium*/
		/*  If the pro-rata amount is greater or equal to the amount left*/
		/*  to pay in the period we should 'pay up' the premium received*/
		/*  field on the FPCO for this period, set the active indicator to*/
		/*  'N' if all the target has been billed for and add this <D9604>*/
		/*  to the up to target working storage                    <D9604>*/
		/*   If we are looking at the current FPCO record we should pay*/
		/*   the whole amount on to the current record*/
		/*   If the pro-rata amount is less than or equal to the amount*/
		/*   left to pay, the whole amount should be added into up to*/
		/*   target working storage*/
		/*   If the pro-rata amount is greater than the amount left to pay*/
		/*   we add the amount left to pay into the up to target working*/
		/*   storage and the remainder into over target working storage*/
		wsaaUpToTgt.set(0);
		wsaaOvrTgt.set(0);
		if (isLTE(fpcoIO.getTargetPremium(), fpcoIO.getPremRecPer())) {
			wsaaTgtDiff.set(0);
		}
		else {
			compute(wsaaTgtDiff, 2).set(sub(fpcoIO.getTargetPremium(), fpcoIO.getPremRecPer()));
		}
		rnlallrec.totrecd.set(fpcoIO.getPremRecPer());
		if (isEQ(th605rec.bonusInd, "Y")) {
			wsaaReceivedPremium.set(fpcoIO.getPremRecPer());
		}
		if (isLT(fpcoIO.getTargto(), payrIO.getBtdate())) {
			if (isLT(wsaaPolAmt, wsaaTgtDiff)) {
				setPrecision(fpcoIO.getPremRecPer(), 2);
				fpcoIO.setPremRecPer(add(fpcoIO.getPremRecPer(), wsaaPolAmt));
				wsaaUpToTgt.add(wsaaPolAmt);
			}
			else {
				setPrecision(fpcoIO.getPremRecPer(), 2);
				fpcoIO.setPremRecPer(add(fpcoIO.getPremRecPer(), wsaaTgtDiff));
				wsaaUpToTgt.add(wsaaTgtDiff);
				if (isGTE(fpcoIO.getBilledInPeriod(), fpcoIO.getTargetPremium())) {
					fpcoIO.setActiveInd("N");
				}
			}
			compute(wsaaPolAmt, 2).set(sub(wsaaPolAmt, wsaaUpToTgt));
		}
		else {
			setPrecision(fpcoIO.getPremRecPer(), 2);
			fpcoIO.setPremRecPer(add(fpcoIO.getPremRecPer(), wsaaPolAmt));
			if (isLTE(wsaaPolAmt, wsaaTgtDiff)) {
				wsaaUpToTgt.add(wsaaPolAmt);
			}
			else {
				wsaaUpToTgt.add(wsaaTgtDiff);
				compute(wsaaOvrTgt, 2).set(sub(wsaaPolAmt, wsaaUpToTgt));
			}
			wsaaPolAmt.set(ZERO);
		}
		if (isNE(th605rec.bonusInd, "Y")) {
			goTo(GotoLabel.skipBonusWorkbench3572);
		}
		/* Bonus Workbench *                                               */
		if (isNE(wsaaUpToTgt, ZERO)) {
			b300WriteArrays();
		}
		/* Write the Over Target Premium                                   */
		if (isNE(wsaaOvrTgt, ZERO)) {
			b500WriteOverTarget();
		}
	}

protected void skipBonusWorkbench3572()
	{
		/*  We need to do the REWRT of the FPCO here for the calculation*/
		/*  of initial commission*/
		fpcoIO.setFormat(formatsInner.fpcorec);
		fpcoIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcoIO.getParams());
			fatalError600();
		}
		processCommission3590();
		genericProcessing5000();
	}

protected void readNextFpco3575()
	{
		if (isEQ(wsaaPolAmt, 0)) {
			fpcoIO.setStatuz(varcom.endp);
			return ;
		}
		fpcoIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, fpcoIO);
		if ((isNE(fpcoIO.getStatuz(), varcom.oK))
		&& (isNE(fpcoIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(fpcoIO.getParams());
			fatalError600();
		}
		if (isNE(fpcoIO.getChdrcoy(), covrlnbIO.getChdrcoy())
		|| isNE(fpcoIO.getChdrnum(), covrlnbIO.getChdrnum())
		|| isNE(fpcoIO.getLife(), covrlnbIO.getLife())
		|| isNE(fpcoIO.getCoverage(), covrlnbIO.getCoverage())
		|| isNE(fpcoIO.getRider(), covrlnbIO.getRider())
		|| isNE(fpcoIO.getPlanSuffix(), covrlnbIO.getPlanSuffix())) {
			fpcoIO.setStatuz(varcom.endp);
			return ;
		}
	}

protected void processCommission3590()
	{
		start3591();
	}

protected void start3591()
	{
		agcmbchIO.setChdrcoy(fpcoIO.getChdrcoy());
		agcmbchIO.setChdrnum(fpcoIO.getChdrnum());
		agcmbchIO.setLife(fpcoIO.getLife());
		agcmbchIO.setCoverage(fpcoIO.getCoverage());
		agcmbchIO.setRider(fpcoIO.getRider());
		agcmbchIO.setPlanSuffix(fpcoIO.getPlanSuffix());
		/*  MOVE BEGNH                  TO AGCMBCH-FUNCTION.             */
		agcmbchIO.setFunction(varcom.begn);
		agcmbchIO.setAgntnum(SPACES);
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
		agcmbchIO.setStatuz(varcom.oK);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), varcom.oK)
		&& isNE(agcmbchIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmbchIO.getParams());
			fatalError600();
		}
		if (isNE(agcmbchIO.getChdrcoy(), fpcoIO.getChdrcoy())
		|| isNE(agcmbchIO.getChdrnum(), fpcoIO.getChdrnum())
		|| isNE(agcmbchIO.getLife(), fpcoIO.getLife())
		|| isNE(agcmbchIO.getCoverage(), fpcoIO.getCoverage())
		|| isNE(agcmbchIO.getRider(), fpcoIO.getRider())
		|| isNE(agcmbchIO.getPlanSuffix(), fpcoIO.getPlanSuffix())) {
			/*        MOVE REWRT              TO AGCMBCH-FUNCTION            */
			/*        CALL 'AGCMBCHIO'        USING AGCMBCH-PARAMS           */
			/*        IF AGCMBCH-STATUZ       NOT = O-K                      */
			/*           MOVE AGCMBCH-PARAMS  TO SYSR-PARAMS                 */
			/*           PERFORM 600-FATAL-ERROR                             */
			/*        END-IF                                                 */
			agcmbchIO.setStatuz(varcom.endp);
			return ;
		}
		while ( !(isEQ(agcmbchIO.getStatuz(), varcom.endp))) {
			processAgcm3610();
		}

	}

protected void processAgcm3610()
	{
		start3611();
		readNextAgcm3615();
	}

protected void start3611()
	{
		/*  <D9604>*/
		/*                                                      *  <D9604>*/
		/* Dont process either dormant or single premium AGCM's *  <D9604>*/
		/* Also, add over target commission to first SEQNO      *  <D9604>*/
		/* only.                                                *  <D9604>*/
		/*                                                      *  <D9604>*/
		/*  <D9604>*/
		if (isEQ(agcmbchIO.getDormantFlag(), "Y")
		|| isEQ(agcmbchIO.getPtdate(), 0)) {
			/*     MOVE REWRT               TO AGCMBCH-FUNCTION              */
			/*     MOVE AGCMBCHREC          TO AGCMBCH-FORMAT                */
			/*     CALL 'AGCMBCHIO'         USING AGCMBCH-PARAMS             */
			/*     IF AGCMBCH-STATUZ        NOT = O-K                        */
			/*        MOVE AGCMBCH-STATUZ   TO SYSR-STATUZ                   */
			/*        MOVE AGCMBCH-PARAMS   TO SYSR-PARAMS                   */
			/*        PERFORM 600-FATAL-ERROR                                */
			/*     END-IF                                                    */
			return ;
		}
		if (isEQ(wsaaUpToTgt, 0)
		&& isGT(agcmbchIO.getSeqno(), 1)) {
			/*     MOVE REWRT               TO AGCMBCH-FUNCTION      <A06966>*/
			/*     MOVE AGCMBCHREC          TO AGCMBCH-FORMAT        <A06966>*/
			/*     CALL 'AGCMBCHIO'         USING AGCMBCH-PARAMS     <A06966>*/
			/*     IF AGCMBCH-STATUZ        NOT = O-K                <A06966>*/
			/*        MOVE AGCMBCH-STATUZ   TO SYSR-STATUZ           <A06966>*/
			/*        MOVE AGCMBCH-PARAMS   TO SYSR-PARAMS           <A06966>*/
			/*        PERFORM 600-FATAL-ERROR                        <A06966>*/
			/*     END-IF                                            <A06966>*/
			return ;
		}
		comlinkrec.agent.set(agcmbchIO.getAgntnum());
		comlinkrec.chdrcoy.set(fprxpfRec.chdrcoy);
		comlinkrec.chdrnum.set(fprxpfRec.chdrnum);
		comlinkrec.language.set(bsscIO.getLanguage());
		comlinkrec.life.set(agcmbchIO.getLife());
		comlinkrec.coverage.set(agcmbchIO.getCoverage());
		comlinkrec.rider.set(agcmbchIO.getRider());
		comlinkrec.planSuffix.set(agcmbchIO.getPlanSuffix());
		comlinkrec.crtable.set(covrlnbIO.getCrtable());
		comlinkrec.jlife.set(covrlnbIO.getJlife());
		comlinkrec.agentClass.set(agcmbchIO.getAgentClass());
		comlinkrec.effdate.set(agcmbchIO.getEfdate());
		comlinkrec.billfreq.set(payrIO.getBillfreq());
		wsaaBillfreq.set(payrIO.getBillfreq());
		comlinkrec.annprem.set(agcmbchIO.getAnnprem());
		comlinkrec.targetPrem.set(fpcoIO.getTargetPremium());
		comlinkrec.currto.set(fpcoIO.getTargto());
		comlinkrec.seqno.set(agcmbchIO.getSeqno());
		/*    MOVE 0                      TO CLNK-PTDATE.          <V65L18>*/
		comlinkrec.ptdate.set(chdrlifIO.getPtdate());
		for (wsaaCommType.set(1); !(isEQ(wsaaCommType, 4)); wsaaCommType.add(1)){
			callSubroutine3630();
		}
		postCommission3650();
		rewriteAgcm3670();
	}

protected void readNextAgcm3615()
	{
		agcmbchIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), varcom.oK)
		&& isNE(agcmbchIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmbchIO.getParams());
			fatalError600();
		}
		if (isNE(agcmbchIO.getChdrcoy(), fpcoIO.getChdrcoy())
		|| isNE(agcmbchIO.getChdrnum(), fpcoIO.getChdrnum())
		|| isNE(agcmbchIO.getLife(), fpcoIO.getLife())
		|| isNE(agcmbchIO.getCoverage(), fpcoIO.getCoverage())
		|| isNE(agcmbchIO.getRider(), fpcoIO.getRider())
		|| isNE(agcmbchIO.getPlanSuffix(), fpcoIO.getPlanSuffix())) {
			/*       MOVE REWRT             TO AGCMBCH-FUNCTION              */
			/*       CALL 'AGCMBCHIO'       USING AGCMBCH-PARAMS             */
			/*       IF AGCMBCH-STATUZ      NOT = O-K                        */
			/*          MOVE AGCMBCH-PARAMS TO SYSR-PARAMS                   */
			/*          PERFORM 600-FATAL-ERROR                              */
			/*       END-IF                                                  */
			agcmbchIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void callSubroutine3630()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start3631();
				case post3632:
					post3632();
				case exit3639:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3631()
	{
		wsaaPayamnt[wsaaCommType.toInt()].set(ZERO);
		wsaaErndamt[wsaaCommType.toInt()].set(ZERO);
		/*  Set up linkage for commission subroutine*/
		comlinkrec.function.set(SPACES);
		comlinkrec.statuz.set(SPACES);
		comlinkrec.icommtot.set(ZERO);
		comlinkrec.icommpd.set(ZERO);
		comlinkrec.icommernd.set(ZERO);
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		comlinkrec.instprem.set(ZERO);
		if (isEQ(wsaaCommType, 1)
		&& isEQ(wsaaUpToTgt, 0)) {
			goTo(GotoLabel.exit3639);
		}
		if (isEQ(wsaaCommType, 1)) {
			if (isEQ(agcmbchIO.getBascpy(), SPACES)
			|| (isEQ(agcmbchIO.getCompay(), agcmbchIO.getInitcom())
			&& isEQ(agcmbchIO.getComern(), agcmbchIO.getInitcom()))) {
				goTo(GotoLabel.exit3639);
			}
			else {
				itemIO.setItemitem(agcmbchIO.getBascpy());
			}
		}
		if (isEQ(wsaaCommType, 2)) {
			if (isEQ(agcmbchIO.getRnwcpy(), SPACES)
			|| isEQ(wsaaUpToTgt, ZERO)) {
				goTo(GotoLabel.exit3639);
			}
			else {
				itemIO.setItemitem(agcmbchIO.getRnwcpy());
			}
		}
		if (isEQ(wsaaCommType, 3)) {
			if (isEQ(agcmbchIO.getSrvcpy(), SPACES)
			|| isEQ(wsaaOvrTgt, ZERO)
			|| isGT(agcmbchIO.getSeqno(), 1)) {
				goTo(GotoLabel.exit3639);
			}
			else {
				itemIO.setItemitem(agcmbchIO.getSrvcpy());
			}
		}
		ArraySearch as1 = ArraySearch.getInstance(wsaaT5644Rec);
		as1.setIndices(wsaaT5644Ix);
		as1.addSearchKey(wsaaT5644Key, itemIO.getItemitem(), true);
		if (as1.binarySearch()) {
			/*CONTINUE_STMT*/
		}
		else {
			goTo(GotoLabel.exit3639);
		}
		comlinkrec.method.set(itemIO.getItemitem());
		if (isEQ(wsaaCommType, 1)) {
			comlinkrec.icommtot.set(agcmbchIO.getInitcom());
			comlinkrec.icommpd.set(agcmbchIO.getCompay());
			comlinkrec.icommernd.set(agcmbchIO.getComern());
		}
		else {
			if (isEQ(wsaaCommType, 2)) {
				comlinkrec.instprem.set(wsaaUpToTgt);
			}
			else {
				comlinkrec.instprem.set(wsaaOvrTgt);
			}
		}
		callProgram(wsaaT5644Comsub[wsaaT5644Ix.toInt()], comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			fatalError600();
		}
		/* MOVE CLNK-PAYAMNT           TO ZRDP-AMOUNT-IN.               */
		/* PERFORM A000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO CLNK-PAYAMNT.                 */
		if (isNE(comlinkrec.payamnt, 0)) {
			zrdecplrec.amountIn.set(comlinkrec.payamnt);
			zrdecplrec.currency.set(chdrlifIO.getCntcurr());
			a000CallRounding();
			comlinkrec.payamnt.set(zrdecplrec.amountOut);
		}
		/* MOVE CLNK-ERNDAMT           TO ZRDP-AMOUNT-IN.               */
		/* PERFORM A000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO CLNK-ERNDAMT.                 */
		if (isNE(comlinkrec.erndamt, 0)) {
			zrdecplrec.amountIn.set(comlinkrec.erndamt);
			zrdecplrec.currency.set(chdrlifIO.getCntcurr());
			a000CallRounding();
			comlinkrec.erndamt.set(zrdecplrec.amountOut);
		}
		wsaaPayamnt[wsaaCommType.toInt()].set(comlinkrec.payamnt);
		wsaaErndamt[wsaaCommType.toInt()].set(comlinkrec.erndamt);
		if (isNE(th605rec.bonusInd, "Y")) {
			goTo(GotoLabel.post3632);
		}
		/* Bonus Workbench *                                               */
		if (isEQ(wsaaCommType, "1")){
			firstPrem.setTrue();
		}
		else if (isEQ(wsaaCommType, "2")){
			renPrem.setTrue();
		}
		else{
			goTo(GotoLabel.post3632);
		}
		/* Skip overriding commission                                      */
		if (isEQ(agcmbchIO.getOvrdcat(), "B")
		&& isNE(comlinkrec.payamnt, ZERO)) {
			zctnIO.setParams(SPACES);
			zctnIO.setAgntcoy(agcmbchIO.getChdrcoy());
			zctnIO.setAgntnum(agcmbchIO.getAgntnum());
			zctnIO.setCommAmt(comlinkrec.payamnt);
			b400LocatePremium();
			zctnIO.setPremium(wsaaAgcmAlcprem);
			setPrecision(zctnIO.getSplitBcomm(), 3);
			zctnIO.setSplitBcomm(div(mult(agcmbchIO.getAnnprem(), 100), wsaaAgcmPremium), true);
			zctnIO.setZprflg(wsaaPremType);
			zctnIO.setChdrcoy(comlinkrec.chdrcoy);
			zctnIO.setChdrnum(comlinkrec.chdrnum);
			zctnIO.setLife(comlinkrec.life);
			zctnIO.setCoverage(comlinkrec.coverage);
			zctnIO.setRider(comlinkrec.rider);
			zctnIO.setTranno(chdrlifIO.getTranno());
			zctnIO.setTransCode(bprdIO.getAuthCode());
			zctnIO.setEffdate(linsrnlIO.getInstfrom());
			zctnIO.setTrandate(bsscIO.getEffectiveDate());
			zctnIO.setFormat(zctnrec);
			zctnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zctnIO);
			if (isNE(zctnIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(zctnIO.getStatuz());
				syserrrec.params.set(zctnIO.getParams());
				fatalError600();
			}
		}
	}

protected void post3632()
	{
		setUpPostingValues3690();
	}

protected void postCommission3650()
	{
		start3651();
	}

protected void start3651()
	{
		/*  Initialise the ACMV area and move the contract type to its*/
		/*  relevant field (same for all calls to LIFACMV).*/
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		wsaaAgntChdrnum.set(covrlnbIO.getChdrnum());
		wsaaAgntLife.set(covrlnbIO.getLife());
		wsaaAgntCoverage.set(covrlnbIO.getCoverage());
		wsaaAgntRider.set(covrlnbIO.getRider());
		wsaaPlan.set(covrlnbIO.getPlanSuffix());
		wsaaAgntPlanSuffix.set(wsaaPlansuff);
		postInitialCommission7000();
		postServiceCommission8000();
		postRenewalCommission9000();
		postOverridCommission10000();
		wsaaBascpyDue.set(0);
		wsaaBascpyErn.set(0);
		wsaaBascpyPay.set(0);
		wsaaSrvcpyDue.set(0);
		wsaaSrvcpyErn.set(0);
		wsaaRnwcpyDue.set(0);
		wsaaRnwcpyErn.set(0);
		wsaaOvrdBascpyPay.set(0);
		wsaaOvrdBascpyDue.set(0);
		wsaaOvrdBascpyErn.set(0);
	}

protected void rewriteAgcm3670()
	{
		start3671();
	}

protected void start3671()
	{
		agcmbchIO.setFunction(varcom.readh);
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			fatalError600();
		}
		if (isNE(agcmbchIO.getBascpy(), SPACES)) {
			setPrecision(agcmbchIO.getCompay(), 2);
			agcmbchIO.setCompay(add(agcmbchIO.getCompay(), wsaaPayamnt[1]));
			setPrecision(agcmbchIO.getComern(), 2);
			agcmbchIO.setComern(add(agcmbchIO.getComern(), wsaaErndamt[1]));
		}
		if (isNE(agcmbchIO.getRnwcpy(), SPACES)) {
			setPrecision(agcmbchIO.getRnlcdue(), 2);
			agcmbchIO.setRnlcdue(add(agcmbchIO.getRnlcdue(), wsaaPayamnt[2]));
			setPrecision(agcmbchIO.getRnlcearn(), 2);
			agcmbchIO.setRnlcearn(add(agcmbchIO.getRnlcearn(), wsaaErndamt[2]));
		}
		if (isNE(agcmbchIO.getSrvcpy(), SPACES)) {
			setPrecision(agcmbchIO.getScmdue(), 2);
			agcmbchIO.setScmdue(add(agcmbchIO.getScmdue(), wsaaPayamnt[3]));
			setPrecision(agcmbchIO.getScmearn(), 2);
			agcmbchIO.setScmearn(add(agcmbchIO.getScmearn(), wsaaErndamt[3]));
		}
		agcmbchIO.setPtdate(payrIO.getPtdate());
		agcmbchIO.setFunction(varcom.rewrt);
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			fatalError600();
		}
	}

protected void setUpPostingValues3690()
	{
		start3691();
	}

protected void start3691()
	{
		/*  Basic Commission*/
		if (isEQ(wsaaCommType, 1)) {
			if (isEQ(agcmbchIO.getOvrdcat(), "O")) {
				wsaaOvrdBascpyDue.set(comlinkrec.payamnt);
				wsaaOvrdBascpyErn.set(comlinkrec.erndamt);
				compute(wsaaOvrdBascpyPay, 2).set(add(wsaaOvrdBascpyPay, (sub(comlinkrec.payamnt, comlinkrec.erndamt))));
			}
			else {
				wsaaBascpyDue.set(comlinkrec.payamnt);
				wsaaBascpyErn.set(comlinkrec.erndamt);
				compute(wsaaBascpyPay, 2).set(add(wsaaBascpyPay, (sub(comlinkrec.payamnt, comlinkrec.erndamt))));
			}
		}
		/*  Service Commission*/
		if (isEQ(wsaaCommType, 3)) {
			wsaaSrvcpyDue.set(comlinkrec.payamnt);
			wsaaSrvcpyErn.set(comlinkrec.erndamt);
		}
		/*  Renewal Commission*/
		if (isEQ(wsaaCommType, 2)) {
			wsaaRnwcpyDue.set(comlinkrec.payamnt);
			wsaaRnwcpyErn.set(comlinkrec.erndamt);
		}
	}

protected void writePtrn3800()
	{
		start3810();
	}

protected void start3810()
	{
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setChdrcoy(fprxpfRec.chdrcoy);
		ptrnIO.setChdrnum(fprxpfRec.chdrnum);
		ptrnIO.setBatcpfx(batcdorrec.prefix);
		ptrnIO.setPtrneff(bsscIO.getEffectiveDate());
		ptrnIO.setDatesub(bsscIO.getEffectiveDate());
		ptrnIO.setCrtuser(bsscIO.getUserName().toString());   //IJS-523
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
		contotrec.totno.set(controlTotalsInner.ct14);
		contotrec.totval.set(1);
		callContot001();
	}

protected void unlkChdr3900()
	{
		start3910();
	}

protected void start3910()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.enttyp.set("CH");
		sftlockrec.company.set(bsprIO.getCompany());
		sftlockrec.user.set(99999);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.entity.set(fprxpfRec.chdrnum);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			wsysSysparams.set(sftlockrec.sftlockRec);
			syserrrec.params.set(wsysSystemErrorParams);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void genericProcessing5000()
	{
		start5010();
	}

protected void start5010()
	{
		wsaaAuthCode.set(bprdIO.getAuthCode());
		wsaaCovrlnbCrtable.set(covrlnbIO.getCrtable());
		wsaaOvrTarget.set(wsaaOvrTgt);
		ArraySearch as1 = ArraySearch.getInstance(wsaaT5671Rec);
		as1.setIndices(wsaaT5671Ix);
		as1.addSearchKey(wsaaT5671Key, wsaaTrcdeCrtable, true);
		if (as1.binarySearch()) {
			/*CONTINUE_STMT*/
		}
		else {
			return ;
		}
		for (wsaaSubprogIx.set(1); !(isGT(wsaaSubprogIx, 4)); wsaaSubprogIx.add(1)){
			if (isNE(wsaaT5671Subprog[wsaaT5671Ix.toInt()][wsaaSubprogIx.toInt()], SPACES)) {
				callGeneric5100();
			}
		}
	}

protected void callGeneric5100()
	{
		start5110();
	}

protected void start5110()
	{
		rnlallrec.statuz.set(varcom.oK);
		rnlallrec.company.set(fprxpfRec.chdrcoy);
		rnlallrec.chdrnum.set(fprxpfRec.chdrnum);
		rnlallrec.life.set(covrlnbIO.getLife());
		rnlallrec.coverage.set(covrlnbIO.getCoverage());
		rnlallrec.rider.set(covrlnbIO.getRider());
		rnlallrec.planSuffix.set(covrlnbIO.getPlanSuffix());
		rnlallrec.crdate.set(covrlnbIO.getCrrcd());
		rnlallrec.crtable.set(covrlnbIO.getCrtable());
		rnlallrec.billcd.set(bsscIO.getEffectiveDate());
		rnlallrec.tranno.set(chdrlifIO.getTranno());
		if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")) {
			rnlallrec.sacscode.set(wsaaT5645Sacscode[30]);
			rnlallrec.sacstyp.set(wsaaT5645Sacstype[30]);
			rnlallrec.genlcde.set(wsaaT5645Glmap[30]);
			rnlallrec.sacscode02.set(wsaaT5645Sacscode[31]);
			rnlallrec.sacstyp02.set(wsaaT5645Sacstype[31]);
			rnlallrec.genlcde02.set(wsaaT5645Glmap[31]);
		}
		else {
			rnlallrec.sacscode.set(wsaaT5645Sacscode[20]);
			rnlallrec.sacstyp.set(wsaaT5645Sacstype[20]);
			rnlallrec.genlcde.set(wsaaT5645Glmap[20]);
			rnlallrec.sacscode02.set(wsaaT5645Sacscode[21]);
			rnlallrec.sacstyp02.set(wsaaT5645Sacstype[21]);
			rnlallrec.genlcde02.set(wsaaT5645Glmap[21]);
		}
		rnlallrec.cntcurr.set(covrlnbIO.getPremCurrency());
		rnlallrec.cnttype.set(chdrlifIO.getCnttype());
		rnlallrec.billfreq.set(payrIO.getBillfreq());
		rnlallrec.duedate.set(bsscIO.getEffectiveDate());
		rnlallrec.anbAtCcd.set(covrlnbIO.getAnbAtCcd());
		/* Calculate the term left to run. It is needed in the generic*/
		/* processing routine to work out the initial unit discount factor*/
		datcon3rec.intDate1.set(bsscIO.getEffectiveDate());
		datcon3rec.intDate2.set(covrlnbIO.getPremCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			datcon3rec.freqFactorx.set(ZERO);
		}
		wsaaTerm.set(datcon3rec.freqFactor);
		if (isNE(wsaaTermLeftRemain,0)) {
			wsaaTerm.add(1);
		}
		rnlallrec.termLeftToRun.set(wsaaTermLeftInteger);
		/* Get the total premium from the temporary table for the coverage*/
		if (isGT(wsaaUpToTgt, 0)) {
			rnlallrec.covrInstprem.set(wsaaUpToTgt);
			if ((isEQ(wsaaT5671Subprog[wsaaT5671Ix.toInt()][wsaaSubprogIx.toInt()],wsaaSubroutineA) || isEQ(wsaaT5671Subprog[wsaaT5671Ix.toInt()][wsaaSubprogIx.toInt()],wsaaSubroutineB) ))
			{
				if ( isEQ(wsaaFirst,"Y") && isEQ(wsaaLoyaltyApp,"Y"))
				{
					callProgram(wsaaT5671Subprog[wsaaT5671Ix.toInt()][wsaaSubprogIx.toInt()], rnlallrec.rnlallRec);					
					wsaaFirst.set("N");
					wsaaLoyaltyApp.set("N");
				}
			}	
			else
			{
				 callProgram(wsaaT5671Subprog[wsaaT5671Ix.toInt()][wsaaSubprogIx.toInt()], rnlallrec.rnlallRec);
				 
			}			
			if (isNE(rnlallrec.statuz, varcom.oK)) {
				wsysSysparams.set(rnlallrec.rnlallRec);
				syserrrec.params.set(wsysSystemErrorParams);
				syserrrec.statuz.set(rnlallrec.statuz);
				fatalError600();
			}
		}
		/* IF WSAA-OVR-TGT  > 0                                 <LA1647>*/
		if (isGT(wsaaOvrTarget, 0)) {
			rnlallrec.covrInstprem.set(wsaaOvrTgt);
			callProgram(Overaloc.class, rnlallrec.rnlallRec);
			if (isNE(rnlallrec.statuz, varcom.oK)) {
				  wsysSysparams.set(rnlallrec.rnlallRec);
				  syserrrec.params.set(wsysSystemErrorParams);
				  syserrrec.statuz.set(rnlallrec.statuz);
				  fatalError600();
			}
			wsaaOvrTarget.set(0);
		}
	}

protected void callLifacmv6000()
	{
		start6010();
	}

protected void start6010()
	{
		/* This is the only place LIFACMV parameters are referenced.  LIFA*/
		/* fields that are changeable are set outside this section within*/
		/* a working storage variable.*/
		lifacmvrec.rdocnum.set(fprxpfRec.chdrnum);
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		/*  If the ACMV is being created for the COVR-INSTPREM, the*/
		/*  seventh table entries will be used.  Move the appropriate*/
		/*  value for the COVR posting.*/
		lifacmvrec.effdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.user.set(0);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[wsaaGlSub.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[wsaaGlSub.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[wsaaGlSub.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[wsaaGlSub.toInt()]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[wsaaGlSub.toInt()]);
		/* Where the system parameter 3 on the process definition       */
		/* is not spaces, LIFACMV will be called with the function      */
		/* NPSTW instead of PSTW, so that the ACBL file will not        */
		/* be updated by this program.                                  */
		if (isEQ(bprdIO.getSystemParam03(), SPACES)) {
			lifacmvrec.function.set("PSTW");
		}
		else {
			if (isEQ(bprdIO.getSystemParam03(), wsaaT5645Sacscode[wsaaGlSub.toInt()])) {
				lifacmvrec.function.set("NPSTW");
				deferredPostings6100();
			}
			else {
				lifacmvrec.function.set("PSTW");
			}
		}
		/*    MOVE 'PSTW'                     TO LIFA-FUNCTION.            */
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			/*     MOVE LIFR-LIFRTRN-REC         TO WSYS-SYSPARAMS          */
			wsysSysparams.set(lifacmvrec.lifacmvRec);
			syserrrec.params.set(wsysSystemErrorParams);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
		wsaaRldgacct.set(SPACES);
	}

protected void deferredPostings6100()
	{
		writeRecord6100();
	}

protected void writeRecord6100()
	{
		acagrnlIO.setRldgcoy(lifacmvrec.rldgcoy);
		acagrnlIO.setSacscode(lifacmvrec.sacscode);
		acagrnlIO.setRldgacct(lifacmvrec.rldgacct);
		acagrnlIO.setOrigcurr(lifacmvrec.origcurr);
		acagrnlIO.setSacstyp(lifacmvrec.sacstyp);
		acagrnlIO.setSacscurbal(lifacmvrec.origamt);
		acagrnlIO.setRdocnum(lifacmvrec.rdocnum);
		acagrnlIO.setGlsign(lifacmvrec.glsign);
		acagrnlIO.setFunction(varcom.writr);
		acagrnlIO.setFormat(formatsInner.acagrnlrec);
		SmartFileCode.execute(appVars, acagrnlIO);
		if (isNE(acagrnlIO.getStatuz(), varcom.oK)
		&& isNE(acagrnlIO.getStatuz(), varcom.dupr)) {
			syserrrec.statuz.set(acagrnlIO.getStatuz());
			syserrrec.params.set(acagrnlIO.getParams());
			fatalError600();
		}
	}

protected void postInitialCommission7000()
	{
		start7010();
	}

protected void start7010()
	{
		/*  Initial commission due*/
		if (isNE(wsaaBascpyDue, 0)) {
			lifacmvrec.jrnseq.add(1);
			wsaaGlSub.set(8);
			wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
			wsaaRldgLife.set(agcmbchIO.getLife());
			wsaaRldgCoverage.set(agcmbchIO.getCoverage());
			wsaaRldgRider.set(agcmbchIO.getRider());
			wsaaPlan.set(agcmbchIO.getPlanSuffix());
			wsaaRldgPlanSuff.set(wsaaPlansuff);
			lifacmvrec.tranref.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.origamt.set(wsaaBascpyDue);
			lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
			callLifacmv6000();
			/*ILIFE-4941--  Post the Fees.		*/
			if (isGT(payrIO.getSinstamt02(), 0)
					&& 	isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")
					&& isNE(wsaaT5688Revacc[wsaaT5688Ix.toInt()], "Y")) {
						lifacmvrec.origamt.set(payrIO.getSinstamt02());
						wsaaGlSub.set(32);
						callLifacmv6000();
					}
		
			/*ILIFE-4941 ends*/
			/* Override Commission                                             */
			if (isEQ(th605rec.indic, "Y")) {
				zorlnkrec.annprem.set(agcmbchIO.getAnnprem());
				zorlnkrec.effdate.set(agcmbchIO.getEfdate());
				b600CallZorcompy();
			}
		}
		/*  Initial commission earned*/
		if (isNE(wsaaBascpyErn, 0)) {
			lifacmvrec.jrnseq.add(1);
			lifacmvrec.origamt.set(wsaaBascpyErn);
			if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")) {
				wsaaGlSub.set(23);
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
			}
			else {
				wsaaGlSub.set(9);
				lifacmvrec.rldgacct.set(fprxpfRec.chdrnum);
			}
			lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
			callLifacmv6000();
		}
		/*  Initial commission paid*/
		if (isNE(wsaaBascpyPay, 0)) {
			lifacmvrec.origamt.set(wsaaBascpyPay);
			lifacmvrec.jrnseq.add(1);
			if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")) {
				wsaaGlSub.set(24);
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
			}
			else {
				wsaaGlSub.set(10);
				lifacmvrec.rldgacct.set(fprxpfRec.chdrnum);
			}
			lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
			callLifacmv6000();
		}
	}

protected void postServiceCommission8000()
	{
		start8010();
	}

protected void start8010()
	{
		/*  Service commission due*/
		if (isNE(wsaaSrvcpyDue, 0)) {
			lifacmvrec.jrnseq.add(1);
			lifacmvrec.origamt.set(wsaaSrvcpyDue);
			/*       MOVE 9                      TO WSAA-GL-SUB                */
			wsaaGlSub.set(11);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
			lifacmvrec.tranref.set(fprxpfRec.chdrnum);
			callLifacmv6000();
			/* Override Commission                                             */
			if (isEQ(th605rec.indic, "Y")) {
				zorlnkrec.annprem.set(agcmbchIO.getAnnprem());
				zorlnkrec.effdate.set(agcmbchIO.getEfdate());
				b600CallZorcompy();
			}
		}
		/*  Service commission earned*/
		if (isNE(wsaaSrvcpyErn, 0)) {
			lifacmvrec.jrnseq.add(1);
			lifacmvrec.origamt.set(wsaaSrvcpyErn);
			if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")) {
				wsaaGlSub.set(25);
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
			}
			else {
				wsaaGlSub.set(12);
				lifacmvrec.substituteCode[6].set(SPACES);
				lifacmvrec.rldgacct.set(fprxpfRec.chdrnum);
			}
			lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
			callLifacmv6000();
		}
	}

protected void postRenewalCommission9000()
	{
		start9010();
	}

protected void start9010()
	{
		/*  Renewal commission due*/
		if (isNE(wsaaRnwcpyDue, 0)) {
			lifacmvrec.jrnseq.add(1);
			wsaaGlSub.set(13);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.origamt.set(wsaaRnwcpyDue);
			lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
			/*       MOVE CHDRNUM                   TO LIFA-TRANREF            */
			wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
			wsaaRldgLife.set(agcmbchIO.getLife());
			wsaaRldgCoverage.set(agcmbchIO.getCoverage());
			wsaaRldgRider.set(agcmbchIO.getRider());
			wsaaPlan.set(agcmbchIO.getPlanSuffix());
			wsaaRldgPlanSuff.set(wsaaPlansuff);
			lifacmvrec.tranref.set(wsaaRldgacct);
			callLifacmv6000();
			/* Override Commission                                             */
			if (isEQ(th605rec.indic, "Y")) {
				zorlnkrec.annprem.set(agcmbchIO.getAnnprem());
				zorlnkrec.effdate.set(agcmbchIO.getEfdate());
				b600CallZorcompy();
			}
		}
		/*  Renewal commission earned*/
		if (isNE(wsaaRnwcpyErn, 0)) {
			lifacmvrec.origamt.set(wsaaRnwcpyErn);
			lifacmvrec.jrnseq.add(1);
			if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")) {
				wsaaGlSub.set(26);
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
			}
			else {
				wsaaGlSub.set(14);
				lifacmvrec.substituteCode[6].set(SPACES);
				lifacmvrec.rldgacct.set(fprxpfRec.chdrnum);
			}
			lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
			callLifacmv6000();
		}
	}

protected void postOverridCommission10000()
	{
		start10100();
	}

protected void start10100()
	{
		/*  Initial over-riding commission due*/
		if (isNE(wsaaOvrdBascpyDue, 0)) {
			lifacmvrec.jrnseq.add(1);
			wsaaGlSub.set(16);
			lifacmvrec.origamt.set(wsaaOvrdBascpyDue);
			lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.tranref.set(agcmbchIO.getCedagent());
			callLifacmv6000();
		}
		/*  Initial over-riding commission earned*/
		if (isNE(wsaaOvrdBascpyErn, 0)) {
			lifacmvrec.origamt.set(wsaaOvrdBascpyErn);
			lifacmvrec.jrnseq.add(1);
			if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")) {
				wsaaGlSub.set(27);
				lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
			}
			else {
				wsaaGlSub.set(17);
				lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
				lifacmvrec.rldgacct.set(fprxpfRec.chdrnum);
				lifacmvrec.substituteCode[6].set(SPACES);
			}
			callLifacmv6000();
		}
		/*  Initial over-riding commission paid*/
		if (isNE(wsaaOvrdBascpyPay, 0)) {
			lifacmvrec.jrnseq.add(1);
			lifacmvrec.origamt.set(wsaaOvrdBascpyPay);
			if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")) {
				wsaaGlSub.set(28);
				lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
			}
			else {
				wsaaGlSub.set(18);
				lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
				lifacmvrec.substituteCode[6].set(SPACES);
				lifacmvrec.rldgacct.set(chdrlifIO.getChdrnum());
			}
			callLifacmv6000();
		}
	}

	/**
	* <pre>
	*11000-WRITE-VARIANCE-REPORT SECTION.
	*11010-START.
	* Get company description for report header.
	*    MOVE 'IT'                   TO DESC-DESCPFX.
	*    MOVE '0'                    TO DESC-DESCCOY.
	*    MOVE T1693                  TO DESC-DESCTABL.
	*    MOVE BSPR-COMPANY           TO DESC-DESCITEM.
	*    MOVE BSSC-LANGUAGE          TO DESC-LANGUAGE.
	*    MOVE READR                  TO DESC-FUNCTION.
	*    CALL 'DESCIO' USING DESC-PARAMS.
	*    IF DESC-STATUZ             NOT = O-K
	*       MOVE SPACES              TO DESC-LONGDESC
	*    END-IF
	*    MOVE PAYR-BTDATE            TO WSAA-TEMP-DATE
	*    PERFORM 11700-CALL-DATCON1
	*    IF FAIL-MAXIMUM-TEST
	*       IF NEW-PAGE-REQ-2
	*          PERFORM 11100-WRITE-MAX-HEADER
	*       END-IF
	*       MOVE CHDRNUM              TO RD01-CHDRNUM-2
	*       MOVE PAYR-SINSTAMT06      TO RD01-INSTPREM-2
	*       MOVE WSAA-AFTER-PREM-PAID TO RD01-AMT-RECD-2
	*       MOVE WSAA-FORMATTED-DATE  TO RD01-BTDATE-2
	*       MOVE WSAA-MAX-ALLOWED     TO RD01-MAX-AMOUNT-2
	*       ADD WSAA-AFTER-PREM-PAID  TO WSAA-TOT-MAX-FAIL
	*       PERFORM 11200-WRITE-MAXIMUM-REPORT
	*       MOVE CT10                 TO CONT-TOTNO
	*       MOVE 1                    TO CONT-TOTVAL
	*       PERFORM 001-CALL-CONTOT
	*       MOVE CT12                 TO CONT-TOTNO
	*       MOVE WSAA-AFTER-PREM-PAID TO CONT-TOTVAL
	*       PERFORM 001-CALL-CONTOT
	*    ELSE
	*       IF NEW-PAGE-REQ-1
	*          PERFORM 11300-WRITE-MIN-HEADER
	*       END-IF
	*       MOVE CHDRNUM             TO RD01-CHDRNUM
	*       MOVE PAYR-SINSTAMT06     TO RD01-INSTPREM
	*       MOVE WSAA-SUSP-AVAIL     TO RD01-AMT-RECD
	*       MOVE WSAA-FORMATTED-DATE TO RD01-BTDATE
	*       MOVE WSAA-MIN-ALLOWED    TO RD01-MIN-AMOUNT
	*       ADD WSAA-SUSP-AVAIL      TO WSAA-TOT-MIN-FAIL
	*       PERFORM 11400-WRITE-MINIMUM-REPORT
	*       MOVE CT09                TO CONT-TOTNO
	*       MOVE 1                   TO CONT-TOTVAL
	*       PERFORM 001-CALL-CONTOT
	*       MOVE CT11                TO CONT-TOTNO
	*       MOVE WSAA-SUSP-AVAIL     TO CONT-TOTVAL
	*       PERFORM 001-CALL-CONTOT
	*    END-IF.
	*11090-EXIT.
	*    EXIT.
	*11100-WRITE-MAX-HEADER SECTION.
	*11110-START.
	*    MOVE BSPR-COMPANY           TO RH01-COMPANY-2
	*    MOVE DESC-LONGDESC          TO RH01-COMPANYNM-2
	*    WRITE PRINTER-REC2 FROM R5359H01-RECORD
	*          FORMAT 'R5359H01'
	*    MOVE 'N'                    TO WSAA-OVERFLOW-2.
	*11190-EXIT.
	*    EXIT.
	*11200-WRITE-MAXIMUM-REPORT SECTION.
	*11210-START.
	*    WRITE PRINTER-REC2 FROM R5359D01-RECORD
	*                       FORMAT 'R5359D01'.
	*    SET MAX-REPORT-WRITTEN        TO TRUE.
	*11290-EXIT.
	*    EXIT.
	*11300-WRITE-MIN-HEADER SECTION.
	*11310-START.
	*    MOVE BSPR-COMPANY           TO RH01-COMPANY
	*    MOVE DESC-LONGDESC          TO RH01-COMPANYNM
	*    WRITE PRINTER-REC1 FROM R5358H01-RECORD
	*          FORMAT 'R5358H01'
	*    MOVE 'N'                    TO WSAA-OVERFLOW-1.
	*11390-EXIT.
	*    EXIT.
	*11400-WRITE-MINIMUM-REPORT SECTION.
	*11410-START.
	*    WRITE PRINTER-REC1 FROM R5358D01-RECORD
	*                       FORMAT 'R5358D01'.
	*    SET MIN-REPORT-WRITTEN          TO TRUE.
	*11490-EXIT.
	*    EXIT.
	*11500-WRITE-MAX-TOTALS-REPORT SECTION.
	*11510-START.
	*    MOVE WSAA-TOT-MAX-FAIL      TO RT01-TOT-RECD-2
	*    WRITE PRINTER-REC2 FROM R5359T01-RECORD
	*          FORMAT 'R5359T01'.
	*    MOVE 'Y'                         TO WSAA-OVERFLOW-2.
	*11590-EXIT.
	*    EXIT.
	*11600-WRITE-MIN-TOTALS-REPORT SECTION.
	*11610-START.
	*    MOVE WSAA-TOT-MIN-FAIL      TO RT01-TOT-RECD
	*    WRITE PRINTER-REC1 FROM R5358T01-RECORD
	*          FORMAT 'R5358T01'.
	*    MOVE 'Y'                         TO WSAA-OVERFLOW-1.
	*11690-EXIT.
	*    EXIT.
	*11700-CALL-DATCON1 SECTION.
	*11710-START.
	*    MOVE WSAA-TEMP-DATE              TO DTC1-INT-DATE.
	*    MOVE CONV                        TO DTC1-FUNCTION.
	*    CALL 'DATCON1' USING DTC1-DATCON1-REC.
	*    IF DTC1-STATUZ              NOT = O-K
	*       MOVE DTC1-DATCON1-REC         TO SYSR-PARAMS
	*       MOVE DTC1-STATUZ              TO SYSR-STATUZ
	*       PERFORM 600-FATAL-ERROR.
	*    MOVE DTC1-EXT-DATE               TO WSAA-FORMATTED-DATE.
	*11790-EXIT.
	*    EXIT.
	* </pre>
	*/
protected void writeFrepRecord12000()
	{
		para12000();
	}

protected void para12000()
	{
		frepIO.setParams(SPACES);
		frepIO.setPlanSuffix(ZERO);
		frepIO.setTargetPremium(ZERO);
		frepIO.setPremRecPer(ZERO);
		frepIO.setBilledInPeriod(ZERO);
		frepIO.setOverdueMin(ZERO);
		frepIO.setPayrseqno(ZERO);
		frepIO.setMinprem(ZERO);
		frepIO.setMaxprem(ZERO);
		frepIO.setMinOverduePer(ZERO);
		frepIO.setTargfrom(varcom.maxdate);
		frepIO.setAnnivProcDate(varcom.maxdate);
		frepIO.setTargto(varcom.maxdate);
		frepIO.setCompany(bsprIO.getCompany());
		frepIO.setChdrnum(fprxpfRec.chdrnum);
		frepIO.setPayrseqno(fprxpfRec.payrseqno);
		frepIO.setProcflag("C");
		frepIO.setBtdate(payrIO.getBtdate());
		frepIO.setLinstamt(payrIO.getSinstamt06());
		if (failMaximumTest.isTrue()) {
			frepIO.setMaxprem(wsaaMaxAllowed);
			frepIO.setSusamt(wsaaAfterPremPaid);
		}
		else {
			frepIO.setSusamt(wsaaSuspAvail);
			frepIO.setMinprem(wsaaMinAllowed);
		}
		r5358printRecordInner.rd01Chdrnum.set(fprxpfRec.chdrnum);
		r5359printRecordInner.rd01Chdrnum.set(fprxpfRec.chdrnum);
		r5358printRecordInner.rd01Btdate.set(payrIO.getBtdate());
		r5359printRecordInner.rd01Btdate.set(payrIO.getBtdate());
		r5358printRecordInner.rd01Linstamt.set(payrIO.getSinstamt06());
		r5359printRecordInner.rd01Linstamt.set(payrIO.getSinstamt06());
		r5358printRecordInner.rd01MinPrem.set(wsaaMinAllowed);
		r5359printRecordInner.rd01MaxPrem.set(wsaaMaxAllowed);
		r5358printRecordInner.rd01SusAmt.set(wsaaSuspAvail);// ILIFE-4707
		r5359printRecordInner.rd01SusAmt.set(wsaaAfterPremPaid);		
		frepIO.setFormat(formatsInner.freprec);
		frepIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, frepIO);
		if (isNE(frepIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(frepIO.getParams());
			fatalError600();
		}
		wsaaFrepCreated.set("Y");
		contotrec.totno.set(controlTotalsInner.ct15);
		contotrec.totval.set(1);
		callContot001();
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*START*/
		fprxpf.close();
		/*  CLOSE PRINTER-FILE1                                          */
		/*  CLOSE PRINTER-FILE2                                          */
		wsaaQcmdexc.set("DLTOVR FILE(FPRXPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		minVarReport.close();
		maxVarReport.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		/* MOVE PAYR-BILLCURR          TO ZRDP-CURRENCY.                */
		zrdecplrec.batctrcde.set(batcdorrec.trcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}

protected void b100InitializeArrays()
	{
		/*B110-INIT*/
		for (wsaaAgcmIx.set(1); !(isGT(wsaaAgcmIx, wsaaAgcmIxSize)); wsaaAgcmIx.add(1)){
			wsaaAgcmSummaryInner.wsaaAgcmChdrcoy[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmSummaryInner.wsaaAgcmChdrnum[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmSummaryInner.wsaaAgcmLife[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmSummaryInner.wsaaAgcmCoverage[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmSummaryInner.wsaaAgcmRider[wsaaAgcmIx.toInt()].set(SPACES);
			for (wsaaAgcmIy.set(1); !(isGT(wsaaAgcmIy, wsaaAgcmIySize)); wsaaAgcmIy.add(1)){
				wsaaAgcmSummaryInner.wsaaAgcmSeqno[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(ZERO);
				wsaaAgcmSummaryInner.wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(ZERO);
				wsaaAgcmSummaryInner.wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(ZERO);
				wsaaAgcmSummaryInner.wsaaAgcmRecprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(ZERO);
			}
		}
		/*B190-EXIT*/
	}

protected void b200PremiumHistory()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					b210Init();
				case b220Call:
					b220Call();
					b230Summary();
				case b270Next:
					b270Next();
				case b290Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b210Init()
	{
		/* Read the AGCM for differentiating the First & Renewal Year      */
		/* Premium                                                         */
		agcmseqIO.setDataArea(SPACES);
		agcmseqIO.setChdrcoy(covrlnbIO.getChdrcoy());
		agcmseqIO.setChdrnum(covrlnbIO.getChdrnum());
		agcmseqIO.setLife(covrlnbIO.getLife());
		agcmseqIO.setCoverage(covrlnbIO.getCoverage());
		agcmseqIO.setRider(covrlnbIO.getRider());
		agcmseqIO.setPlanSuffix(covrlnbIO.getPlanSuffix());
		agcmseqIO.setSeqno(ZERO);
		agcmseqIO.setAgntnum(SPACES);
		agcmseqIO.setFormat("AGCMSEQREC");
		agcmseqIO.setFunction(varcom.begn);
	}

protected void b220Call()
	{
		SmartFileCode.execute(appVars, agcmseqIO);
		if (isNE(agcmseqIO.getStatuz(), varcom.oK)
		&& isNE(agcmseqIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(agcmseqIO.getStatuz());
			syserrrec.params.set(agcmseqIO.getParams());
			fatalError600();
		}
		if (isEQ(agcmseqIO.getStatuz(), varcom.endp)
		|| isNE(agcmseqIO.getChdrcoy(), covrlnbIO.getChdrcoy())
		|| isNE(agcmseqIO.getChdrnum(), covrlnbIO.getChdrnum())
		|| isNE(agcmseqIO.getLife(), covrlnbIO.getLife())
		|| isNE(agcmseqIO.getCoverage(), covrlnbIO.getCoverage())
		|| isNE(agcmseqIO.getRider(), covrlnbIO.getRider())) {
			agcmbchIO.setStatuz(varcom.endp);
			goTo(GotoLabel.b290Exit);
		}
		/* Skip those irrelevant records                                   */
		if (isNE(agcmseqIO.getOvrdcat(), "B")
		|| isEQ(agcmseqIO.getPtdate(), ZERO)
		|| isEQ(agcmseqIO.getEfdate(), ZERO)
		|| isEQ(agcmseqIO.getEfdate(), varcom.vrcmMaxDate)) {
			goTo(GotoLabel.b270Next);
		}
	}

protected void b230Summary()
	{
		wsaaAgcmFound.set("N");
		for (wsaaAgcmIy.set(1); !(isGT(wsaaAgcmIy, wsaaAgcmIySize)
		|| isEQ(wsaaAgcmSummaryInner.wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], ZERO)
		|| agcmFound.isTrue()); wsaaAgcmIy.add(1)){
			if (isEQ(agcmseqIO.getSeqno(), wsaaAgcmSummaryInner.wsaaAgcmSeqno[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()])) {
				wsaaAgcmSummaryInner.wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].add(agcmseqIO.getAnnprem());
				agcmFound.setTrue();
			}
		}
		if (!agcmFound.isTrue()) {
			if (isGT(wsaaAgcmIy, wsaaAgcmIySize)) {
				syserrrec.statuz.set(e103);
				fatalError600();
			}
			wsaaAgcmSummaryInner.wsaaAgcmSeqno[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(agcmseqIO.getSeqno());
			wsaaAgcmSummaryInner.wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(agcmseqIO.getEfdate());
			wsaaAgcmSummaryInner.wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(agcmseqIO.getAnnprem());
		}
	}

protected void b270Next()
	{
		agcmseqIO.setFunction(varcom.nextr);
		goTo(GotoLabel.b220Call);
	}

protected void b300WriteArrays()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					b310Init();
				case b320Locate:
					b320Locate();
				case b330Loop:
					b330Loop();
					b340Writ();
					b380Next();
				case b390Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b310Init()
	{
		wsaaAgcmIy.set(1);
		wsaaAgcmPremLeft.set(wsaaUpToTgt);
		wsaaOutstandingPremium.set(ZERO);
		firstTime.setTrue();
	}

protected void b320Locate()
	{
		if (isEQ(wsaaAgcmSummaryInner.wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], ZERO)) {
			goTo(GotoLabel.b390Exit);
		}
		if (isGTE(wsaaAgcmSummaryInner.wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], fpcoIO.getTargto())) {
			wsaaAgcmIy.add(1);
			goTo(GotoLabel.b320Locate);
		}
		/*  Locate the current AGCM that premium is allocated              */
		if (isLTE(wsaaAgcmSummaryInner.wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], wsaaReceivedPremium)) {
			compute(wsaaReceivedPremium, 2).set(sub(wsaaReceivedPremium, wsaaAgcmSummaryInner.wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()]));
			wsaaAgcmIy.add(1);
			goTo(GotoLabel.b320Locate);
		}
	}

protected void b330Loop()
	{
		if (firstTime.isTrue()) {
			compute(wsaaOutstandingPremium, 2).set(sub(wsaaAgcmSummaryInner.wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], wsaaReceivedPremium));
			wsaaFirstTime.set("N");
		}
		else {
			wsaaOutstandingPremium.set(wsaaAgcmSummaryInner.wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()]);
		}
		if (isEQ(wsaaAgcmPremLeft, ZERO)) {
			goTo(GotoLabel.b390Exit);
		}
	}

protected void b340Writ()
	{
		initialize(datcon3rec.datcon3Rec);
		datcon3rec.intDate1.set(wsaaAgcmSummaryInner.wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()]);
		datcon3rec.intDate2.set(fpcoIO.getTargfrom());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		if (isGTE(datcon3rec.freqFactor, 1)) {
			renPrem.setTrue();
		}
		else {
			firstPrem.setTrue();
		}
		if (isNE(wsaaAgcmPremLeft, ZERO)) {
			zptnIO.setParams(SPACES);
			zptnIO.setChdrcoy(covrlnbIO.getChdrcoy());
			zptnIO.setChdrnum(covrlnbIO.getChdrnum());
			zptnIO.setLife(covrlnbIO.getLife());
			zptnIO.setCoverage(covrlnbIO.getCoverage());
			zptnIO.setRider(covrlnbIO.getRider());
			zptnIO.setTranno(chdrlifIO.getTranno());
			if (isGTE(wsaaOutstandingPremium, wsaaAgcmPremLeft)) {
				zptnIO.setOrigamt(wsaaAgcmPremLeft);
				wsaaAgcmPremLeft.set(ZERO);
			}
			else {
				zptnIO.setOrigamt(wsaaOutstandingPremium);
				compute(wsaaAgcmPremLeft, 2).set(sub(wsaaAgcmPremLeft, wsaaOutstandingPremium));
			}
			wsaaAgcmSummaryInner.wsaaAgcmRecprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(zptnIO.getOrigamt());
			zptnIO.setTransCode(bprdIO.getAuthCode());
			if (isGTE(linsrnlIO.getInstfrom(), fpcoIO.getTargto())) {
				zptnIO.setEffdate(fpcoIO.getTargto());
				zptnIO.setInstfrom(fpcoIO.getTargto());
			}
			else {
				zptnIO.setEffdate(linsrnlIO.getInstfrom());
				zptnIO.setInstfrom(linsrnlIO.getInstfrom());
			}
			zptnIO.setBillcd(linsrnlIO.getBillcd());
			if (isGTE(linsrnlIO.getInstto(), fpcoIO.getTargto())) {
				zptnIO.setInstto(fpcoIO.getTargto());
			}
			else {
				zptnIO.setInstto(linsrnlIO.getInstto());
			}
			zptnIO.setTrandate(bsscIO.getEffectiveDate());
			zptnIO.setZprflg(wsaaPremType);
			zptnIO.setFormat(zptnrec);
			zptnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, zptnIO);
			if (isNE(zptnIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(zptnIO.getStatuz());
				syserrrec.params.set(zptnIO.getParams());
				fatalError600();
			}
		}
	}

protected void b380Next()
	{
		wsaaAgcmIy.add(1);
		goTo(GotoLabel.b330Loop);
	}

protected void b400LocatePremium()
	{
		/*B410-INIT*/
		wsaaAgcmFound.set("N");
		wsaaAgcmPremium.set(ZERO);
		wsaaAgcmAlcprem.set(ZERO);
		for (wsaaAgcmIa.set(1); !(isGT(wsaaAgcmIa, wsaaAgcmIxSize)
		|| agcmFound.isTrue()); wsaaAgcmIa.add(1)){
			if (isEQ(agcmbchIO.getChdrcoy(), wsaaAgcmSummaryInner.wsaaAgcmChdrcoy[wsaaAgcmIa.toInt()])
			&& isEQ(agcmbchIO.getChdrnum(), wsaaAgcmSummaryInner.wsaaAgcmChdrnum[wsaaAgcmIa.toInt()])
			&& isEQ(agcmbchIO.getLife(), wsaaAgcmSummaryInner.wsaaAgcmLife[wsaaAgcmIa.toInt()])
			&& isEQ(agcmbchIO.getCoverage(), wsaaAgcmSummaryInner.wsaaAgcmCoverage[wsaaAgcmIa.toInt()])
			&& isEQ(agcmbchIO.getRider(), wsaaAgcmSummaryInner.wsaaAgcmRider[wsaaAgcmIa.toInt()])) {
				for (wsaaAgcmIb.set(1); !(isGT(wsaaAgcmIb, wsaaAgcmIySize)
				|| agcmFound.isTrue()); wsaaAgcmIb.add(1)){
					if (isEQ(agcmbchIO.getSeqno(), wsaaAgcmSummaryInner.wsaaAgcmSeqno[wsaaAgcmIa.toInt()][wsaaAgcmIb.toInt()])) {
						agcmFound.setTrue();
						wsaaAgcmPremium.set(wsaaAgcmSummaryInner.wsaaAgcmAnnprem[wsaaAgcmIa.toInt()][wsaaAgcmIb.toInt()]);
						wsaaAgcmAlcprem.set(wsaaAgcmSummaryInner.wsaaAgcmRecprem[wsaaAgcmIa.toInt()][wsaaAgcmIb.toInt()]);
					}
				}
			}
		}
		/*B490-EXIT*/
	}

protected void b500WriteOverTarget()
	{
		b510Writ();
	}

protected void b510Writ()
	{
		initialize(datcon3rec.datcon3Rec);
		datcon3rec.intDate1.set(covrlnbIO.getCrrcd());
		datcon3rec.intDate2.set(fpcoIO.getTargfrom());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		if (isGTE(datcon3rec.freqFactor, 1)) {
			renPrem.setTrue();
		}
		else {
			firstPrem.setTrue();
		}
		zptnIO.setParams(SPACES);
		zptnIO.setChdrcoy(covrlnbIO.getChdrcoy());
		zptnIO.setChdrnum(covrlnbIO.getChdrnum());
		zptnIO.setLife(covrlnbIO.getLife());
		zptnIO.setCoverage(covrlnbIO.getCoverage());
		zptnIO.setRider(covrlnbIO.getRider());
		zptnIO.setTranno(chdrlifIO.getTranno());
		zptnIO.setOrigamt(wsaaOvrTgt);
		zptnIO.setTransCode(bprdIO.getAuthCode());
		if (isGTE(linsrnlIO.getInstfrom(), fpcoIO.getTargto())) {
			zptnIO.setEffdate(fpcoIO.getTargto());
			zptnIO.setInstfrom(fpcoIO.getTargto());
		}
		else {
			zptnIO.setEffdate(linsrnlIO.getInstfrom());
			zptnIO.setInstfrom(linsrnlIO.getInstfrom());
		}
		zptnIO.setBillcd(linsrnlIO.getBillcd());
		if (isGTE(linsrnlIO.getInstto(), fpcoIO.getTargto())) {
			zptnIO.setInstto(fpcoIO.getTargto());
		}
		else {
			zptnIO.setInstto(linsrnlIO.getInstto());
		}
		zptnIO.setTrandate(bsscIO.getEffectiveDate());
		zptnIO.setZprflg(wsaaPremType);
		zptnIO.setFormat(zptnrec);
		zptnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zptnIO);
		if (isNE(zptnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zptnIO.getStatuz());
			syserrrec.params.set(zptnIO.getParams());
			fatalError600();
		}
	}

protected void b600CallZorcompy()
	{
		b610Start();
	}

protected void b610Start()
	{
		zorlnkrec.function.set(SPACES);
		zorlnkrec.clawback.set(SPACES);
		zorlnkrec.agent.set(lifacmvrec.rldgacct);
		zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
		zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
		zorlnkrec.crtable.set(lifacmvrec.substituteCode[6]);
		zorlnkrec.ptdate.set(chdrlifIO.getPtdate());
		zorlnkrec.origcurr.set(lifacmvrec.origcurr);
		zorlnkrec.crate.set(lifacmvrec.crate);
		zorlnkrec.origamt.set(lifacmvrec.origamt);
		zorlnkrec.tranno.set(chdrlifIO.getTranno());
		zorlnkrec.trandesc.set(lifacmvrec.trandesc);
		zorlnkrec.tranref.set(lifacmvrec.tranref);
		zorlnkrec.genlcur.set(lifacmvrec.genlcur);
		zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
		zorlnkrec.termid.set(lifacmvrec.termid);
		zorlnkrec.cnttype.set(lifacmvrec.substituteCode[1]);
		zorlnkrec.batchKey.set(lifacmvrec.batckey);
		zorlnkrec.clawback.set(SPACES);
		callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
		if (isNE(zorlnkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zorlnkrec.statuz);
			syserrrec.params.set(zorlnkrec.zorlnkRec);
			fatalError600();
		}
	}

protected void b700PropTax()
	{
		b700Start();
	}

	/**
	* <pre>
	* this section will prorate the tax in proportion to the
	* net premium
	* </pre>
	*/
protected void b700Start()
	{
		if (isEQ(wsaaVariablesInner.wsaaLife[wsaaVariablesInner.wsaaIx.toInt()], SPACES)
		&& isEQ(wsaaVariablesInner.wsaaCoverage[wsaaVariablesInner.wsaaIx.toInt()], SPACES)
		&& isEQ(wsaaVariablesInner.wsaaRider[wsaaVariablesInner.wsaaIx.toInt()], SPACES)) {
			wsaaEndPost.set("Y");
			return ;
		}
		if (isEQ(covrlnbIO.getLife(), wsaaVariablesInner.wsaaLife[wsaaVariablesInner.wsaaIx.toInt()])
		&& isEQ(covrlnbIO.getCoverage(), wsaaVariablesInner.wsaaCoverage[wsaaVariablesInner.wsaaIx.toInt()])
		&& isEQ(covrlnbIO.getRider(), wsaaVariablesInner.wsaaRider[wsaaVariablesInner.wsaaIx.toInt()])
		&& isEQ(covrlnbIO.getPlanSuffix(), wsaaVariablesInner.wsaaPlanSuffix[wsaaVariablesInner.wsaaIx.toInt()])) {
			if (isNE(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][1], 0)) {
				compute(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][1], 2).set(mult(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][1], (div(wsaaVariablesInner.wsaaPremNet, payrIO.getSinstamt01()))));
				if (isNE(wsaaVariablesInner.wsaaTaxAbs[wsaaVariablesInner.wsaaIx.toInt()][1], "Y")) {
					wsaaVariablesInner.wsaaTotTaxCharged.add(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][1]);
				}
			}
			if (isNE(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][2], 0)) {
				compute(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][2], 2).set(mult(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][2], (div(wsaaVariablesInner.wsaaPremNet, payrIO.getSinstamt01()))));
				if (isNE(wsaaVariablesInner.wsaaTaxAbs[wsaaVariablesInner.wsaaIx.toInt()][2], "Y")) {
					wsaaVariablesInner.wsaaTotTaxCharged.add(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][2]);
				}
			}
			/*   ensure that the premium amount + tax is equal                */
			/*   to the amount in suspense                                    */
			if ((setPrecision(wsaaVariablesInner.wsaaOrigSusp, 2)
			&& isNE((add(wsaaPolamnt, wsaaVariablesInner.wsaaTotTaxCharged)), wsaaVariablesInner.wsaaOrigSusp))) {
				compute(wsaaVariablesInner.wsaaDiff, 2).set(sub(sub(wsaaVariablesInner.wsaaOrigSusp, wsaaPolamnt), wsaaVariablesInner.wsaaTotTaxCharged));
				/*   add/subtract the rounding differences to the tax amount      */
				if (isNE(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][1], 0)) {
					wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][1].add(wsaaVariablesInner.wsaaDiff);
				}
				else {
					wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][2].add(wsaaVariablesInner.wsaaDiff);
				}
			}
		}
	}

protected void b750PostTax()
	{
		b750Start();
	}

protected void b750Start()
	{
		if (isEQ(wsaaVariablesInner.wsaaLife[wsaaVariablesInner.wsaaIx.toInt()], SPACES)
		&& isEQ(wsaaVariablesInner.wsaaCoverage[wsaaVariablesInner.wsaaIx.toInt()], SPACES)
		&& isEQ(wsaaVariablesInner.wsaaRider[wsaaVariablesInner.wsaaIx.toInt()], SPACES)) {
			wsaaEndPost.set("Y");
			return ;
		}
		if (isEQ(covrlnbIO.getLife(), wsaaVariablesInner.wsaaLife[wsaaVariablesInner.wsaaIx.toInt()])
		&& isEQ(covrlnbIO.getCoverage(), wsaaVariablesInner.wsaaCoverage[wsaaVariablesInner.wsaaIx.toInt()])
		&& isEQ(covrlnbIO.getRider(), wsaaVariablesInner.wsaaRider[wsaaVariablesInner.wsaaIx.toInt()])
		&& isEQ(covrlnbIO.getPlanSuffix(), wsaaVariablesInner.wsaaPlanSuffix[wsaaVariablesInner.wsaaIx.toInt()])
		&& (isNE(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][1], 0)
		|| isNE(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][2], 0))) {
			initialize(txcalcrec.linkRec);
			txcalcrec.function.set("POST");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.transType.set("PREM");
			txcalcrec.chdrcoy.set(chdrlifIO.getChdrcoy());
			txcalcrec.chdrnum.set(chdrlifIO.getChdrnum());
			txcalcrec.life.set(covrlnbIO.getLife());
			txcalcrec.coverage.set(covrlnbIO.getCoverage());
			txcalcrec.rider.set(covrlnbIO.getRider());
			txcalcrec.planSuffix.set(covrlnbIO.getPlanSuffix());
			txcalcrec.crtable.set(covrlnbIO.getCrtable());
			txcalcrec.cnttype.set(chdrlifIO.getCnttype());
			txcalcrec.register.set(chdrlifIO.getCnttype());
			txcalcrec.taxrule.set(wsaaVariablesInner.wsaaTaxRule[wsaaVariablesInner.wsaaIx.toInt()]);
			txcalcrec.rateItem.set(wsaaVariablesInner.wsaaTaxItem[wsaaVariablesInner.wsaaIx.toInt()]);
			txcalcrec.amountIn.set(wsaaPolamnt);
			txcalcrec.effdate.set(bsscIO.getEffectiveDate());
			txcalcrec.taxType[1].set(wsaaVariablesInner.wsaaTaxType[wsaaVariablesInner.wsaaIx.toInt()][1]);
			txcalcrec.taxType[2].set(wsaaVariablesInner.wsaaTaxType[wsaaVariablesInner.wsaaIx.toInt()][2]);
			txcalcrec.taxAbsorb[1].set(wsaaVariablesInner.wsaaTaxAbs[wsaaVariablesInner.wsaaIx.toInt()][1]);
			txcalcrec.taxAbsorb[2].set(wsaaVariablesInner.wsaaTaxAbs[wsaaVariablesInner.wsaaIx.toInt()][2]);
			txcalcrec.taxAmt[1].set(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][1]);
			txcalcrec.taxAmt[2].set(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][2]);
			txcalcrec.jrnseq.set(lifacmvrec.jrnseq);
			txcalcrec.jrnseq.add(1);
			txcalcrec.batckey.set(batcdorrec.batchkey);
			txcalcrec.ccy.set(chdrlifIO.getCntcurr());
			txcalcrec.tranno.set(chdrlifIO.getTranno());
			txcalcrec.language.set(bsscIO.getLanguage());
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			b90aCreateTaxd();
		}
	}

protected void b800ReadTr52d()
	{
		b810Start();
	}

protected void b810Start()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlifIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrlifIO.getRegister());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(chdrlifIO.getChdrcoy());
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFormat(formatsInner.itemrec);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
	}

protected void b900ReadTr52e()
	{
		b910Start();
	}

protected void b910Start()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(fprxpfRec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(bsscIO.getEffectiveDate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isNE(itdmIO.getItemcoy(), fprxpfRec.chdrcoy))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.params.set(wsaaTr52eKey);
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isEQ(itdmIO.getItemcoy(), fprxpfRec.chdrcoy))
		&& (isEQ(itdmIO.getItemtabl(), tablesInner.tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}

protected void b90aCreateTaxd()
	{
		b90aStart();
	}

protected void b90aStart()
	{
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(chdrlifIO.getChdrcoy());
		taxdIO.setChdrnum(chdrlifIO.getChdrnum());
		taxdIO.setTrantype("PREM");
		taxdIO.setLife(covrlnbIO.getLife());
		taxdIO.setCoverage(covrlnbIO.getCoverage());
		taxdIO.setRider(covrlnbIO.getRider());
		taxdIO.setPlansfx(covrlnbIO.getPlanSuffix());
		taxdIO.setEffdate(txcalcrec.effdate);
		taxdIO.setBillcd(txcalcrec.effdate);
		wsaaVariablesInner.wsaaTranref.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(wsaaVariablesInner.wsaaTranref);
		taxdIO.setTranref(wsaaVariablesInner.wsaaTranref);
		taxdIO.setInstfrom(payrIO.getPtdate());
		taxdIO.setInstto(payrIO.getBtdate());
		taxdIO.setTranno(chdrlifIO.getTranno());
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(ZERO);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxabsind03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setPostflg("P");
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			fatalError600();
		}
	}
protected void checkOverdueprocess()
{
	getT554010300();	 	 
	getT553511100();	
	
}	
protected void getT554010300()
{
	para10310();
}
	
protected void para10310()
{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrlifIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5540);
		itdmIO.setItemitem(wsaaCrtable);
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.mrnf))) 
		{
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}		 
		else 
		{
			t5540rec.t5540Rec.set(itdmIO.getGenarea());
		}
}
protected void getT553511100()
{
		para11110();
}

protected void para11110()
{
	 
		if (isEQ(t5540rec.ltypst,SPACES))
		{
			wsaaLoyaltyApp.set("N");
		}
		else
		{			
			calcLeadDays();
		}
	 
}

protected void calcLeadDays()
{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(payrIO.getPtdate());
		datcon3rec.intDate2.set(bsscIO.getEffectiveDate());
		datcon3rec.frequency.set("DY");
		datcon3rec.freqFactor.set(0);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		wsaaOverdueDays.set(datcon3rec.freqFactor);
		String key = payrIO.getBillchnl().toString().trim().concat(chdrlifIO.getCnttype().toString().trim());
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		if (BTPRO028Permission) {
			key = key.concat(payrIO.getBillfreq().toString().trim());
		}
		wsaaT6654Ix.set(1);
		if (readT6654(key))
		{
			if (isLT(t6654rec.leadDays, wsaaOverdueDays) 	&& isNE(t6654rec.leadDays, 0))
			{
						 
				wsaaLoyaltyApp.set("N");
			}
			else
			{
				wsaaLoyaltyApp.set("Y");
			}
		}
		
	/*	if (isLTE(wsaaT6654ArrayInner.wsaaT6654NonForfDays[wsaaT6654Ix.toInt()], wsaaOverdueDays) 	&& isNE(wsaaT6654ArrayInner.wsaaT6654NonForfDays[wsaaT6654Ix.toInt()], 0)) 
		{
			wsaaLoyaltyApp.set("N");
		}
		else
		{
			wsaaLoyaltyApp.set("Y");
		}
		*/
}

protected boolean readT6654(String searchItem)  
{
			String strEffDate = bsscIO.getEffectiveDate().toString();
			String keyItemitem = searchItem.trim();
			List<Itempf> itempfList = new ArrayList<Itempf>();
			boolean itemFound = false;	
			String itemcoy = bsprIO.getCompany().toString();
			
			t6654ListMap = itemDAO.loadSmartTable("IT", itemcoy, t6654);
			if(null == t6654ListMap || t6654ListMap.isEmpty())
			{
				syserrrec.params.set("t6654");
				fatalError600();
			}
			
			if (t6654ListMap.containsKey(keyItemitem)){	
				itempfList = t6654ListMap.get(keyItemitem);
				Iterator<Itempf> iterator = itempfList.iterator();
				while (iterator.hasNext()) {
					Itempf itempf = new Itempf();
					itempf = iterator.next();
					if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
						if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
								&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
							t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
							
							wsaaT6654ArrayInner.wsaaT6654Key[wsaaT6654Ix.toInt()].set(itempf.getItemitem());
							wsaaT6654ArrayInner.wsaaT6654NonForfDays[wsaaT6654Ix.toInt()].set(t6654rec.nonForfietureDays);
							wsaaT6654ArrayInner.wsaaT6654ArrearsMethod[wsaaT6654Ix.toInt()].set(t6654rec.arrearsMethod);
							for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
								wsaaT6654SubrIx.set(wsaaSub);
								wsaaT6654ArrayInner.wsaaT6654Doctid[wsaaT6654Ix.toInt()][wsaaT6654SubrIx.toInt()].set(t6654rec.doctid[wsaaSub.toInt()]);
							}
							for (wsaaSub.set(1); !(isGT(wsaaSub, 3)); wsaaSub.add(1)){
								wsaaT6654ExpyIx.set(wsaaSub);
								wsaaT6654ArrayInner.wsaaT6654Daexpy[wsaaT6654Ix.toInt()][wsaaT6654ExpyIx.toInt()].set(t6654rec.daexpy[wsaaSub.toInt()]);
							}
							wsaaT6654Ix.add(1);					itemFound = true;
						}
					}else{
						t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
						wsaaT6654ArrayInner.wsaaT6654Key[wsaaT6654Ix.toInt()].set(itempf.getItemitem());
						wsaaT6654ArrayInner.wsaaT6654NonForfDays[wsaaT6654Ix.toInt()].set(t6654rec.nonForfietureDays);
						wsaaT6654ArrayInner.wsaaT6654ArrearsMethod[wsaaT6654Ix.toInt()].set(t6654rec.arrearsMethod);
						for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
							wsaaT6654SubrIx.set(wsaaSub);
							wsaaT6654ArrayInner.wsaaT6654Doctid[wsaaT6654Ix.toInt()][wsaaT6654SubrIx.toInt()].set(t6654rec.doctid[wsaaSub.toInt()]);
						}
						for (wsaaSub.set(1); !(isGT(wsaaSub, 3)); wsaaSub.add(1)){
							wsaaT6654ExpyIx.set(wsaaSub);
							wsaaT6654ArrayInner.wsaaT6654Daexpy[wsaaT6654Ix.toInt()][wsaaT6654ExpyIx.toInt()].set(t6654rec.daexpy[wsaaSub.toInt()]);
						}
						wsaaT6654Ix.add(1);				itemFound = true;					
					}				
				}		
			}
			return itemFound;	
		
}
/*
 * Class transformed  from Data Structure WSAA-T5729-ARRAY--INNER
 */
private static final class WsaaT5729ArrayInner {

		/* WSAA-T5729-ARRAY */
	private FixedLengthStringData[] wsaaT5729Rec = FLSInittedArray (60, 380);
	private FixedLengthStringData[] wsaaT5729Key = FLSDArrayPartOfArrayStructure(3, wsaaT5729Rec, 0);
	private FixedLengthStringData[] wsaaT5729Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5729Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5729Data = FLSDArrayPartOfArrayStructure(377, wsaaT5729Rec, 3);
	private PackedDecimalData[] wsaaT5729Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5729Data, 0);
	private FixedLengthStringData[] wsaaT5729Frqcys = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 5);
	private FixedLengthStringData[][] wsaaT5729Frqcy = FLSDArrayPartOfArrayStructure(6, 2, wsaaT5729Frqcys, 0);
	private FixedLengthStringData[] wsaaT5729Durationas = FLSDArrayPartOfArrayStructure(16, wsaaT5729Data, 17);
	private ZonedDecimalData[][] wsaaT5729Durationa = ZDArrayPartOfArrayStructure(4, 4, 0, wsaaT5729Durationas, 0);
	private FixedLengthStringData[] wsaaT5729Durationbs = FLSDArrayPartOfArrayStructure(16, wsaaT5729Data, 33);
	private ZonedDecimalData[][] wsaaT5729Durationb = ZDArrayPartOfArrayStructure(4, 4, 0, wsaaT5729Durationbs, 0);
	private FixedLengthStringData[] wsaaT5729Durationcs = FLSDArrayPartOfArrayStructure(16, wsaaT5729Data, 49);
	private ZonedDecimalData[][] wsaaT5729Durationc = ZDArrayPartOfArrayStructure(4, 4, 0, wsaaT5729Durationcs, 0);
	private FixedLengthStringData[] wsaaT5729Durationds = FLSDArrayPartOfArrayStructure(16, wsaaT5729Data, 65);
	private ZonedDecimalData[][] wsaaT5729Durationd = ZDArrayPartOfArrayStructure(4, 4, 0, wsaaT5729Durationds, 0);
	private FixedLengthStringData[] wsaaT5729Durationes = FLSDArrayPartOfArrayStructure(16, wsaaT5729Data, 81);
	private ZonedDecimalData[][] wsaaT5729Duratione = ZDArrayPartOfArrayStructure(4, 4, 0, wsaaT5729Durationes, 0);
	private FixedLengthStringData[] wsaaT5729Durationfs = FLSDArrayPartOfArrayStructure(16, wsaaT5729Data, 97);
	private ZonedDecimalData[][] wsaaT5729Durationf = ZDArrayPartOfArrayStructure(4, 4, 0, wsaaT5729Durationfs, 0);
	private FixedLengthStringData[] wsaaT5729TargetMinas = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 113);
	private ZonedDecimalData[][] wsaaT5729TargetMina = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729TargetMinas, 0);
	private FixedLengthStringData[] wsaaT5729TargetMinbs = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 125);
	private ZonedDecimalData[][] wsaaT5729TargetMinb = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729TargetMinbs, 0);
	private FixedLengthStringData[] wsaaT5729TargetMincs = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 137);
	private ZonedDecimalData[][] wsaaT5729TargetMinc = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729TargetMincs, 0);
	private FixedLengthStringData[] wsaaT5729TargetMinds = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 149);
	private ZonedDecimalData[][] wsaaT5729TargetMind = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729TargetMinds, 0);
	private FixedLengthStringData[] wsaaT5729TargetMines = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 161);
	private ZonedDecimalData[][] wsaaT5729TargetMine = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729TargetMines, 0);
	private FixedLengthStringData[] wsaaT5729TargetMinfs = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 173);
	private ZonedDecimalData[][] wsaaT5729TargetMinf = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729TargetMinfs, 0);
	private FixedLengthStringData[] wsaaT5729TargetMaxas = FLSDArrayPartOfArrayStructure(20, wsaaT5729Data, 185);
	private ZonedDecimalData[][] wsaaT5729TargetMaxa = ZDArrayPartOfArrayStructure(4, 5, 0, wsaaT5729TargetMaxas, 0);
	private FixedLengthStringData[] wsaaT5729TargetMaxbs = FLSDArrayPartOfArrayStructure(20, wsaaT5729Data, 205);
	private ZonedDecimalData[][] wsaaT5729TargetMaxb = ZDArrayPartOfArrayStructure(4, 5, 0, wsaaT5729TargetMaxbs, 0);
	private FixedLengthStringData[] wsaaT5729TargetMaxcs = FLSDArrayPartOfArrayStructure(20, wsaaT5729Data, 225);
	private ZonedDecimalData[][] wsaaT5729TargetMaxc = ZDArrayPartOfArrayStructure(4, 5, 0, wsaaT5729TargetMaxcs, 0);
	private FixedLengthStringData[] wsaaT5729TargetMaxds = FLSDArrayPartOfArrayStructure(20, wsaaT5729Data, 245);
	private ZonedDecimalData[][] wsaaT5729TargetMaxd = ZDArrayPartOfArrayStructure(4, 5, 0, wsaaT5729TargetMaxds, 0);
	private FixedLengthStringData[] wsaaT5729TargetMaxes = FLSDArrayPartOfArrayStructure(20, wsaaT5729Data, 265);
	private ZonedDecimalData[][] wsaaT5729TargetMaxe = ZDArrayPartOfArrayStructure(4, 5, 0, wsaaT5729TargetMaxes, 0);
	private FixedLengthStringData[] wsaaT5729TargetMaxfs = FLSDArrayPartOfArrayStructure(20, wsaaT5729Data, 285);
	private ZonedDecimalData[][] wsaaT5729TargetMaxf = ZDArrayPartOfArrayStructure(4, 5, 0, wsaaT5729TargetMaxfs, 0);
	private FixedLengthStringData[] wsaaT5729OverdueMinas = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 305);
	private ZonedDecimalData[][] wsaaT5729OverdueMina = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729OverdueMinas, 0);
	private FixedLengthStringData[] wsaaT5729OverdueMinbs = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 317);
	private ZonedDecimalData[][] wsaaT5729OverdueMinb = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729OverdueMinbs, 0);
	private FixedLengthStringData[] wsaaT5729OverdueMincs = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 329);
	private ZonedDecimalData[][] wsaaT5729OverdueMinc = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729OverdueMincs, 0);
	private FixedLengthStringData[] wsaaT5729OverdueMinds = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 341);
	private ZonedDecimalData[][] wsaaT5729OverdueMind = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729OverdueMinds, 0);
	private FixedLengthStringData[] wsaaT5729OverdueMines = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 353);
	private ZonedDecimalData[][] wsaaT5729OverdueMine = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729OverdueMines, 0);
	private FixedLengthStringData[] wsaaT5729OverdueMinfs = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 365);
	private ZonedDecimalData[][] wsaaT5729OverdueMinf = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729OverdueMinfs, 0);
}
/*
 * Class transformed  from Data Structure WSAA-AGCM-SUMMARY--INNER
 */
private static final class WsaaAgcmSummaryInner {

		/* WSAA-AGCM-SUMMARY */
	private FixedLengthStringData[] wsaaAgcmRec = FLSInittedArray (100, 13015);
	private FixedLengthStringData[] wsaaAgcmChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaAgcmRec, 0);
	private FixedLengthStringData[] wsaaAgcmChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaAgcmRec, 1);
	private FixedLengthStringData[] wsaaAgcmLife = FLSDArrayPartOfArrayStructure(2, wsaaAgcmRec, 9);
	private FixedLengthStringData[] wsaaAgcmCoverage = FLSDArrayPartOfArrayStructure(2, wsaaAgcmRec, 11);
	private FixedLengthStringData[] wsaaAgcmRider = FLSDArrayPartOfArrayStructure(2, wsaaAgcmRec, 13);
	private FixedLengthStringData[][] wsaaAgcmPremRec = FLSDArrayPartOfArrayStructure(500, 26, wsaaAgcmRec, 15);
	private ZonedDecimalData[][] wsaaAgcmSeqno = ZDArrayPartOfArrayStructure(3, 0, wsaaAgcmPremRec, 0, UNSIGNED_TRUE);
	private PackedDecimalData[][] wsaaAgcmEfdate = PDArrayPartOfArrayStructure(8, 0, wsaaAgcmPremRec, 3, UNSIGNED_TRUE);
	private PackedDecimalData[][] wsaaAgcmAnnprem = PDArrayPartOfArrayStructure(17, 2, wsaaAgcmPremRec, 8);
	private PackedDecimalData[][] wsaaAgcmRecprem = PDArrayPartOfArrayStructure(17, 2, wsaaAgcmPremRec, 17);
}
/*
 * Class transformed  from Data Structure WSAA-VARIABLES--INNER
 */
private static final class WsaaVariablesInner {
		/* WSAA-VARIABLES */
	private ZonedDecimalData wsaaTotTaxCharged = new ZonedDecimalData(11, 2);
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData[] wsaaComponents = FLSInittedArray (99, 62);
	private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaComponents, 0);
	private FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaComponents, 2);
	private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaComponents, 4);
	private ZonedDecimalData[] wsaaPlanSuffix = ZDArrayPartOfArrayStructure(2, 0, wsaaComponents, 6, UNSIGNED_TRUE);
	private ZonedDecimalData[][] wsaaTax = ZDArrayPartOfArrayStructure(2, 16, 5, wsaaComponents, 8);
	private FixedLengthStringData[][] wsaaTaxAbs = FLSDArrayPartOfArrayStructure(2, 1, wsaaComponents, 40);
	private FixedLengthStringData[][] wsaaTaxType = FLSDArrayPartOfArrayStructure(2, 2, wsaaComponents, 42);
	private FixedLengthStringData[] wsaaTaxRule = FLSDArrayPartOfArrayStructure(8, wsaaComponents, 46);
	private FixedLengthStringData[] wsaaTaxItem = FLSDArrayPartOfArrayStructure(8, wsaaComponents, 54);
	private FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
	private ZonedDecimalData wsaaTaxProp = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaPremNet = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOrigSusp = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaTaxPercent = new ZonedDecimalData(8, 5);
	private ZonedDecimalData wsaaDiff = new ZonedDecimalData(7, 2);
	private FixedLengthStringData wsaaTimex = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTimex, 0).setUnsigned().setPattern("99/99/99");

	private FixedLengthStringData wsaaDatex = new FixedLengthStringData(10);
	private ZonedDecimalData wsaaDate = new ZonedDecimalData(8, 0).isAPartOf(wsaaDatex, 0).setUnsigned().setPattern("99/99/9999");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t1688 = new FixedLengthStringData(6).init("T1688");
	private FixedLengthStringData t3695 = new FixedLengthStringData(6).init("T3695");
	private FixedLengthStringData t5644 = new FixedLengthStringData(6).init("T5644");
	private FixedLengthStringData t5645 = new FixedLengthStringData(6).init("T5645");
	private FixedLengthStringData t5671 = new FixedLengthStringData(6).init("T5671");
	private FixedLengthStringData t5679 = new FixedLengthStringData(6).init("T5679");
	private FixedLengthStringData t5688 = new FixedLengthStringData(6).init("T5688");
	private FixedLengthStringData t6654 = new FixedLengthStringData(6).init("T6654");
	private FixedLengthStringData t5729 = new FixedLengthStringData(6).init("T5729");
	private FixedLengthStringData t3620 = new FixedLengthStringData(6).init("T3620");
	private FixedLengthStringData th605 = new FixedLengthStringData(6).init("TH605");
	private FixedLengthStringData tr52d = new FixedLengthStringData(6).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(6).init("TR52E");
	private FixedLengthStringData t5540 = new FixedLengthStringData(6).init("T5540");
}
/*
 * Class transformed  from Data Structure CONTROL-TOTALS--INNER
 */
private static final class ControlTotalsInner {
		/* CONTROL-TOTALS */
	private ZonedDecimalData ct01 = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private ZonedDecimalData ct02 = new ZonedDecimalData(2, 0).init(2).setUnsigned();
	private ZonedDecimalData ct03 = new ZonedDecimalData(2, 0).init(3).setUnsigned();
	private ZonedDecimalData ct04 = new ZonedDecimalData(2, 0).init(4).setUnsigned();
	private ZonedDecimalData ct05 = new ZonedDecimalData(2, 0).init(5).setUnsigned();
	private ZonedDecimalData ct06 = new ZonedDecimalData(2, 0).init(6).setUnsigned();
	private ZonedDecimalData ct07 = new ZonedDecimalData(2, 0).init(7).setUnsigned();
	private ZonedDecimalData ct08 = new ZonedDecimalData(2, 0).init(8).setUnsigned();
	private ZonedDecimalData ct13 = new ZonedDecimalData(2, 0).init(13).setUnsigned();
	private ZonedDecimalData ct14 = new ZonedDecimalData(2, 0).init(14).setUnsigned();
	private ZonedDecimalData ct15 = new ZonedDecimalData(2, 0).init(15).setUnsigned();
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData itemrec = new FixedLengthStringData(7).init("ITEMREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData linsrnlrec = new FixedLengthStringData(10).init("LINSRNLREC");
	private FixedLengthStringData acblrec = new FixedLengthStringData(10).init("ACBLREC");
	private FixedLengthStringData agcmbchrec = new FixedLengthStringData(10).init("AGCMBCHREC");
	private FixedLengthStringData covrlnbrec = new FixedLengthStringData(10).init("COVRLNBREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(7).init("PTRNREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(7).init("PAYRREC");
	private FixedLengthStringData fprmrec = new FixedLengthStringData(7).init("FPRMREC");
	private FixedLengthStringData fpcorec = new FixedLengthStringData(7).init("FPCOREC");
	private FixedLengthStringData dddelf2rec = new FixedLengthStringData(10).init("DDDELF2REC");
	private FixedLengthStringData freprec = new FixedLengthStringData(10).init("FREPREC");
	private FixedLengthStringData acagrnlrec = new FixedLengthStringData(10).init("ACAGRNLREC");
	private FixedLengthStringData taxdrec = new FixedLengthStringData(10).init("TAXDREC");
}

/**
 * 
 * Ticket #ILIFE-1296
 *
 * @author vhukumagrawa
 * @date Mar 30, 2015
 *
 */
private static final class R5358d01RecordInner{
	private FixedLengthStringData r5358d01Record = new FixedLengthStringData(63);
	private FixedLengthStringData r5358d01O = new FixedLengthStringData(63).isAPartOf(r5358d01Record, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(r5358d01O, 0);
	private FixedLengthStringData rd01Btdate = new FixedLengthStringData(10).isAPartOf(r5358d01O, 8);
	private ZonedDecimalData rd01Linstamt = new ZonedDecimalData(11,2).isAPartOf(r5358d01O, 18);
	private ZonedDecimalData rd01MinPrem = new ZonedDecimalData(17,2).isAPartOf(r5358d01O, 29);
	private ZonedDecimalData rd01SusAmt = new ZonedDecimalData(17,2).isAPartOf(r5358d01O, 46);
}

/**
 * 
 * Ticket #ILIFE-1296
 *
 * @author vhukumagrawa
 * @date Mar 30, 2015
 *
 */
private static final class R5359d01RecordInner{
	private FixedLengthStringData r5359d01Record = new FixedLengthStringData(63);
	private FixedLengthStringData r5359d01O = new FixedLengthStringData(63).isAPartOf(r5359d01Record, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(r5359d01O, 0);
	private FixedLengthStringData rd01Btdate = new FixedLengthStringData(10).isAPartOf(r5359d01O, 8);
	private ZonedDecimalData rd01Linstamt = new ZonedDecimalData(11,2).isAPartOf(r5359d01O, 18);
	private ZonedDecimalData rd01MaxPrem = new ZonedDecimalData(17,2).isAPartOf(r5359d01O, 29);
	private ZonedDecimalData rd01SusAmt = new ZonedDecimalData(17,2).isAPartOf(r5359d01O, 46);
}
	private static final class WsaaT6654ArrayInner {

		/* WSAA-T6654-ARRAY
		 03  WSAA-T6654-REC          OCCURS 100                       */
	private FixedLengthStringData[] wsaaT6654Rec = FLSInittedArray (500, 52);
	private FixedLengthStringData[] wsaaT6654Key = FLSDArrayPartOfArrayStructure(4, wsaaT6654Rec, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT6654Billchnl = FLSDArrayPartOfArrayStructure(1, wsaaT6654Key, 0);
	private FixedLengthStringData[] wsaaT6654Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT6654Key, 1);
	private FixedLengthStringData[] wsaaT6654Data = FLSDArrayPartOfArrayStructure(48, wsaaT6654Rec, 4);
	private ZonedDecimalData[] wsaaT6654NonForfDays = ZDArrayPartOfArrayStructure(3, 0, wsaaT6654Data, 0);
	private FixedLengthStringData[] wsaaT6654ArrearsMethod = FLSDArrayPartOfArrayStructure(4, wsaaT6654Data, 3);
	private FixedLengthStringData[] wsaaT6654Subroutine = FLSDArrayPartOfArrayStructure(32, wsaaT6654Data, 7);
	private FixedLengthStringData[][] wsaaT6654Doctid = FLSDArrayPartOfArrayStructure(4, 8, wsaaT6654Subroutine, 0);
	private FixedLengthStringData[] wsaaT6654Expiry = FLSDArrayPartOfArrayStructure(9, wsaaT6654Data, 39);
	private ZonedDecimalData[][] wsaaT6654Daexpy = ZDArrayPartOfArrayStructure(3, 3, 0, wsaaT6654Expiry, 0);
}
}
