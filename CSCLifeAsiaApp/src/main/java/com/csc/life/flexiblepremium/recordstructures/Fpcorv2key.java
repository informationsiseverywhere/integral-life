package com.csc.life.flexiblepremium.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:42
 * Description:
 * Copybook name: FPCORV2KEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Fpcorv2key extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData fpcorv2FileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData fpcorv2Key = new FixedLengthStringData(64).isAPartOf(fpcorv2FileKey, 0, REDEFINE);
  	public FixedLengthStringData fpcorv2Chdrcoy = new FixedLengthStringData(1).isAPartOf(fpcorv2Key, 0);
  	public FixedLengthStringData fpcorv2Chdrnum = new FixedLengthStringData(8).isAPartOf(fpcorv2Key, 1);
  	public FixedLengthStringData fpcorv2Life = new FixedLengthStringData(2).isAPartOf(fpcorv2Key, 9);
  	public FixedLengthStringData fpcorv2Coverage = new FixedLengthStringData(2).isAPartOf(fpcorv2Key, 11);
  	public FixedLengthStringData fpcorv2Rider = new FixedLengthStringData(2).isAPartOf(fpcorv2Key, 13);
  	public PackedDecimalData fpcorv2PlanSuffix = new PackedDecimalData(4, 0).isAPartOf(fpcorv2Key, 15);
  	public PackedDecimalData fpcorv2Targfrom = new PackedDecimalData(8, 0).isAPartOf(fpcorv2Key, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(41).isAPartOf(fpcorv2Key, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(fpcorv2FileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		fpcorv2FileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}