package com.csc.life.flexiblepremium.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:01
 * Description:
 * Copybook name: AGCMSEQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agcmseqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agcmseqFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agcmseqKey = new FixedLengthStringData(64).isAPartOf(agcmseqFileKey, 0, REDEFINE);
  	public FixedLengthStringData agcmseqChdrcoy = new FixedLengthStringData(1).isAPartOf(agcmseqKey, 0);
  	public FixedLengthStringData agcmseqChdrnum = new FixedLengthStringData(8).isAPartOf(agcmseqKey, 1);
  	public FixedLengthStringData agcmseqLife = new FixedLengthStringData(2).isAPartOf(agcmseqKey, 9);
  	public FixedLengthStringData agcmseqCoverage = new FixedLengthStringData(2).isAPartOf(agcmseqKey, 11);
  	public FixedLengthStringData agcmseqRider = new FixedLengthStringData(2).isAPartOf(agcmseqKey, 13);
  	public PackedDecimalData agcmseqPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(agcmseqKey, 15);
  	public PackedDecimalData agcmseqSeqno = new PackedDecimalData(2, 0).isAPartOf(agcmseqKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(agcmseqKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agcmseqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agcmseqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}