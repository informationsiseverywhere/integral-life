package com.csc.life.flexiblepremium.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: FrepTableDAM.java
 * Date: Sun, 30 Aug 2009 03:38:10
 * Class transformed from FREP.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class FrepTableDAM extends FreppfTableDAM {

	public FrepTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("FREP");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "COMPANY"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
		            "COMPANY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "TARGFROM, " +
		            "TARGTO, " +
		            "PRMPER, " +
		            "BILLEDP, " +
		            "PRMRCDP, " +
		            "OVRMINREQ, " +
		            "MINOVRPRO, " +
		            "BTDATE, " +
		            "LINSTAMT, " +
		            "MINPREM, " +
		            "MAXPREM, " +
		            "SUSAMT, " +
		            "PAYRSEQNO, " +
		            "PROCFLAG, " +
		            "CBANPR, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "COMPANY ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "COMPANY DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               jobName,
                               userProfile,
                               datime,
                               company,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               targfrom,
                               targto,
                               targetPremium,
                               billedInPeriod,
                               premRecPer,
                               overdueMin,
                               minOverduePer,
                               btdate,
                               linstamt,
                               minprem,
                               maxprem,
                               susamt,
                               payrseqno,
                               procflag,
                               annivProcDate,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(55);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getCompany().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, company);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller40.setInternal(company.toInternal());
	nonKeyFiller50.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(158);
		
		nonKeyData.set(
					getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ getTargfrom().toInternal()
					+ getTargto().toInternal()
					+ getTargetPremium().toInternal()
					+ getBilledInPeriod().toInternal()
					+ getPremRecPer().toInternal()
					+ getOverdueMin().toInternal()
					+ getMinOverduePer().toInternal()
					+ getBtdate().toInternal()
					+ getLinstamt().toInternal()
					+ getMinprem().toInternal()
					+ getMaxprem().toInternal()
					+ getSusamt().toInternal()
					+ getPayrseqno().toInternal()
					+ getProcflag().toInternal()
					+ getAnnivProcDate().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, targfrom);
			what = ExternalData.chop(what, targto);
			what = ExternalData.chop(what, targetPremium);
			what = ExternalData.chop(what, billedInPeriod);
			what = ExternalData.chop(what, premRecPer);
			what = ExternalData.chop(what, overdueMin);
			what = ExternalData.chop(what, minOverduePer);
			what = ExternalData.chop(what, btdate);
			what = ExternalData.chop(what, linstamt);
			what = ExternalData.chop(what, minprem);
			what = ExternalData.chop(what, maxprem);
			what = ExternalData.chop(what, susamt);
			what = ExternalData.chop(what, payrseqno);
			what = ExternalData.chop(what, procflag);
			what = ExternalData.chop(what, annivProcDate);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getCompany() {
		return company;
	}
	public void setCompany(Object what) {
		company.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}	
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}	
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}	
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}	
	public PackedDecimalData getTargfrom() {
		return targfrom;
	}
	public void setTargfrom(Object what) {
		setTargfrom(what, false);
	}
	public void setTargfrom(Object what, boolean rounded) {
		if (rounded)
			targfrom.setRounded(what);
		else
			targfrom.set(what);
	}	
	public PackedDecimalData getTargto() {
		return targto;
	}
	public void setTargto(Object what) {
		setTargto(what, false);
	}
	public void setTargto(Object what, boolean rounded) {
		if (rounded)
			targto.setRounded(what);
		else
			targto.set(what);
	}	
	public PackedDecimalData getTargetPremium() {
		return targetPremium;
	}
	public void setTargetPremium(Object what) {
		setTargetPremium(what, false);
	}
	public void setTargetPremium(Object what, boolean rounded) {
		if (rounded)
			targetPremium.setRounded(what);
		else
			targetPremium.set(what);
	}	
	public PackedDecimalData getBilledInPeriod() {
		return billedInPeriod;
	}
	public void setBilledInPeriod(Object what) {
		setBilledInPeriod(what, false);
	}
	public void setBilledInPeriod(Object what, boolean rounded) {
		if (rounded)
			billedInPeriod.setRounded(what);
		else
			billedInPeriod.set(what);
	}	
	public PackedDecimalData getPremRecPer() {
		return premRecPer;
	}
	public void setPremRecPer(Object what) {
		setPremRecPer(what, false);
	}
	public void setPremRecPer(Object what, boolean rounded) {
		if (rounded)
			premRecPer.setRounded(what);
		else
			premRecPer.set(what);
	}	
	public PackedDecimalData getOverdueMin() {
		return overdueMin;
	}
	public void setOverdueMin(Object what) {
		setOverdueMin(what, false);
	}
	public void setOverdueMin(Object what, boolean rounded) {
		if (rounded)
			overdueMin.setRounded(what);
		else
			overdueMin.set(what);
	}	
	public PackedDecimalData getMinOverduePer() {
		return minOverduePer;
	}
	public void setMinOverduePer(Object what) {
		setMinOverduePer(what, false);
	}
	public void setMinOverduePer(Object what, boolean rounded) {
		if (rounded)
			minOverduePer.setRounded(what);
		else
			minOverduePer.set(what);
	}	
	public PackedDecimalData getBtdate() {
		return btdate;
	}
	public void setBtdate(Object what) {
		setBtdate(what, false);
	}
	public void setBtdate(Object what, boolean rounded) {
		if (rounded)
			btdate.setRounded(what);
		else
			btdate.set(what);
	}	
	public PackedDecimalData getLinstamt() {
		return linstamt;
	}
	public void setLinstamt(Object what) {
		setLinstamt(what, false);
	}
	public void setLinstamt(Object what, boolean rounded) {
		if (rounded)
			linstamt.setRounded(what);
		else
			linstamt.set(what);
	}	
	public PackedDecimalData getMinprem() {
		return minprem;
	}
	public void setMinprem(Object what) {
		setMinprem(what, false);
	}
	public void setMinprem(Object what, boolean rounded) {
		if (rounded)
			minprem.setRounded(what);
		else
			minprem.set(what);
	}	
	public PackedDecimalData getMaxprem() {
		return maxprem;
	}
	public void setMaxprem(Object what) {
		setMaxprem(what, false);
	}
	public void setMaxprem(Object what, boolean rounded) {
		if (rounded)
			maxprem.setRounded(what);
		else
			maxprem.set(what);
	}	
	public PackedDecimalData getSusamt() {
		return susamt;
	}
	public void setSusamt(Object what) {
		setSusamt(what, false);
	}
	public void setSusamt(Object what, boolean rounded) {
		if (rounded)
			susamt.setRounded(what);
		else
			susamt.set(what);
	}	
	public PackedDecimalData getPayrseqno() {
		return payrseqno;
	}
	public void setPayrseqno(Object what) {
		setPayrseqno(what, false);
	}
	public void setPayrseqno(Object what, boolean rounded) {
		if (rounded)
			payrseqno.setRounded(what);
		else
			payrseqno.set(what);
	}	
	public FixedLengthStringData getProcflag() {
		return procflag;
	}
	public void setProcflag(Object what) {
		procflag.set(what);
	}	
	public PackedDecimalData getAnnivProcDate() {
		return annivProcDate;
	}
	public void setAnnivProcDate(Object what) {
		setAnnivProcDate(what, false);
	}
	public void setAnnivProcDate(Object what, boolean rounded) {
		if (rounded)
			annivProcDate.setRounded(what);
		else
			annivProcDate.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		company.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		jobName.clear();
		userProfile.clear();
		datime.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		targfrom.clear();
		targto.clear();
		targetPremium.clear();
		billedInPeriod.clear();
		premRecPer.clear();
		overdueMin.clear();
		minOverduePer.clear();
		btdate.clear();
		linstamt.clear();
		minprem.clear();
		maxprem.clear();
		susamt.clear();
		payrseqno.clear();
		procflag.clear();
		annivProcDate.clear();		
	}


}