/*
 * File: Bh562.java
 * Date: 29 August 2009 21:34:22
 * Author: Quipoz Limited
 * 
 * Class transformed from BH562.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsu.general.procedures.Datcon6;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon6rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.newbusiness.dataaccess.ZstreffTableDAM;
import com.csc.life.newbusiness.dataaccess.ZstrprdTableDAM;
import com.csc.life.newbusiness.recordstructures.Ph562par;
import com.csc.life.newbusiness.reports.Rh562Report;
import com.csc.life.newbusiness.tablestructures.Th565rec;
import com.csc.life.newbusiness.tablestructures.Th566rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   NEW BUSINESS PRODUCTION REPORT.
*
* To generate a Daily Report on New Business Production. It is
* grouped by the product type if new business and then by branch
* and company.
*
* This report contains the total annualised premium of New Busines 
* Production for each product type with 4 periods (today, current
* month, curent quarter and current year), percentage in total
* production and the number of New Business Production for each
* Product Type.
*
* A Parameter prompt screen is required.
*
*****************************************************************
* </pre>
*/
public class Bh562 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlzstrpf1rs = null;
	private java.sql.PreparedStatement sqlzstrpf1ps = null;
	private java.sql.Connection sqlzstrpf1conn = null;
	private String sqlzstrpf1 = "";
	private java.sql.ResultSet sqlchdrpfrs = null;
	private java.sql.PreparedStatement sqlchdrpfps = null;
	private java.sql.Connection sqlchdrpfconn = null;
	private String sqlchdrpf = "";
	private Rh562Report printerFile = new Rh562Report();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH562");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaOccdate = new PackedDecimalData(8, 0);
	private static final String wsaaPercent = "%";

	private FixedLengthStringData wsaaDatetext = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaDatetextD1 = new FixedLengthStringData(1).isAPartOf(wsaaDatetext, 0);
		/* WSAA-OUTPUT-FIELDS */
	private FixedLengthStringData wsaaSdate = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaCompanynm = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaCurrencynm = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
		/* WSAA-COMPANY-TOTALS */
	private PackedDecimalData wsaaCompanyTday = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCompanyCntTday = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaCompanyMtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCompanyCntMtd = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaCompanyQtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCompanyCntQtd = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaCompanyYtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCompanyCntYtd = new PackedDecimalData(5, 0);
		/* WSAA-ANNUAL-TOTALS */
	private PackedDecimalData wsaaAnnTday = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAnnCntTday = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaAnnMtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAnnCntMtd = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaAnnQtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAnnCntQtd = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaAnnYtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAnnCntYtd = new PackedDecimalData(5, 0);
		/* WSAA-SINGLES-TOTALS */
	private PackedDecimalData wsaaSgpTday = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSgpCntTday = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaSgpMtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSgpCntMtd = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaSgpQtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSgpCntQtd = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaSgpYtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSgpCntYtd = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaEffectiveDate = new ZonedDecimalData(8, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(wsaaEffectiveDate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaEffYr = new ZonedDecimalData(4, 0).isAPartOf(filler1, 0);
	private ZonedDecimalData wsaaEffMth = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4);
	private ZonedDecimalData wsaaZstrEffDate = new ZonedDecimalData(8, 0);

	private FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(wsaaZstrEffDate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaZstrEffmth = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4);
	private ZonedDecimalData wsaaStartOfYear = new ZonedDecimalData(8, 0);

	private FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(wsaaStartOfYear, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaStartYr = new ZonedDecimalData(4, 0).isAPartOf(filler3, 0);
	private ZonedDecimalData wsaaStartMth = new ZonedDecimalData(2, 0).isAPartOf(filler3, 4);
	private ZonedDecimalData wsaaStartDay = new ZonedDecimalData(2, 0).isAPartOf(filler3, 6);
	private ZonedDecimalData wsaaCurrentQuarter = new ZonedDecimalData(8, 0);

	private FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(wsaaCurrentQuarter, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaQtrYr = new ZonedDecimalData(4, 0).isAPartOf(filler4, 0);
	private ZonedDecimalData wsaaQtrMth = new ZonedDecimalData(2, 0).isAPartOf(filler4, 4);
	private ZonedDecimalData wsaaQtrDay = new ZonedDecimalData(2, 0).isAPartOf(filler4, 6);
	private ZonedDecimalData wsaaQuarter = new ZonedDecimalData(1, 0).setUnsigned();
	private ZonedDecimalData wsaaOddQuarter = new ZonedDecimalData(1, 0).setUnsigned();
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSkipBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3);
	private PackedDecimalData wsaaPremium = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaBillingFreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreq = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillingFreq, 0).setUnsigned();
	private FixedLengthStringData wsaaClnkCurrIn = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaClnkCompany = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaPrintAnnual = new FixedLengthStringData(1);
	private Validator annualPremium = new Validator(wsaaPrintAnnual, "Y");

	private FixedLengthStringData wsaaFirstRead = new FixedLengthStringData(1);
	private Validator firstRead = new Validator(wsaaFirstRead, "Y");

	private FixedLengthStringData wsaaLastRead = new FixedLengthStringData(1);
	private Validator lastRead = new Validator(wsaaLastRead, "Y");
	private String wsaaOpenedFile = "";
	private String wsaaPrintCoyTotal = "";
	private String wsaaZeroPrdAnn = "";
	private String wsaaZeroPrdSgp = "";
	private String wsaaZeroBrnAnn = "";
	private String wsaaZeroBrnSgp = "";
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private static final String zstreffrec = "ZSTREFFREC";
	private static final String zstrprdrec = "ZSTRPRDREC";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t3629 = "T3629";
	private static final String t5688 = "T5688";
	private static final String th565 = "TH565";
	private static final String th566 = "TH566";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler6 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler6, 5);

		/* SQL-ZSTRPF */
	private FixedLengthStringData zstrrec = new FixedLengthStringData(6);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(zstrrec, 0);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(zstrrec, 1);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(zstrrec, 3);

		/* SQL-CHDRPF */
	private FixedLengthStringData zstrrec2 = new FixedLengthStringData(6);
	private FixedLengthStringData sqlChdrcoy2 = new FixedLengthStringData(1).isAPartOf(zstrrec2, 0);
	private FixedLengthStringData sqlCntbranch2 = new FixedLengthStringData(2).isAPartOf(zstrrec2, 1);
	private FixedLengthStringData sqlCnttype2 = new FixedLengthStringData(3).isAPartOf(zstrrec2, 3);

		/*  Control indicators                                             */
	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private String wsaaEndSingle = "";

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ZstreffTableDAM zstreffIO = new ZstreffTableDAM();
	private ZstrprdTableDAM zstrprdIO = new ZstrprdTableDAM();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon6rec datcon6rec = new Datcon6rec();
	private Ph562par ph562par = new Ph562par();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Th565rec th565rec = new Th565rec();
	private Th566rec th566rec = new Th566rec();
	private PrinterRecInner printerRecInner = new PrinterRecInner();
	private WsaaPrdAnnTotalsInner wsaaPrdAnnTotalsInner = new WsaaPrdAnnTotalsInner();
	private WsaaPrdSgpTotalsInner wsaaPrdSgpTotalsInner = new WsaaPrdSgpTotalsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		endOfFile2080, 
		exit2090, 
		zstreffio2605, 
		nextZstreff2608, 
		exit2609, 
		nextZstrprd2728, 
		exit2729, 
		endOfFile2818, 
		exit2819, 
		nextZstrprd2828, 
		exit2829
	}

	public Bh562() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		syserrrec.syserrStatuz.set(sqlStatuz);
		fatalError600();
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.                */
		/*EXIT*/
	}

protected void initialise1000()
	{
		try {
			initialise1010();
			setUpHeadingCompany1020();
			setUpHeadingDates1030();
			workingStorageFields1040();
			readTh5651050();
			setUpHeadingCurrency1060();
			calendarDesc1061();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void initialise1010()
	{
		ph562par.parmRecord.set(bupaIO.getParmarea());
		wsaaOpenedFile = "N";
		if (isNE(ph562par.znbprod, "Y")) {
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit1090);
		}
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
		wsaaOverflow.set("Y");
	}

protected void setUpHeadingCompany1020()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		wsaaCompany.set(bsprIO.getCompany());
		wsaaCompanynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1030()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaSdate.set(datcon1rec.extDate);
		wsaaToday.set(datcon1rec.intDate);
		datcon6rec.language.set(bsscIO.getLanguage());
		datcon6rec.company.set(bsprIO.getCompany());
		datcon6rec.intDate1.set(bsscIO.getEffectiveDate());
		callProgram(Datcon6.class, datcon6rec.datcon6Rec);
		wsaaDatetext.set(datcon6rec.intDate2);
		if (isEQ(wsaaDatetextD1, SPACES)) {
			wsaaDatetextD1.set("0");
		}
	}

protected void workingStorageFields1040()
	{
		/*  Initialize the working storage fields                          */
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		indOff.setTrue(11);
		indOff.setTrue(12);
		indOff.setTrue(13);
		indOff.setTrue(14);
		indOff.setTrue(15);
		indOff.setTrue(16);
		indOff.setTrue(17);
		indOff.setTrue(18);
		indOff.setTrue(19);
		indOff.setTrue(20);
		indOff.setTrue(21);
		indOff.setTrue(22);
		if (isEQ(wsaaEffMth, 1)){
			indOn.setTrue(11);
		}
		else if (isEQ(wsaaEffMth, 2)){
			indOn.setTrue(12);
		}
		else if (isEQ(wsaaEffMth, 3)){
			indOn.setTrue(13);
		}
		else if (isEQ(wsaaEffMth, 4)){
			indOn.setTrue(14);
		}
		else if (isEQ(wsaaEffMth, 5)){
			indOn.setTrue(15);
		}
		else if (isEQ(wsaaEffMth, 6)){
			indOn.setTrue(16);
		}
		else if (isEQ(wsaaEffMth, 7)){
			indOn.setTrue(17);
		}
		else if (isEQ(wsaaEffMth, 8)){
			indOn.setTrue(18);
		}
		else if (isEQ(wsaaEffMth, 9)){
			indOn.setTrue(19);
		}
		else if (isEQ(wsaaEffMth, 10)){
			indOn.setTrue(20);
		}
		else if (isEQ(wsaaEffMth, 11)){
			indOn.setTrue(21);
		}
		else if (isEQ(wsaaEffMth, 12)){
			indOn.setTrue(22);
		}
		wsaaCompanyTday.set(ZERO);
		wsaaCompanyCntTday.set(ZERO);
		wsaaCompanyMtd.set(ZERO);
		wsaaCompanyCntMtd.set(ZERO);
		wsaaCompanyQtd.set(ZERO);
		wsaaCompanyCntQtd.set(ZERO);
		wsaaCompanyYtd.set(ZERO);
		wsaaCompanyCntYtd.set(ZERO);
		wsaaAnnTday.set(ZERO);
		wsaaAnnCntTday.set(ZERO);
		wsaaAnnMtd.set(ZERO);
		wsaaAnnCntMtd.set(ZERO);
		wsaaAnnQtd.set(ZERO);
		wsaaAnnCntQtd.set(ZERO);
		wsaaAnnYtd.set(ZERO);
		wsaaAnnCntYtd.set(ZERO);
		wsaaSgpTday.set(ZERO);
		wsaaSgpCntTday.set(ZERO);
		wsaaSgpMtd.set(ZERO);
		wsaaSgpCntMtd.set(ZERO);
		wsaaSgpQtd.set(ZERO);
		wsaaSgpCntQtd.set(ZERO);
		wsaaSgpYtd.set(ZERO);
		wsaaSgpCntYtd.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdAnnTday.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdAnnTdayCnt.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdAnnMtd.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdAnnMtdCnt.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdAnnQtd.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdAnnQtdCnt.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdAnnYtd.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdAnnYtdCnt.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdTdayPrcAnn.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdMtdPrcAnn.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdQtdPrcAnn.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdYtdPrcAnn.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdSgpTday.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdSgpTdayCnt.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdSgpMtd.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdSgpMtdCnt.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdSgpQtd.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdSgpQtdCnt.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdSgpYtd.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdSgpYtdCnt.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdTdayPrcSgp.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdMtdPrcSgp.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdQtdPrcSgp.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdYtdPrcSgp.set(ZERO);
		wsaaPremium.set(ZERO);
		wsaaQuarter.set(ZERO);
		wsaaOddQuarter.set(ZERO);
		wsaaX.set(ZERO);
		wsaaBranch.set(SPACES);
		wsaaSkipBranch.set(SPACES);
		wsaaCnttype.set(SPACES);
		wsaaLastRead.set("N");
		wsaaPrintCoyTotal = "N";
		wsaaFirstRead.set("Y");
		wsaaPrintAnnual.set("Y");
		wsaaOccdate.set(varcom.vrcmMaxDate);
	}

protected void readTh5651050()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th565);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th565rec.th565Rec.set(itemIO.getGenarea());
		if (isLT(wsaaEffMth, th565rec.zmthfrm01)) {
			compute(wsaaStartYr, 0).set(sub(wsaaEffYr, 1));
		}
		else {
			wsaaStartYr.set(wsaaEffYr);
		}
		wsaaStartMth.set(th565rec.zmthfrm01);
		wsaaStartDay.set(1);
		for (wsaaX.set(1); !(isGT(wsaaX, 4)
		|| isNE(wsaaQuarter, ZERO)); wsaaX.add(1)){
			if (isLT(th565rec.zmthfrm[wsaaX.toInt()], th565rec.zmthto[wsaaX.toInt()])) {
				if (isGTE(wsaaEffMth, th565rec.zmthfrm[wsaaX.toInt()])
				&& isLTE(wsaaEffMth, th565rec.zmthto[wsaaX.toInt()])) {
					wsaaQuarter.set(wsaaX);
				}
			}
			else {
				wsaaOddQuarter.set(wsaaX);
			}
		}
		if (isEQ(wsaaQuarter, ZERO)) {
			wsaaQuarter.set(wsaaOddQuarter);
		}
		if (isLT(wsaaEffMth, th565rec.zmthfrm[wsaaQuarter.toInt()])) {
			compute(wsaaQtrYr, 0).set(sub(wsaaEffYr, 1));
		}
		else {
			wsaaQtrYr.set(wsaaEffYr);
		}
		wsaaQtrMth.set(th565rec.zmthfrm[wsaaQuarter.toInt()]);
		wsaaQtrDay.set(1);
	}

protected void setUpHeadingCurrency1060()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(th565rec.currcode);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		/*  IF DESC-STATUZ           NOT = O-K                   <LA2679>*/
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setDescitem("***");
			descIO.setFunction(varcom.readr);
			descIO.setFormat(descrec);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
		}
		wsaaCurrencynm.set(descIO.getLongdesc());
	}

protected void calendarDesc1061()
	{
		/***** MOVE SPACES                 TO WSAA-TR386-ID.        <FA1226>*/
		/**    PERFORM 1100-READ-TR386.                             <LA3235>*/
		/***** MOVE TR386-PROGDESC-1       TO WSAA-JANUARY.         <FA1226>*/
		/***** MOVE TR386-PROGDESC-2       TO WSAA-FEBRUARY.        <FA1226>*/
		/***** MOVE TR386-PROGDESC-3       TO WSAA-MARCH.           <FA1226>*/
		/***** MOVE TR386-PROGDESC-4       TO WSAA-APRIL.           <FA1226>*/
		/***** MOVE TR386-PROGDESC-5       TO WSAA-MAY.             <FA1226>*/
		/***** MOVE '1'                    TO WSAA-TR386-ID.        <FA1226>*/
		/***** PERFORM 1100-READ-TR386.                             <FA1226>*/
		/***** MOVE TR386-PROGDESC-1       TO WSAA-JUNE.            <FA1226>*/
		/***** MOVE TR386-PROGDESC-2       TO WSAA-JULY.            <FA1226>*/
		/***** MOVE TR386-PROGDESC-3       TO WSAA-AUGUST.          <FA1226>*/
		/***** MOVE TR386-PROGDESC-4       TO WSAA-SEPTEMBER.       <FA1226>*/
		/***** MOVE TR386-PROGDESC-5       TO WSAA-OCTOBER.         <FA1226>*/
		/***** MOVE '2'                    TO WSAA-TR386-ID.        <FA1226>*/
		/***** PERFORM 1100-READ-TR386.                             <FA1226>*/
		/***** MOVE TR386-PROGDESC-1       TO WSAA-NOVEMBER.        <FA1226>*/
		/***** MOVE TR386-PROGDESC-2       TO WSAA-DECEMBER.        <FA1226>*/
		/*DEFINE-CURSOR*/
		/*  Define the query required by declaring a cursor                */
		sqlzstrpf1 = " SELECT DISTINCT CHDRCOY, CNTBRANCH, CNTTYPE" +
" FROM   " + getAppVars().getTableNameOverriden("ZSTRPF") + " " +
" WHERE CHDRCOY = ?" +
" ORDER BY CHDRCOY, CNTBRANCH, CNTTYPE";
		/*   Open the cursor (this runs the query)                         */
		sqlerrorflag = false;
		try {
			sqlzstrpf1conn = getAppVars().getDBConnectionForTable(new com.csc.life.newbusiness.dataaccess.ZstrpfTableDAM());
			sqlzstrpf1ps = getAppVars().prepareStatementEmbeded(sqlzstrpf1conn, sqlzstrpf1, "ZSTRPF");
			getAppVars().setDBString(sqlzstrpf1ps, 1, wsaaCompany);
			sqlzstrpf1rs = getAppVars().executeQuery(sqlzstrpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		wsaaOpenedFile = "Y";
	}

protected void readTr3861100()
	{
		/*READ-TR386*/
		/**--- Read TR386 table to get screen literals                      */
		/**    MOVE ITEMREC                TO ITEM-FORMAT.          <LA3235>*/
		/**    MOVE 'IT'                   TO ITEM-ITEMPFX.         <LA3235>*/
		/**    MOVE BSPR-COMPANY           TO ITEM-ITEMCOY.         <LA3235>*/
		/**    MOVE TR386                  TO ITEM-ITEMTABL.        <LA3235>*/
		/**    MOVE BSSC-LANGUAGE          TO WSAA-TR386-LANG.      <LA3235>*/
		/**    MOVE WSAA-PROG              TO WSAA-TR386-PGM.       <LA3235>*/
		/**    MOVE WSAA-TR386-KEY         TO ITEM-ITEMITEM.        <LA3235>*/
		/**    MOVE SPACES                 TO ITEM-ITEMSEQ.         <LA3235>*/
		/***** MOVE READR                  TO ITEM-FUNCTION.        <FA1226>*/
		/**    MOVE BEGN                   TO ITEM-FUNCTION.        <LA3235>*/
		/**    MOVE O-K                    TO ITEM-STATUZ.          <LA3235>*/
		/**    MOVE 1                      TO IY.                   <LA3235>*/
		/***   PERFORM 1200-LOAD-TR386                              <LA3235>*/
		/**        UNTIL ITEM-STATUZ = ENDP.                        <LA3235>*/
		/***** CALL  'ITEMIO'           USING ITEM-PARAMS.          <FA1226>*/
		/***** IF  ITEM-STATUZ          NOT = O-K                   <FA1226>*/
		/*****     MOVE ITEM-STATUZ        TO SYSR-STATUZ           <FA1226>*/
		/*****     MOVE ITEM-PARAMS        TO SYSR-PARAMS           <FA1226>*/
		/*****     PERFORM 600-FATAL-ERROR                          <FA1226>*/
		/***** END-IF.                                              <FA1226>*/
		/***** MOVE ITEM-GENAREA        TO TR386-TR386-REC.         <FA1226>*/
		/*EXIT*/
	}

	/**
	* <pre>
	*1200-LOAD-TR386 SECTION.                                 <LA3235>
	*1210-START.                                              <LA3235>
	*    CALL 'ITEMIO'               USING ITEM-PARAMS.       <LA3235>
	*    IF ITEM-ITEMCOY             NOT = BSPR-COMPANY       <LA3235>
	*    OR ITEM-ITEMTABL            NOT = TR386              <LA3235>
	*    OR ITEM-ITEMITEM            NOT = WSAA-TR386-KEY     <LA3235>
	*    OR ITEM-STATUZ              NOT = O-K                <LA3235>
	*        IF ITEM-FUNCTION        = BEGN                   <LA3235>
	*            MOVE WSAA-TR386-KEY TO ITEM-ITEMITEM         <LA3235>
	*            MOVE ITEM-PARAMS    TO SYSR-PARAMS           <LA3235>
	*            PERFORM 600-FATAL-ERROR                      <LA3235>
	*        ELSE                                             <LA3235>
	*            MOVE ENDP           TO ITEM-STATUZ           <LA3235>
	*            GO TO 1290-EXIT                              <LA3235>
	*       END-IF                                            <LA3235>
	*    END-IF.                                              <LA3235>
	*    MOVE ITEM-GENAREA           TO TR386-TR386-REC.      <LA3235>
	*    MOVE 1                      TO IX.                   <LA3235>
	*    PERFORM UNTIL IX            > 10                     <LA3235>
	*       IF  TR386-PROGDESC(IX)   NOT = SPACES             <LA3235>
	*       AND IY                   < 13                     <LA3235>
	*           MOVE TR386-PROGDESC(IX) TO WSAA-CALENDAR-MONTH(IY)    
	*           ADD 1                TO IY                    <LA3235>
	*       END-IF                                            <LA3235>
	*       ADD 1                    TO IX                    <LA3235>
	*    END-PERFORM.                                         <LA3235>
	*    MOVE NEXTR                  TO ITEM-FUNCTION.        <LA3235>
	*1290-EXIT.                                               <LA3235>
	*    EXIT.                                                <LA3235>
	* </pre>
	*/
protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case endOfFile2080: 
					endOfFile2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		/*   Fetch record                                                  */
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlzstrpf1rs)) {
				getAppVars().getDBObject(sqlzstrpf1rs, 1, sqlChdrcoy);
				getAppVars().getDBObject(sqlzstrpf1rs, 2, sqlCntbranch);
				getAppVars().getDBObject(sqlzstrpf1rs, 3, sqlCnttype);
			}
			else {
				goTo(GotoLabel.endOfFile2080);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		goTo(GotoLabel.exit2090);
	}

protected void endOfFile2080()
	{
		if (!firstRead.isTrue()) {
			wsaaLastRead.set("Y");
			sqlCntbranch.set(SPACES);
		}
		else {
			wsspEdterror.set(varcom.endp);
		}
	}

protected void edit2500()
	{
		edit2510();
	}

protected void edit2510()
	{
		wsspEdterror.set(varcom.oK);
		if (isEQ(sqlCntbranch, wsaaSkipBranch)
		&& !lastRead.isTrue()) {
			return ;
		}
		if (isNE(sqlCntbranch, wsaaBranch)) {
			/* print previous branch total                                     */
			if (!firstRead.isTrue()
			&& isNE(wsaaZeroBrnAnn, "Y")) {
				printBranchTotals2940();
				if (isEQ(wsaaZeroBrnSgp, "Y")
				&& lastRead.isTrue()) {
					wsaaPrintCoyTotal = "Y";
				}
			}
			/*  Accumulate and print previous branch's product single premium  */
			/*  before accumulating new branch's total.                        */
			if (!firstRead.isTrue()) {
				if (isNE(wsaaZeroBrnSgp, "Y")) {
					wsaaEndSingle = "N";
					accumulateSinglePremium2800();
					printBranchTotals2940();
					if (lastRead.isTrue()) {
						wsaaPrintCoyTotal = "Y";
					}
				}
				else {
					wsaaPrintAnnual.set("Y");
				}
			}
			/*  If end of file, print company total and exit.                  */
			if (isEQ(wsaaPrintCoyTotal, "Y")) {
				printCompanyTotal2960();
				wsspEdterror.set(varcom.endp);
				return ;
			}
			/*  Accumulate branch totals for both annual & single premium      */
			wsaaAnnTday.set(ZERO);
			wsaaAnnMtd.set(ZERO);
			wsaaAnnQtd.set(ZERO);
			wsaaAnnYtd.set(ZERO);
			wsaaAnnCntTday.set(ZERO);
			wsaaAnnCntMtd.set(ZERO);
			wsaaAnnCntQtd.set(ZERO);
			wsaaAnnCntYtd.set(ZERO);
			wsaaSgpTday.set(ZERO);
			wsaaSgpMtd.set(ZERO);
			wsaaSgpQtd.set(ZERO);
			wsaaSgpYtd.set(ZERO);
			wsaaSgpCntTday.set(ZERO);
			wsaaSgpCntMtd.set(ZERO);
			wsaaSgpCntQtd.set(ZERO);
			wsaaSgpCntYtd.set(ZERO);
			zstreffIO.setStatuz(varcom.oK);
			while ( !(isEQ(zstreffIO.getStatuz(), varcom.endp))) {
				loopThruZstreff2600();
			}
			
			checkBranchTotal2620();
			if (isEQ(wsaaZeroBrnAnn, "Y")
			&& isEQ(wsaaZeroBrnSgp, "Y")) {
				wsaaSkipBranch.set(sqlCntbranch);
				wsaaBranch.set(sqlCntbranch);
				return ;
			}
			wsaaFirstRead.set("N");
			wsaaBranch.set(sqlCntbranch);
		}
		accumulateAnnualPrem2700();
	}

protected void loopThruZstreff2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					loopZstreff2600();
				case zstreffio2605: 
					zstreffio2605();
				case nextZstreff2608: 
					nextZstreff2608();
				case exit2609: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void loopZstreff2600()
	{
		zstreffIO.setDataArea(SPACES);
		zstreffIO.setChdrcoy(sqlChdrcoy);
		zstreffIO.setCntbranch(sqlCntbranch);
		zstreffIO.setEffdate(bsscIO.getEffectiveDate());
		zstreffIO.setFormat(zstreffrec);
		zstreffIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zstreffIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zstreffIO.setFitKeysSearch("CHDRCOY", "CNTBRANCH");
	}

protected void zstreffio2605()
	{
		SmartFileCode.execute(appVars, zstreffIO);
		if (isNE(zstreffIO.getStatuz(), varcom.oK)
		&& isNE(zstreffIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zstreffIO.getStatuz());
			syserrrec.params.set(zstreffIO.getParams());
			fatalError600();
		}
		if (isEQ(zstreffIO.getStatuz(), varcom.endp)
		|| isNE(zstreffIO.getChdrcoy(), sqlChdrcoy)
		|| isNE(zstreffIO.getCntbranch(), sqlCntbranch)
		|| isLT(zstreffIO.getEffdate(), wsaaStartOfYear)) {
			zstreffIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2609);
		}
		/*  Check if the transaction code of the record read is a one of   */
		/*  the allowable transactions in TH565.                           */
		for (wsaaX.set(1); !(isGT(wsaaX, 10)
		|| isEQ(zstreffIO.getBatctrcde(), th565rec.ztrcde[wsaaX.toInt()])); wsaaX.add(1)){
			if (isNE(th565rec.ztrcde[wsaaX.toInt()], zstreffIO.getBatctrcde())
			&& isEQ(wsaaX, 10)) {
				goTo(GotoLabel.nextZstreff2608);
			}
		}
		/*  If effective date of record is greater than BSSC-EFFECTIVE-DATE*/
		/*  ignore this record as belong to a previous run.                */
		if (isGT(zstreffIO.getEffdate(), bsscIO.getEffectiveDate())) {
			goTo(GotoLabel.nextZstreff2608);
		}
		/*  Read TH566 for policy count - whether to add, subtract         */
		/*  or do nothing.                                                 */
		itemIO.setDataKey(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItemitem(zstreffIO.getBatctrcde());
		readTh5665300();
		wsaaOccdate.set(zstreffIO.getOccdate());
		wsaaBillingFreq.set(zstreffIO.getBillfreq());
		/*  When the effective date = BSSC-EFFECTIVE-DATE, check the       */
		/*  currency code, covert if necessary.  Accumulate INSTPREM &     */
		/*  SINGP for TDAY, MTD, QTD and YTD.                              */
		if (isEQ(zstreffIO.getEffdate(), bsscIO.getEffectiveDate())) {
			if (isNE(zstreffIO.getInstprem(), ZERO)) {
				compute(wsaaPremium, 3).setRounded(mult((add(zstreffIO.getInstprem(), zstreffIO.getCntfee())), wsaaBillfreq));
				if (isNE(zstreffIO.getCntcurr(), th565rec.currcode)) {
					wsaaClnkCurrIn.set(zstreffIO.getCntcurr());
					wsaaClnkCompany.set(zstreffIO.getChdrcoy());
					convertPremium5000();
				}
				wsaaAnnTday.add(wsaaPremium);
				wsaaAnnMtd.add(wsaaPremium);
				wsaaAnnQtd.add(wsaaPremium);
				wsaaAnnYtd.add(wsaaPremium);
				wsaaAnnCntTday.add(th566rec.cnt);
				wsaaAnnCntMtd.add(th566rec.cnt);
				wsaaAnnCntQtd.add(th566rec.cnt);
				wsaaAnnCntYtd.add(th566rec.cnt);
			}
			if (isNE(zstreffIO.getSingp(), ZERO)) {
				compute(wsaaPremium, 2).set(add(zstreffIO.getSingp(), zstreffIO.getCntfee()));
				if (isNE(zstreffIO.getCntcurr(), th565rec.currcode)) {
					wsaaClnkCurrIn.set(zstreffIO.getCntcurr());
					wsaaClnkCompany.set(zstreffIO.getChdrcoy());
					convertPremium5000();
				}
				wsaaSgpTday.add(wsaaPremium);
				wsaaSgpMtd.add(wsaaPremium);
				wsaaSgpQtd.add(wsaaPremium);
				wsaaSgpYtd.add(wsaaPremium);
				wsaaSgpCntTday.add(th566rec.cnt);
				wsaaSgpCntMtd.add(th566rec.cnt);
				wsaaSgpCntQtd.add(th566rec.cnt);
				wsaaSgpCntYtd.add(th566rec.cnt);
			}
			goTo(GotoLabel.nextZstreff2608);
		}
		/*  When ZSTREFF-EFFDATE and BSSC-EFFECTIVE-DATE falls within the  */
		/*  same calendar month, accumulate the MTD, QTD and YTD premiums. */
		wsaaZstrEffDate.set(zstreffIO.getEffdate());
		if (isEQ(wsaaZstrEffmth, wsaaEffMth)) {
			if (isNE(zstreffIO.getInstprem(), ZERO)) {
				compute(wsaaPremium, 3).setRounded(mult((add(zstreffIO.getInstprem(), zstreffIO.getCntfee())), wsaaBillfreq));
				if (isNE(zstreffIO.getCntcurr(), th565rec.currcode)) {
					wsaaClnkCurrIn.set(zstreffIO.getCntcurr());
					wsaaClnkCompany.set(zstreffIO.getChdrcoy());
					convertPremium5000();
				}
				wsaaAnnMtd.add(wsaaPremium);
				wsaaAnnQtd.add(wsaaPremium);
				wsaaAnnYtd.add(wsaaPremium);
				wsaaAnnCntMtd.add(th566rec.cnt);
				wsaaAnnCntQtd.add(th566rec.cnt);
				wsaaAnnCntYtd.add(th566rec.cnt);
			}
			if (isNE(zstreffIO.getSingp(), ZERO)) {
				compute(wsaaPremium, 2).set(add(zstreffIO.getSingp(), zstreffIO.getCntfee()));
				if (isNE(zstreffIO.getCntcurr(), th565rec.currcode)) {
					wsaaClnkCurrIn.set(zstreffIO.getCntcurr());
					wsaaClnkCompany.set(zstreffIO.getChdrcoy());
					convertPremium5000();
				}
				wsaaSgpMtd.add(wsaaPremium);
				wsaaSgpQtd.add(wsaaPremium);
				wsaaSgpYtd.add(wsaaPremium);
				wsaaSgpCntMtd.add(th566rec.cnt);
				wsaaSgpCntQtd.add(th566rec.cnt);
				wsaaSgpCntYtd.add(th566rec.cnt);
			}
			goTo(GotoLabel.nextZstreff2608);
		}
		/*   When ZSTREFF-EFFDATE falls within the current quarter,        */
		/*   accumulate QTD and YTD.                                       */
		if (isGTE(zstreffIO.getEffdate(), wsaaCurrentQuarter)) {
			if (isNE(zstreffIO.getInstprem(), ZERO)) {
				compute(wsaaPremium, 3).setRounded(mult((add(zstreffIO.getInstprem(), zstreffIO.getCntfee())), wsaaBillfreq));
				if (isNE(zstreffIO.getCntcurr(), th565rec.currcode)) {
					wsaaClnkCurrIn.set(zstreffIO.getCntcurr());
					wsaaClnkCompany.set(zstreffIO.getChdrcoy());
					convertPremium5000();
				}
				wsaaAnnQtd.add(wsaaPremium);
				wsaaAnnYtd.add(wsaaPremium);
				wsaaAnnCntQtd.add(th566rec.cnt);
				wsaaAnnCntYtd.add(th566rec.cnt);
			}
			if (isNE(zstreffIO.getSingp(), ZERO)) {
				compute(wsaaPremium, 2).set(add(zstreffIO.getSingp(), zstreffIO.getCntfee()));
				if (isNE(zstreffIO.getCntcurr(), th565rec.currcode)) {
					wsaaClnkCurrIn.set(zstreffIO.getCntcurr());
					wsaaClnkCompany.set(zstreffIO.getChdrcoy());
					convertPremium5000();
				}
				wsaaSgpQtd.add(wsaaPremium);
				wsaaSgpYtd.add(wsaaPremium);
				wsaaSgpCntQtd.add(th566rec.cnt);
				wsaaSgpCntYtd.add(th566rec.cnt);
			}
			goTo(GotoLabel.nextZstreff2608);
		}
		/*   When ZSTREFF-EFFDATE is not in the current quarter, accumulate*/
		/*   only the YTD.                                                 */
		if (isLT(zstreffIO.getEffdate(), wsaaCurrentQuarter)) {
			if (isNE(zstreffIO.getInstprem(), ZERO)) {
				compute(wsaaPremium, 3).setRounded(mult((add(zstreffIO.getInstprem(), zstreffIO.getCntfee())), wsaaBillfreq));
				if (isNE(zstreffIO.getCntcurr(), th565rec.currcode)) {
					wsaaClnkCurrIn.set(zstreffIO.getCntcurr());
					wsaaClnkCompany.set(zstreffIO.getChdrcoy());
					convertPremium5000();
				}
				wsaaAnnYtd.add(wsaaPremium);
				wsaaAnnCntYtd.add(th566rec.cnt);
			}
			if (isNE(zstreffIO.getSingp(), ZERO)) {
				compute(wsaaPremium, 2).set(add(zstreffIO.getSingp(), zstreffIO.getCntfee()));
				if (isNE(zstreffIO.getCntcurr(), th565rec.currcode)) {
					wsaaClnkCurrIn.set(zstreffIO.getCntcurr());
					wsaaClnkCompany.set(zstreffIO.getChdrcoy());
					convertPremium5000();
				}
				wsaaSgpYtd.add(wsaaPremium);
				wsaaSgpCntYtd.add(th566rec.cnt);
			}
		}
	}

protected void nextZstreff2608()
	{
		zstreffIO.setFunction(varcom.nextr);
		goTo(GotoLabel.zstreffio2605);
	}

protected void checkBranchTotal2620()
	{
		/*CHK-BRN-TOT*/
		wsaaZeroBrnAnn = "N";
		wsaaZeroBrnSgp = "N";
		if (isEQ(wsaaAnnTday, ZERO)
		&& isEQ(wsaaAnnMtd, ZERO)
		&& isEQ(wsaaAnnQtd, ZERO)
		&& isEQ(wsaaAnnYtd, ZERO)) {
			wsaaZeroBrnAnn = "Y";
		}
		if (isEQ(wsaaSgpTday, ZERO)
		&& isEQ(wsaaSgpMtd, ZERO)
		&& isEQ(wsaaSgpQtd, ZERO)
		&& isEQ(wsaaSgpYtd, ZERO)) {
			wsaaZeroBrnSgp = "Y";
		}
		/*EXIT*/
	}

protected void accumulateAnnualPrem2700()
	{
		productAnnual2700();
	}

protected void productAnnual2700()
	{
		/*  Loop through ZSTRPRD to obtain the total INSTPREM of each produ*/
		zstrprdIO.setDataKey(SPACES);
		zstrprdIO.setChdrcoy(sqlChdrcoy);
		zstrprdIO.setCntbranch(sqlCntbranch);
		zstrprdIO.setCnttype(sqlCnttype);
		zstrprdIO.setEffdate(wsaaToday);
		zstrprdIO.setStatuz(varcom.oK);
		zstrprdIO.setFormat(zstrprdrec);
		zstrprdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zstrprdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zstrprdIO.setFitKeysSearch("CHDRCOY", "CNTBRANCH", "CNTTYPE");
		while ( !(isEQ(zstrprdIO.getStatuz(),varcom.endp))) {
			addAnnualPrem2720();
		}
		
		checkPrdAnnTotal2740();
		if (isEQ(wsaaZeroPrdAnn, "Y")) {
			return ;
		}
		compute(wsaaPrdAnnTotalsInner.wsaaPrdTdayPrcAnn, 3).setRounded(div((mult(wsaaPrdAnnTotalsInner.wsaaPrdAnnTday, 100)), wsaaAnnTday));
		compute(wsaaPrdAnnTotalsInner.wsaaPrdMtdPrcAnn, 3).setRounded(div((mult(wsaaPrdAnnTotalsInner.wsaaPrdAnnMtd, 100)), wsaaAnnMtd));
		compute(wsaaPrdAnnTotalsInner.wsaaPrdQtdPrcAnn, 3).setRounded(div((mult(wsaaPrdAnnTotalsInner.wsaaPrdAnnQtd, 100)), wsaaAnnQtd));
		compute(wsaaPrdAnnTotalsInner.wsaaPrdYtdPrcAnn, 3).setRounded(div((mult(wsaaPrdAnnTotalsInner.wsaaPrdAnnYtd, 100)), wsaaAnnYtd));
		printProductAnnual2900();
		/*  Reset product annual totals for next round of accumulation.    */
		wsaaPrdAnnTotalsInner.wsaaPrdAnnTday.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdAnnMtd.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdAnnQtd.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdAnnYtd.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdAnnTdayCnt.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdAnnMtdCnt.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdAnnQtdCnt.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdAnnYtdCnt.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdTdayPrcAnn.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdMtdPrcAnn.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdQtdPrcAnn.set(ZERO);
		wsaaPrdAnnTotalsInner.wsaaPrdYtdPrcAnn.set(ZERO);
	}

protected void addAnnualPrem2720()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					addAnnPrem2720();
				case nextZstrprd2728: 
					nextZstrprd2728();
				case exit2729: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void addAnnPrem2720()
	{
		SmartFileCode.execute(appVars, zstrprdIO);
		if (isNE(zstrprdIO.getStatuz(), varcom.oK)
		&& isNE(zstrprdIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zstrprdIO.getStatuz());
			syserrrec.params.set(zstrprdIO.getParams());
			fatalError600();
		}
		if (isEQ(zstrprdIO.getStatuz(), varcom.endp)
		|| isNE(zstrprdIO.getChdrcoy(), sqlChdrcoy)
		|| isNE(zstrprdIO.getCntbranch(), sqlCntbranch)
		|| isNE(zstrprdIO.getCnttype(), sqlCnttype)
		|| isLT(zstrprdIO.getEffdate(), wsaaStartOfYear)) {
			zstrprdIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2729);
		}
		/*  Check if the transaction code is one of the allowables in TH565*/
		for (wsaaX.set(1); !(isGT(wsaaX, 10)
		|| isEQ(zstrprdIO.getBatctrcde(), th565rec.ztrcde[wsaaX.toInt()])); wsaaX.add(1)){
			if (isNE(zstrprdIO.getBatctrcde(), th565rec.ztrcde[wsaaX.toInt()])
			&& isEQ(wsaaX, 10)) {
				goTo(GotoLabel.nextZstrprd2728);
			}
		}
		/*  Skip record if annual premium is zero.                         */
		if (isEQ(zstrprdIO.getInstprem(), ZERO)) {
			goTo(GotoLabel.nextZstrprd2728);
		}
		/*  Skip records with effective dates > BSSC-EFFECTIVE-DATE as     */
		/*  they are for previous runs.                                    */
		if (isGT(zstrprdIO.getEffdate(), bsscIO.getEffectiveDate())) {
			goTo(GotoLabel.nextZstrprd2728);
		}
		/*  Read TH566 for policy count - whether to add, subtract         */
		/*  or do nothing.                                                 */
		itemIO.setDataKey(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItemitem(zstrprdIO.getBatctrcde());
		readTh5665300();
		wsaaOccdate.set(zstrprdIO.getOccdate());
		wsaaBillingFreq.set(zstrprdIO.getBillfreq());
		/*  ZSTRPRD-EFFDATE = BSSC-EFFECTIVE-DATE, accumulate to date,     */
		/*  month to date, quarter to date and year to date amounts        */
		/*  for each product.                                              */
		if (isEQ(zstrprdIO.getEffdate(), bsscIO.getEffectiveDate())) {
			compute(wsaaPremium, 3).setRounded(mult((add(zstrprdIO.getInstprem(), zstrprdIO.getCntfee())), wsaaBillfreq));
			if (isNE(zstrprdIO.getCntcurr(), th565rec.currcode)) {
				wsaaClnkCurrIn.set(zstrprdIO.getCntcurr());
				wsaaClnkCompany.set(zstrprdIO.getChdrcoy());
				convertPremium5000();
			}
			wsaaPrdAnnTotalsInner.wsaaPrdAnnTday.add(wsaaPremium);
			wsaaPrdAnnTotalsInner.wsaaPrdAnnMtd.add(wsaaPremium);
			wsaaPrdAnnTotalsInner.wsaaPrdAnnQtd.add(wsaaPremium);
			wsaaPrdAnnTotalsInner.wsaaPrdAnnYtd.add(wsaaPremium);
			wsaaPrdAnnTotalsInner.wsaaPrdAnnTdayCnt.add(th566rec.cnt);
			wsaaPrdAnnTotalsInner.wsaaPrdAnnMtdCnt.add(th566rec.cnt);
			wsaaPrdAnnTotalsInner.wsaaPrdAnnQtdCnt.add(th566rec.cnt);
			wsaaPrdAnnTotalsInner.wsaaPrdAnnYtdCnt.add(th566rec.cnt);
			goTo(GotoLabel.nextZstrprd2728);
		}
		/*  If ZSTRPRD-ZEFFDATE and BSSC-EFFECTIVE-DATE are in the same    */
		/*  calender year, include premium in the month to date,           */
		/*  quarter to date and year to date for each product.             */
		wsaaZstrEffDate.set(zstrprdIO.getEffdate());
		if (isEQ(wsaaZstrEffmth, wsaaEffMth)) {
			compute(wsaaPremium, 3).setRounded(mult((add(zstrprdIO.getInstprem(), zstrprdIO.getCntfee())), wsaaBillfreq));
			if (isNE(zstrprdIO.getCntcurr(), th565rec.currcode)) {
				wsaaClnkCurrIn.set(zstrprdIO.getCntcurr());
				wsaaClnkCompany.set(zstrprdIO.getChdrcoy());
				convertPremium5000();
			}
			wsaaPrdAnnTotalsInner.wsaaPrdAnnMtd.add(wsaaPremium);
			wsaaPrdAnnTotalsInner.wsaaPrdAnnQtd.add(wsaaPremium);
			wsaaPrdAnnTotalsInner.wsaaPrdAnnYtd.add(wsaaPremium);
			wsaaPrdAnnTotalsInner.wsaaPrdAnnMtdCnt.add(th566rec.cnt);
			wsaaPrdAnnTotalsInner.wsaaPrdAnnQtdCnt.add(th566rec.cnt);
			wsaaPrdAnnTotalsInner.wsaaPrdAnnYtdCnt.add(th566rec.cnt);
			goTo(GotoLabel.nextZstrprd2728);
		}
		/*  If ZSTRPRD-EFFDATE is in the current quarter, include premium  */
		/*  in the quarter to date and year to date amount for each product*/
		if (isGTE(zstrprdIO.getEffdate(), wsaaCurrentQuarter)) {
			compute(wsaaPremium, 3).setRounded(mult((add(zstrprdIO.getInstprem(), zstrprdIO.getCntfee())), wsaaBillfreq));
			if (isNE(zstrprdIO.getCntcurr(), th565rec.currcode)) {
				wsaaClnkCurrIn.set(zstrprdIO.getCntcurr());
				wsaaClnkCompany.set(zstrprdIO.getChdrcoy());
				convertPremium5000();
			}
			wsaaPrdAnnTotalsInner.wsaaPrdAnnQtd.add(wsaaPremium);
			wsaaPrdAnnTotalsInner.wsaaPrdAnnYtd.add(wsaaPremium);
			wsaaPrdAnnTotalsInner.wsaaPrdAnnQtdCnt.add(th566rec.cnt);
			wsaaPrdAnnTotalsInner.wsaaPrdAnnYtdCnt.add(th566rec.cnt);
			goTo(GotoLabel.nextZstrprd2728);
		}
		/*  If ZSTRPRD-EFFDATE is not in the current quarter, include the  */
		/*  premium in only the year to date amount.                       */
		if (isLT(zstrprdIO.getEffdate(), wsaaCurrentQuarter)) {
			compute(wsaaPremium, 3).setRounded(mult((add(zstrprdIO.getInstprem(), zstrprdIO.getCntfee())), wsaaBillfreq));
			if (isNE(zstrprdIO.getCntcurr(), th565rec.currcode)) {
				wsaaClnkCurrIn.set(zstrprdIO.getCntcurr());
				wsaaClnkCompany.set(zstrprdIO.getChdrcoy());
				convertPremium5000();
			}
			wsaaPrdAnnTotalsInner.wsaaPrdAnnYtd.add(wsaaPremium);
			wsaaPrdAnnTotalsInner.wsaaPrdAnnYtdCnt.add(th566rec.cnt);
		}
	}

protected void nextZstrprd2728()
	{
		zstrprdIO.setFunction(varcom.nextr);
	}

protected void checkPrdAnnTotal2740()
	{
		/*CHK-PRD-ANN-TOT*/
		wsaaZeroPrdAnn = "N";
		if (isEQ(wsaaPrdAnnTotalsInner.wsaaPrdAnnTday, ZERO)
		&& isEQ(wsaaPrdAnnTotalsInner.wsaaPrdAnnMtd, ZERO)
		&& isEQ(wsaaPrdAnnTotalsInner.wsaaPrdAnnQtd, ZERO)
		&& isEQ(wsaaPrdAnnTotalsInner.wsaaPrdAnnYtd, ZERO)) {
			wsaaZeroPrdAnn = "Y";
		}
		/*EXIT*/
	}

protected void accumulateSinglePremium2800()
	{
		/*ACCUM-SINGLE-PREM*/
		sqlchdrpf = " SELECT DISTINCT CHDRCOY, CNTBRANCH, CNTTYPE" +
" FROM   " + getAppVars().getTableNameOverriden("ZSTRPF") + " " +
" WHERE CHDRCOY = ?" +
" AND CNTBRANCH = ?" +
" ORDER BY CHDRCOY, CNTBRANCH, CNTTYPE";
		/*   Open the cursor (this runs the query)                         */
		sqlerrorflag = false;
		try {
			sqlchdrpfconn = getAppVars().getDBConnectionForTable(new com.csc.life.newbusiness.dataaccess.ZstrpfTableDAM());
			sqlchdrpfps = getAppVars().prepareStatementEmbeded(sqlchdrpfconn, sqlchdrpf, "ZSTRPF");
			getAppVars().setDBString(sqlchdrpfps, 1, wsaaCompany);
			getAppVars().setDBString(sqlchdrpfps, 2, wsaaBranch);
			sqlchdrpfrs = getAppVars().executeQuery(sqlchdrpfps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		wsaaEndSingle = "N";
		while ( !(isEQ(wsaaEndSingle, "Y"))) {
			singlePremium2810();
		}
		
		getAppVars().freeDBConnectionIgnoreErr(sqlchdrpfconn, sqlchdrpfps, sqlchdrpfrs);
		/*EXIT*/
	}

protected void singlePremium2810()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					singlePrem2810();
				case endOfFile2818: 
					endOfFile2818();
				case exit2819: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void singlePrem2810()
	{
		/*   Fetch record                                                  */
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlchdrpfrs)) {
				getAppVars().getDBObject(sqlchdrpfrs, 1, sqlChdrcoy2);
				getAppVars().getDBObject(sqlchdrpfrs, 2, sqlCntbranch2);
				getAppVars().getDBObject(sqlchdrpfrs, 3, sqlCnttype2);
			}
			else {
				goTo(GotoLabel.endOfFile2818);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*  Set up ZSTRPRD keys using the first contract type of the       */
		/*  branch and loop through ZSTRPRD, accumulate and print each     */
		/*  product total single premium                                   */
		zstrprdIO.setRecKeyData(SPACES);
		zstrprdIO.setChdrcoy(sqlChdrcoy2);
		zstrprdIO.setCntbranch(sqlCntbranch2);
		zstrprdIO.setCnttype(sqlCnttype2);
		zstrprdIO.setEffdate(wsaaToday);
		zstrprdIO.setFormat(zstrprdrec);
		zstrprdIO.setStatuz(varcom.oK);
		zstrprdIO.setFunction(varcom.begn);
		while ( !(isEQ(zstrprdIO.getStatuz(), varcom.endp))) {
			loopThruZstrprd2820();
		}
		
		checkPrdSgpTotal2840();
		if (isEQ(wsaaZeroPrdSgp, "Y")) {
			goTo(GotoLabel.exit2819);
		}
		compute(wsaaPrdSgpTotalsInner.wsaaPrdTdayPrcSgp, 3).setRounded(div((mult(wsaaPrdSgpTotalsInner.wsaaPrdSgpTday, 100)), wsaaSgpTday));
		compute(wsaaPrdSgpTotalsInner.wsaaPrdMtdPrcSgp, 3).setRounded(div((mult(wsaaPrdSgpTotalsInner.wsaaPrdSgpMtd, 100)), wsaaSgpMtd));
		compute(wsaaPrdSgpTotalsInner.wsaaPrdQtdPrcSgp, 3).setRounded(div((mult(wsaaPrdSgpTotalsInner.wsaaPrdSgpQtd, 100)), wsaaSgpQtd));
		compute(wsaaPrdSgpTotalsInner.wsaaPrdYtdPrcSgp, 3).setRounded(div((mult(wsaaPrdSgpTotalsInner.wsaaPrdSgpYtd, 100)), wsaaSgpYtd));
		printProductSingle2920();
		/*  Reset product single totals for next round of accumulation.    */
		wsaaPrdSgpTotalsInner.wsaaPrdSgpTday.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdSgpMtd.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdSgpQtd.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdSgpYtd.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdSgpTdayCnt.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdSgpMtdCnt.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdSgpQtdCnt.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdSgpYtdCnt.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdTdayPrcSgp.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdMtdPrcSgp.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdQtdPrcSgp.set(ZERO);
		wsaaPrdSgpTotalsInner.wsaaPrdYtdPrcSgp.set(ZERO);
		goTo(GotoLabel.exit2819);
	}

protected void endOfFile2818()
	{
		wsaaEndSingle = "Y";
	}

protected void loopThruZstrprd2820()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					loopZstrprd2820();
				case nextZstrprd2828: 
					nextZstrprd2828();
				case exit2829: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void loopZstrprd2820()
	{
		SmartFileCode.execute(appVars, zstrprdIO);
		if (isNE(zstrprdIO.getStatuz(), varcom.oK)
		&& isNE(zstrprdIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zstrprdIO.getStatuz());
			syserrrec.params.set(zstrprdIO.getParams());
			fatalError600();
		}
		if (isEQ(zstrprdIO.getStatuz(), varcom.endp)
		|| isNE(zstrprdIO.getChdrcoy(), sqlChdrcoy2)
		|| isNE(zstrprdIO.getCntbranch(), sqlCntbranch2)
		|| isNE(zstrprdIO.getCnttype(), sqlCnttype2)
		|| isLT(zstrprdIO.getEffdate(), wsaaStartOfYear)) {
			zstrprdIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2829);
		}
		/*  Check if the transaction code is one of the allowables in TH565*/
		for (wsaaX.set(1); !(isGT(wsaaX, 10)
		|| isEQ(zstrprdIO.getBatctrcde(), th565rec.ztrcde[wsaaX.toInt()])); wsaaX.add(1)){
			if (isNE(zstrprdIO.getBatctrcde(), th565rec.ztrcde[wsaaX.toInt()])
			&& isEQ(wsaaX, 10)) {
				goTo(GotoLabel.nextZstrprd2828);
			}
		}
		/*  Skip record if annual premium is zero.                         */
		if (isEQ(zstrprdIO.getSingp(), ZERO)) {
			goTo(GotoLabel.nextZstrprd2828);
		}
		/*  Skip records with effective dates > BSSC-EFFECTIVE-DATE as     */
		/*  they are for previous runs.                                    */
		if (isGT(zstrprdIO.getEffdate(), bsscIO.getEffectiveDate())) {
			goTo(GotoLabel.nextZstrprd2828);
		}
		/*  Read TH566 for policy count - whether to add, subtract         */
		/*  or do nothing.                                                 */
		itemIO.setDataKey(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItemitem(zstrprdIO.getBatctrcde());
		readTh5665300();
		wsaaOccdate.set(zstrprdIO.getOccdate());
		/*  ZSTRPRD-EFFDATE = BSSC-EFFECTIVE-DATE, accumulate to date,     */
		/*  month to date, quarter to date and year to date amounts        */
		/*  for each product.                                              */
		if (isEQ(zstrprdIO.getEffdate(), bsscIO.getEffectiveDate())) {
			compute(wsaaPremium, 2).set(add(zstrprdIO.getSingp(), zstrprdIO.getCntfee()));
			if (isNE(zstrprdIO.getCntcurr(), th565rec.currcode)) {
				wsaaClnkCurrIn.set(zstrprdIO.getCntcurr());
				wsaaClnkCompany.set(zstrprdIO.getChdrcoy());
				convertPremium5000();
			}
			wsaaPrdSgpTotalsInner.wsaaPrdSgpTday.add(wsaaPremium);
			wsaaPrdSgpTotalsInner.wsaaPrdSgpMtd.add(wsaaPremium);
			wsaaPrdSgpTotalsInner.wsaaPrdSgpQtd.add(wsaaPremium);
			wsaaPrdSgpTotalsInner.wsaaPrdSgpYtd.add(wsaaPremium);
			wsaaPrdSgpTotalsInner.wsaaPrdSgpTdayCnt.add(th566rec.cnt);
			wsaaPrdSgpTotalsInner.wsaaPrdSgpMtdCnt.add(th566rec.cnt);
			wsaaPrdSgpTotalsInner.wsaaPrdSgpQtdCnt.add(th566rec.cnt);
			wsaaPrdSgpTotalsInner.wsaaPrdSgpYtdCnt.add(th566rec.cnt);
			goTo(GotoLabel.nextZstrprd2828);
		}
		/*  If ZSTRPRD-ZEFFDATE and BSSC-EFFECTIVE-DATE are in the same    */
		/*  calender month, include premium in the month to date,          */
		/*  quarter to date and year to date for each product.             */
		wsaaZstrEffDate.set(zstrprdIO.getEffdate());
		if (isEQ(wsaaZstrEffmth, wsaaEffMth)) {
			compute(wsaaPremium, 2).set(add(zstrprdIO.getSingp(), zstrprdIO.getCntfee()));
			if (isNE(zstrprdIO.getCntcurr(), th565rec.currcode)) {
				wsaaClnkCurrIn.set(zstrprdIO.getCntcurr());
				wsaaClnkCompany.set(zstrprdIO.getChdrcoy());
				convertPremium5000();
			}
			wsaaPrdSgpTotalsInner.wsaaPrdSgpMtd.add(wsaaPremium);
			wsaaPrdSgpTotalsInner.wsaaPrdSgpQtd.add(wsaaPremium);
			wsaaPrdSgpTotalsInner.wsaaPrdSgpYtd.add(wsaaPremium);
			wsaaPrdSgpTotalsInner.wsaaPrdSgpMtdCnt.add(th566rec.cnt);
			wsaaPrdSgpTotalsInner.wsaaPrdSgpQtdCnt.add(th566rec.cnt);
			wsaaPrdSgpTotalsInner.wsaaPrdSgpYtdCnt.add(th566rec.cnt);
			goTo(GotoLabel.nextZstrprd2828);
		}
		/*  If ZSTRPRD-EFFDATE is in the current quarter, include premium  */
		/*  in the quarter to date and year to date amount for each product*/
		if (isGTE(zstrprdIO.getEffdate(), wsaaCurrentQuarter)) {
			compute(wsaaPremium, 2).set(add(zstrprdIO.getSingp(), zstrprdIO.getCntfee()));
			if (isNE(zstrprdIO.getCntcurr(), th565rec.currcode)) {
				wsaaClnkCurrIn.set(zstrprdIO.getCntcurr());
				wsaaClnkCompany.set(zstrprdIO.getChdrcoy());
				convertPremium5000();
			}
			wsaaPrdSgpTotalsInner.wsaaPrdSgpQtd.add(wsaaPremium);
			wsaaPrdSgpTotalsInner.wsaaPrdSgpYtd.add(wsaaPremium);
			wsaaPrdSgpTotalsInner.wsaaPrdSgpQtdCnt.add(th566rec.cnt);
			wsaaPrdSgpTotalsInner.wsaaPrdSgpYtdCnt.add(th566rec.cnt);
			goTo(GotoLabel.nextZstrprd2828);
		}
		/*  If ZSTRPRD-EFFDATE is not in the current quarter, include the  */
		/*  premium in only the year to date amount.                       */
		if (isLT(zstrprdIO.getEffdate(), wsaaCurrentQuarter)) {
			compute(wsaaPremium, 2).set(add(zstrprdIO.getSingp(), zstrprdIO.getCntfee()));
			if (isNE(zstrprdIO.getCntcurr(), th565rec.currcode)) {
				wsaaClnkCurrIn.set(zstrprdIO.getCntcurr());
				wsaaClnkCompany.set(zstrprdIO.getChdrcoy());
				convertPremium5000();
			}
			wsaaPrdSgpTotalsInner.wsaaPrdSgpYtd.add(wsaaPremium);
			wsaaPrdSgpTotalsInner.wsaaPrdSgpYtdCnt.add(th566rec.cnt);
		}
	}

protected void nextZstrprd2828()
	{
		zstrprdIO.setFunction(varcom.nextr);
	}

protected void checkPrdSgpTotal2840()
	{
		/*CHK-PRD-SGP-TOT*/
		wsaaZeroPrdSgp = "N";
		if (isEQ(wsaaPrdSgpTotalsInner.wsaaPrdSgpTday, ZERO)
		&& isEQ(wsaaPrdSgpTotalsInner.wsaaPrdSgpMtd, ZERO)
		&& isEQ(wsaaPrdSgpTotalsInner.wsaaPrdSgpQtd, ZERO)
		&& isEQ(wsaaPrdSgpTotalsInner.wsaaPrdSgpYtd, ZERO)) {
			wsaaZeroPrdSgp = "Y";
		}
		/*EXIT*/
	}

protected void printProductAnnual2900()
	{
		printPrdAnn2900();
	}

protected void printPrdAnn2900()
	{
		if (newPageReq.isTrue()) {
			printPageHeading2980();
		}
		printerRecInner.printerRec.set(SPACES);
		printerRecInner.instprem01.set(ZERO);
		printerRecInner.instprem02.set(ZERO);
		printerRecInner.instprem03.set(ZERO);
		printerRecInner.instprem04.set(ZERO);
		printerRecInner.znofpol01.set(ZERO);
		printerRecInner.znofpol02.set(ZERO);
		printerRecInner.znofpol03.set(ZERO);
		printerRecInner.znofpol04.set(ZERO);
		printerRecInner.tgtpcnt01.set(ZERO);
		printerRecInner.tgtpcnt02.set(ZERO);
		printerRecInner.tgtpcnt03.set(ZERO);
		printerRecInner.tgtpcnt04.set(ZERO);
		wsaaCnttype.set(sqlCnttype);
		getProduct5200();
		printerRecInner.instprem01.set(wsaaPrdAnnTotalsInner.wsaaPrdAnnTday);
		printerRecInner.instprem02.set(wsaaPrdAnnTotalsInner.wsaaPrdAnnMtd);
		printerRecInner.instprem03.set(wsaaPrdAnnTotalsInner.wsaaPrdAnnQtd);
		printerRecInner.instprem04.set(wsaaPrdAnnTotalsInner.wsaaPrdAnnYtd);
		printerRecInner.znofpol01.set(wsaaPrdAnnTotalsInner.wsaaPrdAnnTdayCnt);
		printerRecInner.znofpol02.set(wsaaPrdAnnTotalsInner.wsaaPrdAnnMtdCnt);
		printerRecInner.znofpol03.set(wsaaPrdAnnTotalsInner.wsaaPrdAnnQtdCnt);
		printerRecInner.znofpol04.set(wsaaPrdAnnTotalsInner.wsaaPrdAnnYtdCnt);
		printerRecInner.tgtpcnt01.set(wsaaPrdAnnTotalsInner.wsaaPrdTdayPrcAnn);
		printerRecInner.tgtpcnt02.set(wsaaPrdAnnTotalsInner.wsaaPrdMtdPrcAnn);
		printerRecInner.tgtpcnt03.set(wsaaPrdAnnTotalsInner.wsaaPrdQtdPrcAnn);
		printerRecInner.tgtpcnt04.set(wsaaPrdAnnTotalsInner.wsaaPrdYtdPrcAnn);
		if (isNE(wsaaPrdAnnTotalsInner.wsaaPrdTdayPrcAnn, ZERO)) {
			printerRecInner.pcamtind01.set(wsaaPercent);
		}
		else {
			printerRecInner.pcamtind01.set(SPACES);
		}
		if (isNE(wsaaPrdAnnTotalsInner.wsaaPrdMtdPrcAnn, ZERO)) {
			printerRecInner.pcamtind02.set(wsaaPercent);
		}
		else {
			printerRecInner.pcamtind02.set(SPACES);
		}
		if (isNE(wsaaPrdAnnTotalsInner.wsaaPrdQtdPrcAnn, ZERO)) {
			printerRecInner.pcamtind03.set(wsaaPercent);
		}
		else {
			printerRecInner.pcamtind03.set(SPACES);
		}
		if (isNE(wsaaPrdAnnTotalsInner.wsaaPrdYtdPrcAnn, ZERO)) {
			printerRecInner.pcamtind04.set(wsaaPercent);
		}
		else {
			printerRecInner.pcamtind04.set(SPACES);
		}
		printerFile.printRh562d01(printerRecInner.printerRec);
	}

protected void printProductSingle2920()
	{
		printPrdSgp2920();
	}

protected void printPrdSgp2920()
	{
		if (newPageReq.isTrue()) {
			printPageHeading2980();
		}
		printerRecInner.printerRec.set(SPACES);
		printerRecInner.instprem01.set(ZERO);
		printerRecInner.instprem02.set(ZERO);
		printerRecInner.instprem03.set(ZERO);
		printerRecInner.instprem04.set(ZERO);
		printerRecInner.znofpol01.set(ZERO);
		printerRecInner.znofpol02.set(ZERO);
		printerRecInner.znofpol03.set(ZERO);
		printerRecInner.znofpol04.set(ZERO);
		printerRecInner.tgtpcnt01.set(ZERO);
		printerRecInner.tgtpcnt02.set(ZERO);
		printerRecInner.tgtpcnt03.set(ZERO);
		printerRecInner.tgtpcnt04.set(ZERO);
		wsaaCnttype.set(sqlCnttype2);
		getProduct5200();
		printerRecInner.instprem01.set(wsaaPrdSgpTotalsInner.wsaaPrdSgpTday);
		printerRecInner.instprem02.set(wsaaPrdSgpTotalsInner.wsaaPrdSgpMtd);
		printerRecInner.instprem03.set(wsaaPrdSgpTotalsInner.wsaaPrdSgpQtd);
		printerRecInner.instprem04.set(wsaaPrdSgpTotalsInner.wsaaPrdSgpYtd);
		printerRecInner.znofpol01.set(wsaaPrdSgpTotalsInner.wsaaPrdSgpTdayCnt);
		printerRecInner.znofpol02.set(wsaaPrdSgpTotalsInner.wsaaPrdSgpMtdCnt);
		printerRecInner.znofpol03.set(wsaaPrdSgpTotalsInner.wsaaPrdSgpQtdCnt);
		printerRecInner.znofpol04.set(wsaaPrdSgpTotalsInner.wsaaPrdSgpYtdCnt);
		printerRecInner.tgtpcnt01.set(wsaaPrdSgpTotalsInner.wsaaPrdTdayPrcSgp);
		printerRecInner.tgtpcnt02.set(wsaaPrdSgpTotalsInner.wsaaPrdMtdPrcSgp);
		printerRecInner.tgtpcnt03.set(wsaaPrdSgpTotalsInner.wsaaPrdQtdPrcSgp);
		printerRecInner.tgtpcnt04.set(wsaaPrdSgpTotalsInner.wsaaPrdYtdPrcSgp);
		if (isNE(wsaaPrdSgpTotalsInner.wsaaPrdTdayPrcSgp, ZERO)) {
			printerRecInner.pcamtind01.set(wsaaPercent);
		}
		else {
			printerRecInner.pcamtind01.set(SPACES);
		}
		if (isNE(wsaaPrdSgpTotalsInner.wsaaPrdMtdPrcSgp, ZERO)) {
			printerRecInner.pcamtind02.set(wsaaPercent);
		}
		else {
			printerRecInner.pcamtind02.set(SPACES);
		}
		if (isNE(wsaaPrdSgpTotalsInner.wsaaPrdQtdPrcSgp, ZERO)) {
			printerRecInner.pcamtind03.set(wsaaPercent);
		}
		else {
			printerRecInner.pcamtind03.set(SPACES);
		}
		if (isNE(wsaaPrdSgpTotalsInner.wsaaPrdYtdPrcSgp, ZERO)) {
			printerRecInner.pcamtind04.set(wsaaPercent);
		}
		else {
			printerRecInner.pcamtind04.set(SPACES);
		}
		printerFile.printRh562d01(printerRecInner.printerRec);
	}

protected void printBranchTotals2940()
	{
		branchTotals2940();
	}

protected void branchTotals2940()
	{
		if (newPageReq.isTrue()) {
			printPageHeading2980();
		}
		printerRecInner.printerRec.set(SPACES);
		printerRecInner.instprem011.set(ZERO);
		printerRecInner.instprem021.set(ZERO);
		printerRecInner.instprem031.set(ZERO);
		printerRecInner.instprem041.set(ZERO);
		printerRecInner.znofpol011.set(ZERO);
		printerRecInner.znofpol021.set(ZERO);
		printerRecInner.znofpol031.set(ZERO);
		printerRecInner.znofpol041.set(ZERO);
		printerRecInner.tgtpcnt011.set(ZERO);
		printerRecInner.tgtpcnt021.set(ZERO);
		printerRecInner.tgtpcnt031.set(ZERO);
		printerRecInner.tgtpcnt041.set(ZERO);
		if (annualPremium.isTrue()) {
			printerRecInner.instprem011.set(wsaaAnnTday);
			printerRecInner.instprem021.set(wsaaAnnMtd);
			printerRecInner.instprem031.set(wsaaAnnQtd);
			printerRecInner.instprem041.set(wsaaAnnYtd);
			printerRecInner.znofpol011.set(wsaaAnnCntTday);
			printerRecInner.znofpol021.set(wsaaAnnCntMtd);
			printerRecInner.znofpol031.set(wsaaAnnCntQtd);
			printerRecInner.znofpol041.set(wsaaAnnCntYtd);
			printerRecInner.tgtpcnt011.set(100);
			printerRecInner.tgtpcnt021.set(100);
			printerRecInner.tgtpcnt031.set(100);
			printerRecInner.tgtpcnt041.set(100);
			printerRecInner.pcamtind011.set(wsaaPercent);
			printerRecInner.pcamtind021.set(wsaaPercent);
			printerRecInner.pcamtind031.set(wsaaPercent);
			printerRecInner.pcamtind041.set(wsaaPercent);
			/*  Accumulate company total.                                      */
			wsaaCompanyTday.add(wsaaAnnTday);
			wsaaCompanyCntTday.add(wsaaAnnCntTday);
			wsaaCompanyMtd.add(wsaaAnnMtd);
			wsaaCompanyCntMtd.add(wsaaAnnCntMtd);
			wsaaCompanyQtd.add(wsaaAnnQtd);
			wsaaCompanyCntQtd.add(wsaaAnnCntQtd);
			wsaaCompanyYtd.add(wsaaAnnYtd);
			wsaaCompanyCntYtd.add(wsaaAnnCntYtd);
			/*  Reset branch totals                                            */
			wsaaPrintAnnual.set("N");
		}
		else {
			printerRecInner.instprem011.set(wsaaSgpTday);
			printerRecInner.instprem021.set(wsaaSgpMtd);
			printerRecInner.instprem031.set(wsaaSgpQtd);
			printerRecInner.instprem041.set(wsaaSgpYtd);
			printerRecInner.znofpol011.set(wsaaSgpCntTday);
			printerRecInner.znofpol021.set(wsaaSgpCntMtd);
			printerRecInner.znofpol031.set(wsaaSgpCntQtd);
			printerRecInner.znofpol041.set(wsaaSgpCntYtd);
			printerRecInner.tgtpcnt011.set(100);
			printerRecInner.tgtpcnt021.set(100);
			printerRecInner.tgtpcnt031.set(100);
			printerRecInner.tgtpcnt041.set(100);
			printerRecInner.pcamtind011.set(wsaaPercent);
			printerRecInner.pcamtind021.set(wsaaPercent);
			printerRecInner.pcamtind031.set(wsaaPercent);
			printerRecInner.pcamtind041.set(wsaaPercent);
			/*  Accumulate company total.                                      */
			wsaaCompanyTday.add(wsaaSgpTday);
			wsaaCompanyCntTday.add(wsaaSgpCntTday);
			wsaaCompanyMtd.add(wsaaSgpMtd);
			wsaaCompanyCntMtd.add(wsaaSgpCntMtd);
			wsaaCompanyQtd.add(wsaaSgpQtd);
			wsaaCompanyCntQtd.add(wsaaSgpCntQtd);
			wsaaCompanyYtd.add(wsaaSgpYtd);
			wsaaCompanyCntYtd.add(wsaaSgpCntYtd);
			/*  Reset branch totals                                            */
			wsaaPrintAnnual.set("Y");
		}
		printerFile.printRh562t01(printerRecInner.printerRec);
		if (!newPageReq.isTrue()) {
			if (!lastRead.isTrue()) {
				wsaaOverflow.set("Y");
			}
			else {
				if (!annualPremium.isTrue()
				&& isNE(wsaaZeroBrnSgp, "Y")) {
					wsaaOverflow.set("Y");
				}
			}
		}
	}

protected void printCompanyTotal2960()
	{
		companyTotals2960();
	}

protected void companyTotals2960()
	{
		if (newPageReq.isTrue()) {
			printPageHeading2980();
		}
		printerRecInner.printerRec.set(SPACES);
		printerRecInner.instprem012.set(ZERO);
		printerRecInner.instprem022.set(ZERO);
		printerRecInner.instprem032.set(ZERO);
		printerRecInner.instprem042.set(ZERO);
		printerRecInner.znofpol012.set(ZERO);
		printerRecInner.znofpol022.set(ZERO);
		printerRecInner.znofpol032.set(ZERO);
		printerRecInner.znofpol042.set(ZERO);
		printerRecInner.tgtpcnt012.set(ZERO);
		printerRecInner.tgtpcnt022.set(ZERO);
		printerRecInner.tgtpcnt032.set(ZERO);
		printerRecInner.tgtpcnt042.set(ZERO);
		printerRecInner.instprem012.set(wsaaCompanyTday);
		printerRecInner.instprem022.set(wsaaCompanyMtd);
		printerRecInner.instprem032.set(wsaaCompanyQtd);
		printerRecInner.instprem042.set(wsaaCompanyYtd);
		printerRecInner.znofpol012.set(wsaaCompanyCntTday);
		printerRecInner.znofpol022.set(wsaaCompanyCntMtd);
		printerRecInner.znofpol032.set(wsaaCompanyCntQtd);
		printerRecInner.znofpol042.set(wsaaCompanyCntYtd);
		printerRecInner.tgtpcnt012.set(100);
		printerRecInner.tgtpcnt022.set(100);
		printerRecInner.tgtpcnt032.set(100);
		printerRecInner.tgtpcnt042.set(100);
		printerRecInner.pcamtind012.set(wsaaPercent);
		printerRecInner.pcamtind022.set(wsaaPercent);
		printerRecInner.pcamtind032.set(wsaaPercent);
		printerRecInner.pcamtind042.set(wsaaPercent);
		printerFile.printRh562t02(printerRecInner.printerRec);
	}

protected void printPageHeading2980()
	{
		pageHeading2980();
	}

protected void pageHeading2980()
	{
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		printerRecInner.printerRec.set(SPACES);
		/* MOVE WSAA-DATETEXT    TO DATETEXT    OF RH562H01-O.  <MLS002>*/
		printerRecInner.datetexc.set(wsaaDatetext);
		printerRecInner.company.set(bsprIO.getCompany());
		printerRecInner.companynm.set(wsaaCompanynm);
		printerRecInner.sdate.set(wsaaSdate);
		getBranch5100();
		printerRecInner.currcode.set(th565rec.currcode);
		printerRecInner.currencynm.set(wsaaCurrencynm);
		printerFile.printRh562h01(printerRecInner.printerRec);
		printerRecInner.printerRec.set(SPACES);
		if (annualPremium.isTrue()) {
			printerFile.printRh562h02(printerRecInner.printerRec);
		}
		else {
			printerFile.printRh562h03(printerRecInner.printerRec);
		}
		printerRecInner.printerRec.set(SPACES);
		/* MOVE WSAA-DATETEXT       TO DATETEXT01 OF RH562H04-O.<MLS002>*/
		printerRecInner.datetexc01.set(wsaaDatetext);
		/*    MOVE WSAA-CALENDAR-MONTH (WSAA-EFF-MTH)              <LA3235>*/
		/*                                TO DATETEXT02 OF RH562H04-O.     */
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(wsaaCurrentQuarter);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		printerRecInner.effdate.set(datcon1rec.extDate);
		/* MOVE WSAA-QUARTER-MONTH (WSAA-QTR-MTH)                <UWRPT>*/
		/*                   TO MTHLDESC   OF RH562H04-O.        <UWRPT>*/
		/*    WRITE PRINTER-REC           FORMAT IS 'RH562H04'.    <LA3235>*/
		printerFile.printRh562h04(printerRecInner.printerRec, indicArea);
		printerRecInner.printerRec.set(SPACES);
		if (annualPremium.isTrue()) {
			printerFile.printRh562h05(printerRecInner.printerRec);
		}
		else {
			printerFile.printRh562h06(printerRecInner.printerRec);
		}
		wsaaOverflow.set("N");
	}

protected void update3000()
	{
		/*UPDATE*/
		/**  Update database records.                                       */
		/*WRITE-DETAIL*/
		/** If first page/overflow - write standard headings                */
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.             */
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.               */
		/*EXIT*/
	}

protected void close4000()
	{
		closeFiles4010();
		resetStatuz4080();
	}

protected void closeFiles4010()
	{
		if (isNE(wsaaOpenedFile, "Y")) {
			return ;
		}
		/*   Close the cursor                                              */
		getAppVars().freeDBConnectionIgnoreErr(sqlzstrpf1conn, sqlzstrpf1ps, sqlzstrpf1rs);
		/*  Close any open files.                                          */
		printerFile.close();
	}

protected void resetStatuz4080()
	{
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void convertPremium5000()
	{
		convertPrem5000();
	}

protected void convertPrem5000()
	{
		initialize(conlinkrec.clnk002Rec);
		conlinkrec.function.set("CVRT");
		conlinkrec.currIn.set(wsaaClnkCurrIn);
		conlinkrec.currOut.set(th565rec.currcode);
		conlinkrec.amountIn.set(wsaaPremium);
		conlinkrec.cashdate.set(wsaaOccdate);
		conlinkrec.company.set(wsaaClnkCompany);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			fatalError600();
		}
		/* MOVE CLNK-AMOUNT-OUT        TO ZRDP-AMOUNT-IN.               */
		/* PERFORM 8000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO CLNK-AMOUNT-OUT.              */
		if (isNE(conlinkrec.amountOut, 0)) {
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			callRounding8000();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
		}
		wsaaPremium.set(conlinkrec.amountOut);
	}

protected void getBranch5100()
	{
		getBrn5100();
	}

protected void getBrn5100()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		descIO.setDescitem(wsaaBranch);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		printerRecInner.branch.set(wsaaBranch);
		printerRecInner.branchnm.set(descIO.getLongdesc());
	}

protected void getProduct5200()
	{
		getProd5200();
	}

protected void getProd5200()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5688);
		descIO.setDescitem(wsaaCnttype);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		printerRecInner.cntdesc.set(descIO.getLongdesc());
	}

protected void readTh5665300()
	{
		th5665300();
	}

protected void th5665300()
	{
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th566);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th566rec.th566Rec.set(itemIO.getGenarea());
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(th565rec.currcode);
		zrdecplrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure PRINTER-REC--INNER
 */
private static final class PrinterRecInner { 

	private FixedLengthStringData printerRec = new FixedLengthStringData(142);
	private FixedLengthStringData rh562Record = new FixedLengthStringData(142).isAPartOf(printerRec, 0);
	private FixedLengthStringData rh562h01O = new FixedLengthStringData(128).isAPartOf(rh562Record, 0, REDEFINE);
	private FixedLengthStringData datetexc = new FixedLengthStringData(22).isAPartOf(rh562h01O, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(rh562h01O, 22);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(rh562h01O, 23);
	private FixedLengthStringData sdate = new FixedLengthStringData(10).isAPartOf(rh562h01O, 53);
	private FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(rh562h01O, 63);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30).isAPartOf(rh562h01O, 65);
	private FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(rh562h01O, 95);
	private FixedLengthStringData currencynm = new FixedLengthStringData(30).isAPartOf(rh562h01O, 98);
	private FixedLengthStringData rh562h04O = new FixedLengthStringData(32).isAPartOf(rh562Record, 0, REDEFINE);
	private FixedLengthStringData datetexc01 = new FixedLengthStringData(22).isAPartOf(rh562h04O, 0);
	private FixedLengthStringData effdate = new FixedLengthStringData(10).isAPartOf(rh562h04O, 22);
	private FixedLengthStringData rh562d01O = new FixedLengthStringData(142).isAPartOf(rh562Record, 0, REDEFINE);
	private FixedLengthStringData cntdesc = new FixedLengthStringData(30).isAPartOf(rh562d01O, 0);
	private ZonedDecimalData instprem01 = new ZonedDecimalData(17, 2).isAPartOf(rh562d01O, 30);
	private ZonedDecimalData tgtpcnt01 = new ZonedDecimalData(5, 2).isAPartOf(rh562d01O, 47);
	private FixedLengthStringData pcamtind01 = new FixedLengthStringData(1).isAPartOf(rh562d01O, 52);
	private ZonedDecimalData znofpol01 = new ZonedDecimalData(5, 0).isAPartOf(rh562d01O, 53);
	private ZonedDecimalData instprem02 = new ZonedDecimalData(17, 2).isAPartOf(rh562d01O, 58);
	private ZonedDecimalData tgtpcnt02 = new ZonedDecimalData(5, 2).isAPartOf(rh562d01O, 75);
	private FixedLengthStringData pcamtind02 = new FixedLengthStringData(1).isAPartOf(rh562d01O, 80);
	private ZonedDecimalData znofpol02 = new ZonedDecimalData(5, 0).isAPartOf(rh562d01O, 81);
	private ZonedDecimalData instprem03 = new ZonedDecimalData(17, 2).isAPartOf(rh562d01O, 86);
	private ZonedDecimalData tgtpcnt03 = new ZonedDecimalData(5, 2).isAPartOf(rh562d01O, 103);
	private FixedLengthStringData pcamtind03 = new FixedLengthStringData(1).isAPartOf(rh562d01O, 108);
	private ZonedDecimalData znofpol03 = new ZonedDecimalData(5, 0).isAPartOf(rh562d01O, 109);
	private ZonedDecimalData instprem04 = new ZonedDecimalData(17, 2).isAPartOf(rh562d01O, 114);
	private ZonedDecimalData tgtpcnt04 = new ZonedDecimalData(5, 2).isAPartOf(rh562d01O, 131);
	private FixedLengthStringData pcamtind04 = new FixedLengthStringData(1).isAPartOf(rh562d01O, 136);
	private ZonedDecimalData znofpol04 = new ZonedDecimalData(5, 0).isAPartOf(rh562d01O, 137);
	private FixedLengthStringData rh562t01O = new FixedLengthStringData(112).isAPartOf(rh562Record, 0, REDEFINE);
	private ZonedDecimalData instprem011 = new ZonedDecimalData(17, 2).isAPartOf(rh562t01O, 0);
	private ZonedDecimalData tgtpcnt011 = new ZonedDecimalData(5, 2).isAPartOf(rh562t01O, 17);
	private FixedLengthStringData pcamtind011 = new FixedLengthStringData(1).isAPartOf(rh562t01O, 22);
	private ZonedDecimalData znofpol011 = new ZonedDecimalData(5, 0).isAPartOf(rh562t01O, 23);
	private ZonedDecimalData instprem021 = new ZonedDecimalData(17, 2).isAPartOf(rh562t01O, 28);
	private ZonedDecimalData tgtpcnt021 = new ZonedDecimalData(5, 2).isAPartOf(rh562t01O, 45);
	private FixedLengthStringData pcamtind021 = new FixedLengthStringData(1).isAPartOf(rh562t01O, 50);
	private ZonedDecimalData znofpol021 = new ZonedDecimalData(5, 0).isAPartOf(rh562t01O, 51);
	private ZonedDecimalData instprem031 = new ZonedDecimalData(17, 2).isAPartOf(rh562t01O, 56);
	private ZonedDecimalData tgtpcnt031 = new ZonedDecimalData(5, 2).isAPartOf(rh562t01O, 73);
	private FixedLengthStringData pcamtind031 = new FixedLengthStringData(1).isAPartOf(rh562t01O, 78);
	private ZonedDecimalData znofpol031 = new ZonedDecimalData(5, 0).isAPartOf(rh562t01O, 79);
	private ZonedDecimalData instprem041 = new ZonedDecimalData(17, 2).isAPartOf(rh562t01O, 84);
	private ZonedDecimalData tgtpcnt041 = new ZonedDecimalData(5, 2).isAPartOf(rh562t01O, 101);
	private FixedLengthStringData pcamtind041 = new FixedLengthStringData(1).isAPartOf(rh562t01O, 106);
	private ZonedDecimalData znofpol041 = new ZonedDecimalData(5, 0).isAPartOf(rh562t01O, 107);
	private FixedLengthStringData rh562t02O = new FixedLengthStringData(112).isAPartOf(rh562Record, 0, REDEFINE);
	private ZonedDecimalData instprem012 = new ZonedDecimalData(17, 2).isAPartOf(rh562t02O, 0);
	private ZonedDecimalData tgtpcnt012 = new ZonedDecimalData(5, 2).isAPartOf(rh562t02O, 17);
	private FixedLengthStringData pcamtind012 = new FixedLengthStringData(1).isAPartOf(rh562t02O, 22);
	private ZonedDecimalData znofpol012 = new ZonedDecimalData(5, 0).isAPartOf(rh562t02O, 23);
	private ZonedDecimalData instprem022 = new ZonedDecimalData(17, 2).isAPartOf(rh562t02O, 28);
	private ZonedDecimalData tgtpcnt022 = new ZonedDecimalData(5, 2).isAPartOf(rh562t02O, 45);
	private FixedLengthStringData pcamtind022 = new FixedLengthStringData(1).isAPartOf(rh562t02O, 50);
	private ZonedDecimalData znofpol022 = new ZonedDecimalData(5, 0).isAPartOf(rh562t02O, 51);
	private ZonedDecimalData instprem032 = new ZonedDecimalData(17, 2).isAPartOf(rh562t02O, 56);
	private ZonedDecimalData tgtpcnt032 = new ZonedDecimalData(5, 2).isAPartOf(rh562t02O, 73);
	private FixedLengthStringData pcamtind032 = new FixedLengthStringData(1).isAPartOf(rh562t02O, 78);
	private ZonedDecimalData znofpol032 = new ZonedDecimalData(5, 0).isAPartOf(rh562t02O, 79);
	private ZonedDecimalData instprem042 = new ZonedDecimalData(17, 2).isAPartOf(rh562t02O, 84);
	private ZonedDecimalData tgtpcnt042 = new ZonedDecimalData(5, 2).isAPartOf(rh562t02O, 101);
	private FixedLengthStringData pcamtind042 = new FixedLengthStringData(1).isAPartOf(rh562t02O, 106);
	private ZonedDecimalData znofpol042 = new ZonedDecimalData(5, 0).isAPartOf(rh562t02O, 107);
}
/*
 * Class transformed  from Data Structure WSAA-PRD-ANN-TOTALS--INNER
 */
private static final class WsaaPrdAnnTotalsInner { 
		/* WSAA-PRD-ANN-TOTALS */
	private PackedDecimalData wsaaPrdAnnTday = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPrdAnnTdayCnt = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaPrdAnnMtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPrdAnnMtdCnt = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaPrdAnnQtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPrdAnnQtdCnt = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaPrdAnnYtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPrdAnnYtdCnt = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaPrdTdayPrcAnn = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaPrdMtdPrcAnn = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaPrdQtdPrcAnn = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaPrdYtdPrcAnn = new PackedDecimalData(5, 2);
}
/*
 * Class transformed  from Data Structure WSAA-PRD-SGP-TOTALS--INNER
 */
private static final class WsaaPrdSgpTotalsInner { 
		/* WSAA-PRD-SGP-TOTALS */
	private PackedDecimalData wsaaPrdSgpTday = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPrdSgpTdayCnt = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaPrdSgpMtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPrdSgpMtdCnt = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaPrdSgpQtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPrdSgpQtdCnt = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaPrdSgpYtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPrdSgpYtdCnt = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaPrdTdayPrcSgp = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaPrdMtdPrcSgp = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaPrdQtdPrcSgp = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaPrdYtdPrcSgp = new PackedDecimalData(5, 2);
}
}
