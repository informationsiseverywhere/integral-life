package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sh601screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 14, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh601ScreenVars sv = (Sh601ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh601screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh601ScreenVars screenVars = (Sh601ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.lapday01.setClassString("");
		screenVars.lapday02.setClassString("");
		screenVars.lapday03.setClassString("");
		screenVars.lapday04.setClassString("");
		screenVars.lapday05.setClassString("");
		screenVars.lapday06.setClassString("");
		screenVars.lapday07.setClassString("");
		screenVars.lapday08.setClassString("");
		screenVars.weeks01.setClassString("");
		screenVars.weeks02.setClassString("");
		screenVars.weeks03.setClassString("");
		screenVars.weeks04.setClassString("");
		screenVars.weeks05.setClassString("");
		screenVars.weeks06.setClassString("");
		screenVars.weeks07.setClassString("");
		screenVars.weeks08.setClassString("");
		screenVars.weeks09.setClassString("");
		screenVars.weeks10.setClassString("");
	}

/**
 * Clear all the variables in Sh601screen
 */
	public static void clear(VarModel pv) {
		Sh601ScreenVars screenVars = (Sh601ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.lapday01.clear();
		screenVars.lapday02.clear();
		screenVars.lapday03.clear();
		screenVars.lapday04.clear();
		screenVars.lapday05.clear();
		screenVars.lapday06.clear();
		screenVars.lapday07.clear();
		screenVars.lapday08.clear();
		screenVars.weeks01.clear();
		screenVars.weeks02.clear();
		screenVars.weeks03.clear();
		screenVars.weeks04.clear();
		screenVars.weeks05.clear();
		screenVars.weeks06.clear();
		screenVars.weeks07.clear();
		screenVars.weeks08.clear();
		screenVars.weeks09.clear();
		screenVars.weeks10.clear();
	}
}
