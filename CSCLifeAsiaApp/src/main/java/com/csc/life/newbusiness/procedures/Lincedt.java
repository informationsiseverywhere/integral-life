package com.csc.life.newbusiness.procedures;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.commons.lang3.StringUtils;
import com.csc.life.contractservicing.recordstructures.LincpfHelper;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.LincpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.SlncpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.dataaccess.model.Lincpf;
import com.csc.life.newbusiness.dataaccess.model.Slncpf;
import com.csc.life.newbusiness.tablestructures.Tjl47rec;

/**
 * <pre>
 * This subroutine writes data to LINCPF and SLNCPF based on comparisons 
 * done with values present in TJL06.
 * 
 * Case 1:
 * If Life Assured's age is less than the age specified in Juvenile Age
 * field of TJL06 and the sum insured on this policy for components present
 * in TJL06 is greater than or equal to the Minimum Sum Assured for Juvenile
 * field in TJL06 
 * OR
 * Case 2:
 * If Life Assured's age is greater than the age specified in Juvenile Age
 * field of TJL06 and the sum insured on this policy for components present
 * in TJL06 is greater than or equal to the Minimum Sum Assured field in TJL06
 * then  in both Case 1 and Case 2 create a record in LINCPF and SLNCPF.
 * 
 * There are two type of components for this requirement.
 * 1. Hospitalization Benefit Component
 * 2. Death Benefit component
 * 
 * If there are multiple Death Benefit Components or Hospital Benefit Components
 * on a policy such as TEN1 and TRM1 then their cumulative Sum Insured will be 
 * sent to LINC.
 * 
 * Whenever a new record is inserted in LINCPF or previous follow up is marked as
 * complete but response from LINC is not yet received then a follow up SLP is generated.
 * 
 * This subroutine emphasizes on New Business Transactions.
 * 
 * </pre>
 * @author gsaluja2
 *
 */
public class Lincedt implements LincNBService{

	@Autowired
	private LincpfDAO lincpfDAO;
	
	@Autowired
	private SlncpfDAO slncpfDAO;
	
	@Autowired
	private CovtpfDAO covtpfDAO;
	
	@Autowired
	private LincCommonsUtil lincCommonsUtil;
		
	private Lincpf lincpf;
		
	private Slncpf slncpfRecord;
	
	private LincpfHelper lincpfHelper = null;
	
	private Map<String, BigDecimal> crtableSuminsMap;
	
	private static final String ZERO = "0";
	
	private int zadrddte;
	
	private int zexdrddte;
	
	public Lincedt() {
		// Does nothing
	}
	
	@Override
	public void newBusinessProposal(LincpfHelper lincpfHelper, Tjl47rec tjl47rec) {
		slncpfRecord = null;
		boolean insertLincData = false;
		boolean insertSlncData = false;
		this.lincpfHelper = lincpfHelper;
		if(this.lincpfHelper != null) {
			lincpf = lincpfDAO.readLincpfData(lincpfHelper.getChdrnum());
			if(lincpf==null)
				insertLincData = true;
			else
				slncpfRecord = slncpfDAO.readSlncpfData(lincpf.getChdrnum());
			if(slncpfRecord == null)
				insertSlncData = true;
			getCoverageDetails();
			performNBOperations(tjl47rec, insertLincData, insertSlncData);
		}
	}
	
	public void getCoverageDetails(){
		zadrddte = 0;
		zexdrddte = 0;
		crtableSuminsMap = new HashMap<>();
		List<Covtpf> covtList = covtpfDAO.searchCovtRecordByCoyNum(lincpfHelper.getChdrcoy(), lincpfHelper.getChdrnum());
		if(covtList!=null && !covtList.isEmpty()) {
			for(Covtpf covt: covtList) {
				if(crtableSuminsMap.containsKey(covt.getCrtable())) {
					crtableSuminsMap.put(covt.getCrtable(), crtableSuminsMap.get(covt.getCrtable()).add(covt.getSumins()));
				}
				else
					crtableSuminsMap.put(covt.getCrtable(), covt.getSumins());
			}
		}
		if(!crtableSuminsMap.isEmpty()) {
			lincCommonsUtil.validateLifeAssuredAge(lincpfHelper, lincpfHelper.getOccdate());
			if(lincpfHelper.getLifeage() != 0)
				lincCommonsUtil.checkCriteria(crtableSuminsMap, lincpfHelper);
		}
	}
	
	public void performNBOperations(Tjl47rec tjl47rec, boolean insertLincData, 
			boolean insertSlncData) {
		if(lincpfHelper.isCritSatisfied() && lincCommonsUtil.validateDataForUpdate(lincpf, lincpfHelper)) {
			lincpf = lincCommonsUtil.setupLincpf(lincpfHelper, lincpf);
			lincpf.setZreclass(tjl47rec.rectype01.toString());
			lincpf.setZmodtype(tjl47rec.chgtype01.toString());
			lincCommonsUtil.setupCommonLinc(lincpf, lincpfHelper);
			lincCommonsUtil.setupLincDates(lincpf, 0, lincpfHelper.getOccdate(), 0, 0, zadrddte, zexdrddte);
			lincCommonsUtil.generateFollowUp(lincpfHelper);
			if(insertLincData) {
				lincpf.setKlinckey(StringUtils.leftPad(StringUtils.EMPTY, 12, ZERO));
				lincpf.setRecvdate(0);
				lincpfDAO.insertLincData(lincpf);
			}
			else
				lincpfDAO.updateLincData(lincpf);
			slncpfRecord = lincCommonsUtil.setupSlncpf(lincpf, slncpfRecord);
			if(insertSlncData)
				slncpfDAO.insertSlncData(slncpfRecord);
			else
				slncpfDAO.updateSlncData(slncpfRecord);
		}
	}
	
	@Override
	public void newBusinessIssuance(LincpfHelper lincpfHelper, Tjl47rec tjl47rec) {
		this.lincpfHelper = lincpfHelper;
		if(this.lincpfHelper != null) {
			lincpf = lincpfDAO.readLincpfData(this.lincpfHelper.getChdrnum());
			if(lincpf!=null) {
				getCoverageDetails();
				if(lincpfHelper.isCritSatisfied()) {
					lincCommonsUtil.setupCommonLinc(lincpf, lincpfHelper);
					lincCommonsUtil.setupLincDates(lincpf, lincpf.getZoccdate(), 0, 0, 0, zadrddte, zexdrddte);
					lincpf.setZreclass(tjl47rec.rectype01.toString());
					lincpf.setZmodtype(tjl47rec.chgtype01.toString());
					lincpfDAO.updateLincData(lincpf);
					slncpfRecord = lincCommonsUtil.setupSlncpf(lincpf, slncpfRecord);
					slncpfDAO.insertSlncData(slncpfRecord);
				}
			}
		}
	}
}