package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: LifepfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:44
 * Class transformed from LIFEPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LifepfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 94+DD.relationwithowner.length+DD.datime.length; //fwang3 ICIL-4
	public FixedLengthStringData liferec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData lifepfRecord = liferec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(liferec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(liferec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(liferec);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(liferec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(liferec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(liferec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(liferec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(liferec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(liferec);
	public PackedDecimalData lifeCommDate = DD.lcdte.copy().isAPartOf(liferec);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(liferec);
	public FixedLengthStringData liferel = DD.liferel.copy().isAPartOf(liferec);
	public PackedDecimalData cltdob = DD.cltdob.copy().isAPartOf(liferec);
	public FixedLengthStringData cltsex = DD.cltsex.copy().isAPartOf(liferec);
	public PackedDecimalData anbAtCcd = DD.anbccd.copy().isAPartOf(liferec);
	public FixedLengthStringData ageadm = DD.ageadm.copy().isAPartOf(liferec);
	public FixedLengthStringData selection = DD.selection.copy().isAPartOf(liferec);
	public FixedLengthStringData smoking = DD.smoking.copy().isAPartOf(liferec);
	public FixedLengthStringData occup = DD.occup.copy().isAPartOf(liferec);
	public FixedLengthStringData pursuit01 = DD.pursuit.copy().isAPartOf(liferec);
	public FixedLengthStringData pursuit02 = DD.pursuit.copy().isAPartOf(liferec);
	public FixedLengthStringData maritalState = DD.martal.copy().isAPartOf(liferec);
	public FixedLengthStringData sbstdl = DD.sbstdl.copy().isAPartOf(liferec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(liferec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(liferec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(liferec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(liferec);
	public FixedLengthStringData relation = DD.relationwithowner.copy().isAPartOf(liferec);//fwang3 ICIL-4
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(liferec);
	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public LifepfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for LifepfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public LifepfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for LifepfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public LifepfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for LifepfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public LifepfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("LIFEPF");
	}
	@Override
	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"VALIDFLAG, " +
							"STATCODE, " +
							"TRANNO, " +
							"CURRFROM, " +
							"CURRTO, " +
							"LIFE, " +
							"JLIFE, " +
							"LCDTE, " +
							"LIFCNUM, " +
							"LIFEREL, " +
							"CLTDOB, " +
							"CLTSEX, " +
							"ANBCCD, " +
							"AGEADM, " +
							"SELECTION, " +
							"SMOKING, " +
							"OCCUP, " +
							"PURSUIT01, " +
							"PURSUIT02, " +
							"MARTAL, " +
							"SBSTDL, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"RELATION, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     validflag,
                                     statcode,
                                     tranno,
                                     currfrom,
                                     currto,
                                     life,
                                     jlife,
                                     lifeCommDate,
                                     lifcnum,
                                     liferel,
                                     cltdob,
                                     cltsex,
                                     anbAtCcd,
                                     ageadm,
                                     selection,
                                     smoking,
                                     occup,
                                     pursuit01,
                                     pursuit02,
                                     maritalState,
                                     sbstdl,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     relation,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/
	@Override
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		validflag.clear();
  		statcode.clear();
  		tranno.clear();
  		currfrom.clear();
  		currto.clear();
  		life.clear();
  		jlife.clear();
  		lifeCommDate.clear();
  		lifcnum.clear();
  		liferel.clear();
  		cltdob.clear();
  		cltsex.clear();
  		anbAtCcd.clear();
  		ageadm.clear();
  		selection.clear();
  		smoking.clear();
  		occup.clear();
  		pursuit01.clear();
  		pursuit02.clear();
  		maritalState.clear();
  		sbstdl.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		relation.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getLiferec() {
  		return liferec;
	}

	public FixedLengthStringData getLifepfRecord() {
  		return lifepfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/
	@Override
	public void set(Object what) {
  		setLiferec(what);
	}

	public void setLiferec(Object what) {
  		this.liferec.set(what);
	}

	public void setLifepfRecord(Object what) {
  		this.lifepfRecord.set(what);
	}
	@Override
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(liferec.getLength());
		result.set(liferec);
  		return result;
	}
	@Override
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}