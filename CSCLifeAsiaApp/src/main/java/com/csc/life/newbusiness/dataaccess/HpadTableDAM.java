package com.csc.life.newbusiness.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HpadTableDAM.java
 * Date: Sun, 30 Aug 2009 03:41:12
 * Class transformed from HPAD.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HpadTableDAM extends HpadpfTableDAM {

	public HpadTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("HPAD");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "VALIDFLAG, " +
		            "HPROPDTE, " +
		            "HPRRCVDT, " +
		            "HISSDTE, " +
		            "HUWDCDTE, " +
		            "HOISSDTE, " +
		            "ZDOCTOR, " +
		            "ZNFOPT, " +
		            "ZSUFCDTE, " +
		            "PROCFLG, " +
		            "DLVRMODE, " +
		            "DESPDATE, " +
		            "PACKDATE, " +
		            "REMDTE, " +
		            "DEEMDATE, " +
		            "NXTDTE, " +
		            "INCEXC, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               validflag,
                               hpropdte,
                               hprrcvdt,
                               hissdte,
                               huwdcdte,
                               hoissdte,
                               zdoctor,
                               znfopt,
                               zsufcdte,
                               procflg,
                               dlvrmode,
                               despdate,
                               packdate,
                               remdte,
                               deemdate,
                               nextActDate,
                               incexc,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(247);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(128);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getValidflag().toInternal()
					+ getHpropdte().toInternal()
					+ getHprrcvdt().toInternal()
					+ getHissdte().toInternal()
					+ getHuwdcdte().toInternal()
					+ getHoissdte().toInternal()
					+ getZdoctor().toInternal()
					+ getZnfopt().toInternal()
					+ getZsufcdte().toInternal()
					+ getProcflg().toInternal()
					+ getDlvrmode().toInternal()
					+ getDespdate().toInternal()
					+ getPackdate().toInternal()
					+ getRemdte().toInternal()
					+ getDeemdate().toInternal()
					+ getNextActDate().toInternal()
					+ getIncexc().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, hpropdte);
			what = ExternalData.chop(what, hprrcvdt);
			what = ExternalData.chop(what, hissdte);
			what = ExternalData.chop(what, huwdcdte);
			what = ExternalData.chop(what, hoissdte);
			what = ExternalData.chop(what, zdoctor);
			what = ExternalData.chop(what, znfopt);
			what = ExternalData.chop(what, zsufcdte);
			what = ExternalData.chop(what, procflg);
			what = ExternalData.chop(what, dlvrmode);
			what = ExternalData.chop(what, despdate);
			what = ExternalData.chop(what, packdate);
			what = ExternalData.chop(what, remdte);
			what = ExternalData.chop(what, deemdate);
			what = ExternalData.chop(what, nextActDate);
			what = ExternalData.chop(what, incexc);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getHpropdte() {
		return hpropdte;
	}
	public void setHpropdte(Object what) {
		setHpropdte(what, false);
	}
	public void setHpropdte(Object what, boolean rounded) {
		if (rounded)
			hpropdte.setRounded(what);
		else
			hpropdte.set(what);
	}	
	public PackedDecimalData getHprrcvdt() {
		return hprrcvdt;
	}
	public void setHprrcvdt(Object what) {
		setHprrcvdt(what, false);
	}
	public void setHprrcvdt(Object what, boolean rounded) {
		if (rounded)
			hprrcvdt.setRounded(what);
		else
			hprrcvdt.set(what);
	}	
	public PackedDecimalData getHissdte() {
		return hissdte;
	}
	public void setHissdte(Object what) {
		setHissdte(what, false);
	}
	public void setHissdte(Object what, boolean rounded) {
		if (rounded)
			hissdte.setRounded(what);
		else
			hissdte.set(what);
	}	
	public PackedDecimalData getHuwdcdte() {
		return huwdcdte;
	}
	public void setHuwdcdte(Object what) {
		setHuwdcdte(what, false);
	}
	public void setHuwdcdte(Object what, boolean rounded) {
		if (rounded)
			huwdcdte.setRounded(what);
		else
			huwdcdte.set(what);
	}	
	public PackedDecimalData getHoissdte() {
		return hoissdte;
	}
	public void setHoissdte(Object what) {
		setHoissdte(what, false);
	}
	public void setHoissdte(Object what, boolean rounded) {
		if (rounded)
			hoissdte.setRounded(what);
		else
			hoissdte.set(what);
	}	
	public FixedLengthStringData getZdoctor() {
		return zdoctor;
	}
	public void setZdoctor(Object what) {
		zdoctor.set(what);
	}	
	public FixedLengthStringData getZnfopt() {
		return znfopt;
	}
	public void setZnfopt(Object what) {
		znfopt.set(what);
	}	
	public PackedDecimalData getZsufcdte() {
		return zsufcdte;
	}
	public void setZsufcdte(Object what) {
		setZsufcdte(what, false);
	}
	public void setZsufcdte(Object what, boolean rounded) {
		if (rounded)
			zsufcdte.setRounded(what);
		else
			zsufcdte.set(what);
	}	
	public FixedLengthStringData getProcflg() {
		return procflg;
	}
	public void setProcflg(Object what) {
		procflg.set(what);
	}	
	public FixedLengthStringData getDlvrmode() {
		return dlvrmode;
	}
	public void setDlvrmode(Object what) {
		dlvrmode.set(what);
	}	
	public PackedDecimalData getDespdate() {
		return despdate;
	}
	public void setDespdate(Object what) {
		setDespdate(what, false);
	}
	public void setDespdate(Object what, boolean rounded) {
		if (rounded)
			despdate.setRounded(what);
		else
			despdate.set(what);
	}	
	public PackedDecimalData getPackdate() {
		return packdate;
	}
	public void setPackdate(Object what) {
		setPackdate(what, false);
	}
	public void setPackdate(Object what, boolean rounded) {
		if (rounded)
			packdate.setRounded(what);
		else
			packdate.set(what);
	}	
	public PackedDecimalData getRemdte() {
		return remdte;
	}
	public void setRemdte(Object what) {
		setRemdte(what, false);
	}
	public void setRemdte(Object what, boolean rounded) {
		if (rounded)
			remdte.setRounded(what);
		else
			remdte.set(what);
	}	
	public PackedDecimalData getDeemdate() {
		return deemdate;
	}
	public void setDeemdate(Object what) {
		setDeemdate(what, false);
	}
	public void setDeemdate(Object what, boolean rounded) {
		if (rounded)
			deemdate.setRounded(what);
		else
			deemdate.set(what);
	}	
	public PackedDecimalData getNextActDate() {
		return nextActDate;
	}
	public void setNextActDate(Object what) {
		setNextActDate(what, false);
	}
	public void setNextActDate(Object what, boolean rounded) {
		if (rounded)
			nextActDate.setRounded(what);
		else
			nextActDate.set(what);
	}	
	public FixedLengthStringData getIncexc() {
		return incexc;
	}
	public void setIncexc(Object what) {
		incexc.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		validflag.clear();
		hpropdte.clear();
		hprrcvdt.clear();
		hissdte.clear();
		huwdcdte.clear();
		hoissdte.clear();
		zdoctor.clear();
		znfopt.clear();
		zsufcdte.clear();
		procflg.clear();
		dlvrmode.clear();
		despdate.clear();
		packdate.clear();
		remdte.clear();
		deemdate.clear();
		nextActDate.clear();
		incexc.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}