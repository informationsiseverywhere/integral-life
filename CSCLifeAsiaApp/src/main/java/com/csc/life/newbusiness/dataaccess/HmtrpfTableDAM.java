package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HmtrpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:32
 * Class transformed from HMTRPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HmtrpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 66;
	public FixedLengthStringData hmtrrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hmtrpfRecord = hmtrrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hmtrrec);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(hmtrrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(hmtrrec);
	public PackedDecimalData mnth = DD.mnth.copy().isAPartOf(hmtrrec);
	public PackedDecimalData year = DD.year.copy().isAPartOf(hmtrrec);
	public PackedDecimalData cntcount = DD.cntcount.copy().isAPartOf(hmtrrec);
	public PackedDecimalData updateDate = DD.upddte.copy().isAPartOf(hmtrrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hmtrrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hmtrrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hmtrrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HmtrpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HmtrpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HmtrpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HmtrpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HmtrpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HmtrpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HmtrpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HMTRPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CNTBRANCH, " +
							"CNTTYPE, " +
							"MNTH, " +
							"YEAR, " +
							"CNTCOUNT, " +
							"UPDDTE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     cntbranch,
                                     cnttype,
                                     mnth,
                                     year,
                                     cntcount,
                                     updateDate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		cntbranch.clear();
  		cnttype.clear();
  		mnth.clear();
  		year.clear();
  		cntcount.clear();
  		updateDate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHmtrrec() {
  		return hmtrrec;
	}

	public FixedLengthStringData getHmtrpfRecord() {
  		return hmtrpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHmtrrec(what);
	}

	public void setHmtrrec(Object what) {
  		this.hmtrrec.set(what);
	}

	public void setHmtrpfRecord(Object what) {
  		this.hmtrpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hmtrrec.getLength());
		result.set(hmtrrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}