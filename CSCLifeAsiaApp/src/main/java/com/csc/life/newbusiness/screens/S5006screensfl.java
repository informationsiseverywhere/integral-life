package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5006screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 6, 24, 16, 1, 2, 3, 21}; 
	public static int maxRecords = 15;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1, 2}; 
	private static final String UWORFLAG = "uworflag";
	private static final String UWORREASON = "uworreason";
	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {8, 21, 9, 72}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5006ScreenVars sv = (S5006ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5006screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5006screensfl, 
			sv.S5006screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5006ScreenVars sv = (S5006ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5006screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5006ScreenVars sv = (S5006ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5006screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5006screensflWritten.gt(0))
		{
			sv.s5006screensfl.setCurrentIndex(0);
			sv.S5006screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5006ScreenVars sv = (S5006ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5006screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5006ScreenVars screenVars = (S5006ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.ctable.setFieldName("ctable");
				screenVars.rtable.setFieldName("rtable");
				screenVars.longdesc.setFieldName("longdesc");
				screenVars.hrequired.setFieldName("hrequired");
				
				screenVars.uworflag.setFieldName(UWORFLAG);
				screenVars.uworreason.setFieldName(UWORREASON);
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.select.set(dm.getField("select"));
			screenVars.ctable.set(dm.getField("ctable"));
			screenVars.rtable.set(dm.getField("rtable"));
			screenVars.longdesc.set(dm.getField("longdesc"));
			screenVars.hrequired.set(dm.getField("hrequired"));
			
			screenVars.uworflag.set(dm.getField(UWORFLAG));
			screenVars.uworreason.set(dm.getField(UWORREASON));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5006ScreenVars screenVars = (S5006ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.ctable.setFieldName("ctable");
				screenVars.rtable.setFieldName("rtable");
				screenVars.longdesc.setFieldName("longdesc");
				screenVars.hrequired.setFieldName("hrequired");
				
				screenVars.uworflag.setFieldName(UWORFLAG);
				screenVars.uworreason.setFieldName(UWORREASON);
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("select").set(screenVars.select);
			dm.getField("ctable").set(screenVars.ctable);
			dm.getField("rtable").set(screenVars.rtable);
			dm.getField("longdesc").set(screenVars.longdesc);
			dm.getField("hrequired").set(screenVars.hrequired);
			
			dm.getField(UWORFLAG).set(screenVars.uworflag);
			dm.getField(UWORREASON).set(screenVars.uworreason);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5006screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5006ScreenVars screenVars = (S5006ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.ctable.clearFormatting();
		screenVars.rtable.clearFormatting();
		screenVars.longdesc.clearFormatting();
		screenVars.hrequired.clearFormatting();
		
		screenVars.uworflag.clearFormatting();
		screenVars.uworreason.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5006ScreenVars screenVars = (S5006ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.select.setClassString("");
		screenVars.ctable.setClassString("");
		screenVars.rtable.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.hrequired.setClassString("");
		
		screenVars.uworflag.setClassString("");
		screenVars.uworreason.setClassString("");
	}

/**
 * Clear all the variables in S5006screensfl
 */
	public static void clear(VarModel pv) {
		S5006ScreenVars screenVars = (S5006ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.select.clear();
		screenVars.ctable.clear();
		screenVars.rtable.clear();
		screenVars.longdesc.clear();
		screenVars.hrequired.clear();
		
		screenVars.uworflag.clear();
		screenVars.uworreason.clear();
	}
}
