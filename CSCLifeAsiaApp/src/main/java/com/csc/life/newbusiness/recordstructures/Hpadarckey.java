package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:43
 * Description:
 * Copybook name: HPADARCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hpadarckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hpadarcFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hpadarcKey = new FixedLengthStringData(64).isAPartOf(hpadarcFileKey, 0, REDEFINE);
  	public FixedLengthStringData hpadarcChdrcoy = new FixedLengthStringData(1).isAPartOf(hpadarcKey, 0);
  	public FixedLengthStringData hpadarcChdrnum = new FixedLengthStringData(8).isAPartOf(hpadarcKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(hpadarcKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hpadarcFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hpadarcFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}