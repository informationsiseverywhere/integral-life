package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH609
 * @version 1.0 generated on 30/08/09 07:06
 * @author Quipoz
 */
public class Sh609ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(200);
	public FixedLengthStringData dataFields = new FixedLengthStringData(56).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData fupcdes = DD.fupcdes.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData indic = DD.indic.copy().isAPartOf(dataFields,5);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData language = DD.language.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,15);
	public ZonedDecimalData prcnt = DD.prcnt.copyToZonedDecimal().isAPartOf(dataFields,45);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData taxind = DD.taxind.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 56);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData fupcdesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData indicErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData languageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData prcntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData taxindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 92);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] fupcdesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] indicOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] languageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] prcntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] taxindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sh609screenWritten = new LongData(0);
	public LongData Sh609protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh609ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {language, company, tabl, item, longdesc, fupcdes, taxind, indic, prcnt};
		screenOutFields = new BaseData[][] {languageOut, companyOut, tablOut, itemOut, longdescOut, fupcdesOut, taxindOut, indicOut, prcntOut};
		screenErrFields = new BaseData[] {languageErr, companyErr, tablErr, itemErr, longdescErr, fupcdesErr, taxindErr, indicErr, prcntErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh609screen.class;
		protectRecord = Sh609protect.class;
	}

}
