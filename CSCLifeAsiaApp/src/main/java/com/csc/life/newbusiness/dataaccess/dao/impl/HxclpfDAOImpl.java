package com.csc.life.newbusiness.dataaccess.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.csc.life.newbusiness.dataaccess.dao.HxclpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Hxclpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class HxclpfDAOImpl extends BaseDAOImpl<Hxclpf> implements HxclpfDAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(HxclpfDAOImpl.class);
	
	public Hxclpf readRecord(String chdrcoy, String chdrnum, int fupno){
		StringBuilder sqlSchemeSelect1 = new StringBuilder(
				"select * From HXCLPF where CHDRCOY=? and CHDRNUM=? and FUPNO=?");
		Hxclpf hxclpfRec = null;
		
		try {
		  PreparedStatement ps = getPrepareStatement(sqlSchemeSelect1.toString());
		  ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setInt(3, fupno);
			ResultSet rs = null;
				rs = ps.executeQuery();
			    while (rs.next()) {		
			    hxclpfRec = new Hxclpf();
			    hxclpfRec.setChdrcoy(rs.getString("CHDRCOY"));
			    hxclpfRec.setChdrnum(rs.getString("CHDRNUM"));
			    }
		}
			catch(SQLException e)
			{
				LOGGER.error(" HxclpfDAOImpl.readRecord", e);//IJTI-1561
				throw new SQLRuntimeException(e);	
			}
			
			return hxclpfRec;
	}
	public void deleteRecords(String chdrcoy, String chdrnum, int fupno){
		StringBuilder sql_hxclpf_delete = new StringBuilder();
		sql_hxclpf_delete.append("DELETE FROM HXCLPF  ");
		sql_hxclpf_delete.append("WHERE CHDRCOY='"+chdrcoy+"'");
		sql_hxclpf_delete.append("AND CHDRNUM='"+chdrnum+"'");
		sql_hxclpf_delete.append("AND FUPNO='"+fupno+"'");
		
		PreparedStatement preparedStmt = getPrepareStatement(sql_hxclpf_delete.toString());
        int count;
        try{
        	count = preparedStmt.executeUpdate();
        }catch (SQLException e) {
            LOGGER.error("deleteRecords()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
        	close(preparedStmt,null);
        }
	}
 //IBPLIFE-504 start
	@Override
	public Hxclpf readHxcl(String chdrcoy, String chdrnum, int fupno) {

		StringBuilder sqlSchemeSelect1 = new StringBuilder(
				"select CHDRCOY, CHDRNUM, FUPNO "
				+ "from HXCLPF where CHDRCOY=? and CHDRNUM=? and FUPNO=?");
		Hxclpf hxclpfRec = null;
		 PreparedStatement ps = getPrepareStatement(sqlSchemeSelect1.toString());
		 ResultSet rs = null;
		try {
		 
		  ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setInt(3, fupno);
			
				rs = executeQuery(ps);
				if(rs.next()) {		
			    hxclpfRec = new Hxclpf();
			    hxclpfRec.setChdrcoy(rs.getString("CHDRCOY"));
			    hxclpfRec.setChdrnum(rs.getString("CHDRNUM"));
			    hxclpfRec.setFupno(rs.getInt("FUPNO"));
			    }
		}
			catch(SQLException e)
			{
				LOGGER.error("readHxcl()", e);
				throw new SQLRuntimeException(e);	
			}
			finally {
            close(ps, rs);
        }
			return hxclpfRec;
	
	}
 //IBPLIFE-504 end
}
