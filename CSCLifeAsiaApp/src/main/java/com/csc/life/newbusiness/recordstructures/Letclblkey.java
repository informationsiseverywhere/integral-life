package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:05:53
 * Description:
 * Copybook name: LETCLBLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Letclblkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData letclblFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData letclblKey = new FixedLengthStringData(256).isAPartOf(letclblFileKey, 0, REDEFINE);
  	public FixedLengthStringData letclblRequestCompany = new FixedLengthStringData(1).isAPartOf(letclblKey, 0);
  	public FixedLengthStringData letclblLetterType = new FixedLengthStringData(8).isAPartOf(letclblKey, 1);
  	public PackedDecimalData letclblLetterSeqno = new PackedDecimalData(7, 0).isAPartOf(letclblKey, 9);
  	public FixedLengthStringData letclblClntcoy = new FixedLengthStringData(1).isAPartOf(letclblKey, 13);
  	public FixedLengthStringData letclblClntnum = new FixedLengthStringData(8).isAPartOf(letclblKey, 14);
  	public FixedLengthStringData filler = new FixedLengthStringData(234).isAPartOf(letclblKey, 22, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(letclblFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		letclblFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}