package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR591
 * @version 1.0 generated on 30/08/09 07:21
 * @author Quipoz
 */
public class Sr591ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(647);
	public FixedLengthStringData dataFields = new FixedLengthStringData(471).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData fupcode = DD.fupcde.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData hxcllettyp = DD.hxcllettyp.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData hxclnotes = new FixedLengthStringData(450).isAPartOf(dataFields, 20);
	public FixedLengthStringData[] hxclnote = FLSArrayPartOfStructure(6, 75, hxclnotes, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(450).isAPartOf(hxclnotes, 0, FILLER_REDEFINE);
	public FixedLengthStringData hxclnote01 = DD.hxclnote.copy().isAPartOf(filler,0);
	public FixedLengthStringData hxclnote02 = DD.hxclnote.copy().isAPartOf(filler,75);
	public FixedLengthStringData hxclnote03 = DD.hxclnote.copy().isAPartOf(filler,150);
	public FixedLengthStringData hxclnote04 = DD.hxclnote.copy().isAPartOf(filler,225);
	public FixedLengthStringData hxclnote05 = DD.hxclnote.copy().isAPartOf(filler,300);
	public FixedLengthStringData hxclnote06 = DD.hxclnote.copy().isAPartOf(filler,375);
	public FixedLengthStringData notemore = DD.notemore.copy().isAPartOf(dataFields,470);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(44).isAPartOf(dataArea, 471);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData fupcdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData hxcllettypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData hxclnotesErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData[] hxclnoteErr = FLSArrayPartOfStructure(6, 4, hxclnotesErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(hxclnotesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData hxclnote01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData hxclnote02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData hxclnote03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData hxclnote04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData hxclnote05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData hxclnote06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData notemoreErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(132).isAPartOf(dataArea, 515);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] fupcdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] hxcllettypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData hxclnotesOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] hxclnoteOut = FLSArrayPartOfStructure(6, 12, hxclnotesOut, 0);
	public FixedLengthStringData[][] hxclnoteO = FLSDArrayPartOfArrayStructure(12, 1, hxclnoteOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(72).isAPartOf(hxclnotesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] hxclnote01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] hxclnote02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] hxclnote03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] hxclnote04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] hxclnote05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] hxclnote06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] notemoreOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr591screenWritten = new LongData(0);
	public LongData Sr591protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr591ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(hxclnote01Out,new String[] {null, "11",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hxclnote02Out,new String[] {null, "11",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hxclnote03Out,new String[] {null, "11",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hxclnote04Out,new String[] {null, "11",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hxclnote05Out,new String[] {null, "11",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hxclnote06Out,new String[] {null, "11",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(actionOut,new String[] {"20","12","-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hxcllettypOut,new String[] {"21","11","-21",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, fupcode, hxclnote01, hxclnote02, hxclnote03, hxclnote04, hxclnote05, hxclnote06, action, notemore, hxcllettyp};
		screenOutFields = new BaseData[][] {chdrnumOut, fupcdeOut, hxclnote01Out, hxclnote02Out, hxclnote03Out, hxclnote04Out, hxclnote05Out, hxclnote06Out, actionOut, notemoreOut, hxcllettypOut};
		screenErrFields = new BaseData[] {chdrnumErr, fupcdeErr, hxclnote01Err, hxclnote02Err, hxclnote03Err, hxclnote04Err, hxclnote05Err, hxclnote06Err, actionErr, notemoreErr, hxcllettypErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = Sr591screen.class;
		protectRecord = Sr591protect.class;
	}

}
