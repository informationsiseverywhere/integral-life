package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:48
 * Description:
 * Copybook name: PH599PAR
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ph599par extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData parmRecord = new FixedLengthStringData(13);
  	public FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(parmRecord, 0);
  	public FixedLengthStringData chdrtype = new FixedLengthStringData(3).isAPartOf(parmRecord, 2);
  	public FixedLengthStringData ctrlbrks = new FixedLengthStringData(2).isAPartOf(parmRecord, 5);
  	public FixedLengthStringData[] ctrlbrk = FLSArrayPartOfStructure(2, 1, ctrlbrks, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(ctrlbrks, 0, FILLER_REDEFINE);
  	public FixedLengthStringData ctrlbrk01 = new FixedLengthStringData(1).isAPartOf(filler, 0);
  	public FixedLengthStringData ctrlbrk02 = new FixedLengthStringData(1).isAPartOf(filler, 1);
  	public ZonedDecimalData mnth = new ZonedDecimalData(2, 0).isAPartOf(parmRecord, 7);
  	public ZonedDecimalData year = new ZonedDecimalData(4, 0).isAPartOf(parmRecord, 9);


	public void initialize() {
		COBOLFunctions.initialize(parmRecord);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRecord.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}