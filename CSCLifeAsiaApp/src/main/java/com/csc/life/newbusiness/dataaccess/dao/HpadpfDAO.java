package com.csc.life.newbusiness.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface HpadpfDAO extends BaseDAO<Hpadpf> {
	public void updateHpadRcds(List<Hpadpf> updateList);

	public Hpadpf getHpadData(String chdrcoy, String chdrnum);
	void updateRecord(Hpadpf pf);
	void insertRecord(Hpadpf pf);
	void updateDoctor(String chdrcoy, String chdrnum,String doctor);

	//ILIFE-6289 by xma3
	public void updateHuwdcdte(String chdrcoy, String chdrnum,int huwdcdte);
	public void updateFirstprmrcpdate(String chdrcoy, String chdrnum,int firstPremRcpDt);//ILJ-42
	public void updateRiskCommDate(String chdrcoy, String chdrnum,int riskCommDate);
	
	public void updateRecordsByUniqueNumber(List<Hpadpf> data);
	public void updateTntapdate(String chdrcoy, String chdrnum,int tntapdate);//IBPLIFE-2100
	public boolean updateFirstprmrcpdateList(List<Hpadpf> hpadpfList);
}