package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:33
 * Description:
 * Copybook name: T6799REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6799rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6799Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData ind = new FixedLengthStringData(1).isAPartOf(t6799Rec, 0);
  	public FixedLengthStringData subprog = new FixedLengthStringData(10).isAPartOf(t6799Rec, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(489).isAPartOf(t6799Rec, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6799Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6799Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}