package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.csv.CSVGenerator;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.cashdividends.programs.Ph504;
import com.csc.life.cashdividends.screens.Sh504ScreenVars;
import com.csc.life.contractservicing.dataaccess.PtrncwfTableDAM;
import com.csc.life.enquiries.dataaccess.AcblenqTableDAM;
import com.csc.life.enquiries.dataaccess.RtrnsacTableDAM;
import com.csc.life.newbusiness.dataaccess.BnfylnbTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.ZchxpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Zchxpf;
import com.csc.life.newbusiness.programs.P5002;
import com.csc.life.newbusiness.programs.P5003;
import com.csc.life.newbusiness.programs.P5004;
import com.csc.life.newbusiness.programs.P5005;
import com.csc.life.newbusiness.programs.P5073;
import com.csc.life.newbusiness.programs.P5074;
import com.csc.life.newbusiness.programs.P5123;
import com.csc.life.newbusiness.programs.P6378;
import com.csc.life.newbusiness.reports.R5222Report;
import com.csc.life.newbusiness.screens.S5002ScreenVars;
import com.csc.life.newbusiness.screens.S5003ScreenVars;
import com.csc.life.newbusiness.screens.S5004ScreenVars;
import com.csc.life.newbusiness.screens.S5005ScreenVars;
import com.csc.life.newbusiness.screens.S5073ScreenVars;
import com.csc.life.newbusiness.screens.S5074ScreenVars;
import com.csc.life.newbusiness.screens.S5123ScreenVars;
import com.csc.life.newbusiness.screens.S6378ScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.reassurance.dataaccess.RacdlnbTableDAM;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.life.terminationclaims.programs.P5125;
import com.csc.life.terminationclaims.programs.Pr516;
import com.csc.life.terminationclaims.programs.Pr676;
import com.csc.life.terminationclaims.screens.S5125ScreenVars;
import com.csc.life.terminationclaims.screens.Sr516ScreenVars;
import com.csc.life.terminationclaims.screens.Sr676ScreenVars;
import com.csc.life.unitlinkedprocessing.programs.P6326;
import com.csc.life.unitlinkedprocessing.screens.S6326ScreenVars;
import com.csc.smart.dataaccess.BmsgTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ErorTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.dataaccess.UsrdTableDAM;
import com.csc.smart.procedures.Conlog;
import com.csc.smart.procedures.Geterror;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Conlogrec;
import com.csc.smart.recordstructures.Scrnparams;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.SmartVarModel;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
*<pre>
*
*REMARKS.
*
*                   Contract Issue
*                   --------------
* Overview
* ________
*
* This program is part of the new 'Multi-Threading Batch
* Performance' suite. It runs directly after B5221, which 'splits'
* the ZCHXPF according to the number of billing programs to run.
* All references to the CHDR are via ZCHXPF - a temporary file
* holding all the CHDR records for this program to process.
*
* B5222 will perform all the processing for Contract Issue that is
* applicable to LIFE/400. Appropriate online programs will be called 
* from within the batch job to perform the updates that
* is applicable to FSU.
*
* Contract issue processes only those CHDR records which were
* extracted in B5221 splitter program.
*
* The following control totals are maintained within the program:
*
*       1 - No. of Proposals Read
*       2 - No. of Locked Proposals
*       3 - Underwriting Not Approvd
*       4 - No Premium Suspense
*       5 - Failed First Validation
*       6 - Failed Pre-Update
*       7 - Failed Update
*       8 - Failed Pre-Issue Validtn
*       9 - Failed Issue
*      10 - No. of Proposals Issued
*
* 1000-INITIALISE SECTION
* _______________________
*
*  -  Override ZCHXPF
*     The CRTTMPF process has already created a file specific to
*     the run using the  ZCHXPF fields and is identified  by
*     concatenating the following parameters:-
*
*     'ZCHX'
*      BPRD-SYSTEM-PARAM04
*      BSSC-SCHEDULE-NUMBER
*
*      eg ZCHXIS0001,  for the schedule number 1
*         ZCHXIS0002,  for the schedule number 2
*
*     The number of threads would have been created by the
*     CRTTMPF process by passing the parameters of the process
*     definition of the CRTTMPF.
*     To get the correct member of the above physical for B5222
*     to read from, concatenate :-
*
*     'THREAD'
*     BSPR-PROCESS-OCC-NUM
*
*  - Read the Transaction Accounting Rules table, T5645, for the 
*  	 batch program (B5222) to obtain the Premium Suspense account 
*  	 details.
*  
*  - Print the Error Report Headings.
*  
*  - Initialise Wsspcomn and Scrnparams.
*  
*
* 2000-READ SECTION
* _________________
*
* -  Read the PAYX records sequentially incrementing the control
*    total.
*
* -  If end of file move ENDP to WSSP-EDTERROR.
*
* 2500-EDIT SECTION
* _________________
*
* -  Move OK to WSSP-EDTERROR.
*
* -  Read the Contract Additional Details (HPADPF) record for 
* 	 the proposal to check its Underwriting Approved Date
* 
* -  Check that there is some money in Premium Suspense by reading 
*    the Account Balance (ACBLPF) file for the proposal, using the 
*    Subsidiary Ledger Code and Type from the first line of the 
*    Transaction Accounting Rules table, T5645.
*    
* -  Check that the proposal is valid for issue (before the risk 
*    commencement date update) by calling the Pre-Issue Validation 
*    program, P6378.
*    
* - Check if the proposal is already locked by calling the Softlocking 
*   subroutine to try and lock it.  If the proposal is already locked, 
*   increment Number of locked proposals control total and do no 
*   further processing on this proposal.  Otherwise, call the Softlocking 
*   subroutine again to unlock the proposal since it will be locked again 
*   by the online programs which will be called to update and issue 
*   the proposal
*
*  3000-UPDATE SECTION
*  ___________________
*
*  - Check if the proposal has been altered from inception by reading 
*    the Contract Transaction History (PTRNPF) for the Alter from Inception 
*    (AFI) transaction code.
*    
*  - Call the New Contract Proposal Submenu (P5002)
*  
*  - Compare the latest Money In date against the Underwriting Approved Date 
*    and use the latest of the two dates as the new risk commencement date for 
*    the proposal.
*    
*  - Update Contract Header by calling P5004
*  
*  - Update 'Life Assured Age' by calling P5005
*  
*  - Perform 'Non-Waiver' updates
*  
*  - Perform 'Waiver' updates
*  
*  - Perform Underwriting updates
*  
*  - Perform Beneficiary updates
*  
*  - Call Pre-Issue Validation program (P6378) post risk commencement date update
*  
*  - Submit Contract Issue
*
* 4000-CLOSE SECTION
* __________________
*
*  - Close Files.
*
*  - Delete the Override function for the ZCHX file
*
*   
*   Error Processing
*	----------------
*  -  Perform the 600-FATAL-ERROR section. The
*     SYSR-SYSERR-TYPE flag does not need to be set in this
*     program, because MAINB takes care of a system errors.
*
*          (BATD processing is handled in MAINB)
*          
*  - Standard Screen Error Handling after returning from
*  	 online program call. It is handled by following function
*  
*  			handleError3300(SmartVarModel screenVars)
*  
*  
*	Online Program Call
*	-------------------
*
*  - This batch process calls lots of online programs for certain
*  	 validations. Online program can't be called directly, as 
*    in online program there are lots of functionality implemented
*    at parent level. When online program is called from batch process
*    it is not required call everything here, so most of parent level 
*    functionality is bypassed when making online program call.
*    
*  - In each online program, called here, a method is implemented with
*    signature as shown below:
*    
*    	public void processBo(Object... parmArray)
*    
*  - Each online program requires special handling when it is called
*    from batch process. Hence, each online program call is handled
*    manually. This handling is done in processBo() method itself.
*    
*  - Each online program needs specific parameters to be initialized 
*    before call. This initialization is also program specific.
*    
*  - Different sections of online programs, i.e. 1000, 2000 etc., are
*    called manually by passing section numbers.
*    
*****************************************************************
* </pre>
 * 
 * 
 * @author vhukumagrawa
 * 
 */
public class B5222 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5222");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaZchxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaZchxFn, 0, FILLER).init("ZCHX");
	private FixedLengthStringData wsaaZchxRunid = new FixedLengthStringData(2).isAPartOf(wsaaZchxFn, 4);
	private ZonedDecimalData wsaaZchxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaZchxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

	private FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(85);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysSystemErrorParams, 0);
	private FixedLengthStringData wsysSysparams = new FixedLengthStringData(77).isAPartOf(wsysSystemErrorParams, 8);
	
	private ZonedDecimalData wsaaUwDCDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaRtrnsacEffdate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaNewCommDate = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTranscd = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 4);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private String wsaaWaiveIt = "";
	private String wsaaAlreadyStored = "";
	private FixedLengthStringData wsaaCntTypeCoverage = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaCntType = new FixedLengthStringData(3).isAPartOf(wsaaCntTypeCoverage, 0);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(4).isAPartOf(wsaaCntTypeCoverage, 3);
	private FixedLengthStringData wsaaDesc = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaLang = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaStat = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaEror = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaErrorFlag = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaScreen = new FixedLengthStringData(5);
	
	private R5222Report printerFile = new R5222Report();
	private FixedLengthStringData r5222H01 = new FixedLengthStringData(31);
	private FixedLengthStringData r5222h01O = new FixedLengthStringData(31).isAPartOf(r5222H01, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5222h01O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5222h01O, 1);
	
	private FixedLengthStringData r5222D01 = new FixedLengthStringData(77);
	private FixedLengthStringData r5222d01O = new FixedLengthStringData(77).isAPartOf(r5222D01, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5222d01O, 0);
	private FixedLengthStringData errorcode = new FixedLengthStringData(4).isAPartOf(r5222d01O, 8);
	private FixedLengthStringData errormsg = new FixedLengthStringData(30).isAPartOf(r5222d01O, 12);
	private FixedLengthStringData screen = new FixedLengthStringData(5).isAPartOf(r5222d01O, 42);
	private FixedLengthStringData addndtl = new FixedLengthStringData(30).isAPartOf(r5222d01O, 47);
	
	private FixedLengthStringData r5222E01 = new FixedLengthStringData(1);
	private FixedLengthStringData r5222e01O = new FixedLengthStringData(1).isAPartOf(r5222E01, 0);
	private FixedLengthStringData dummy = new FixedLengthStringData(1).isAPartOf(r5222e01O, 0);
	
	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	
	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");
	
	/* Commonly Called sections */
	private FixedLengthStringData wsaaCalledSections = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaCalledSection = FLSArrayPartOfStructure(6, 5, wsaaCalledSections, 0);
	private FixedLengthStringData filler2 = new FixedLengthStringData(30).isAPartOf(wsaaCalledSections, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaP5123sec = new FixedLengthStringData(5).isAPartOf(filler2, 0).init("P5123");
	private FixedLengthStringData wsaaP5125sec = new FixedLengthStringData(5).isAPartOf(filler2, 5).init("P5125");
	private FixedLengthStringData wsaaP6326sec = new FixedLengthStringData(5).isAPartOf(filler2, 10).init("P6326");
	private FixedLengthStringData wsaaPh504sec = new FixedLengthStringData(5).isAPartOf(filler2, 15).init("PH504");
	private FixedLengthStringData wsaaPr516sec = new FixedLengthStringData(5).isAPartOf(filler2, 20).init("PR516");
	private FixedLengthStringData wsaaPr676sec = new FixedLengthStringData(5).isAPartOf(filler2, 25).init("PR676");
	
	/* Waiver components */
	private FixedLengthStringData waiverComponents = new FixedLengthStringData(20);
	private FixedLengthStringData[] waiverComponent = FLSArrayPartOfStructure(5, 4, waiverComponents, 0);

	/* ERRORS */
	private String ivrm = "IVRM";
	private String r065 = "R065";

	/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private int ct06 = 6;
	private int ct07 = 7;
	private int ct08 = 8;
	private int ct09 = 9;
	private int ct10 = 10;

	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);

	private String t1693 = "T1693";
	private String t5645 = "T5645";
	private String tr517 = "TR517";
	private String t5671 = "T5671";
	private String t5448 = "T5448";

	private Scrnparams scrnparams = new Scrnparams();
	private Wssplife wssplife = new Wssplife();
	private Wsspcomn wsspcomn = new Wsspcomn();

	/* Logical File: SMART table reference data */
	private ItemTableDAM itemIO = new ItemTableDAM();
	/* Contract Additional Details */
	private HpadTableDAM hpadIO = new HpadTableDAM();
	/* Sub Account Balances - Contract Enquiry. */
	private AcblenqTableDAM acblenqIO = new AcblenqTableDAM();
	/* Contract header - life new business */
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	/* PTRN - Contract Transaction Hisotry */
	private PtrncwfTableDAM ptrncwfIO = new PtrncwfTableDAM();
	/*Receipt Details - Contract Enquiry.*/
	private RtrnsacTableDAM rtrnsacIO = new RtrnsacTableDAM();
	/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	/*Coverage/rider transactions - new busine*/
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	/*Reassurance Cession Details New Busines*/
	private RacdlnbTableDAM racdlnbIO = new RacdlnbTableDAM();
	/*Beneficiery records for poroposal */
	private BnfylnbTableDAM bnfylnbIO = new BnfylnbTableDAM();
	/* User Details Logical - Validflag '1's on */
	private UsrdTableDAM usrdIO = new UsrdTableDAM();
	/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	
	//TMLII-1285
	private BmsgTableDAM bmsgIO = new BmsgTableDAM();
	private ErorTableDAM erorIO = new ErorTableDAM();
	
	/* FORMATS */
	private String descrec = "DESCREC";
	private String chdrlnbrec = "CHDRLNBREC";
	private String hpadrec = "HPADREC";
	private String ptrncwfrec = "PTRNCWFREC";
	private String rtrnsacrec = "RTRNREC";
	private String lifelnbrec = "LIFELNBREC";
	private String covtlnbrec = "COVTLNBREC";
	//TMLII-1285
	private String bmsgrec = "BMSGREC";
	private String erorrec = "ERORREC";

	private T5645rec t5645rec = new T5645rec();
	private Tr517rec tr517rec = new Tr517rec();
	private T5671rec t5671rec = new T5671rec();
	private T5448rec t5448rec = new T5448rec();
	Conlogrec conlogrec = new Conlogrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	
	private Batckey wsaaBatckey = new Batckey();
	
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private FixedLengthStringData wsspFiller1 = new FixedLengthStringData(767);
	
	private S6378ScreenVars s6378ScreenVars = ScreenProgram.getScreenVars(S6378ScreenVars.class);
	private S5002ScreenVars s5002ScreenVars = ScreenProgram.getScreenVars(S5002ScreenVars.class);
	private S5004ScreenVars s5004ScreenVars = ScreenProgram.getScreenVars( S5004ScreenVars.class);
	private S5005ScreenVars s5005ScreenVars = ScreenProgram.getScreenVars( S5005ScreenVars.class);
	private S5003ScreenVars s5003ScreenVars = ScreenProgram.getScreenVars( S5003ScreenVars.class);
	private S5073ScreenVars s5073ScreenVars = ScreenProgram.getScreenVars( S5073ScreenVars.class);
	private S5074ScreenVars s5074ScreenVars = ScreenProgram.getScreenVars( S5074ScreenVars.class);
	
	//TMLII-1285
	private List<String> valErrorCodes = new ArrayList<String>();
	private Boolean waiverNeeded = false; 
	boolean isFeaAllowed = false;
	
	private ZchxpfDAO zchxpfDAO = getApplicationContext().getBean("zchxpfDAO", ZchxpfDAO.class); 
	private int batchID = 0;
	private int batchExtractSize;
    private List<Zchxpf> list = null; 
    private Iterator<Zchxpf> iter;
    private Zchxpf zchxpf = null;	
	private boolean onePflag = false;	
	private static final String NBPRP124="NBPRP124"; 
	private static final String ISSUED_CONTRACTS = "IssuedContracts";
	private static final String UNISSUED_CONTRACTS = "UnissuedContracts";
	private static final String folder = "OUTGOING";
	private String fileName = "";
	private String Schdname = "";	
	private Clntpf clntpf;	
	private String wsaaClntNum = "";
	private String wsaaClntName = "";
	private List<String> tapeRecordData = new ArrayList<String>();
	private List<String> errorRecordData = new ArrayList<String>();	
	private Map<String, String> errorDataMap = new HashMap<>();
	private List<Clntpf> clntpfList = null;	
	private Map<String, ErrorObject> errObjectMap = new HashMap<>();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		releaseSoftlock2560,
		exit2590, 
		nextPtrncwf3022,
		exit3029,
		nextRtrnsac3032,
		calcDate3033,
		exit3049,
		nextCovtlnb3090,
		readT56713110,
		performFacReassureUpdates3130,
		nextRacdlnb3133,
		calcNextReviewDate3134,
		nextCovtlnb3170,
		readT56713180,
		exit3279,
		performFacReassureUpdates3200,
		nextByfylnb3232,
		exit3239,
		postRskDtUpdatePreIssueVal3240,
		contractIssue3260,
		exit3290,
		gotoNextCovtlnb3210
	}

	public B5222() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}

	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}

	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}

	public void mainline(Object... parmArray) {
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
		}
	}

	protected void restart0900() {
		/* RESTART */
		/* EXIT */
	}

	protected void initialise1000() {
		initialise1010();
		if(onePflag){
			initTapeFileHeader();
			initErrorFileHeader();
		}
	}

	protected void initialise1010() {
		if (isNE(bprdIO.getRestartMethod(), "3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				batchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				batchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			batchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
		
		isFeaAllowed = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "MTL31201", appVars,"IT");
		varcom.vrcmDate.set(getCobolDate());
		printerFile.openOutput();
		wsaaZchxRunid.set(bprdIO.getSystemParam04());
		wsaaZchxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		onePflag = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), NBPRP124, appVars, "IT");
		if(onePflag){
			Schdname = bsprIO.jobName.toString().trim();
			if(Schdname.length() < 15){
				Schdname = String.format("%-15s", Schdname).replace(' ', '0');
			}
			fileName = Schdname+bsscIO.getScheduleNumber().toInt();	
		}
		readChunk();
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());

		/* If first page spills over - write standard headings*/
		setUpHeadingCompany1011();
		if (newPageReq.isTrue()) {
			// Print error report heading
			printerFile.printR5222h01(r5222H01, indicArea);
			wsaaOverflow.set("N");
		}

		// initialze wsspcomn, wsslife and scrnparam
		initWsspcomn1012();
		initScrnParams1013();
		//initWssplife1013(); - not required at present, kept for future scope
		/* Clear screen name*/
		wsaaScreen.set(SPACES);

	}
	
	protected void readChunk() {
		list = zchxpfDAO.findResults(wsaaZchxFn.toString(), wsaaThreadMember.toString(), batchExtractSize, batchID);
		if (null == list || list.isEmpty()) {
			wsspEdterror.set(Varcom.endp);
			return;
		}
		this.iter = list.iterator();
	}
	
	protected void setUpHeadingCompany1011()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		company.set(bsprIO.getCompany());
		companynm.set(descIO.getLongdesc());
	}

	protected void initWsspcomn1012() {
		wsspcomn.commonArea.set(SPACES);
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		wsspcomn.branch.set(bsscIO.getInitBranch());
		wsspcomn.company.set(bsprIO.getCompany());
		wsspcomn.fsuco.set(bsprIO.getFsuco());
		wsspcomn.language.set(bsscIO.getLanguage());
		wsspcomn.acctyear.set(bsscIO.getAcctYear());
		wsspcomn.acctmonth.set(bsscIO.getAcctMonth());
		usrdIO.setParams(SPACE);
		usrdIO.setFunction(varcom.readr);
		usrdIO.setUserid(bsscIO.getUserName());
		SmartFileCode.execute(appVars, usrdIO);
		if (!isEQ(usrdIO.getStatuz(), varcom.oK) && !isEQ(usrdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(usrdIO.getParams());
			syserrrec.syserrType.set("1");
			// diff policy with life tom chi
			// syserr(syserrrec.syserrRec);
			callProgram(Syserr.class, syserrrec.syserrRec);
			// end
		}
		if (!isEQ(usrdIO.getStatuz(), varcom.oK)) {
			appVars.addMessage("User Not Authorised to Driver System");
		}
		usrdIO.setFunction(varcom.closf);
		SmartFileCode.execute(appVars, usrdIO);
		if (!isEQ(usrdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(usrdIO.getParams());
			syserrrec.syserrType.set("1");
			// diff policy with life tom chi
			// syserr(syserrrec.syserrRec);
			callProgram(Syserr.class, syserrrec.syserrRec);
			// end
		}
		varcom.vrcmTerminal.set(appVars.getTerminalId());
		varcom.vrcmTermid.set(varcom.vrcmTerm);
		varcom.vrcmTranidN.set(0);
		varcom.vrcmUser.set(usrdIO.getUsernum());
		varcom.vrcmDate.set(getCobolDate());
		varcom.vrcmTime.set(getCobolTime());
		wsspcomn.userid.set(usrdIO.getUserid());
		wsspcomn.term.set(varcom.vrcmTerm);
		wsspcomn.tranid.set(varcom.vrcmTranid);
	}

	protected void initScrnParams1013() {
		scrnparams.screenParams.set(SPACES);
		scrnparams.company.set(bsprIO.getCompany());
		scrnparams.language.set(bsscIO.getLanguage());
		scrnparams.deviceInd.set("*RMT");
	}

	protected void initWssplife1013() {
		/* START */
		/* EXIT*/
	}

	protected void readFile2000() {
		try {
			readFile2010();
		} catch (GOTOException e) {
		}
	}

	protected void readFile2010() {
		
		if (wsspEdterror.equals(Varcom.endp)) {
			return;
		}
		
		if (!iter.hasNext()) {
			batchID++;
			readChunk();
			if (wsspEdterror.equals(Varcom.endp)) {
				return;
			}
		}
		zchxpf = iter.next();
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		wsysChdrnum.set(zchxpf.getChdrnum());
	}

	protected void edit2500() {
		wsspEdterror.set(varcom.oK);
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					checkUnderwrApproval2510();
					callPreIssueValidation2530();
					checkPremSusp2520();					
					softlock2550();
				}
				case releaseSoftlock2560:{
					releaseSoftlock2560();
				}
				case exit2590: {
				}
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void checkUnderwrApproval2510() {
		hpadIO.setDataArea(SPACES);
		hpadIO.setChdrcoy(zchxpf.getChdrcoy());
		hpadIO.setChdrnum(zchxpf.getChdrnum());
		hpadIO.setFormat(hpadrec);
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)
				&& isNE(hpadIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hpadIO.getParams());
			syserrrec.statuz.set(hpadIO.getStatuz());
			fatalError600();
		}
		if (isEQ(hpadIO.getStatuz(), varcom.mrnf)
				|| (hpadIO.getHuwdcdte() == null
				|| isEQ(hpadIO.getHuwdcdte(), "SPACES") 
				|| isEQ(hpadIO.getHuwdcdte(), "99999999"))) {
			contotrec.totno.set(ct03);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit2590);
		}
		wsaaUwDCDate.set(hpadIO.getHuwdcdte());
	}

	protected void checkPremSusp2520() {
		/*acblenqIO.setDataArea(SPACES);
		acblenqIO.setRldgacct(ZERO);
		acblenqIO.setRldgcoy(zchxpfRec.chdrcoy);
		acblenqIO.setRldgacct(zchxpfRec.chdrnum);
		acblenqIO.setSacscode(t5645rec.sacscode01);
		acblenqIO.setSacstyp(t5645rec.sacstype01);
		//acblenqIO.setOrigcurr(SPACES);
		acblenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, acblenqIO);
		if ((isNE(acblenqIO.getStatuz(), varcom.oK))
				&& (isNE(acblenqIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(acblenqIO.getParams());
			syserrrec.statuz.set(acblenqIO.getStatuz());
			fatalError600();
		}*/
		if(isEQ(chdrlnbIO.getPstatcode(),"PR") 
				|| isEQ(chdrlnbIO.getPstatcode(),"NR")) {
			contotrec.totno.set(ct04);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit2590);
		}
	}

	protected void callPreIssueValidation2530() {
		wsaaBatckey.set(batcdorrec.batchkey);
		wsaaBatckey.batcBatctrcde.set(bprdIO.getSystemParam01());
		wsspcomn.batchkey.set(wsaaBatckey.batcKey);
		readChdrlnb2540();
		/* KEEPS */
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/* Call to P6378 program (Pre-issue validation) */
		wsaaScreen.set("S6378");
		wsaaErrorFlag.set("N");
		callP63782535();

		/* RETRV */
		chdrlnbIO.setFunction(varcom.retrv);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/* RLSE */
		chdrlnbIO.setFunction(varcom.rlse);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		if(onePflag){
			for(String errCd : valErrorCodes){
				generateErrorObjMap(errCd.trim());
			}								
		}
		if (isNE(chdrlnbIO.getAvlisu(), "Y")) {
			//TMLII-1285 start
			if(valErrorCodes.contains("RL06")
				&& valErrorCodes.size() ==1){
				readEror();
				StringBuilder str1 = new StringBuilder();
				str1.append(zchxpf.getChdrnum());
				str1.append(erorIO.getErordesc().toString());
				bmsgIO.setParams(SPACES);
				bmsgIO.setCompany(bsprIO.getCompany());
				bmsgIO.setScheduleName(bsscIO.getScheduleName());
				bmsgIO.setScheduleNumber(bsscIO.getScheduleNumber());
				bmsgIO.setProcessName(bsprIO.getProcessName());
				bmsgIO.setProcessOccNum(bsprIO.getProcessOccNum());
				bmsgIO.setFormat(bmsgrec);
				bmsgIO.setFunction(varcom.begn);
				while(isNE(bmsgIO.getStatuz(), varcom.endp)){
					SmartFileCode.execute(appVars, bmsgIO);
					if(isNE(bmsgIO.getStatuz(), varcom.oK)
						&& isNE(bmsgIO.getStatuz(), varcom.endp)){
						syserrrec.params.set(bmsgIO.getParams());
						syserrrec.statuz.set(bmsgIO.getStatuz());
						fatalError600();
					}
					if(isNE(bmsgIO.getCompany(), bsprIO.getCompany())
						|| isNE(bmsgIO.getScheduleName(), bsscIO.getScheduleName())
						|| isNE(bmsgIO.getScheduleNumber(), bsscIO.getScheduleNumber())
						|| isNE(bmsgIO.getProcessName(), bsprIO.getProcessName())
						|| isNE(bmsgIO.getProcessOccNum(), bsprIO.getProcessOccNum())){
						bmsgIO.setStatuz(varcom.endp);
						break;
					}
					if(bmsgIO.getDatakey().toString().trim().equals(str1.toString().trim())){
						bmsgIO.setFunction(varcom.deltd);
						SmartFileCode.execute(appVars, bmsgIO);
						if(isNE(bmsgIO.getStatuz(), varcom.oK)){
							syserrrec.params.set(bmsgIO.getParams());
							syserrrec.statuz.set(bmsgIO.getStatuz());
							fatalError600();
						}
					}
					bmsgIO.setFunction(varcom.nextr);
				}
				//clear array
				valErrorCodes.clear();
				waiverNeeded = true;
				//TMLII-1285 start end
			} else {
				contotrec.totno.set(ct05);
				contotrec.totval.set(1);
				callContot001();
				wsspEdterror.set(SPACES);
				//clear array
				valErrorCodes.clear();
				goTo(GotoLabel.exit2590);
			}
		}
		//TMLII-1285
		valErrorCodes.clear();
	}
	
	//TMLII-1285 START
	private void readEror(){
		erorIO.setParams(SPACES);
		erorIO.setErorpfx("ER");
		erorIO.setErorcoy(SPACES);
		erorIO.setErorlang(bsscIO.getLanguage());
		erorIO.setErorprog(SPACES);
		erorIO.setEroreror("RL06");
		erorIO.setErorfile(SPACES);
		erorIO.setFormat(erorrec);
		erorIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, erorIO);
		if(isNE(erorIO.getStatuz(), varcom.oK)
			&& isNE(erorIO.getStatuz(), varcom.mrnf)){
			syserrrec.params.set(erorIO.getParams());
			syserrrec.statuz.set(erorIO.getStatuz());
			fatalError600();
		}
	}
	//TMLII-1285 END
	
	protected void callP63782535(){
		wsspcomn.programPtr.set(0);
		wsspcomn.nextprog.set("NONE");
		scrnparams.logo.set(wsspcomn.logo);
		scrnparams.fsuCompany.set(wsspcomn.fsuco);
		wsspcomn.edterror.clear();
		if (varcom.vrcmMsg.equals("E")) {
			scrnparams.function.set(Varcom.scerr);
		}
		P6378 p6378 = new P6378();
		/* P6378 - 1000 */
		wsspcomn.sectionno.set("1000");
		//TMLII-1285
		p6378.processBo1(wsspcomn.commonArea, wsspFiller, scrnparams.screenParams, s6378ScreenVars.dataArea, s6378ScreenVars.subfileArea, valErrorCodes);
		handleError3300(s6378ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct06);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit2590);
		}
		/* P6378 - PRE */
		wsspcomn.sectionno.set("PRE");
		//TMLII-1285
		p6378.processBo1(wsspcomn.commonArea, wsspFiller, scrnparams.screenParams, s6378ScreenVars.dataArea, s6378ScreenVars.subfileArea, valErrorCodes);
		handleError3300(s6378ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct06);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit2590);
		}
		/* P6378 - 2000 */
		wsspcomn.sectionno.set("2000");
		//TMLII-1285
		p6378.processBo1(wsspcomn.commonArea, wsspFiller, scrnparams.screenParams, s6378ScreenVars.dataArea, s6378ScreenVars.subfileArea, valErrorCodes);
		handleError3300(s6378ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct06);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit2590);
		}
		/* P6378 - 3000 */
		wsspcomn.sectionno.set("3000");
		//TMLII-1285
		p6378.processBo1(wsspcomn.commonArea, wsspFiller, scrnparams.screenParams, s6378ScreenVars.dataArea, s6378ScreenVars.subfileArea, valErrorCodes);
		handleError3300(s6378ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct06);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit2590);
		}
		/* No need to call section P6378 - 4000 */
//		wsspcomn.sectionno.set("4000");
//		p6378.processBo(wsspcomn.commonArea, wsspFiller, scrnparams.screenParams, s6378ScreenVars.dataArea, s6378ScreenVars.subfileArea);
//		handleError3300(s6378ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct06);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit2590);
		}
	}

	protected void readChdrlnb2540() {
		chdrlnbIO.setDataArea(SPACES);
		chdrlnbIO.setChdrcoy(bsprIO.getCompany());
		chdrlnbIO.setChdrnum(zchxpf.getChdrnum());
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
				&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}

		if (isEQ(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit2590);
		}
		/* Read Contract Type (This will be required to read T5448 table while performing Facultative Reassurance Updates)*/
		wsaaCntType.set(chdrlnbIO.getCnttype());
	}

	protected void softlock2550() {
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(zchxpf.getChdrcoy());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(zchxpf.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
				&& isNE(sftlockrec.statuz, "LOCK")) {
			wsysSysparams.set(sftlockrec.sftlockRec);
			syserrrec.params.set(wsysSystemErrorParams);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit2590);
		} else {
			goTo(GotoLabel.releaseSoftlock2560);
		}
	}
	
	protected void releaseSoftlock2560(){
		sftlockrec.company.set(batcdorrec.company);
		sftlockrec.entity.set(zchxpf.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(999999);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	
	}

	protected void update3000() {
		update3010();
	}

	protected void update3010() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while(true){
			try {
				switch (nextMethod) {
					case DEFAULT: {
						checkAlterFromInception3020();
						calcNewCommDate3030();
						validateSubmenu3040();
						contractHeaderUpdate3050();
						lifeAssuredAgeUpdate3060();
						nonWaiverUpdates3070();
						waiverUpdates3150();
						underwrUpdates3220();
						beneficieryUpdates3230();
					}
					
					case postRskDtUpdatePreIssueVal3240:{
						postRskDtUpdatePreIssueVal3240();
					}
					case contractIssue3260: {
						contractIssue3260();
					}
					case exit3290:{
						
					}
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void checkAlterFromInception3020() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3021();
				}
				case nextPtrncwf3022: {
					nextPtrncwf3022();
				}
				case exit3029: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
				if(isEQ(nextMethod, GotoLabel.contractIssue3260)){
					goTo(GotoLabel.contractIssue3260);
				}
			}
		}
	}
	
	protected void start3021(){		
		ptrncwfIO.setDataArea(SPACES);
		ptrncwfIO.setChdrcoy(zchxpf.getChdrcoy());
		ptrncwfIO.setBatctrcde(bprdIO.getSystemParam02());
		ptrncwfIO.setChdrnum(zchxpf.getChdrnum());
		ptrncwfIO.setFunction(varcom.begn);
	}
	
	protected void nextPtrncwf3022(){
		ptrncwfIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrncwfIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "BATCTRCDE");
		ptrncwfIO.setFormat(ptrncwfrec);
		SmartFileCode.execute(appVars, ptrncwfIO);
		if (isNE(ptrncwfIO.getStatuz(), varcom.oK)
				&& isNE(ptrncwfIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(ptrncwfIO.getParams());
			syserrrec.statuz.set(ptrncwfIO.getStatuz());
			fatalError600();
		}
		if (isEQ(ptrncwfIO.getStatuz(), varcom.endp) 
				|| isNE(ptrncwfIO.getChdrcoy(), zchxpf.getChdrcoy())
				|| isNE(ptrncwfIO.getChdrnum(), zchxpf.getChdrnum())
				|| isNE(ptrncwfIO.getBatctrcde(), bprdIO.getSystemParam02())) {
			ptrncwfIO.setFunction(varcom.endp);
			goTo(GotoLabel.exit3029);
		} else {
			// Valid flag 3 for contract proposal
			if(isEQ(ptrncwfIO.validflag, "3")) {
				/* Update contract header for available for issue flag. */
				chdrlnbIO.setFunction(varcom.writd);
				chdrlnbIO.setFormat(chdrlnbrec);
				SmartFileCode.execute(appVars, chdrlnbIO);
				if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(chdrlnbIO.getParams());
					fatalError600();
				}
				/* go to Contract Issue*/
				goTo(GotoLabel.contractIssue3260);
			}
			ptrncwfIO.setFunction(varcom.nextr);
			goTo(GotoLabel.nextPtrncwf3022);
		}

	}
	
	protected void validateSubmenu3040(){
		wsspcomn.sbmaction.set("B");
		/* Call Program P5002 (Contract Proposal Submenu) */
		wsaaScreen.set("S5002");
		wsaaErrorFlag.set("N");
		callP50023045();
		/* RETRV */
		chdrlnbIO.setFunction(varcom.retrv);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		
	}

	protected void callP50023045(){
		wsspcomn.programPtr.set(0);
		wsspcomn.nextprog.set("NONE");
		scrnparams.logo.set(wsspcomn.logo);
		scrnparams.fsuCompany.set(wsspcomn.fsuco);
		wsspcomn.edterror.clear();
		if (varcom.vrcmMsg.equals("E")) {
			scrnparams.function.set(Varcom.scerr);
		}
		
		P5002 p5002 = new P5002();
		/* P5002 - 1000 */
		wsspcomn.sectionno.set("1000");
		p5002.processBoNew(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5002ScreenVars.dataArea);
		handleError3300(s5002ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5002 - PRE */
		wsspcomn.sectionno.set("PRE");
		p5002.processBoNew(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5002ScreenVars.dataArea);
		handleError3300(s5002ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		// it is required to set action before 2000 section
		scrnparams.action.set(s5002ScreenVars.action);
		s5002ScreenVars.chdrsel.set(zchxpf.getChdrnum());
		/* P5002 - 2000 */
		wsspcomn.sectionno.set("2000");
		p5002.processBoNew(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5002ScreenVars.dataArea);
		handleError3300(s5002ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5002 - 3000 */
		wsspcomn.sectionno.set("3000");
		p5002.processBoNew(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5002ScreenVars.dataArea);
		handleError3300(s5002ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5002 - 4000 */
		wsspcomn.sectionno.set("4000");
		p5002.processBoNew(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5002ScreenVars.dataArea);
		handleError3300(s5002ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
	}
	protected void calcNewCommDate3030(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3031();
				}
				case nextRtrnsac3032: {
					nextRtrnsac3032();
				}
				case calcDate3033: {
					calcDate3033();
				}
				case exit3049: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
				if(isEQ(nextMethod, GotoLabel.contractIssue3260)){
					goTo(GotoLabel.contractIssue3260);
				}
			}
		}
	
	}
	
	protected void start3031(){
		if(!isFeaAllowed) {
			goTo(GotoLabel.contractIssue3260); 
		}
		wsaaRtrnsacEffdate.set(varcom.mindate);
		rtrnsacIO.setDataArea(SPACES);
		rtrnsacIO.setRldgcoy(zchxpf.getChdrcoy());
		rtrnsacIO.setSacscode(t5645rec.sacscode01);
		rtrnsacIO.setSacstyp(t5645rec.sacstype01);
		rtrnsacIO.setRldgacct(zchxpf.getChdrnum());
		//rtrnsacIO.setOrigccy(SPACES);
		rtrnsacIO.setFormat(rtrnsacrec);
		rtrnsacIO.setFunction(varcom.begn);
	}
	
	protected void nextRtrnsac3032(){
		rtrnsacIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		rtrnsacIO.setFitKeysSearch("RLDGCOY","SACSCODE","SACSTYP","RLDGACCT");
		SmartFileCode.execute(appVars, rtrnsacIO);
		if (isNE(rtrnsacIO.getStatuz(),varcom.oK)
		&& isNE(rtrnsacIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(rtrnsacIO.getParams());
			syserrrec.statuz.set(rtrnsacIO.getStatuz());
			fatalError600();
		}
		if (isEQ(rtrnsacIO.getStatuz(),varcom.endp)
				|| isNE(rtrnsacIO.getRldgcoy(), zchxpf.getChdrcoy())
				|| isNE(rtrnsacIO.getRldgacct(), zchxpf.getChdrnum())
				|| isNE(rtrnsacIO.getSacscode(), t5645rec.sacscode01)
				|| isNE(rtrnsacIO.getSacstyp(), t5645rec.sacstype01)){
			rtrnsacIO.setStatuz(varcom.endp);
			goTo(GotoLabel.calcDate3033);
		}
		
		if(isGT(rtrnsacIO.getEffdate(),wsaaRtrnsacEffdate)){
			wsaaRtrnsacEffdate.set(rtrnsacIO.getEffdate());
		}
		rtrnsacIO.setFunction(varcom.nextr);
		goTo(GotoLabel.nextRtrnsac3032);
	}
	
	protected void calcDate3033() {
		if (isEQ(wsaaUwDCDate, wsaaRtrnsacEffdate)) {
			wsaaNewCommDate.set(wsaaUwDCDate);
		} else if (isGT(wsaaUwDCDate, wsaaRtrnsacEffdate)) {
			wsaaNewCommDate.set(wsaaUwDCDate);
		} else {
			wsaaNewCommDate.set(wsaaRtrnsacEffdate);
		}
		
		/* Compare New Commencement Date against existing commencement date */
		if(isEQ(wsaaNewCommDate, chdrlnbIO.getOccdate())){
			/* Update contract header for available for issue flag. */
			chdrlnbIO.setFunction(varcom.writd);
			chdrlnbIO.setFormat(chdrlnbrec);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrlnbIO.getParams());
				fatalError600();
			}
			/* go to Contract Issue */
			goTo(GotoLabel.contractIssue3260); 
		} else
		{
			waiverNeeded = false;	
		}
	}
	
	protected void contractHeaderUpdate3050(){
		chdrlnbIO.setBillcd(varcom.vrcmMaxDate);
		chdrlnbIO.setOccdate(wsaaNewCommDate);
		/* KEEPS */
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/* Call P5004 (Life New Business Contract Header Maintenance Program) */
		wsaaErrorFlag.set("N");
		wsaaScreen.set("S5004");
		callP50043055();
		/* RETRV */
		chdrlnbIO.setFunction(varcom.retrv);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}
	
	protected void callP50043055(){
		wsspcomn.programPtr.set(1);
		wsspcomn.nextprog.set("NONE");
		/* Set secActns to blank value to avoid NPE in initialise1010 section of P5004 */
		wsspcomn.secActns.set(SPACES);
		scrnparams.logo.set(wsspcomn.logo);
		scrnparams.fsuCompany.set(wsspcomn.fsuco);
		wsspcomn.edterror.clear();
		if (varcom.vrcmMsg.equals("E")) {
			scrnparams.function.set(Varcom.scerr);
		}
		P5004 p5004 = new P5004();
		/* P5004 - 1000 */
		wsspcomn.sectionno.set("1000");
		p5004.processBo(wsspcomn.commonArea, wsspFiller, scrnparams.screenParams, s5004ScreenVars.dataArea);
		handleError3300(s5004ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5004 - PRE */
		wsspcomn.sectionno.set("PRE");
		p5004.processBo(wsspcomn.commonArea, wsspFiller, scrnparams.screenParams, s5004ScreenVars.dataArea);
		handleError3300(s5004ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5004 - 2000 */
		/* Need to set billcd to recalculate next billing date */
		s5004ScreenVars.billcd.set(varcom.vrcmMaxDate);
		wsspcomn.sectionno.set("2000");
		p5004.processBo(wsspcomn.commonArea, wsspFiller, scrnparams.screenParams, s5004ScreenVars.dataArea);
		handleError3300(s5004ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5004 - 3000 */
		wsspcomn.sectionno.set("3000");
		p5004.processBo(wsspcomn.commonArea, wsspFiller, scrnparams.screenParams, s5004ScreenVars.dataArea);
		handleError3300(s5004ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5004 - 4000 */
		wsspcomn.sectionno.set("4000");
		p5004.processBo(wsspcomn.commonArea, wsspFiller, scrnparams.screenParams, s5004ScreenVars.dataArea);
		handleError3300(s5004ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
	}
	
	protected void lifeAssuredAgeUpdate3060(){
		/* KEEPS */
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setChdrcoy(zchxpf.getChdrcoy());
		lifelnbIO.setChdrnum(zchxpf.getChdrnum());
		lifelnbIO.setFunction(varcom.begn);
		while(isNE(lifelnbIO.getStatuz(), varcom.endp)) {
			SmartFileCode.execute(appVars, lifelnbIO);
			if (isNE(lifelnbIO.getStatuz(),varcom.oK)
				&& isNE(lifelnbIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(lifelnbIO.getParams());
					syserrrec.statuz.set(lifelnbIO.getStatuz());
					fatalError600();
			}
			
			if(isEQ(lifelnbIO.getStatuz(),varcom.endp)
					|| isNE(lifelnbIO.getChdrcoy(), zchxpf.getChdrcoy())
					|| isNE(lifelnbIO.getChdrnum(), zchxpf.getChdrnum())){
				lifelnbIO.setStatuz(varcom.endp);
				//goTo(GotoLabel.exit3290);
				break;
			}
			/* KEEPS */
			lifelnbIO.setFunction(varcom.keeps);
			lifelnbIO.setFormat(lifelnbrec);
			SmartFileCode.execute(appVars, lifelnbIO);
			if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(lifelnbIO.getParams());
				fatalError600();
			}
			/* Call P5005 (Life/Joint Life Assured Details program) */
			wsaaScreen.set("S5005");
			wsaaErrorFlag.set("N");
			callP50053065();
			lifelnbIO.setFunction(varcom.nextr);
		}
	}
	
	protected void callP50053065(){
		wsspcomn.programPtr.set(1);
		wsspcomn.nextprog.set("NONE");
		// set secActns to blank value to avoid NPE while in 1000 section
		wsspcomn.secActns.set(SPACES);
		scrnparams.logo.set(wsspcomn.logo);
		scrnparams.fsuCompany.set(wsspcomn.fsuco);
		wsspcomn.edterror.clear();
		if (varcom.vrcmMsg.equals("E")) {
			scrnparams.function.set(Varcom.scerr);
		}
		P5005 p5005 = new P5005();
		/* P5005 - 1000 */
		wsspcomn.sectionno.set("1000");
		p5005.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5005ScreenVars.dataArea);
		handleError3300(s5005ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5005 - PRE */
		wsspcomn.sectionno.set("PRE");
		p5005.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5005ScreenVars.dataArea);
		handleError3300(s5005ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5005 - 2000 */
		wsspcomn.sectionno.set("2000");
		p5005.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5005ScreenVars.dataArea);
		handleError3300(s5005ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5005 - 3000 */
		wsspcomn.sectionno.set("3000");
		p5005.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5005ScreenVars.dataArea);
		handleError3300(s5005ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5005 - 4000 */
		wsspcomn.sectionno.set("4000");
		p5005.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5005ScreenVars.dataArea);
		handleError3300(s5005ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
	}
	protected void nonWaiverUpdates3070(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3080();
				}
				case nextCovtlnb3090: {
					nextCovtlnb3090();
				}
				case readT56713110:{
					readT56713110();
					callProgram3120();
				}
				case performFacReassureUpdates3130:{
					performFacReassureUpdates3130();
					gotoNextCovtlnb3140();
				}
				case exit3279: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
				if(isEQ(nextMethod, GotoLabel.exit3290)){
					goTo(GotoLabel.exit3290);
				}
			}
		}
		
	}
	
	protected void start3080(){
		sub1.set(1);
		covtlnbIO.setDataArea(SPACES);
		covtlnbIO.setChdrcoy(zchxpf.getChdrcoy());
		covtlnbIO.setChdrnum(zchxpf.getChdrnum());
		covtlnbIO.setFunction(varcom.begn);
	}
	
	protected void nextCovtlnb3090(){
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)
				&& isNE(covtlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtlnbIO.getParams());
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			fatalError600();
		}
		if (isEQ(covtlnbIO.getStatuz(),varcom.endp)
			|| isNE(covtlnbIO.getChdrcoy(), zchxpf.getChdrcoy())
			|| isNE(covtlnbIO.getChdrnum(),zchxpf.getChdrnum())) {
				covtlnbIO.setStatuz(varcom.endp);
				goTo(GotoLabel.exit3279);
		} else {
			
			wsaaWaiveIt = "N";
			readTr5173100();
			if(isEQ(wsaaWaiveIt, "N")){
				wsaaCrtable.set(covtlnbIO.getCrtable());
				wsaaCoverage.set(covtlnbIO.getCrtable());
				goTo(GotoLabel.readT56713110);
			} else {
				covtlnbIO.setFunction(varcom.nextr);
				goTo(GotoLabel.nextCovtlnb3090);
			}
		} 
	}
	
	protected void readTr5173100(){
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tr517);
		itemIO.setItemitem(covtlnbIO.crtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
				&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		
		if(isEQ(itemIO.getStatuz(), varcom.mrnf)){
			wsaaWaiveIt = "N";
		} else {
			wsaaWaiveIt = "Y";
			wsaaAlreadyStored = "N";
			
			/* Storing waiver components as key so that it is not required to read Tr517 again */
			/* while performing waiver updates. */
			//check if already stored
			for(sub2.set(1); !(isGT(sub2, sub1)); sub2.add(1)){
				if(isEQ(covtlnbIO.crtable, waiverComponent[sub2.toInt()])){
					wsaaAlreadyStored = "Y";
				}
			}
			//if component is not stored before, store it
			if(isEQ(wsaaAlreadyStored,"N")){
				waiverComponent[sub1.toInt()].set(covtlnbIO.crtable);
				sub1.add(1);
			}
		}
	}
	
	protected void readT56713110(){
		wsaaTranscd.set(bprdIO.getSystemParam01());
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5671);
		itemIO.setItemitem(wsaaTranCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
	}
	
	protected void callProgram3120(){
		
		callRelProgram3121();
		/*RLSE */
		covtlnbIO.setFunction(varcom.rlse);
		covtlnbIO.setFormat(covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
	}
	
	protected void callRelProgram3121(){
		wsaaErrorFlag.set("N");
		/* Call relevant Component Proposal Screen for the coverage type */
		if (isNE(t5671rec.pgm01, wsaaP6326sec)) {
			/* KEEPS */
			covtlnbIO.setFunction(varcom.keeps);
			covtlnbIO.setFormat(covtlnbrec);
			SmartFileCode.execute(appVars, covtlnbIO);
			if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covtlnbIO.getParams());
				fatalError600();
			}
			
			if (isEQ(t5671rec.pgm01, wsaaP5123sec)) {
				callP51233122();
			} else if (isEQ(t5671rec.pgm01, wsaaP5125sec)) {
				callP51253123();
			} else if (isEQ(t5671rec.pgm01, wsaaPh504sec)) {
				callPh5043124();
			} else if (isEQ(t5671rec.pgm01, wsaaPr516sec)) {
				callPr5163125();
			} else if (isEQ(t5671rec.pgm01, wsaaPr676sec)) {
				callPr6763126();
			}
		} else {
			
			covtlnbIO.setReserveUnitsDate(wsaaNewCommDate);
			covtlnbIO.setReserveUnitsInd("Y");
			
			/* KEEPS */
			covtlnbIO.setFunction(varcom.keeps);
			covtlnbIO.setFormat(covtlnbrec);
			SmartFileCode.execute(appVars, covtlnbIO);
			if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covtlnbIO.getParams());
				fatalError600();
			}
			callP63263127();
		}
	}
	
	protected void callP51233122(){
		wsaaScreen.set("S5213");
		wsspcomn.programPtr.set(1);
		wsspcomn.nextprog.set("NONE");
		// set secActns to blank value to avoid NPE while in 1000 section
		wsspcomn.secActns.set(SPACES);
		// setting of secProg is required as it is used to read T5608 table
		wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(wsaaP5123sec);
		scrnparams.logo.set(wsspcomn.logo);
		scrnparams.fsuCompany.set(wsspcomn.fsuco);
		wsspcomn.edterror.clear();
		if (varcom.vrcmMsg.equals("E")) {
			scrnparams.function.set(Varcom.scerr);
		}
		P5123 p5123 = new P5123();
		S5123ScreenVars s5123ScreenVars = ScreenProgram.getScreenVars(S5123ScreenVars.class);
		/* P5123 - 1000 */
		wsspcomn.sectionno.set("1000");
		p5123.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5123ScreenVars.dataArea);
		handleError3300(s5123ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5123 - PRE */
		wsspcomn.sectionno.set("PRE");
		p5123.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5123ScreenVars.dataArea);
		handleError3300(s5123ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5123 - 2000 */
		wsspcomn.sectionno.set("2000");
		p5123.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5123ScreenVars.dataArea);
		handleError3300(s5123ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5123 - 3000 */
		wsspcomn.sectionno.set("3000");
		p5123.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5123ScreenVars.dataArea);
		handleError3300(s5123ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5123 - 4000 */
		wsspcomn.sectionno.set("4000");
		p5123.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5123ScreenVars.dataArea);
		handleError3300(s5123ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
	}
	
	protected void callP51253123(){
		wsaaScreen.set("S5215");
		wsspcomn.programPtr.set(1);
		wsspcomn.nextprog.set("NONE");
		// set secActns to blank value to avoid NPE while in 1000 section
		wsspcomn.secActns.set(SPACES);
		// setting of secProg is required as it is used to read T5606 table
		wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(wsaaP5125sec);
		scrnparams.logo.set(wsspcomn.logo);
		scrnparams.fsuCompany.set(wsspcomn.fsuco);
		wsspcomn.edterror.clear();
		if (varcom.vrcmMsg.equals("E")) {
			scrnparams.function.set(Varcom.scerr);
		}
		P5125 p5125 = new P5125();
		S5125ScreenVars s5125ScreenVars = ScreenProgram.getScreenVars(S5125ScreenVars.class);
		/* P5125 - 1000 */
		wsspcomn.sectionno.set("1000");
		p5125.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5125ScreenVars.dataArea);
		handleError3300(s5125ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5125 - PRE */
		wsspcomn.sectionno.set("PRE");
		p5125.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5125ScreenVars.dataArea);
		handleError3300(s5125ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5125 - 2000 */
		wsspcomn.sectionno.set("2000");
		p5125.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5125ScreenVars.dataArea);
		handleError3300(s5125ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5125 - 3000 */
		wsspcomn.sectionno.set("3000");
		p5125.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5125ScreenVars.dataArea);
		handleError3300(s5125ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5125 - 4000 */
		wsspcomn.sectionno.set("4000");
		p5125.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5125ScreenVars.dataArea);
		handleError3300(s5125ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
	}
	
	protected void callPh5043124(){
		wsaaScreen.set("Sh504");
		wsspcomn.programPtr.set(1);
		wsspcomn.nextprog.set("NONE");
		// set secActns to blank value to avoid NPE while in 1000 section
		wsspcomn.secActns.set(SPACES);
		// setting of secProg is required as it is used to read TH505 table
		wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(wsaaPh504sec);
		scrnparams.logo.set(wsspcomn.logo);
		scrnparams.fsuCompany.set(wsspcomn.fsuco);
		wsspcomn.edterror.clear();
		if (varcom.vrcmMsg.equals("E")) {
			scrnparams.function.set(Varcom.scerr);
		}
		Ph504 ph504 = new Ph504();
		Sh504ScreenVars sh504ScreenVars = ScreenProgram.getScreenVars(Sh504ScreenVars.class);
		/* P5125 - 1000 */
		wsspcomn.sectionno.set("1000");
		ph504.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sh504ScreenVars.dataArea);
		handleError3300(sh504ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* Ph504 - PRE */
		wsspcomn.sectionno.set("PRE");
		ph504.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sh504ScreenVars.dataArea);
		handleError3300(sh504ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* Ph504 - 2000 */
		wsspcomn.sectionno.set("2000");
		ph504.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sh504ScreenVars.dataArea);
		handleError3300(sh504ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* Ph504 - 3000 */
		wsspcomn.sectionno.set("3000");
		ph504.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sh504ScreenVars.dataArea);
		handleError3300(sh504ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* Ph504 - 4000 */
		wsspcomn.sectionno.set("4000");
		ph504.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sh504ScreenVars.dataArea);
		handleError3300(sh504ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
	}

	protected void callPr5163125(){
		wsaaScreen.set("Sr516");
		wsspcomn.programPtr.set(1);
		wsspcomn.nextprog.set("NONE");
		// set secActns to blank value to avoid NPE while in 1000 section
		wsspcomn.secActns.set(SPACES);
		// setting of secProg is required as it is used to read T606 table
		wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(wsaaPr516sec);
		scrnparams.logo.set(wsspcomn.logo);
		scrnparams.fsuCompany.set(wsspcomn.fsuco);
		wsspcomn.edterror.clear();
		if (varcom.vrcmMsg.equals("E")) {
			scrnparams.function.set(Varcom.scerr);
		}
		Pr516 pr516 = new Pr516();
		Sr516ScreenVars sr516ScreenVars = ScreenProgram.getScreenVars(Sr516ScreenVars.class);
		/* Pr516 - 1000 */
		wsspcomn.sectionno.set("1000");
		pr516.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sr516ScreenVars.dataArea);
		handleError3300(sr516ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* Pr516 - PRE */
		wsspcomn.sectionno.set("PRE");
		pr516.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sr516ScreenVars.dataArea);
		handleError3300(sr516ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* Pr516 - 2000 */
		wsspcomn.sectionno.set("2000");
		pr516.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sr516ScreenVars.dataArea);
		handleError3300(sr516ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* Pr516 - 3000 */
		wsspcomn.sectionno.set("3000");
		pr516.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sr516ScreenVars.dataArea);
		handleError3300(sr516ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* Pr516 - 4000 */
		wsspcomn.sectionno.set("4000");
		pr516.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sr516ScreenVars.dataArea);
		handleError3300(sr516ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
	}

	protected void callPr6763126(){
		wsaaScreen.set("Sr676");
		wsspcomn.programPtr.set(1);
		wsspcomn.nextprog.set("NONE");
		// set secActns to blank value to avoid NPE while in 1000 section
		wsspcomn.secActns.set(SPACES);
		// setting of secProg is required as it is used to read T606 table
		wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(wsaaPr676sec);
		scrnparams.logo.set(wsspcomn.logo);
		scrnparams.fsuCompany.set(wsspcomn.fsuco);
		wsspcomn.edterror.clear();
		if (varcom.vrcmMsg.equals("E")) {
			scrnparams.function.set(Varcom.scerr);
		}
		Pr676 pr676 = new Pr676();
		Sr676ScreenVars sr676ScreenVars = ScreenProgram.getScreenVars(Sr676ScreenVars.class);
		/* Pr676 - 1000 */
		wsspcomn.sectionno.set("1000");
		pr676.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sr676ScreenVars.dataArea);
		handleError3300(sr676ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* Pr676 - PRE */
		wsspcomn.sectionno.set("PRE");
		pr676.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sr676ScreenVars.dataArea);
		handleError3300(sr676ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* Pr676 - 2000 */
		wsspcomn.sectionno.set("2000");
		pr676.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sr676ScreenVars.dataArea);
		handleError3300(sr676ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* Pr676 - 3000 */
		wsspcomn.sectionno.set("3000");
		pr676.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sr676ScreenVars.dataArea);
		handleError3300(sr676ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* Pr676 - 4000 */
		wsspcomn.sectionno.set("4000");
		pr676.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sr676ScreenVars.dataArea);
		handleError3300(sr676ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
	}
	
	protected void callP63263127(){
		wsaaScreen.set("S6326");
		wsspcomn.programPtr.set(1);
		wsspcomn.nextprog.set("NONE");
		// set secActns to blank value to avoid NPE while in 1000 section
		wsspcomn.secActns.set(SPACES);
		scrnparams.logo.set(wsspcomn.logo);
		scrnparams.fsuCompany.set(wsspcomn.fsuco);
		wsspcomn.edterror.clear();
		if (varcom.vrcmMsg.equals("E")) {
			scrnparams.function.set(Varcom.scerr);
		}
		P6326 p6326 = new P6326();
		S6326ScreenVars s6326ScreenVars = ScreenProgram.getScreenVars(S6326ScreenVars.class);
		/* P6326 - 1000 */
		wsspcomn.sectionno.set("1000");
		p6326.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s6326ScreenVars.dataArea);
		handleError3300(s6326ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P6326 - PRE */
		wsspcomn.sectionno.set("PRE");
		p6326.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s6326ScreenVars.dataArea);
		handleError3300(s6326ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P6326 - 2000 */
		wsspcomn.sectionno.set("2000");
		p6326.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s6326ScreenVars.dataArea);
		handleError3300(s6326ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P6326 - 3000 */
		wsspcomn.sectionno.set("3000");
		p6326.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s6326ScreenVars.dataArea);
		handleError3300(s6326ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P6326 - 4000 */
		wsspcomn.sectionno.set("4000");
		p6326.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s6326ScreenVars.dataArea);
		handleError3300(s6326ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
	}
	
	protected void performFacReassureUpdates3130(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readT54483131();
					startRacdlnb3132();
				
				}
				case nextRacdlnb3133: {
					nextRacdlnb3133();
				}
				case calcNextReviewDate3134:{
					calcNextReviewDate3134();
					updateRacdlnb3135();
					gotoNextRaclnb3136();
				}
				case exit3279: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	
	protected void readT54483131(){
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5448);
		itemIO.setItemitem(wsaaCntTypeCoverage);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			conlogrec.conlogRec.set(SPACES);
			conlogrec.error.set(r065);
			conlogrec.params.set(zchxpf.getChdrnum());
			callProgram(Conlog.class, conlogrec.conlogRec);
		}
		t5448rec.t5448Rec.set(itemIO.getGenarea());
	}
	
	protected void startRacdlnb3132(){
		racdlnbIO.setDataArea(SPACES);
		racdlnbIO.setChdrcoy(zchxpf.getChdrcoy());
		racdlnbIO.setChdrnum(zchxpf.getChdrnum());
		racdlnbIO.setRetype("F");
		racdlnbIO.setFunction(varcom.begnh);
	}
	
	protected void nextRacdlnb3133(){
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(), varcom.oK)
				&& isNE(racdlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(racdlnbIO.getParams());
			syserrrec.statuz.set(racdlnbIO.getStatuz());
			fatalError600();
		}
		if(isEQ(racdlnbIO.getStatuz(), varcom.endp)
			|| isNE(racdlnbIO.getChdrcoy(), zchxpf.getChdrcoy())
			|| isNE(racdlnbIO.getChdrnum(), zchxpf.getChdrnum())){
				racdlnbIO.setStatuz(varcom.endp);
				goTo(GotoLabel.exit3279);
				
		}
	}
	
	protected void calcNextReviewDate3134(){
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(wsaaNewCommDate);
		datcon3rec.intDate2.set(racdlnbIO.getCmdate());
		datcon3rec.frequency.set(t5448rec.rrevfrq);
		datcon3rec.freqFactor.set(0);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(wsaaNewCommDate);
		datcon2rec.intDate2.set(0);
		datcon2rec.frequency.set(t5448rec.rrevfrq);
		compute(datcon2rec.freqFactor, 5).set(add(1,datcon3rec.freqFactor));
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
	
	}
	
	protected void updateRacdlnb3135(){
		racdlnbIO.setCurrfrom(wsaaNewCommDate);
		racdlnbIO.setCtdate(wsaaNewCommDate);
		racdlnbIO.setCmdate(wsaaNewCommDate);
		if(isNE(t5448rec.rrevfrq, SPACES)){
			racdlnbIO.setRrevdt(datcon2rec.intDate2);
		} else {
			racdlnbIO.setRrevdt(varcom.maxdate);
		}
		racdlnbIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, racdlnbIO);
		if(isNE(racdlnbIO.getStatuz(), varcom.oK)){
			syserrrec.params.set(racdlnbIO.getParams());
			syserrrec.statuz.set(racdlnbIO.getStatuz());
			fatalError600();
		}
	}
	
	protected void gotoNextRaclnb3136(){
		racdlnbIO.setFunction(varcom.nextr);
		goTo(GotoLabel.nextRacdlnb3133);
	}
	
	protected void gotoNextCovtlnb3140(){
		covtlnbIO.setFunction(varcom.nextr);
		goTo(GotoLabel.nextCovtlnb3090);
	}
	
	protected void waiverUpdates3150(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3160();
				}
				case nextCovtlnb3170: {
					nextCovtlnb3170();
				}
				case readT56713180:{
					readT56713180();
					callProgram3190();
				}
				case performFacReassureUpdates3200:{
					performFacReassureUpdates3130();
					gotoNextCovtlnb3210();
				}
				case exit3279: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
				if(isEQ(nextMethod, GotoLabel.exit3290)){
					goTo(GotoLabel.exit3290);
				}
			}
		}
	}
	
	protected void start3160(){
		sub1.set(1);
		covtlnbIO.setDataArea(SPACES);
		covtlnbIO.setChdrcoy(zchxpf.getChdrcoy());
		covtlnbIO.setChdrnum(zchxpf.getChdrnum());
		covtlnbIO.setFunction(varcom.begn);
	}
	
	protected void nextCovtlnb3170(){
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)
				&& isNE(covtlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtlnbIO.getParams());
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			fatalError600();
		}
		if (isEQ(covtlnbIO.getStatuz(),varcom.endp)
			|| isNE(covtlnbIO.getChdrcoy(), zchxpf.getChdrcoy())
			|| isNE(covtlnbIO.getChdrnum(),zchxpf.getChdrnum())) {
				covtlnbIO.setStatuz(varcom.endp);
				goTo(GotoLabel.exit3279);
		} else {
			if(!waiverComponents.contains(covtlnbIO.getCrtable().toString())) {
				wsaaWaiveIt = "N";
				readTr5173100();
			} else  {
			for (sub1.set(1); !(isGT(sub1,5)); sub1.add(1)){
					if (isEQ(covtlnbIO.getCrtable(),waiverComponent[sub1.toInt()])) {
						wsaaWaiveIt = "Y";
						break;
						}
					}
			}
			if(isEQ(wsaaWaiveIt,"N")) {
					covtlnbIO.setFunction(varcom.nextr);
					goTo(GotoLabel.nextCovtlnb3170);
			} else{
				wsaaCrtable.set(covtlnbIO.getCrtable());
				wsaaCoverage.set(covtlnbIO.getCrtable());
				goTo(GotoLabel.readT56713180);
			}
			
		} 
	}
	
	protected void readT56713180(){
		wsaaTranscd.set(bprdIO.getSystemParam01());
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5671);
		itemIO.setItemitem(wsaaTranCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
	}
	
	public void callProgram3190(){
		/* Clearing the Sum Assured on the record so that it is automatically updated to */
		/* the total premium of its associated components.*/ 
		covtlnbIO.sumins.set(0);
		callRelProgram3121();
		/*RLSE */
		covtlnbIO.setFunction(varcom.rlse);
		covtlnbIO.setFormat(covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if(waiverNeeded){
			goTo(GotoLabel.gotoNextCovtlnb3210);
		}
	}
	
	protected void gotoNextCovtlnb3210(){
		covtlnbIO.setFunction(varcom.nextr);
		goTo(GotoLabel.nextCovtlnb3170);
	}
	
	protected void underwrUpdates3220(){
		/* Call P5003 (Work with Proposal Screen)*/
		wsaaScreen.set("S5003");
		wsaaErrorFlag.set("N");
		callP50033225();
	}
	
	protected void callP50033225(){
		wsspcomn.programPtr.set(1);
		wsspcomn.nextprog.set("NONE");
		// set secActns to blank value to avoid NPE while in 1000 section
		wsspcomn.secActns.set(SPACES);
		scrnparams.logo.set(wsspcomn.logo);
		scrnparams.fsuCompany.set(wsspcomn.fsuco);
		wsspcomn.edterror.clear();
		if (varcom.vrcmMsg.equals("E")) {
			scrnparams.function.set(Varcom.scerr);
		}
		P5003 p5003 = new P5003();
		/* P5003 - 1000 */
		wsspcomn.sectionno.set("1000");
		p5003.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5003ScreenVars.dataArea, s5003ScreenVars.subfileArea);
		handleError3300(s5003ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5003 - PRE */
		wsspcomn.sectionno.set("PRE");
		p5003.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5003ScreenVars.dataArea, s5003ScreenVars.subfileArea);
		handleError3300(s5003ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5003 - 2000 */
		wsspcomn.sectionno.set("2000");
		p5003.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5003ScreenVars.dataArea, s5003ScreenVars.subfileArea);
		handleError3300(s5003ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5003 - 3000 */
		wsspcomn.sectionno.set("3000");
		p5003.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5003ScreenVars.dataArea, s5003ScreenVars.subfileArea);
		handleError3300(s5003ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5003 - 4000 */
		wsspcomn.sectionno.set("4000");
		p5003.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5003ScreenVars.dataArea, s5003ScreenVars.subfileArea);
		handleError3300(s5003ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
	}
	protected void beneficieryUpdates3230(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					startBnfylnb3231();
				}
				case nextByfylnb3232: {
					nextByfylnb3232();
				}
				case exit3239: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	
	protected void startBnfylnb3231(){
		bnfylnbIO.setDataArea(SPACES);
		bnfylnbIO.setChdrcoy(zchxpf.getChdrcoy());
		bnfylnbIO.setChdrnum(zchxpf.getChdrnum());
		bnfylnbIO.setFunction(varcom.begnh);
		bnfylnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		bnfylnbIO.setFitKeysSearch("CHDRCOY" , "CHDRNUM");
	}
	
	protected void nextByfylnb3232(){
		SmartFileCode.execute(appVars, bnfylnbIO);
		if ((isNE(bnfylnbIO.getStatuz(),varcom.oK))
		&& (isNE(bnfylnbIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(bnfylnbIO.getParams());
			syserrrec.statuz.set(bnfylnbIO.getStatuz());
			fatalError600();
		}
		if ((isNE(bnfylnbIO.getChdrcoy(),chdrlnbIO.getChdrcoy()))
		|| (isNE(bnfylnbIO.getChdrnum(),chdrlnbIO.getChdrnum()))
		|| (isEQ(bnfylnbIO.getStatuz(),varcom.endp))) {
			bnfylnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3239);
		}
		else {
			bnfylnbIO.setEffdate(wsaaNewCommDate);
			bnfylnbIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, bnfylnbIO);
			if (isNE(bnfylnbIO.getStatuz(),varcom.oK)){
				syserrrec.params.set(bnfylnbIO.getParams());
				fatalError600();
			}
			bnfylnbIO.setFunction(varcom.nextr);
			goTo(GotoLabel.nextByfylnb3232);
		}
	}
	
	protected void postRskDtUpdatePreIssueVal3240(){
		wsaaBatckey.set(batcdorrec.batchkey);
		wsaaBatckey.batcBatctrcde.set(bprdIO.getAuthCode());
		wsspcomn.batchkey.set(wsaaBatckey.batcKey);
		s6378ScreenVars.dataArea.set(SPACES);
		/* Call P6378 (Pre-Issue Validation) post risk date update*/
		wsaaScreen.set("S6378");
		wsaaErrorFlag.set("N");
		callP63783245();
		/* RETRV */
		chdrlnbIO.setFunction(varcom.retrv);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		
		/* Update Database */
		chdrlnbIO.setFunction(varcom.writd);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		
		/* commit changes in database*/
		appVars.commit();
		
		/* release soft lock */
		releaseSoftLock3450();
		if (isNE(chdrlnbIO.getAvlisu(), "Y")) {
			contotrec.totno.set(ct08);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
	}
	
	protected void callP63783245(){
		wsspcomn.programPtr.set(0);
		wsspcomn.nextprog.set("NONE");
		scrnparams.logo.set(wsspcomn.logo);
		scrnparams.fsuCompany.set(wsspcomn.fsuco);
		wsspcomn.edterror.clear();
		if (varcom.vrcmMsg.equals("E")) {
			scrnparams.function.set(Varcom.scerr);
		}
		P6378 p6378 = new P6378();
		/* P6378 - 1000 */
		wsspcomn.sectionno.set("1000");
		p6378.processBo(wsspcomn.commonArea, wsspFiller, scrnparams.screenParams, s6378ScreenVars.dataArea, s6378ScreenVars.subfileArea);
		handleError3300(s6378ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P6378 - PRE */
		wsspcomn.sectionno.set("PRE");
		p6378.processBo(wsspcomn.commonArea, wsspFiller, scrnparams.screenParams, s6378ScreenVars.dataArea, s6378ScreenVars.subfileArea);
		handleError3300(s6378ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P6378 - 2000 */
		wsspcomn.sectionno.set("2000");
		p6378.processBo(wsspcomn.commonArea, wsspFiller, scrnparams.screenParams, s6378ScreenVars.dataArea, s6378ScreenVars.subfileArea);
		handleError3300(s6378ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P6378 - 3000 */
		wsspcomn.sectionno.set("3000");
		p6378.processBo(wsspcomn.commonArea, wsspFiller, scrnparams.screenParams, s6378ScreenVars.dataArea, s6378ScreenVars.subfileArea);
		handleError3300(s6378ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* Skipping P6378 - 4000 section - no need to call it*/
//		wsspcomn.sectionno.set("4000");
//		p6378.processBo(wsspcomn.commonArea, wsspFiller, scrnparams.screenParams, s6378ScreenVars.dataArea, s6378ScreenVars.subfileArea);
//		handleError3300(s6378ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
	}
	
	protected void releaseSoftLock3450(){

		sftlockrec.company.set(batcdorrec.company);
		sftlockrec.entity.set(zchxpf.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(999999);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	
	}
	
	protected void contractIssue3260(){
		callSubmenu3270();
		submitContractIssue3280();
	}
	
	protected void callSubmenu3270(){
		wsspcomn.sbmaction.set("A");
		/* Call P5073 (New Contract Issue Submenu)*/
		wsaaScreen.set("S5073");
		wsaaErrorFlag.set("N");
		callP50733275();
	}
	
	protected void callP50733275(){
		wsspcomn.programPtr.set(0);
		wsspcomn.nextprog.set("NONE");
		scrnparams.logo.set(wsspcomn.logo);
		scrnparams.fsuCompany.set(wsspcomn.fsuco);
		wsspcomn.edterror.clear();
		if (varcom.vrcmMsg.equals("E")) {
			scrnparams.function.set(Varcom.scerr);
		}
		P5073 p5073 = new P5073();
		/* P5073 - 1000 */
		wsspcomn.sectionno.set("1000");
		p5073.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5073ScreenVars.dataArea);
		handleError3300(s5073ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5073 - PRE */
		wsspcomn.sectionno.set("PRE");
		p5073.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5073ScreenVars.dataArea);
		handleError3300(s5073ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		// it is required to set action before 2000 section
		scrnparams.action.set(s5073ScreenVars.action);
		s5073ScreenVars.chdrsel.set(zchxpf.getChdrnum());
		/* P5073 - 2000 */
		wsspcomn.sectionno.set("2000");
		p5073.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5073ScreenVars.dataArea);
		handleError3300(s5073ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5073 - 3000 */
		wsspcomn.sectionno.set("3000");
		p5073.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5073ScreenVars.dataArea);
		handleError3300(s5073ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5073 - 4000 */
		wsspcomn.sectionno.set("4000");
		p5073.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5073ScreenVars.dataArea);
		handleError3300(s5073ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
	}
	
	protected void submitContractIssue3280(){
		/* Call P5074 (Cotract Issue) */
		wsaaScreen.set("S5074");
		wsaaErrorFlag.set("N");
		callP50743285();
		if(isEQ(wsaaErrorFlag, "N")){
			/* Commit database changes */
			appVars.commit();
			contotrec.totno.set(ct10);
			contotrec.totval.set(1);
			callContot001();
			if(onePflag){			
				generateIssuedData();			
			}
		}				
	}
	
	protected void callP50743285(){
		wsspcomn.programPtr.set(1);
		wsspcomn.nextprog.set("NONE");
		// set secActns to blank value to avoid NPE while in 1000 section
		wsspcomn.secActns.set(SPACES);
		scrnparams.logo.set(wsspcomn.logo);
		scrnparams.fsuCompany.set(wsspcomn.fsuco);
		wsspcomn.edterror.clear();
		if(onePflag){			
			wsspcomn.batchkey.set(batcdorrec.batchkey);
		}
		if (varcom.vrcmMsg.equals("E")) {
			scrnparams.function.set(Varcom.scerr);
		}
		P5074 p5074 = new P5074();
		/* P5074 - 1000 */
		wsspcomn.sectionno.set("1000");
		p5074.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5074ScreenVars.dataArea);
		handleError3300(s5074ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct09);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5074 - PRE */
		wsspcomn.sectionno.set("PRE");
		p5074.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5074ScreenVars.dataArea);
		handleError3300(s5074ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct09);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5074 - 2000 */
		wsspcomn.sectionno.set("2000");
		p5074.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5074ScreenVars.dataArea);
		handleError3300(s5074ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct09);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		if (waiverNeeded){
			waiverUpdates3150();
		}
		/* Section 3000 of P5074 submits AT request at the backend. To submit AT request AtSbmJ.java retrieves */ 
		/* USRLIBL attribute from JobInfo stored in current thread. When policy is issued by L2BCHISSUE job, */
		/* USRLIBL is not set in thread and caused exception, so it is required to store USRLIBL attribute */
		/* before calling section 3000 */
		COBOLAppVars batchAppVars = ((COBOLAppVars) AppVars.getInstance());
		/*ILIFE-5900 Starts*/
		batchAppVars.setLoggedOnUser(bsscIO.getUserName().toString());
		/*ILIFE-5900 Ends*/
		JobInfo jobInfo = batchAppVars.getJobInfo();
		jobInfo.changeJobInfo("USRLIBL","USRLIBL");
		/* P5074 - 3000 */
		wsspcomn.sectionno.set("3000");
		p5074.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5074ScreenVars.dataArea);
		handleError3300(s5074ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct09);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
		/* P5074 - 4000 */
		wsspcomn.sectionno.set("4000");
		p5074.processBo(wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, s5074ScreenVars.dataArea);
		handleError3300(s5074ScreenVars);
		if(isEQ(wsaaErrorFlag,"Y")){
			contotrec.totno.set(ct09);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
	}
	
	protected void commit3500() {
		/*COMMIT*/
		/*EXIT*/
	}

	protected void close4000() {
	if(onePflag){		
		CSVGenerator.produceCSV(tapeRecordData, fileName+"_"+ISSUED_CONTRACTS);
		if(!errObjectMap.isEmpty()){			
			for(ErrorObject errObj : errObjectMap.values()){
				generateUnissuedData(errObj);
			}
		}
		CSVGenerator.produceCSV(errorRecordData, fileName+"_"+UNISSUED_CONTRACTS);
	}
	 if(zchxpf != null) {
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(zchxpf.getChdrcoy());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(zchxpf.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
				&& isNE(sftlockrec.statuz, "LOCK")) {
			wsysSysparams.set(sftlockrec.sftlockRec);
			syserrrec.params.set(wsysSystemErrorParams);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		sftlockrec.function.set("TOAT");
		sftlockrec.company.set(zchxpf.getChdrcoy());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(zchxpf.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if ((isNE(sftlockrec.statuz,Varcom.oK))
		&& (isNE(sftlockrec.statuz,"LOCK"))) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	  }
		/*CLOSE-FILES*/
		dummy.set(SPACES);
		printerFile.printR5222e01(r5222E01);
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

	protected void rollback3600() {
		/*ROLLBACK*/
		/*EXIT*/
	}
	
	protected void handleError3300(SmartVarModel screenVars){
		initialize(r5222D01);
		if(isNE(wsspcomn.edterror, varcom.oK) && isEQ(wsspcomn.edterror, varcom.bomb)){
			wsaaErrorFlag.set("Y");
			chdrnum.set(zchxpf.getChdrnum());
			errorcode.set("BOMB");
			errormsg.set(SPACES);
			screen.set(wsaaScreen);
			addndtl.set(SPACES);
			printerFile.printR5222d01(r5222D01, indicArea);
			return;
		} else if(isNE(scrnparams.statuz, varcom.oK)){
			wsaaErrorFlag.set("Y");
			wsaaDesc.set(SPACES);
			wsaaLang.set(bsscIO.getLanguage());
			wsaaEror.set(scrnparams.statuz);
			callProgram(Geterror.class, wsaaEror, wsaaLang, wsaaDesc, wsaaStat);
			if (isNE(wsaaStat,varcom.oK)) {
				wsaaDesc.fill("?");
			}
			chdrnum.set(zchxpf.getChdrnum());
			errorcode.set(wsaaEror);
			errormsg.set(wsaaDesc);
			screen.set(wsaaScreen);
			addndtl.set(SPACES);
			printerFile.printR5222d01(r5222D01, indicArea);
			releaseSoftLock3400();
			return;
			
		} else if (isNE(scrnparams.errorCode, SPACES)){
			wsaaErrorFlag.set("Y");
			wsaaDesc.set(SPACES);
			wsaaLang.set(bsscIO.getLanguage());
			wsaaEror.set(scrnparams.errorCode);
			callProgram(Geterror.class, wsaaEror, wsaaLang, wsaaDesc, wsaaStat);
			if (isNE(wsaaStat,varcom.oK)) {
				wsaaDesc.fill("?");
			}
			chdrnum.set(zchxpf.getChdrnum());
			errorcode.set(wsaaEror);
			errormsg.set(wsaaDesc);
			screen.set(wsaaScreen);
			addndtl.set(SPACES);
			printerFile.printR5222d01(r5222D01, indicArea);
			releaseSoftLock3400();
			return;
		}
		
		writeScreenErrors3350(screenVars);
	}
	
	protected void writeScreenErrors3350(SmartVarModel screenVars){
		for (int i = 0; i < screenVars.screenErrFields.length; i++) {
			if (screenVars.screenErrFields[i].notEquals("")) {
				wsaaErrorFlag.set("Y");
				wsaaDesc.set(SPACES);
				wsaaLang.set(bsscIO.getLanguage());
				wsaaEror.set(screenVars.screenErrFields[i]);
				callProgram(Geterror.class, wsaaEror, wsaaLang, wsaaDesc, wsaaStat);
				if (isNE(wsaaStat,varcom.oK)) {
					wsaaDesc.fill("?");
				}
				chdrnum.set(zchxpf.getChdrnum());
				errorcode.set(wsaaEror);
				errormsg.set(wsaaDesc);
				screen.set(wsaaScreen);
				addndtl.set(screenVars.screenErrFields[i]);
				if(onePflag){
					generateErrorObjMap(wsaaEror.toString().trim());					
				}
				printerFile.printR5222d01(r5222D01, indicArea);
			}
		}
		if(isEQ(wsaaErrorFlag, "Y")){
			releaseSoftLock3400();
		}
		return;
	}
	
	protected void releaseSoftLock3400(){
		sftlockrec.company.set(batcdorrec.company);
		sftlockrec.entity.set(zchxpf.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(999999);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	
	}
	
	private void initTapeFileHeader() {
		StringBuilder header = new StringBuilder();
		header.append("Contract Number,");
		header.append("Client Number(Payor),");
		header.append("Client Name(Payor),");
		header.append("Batch Processing Date");
		tapeRecordData.add(header.toString());
	}
	
	private void initErrorFileHeader() {
		StringBuilder header = new StringBuilder();
		header.append("Contract Number,");
		header.append("Client Number(Payor),");
		header.append("Client Name(Payor),");
		header.append("Batch Processing Date,");
		header.append("Validation Error");		
		errorRecordData.add(header.toString());
	}
	
	private void generateIssuedData(){
		StringBuilder tapeData = new StringBuilder("");
		wsaaClntNum = chdrlnbIO.getCownnum().toString();					
		tapeData.append(zchxpf.getChdrnum());
		tapeData.append(",");
		tapeData.append(wsaaClntNum);
		tapeData.append(",");
		getPayeeName();
		tapeData.append(wsaaClntName);					 
		tapeData.append(",");
		tapeData.append(bsscIO.getEffectiveDate().toInt());
		tapeRecordData.add(tapeData.toString());
	}
	
    private void generateUnissuedData(ErrorObject errObject){
    	StringBuilder errorData = new StringBuilder("");
		wsaaClntNum = chdrlnbIO.getCownnum().toString();					
		errorData.append(errObject.getChdrNum());
		errorData.append(",");
		errorData.append(errObject.getClntNum());
		errorData.append(",");
		getPayeeName();
		errorData.append(errObject.getClntName());					 
		errorData.append(",");
		errorData.append(errObject.getDate());
		errorData.append(",");
		errorData.append(errObject.getErrDesc());			
		errorRecordData.add(errorData.toString());
	}
    
    private void generateErrorObjMap(String errCd){
    	ErrorObject errObject = null;
    	readErorJpn(errCd);
    	String errDesc = erorIO.erordesc.toString().trim();
		String chdrnum = zchxpf.getChdrnum().toString();  	    	
    	if(errObjectMap.isEmpty() || !errObjectMap.containsKey(chdrnum)){
    		errObject = new ErrorObject();
    		wsaaClntNum = chdrlnbIO.getCownnum().toString();
    		errObject.setChdrNum(zchxpf.getChdrnum());
    		errObject.setClntNum(wsaaClntNum);
    		getPayeeName();
    		errObject.setClntName(wsaaClntName);
    		errObject.setDate(bsscIO.getEffectiveDate().toString());    		
    		errObject.setErrDesc(errDesc);    		
    	}else{
    		errObject = errObjectMap.get(chdrnum);
    		if(errObject.getErrDesc() != null && !(errObject.getErrDesc().contains(errDesc))){
    			String preErrDesc = errObject.getErrDesc();
    			if(preErrDesc.charAt(0) == '"' && preErrDesc.charAt(preErrDesc.length()-1) == '"'){
    				StringBuilder sb = new StringBuilder(preErrDesc);    				
    				preErrDesc = sb.deleteCharAt(0).deleteCharAt(sb.length()-1).toString();
    			}
    			errObject.setErrDesc("\""+preErrDesc+", "+errDesc+"\"");
    			
    		}    		    		  		
    	}
    	errObjectMap.put(chdrnum, errObject);  
    }
    
    private void readErorJpn(String errCd){
		erorIO.setParams(SPACES);
		erorIO.setErorpfx("ER");
		erorIO.setErorcoy(SPACES);
		erorIO.setErorlang(bsscIO.getLanguage());
		erorIO.setErorprog(SPACES);
		erorIO.setEroreror(errCd);
		erorIO.setErorfile(SPACES);
		erorIO.setFormat(erorrec);
		erorIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, erorIO);
		if(isNE(erorIO.getStatuz(), varcom.oK)
			&& isNE(erorIO.getStatuz(), varcom.mrnf)){
			syserrrec.params.set(erorIO.getParams());
			syserrrec.statuz.set(erorIO.getStatuz());
			fatalError600();
		}
	}
	
	private void getPayeeName(){
		clntpf = new Clntpf();
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntnum(wsaaClntNum);
		clntpf.setClntpfx("CN");
		clntpfList = clntpfDAO.readClientpfData(clntpf);
		if(clntpfList==null || clntpfList.isEmpty())
		{
			return;
		}
		clntpf = clntpfList.get(0);
		plainname(clntpf);
		wsaaClntName = wsspcomn.longconfname.toString();
	}
	
	protected void plainname(Clntpf clntpf)
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (clntpf.getClttype().equals("C")) {
			corpname(clntpf);
			return ;
		}		
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();			
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}
	
	protected void corpname(Clntpf clntpf)
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}	
	
	class ErrorObject{
		String chdrNum;
		String clntNum;
		String clntName;
		String date;		
		String errDesc;
		
		public String getChdrNum() {
			return chdrNum;
		}

		public void setChdrNum(String chdrNum) {
			this.chdrNum = chdrNum;
		}

		public String getClntNum() {
			return clntNum;
		}

		public void setClntNum(String clntNum) {
			this.clntNum = clntNum;
		}

		public String getClntName() {
			return clntName;
		}

		public void setClntName(String clntName) {
			this.clntName = clntName;
		}

		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public String getErrDesc() {
			return errDesc;
		}

		public void setErrDesc(String errDesc) {
			this.errDesc = errDesc;
		}		
	}
}
