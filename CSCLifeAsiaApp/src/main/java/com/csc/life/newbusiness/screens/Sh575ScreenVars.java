package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH575
 * @version 1.0 generated on 30/08/09 07:04
 * @author Quipoz
 */
public class Sh575ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(207);
	public FixedLengthStringData dataFields = new FixedLengthStringData(95).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData scrtitles = new FixedLengthStringData(34).isAPartOf(dataFields, 39);
	public FixedLengthStringData[] scrtitle = FLSArrayPartOfStructure(2, 17, scrtitles, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(34).isAPartOf(scrtitles, 0, FILLER_REDEFINE);
	public FixedLengthStringData scrtitle01 = DD.scrtitle.copy().isAPartOf(filler,0);
	public FixedLengthStringData scrtitle02 = DD.scrtitle.copy().isAPartOf(filler,17);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,73);
	public FixedLengthStringData ttdesc = DD.ttdesc.copy().isAPartOf(dataFields,78);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(28).isAPartOf(dataArea, 95);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData scrtitlesErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] scrtitleErr = FLSArrayPartOfStructure(2, 4, scrtitlesErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(scrtitlesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData scrtitle01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData scrtitle02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ttdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 123);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData scrtitlesOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] scrtitleOut = FLSArrayPartOfStructure(2, 12, scrtitlesOut, 0);
	public FixedLengthStringData[][] scrtitleO = FLSDArrayPartOfArrayStructure(12, 1, scrtitleOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(scrtitlesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] scrtitle01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] scrtitle02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ttdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sh575screenWritten = new LongData(0);
	public LongData Sh575protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh575ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, scrtitle01, scrtitle02, ttdesc};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, scrtitle01Out, scrtitle02Out, ttdescOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, scrtitle01Err, scrtitle02Err, ttdescErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh575screen.class;
		protectRecord = Sh575protect.class;
	}

}
