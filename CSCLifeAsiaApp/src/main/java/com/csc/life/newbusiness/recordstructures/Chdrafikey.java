package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:13
 * Description:
 * Copybook name: CHDRAFIKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrafikey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrafiFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdrafiKey = new FixedLengthStringData(64).isAPartOf(chdrafiFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrafiChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrafiKey, 0);
  	public FixedLengthStringData chdrafiChdrnum = new FixedLengthStringData(8).isAPartOf(chdrafiKey, 1);
  	public PackedDecimalData chdrafiTranno = new PackedDecimalData(5, 0).isAPartOf(chdrafiKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(chdrafiKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrafiFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrafiFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}