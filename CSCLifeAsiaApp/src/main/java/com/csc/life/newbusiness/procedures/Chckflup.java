/*
 * File: Chckflup.java
 * Date: 29 August 2009 22:38:12
 * Author: Quipoz Limited
 *
 * Class transformed from CHCKFLUP.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.dataaccess.FlupenqTableDAM;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  KEEP CHDR.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Chckflup extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CHCKFLUP");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);

	private FixedLengthStringData wsaaSkip = new FixedLengthStringData(1);
	private Validator notSkipWrite = new Validator(wsaaSkip, "N");
	private Validator skipWrite = new Validator(wsaaSkip, "Y");

	private FixedLengthStringData wsaaOutstandFu = new FixedLengthStringData(1);
	private Validator noOutstandFu = new Validator(wsaaOutstandFu, "N");
	private Validator outstandFu = new Validator(wsaaOutstandFu, "Y");
		/* WSAA-MISC-FIELDS */
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).setUnsigned();
		/* ERRORS */
	private String e351 = "E351";
		/* TABLES */
	private String t5661 = "T5661";
		/* FORMATS */
	private String flupenqrec = "FLUPENQREC";

	private FixedLengthStringData wsaaChckRec = new FixedLengthStringData(37);
	private FixedLengthStringData wsaaChckChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaChckRec, 2);
	private FixedLengthStringData wsaaChckChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaChckRec, 3);
	private FixedLengthStringData wsaaChckStatuz = new FixedLengthStringData(4).isAPartOf(wsaaChckRec, 11);
	private ZonedDecimalData wsaaChckTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaChckRec, 31);
	private FixedLengthStringData wsaaChckLang = new FixedLengthStringData(1).isAPartOf(wsaaChckRec, 36);
		/*Follow Ups For Inquiry*/
	private FlupenqTableDAM flupenqIO = new FlupenqTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T5661rec t5661rec = new T5661rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1090
	}

	public Chckflup() {
		super();
	}

public void mainline(Object... parmArray)
	{
		wsaaChckRec = convertAndSetParam(wsaaChckRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		performs0010();
		exit0090();
	}

protected void performs0010()
	{
		syserrrec.subrname.set(wsaaProg);
		flupenqIO.setRecKeyData(SPACES);
		flupenqIO.setRecNonKeyData(SPACES);
		flupenqIO.setStatuz(varcom.oK);
		flupenqIO.setChdrcoy(wsaaChckChdrcoy);
		flupenqIO.setChdrnum(wsaaChckChdrnum);
		flupenqIO.setTranno(wsaaChckTranno);
		flupenqIO.setFuptype(SPACES);
		flupenqIO.setTransactionDate(varcom.vrcmMaxDate);
		flupenqIO.setFupno(ZERO);
		flupenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		flupenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		flupenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		flupenqIO.setFormat(flupenqrec);
		wsaaChckStatuz.set("NFND");
		while ( !(isEQ(flupenqIO.getStatuz(),varcom.endp))) {
			readFlupenq1000();
		}

	}

protected void exit0090()
	{
		exitProgram();
	}

protected void readFlupenq1000()
	{
		try {
			call1010();
		}
		catch (GOTOException e){
		}
	}

protected void call1010()
	{
		SmartFileCode.execute(appVars, flupenqIO);
		flupenqIO.setFunction(varcom.nextr);
		if (isEQ(flupenqIO.getStatuz(),varcom.endp)
		|| isNE(flupenqIO.getChdrcoy(),wsaaChckChdrcoy)
		|| isNE(flupenqIO.getChdrnum(),wsaaChckChdrnum)) {
			flupenqIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1090);
		}
		if (isEQ(flupenqIO.getFupcode(),SPACES)) {
			outstandFu.setTrue();
			goTo(GotoLabel.exit1090);
		}
		readT56611100();
		outstandFu.setTrue();
		for (wsaaX.set(1); !(isGT(wsaaX,10)
		|| noOutstandFu.isTrue()); wsaaX.add(1)){
			if (isEQ(flupenqIO.getFupstat(),t5661rec.fuposs[wsaaX.toInt()])) {
				noOutstandFu.setTrue();
			}
		}
		if (outstandFu.isTrue()) {
			flupenqIO.setStatuz(varcom.endp);
			wsaaChckStatuz.set("DFND");
		}
	}

protected void readT56611100()
	{
		call1110();
	}

protected void call1110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsaaChckChdrcoy);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5661);
		wsaaT5661Lang.set(wsaaChckLang);
		wsaaT5661Fupcode.set(flupenqIO.getFupcode());
		itemIO.setItemitem(wsaaT5661Key);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(e351);
			fatalError600();
		}
		t5661rec.t5661Rec.set(itemIO.getGenarea());
	}

protected void fatalError600()
	{
		/*PARA*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
	}
}
