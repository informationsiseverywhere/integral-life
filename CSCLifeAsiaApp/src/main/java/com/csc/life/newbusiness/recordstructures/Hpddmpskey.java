package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:44
 * Description:
 * Copybook name: HPDDMPSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hpddmpskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hpddmpsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hpddmpsKey = new FixedLengthStringData(64).isAPartOf(hpddmpsFileKey, 0, REDEFINE);
  	public FixedLengthStringData hpddmpsChdrcoy = new FixedLengthStringData(1).isAPartOf(hpddmpsKey, 0);
  	public PackedDecimalData hpddmpsHuwdcdte = new PackedDecimalData(8, 0).isAPartOf(hpddmpsKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(58).isAPartOf(hpddmpsKey, 6, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hpddmpsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hpddmpsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}