package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5003
 * @version 1.0 generated on 30/08/09 06:29
 * @author Quipoz
 */
public class S5003ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(297);
	public FixedLengthStringData dataFields = new FixedLengthStringData(137).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData cntBranch = DD.bshdinitbr.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData branchdesc = DD.branchdesc.copy().isAPartOf(dataFields,98);
	public ZonedDecimalData acctmonth = DD.acctmonth.copyToZonedDecimal().isAPartOf(dataFields,128);
	public ZonedDecimalData acctyear = DD.acctyear.copyToZonedDecimal().isAPartOf(dataFields,130);
	public FixedLengthStringData contractPlan = DD.cnttype.copy().isAPartOf(dataFields,134);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 137);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cntBranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData branchdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData acctmonthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData acctyearErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData contractPlanErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 177);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cntBranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] branchdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] acctmonthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] acctyearOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] contractPlanOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	
	//ILIFE-2001 starts by mranpise	  
	// The length of subfileArea is increased  by 2 thus, 
	// the offset (i.e. start position ) values for premCessTerm,riskCessTerm and instPrem subfields need to be increased by 2 each.
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(339);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(112).isAPartOf(subfileArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData elemdesc = DD.elemdesc.copy().isAPartOf(subfileFields,3);
	public FixedLengthStringData elemkey = DD.elemkey.copy().isAPartOf(subfileFields,50);
	public FixedLengthStringData hcoverage = DD.hcoverage.copy().isAPartOf(subfileFields,58);
	public FixedLengthStringData hcrtable = DD.hcrtable.copy().isAPartOf(subfileFields,60);
	public FixedLengthStringData hlife = DD.hlife.copy().isAPartOf(subfileFields,64);
	public FixedLengthStringData hrider = DD.hrider.copy().isAPartOf(subfileFields,66);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,68);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,70);
	
	
	
	//ILIFE-2001 continue by mranpise
	//public FixedLengthStringData sumin = DD.enqSumin.copy().isAPartOf(subfileFields,72);
	public ZonedDecimalData sumin = DD.sumins.copyToZonedDecimal().isAPartOf(subfileFields,72); 
		
	public FixedLengthStringData premCessTerm = DD.enqPTerm.copy().isAPartOf(subfileFields,89);
	public FixedLengthStringData riskCessTerm = DD.enqRTerm.copy().isAPartOf(subfileFields,92);
	
	
	//ILIFE-2001 continue by mranpise
	//public FixedLengthStringData instPrem = DD.enqInstPrem.copy().isAPartOf(subfileFields,93); 
	public ZonedDecimalData instPrem = DD.instprem.copyToZonedDecimal().isAPartOf(subfileFields,95);  
	//ILIFE-2001 ends//

		
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(56).isAPartOf(subfileArea, 112);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData elemdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData elemkeyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hcoverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hcrtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData hlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData hriderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData premCessTermErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData riskCessTermErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData instPremErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(168).isAPartOf(subfileArea, 168);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] elemdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] elemkeyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hcoverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hcrtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] hlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] hriderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120); 
	public FixedLengthStringData[] premCessTermOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] riskCessTermOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] instPremOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 336);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S5003screensflWritten = new LongData(0);
	public LongData S5003screenctlWritten = new LongData(0);
	public LongData S5003screenWritten = new LongData(0);
	public LongData S5003protectWritten = new LongData(0);
	public GeneralTable s5003screensfl = new GeneralTable(AppVars.getInstance());
	public FixedLengthStringData contDtCalcScreenflag = new FixedLengthStringData(1);  //ILJ-43
	public FixedLengthStringData cntEnqScreenflag = new FixedLengthStringData(1); //ILJ-388
	public FixedLengthStringData currencyScreenflag = new FixedLengthStringData(1); //ILJ-388
	
	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5003screensfl;
	}

	public S5003ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(actionOut,new String[] {"01","02","-01","02",null, null, null, null, null, null, null, null});
		fieldIndMap.put(cntBranchOut,new String[] {"15","16","-15","16",null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {hlife, hcoverage, hrider, hcrtable, action, life, coverage, rider, elemkey, elemdesc, sumin, premCessTerm, riskCessTerm, instPrem};
		screenSflOutFields = new BaseData[][] {hlifeOut, hcoverageOut, hriderOut, hcrtableOut, actionOut, lifeOut, coverageOut, riderOut, elemkeyOut, elemdescOut, suminOut, premCessTermOut, riskCessTermOut, instPremOut};
		screenSflErrFields = new BaseData[] {hlifeErr, hcoverageErr, hriderErr, hcrtableErr, actionErr, lifeErr, coverageErr, riderErr, elemkeyErr, elemdescErr,suminErr, premCessTermErr, riskCessTermErr, instPremErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cownnum, ownername, cntBranch, branchdesc, acctmonth, acctyear, contractPlan};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cownnumOut, ownernameOut, cntBranchOut, branchdescOut, acctmonthOut, acctyearOut, contractPlanOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cownnumErr, ownernameErr, cntBranchErr, branchdescErr, acctmonthErr, acctyearErr, contractPlanErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenActionVar = action;
		screenRecord = S5003screen.class;
		screenSflRecord = S5003screensfl.class;
		screenCtlRecord = S5003screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5003protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5003screenctl.lrec.pageSubfile);
	}
}
