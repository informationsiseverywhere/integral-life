package com.csc.life.newbusiness.dataaccess.dao;

import java.util.List;

import com.csc.life.newbusiness.dataaccess.model.Zchxpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ZchxpfDAO extends BaseDAO<Zchxpf> {
	public List<Zchxpf> findResults(String tableId, String memName, int batchExtractSize, int batchID);
}
