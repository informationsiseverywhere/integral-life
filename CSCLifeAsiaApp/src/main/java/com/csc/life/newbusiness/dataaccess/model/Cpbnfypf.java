/******************************************************************************
 * File Name 		: Cpbnfypf.java
 * Author			: qzhang52
 * Creation Date	: 05 December 2018
 * Project			: Integral Life
 * Description		: The Model Class for BNFYPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.newbusiness.dataaccess.model;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;

public class Cpbnfypf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	// Constant for database table name
	public static final String TABLE_NAME = "CPBNFYPF";

	//member variables for columns
	private long uniqueNumber;
	private Character chdrcoy;
	private String chdrnum;
	private String bnyclt;
	private Integer tranno;
	private Character validflag;
	private Integer currfr;
	private Integer currto;
	private String bnyrln;
	private Character bnycd;
	private BigDecimal bnypc;
	private String termid;
	private Integer userT;
	private Integer trtm;
	private Integer trdt;
	private Integer effdate;
	private String usrprf;
	private String jobnm;
	private Date datime;
	private String bnytype;
	private String cltreln;
	private Character relto;
	private Character selfind;
	private Character revcflg;
    private int enddate;
    private String sequence;//ICIL-11
    private String paymmeth;//ICIL-11
    private String bankkey;//ICIL-11
    private String bankacckey;//ICIL-11
    private BigDecimal amount;
	

	// Constructor
	public Cpbnfypf ( ) {};

	public Cpbnfypf(long uniqueNumber, Character chdrcoy, String chdrnum, String bnyclt, Integer tranno, Character validflag, Integer currfr, Integer currto, String bnyrln, Character bnycd, BigDecimal bnypc, String termid, Integer userT, Integer trtm, Integer trdt, Integer effdate, String usrprf, String jobnm, Date datime, String bnytype, String cltreln, Character relto, Character selfind, Character revcflg, int enddate, String sequence, String paymmeth, String bankkey, String bankacckey, BigDecimal amount) {
		this.uniqueNumber = uniqueNumber;
		this.chdrcoy = chdrcoy;
		this.chdrnum = chdrnum;
		this.bnyclt = bnyclt;
		this.tranno = tranno;
		this.validflag = validflag;
		this.currfr = currfr;
		this.currto = currto;
		this.bnyrln = bnyrln;
		this.bnycd = bnycd;
		this.bnypc = bnypc;
		this.termid = termid;
		this.userT = userT;
		this.trtm = trtm;
		this.trdt = trdt;
		this.effdate = effdate;
		this.usrprf = usrprf;
		this.jobnm = jobnm;
		this.datime = datime;
		this.bnytype = bnytype;
		this.cltreln = cltreln;
		this.relto = relto;
		this.selfind = selfind;
		this.revcflg = revcflg;
		this.enddate = enddate;
		this.sequence = sequence;
		this.paymmeth = paymmeth;
		this.bankkey = bankkey;
		this.bankacckey = bankacckey;
		this.amount = amount;
	}

	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public Character getChdrcoy(){
		return this.chdrcoy;
	}
	public String getChdrnum(){
		return this.chdrnum;
	}
	public String getBnyclt(){
		return this.bnyclt;
	}
	public Integer getTranno(){
		return this.tranno;
	}
	public Character getValidflag(){
		return this.validflag;
	}
	public Integer getCurrfr(){
		return this.currfr;
	}
	public Integer getCurrto(){
		return this.currto;
	}
	public String getBnyrln(){
		return this.bnyrln;
	}
	public Character getBnycd(){
		return this.bnycd;
	}
	public BigDecimal getBnypc(){
		return this.bnypc;
	}
	public BigDecimal getAmount() {
		return this.amount;
	}

	
	
	public String getTermid(){
		return this.termid;
	}
	public Integer getUserT(){
		return this.userT;
	}
	public Integer getTrtm(){
		return this.trtm;
	}
	public Integer getTrdt(){
		return this.trdt;
	}
	public Integer getEffdate(){
		return this.effdate;
	}
	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public Date getDatime(){
		return new Date(this.datime.getTime());//IJTI-316
	}
	public String getBnytype(){
		return this.bnytype;
	}
	public String getCltreln(){
		return this.cltreln;
	}
	public Character getRelto(){
		return this.relto;
	}
	public Character getSelfind(){
		return this.selfind;
	}
	public Character getRevcflg(){
		return this.revcflg;
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setChdrcoy( Character chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setChdrnum( String chdrnum ){
		 this.chdrnum = chdrnum;
	}
	public void setBnyclt( String bnyclt ){
		 this.bnyclt = bnyclt;
	}
	public void setTranno( Integer tranno ){
		 this.tranno = tranno;
	}
	public void setValidflag( Character validflag ){
		 this.validflag = validflag;
	}
	public void setCurrfr( Integer currfr ){
		 this.currfr = currfr;
	}
	public void setCurrto( Integer currto ){
		 this.currto = currto;
	}
	public void setBnyrln( String bnyrln ){
		 this.bnyrln = bnyrln;
	}
	public void setBnycd( Character bnycd ){
		 this.bnycd = bnycd;
	}
	public void setBnypc( BigDecimal bnypc ){
		 this.bnypc = bnypc;
	}
	
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public void setTermid( String termid ){
		 this.termid = termid;
	}
	public void setUserT( Integer userT ){
		 this.userT = userT;
	}
	public void setTrtm( Integer trtm ){
		 this.trtm = trtm;
	}
	public void setTrdt( Integer trdt ){
		 this.trdt = trdt;
	}
	public void setEffdate( Integer effdate ){
		 this.effdate = effdate;
	}
	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime( Date datime ){
		this.datime = new Date(datime.getTime());//IJTI-314
	}
	public void setBnytype( String bnytype ){
		 this.bnytype = bnytype;
	}
	public void setCltreln( String cltreln ){
		 this.cltreln = cltreln;
	}
	public void setRelto( Character relto ){
		 this.relto = relto;
	}
	public void setSelfind( Character selfind ){
		 this.selfind = selfind;
	}
	public void setRevcflg( Character revcflg ){
		 this.revcflg = revcflg;
	}
	public int getEnddate() {
		return enddate;
	}
	public void setEnddate(int enddate) {
		this.enddate = enddate;
	}	
    /*ICIL-11*/
	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getPaymmeth() {
		return paymmeth;
	}

	public void setPaymmeth(String paymmeth) {
		this.paymmeth = paymmeth;
	}	

	public String getBankkey() {
		return bankkey;
	}

	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}

	public String getBankacckey() {
		return bankacckey;
	}

	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}

	// ToString method
	public String toString(){

		StringBuilder output = new StringBuilder();

		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("CHDRCOY:		");
		output.append(getChdrcoy());
		output.append("\r\n");
		output.append("CHDRNUM:		");
		output.append(getChdrnum());
		output.append("\r\n");
		output.append("BNYCLT:		");
		output.append(getBnyclt());
		output.append("\r\n");
		output.append("TRANNO:		");
		output.append(getTranno());
		output.append("\r\n");
		output.append("VALIDFLAG:		");
		output.append(getValidflag());
		output.append("\r\n");
		output.append("CURRFR:		");
		output.append(getCurrfr());
		output.append("\r\n");
		output.append("CURRTO:		");
		output.append(getCurrto());
		output.append("\r\n");
		output.append("BNYRLN:		");
		output.append(getBnyrln());
		output.append("\r\n");
		output.append("BNYCD:		");
		output.append(getBnycd());
		output.append("\r\n");
		output.append("BNYPC:		");
		output.append(getBnypc());
		output.append("\r\n");
		output.append("TERMID:		");
		output.append(getTermid());
		output.append("\r\n");
		output.append("USER_T:		");
		output.append(getUserT());
		output.append("\r\n");
		output.append("TRTM:		");
		output.append(getTrtm());
		output.append("\r\n");
		output.append("TRDT:		");
		output.append(getTrdt());
		output.append("\r\n");
		output.append("EFFDATE:		");
		output.append(getEffdate());
		output.append("\r\n");
		output.append("USRPRF:		");
		output.append(getUsrprf());
		output.append("\r\n");
		output.append("JOBNM:		");
		output.append(getJobnm());
		output.append("\r\n");
		output.append("DATIME:		");
		output.append(getDatime());
		output.append("\r\n");
		output.append("BNYTYPE:		");
		output.append(getBnytype());
		output.append("\r\n");
		output.append("CLTRELN:		");
		output.append(getCltreln());
		output.append("\r\n");
		output.append("RELTO:		");
		output.append(getRelto());
		output.append("\r\n");
		output.append("SELFIND:		");
		output.append(getSelfind());
		output.append("\r\n");
		output.append("REVCFLG:		");
		output.append(getRevcflg());
		output.append("\r\n");

		return output.toString();
	}

}
