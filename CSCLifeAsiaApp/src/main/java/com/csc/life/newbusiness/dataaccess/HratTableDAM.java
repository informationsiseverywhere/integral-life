package com.csc.life.newbusiness.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HratTableDAM.java
 * Date: Sun, 30 Aug 2009 03:41:24
 * Class transformed from HRAT.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HratTableDAM extends HratpfTableDAM {

	public HratTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("HRAT");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "ITEMITEM";
		
		QUALIFIEDCOLUMNS = 
		            "ITEMITEM, " +
		            "MFACTHM, " +
		            "MFACTHY, " +
		            "MFACTM, " +
		            "MFACTQ, " +
		            "MFACTW, " +
		            "MFACTWB, " +
		            "MFACTWD, " +
		            "MFACTY, " +
		            "PMUNIT, " +
		            "UNIT, " +
		            "DISCNTMETH, " +
		            "INSPRM01, " +
		            "INSPRM02, " +
		            "INSPRM03, " +
		            "INSPRM04, " +
		            "INSPRM05, " +
		            "INSPRM06, " +
		            "INSPRM07, " +
		            "INSPRM08, " +
		            "INSPRM09, " +
		            "INSPRM10, " +
		            "INSPRM11, " +
		            "INSPRM12, " +
		            "INSPRM13, " +
		            "INSPRM14, " +
		            "INSPRM15, " +
		            "INSPRM16, " +
		            "INSPRM17, " +
		            "INSPRM18, " +
		            "INSPRM19, " +
		            "INSPRM20, " +
		            "INSPRM21, " +
		            "INSPRM22, " +
		            "INSPRM23, " +
		            "INSPRM24, " +
		            "INSPRM25, " +
		            "INSPRM26, " +
		            "INSPRM27, " +
		            "INSPRM28, " +
		            "INSPRM29, " +
		            "INSPRM30, " +
		            "INSPRM31, " +
		            "INSPRM32, " +
		            "INSPRM33, " +
		            "INSPRM34, " +
		            "INSPRM35, " +
		            "INSPRM36, " +
		            "INSPRM37, " +
		            "INSPRM38, " +
		            "INSPRM39, " +
		            "INSPRM40, " +
		            "INSPRM41, " +
		            "INSPRM42, " +
		            "INSPRM43, " +
		            "INSPRM44, " +
		            "INSPRM45, " +
		            "INSPRM46, " +
		            "INSPRM47, " +
		            "INSPRM48, " +
		            "INSPRM49, " +
		            "INSPRM50, " +
		            "INSPRM51, " +
		            "INSPRM52, " +
		            "INSPRM53, " +
		            "INSPRM54, " +
		            "INSPRM55, " +
		            "INSPRM56, " +
		            "INSPRM57, " +
		            "INSPRM58, " +
		            "INSPRM59, " +
		            "INSPRM60, " +
		            "INSPRM61, " +
		            "INSPRM62, " +
		            "INSPRM63, " +
		            "INSPRM64, " +
		            "INSPRM65, " +
		            "INSPRM66, " +
		            "INSPRM67, " +
		            "INSPRM68, " +
		            "INSPRM69, " +
		            "INSPRM70, " +
		            "INSPRM71, " +
		            "INSPRM72, " +
		            "INSPRM73, " +
		            "INSPRM74, " +
		            "INSPRM75, " +
		            "INSPRM76, " +
		            "INSPRM77, " +
		            "INSPRM78, " +
		            "INSPRM79, " +
		            "INSPRM80, " +
		            "INSPRM81, " +
		            "INSPRM82, " +
		            "INSPRM83, " +
		            "INSPRM84, " +
		            "INSPRM85, " +
		            "INSPRM86, " +
		            "INSPRM87, " +
		            "INSPRM88, " +
		            "INSPRM89, " +
		            "INSPRM90, " +
		            "INSPRM91, " +
		            "INSPRM92, " +
		            "INSPRM93, " +
		            "INSPRM94, " +
		            "INSPRM95, " +
		            "INSPRM96, " +
		            "INSPRM97, " +
		            "INSPRM98, " +
		            "INSPRM99, " +
		            "INSPREM, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "INSTPR01, " +
		            "INSTPR02, " +
		            "INSTPR03, " +
		            "INSTPR04, " +
		            "INSTPR05, " +
		            "INSTPR06, " +
		            "INSTPR07, " +
		            "INSTPR08, " +
		            "INSTPR09, " +
		            "INSTPR10, " +
		            "INSTPR11, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "ITEMITEM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "ITEMITEM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               itemitem,
                               mfacthm,
                               mfacthy,
                               mfactm,
                               mfactq,
                               mfactw,
                               mfact2w,
                               mfact4w,
                               mfacty,
                               premUnit,
                               unit,
                               disccntmeth,
                               insprm01,
                               insprm02,
                               insprm03,
                               insprm04,
                               insprm05,
                               insprm06,
                               insprm07,
                               insprm08,
                               insprm09,
                               insprm10,
                               insprm11,
                               insprm12,
                               insprm13,
                               insprm14,
                               insprm15,
                               insprm16,
                               insprm17,
                               insprm18,
                               insprm19,
                               insprm20,
                               insprm21,
                               insprm22,
                               insprm23,
                               insprm24,
                               insprm25,
                               insprm26,
                               insprm27,
                               insprm28,
                               insprm29,
                               insprm30,
                               insprm31,
                               insprm32,
                               insprm33,
                               insprm34,
                               insprm35,
                               insprm36,
                               insprm37,
                               insprm38,
                               insprm39,
                               insprm40,
                               insprm41,
                               insprm42,
                               insprm43,
                               insprm44,
                               insprm45,
                               insprm46,
                               insprm47,
                               insprm48,
                               insprm49,
                               insprm50,
                               insprm51,
                               insprm52,
                               insprm53,
                               insprm54,
                               insprm55,
                               insprm56,
                               insprm57,
                               insprm58,
                               insprm59,
                               insprm60,
                               insprm61,
                               insprm62,
                               insprm63,
                               insprm64,
                               insprm65,
                               insprm66,
                               insprm67,
                               insprm68,
                               insprm69,
                               insprm70,
                               insprm71,
                               insprm72,
                               insprm73,
                               insprm74,
                               insprm75,
                               insprm76,
                               insprm77,
                               insprm78,
                               insprm79,
                               insprm80,
                               insprm81,
                               insprm82,
                               insprm83,
                               insprm84,
                               insprm85,
                               insprm86,
                               insprm87,
                               insprm88,
                               insprm89,
                               insprm90,
                               insprm91,
                               insprm92,
                               insprm93,
                               insprm94,
                               insprm95,
                               insprm96,
                               insprm97,
                               insprm98,
                               insprm99,
                               insprem,
                               userProfile,
                               jobName,
                               datime,
                               instpr01,
                               instpr02,
                               instpr03,
                               instpr04,
                               instpr05,
                               instpr06,
                               instpr07,
                               instpr08,
                               instpr09,
                               instpr10,
                               instpr11,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(248);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getItemitem().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, itemitem);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(itemitem.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(533);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ getMfacthm().toInternal()
					+ getMfacthy().toInternal()
					+ getMfactm().toInternal()
					+ getMfactq().toInternal()
					+ getMfactw().toInternal()
					+ getMfact2w().toInternal()
					+ getMfact4w().toInternal()
					+ getMfacty().toInternal()
					+ getPremUnit().toInternal()
					+ getUnit().toInternal()
					+ getDisccntmeth().toInternal()
					+ getInsprm01().toInternal()
					+ getInsprm02().toInternal()
					+ getInsprm03().toInternal()
					+ getInsprm04().toInternal()
					+ getInsprm05().toInternal()
					+ getInsprm06().toInternal()
					+ getInsprm07().toInternal()
					+ getInsprm08().toInternal()
					+ getInsprm09().toInternal()
					+ getInsprm10().toInternal()
					+ getInsprm11().toInternal()
					+ getInsprm12().toInternal()
					+ getInsprm13().toInternal()
					+ getInsprm14().toInternal()
					+ getInsprm15().toInternal()
					+ getInsprm16().toInternal()
					+ getInsprm17().toInternal()
					+ getInsprm18().toInternal()
					+ getInsprm19().toInternal()
					+ getInsprm20().toInternal()
					+ getInsprm21().toInternal()
					+ getInsprm22().toInternal()
					+ getInsprm23().toInternal()
					+ getInsprm24().toInternal()
					+ getInsprm25().toInternal()
					+ getInsprm26().toInternal()
					+ getInsprm27().toInternal()
					+ getInsprm28().toInternal()
					+ getInsprm29().toInternal()
					+ getInsprm30().toInternal()
					+ getInsprm31().toInternal()
					+ getInsprm32().toInternal()
					+ getInsprm33().toInternal()
					+ getInsprm34().toInternal()
					+ getInsprm35().toInternal()
					+ getInsprm36().toInternal()
					+ getInsprm37().toInternal()
					+ getInsprm38().toInternal()
					+ getInsprm39().toInternal()
					+ getInsprm40().toInternal()
					+ getInsprm41().toInternal()
					+ getInsprm42().toInternal()
					+ getInsprm43().toInternal()
					+ getInsprm44().toInternal()
					+ getInsprm45().toInternal()
					+ getInsprm46().toInternal()
					+ getInsprm47().toInternal()
					+ getInsprm48().toInternal()
					+ getInsprm49().toInternal()
					+ getInsprm50().toInternal()
					+ getInsprm51().toInternal()
					+ getInsprm52().toInternal()
					+ getInsprm53().toInternal()
					+ getInsprm54().toInternal()
					+ getInsprm55().toInternal()
					+ getInsprm56().toInternal()
					+ getInsprm57().toInternal()
					+ getInsprm58().toInternal()
					+ getInsprm59().toInternal()
					+ getInsprm60().toInternal()
					+ getInsprm61().toInternal()
					+ getInsprm62().toInternal()
					+ getInsprm63().toInternal()
					+ getInsprm64().toInternal()
					+ getInsprm65().toInternal()
					+ getInsprm66().toInternal()
					+ getInsprm67().toInternal()
					+ getInsprm68().toInternal()
					+ getInsprm69().toInternal()
					+ getInsprm70().toInternal()
					+ getInsprm71().toInternal()
					+ getInsprm72().toInternal()
					+ getInsprm73().toInternal()
					+ getInsprm74().toInternal()
					+ getInsprm75().toInternal()
					+ getInsprm76().toInternal()
					+ getInsprm77().toInternal()
					+ getInsprm78().toInternal()
					+ getInsprm79().toInternal()
					+ getInsprm80().toInternal()
					+ getInsprm81().toInternal()
					+ getInsprm82().toInternal()
					+ getInsprm83().toInternal()
					+ getInsprm84().toInternal()
					+ getInsprm85().toInternal()
					+ getInsprm86().toInternal()
					+ getInsprm87().toInternal()
					+ getInsprm88().toInternal()
					+ getInsprm89().toInternal()
					+ getInsprm90().toInternal()
					+ getInsprm91().toInternal()
					+ getInsprm92().toInternal()
					+ getInsprm93().toInternal()
					+ getInsprm94().toInternal()
					+ getInsprm95().toInternal()
					+ getInsprm96().toInternal()
					+ getInsprm97().toInternal()
					+ getInsprm98().toInternal()
					+ getInsprm99().toInternal()
					+ getInsprem().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()
					+ getInstpr01().toInternal()
					+ getInstpr02().toInternal()
					+ getInstpr03().toInternal()
					+ getInstpr04().toInternal()
					+ getInstpr05().toInternal()
					+ getInstpr06().toInternal()
					+ getInstpr07().toInternal()
					+ getInstpr08().toInternal()
					+ getInstpr09().toInternal()
					+ getInstpr10().toInternal()
					+ getInstpr11().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, mfacthm);
			what = ExternalData.chop(what, mfacthy);
			what = ExternalData.chop(what, mfactm);
			what = ExternalData.chop(what, mfactq);
			what = ExternalData.chop(what, mfactw);
			what = ExternalData.chop(what, mfact2w);
			what = ExternalData.chop(what, mfact4w);
			what = ExternalData.chop(what, mfacty);
			what = ExternalData.chop(what, premUnit);
			what = ExternalData.chop(what, unit);
			what = ExternalData.chop(what, disccntmeth);
			what = ExternalData.chop(what, insprm01);
			what = ExternalData.chop(what, insprm02);
			what = ExternalData.chop(what, insprm03);
			what = ExternalData.chop(what, insprm04);
			what = ExternalData.chop(what, insprm05);
			what = ExternalData.chop(what, insprm06);
			what = ExternalData.chop(what, insprm07);
			what = ExternalData.chop(what, insprm08);
			what = ExternalData.chop(what, insprm09);
			what = ExternalData.chop(what, insprm10);
			what = ExternalData.chop(what, insprm11);
			what = ExternalData.chop(what, insprm12);
			what = ExternalData.chop(what, insprm13);
			what = ExternalData.chop(what, insprm14);
			what = ExternalData.chop(what, insprm15);
			what = ExternalData.chop(what, insprm16);
			what = ExternalData.chop(what, insprm17);
			what = ExternalData.chop(what, insprm18);
			what = ExternalData.chop(what, insprm19);
			what = ExternalData.chop(what, insprm20);
			what = ExternalData.chop(what, insprm21);
			what = ExternalData.chop(what, insprm22);
			what = ExternalData.chop(what, insprm23);
			what = ExternalData.chop(what, insprm24);
			what = ExternalData.chop(what, insprm25);
			what = ExternalData.chop(what, insprm26);
			what = ExternalData.chop(what, insprm27);
			what = ExternalData.chop(what, insprm28);
			what = ExternalData.chop(what, insprm29);
			what = ExternalData.chop(what, insprm30);
			what = ExternalData.chop(what, insprm31);
			what = ExternalData.chop(what, insprm32);
			what = ExternalData.chop(what, insprm33);
			what = ExternalData.chop(what, insprm34);
			what = ExternalData.chop(what, insprm35);
			what = ExternalData.chop(what, insprm36);
			what = ExternalData.chop(what, insprm37);
			what = ExternalData.chop(what, insprm38);
			what = ExternalData.chop(what, insprm39);
			what = ExternalData.chop(what, insprm40);
			what = ExternalData.chop(what, insprm41);
			what = ExternalData.chop(what, insprm42);
			what = ExternalData.chop(what, insprm43);
			what = ExternalData.chop(what, insprm44);
			what = ExternalData.chop(what, insprm45);
			what = ExternalData.chop(what, insprm46);
			what = ExternalData.chop(what, insprm47);
			what = ExternalData.chop(what, insprm48);
			what = ExternalData.chop(what, insprm49);
			what = ExternalData.chop(what, insprm50);
			what = ExternalData.chop(what, insprm51);
			what = ExternalData.chop(what, insprm52);
			what = ExternalData.chop(what, insprm53);
			what = ExternalData.chop(what, insprm54);
			what = ExternalData.chop(what, insprm55);
			what = ExternalData.chop(what, insprm56);
			what = ExternalData.chop(what, insprm57);
			what = ExternalData.chop(what, insprm58);
			what = ExternalData.chop(what, insprm59);
			what = ExternalData.chop(what, insprm60);
			what = ExternalData.chop(what, insprm61);
			what = ExternalData.chop(what, insprm62);
			what = ExternalData.chop(what, insprm63);
			what = ExternalData.chop(what, insprm64);
			what = ExternalData.chop(what, insprm65);
			what = ExternalData.chop(what, insprm66);
			what = ExternalData.chop(what, insprm67);
			what = ExternalData.chop(what, insprm68);
			what = ExternalData.chop(what, insprm69);
			what = ExternalData.chop(what, insprm70);
			what = ExternalData.chop(what, insprm71);
			what = ExternalData.chop(what, insprm72);
			what = ExternalData.chop(what, insprm73);
			what = ExternalData.chop(what, insprm74);
			what = ExternalData.chop(what, insprm75);
			what = ExternalData.chop(what, insprm76);
			what = ExternalData.chop(what, insprm77);
			what = ExternalData.chop(what, insprm78);
			what = ExternalData.chop(what, insprm79);
			what = ExternalData.chop(what, insprm80);
			what = ExternalData.chop(what, insprm81);
			what = ExternalData.chop(what, insprm82);
			what = ExternalData.chop(what, insprm83);
			what = ExternalData.chop(what, insprm84);
			what = ExternalData.chop(what, insprm85);
			what = ExternalData.chop(what, insprm86);
			what = ExternalData.chop(what, insprm87);
			what = ExternalData.chop(what, insprm88);
			what = ExternalData.chop(what, insprm89);
			what = ExternalData.chop(what, insprm90);
			what = ExternalData.chop(what, insprm91);
			what = ExternalData.chop(what, insprm92);
			what = ExternalData.chop(what, insprm93);
			what = ExternalData.chop(what, insprm94);
			what = ExternalData.chop(what, insprm95);
			what = ExternalData.chop(what, insprm96);
			what = ExternalData.chop(what, insprm97);
			what = ExternalData.chop(what, insprm98);
			what = ExternalData.chop(what, insprm99);
			what = ExternalData.chop(what, insprem);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);
			what = ExternalData.chop(what, instpr01);
			what = ExternalData.chop(what, instpr02);
			what = ExternalData.chop(what, instpr03);
			what = ExternalData.chop(what, instpr04);
			what = ExternalData.chop(what, instpr05);
			what = ExternalData.chop(what, instpr06);
			what = ExternalData.chop(what, instpr07);
			what = ExternalData.chop(what, instpr08);
			what = ExternalData.chop(what, instpr09);
			what = ExternalData.chop(what, instpr10);
			what = ExternalData.chop(what, instpr11);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getItemitem() {
		return itemitem;
	}
	public void setItemitem(Object what) {
		itemitem.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getMfacthm() {
		return mfacthm;
	}
	public void setMfacthm(Object what) {
		setMfacthm(what, false);
	}
	public void setMfacthm(Object what, boolean rounded) {
		if (rounded)
			mfacthm.setRounded(what);
		else
			mfacthm.set(what);
	}	
	public PackedDecimalData getMfacthy() {
		return mfacthy;
	}
	public void setMfacthy(Object what) {
		setMfacthy(what, false);
	}
	public void setMfacthy(Object what, boolean rounded) {
		if (rounded)
			mfacthy.setRounded(what);
		else
			mfacthy.set(what);
	}	
	public PackedDecimalData getMfactm() {
		return mfactm;
	}
	public void setMfactm(Object what) {
		setMfactm(what, false);
	}
	public void setMfactm(Object what, boolean rounded) {
		if (rounded)
			mfactm.setRounded(what);
		else
			mfactm.set(what);
	}	
	public PackedDecimalData getMfactq() {
		return mfactq;
	}
	public void setMfactq(Object what) {
		setMfactq(what, false);
	}
	public void setMfactq(Object what, boolean rounded) {
		if (rounded)
			mfactq.setRounded(what);
		else
			mfactq.set(what);
	}	
	public PackedDecimalData getMfactw() {
		return mfactw;
	}
	public void setMfactw(Object what) {
		setMfactw(what, false);
	}
	public void setMfactw(Object what, boolean rounded) {
		if (rounded)
			mfactw.setRounded(what);
		else
			mfactw.set(what);
	}	
	public PackedDecimalData getMfact2w() {
		return mfact2w;
	}
	public void setMfact2w(Object what) {
		setMfact2w(what, false);
	}
	public void setMfact2w(Object what, boolean rounded) {
		if (rounded)
			mfact2w.setRounded(what);
		else
			mfact2w.set(what);
	}	
	public PackedDecimalData getMfact4w() {
		return mfact4w;
	}
	public void setMfact4w(Object what) {
		setMfact4w(what, false);
	}
	public void setMfact4w(Object what, boolean rounded) {
		if (rounded)
			mfact4w.setRounded(what);
		else
			mfact4w.set(what);
	}	
	public PackedDecimalData getMfacty() {
		return mfacty;
	}
	public void setMfacty(Object what) {
		setMfacty(what, false);
	}
	public void setMfacty(Object what, boolean rounded) {
		if (rounded)
			mfacty.setRounded(what);
		else
			mfacty.set(what);
	}	
	public PackedDecimalData getPremUnit() {
		return premUnit;
	}
	public void setPremUnit(Object what) {
		setPremUnit(what, false);
	}
	public void setPremUnit(Object what, boolean rounded) {
		if (rounded)
			premUnit.setRounded(what);
		else
			premUnit.set(what);
	}	
	public PackedDecimalData getUnit() {
		return unit;
	}
	public void setUnit(Object what) {
		setUnit(what, false);
	}
	public void setUnit(Object what, boolean rounded) {
		if (rounded)
			unit.setRounded(what);
		else
			unit.set(what);
	}	
	public FixedLengthStringData getDisccntmeth() {
		return disccntmeth;
	}
	public void setDisccntmeth(Object what) {
		disccntmeth.set(what);
	}	
	public PackedDecimalData getInsprm01() {
		return insprm01;
	}
	public void setInsprm01(Object what) {
		setInsprm01(what, false);
	}
	public void setInsprm01(Object what, boolean rounded) {
		if (rounded)
			insprm01.setRounded(what);
		else
			insprm01.set(what);
	}	
	public PackedDecimalData getInsprm02() {
		return insprm02;
	}
	public void setInsprm02(Object what) {
		setInsprm02(what, false);
	}
	public void setInsprm02(Object what, boolean rounded) {
		if (rounded)
			insprm02.setRounded(what);
		else
			insprm02.set(what);
	}	
	public PackedDecimalData getInsprm03() {
		return insprm03;
	}
	public void setInsprm03(Object what) {
		setInsprm03(what, false);
	}
	public void setInsprm03(Object what, boolean rounded) {
		if (rounded)
			insprm03.setRounded(what);
		else
			insprm03.set(what);
	}	
	public PackedDecimalData getInsprm04() {
		return insprm04;
	}
	public void setInsprm04(Object what) {
		setInsprm04(what, false);
	}
	public void setInsprm04(Object what, boolean rounded) {
		if (rounded)
			insprm04.setRounded(what);
		else
			insprm04.set(what);
	}	
	public PackedDecimalData getInsprm05() {
		return insprm05;
	}
	public void setInsprm05(Object what) {
		setInsprm05(what, false);
	}
	public void setInsprm05(Object what, boolean rounded) {
		if (rounded)
			insprm05.setRounded(what);
		else
			insprm05.set(what);
	}	
	public PackedDecimalData getInsprm06() {
		return insprm06;
	}
	public void setInsprm06(Object what) {
		setInsprm06(what, false);
	}
	public void setInsprm06(Object what, boolean rounded) {
		if (rounded)
			insprm06.setRounded(what);
		else
			insprm06.set(what);
	}	
	public PackedDecimalData getInsprm07() {
		return insprm07;
	}
	public void setInsprm07(Object what) {
		setInsprm07(what, false);
	}
	public void setInsprm07(Object what, boolean rounded) {
		if (rounded)
			insprm07.setRounded(what);
		else
			insprm07.set(what);
	}	
	public PackedDecimalData getInsprm08() {
		return insprm08;
	}
	public void setInsprm08(Object what) {
		setInsprm08(what, false);
	}
	public void setInsprm08(Object what, boolean rounded) {
		if (rounded)
			insprm08.setRounded(what);
		else
			insprm08.set(what);
	}	
	public PackedDecimalData getInsprm09() {
		return insprm09;
	}
	public void setInsprm09(Object what) {
		setInsprm09(what, false);
	}
	public void setInsprm09(Object what, boolean rounded) {
		if (rounded)
			insprm09.setRounded(what);
		else
			insprm09.set(what);
	}	
	public PackedDecimalData getInsprm10() {
		return insprm10;
	}
	public void setInsprm10(Object what) {
		setInsprm10(what, false);
	}
	public void setInsprm10(Object what, boolean rounded) {
		if (rounded)
			insprm10.setRounded(what);
		else
			insprm10.set(what);
	}	
	public PackedDecimalData getInsprm11() {
		return insprm11;
	}
	public void setInsprm11(Object what) {
		setInsprm11(what, false);
	}
	public void setInsprm11(Object what, boolean rounded) {
		if (rounded)
			insprm11.setRounded(what);
		else
			insprm11.set(what);
	}	
	public PackedDecimalData getInsprm12() {
		return insprm12;
	}
	public void setInsprm12(Object what) {
		setInsprm12(what, false);
	}
	public void setInsprm12(Object what, boolean rounded) {
		if (rounded)
			insprm12.setRounded(what);
		else
			insprm12.set(what);
	}	
	public PackedDecimalData getInsprm13() {
		return insprm13;
	}
	public void setInsprm13(Object what) {
		setInsprm13(what, false);
	}
	public void setInsprm13(Object what, boolean rounded) {
		if (rounded)
			insprm13.setRounded(what);
		else
			insprm13.set(what);
	}	
	public PackedDecimalData getInsprm14() {
		return insprm14;
	}
	public void setInsprm14(Object what) {
		setInsprm14(what, false);
	}
	public void setInsprm14(Object what, boolean rounded) {
		if (rounded)
			insprm14.setRounded(what);
		else
			insprm14.set(what);
	}	
	public PackedDecimalData getInsprm15() {
		return insprm15;
	}
	public void setInsprm15(Object what) {
		setInsprm15(what, false);
	}
	public void setInsprm15(Object what, boolean rounded) {
		if (rounded)
			insprm15.setRounded(what);
		else
			insprm15.set(what);
	}	
	public PackedDecimalData getInsprm16() {
		return insprm16;
	}
	public void setInsprm16(Object what) {
		setInsprm16(what, false);
	}
	public void setInsprm16(Object what, boolean rounded) {
		if (rounded)
			insprm16.setRounded(what);
		else
			insprm16.set(what);
	}	
	public PackedDecimalData getInsprm17() {
		return insprm17;
	}
	public void setInsprm17(Object what) {
		setInsprm17(what, false);
	}
	public void setInsprm17(Object what, boolean rounded) {
		if (rounded)
			insprm17.setRounded(what);
		else
			insprm17.set(what);
	}	
	public PackedDecimalData getInsprm18() {
		return insprm18;
	}
	public void setInsprm18(Object what) {
		setInsprm18(what, false);
	}
	public void setInsprm18(Object what, boolean rounded) {
		if (rounded)
			insprm18.setRounded(what);
		else
			insprm18.set(what);
	}	
	public PackedDecimalData getInsprm19() {
		return insprm19;
	}
	public void setInsprm19(Object what) {
		setInsprm19(what, false);
	}
	public void setInsprm19(Object what, boolean rounded) {
		if (rounded)
			insprm19.setRounded(what);
		else
			insprm19.set(what);
	}	
	public PackedDecimalData getInsprm20() {
		return insprm20;
	}
	public void setInsprm20(Object what) {
		setInsprm20(what, false);
	}
	public void setInsprm20(Object what, boolean rounded) {
		if (rounded)
			insprm20.setRounded(what);
		else
			insprm20.set(what);
	}	
	public PackedDecimalData getInsprm21() {
		return insprm21;
	}
	public void setInsprm21(Object what) {
		setInsprm21(what, false);
	}
	public void setInsprm21(Object what, boolean rounded) {
		if (rounded)
			insprm21.setRounded(what);
		else
			insprm21.set(what);
	}	
	public PackedDecimalData getInsprm22() {
		return insprm22;
	}
	public void setInsprm22(Object what) {
		setInsprm22(what, false);
	}
	public void setInsprm22(Object what, boolean rounded) {
		if (rounded)
			insprm22.setRounded(what);
		else
			insprm22.set(what);
	}	
	public PackedDecimalData getInsprm23() {
		return insprm23;
	}
	public void setInsprm23(Object what) {
		setInsprm23(what, false);
	}
	public void setInsprm23(Object what, boolean rounded) {
		if (rounded)
			insprm23.setRounded(what);
		else
			insprm23.set(what);
	}	
	public PackedDecimalData getInsprm24() {
		return insprm24;
	}
	public void setInsprm24(Object what) {
		setInsprm24(what, false);
	}
	public void setInsprm24(Object what, boolean rounded) {
		if (rounded)
			insprm24.setRounded(what);
		else
			insprm24.set(what);
	}	
	public PackedDecimalData getInsprm25() {
		return insprm25;
	}
	public void setInsprm25(Object what) {
		setInsprm25(what, false);
	}
	public void setInsprm25(Object what, boolean rounded) {
		if (rounded)
			insprm25.setRounded(what);
		else
			insprm25.set(what);
	}	
	public PackedDecimalData getInsprm26() {
		return insprm26;
	}
	public void setInsprm26(Object what) {
		setInsprm26(what, false);
	}
	public void setInsprm26(Object what, boolean rounded) {
		if (rounded)
			insprm26.setRounded(what);
		else
			insprm26.set(what);
	}	
	public PackedDecimalData getInsprm27() {
		return insprm27;
	}
	public void setInsprm27(Object what) {
		setInsprm27(what, false);
	}
	public void setInsprm27(Object what, boolean rounded) {
		if (rounded)
			insprm27.setRounded(what);
		else
			insprm27.set(what);
	}	
	public PackedDecimalData getInsprm28() {
		return insprm28;
	}
	public void setInsprm28(Object what) {
		setInsprm28(what, false);
	}
	public void setInsprm28(Object what, boolean rounded) {
		if (rounded)
			insprm28.setRounded(what);
		else
			insprm28.set(what);
	}	
	public PackedDecimalData getInsprm29() {
		return insprm29;
	}
	public void setInsprm29(Object what) {
		setInsprm29(what, false);
	}
	public void setInsprm29(Object what, boolean rounded) {
		if (rounded)
			insprm29.setRounded(what);
		else
			insprm29.set(what);
	}	
	public PackedDecimalData getInsprm30() {
		return insprm30;
	}
	public void setInsprm30(Object what) {
		setInsprm30(what, false);
	}
	public void setInsprm30(Object what, boolean rounded) {
		if (rounded)
			insprm30.setRounded(what);
		else
			insprm30.set(what);
	}	
	public PackedDecimalData getInsprm31() {
		return insprm31;
	}
	public void setInsprm31(Object what) {
		setInsprm31(what, false);
	}
	public void setInsprm31(Object what, boolean rounded) {
		if (rounded)
			insprm31.setRounded(what);
		else
			insprm31.set(what);
	}	
	public PackedDecimalData getInsprm32() {
		return insprm32;
	}
	public void setInsprm32(Object what) {
		setInsprm32(what, false);
	}
	public void setInsprm32(Object what, boolean rounded) {
		if (rounded)
			insprm32.setRounded(what);
		else
			insprm32.set(what);
	}	
	public PackedDecimalData getInsprm33() {
		return insprm33;
	}
	public void setInsprm33(Object what) {
		setInsprm33(what, false);
	}
	public void setInsprm33(Object what, boolean rounded) {
		if (rounded)
			insprm33.setRounded(what);
		else
			insprm33.set(what);
	}	
	public PackedDecimalData getInsprm34() {
		return insprm34;
	}
	public void setInsprm34(Object what) {
		setInsprm34(what, false);
	}
	public void setInsprm34(Object what, boolean rounded) {
		if (rounded)
			insprm34.setRounded(what);
		else
			insprm34.set(what);
	}	
	public PackedDecimalData getInsprm35() {
		return insprm35;
	}
	public void setInsprm35(Object what) {
		setInsprm35(what, false);
	}
	public void setInsprm35(Object what, boolean rounded) {
		if (rounded)
			insprm35.setRounded(what);
		else
			insprm35.set(what);
	}	
	public PackedDecimalData getInsprm36() {
		return insprm36;
	}
	public void setInsprm36(Object what) {
		setInsprm36(what, false);
	}
	public void setInsprm36(Object what, boolean rounded) {
		if (rounded)
			insprm36.setRounded(what);
		else
			insprm36.set(what);
	}	
	public PackedDecimalData getInsprm37() {
		return insprm37;
	}
	public void setInsprm37(Object what) {
		setInsprm37(what, false);
	}
	public void setInsprm37(Object what, boolean rounded) {
		if (rounded)
			insprm37.setRounded(what);
		else
			insprm37.set(what);
	}	
	public PackedDecimalData getInsprm38() {
		return insprm38;
	}
	public void setInsprm38(Object what) {
		setInsprm38(what, false);
	}
	public void setInsprm38(Object what, boolean rounded) {
		if (rounded)
			insprm38.setRounded(what);
		else
			insprm38.set(what);
	}	
	public PackedDecimalData getInsprm39() {
		return insprm39;
	}
	public void setInsprm39(Object what) {
		setInsprm39(what, false);
	}
	public void setInsprm39(Object what, boolean rounded) {
		if (rounded)
			insprm39.setRounded(what);
		else
			insprm39.set(what);
	}	
	public PackedDecimalData getInsprm40() {
		return insprm40;
	}
	public void setInsprm40(Object what) {
		setInsprm40(what, false);
	}
	public void setInsprm40(Object what, boolean rounded) {
		if (rounded)
			insprm40.setRounded(what);
		else
			insprm40.set(what);
	}	
	public PackedDecimalData getInsprm41() {
		return insprm41;
	}
	public void setInsprm41(Object what) {
		setInsprm41(what, false);
	}
	public void setInsprm41(Object what, boolean rounded) {
		if (rounded)
			insprm41.setRounded(what);
		else
			insprm41.set(what);
	}	
	public PackedDecimalData getInsprm42() {
		return insprm42;
	}
	public void setInsprm42(Object what) {
		setInsprm42(what, false);
	}
	public void setInsprm42(Object what, boolean rounded) {
		if (rounded)
			insprm42.setRounded(what);
		else
			insprm42.set(what);
	}	
	public PackedDecimalData getInsprm43() {
		return insprm43;
	}
	public void setInsprm43(Object what) {
		setInsprm43(what, false);
	}
	public void setInsprm43(Object what, boolean rounded) {
		if (rounded)
			insprm43.setRounded(what);
		else
			insprm43.set(what);
	}	
	public PackedDecimalData getInsprm44() {
		return insprm44;
	}
	public void setInsprm44(Object what) {
		setInsprm44(what, false);
	}
	public void setInsprm44(Object what, boolean rounded) {
		if (rounded)
			insprm44.setRounded(what);
		else
			insprm44.set(what);
	}	
	public PackedDecimalData getInsprm45() {
		return insprm45;
	}
	public void setInsprm45(Object what) {
		setInsprm45(what, false);
	}
	public void setInsprm45(Object what, boolean rounded) {
		if (rounded)
			insprm45.setRounded(what);
		else
			insprm45.set(what);
	}	
	public PackedDecimalData getInsprm46() {
		return insprm46;
	}
	public void setInsprm46(Object what) {
		setInsprm46(what, false);
	}
	public void setInsprm46(Object what, boolean rounded) {
		if (rounded)
			insprm46.setRounded(what);
		else
			insprm46.set(what);
	}	
	public PackedDecimalData getInsprm47() {
		return insprm47;
	}
	public void setInsprm47(Object what) {
		setInsprm47(what, false);
	}
	public void setInsprm47(Object what, boolean rounded) {
		if (rounded)
			insprm47.setRounded(what);
		else
			insprm47.set(what);
	}	
	public PackedDecimalData getInsprm48() {
		return insprm48;
	}
	public void setInsprm48(Object what) {
		setInsprm48(what, false);
	}
	public void setInsprm48(Object what, boolean rounded) {
		if (rounded)
			insprm48.setRounded(what);
		else
			insprm48.set(what);
	}	
	public PackedDecimalData getInsprm49() {
		return insprm49;
	}
	public void setInsprm49(Object what) {
		setInsprm49(what, false);
	}
	public void setInsprm49(Object what, boolean rounded) {
		if (rounded)
			insprm49.setRounded(what);
		else
			insprm49.set(what);
	}	
	public PackedDecimalData getInsprm50() {
		return insprm50;
	}
	public void setInsprm50(Object what) {
		setInsprm50(what, false);
	}
	public void setInsprm50(Object what, boolean rounded) {
		if (rounded)
			insprm50.setRounded(what);
		else
			insprm50.set(what);
	}	
	public PackedDecimalData getInsprm51() {
		return insprm51;
	}
	public void setInsprm51(Object what) {
		setInsprm51(what, false);
	}
	public void setInsprm51(Object what, boolean rounded) {
		if (rounded)
			insprm51.setRounded(what);
		else
			insprm51.set(what);
	}	
	public PackedDecimalData getInsprm52() {
		return insprm52;
	}
	public void setInsprm52(Object what) {
		setInsprm52(what, false);
	}
	public void setInsprm52(Object what, boolean rounded) {
		if (rounded)
			insprm52.setRounded(what);
		else
			insprm52.set(what);
	}	
	public PackedDecimalData getInsprm53() {
		return insprm53;
	}
	public void setInsprm53(Object what) {
		setInsprm53(what, false);
	}
	public void setInsprm53(Object what, boolean rounded) {
		if (rounded)
			insprm53.setRounded(what);
		else
			insprm53.set(what);
	}	
	public PackedDecimalData getInsprm54() {
		return insprm54;
	}
	public void setInsprm54(Object what) {
		setInsprm54(what, false);
	}
	public void setInsprm54(Object what, boolean rounded) {
		if (rounded)
			insprm54.setRounded(what);
		else
			insprm54.set(what);
	}	
	public PackedDecimalData getInsprm55() {
		return insprm55;
	}
	public void setInsprm55(Object what) {
		setInsprm55(what, false);
	}
	public void setInsprm55(Object what, boolean rounded) {
		if (rounded)
			insprm55.setRounded(what);
		else
			insprm55.set(what);
	}	
	public PackedDecimalData getInsprm56() {
		return insprm56;
	}
	public void setInsprm56(Object what) {
		setInsprm56(what, false);
	}
	public void setInsprm56(Object what, boolean rounded) {
		if (rounded)
			insprm56.setRounded(what);
		else
			insprm56.set(what);
	}	
	public PackedDecimalData getInsprm57() {
		return insprm57;
	}
	public void setInsprm57(Object what) {
		setInsprm57(what, false);
	}
	public void setInsprm57(Object what, boolean rounded) {
		if (rounded)
			insprm57.setRounded(what);
		else
			insprm57.set(what);
	}	
	public PackedDecimalData getInsprm58() {
		return insprm58;
	}
	public void setInsprm58(Object what) {
		setInsprm58(what, false);
	}
	public void setInsprm58(Object what, boolean rounded) {
		if (rounded)
			insprm58.setRounded(what);
		else
			insprm58.set(what);
	}	
	public PackedDecimalData getInsprm59() {
		return insprm59;
	}
	public void setInsprm59(Object what) {
		setInsprm59(what, false);
	}
	public void setInsprm59(Object what, boolean rounded) {
		if (rounded)
			insprm59.setRounded(what);
		else
			insprm59.set(what);
	}	
	public PackedDecimalData getInsprm60() {
		return insprm60;
	}
	public void setInsprm60(Object what) {
		setInsprm60(what, false);
	}
	public void setInsprm60(Object what, boolean rounded) {
		if (rounded)
			insprm60.setRounded(what);
		else
			insprm60.set(what);
	}	
	public PackedDecimalData getInsprm61() {
		return insprm61;
	}
	public void setInsprm61(Object what) {
		setInsprm61(what, false);
	}
	public void setInsprm61(Object what, boolean rounded) {
		if (rounded)
			insprm61.setRounded(what);
		else
			insprm61.set(what);
	}	
	public PackedDecimalData getInsprm62() {
		return insprm62;
	}
	public void setInsprm62(Object what) {
		setInsprm62(what, false);
	}
	public void setInsprm62(Object what, boolean rounded) {
		if (rounded)
			insprm62.setRounded(what);
		else
			insprm62.set(what);
	}	
	public PackedDecimalData getInsprm63() {
		return insprm63;
	}
	public void setInsprm63(Object what) {
		setInsprm63(what, false);
	}
	public void setInsprm63(Object what, boolean rounded) {
		if (rounded)
			insprm63.setRounded(what);
		else
			insprm63.set(what);
	}	
	public PackedDecimalData getInsprm64() {
		return insprm64;
	}
	public void setInsprm64(Object what) {
		setInsprm64(what, false);
	}
	public void setInsprm64(Object what, boolean rounded) {
		if (rounded)
			insprm64.setRounded(what);
		else
			insprm64.set(what);
	}	
	public PackedDecimalData getInsprm65() {
		return insprm65;
	}
	public void setInsprm65(Object what) {
		setInsprm65(what, false);
	}
	public void setInsprm65(Object what, boolean rounded) {
		if (rounded)
			insprm65.setRounded(what);
		else
			insprm65.set(what);
	}	
	public PackedDecimalData getInsprm66() {
		return insprm66;
	}
	public void setInsprm66(Object what) {
		setInsprm66(what, false);
	}
	public void setInsprm66(Object what, boolean rounded) {
		if (rounded)
			insprm66.setRounded(what);
		else
			insprm66.set(what);
	}	
	public PackedDecimalData getInsprm67() {
		return insprm67;
	}
	public void setInsprm67(Object what) {
		setInsprm67(what, false);
	}
	public void setInsprm67(Object what, boolean rounded) {
		if (rounded)
			insprm67.setRounded(what);
		else
			insprm67.set(what);
	}	
	public PackedDecimalData getInsprm68() {
		return insprm68;
	}
	public void setInsprm68(Object what) {
		setInsprm68(what, false);
	}
	public void setInsprm68(Object what, boolean rounded) {
		if (rounded)
			insprm68.setRounded(what);
		else
			insprm68.set(what);
	}	
	public PackedDecimalData getInsprm69() {
		return insprm69;
	}
	public void setInsprm69(Object what) {
		setInsprm69(what, false);
	}
	public void setInsprm69(Object what, boolean rounded) {
		if (rounded)
			insprm69.setRounded(what);
		else
			insprm69.set(what);
	}	
	public PackedDecimalData getInsprm70() {
		return insprm70;
	}
	public void setInsprm70(Object what) {
		setInsprm70(what, false);
	}
	public void setInsprm70(Object what, boolean rounded) {
		if (rounded)
			insprm70.setRounded(what);
		else
			insprm70.set(what);
	}	
	public PackedDecimalData getInsprm71() {
		return insprm71;
	}
	public void setInsprm71(Object what) {
		setInsprm71(what, false);
	}
	public void setInsprm71(Object what, boolean rounded) {
		if (rounded)
			insprm71.setRounded(what);
		else
			insprm71.set(what);
	}	
	public PackedDecimalData getInsprm72() {
		return insprm72;
	}
	public void setInsprm72(Object what) {
		setInsprm72(what, false);
	}
	public void setInsprm72(Object what, boolean rounded) {
		if (rounded)
			insprm72.setRounded(what);
		else
			insprm72.set(what);
	}	
	public PackedDecimalData getInsprm73() {
		return insprm73;
	}
	public void setInsprm73(Object what) {
		setInsprm73(what, false);
	}
	public void setInsprm73(Object what, boolean rounded) {
		if (rounded)
			insprm73.setRounded(what);
		else
			insprm73.set(what);
	}	
	public PackedDecimalData getInsprm74() {
		return insprm74;
	}
	public void setInsprm74(Object what) {
		setInsprm74(what, false);
	}
	public void setInsprm74(Object what, boolean rounded) {
		if (rounded)
			insprm74.setRounded(what);
		else
			insprm74.set(what);
	}	
	public PackedDecimalData getInsprm75() {
		return insprm75;
	}
	public void setInsprm75(Object what) {
		setInsprm75(what, false);
	}
	public void setInsprm75(Object what, boolean rounded) {
		if (rounded)
			insprm75.setRounded(what);
		else
			insprm75.set(what);
	}	
	public PackedDecimalData getInsprm76() {
		return insprm76;
	}
	public void setInsprm76(Object what) {
		setInsprm76(what, false);
	}
	public void setInsprm76(Object what, boolean rounded) {
		if (rounded)
			insprm76.setRounded(what);
		else
			insprm76.set(what);
	}	
	public PackedDecimalData getInsprm77() {
		return insprm77;
	}
	public void setInsprm77(Object what) {
		setInsprm77(what, false);
	}
	public void setInsprm77(Object what, boolean rounded) {
		if (rounded)
			insprm77.setRounded(what);
		else
			insprm77.set(what);
	}	
	public PackedDecimalData getInsprm78() {
		return insprm78;
	}
	public void setInsprm78(Object what) {
		setInsprm78(what, false);
	}
	public void setInsprm78(Object what, boolean rounded) {
		if (rounded)
			insprm78.setRounded(what);
		else
			insprm78.set(what);
	}	
	public PackedDecimalData getInsprm79() {
		return insprm79;
	}
	public void setInsprm79(Object what) {
		setInsprm79(what, false);
	}
	public void setInsprm79(Object what, boolean rounded) {
		if (rounded)
			insprm79.setRounded(what);
		else
			insprm79.set(what);
	}	
	public PackedDecimalData getInsprm80() {
		return insprm80;
	}
	public void setInsprm80(Object what) {
		setInsprm80(what, false);
	}
	public void setInsprm80(Object what, boolean rounded) {
		if (rounded)
			insprm80.setRounded(what);
		else
			insprm80.set(what);
	}	
	public PackedDecimalData getInsprm81() {
		return insprm81;
	}
	public void setInsprm81(Object what) {
		setInsprm81(what, false);
	}
	public void setInsprm81(Object what, boolean rounded) {
		if (rounded)
			insprm81.setRounded(what);
		else
			insprm81.set(what);
	}	
	public PackedDecimalData getInsprm82() {
		return insprm82;
	}
	public void setInsprm82(Object what) {
		setInsprm82(what, false);
	}
	public void setInsprm82(Object what, boolean rounded) {
		if (rounded)
			insprm82.setRounded(what);
		else
			insprm82.set(what);
	}	
	public PackedDecimalData getInsprm83() {
		return insprm83;
	}
	public void setInsprm83(Object what) {
		setInsprm83(what, false);
	}
	public void setInsprm83(Object what, boolean rounded) {
		if (rounded)
			insprm83.setRounded(what);
		else
			insprm83.set(what);
	}	
	public PackedDecimalData getInsprm84() {
		return insprm84;
	}
	public void setInsprm84(Object what) {
		setInsprm84(what, false);
	}
	public void setInsprm84(Object what, boolean rounded) {
		if (rounded)
			insprm84.setRounded(what);
		else
			insprm84.set(what);
	}	
	public PackedDecimalData getInsprm85() {
		return insprm85;
	}
	public void setInsprm85(Object what) {
		setInsprm85(what, false);
	}
	public void setInsprm85(Object what, boolean rounded) {
		if (rounded)
			insprm85.setRounded(what);
		else
			insprm85.set(what);
	}	
	public PackedDecimalData getInsprm86() {
		return insprm86;
	}
	public void setInsprm86(Object what) {
		setInsprm86(what, false);
	}
	public void setInsprm86(Object what, boolean rounded) {
		if (rounded)
			insprm86.setRounded(what);
		else
			insprm86.set(what);
	}	
	public PackedDecimalData getInsprm87() {
		return insprm87;
	}
	public void setInsprm87(Object what) {
		setInsprm87(what, false);
	}
	public void setInsprm87(Object what, boolean rounded) {
		if (rounded)
			insprm87.setRounded(what);
		else
			insprm87.set(what);
	}	
	public PackedDecimalData getInsprm88() {
		return insprm88;
	}
	public void setInsprm88(Object what) {
		setInsprm88(what, false);
	}
	public void setInsprm88(Object what, boolean rounded) {
		if (rounded)
			insprm88.setRounded(what);
		else
			insprm88.set(what);
	}	
	public PackedDecimalData getInsprm89() {
		return insprm89;
	}
	public void setInsprm89(Object what) {
		setInsprm89(what, false);
	}
	public void setInsprm89(Object what, boolean rounded) {
		if (rounded)
			insprm89.setRounded(what);
		else
			insprm89.set(what);
	}	
	public PackedDecimalData getInsprm90() {
		return insprm90;
	}
	public void setInsprm90(Object what) {
		setInsprm90(what, false);
	}
	public void setInsprm90(Object what, boolean rounded) {
		if (rounded)
			insprm90.setRounded(what);
		else
			insprm90.set(what);
	}	
	public PackedDecimalData getInsprm91() {
		return insprm91;
	}
	public void setInsprm91(Object what) {
		setInsprm91(what, false);
	}
	public void setInsprm91(Object what, boolean rounded) {
		if (rounded)
			insprm91.setRounded(what);
		else
			insprm91.set(what);
	}	
	public PackedDecimalData getInsprm92() {
		return insprm92;
	}
	public void setInsprm92(Object what) {
		setInsprm92(what, false);
	}
	public void setInsprm92(Object what, boolean rounded) {
		if (rounded)
			insprm92.setRounded(what);
		else
			insprm92.set(what);
	}	
	public PackedDecimalData getInsprm93() {
		return insprm93;
	}
	public void setInsprm93(Object what) {
		setInsprm93(what, false);
	}
	public void setInsprm93(Object what, boolean rounded) {
		if (rounded)
			insprm93.setRounded(what);
		else
			insprm93.set(what);
	}	
	public PackedDecimalData getInsprm94() {
		return insprm94;
	}
	public void setInsprm94(Object what) {
		setInsprm94(what, false);
	}
	public void setInsprm94(Object what, boolean rounded) {
		if (rounded)
			insprm94.setRounded(what);
		else
			insprm94.set(what);
	}	
	public PackedDecimalData getInsprm95() {
		return insprm95;
	}
	public void setInsprm95(Object what) {
		setInsprm95(what, false);
	}
	public void setInsprm95(Object what, boolean rounded) {
		if (rounded)
			insprm95.setRounded(what);
		else
			insprm95.set(what);
	}	
	public PackedDecimalData getInsprm96() {
		return insprm96;
	}
	public void setInsprm96(Object what) {
		setInsprm96(what, false);
	}
	public void setInsprm96(Object what, boolean rounded) {
		if (rounded)
			insprm96.setRounded(what);
		else
			insprm96.set(what);
	}	
	public PackedDecimalData getInsprm97() {
		return insprm97;
	}
	public void setInsprm97(Object what) {
		setInsprm97(what, false);
	}
	public void setInsprm97(Object what, boolean rounded) {
		if (rounded)
			insprm97.setRounded(what);
		else
			insprm97.set(what);
	}	
	public PackedDecimalData getInsprm98() {
		return insprm98;
	}
	public void setInsprm98(Object what) {
		setInsprm98(what, false);
	}
	public void setInsprm98(Object what, boolean rounded) {
		if (rounded)
			insprm98.setRounded(what);
		else
			insprm98.set(what);
	}	
	public PackedDecimalData getInsprm99() {
		return insprm99;
	}
	public void setInsprm99(Object what) {
		setInsprm99(what, false);
	}
	public void setInsprm99(Object what, boolean rounded) {
		if (rounded)
			insprm99.setRounded(what);
		else
			insprm99.set(what);
	}	
	public PackedDecimalData getInsprem() {
		return insprem;
	}
	public void setInsprem(Object what) {
		setInsprem(what, false);
	}
	public void setInsprem(Object what, boolean rounded) {
		if (rounded)
			insprem.setRounded(what);
		else
			insprem.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	public PackedDecimalData getInstpr01() {
		return instpr01;
	}
	public void setInstpr01(Object what) {
		setInstpr01(what, false);
	}
	public void setInstpr01(Object what, boolean rounded) {
		if (rounded)
			instpr01.setRounded(what);
		else
			instpr01.set(what);
	}	
	public PackedDecimalData getInstpr02() {
		return instpr02;
	}
	public void setInstpr02(Object what) {
		setInstpr02(what, false);
	}
	public void setInstpr02(Object what, boolean rounded) {
		if (rounded)
			instpr02.setRounded(what);
		else
			instpr02.set(what);
	}	
	public PackedDecimalData getInstpr03() {
		return instpr03;
	}
	public void setInstpr03(Object what) {
		setInstpr03(what, false);
	}
	public void setInstpr03(Object what, boolean rounded) {
		if (rounded)
			instpr03.setRounded(what);
		else
			instpr03.set(what);
	}	
	public PackedDecimalData getInstpr04() {
		return instpr04;
	}
	public void setInstpr04(Object what) {
		setInstpr04(what, false);
	}
	public void setInstpr04(Object what, boolean rounded) {
		if (rounded)
			instpr04.setRounded(what);
		else
			instpr04.set(what);
	}	
	public PackedDecimalData getInstpr05() {
		return instpr05;
	}
	public void setInstpr05(Object what) {
		setInstpr05(what, false);
	}
	public void setInstpr05(Object what, boolean rounded) {
		if (rounded)
			instpr05.setRounded(what);
		else
			instpr05.set(what);
	}	
	public PackedDecimalData getInstpr06() {
		return instpr06;
	}
	public void setInstpr06(Object what) {
		setInstpr06(what, false);
	}
	public void setInstpr06(Object what, boolean rounded) {
		if (rounded)
			instpr06.setRounded(what);
		else
			instpr06.set(what);
	}	
	public PackedDecimalData getInstpr07() {
		return instpr07;
	}
	public void setInstpr07(Object what) {
		setInstpr07(what, false);
	}
	public void setInstpr07(Object what, boolean rounded) {
		if (rounded)
			instpr07.setRounded(what);
		else
			instpr07.set(what);
	}	
	public PackedDecimalData getInstpr08() {
		return instpr08;
	}
	public void setInstpr08(Object what) {
		setInstpr08(what, false);
	}
	public void setInstpr08(Object what, boolean rounded) {
		if (rounded)
			instpr08.setRounded(what);
		else
			instpr08.set(what);
	}	
	public PackedDecimalData getInstpr09() {
		return instpr09;
	}
	public void setInstpr09(Object what) {
		setInstpr09(what, false);
	}
	public void setInstpr09(Object what, boolean rounded) {
		if (rounded)
			instpr09.setRounded(what);
		else
			instpr09.set(what);
	}	
	public PackedDecimalData getInstpr10() {
		return instpr10;
	}
	public void setInstpr10(Object what) {
		setInstpr10(what, false);
	}
	public void setInstpr10(Object what, boolean rounded) {
		if (rounded)
			instpr10.setRounded(what);
		else
			instpr10.set(what);
	}	
	public PackedDecimalData getInstpr11() {
		return instpr11;
	}
	public void setInstpr11(Object what) {
		setInstpr11(what, false);
	}
	public void setInstpr11(Object what, boolean rounded) {
		if (rounded)
			instpr11.setRounded(what);
		else
			instpr11.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getInstprs() {
		return new FixedLengthStringData(instpr01.toInternal()
										+ instpr02.toInternal()
										+ instpr03.toInternal()
										+ instpr04.toInternal()
										+ instpr05.toInternal()
										+ instpr06.toInternal()
										+ instpr07.toInternal()
										+ instpr08.toInternal()
										+ instpr09.toInternal()
										+ instpr10.toInternal()
										+ instpr11.toInternal());
	}
	public void setInstprs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getInstprs().getLength()).init(obj);
	
		what = ExternalData.chop(what, instpr01);
		what = ExternalData.chop(what, instpr02);
		what = ExternalData.chop(what, instpr03);
		what = ExternalData.chop(what, instpr04);
		what = ExternalData.chop(what, instpr05);
		what = ExternalData.chop(what, instpr06);
		what = ExternalData.chop(what, instpr07);
		what = ExternalData.chop(what, instpr08);
		what = ExternalData.chop(what, instpr09);
		what = ExternalData.chop(what, instpr10);
		what = ExternalData.chop(what, instpr11);
	}
	public PackedDecimalData getInstpr(BaseData indx) {
		return getInstpr(indx.toInt());
	}
	public PackedDecimalData getInstpr(int indx) {

		switch (indx) {
			case 1 : return instpr01;
			case 2 : return instpr02;
			case 3 : return instpr03;
			case 4 : return instpr04;
			case 5 : return instpr05;
			case 6 : return instpr06;
			case 7 : return instpr07;
			case 8 : return instpr08;
			case 9 : return instpr09;
			case 10 : return instpr10;
			case 11 : return instpr11;
			default: return null; // Throw error instead?
		}
	
	}
	public void setInstpr(BaseData indx, Object what) {
		setInstpr(indx, what, false);
	}
	public void setInstpr(BaseData indx, Object what, boolean rounded) {
		setInstpr(indx.toInt(), what, rounded);
	}
	public void setInstpr(int indx, Object what) {
		setInstpr(indx, what, false);
	}
	public void setInstpr(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInstpr01(what, rounded);
					 break;
			case 2 : setInstpr02(what, rounded);
					 break;
			case 3 : setInstpr03(what, rounded);
					 break;
			case 4 : setInstpr04(what, rounded);
					 break;
			case 5 : setInstpr05(what, rounded);
					 break;
			case 6 : setInstpr06(what, rounded);
					 break;
			case 7 : setInstpr07(what, rounded);
					 break;
			case 8 : setInstpr08(what, rounded);
					 break;
			case 9 : setInstpr09(what, rounded);
					 break;
			case 10 : setInstpr10(what, rounded);
					 break;
			case 11 : setInstpr11(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getInsprms() {
		return new FixedLengthStringData(insprm01.toInternal()
										+ insprm02.toInternal()
										+ insprm03.toInternal()
										+ insprm04.toInternal()
										+ insprm05.toInternal()
										+ insprm06.toInternal()
										+ insprm07.toInternal()
										+ insprm08.toInternal()
										+ insprm09.toInternal()
										+ insprm10.toInternal()
										+ insprm11.toInternal()
										+ insprm12.toInternal()
										+ insprm13.toInternal()
										+ insprm14.toInternal()
										+ insprm15.toInternal()
										+ insprm16.toInternal()
										+ insprm17.toInternal()
										+ insprm18.toInternal()
										+ insprm19.toInternal()
										+ insprm20.toInternal()
										+ insprm21.toInternal()
										+ insprm22.toInternal()
										+ insprm23.toInternal()
										+ insprm24.toInternal()
										+ insprm25.toInternal()
										+ insprm26.toInternal()
										+ insprm27.toInternal()
										+ insprm28.toInternal()
										+ insprm29.toInternal()
										+ insprm30.toInternal()
										+ insprm31.toInternal()
										+ insprm32.toInternal()
										+ insprm33.toInternal()
										+ insprm34.toInternal()
										+ insprm35.toInternal()
										+ insprm36.toInternal()
										+ insprm37.toInternal()
										+ insprm38.toInternal()
										+ insprm39.toInternal()
										+ insprm40.toInternal()
										+ insprm41.toInternal()
										+ insprm42.toInternal()
										+ insprm43.toInternal()
										+ insprm44.toInternal()
										+ insprm45.toInternal()
										+ insprm46.toInternal()
										+ insprm47.toInternal()
										+ insprm48.toInternal()
										+ insprm49.toInternal()
										+ insprm50.toInternal()
										+ insprm51.toInternal()
										+ insprm52.toInternal()
										+ insprm53.toInternal()
										+ insprm54.toInternal()
										+ insprm55.toInternal()
										+ insprm56.toInternal()
										+ insprm57.toInternal()
										+ insprm58.toInternal()
										+ insprm59.toInternal()
										+ insprm60.toInternal()
										+ insprm61.toInternal()
										+ insprm62.toInternal()
										+ insprm63.toInternal()
										+ insprm64.toInternal()
										+ insprm65.toInternal()
										+ insprm66.toInternal()
										+ insprm67.toInternal()
										+ insprm68.toInternal()
										+ insprm69.toInternal()
										+ insprm70.toInternal()
										+ insprm71.toInternal()
										+ insprm72.toInternal()
										+ insprm73.toInternal()
										+ insprm74.toInternal()
										+ insprm75.toInternal()
										+ insprm76.toInternal()
										+ insprm77.toInternal()
										+ insprm78.toInternal()
										+ insprm79.toInternal()
										+ insprm80.toInternal()
										+ insprm81.toInternal()
										+ insprm82.toInternal()
										+ insprm83.toInternal()
										+ insprm84.toInternal()
										+ insprm85.toInternal()
										+ insprm86.toInternal()
										+ insprm87.toInternal()
										+ insprm88.toInternal()
										+ insprm89.toInternal()
										+ insprm90.toInternal()
										+ insprm91.toInternal()
										+ insprm92.toInternal()
										+ insprm93.toInternal()
										+ insprm94.toInternal()
										+ insprm95.toInternal()
										+ insprm96.toInternal()
										+ insprm97.toInternal()
										+ insprm98.toInternal()
										+ insprm99.toInternal());
	}
	public void setInsprms(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getInsprms().getLength()).init(obj);
	
		what = ExternalData.chop(what, insprm01);
		what = ExternalData.chop(what, insprm02);
		what = ExternalData.chop(what, insprm03);
		what = ExternalData.chop(what, insprm04);
		what = ExternalData.chop(what, insprm05);
		what = ExternalData.chop(what, insprm06);
		what = ExternalData.chop(what, insprm07);
		what = ExternalData.chop(what, insprm08);
		what = ExternalData.chop(what, insprm09);
		what = ExternalData.chop(what, insprm10);
		what = ExternalData.chop(what, insprm11);
		what = ExternalData.chop(what, insprm12);
		what = ExternalData.chop(what, insprm13);
		what = ExternalData.chop(what, insprm14);
		what = ExternalData.chop(what, insprm15);
		what = ExternalData.chop(what, insprm16);
		what = ExternalData.chop(what, insprm17);
		what = ExternalData.chop(what, insprm18);
		what = ExternalData.chop(what, insprm19);
		what = ExternalData.chop(what, insprm20);
		what = ExternalData.chop(what, insprm21);
		what = ExternalData.chop(what, insprm22);
		what = ExternalData.chop(what, insprm23);
		what = ExternalData.chop(what, insprm24);
		what = ExternalData.chop(what, insprm25);
		what = ExternalData.chop(what, insprm26);
		what = ExternalData.chop(what, insprm27);
		what = ExternalData.chop(what, insprm28);
		what = ExternalData.chop(what, insprm29);
		what = ExternalData.chop(what, insprm30);
		what = ExternalData.chop(what, insprm31);
		what = ExternalData.chop(what, insprm32);
		what = ExternalData.chop(what, insprm33);
		what = ExternalData.chop(what, insprm34);
		what = ExternalData.chop(what, insprm35);
		what = ExternalData.chop(what, insprm36);
		what = ExternalData.chop(what, insprm37);
		what = ExternalData.chop(what, insprm38);
		what = ExternalData.chop(what, insprm39);
		what = ExternalData.chop(what, insprm40);
		what = ExternalData.chop(what, insprm41);
		what = ExternalData.chop(what, insprm42);
		what = ExternalData.chop(what, insprm43);
		what = ExternalData.chop(what, insprm44);
		what = ExternalData.chop(what, insprm45);
		what = ExternalData.chop(what, insprm46);
		what = ExternalData.chop(what, insprm47);
		what = ExternalData.chop(what, insprm48);
		what = ExternalData.chop(what, insprm49);
		what = ExternalData.chop(what, insprm50);
		what = ExternalData.chop(what, insprm51);
		what = ExternalData.chop(what, insprm52);
		what = ExternalData.chop(what, insprm53);
		what = ExternalData.chop(what, insprm54);
		what = ExternalData.chop(what, insprm55);
		what = ExternalData.chop(what, insprm56);
		what = ExternalData.chop(what, insprm57);
		what = ExternalData.chop(what, insprm58);
		what = ExternalData.chop(what, insprm59);
		what = ExternalData.chop(what, insprm60);
		what = ExternalData.chop(what, insprm61);
		what = ExternalData.chop(what, insprm62);
		what = ExternalData.chop(what, insprm63);
		what = ExternalData.chop(what, insprm64);
		what = ExternalData.chop(what, insprm65);
		what = ExternalData.chop(what, insprm66);
		what = ExternalData.chop(what, insprm67);
		what = ExternalData.chop(what, insprm68);
		what = ExternalData.chop(what, insprm69);
		what = ExternalData.chop(what, insprm70);
		what = ExternalData.chop(what, insprm71);
		what = ExternalData.chop(what, insprm72);
		what = ExternalData.chop(what, insprm73);
		what = ExternalData.chop(what, insprm74);
		what = ExternalData.chop(what, insprm75);
		what = ExternalData.chop(what, insprm76);
		what = ExternalData.chop(what, insprm77);
		what = ExternalData.chop(what, insprm78);
		what = ExternalData.chop(what, insprm79);
		what = ExternalData.chop(what, insprm80);
		what = ExternalData.chop(what, insprm81);
		what = ExternalData.chop(what, insprm82);
		what = ExternalData.chop(what, insprm83);
		what = ExternalData.chop(what, insprm84);
		what = ExternalData.chop(what, insprm85);
		what = ExternalData.chop(what, insprm86);
		what = ExternalData.chop(what, insprm87);
		what = ExternalData.chop(what, insprm88);
		what = ExternalData.chop(what, insprm89);
		what = ExternalData.chop(what, insprm90);
		what = ExternalData.chop(what, insprm91);
		what = ExternalData.chop(what, insprm92);
		what = ExternalData.chop(what, insprm93);
		what = ExternalData.chop(what, insprm94);
		what = ExternalData.chop(what, insprm95);
		what = ExternalData.chop(what, insprm96);
		what = ExternalData.chop(what, insprm97);
		what = ExternalData.chop(what, insprm98);
		what = ExternalData.chop(what, insprm99);
	}
	public PackedDecimalData getInsprm(BaseData indx) {
		return getInsprm(indx.toInt());
	}
	public PackedDecimalData getInsprm(int indx) {

		switch (indx) {
			case 1 : return insprm01;
			case 2 : return insprm02;
			case 3 : return insprm03;
			case 4 : return insprm04;
			case 5 : return insprm05;
			case 6 : return insprm06;
			case 7 : return insprm07;
			case 8 : return insprm08;
			case 9 : return insprm09;
			case 10 : return insprm10;
			case 11 : return insprm11;
			case 12 : return insprm12;
			case 13 : return insprm13;
			case 14 : return insprm14;
			case 15 : return insprm15;
			case 16 : return insprm16;
			case 17 : return insprm17;
			case 18 : return insprm18;
			case 19 : return insprm19;
			case 20 : return insprm20;
			case 21 : return insprm21;
			case 22 : return insprm22;
			case 23 : return insprm23;
			case 24 : return insprm24;
			case 25 : return insprm25;
			case 26 : return insprm26;
			case 27 : return insprm27;
			case 28 : return insprm28;
			case 29 : return insprm29;
			case 30 : return insprm30;
			case 31 : return insprm31;
			case 32 : return insprm32;
			case 33 : return insprm33;
			case 34 : return insprm34;
			case 35 : return insprm35;
			case 36 : return insprm36;
			case 37 : return insprm37;
			case 38 : return insprm38;
			case 39 : return insprm39;
			case 40 : return insprm40;
			case 41 : return insprm41;
			case 42 : return insprm42;
			case 43 : return insprm43;
			case 44 : return insprm44;
			case 45 : return insprm45;
			case 46 : return insprm46;
			case 47 : return insprm47;
			case 48 : return insprm48;
			case 49 : return insprm49;
			case 50 : return insprm50;
			case 51 : return insprm51;
			case 52 : return insprm52;
			case 53 : return insprm53;
			case 54 : return insprm54;
			case 55 : return insprm55;
			case 56 : return insprm56;
			case 57 : return insprm57;
			case 58 : return insprm58;
			case 59 : return insprm59;
			case 60 : return insprm60;
			case 61 : return insprm61;
			case 62 : return insprm62;
			case 63 : return insprm63;
			case 64 : return insprm64;
			case 65 : return insprm65;
			case 66 : return insprm66;
			case 67 : return insprm67;
			case 68 : return insprm68;
			case 69 : return insprm69;
			case 70 : return insprm70;
			case 71 : return insprm71;
			case 72 : return insprm72;
			case 73 : return insprm73;
			case 74 : return insprm74;
			case 75 : return insprm75;
			case 76 : return insprm76;
			case 77 : return insprm77;
			case 78 : return insprm78;
			case 79 : return insprm79;
			case 80 : return insprm80;
			case 81 : return insprm81;
			case 82 : return insprm82;
			case 83 : return insprm83;
			case 84 : return insprm84;
			case 85 : return insprm85;
			case 86 : return insprm86;
			case 87 : return insprm87;
			case 88 : return insprm88;
			case 89 : return insprm89;
			case 90 : return insprm90;
			case 91 : return insprm91;
			case 92 : return insprm92;
			case 93 : return insprm93;
			case 94 : return insprm94;
			case 95 : return insprm95;
			case 96 : return insprm96;
			case 97 : return insprm97;
			case 98 : return insprm98;
			case 99 : return insprm99;
			default: return null; // Throw error instead?
		}
	
	}
	public void setInsprm(BaseData indx, Object what) {
		setInsprm(indx, what, false);
	}
	public void setInsprm(BaseData indx, Object what, boolean rounded) {
		setInsprm(indx.toInt(), what, rounded);
	}
	public void setInsprm(int indx, Object what) {
		setInsprm(indx, what, false);
	}
	public void setInsprm(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInsprm01(what, rounded);
					 break;
			case 2 : setInsprm02(what, rounded);
					 break;
			case 3 : setInsprm03(what, rounded);
					 break;
			case 4 : setInsprm04(what, rounded);
					 break;
			case 5 : setInsprm05(what, rounded);
					 break;
			case 6 : setInsprm06(what, rounded);
					 break;
			case 7 : setInsprm07(what, rounded);
					 break;
			case 8 : setInsprm08(what, rounded);
					 break;
			case 9 : setInsprm09(what, rounded);
					 break;
			case 10 : setInsprm10(what, rounded);
					 break;
			case 11 : setInsprm11(what, rounded);
					 break;
			case 12 : setInsprm12(what, rounded);
					 break;
			case 13 : setInsprm13(what, rounded);
					 break;
			case 14 : setInsprm14(what, rounded);
					 break;
			case 15 : setInsprm15(what, rounded);
					 break;
			case 16 : setInsprm16(what, rounded);
					 break;
			case 17 : setInsprm17(what, rounded);
					 break;
			case 18 : setInsprm18(what, rounded);
					 break;
			case 19 : setInsprm19(what, rounded);
					 break;
			case 20 : setInsprm20(what, rounded);
					 break;
			case 21 : setInsprm21(what, rounded);
					 break;
			case 22 : setInsprm22(what, rounded);
					 break;
			case 23 : setInsprm23(what, rounded);
					 break;
			case 24 : setInsprm24(what, rounded);
					 break;
			case 25 : setInsprm25(what, rounded);
					 break;
			case 26 : setInsprm26(what, rounded);
					 break;
			case 27 : setInsprm27(what, rounded);
					 break;
			case 28 : setInsprm28(what, rounded);
					 break;
			case 29 : setInsprm29(what, rounded);
					 break;
			case 30 : setInsprm30(what, rounded);
					 break;
			case 31 : setInsprm31(what, rounded);
					 break;
			case 32 : setInsprm32(what, rounded);
					 break;
			case 33 : setInsprm33(what, rounded);
					 break;
			case 34 : setInsprm34(what, rounded);
					 break;
			case 35 : setInsprm35(what, rounded);
					 break;
			case 36 : setInsprm36(what, rounded);
					 break;
			case 37 : setInsprm37(what, rounded);
					 break;
			case 38 : setInsprm38(what, rounded);
					 break;
			case 39 : setInsprm39(what, rounded);
					 break;
			case 40 : setInsprm40(what, rounded);
					 break;
			case 41 : setInsprm41(what, rounded);
					 break;
			case 42 : setInsprm42(what, rounded);
					 break;
			case 43 : setInsprm43(what, rounded);
					 break;
			case 44 : setInsprm44(what, rounded);
					 break;
			case 45 : setInsprm45(what, rounded);
					 break;
			case 46 : setInsprm46(what, rounded);
					 break;
			case 47 : setInsprm47(what, rounded);
					 break;
			case 48 : setInsprm48(what, rounded);
					 break;
			case 49 : setInsprm49(what, rounded);
					 break;
			case 50 : setInsprm50(what, rounded);
					 break;
			case 51 : setInsprm51(what, rounded);
					 break;
			case 52 : setInsprm52(what, rounded);
					 break;
			case 53 : setInsprm53(what, rounded);
					 break;
			case 54 : setInsprm54(what, rounded);
					 break;
			case 55 : setInsprm55(what, rounded);
					 break;
			case 56 : setInsprm56(what, rounded);
					 break;
			case 57 : setInsprm57(what, rounded);
					 break;
			case 58 : setInsprm58(what, rounded);
					 break;
			case 59 : setInsprm59(what, rounded);
					 break;
			case 60 : setInsprm60(what, rounded);
					 break;
			case 61 : setInsprm61(what, rounded);
					 break;
			case 62 : setInsprm62(what, rounded);
					 break;
			case 63 : setInsprm63(what, rounded);
					 break;
			case 64 : setInsprm64(what, rounded);
					 break;
			case 65 : setInsprm65(what, rounded);
					 break;
			case 66 : setInsprm66(what, rounded);
					 break;
			case 67 : setInsprm67(what, rounded);
					 break;
			case 68 : setInsprm68(what, rounded);
					 break;
			case 69 : setInsprm69(what, rounded);
					 break;
			case 70 : setInsprm70(what, rounded);
					 break;
			case 71 : setInsprm71(what, rounded);
					 break;
			case 72 : setInsprm72(what, rounded);
					 break;
			case 73 : setInsprm73(what, rounded);
					 break;
			case 74 : setInsprm74(what, rounded);
					 break;
			case 75 : setInsprm75(what, rounded);
					 break;
			case 76 : setInsprm76(what, rounded);
					 break;
			case 77 : setInsprm77(what, rounded);
					 break;
			case 78 : setInsprm78(what, rounded);
					 break;
			case 79 : setInsprm79(what, rounded);
					 break;
			case 80 : setInsprm80(what, rounded);
					 break;
			case 81 : setInsprm81(what, rounded);
					 break;
			case 82 : setInsprm82(what, rounded);
					 break;
			case 83 : setInsprm83(what, rounded);
					 break;
			case 84 : setInsprm84(what, rounded);
					 break;
			case 85 : setInsprm85(what, rounded);
					 break;
			case 86 : setInsprm86(what, rounded);
					 break;
			case 87 : setInsprm87(what, rounded);
					 break;
			case 88 : setInsprm88(what, rounded);
					 break;
			case 89 : setInsprm89(what, rounded);
					 break;
			case 90 : setInsprm90(what, rounded);
					 break;
			case 91 : setInsprm91(what, rounded);
					 break;
			case 92 : setInsprm92(what, rounded);
					 break;
			case 93 : setInsprm93(what, rounded);
					 break;
			case 94 : setInsprm94(what, rounded);
					 break;
			case 95 : setInsprm95(what, rounded);
					 break;
			case 96 : setInsprm96(what, rounded);
					 break;
			case 97 : setInsprm97(what, rounded);
					 break;
			case 98 : setInsprm98(what, rounded);
					 break;
			case 99 : setInsprm99(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		itemitem.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		mfacthm.clear();
		mfacthy.clear();
		mfactm.clear();
		mfactq.clear();
		mfactw.clear();
		mfact2w.clear();
		mfact4w.clear();
		mfacty.clear();
		premUnit.clear();
		unit.clear();
		disccntmeth.clear();
		insprm01.clear();
		insprm02.clear();
		insprm03.clear();
		insprm04.clear();
		insprm05.clear();
		insprm06.clear();
		insprm07.clear();
		insprm08.clear();
		insprm09.clear();
		insprm10.clear();
		insprm11.clear();
		insprm12.clear();
		insprm13.clear();
		insprm14.clear();
		insprm15.clear();
		insprm16.clear();
		insprm17.clear();
		insprm18.clear();
		insprm19.clear();
		insprm20.clear();
		insprm21.clear();
		insprm22.clear();
		insprm23.clear();
		insprm24.clear();
		insprm25.clear();
		insprm26.clear();
		insprm27.clear();
		insprm28.clear();
		insprm29.clear();
		insprm30.clear();
		insprm31.clear();
		insprm32.clear();
		insprm33.clear();
		insprm34.clear();
		insprm35.clear();
		insprm36.clear();
		insprm37.clear();
		insprm38.clear();
		insprm39.clear();
		insprm40.clear();
		insprm41.clear();
		insprm42.clear();
		insprm43.clear();
		insprm44.clear();
		insprm45.clear();
		insprm46.clear();
		insprm47.clear();
		insprm48.clear();
		insprm49.clear();
		insprm50.clear();
		insprm51.clear();
		insprm52.clear();
		insprm53.clear();
		insprm54.clear();
		insprm55.clear();
		insprm56.clear();
		insprm57.clear();
		insprm58.clear();
		insprm59.clear();
		insprm60.clear();
		insprm61.clear();
		insprm62.clear();
		insprm63.clear();
		insprm64.clear();
		insprm65.clear();
		insprm66.clear();
		insprm67.clear();
		insprm68.clear();
		insprm69.clear();
		insprm70.clear();
		insprm71.clear();
		insprm72.clear();
		insprm73.clear();
		insprm74.clear();
		insprm75.clear();
		insprm76.clear();
		insprm77.clear();
		insprm78.clear();
		insprm79.clear();
		insprm80.clear();
		insprm81.clear();
		insprm82.clear();
		insprm83.clear();
		insprm84.clear();
		insprm85.clear();
		insprm86.clear();
		insprm87.clear();
		insprm88.clear();
		insprm89.clear();
		insprm90.clear();
		insprm91.clear();
		insprm92.clear();
		insprm93.clear();
		insprm94.clear();
		insprm95.clear();
		insprm96.clear();
		insprm97.clear();
		insprm98.clear();
		insprm99.clear();
		insprem.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();
		instpr01.clear();
		instpr02.clear();
		instpr03.clear();
		instpr04.clear();
		instpr05.clear();
		instpr06.clear();
		instpr07.clear();
		instpr08.clear();
		instpr09.clear();
		instpr10.clear();
		instpr11.clear();		
	}


}