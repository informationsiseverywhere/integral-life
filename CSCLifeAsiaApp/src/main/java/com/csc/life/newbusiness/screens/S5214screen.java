package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5214screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 24, 15, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 18, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5214ScreenVars sv = (S5214ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5214screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5214ScreenVars screenVars = (S5214ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.descrip.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.payrnum.setClassString("");
		screenVars.agntnum.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.ctypdesc.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.payorname.setClassString("");
		screenVars.agentname.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.effdateDisp.setClassString("");
	}

/**
 * Clear all the variables in S5214screen
 */
	public static void clear(VarModel pv) {
		S5214ScreenVars screenVars = (S5214ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.descrip.clear();
		screenVars.chdrnum.clear();
		screenVars.rstate.clear();
		screenVars.cnttype.clear();
		screenVars.cownnum.clear();
		screenVars.payrnum.clear();
		screenVars.agntnum.clear();
		screenVars.billfreq.clear();
		screenVars.ctypdesc.clear();
		screenVars.pstate.clear();
		screenVars.ownername.clear();
		screenVars.payorname.clear();
		screenVars.agentname.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.mop.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
	}
}
