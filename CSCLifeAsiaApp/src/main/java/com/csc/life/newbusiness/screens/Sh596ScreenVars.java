package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH596
 * @version 1.0 generated on 30/08/09 07:05
 * @author Quipoz
 */
public class Sh596ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(2118);
	public FixedLengthStringData dataFields = new FixedLengthStringData(422).isAPartOf(dataArea, 0);
	public FixedLengthStringData cnRiskStats = new FixedLengthStringData(24).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] cnRiskStat = FLSArrayPartOfStructure(12, 2, cnRiskStats, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(cnRiskStats, 0, FILLER_REDEFINE);
	public FixedLengthStringData cnRiskStat01 = DD.cnrstat.copy().isAPartOf(filler,0);
	public FixedLengthStringData cnRiskStat02 = DD.cnrstat.copy().isAPartOf(filler,2);
	public FixedLengthStringData cnRiskStat03 = DD.cnrstat.copy().isAPartOf(filler,4);
	public FixedLengthStringData cnRiskStat04 = DD.cnrstat.copy().isAPartOf(filler,6);
	public FixedLengthStringData cnRiskStat05 = DD.cnrstat.copy().isAPartOf(filler,8);
	public FixedLengthStringData cnRiskStat06 = DD.cnrstat.copy().isAPartOf(filler,10);
	public FixedLengthStringData cnRiskStat07 = DD.cnrstat.copy().isAPartOf(filler,12);
	public FixedLengthStringData cnRiskStat08 = DD.cnrstat.copy().isAPartOf(filler,14);
	public FixedLengthStringData cnRiskStat09 = DD.cnrstat.copy().isAPartOf(filler,16);
	public FixedLengthStringData cnRiskStat10 = DD.cnrstat.copy().isAPartOf(filler,18);
	public FixedLengthStringData cnRiskStat11 = DD.cnrstat.copy().isAPartOf(filler,20);
	public FixedLengthStringData cnRiskStat12 = DD.cnrstat.copy().isAPartOf(filler,22);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData fupcdess = new FixedLengthStringData(240).isAPartOf(dataFields, 25);
	public FixedLengthStringData[] fupcdes = FLSArrayPartOfStructure(60, 4, fupcdess, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(240).isAPartOf(fupcdess, 0, FILLER_REDEFINE);
	public FixedLengthStringData fupcdes01 = DD.fupcdes.copy().isAPartOf(filler1,0);
	public FixedLengthStringData fupcdes02 = DD.fupcdes.copy().isAPartOf(filler1,4);
	public FixedLengthStringData fupcdes03 = DD.fupcdes.copy().isAPartOf(filler1,8);
	public FixedLengthStringData fupcdes04 = DD.fupcdes.copy().isAPartOf(filler1,12);
	public FixedLengthStringData fupcdes05 = DD.fupcdes.copy().isAPartOf(filler1,16);
	public FixedLengthStringData fupcdes06 = DD.fupcdes.copy().isAPartOf(filler1,20);
	public FixedLengthStringData fupcdes07 = DD.fupcdes.copy().isAPartOf(filler1,24);
	public FixedLengthStringData fupcdes08 = DD.fupcdes.copy().isAPartOf(filler1,28);
	public FixedLengthStringData fupcdes09 = DD.fupcdes.copy().isAPartOf(filler1,32);
	public FixedLengthStringData fupcdes10 = DD.fupcdes.copy().isAPartOf(filler1,36);
	public FixedLengthStringData fupcdes11 = DD.fupcdes.copy().isAPartOf(filler1,40);
	public FixedLengthStringData fupcdes12 = DD.fupcdes.copy().isAPartOf(filler1,44);
	public FixedLengthStringData fupcdes13 = DD.fupcdes.copy().isAPartOf(filler1,48);
	public FixedLengthStringData fupcdes14 = DD.fupcdes.copy().isAPartOf(filler1,52);
	public FixedLengthStringData fupcdes15 = DD.fupcdes.copy().isAPartOf(filler1,56);
	public FixedLengthStringData fupcdes16 = DD.fupcdes.copy().isAPartOf(filler1,60);
	public FixedLengthStringData fupcdes17 = DD.fupcdes.copy().isAPartOf(filler1,64);
	public FixedLengthStringData fupcdes18 = DD.fupcdes.copy().isAPartOf(filler1,68);
	public FixedLengthStringData fupcdes19 = DD.fupcdes.copy().isAPartOf(filler1,72);
	public FixedLengthStringData fupcdes20 = DD.fupcdes.copy().isAPartOf(filler1,76);
	public FixedLengthStringData fupcdes21 = DD.fupcdes.copy().isAPartOf(filler1,80);
	public FixedLengthStringData fupcdes22 = DD.fupcdes.copy().isAPartOf(filler1,84);
	public FixedLengthStringData fupcdes23 = DD.fupcdes.copy().isAPartOf(filler1,88);
	public FixedLengthStringData fupcdes24 = DD.fupcdes.copy().isAPartOf(filler1,92);
	public FixedLengthStringData fupcdes25 = DD.fupcdes.copy().isAPartOf(filler1,96);
	public FixedLengthStringData fupcdes26 = DD.fupcdes.copy().isAPartOf(filler1,100);
	public FixedLengthStringData fupcdes27 = DD.fupcdes.copy().isAPartOf(filler1,104);
	public FixedLengthStringData fupcdes28 = DD.fupcdes.copy().isAPartOf(filler1,108);
	public FixedLengthStringData fupcdes29 = DD.fupcdes.copy().isAPartOf(filler1,112);
	public FixedLengthStringData fupcdes30 = DD.fupcdes.copy().isAPartOf(filler1,116);
	public FixedLengthStringData fupcdes31 = DD.fupcdes.copy().isAPartOf(filler1,120);
	public FixedLengthStringData fupcdes32 = DD.fupcdes.copy().isAPartOf(filler1,124);
	public FixedLengthStringData fupcdes33 = DD.fupcdes.copy().isAPartOf(filler1,128);
	public FixedLengthStringData fupcdes34 = DD.fupcdes.copy().isAPartOf(filler1,132);
	public FixedLengthStringData fupcdes35 = DD.fupcdes.copy().isAPartOf(filler1,136);
	public FixedLengthStringData fupcdes36 = DD.fupcdes.copy().isAPartOf(filler1,140);
	public FixedLengthStringData fupcdes37 = DD.fupcdes.copy().isAPartOf(filler1,144);
	public FixedLengthStringData fupcdes38 = DD.fupcdes.copy().isAPartOf(filler1,148);
	public FixedLengthStringData fupcdes39 = DD.fupcdes.copy().isAPartOf(filler1,152);
	public FixedLengthStringData fupcdes40 = DD.fupcdes.copy().isAPartOf(filler1,156);
	public FixedLengthStringData fupcdes41 = DD.fupcdes.copy().isAPartOf(filler1,160);
	public FixedLengthStringData fupcdes42 = DD.fupcdes.copy().isAPartOf(filler1,164);
	public FixedLengthStringData fupcdes43 = DD.fupcdes.copy().isAPartOf(filler1,168);
	public FixedLengthStringData fupcdes44 = DD.fupcdes.copy().isAPartOf(filler1,172);
	public FixedLengthStringData fupcdes45 = DD.fupcdes.copy().isAPartOf(filler1,176);
	public FixedLengthStringData fupcdes46 = DD.fupcdes.copy().isAPartOf(filler1,180);
	public FixedLengthStringData fupcdes47 = DD.fupcdes.copy().isAPartOf(filler1,184);
	public FixedLengthStringData fupcdes48 = DD.fupcdes.copy().isAPartOf(filler1,188);
	public FixedLengthStringData fupcdes49 = DD.fupcdes.copy().isAPartOf(filler1,192);
	public FixedLengthStringData fupcdes50 = DD.fupcdes.copy().isAPartOf(filler1,196);
	public FixedLengthStringData fupcdes51 = DD.fupcdes.copy().isAPartOf(filler1,200);
	public FixedLengthStringData fupcdes52 = DD.fupcdes.copy().isAPartOf(filler1,204);
	public FixedLengthStringData fupcdes53 = DD.fupcdes.copy().isAPartOf(filler1,208);
	public FixedLengthStringData fupcdes54 = DD.fupcdes.copy().isAPartOf(filler1,212);
	public FixedLengthStringData fupcdes55 = DD.fupcdes.copy().isAPartOf(filler1,216);
	public FixedLengthStringData fupcdes56 = DD.fupcdes.copy().isAPartOf(filler1,220);
	public FixedLengthStringData fupcdes57 = DD.fupcdes.copy().isAPartOf(filler1,224);
	public FixedLengthStringData fupcdes58 = DD.fupcdes.copy().isAPartOf(filler1,228);
	public FixedLengthStringData fupcdes59 = DD.fupcdes.copy().isAPartOf(filler1,232);
	public FixedLengthStringData fupcdes60 = DD.fupcdes.copy().isAPartOf(filler1,236);
	public FixedLengthStringData hcondate = DD.hcondate.copy().isAPartOf(dataFields,265);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,266);
	public FixedLengthStringData language = DD.language.copy().isAPartOf(dataFields,274);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,275);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,305);
	public FixedLengthStringData trcodes = new FixedLengthStringData(112).isAPartOf(dataFields, 310);
	public FixedLengthStringData[] trcode = FLSArrayPartOfStructure(28, 4, trcodes, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(112).isAPartOf(trcodes, 0, FILLER_REDEFINE);
	public FixedLengthStringData trcode01 = DD.trcode.copy().isAPartOf(filler2,0);
	public FixedLengthStringData trcode02 = DD.trcode.copy().isAPartOf(filler2,4);
	public FixedLengthStringData trcode03 = DD.trcode.copy().isAPartOf(filler2,8);
	public FixedLengthStringData trcode04 = DD.trcode.copy().isAPartOf(filler2,12);
	public FixedLengthStringData trcode05 = DD.trcode.copy().isAPartOf(filler2,16);
	public FixedLengthStringData trcode06 = DD.trcode.copy().isAPartOf(filler2,20);
	public FixedLengthStringData trcode07 = DD.trcode.copy().isAPartOf(filler2,24);
	public FixedLengthStringData trcode08 = DD.trcode.copy().isAPartOf(filler2,28);
	public FixedLengthStringData trcode09 = DD.trcode.copy().isAPartOf(filler2,32);
	public FixedLengthStringData trcode10 = DD.trcode.copy().isAPartOf(filler2,36);
	public FixedLengthStringData trcode11 = DD.trcode.copy().isAPartOf(filler2,40);
	public FixedLengthStringData trcode12 = DD.trcode.copy().isAPartOf(filler2,44);
	public FixedLengthStringData trcode13 = DD.trcode.copy().isAPartOf(filler2,48);
	public FixedLengthStringData trcode14 = DD.trcode.copy().isAPartOf(filler2,52);
	public FixedLengthStringData trcode15 = DD.trcode.copy().isAPartOf(filler2,56);
	public FixedLengthStringData trcode16 = DD.trcode.copy().isAPartOf(filler2,60);
	public FixedLengthStringData trcode17 = DD.trcode.copy().isAPartOf(filler2,64);
	public FixedLengthStringData trcode18 = DD.trcode.copy().isAPartOf(filler2,68);
	public FixedLengthStringData trcode19 = DD.trcode.copy().isAPartOf(filler2,72);
	public FixedLengthStringData trcode20 = DD.trcode.copy().isAPartOf(filler2,76);
	public FixedLengthStringData trcode21 = DD.trcode.copy().isAPartOf(filler2,80);
	public FixedLengthStringData trcode22 = DD.trcode.copy().isAPartOf(filler2,84);
	public FixedLengthStringData trcode23 = DD.trcode.copy().isAPartOf(filler2,88);
	public FixedLengthStringData trcode24 = DD.trcode.copy().isAPartOf(filler2,92);
	public FixedLengthStringData trcode25 = DD.trcode.copy().isAPartOf(filler2,96);
	public FixedLengthStringData trcode26 = DD.trcode.copy().isAPartOf(filler2,100);
	public FixedLengthStringData trcode27 = DD.trcode.copy().isAPartOf(filler2,104);
	public FixedLengthStringData trcode28 = DD.trcode.copy().isAPartOf(filler2,108);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(424).isAPartOf(dataArea, 422);
	public FixedLengthStringData cnrstatsErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] cnrstatErr = FLSArrayPartOfStructure(12, 4, cnrstatsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(48).isAPartOf(cnrstatsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData cnrstat01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData cnrstat02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData cnrstat03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData cnrstat04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData cnrstat05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData cnrstat06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData cnrstat07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData cnrstat08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData cnrstat09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData cnrstat10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData cnrstat11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData cnrstat12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData fupcdessErr = new FixedLengthStringData(240).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData[] fupcdesErr = FLSArrayPartOfStructure(60, 4, fupcdessErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(240).isAPartOf(fupcdessErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData fupcdes01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData fupcdes02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData fupcdes03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData fupcdes04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData fupcdes05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData fupcdes06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData fupcdes07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData fupcdes08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData fupcdes09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData fupcdes10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData fupcdes11Err = new FixedLengthStringData(4).isAPartOf(filler4, 40);
	public FixedLengthStringData fupcdes12Err = new FixedLengthStringData(4).isAPartOf(filler4, 44);
	public FixedLengthStringData fupcdes13Err = new FixedLengthStringData(4).isAPartOf(filler4, 48);
	public FixedLengthStringData fupcdes14Err = new FixedLengthStringData(4).isAPartOf(filler4, 52);
	public FixedLengthStringData fupcdes15Err = new FixedLengthStringData(4).isAPartOf(filler4, 56);
	public FixedLengthStringData fupcdes16Err = new FixedLengthStringData(4).isAPartOf(filler4, 60);
	public FixedLengthStringData fupcdes17Err = new FixedLengthStringData(4).isAPartOf(filler4, 64);
	public FixedLengthStringData fupcdes18Err = new FixedLengthStringData(4).isAPartOf(filler4, 68);
	public FixedLengthStringData fupcdes19Err = new FixedLengthStringData(4).isAPartOf(filler4, 72);
	public FixedLengthStringData fupcdes20Err = new FixedLengthStringData(4).isAPartOf(filler4, 76);
	public FixedLengthStringData fupcdes21Err = new FixedLengthStringData(4).isAPartOf(filler4, 80);
	public FixedLengthStringData fupcdes22Err = new FixedLengthStringData(4).isAPartOf(filler4, 84);
	public FixedLengthStringData fupcdes23Err = new FixedLengthStringData(4).isAPartOf(filler4, 88);
	public FixedLengthStringData fupcdes24Err = new FixedLengthStringData(4).isAPartOf(filler4, 92);
	public FixedLengthStringData fupcdes25Err = new FixedLengthStringData(4).isAPartOf(filler4, 96);
	public FixedLengthStringData fupcdes26Err = new FixedLengthStringData(4).isAPartOf(filler4, 100);
	public FixedLengthStringData fupcdes27Err = new FixedLengthStringData(4).isAPartOf(filler4, 104);
	public FixedLengthStringData fupcdes28Err = new FixedLengthStringData(4).isAPartOf(filler4, 108);
	public FixedLengthStringData fupcdes29Err = new FixedLengthStringData(4).isAPartOf(filler4, 112);
	public FixedLengthStringData fupcdes30Err = new FixedLengthStringData(4).isAPartOf(filler4, 116);
	public FixedLengthStringData fupcdes31Err = new FixedLengthStringData(4).isAPartOf(filler4, 120);
	public FixedLengthStringData fupcdes32Err = new FixedLengthStringData(4).isAPartOf(filler4, 124);
	public FixedLengthStringData fupcdes33Err = new FixedLengthStringData(4).isAPartOf(filler4, 128);
	public FixedLengthStringData fupcdes34Err = new FixedLengthStringData(4).isAPartOf(filler4, 132);
	public FixedLengthStringData fupcdes35Err = new FixedLengthStringData(4).isAPartOf(filler4, 136);
	public FixedLengthStringData fupcdes36Err = new FixedLengthStringData(4).isAPartOf(filler4, 140);
	public FixedLengthStringData fupcdes37Err = new FixedLengthStringData(4).isAPartOf(filler4, 144);
	public FixedLengthStringData fupcdes38Err = new FixedLengthStringData(4).isAPartOf(filler4, 148);
	public FixedLengthStringData fupcdes39Err = new FixedLengthStringData(4).isAPartOf(filler4, 152);
	public FixedLengthStringData fupcdes40Err = new FixedLengthStringData(4).isAPartOf(filler4, 156);
	public FixedLengthStringData fupcdes41Err = new FixedLengthStringData(4).isAPartOf(filler4, 160);
	public FixedLengthStringData fupcdes42Err = new FixedLengthStringData(4).isAPartOf(filler4, 164);
	public FixedLengthStringData fupcdes43Err = new FixedLengthStringData(4).isAPartOf(filler4, 168);
	public FixedLengthStringData fupcdes44Err = new FixedLengthStringData(4).isAPartOf(filler4, 172);
	public FixedLengthStringData fupcdes45Err = new FixedLengthStringData(4).isAPartOf(filler4, 176);
	public FixedLengthStringData fupcdes46Err = new FixedLengthStringData(4).isAPartOf(filler4, 180);
	public FixedLengthStringData fupcdes47Err = new FixedLengthStringData(4).isAPartOf(filler4, 184);
	public FixedLengthStringData fupcdes48Err = new FixedLengthStringData(4).isAPartOf(filler4, 188);
	public FixedLengthStringData fupcdes49Err = new FixedLengthStringData(4).isAPartOf(filler4, 192);
	public FixedLengthStringData fupcdes50Err = new FixedLengthStringData(4).isAPartOf(filler4, 196);
	public FixedLengthStringData fupcdes51Err = new FixedLengthStringData(4).isAPartOf(filler4, 200);
	public FixedLengthStringData fupcdes52Err = new FixedLengthStringData(4).isAPartOf(filler4, 204);
	public FixedLengthStringData fupcdes53Err = new FixedLengthStringData(4).isAPartOf(filler4, 208);
	public FixedLengthStringData fupcdes54Err = new FixedLengthStringData(4).isAPartOf(filler4, 212);
	public FixedLengthStringData fupcdes55Err = new FixedLengthStringData(4).isAPartOf(filler4, 216);
	public FixedLengthStringData fupcdes56Err = new FixedLengthStringData(4).isAPartOf(filler4, 220);
	public FixedLengthStringData fupcdes57Err = new FixedLengthStringData(4).isAPartOf(filler4, 224);
	public FixedLengthStringData fupcdes58Err = new FixedLengthStringData(4).isAPartOf(filler4, 228);
	public FixedLengthStringData fupcdes59Err = new FixedLengthStringData(4).isAPartOf(filler4, 232);
	public FixedLengthStringData fupcdes60Err = new FixedLengthStringData(4).isAPartOf(filler4, 236);
	public FixedLengthStringData hcondateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 292);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 296);
	public FixedLengthStringData languageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 300);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 304);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 308);
	public FixedLengthStringData trcodesErr = new FixedLengthStringData(112).isAPartOf(errorIndicators, 312);
	public FixedLengthStringData[] trcodeErr = FLSArrayPartOfStructure(28, 4, trcodesErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(112).isAPartOf(trcodesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData trcode01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData trcode02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData trcode03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData trcode04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData trcode05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData trcode06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData trcode07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData trcode08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData trcode09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData trcode10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData trcode11Err = new FixedLengthStringData(4).isAPartOf(filler5, 40);
	public FixedLengthStringData trcode12Err = new FixedLengthStringData(4).isAPartOf(filler5, 44);
	public FixedLengthStringData trcode13Err = new FixedLengthStringData(4).isAPartOf(filler5, 48);
	public FixedLengthStringData trcode14Err = new FixedLengthStringData(4).isAPartOf(filler5, 52);
	public FixedLengthStringData trcode15Err = new FixedLengthStringData(4).isAPartOf(filler5, 56);
	public FixedLengthStringData trcode16Err = new FixedLengthStringData(4).isAPartOf(filler5, 60);
	public FixedLengthStringData trcode17Err = new FixedLengthStringData(4).isAPartOf(filler5, 64);
	public FixedLengthStringData trcode18Err = new FixedLengthStringData(4).isAPartOf(filler5, 68);
	public FixedLengthStringData trcode19Err = new FixedLengthStringData(4).isAPartOf(filler5, 72);
	public FixedLengthStringData trcode20Err = new FixedLengthStringData(4).isAPartOf(filler5, 76);
	public FixedLengthStringData trcode21Err = new FixedLengthStringData(4).isAPartOf(filler5, 80);
	public FixedLengthStringData trcode22Err = new FixedLengthStringData(4).isAPartOf(filler5, 84);
	public FixedLengthStringData trcode23Err = new FixedLengthStringData(4).isAPartOf(filler5, 88);
	public FixedLengthStringData trcode24Err = new FixedLengthStringData(4).isAPartOf(filler5, 92);
	public FixedLengthStringData trcode25Err = new FixedLengthStringData(4).isAPartOf(filler5, 96);
	public FixedLengthStringData trcode26Err = new FixedLengthStringData(4).isAPartOf(filler5, 100);
	public FixedLengthStringData trcode27Err = new FixedLengthStringData(4).isAPartOf(filler5, 104);
	public FixedLengthStringData trcode28Err = new FixedLengthStringData(4).isAPartOf(filler5, 108);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(1272).isAPartOf(dataArea, 846);
	public FixedLengthStringData cnrstatsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] cnrstatOut = FLSArrayPartOfStructure(12, 12, cnrstatsOut, 0);
	public FixedLengthStringData[][] cnrstatO = FLSDArrayPartOfArrayStructure(12, 1, cnrstatOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(144).isAPartOf(cnrstatsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] cnrstat01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] cnrstat02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] cnrstat03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] cnrstat04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] cnrstat05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] cnrstat06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] cnrstat07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);
	public FixedLengthStringData[] cnrstat08Out = FLSArrayPartOfStructure(12, 1, filler6, 84);
	public FixedLengthStringData[] cnrstat09Out = FLSArrayPartOfStructure(12, 1, filler6, 96);
	public FixedLengthStringData[] cnrstat10Out = FLSArrayPartOfStructure(12, 1, filler6, 108);
	public FixedLengthStringData[] cnrstat11Out = FLSArrayPartOfStructure(12, 1, filler6, 120);
	public FixedLengthStringData[] cnrstat12Out = FLSArrayPartOfStructure(12, 1, filler6, 132);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData fupcdessOut = new FixedLengthStringData(720).isAPartOf(outputIndicators, 156);
	public FixedLengthStringData[] fupcdesOut = FLSArrayPartOfStructure(60, 12, fupcdessOut, 0);
	public FixedLengthStringData[][] fupcdesO = FLSDArrayPartOfArrayStructure(12, 1, fupcdesOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(720).isAPartOf(fupcdessOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] fupcdes01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] fupcdes02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] fupcdes03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] fupcdes04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] fupcdes05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] fupcdes06Out = FLSArrayPartOfStructure(12, 1, filler7, 60);
	public FixedLengthStringData[] fupcdes07Out = FLSArrayPartOfStructure(12, 1, filler7, 72);
	public FixedLengthStringData[] fupcdes08Out = FLSArrayPartOfStructure(12, 1, filler7, 84);
	public FixedLengthStringData[] fupcdes09Out = FLSArrayPartOfStructure(12, 1, filler7, 96);
	public FixedLengthStringData[] fupcdes10Out = FLSArrayPartOfStructure(12, 1, filler7, 108);
	public FixedLengthStringData[] fupcdes11Out = FLSArrayPartOfStructure(12, 1, filler7, 120);
	public FixedLengthStringData[] fupcdes12Out = FLSArrayPartOfStructure(12, 1, filler7, 132);
	public FixedLengthStringData[] fupcdes13Out = FLSArrayPartOfStructure(12, 1, filler7, 144);
	public FixedLengthStringData[] fupcdes14Out = FLSArrayPartOfStructure(12, 1, filler7, 156);
	public FixedLengthStringData[] fupcdes15Out = FLSArrayPartOfStructure(12, 1, filler7, 168);
	public FixedLengthStringData[] fupcdes16Out = FLSArrayPartOfStructure(12, 1, filler7, 180);
	public FixedLengthStringData[] fupcdes17Out = FLSArrayPartOfStructure(12, 1, filler7, 192);
	public FixedLengthStringData[] fupcdes18Out = FLSArrayPartOfStructure(12, 1, filler7, 204);
	public FixedLengthStringData[] fupcdes19Out = FLSArrayPartOfStructure(12, 1, filler7, 216);
	public FixedLengthStringData[] fupcdes20Out = FLSArrayPartOfStructure(12, 1, filler7, 228);
	public FixedLengthStringData[] fupcdes21Out = FLSArrayPartOfStructure(12, 1, filler7, 240);
	public FixedLengthStringData[] fupcdes22Out = FLSArrayPartOfStructure(12, 1, filler7, 252);
	public FixedLengthStringData[] fupcdes23Out = FLSArrayPartOfStructure(12, 1, filler7, 264);
	public FixedLengthStringData[] fupcdes24Out = FLSArrayPartOfStructure(12, 1, filler7, 276);
	public FixedLengthStringData[] fupcdes25Out = FLSArrayPartOfStructure(12, 1, filler7, 288);
	public FixedLengthStringData[] fupcdes26Out = FLSArrayPartOfStructure(12, 1, filler7, 300);
	public FixedLengthStringData[] fupcdes27Out = FLSArrayPartOfStructure(12, 1, filler7, 312);
	public FixedLengthStringData[] fupcdes28Out = FLSArrayPartOfStructure(12, 1, filler7, 324);
	public FixedLengthStringData[] fupcdes29Out = FLSArrayPartOfStructure(12, 1, filler7, 336);
	public FixedLengthStringData[] fupcdes30Out = FLSArrayPartOfStructure(12, 1, filler7, 348);
	public FixedLengthStringData[] fupcdes31Out = FLSArrayPartOfStructure(12, 1, filler7, 360);
	public FixedLengthStringData[] fupcdes32Out = FLSArrayPartOfStructure(12, 1, filler7, 372);
	public FixedLengthStringData[] fupcdes33Out = FLSArrayPartOfStructure(12, 1, filler7, 384);
	public FixedLengthStringData[] fupcdes34Out = FLSArrayPartOfStructure(12, 1, filler7, 396);
	public FixedLengthStringData[] fupcdes35Out = FLSArrayPartOfStructure(12, 1, filler7, 408);
	public FixedLengthStringData[] fupcdes36Out = FLSArrayPartOfStructure(12, 1, filler7, 420);
	public FixedLengthStringData[] fupcdes37Out = FLSArrayPartOfStructure(12, 1, filler7, 432);
	public FixedLengthStringData[] fupcdes38Out = FLSArrayPartOfStructure(12, 1, filler7, 444);
	public FixedLengthStringData[] fupcdes39Out = FLSArrayPartOfStructure(12, 1, filler7, 456);
	public FixedLengthStringData[] fupcdes40Out = FLSArrayPartOfStructure(12, 1, filler7, 468);
	public FixedLengthStringData[] fupcdes41Out = FLSArrayPartOfStructure(12, 1, filler7, 480);
	public FixedLengthStringData[] fupcdes42Out = FLSArrayPartOfStructure(12, 1, filler7, 492);
	public FixedLengthStringData[] fupcdes43Out = FLSArrayPartOfStructure(12, 1, filler7, 504);
	public FixedLengthStringData[] fupcdes44Out = FLSArrayPartOfStructure(12, 1, filler7, 516);
	public FixedLengthStringData[] fupcdes45Out = FLSArrayPartOfStructure(12, 1, filler7, 528);
	public FixedLengthStringData[] fupcdes46Out = FLSArrayPartOfStructure(12, 1, filler7, 540);
	public FixedLengthStringData[] fupcdes47Out = FLSArrayPartOfStructure(12, 1, filler7, 552);
	public FixedLengthStringData[] fupcdes48Out = FLSArrayPartOfStructure(12, 1, filler7, 564);
	public FixedLengthStringData[] fupcdes49Out = FLSArrayPartOfStructure(12, 1, filler7, 576);
	public FixedLengthStringData[] fupcdes50Out = FLSArrayPartOfStructure(12, 1, filler7, 588);
	public FixedLengthStringData[] fupcdes51Out = FLSArrayPartOfStructure(12, 1, filler7, 600);
	public FixedLengthStringData[] fupcdes52Out = FLSArrayPartOfStructure(12, 1, filler7, 612);
	public FixedLengthStringData[] fupcdes53Out = FLSArrayPartOfStructure(12, 1, filler7, 624);
	public FixedLengthStringData[] fupcdes54Out = FLSArrayPartOfStructure(12, 1, filler7, 636);
	public FixedLengthStringData[] fupcdes55Out = FLSArrayPartOfStructure(12, 1, filler7, 648);
	public FixedLengthStringData[] fupcdes56Out = FLSArrayPartOfStructure(12, 1, filler7, 660);
	public FixedLengthStringData[] fupcdes57Out = FLSArrayPartOfStructure(12, 1, filler7, 672);
	public FixedLengthStringData[] fupcdes58Out = FLSArrayPartOfStructure(12, 1, filler7, 684);
	public FixedLengthStringData[] fupcdes59Out = FLSArrayPartOfStructure(12, 1, filler7, 696);
	public FixedLengthStringData[] fupcdes60Out = FLSArrayPartOfStructure(12, 1, filler7, 708);
	public FixedLengthStringData[] hcondateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 876);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 888);
	public FixedLengthStringData[] languageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 900);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 912);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 924);
	public FixedLengthStringData trcodesOut = new FixedLengthStringData(336).isAPartOf(outputIndicators, 936);
	public FixedLengthStringData[] trcodeOut = FLSArrayPartOfStructure(28, 12, trcodesOut, 0);
	public FixedLengthStringData[][] trcodeO = FLSDArrayPartOfArrayStructure(12, 1, trcodeOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(336).isAPartOf(trcodesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] trcode01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] trcode02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] trcode03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] trcode04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] trcode05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] trcode06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] trcode07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] trcode08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] trcode09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] trcode10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public FixedLengthStringData[] trcode11Out = FLSArrayPartOfStructure(12, 1, filler8, 120);
	public FixedLengthStringData[] trcode12Out = FLSArrayPartOfStructure(12, 1, filler8, 132);
	public FixedLengthStringData[] trcode13Out = FLSArrayPartOfStructure(12, 1, filler8, 144);
	public FixedLengthStringData[] trcode14Out = FLSArrayPartOfStructure(12, 1, filler8, 156);
	public FixedLengthStringData[] trcode15Out = FLSArrayPartOfStructure(12, 1, filler8, 168);
	public FixedLengthStringData[] trcode16Out = FLSArrayPartOfStructure(12, 1, filler8, 180);
	public FixedLengthStringData[] trcode17Out = FLSArrayPartOfStructure(12, 1, filler8, 192);
	public FixedLengthStringData[] trcode18Out = FLSArrayPartOfStructure(12, 1, filler8, 204);
	public FixedLengthStringData[] trcode19Out = FLSArrayPartOfStructure(12, 1, filler8, 216);
	public FixedLengthStringData[] trcode20Out = FLSArrayPartOfStructure(12, 1, filler8, 228);
	public FixedLengthStringData[] trcode21Out = FLSArrayPartOfStructure(12, 1, filler8, 240);
	public FixedLengthStringData[] trcode22Out = FLSArrayPartOfStructure(12, 1, filler8, 252);
	public FixedLengthStringData[] trcode23Out = FLSArrayPartOfStructure(12, 1, filler8, 264);
	public FixedLengthStringData[] trcode24Out = FLSArrayPartOfStructure(12, 1, filler8, 276);
	public FixedLengthStringData[] trcode25Out = FLSArrayPartOfStructure(12, 1, filler8, 288);
	public FixedLengthStringData[] trcode26Out = FLSArrayPartOfStructure(12, 1, filler8, 300);
	public FixedLengthStringData[] trcode27Out = FLSArrayPartOfStructure(12, 1, filler8, 312);
	public FixedLengthStringData[] trcode28Out = FLSArrayPartOfStructure(12, 1, filler8, 324);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sh596screenWritten = new LongData(0);
	public LongData Sh596protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh596ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {language, company, tabl, item, longdesc, trcode01, trcode02, trcode03, trcode04, trcode05, trcode06, trcode07, trcode08, trcode09, trcode10, trcode11, trcode12, trcode13, trcode14, trcode15, trcode16, trcode17, trcode18, trcode19, trcode20, trcode21, trcode22, trcode23, trcode24, trcode25, trcode26, trcode27, trcode28, cnRiskStat01, cnRiskStat02, cnRiskStat03, cnRiskStat04, cnRiskStat05, cnRiskStat06, cnRiskStat07, cnRiskStat08, cnRiskStat09, cnRiskStat10, cnRiskStat11, cnRiskStat12, fupcdes01, fupcdes02, fupcdes03, fupcdes04, fupcdes05, fupcdes06, fupcdes07, fupcdes08, fupcdes09, fupcdes10, fupcdes11, fupcdes12, fupcdes13, fupcdes14, fupcdes15, fupcdes16, fupcdes17, fupcdes18, fupcdes19, fupcdes20, fupcdes21, fupcdes22, fupcdes23, fupcdes24, fupcdes25, fupcdes26, fupcdes27, fupcdes28, fupcdes29, fupcdes30, fupcdes31, fupcdes32, fupcdes33, fupcdes34, fupcdes35, fupcdes36, fupcdes37, fupcdes38, fupcdes39, fupcdes40, fupcdes41, fupcdes42, fupcdes43, fupcdes44, fupcdes45, fupcdes46, fupcdes47, fupcdes48, fupcdes49, fupcdes50, fupcdes51, fupcdes52, fupcdes53, fupcdes54, fupcdes55, fupcdes56, fupcdes57, fupcdes58, fupcdes59, fupcdes60, hcondate};
		screenOutFields = new BaseData[][] {languageOut, companyOut, tablOut, itemOut, longdescOut, trcode01Out, trcode02Out, trcode03Out, trcode04Out, trcode05Out, trcode06Out, trcode07Out, trcode08Out, trcode09Out, trcode10Out, trcode11Out, trcode12Out, trcode13Out, trcode14Out, trcode15Out, trcode16Out, trcode17Out, trcode18Out, trcode19Out, trcode20Out, trcode21Out, trcode22Out, trcode23Out, trcode24Out, trcode25Out, trcode26Out, trcode27Out, trcode28Out, cnrstat01Out, cnrstat02Out, cnrstat03Out, cnrstat04Out, cnrstat05Out, cnrstat06Out, cnrstat07Out, cnrstat08Out, cnrstat09Out, cnrstat10Out, cnrstat11Out, cnrstat12Out, fupcdes01Out, fupcdes02Out, fupcdes03Out, fupcdes04Out, fupcdes05Out, fupcdes06Out, fupcdes07Out, fupcdes08Out, fupcdes09Out, fupcdes10Out, fupcdes11Out, fupcdes12Out, fupcdes13Out, fupcdes14Out, fupcdes15Out, fupcdes16Out, fupcdes17Out, fupcdes18Out, fupcdes19Out, fupcdes20Out, fupcdes21Out, fupcdes22Out, fupcdes23Out, fupcdes24Out, fupcdes25Out, fupcdes26Out, fupcdes27Out, fupcdes28Out, fupcdes29Out, fupcdes30Out, fupcdes31Out, fupcdes32Out, fupcdes33Out, fupcdes34Out, fupcdes35Out, fupcdes36Out, fupcdes37Out, fupcdes38Out, fupcdes39Out, fupcdes40Out, fupcdes41Out, fupcdes42Out, fupcdes43Out, fupcdes44Out, fupcdes45Out, fupcdes46Out, fupcdes47Out, fupcdes48Out, fupcdes49Out, fupcdes50Out, fupcdes51Out, fupcdes52Out, fupcdes53Out, fupcdes54Out, fupcdes55Out, fupcdes56Out, fupcdes57Out, fupcdes58Out, fupcdes59Out, fupcdes60Out, hcondateOut};
		screenErrFields = new BaseData[] {languageErr, companyErr, tablErr, itemErr, longdescErr, trcode01Err, trcode02Err, trcode03Err, trcode04Err, trcode05Err, trcode06Err, trcode07Err, trcode08Err, trcode09Err, trcode10Err, trcode11Err, trcode12Err, trcode13Err, trcode14Err, trcode15Err, trcode16Err, trcode17Err, trcode18Err, trcode19Err, trcode20Err, trcode21Err, trcode22Err, trcode23Err, trcode24Err, trcode25Err, trcode26Err, trcode27Err, trcode28Err, cnrstat01Err, cnrstat02Err, cnrstat03Err, cnrstat04Err, cnrstat05Err, cnrstat06Err, cnrstat07Err, cnrstat08Err, cnrstat09Err, cnrstat10Err, cnrstat11Err, cnrstat12Err, fupcdes01Err, fupcdes02Err, fupcdes03Err, fupcdes04Err, fupcdes05Err, fupcdes06Err, fupcdes07Err, fupcdes08Err, fupcdes09Err, fupcdes10Err, fupcdes11Err, fupcdes12Err, fupcdes13Err, fupcdes14Err, fupcdes15Err, fupcdes16Err, fupcdes17Err, fupcdes18Err, fupcdes19Err, fupcdes20Err, fupcdes21Err, fupcdes22Err, fupcdes23Err, fupcdes24Err, fupcdes25Err, fupcdes26Err, fupcdes27Err, fupcdes28Err, fupcdes29Err, fupcdes30Err, fupcdes31Err, fupcdes32Err, fupcdes33Err, fupcdes34Err, fupcdes35Err, fupcdes36Err, fupcdes37Err, fupcdes38Err, fupcdes39Err, fupcdes40Err, fupcdes41Err, fupcdes42Err, fupcdes43Err, fupcdes44Err, fupcdes45Err, fupcdes46Err, fupcdes47Err, fupcdes48Err, fupcdes49Err, fupcdes50Err, fupcdes51Err, fupcdes52Err, fupcdes53Err, fupcdes54Err, fupcdes55Err, fupcdes56Err, fupcdes57Err, fupcdes58Err, fupcdes59Err, fupcdes60Err, hcondateErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh596screen.class;
		protectRecord = Sh596protect.class;
	}

}
