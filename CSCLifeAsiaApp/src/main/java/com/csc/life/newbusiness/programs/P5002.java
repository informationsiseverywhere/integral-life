/*
 * File: P5002.java
 * Date: 29 August 2009 23:52:22
 * Author: Quipoz Limited
 *
 * Class transformed from P5002.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.exceptions.WorkflowServiceException;
import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.BextpfDAO;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.MandpfDAO;
import com.csc.fsu.general.dataaccess.model.Bextpf;
import com.csc.fsu.general.dataaccess.model.Mandpf;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Letrqstrec;//ILIFE-3139
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsu.general.recordstructures.Undactncpy;
import com.csc.fsu.general.tablestructures.Th5agrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.newbusiness.screens.S5002ScreenVars;
import com.csc.life.newbusiness.tablestructures.T3615rec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5677rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.underwriting.dataaccess.dao.UndlpfDAO;
import com.csc.life.underwriting.dataaccess.model.Undlpf;
import com.csc.life.workflow.LifeWorkflowClient;
import com.csc.life.workflow.config.IntegralLifeWorkflowProperties;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Errmesgrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.utils.UserMapingUtils;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Errmesg;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.COBOLFunctions;
//import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
//ILIFE-3139-ends
//ILIFE-3139-starts
import com.quipoz.framework.util.ThreadLocalStore;

/**
 * <pre>
 * Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
 *REMARKS.
 *
 *                  Life New Business Submenu
 *                  =========================
 *
 *
 * Initialisation
 * --------------
 *
 *  Retrieve todays date (DATCON1) and store for later use.
 *
 *
 * Validation
 * ----------
 *
 *  Key 1 - Contract proposal number (CHDRLNB)
 *
 *       Y = mandatory, must exist on file.
 *            - CHDRLNB  -  Life  new  business  contract  header
 *                 logical view  (LIFO,  keyed  on  company   and
 *                 contract number, select service unit = LP).
 *            - must be correct status for transaction (T5679).
 *            - for maintenance  transactions,   servicing  branch
 *              must be sign-on branch
 *
 *       N = optional, if entered must not exist on file, must be
 *            within correct number range (call ALOCNO to check).
 *
 *       Blank = irrelevant.
 *
 *  Key 2 - contract type (T5688 I/O module validated)
 *
 *       Y = mandatory, new business must be allowable.
 *
 *       N = irrelevant.
 *
 *       Blank = irrelevant.
 *
 *  Key 3 - Contract number (CHDRENQ).
 *
 *       Y = mandatory, must exist on file.
 *            - must be correct status for transaction (T5679).
 *
 *       N = irrelevant.
 *
 *       Blank = irrelevant.
 *
 *
 * Updating
 * --------
 *
 *  Set up WSSP-FLAG:
 *       - "I" for actions C and D, otherwise "M".
 *  For enquiry transactions ("I"), nothing else is required.
 *
 *  If creating a new  proposal and the contract number is blank,
 *  call the automatic  number allocation routine (ALOCNO) to get
 *  a number.
 *
 *  For maintenance transactions  (WSSP-FLAG  set to  'M' above),
 *  soft lock the contract header (call SFTLOCK). If the contract
 *  is already locked, display an error message.
 *
 *  Store  the  contract  header   in  the  I/O  module  for  the
 *  transactions programs called.
 *  For modifications, add one to the current transaction number.
 *  For New contract proposals,  initialise  all the details:
 *            Service unit to 'LP',
 *            Valid flag to '3',
 *            All numeric fields and dates to zero,
 *            Set current-to date to VRCM-MAX-DATE,
 *            Transaction number to 1,
 *            Company to sign-on company,
 *            Contract type as entered,
 *            Contract number as returned from ALOCNO,
 *            Contract prefix to 'CH',
 *            Contract status from T5679 for current transaction.
 *
 *  For new proposals, set up key of first Life for use later.
 *
 *  For new proposals, if  the  Default  Follow-up  Type from the
 *  contract structure table  (T5688  read  during validation) is
 *  not blank, read  the  default  follow-ups table (T5677) using
 *  the current transaction  number and follow-up type.  For each
 *  Follow-up  code,  add   a   record  to  the  Follow-ups  file
 *  (FLUPLNB), initialising fields as follows:
 *            Contract company to sign-on company,
 *            Contract number for current contract,
 *            Transaction number 1,
 *            Follow-up number sequentially from 1,
 *            Follow-up type to 'P',
 *            Follow up code from T5677,
 *            Follow-up date set to today's date (stored earlier),
 *            Follow-up status  to  default  status for follow-up
 *                 code from T6551,
 *            Remarks from long  description of follow-up code on
 *                 T5661.
 *
 *
 *****************************************************************
 * </pre>
 */
public class P5002 extends ScreenProgCS {
	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(P5002.class);

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5002");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
	//TMLII-268

	/* FORMATS */
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String lifelnbrec = "LIFELNBREC";


	private FixedLengthStringData wsaaGenkey = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaServunit = new FixedLengthStringData(2).isAPartOf(wsaaGenkey, 0);//IJS-183
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).isAPartOf(wsaaGenkey, 2);

	private FixedLengthStringData wsaaT5677Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5677Tranno = new FixedLengthStringData(4).isAPartOf(wsaaT5677Key, 0);
	private FixedLengthStringData wsaaT5677Followup = new FixedLengthStringData(4).isAPartOf(wsaaT5677Key, 4);

	private FixedLengthStringData wsaaProgname = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaPrognum = new FixedLengthStringData(4).isAPartOf(wsaaProgname, 1);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Prognum = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0);
	private FixedLengthStringData wsaaT5671Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT5671Key, 4);
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaT5671Key, 7, FILLER).init("*");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaX = new PackedDecimalData(2, 0).init(0).setUnsigned();
	private String wsaaAutonum = "";
	private FixedLengthStringData wsaaCharFound = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCharError = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSpaceFound = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaNewStrnArray = new FixedLengthStringData(10);
	private FixedLengthStringData[] wsaaNewArray = FLSArrayPartOfStructure(10, 1, wsaaNewStrnArray, 0);

	private FixedLengthStringData wsaaNewStrn = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaNstr1 = new FixedLengthStringData(8).isAPartOf(wsaaNewStrn, 0);
	private FixedLengthStringData wsaaNstr2 = new FixedLengthStringData(2).isAPartOf(wsaaNewStrn, 8);
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaChdrnumArray = new FixedLengthStringData(10);
	private FixedLengthStringData[] wsaaChdrnumField = FLSArrayPartOfStructure(10, 1, wsaaChdrnumArray, 0);
	//ILIFE-3139-starts
	private FixedLengthStringData wsaaLetrFnd =  new FixedLengthStringData(1);
	private Letrqstrec letrqstrec = new Letrqstrec();
	//ILIFE-3139-ends
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Alocnorec alocnorec = new Alocnorec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5688rec t5688rec = new T5688rec();
	private T5679rec t5679rec = new T5679rec();
	private T5677rec t5677rec = new T5677rec();
	private T5671rec t5671rec = new T5671rec();
	private T5661rec t5661rec = new T5661rec();
	protected Batckey wsaaBatchkey = new Batckey();	
	private Tr384rec tr384rec = new Tr384rec();//ILIFE-3139
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Undactncpy undactncpy = new Undactncpy();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Wssplife wssplife = new Wssplife();
	private S5002ScreenVars sv = getPScreenVars(); //ScreenProgram.getScreenVars( S5002ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList;
	private Itempf itempf = null;
	private Descpf descpf;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private boolean exitFlag;
	private LifeWorkflowClient workflowClient = getApplicationContext().getBean("lifefWorkflowClient", LifeWorkflowClient.class);
	//ILIFE-6587 by wli31
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private UndlpfDAO undldao =getApplicationContext().getBean("undlpfDAO",UndlpfDAO.class);
	private List<Fluppf> fluplnbList = new ArrayList<Fluppf>();
	private List<Undlpf> undlpfList = new ArrayList<Undlpf>();
	private Fluppf fluppf = null;
	private int fupno = 0;
	private Chdrpf chdrenq = null;
	private boolean foundFlag = false;
	private FixedLengthStringData wsaaKey = new FixedLengthStringData(18);	//IBPLIFE-1718
	private FixedLengthStringData wsaaKeyProposal = new FixedLengthStringData(8).isAPartOf(wsaaKey, 0);
	private FixedLengthStringData wsaaKeyAction = new FixedLengthStringData(10).isAPartOf(wsaaKey, 8);
	private boolean onePflag = false; //IBPLIFE-2470
	private static final String  ONE_P_CASHLESS="NBPRP124"; //IBPLIFE-2470
	private T3615rec t3615rec = new T3615rec();//IBPLIFE-2470
	private String t3615 = "T3615"; //IBPLIFE-2470
	private Th5agrec th5agrec = new Th5agrec();//IBPLIFE-2470
	private String th5ag = "Th5ag"; //IBPLIFE-2470
	private Datcon1rec datcon1 = new Datcon1rec();//IBPLIFE-2470
	private MandpfDAO mandpfDAO = getApplicationContext().getBean("mandpfDAO", MandpfDAO.class);//IBPLIFE-2470
	private ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);//IBPLIFE-2470
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);//IBPLIFE-2470
	List<Mandpf> mandpflist = new ArrayList<>();//IBPLIFE-2470
	private FixedLengthStringData currentDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaTodayDate = new ZonedDecimalData(8).isAPartOf(currentDate);
	private ZonedDecimalData wsaaTodayYear = new ZonedDecimalData(4).isAPartOf(currentDate, 0);
	private ZonedDecimalData wsaaTodayMnth = new ZonedDecimalData(2).isAPartOf(currentDate);
	private FixedLengthStringData firstPremdate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaTodayfirstPremdate = new ZonedDecimalData(8).isAPartOf(firstPremdate);
	private ZonedDecimalData wsaaTodayfirstPremYear = new ZonedDecimalData(4).isAPartOf(firstPremdate, 0);
	private ZonedDecimalData wsaaTodayfirstPremMnth = new ZonedDecimalData(2).isAPartOf(firstPremdate);
	private BextpfDAO bextpfDAO = getApplicationContext().getBean("bextpfDAO", BextpfDAO.class);
	private List<Bextpf> bextpfList = null;
	private Errmesgrec errmesgrec = new Errmesgrec();//IBPTE-1239
	
	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit2190,
		checkChdr2215,
		validateKey22220,
		validateKey32230,
		exit2290,
		search2510,
		exit12090,
		keeps3070,
		batching3080,
		exit3090,
		exit3390,
		exit3490,
		exit3590
	}

	public P5002() {
		super();
		screenVars = sv;
		new ScreenModel("S5002", AppVars.getInstance(), sv);
	}

	protected S5002ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( S5002ScreenVars.class);
	}
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}


	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}
	//TMLII-268 NB-08-001 start
	public void processBoNew(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			/* Initialization*/
			scrnparams.version.set(getWsaaVersion());
			wsspcomn.version.set(getWsaaVersion());
			scrnparams.errorline.set(varcom.vrcmMsg);
			// need to skip certain areas of validation for 2000 call
			if(isEQ(wsspcomn.sectionno, "2000")){
				// Initialization, as is done when we call processBoMainline() method - start
				appVars = getAppVars();
				// save appvars indicators before making Pnnnn call
				appVars.saveInds(1, wsaaSavedIndicAreaP);
				syserrrec.subrname.set(getWsaaProg());
				syserrrec.statuz.set(Varcom.oK);
				varcom.vrcmTranid.set(wsspcomn.tranid);
				opstatsrec.opsrecTim2000.set(COBOLFunctions.getCobolTime());
				wsspcomn.edterror.set(Varcom.oK);
				// Initialization, as is done when we call processBoMainline() method - end
				/*VALIDATE*/
				validateAction2100();
				/* Need to read CHDRLNB logical file as it is being stored in 3000 section using KEEPS */
				chdrlnbIO.setChdrcoy(wsspcomn.company);
				chdrlnbIO.setChdrnum(sv.chdrsel);
				chdrlnbIO.setFunction(varcom.readr);
				if (isNE(sv.chdrsel,SPACES)) {
					SmartFileCode.execute(appVars, chdrlnbIO);
				}
				else {
					chdrlnbIO.setStatuz(varcom.mrnf);
				}
				if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
						&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
					syserrrec.params.set(chdrlnbIO.getParams());
					fatalError600();
				}
				if (isEQ(chdrlnbIO.getStatuz(),varcom.mrnf)
						&& isEQ(subprogrec.key1,"Y")) {
					sv.chdrselErr.set(errorsInner.e544);
					wsspcomn.edterror.set("Y");
				}
				/*CHECK-FOR-ERRORS*/
				if (isNE(sv.errorIndicators,SPACES)) {
					wsspcomn.edterror.set("Y");
				}
				/*EXIT*/
				// restore appvars indicators after Pnnnn call
				appVars.restoreInds(1, wsaaSavedIndicAreaP);

			} else {
				processBoMainline(sv, sv.dataArea, parmArray);
			}
		}
		catch (COBOLExitProgramException e) {
		}
	}
	//TMLII-268 NB-08-001 end

	/**
	 * <pre>
	 *      INITIALISE FIELDS FOR SHOWING ON SCREEN
	 * </pre>
	 */
	protected void initialise1000()
	{ 
		ThreadLocalStore.put(P5006.UWREC_CACHE, null);
		ThreadLocalStore.put(P5006.UWQUESTIONNAIRECACHE, null);
		ThreadLocalStore.put(P5006.AUTO_TERMS_LIST, null);
		ThreadLocalStore.put(P5006.COMUW_RESULT_CACHE, null);
		/*
		 * Added condition for workflow triggers so that they are triggered only
		 * after proposal number is committed in database.
		 */
		boolean chdrnumStatus = chdrpfDAO.isChdrnumExists(wsspcomn.company.toString(),
				 wssplife.chdrnum.toString()); // IJTI-1979
		if (chdrnumStatus) { // IJTI-1979
			// IJTI-400 starts
			boolean isWorkflowFeatureEnabled = false;
			isWorkflowFeatureEnabled = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP089", appVars, "IT");
			if (isEQ(wsspcomn.sbmaction, "A") && IntegralLifeWorkflowProperties.getWorkflowServiceRuntimeURL() != null
					&& (IntegralLifeWorkflowProperties.isWorkflowEnabled() && isNE(wssplife.chdrnum, SPACES)
							&& isWorkflowFeatureEnabled)) {
				triggerNewBusinessWorkflow();
			}
			// IJTI-400 ends

			// IJTI-840 starts
			isWorkflowFeatureEnabled = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP098", appVars, "IT");
			if (isEQ(wsspcomn.sbmaction, "A") && IntegralLifeWorkflowProperties.getWorkflowServiceRuntimeURL() != null
					&& (IntegralLifeWorkflowProperties.isWorkflowEnabled() && isNE(wssplife.chdrnum, SPACES)
							&& isWorkflowFeatureEnabled)) {
				try {
					String userid = UserMapingUtils.getLongUserIdValue(wsspcomn.userid.toString()).toLowerCase().trim();
					workflowClient.startUWWorkflow(wssplife.chdrnum.trim(), userid, userid); // IJTI-1196
				} catch (WorkflowServiceException e) {
					//IBPTE-1239
					String message = getMessage("RUQ6", "Error occurred during New Business Workflow Launch");
					LOGGER.error(message, e);
					syserrrec.params.set(e.getMessage());
					//IBPTE-1239
					appVars.addMessage(message); // IJTI-1389
				}
				wssplife.userArea.set(SPACES);
			}
		} // IJTI-1979
		//IJTI-840 ends
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		wsaaAutonum = "N";
		sv.action.set(wsspcomn.sbmaction);
		exitFlag = false;
		
		
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
				|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
		if (isEQ(wsaaToday, 0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		/*EXIT*/
		onePflag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), ONE_P_CASHLESS, appVars, "IT"); //IBPLIFE-2470
		if(!onePflag) {
			sv.actionOut[Varcom.nd.toInt()].set("Y");
		}
		//IJS-183
		List<Itempf> items = itempfDAO.getItemsbyItemcodeLikeOperator("IT", wsspcomn.company.toString(), "T3642", "CH");
		Itempf itempf = items.get(0);
		if(itempf!=null) {
			wsaaServunit.set(itempf.getItemitem().substring(2,4));
		}
		//IJS-183 END
	}
	
	// IJTI-1979 - Starts
	/**
	 *  Trigger New Business Workflow
	 */
	protected void triggerNewBusinessWorkflow() {

		if (isEQ(wssplife.isUWRequired, "Y") && isEQ(wssplife.isUWConfirmed, "N")) {
			try {
				// IJTI-1022
				workflowClient.startNewBusinessWorkflow(wssplife.chdrnum.trim(),
						UserMapingUtils.getLongUserIdValue(wsspcomn.userid.toString()).toLowerCase().trim(),
						UserMapingUtils.getLongUserIdValue(wsspcomn.userid.toString()).toLowerCase().trim());
			} catch (WorkflowServiceException e) {
				//IBPTE-1239
				String message = getMessage("RUQ6", "Error occurred during CIL UW Workflow Launch");
				LOGGER.error(message, e);
				syserrrec.params.set(e.getMessage());
				// IBPTE-1239
				appVars.addMessage(message);
			}
		}
		wssplife.userArea.set(SPACES);
	}
	// IJTI-1979 - Ends
	
	/**
	 * <pre>
	 *     RETRIEVE SCREEN FIELDS AND EDIT
	 * </pre>
	 */
	protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    IF S5002-CHDRSEL-ERR        = F910                           */
		/*       MOVE O-K                 TO WSSP-EDTERROR                 */
		/*       MOVE SPACES              TO S5002-CHDRSEL-ERR             */
		/*       MOVE SPACES              TO S5002-CHDRSEL                 */
		/*       GO TO 2020-VALIDATE.                                      */
		return ;
		/*PRE-EXIT*/
	}

	protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'S5002IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5002-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)) {
			if (isNE(scrnparams.statuz, "BACH")) {
				validateKeys2200("DEFAULT");
			}
			else {
				verifyBatchControl2900();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
		
		if (isEQ(sv.action,"K") && onePflag && foundFlag)
			validateOnePCashless();
		//IBPLIFE-2472 Start
		if (onePflag && isEQ(sv.action,"L")) {
			readT3615();
		   if (isEQ(sv.chdrselErr, SPACES) && (isNE(chdrlnbIO.getStatcode(),"SI") || isNE(chdrlnbIO.getPstatcode(),"NR") || isNE(t3615rec.onepcashless,"Y"))) { 
			sv.chdrselErr.set(errorsInner.e767);
			return;
		   }
		   bextpfList = bextpfDAO.searchBextrevRecordForDel(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
			if(null != bextpfList && !bextpfList.isEmpty()){
				sv.chdrselErr.set(errorsInner.ruoq);
				wsspcomn.edterror.set("Y");
				return;
			}
		   validateBilling();
		}
		//IBPLIFE-2472 End
	}

	protected void validateOnePCashless() {
		readT3615();
		if(isEQ(chdrlnbIO.getStatcode(),"UW") && isEQ(chdrlnbIO.getPstatcode(),"NR") && isNE(t3615rec.onepcashless,'Y')) {
			sv.errorIndicators.set(errorsInner.rulm);
			wsspcomn.edterror.set("Y");
			return;
		}
		validateBilling();
	}
	
	protected void readT3615() {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t3615);
		itempf.setItemitem(chdrlnbIO.getSrcebus().toString());
		itempf.setValidflag("1");
		itempf = itempfDAO.getItemRecordByItemkey(itempf);			
		if (itempf != null) {
			t3615rec.t3615Rec.set(StringUtil.rawToString(itempf.getGenarea()));			
		}
	}
	
	protected void validateBilling() {
		if(isEQ(t3615rec.onepcashless,'Y')) {
			datcon1.function.set(Varcom.tday);
			Datcon1.getInstance().mainline(datcon1.datcon1Rec);
			currentDate.set(datcon1.intDate);
			Clbapf clbapf;
			Hpadpf hpadpf;
			
			hpadpf = hpadpfDAO.getHpadData(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
			firstPremdate.set(hpadpf.getRskcommdate());	
			
			mandpflist = mandpfDAO.getMandpfList(wsspcomn.fsuco.toString(), chdrlnbIO.getCownnum().toString());
			
			if(!mandpflist.isEmpty()) {
				clbapf = clbapfDAO.searchClblData(mandpflist.get(0).getBankkey(), mandpflist.get(0).getBankacckey(),
						wsspcomn.fsuco.toString(), chdrlnbIO.getCownnum().toString());
			
				itempf = new Itempf();
				itempf.setItempfx("IT");
				itempf.setItemcoy(wsspcomn.fsuco.toString());
				itempf.setItemtabl(th5ag);
				itempf.setItemitem(wsaaTodayYear.toString().trim().concat(chdrlnbIO.getBillchnl().toString()
						.trim()).concat(clbapf.getFacthous()));
				itempf.setValidflag("1");
				itempf = itempfDAO.getItemRecordByItemkey(itempf);			
				if (itempf != null) {
					th5agrec.th5agRec.set(StringUtil.rawToString(itempf.getGenarea()));			
				}
				
				if(isGT(currentDate ,th5agrec.prcbilldte[wsaaTodayfirstPremMnth.toInt()]) ||
						isEQ(th5agrec.prcbilldte[wsaaTodayMnth.toInt()],hpadpf.getTntapdate())) {
					sv.errorIndicators.set(errorsInner.ruln);
					wsspcomn.edterror.set("Y");
				}
			}
		}
	}

	protected void validateAction2100()
	{
		checkAgainstTable2110();
		checkSanctions2120();
	}

	protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			//goTo(GotoLabel.exit2190);
			return;
		}
	}

	protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
	
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}


	protected void validateKeys2200(String nextMthod)
	{
		boolean errorFlag = false;
		while (true) {
			switch (nextMthod) {
			case "DEFAULT":
				errorFlag = validateKey12210();
				if(errorFlag){
					break;
				}
			case "checkChdr2215":
				errorFlag = checkChdr2215();
				if(errorFlag){
					break;
				}
			case "validateKey22220":
				errorFlag = validateKey22220();
				if(errorFlag){
					break;
				}
			case "validateKey32230":
				validateKey32230();
			case "exit2290":
			}
			break;
		}
	}

	protected boolean validateKey12210()
	{
	
		
		if(!sv.chdrsel.toString().trim().equalsIgnoreCase("")){
			chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString(), sv.chdrsel.toString());
			/*ILIFE-4853 STARTS*/
			if (!(chdrpf == null)) {
				chdrpfDAO.setCacheObject(chdrpf);
			}
			//IBPLIFE-6259 starts
			else {
				if(isEQ(sv.action, "F")){
					sv.chdrselErr.set(errorsInner.f917);
					wsspcomn.edterror.set("Y");
				}
			}
			//IBPLIFE-6259 ends
			/*else {
				sv.chdrselErr.set(errorsInner.e544);
				wsspcomn.edterror.set("Y");
				// goTo(GotoLabel.exit2290);
				return true;
			}*/
			/*   ILIFE-4853 ENDS*/
		}
		if (isEQ(subprogrec.key1, SPACES)) {
			//goTo(GotoLabel.validateKey22220);
			return validateKey22220();
		}
		if (isEQ(subprogrec.key1, "N")
				&& isEQ(sv.chdrsel, SPACES)
				&& isNE(sv.chdrtype, SPACES)) {
			allocateNumber2600();
			wsaaAutonum = "Y";
		}
		return false;
	}
	

	protected boolean checkChdr2215()
	{

		if(exitFlag == true)
			return false;
		if (isNE(sv.chdrsel, SPACES)
				&& isNE(wsaaAutonum, "Y")) {
			checkChdrsel2800();
			if (isNE(sv.chdrselErr, SPACES)) {
				//goTo(GotoLabel.exit2290);
				return true;
			}
		}
		//htruong25
		checkChdr2215CustomerSpecific();
		
		chdrlnbIO.setChdrcoy(wsspcomn.company);
		chdrlnbIO.setChdrnum(sv.chdrsel);
		chdrlnbIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel, SPACES)) {
			SmartFileCode.execute(appVars, chdrlnbIO);
		}
		else {
			chdrlnbIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
				&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		
		if (isEQ(chdrlnbIO.getStatuz(), varcom.mrnf)
				&& isEQ(subprogrec.key1, "Y")) {
			sv.chdrselErr.set(errorsInner.e544);
			wsspcomn.edterror.set("Y");
			//goTo(GotoLabel.exit2290);
			return true;
		}
		
		
		
		if (isEQ(subprogrec.key1, "Y")) {
			initialize(sdasancrec.sancRec);
			sdasancrec.function.set("VENTY");
			sdasancrec.userid.set(wsspcomn.userid);
			sdasancrec.entypfx.set(fsupfxcpy.chdr);
			sdasancrec.entycoy.set(wsspcomn.company);
			sdasancrec.entynum.set(sv.chdrsel);
			callProgram(Sdasanc.class, sdasancrec.sancRec);
			if (isNE(sdasancrec.statuz, varcom.oK)) {
				sv.chdrselErr.set(sdasancrec.statuz);
				//goTo(GotoLabel.exit2290);
				return true;
			}
		}
		if (isEQ(chdrlnbIO.getStatuz(), varcom.oK)
				&& isEQ(subprogrec.key1, "N")) {
			if (isEQ(wsaaAutonum, "N")) {
				sv.chdrselErr.set(errorsInner.f918);
			}
			else {
				allocateNumber2600();
				return checkChdr2215();
			}
		}
		/*    For new proposals, if a number is entered,*/
		/*       check it against the automatic number allocation range.*/
		if (isEQ(subprogrec.key1, "N")
				&& isNE(sv.chdrsel, SPACES)
				&& isEQ(sv.chdrselErr, SPACES)
				&& isEQ(wsaaAutonum, "N")) {
			checkAlocno2300();
		}
		/*    Release the covtlnb io area in case it has been*/
		/*       previously kept*/
		covtlnbIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)
				&& isNE(covtlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		/*    For transactions on existing proposals,*/
		/*       check the contract selected is of the correct status*/
		/*       and from correct branck.*/
		if (isEQ(subprogrec.key1, "Y")
				&& isEQ(sv.chdrselErr, SPACES)) {
			checkStatus2400();
		}
		if (isEQ(subprogrec.key1, "Y")
				&& isEQ(sv.chdrselErr, SPACES)
				&& isNE(sv.action, "E")
				&& isNE(wsspcomn.branch, chdrlnbIO.getCntbranch())) {
			sv.chdrselErr.set(errorsInner.e455);
		}
		//ILIFE-3139-STARTS */
		if (isEQ(sv.action,"J")
				&& isNE(sv.chdrsel,SPACES)
				&& isEQ(sv.chdrselErr,SPACES)) {
			chkUndl5000();
		}
		return false;
	}


	protected void chkUndl5000() {
		//ILIFE-6587
		undlpfList = undldao.searchUndlpfList(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim(), "01", "00");
		if(undlpfList.size() > 0 ){
			for(Undlpf undlpf : undlpfList){
				if (isNE(undlpf.getUndwflag(),"Y")) {
					sv.chdrselErr.set("E964");
					return;
				}
			}
		}else{
			//fatalError600();
			return;	//ILIFE-6926
		}
	}	

	//ILIFE-3139-ENDS

	protected boolean validateKey22220()
	{
		/*    Already checked against the table by the I/O module.*/
		if (isEQ(subprogrec.key2, SPACES)
				|| isEQ(subprogrec.key2, "N")
				|| isNE(sv.chdrtypeErr, SPACES)) {
			//goTo(GotoLabel.validateKey32230);
			validateKey32230();
			return true;
		}
		if (isEQ(sv.chdrtype, SPACES)) {
			sv.chdrtypeErr.set(errorsInner.e186);
			//goTo(GotoLabel.validateKey32230);
			validateKey32230();
			return true;
		}
		chdrlnbIO.setCnttype(sv.chdrtype);
		/* MOVE SPACES                 TO ITEM-DATA-KEY.                */
		/* MOVE 'IT'                   TO ITEM-ITEMPFX.                 */
		/* MOVE WSSP-COMPANY           TO ITEM-ITEMCOY.                 */
		/* MOVE T5688                  TO ITEM-ITEMTABL.                */
		/* MOVE S5002-CHDRTYPE         TO ITEM-ITEMITEM.                */
		/* MOVE READR                  TO ITEM-FUNCTION.                */
		/* CALL 'ITEMIO' USING ITEM-PARAMS.                             */
		/* IF ITEM-STATUZ              NOT = O-K                        */
		/*    MOVE ITEM-PARAMS         TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                  */
		/* MOVE ITEM-GENAREA           TO T5688-T5688-REC.              */

		itempfList = itempfDAO.getItdmByFrmdate(wsspcomn.company.toString(), "T5688" ,sv.chdrtype.toString(),wsaaToday.toInt());


		if(null == itempfList || itempfList.size() < 1 ) {
			//syserrrec.params.set(itdmIO.getParams());
			//syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		t5688rec.t5688Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		if (isNE(t5688rec.nbusallw, "Y")) {
			sv.chdrtypeErr.set(errorsInner.f235);
		}
		/*    IF S5002-ACTION             = 'E'                            */
		if (isEQ(sv.action, "H")) {
			ftValidateT56712700();
		}
		return false;
	}

	protected void validateKey32230()
	{   
		if (isEQ(subprogrec.key3, SPACES)
				|| isEQ(subprogrec.key3, "N")) {
			return;
		}
		if (isNE(sv.chdrsel, SPACES)) {
			chdrenq = chdrpfDAO.getChdrenqRecord(wsspcomn.company.toString().trim(),sv.chdrsel.toString().trim());
		}
		else {
			sv.chdrselErr.set(errorsInner.e544);
		}
		if (chdrenq == null) {
			//fatalError600();
			return;	//ILIFE-6955
		}
		if (isEQ(sv.chdrselErr, SPACES)) {
			initialize(sdasancrec.sancRec);
			sdasancrec.function.set("VENTY");
			sdasancrec.userid.set(wsspcomn.userid);
			sdasancrec.entypfx.set(fsupfxcpy.chdr);
			sdasancrec.entycoy.set(wsspcomn.company);
			sdasancrec.entynum.set(sv.chdrsel);
			callProgram(Sdasanc.class, sdasancrec.sancRec);
			if (isNE(sdasancrec.statuz, varcom.oK)) {
				sv.chdrselErr.set(sdasancrec.statuz);
				return ;
			}
		}
		/*    For transactions on existing proposals,                      */
		/*       check the contract selected is of the correct status      */
		/*       and from correct branck.                                  */
		if (isEQ(sv.chdrselErr, SPACES)) {
			chdrlnbIO.setStatcode(chdrenq.getStatcode());
			checkStatus2400();
		}
	}

	protected void checkAlocno2300()
	{
		alocnorec.function.set("CHECK");
		alocnorec.prefix.set("CH");
		alocnorec.company.set(wsspcomn.company);
		wsaaBranch.set(wsspcomn.branch);
		alocnorec.genkey.set(wsaaGenkey);
		alocnorec.alocNo.set(sv.chdrsel);
		callProgram(Alocno.class, alocnorec.alocnoRec);
		if (isEQ(alocnorec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(alocnorec.statuz);
			fatalError600();
		}
		if (isNE(alocnorec.statuz, varcom.oK)) {
			sv.chdrselErr.set(alocnorec.statuz);
		}
	}

	protected void checkStatus2400()
	{

		readStatusTable2410(subprogrec.transcd);
	}
	protected void readStatusTable2410(FixedLengthStringData contitem)
	{  
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5679");
		itempf.setItemitem(contitem.toString());
		itempf = itempfDAO. getItempfRecord(itempf);

		if (null == itempf) {
			/*       MOVE F321                TO S5002-ACTION                  */
			sv.chdrselErr.set(errorsInner.f321);
			return ;
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		wsaaSub.set(ZERO);
		lookForStat2500();
	}

	protected void lookForStat2500()
	{  
		wsaaSub.add(1);
		foundFlag = false;
		while(isLTE(wsaaSub, 12)){

			if (isNE(chdrlnbIO.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				wsaaSub.add(1);
				continue;
			}else{
				foundFlag = true;
				break;
			}
		}
		if(!foundFlag){
			readStatusTable2410(t5679rec.contitem);
			if(!foundFlag){
				sv.chdrselErr.set(errorsInner.e767);
				wsspcomn.edterror.set("Y");
			}
		}
		/*EXIT*/
	}

	/**
	 * <pre>
	 *    Used to be 3200-ALLOCATE-NUMBER !!!
	 * </pre>
	 */
	protected void allocateNumber2600()
	{
		alocnorec.function.set("NEXT");
		alocnorec.prefix.set("CH");
		alocnorec.company.set(wsspcomn.company);
		wsaaBranch.set(wsspcomn.branch);
		alocnorec.genkey.set(wsaaGenkey);
		callProgram(Alocno.class, alocnorec.alocnoRec);
		if (isEQ(alocnorec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(alocnorec.statuz);
			fatalError600();
		}
		if (isNE(alocnorec.statuz, varcom.oK)) {
			sv.chdrselErr.set(alocnorec.statuz);
		}
		else {
			sv.chdrsel.set(alocnorec.alocNo);
		}
	}

	/**
	 * <pre>
	 *   The item key is the concatenation of four digits of the
	 *   program number (5105) and contract type and '*'.
	 * </pre>
	 */
	protected void ftValidateT56712700()
	{
		initialize2700();
		readTableT56712701();
		readT57042704();
		readT57052705();
	}

	protected void initialize2700()
	{
		wsaaProgname.set(subprogrec.nxt1prog);
		wsaaT5671Prognum.set(wsaaPrognum);
		wsaaT5671Cnttype.set(chdrlnbIO.getCnttype());
	}

	protected void readTableT56712701()
	{	

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5671");
		itempf.setItemitem(wsaaT5671Key.toString());
		itempf = itempfDAO. getItempfRecord(itempf);

		if (null == itempf) {
			sv.chdrtypeErr.set(errorsInner.f025);
		}
		else {
			t5671rec.t5671Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}

	/**
	 * <pre>
	 * Read Fast Track Conponent Rules table.
	 * </pre>
	 */
	protected void readT57042704()
	{   

		itempfList = itempfDAO.getItdmByFrmdate(wsspcomn.company.toString(),"T5704",sv.chdrtype.toString(),wsaaToday.toInt());


		if(null == itempfList || itempfList.size() < 1) {
			sv.chdrtypeErr.set(errorsInner.t027);
		}
	}

	/**
	 * <pre>
	 *  Read Fast Track Contract/Life default table
	 * </pre>
	 */
	protected void readT57052705()
	{   

		itempfList = itempfDAO.getItdmByFrmdate(wsspcomn.company.toString(),"T5705",chdrlnbIO.getCnttype().toString(),wsaaToday.toInt());


		if(null == itempfList || itempfList.size() < 1) {
			sv.chdrtypeErr.set(errorsInner.t029);
		}
	}

	protected void checkChdrsel2800()
	{
		wsaaChdrnumArray.set(sv.chdrsel);
		wsaaSub1.set(ZERO);
		wsaaSub2.set(ZERO);
		wsaaCharFound.set(SPACES);
		wsaaSpaceFound.set(SPACES);
		wsaaCharError.set(SPACES);
		wsaaNewStrn.set(SPACES);
		wsaaNewStrnArray.set(SPACES);
		while ( !(isEQ(wsaaSub1, 10))) {
			wsaaSub1.add(1);
			if (isNE(wsaaChdrnumField[wsaaSub1.toInt()], SPACES)) {
				if (isEQ(wsaaSpaceFound, SPACES)) {
					wsaaSub2.add(1);
					wsaaCharFound.set("Y");
					wsaaNewArray[wsaaSub2.toInt()].set(wsaaChdrnumField[wsaaSub1.toInt()]);
				}
				else {
					wsaaCharError.set("Y");
					wsaaSub1.set(10);
				}
			}
			else {
				if (isEQ(wsaaCharFound, "Y")
						&& isEQ(wsaaSpaceFound, SPACES)) {
					wsaaSpaceFound.set("Y");
				}
			}
		}

		if (isEQ(wsaaCharFound, "Y")
				&& isEQ(wsaaCharError, SPACES)) {
			wsaaNewStrn.set(wsaaNewStrnArray);
			if (isNE(wsaaNstr2, SPACES)) {
				wsspcomn.edterror.set("Y");
				sv.chdrselErr.set(errorsInner.f259);
			}
			else {
				sv.chdrsel.set(wsaaNstr1);
			}
		}
		if (isEQ(wsaaCharError, "Y")) {
			wsspcomn.edterror.set("Y");
			sv.chdrselErr.set(errorsInner.f259);
		}
	}

	protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd, "Y")) {
			sv.actionErr.set(errorsInner.e073);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			//goTo(GotoLabel.exit12090);
			return;
		}
	}

	protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

	/**
	 * <pre>
	 *     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	 * </pre>
	 */
	protected void update3000()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz, "BACH")) {
			//goTo(GotoLabel.exit3090);
			return;
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		/*  Update WSSP Key details*/
		wsspcomn.chdrChdrpfx.set(fsupfxcpy.chdr);
		wsspcomn.chdrChdrcoy.set(wsspcomn.company);
		wsspcomn.chdrChdrnum.set(sv.chdrsel);
		if (isEQ(sv.action, "A")) {
			wsspcomn.undAction.set(undactncpy.create1);
			wsspcomn.confirmationKey.set(SPACES);
			wsaaKeyProposal.set(sv.chdrsel.trim());
			wsaaKeyAction.set("created");
			wsspcomn.confirmationKey.set(wsaaKey);	//IBPLIFE-1718
		}
		else {
			if (isEQ(sv.action, "B")) {
				wsspcomn.undAction.set(undactncpy.modification1);
				wsspcomn.confirmationKey.set(SPACES);
				wsaaKeyProposal.set(sv.chdrsel.trim());
				wsaaKeyAction.set("modified");
				wsspcomn.confirmationKey.set(wsaaKey);	//IBPLIFE-1718
			}
			else {
				/*            IF S5002-ACTION = 'C' OR 'D'                         */
				if (isEQ(sv.action, "E")
						|| isEQ(sv.action, "F")) {
					wsspcomn.undAction.set(undactncpy.inquire1);
				}
				else {
					wsspcomn.undAction.set(SPACES);
				}
			}
		}
		/*    IF S5002-ACTION = 'C' OR 'D'*/
		if (isEQ(sv.action, "E")
				|| isEQ(sv.action, "F")
				|| isEQ(sv.action,"J"))  { //ILIFE-3139
			wsspcomn.flag.set("I");
			//goTo(GotoLabel.keeps3070);
			keeps3070();
			return;
		}
		else {
			if (isEQ(sv.action, "A")) {
				wsspcomn.flag.set("C");
			}
			else {
				if (isEQ(sv.action, "G")) {
					wsspcomn.flag.set("R");
				}
				else {
					if (isEQ(sv.action, "I")) {
						wsspcomn.flag.set("P");
					}
					else {
						if (isEQ(sv.action, "L")) {
							wsspcomn.flag.set("L");	//IBPLIFE-2472
						}
						else {
							wsspcomn.flag.set("M");
						}
					}
				}
			}
		}
		/*    Allocate new contract number if not entered.*/
		/*    ***** Now done in 2100-validate section ********             */
		/*IF S5002-CHDRSEL            = SPACES                         */
		/* AND SUBP-KEY1              = 'N'                            */
		/*   PERFORM 3200-ALLOCATE-NUMBER.                             */
		if (isNE(sv.errorIndicators, SPACES)) {
			//goTo(GotoLabel.batching3080);
			batching3080();
			return;
		}
		/*    Soft lock contract.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		if (isNE(sv.chdrsel, SPACES)) {
			/*    MOVE S5002-CHDRSEL       TO SFTL-ENTITY*/
			sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		}
		else {
			sftlockrec.entity.set(alocnorec.alocNo);
		}
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
				&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set(errorsInner.f910);
			wsspcomn.edterror.set("Y");
			//goTo(GotoLabel.exit3090);
			return;
		}
		/* THE FOLLOWING CODING REMOVED AS THE TRANNO SHOULD NOT BE     */
		/* INCREMENTED FOR EXISTING PROPOSALS.                          */
		/* For existing proposals, add one to the transaction number.   */
		/* IF SUBP-KEY1                = 'Y'                            */
		/*  AND S5002-ERROR-INDICATORS = SPACES                         */
		/*ADD 1                    TO CHDRLNB-TRANNO.               */
		/*    MOVE 1                   TO CHDRLNB-TRANNO.          <008>*/
		/*    ADD 1                    TO CHDRLNB-TRANNO.          <010>*/
		/*    For new contracts, initialise the header*/
		/*                   and create contract level follow-ups.*/
		if (isEQ(subprogrec.key1, "N")
				&& isEQ(sv.errorIndicators, SPACES)) {
			initialiseHeader3300();
		}
		if (isEQ(subprogrec.key1, "N")
				&& isEQ(sv.errorIndicators, SPACES)) {
			createFollowUps3400();
		}
		keeps3070();
		return;

	}

	protected void keeps3070()
	{
		/*   Store the contract header for use by the transaction programs*/
		/*   (For portfiolio enquiry, details already stored)              */
		/*    IF S5002-ACTION = 'D'                                        */
		if (isEQ(sv.action, "F")
				|| isEQ(sv.action,"J"))  { //ILIFE-3139
			//goTo(GotoLabel.batching3080);
			batching3080();
			return;
		}
		chdrlnbIO.setFunction("KEEPS");
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}

		batching3080();
		return;
	}

	protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd, "Y")
				&& isEQ(sv.errorIndicators, SPACES)) {
			updateBatchControl3100();
		}

		//ILIFE-3139-STARTS
		if(isEQ(sv.action,"J")
				&& isEQ(sv.errorIndicators,SPACES)) {
			callLetrqst3600();
		}
		//ILIFE-3139-ENDS
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}
	//ILIFE-3139-STARTS
	protected void callLetrqst3600() {
		wsaaLetrFnd.set(SPACES);

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("TR384");
		itempf.setItemitem(chdrlnbIO.cnttype.toString().concat(subprogrec.transcd.toString()));
		itempf = itempfDAO. getItempfRecord(itempf);



		if (null == itempf) {
			itempf=new Itempf();//ILIFE-5862
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl("TR384");
			itempf.setItemitem("***".concat(subprogrec.transcd.toString()));
			itempf = itempfDAO. getItempfRecord(itempf);
			if (null == itempf) {
				return;
			}
		}
		tr384rec.tr384Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		if(isEQ(tr384rec.letterType,SPACES)) {
			return;
		}
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrlnbIO.getChdrcoy());
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.clntcoy.set(chdrlnbIO.getCowncoy());
		letrqstrec.clntnum.set(chdrlnbIO.getCownnum());
		letrqstrec.letterRequestDate.set(wsaaToday);
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(chdrlnbIO.getChdrcoy());
		letrqstrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrlnbIO.getChdrnum());
		letrqstrec.rdocnum.set(chdrlnbIO.getChdrnum());
		letrqstrec.tranno.set(chdrlnbIO.getTranno());
		letrqstrec.branch.set(wsspcomn.branch);
		letrqstrec.despnum.set(chdrlnbIO.getDespnum());
		letrqstrec.trcde.set(subprogrec.transcd);
		letrqstrec.otherKeys.set(wsspcomn.language);
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz,varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
		wsaaLetrFnd.set("Y");
	}
	//ILIFE-3139-ENDS
	protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

	protected void initialiseHeader3300()
	{
		try {
			setup3310();
			setContractStatus3320();
			storeFirstLifeKey3340();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void setup3310()
	{
		chdrlnbIO.setDataKey(SPACES);
		chdrlnbIO.setNonKey(SPACES);
		chdrlnbIO.setChdrcoy(wsspcomn.company);
		if (isNE(sv.chdrsel, SPACES)) {
			chdrlnbIO.setChdrnum(sv.chdrsel);
		}
		else {
			chdrlnbIO.setChdrnum(alocnorec.alocNo);
		}
		chdrlnbIO.setInsttot01(ZERO);
		chdrlnbIO.setInsttot02(ZERO);
		chdrlnbIO.setInsttot03(ZERO);
		chdrlnbIO.setInsttot04(ZERO);
		chdrlnbIO.setInsttot05(ZERO);
		chdrlnbIO.setInsttot06(ZERO);
		chdrlnbIO.setSinstamt01(ZERO);
		chdrlnbIO.setSinstamt02(ZERO);
		chdrlnbIO.setSinstamt03(ZERO);
		chdrlnbIO.setSinstamt04(ZERO);
		chdrlnbIO.setSinstamt05(ZERO);
		chdrlnbIO.setSinstamt06(ZERO);
		chdrlnbIO.setChdrpfx("CH");
		chdrlnbIO.setServunit("LP");
		chdrlnbIO.setCnttype(sv.chdrtype);
		chdrlnbIO.setTranno(1);
		chdrlnbIO.setValidflag("3");
		chdrlnbIO.setCurrfrom(ZERO);
		chdrlnbIO.setStatdate(ZERO);
		chdrlnbIO.setStattran(ZERO);
		chdrlnbIO.setPstatdate(ZERO);
		chdrlnbIO.setPstattran(ZERO);
		chdrlnbIO.setTranlused(ZERO);
		chdrlnbIO.setOccdate(ZERO);
		chdrlnbIO.setCcdate(ZERO);
		chdrlnbIO.setBillcd(ZERO);
		chdrlnbIO.setBtdate(ZERO);
		chdrlnbIO.setPtdate(ZERO);
		chdrlnbIO.setSinstfrom(ZERO);
		chdrlnbIO.setSinstto(ZERO);
		chdrlnbIO.setInstfrom(ZERO);
		chdrlnbIO.setInstto(ZERO);
		chdrlnbIO.setInstfrom(ZERO);
		chdrlnbIO.setInstto(ZERO);
		chdrlnbIO.setPolinc(ZERO);
		chdrlnbIO.setPolsum(ZERO);
		chdrlnbIO.setStatementDate(ZERO);
		chdrlnbIO.setNxtsfx(ZERO);
		chdrlnbIO.setCurrto(varcom.vrcmMaxDate);
	}

	protected void setContractStatus3320()
	{  

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5679");
		itempf.setItemitem(subprogrec.transcd.toString());
		itempf = itempfDAO. getItempfRecord(itempf);

		if (null == itempf) {
			sv.action.set(errorsInner.f321);
			//goTo(GotoLabel.exit3390);
			return;
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		chdrlnbIO.setStatcode(t5679rec.setCnRiskStat);
		if (isEQ(chdrlnbIO.getBillfreq(), "00")) {
			chdrlnbIO.setPstatcode(t5679rec.setSngpCnStat);
		}
		else {
			chdrlnbIO.setPstatcode(t5679rec.setCnPremStat);
		}
	}

	protected void storeFirstLifeKey3340()
	{
		lifelnbIO.setDataKey(SPACES);
		lifelnbIO.setNonKey(SPACES);
		lifelnbIO.setChdrcoy(wsspcomn.company);
		lifelnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		lifelnbIO.setTranno(1);
		lifelnbIO.setValidflag("3");
		lifelnbIO.setCurrfrom(ZERO);
		lifelnbIO.setLifeCommDate(ZERO);
		lifelnbIO.setAnbAtCcd(ZERO);
		lifelnbIO.setCltdob(ZERO);
		lifelnbIO.setTransactionDate(ZERO);
		lifelnbIO.setTransactionTime(ZERO);
		lifelnbIO.setUser(ZERO);
		lifelnbIO.setCurrto(varcom.vrcmMaxDate);
		lifelnbIO.setFunction("KEEPS");
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
	}

	protected void createFollowUps3400()
	{
		try {
			chekIfRequired3410();
			readDefaultsTable3420();
			writeFollowUps3430();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void chekIfRequired3410()
	{
		if (isEQ(t5688rec.defFupMeth, SPACES)) {
			goTo(GotoLabel.exit3490);
		}
	}

	protected void readDefaultsTable3420()
	{
		wsaaT5677Tranno.set(subprogrec.transcd);
		wsaaT5677Followup.set(t5688rec.defFupMeth);


		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5677");
		itempf.setItemitem(wsaaT5677Key.toString());
		itempf = itempfDAO. getItempfRecord(itempf);
		if (null == itempf) {
			scrnparams.errorCode.set(errorsInner.g619);
			goTo(GotoLabel.exit3490);
			
		}
		t5677rec.t5677Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

	protected void writeFollowUps3430()
	{
		fupno = 0;
		for (wsaaX.set(1); !(isGT(wsaaX, 16)
				|| isNE(sv.errorIndicators, SPACES)); wsaaX.add(1)){
			writeFollowUp3500();
		}
		fluppfDAO.insertFlupRecord(fluplnbList);
	}

	protected void writeFollowUp3500()
	{
		try {
			fluppf = new Fluppf();
			lookUpStatus3510();
			lookUpDescription3520();
			writeRecord3530();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void lookUpStatus3510()
	{
		/*    IF T5677-FUPCODE (WSAA-X) = SPACES                           */
		if (isEQ(t5677rec.fupcdes[wsaaX.toInt()], SPACES)) {
			//goTo(GotoLabel.exit3590);
			return;
		}

		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(t5677rec.fupcdes[wsaaX.toInt()]);

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5661");
		itempf.setItemitem(wsaaT5661Key.toString());
		itempf = itempfDAO. getItempfRecord(itempf);


		if (null == itempf) {
			scrnparams.errorCode.set(errorsInner.e558);
			//goTo(GotoLabel.exit3590);
			return;
		}
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
	}

	protected void lookUpDescription3520()
	{ 
		/*    MOVE T5677-FUPCODE (WSAA-X) TO DESC-DESCITEM.                */
		/*    MOVE T5677-FUPCDES (WSAA-X) TO DESC-DESCITEM.        <LA4314>*/


		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(t5677rec.fupcdes[wsaaX.toInt()]);
		descpf=descDAO.getdescData("IT", "T5661", wsaaT5661Key.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());

	}

	protected void writeRecord3530()
	{
		fupno++;
		
		fluppf.setChdrcoy(wsspcomn.company.toString().charAt(0));
		fluppf.setChdrnum(chdrlnbIO.getChdrnum().toString());
		setPrecision(fupno, 0);
		fluppf.setFupNo(fupno);
		fluppf.setLife("01");
		fluppf.setTranNo(chdrlnbIO.getTranno().toInt());
		fluppf.setFupDt(wsaaToday.toInt());
		fluppf.setFupCde(t5677rec.fupcdes[wsaaX.toInt()].toString());
		fluppf.setFupTyp('P');
		fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
		fluppf.setFupRmk(descpf.getLongdesc());/* IJTI-1523 */
		fluppf.setjLife("00");
		fluppf.setTrdt(varcom.vrcmDate.toInt());
		fluppf.setTrtm(varcom.vrcmTime.toInt());
		fluppf.setUserT(varcom.vrcmUser.toInt());
		fluppf.setEffDate(wsaaToday.toInt());
		fluppf.setCrtDate(wsaaToday.toInt());
		fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
		fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
		fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
		fluplnbList.add(fluppf);
	}

	/**
	 * <pre>
	 *     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	 * </pre>
	 */
	protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
 		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}

	protected void exit2290()
	{
		//	return;
	}

	protected void checkChdr2215CustomerSpecific()
	{
		
	}
	
	public Subprogrec getSubprogrec() {
		return subprogrec;
	}

	public void setSubprogrec(Subprogrec subprogrec) {
		this.subprogrec = subprogrec;
	}
	public Sdasancrec getSdasancrec() {
		return sdasancrec;
	}

	public void setSdasancrec(Sdasancrec sdasancrec) {
		this.sdasancrec = sdasancrec;
	}
	public ChdrlnbTableDAM getChdrlnbIO() {
		return chdrlnbIO;
	}

	public void setChdrlnbIO(ChdrlnbTableDAM chdrlnbIO) {
		this.chdrlnbIO = chdrlnbIO;
	}
	public Fsupfxcpy getFsupfxcpy() {
		return fsupfxcpy;
	}

	public void setFsupfxcpy(Fsupfxcpy fsupfxcpy) {
		this.fsupfxcpy = fsupfxcpy;
	}
	public PackedDecimalData getWsaaX() {
		return wsaaX;
	}

	public void setWsaaX(PackedDecimalData wsaaX) {
		this.wsaaX = wsaaX;
	}
	public T5677rec getT5677rec() {
		return t5677rec;
	}

	public void setT5677rec(T5677rec t5677rec) {
		this.t5677rec = t5677rec;
	}
	public FixedLengthStringData getWsaaT5661Lang() {
		return wsaaT5661Lang;
	}

	public void setWsaaT5661Lang(FixedLengthStringData wsaaT5661Lang) {
		this.wsaaT5661Lang = wsaaT5661Lang;
	}
	public FixedLengthStringData getWsaaT5661Fupcode() {
		return wsaaT5661Fupcode;
	}

	public void setWsaaT5661Fupcode(FixedLengthStringData wsaaT5661Fupcode) {
		this.wsaaT5661Fupcode = wsaaT5661Fupcode;
	}
	public FixedLengthStringData getWsaaT5661Key() {
		return wsaaT5661Key;
	}

	public void setWsaaT5661Key(FixedLengthStringData wsaaT5661Key) {
		this.wsaaT5661Key = wsaaT5661Key;
	}
	public T5661rec getT5661rec() {
		return t5661rec;
	}

	public void setT5661rec(T5661rec t5661rec) {
		this.t5661rec = t5661rec;
	}
	public PackedDecimalData getWsaaToday() {
		return wsaaToday;
	}

	public void setWsaaToday(PackedDecimalData wsaaToday) {
		this.wsaaToday = wsaaToday;
	}
	public Descpf getDescpf() {
		return descpf;
	}

	public void setDescpf(Descpf descpf) {
		this.descpf = descpf;
	}
	public String getWsaaAutonum() {
		return wsaaAutonum;
	}

	public void setWsaaAutonum(String wsaaAutonum) {
		this.wsaaAutonum = wsaaAutonum;
	}
	public boolean isExitFlag() {
		return exitFlag;
	}

	public void setExitFlag(boolean exitFlag) {
		this.exitFlag = exitFlag;
	}
	public Datcon1rec getDatcon1rec() {
		return datcon1rec;
	}

	public void setDatcon1rec(Datcon1rec datcon1rec) {
		this.datcon1rec = datcon1rec;
	}

	/* IBPTE-1239 START */
	private String getMessage(String errorCode, String errorMessage) {
		errmesgrec.eror.set(errorCode);
		errmesgrec.language.set(scrnparams.language);
		errmesgrec.erorProg.set(wsaaScreen);
		errmesgrec.company.set(scrnparams.company);
		errmesgrec.function.clear();
		callProgram(Errmesg.class, errmesgrec.errmesgRec);
		if (isEQ(errmesgrec.statuz, "BOMB")) {
			scrnparams.statuz.set(errmesgrec.statuz);
			screenErrors200();
		}
		return errorMessage + ". " + errmesgrec.errorline.trim();
	}

	/* IBPTE-1239 END */
	/*
	 * Class transformed  from Data Structure ERRORS--INNER
	 */
	private static final class ErrorsInner {
		/* ERRORS */
		private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
		private FixedLengthStringData e073 = new FixedLengthStringData(4).init("E073");
		private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
		private FixedLengthStringData e455 = new FixedLengthStringData(4).init("E455");
		private FixedLengthStringData e544 = new FixedLengthStringData(4).init("E544");
		private FixedLengthStringData e558 = new FixedLengthStringData(4).init("E558");
		private FixedLengthStringData e767 = new FixedLengthStringData(4).init("E767");
		private FixedLengthStringData f025 = new FixedLengthStringData(4).init("F025");
		private FixedLengthStringData f235 = new FixedLengthStringData(4).init("F235");
		private FixedLengthStringData f321 = new FixedLengthStringData(4).init("F321");
		private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
		private FixedLengthStringData f918 = new FixedLengthStringData(4).init("F918");
		private FixedLengthStringData g619 = new FixedLengthStringData(4).init("G619");
		private FixedLengthStringData t027 = new FixedLengthStringData(4).init("T027");
		private FixedLengthStringData t029 = new FixedLengthStringData(4).init("T029");
		private FixedLengthStringData f259 = new FixedLengthStringData(4).init("F259");
		private FixedLengthStringData rulm = new FixedLengthStringData(4).init("RULM");
		private FixedLengthStringData ruln = new FixedLengthStringData(4).init("RULN");
		private FixedLengthStringData ruoq = new FixedLengthStringData(4).init("RUOQ");
		private FixedLengthStringData f917 = new FixedLengthStringData(4).init("F917");
	}
}