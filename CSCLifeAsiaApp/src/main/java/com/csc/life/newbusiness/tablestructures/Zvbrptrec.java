package com.csc.life.newbusiness.tablestructures;

import com.csc.common.DD;
import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.quipoz.COBOLFramework.COBOLFunctions;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Web, 3 July 2013 
 * Description:
 * Copybook name: zvbrptrec
 *
 * Copyright (2013) CSC Asia, all rights reserved
 *
 */
public class Zvbrptrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData zvbrptrec = new FixedLengthStringData(81);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(zvbrptrec, 0);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(zvbrptrec, 5);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(zvbrptrec, 6);
  	public FixedLengthStringData clntcoy = new FixedLengthStringData(8).isAPartOf(zvbrptrec, 14);
	public FixedLengthStringData clntnum = new FixedLengthStringData(8).isAPartOf(zvbrptrec, 22);
	public FixedLengthStringData issuebnk = new FixedLengthStringData(8).isAPartOf(zvbrptrec, 30);
  	public PackedDecimalData sinstamt = new PackedDecimalData(17,2).isAPartOf(zvbrptrec, 38);
  	public PackedDecimalData exprdate = new PackedDecimalData(8,0).isAPartOf(zvbrptrec, 47);
  	public FixedLengthStringData billchnl = new FixedLengthStringData(2).isAPartOf(zvbrptrec, 51);
	public FixedLengthStringData billcurr = new FixedLengthStringData(3).isAPartOf(zvbrptrec, 53);
	public FixedLengthStringData rectype = new FixedLengthStringData(3).isAPartOf(zvbrptrec, 56);
	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(zvbrptrec, 59);
	public FixedLengthStringData zvrtbnkac = new FixedLengthStringData(18).isAPartOf(zvbrptrec, 63);
  	

	public void initialize() {
		COBOLFunctions.initialize(zvbrptrec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			zvbrptrec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}