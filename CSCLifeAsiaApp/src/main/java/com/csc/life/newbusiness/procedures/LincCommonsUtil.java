/**
 * 
 */
package com.csc.life.newbusiness.procedures;

import java.math.BigDecimal;
import java.util.Map;
import com.csc.life.contractservicing.recordstructures.LincpfHelper;
import com.csc.life.newbusiness.dataaccess.model.Lincpf;
import com.csc.life.newbusiness.dataaccess.model.Slncpf;

/**
 * Common utility processing for LINC.
 * 
 * @author gsaluja2
 *
 */
public interface LincCommonsUtil {
	/**
	 * Calculates Life Assured's Age based on effective date.
	 * 
	 * @param lincpfHelper
	 * @param effDate
	 */
	public void validateLifeAssuredAge(LincpfHelper lincpfHelper, int effDate);
	/**
	 * Calculates difference between two dates (in years).
	 * 
	 * @param occdate
	 * @param transactionDate
	 * @return BigDecimal object
	 */
	public BigDecimal getTermDiff(int occdate, int transactionDate);
	/**
	 * Checks for LINC criteria basis TJL06.
	 * 
	 * @param crtableSuminsMap
	 * @param lincpfHelper
	 */
	public void checkCriteria(Map<String, BigDecimal> crtableSuminsMap, LincpfHelper lincpfHelper);
	/**
	 * Sets up data to be written to LINCPF.
	 * 
	 * @param lincpfHelper
	 * @param lincpf
	 * @return Lincpf object
	 */
	public Lincpf setupLincpf(LincpfHelper lincpfHelper, Lincpf lincpf);
	/**
	 * Sets up common fields from various services which needs to be written to LINCPF.
	 * 
	 * @param lincpf
	 * @param lincpfHelper
	 */
	public void setupCommonLinc(Lincpf lincpf, LincpfHelper lincpfHelper);
	/**
	 * Sets up common date fields from various services which needs to be written to LINCPF.
	 * 
	 * @param lincpf
	 * @param effdate
	 * @param occdate
	 * @param dtetrmntd
	 * @param ztrdate
	 * @param zadrddte
	 * @param zexdrddte
	 */
	public void setupLincDates(Lincpf lincpf, int effdate, int occdate, int dtetrmntd, 
			int ztrdate, int zadrddte, int zexdrddte);
	/**
	 * Sets up data to be written to SLNCPF.
	 * 
	 * @param lincpf
	 * @param slncpf
	 * @return Slncpf object
	 */
	public Slncpf setupSlncpf(Lincpf lincpf, Slncpf slncpf);
	/**
	 * Generates follow up.
	 * 
	 * @param lincpfHelper
	 */
	public void generateFollowUp(LincpfHelper lincpfHelper);
	/**
	 * Checks for errors associated with LINC.
	 * 
	 * @param chdrnum
	 * @return String
	 */
	public String validateLincRecord(String chdrnum);
	/**
	 * Checks if data in LINCPF needs to be updated. 
	 * 
	 * @param lincpf
	 * @param lincpfHelper
	 * @return boolean
	 */
	public boolean validateDataForUpdate(Lincpf lincpf, LincpfHelper lincpfHelper);
}
