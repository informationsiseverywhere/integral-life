package com.csc.life.newbusiness.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RT501.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:52
 * @author Quipoz
 */
public class Rt501Report extends SMARTReportLayout { 

	private FixedLengthStringData chdrno = new FixedLengthStringData(8);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData currcode = new FixedLengthStringData(3);
	private FixedLengthStringData currencynm = new FixedLengthStringData(30);
	private ZonedDecimalData gcnt = new ZonedDecimalData(5, 0);
	private FixedLengthStringData linsname = new FixedLengthStringData(47);
	private ZonedDecimalData linstamt = new ZonedDecimalData(11, 2);
	private ZonedDecimalData loannumber = new ZonedDecimalData(2, 0);
	private FixedLengthStringData loanstdate = new FixedLengthStringData(10);
	private FixedLengthStringData loantype = new FixedLengthStringData(1);
	private FixedLengthStringData occdate = new FixedLengthStringData(10);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData plint = new ZonedDecimalData(11, 2);
	private FixedLengthStringData repdate = new FixedLengthStringData(10);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private ZonedDecimalData stpdty = new ZonedDecimalData(9, 2);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData zbamt = new ZonedDecimalData(11, 2);
	private ZonedDecimalData ztamt = new ZonedDecimalData(11, 2);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rt501Report() {
		super();
	}


	/**
	 * Print the XML for Rt501d01
	 */
	public void printRt501d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(91).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrno.setFieldName("chdrno");
		chdrno.setInternal(subString(recordData, 1, 8));
		linsname.setFieldName("linsname");
		linsname.setInternal(subString(recordData, 9, 47));
		cnttype.setFieldName("cnttype");
		cnttype.setInternal(subString(recordData, 56, 3));
		occdate.setFieldName("occdate");
		occdate.setInternal(subString(recordData, 59, 10));
		loannumber.setFieldName("loannumber");
		loannumber.setInternal(subString(recordData, 69, 2));
		loantype.setFieldName("loantype");
		loantype.setInternal(subString(recordData, 71, 1));
		loanstdate.setFieldName("loanstdate");
		loanstdate.setInternal(subString(recordData, 72, 10));
		linstamt.setFieldName("linstamt");
		linstamt.setInternal(subString(recordData, 82, 11));
		zbamt.setFieldName("zbamt");
		zbamt.setInternal(subString(recordData, 93, 11));
		plint.setFieldName("plint");
		plint.setInternal(subString(recordData, 104, 11));
		ztamt.setFieldName("ztamt");
		ztamt.setInternal(subString(recordData, 115, 11));
		stpdty.setFieldName("stpdty");
		stpdty.setInternal(subString(recordData, 126, 9));
		printLayout("Rt501d01",			// Record name
			new BaseData[]{			// Fields:
				chdrno,
				linsname,
				cnttype,
				occdate,
				loannumber,
				loantype,
				loanstdate,
				linstamt,
				zbamt,
				plint,
				ztamt,
				stpdty
			}
			, new Object[] {			// indicators
				new Object[]{"ind91", indicArea.charAt(91)}
			}
		);

	}

	/**
	 * Print the XML for Rt501h01
	 */
	public void printRt501h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 1, 10));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 11, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 12, 30));
		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 42, 10));
		time.setFieldName("time");
		time.set(getTime());
		currcode.setFieldName("currcode");
		currcode.setInternal(subString(recordData, 52, 3));
		currencynm.setFieldName("currencynm");
		currencynm.setInternal(subString(recordData, 55, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rt501h01",			// Record name
			new BaseData[]{			// Fields:
				sdate,
				company,
				companynm,
				repdate,
				time,
				currcode,
				currencynm,
				pagnbr
			}
		);

		currentPrintLine.set(7);
	}

	/**
	 * Print the XML for Rt501t01
	 */
	public void printRt501t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(1).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		gcnt.setFieldName("gcnt");
		gcnt.setInternal(subString(recordData, 1, 5));
		linstamt.setFieldName("linstamt");
		linstamt.setInternal(subString(recordData, 6, 11));
		zbamt.setFieldName("zbamt");
		zbamt.setInternal(subString(recordData, 17, 11));
		plint.setFieldName("plint");
		plint.setInternal(subString(recordData, 28, 11));
		ztamt.setFieldName("ztamt");
		ztamt.setInternal(subString(recordData, 39, 11));
		stpdty.setFieldName("stpdty");
		stpdty.setInternal(subString(recordData, 50, 9));
		printLayout("Rt501t01",			// Record name
			new BaseData[]{			// Fields:
				gcnt,
				linstamt,
				zbamt,
				plint,
				ztamt,
				stpdty
			}
			, new Object[] {			// indicators
				new Object[]{"ind01", indicArea.charAt(1)}
			}
		);

		currentPrintLine.add(1);
	}


}
