/******************************************************************************
 * File Name 		: Ackdpf.java
 * Author			: sbatra9
 * Creation Date	: 17 April 2020
 * Project			: Integral Life
 * Description		: The Model Class for ACKDPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.newbusiness.dataaccess.model;

import java.util.Date;
import java.io.Serializable;

public class Ackdpf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	// Constant for database table name
	public static final String TABLE_NAME = "ACKDPF";

	// member variables for columns
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String dlvrmode;
	private int despdate;
	private int packdate;
	private int deemdate;
	private int nextActDate;
	private long hpaduqn;

	// Constructor
	public Ackdpf() {
		//Empty constructor
	}

	//Get Methods
	
	public long getUniqueNumber() {
		return this.uniqueNumber;
	}

	public String getChdrcoy() {
		return this.chdrcoy;
	}
	
	public String getChdrnum() {
		return this.chdrnum;
	}
	
	public String getDlvrmode() {
		return this.dlvrmode;
	}
	
	public int getDespdate() {
		return this.despdate;
	}
	
	public int getPackdate() {
		return this.packdate;
	}
	
	public int getDeemdate() {
		return this.deemdate;
	}

	public int getNextActDate() {
		return this.nextActDate;
	}
	
	//Set Methods
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public void setDlvrmode(String dlvrmode) {
		this.dlvrmode = dlvrmode;
	}

	public void setDespdate(int despdate) {
		this.despdate = despdate;
	}

	public void setPackdate(int packdate) {
		this.packdate = packdate;
	}
	
	public void setDeemdate(int deemdate) {
		this.deemdate = deemdate;
	}

	public void setNextActDate(int nextActDate) {
		this.nextActDate = nextActDate;
	}

	public long getHpaduqn() {
		return hpaduqn;
	}

	public void setHpaduqn(long hpaduqn) {
		this.hpaduqn = hpaduqn;
	}
	
	
}
