package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:18
 * Description:
 * Copybook name: COMPREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Comprec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData compRec = new FixedLengthStringData(266);
  	public FixedLengthStringData compEntid = new FixedLengthStringData(8).isAPartOf(compRec, 0);
  	public FixedLengthStringData compParent = new FixedLengthStringData(8).isAPartOf(compRec, 8);
  	public FixedLengthStringData compMortcls = new FixedLengthStringData(1).isAPartOf(compRec, 16);
  	public ZonedDecimalData compPcessage = new ZonedDecimalData(2, 0).isAPartOf(compRec, 17).setUnsigned();
  	public ZonedDecimalData compPcesstrm = new ZonedDecimalData(2, 0).isAPartOf(compRec, 19).setUnsigned();
  	public ZonedDecimalData compRcessage = new ZonedDecimalData(2, 0).isAPartOf(compRec, 21).setUnsigned();
  	public ZonedDecimalData compRcesstrm = new ZonedDecimalData(2, 0).isAPartOf(compRec, 23).setUnsigned();
  	public ZonedDecimalData compSumin = new ZonedDecimalData(15, 0).isAPartOf(compRec, 25).setUnsigned();
  	public ZonedDecimalData compInstprm = new ZonedDecimalData(18, 2).isAPartOf(compRec, 40);
  	public FixedLengthStringData compLife = new FixedLengthStringData(2).isAPartOf(compRec, 58);
  	public FixedLengthStringData compCoverage = new FixedLengthStringData(2).isAPartOf(compRec, 60);
  	public FixedLengthStringData compRider = new FixedLengthStringData(2).isAPartOf(compRec, 62);
  	public FixedLengthStringData compCrtable = new FixedLengthStringData(4).isAPartOf(compRec, 64);
  	public ZonedDecimalData compLmpcnt = new ZonedDecimalData(13, 2).isAPartOf(compRec, 68).setUnsigned();
  	public FixedLengthStringData compRsunin = new FixedLengthStringData(1).isAPartOf(compRec, 81);
  	public ZonedDecimalData compRundte = new ZonedDecimalData(8, 0).isAPartOf(compRec, 82).setUnsigned();
  	public ZonedDecimalData filler = new ZonedDecimalData(8, 0).isAPartOf(compRundte, 0, FILLER_REDEFINE);
  	public ZonedDecimalData compRundteCcyy = new ZonedDecimalData(4, 0).isAPartOf(filler, 0).setUnsigned();
  	public ZonedDecimalData compRundteMm = new ZonedDecimalData(2, 0).isAPartOf(filler, 4).setUnsigned();
  	public ZonedDecimalData compRundteDd = new ZonedDecimalData(2, 0).isAPartOf(filler, 6).setUnsigned();
  	public FixedLengthStringData[] s5013Sfl = FLSArrayPartOfStructure(8, 22, compRec, 90);
  	public ZonedDecimalData[] compAgerate = ZDArrayPartOfArrayStructure(4, 0, s5013Sfl, 0);
  	public ZonedDecimalData[] compEcestrm = ZDArrayPartOfArrayStructure(3, 0, s5013Sfl, 4, UNSIGNED_TRUE);
  	public ZonedDecimalData[] compInsprm = ZDArrayPartOfArrayStructure(6, 0, s5013Sfl, 7, UNSIGNED_TRUE);
  	public FixedLengthStringData[] compOpcda = FLSDArrayPartOfArrayStructure(2, s5013Sfl, 13);
  	public ZonedDecimalData[] compOppc = ZDArrayPartOfArrayStructure(5, 2, s5013Sfl, 15, UNSIGNED_TRUE);
  	public FixedLengthStringData[] compReasind = FLSDArrayPartOfArrayStructure(1, s5013Sfl, 20);
  	public FixedLengthStringData[] compSelect = FLSDArrayPartOfArrayStructure(1, s5013Sfl, 21);


	public void initialize() {
		COBOLFunctions.initialize(compRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		compRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}