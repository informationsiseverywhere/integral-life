package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:33
 * Description:
 * Copybook name: T5657REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5657rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5657Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData agerate = new ZonedDecimalData(3, 0).isAPartOf(t5657Rec, 0);
  	public ZonedDecimalData insprm = new ZonedDecimalData(6, 0).isAPartOf(t5657Rec, 3);
  	public ZonedDecimalData oppc = new ZonedDecimalData(5, 2).isAPartOf(t5657Rec, 9);
  	public FixedLengthStringData sbstdl = new FixedLengthStringData(1).isAPartOf(t5657Rec, 14);
  	public FixedLengthStringData tabtype = new FixedLengthStringData(1).isAPartOf(t5657Rec, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(484).isAPartOf(t5657Rec, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5657Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5657Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}