/*
 * File: Br629.java
 * Date: 29 August 2009 22:29:45
 * Author: Quipoz Limited
 *
 * Class transformed from BR629.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.csc.fsu.clients.procedures.CltrelnDTO;
import com.csc.fsu.clients.procedures.CltrelnUtils;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.life.newbusiness.dataaccess.HpaxpfTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.Br629TempDAO;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Br629DTO;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.newbusiness.reports.Rr629Report;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   This program is responsible for performing the processing
*   for the automate change of policy owner to life assured
*   when the life assured attained a pre-defined age either on
*   the exact birthday age or policy anniversary depending on
*   the product set up on table TR627.
*
*
*   PROCESSING
*   ==========
*
*   Initialise
*
*  - Check the restart method is 3.
*  - Find the name of the HPAX temporary file. HPAX is a temporary
*    file which has the name of "HPAX" + first two characters of
*    param 4 + the last four digits of the job number.
*  - Open HPAXPF as input.
*  - Open Printer file.
*  - Set up report header.
*  - Load table T5679 to check the contract status.
*
*  Read.
*
*  - Read a HPAXPF.
*  - Check end of file and end processing
*
*  Edit.
*
*  - Read Contract Header.
*  - Check the contract status against T5679.
*    If status is not specified in T5679, read next HPAXPF.
*
*  Update.
*
*  - Set up the contract header for the report.
*  - Get Policy Owner name
*  - Get New Owner name and Life Assured name
*  - Get the age and effective date of ownership changed
*  - Remove policy owner client role and add the new owner
*    client role.
*  - Update contract header from old owner to new owner
*  - Write a transaction history.
*
*  Finalise.
*
*  - Close HPAXPF
*  - Close Printer file
*  - Remove the override
*  - Set the parameter statuz to O-K
*
*
*   Control totals:
*     01 - No. of HPAX records read
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
***********************************************************************
* </pre>
*/
public class Br629 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private HpaxpfTableDAM hpaxpf = new HpaxpfTableDAM();
	private Rr629Report printerFile = new Rr629Report();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR629");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaNumber = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1);
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");
		/* WSAA-TRANS-INFO */
	private ZonedDecimalData wsaaDate = new ZonedDecimalData(6, 0).setUnsigned();
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(6, 0).setUnsigned();

	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTranid, 0);

	private FixedLengthStringData wsaaHpaxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaHpaxFn, 0, FILLER).init("HPAX");
	private FixedLengthStringData wsaaHpaxRunid = new FixedLengthStringData(2).isAPartOf(wsaaHpaxFn, 4);
	private ZonedDecimalData wsaaHpaxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaHpaxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");


	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);

		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;

	private FixedLengthStringData rr629H01 = new FixedLengthStringData(83);
	private FixedLengthStringData rr629h01O = new FixedLengthStringData(83).isAPartOf(rr629H01, 0);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rr629h01O, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rr629h01O, 10);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rr629h01O, 11);
	private FixedLengthStringData rh01Repdate = new FixedLengthStringData(10).isAPartOf(rr629h01O, 41);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(rr629h01O, 51);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(rr629h01O, 53);

	private FixedLengthStringData rr629E01 = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private T5679rec t5679rec = new T5679rec();
	private Bldenrlrec bldenrlrec = new Bldenrlrec();
	private Rr629D01Inner rr629D01Inner = new Rr629D01Inner();
	//fwang3 
    private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
    private Br629TempDAO tempDAO = getApplicationContext().getBean("br629TempDAO", Br629TempDAO.class);
    private ChdrpfDAO chdrDAO = getApplicationContext().getBean("chdrDAO", ChdrpfDAO.class);
    private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
    private HpadpfDAO hpadpfDAO =  getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);

	private List<Br629DTO> br629DtoList = new LinkedList<Br629DTO>();
	private Iterator<Br629DTO> br629DtoIter;
	private List<Chdrpf> chdrpfList = new LinkedList<Chdrpf>();
	private List<Chdrpf> chdrpfListInsert = new LinkedList<Chdrpf>();
	private List<Ptrnpf> ptrnpfList = new LinkedList<Ptrnpf>();
	private List<Hpadpf> hpadpfList = new LinkedList<Hpadpf>();
	
    private int batchID = 0;
    private int batchExtractSize;
	private Br629DTO br629Dto;
	
	private int ctrCT01; 
	private int ctrCT02;
	
	private CltrelnUtils cltrelnUtils = getApplicationContext().getBean("cltrelnUtils", CltrelnUtils.class);
	private CltrelnDTO cltrelnDTO = null;
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private Descpf descpf = null;
	
	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	
	public Br629() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}

	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}

	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray) {
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void restart0900() {
		/* RESTART */
		/* EXIT */
	}

	protected void initialise1000() {
		wsspEdterror.set(varcom.oK);
		wsaaNumber.set(ZERO);
		wsaaForenum.set(SPACES);
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				batchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				batchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			batchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
		
		wsaaHpaxRunid.set(bprdIO.getSystemParam04());
		wsaaHpaxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		
		// fwang3
		tempDAO.buildTempData(wsaaHpaxFn.toString(), fsupfxcpy.clnt.toString(), bsprIO.getFsuco().toString());
		br629DtoList = tempDAO.findTempDataResult(batchExtractSize, batchID);
		
        if (br629DtoList != null && br629DtoList.size() > 0) {
            br629DtoIter = br629DtoList.iterator();
        } else {
            wsspEdterror.set(varcom.endp);
            return;
        }
		
		printerFile.openOutput();
		
		setUpHeadingCompany1020();
		setUpHeadingBranch1030();
		setUpHeadingDates1040();
		readT56791050();
		ctrCT01 = 0;
		ctrCT02 = 0;
	}
	protected void setUpHeadingCompany1020() {
		descpf = descDAO.getdescData("IT", "T1693" ,bsprIO.getCompany().toString(),"0", bsscIO.getLanguage().toString());
		if (descpf == null) {
			fatalError600();
		}
		rh01Company.set(bsprIO.getCompany());
		rh01Companynm.set(descpf.getLongdesc());
	}

	protected void setUpHeadingBranch1030() {
		descpf = descDAO.getdescData("IT", "T1692" ,bsprIO.getDefaultBranch().toString(),bsprIO.getCompany().toString(), bsscIO.getLanguage().toString());
		if (descpf == null) {
			fatalError600();
		}
		rh01Branch.set(bprdIO.getDefaultBranch());
		rh01Branchnm.set(descpf.getLongdesc());
	}

	protected void setUpHeadingDates1040() {
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(bsscIO.getEffectiveDate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Repdate.set(datcon1rec.extDate);
	}

	protected void readT56791050() {
		/* Read T5679, later used it for checking the policy status */
		Itempf itempf = new Itempf();
	    itempf.setItempfx("IT");
		itempf.setItemcoy(bsprIO.getCompany().toString());
		itempf.setItemtabl("T5679");
		itempf.setItemitem(bprdIO.getAuthCode().toString());
	    itempf = itemDAO.getItempfRecord(itempf);
	    if(itempf == null){
	    	fatalError600();
	    }
	    t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}


	protected void readFile2000() {
		// fwang3
		if (br629DtoIter.hasNext()) {
			br629Dto = br629DtoIter.next();
		} else {
            //minRecord += incrRange;
			batchID++;
            br629DtoList = tempDAO.findTempDataResult(batchExtractSize, batchID);
            if (br629DtoList != null && br629DtoList.size() > 0) {
            	br629DtoIter = br629DtoList.iterator();
            	br629Dto = br629DtoIter.next();
            } else {
                wsspEdterror.set(varcom.endp);
                return;
            }
		}
		ctrCT01++;
	}

	protected void edit2500() {
		wsspEdterror.set(varcom.oK);
		/* Check contract status against T5679 for valid statii. */
		wsaaValidStatus.set("N");
		for (ix.set(1); !(isGT(ix, 12) || validStatus.isTrue()); ix.add(1)) {
			if (isEQ(t5679rec.cnRiskStat[ix.toInt()], br629Dto.getStatcode()/*chdrlnbIO.getStatcode()*/)) {
				for (ix.set(1); !(isGT(ix, 12) || validStatus.isTrue()); ix.add(1)) {
					if (isEQ(t5679rec.cnPremStat[ix.toInt()], br629Dto.getPstcde()/*chdrlnbIO.getPstatcode()*/)) {
						wsaaValidStatus.set("Y");
						ix.set(13);
					}
				}
			}
		}
		
		if (!validStatus.isTrue()) {
			wsspEdterror.set(SPACES);
			return;
		}
	}

	protected void update3000() {
		initialize(rr629D01Inner.rr629d01O);
		wsaaNumber.add(1);
		rr629D01Inner.zseqno.set(wsaaNumber);
//		rr629D01Inner.chdrnum.set(chdrlnbIO.getChdrnum());
//		rr629D01Inner.cownum.set(chdrlnbIO.getCownnum());
//		rr629D01Inner.cnttype.set(chdrlnbIO.getCnttype());
		//fwang3
		rr629D01Inner.chdrnum.set(br629Dto.getChdrnum());
		rr629D01Inner.cownum.set(br629Dto.getCownnum());
		rr629D01Inner.cnttype.set(br629Dto.getCnttype());
		
		/* Contract Commencement Date */
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.conv);
//		datcon1rec.intDate.set(chdrlnbIO.getOccdate());
		datcon1rec.intDate.set(br629Dto.getOccdate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rr629D01Inner.occdate.set(datcon1rec.extDate);
		/* Get Effective Date of Ownership changed */
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.conv);
//		datcon1rec.intDate.set(hpaxpfRec.zsufcdte);
		datcon1rec.intDate.set(br629Dto.getZsufcdte());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rr629D01Inner.effdates.set(datcon1rec.extDate);
		oldOwnerName3100();
		newOwner3200();
		updateOwner3700();
		updateProcflag3800();
		callBldenrl3900();
		if (newPageReq.isTrue()) {
			printerFile.printRr629h01(rr629H01, indicArea);
			wsaaOverflow.set("N");
		}
		printerFile.printRr629d01(rr629D01Inner.rr629D01, indicArea);
		ctrCT02++;
	}

	protected void oldOwnerName3100() {
		/* Get the owner name before ownership transfer */
		rr629D01Inner.rname.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(br629Dto.getCl1lsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(br629Dto.getCl1lgivname(), "  ");
		stringVariable1.setStringInto(rr629D01Inner.rname);
	}

	protected void newOwner3200() {
		/* Get the new owner name after ownership transfer */
		rr629D01Inner.lname.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(br629Dto.getCl2lsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(br629Dto.getCl2lgivname(), "  ");
		stringVariable1.setStringInto(rr629D01Inner.lname);
		rr629D01Inner.cownnum.set(br629Dto.getLifcnum());
		rr629D01Inner.lifcnum.set(br629Dto.getLifcnum());
		rr629D01Inner.clntlname.set(rr629D01Inner.lname);
		/* Get New Owner Date of Birth */
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(br629Dto.getCl2cltdob());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rr629D01Inner.cltdob.set(datcon1rec.extDate);
	}

	protected void updateOwner3700() {
		start3710();
		rewriteChdr3750();
		writePtrn3770();
	}

	protected void start3710() {
		/* Remove owner role */
		cltrelnDTO = new CltrelnDTO();
		cltrelnDTO.setClntpfx(br629Dto.getCownpfx());
		cltrelnDTO.setClntcoy(br629Dto.getCowncoy());
		cltrelnDTO.setClntnum(br629Dto.getCownnum());
		cltrelnDTO.setFunction("REM");
		cltrelnDTO.setClrrrole("OW");
		cltrelnDTO.setForepfx(fsupfxcpy.chdr.toString());
		cltrelnDTO.setForecoy(br629Dto.getChdrcoy());
		wsaaForenum.set(br629Dto.getChdrnum());
		cltrelnDTO.setForenum(wsaaForenum.toString());
		cltrelnDTO = cltrelnUtils.removeRole(cltrelnDTO);
		if (!"****".equals(cltrelnDTO.getStatus())) {
			fatalError600();
		}
		/* Add new role for owner */
		
		cltrelnDTO = new CltrelnDTO();
		cltrelnDTO.setClntpfx(fsupfxcpy.clnt.toString());
		cltrelnDTO.setClntcoy(bsprIO.getFsuco().toString());
		cltrelnDTO.setClntnum(br629Dto.getLifcnum());
		cltrelnDTO.setFunction("ADD");
		cltrelnDTO.setClrrrole("OW");
		cltrelnDTO.setForepfx(fsupfxcpy.chdr.toString());
		cltrelnDTO.setForecoy(br629Dto.getChdrcoy());
		wsaaForenum.set(br629Dto.getChdrnum());
		cltrelnDTO.setForenum(wsaaForenum.toString());
		cltrelnDTO = cltrelnUtils.addRole(cltrelnDTO);
		if (!"****".equals(cltrelnDTO.getStatus())) {
			fatalError600();
		}
		/* Remove despatch role */
		
		cltrelnDTO = new CltrelnDTO();
		cltrelnDTO.setClntpfx(br629Dto.getCownpfx());
		cltrelnDTO.setClntcoy(br629Dto.getCowncoy());
		cltrelnDTO.setClntnum(br629Dto.getCownnum());
		cltrelnDTO.setFunction("REM");
		cltrelnDTO.setClrrrole("DA");
		cltrelnDTO.setForepfx(fsupfxcpy.chdr.toString());
		cltrelnDTO.setForecoy(br629Dto.getChdrcoy());
		wsaaForenum.set(br629Dto.getChdrnum());
		cltrelnDTO.setForenum(wsaaForenum.toString());
		cltrelnDTO = cltrelnUtils.removeRole(cltrelnDTO);
		if (!"****".equals(cltrelnDTO.getStatus())) {
			fatalError600();
		}
		
		/* Add new role for despatch */
		cltrelnDTO = new CltrelnDTO();
		cltrelnDTO.setClntpfx(fsupfxcpy.clnt.toString());
		cltrelnDTO.setClntcoy(bsprIO.getFsuco().toString());
		cltrelnDTO.setClntnum(br629Dto.getLifcnum());
		cltrelnDTO.setFunction("ADD");
		cltrelnDTO.setClrrrole("DA");
		cltrelnDTO.setForepfx(fsupfxcpy.chdr.toString());
		cltrelnDTO.setForecoy(br629Dto.getChdrcoy());
		wsaaForenum.set(br629Dto.getChdrnum());
		cltrelnDTO.setForenum(wsaaForenum.toString());
		cltrelnDTO = cltrelnUtils.addRole(cltrelnDTO);
		if (!"****".equals(cltrelnDTO.getStatus())) {
			fatalError600();
		}
	}

	protected void rewriteChdr3750() {
	
		//fwang3
		Chdrpf chdrpf = new Chdrpf();
		chdrpf.setChdrcoy(this.br629Dto.getChdrcoy().charAt(0));
		chdrpf.setChdrnum(this.br629Dto.getChdrnum());
		chdrpf.setValidflag('2');
		chdrpf.setCurrto(bsscIO.getEffectiveDate().toInt());
		chdrpfList.add(chdrpf);
		
				
		/* Write new record for the new ownership */

		wsaaTermid.set(SPACES);
		wsaaDate.set(getCobolDate());
		wsaaTime.set(getCobolTime());
		
		Chdrpf chdrpfInsert = new Chdrpf();
		chdrpfInsert.setChdrcoy(this.br629Dto.getChdrcoy().charAt(0));
		chdrpfInsert.setChdrnum(this.br629Dto.getChdrnum());
		chdrpfInsert.setValidflag('1'); //ILIFE-3901
		br629Dto.setTranno(br629Dto.getTranno() + 1);
		chdrpfInsert.setTranno(br629Dto.getTranno());
		chdrpfInsert.setTranid(wsaaTranid.toString());
		chdrpfInsert.setCownnum(br629Dto.getLifcnum());
		chdrpfListInsert.add(chdrpfInsert);
	}

	protected void writePtrn3770() {
		/* Write a PTRN record. */
		Ptrnpf ptrnIO = new Ptrnpf();
		ptrnIO.setBatcpfx(batcdorrec.prefix.toString());
		ptrnIO.setBatccoy(batcdorrec.company.toString());
		ptrnIO.setBatcbrn(batcdorrec.branch.toString());
		ptrnIO.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnIO.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnIO.setBatcbatch(batcdorrec.batch.toString());
		ptrnIO.setBatctrcde(batcdorrec.trcde.toString());
		ptrnIO.setChdrcoy(br629Dto.getChdrcoy());
		ptrnIO.setChdrnum(br629Dto.getChdrnum());
		ptrnIO.setTranno(br629Dto.getTranno());
		ptrnIO.setTrdt(Integer.parseInt(getCobolDate()));
		ZonedDecimalData wsaaTime = new ZonedDecimalData(6, 0).init(0).setUnsigned();
		wsaaTime.set(getCobolTime());
		ptrnIO.setTrtm(wsaaTime.toInt());
		ptrnIO.setPtrneff(bsscIO.getEffectiveDate().toInt());
		ptrnIO.setDatesub(bsscIO.getEffectiveDate().toInt());
		ptrnIO.setTermid("");
		ptrnIO.setUserT(999999);
		ptrnpfList.add(ptrnIO);
	}

	protected void updateProcflag3800() {
		/* Write the Process Flag of HPADTRF to 'Y' since the contract */
		/* ownership has changed */
		Hpadpf hpadtrfIO = new Hpadpf();
		hpadtrfIO.setChdrcoy(br629Dto.getChdrcoy());
		hpadtrfIO.setChdrnum(br629Dto.getChdrnum());
		hpadtrfIO.setProcflg("Y");
		hpadpfList.add(hpadtrfIO);
	}

	protected void callBldenrl3900() {
		/* START */
		initialize(bldenrlrec.bldenrlrec);
		bldenrlrec.prefix.set(br629Dto.getChdrpfx());
		bldenrlrec.company.set(br629Dto.getChdrcoy());
		bldenrlrec.uentity.set(br629Dto.getChdrnum());
//		callProgram(Bldenrl.class, bldenrlrec.bldenrlrec);
		Bldenrl.getInstance().mainline(bldenrlrec.bldenrlrec);
		if (isNE(bldenrlrec.statuz, varcom.oK)) {
			syserrrec.params.set(bldenrlrec.bldenrlrec);
			syserrrec.statuz.set(bldenrlrec.statuz);
			fatalError600();
		}
		/* EXIT */
	}

	protected void commit3500() {
		commitControlTotals();
		chdrDAO.updateChdrRcds(chdrpfList);
		chdrpfList.clear();
		chdrDAO.insertChdrRcdsBr629(chdrpfListInsert);
		chdrpfListInsert.clear();
		ptrnpfDAO.insertPtrnPF(ptrnpfList);
		ptrnpfList.clear();
		hpadpfDAO.updateHpadRcds(hpadpfList);
		hpadpfList.clear();
	}

	protected void commitControlTotals() {
		// ct01
		contotrec.totno.set(ct01);
		contotrec.totval.set(ctrCT01);
		callContot001();
		ctrCT01 = 0;

		// ct02
		contotrec.totno.set(ct02);
		contotrec.totval.set(ctrCT02);
		callContot001();
		ctrCT02 = 0;
	}
	
	protected void rollback3600() {
		/* ROLLBACK */
		/* EXIT */
	}

	protected void close4000() {
		/* CLOSE-FILES */
		if (isEQ(wsaaNumber, ZERO)) {
			printerFile.printRr629h01(rr629H01, indicArea);
			wsaaOverflow.set("N");
		}
		printerFile.printRr629e01(rr629E01, indicArea);
		printerFile.close();
		hpaxpf.close();

		br629DtoList.clear();
		br629DtoList = null;
		chdrpfList.clear();
		chdrpfList = null;
		chdrpfListInsert.clear();
		chdrpfListInsert = null;
		ptrnpfList.clear();
		ptrnpfList = null;
		hpadpfList.clear();
		hpadpfList = null;
		
		lsaaStatuz.set(varcom.oK);
		/* EXIT */
	}
/*
 * Class transformed  from Data Structure RR629-D01--INNER
 */
private static final class Rr629D01Inner {

	private FixedLengthStringData rr629D01 = new FixedLengthStringData(189);
	private FixedLengthStringData rr629d01O = new FixedLengthStringData(189).isAPartOf(rr629D01, 0);
	private ZonedDecimalData zseqno = new ZonedDecimalData(4, 0).isAPartOf(rr629d01O, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rr629d01O, 4);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(rr629d01O, 12);
	private FixedLengthStringData lifcnum = new FixedLengthStringData(8).isAPartOf(rr629d01O, 15);
	private FixedLengthStringData lname = new FixedLengthStringData(40).isAPartOf(rr629d01O, 23);
	private FixedLengthStringData cownum = new FixedLengthStringData(8).isAPartOf(rr629d01O, 63);
	private FixedLengthStringData rname = new FixedLengthStringData(40).isAPartOf(rr629d01O, 71);
	private FixedLengthStringData cownnum = new FixedLengthStringData(8).isAPartOf(rr629d01O, 111);
	private FixedLengthStringData clntlname = new FixedLengthStringData(40).isAPartOf(rr629d01O, 119);
	private FixedLengthStringData cltdob = new FixedLengthStringData(10).isAPartOf(rr629d01O, 159);
	private FixedLengthStringData occdate = new FixedLengthStringData(10).isAPartOf(rr629d01O, 169);
	private FixedLengthStringData effdates = new FixedLengthStringData(10).isAPartOf(rr629d01O, 179);
}
}