package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;


public class Nlgcalcrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData nlgcalcRec = new FixedLengthStringData(162);
  
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(nlgcalcRec, 0);
  	public FixedLengthStringData status = new FixedLengthStringData(4).isAPartOf(nlgcalcRec, 5);
  	public FixedLengthStringData fsuco = new FixedLengthStringData(1).isAPartOf(nlgcalcRec, 9);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(nlgcalcRec, 10);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(nlgcalcRec, 11);
  	public ZonedDecimalData tranno = new ZonedDecimalData(5, 0).isAPartOf(nlgcalcRec, 19).setUnsigned();
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(nlgcalcRec, 24).setUnsigned();
  	public ZonedDecimalData occdate = new ZonedDecimalData(8, 0).isAPartOf(nlgcalcRec, 32);  
  	public ZonedDecimalData ptdate = new ZonedDecimalData(8, 0).isAPartOf(nlgcalcRec, 40);  
  	public ZonedDecimalData btdate = new ZonedDecimalData(8, 0).isAPartOf(nlgcalcRec, 48);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(nlgcalcRec, 56);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(nlgcalcRec, 58);
  	public ZonedDecimalData cltdob = new ZonedDecimalData(8, 0).isAPartOf(nlgcalcRec, 61 ).setUnsigned();
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(nlgcalcRec, 69);
  	public PackedDecimalData batcactyr = new PackedDecimalData(4, 0).isAPartOf(nlgcalcRec, 70);
  	public PackedDecimalData batcactmn = new PackedDecimalData(2, 0).isAPartOf(nlgcalcRec, 73);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(nlgcalcRec, 75);
  	public FixedLengthStringData billchnl = new FixedLengthStringData(2).isAPartOf(nlgcalcRec, 79);
  	public FixedLengthStringData Cntcurr = new FixedLengthStringData(3).isAPartOf(nlgcalcRec, 81);
  	public PackedDecimalData inputAmt = new PackedDecimalData(17, 2).isAPartOf(nlgcalcRec, 84);
  	public ZonedDecimalData frmdate = new ZonedDecimalData(8, 0).isAPartOf(nlgcalcRec, 93).setUnsigned();
  	public ZonedDecimalData todate = new ZonedDecimalData(8, 0).isAPartOf(nlgcalcRec,101 ).setUnsigned();
  	public PackedDecimalData nlgBalance = new PackedDecimalData(17,2).isAPartOf(nlgcalcRec, 109);
  	public PackedDecimalData totTopup = new PackedDecimalData(17,2).isAPartOf(nlgcalcRec, 118);
  	public PackedDecimalData totWdrAmt = new PackedDecimalData(17,2).isAPartOf(nlgcalcRec, 127);
  	public PackedDecimalData ovduePrem = new PackedDecimalData(17,2).isAPartOf(nlgcalcRec, 136);
  	public PackedDecimalData unpaidPrem = new PackedDecimalData(17,2).isAPartOf(nlgcalcRec, 145);
  	public ZonedDecimalData currAge = new ZonedDecimalData(3,0).isAPartOf(nlgcalcRec, 154);
  	public ZonedDecimalData yrsInf = new ZonedDecimalData(3,0).isAPartOf(nlgcalcRec, 157);
  	public FixedLengthStringData nlgFlag = new FixedLengthStringData(1).isAPartOf(nlgcalcRec, 160);
  	public FixedLengthStringData transMode = new FixedLengthStringData(1).isAPartOf(nlgcalcRec, 161);
  	
  	
  	

	public void initialize() {
		COBOLFunctions.initialize(nlgcalcRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			nlgcalcRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}