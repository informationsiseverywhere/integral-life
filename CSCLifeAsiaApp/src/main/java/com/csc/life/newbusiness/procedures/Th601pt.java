/*
 * File: Th601pt.java
 * Date: 30 August 2009 2:36:34
 * Author: Quipoz Limited
 * 
 * Class transformed from TH601PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.newbusiness.tablestructures.Th601rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR TH601.
*
*
*****************************************************************
* </pre>
*/
public class Th601pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine001, 28, FILLER).init("Monthly NB Turnaround Lag Times            SH601");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(17);
	private FixedLengthStringData filler7 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine003, 5, FILLER).init("Total Issued");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(78);
	private FixedLengthStringData filler9 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo005 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine004, 5).setPattern("ZZ");
	private FixedLengthStringData filler10 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine004, 7, FILLER).init(" Days");
	private ZonedDecimalData fieldNo006 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine004, 15).setPattern("ZZ");
	private FixedLengthStringData filler11 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine004, 17, FILLER).init(" Days");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine004, 25).setPattern("ZZ");
	private FixedLengthStringData filler12 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine004, 27, FILLER).init(" Days");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine004, 35).setPattern("ZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine004, 37, FILLER).init(" Days");
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine004, 45).setPattern("ZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine004, 47, FILLER).init(" DDays");
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine004, 54).setPattern("ZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 56, FILLER).init(" to");
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine004, 60).setPattern("ZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine004, 62, FILLER).init(" Days  >");
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine004, 71).setPattern("ZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine004, 73, FILLER).init(" Days");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(22);
	private FixedLengthStringData filler18 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler19 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine005, 5, FILLER).init("Total Outstanding");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(65);
	private FixedLengthStringData filler20 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler21 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 5, FILLER).init("Under");
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 11).setPattern("ZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 16).setPattern("ZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 18, FILLER).init(" to");
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 22).setPattern("ZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 27).setPattern("ZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 29, FILLER).init(" to");
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 33).setPattern("ZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 38).setPattern("ZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 40, FILLER).init(" to");
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 44).setPattern("ZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 49).setPattern("ZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 51, FILLER).init(" to");
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 55).setPattern("ZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 57, FILLER).init("    >");
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 63).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(65);
	private FixedLengthStringData filler31 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler32 = new FixedLengthStringData(57).isAPartOf(wsaaPrtLine007, 8, FILLER).init("Weeks     Weeks      Weeks      Weeks      Weeks    Weeks");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Th601rec th601rec = new Th601rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Th601pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		th601rec.th601Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(th601rec.lapday01);
		fieldNo006.set(th601rec.lapday02);
		fieldNo007.set(th601rec.lapday03);
		fieldNo008.set(th601rec.lapday04);
		fieldNo009.set(th601rec.lapday05);
		fieldNo010.set(th601rec.lapday06);
		fieldNo011.set(th601rec.lapday07);
		fieldNo012.set(th601rec.lapday08);
		fieldNo013.set(th601rec.weeks01);
		fieldNo014.set(th601rec.weeks02);
		fieldNo015.set(th601rec.weeks03);
		fieldNo016.set(th601rec.weeks04);
		fieldNo017.set(th601rec.weeks05);
		fieldNo018.set(th601rec.weeks06);
		fieldNo019.set(th601rec.weeks07);
		fieldNo020.set(th601rec.weeks08);
		fieldNo021.set(th601rec.weeks09);
		fieldNo022.set(th601rec.weeks10);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
