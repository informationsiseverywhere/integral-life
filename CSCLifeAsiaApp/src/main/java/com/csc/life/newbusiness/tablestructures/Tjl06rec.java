package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;


public class Tjl06rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
	private static final long serialVersionUID = 1L;
  	public FixedLengthStringData tjl06Rec = new FixedLengthStringData(587);
  	public FixedLengthStringData billfreqs = new FixedLengthStringData(20).isAPartOf(tjl06Rec, 0);
  	public FixedLengthStringData[] billfreq = FLSArrayPartOfStructure(10, 2, billfreqs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(billfreqs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData billfreq01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData billfreq02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData billfreq03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData billfreq04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData billfreq05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData billfreq06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData billfreq07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData billfreq08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData billfreq09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData billfreq10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	
	public FixedLengthStringData mops = new FixedLengthStringData(5).isAPartOf(tjl06Rec, 20);
  	public FixedLengthStringData[] mop = FLSArrayPartOfStructure(5, 1, mops, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(5).isAPartOf(mops, 0, FILLER_REDEFINE);
  	public FixedLengthStringData mop01 = new FixedLengthStringData(1).isAPartOf(filler1, 0);
  	public FixedLengthStringData mop02 = new FixedLengthStringData(1).isAPartOf(filler1, 1);
  	public FixedLengthStringData mop03 = new FixedLengthStringData(1).isAPartOf(filler1, 2);
  	public FixedLengthStringData mop04 = new FixedLengthStringData(1).isAPartOf(filler1, 3);
  	public FixedLengthStringData mop05 = new FixedLengthStringData(1).isAPartOf(filler1, 4);
  	
	public FixedLengthStringData contitem = new FixedLengthStringData(8).isAPartOf(tjl06Rec, 25);
	
	public ZonedDecimalData minsumin = DD.minsumin.copyToZonedDecimal().isAPartOf(tjl06Rec,33);
	public ZonedDecimalData minDhb = DD.minDhb.copyToZonedDecimal().isAPartOf(tjl06Rec,48);
	public ZonedDecimalData minsum = DD.minsum.copyToZonedDecimal().isAPartOf(tjl06Rec,63);
	
	public FixedLengthStringData instprms = new FixedLengthStringData(85).isAPartOf(tjl06Rec, 78);
	public ZonedDecimalData[] instprm = ZDArrayPartOfStructure(5, 17, 0, instprms, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(85).isAPartOf(instprms, 0, FILLER_REDEFINE);
	public ZonedDecimalData instPrem01 = DD.instprm.copyToZonedDecimal().isAPartOf(filler8,0);
	public ZonedDecimalData instPrem02 = DD.instprm.copyToZonedDecimal().isAPartOf(filler8,17);
	public ZonedDecimalData instPrem03 = DD.instprm.copyToZonedDecimal().isAPartOf(filler8,34);
	public ZonedDecimalData instPrem04 = DD.instprm.copyToZonedDecimal().isAPartOf(filler8,51);
	public ZonedDecimalData instPrem05 = DD.instprm.copyToZonedDecimal().isAPartOf(filler8,68);

	
	public FixedLengthStringData ctables = new FixedLengthStringData(300).isAPartOf(tjl06Rec, 163);
	public FixedLengthStringData[] ctable = FLSArrayPartOfStructure(75, 4, ctables, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(300).isAPartOf(ctables, 0, FILLER_REDEFINE);
	public FixedLengthStringData ctable01 = new FixedLengthStringData(4).isAPartOf(filler11,0);
	public FixedLengthStringData ctable02 = new FixedLengthStringData(4).isAPartOf(filler11,4);
	public FixedLengthStringData ctable03 = new FixedLengthStringData(4).isAPartOf(filler11,8);
	public FixedLengthStringData ctable04 = new FixedLengthStringData(4).isAPartOf(filler11,12);
	public FixedLengthStringData ctable05 = new FixedLengthStringData(4).isAPartOf(filler11,16);
	public FixedLengthStringData ctable06 = new FixedLengthStringData(4).isAPartOf(filler11,20);
	public FixedLengthStringData ctable07 = new FixedLengthStringData(4).isAPartOf(filler11,24);
	public FixedLengthStringData ctable08 = new FixedLengthStringData(4).isAPartOf(filler11,28);
	public FixedLengthStringData ctable09 = new FixedLengthStringData(4).isAPartOf(filler11,32);
	public FixedLengthStringData ctable10 = new FixedLengthStringData(4).isAPartOf(filler11,36);
	public FixedLengthStringData ctable11 = new FixedLengthStringData(4).isAPartOf(filler11,40);
	public FixedLengthStringData ctable12 = new FixedLengthStringData(4).isAPartOf(filler11,44);
	public FixedLengthStringData ctable13 = new FixedLengthStringData(4).isAPartOf(filler11,48);
	public FixedLengthStringData ctable14 = new FixedLengthStringData(4).isAPartOf(filler11,52);
	public FixedLengthStringData ctable15 = new FixedLengthStringData(4).isAPartOf(filler11,56);
	public FixedLengthStringData ctable16 = new FixedLengthStringData(4).isAPartOf(filler11,60);
	public FixedLengthStringData ctable17 = new FixedLengthStringData(4).isAPartOf(filler11,64);
	public FixedLengthStringData ctable18 = new FixedLengthStringData(4).isAPartOf(filler11,68);
	public FixedLengthStringData ctable19 = new FixedLengthStringData(4).isAPartOf(filler11,72);
	public FixedLengthStringData ctable20 = new FixedLengthStringData(4).isAPartOf(filler11,76);
	public FixedLengthStringData ctable21 = new FixedLengthStringData(4).isAPartOf(filler11,80);
	public FixedLengthStringData ctable22 = new FixedLengthStringData(4).isAPartOf(filler11,84);
	public FixedLengthStringData ctable23 = new FixedLengthStringData(4).isAPartOf(filler11,88);
	public FixedLengthStringData ctable24 = new FixedLengthStringData(4).isAPartOf(filler11,92);
	public FixedLengthStringData ctable25 = new FixedLengthStringData(4).isAPartOf(filler11,96);
	public FixedLengthStringData ctable26 = new FixedLengthStringData(4).isAPartOf(filler11,100);
	public FixedLengthStringData ctable27 = new FixedLengthStringData(4).isAPartOf(filler11,104);
	public FixedLengthStringData ctable28 = new FixedLengthStringData(4).isAPartOf(filler11,108);
	public FixedLengthStringData ctable29 = new FixedLengthStringData(4).isAPartOf(filler11,112);
	public FixedLengthStringData ctable30 = new FixedLengthStringData(4).isAPartOf(filler11,116);
	public FixedLengthStringData ctable31 = new FixedLengthStringData(4).isAPartOf(filler11,120);
	public FixedLengthStringData ctable32 = new FixedLengthStringData(4).isAPartOf(filler11,124);
	public FixedLengthStringData ctable33 = new FixedLengthStringData(4).isAPartOf(filler11,128);
	public FixedLengthStringData ctable34 = new FixedLengthStringData(4).isAPartOf(filler11,132);
	public FixedLengthStringData ctable35 = new FixedLengthStringData(4).isAPartOf(filler11,136);
	public FixedLengthStringData ctable36 = new FixedLengthStringData(4).isAPartOf(filler11,140);
	public FixedLengthStringData ctable37 = new FixedLengthStringData(4).isAPartOf(filler11,144);
	public FixedLengthStringData ctable38 = new FixedLengthStringData(4).isAPartOf(filler11,148);
	public FixedLengthStringData ctable39 = new FixedLengthStringData(4).isAPartOf(filler11,152);
	public FixedLengthStringData ctable40 = new FixedLengthStringData(4).isAPartOf(filler11,156);
	public FixedLengthStringData ctable41 = new FixedLengthStringData(4).isAPartOf(filler11,160);
	public FixedLengthStringData ctable42 = new FixedLengthStringData(4).isAPartOf(filler11,164);
	public FixedLengthStringData ctable43 = new FixedLengthStringData(4).isAPartOf(filler11,168);
	public FixedLengthStringData ctable44 = new FixedLengthStringData(4).isAPartOf(filler11,172);
	public FixedLengthStringData ctable45 = new FixedLengthStringData(4).isAPartOf(filler11,176);
	public FixedLengthStringData ctable46 = new FixedLengthStringData(4).isAPartOf(filler11,180);
	public FixedLengthStringData ctable47 = new FixedLengthStringData(4).isAPartOf(filler11,184);
	public FixedLengthStringData ctable48 = new FixedLengthStringData(4).isAPartOf(filler11,188);
	public FixedLengthStringData ctable49 = new FixedLengthStringData(4).isAPartOf(filler11,192);
	public FixedLengthStringData ctable50 = new FixedLengthStringData(4).isAPartOf(filler11,196);
	public FixedLengthStringData ctable51 = new FixedLengthStringData(4).isAPartOf(filler11,200);
	public FixedLengthStringData ctable52 = new FixedLengthStringData(4).isAPartOf(filler11,204);
	public FixedLengthStringData ctable53 = new FixedLengthStringData(4).isAPartOf(filler11,208);
	public FixedLengthStringData ctable54 = new FixedLengthStringData(4).isAPartOf(filler11,212);
	public FixedLengthStringData ctable55 = new FixedLengthStringData(4).isAPartOf(filler11,216);
	public FixedLengthStringData ctable56 = new FixedLengthStringData(4).isAPartOf(filler11,220);
	public FixedLengthStringData ctable57 = new FixedLengthStringData(4).isAPartOf(filler11,224);
	public FixedLengthStringData ctable58 = new FixedLengthStringData(4).isAPartOf(filler11,228);
	public FixedLengthStringData ctable59 = new FixedLengthStringData(4).isAPartOf(filler11,232);
	public FixedLengthStringData ctable60 = new FixedLengthStringData(4).isAPartOf(filler11,236);
	public FixedLengthStringData ctable61 = new FixedLengthStringData(4).isAPartOf(filler11,240);
	public FixedLengthStringData ctable62 = new FixedLengthStringData(4).isAPartOf(filler11,244);
	public FixedLengthStringData ctable63 = new FixedLengthStringData(4).isAPartOf(filler11,248);
	public FixedLengthStringData ctable64 = new FixedLengthStringData(4).isAPartOf(filler11,252);
	public FixedLengthStringData ctable65 = new FixedLengthStringData(4).isAPartOf(filler11,256);
	public FixedLengthStringData ctable66 = new FixedLengthStringData(4).isAPartOf(filler11,260);
	public FixedLengthStringData ctable67 = new FixedLengthStringData(4).isAPartOf(filler11,264);
	public FixedLengthStringData ctable68 = new FixedLengthStringData(4).isAPartOf(filler11,268);
	public FixedLengthStringData ctable69 = new FixedLengthStringData(4).isAPartOf(filler11,272);
	public FixedLengthStringData ctable70 = new FixedLengthStringData(4).isAPartOf(filler11,276);
	public FixedLengthStringData ctable71 = new FixedLengthStringData(4).isAPartOf(filler11,280);
	public FixedLengthStringData ctable72 = new FixedLengthStringData(4).isAPartOf(filler11,284);
	public FixedLengthStringData ctable73 = new FixedLengthStringData(4).isAPartOf(filler11,288);
	public FixedLengthStringData ctable74 = new FixedLengthStringData(4).isAPartOf(filler11,292);
	public FixedLengthStringData ctable75 = new FixedLengthStringData(4).isAPartOf(filler11,296);
	
	public FixedLengthStringData hosbens = new FixedLengthStringData(108).isAPartOf(tjl06Rec, 463);
	public FixedLengthStringData[] hosben = FLSArrayPartOfStructure(27, 4, hosbens, 0);
	public FixedLengthStringData filler15 = new FixedLengthStringData(108).isAPartOf(hosbens, 0, FILLER_REDEFINE);
	public FixedLengthStringData hosben01 = new FixedLengthStringData(4).isAPartOf(filler15,0);
	public FixedLengthStringData hosben02 = new FixedLengthStringData(4).isAPartOf(filler15,4);
	public FixedLengthStringData hosben03 = new FixedLengthStringData(4).isAPartOf(filler15,8);
	public FixedLengthStringData hosben04 = new FixedLengthStringData(4).isAPartOf(filler15,12);
	public FixedLengthStringData hosben05 = new FixedLengthStringData(4).isAPartOf(filler15,16);
	public FixedLengthStringData hosben06 = new FixedLengthStringData(4).isAPartOf(filler15,20);
	public FixedLengthStringData hosben07 = new FixedLengthStringData(4).isAPartOf(filler15,24);
	public FixedLengthStringData hosben08 = new FixedLengthStringData(4).isAPartOf(filler15,28);
	public FixedLengthStringData hosben09 = new FixedLengthStringData(4).isAPartOf(filler15,32);
	public FixedLengthStringData hosben10 = new FixedLengthStringData(4).isAPartOf(filler15,36);
	public FixedLengthStringData hosben11 = new FixedLengthStringData(4).isAPartOf(filler15,40);
	public FixedLengthStringData hosben12 = new FixedLengthStringData(4).isAPartOf(filler15,44);
	public FixedLengthStringData hosben13 = new FixedLengthStringData(4).isAPartOf(filler15,48);
	public FixedLengthStringData hosben14 = new FixedLengthStringData(4).isAPartOf(filler15,52);
	public FixedLengthStringData hosben15 = new FixedLengthStringData(4).isAPartOf(filler15,56);
	public FixedLengthStringData hosben16 = new FixedLengthStringData(4).isAPartOf(filler15,60);
	public FixedLengthStringData hosben17 = new FixedLengthStringData(4).isAPartOf(filler15,64);
	public FixedLengthStringData hosben18 = new FixedLengthStringData(4).isAPartOf(filler15,68);
	public FixedLengthStringData hosben19 = new FixedLengthStringData(4).isAPartOf(filler15,72);
	public FixedLengthStringData hosben20 = new FixedLengthStringData(4).isAPartOf(filler15,76);
	public FixedLengthStringData hosben21 = new FixedLengthStringData(4).isAPartOf(filler15,80);
	public FixedLengthStringData hosben22 = new FixedLengthStringData(4).isAPartOf(filler15,84);
	public FixedLengthStringData hosben23 = new FixedLengthStringData(4).isAPartOf(filler15,88);
	public FixedLengthStringData hosben24 = new FixedLengthStringData(4).isAPartOf(filler15,92);
	public FixedLengthStringData hosben25 = new FixedLengthStringData(4).isAPartOf(filler15,96);
	public FixedLengthStringData hosben26 = new FixedLengthStringData(4).isAPartOf(filler15,100);
	public FixedLengthStringData hosben27 = new FixedLengthStringData(4).isAPartOf(filler15,104);
  	
	public FixedLengthStringData fupcdes = new FixedLengthStringData(12).isAPartOf(tjl06Rec, 571);
	public FixedLengthStringData[] fupcde = FLSArrayPartOfStructure(3, 4, fupcdes, 0);
	public FixedLengthStringData filler16 = new FixedLengthStringData(12).isAPartOf(fupcdes, 0, FILLER_REDEFINE);
	public FixedLengthStringData fupcde01 = new FixedLengthStringData(4).isAPartOf(filler16, 0);
	public FixedLengthStringData fupcde02 = new FixedLengthStringData(4).isAPartOf(filler16, 4);
	public FixedLengthStringData fupcde03 = new FixedLengthStringData(4).isAPartOf(filler16, 8);
	public ZonedDecimalData cdateduration = DD.cdateduration.copyToZonedDecimal().isAPartOf(tjl06Rec, 583);
	public ZonedDecimalData juvenileage = DD.juvenileage.copyToZonedDecimal().isAPartOf(tjl06Rec, 585);

	public void initialize() {
		COBOLFunctions.initialize(tjl06Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			tjl06Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}