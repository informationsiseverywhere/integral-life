package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6278
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class S6278ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(620);
	public FixedLengthStringData dataFields = new FixedLengthStringData(316).isAPartOf(dataArea, 0);
	public FixedLengthStringData bankaccdsc = DD.bankaccdsc.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData bankdesc = DD.bankdesc.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(dataFields,80);
	public FixedLengthStringData branchdesc = DD.branchdesc.copy().isAPartOf(dataFields,90);
	public FixedLengthStringData compdesc = DD.compdesc.copy().isAPartOf(dataFields,120);
	public FixedLengthStringData curdesc = DD.curdesc.copy().isAPartOf(dataFields,150);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(dataFields,180);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,183);
	public FixedLengthStringData facthous = DD.facthous.copy().isAPartOf(dataFields,191);
	public ZonedDecimalData mandAmt = DD.mandamt.copyToZonedDecimal().isAPartOf(dataFields,193);
	public FixedLengthStringData mandref = DD.mandref.copy().isAPartOf(dataFields,210);
	public FixedLengthStringData mandstat = DD.mandstat.copy().isAPartOf(dataFields,215);
	public FixedLengthStringData numsel = DD.numsel.copy().isAPartOf(dataFields,217);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,227);
	public FixedLengthStringData payrcoy = DD.payrcoy.copy().isAPartOf(dataFields,274);
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields,275);
	public FixedLengthStringData statdets = DD.statdets.copy().isAPartOf(dataFields,283);
	public ZonedDecimalData timesUse = DD.timesuse.copyToZonedDecimal().isAPartOf(dataFields,313);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(76).isAPartOf(dataArea, 316);
	public FixedLengthStringData bankaccdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bankacckeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bankdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bankkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData branchdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData compdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData curdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData facthousErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData mandamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData mandrefErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData mandstatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData numselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData payrcoyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData statdetsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData timesuseErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(228).isAPartOf(dataArea, 392);
	public FixedLengthStringData[] bankaccdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bankacckeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bankdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bankkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] branchdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] compdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] curdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] facthousOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] mandamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] mandrefOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] mandstatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] numselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] payrcoyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] statdetsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] timesuseOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData S6278screenWritten = new LongData(0);
	public LongData S6278protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6278ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(mandamtOut,new String[] {"04","05","-04","05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(timesuseOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankkeyOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankacckeyOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(facthousOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {payrcoy, payrnum, mandstat, mandAmt, timesUse, bankkey, bankacckey, statdets, branchdesc, bankdesc, bankaccdsc, mandref, ownername, compdesc, currcode, curdesc, effdate, numsel, facthous};
		screenOutFields = new BaseData[][] {payrcoyOut, payrnumOut, mandstatOut, mandamtOut, timesuseOut, bankkeyOut, bankacckeyOut, statdetsOut, branchdescOut, bankdescOut, bankaccdscOut, mandrefOut, ownernameOut, compdescOut, currcodeOut, curdescOut, effdateOut, numselOut, facthousOut};
		screenErrFields = new BaseData[] {payrcoyErr, payrnumErr, mandstatErr, mandamtErr, timesuseErr, bankkeyErr, bankacckeyErr, statdetsErr, branchdescErr, bankdescErr, bankaccdscErr, mandrefErr, ownernameErr, compdescErr, currcodeErr, curdescErr, effdateErr, numselErr, facthousErr};
		screenDateFields = new BaseData[] {effdate};
		screenDateErrFields = new BaseData[] {effdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6278screen.class;
		protectRecord = S6278protect.class;
	}

}
