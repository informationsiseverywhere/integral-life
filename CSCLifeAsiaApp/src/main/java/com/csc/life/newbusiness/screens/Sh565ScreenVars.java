package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH565
 * @version 1.0 generated on 30/08/09 07:04
 * @author Quipoz
 */
public class Sh565ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(931);
	public FixedLengthStringData dataFields = new FixedLengthStringData(403).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,4);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,42);
	public FixedLengthStringData trandescs = new FixedLengthStringData(300).isAPartOf(dataFields, 47);
	public FixedLengthStringData[] trandesc = FLSArrayPartOfStructure(10, 30, trandescs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(300).isAPartOf(trandescs, 0, FILLER_REDEFINE);
	public FixedLengthStringData trandesc01 = DD.trandesc.copy().isAPartOf(filler,0);
	public FixedLengthStringData trandesc02 = DD.trandesc.copy().isAPartOf(filler,30);
	public FixedLengthStringData trandesc03 = DD.trandesc.copy().isAPartOf(filler,60);
	public FixedLengthStringData trandesc04 = DD.trandesc.copy().isAPartOf(filler,90);
	public FixedLengthStringData trandesc05 = DD.trandesc.copy().isAPartOf(filler,120);
	public FixedLengthStringData trandesc06 = DD.trandesc.copy().isAPartOf(filler,150);
	public FixedLengthStringData trandesc07 = DD.trandesc.copy().isAPartOf(filler,180);
	public FixedLengthStringData trandesc08 = DD.trandesc.copy().isAPartOf(filler,210);
	public FixedLengthStringData trandesc09 = DD.trandesc.copy().isAPartOf(filler,240);
	public FixedLengthStringData trandesc10 = DD.trandesc.copy().isAPartOf(filler,270);
	public FixedLengthStringData zmthfrms = new FixedLengthStringData(8).isAPartOf(dataFields, 347);
	public ZonedDecimalData[] zmthfrm = ZDArrayPartOfStructure(4, 2, 0, zmthfrms, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(zmthfrms, 0, FILLER_REDEFINE);
	public ZonedDecimalData zmthfrm01 = DD.zmthfrm.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData zmthfrm02 = DD.zmthfrm.copyToZonedDecimal().isAPartOf(filler1,2);
	public ZonedDecimalData zmthfrm03 = DD.zmthfrm.copyToZonedDecimal().isAPartOf(filler1,4);
	public ZonedDecimalData zmthfrm04 = DD.zmthfrm.copyToZonedDecimal().isAPartOf(filler1,6);
	public FixedLengthStringData zmthtos = new FixedLengthStringData(8).isAPartOf(dataFields, 355);
	public ZonedDecimalData[] zmthto = ZDArrayPartOfStructure(4, 2, 0, zmthtos, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(zmthtos, 0, FILLER_REDEFINE);
	public ZonedDecimalData zmthto01 = DD.zmthto.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData zmthto02 = DD.zmthto.copyToZonedDecimal().isAPartOf(filler2,2);
	public ZonedDecimalData zmthto03 = DD.zmthto.copyToZonedDecimal().isAPartOf(filler2,4);
	public ZonedDecimalData zmthto04 = DD.zmthto.copyToZonedDecimal().isAPartOf(filler2,6);
	public FixedLengthStringData ztrcdes = new FixedLengthStringData(40).isAPartOf(dataFields, 363);
	public FixedLengthStringData[] ztrcde = FLSArrayPartOfStructure(10, 4, ztrcdes, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(ztrcdes, 0, FILLER_REDEFINE);
	public FixedLengthStringData ztrcde01 = DD.ztrcde.copy().isAPartOf(filler3,0);
	public FixedLengthStringData ztrcde02 = DD.ztrcde.copy().isAPartOf(filler3,4);
	public FixedLengthStringData ztrcde03 = DD.ztrcde.copy().isAPartOf(filler3,8);
	public FixedLengthStringData ztrcde04 = DD.ztrcde.copy().isAPartOf(filler3,12);
	public FixedLengthStringData ztrcde05 = DD.ztrcde.copy().isAPartOf(filler3,16);
	public FixedLengthStringData ztrcde06 = DD.ztrcde.copy().isAPartOf(filler3,20);
	public FixedLengthStringData ztrcde07 = DD.ztrcde.copy().isAPartOf(filler3,24);
	public FixedLengthStringData ztrcde08 = DD.ztrcde.copy().isAPartOf(filler3,28);
	public FixedLengthStringData ztrcde09 = DD.ztrcde.copy().isAPartOf(filler3,32);
	public FixedLengthStringData ztrcde10 = DD.ztrcde.copy().isAPartOf(filler3,36);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(132).isAPartOf(dataArea, 403);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData trandescsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData[] trandescErr = FLSArrayPartOfStructure(10, 4, trandescsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(40).isAPartOf(trandescsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData trandesc01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData trandesc02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData trandesc03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData trandesc04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData trandesc05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData trandesc06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData trandesc07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData trandesc08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData trandesc09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData trandesc10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData zmthfrmsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData[] zmthfrmErr = FLSArrayPartOfStructure(4, 4, zmthfrmsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(16).isAPartOf(zmthfrmsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zmthfrm01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData zmthfrm02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData zmthfrm03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData zmthfrm04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData zmthtosErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData[] zmthtoErr = FLSArrayPartOfStructure(4, 4, zmthtosErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(16).isAPartOf(zmthtosErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zmthto01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData zmthto02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData zmthto03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData zmthto04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData ztrcdesErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData[] ztrcdeErr = FLSArrayPartOfStructure(10, 4, ztrcdesErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(40).isAPartOf(ztrcdesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ztrcde01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData ztrcde02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData ztrcde03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData ztrcde04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData ztrcde05Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);
	public FixedLengthStringData ztrcde06Err = new FixedLengthStringData(4).isAPartOf(filler7, 20);
	public FixedLengthStringData ztrcde07Err = new FixedLengthStringData(4).isAPartOf(filler7, 24);
	public FixedLengthStringData ztrcde08Err = new FixedLengthStringData(4).isAPartOf(filler7, 28);
	public FixedLengthStringData ztrcde09Err = new FixedLengthStringData(4).isAPartOf(filler7, 32);
	public FixedLengthStringData ztrcde10Err = new FixedLengthStringData(4).isAPartOf(filler7, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(396).isAPartOf(dataArea, 535);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData trandescsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 60);
	public FixedLengthStringData[] trandescOut = FLSArrayPartOfStructure(10, 12, trandescsOut, 0);
	public FixedLengthStringData[][] trandescO = FLSDArrayPartOfArrayStructure(12, 1, trandescOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(120).isAPartOf(trandescsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] trandesc01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] trandesc02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] trandesc03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] trandesc04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] trandesc05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] trandesc06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] trandesc07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] trandesc08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] trandesc09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] trandesc10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public FixedLengthStringData zmthfrmsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 180);
	public FixedLengthStringData[] zmthfrmOut = FLSArrayPartOfStructure(4, 12, zmthfrmsOut, 0);
	public FixedLengthStringData[][] zmthfrmO = FLSDArrayPartOfArrayStructure(12, 1, zmthfrmOut, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(48).isAPartOf(zmthfrmsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zmthfrm01Out = FLSArrayPartOfStructure(12, 1, filler9, 0);
	public FixedLengthStringData[] zmthfrm02Out = FLSArrayPartOfStructure(12, 1, filler9, 12);
	public FixedLengthStringData[] zmthfrm03Out = FLSArrayPartOfStructure(12, 1, filler9, 24);
	public FixedLengthStringData[] zmthfrm04Out = FLSArrayPartOfStructure(12, 1, filler9, 36);
	public FixedLengthStringData zmthtosOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 228);
	public FixedLengthStringData[] zmthtoOut = FLSArrayPartOfStructure(4, 12, zmthtosOut, 0);
	public FixedLengthStringData[][] zmthtoO = FLSDArrayPartOfArrayStructure(12, 1, zmthtoOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(48).isAPartOf(zmthtosOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zmthto01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] zmthto02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] zmthto03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] zmthto04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData ztrcdesOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 276);
	public FixedLengthStringData[] ztrcdeOut = FLSArrayPartOfStructure(10, 12, ztrcdesOut, 0);
	public FixedLengthStringData[][] ztrcdeO = FLSDArrayPartOfArrayStructure(12, 1, ztrcdeOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(120).isAPartOf(ztrcdesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ztrcde01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] ztrcde02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] ztrcde03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] ztrcde04Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public FixedLengthStringData[] ztrcde05Out = FLSArrayPartOfStructure(12, 1, filler11, 48);
	public FixedLengthStringData[] ztrcde06Out = FLSArrayPartOfStructure(12, 1, filler11, 60);
	public FixedLengthStringData[] ztrcde07Out = FLSArrayPartOfStructure(12, 1, filler11, 72);
	public FixedLengthStringData[] ztrcde08Out = FLSArrayPartOfStructure(12, 1, filler11, 84);
	public FixedLengthStringData[] ztrcde09Out = FLSArrayPartOfStructure(12, 1, filler11, 96);
	public FixedLengthStringData[] ztrcde10Out = FLSArrayPartOfStructure(12, 1, filler11, 108);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sh565screenWritten = new LongData(0);
	public LongData Sh565protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh565ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, currcode, ztrcde01, trandesc01, ztrcde02, trandesc02, ztrcde03, trandesc03, ztrcde04, trandesc04, ztrcde05, trandesc05, ztrcde06, trandesc06, ztrcde07, trandesc07, ztrcde08, trandesc08, ztrcde09, trandesc09, ztrcde10, trandesc10, zmthfrm01, zmthfrm02, zmthfrm03, zmthfrm04, zmthto01, zmthto02, zmthto03, zmthto04};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, currcodeOut, ztrcde01Out, trandesc01Out, ztrcde02Out, trandesc02Out, ztrcde03Out, trandesc03Out, ztrcde04Out, trandesc04Out, ztrcde05Out, trandesc05Out, ztrcde06Out, trandesc06Out, ztrcde07Out, trandesc07Out, ztrcde08Out, trandesc08Out, ztrcde09Out, trandesc09Out, ztrcde10Out, trandesc10Out, zmthfrm01Out, zmthfrm02Out, zmthfrm03Out, zmthfrm04Out, zmthto01Out, zmthto02Out, zmthto03Out, zmthto04Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, currcodeErr, ztrcde01Err, trandesc01Err, ztrcde02Err, trandesc02Err, ztrcde03Err, trandesc03Err, ztrcde04Err, trandesc04Err, ztrcde05Err, trandesc05Err, ztrcde06Err, trandesc06Err, ztrcde07Err, trandesc07Err, ztrcde08Err, trandesc08Err, ztrcde09Err, trandesc09Err, ztrcde10Err, trandesc10Err, zmthfrm01Err, zmthfrm02Err, zmthfrm03Err, zmthfrm04Err, zmthto01Err, zmthto02Err, zmthto03Err, zmthto04Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh565screen.class;
		protectRecord = Sh565protect.class;
	}

}
