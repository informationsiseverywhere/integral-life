package com.csc.life.newbusiness.dataaccess.model;

public class Br629DTO {
	private String chdrpfx;// CHAR(2 CHAR),
	private String chdrcoy;// CHAR(1 CHAR),
	private String chdrnum;// CHAR(8 CHAR),
	private String cownpfx;// CHAR(2 CHAR),
	private String cowncoy; // CHAR(1 CHAR),
	private String cownnum; // CHAR(8 CHAR),
	private String cnttype; // CHAR(3 CHAR),
	private int tranno; // NUMBER(5),
	private int occdate; // NUMBER(8),
	private String statcode;// CHAR(2 CHAR),
	private String pstcde;// CHAR(2 CHAR),
	private int zsufcdte;// NUMBER(8),
	private String cl1lsurname; // CHAR(60 CHAR),
	private String cl1lgivname; // CHAR(60 CHAR),
	private String cl2lsurname;
	private String cl2lgivname;
	private String cl2cltdob;
	private String lifcnum; // CHAR(8 CHAR)

	public String getChdrpfx() {
		return chdrpfx;
	}

	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getCownpfx() {
		return cownpfx;
	}

	public void setCownpfx(String cownpfx) {
		this.cownpfx = cownpfx;
	}

	public String getCowncoy() {
		return cowncoy;
	}

	public void setCowncoy(String cowncoy) {
		this.cowncoy = cowncoy;
	}

	public String getCownnum() {
		return cownnum;
	}

	public void setCownnum(String cownnum) {
		this.cownnum = cownnum;
	}

	public String getCnttype() {
		return cnttype;
	}

	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public int getOccdate() {
		return occdate;
	}

	public void setOccdate(int occdate) {
		this.occdate = occdate;
	}

	public String getStatcode() {
		return statcode;
	}

	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	public String getPstcde() {
		return pstcde;
	}

	public void setPstcde(String pstcde) {
		this.pstcde = pstcde;
	}

	public int getZsufcdte() {
		return zsufcdte;
	}

	public void setZsufcdte(int zsufcdte) {
		this.zsufcdte = zsufcdte;
	}

	public String getCl1lsurname() {
		return cl1lsurname;
	}

	public void setCl1lsurname(String cl1lsurname) {
		this.cl1lsurname = cl1lsurname;
	}

	public String getCl1lgivname() {
		return cl1lgivname;
	}

	public void setCl1lgivname(String cl1lgivname) {
		this.cl1lgivname = cl1lgivname;
	}

	public String getCl2lsurname() {
		return cl2lsurname;
	}

	public void setCl2lsurname(String cl2lsurname) {
		this.cl2lsurname = cl2lsurname;
	}

	public String getCl2lgivname() {
		return cl2lgivname;
	}

	public void setCl2lgivname(String cl2lgivname) {
		this.cl2lgivname = cl2lgivname;
	}

	public String getCl2cltdob() {
		return cl2cltdob;
	}

	public void setCl2cltdob(String cl2cltdob) {
		this.cl2cltdob = cl2cltdob;
	}

	public String getLifcnum() {
		return lifcnum;
	}

	public void setLifcnum(String lifcnum) {
		this.lifcnum = lifcnum;
	}

}
