package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5002
 * @version 1.0 generated on 30/08/09 06:29
 * @author Quipoz
 */
public class Sr59sScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(207);
	public FixedLengthStringData dataFields = new FixedLengthStringData(63).isAPartOf(dataArea, 0);
	public FixedLengthStringData growth = DD.gcdblind.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData stepup = DD.gcdthclm.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData higherGrowth = DD.gcsetlmd.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,4);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,17);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,47);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,55);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 63);
	public FixedLengthStringData growthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData stepupErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData higherGrowthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 99);
	public FixedLengthStringData[] growthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] stepupOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] higherGrowthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sr59sscreenWritten = new LongData(0);
	public LongData Sr59sprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr59sScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(growthOut,new String[] {"3",null, "-3",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stepupOut,new String[] {"4",null, "-4",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(higherGrowthOut,new String[] {"5",null, "-5",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {growth, stepup, higherGrowth,company,tabl,item,longdesc,itmfrm,itmto};
		screenOutFields = new BaseData[][] {growthOut, stepupOut, higherGrowthOut,companyOut,tablOut,itemOut,longdescOut,itmfrmOut,itmtoOut};
		screenErrFields = new BaseData[] {growthErr, stepupErr, higherGrowthErr,companyErr,tablErr,itemErr,longdescErr,itmfrmErr,itmtoErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr59sscreen.class;
		protectRecord = Sr59sprotect.class;
		
	}

}
