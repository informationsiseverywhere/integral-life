package com.csc.life.newbusiness.tablestructures;


import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.quipoz.COBOLFramework.COBOLFunctions;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Web, 3 July 2013 
 * Description:
 * Copybook name: zvbupdRec
 *
 * Copyright (2013) CSC Asia, all rights reserved
 *
 */
public class Zvbupdrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
	public FixedLengthStringData zvbupdRec = new FixedLengthStringData(50);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(zvbupdRec, 0);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(zvbupdRec, 5);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(zvbupdRec, 6);
  	public PackedDecimalData sinstamt = new PackedDecimalData(17,2).isAPartOf(zvbupdRec, 14);
  	public ZonedDecimalData exprdate = new ZonedDecimalData(8,0).isAPartOf(zvbupdRec, 31);
  	public FixedLengthStringData billchnl = new FixedLengthStringData(2).isAPartOf(zvbupdRec, 39);
	public FixedLengthStringData billcurr = new FixedLengthStringData(3).isAPartOf(zvbupdRec, 41);
	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(zvbupdRec, 44);
	public FixedLengthStringData rectype = new FixedLengthStringData(1).isAPartOf(zvbupdRec, 48);
	public FixedLengthStringData extrval = new FixedLengthStringData(1).isAPartOf(zvbupdRec, 49);
  	

	public void initialize() {
		COBOLFunctions.initialize(zvbupdRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zvbupdRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}