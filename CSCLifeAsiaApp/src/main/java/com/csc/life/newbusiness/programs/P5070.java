/*
 * File: P5070.java
 * Date: 30 August 2009 0:01:51
 * Author: Quipoz Limited
 *
 * Class transformed from P5070.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.ClblTableDAM;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.dao.BabrpfDAO;
import com.csc.fsu.clients.dataaccess.model.Babrpf;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.MandTableDAM;
import com.csc.fsu.general.dataaccess.MandlnbTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.screens.S5070ScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.MaskingBankAccUtil;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*
* DIRECT DEBIT DETAILS.
*
*
* Partial window displaying the bank code, bank description,
* account number and account description detailing the
* client's account to be debited.
*
* Retrieves the contract header details from the contract header
* file (CHDRLNB).    Using RETRV.
*
* Hidden screen fields are used to hold the payer number, the
* contract currency and the factoring house.
*
* The bank code and account details are retrieved from the
* Client Bank Account Details (CLBL) file. If the client for
* the bank details is not the same as the 'hidden' payer then
* the payer has changed so the bank details are thus not
* applicable.
*
* The bank code and account descriptions are retrieved from
* the BABR file.
*
* The CLIENT NUMBER must always be the same as the hidden
* PAYER.
*
* If 'CALC' is pressed then the screen is redisplayed.
*
* If 'KILL' is pressed then all validation is ignored and the
* client header record is left as it was on entry to this
* program.
*
* Windowing on the account field allows new bank details to
* be created (automatically). This is controlled externally to
* this program - the details being returned to this program
* automatically in the hidden fields in the screen.
*
*****************************************************************
* </pre>
*/
public class P5070 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5070");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private FixedLengthStringData wsaaClntkey = new FixedLengthStringData(12).init(SPACES);
	private final String wsaaLargeName = "LGNMS";

	private FixedLengthStringData wsbbBankdesc = new FixedLengthStringData(60);
	private FixedLengthStringData wsbbBankdescLine1 = new FixedLengthStringData(30).isAPartOf(wsbbBankdesc, 0);
	private FixedLengthStringData wsbbBankdescLine2 = new FixedLengthStringData(30).isAPartOf(wsbbBankdesc, 30);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
		/* ERRORS */
		/* TABLES */
	private static final String t3684 = "T3684";
	private static final String clrfrec = "CLRFREC";
	private static final String descrec = "DESCREC";
		/*Bank/Branch Name File*/
	private BabrTableDAM babrIO = new BabrTableDAM();
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Logical File: Client/Bank Account Record*/
	private ClblTableDAM clblIO = new ClblTableDAM();
		/*Client Roles by Foreign Key*/
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Direct Debit Mandates used in Life New B*/
	private MandlnbTableDAM mandlnbIO = new MandlnbTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
		/*Payor Details Logical File*/
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Wssplife wssplife = new Wssplife();
	private S5070ScreenVars sv = ScreenProgram.getScreenVars( S5070ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	//ILIFE-2472 Starts
	private MandTableDAM mandIO = new MandTableDAM();
	private static final String mandrec = "MANDREC";
	//ILIFE-2472 Ends
	//IFSU-852 starts
	boolean isbankclassificationpermission = false;
	//IFSU-852 ends
	private Batckey wsaaBatckey = new Batckey();
	private BabrpfDAO babrpfDao = getApplicationContext().getBean("babrpfDAO", BabrpfDAO.class);
	

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		checkForErrors2080,
		exit2090
	}

	public P5070() {
		super();
		screenVars = sv;
		new ScreenModel("S5070", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
			initialise1010();
		}

protected void initialise1010()
	{
		wsspcomn.bchaction.set(SPACES);
		sv.dataArea.set(SPACES);
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		payrIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		clrfIO.setForepfx(chdrlnbIO.getChdrpfx());
		clrfIO.setForecoy(payrIO.getChdrcoy());
		wsaaChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFormat(clrfrec);
		clrfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(clrfIO.getStatuz());
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
		sv.payrnum.set(clrfIO.getClntnum());
		sv.numsel.set(clrfIO.getClntnum());
		a1000GetPayorname();
		sv.payorname.set(namadrsrec.name);
		wsaaClntkey.set(SPACES);
		if (isNE(clrfIO.getClntnum(),SPACES)) {
			wsaaClntkey.set(wsspcomn.clntkey);
			wsspcomn.clntkey.set(SPACES);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clrfIO.getClntpfx());
			stringVariable1.addExpression(clrfIO.getClntcoy());
			stringVariable1.addExpression(clrfIO.getClntnum());
			stringVariable1.setStringInto(wsspcomn.clntkey);
		}
		sv.currcode.set(SPACES);
		sv.facthous.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.billcd.set(payrIO.getBillcd());
		/*    IF (CHDRLNB-BANKKEY         = SPACES) AND                    */
		/*       (CHDRLNB-BANKACCKEY      = SPACES)                        */
		/*       GO TO 1090-EXIT.                                          */
		/* First time in (create bank details) or*/
		/* not first time in (modify bank details)*/
		/*  MOVE CHDRLNB-MANDREF        TO S5070-MANDREF.           <005>*/
		/* MOVE PAYR-MANDREF           TO S5070-MANDREF.        <V76F13>*/
		/*IFSU-852 starts*/
		isbankclassificationpermission = false;
		isbankclassificationpermission=FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "MTL29603",appVars, "IT");
		 /*IFSU-852 starts*/
	    if(!isbankclassificationpermission){
	    	 sv.mrbnkOut[varcom.nd.toInt()].set("Y");
	    }
	    /*IFSU-852 ends*/
		/*IFSU-852 ends*/
		keepMand(); //ILIFE-2742
		
		if (isNE(payrIO.getMandref(), SPACES)) {
			if (isEQ(payrIO.getBillchnl(), "D")) {
		sv.mandref.set(payrIO.getMandref());
		}
			else {
				sv.mandref.set(SPACES);
				return ;
			}
		}
		/* Instead of using CHDRLNB-BANKKEY and CHDRLNB-BANKACCKEY*/
		/* to read CLBL, we will use the information obtained from*/
		/* MANDPF*/
		/*    MOVE CHDRLNB-BANKKEY        TO CLBL-BANKKEY                  */
		/*                                   S5070-BANKKEY.                */
		/*    MOVE CHDRLNB-BANKACCKEY     TO CLBL-BANKACCKEY               */
		/*                                   S5070-BANKACCKEY.             */
		/* If in enquiry mode, protect field                               */
		/* IF WSSP-FLAG = 'I'                                   <V76F13>*/
		/*    MOVE 'Y'                 TO S5070-MANDREF-OUT(PR) <V76F13>*/
		/* END-IF.                                              <V76F13>*/
		/*  IF CHDRLNB-MANDREF          = SPACES                    <005>*/
		if (isEQ(payrIO.getMandref(),SPACES)) {
			return ;
		}
		mandlnbIO.setParams(SPACES);
		mandlnbIO.setPayrcoy(wsspcomn.fsuco);
		mandlnbIO.setPayrnum(sv.payrnum);
		mandlnbIO.setMandref(chdrlnbIO.getMandref());
		mandlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mandlnbIO);
		if ((isNE(mandlnbIO.getStatuz(),varcom.oK))
		&& (isNE(mandlnbIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(mandlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(mandlnbIO.getStatuz(),varcom.mrnf)) {
			sv.mandrefErr.set("H929");
			return ;
		}
		clblIO.setParams(SPACES);
		clblIO.setBankkey(mandlnbIO.getBankkey());
		sv.bankkey.set(mandlnbIO.getBankkey());
		clblIO.setBankacckey(mandlnbIO.getBankacckey());
		// IFSU-2113 :Reopen
		if (isEQ(wsspcomn.flag, "I") || isEQ(wsspcomn.flag, "R")) {
			if (!StringUtil.isEmptyOrSpace((mandlnbIO.getBankacckey().toString()))) {
				String masked = MaskingBankAccUtil.getInstance()
						.doMaskingBankAcc((mandlnbIO.getBankacckey().trim()));/* IJTI-1523 */
				sv.bankacckey.set(masked);
			}
		} else {
			sv.bankacckey.set(mandlnbIO.getBankacckey());
		}
		clblIO.setClntcoy(wsspcomn.fsuco);
		if (isEQ(clrfIO.getClntnum(),SPACES)) {
			clblIO.setClntnum(chdrlnbIO.getCownnum());
		}
		else {
			clblIO.setClntnum(clrfIO.getClntnum());
		}
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if (isNE(clblIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clblIO.getParams());
			fatalError600();
		}
		if (isEQ(clrfIO.getClntnum(),SPACES)) {
			if (isNE(chdrlnbIO.getCownnum(),clblIO.getClntnum())) {
				sv.payrnumErr.set(errorsInner.f373);
				return ;
			}
		}
		else {
			if (isNE(clrfIO.getClntnum(),clblIO.getClntnum())) {
				sv.payrnumErr.set(errorsInner.f373);
				return ;
			}
		}
		if (isNE(clblIO.getClntnum(),sv.payrnum)) {
			sv.facthous.set(SPACES);
			sv.longdesc.set(SPACES);
			sv.bankkey.set(SPACES);
			sv.bankacckey.set(SPACES);
			return ;
		}
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if ((isNE(babrIO.getStatuz(),varcom.oK))
		&& (isNE(babrIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
		//sv.mrbnk.set(clblIO.getMrbnk()); 			// ILIFE-2476
		/*IFSU-852 starts*/
		if(isbankclassificationpermission){
			sv.mrbnk.set(clblIO.getMrbnk());
		}
		else{
			sv.mrbnk.set(SPACES);
		}
		/*IFSU-852 ends*/
		sv.bnkbrn.set(clblIO.getBnkbrn()); 			// ILIFE-2476
		wsbbBankdesc.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsbbBankdescLine1);
		sv.branchdesc.set(wsbbBankdescLine2);
		sv.facthous.set(clblIO.getFacthous());
		wsspcomn.typinj.set(sv.facthous);
		if (isNE(sv.facthous,SPACES)) {
			a1200GetFacthousDesc();
		}
	}
//ILIFE-2742 Starts
protected void keepMand(){
	mandIO.setPayrcoy(wsspcomn.fsuco);
	mandIO.setFormat(mandrec);
	mandIO.setMandref(sv.mandref);
	mandIO.setPayrnum(sv.payrnum);
	wsspcomn.chdrCownnum.set(sv.payrnum);
	mandIO.setTransactionDate(varcom.vrcmDate);
	mandIO.setTermid(varcom.vrcmTermid);
	mandIO.setUser(varcom.vrcmUser);
	mandIO.setTransactionTime(varcom.vrcmTime);
	mandIO.setTimesUse(999);
	mandIO.setStatChangeDate(varcom.vrcmMaxDate);
	mandIO.setLastUseDate(varcom.vrcmMaxDate);
	mandIO.setEffdate(varcom.vrcmMaxDate);
	mandIO.setLastUseAmt(ZERO);
	mandIO.setMandAmt(ZERO);
	mandIO.setFunction(varcom.keeps);
	SmartFileCode.execute(appVars, mandIO);
	if (isNE(mandIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(mandIO.getParams());
		fatalError600();
	}
}
//ILIFE-2742 Ends

protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.clntkey.set(SPACES);
		/* If in enquiry mode, protect field                               */
		if (isEQ(wsspcomn.flag, "I")) {
			sv.mandrefOut[varcom.pr.toInt()].set("Y");
		}
		 /*IFSU-852 starts*/
	    if(!isbankclassificationpermission){
	    	 sv.mrbnkOut[varcom.nd.toInt()].set("Y");
	    }
	    /*IFSU-852 ends*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					screenIo2010();
					validate2020();
				case checkForErrors2080:
					checkForErrors2080();
					bankbrndsc();
				case exit2090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S5070IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                S5070-DATA-AREA.                 */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.mandref,SPACES)) {
			sv.mandrefErr.set(errorsInner.h926);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.mandref,SPACES)) {
			if (isNE(sv.mandref,NUMERIC)) {
				sv.mandrefErr.set(errorsInner.h421);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
		mandlnbIO.setParams(SPACES);
		mandlnbIO.setPayrcoy(wsspcomn.fsuco);
		mandlnbIO.setPayrnum(sv.payrnum);
		mandlnbIO.setMandref(sv.mandref);
		mandlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mandlnbIO);
		if ((isNE(mandlnbIO.getStatuz(),varcom.oK))
		&& (isNE(mandlnbIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(mandlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(mandlnbIO.getStatuz(),varcom.mrnf)
				|| isEQ(mandlnbIO.getPayind(), "P")) { // ILIFE-2472
			sv.mandrefErr.set(errorsInner.h928);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(mandlnbIO.getCrcind(), "C")) {
			sv.mandrefErr.set(errorsInner.h928);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* Move bank account details to corresponding fields*/
		sv.bankkey.set(mandlnbIO.getBankkey());
		sv.bankacckey.set(mandlnbIO.getBankacckey());
	}

protected void validate2020()
	{
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if ((isNE(babrIO.getStatuz(),varcom.oK))
		&& (isNE(babrIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		if (isEQ(babrIO.getStatuz(),varcom.mrnf)) {
			sv.bankkeyErr.set(errorsInner.e756);
			goTo(GotoLabel.checkForErrors2080);
		}
		wsbbBankdesc.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsbbBankdescLine1);
		sv.branchdesc.set(wsbbBankdescLine2);
		clblIO.setBankkey(sv.bankkey);
		clblIO.setBankacckey(sv.bankacckey);
		clblIO.setClntcoy(wsspcomn.fsuco);
		clblIO.setClntnum(sv.payrnum);
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if ((isNE(clblIO.getStatuz(),varcom.oK))
		&& (isNE(clblIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(clblIO.getParams());
			fatalError600();
		}
		if (isEQ(clblIO.getStatuz(),varcom.mrnf)) {
			sv.bankacckeyErr.set(errorsInner.g600);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isNE(clblIO.getClntnum(),sv.payrnum)) {
			sv.bankacckeyErr.set(errorsInner.f373);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isNE(chdrlnbIO.getBillcurr(),clblIO.getCurrcode())) {
			sv.bankacckeyErr.set(errorsInner.f955);
			goTo(GotoLabel.checkForErrors2080);
		}
		//sv.mrbnk.set(clblIO.getMrbnk()); 			// ILIFE-2476
		/*IFSU-852 starts*/
		if(isbankclassificationpermission){
			sv.mrbnk.set(clblIO.getMrbnk());
		}
		else{
			sv.mrbnk.set(SPACES);
		}
		/*IFSU-852 ends*/
		sv.bnkbrn.set(clblIO.getBnkbrn()); 			// ILIFE-2476
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
		sv.facthous.set(clblIO.getFacthous());
		wsspcomn.typinj.set(sv.facthous);
		if (isNE(sv.facthous,SPACES)) {
			a1200GetFacthousDesc();
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}
////ILIFE-2476 Starts
//		protected void bankbrndsc() {
//			if(isEQ(sv.bnkbrn, SPACES)){
//				sv.bnkbrn.set("000"+ sv.bankacckey.sub1string(1, 3));
//			}
//			
//			babrIO.setParams(SPACES);
//		    	babrIO.setDataKey(SPACES);
//		    	babrIO.setFunction("BEGN");	
//		    	babrIO.setAppflag("Y");	
//			SmartFileCode.execute(appVars, babrIO);
//		    
//			for (; !(isEQ(babrIO.getStatuz(),"ENDP")); ){        
//		        babrIO.setFunction("NEXTR");
//		        SmartFileCode.execute(appVars, babrIO);        
//		        if (isEQ(babrIO.getStatuz(), "ENDP")
//		                    || isEQ(babrIO.getBankkey().sub1string(5, 6), sv.bnkbrn)) {
//		        	
//		        	if(isEQ(babrIO.getBankkey().sub1string(5, 6), sv.bnkbrn))
//		        		sv.bnkbrndesc.set(babrIO.getBankdesc());
//		        	else 
//		        		sv.bnkbrndesc.set("Branch Name not exist");
//		        	babrIO.setStatuz("ENDP");
//		        }
//		    		}
//			}
//		//ILIFE-2476 Ends
//		
		
		//ILIFE-2476 Starts
		protected void bankbrndsc() {
			if(isEQ(sv.bnkbrn, SPACES)){
				sv.bnkbrn.set("000"+ sv.bankacckey.sub1string(1, 3));
			}
			List<Babrpf> lstBabrpf = babrpfDao.getRecordsByBankKey(sv.bnkbrn.toString());
			if(lstBabrpf.isEmpty()){
				sv.bnkbrndesc.set("Branch Name not exist");
			}else{
				for(Babrpf babrpf : lstBabrpf){
					if(babrpf.getBankkey().substring(5, 6).equals(sv.bnkbrn.toString()) && "Y".equals(babrpf.getAppflag())){
						sv.bnkbrndesc.set(babrpf.getBankdesc());
						break;
					}
				}
			}
		}//ILIFE-2476 Ends

protected void update3000()
	{
			updateDatabase3010();
		}

protected void updateDatabase3010()
	{
		if (isNE(wsaaClntkey,SPACES)) {
			wsspcomn.clntkey.set(wsaaClntkey);
		}
		if (isEQ(wsspcomn.flag,"I")) {
			return ;
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			return ;
		}
		chdrlnbIO.setMandref(sv.mandref);
		chdrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		payrIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		if(isNE(wsaaBatckey.batcBatctrcde.toString(),"TAFP") || isNE(wsaaBatckey.batcBatctrcde.toString(),"TAFQ")
				|| isNE(wsaaBatckey.batcBatctrcde.toString(),"TAFR")){
			payrIO.setMandref(sv.mandref);
			payrIO.setFunction(varcom.updat);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(payrIO.getParams());
				fatalError600();
			}
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void a1000GetPayorname()
	{
		/*A1010-NAMADRS*/
		initialize(namadrsrec.namadrsRec);
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(sv.payrnum);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set(wsaaLargeName);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		/*A1090-EXIT*/
	}

protected void a1200GetFacthousDesc()
	{
		a1210Desc();
	}

protected void a1210Desc()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.fsuco);
		descIO.setDesctabl(t3684);
		descIO.setDescitem(sv.facthous);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		sv.longdesc.set(descIO.getLongdesc());
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner {
		/* ERRORS */
	private FixedLengthStringData f955 = new FixedLengthStringData(4).init("F955");
	private FixedLengthStringData e756 = new FixedLengthStringData(4).init("E756");
	private FixedLengthStringData g600 = new FixedLengthStringData(4).init("G600");
	private FixedLengthStringData f373 = new FixedLengthStringData(4).init("F373");
	private FixedLengthStringData h926 = new FixedLengthStringData(4).init("H926");
	private FixedLengthStringData h421 = new FixedLengthStringData(4).init("H421");
	private FixedLengthStringData h928 = new FixedLengthStringData(4).init("H928");
}
}
