package com.csc.life.newbusiness.dataaccess;

import com.csc.fsu.general.dataaccess.ChdrpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HchrmpsTableDAM.java
 * Date: Sun, 30 Aug 2009 03:40:15
 * Class transformed from HCHRMPS.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HchrmpsTableDAM extends ChdrpfTableDAM {

	public HchrmpsTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("HCHRMPS");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRPFX, " +
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "RECODE, " +
		            "SERVUNIT, " +
		            "CNTTYPE, " +
		            "TRANNO, " +
		            "TRANID, " +
		            "VALIDFLAG, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "PROCTRANCD, " +
		            "PROCFLAG, " +
		            "PROCID, " +
		            "STATCODE, " +
		            "STATREASN, " +
		            "STATDATE, " +
		            "STATTRAN, " +
		            "TRANLUSED, " +
		            "OCCDATE, " +
		            "CCDATE, " +
		            "CRDATE, " +
		            "ANNAMT01, " +
		            "ANNAMT02, " +
		            "ANNAMT03, " +
		            "ANNAMT04, " +
		            "ANNAMT05, " +
		            "ANNAMT06, " +
		            "RNLTYPE, " +
		            "RNLNOTS, " +
		            "RNLNOTTO, " +
		            "RNLATTN, " +
		            "RNLDURN, " +
		            "REPTYPE, " +
		            "REPNUM, " +
		            "COWNPFX, " +
		            "COWNCOY, " +
		            "COWNNUM, " +
		            "PAYRPFX, " +
		            "PAYRCOY, " +
		            "PAYRNUM, " +
		            "DESPPFX, " +
		            "DESPCOY, " +
		            "DESPNUM, " +
		            "ASGNPFX, " +
		            "ASGNCOY, " +
		            "ASGNNUM, " +
		            "CNTBRANCH, " +
		            "AGNTPFX, " +
		            "AGNTCOY, " +
		            "AGNTNUM, " +
		            "CNTCURR, " +
		            "ACCTCCY, " +
		            "CRATE, " +
		            "PAYPLAN, " +
		            "ACCTMETH, " +
		            "BILLFREQ, " +
		            "BILLCHNL, " +
		            "COLLCHNL, " +
		            "MANDREF, " +
		            "BILLDAY, " +
		            "BILLMONTH, " +
		            "BILLCD, " +
		            "BTDATE, " +
		            "PTDATE, " +
		            "PAYFLAG, " +
		            "SINSTFROM, " +
		            "SINSTTO, " +
		            "SINSTAMT01, " +
		            "SINSTAMT02, " +
		            "SINSTAMT03, " +
		            "SINSTAMT04, " +
		            "SINSTAMT05, " +
		            "SINSTAMT06, " +
		            "INSTFROM, " +
		            "INSTTO, " +
		            "INSTBCHNL, " +
		            "INSTCCHNL, " +
		            "INSTFREQ, " +
		            "INSTTOT01, " +
		            "INSTTOT02, " +
		            "INSTTOT03, " +
		            "INSTTOT04, " +
		            "INSTTOT05, " +
		            "INSTTOT06, " +
		            "ISAM01, " +
		            "ISAM02, " +
		            "ISAM03, " +
		            "ISAM04, " +
		            "ISAM05, " +
		            "ISAM06, " +
		            "INSTPAST01, " +
		            "INSTPAST02, " +
		            "INSTPAST03, " +
		            "INSTPAST04, " +
		            "INSTPAST05, " +
		            "INSTPAST06, " +
		            "INSTJCTL, " +
		            "NOFOUTINST, " +
		            "OUTSTAMT, " +
		            "BILLDATE01, " +
		            "BILLDATE02, " +
		            "BILLDATE03, " +
		            "BILLDATE04, " +
		            "BILLAMT01, " +
		            "BILLAMT02, " +
		            "BILLAMT03, " +
		            "BILLAMT04, " +
		            "FACTHOUS, " +
		            "BANKKEY, " +
		            "BANKACCKEY, " +
		            "DISCODE01, " +
		            "DISCODE02, " +
		            "DISCODE03, " +
		            "DISCODE04, " +
		            "GRUPKEY, " +
		            "MEMBSEL, " +
		            "APLSUPR, " +
		            "BILLSUPR, " +
		            "COMMSUPR, " +
		            "LAPSSUPR, " +
		            "MAILSUPR, " +
		            "NOTSSUPR, " +
		            "RNWLSUPR, " +
		            "APLSPFROM, " +
		            "BILLSPFROM, " +
		            "COMMSPFROM, " +
		            "LAPSSPFROM, " +
		            "MAILSPFROM, " +
		            "NOTSSPFROM, " +
		            "RNWLSPFROM, " +
		            "APLSPTO, " +
		            "BILLSPTO, " +
		            "COMMSPTO, " +
		            "LAPSSPTO, " +
		            "MAILSPTO, " +
		            "NOTSSPTO, " +
		            "RNWLSPTO, " +
		            "CAMPAIGN, " +
		            "SRCEBUS, " +
		            "STCA, " +
		            "STCB, " +
		            "STCC, " +
		            "STCD, " +
		            "STCE, " +
		            "NOFRISKS, " +
		            "JACKET, " +
		            "PSTCDE, " +
		            "PSTRSN, " +
		            "PSTDAT, " +
		            "PSTTRN, " +
		            "PDIND, " +
		            "REG, " +
		            "MPLPFX, " +
		            "MPLCOY, " +
		            "MPLNUM, " +
		            "CLUPFX, " +
		            "CLUCOY, " +
		            "CLUNUM, " +
		            "FINPFX, " +
		            "FINCOY, " +
		            "FINNUM, " +
		            "POAPFX, " +
		            "POACOY, " +
		            "POANUM, " +
		            "DUEFLG, " +
		            "LAPRIND, " +
		            "SPECIND, " +
		            "CHGFLAG, " +
		            "POLPLN, " +
		            "WVFDAT, " +
		            "WVTDAT, " +
		            "WVFIND, " +
		            "BFCHARGE, " +
		            "DISHNRCNT, " +
		            "PDTYPE, " +
		            "DISHNRDTE, " +
		            "STMPDTYAMT, " +
		            "STMPDTYDTE, " +
		            "COVERNT, " +
		            "CNTISS, " +
		            "CNTRCV, " +
		            "QUOTENO, " +
		            "BANKCODE, " +
		            "DOCNUM, " +
		            "RNLSTS, " +
		            "SUSTRCDE, " +
		            "DTECAN, " +
		            "COTYPE, " +
		            "COPPN, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "ZMANDREF," +//ILIFE-2472
		            "REQNTYPE," +//ILIFE-2472
		            "PAYCLT," + //ILIFE-2472 PH2
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrpfx,
                               chdrcoy,
                               chdrnum,
                               recode,
                               servunit,
                               cnttype,
                               tranno,
                               tranid,
                               validflag,
                               currfrom,
                               currto,
                               proctrancd,
                               procflag,
                               procid,
                               statcode,
                               statreasn,
                               statdate,
                               stattran,
                               tranlused,
                               occdate,
                               ccdate,
                               crdate,
                               annamt01,
                               annamt02,
                               annamt03,
                               annamt04,
                               annamt05,
                               annamt06,
                               rnltype,
                               rnlnots,
                               rnlnotto,
                               rnlattn,
                               rnldurn,
                               reptype,
                               repnum,
                               cownpfx,
                               cowncoy,
                               cownnum,
                               payrpfx,
                               payrcoy,
                               payrnum,
                               desppfx,
                               despcoy,
                               despnum,
                               asgnpfx,
                               asgncoy,
                               asgnnum,
                               cntbranch,
                               agntpfx,
                               agntcoy,
                               agntnum,
                               cntcurr,
                               acctccy,
                               crate,
                               payplan,
                               acctmeth,
                               billfreq,
                               billchnl,
                               collchnl,
                               mandref,
                               billday,
                               billmonth,
                               billcd,
                               btdate,
                               ptdate,
                               payflag,
                               sinstfrom,
                               sinstto,
                               sinstamt01,
                               sinstamt02,
                               sinstamt03,
                               sinstamt04,
                               sinstamt05,
                               sinstamt06,
                               instfrom,
                               instto,
                               instbchnl,
                               instcchnl,
                               instfreq,
                               insttot01,
                               insttot02,
                               insttot03,
                               insttot04,
                               insttot05,
                               insttot06,
                               inststamt01,
                               inststamt02,
                               inststamt03,
                               inststamt04,
                               inststamt05,
                               inststamt06,
                               instpast01,
                               instpast02,
                               instpast03,
                               instpast04,
                               instpast05,
                               instpast06,
                               instjctl,
                               nofoutinst,
                               outstamt,
                               billdate01,
                               billdate02,
                               billdate03,
                               billdate04,
                               billamt01,
                               billamt02,
                               billamt03,
                               billamt04,
                               facthous,
                               bankkey,
                               bankacckey,
                               discode01,
                               discode02,
                               discode03,
                               discode04,
                               grupkey,
                               membsel,
                               aplsupr,
                               billsupr,
                               commsupr,
                               lapssupr,
                               mailsupr,
                               notssupr,
                               rnwlsupr,
                               aplspfrom,
                               billspfrom,
                               commspfrom,
                               lapsspfrom,
                               mailspfrom,
                               notsspfrom,
                               rnwlspfrom,
                               aplspto,
                               billspto,
                               commspto,
                               lapsspto,
                               mailspto,
                               notsspto,
                               rnwlspto,
                               campaign,
                               srcebus,
                               chdrstcda,
                               chdrstcdb,
                               chdrstcdc,
                               chdrstcdd,
                               chdrstcde,
                               nofrisks,
                               jacket,
                               pstatcode,
                               pstatreasn,
                               pstatdate,
                               pstattran,
                               pdind,
                               register,
                               mplpfx,
                               mplcoy,
                               mplnum,
                               clupfx,
                               clucoy,
                               clunum,
                               finpfx,
                               fincoy,
                               finnum,
                               poapfx,
                               poacoy,
                               poanum,
                               dueflg,
                               laprind,
                               specind,
                               chgflag,
                               polpln,
                               wvfdat,
                               wvtdat,
                               wvfind,
                               bfcharge,
                               dishnrcnt,
                               pdtype,
                               dishnrdte,
                               stmpdtyamt,
                               stmpdtydte,
                               covernt,
                               cntiss,
                               cntrcv,
                               quoteno,
                               bankcode,
                               docnum,
                               rnlsts,
                               sustrcde,
                               dtecan,
                               cotype,
                               coppn,
                               userProfile,
                               jobName,
                               datime,
                               zmandref,//ILIFE-2472
                               reqntype,//ILIFE-2472
                               payclt, //ILIFE-2472 PH2
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(63);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(1);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller20.setInternal(chdrcoy.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		//FixedLengthStringData nonKeyData = new FixedLengthStringData(1008);
		//FixedLengthStringData nonKeyData = new FixedLengthStringData(1014);
		FixedLengthStringData nonKeyData = new FixedLengthStringData(1022);//ILIFE-2472 PH2
		nonKeyData.set(
					getChdrpfx().toInternal()
					+ nonKeyFiller20.toInternal()
					+ getChdrnum().toInternal()
					+ getRecode().toInternal()
					+ getServunit().toInternal()
					+ getCnttype().toInternal()
					+ getTranno().toInternal()
					+ getTranid().toInternal()
					+ getValidflag().toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getProctrancd().toInternal()
					+ getProcflag().toInternal()
					+ getProcid().toInternal()
					+ getStatcode().toInternal()
					+ getStatreasn().toInternal()
					+ getStatdate().toInternal()
					+ getStattran().toInternal()
					+ getTranlused().toInternal()
					+ getOccdate().toInternal()
					+ getCcdate().toInternal()
					+ getCrdate().toInternal()
					+ getAnnamt01().toInternal()
					+ getAnnamt02().toInternal()
					+ getAnnamt03().toInternal()
					+ getAnnamt04().toInternal()
					+ getAnnamt05().toInternal()
					+ getAnnamt06().toInternal()
					+ getRnltype().toInternal()
					+ getRnlnots().toInternal()
					+ getRnlnotto().toInternal()
					+ getRnlattn().toInternal()
					+ getRnldurn().toInternal()
					+ getReptype().toInternal()
					+ getRepnum().toInternal()
					+ getCownpfx().toInternal()
					+ getCowncoy().toInternal()
					+ getCownnum().toInternal()
					+ getPayrpfx().toInternal()
					+ getPayrcoy().toInternal()
					+ getPayrnum().toInternal()
					+ getDesppfx().toInternal()
					+ getDespcoy().toInternal()
					+ getDespnum().toInternal()
					+ getAsgnpfx().toInternal()
					+ getAsgncoy().toInternal()
					+ getAsgnnum().toInternal()
					+ getCntbranch().toInternal()
					+ getAgntpfx().toInternal()
					+ getAgntcoy().toInternal()
					+ getAgntnum().toInternal()
					+ getCntcurr().toInternal()
					+ getAcctccy().toInternal()
					+ getCrate().toInternal()
					+ getPayplan().toInternal()
					+ getAcctmeth().toInternal()
					+ getBillfreq().toInternal()
					+ getBillchnl().toInternal()
					+ getCollchnl().toInternal()
					+ getMandref().toInternal()
					+ getBillday().toInternal()
					+ getBillmonth().toInternal()
					+ getBillcd().toInternal()
					+ getBtdate().toInternal()
					+ getPtdate().toInternal()
					+ getPayflag().toInternal()
					+ getSinstfrom().toInternal()
					+ getSinstto().toInternal()
					+ getSinstamt01().toInternal()
					+ getSinstamt02().toInternal()
					+ getSinstamt03().toInternal()
					+ getSinstamt04().toInternal()
					+ getSinstamt05().toInternal()
					+ getSinstamt06().toInternal()
					+ getInstfrom().toInternal()
					+ getInstto().toInternal()
					+ getInstbchnl().toInternal()
					+ getInstcchnl().toInternal()
					+ getInstfreq().toInternal()
					+ getInsttot01().toInternal()
					+ getInsttot02().toInternal()
					+ getInsttot03().toInternal()
					+ getInsttot04().toInternal()
					+ getInsttot05().toInternal()
					+ getInsttot06().toInternal()
					+ getInststamt01().toInternal()
					+ getInststamt02().toInternal()
					+ getInststamt03().toInternal()
					+ getInststamt04().toInternal()
					+ getInststamt05().toInternal()
					+ getInststamt06().toInternal()
					+ getInstpast01().toInternal()
					+ getInstpast02().toInternal()
					+ getInstpast03().toInternal()
					+ getInstpast04().toInternal()
					+ getInstpast05().toInternal()
					+ getInstpast06().toInternal()
					+ getInstjctl().toInternal()
					+ getNofoutinst().toInternal()
					+ getOutstamt().toInternal()
					+ getBilldate01().toInternal()
					+ getBilldate02().toInternal()
					+ getBilldate03().toInternal()
					+ getBilldate04().toInternal()
					+ getBillamt01().toInternal()
					+ getBillamt02().toInternal()
					+ getBillamt03().toInternal()
					+ getBillamt04().toInternal()
					+ getFacthous().toInternal()
					+ getBankkey().toInternal()
					+ getBankacckey().toInternal()
					+ getDiscode01().toInternal()
					+ getDiscode02().toInternal()
					+ getDiscode03().toInternal()
					+ getDiscode04().toInternal()
					+ getGrupkey().toInternal()
					+ getMembsel().toInternal()
					+ getAplsupr().toInternal()
					+ getBillsupr().toInternal()
					+ getCommsupr().toInternal()
					+ getLapssupr().toInternal()
					+ getMailsupr().toInternal()
					+ getNotssupr().toInternal()
					+ getRnwlsupr().toInternal()
					+ getAplspfrom().toInternal()
					+ getBillspfrom().toInternal()
					+ getCommspfrom().toInternal()
					+ getLapsspfrom().toInternal()
					+ getMailspfrom().toInternal()
					+ getNotsspfrom().toInternal()
					+ getRnwlspfrom().toInternal()
					+ getAplspto().toInternal()
					+ getBillspto().toInternal()
					+ getCommspto().toInternal()
					+ getLapsspto().toInternal()
					+ getMailspto().toInternal()
					+ getNotsspto().toInternal()
					+ getRnwlspto().toInternal()
					+ getCampaign().toInternal()
					+ getSrcebus().toInternal()
					+ getChdrstcda().toInternal()
					+ getChdrstcdb().toInternal()
					+ getChdrstcdc().toInternal()
					+ getChdrstcdd().toInternal()
					+ getChdrstcde().toInternal()
					+ getNofrisks().toInternal()
					+ getJacket().toInternal()
					+ getPstatcode().toInternal()
					+ getPstatreasn().toInternal()
					+ getPstatdate().toInternal()
					+ getPstattran().toInternal()
					+ getPdind().toInternal()
					+ getRegister().toInternal()
					+ getMplpfx().toInternal()
					+ getMplcoy().toInternal()
					+ getMplnum().toInternal()
					+ getClupfx().toInternal()
					+ getClucoy().toInternal()
					+ getClunum().toInternal()
					+ getFinpfx().toInternal()
					+ getFincoy().toInternal()
					+ getFinnum().toInternal()
					+ getPoapfx().toInternal()
					+ getPoacoy().toInternal()
					+ getPoanum().toInternal()
					+ getDueflg().toInternal()
					+ getLaprind().toInternal()
					+ getSpecind().toInternal()
					+ getChgflag().toInternal()
					+ getPolpln().toInternal()
					+ getWvfdat().toInternal()
					+ getWvtdat().toInternal()
					+ getWvfind().toInternal()
					+ getBfcharge().toInternal()
					+ getDishnrcnt().toInternal()
					+ getPdtype().toInternal()
					+ getDishnrdte().toInternal()
					+ getStmpdtyamt().toInternal()
					+ getStmpdtydte().toInternal()
					+ getCovernt().toInternal()
					+ getCntiss().toInternal()
					+ getCntrcv().toInternal()
					+ getQuoteno().toInternal()
					+ getBankcode().toInternal()
					+ getDocnum().toInternal()
					+ getRnlsts().toInternal()
					+ getSustrcde().toInternal()
					+ getDtecan().toInternal()
					+ getCotype().toInternal()
					+ getCoppn().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()
					+ getZmandref().toInternal()//ILIFE-2472
					+ getReqntype().toInternal()//ILIFE-2472
					+ getPayclt().toInternal()); //ILIFE-2472 PH2
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrpfx);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, recode);
			what = ExternalData.chop(what, servunit);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, tranid);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, proctrancd);
			what = ExternalData.chop(what, procflag);
			what = ExternalData.chop(what, procid);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, statreasn);
			what = ExternalData.chop(what, statdate);
			what = ExternalData.chop(what, stattran);
			what = ExternalData.chop(what, tranlused);
			what = ExternalData.chop(what, occdate);
			what = ExternalData.chop(what, ccdate);
			what = ExternalData.chop(what, crdate);
			what = ExternalData.chop(what, annamt01);
			what = ExternalData.chop(what, annamt02);
			what = ExternalData.chop(what, annamt03);
			what = ExternalData.chop(what, annamt04);
			what = ExternalData.chop(what, annamt05);
			what = ExternalData.chop(what, annamt06);
			what = ExternalData.chop(what, rnltype);
			what = ExternalData.chop(what, rnlnots);
			what = ExternalData.chop(what, rnlnotto);
			what = ExternalData.chop(what, rnlattn);
			what = ExternalData.chop(what, rnldurn);
			what = ExternalData.chop(what, reptype);
			what = ExternalData.chop(what, repnum);
			what = ExternalData.chop(what, cownpfx);
			what = ExternalData.chop(what, cowncoy);
			what = ExternalData.chop(what, cownnum);
			what = ExternalData.chop(what, payrpfx);
			what = ExternalData.chop(what, payrcoy);
			what = ExternalData.chop(what, payrnum);
			what = ExternalData.chop(what, desppfx);
			what = ExternalData.chop(what, despcoy);
			what = ExternalData.chop(what, despnum);
			what = ExternalData.chop(what, asgnpfx);
			what = ExternalData.chop(what, asgncoy);
			what = ExternalData.chop(what, asgnnum);
			what = ExternalData.chop(what, cntbranch);
			what = ExternalData.chop(what, agntpfx);
			what = ExternalData.chop(what, agntcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, acctccy);
			what = ExternalData.chop(what, crate);
			what = ExternalData.chop(what, payplan);
			what = ExternalData.chop(what, acctmeth);
			what = ExternalData.chop(what, billfreq);
			what = ExternalData.chop(what, billchnl);
			what = ExternalData.chop(what, collchnl);
			what = ExternalData.chop(what, mandref);
			what = ExternalData.chop(what, billday);
			what = ExternalData.chop(what, billmonth);
			what = ExternalData.chop(what, billcd);
			what = ExternalData.chop(what, btdate);
			what = ExternalData.chop(what, ptdate);
			what = ExternalData.chop(what, payflag);
			what = ExternalData.chop(what, sinstfrom);
			what = ExternalData.chop(what, sinstto);
			what = ExternalData.chop(what, sinstamt01);
			what = ExternalData.chop(what, sinstamt02);
			what = ExternalData.chop(what, sinstamt03);
			what = ExternalData.chop(what, sinstamt04);
			what = ExternalData.chop(what, sinstamt05);
			what = ExternalData.chop(what, sinstamt06);
			what = ExternalData.chop(what, instfrom);
			what = ExternalData.chop(what, instto);
			what = ExternalData.chop(what, instbchnl);
			what = ExternalData.chop(what, instcchnl);
			what = ExternalData.chop(what, instfreq);
			what = ExternalData.chop(what, insttot01);
			what = ExternalData.chop(what, insttot02);
			what = ExternalData.chop(what, insttot03);
			what = ExternalData.chop(what, insttot04);
			what = ExternalData.chop(what, insttot05);
			what = ExternalData.chop(what, insttot06);
			what = ExternalData.chop(what, inststamt01);
			what = ExternalData.chop(what, inststamt02);
			what = ExternalData.chop(what, inststamt03);
			what = ExternalData.chop(what, inststamt04);
			what = ExternalData.chop(what, inststamt05);
			what = ExternalData.chop(what, inststamt06);
			what = ExternalData.chop(what, instpast01);
			what = ExternalData.chop(what, instpast02);
			what = ExternalData.chop(what, instpast03);
			what = ExternalData.chop(what, instpast04);
			what = ExternalData.chop(what, instpast05);
			what = ExternalData.chop(what, instpast06);
			what = ExternalData.chop(what, instjctl);
			what = ExternalData.chop(what, nofoutinst);
			what = ExternalData.chop(what, outstamt);
			what = ExternalData.chop(what, billdate01);
			what = ExternalData.chop(what, billdate02);
			what = ExternalData.chop(what, billdate03);
			what = ExternalData.chop(what, billdate04);
			what = ExternalData.chop(what, billamt01);
			what = ExternalData.chop(what, billamt02);
			what = ExternalData.chop(what, billamt03);
			what = ExternalData.chop(what, billamt04);
			what = ExternalData.chop(what, facthous);
			what = ExternalData.chop(what, bankkey);
			what = ExternalData.chop(what, bankacckey);
			what = ExternalData.chop(what, discode01);
			what = ExternalData.chop(what, discode02);
			what = ExternalData.chop(what, discode03);
			what = ExternalData.chop(what, discode04);
			what = ExternalData.chop(what, grupkey);
			what = ExternalData.chop(what, membsel);
			what = ExternalData.chop(what, aplsupr);
			what = ExternalData.chop(what, billsupr);
			what = ExternalData.chop(what, commsupr);
			what = ExternalData.chop(what, lapssupr);
			what = ExternalData.chop(what, mailsupr);
			what = ExternalData.chop(what, notssupr);
			what = ExternalData.chop(what, rnwlsupr);
			what = ExternalData.chop(what, aplspfrom);
			what = ExternalData.chop(what, billspfrom);
			what = ExternalData.chop(what, commspfrom);
			what = ExternalData.chop(what, lapsspfrom);
			what = ExternalData.chop(what, mailspfrom);
			what = ExternalData.chop(what, notsspfrom);
			what = ExternalData.chop(what, rnwlspfrom);
			what = ExternalData.chop(what, aplspto);
			what = ExternalData.chop(what, billspto);
			what = ExternalData.chop(what, commspto);
			what = ExternalData.chop(what, lapsspto);
			what = ExternalData.chop(what, mailspto);
			what = ExternalData.chop(what, notsspto);
			what = ExternalData.chop(what, rnwlspto);
			what = ExternalData.chop(what, campaign);
			what = ExternalData.chop(what, srcebus);
			what = ExternalData.chop(what, chdrstcda);
			what = ExternalData.chop(what, chdrstcdb);
			what = ExternalData.chop(what, chdrstcdc);
			what = ExternalData.chop(what, chdrstcdd);
			what = ExternalData.chop(what, chdrstcde);
			what = ExternalData.chop(what, nofrisks);
			what = ExternalData.chop(what, jacket);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, pstatreasn);
			what = ExternalData.chop(what, pstatdate);
			what = ExternalData.chop(what, pstattran);
			what = ExternalData.chop(what, pdind);
			what = ExternalData.chop(what, register);
			what = ExternalData.chop(what, mplpfx);
			what = ExternalData.chop(what, mplcoy);
			what = ExternalData.chop(what, mplnum);
			what = ExternalData.chop(what, clupfx);
			what = ExternalData.chop(what, clucoy);
			what = ExternalData.chop(what, clunum);
			what = ExternalData.chop(what, finpfx);
			what = ExternalData.chop(what, fincoy);
			what = ExternalData.chop(what, finnum);
			what = ExternalData.chop(what, poapfx);
			what = ExternalData.chop(what, poacoy);
			what = ExternalData.chop(what, poanum);
			what = ExternalData.chop(what, dueflg);
			what = ExternalData.chop(what, laprind);
			what = ExternalData.chop(what, specind);
			what = ExternalData.chop(what, chgflag);
			what = ExternalData.chop(what, polpln);
			what = ExternalData.chop(what, wvfdat);
			what = ExternalData.chop(what, wvtdat);
			what = ExternalData.chop(what, wvfind);
			what = ExternalData.chop(what, bfcharge);
			what = ExternalData.chop(what, dishnrcnt);
			what = ExternalData.chop(what, pdtype);
			what = ExternalData.chop(what, dishnrdte);
			what = ExternalData.chop(what, stmpdtyamt);
			what = ExternalData.chop(what, stmpdtydte);
			what = ExternalData.chop(what, covernt);
			what = ExternalData.chop(what, cntiss);
			what = ExternalData.chop(what, cntrcv);
			what = ExternalData.chop(what, quoteno);
			what = ExternalData.chop(what, bankcode);
			what = ExternalData.chop(what, docnum);
			what = ExternalData.chop(what, rnlsts);
			what = ExternalData.chop(what, sustrcde);
			what = ExternalData.chop(what, dtecan);
			what = ExternalData.chop(what, cotype);
			what = ExternalData.chop(what, coppn);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);
			what = ExternalData.chop(what, zmandref);//ILIFE-2472
			what = ExternalData.chop(what, reqntype);//ILIFE-2472		
			what = ExternalData.chop(what, payclt); //ILIFE-2472 PH2
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(Object what) {
		chdrpfx.set(what);
	}	
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}	
	public FixedLengthStringData getRecode() {
		return recode;
	}
	public void setRecode(Object what) {
		recode.set(what);
	}	
	public FixedLengthStringData getServunit() {
		return servunit;
	}
	public void setServunit(Object what) {
		servunit.set(what);
	}	
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getTranid() {
		return tranid;
	}
	public void setTranid(Object what) {
		tranid.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public FixedLengthStringData getProctrancd() {
		return proctrancd;
	}
	public void setProctrancd(Object what) {
		proctrancd.set(what);
	}	
	public FixedLengthStringData getProcflag() {
		return procflag;
	}
	public void setProcflag(Object what) {
		procflag.set(what);
	}	
	public FixedLengthStringData getProcid() {
		return procid;
	}
	public void setProcid(Object what) {
		procid.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public FixedLengthStringData getStatreasn() {
		return statreasn;
	}
	public void setStatreasn(Object what) {
		statreasn.set(what);
	}	
	public PackedDecimalData getStatdate() {
		return statdate;
	}
	public void setStatdate(Object what) {
		setStatdate(what, false);
	}
	public void setStatdate(Object what, boolean rounded) {
		if (rounded)
			statdate.setRounded(what);
		else
			statdate.set(what);
	}	
	public PackedDecimalData getStattran() {
		return stattran;
	}
	public void setStattran(Object what) {
		setStattran(what, false);
	}
	public void setStattran(Object what, boolean rounded) {
		if (rounded)
			stattran.setRounded(what);
		else
			stattran.set(what);
	}	
	public PackedDecimalData getTranlused() {
		return tranlused;
	}
	public void setTranlused(Object what) {
		setTranlused(what, false);
	}
	public void setTranlused(Object what, boolean rounded) {
		if (rounded)
			tranlused.setRounded(what);
		else
			tranlused.set(what);
	}	
	public PackedDecimalData getOccdate() {
		return occdate;
	}
	public void setOccdate(Object what) {
		setOccdate(what, false);
	}
	public void setOccdate(Object what, boolean rounded) {
		if (rounded)
			occdate.setRounded(what);
		else
			occdate.set(what);
	}	
	public PackedDecimalData getCcdate() {
		return ccdate;
	}
	public void setCcdate(Object what) {
		setCcdate(what, false);
	}
	public void setCcdate(Object what, boolean rounded) {
		if (rounded)
			ccdate.setRounded(what);
		else
			ccdate.set(what);
	}	
	public PackedDecimalData getCrdate() {
		return crdate;
	}
	public void setCrdate(Object what) {
		setCrdate(what, false);
	}
	public void setCrdate(Object what, boolean rounded) {
		if (rounded)
			crdate.setRounded(what);
		else
			crdate.set(what);
	}	
	public PackedDecimalData getAnnamt01() {
		return annamt01;
	}
	public void setAnnamt01(Object what) {
		setAnnamt01(what, false);
	}
	public void setAnnamt01(Object what, boolean rounded) {
		if (rounded)
			annamt01.setRounded(what);
		else
			annamt01.set(what);
	}	
	public PackedDecimalData getAnnamt02() {
		return annamt02;
	}
	public void setAnnamt02(Object what) {
		setAnnamt02(what, false);
	}
	public void setAnnamt02(Object what, boolean rounded) {
		if (rounded)
			annamt02.setRounded(what);
		else
			annamt02.set(what);
	}	
	public PackedDecimalData getAnnamt03() {
		return annamt03;
	}
	public void setAnnamt03(Object what) {
		setAnnamt03(what, false);
	}
	public void setAnnamt03(Object what, boolean rounded) {
		if (rounded)
			annamt03.setRounded(what);
		else
			annamt03.set(what);
	}	
	public PackedDecimalData getAnnamt04() {
		return annamt04;
	}
	public void setAnnamt04(Object what) {
		setAnnamt04(what, false);
	}
	public void setAnnamt04(Object what, boolean rounded) {
		if (rounded)
			annamt04.setRounded(what);
		else
			annamt04.set(what);
	}	
	public PackedDecimalData getAnnamt05() {
		return annamt05;
	}
	public void setAnnamt05(Object what) {
		setAnnamt05(what, false);
	}
	public void setAnnamt05(Object what, boolean rounded) {
		if (rounded)
			annamt05.setRounded(what);
		else
			annamt05.set(what);
	}	
	public PackedDecimalData getAnnamt06() {
		return annamt06;
	}
	public void setAnnamt06(Object what) {
		setAnnamt06(what, false);
	}
	public void setAnnamt06(Object what, boolean rounded) {
		if (rounded)
			annamt06.setRounded(what);
		else
			annamt06.set(what);
	}	
	public FixedLengthStringData getRnltype() {
		return rnltype;
	}
	public void setRnltype(Object what) {
		rnltype.set(what);
	}	
	public FixedLengthStringData getRnlnots() {
		return rnlnots;
	}
	public void setRnlnots(Object what) {
		rnlnots.set(what);
	}	
	public FixedLengthStringData getRnlnotto() {
		return rnlnotto;
	}
	public void setRnlnotto(Object what) {
		rnlnotto.set(what);
	}	
	public FixedLengthStringData getRnlattn() {
		return rnlattn;
	}
	public void setRnlattn(Object what) {
		rnlattn.set(what);
	}	
	public PackedDecimalData getRnldurn() {
		return rnldurn;
	}
	public void setRnldurn(Object what) {
		setRnldurn(what, false);
	}
	public void setRnldurn(Object what, boolean rounded) {
		if (rounded)
			rnldurn.setRounded(what);
		else
			rnldurn.set(what);
	}	
	public FixedLengthStringData getReptype() {
		return reptype;
	}
	public void setReptype(Object what) {
		reptype.set(what);
	}	
	public FixedLengthStringData getRepnum() {
		return repnum;
	}
	public void setRepnum(Object what) {
		repnum.set(what);
	}	
	public FixedLengthStringData getCownpfx() {
		return cownpfx;
	}
	public void setCownpfx(Object what) {
		cownpfx.set(what);
	}	
	public FixedLengthStringData getCowncoy() {
		return cowncoy;
	}
	public void setCowncoy(Object what) {
		cowncoy.set(what);
	}	
	public FixedLengthStringData getCownnum() {
		return cownnum;
	}
	public void setCownnum(Object what) {
		cownnum.set(what);
	}	
	public FixedLengthStringData getPayrpfx() {
		return payrpfx;
	}
	public void setPayrpfx(Object what) {
		payrpfx.set(what);
	}	
	public FixedLengthStringData getPayrcoy() {
		return payrcoy;
	}
	public void setPayrcoy(Object what) {
		payrcoy.set(what);
	}	
	public FixedLengthStringData getPayrnum() {
		return payrnum;
	}
	public void setPayrnum(Object what) {
		payrnum.set(what);
	}	
	public FixedLengthStringData getDesppfx() {
		return desppfx;
	}
	public void setDesppfx(Object what) {
		desppfx.set(what);
	}	
	public FixedLengthStringData getDespcoy() {
		return despcoy;
	}
	public void setDespcoy(Object what) {
		despcoy.set(what);
	}	
	public FixedLengthStringData getDespnum() {
		return despnum;
	}
	public void setDespnum(Object what) {
		despnum.set(what);
	}	
	public FixedLengthStringData getAsgnpfx() {
		return asgnpfx;
	}
	public void setAsgnpfx(Object what) {
		asgnpfx.set(what);
	}	
	public FixedLengthStringData getAsgncoy() {
		return asgncoy;
	}
	public void setAsgncoy(Object what) {
		asgncoy.set(what);
	}	
	public FixedLengthStringData getAsgnnum() {
		return asgnnum;
	}
	public void setAsgnnum(Object what) {
		asgnnum.set(what);
	}	
	public FixedLengthStringData getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(Object what) {
		cntbranch.set(what);
	}	
	public FixedLengthStringData getAgntpfx() {
		return agntpfx;
	}
	public void setAgntpfx(Object what) {
		agntpfx.set(what);
	}	
	public FixedLengthStringData getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(Object what) {
		agntcoy.set(what);
	}	
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public FixedLengthStringData getAcctccy() {
		return acctccy;
	}
	public void setAcctccy(Object what) {
		acctccy.set(what);
	}	
	public PackedDecimalData getCrate() {
		return crate;
	}
	public void setCrate(Object what) {
		setCrate(what, false);
	}
	public void setCrate(Object what, boolean rounded) {
		if (rounded)
			crate.setRounded(what);
		else
			crate.set(what);
	}	
	public FixedLengthStringData getPayplan() {
		return payplan;
	}
	public void setPayplan(Object what) {
		payplan.set(what);
	}	
	public FixedLengthStringData getAcctmeth() {
		return acctmeth;
	}
	public void setAcctmeth(Object what) {
		acctmeth.set(what);
	}	
	public FixedLengthStringData getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(Object what) {
		billfreq.set(what);
	}	
	public FixedLengthStringData getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(Object what) {
		billchnl.set(what);
	}	
	public FixedLengthStringData getCollchnl() {
		return collchnl;
	}
	public void setCollchnl(Object what) {
		collchnl.set(what);
	}	
	public FixedLengthStringData getMandref() {
		return mandref;
	}
	public void setMandref(Object what) {
		mandref.set(what);
	}	
	public FixedLengthStringData getBillday() {
		return billday;
	}
	public void setBillday(Object what) {
		billday.set(what);
	}	
	public FixedLengthStringData getBillmonth() {
		return billmonth;
	}
	public void setBillmonth(Object what) {
		billmonth.set(what);
	}	
	public PackedDecimalData getBillcd() {
		return billcd;
	}
	public void setBillcd(Object what) {
		setBillcd(what, false);
	}
	public void setBillcd(Object what, boolean rounded) {
		if (rounded)
			billcd.setRounded(what);
		else
			billcd.set(what);
	}	
	public PackedDecimalData getBtdate() {
		return btdate;
	}
	public void setBtdate(Object what) {
		setBtdate(what, false);
	}
	public void setBtdate(Object what, boolean rounded) {
		if (rounded)
			btdate.setRounded(what);
		else
			btdate.set(what);
	}	
	public PackedDecimalData getPtdate() {
		return ptdate;
	}
	public void setPtdate(Object what) {
		setPtdate(what, false);
	}
	public void setPtdate(Object what, boolean rounded) {
		if (rounded)
			ptdate.setRounded(what);
		else
			ptdate.set(what);
	}	
	public FixedLengthStringData getPayflag() {
		return payflag;
	}
	public void setPayflag(Object what) {
		payflag.set(what);
	}	
	public PackedDecimalData getSinstfrom() {
		return sinstfrom;
	}
	public void setSinstfrom(Object what) {
		setSinstfrom(what, false);
	}
	public void setSinstfrom(Object what, boolean rounded) {
		if (rounded)
			sinstfrom.setRounded(what);
		else
			sinstfrom.set(what);
	}	
	public PackedDecimalData getSinstto() {
		return sinstto;
	}
	public void setSinstto(Object what) {
		setSinstto(what, false);
	}
	public void setSinstto(Object what, boolean rounded) {
		if (rounded)
			sinstto.setRounded(what);
		else
			sinstto.set(what);
	}	
	public PackedDecimalData getSinstamt01() {
		return sinstamt01;
	}
	public void setSinstamt01(Object what) {
		setSinstamt01(what, false);
	}
	public void setSinstamt01(Object what, boolean rounded) {
		if (rounded)
			sinstamt01.setRounded(what);
		else
			sinstamt01.set(what);
	}	
	public PackedDecimalData getSinstamt02() {
		return sinstamt02;
	}
	public void setSinstamt02(Object what) {
		setSinstamt02(what, false);
	}
	public void setSinstamt02(Object what, boolean rounded) {
		if (rounded)
			sinstamt02.setRounded(what);
		else
			sinstamt02.set(what);
	}	
	public PackedDecimalData getSinstamt03() {
		return sinstamt03;
	}
	public void setSinstamt03(Object what) {
		setSinstamt03(what, false);
	}
	public void setSinstamt03(Object what, boolean rounded) {
		if (rounded)
			sinstamt03.setRounded(what);
		else
			sinstamt03.set(what);
	}	
	public PackedDecimalData getSinstamt04() {
		return sinstamt04;
	}
	public void setSinstamt04(Object what) {
		setSinstamt04(what, false);
	}
	public void setSinstamt04(Object what, boolean rounded) {
		if (rounded)
			sinstamt04.setRounded(what);
		else
			sinstamt04.set(what);
	}	
	public PackedDecimalData getSinstamt05() {
		return sinstamt05;
	}
	public void setSinstamt05(Object what) {
		setSinstamt05(what, false);
	}
	public void setSinstamt05(Object what, boolean rounded) {
		if (rounded)
			sinstamt05.setRounded(what);
		else
			sinstamt05.set(what);
	}	
	public PackedDecimalData getSinstamt06() {
		return sinstamt06;
	}
	public void setSinstamt06(Object what) {
		setSinstamt06(what, false);
	}
	public void setSinstamt06(Object what, boolean rounded) {
		if (rounded)
			sinstamt06.setRounded(what);
		else
			sinstamt06.set(what);
	}	
	public PackedDecimalData getInstfrom() {
		return instfrom;
	}
	public void setInstfrom(Object what) {
		setInstfrom(what, false);
	}
	public void setInstfrom(Object what, boolean rounded) {
		if (rounded)
			instfrom.setRounded(what);
		else
			instfrom.set(what);
	}	
	public PackedDecimalData getInstto() {
		return instto;
	}
	public void setInstto(Object what) {
		setInstto(what, false);
	}
	public void setInstto(Object what, boolean rounded) {
		if (rounded)
			instto.setRounded(what);
		else
			instto.set(what);
	}	
	public FixedLengthStringData getInstbchnl() {
		return instbchnl;
	}
	public void setInstbchnl(Object what) {
		instbchnl.set(what);
	}	
	public FixedLengthStringData getInstcchnl() {
		return instcchnl;
	}
	public void setInstcchnl(Object what) {
		instcchnl.set(what);
	}	
	public FixedLengthStringData getInstfreq() {
		return instfreq;
	}
	public void setInstfreq(Object what) {
		instfreq.set(what);
	}	
	public PackedDecimalData getInsttot01() {
		return insttot01;
	}
	public void setInsttot01(Object what) {
		setInsttot01(what, false);
	}
	public void setInsttot01(Object what, boolean rounded) {
		if (rounded)
			insttot01.setRounded(what);
		else
			insttot01.set(what);
	}	
	public PackedDecimalData getInsttot02() {
		return insttot02;
	}
	public void setInsttot02(Object what) {
		setInsttot02(what, false);
	}
	public void setInsttot02(Object what, boolean rounded) {
		if (rounded)
			insttot02.setRounded(what);
		else
			insttot02.set(what);
	}	
	public PackedDecimalData getInsttot03() {
		return insttot03;
	}
	public void setInsttot03(Object what) {
		setInsttot03(what, false);
	}
	public void setInsttot03(Object what, boolean rounded) {
		if (rounded)
			insttot03.setRounded(what);
		else
			insttot03.set(what);
	}	
	public PackedDecimalData getInsttot04() {
		return insttot04;
	}
	public void setInsttot04(Object what) {
		setInsttot04(what, false);
	}
	public void setInsttot04(Object what, boolean rounded) {
		if (rounded)
			insttot04.setRounded(what);
		else
			insttot04.set(what);
	}	
	public PackedDecimalData getInsttot05() {
		return insttot05;
	}
	public void setInsttot05(Object what) {
		setInsttot05(what, false);
	}
	public void setInsttot05(Object what, boolean rounded) {
		if (rounded)
			insttot05.setRounded(what);
		else
			insttot05.set(what);
	}	
	public PackedDecimalData getInsttot06() {
		return insttot06;
	}
	public void setInsttot06(Object what) {
		setInsttot06(what, false);
	}
	public void setInsttot06(Object what, boolean rounded) {
		if (rounded)
			insttot06.setRounded(what);
		else
			insttot06.set(what);
	}	
	public PackedDecimalData getInststamt01() {
		return inststamt01;
	}
	public void setInststamt01(Object what) {
		setInststamt01(what, false);
	}
	public void setInststamt01(Object what, boolean rounded) {
		if (rounded)
			inststamt01.setRounded(what);
		else
			inststamt01.set(what);
	}	
	public PackedDecimalData getInststamt02() {
		return inststamt02;
	}
	public void setInststamt02(Object what) {
		setInststamt02(what, false);
	}
	public void setInststamt02(Object what, boolean rounded) {
		if (rounded)
			inststamt02.setRounded(what);
		else
			inststamt02.set(what);
	}	
	public PackedDecimalData getInststamt03() {
		return inststamt03;
	}
	public void setInststamt03(Object what) {
		setInststamt03(what, false);
	}
	public void setInststamt03(Object what, boolean rounded) {
		if (rounded)
			inststamt03.setRounded(what);
		else
			inststamt03.set(what);
	}	
	public PackedDecimalData getInststamt04() {
		return inststamt04;
	}
	public void setInststamt04(Object what) {
		setInststamt04(what, false);
	}
	public void setInststamt04(Object what, boolean rounded) {
		if (rounded)
			inststamt04.setRounded(what);
		else
			inststamt04.set(what);
	}	
	public PackedDecimalData getInststamt05() {
		return inststamt05;
	}
	public void setInststamt05(Object what) {
		setInststamt05(what, false);
	}
	public void setInststamt05(Object what, boolean rounded) {
		if (rounded)
			inststamt05.setRounded(what);
		else
			inststamt05.set(what);
	}	
	public PackedDecimalData getInststamt06() {
		return inststamt06;
	}
	public void setInststamt06(Object what) {
		setInststamt06(what, false);
	}
	public void setInststamt06(Object what, boolean rounded) {
		if (rounded)
			inststamt06.setRounded(what);
		else
			inststamt06.set(what);
	}	
	public PackedDecimalData getInstpast01() {
		return instpast01;
	}
	public void setInstpast01(Object what) {
		setInstpast01(what, false);
	}
	public void setInstpast01(Object what, boolean rounded) {
		if (rounded)
			instpast01.setRounded(what);
		else
			instpast01.set(what);
	}	
	public PackedDecimalData getInstpast02() {
		return instpast02;
	}
	public void setInstpast02(Object what) {
		setInstpast02(what, false);
	}
	public void setInstpast02(Object what, boolean rounded) {
		if (rounded)
			instpast02.setRounded(what);
		else
			instpast02.set(what);
	}	
	public PackedDecimalData getInstpast03() {
		return instpast03;
	}
	public void setInstpast03(Object what) {
		setInstpast03(what, false);
	}
	public void setInstpast03(Object what, boolean rounded) {
		if (rounded)
			instpast03.setRounded(what);
		else
			instpast03.set(what);
	}	
	public PackedDecimalData getInstpast04() {
		return instpast04;
	}
	public void setInstpast04(Object what) {
		setInstpast04(what, false);
	}
	public void setInstpast04(Object what, boolean rounded) {
		if (rounded)
			instpast04.setRounded(what);
		else
			instpast04.set(what);
	}	
	public PackedDecimalData getInstpast05() {
		return instpast05;
	}
	public void setInstpast05(Object what) {
		setInstpast05(what, false);
	}
	public void setInstpast05(Object what, boolean rounded) {
		if (rounded)
			instpast05.setRounded(what);
		else
			instpast05.set(what);
	}	
	public PackedDecimalData getInstpast06() {
		return instpast06;
	}
	public void setInstpast06(Object what) {
		setInstpast06(what, false);
	}
	public void setInstpast06(Object what, boolean rounded) {
		if (rounded)
			instpast06.setRounded(what);
		else
			instpast06.set(what);
	}	
	public FixedLengthStringData getInstjctl() {
		return instjctl;
	}
	public void setInstjctl(Object what) {
		instjctl.set(what);
	}	
	public PackedDecimalData getNofoutinst() {
		return nofoutinst;
	}
	public void setNofoutinst(Object what) {
		setNofoutinst(what, false);
	}
	public void setNofoutinst(Object what, boolean rounded) {
		if (rounded)
			nofoutinst.setRounded(what);
		else
			nofoutinst.set(what);
	}	
	public PackedDecimalData getOutstamt() {
		return outstamt;
	}
	public void setOutstamt(Object what) {
		setOutstamt(what, false);
	}
	public void setOutstamt(Object what, boolean rounded) {
		if (rounded)
			outstamt.setRounded(what);
		else
			outstamt.set(what);
	}	
	public PackedDecimalData getBilldate01() {
		return billdate01;
	}
	public void setBilldate01(Object what) {
		setBilldate01(what, false);
	}
	public void setBilldate01(Object what, boolean rounded) {
		if (rounded)
			billdate01.setRounded(what);
		else
			billdate01.set(what);
	}	
	public PackedDecimalData getBilldate02() {
		return billdate02;
	}
	public void setBilldate02(Object what) {
		setBilldate02(what, false);
	}
	public void setBilldate02(Object what, boolean rounded) {
		if (rounded)
			billdate02.setRounded(what);
		else
			billdate02.set(what);
	}	
	public PackedDecimalData getBilldate03() {
		return billdate03;
	}
	public void setBilldate03(Object what) {
		setBilldate03(what, false);
	}
	public void setBilldate03(Object what, boolean rounded) {
		if (rounded)
			billdate03.setRounded(what);
		else
			billdate03.set(what);
	}	
	public PackedDecimalData getBilldate04() {
		return billdate04;
	}
	public void setBilldate04(Object what) {
		setBilldate04(what, false);
	}
	public void setBilldate04(Object what, boolean rounded) {
		if (rounded)
			billdate04.setRounded(what);
		else
			billdate04.set(what);
	}	
	public PackedDecimalData getBillamt01() {
		return billamt01;
	}
	public void setBillamt01(Object what) {
		setBillamt01(what, false);
	}
	public void setBillamt01(Object what, boolean rounded) {
		if (rounded)
			billamt01.setRounded(what);
		else
			billamt01.set(what);
	}	
	public PackedDecimalData getBillamt02() {
		return billamt02;
	}
	public void setBillamt02(Object what) {
		setBillamt02(what, false);
	}
	public void setBillamt02(Object what, boolean rounded) {
		if (rounded)
			billamt02.setRounded(what);
		else
			billamt02.set(what);
	}	
	public PackedDecimalData getBillamt03() {
		return billamt03;
	}
	public void setBillamt03(Object what) {
		setBillamt03(what, false);
	}
	public void setBillamt03(Object what, boolean rounded) {
		if (rounded)
			billamt03.setRounded(what);
		else
			billamt03.set(what);
	}	
	public PackedDecimalData getBillamt04() {
		return billamt04;
	}
	public void setBillamt04(Object what) {
		setBillamt04(what, false);
	}
	public void setBillamt04(Object what, boolean rounded) {
		if (rounded)
			billamt04.setRounded(what);
		else
			billamt04.set(what);
	}	
	public FixedLengthStringData getFacthous() {
		return facthous;
	}
	public void setFacthous(Object what) {
		facthous.set(what);
	}	
	public FixedLengthStringData getBankkey() {
		return bankkey;
	}
	public void setBankkey(Object what) {
		bankkey.set(what);
	}	
	public FixedLengthStringData getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(Object what) {
		bankacckey.set(what);
	}	
	public FixedLengthStringData getDiscode01() {
		return discode01;
	}
	public void setDiscode01(Object what) {
		discode01.set(what);
	}	
	public FixedLengthStringData getDiscode02() {
		return discode02;
	}
	public void setDiscode02(Object what) {
		discode02.set(what);
	}	
	public FixedLengthStringData getDiscode03() {
		return discode03;
	}
	public void setDiscode03(Object what) {
		discode03.set(what);
	}	
	public FixedLengthStringData getDiscode04() {
		return discode04;
	}
	public void setDiscode04(Object what) {
		discode04.set(what);
	}	
	public FixedLengthStringData getGrupkey() {
		return grupkey;
	}
	public void setGrupkey(Object what) {
		grupkey.set(what);
	}	
	public FixedLengthStringData getMembsel() {
		return membsel;
	}
	public void setMembsel(Object what) {
		membsel.set(what);
	}	
	public FixedLengthStringData getAplsupr() {
		return aplsupr;
	}
	public void setAplsupr(Object what) {
		aplsupr.set(what);
	}	
	public FixedLengthStringData getBillsupr() {
		return billsupr;
	}
	public void setBillsupr(Object what) {
		billsupr.set(what);
	}	
	public FixedLengthStringData getCommsupr() {
		return commsupr;
	}
	public void setCommsupr(Object what) {
		commsupr.set(what);
	}	
	public FixedLengthStringData getLapssupr() {
		return lapssupr;
	}
	public void setLapssupr(Object what) {
		lapssupr.set(what);
	}	
	public FixedLengthStringData getMailsupr() {
		return mailsupr;
	}
	public void setMailsupr(Object what) {
		mailsupr.set(what);
	}	
	public FixedLengthStringData getNotssupr() {
		return notssupr;
	}
	public void setNotssupr(Object what) {
		notssupr.set(what);
	}	
	public FixedLengthStringData getRnwlsupr() {
		return rnwlsupr;
	}
	public void setRnwlsupr(Object what) {
		rnwlsupr.set(what);
	}	
	public PackedDecimalData getAplspfrom() {
		return aplspfrom;
	}
	public void setAplspfrom(Object what) {
		setAplspfrom(what, false);
	}
	public void setAplspfrom(Object what, boolean rounded) {
		if (rounded)
			aplspfrom.setRounded(what);
		else
			aplspfrom.set(what);
	}	
	public PackedDecimalData getBillspfrom() {
		return billspfrom;
	}
	public void setBillspfrom(Object what) {
		setBillspfrom(what, false);
	}
	public void setBillspfrom(Object what, boolean rounded) {
		if (rounded)
			billspfrom.setRounded(what);
		else
			billspfrom.set(what);
	}	
	public PackedDecimalData getCommspfrom() {
		return commspfrom;
	}
	public void setCommspfrom(Object what) {
		setCommspfrom(what, false);
	}
	public void setCommspfrom(Object what, boolean rounded) {
		if (rounded)
			commspfrom.setRounded(what);
		else
			commspfrom.set(what);
	}	
	public PackedDecimalData getLapsspfrom() {
		return lapsspfrom;
	}
	public void setLapsspfrom(Object what) {
		setLapsspfrom(what, false);
	}
	public void setLapsspfrom(Object what, boolean rounded) {
		if (rounded)
			lapsspfrom.setRounded(what);
		else
			lapsspfrom.set(what);
	}	
	public PackedDecimalData getMailspfrom() {
		return mailspfrom;
	}
	public void setMailspfrom(Object what) {
		setMailspfrom(what, false);
	}
	public void setMailspfrom(Object what, boolean rounded) {
		if (rounded)
			mailspfrom.setRounded(what);
		else
			mailspfrom.set(what);
	}	
	public PackedDecimalData getNotsspfrom() {
		return notsspfrom;
	}
	public void setNotsspfrom(Object what) {
		setNotsspfrom(what, false);
	}
	public void setNotsspfrom(Object what, boolean rounded) {
		if (rounded)
			notsspfrom.setRounded(what);
		else
			notsspfrom.set(what);
	}	
	public PackedDecimalData getRnwlspfrom() {
		return rnwlspfrom;
	}
	public void setRnwlspfrom(Object what) {
		setRnwlspfrom(what, false);
	}
	public void setRnwlspfrom(Object what, boolean rounded) {
		if (rounded)
			rnwlspfrom.setRounded(what);
		else
			rnwlspfrom.set(what);
	}	
	public PackedDecimalData getAplspto() {
		return aplspto;
	}
	public void setAplspto(Object what) {
		setAplspto(what, false);
	}
	public void setAplspto(Object what, boolean rounded) {
		if (rounded)
			aplspto.setRounded(what);
		else
			aplspto.set(what);
	}	
	public PackedDecimalData getBillspto() {
		return billspto;
	}
	public void setBillspto(Object what) {
		setBillspto(what, false);
	}
	public void setBillspto(Object what, boolean rounded) {
		if (rounded)
			billspto.setRounded(what);
		else
			billspto.set(what);
	}	
	public PackedDecimalData getCommspto() {
		return commspto;
	}
	public void setCommspto(Object what) {
		setCommspto(what, false);
	}
	public void setCommspto(Object what, boolean rounded) {
		if (rounded)
			commspto.setRounded(what);
		else
			commspto.set(what);
	}	
	public PackedDecimalData getLapsspto() {
		return lapsspto;
	}
	public void setLapsspto(Object what) {
		setLapsspto(what, false);
	}
	public void setLapsspto(Object what, boolean rounded) {
		if (rounded)
			lapsspto.setRounded(what);
		else
			lapsspto.set(what);
	}	
	public PackedDecimalData getMailspto() {
		return mailspto;
	}
	public void setMailspto(Object what) {
		setMailspto(what, false);
	}
	public void setMailspto(Object what, boolean rounded) {
		if (rounded)
			mailspto.setRounded(what);
		else
			mailspto.set(what);
	}	
	public PackedDecimalData getNotsspto() {
		return notsspto;
	}
	public void setNotsspto(Object what) {
		setNotsspto(what, false);
	}
	public void setNotsspto(Object what, boolean rounded) {
		if (rounded)
			notsspto.setRounded(what);
		else
			notsspto.set(what);
	}	
	public PackedDecimalData getRnwlspto() {
		return rnwlspto;
	}
	public void setRnwlspto(Object what) {
		setRnwlspto(what, false);
	}
	public void setRnwlspto(Object what, boolean rounded) {
		if (rounded)
			rnwlspto.setRounded(what);
		else
			rnwlspto.set(what);
	}	
	public FixedLengthStringData getCampaign() {
		return campaign;
	}
	public void setCampaign(Object what) {
		campaign.set(what);
	}	
	public FixedLengthStringData getSrcebus() {
		return srcebus;
	}
	public void setSrcebus(Object what) {
		srcebus.set(what);
	}	
	public FixedLengthStringData getChdrstcda() {
		return chdrstcda;
	}
	public void setChdrstcda(Object what) {
		chdrstcda.set(what);
	}	
	public FixedLengthStringData getChdrstcdb() {
		return chdrstcdb;
	}
	public void setChdrstcdb(Object what) {
		chdrstcdb.set(what);
	}	
	public FixedLengthStringData getChdrstcdc() {
		return chdrstcdc;
	}
	public void setChdrstcdc(Object what) {
		chdrstcdc.set(what);
	}	
	public FixedLengthStringData getChdrstcdd() {
		return chdrstcdd;
	}
	public void setChdrstcdd(Object what) {
		chdrstcdd.set(what);
	}	
	public FixedLengthStringData getChdrstcde() {
		return chdrstcde;
	}
	public void setChdrstcde(Object what) {
		chdrstcde.set(what);
	}	
	public PackedDecimalData getNofrisks() {
		return nofrisks;
	}
	public void setNofrisks(Object what) {
		setNofrisks(what, false);
	}
	public void setNofrisks(Object what, boolean rounded) {
		if (rounded)
			nofrisks.setRounded(what);
		else
			nofrisks.set(what);
	}	
	public FixedLengthStringData getJacket() {
		return jacket;
	}
	public void setJacket(Object what) {
		jacket.set(what);
	}	
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}	
	public FixedLengthStringData getPstatreasn() {
		return pstatreasn;
	}
	public void setPstatreasn(Object what) {
		pstatreasn.set(what);
	}	
	public PackedDecimalData getPstatdate() {
		return pstatdate;
	}
	public void setPstatdate(Object what) {
		setPstatdate(what, false);
	}
	public void setPstatdate(Object what, boolean rounded) {
		if (rounded)
			pstatdate.setRounded(what);
		else
			pstatdate.set(what);
	}	
	public PackedDecimalData getPstattran() {
		return pstattran;
	}
	public void setPstattran(Object what) {
		setPstattran(what, false);
	}
	public void setPstattran(Object what, boolean rounded) {
		if (rounded)
			pstattran.setRounded(what);
		else
			pstattran.set(what);
	}	
	public FixedLengthStringData getPdind() {
		return pdind;
	}
	public void setPdind(Object what) {
		pdind.set(what);
	}	
	public FixedLengthStringData getRegister() {
		return register;
	}
	public void setRegister(Object what) {
		register.set(what);
	}	
	public FixedLengthStringData getMplpfx() {
		return mplpfx;
	}
	public void setMplpfx(Object what) {
		mplpfx.set(what);
	}	
	public FixedLengthStringData getMplcoy() {
		return mplcoy;
	}
	public void setMplcoy(Object what) {
		mplcoy.set(what);
	}	
	public FixedLengthStringData getMplnum() {
		return mplnum;
	}
	public void setMplnum(Object what) {
		mplnum.set(what);
	}	
	public FixedLengthStringData getClupfx() {
		return clupfx;
	}
	public void setClupfx(Object what) {
		clupfx.set(what);
	}	
	public FixedLengthStringData getClucoy() {
		return clucoy;
	}
	public void setClucoy(Object what) {
		clucoy.set(what);
	}	
	public FixedLengthStringData getClunum() {
		return clunum;
	}
	public void setClunum(Object what) {
		clunum.set(what);
	}	
	public FixedLengthStringData getFinpfx() {
		return finpfx;
	}
	public void setFinpfx(Object what) {
		finpfx.set(what);
	}	
	public FixedLengthStringData getFincoy() {
		return fincoy;
	}
	public void setFincoy(Object what) {
		fincoy.set(what);
	}	
	public FixedLengthStringData getFinnum() {
		return finnum;
	}
	public void setFinnum(Object what) {
		finnum.set(what);
	}	
	public FixedLengthStringData getPoapfx() {
		return poapfx;
	}
	public void setPoapfx(Object what) {
		poapfx.set(what);
	}	
	public FixedLengthStringData getPoacoy() {
		return poacoy;
	}
	public void setPoacoy(Object what) {
		poacoy.set(what);
	}	
	public FixedLengthStringData getPoanum() {
		return poanum;
	}
	public void setPoanum(Object what) {
		poanum.set(what);
	}	
	public FixedLengthStringData getDueflg() {
		return dueflg;
	}
	public void setDueflg(Object what) {
		dueflg.set(what);
	}	
	public FixedLengthStringData getLaprind() {
		return laprind;
	}
	public void setLaprind(Object what) {
		laprind.set(what);
	}	
	public FixedLengthStringData getSpecind() {
		return specind;
	}
	public void setSpecind(Object what) {
		specind.set(what);
	}	
	public FixedLengthStringData getChgflag() {
		return chgflag;
	}
	public void setChgflag(Object what) {
		chgflag.set(what);
	}	
	public FixedLengthStringData getPolpln() {
		return polpln;
	}
	public void setPolpln(Object what) {
		polpln.set(what);
	}	
	public PackedDecimalData getWvfdat() {
		return wvfdat;
	}
	public void setWvfdat(Object what) {
		setWvfdat(what, false);
	}
	public void setWvfdat(Object what, boolean rounded) {
		if (rounded)
			wvfdat.setRounded(what);
		else
			wvfdat.set(what);
	}	
	public PackedDecimalData getWvtdat() {
		return wvtdat;
	}
	public void setWvtdat(Object what) {
		setWvtdat(what, false);
	}
	public void setWvtdat(Object what, boolean rounded) {
		if (rounded)
			wvtdat.setRounded(what);
		else
			wvtdat.set(what);
	}	
	public FixedLengthStringData getWvfind() {
		return wvfind;
	}
	public void setWvfind(Object what) {
		wvfind.set(what);
	}	
	public FixedLengthStringData getBfcharge() {
		return bfcharge;
	}
	public void setBfcharge(Object what) {
		bfcharge.set(what);
	}	
	public PackedDecimalData getDishnrcnt() {
		return dishnrcnt;
	}
	public void setDishnrcnt(Object what) {
		setDishnrcnt(what, false);
	}
	public void setDishnrcnt(Object what, boolean rounded) {
		if (rounded)
			dishnrcnt.setRounded(what);
		else
			dishnrcnt.set(what);
	}	
	public FixedLengthStringData getPdtype() {
		return pdtype;
	}
	public void setPdtype(Object what) {
		pdtype.set(what);
	}	
	public PackedDecimalData getDishnrdte() {
		return dishnrdte;
	}
	public void setDishnrdte(Object what) {
		setDishnrdte(what, false);
	}
	public void setDishnrdte(Object what, boolean rounded) {
		if (rounded)
			dishnrdte.setRounded(what);
		else
			dishnrdte.set(what);
	}	
	public PackedDecimalData getStmpdtyamt() {
		return stmpdtyamt;
	}
	public void setStmpdtyamt(Object what) {
		setStmpdtyamt(what, false);
	}
	public void setStmpdtyamt(Object what, boolean rounded) {
		if (rounded)
			stmpdtyamt.setRounded(what);
		else
			stmpdtyamt.set(what);
	}	
	public PackedDecimalData getStmpdtydte() {
		return stmpdtydte;
	}
	public void setStmpdtydte(Object what) {
		setStmpdtydte(what, false);
	}
	public void setStmpdtydte(Object what, boolean rounded) {
		if (rounded)
			stmpdtydte.setRounded(what);
		else
			stmpdtydte.set(what);
	}	
	public FixedLengthStringData getCovernt() {
		return covernt;
	}
	public void setCovernt(Object what) {
		covernt.set(what);
	}	
	public PackedDecimalData getCntiss() {
		return cntiss;
	}
	public void setCntiss(Object what) {
		setCntiss(what, false);
	}
	public void setCntiss(Object what, boolean rounded) {
		if (rounded)
			cntiss.setRounded(what);
		else
			cntiss.set(what);
	}	
	public PackedDecimalData getCntrcv() {
		return cntrcv;
	}
	public void setCntrcv(Object what) {
		setCntrcv(what, false);
	}
	public void setCntrcv(Object what, boolean rounded) {
		if (rounded)
			cntrcv.setRounded(what);
		else
			cntrcv.set(what);
	}	
	public FixedLengthStringData getQuoteno() {
		return quoteno;
	}
	public void setQuoteno(Object what) {
		quoteno.set(what);
	}	
	public FixedLengthStringData getBankcode() {
		return bankcode;
	}
	public void setBankcode(Object what) {
		bankcode.set(what);
	}	
	public FixedLengthStringData getDocnum() {
		return docnum;
	}
	public void setDocnum(Object what) {
		docnum.set(what);
	}	
	public FixedLengthStringData getRnlsts() {
		return rnlsts;
	}
	public void setRnlsts(Object what) {
		rnlsts.set(what);
	}	
	public FixedLengthStringData getSustrcde() {
		return sustrcde;
	}
	public void setSustrcde(Object what) {
		sustrcde.set(what);
	}	
	public PackedDecimalData getDtecan() {
		return dtecan;
	}
	public void setDtecan(Object what) {
		setDtecan(what, false);
	}
	public void setDtecan(Object what, boolean rounded) {
		if (rounded)
			dtecan.setRounded(what);
		else
			dtecan.set(what);
	}	
	public FixedLengthStringData getCotype() {
		return cotype;
	}
	public void setCotype(Object what) {
		cotype.set(what);
	}	
	public PackedDecimalData getCoppn() {
		return coppn;
	}
	public void setCoppn(Object what) {
		setCoppn(what, false);
	}
	public void setCoppn(Object what, boolean rounded) {
		if (rounded)
			coppn.setRounded(what);
		else
			coppn.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	//start ILIFE-2472
	public void setZmandref(Object what){
	  zmandref.set(what);
	}
	
	public FixedLengthStringData getZmandref(){
		return zmandref;
	}
	
	public FixedLengthStringData getReqntype() {
		return reqntype;
	}
	
	public void setReqntype(Object what) {
		reqntype.set(what);
	}
	//END ILIFE-2472
	//ILIFE-2472 PH2
	public FixedLengthStringData getPayclt() {
		return payclt;
	}
	//ILIFE-2472 PH2	
	public void setPayclt(Object what) {
		payclt.set(what);
		}
	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getSuprtos() {
		return new FixedLengthStringData(aplspto.toInternal()
										+ billspto.toInternal()
										+ commspto.toInternal()
										+ lapsspto.toInternal()
										+ mailspto.toInternal()
										+ notsspto.toInternal()
										+ rnwlspto.toInternal());
	}
	public void setSuprtos(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSuprtos().getLength()).init(obj);
	
		what = ExternalData.chop(what, aplspto);
		what = ExternalData.chop(what, billspto);
		what = ExternalData.chop(what, commspto);
		what = ExternalData.chop(what, lapsspto);
		what = ExternalData.chop(what, mailspto);
		what = ExternalData.chop(what, notsspto);
		what = ExternalData.chop(what, rnwlspto);
	}
	public PackedDecimalData getSuprto(BaseData indx) {
		return getSuprto(indx.toInt());
	}
	public PackedDecimalData getSuprto(int indx) {

		switch (indx) {
			case 1 : return aplspto;
			case 2 : return billspto;
			case 3 : return commspto;
			case 4 : return lapsspto;
			case 5 : return mailspto;
			case 6 : return notsspto;
			case 7 : return rnwlspto;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSuprto(BaseData indx, Object what) {
		setSuprto(indx, what, false);
	}
	public void setSuprto(BaseData indx, Object what, boolean rounded) {
		setSuprto(indx.toInt(), what, rounded);
	}
	public void setSuprto(int indx, Object what) {
		setSuprto(indx, what, false);
	}
	public void setSuprto(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setAplspto(what, rounded);
					 break;
			case 2 : setBillspto(what, rounded);
					 break;
			case 3 : setCommspto(what, rounded);
					 break;
			case 4 : setLapsspto(what, rounded);
					 break;
			case 5 : setMailspto(what, rounded);
					 break;
			case 6 : setNotsspto(what, rounded);
					 break;
			case 7 : setRnwlspto(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getSuprfroms() {
		return new FixedLengthStringData(aplspfrom.toInternal()
										+ billspfrom.toInternal()
										+ commspfrom.toInternal()
										+ lapsspfrom.toInternal()
										+ mailspfrom.toInternal()
										+ notsspfrom.toInternal()
										+ rnwlspfrom.toInternal());
	}
	public void setSuprfroms(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSuprfroms().getLength()).init(obj);
	
		what = ExternalData.chop(what, aplspfrom);
		what = ExternalData.chop(what, billspfrom);
		what = ExternalData.chop(what, commspfrom);
		what = ExternalData.chop(what, lapsspfrom);
		what = ExternalData.chop(what, mailspfrom);
		what = ExternalData.chop(what, notsspfrom);
		what = ExternalData.chop(what, rnwlspfrom);
	}
	public PackedDecimalData getSuprfrom(BaseData indx) {
		return getSuprfrom(indx.toInt());
	}
	public PackedDecimalData getSuprfrom(int indx) {

		switch (indx) {
			case 1 : return aplspfrom;
			case 2 : return billspfrom;
			case 3 : return commspfrom;
			case 4 : return lapsspfrom;
			case 5 : return mailspfrom;
			case 6 : return notsspfrom;
			case 7 : return rnwlspfrom;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSuprfrom(BaseData indx, Object what) {
		setSuprfrom(indx, what, false);
	}
	public void setSuprfrom(BaseData indx, Object what, boolean rounded) {
		setSuprfrom(indx.toInt(), what, rounded);
	}
	public void setSuprfrom(int indx, Object what) {
		setSuprfrom(indx, what, false);
	}
	public void setSuprfrom(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setAplspfrom(what, rounded);
					 break;
			case 2 : setBillspfrom(what, rounded);
					 break;
			case 3 : setCommspfrom(what, rounded);
					 break;
			case 4 : setLapsspfrom(what, rounded);
					 break;
			case 5 : setMailspfrom(what, rounded);
					 break;
			case 6 : setNotsspfrom(what, rounded);
					 break;
			case 7 : setRnwlspfrom(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getSuprflags() {
		return new FixedLengthStringData(aplsupr.toInternal()
										+ billsupr.toInternal()
										+ commsupr.toInternal()
										+ lapssupr.toInternal()
										+ mailsupr.toInternal()
										+ notssupr.toInternal()
										+ rnwlsupr.toInternal());
	}
	public void setSuprflags(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSuprflags().getLength()).init(obj);
	
		what = ExternalData.chop(what, aplsupr);
		what = ExternalData.chop(what, billsupr);
		what = ExternalData.chop(what, commsupr);
		what = ExternalData.chop(what, lapssupr);
		what = ExternalData.chop(what, mailsupr);
		what = ExternalData.chop(what, notssupr);
		what = ExternalData.chop(what, rnwlsupr);
	}
	public FixedLengthStringData getSuprflag(BaseData indx) {
		return getSuprflag(indx.toInt());
	}
	public FixedLengthStringData getSuprflag(int indx) {

		switch (indx) {
			case 1 : return aplsupr;
			case 2 : return billsupr;
			case 3 : return commsupr;
			case 4 : return lapssupr;
			case 5 : return mailsupr;
			case 6 : return notssupr;
			case 7 : return rnwlsupr;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSuprflag(BaseData indx, Object what) {
		setSuprflag(indx.toInt(), what);
	}
	public void setSuprflag(int indx, Object what) {

		switch (indx) {
			case 1 : setAplsupr(what);
					 break;
			case 2 : setBillsupr(what);
					 break;
			case 3 : setCommsupr(what);
					 break;
			case 4 : setLapssupr(what);
					 break;
			case 5 : setMailsupr(what);
					 break;
			case 6 : setNotssupr(what);
					 break;
			case 7 : setRnwlsupr(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getSinstamts() {
		return new FixedLengthStringData(sinstamt01.toInternal()
										+ sinstamt02.toInternal()
										+ sinstamt03.toInternal()
										+ sinstamt04.toInternal()
										+ sinstamt05.toInternal()
										+ sinstamt06.toInternal());
	}
	public void setSinstamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSinstamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, sinstamt01);
		what = ExternalData.chop(what, sinstamt02);
		what = ExternalData.chop(what, sinstamt03);
		what = ExternalData.chop(what, sinstamt04);
		what = ExternalData.chop(what, sinstamt05);
		what = ExternalData.chop(what, sinstamt06);
	}
	public PackedDecimalData getSinstamt(BaseData indx) {
		return getSinstamt(indx.toInt());
	}
	public PackedDecimalData getSinstamt(int indx) {

		switch (indx) {
			case 1 : return sinstamt01;
			case 2 : return sinstamt02;
			case 3 : return sinstamt03;
			case 4 : return sinstamt04;
			case 5 : return sinstamt05;
			case 6 : return sinstamt06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSinstamt(BaseData indx, Object what) {
		setSinstamt(indx, what, false);
	}
	public void setSinstamt(BaseData indx, Object what, boolean rounded) {
		setSinstamt(indx.toInt(), what, rounded);
	}
	public void setSinstamt(int indx, Object what) {
		setSinstamt(indx, what, false);
	}
	public void setSinstamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setSinstamt01(what, rounded);
					 break;
			case 2 : setSinstamt02(what, rounded);
					 break;
			case 3 : setSinstamt03(what, rounded);
					 break;
			case 4 : setSinstamt04(what, rounded);
					 break;
			case 5 : setSinstamt05(what, rounded);
					 break;
			case 6 : setSinstamt06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getIsams() {
		return new FixedLengthStringData(inststamt01.toInternal()
										+ inststamt02.toInternal()
										+ inststamt03.toInternal()
										+ inststamt04.toInternal()
										+ inststamt05.toInternal()
										+ inststamt06.toInternal());
	}
	public void setIsams(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getIsams().getLength()).init(obj);
	
		what = ExternalData.chop(what, inststamt01);
		what = ExternalData.chop(what, inststamt02);
		what = ExternalData.chop(what, inststamt03);
		what = ExternalData.chop(what, inststamt04);
		what = ExternalData.chop(what, inststamt05);
		what = ExternalData.chop(what, inststamt06);
	}
	public PackedDecimalData getIsam(BaseData indx) {
		return getIsam(indx.toInt());
	}
	public PackedDecimalData getIsam(int indx) {

		switch (indx) {
			case 1 : return inststamt01;
			case 2 : return inststamt02;
			case 3 : return inststamt03;
			case 4 : return inststamt04;
			case 5 : return inststamt05;
			case 6 : return inststamt06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setIsam(BaseData indx, Object what) {
		setIsam(indx, what, false);
	}
	public void setIsam(BaseData indx, Object what, boolean rounded) {
		setIsam(indx.toInt(), what, rounded);
	}
	public void setIsam(int indx, Object what) {
		setIsam(indx, what, false);
	}
	public void setIsam(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInststamt01(what, rounded);
					 break;
			case 2 : setInststamt02(what, rounded);
					 break;
			case 3 : setInststamt03(what, rounded);
					 break;
			case 4 : setInststamt04(what, rounded);
					 break;
			case 5 : setInststamt05(what, rounded);
					 break;
			case 6 : setInststamt06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getInsttots() {
		return new FixedLengthStringData(insttot01.toInternal()
										+ insttot02.toInternal()
										+ insttot03.toInternal()
										+ insttot04.toInternal()
										+ insttot05.toInternal()
										+ insttot06.toInternal());
	}
	public void setInsttots(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getInsttots().getLength()).init(obj);
	
		what = ExternalData.chop(what, insttot01);
		what = ExternalData.chop(what, insttot02);
		what = ExternalData.chop(what, insttot03);
		what = ExternalData.chop(what, insttot04);
		what = ExternalData.chop(what, insttot05);
		what = ExternalData.chop(what, insttot06);
	}
	public PackedDecimalData getInsttot(BaseData indx) {
		return getInsttot(indx.toInt());
	}
	public PackedDecimalData getInsttot(int indx) {

		switch (indx) {
			case 1 : return insttot01;
			case 2 : return insttot02;
			case 3 : return insttot03;
			case 4 : return insttot04;
			case 5 : return insttot05;
			case 6 : return insttot06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setInsttot(BaseData indx, Object what) {
		setInsttot(indx, what, false);
	}
	public void setInsttot(BaseData indx, Object what, boolean rounded) {
		setInsttot(indx.toInt(), what, rounded);
	}
	public void setInsttot(int indx, Object what) {
		setInsttot(indx, what, false);
	}
	public void setInsttot(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInsttot01(what, rounded);
					 break;
			case 2 : setInsttot02(what, rounded);
					 break;
			case 3 : setInsttot03(what, rounded);
					 break;
			case 4 : setInsttot04(what, rounded);
					 break;
			case 5 : setInsttot05(what, rounded);
					 break;
			case 6 : setInsttot06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getInstpasts() {
		return new FixedLengthStringData(instpast01.toInternal()
										+ instpast02.toInternal()
										+ instpast03.toInternal()
										+ instpast04.toInternal()
										+ instpast05.toInternal()
										+ instpast06.toInternal());
	}
	public void setInstpasts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getInstpasts().getLength()).init(obj);
	
		what = ExternalData.chop(what, instpast01);
		what = ExternalData.chop(what, instpast02);
		what = ExternalData.chop(what, instpast03);
		what = ExternalData.chop(what, instpast04);
		what = ExternalData.chop(what, instpast05);
		what = ExternalData.chop(what, instpast06);
	}
	public PackedDecimalData getInstpast(BaseData indx) {
		return getInstpast(indx.toInt());
	}
	public PackedDecimalData getInstpast(int indx) {

		switch (indx) {
			case 1 : return instpast01;
			case 2 : return instpast02;
			case 3 : return instpast03;
			case 4 : return instpast04;
			case 5 : return instpast05;
			case 6 : return instpast06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setInstpast(BaseData indx, Object what) {
		setInstpast(indx, what, false);
	}
	public void setInstpast(BaseData indx, Object what, boolean rounded) {
		setInstpast(indx.toInt(), what, rounded);
	}
	public void setInstpast(int indx, Object what) {
		setInstpast(indx, what, false);
	}
	public void setInstpast(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInstpast01(what, rounded);
					 break;
			case 2 : setInstpast02(what, rounded);
					 break;
			case 3 : setInstpast03(what, rounded);
					 break;
			case 4 : setInstpast04(what, rounded);
					 break;
			case 5 : setInstpast05(what, rounded);
					 break;
			case 6 : setInstpast06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getDiscodes() {
		return new FixedLengthStringData(discode01.toInternal()
										+ discode02.toInternal()
										+ discode03.toInternal()
										+ discode04.toInternal());
	}
	public void setDiscodes(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getDiscodes().getLength()).init(obj);
	
		what = ExternalData.chop(what, discode01);
		what = ExternalData.chop(what, discode02);
		what = ExternalData.chop(what, discode03);
		what = ExternalData.chop(what, discode04);
	}
	public FixedLengthStringData getDiscode(BaseData indx) {
		return getDiscode(indx.toInt());
	}
	public FixedLengthStringData getDiscode(int indx) {

		switch (indx) {
			case 1 : return discode01;
			case 2 : return discode02;
			case 3 : return discode03;
			case 4 : return discode04;
			default: return null; // Throw error instead?
		}
	
	}
	public void setDiscode(BaseData indx, Object what) {
		setDiscode(indx.toInt(), what);
	}
	public void setDiscode(int indx, Object what) {

		switch (indx) {
			case 1 : setDiscode01(what);
					 break;
			case 2 : setDiscode02(what);
					 break;
			case 3 : setDiscode03(what);
					 break;
			case 4 : setDiscode04(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getBilldates() {
		return new FixedLengthStringData(billdate01.toInternal()
										+ billdate02.toInternal()
										+ billdate03.toInternal()
										+ billdate04.toInternal());
	}
	public void setBilldates(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getBilldates().getLength()).init(obj);
	
		what = ExternalData.chop(what, billdate01);
		what = ExternalData.chop(what, billdate02);
		what = ExternalData.chop(what, billdate03);
		what = ExternalData.chop(what, billdate04);
	}
	public PackedDecimalData getBilldate(BaseData indx) {
		return getBilldate(indx.toInt());
	}
	public PackedDecimalData getBilldate(int indx) {

		switch (indx) {
			case 1 : return billdate01;
			case 2 : return billdate02;
			case 3 : return billdate03;
			case 4 : return billdate04;
			default: return null; // Throw error instead?
		}
	
	}
	public void setBilldate(BaseData indx, Object what) {
		setBilldate(indx, what, false);
	}
	public void setBilldate(BaseData indx, Object what, boolean rounded) {
		setBilldate(indx.toInt(), what, rounded);
	}
	public void setBilldate(int indx, Object what) {
		setBilldate(indx, what, false);
	}
	public void setBilldate(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setBilldate01(what, rounded);
					 break;
			case 2 : setBilldate02(what, rounded);
					 break;
			case 3 : setBilldate03(what, rounded);
					 break;
			case 4 : setBilldate04(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getBillamts() {
		return new FixedLengthStringData(billamt01.toInternal()
										+ billamt02.toInternal()
										+ billamt03.toInternal()
										+ billamt04.toInternal());
	}
	public void setBillamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getBillamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, billamt01);
		what = ExternalData.chop(what, billamt02);
		what = ExternalData.chop(what, billamt03);
		what = ExternalData.chop(what, billamt04);
	}
	public PackedDecimalData getBillamt(BaseData indx) {
		return getBillamt(indx.toInt());
	}
	public PackedDecimalData getBillamt(int indx) {

		switch (indx) {
			case 1 : return billamt01;
			case 2 : return billamt02;
			case 3 : return billamt03;
			case 4 : return billamt04;
			default: return null; // Throw error instead?
		}
	
	}
	public void setBillamt(BaseData indx, Object what) {
		setBillamt(indx, what, false);
	}
	public void setBillamt(BaseData indx, Object what, boolean rounded) {
		setBillamt(indx.toInt(), what, rounded);
	}
	public void setBillamt(int indx, Object what) {
		setBillamt(indx, what, false);
	}
	public void setBillamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setBillamt01(what, rounded);
					 break;
			case 2 : setBillamt02(what, rounded);
					 break;
			case 3 : setBillamt03(what, rounded);
					 break;
			case 4 : setBillamt04(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getAnnamts() {
		return new FixedLengthStringData(annamt01.toInternal()
										+ annamt02.toInternal()
										+ annamt03.toInternal()
										+ annamt04.toInternal()
										+ annamt05.toInternal()
										+ annamt06.toInternal());
	}
	public void setAnnamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getAnnamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, annamt01);
		what = ExternalData.chop(what, annamt02);
		what = ExternalData.chop(what, annamt03);
		what = ExternalData.chop(what, annamt04);
		what = ExternalData.chop(what, annamt05);
		what = ExternalData.chop(what, annamt06);
	}
	public PackedDecimalData getAnnamt(BaseData indx) {
		return getAnnamt(indx.toInt());
	}
	public PackedDecimalData getAnnamt(int indx) {

		switch (indx) {
			case 1 : return annamt01;
			case 2 : return annamt02;
			case 3 : return annamt03;
			case 4 : return annamt04;
			case 5 : return annamt05;
			case 6 : return annamt06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setAnnamt(BaseData indx, Object what) {
		setAnnamt(indx, what, false);
	}
	public void setAnnamt(BaseData indx, Object what, boolean rounded) {
		setAnnamt(indx.toInt(), what, rounded);
	}
	public void setAnnamt(int indx, Object what) {
		setAnnamt(indx, what, false);
	}
	public void setAnnamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setAnnamt01(what, rounded);
					 break;
			case 2 : setAnnamt02(what, rounded);
					 break;
			case 3 : setAnnamt03(what, rounded);
					 break;
			case 4 : setAnnamt04(what, rounded);
					 break;
			case 5 : setAnnamt05(what, rounded);
					 break;
			case 6 : setAnnamt06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		chdrpfx.clear();
		nonKeyFiller20.clear();
		chdrnum.clear();
		recode.clear();
		servunit.clear();
		cnttype.clear();
		tranno.clear();
		tranid.clear();
		validflag.clear();
		currfrom.clear();
		currto.clear();
		proctrancd.clear();
		procflag.clear();
		procid.clear();
		statcode.clear();
		statreasn.clear();
		statdate.clear();
		stattran.clear();
		tranlused.clear();
		occdate.clear();
		ccdate.clear();
		crdate.clear();
		annamt01.clear();
		annamt02.clear();
		annamt03.clear();
		annamt04.clear();
		annamt05.clear();
		annamt06.clear();
		rnltype.clear();
		rnlnots.clear();
		rnlnotto.clear();
		rnlattn.clear();
		rnldurn.clear();
		reptype.clear();
		repnum.clear();
		cownpfx.clear();
		cowncoy.clear();
		cownnum.clear();
		payrpfx.clear();
		payrcoy.clear();
		payrnum.clear();
		desppfx.clear();
		despcoy.clear();
		despnum.clear();
		asgnpfx.clear();
		asgncoy.clear();
		asgnnum.clear();
		cntbranch.clear();
		agntpfx.clear();
		agntcoy.clear();
		agntnum.clear();
		cntcurr.clear();
		acctccy.clear();
		crate.clear();
		payplan.clear();
		acctmeth.clear();
		billfreq.clear();
		billchnl.clear();
		collchnl.clear();
		mandref.clear();
		billday.clear();
		billmonth.clear();
		billcd.clear();
		btdate.clear();
		ptdate.clear();
		payflag.clear();
		sinstfrom.clear();
		sinstto.clear();
		sinstamt01.clear();
		sinstamt02.clear();
		sinstamt03.clear();
		sinstamt04.clear();
		sinstamt05.clear();
		sinstamt06.clear();
		instfrom.clear();
		instto.clear();
		instbchnl.clear();
		instcchnl.clear();
		instfreq.clear();
		insttot01.clear();
		insttot02.clear();
		insttot03.clear();
		insttot04.clear();
		insttot05.clear();
		insttot06.clear();
		inststamt01.clear();
		inststamt02.clear();
		inststamt03.clear();
		inststamt04.clear();
		inststamt05.clear();
		inststamt06.clear();
		instpast01.clear();
		instpast02.clear();
		instpast03.clear();
		instpast04.clear();
		instpast05.clear();
		instpast06.clear();
		instjctl.clear();
		nofoutinst.clear();
		outstamt.clear();
		billdate01.clear();
		billdate02.clear();
		billdate03.clear();
		billdate04.clear();
		billamt01.clear();
		billamt02.clear();
		billamt03.clear();
		billamt04.clear();
		facthous.clear();
		bankkey.clear();
		bankacckey.clear();
		discode01.clear();
		discode02.clear();
		discode03.clear();
		discode04.clear();
		grupkey.clear();
		membsel.clear();
		aplsupr.clear();
		billsupr.clear();
		commsupr.clear();
		lapssupr.clear();
		mailsupr.clear();
		notssupr.clear();
		rnwlsupr.clear();
		aplspfrom.clear();
		billspfrom.clear();
		commspfrom.clear();
		lapsspfrom.clear();
		mailspfrom.clear();
		notsspfrom.clear();
		rnwlspfrom.clear();
		aplspto.clear();
		billspto.clear();
		commspto.clear();
		lapsspto.clear();
		mailspto.clear();
		notsspto.clear();
		rnwlspto.clear();
		campaign.clear();
		srcebus.clear();
		chdrstcda.clear();
		chdrstcdb.clear();
		chdrstcdc.clear();
		chdrstcdd.clear();
		chdrstcde.clear();
		nofrisks.clear();
		jacket.clear();
		pstatcode.clear();
		pstatreasn.clear();
		pstatdate.clear();
		pstattran.clear();
		pdind.clear();
		register.clear();
		mplpfx.clear();
		mplcoy.clear();
		mplnum.clear();
		clupfx.clear();
		clucoy.clear();
		clunum.clear();
		finpfx.clear();
		fincoy.clear();
		finnum.clear();
		poapfx.clear();
		poacoy.clear();
		poanum.clear();
		dueflg.clear();
		laprind.clear();
		specind.clear();
		chgflag.clear();
		polpln.clear();
		wvfdat.clear();
		wvtdat.clear();
		wvfind.clear();
		bfcharge.clear();
		dishnrcnt.clear();
		pdtype.clear();
		dishnrdte.clear();
		stmpdtyamt.clear();
		stmpdtydte.clear();
		covernt.clear();
		cntiss.clear();
		cntrcv.clear();
		quoteno.clear();
		bankcode.clear();
		docnum.clear();
		rnlsts.clear();
		sustrcde.clear();
		dtecan.clear();
		cotype.clear();
		coppn.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();	
		zmandref.clear(); //ILIFE-2472
  		reqntype.clear(); //ILIFE-2472	
  		payclt.clear();//ILIFE-2472 PH2
	}


}