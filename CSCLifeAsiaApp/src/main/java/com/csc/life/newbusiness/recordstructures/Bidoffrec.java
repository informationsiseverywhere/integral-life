package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:35
 * Description:
 * Copybook name: BIDOFFREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Bidoffrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData bidoffRec = new FixedLengthStringData(184);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(bidoffRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(bidoffRec, 1);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(bidoffRec, 5);
  	public FixedLengthStringData fund = new FixedLengthStringData(4).isAPartOf(bidoffRec, 6);
  	public ZonedDecimalData barePrice = new ZonedDecimalData(9, 5).isAPartOf(bidoffRec, 10);
  	public ZonedDecimalData bidPrice = new ZonedDecimalData(9, 5).isAPartOf(bidoffRec, 19);
  	public ZonedDecimalData offerPrice = new ZonedDecimalData(9, 5).isAPartOf(bidoffRec, 28);
  	public FixedLengthStringData unitType = new FixedLengthStringData(1).isAPartOf(bidoffRec, 37);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(bidoffRec, 38);
  	public FixedLengthStringData filler = new FixedLengthStringData(138).isAPartOf(bidoffRec, 46, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(bidoffRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bidoffRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}