package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:25
 * Description:
 * Copybook name: ZSTRAGTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zstragtkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zstragtFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData zstragtKey = new FixedLengthStringData(64).isAPartOf(zstragtFileKey, 0, REDEFINE);
  	public FixedLengthStringData zstragtChdrcoy = new FixedLengthStringData(1).isAPartOf(zstragtKey, 0);
  	public FixedLengthStringData zstragtCntbranch = new FixedLengthStringData(2).isAPartOf(zstragtKey, 1);
  	public FixedLengthStringData zstragtAracde = new FixedLengthStringData(3).isAPartOf(zstragtKey, 3);
  	public FixedLengthStringData zstragtAgntnum = new FixedLengthStringData(8).isAPartOf(zstragtKey, 6);
  	public PackedDecimalData zstragtEffdate = new PackedDecimalData(8, 0).isAPartOf(zstragtKey, 14);
  	public FixedLengthStringData filler = new FixedLengthStringData(45).isAPartOf(zstragtKey, 19, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zstragtFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zstragtFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}