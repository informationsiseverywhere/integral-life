package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;


public class T3615rec extends ExternalData {

  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t3615Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData onepcashless = new FixedLengthStringData(1).isAPartOf(t3615Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(498).isAPartOf(t3615Rec, 1, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t3615Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t3615Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}