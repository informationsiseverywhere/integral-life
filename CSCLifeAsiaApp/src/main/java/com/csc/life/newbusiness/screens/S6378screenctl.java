package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6378screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	//public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 20, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "S6378screensfl";
		lrec.subfileClass = S6378screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 11;
		lrec.pageSubfile = 4;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 18, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6378ScreenVars sv = (S6378ScreenVars) pv;
		clearInds(av, sv.getScreenSflPfInds());
		write(lrec, sv.S6378screenctlWritten, sv.S6378screensflWritten, av, sv.s6378screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6378ScreenVars screenVars = (S6378ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		ScreenRecord.setClassStringFormatting(pv);
/*		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.billcurr.setClassString("");
		screenVars.exrat.setClassString("");
		screenVars.instPrem.setClassString("");
		screenVars.premCurr.setClassString("");
		screenVars.cntfee.setClassString("");
		screenVars.pufee.setClassString("");
		screenVars.premsusp.setClassString("");
		screenVars.prmdepst.setClassString("");
		screenVars.taxamt01.setClassString("");
		screenVars.taxamt02.setClassString("");
		screenVars.totlprm.setClassString("");*/
	}

/**
 * Clear all the variables in S6378screenctl
 */
	public static void clear(VarModel pv) {
		S6378ScreenVars screenVars = (S6378ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		ScreenRecord.clear(pv);
		/*screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.cntcurr.clear();
		screenVars.billcurr.clear();
		screenVars.exrat.clear();
		screenVars.instPrem.clear();
		screenVars.premCurr.clear();
		screenVars.cntfee.clear();
		screenVars.pufee.clear();
		screenVars.premsusp.clear();
		screenVars.prmdepst.clear();
		screenVars.taxamt01.clear();
		screenVars.taxamt02.clear();
		screenVars.totlprm.clear();*/
	}
}
