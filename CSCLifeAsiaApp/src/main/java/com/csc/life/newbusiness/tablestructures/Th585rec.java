package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:27
 * Description:
 * Copybook name: TH585REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th585rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th585Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData hxclnotes = new FixedLengthStringData(450).isAPartOf(th585Rec, 0);
  	public FixedLengthStringData[] hxclnote = FLSArrayPartOfStructure(6, 75, hxclnotes, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(450).isAPartOf(hxclnotes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData hxclnote01 = new FixedLengthStringData(75).isAPartOf(filler, 0);
  	public FixedLengthStringData hxclnote02 = new FixedLengthStringData(75).isAPartOf(filler, 75);
  	public FixedLengthStringData hxclnote03 = new FixedLengthStringData(75).isAPartOf(filler, 150);
  	public FixedLengthStringData hxclnote04 = new FixedLengthStringData(75).isAPartOf(filler, 225);
  	public FixedLengthStringData hxclnote05 = new FixedLengthStringData(75).isAPartOf(filler, 300);
  	public FixedLengthStringData hxclnote06 = new FixedLengthStringData(75).isAPartOf(filler, 375);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(50).isAPartOf(th585Rec, 450, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th585Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th585Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}