package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sh565screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh565ScreenVars sv = (Sh565ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh565screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh565ScreenVars screenVars = (Sh565ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.currcode.setClassString("");
		screenVars.ztrcde01.setClassString("");
		screenVars.trandesc01.setClassString("");
		screenVars.ztrcde02.setClassString("");
		screenVars.trandesc02.setClassString("");
		screenVars.ztrcde03.setClassString("");
		screenVars.trandesc03.setClassString("");
		screenVars.ztrcde04.setClassString("");
		screenVars.trandesc04.setClassString("");
		screenVars.ztrcde05.setClassString("");
		screenVars.trandesc05.setClassString("");
		screenVars.ztrcde06.setClassString("");
		screenVars.trandesc06.setClassString("");
		screenVars.ztrcde07.setClassString("");
		screenVars.trandesc07.setClassString("");
		screenVars.ztrcde08.setClassString("");
		screenVars.trandesc08.setClassString("");
		screenVars.ztrcde09.setClassString("");
		screenVars.trandesc09.setClassString("");
		screenVars.ztrcde10.setClassString("");
		screenVars.trandesc10.setClassString("");
		screenVars.zmthfrm01.setClassString("");
		screenVars.zmthfrm02.setClassString("");
		screenVars.zmthfrm03.setClassString("");
		screenVars.zmthfrm04.setClassString("");
		screenVars.zmthto01.setClassString("");
		screenVars.zmthto02.setClassString("");
		screenVars.zmthto03.setClassString("");
		screenVars.zmthto04.setClassString("");
	}

/**
 * Clear all the variables in Sh565screen
 */
	public static void clear(VarModel pv) {
		Sh565ScreenVars screenVars = (Sh565ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.currcode.clear();
		screenVars.ztrcde01.clear();
		screenVars.trandesc01.clear();
		screenVars.ztrcde02.clear();
		screenVars.trandesc02.clear();
		screenVars.ztrcde03.clear();
		screenVars.trandesc03.clear();
		screenVars.ztrcde04.clear();
		screenVars.trandesc04.clear();
		screenVars.ztrcde05.clear();
		screenVars.trandesc05.clear();
		screenVars.ztrcde06.clear();
		screenVars.trandesc06.clear();
		screenVars.ztrcde07.clear();
		screenVars.trandesc07.clear();
		screenVars.ztrcde08.clear();
		screenVars.trandesc08.clear();
		screenVars.ztrcde09.clear();
		screenVars.trandesc09.clear();
		screenVars.ztrcde10.clear();
		screenVars.trandesc10.clear();
		screenVars.zmthfrm01.clear();
		screenVars.zmthfrm02.clear();
		screenVars.zmthfrm03.clear();
		screenVars.zmthfrm04.clear();
		screenVars.zmthto01.clear();
		screenVars.zmthto02.clear();
		screenVars.zmthto03.clear();
		screenVars.zmthto04.clear();
	}
}
