package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:20
 * Description:
 * Copybook name: CHDRLNBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrlnbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrlnbFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdrlnbKey = new FixedLengthStringData(64).isAPartOf(chdrlnbFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrlnbChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrlnbKey, 0);
  	public FixedLengthStringData chdrlnbChdrnum = new FixedLengthStringData(8).isAPartOf(chdrlnbKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(chdrlnbKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrlnbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrlnbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}