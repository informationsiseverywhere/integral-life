/*
 * File: T5658pt.java
 * Date: 30 August 2009 2:24:57
 * Author: Quipoz Limited
 * 
 * Class transformed from T5658PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.newbusiness.tablestructures.T5658rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5658.
*
*
*
***********************************************************************
* </pre>
*/
public class T5658pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5658rec t5658rec = new T5658rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5658pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5658rec.t5658Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		generalCopyLinesInner.fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		generalCopyLinesInner.fieldNo006.set(datcon1rec.extDate);
		generalCopyLinesInner.fieldNo007.set(t5658rec.unit);
		generalCopyLinesInner.fieldNo008.set(t5658rec.insprm01);
		generalCopyLinesInner.fieldNo009.set(t5658rec.insprm02);
		generalCopyLinesInner.fieldNo010.set(t5658rec.insprm03);
		generalCopyLinesInner.fieldNo011.set(t5658rec.insprm04);
		generalCopyLinesInner.fieldNo012.set(t5658rec.insprm05);
		generalCopyLinesInner.fieldNo013.set(t5658rec.insprm06);
		generalCopyLinesInner.fieldNo014.set(t5658rec.insprm07);
		generalCopyLinesInner.fieldNo015.set(t5658rec.insprm08);
		generalCopyLinesInner.fieldNo016.set(t5658rec.insprm09);
		generalCopyLinesInner.fieldNo017.set(t5658rec.insprm10);
		generalCopyLinesInner.fieldNo018.set(t5658rec.insprm11);
		generalCopyLinesInner.fieldNo019.set(t5658rec.insprm12);
		generalCopyLinesInner.fieldNo020.set(t5658rec.insprm13);
		generalCopyLinesInner.fieldNo021.set(t5658rec.insprm14);
		generalCopyLinesInner.fieldNo022.set(t5658rec.insprm15);
		generalCopyLinesInner.fieldNo023.set(t5658rec.insprm16);
		generalCopyLinesInner.fieldNo024.set(t5658rec.insprm17);
		generalCopyLinesInner.fieldNo025.set(t5658rec.insprm18);
		generalCopyLinesInner.fieldNo026.set(t5658rec.insprm19);
		generalCopyLinesInner.fieldNo027.set(t5658rec.insprm20);
		generalCopyLinesInner.fieldNo028.set(t5658rec.insprm21);
		generalCopyLinesInner.fieldNo029.set(t5658rec.insprm22);
		generalCopyLinesInner.fieldNo030.set(t5658rec.insprm23);
		generalCopyLinesInner.fieldNo031.set(t5658rec.insprm24);
		generalCopyLinesInner.fieldNo032.set(t5658rec.insprm25);
		generalCopyLinesInner.fieldNo033.set(t5658rec.insprm26);
		generalCopyLinesInner.fieldNo034.set(t5658rec.insprm27);
		generalCopyLinesInner.fieldNo035.set(t5658rec.insprm28);
		generalCopyLinesInner.fieldNo036.set(t5658rec.insprm29);
		generalCopyLinesInner.fieldNo037.set(t5658rec.insprm30);
		generalCopyLinesInner.fieldNo038.set(t5658rec.insprm31);
		generalCopyLinesInner.fieldNo039.set(t5658rec.insprm32);
		generalCopyLinesInner.fieldNo040.set(t5658rec.insprm33);
		generalCopyLinesInner.fieldNo041.set(t5658rec.insprm34);
		generalCopyLinesInner.fieldNo042.set(t5658rec.insprm35);
		generalCopyLinesInner.fieldNo043.set(t5658rec.insprm36);
		generalCopyLinesInner.fieldNo044.set(t5658rec.insprm37);
		generalCopyLinesInner.fieldNo045.set(t5658rec.insprm38);
		generalCopyLinesInner.fieldNo046.set(t5658rec.insprm39);
		generalCopyLinesInner.fieldNo047.set(t5658rec.insprm40);
		generalCopyLinesInner.fieldNo048.set(t5658rec.insprm41);
		generalCopyLinesInner.fieldNo049.set(t5658rec.insprm42);
		generalCopyLinesInner.fieldNo050.set(t5658rec.insprm43);
		generalCopyLinesInner.fieldNo051.set(t5658rec.insprm44);
		generalCopyLinesInner.fieldNo052.set(t5658rec.insprm45);
		generalCopyLinesInner.fieldNo053.set(t5658rec.insprm46);
		generalCopyLinesInner.fieldNo054.set(t5658rec.insprm47);
		generalCopyLinesInner.fieldNo055.set(t5658rec.insprm48);
		generalCopyLinesInner.fieldNo056.set(t5658rec.insprm49);
		generalCopyLinesInner.fieldNo057.set(t5658rec.insprm50);
		generalCopyLinesInner.fieldNo058.set(t5658rec.insprm51);
		generalCopyLinesInner.fieldNo059.set(t5658rec.insprm52);
		generalCopyLinesInner.fieldNo060.set(t5658rec.insprm53);
		generalCopyLinesInner.fieldNo061.set(t5658rec.insprm54);
		generalCopyLinesInner.fieldNo062.set(t5658rec.insprm55);
		generalCopyLinesInner.fieldNo063.set(t5658rec.insprm56);
		generalCopyLinesInner.fieldNo064.set(t5658rec.insprm57);
		generalCopyLinesInner.fieldNo065.set(t5658rec.insprm58);
		generalCopyLinesInner.fieldNo066.set(t5658rec.insprm59);
		generalCopyLinesInner.fieldNo067.set(t5658rec.insprm60);
		generalCopyLinesInner.fieldNo068.set(t5658rec.insprm61);
		generalCopyLinesInner.fieldNo069.set(t5658rec.insprm62);
		generalCopyLinesInner.fieldNo070.set(t5658rec.insprm63);
		generalCopyLinesInner.fieldNo071.set(t5658rec.insprm64);
		generalCopyLinesInner.fieldNo072.set(t5658rec.insprm65);
		generalCopyLinesInner.fieldNo073.set(t5658rec.insprm66);
		generalCopyLinesInner.fieldNo074.set(t5658rec.insprm67);
		generalCopyLinesInner.fieldNo075.set(t5658rec.insprm68);
		generalCopyLinesInner.fieldNo076.set(t5658rec.insprm69);
		generalCopyLinesInner.fieldNo077.set(t5658rec.insprm70);
		generalCopyLinesInner.fieldNo078.set(t5658rec.insprm71);
		generalCopyLinesInner.fieldNo079.set(t5658rec.insprm72);
		generalCopyLinesInner.fieldNo080.set(t5658rec.insprm73);
		generalCopyLinesInner.fieldNo081.set(t5658rec.insprm74);
		generalCopyLinesInner.fieldNo082.set(t5658rec.insprm75);
		generalCopyLinesInner.fieldNo083.set(t5658rec.insprm76);
		generalCopyLinesInner.fieldNo084.set(t5658rec.insprm77);
		generalCopyLinesInner.fieldNo085.set(t5658rec.insprm78);
		generalCopyLinesInner.fieldNo086.set(t5658rec.insprm79);
		generalCopyLinesInner.fieldNo087.set(t5658rec.insprm80);
		generalCopyLinesInner.fieldNo088.set(t5658rec.insprm81);
		generalCopyLinesInner.fieldNo089.set(t5658rec.insprm82);
		generalCopyLinesInner.fieldNo090.set(t5658rec.insprm83);
		generalCopyLinesInner.fieldNo091.set(t5658rec.insprm84);
		generalCopyLinesInner.fieldNo092.set(t5658rec.insprm85);
		generalCopyLinesInner.fieldNo093.set(t5658rec.insprm86);
		generalCopyLinesInner.fieldNo094.set(t5658rec.insprm87);
		generalCopyLinesInner.fieldNo095.set(t5658rec.insprm88);
		generalCopyLinesInner.fieldNo096.set(t5658rec.insprm89);
		generalCopyLinesInner.fieldNo097.set(t5658rec.insprm90);
		generalCopyLinesInner.fieldNo098.set(t5658rec.insprm91);
		generalCopyLinesInner.fieldNo099.set(t5658rec.insprm92);
		generalCopyLinesInner.fieldNo100.set(t5658rec.insprm93);
		generalCopyLinesInner.fieldNo101.set(t5658rec.insprm94);
		generalCopyLinesInner.fieldNo102.set(t5658rec.insprm95);
		generalCopyLinesInner.fieldNo103.set(t5658rec.insprm96);
		generalCopyLinesInner.fieldNo104.set(t5658rec.insprm97);
		generalCopyLinesInner.fieldNo105.set(t5658rec.insprm98);
		generalCopyLinesInner.fieldNo106.set(t5658rec.insprm99);
		generalCopyLinesInner.fieldNo107.set(t5658rec.instpr01);
		generalCopyLinesInner.fieldNo108.set(t5658rec.instpr02);
		generalCopyLinesInner.fieldNo109.set(t5658rec.instpr03);
		generalCopyLinesInner.fieldNo110.set(t5658rec.instpr04);
		generalCopyLinesInner.fieldNo111.set(t5658rec.instpr05);
		generalCopyLinesInner.fieldNo112.set(t5658rec.instpr06);
		generalCopyLinesInner.fieldNo113.set(t5658rec.instpr07);
		generalCopyLinesInner.fieldNo114.set(t5658rec.instpr08);
		generalCopyLinesInner.fieldNo115.set(t5658rec.instpr09);
		generalCopyLinesInner.fieldNo116.set(t5658rec.instpr10);
		generalCopyLinesInner.fieldNo117.set(t5658rec.instpr11);
		generalCopyLinesInner.fieldNo119.set(t5658rec.mfacty);
		generalCopyLinesInner.fieldNo120.set(t5658rec.mfacthy);
		generalCopyLinesInner.fieldNo122.set(t5658rec.mfactm);
		generalCopyLinesInner.fieldNo123.set(t5658rec.mfacthm);
		generalCopyLinesInner.fieldNo124.set(t5658rec.mfact4w);
		generalCopyLinesInner.fieldNo125.set(t5658rec.mfact2w);
		generalCopyLinesInner.fieldNo126.set(t5658rec.mfactw);
		generalCopyLinesInner.fieldNo128.set(t5658rec.premUnit);
		generalCopyLinesInner.fieldNo121.set(t5658rec.mfactq);
		generalCopyLinesInner.fieldNo127.set(t5658rec.disccntmeth);
		generalCopyLinesInner.fieldNo118.set(t5658rec.insprem);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine018);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine019);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(54).isAPartOf(wsaaPrtLine001, 22, FILLER).init("Basic Annual Premium Parameters - Term           S5658");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(74);
	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 0, FILLER).init("Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 9);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 10, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 20);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 25, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 33);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 44);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(71);
	private FixedLengthStringData filler7 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Valid from:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 15);
	private FixedLengthStringData filler8 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 25, FILLER).init(SPACES);
	private FixedLengthStringData filler9 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 30, FILLER).init("To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 35);
	private FixedLengthStringData filler10 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 45, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine003, 50, FILLER).init("Risk Unit:");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(9, 0).isAPartOf(wsaaPrtLine003, 62).setPattern("ZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(76);
	private FixedLengthStringData filler12 = new FixedLengthStringData(76).isAPartOf(wsaaPrtLine004, 0, FILLER).init("  Age:      1      2      3      4      5      6      7 8      9     10");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(79);
	private FixedLengthStringData filler13 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 0, FILLER).init("   0+");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine005, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(79);
	private FixedLengthStringData filler23 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine006, 0, FILLER).init("  10+");
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine006, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(79);
	private FixedLengthStringData filler33 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 0, FILLER).init("  20+");
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine007, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(79);
	private FixedLengthStringData filler43 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine008, 0, FILLER).init("  30+");
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine008, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(79);
	private FixedLengthStringData filler53 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 0, FILLER).init("  40+");
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine009, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(79);
	private FixedLengthStringData filler63 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine010, 0, FILLER).init("  50+");
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler69 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler72 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine010, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(79);
	private FixedLengthStringData filler73 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 0, FILLER).init("  60+");
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler79 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler80 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler81 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo076 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine011, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(79);
	private FixedLengthStringData filler83 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 0, FILLER).init("  70+");
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler84 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo079 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo080 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler86 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler87 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo082 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler88 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler89 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler90 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo085 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo086 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler92 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo087 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine012, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(79);
	private FixedLengthStringData filler93 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine013, 0, FILLER).init("  80+");
	private ZonedDecimalData fieldNo088 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler94 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo089 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler95 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo090 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler96 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo091 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler97 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo092 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler98 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler99 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo094 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler100 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo095 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler101 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo096 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler102 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo097 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(79);
	private FixedLengthStringData filler103 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine014, 0, FILLER).init("  90+");
	private ZonedDecimalData fieldNo098 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler104 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo099 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler105 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo100 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler106 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo101 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler107 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo102 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler108 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo103 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler109 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo104 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler110 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo105 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler111 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo106 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler112 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo107 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine014, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(79);
	private FixedLengthStringData filler113 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine015, 0, FILLER).init("  100+");
	private ZonedDecimalData fieldNo108 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine015, 10).setPattern("ZZZZZZ");
	private FixedLengthStringData filler114 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo109 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine015, 17).setPattern("ZZZZZZ");
	private FixedLengthStringData filler115 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo110 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine015, 24).setPattern("ZZZZZZ");
	private FixedLengthStringData filler116 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo111 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine015, 31).setPattern("ZZZZZZ");
	private FixedLengthStringData filler117 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 37, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo112 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine015, 38).setPattern("ZZZZZZ");
	private FixedLengthStringData filler118 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo113 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine015, 45).setPattern("ZZZZZZ");
	private FixedLengthStringData filler119 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo114 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine015, 52).setPattern("ZZZZZZ");
	private FixedLengthStringData filler120 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo115 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine015, 59).setPattern("ZZZZZZ");
	private FixedLengthStringData filler121 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 65, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo116 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine015, 66).setPattern("ZZZZZZ");
	private FixedLengthStringData filler122 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo117 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine015, 73).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(16);
	private FixedLengthStringData filler123 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine016, 0, FILLER).init("  Age 0:");
	private ZonedDecimalData fieldNo118 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine016, 10).setPattern("ZZZZZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(74);
	private FixedLengthStringData filler124 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine017, 0, FILLER).init("  Modal factors:  Y:");
	private ZonedDecimalData fieldNo119 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine017, 21).setPattern("Z.ZZZZ");
	private FixedLengthStringData filler125 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine017, 27, FILLER).init("  HY:");
	private ZonedDecimalData fieldNo120 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine017, 33).setPattern("Z.ZZZZ");
	private FixedLengthStringData filler126 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine017, 39, FILLER).init("   Q:");
	private ZonedDecimalData fieldNo121 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine017, 45).setPattern("Z.ZZZZ");
	private FixedLengthStringData filler127 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine017, 51, FILLER).init("  M:");
	private ZonedDecimalData fieldNo122 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine017, 56).setPattern("Z.ZZZZ");
	private FixedLengthStringData filler128 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine017, 62, FILLER).init("  HM:");
	private ZonedDecimalData fieldNo123 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine017, 68).setPattern("Z.ZZZZ");

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(51);
	private FixedLengthStringData filler129 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine018, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler130 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine018, 17, FILLER).init("4W:");
	private ZonedDecimalData fieldNo124 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine018, 21).setPattern("Z.ZZZZ");
	private FixedLengthStringData filler131 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine018, 27, FILLER).init("  2W:");
	private ZonedDecimalData fieldNo125 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine018, 33).setPattern("Z.ZZZZ");
	private FixedLengthStringData filler132 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine018, 39, FILLER).init("   W:");
	private ZonedDecimalData fieldNo126 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine018, 45).setPattern("Z.ZZZZ");

	private FixedLengthStringData wsaaPrtLine019 = new FixedLengthStringData(43);
	private FixedLengthStringData filler133 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine019, 0, FILLER).init("  Discount Method:");
	private FixedLengthStringData fieldNo127 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine019, 21);
	private FixedLengthStringData filler134 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine019, 25, FILLER).init("    Prem Unit:");
	private ZonedDecimalData fieldNo128 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine019, 40).setPattern("ZZZ");
	}
}
