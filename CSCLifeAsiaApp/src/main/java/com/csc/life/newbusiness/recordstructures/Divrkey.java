package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:06
 * Description:
 * Copybook name: DIVRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Divrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData divrFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData divrKey = new FixedLengthStringData(256).isAPartOf(divrFileKey, 0, REDEFINE);
  	public FixedLengthStringData divrItemitem = new FixedLengthStringData(8).isAPartOf(divrKey, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(248).isAPartOf(divrKey, 8, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(divrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		divrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}