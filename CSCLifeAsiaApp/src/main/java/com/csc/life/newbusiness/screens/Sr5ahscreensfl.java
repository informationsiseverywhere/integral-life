package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.TableModel.Subfile.RecInfo;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sr5ahscreensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 8;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {8, 9, 4, 5, 6, 7, 1, 2, 3, 10, 50}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {15, 22, 2, 75}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr5ahScreenVars sv = (Sr5ahScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr5ahscreensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr5ahscreensfl, 
			sv.Sr5ahscreensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr5ahScreenVars sv = (Sr5ahScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr5ahscreensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr5ahScreenVars sv = (Sr5ahScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr5ahscreensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr5ahscreensflWritten.gt(0))
		{
			sv.sr5ahscreensfl.setCurrentIndex(0);
			sv.Sr5ahscreensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr5ahScreenVars sv = (Sr5ahScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr5ahscreensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr5ahScreenVars screenVars = (Sr5ahScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.seqnbr.setFieldName("seqnbr");
				screenVars.excda.setFieldName("excda");
				screenVars.longdesc.setFieldName("longdesc");
				screenVars.select.setFieldName("select");
				screenVars.prntstat.setFieldName("prntstat");
				screenVars.uniqueNum.setFieldName("uniqueNum");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.seqnbr.set(dm.getField("seqnbr"));
			screenVars.excda.set(dm.getField("excda"));
			screenVars.longdesc.set(dm.getField("longdesc"));
			screenVars.select.set(dm.getField("select"));
			screenVars.prntstat.set(dm.getField("prntstat"));
			screenVars.uniqueNum.set(dm.getField("uniqueNum"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr5ahScreenVars screenVars = (Sr5ahScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.seqnbr.setFieldName("seqnbr");
				screenVars.excda.setFieldName("excda");
				screenVars.longdesc.setFieldName("longdesc");
				screenVars.select.setFieldName("select");
				screenVars.prntstat.setFieldName("prntstat");
				screenVars.uniqueNum.setFieldName("uniqueNum");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("seqnbr").set(screenVars.seqnbr);
			dm.getField("excda").set(screenVars.excda);
			dm.getField("longdesc").set(screenVars.longdesc);
			dm.getField("select").set(screenVars.select);
			dm.getField("prntstat").set(screenVars.prntstat);
			dm.getField("uniqueNum").set(screenVars.uniqueNum);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr5ahscreensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr5ahScreenVars screenVars = (Sr5ahScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.seqnbr.clearFormatting();
		screenVars.excda.clearFormatting();
		screenVars.longdesc.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.prntstat.clearFormatting();
		screenVars.uniqueNum.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr5ahScreenVars screenVars = (Sr5ahScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.seqnbr.setClassString("");
		screenVars.excda.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.select.setClassString("");
		screenVars.prntstat.setClassString("");
		screenVars.uniqueNum.setClassString("");
		
	}

/**
 * Clear all the variables in Sr5ahscreensfl
 */
	public static void clear(VarModel pv) {
		Sr5ahScreenVars screenVars = (Sr5ahScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.seqnbr.clear();
		screenVars.excda.clear();
		screenVars.longdesc.clear();
		screenVars.select.clear();
		screenVars.prntstat.clear();
		screenVars.uniqueNum.clear();
	}
}
