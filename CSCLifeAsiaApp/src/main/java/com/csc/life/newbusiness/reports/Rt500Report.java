package com.csc.life.newbusiness.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RT500.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:52
 * @author Quipoz
 */
public class Rt500Report extends SMARTReportLayout { 

	private ZonedDecimalData anetpre = new ZonedDecimalData(13, 2);
	private ZonedDecimalData astpdty = new ZonedDecimalData(9, 2);
	private FixedLengthStringData ccdate = new FixedLengthStringData(10);
	private FixedLengthStringData chdrno = new FixedLengthStringData(8);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData currcode = new FixedLengthStringData(3);
	private FixedLengthStringData currencynm = new FixedLengthStringData(30);
	private ZonedDecimalData gcnt = new ZonedDecimalData(5, 0);
	private FixedLengthStringData linsname = new FixedLengthStringData(47);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData repdate = new FixedLengthStringData(10);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rt500Report() {
		super();
	}


	/**
	 * Print the XML for Rt500d01
	 */
	public void printRt500d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrno.setFieldName("chdrno");
		chdrno.setInternal(subString(recordData, 1, 8));
		linsname.setFieldName("linsname");
		linsname.setInternal(subString(recordData, 9, 47));
		ccdate.setFieldName("ccdate");
		ccdate.setInternal(subString(recordData, 56, 10));
		anetpre.setFieldName("anetpre");
		anetpre.setInternal(subString(recordData, 66, 13));
		astpdty.setFieldName("astpdty");
		astpdty.setInternal(subString(recordData, 79, 9));
		printLayout("Rt500d01",			// Record name
			new BaseData[]{			// Fields:
				chdrno,
				linsname,
				ccdate,
				anetpre,
				astpdty
			}
		);

	}

	/**
	 * Print the XML for Rt500h01
	 */
	public void printRt500h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 1, 10));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 11, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 12, 30));
		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 42, 10));
		time.setFieldName("time");
		time.set(getTime());
		currcode.setFieldName("currcode");
		currcode.setInternal(subString(recordData, 52, 3));
		currencynm.setFieldName("currencynm");
		currencynm.setInternal(subString(recordData, 55, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rt500h01",			// Record name
			new BaseData[]{			// Fields:
				sdate,
				company,
				companynm,
				repdate,
				time,
				currcode,
				currencynm,
				pagnbr
			}
		);

		currentPrintLine.set(8);
	}

	/**
	 * Print the XML for Rt500t01
	 */
	public void printRt500t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(1).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		gcnt.setFieldName("gcnt");
		gcnt.setInternal(subString(recordData, 1, 5));
		anetpre.setFieldName("anetpre");
		anetpre.setInternal(subString(recordData, 6, 13));
		astpdty.setFieldName("astpdty");
		astpdty.setInternal(subString(recordData, 19, 9));
		printLayout("Rt500t01",			// Record name
			new BaseData[]{			// Fields:
				gcnt,
				anetpre,
				astpdty
			}
			, new Object[] {			// indicators
				new Object[]{"ind01", indicArea.charAt(1)}
			}
		);

		currentPrintLine.add(1);
	}


}
