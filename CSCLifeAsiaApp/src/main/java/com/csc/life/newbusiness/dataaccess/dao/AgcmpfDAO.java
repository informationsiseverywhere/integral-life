package com.csc.life.newbusiness.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.newbusiness.dataaccess.model.Agcmpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface AgcmpfDAO extends BaseDAO<Agcmpf> {
    public Map<String,List<Agcmpf>> searchAgcmpf(String coy,List<String> chdrnum);
    public void updateAgcmByUniqNum(List<Agcmpf> a);
    public List<Agcmpf> readAgcmpfData(String chdrcoy,String chdrnum,String agntnum);    //ILIFE-1622
	  //add by wli31 ILIFE-7308
    public Agcmpf searchAgcmRecord(Agcmpf agcmpf);
    public Map<String, List<Agcmpf>> searchAgcmrnl(String coy, List<String> chdrnumList);
    public void insertAgcmpfList(List<Agcmpf> agcmpfList);
    public void updateInvalidAgcmByUniqNum(List<Agcmpf> a);
}