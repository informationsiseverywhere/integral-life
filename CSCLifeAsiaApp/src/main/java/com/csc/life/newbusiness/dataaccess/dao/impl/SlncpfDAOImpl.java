package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.newbusiness.dataaccess.dao.SlncpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Slncpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

/**
 * @author gsaluja2
 *
 */
public class SlncpfDAOImpl extends BaseDAOImpl<Slncpf> implements SlncpfDAO{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SlncpfDAOImpl.class);
	
	private static final String TABLE = "VM1DTA.SLNCPF";
	
	@Override
	public void insertSlncData(Slncpf slncpf) {
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ").append(TABLE).append("(ZDATACLS,ZRECLASS,ZMODTYPE,ZRSLTCDE,ZRSLTRSN,ZERRORNO, ");
		sql.append("ZNOFRECS,KLINCKEY,HCLTNAM,CLTSEX,ZCLNTDOB,ZEFFDATE,FLAG,KGLCMPCD, ADDRSS,ZHOSPBNF,ZSICKBNF,ZCANCBNF, ");
		sql.append("ZOTHSBNF,ZREGDATE,ZLINCDTE,KJHCLTNAM,ZHOSPOPT,ZREVDATE,ZLOWNNAM,ZCSUMINS,ZASUMINS,ZDETHOPT,ZRQFLAG, ");
		sql.append("ZRQSETDTE,ZRQCANDTE, ZEPFLAG,ZCVFLAG,ZCONTSEX,ZCONTDOB,ZCONTADDR,LIFNAMKJ,OWNNAMKJ,WFLGA,ZERRFLG, ");
		sql.append("ZOCCDATE,ZTRDATE,ZADRDDTE,ZEXDRDDTE,USRPRF,JOBNM,DATIME) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,");
		sql.append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		LOGGER.info("insertSlncData {}", sql);
		PreparedStatement ps = null;
		try{
			ps = getPrepareStatement(sql.toString());
			ps.setString(1,slncpf.getZdatacls());
			ps.setString(2,slncpf.getZreclass());
			ps.setString(3,slncpf.getZmodtype());
			ps.setString(4,slncpf.getZrsltcde());
			ps.setString(5,slncpf.getZrsltrsn());
			ps.setString(6,slncpf.getZerrorno());
			ps.setString(7,slncpf.getZnofrecs());
			ps.setString(8,slncpf.getKlinckey());
			ps.setString(9,slncpf.getHcltnam());
			ps.setString(10,slncpf.getCltsex());
			ps.setInt(11,slncpf.getZclntdob());
			ps.setInt(12,slncpf.getZeffdate());
			ps.setString(13,slncpf.getFlag());
			ps.setString(14,slncpf.getKglcmpcd());
			ps.setString(15,slncpf.getAddrss());
			ps.setBigDecimal(16,slncpf.getZhospbnf());
			ps.setBigDecimal(17,slncpf.getZsickbnf());
			ps.setBigDecimal(18,slncpf.getZcancbnf());
			ps.setBigDecimal(19,slncpf.getZothsbnf());
			ps.setInt(20, slncpf.getZregdate());
			ps.setInt(21, slncpf.getZlincdte());
			ps.setString(22,slncpf.getKjhcltnam());
			ps.setString(23,slncpf.getZhospopt());
			ps.setInt(24,slncpf.getZrevdate());
			ps.setString(25,slncpf.getZlownnam());
			ps.setBigDecimal(26,slncpf.getZcsumins());
			ps.setBigDecimal(27,slncpf.getZasumins());
			ps.setString(28,slncpf.getZdethopt());
			ps.setString(29,slncpf.getZrqflag());
			ps.setInt(30,slncpf.getZrqsetdte());
			ps.setInt(31,slncpf.getZrqcandte());
			ps.setString(32,slncpf.getZepflag());
			ps.setString(33,slncpf.getZcvflag());
			ps.setString(34,slncpf.getZcontsex());
			ps.setInt(35,slncpf.getZcontdob());
			ps.setString(36,slncpf.getZcontaddr());
			ps.setString(37,slncpf.getLifnamkj());
			ps.setString(38,slncpf.getOwnnamkj());
			ps.setString(39,slncpf.getWflga());
			ps.setString(40,slncpf.getZerrflg());
			ps.setInt(41,slncpf.getZoccdate());
			ps.setInt(42,slncpf.getZtrdate());
			ps.setInt(43,slncpf.getZadrddte());
			ps.setInt(44,slncpf.getZexdrddte());
			ps.setString(45, slncpf.getUsrprf());
			ps.setString(46, slncpf.getJobnm());
			ps.setTimestamp(47, new Timestamp(System.currentTimeMillis()));
			ps.executeUpdate();
		}
		catch (SQLException e) {
			LOGGER.error("insertSlncData()", e);	
		} finally {
			close(ps, null);			
		}
	}
	
	public void updateSlncData(Slncpf slncpf) {
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE ").append(TABLE).append(" SET ZDATACLS=?,ZRECLASS=?,ZMODTYPE=?,ZRSLTCDE=?,ZRSLTRSN=?,");
		sql.append("ZERRORNO=?,ZNOFRECS=?,KLINCKEY=?,HCLTNAM=?,CLTSEX=?,ZCLNTDOB=?,ZEFFDATE=?,FLAG=?,KGLCMPCD=?,ADDRSS=?,");
		sql.append("ZHOSPBNF=?,ZSICKBNF=?,ZCANCBNF=?,ZOTHSBNF=?,ZREGDATE=?,ZLINCDTE=?,KJHCLTNAM=?,ZHOSPOPT=?,ZREVDATE=?,");
		sql.append("ZLOWNNAM=?,ZCSUMINS=?,ZASUMINS=?,ZDETHOPT=?,ZRQFLAG=?,ZRQSETDTE=?,ZRQCANDTE=?, ZEPFLAG=?,ZCVFLAG=?,");
		sql.append("ZCONTSEX=?,ZCONTDOB=?,ZCONTADDR=?,LIFNAMKJ=?,OWNNAMKJ=?,WFLGA=?,ZERRFLG=?,ZOCCDATE=?,ZTRDATE=?,");
		sql.append("ZADRDDTE=?,ZEXDRDDTE=?,USRPRF=?,JOBNM=?,DATIME=? WHERE UNIQUE_NUMBER=?");
		LOGGER.info("updateSlncData {}", sql);
		PreparedStatement ps = null;
		try{
			ps = getPrepareStatement(sql.toString());
			ps.setString(1,slncpf.getZdatacls());
			ps.setString(2,slncpf.getZreclass());
			ps.setString(3,slncpf.getZmodtype());
			ps.setString(4,slncpf.getZrsltcde());
			ps.setString(5,slncpf.getZrsltrsn());
			ps.setString(6,slncpf.getZerrorno());
			ps.setString(7,slncpf.getZnofrecs());
			ps.setString(8,slncpf.getKlinckey());
			ps.setString(9,slncpf.getHcltnam());
			ps.setString(10,slncpf.getCltsex());
			ps.setInt(11,slncpf.getZclntdob());
			ps.setInt(12,slncpf.getZeffdate());
			ps.setString(13,slncpf.getFlag());
			ps.setString(14,slncpf.getKglcmpcd());
			ps.setString(15,slncpf.getAddrss());
			ps.setBigDecimal(16,slncpf.getZhospbnf());
			ps.setBigDecimal(17,slncpf.getZsickbnf());
			ps.setBigDecimal(18,slncpf.getZcancbnf());
			ps.setBigDecimal(19,slncpf.getZothsbnf());
			ps.setInt(20, slncpf.getZregdate());
			ps.setInt(21, slncpf.getZlincdte());
			ps.setString(22,slncpf.getKjhcltnam());
			ps.setString(23,slncpf.getZhospopt());
			ps.setInt(24,slncpf.getZrevdate());
			ps.setString(25,slncpf.getZlownnam());
			ps.setBigDecimal(26,slncpf.getZcsumins());
			ps.setBigDecimal(27,slncpf.getZasumins());
			ps.setString(28,slncpf.getZdethopt());
			ps.setString(29,slncpf.getZrqflag());
			ps.setInt(30,slncpf.getZrqsetdte());
			ps.setInt(31,slncpf.getZrqcandte());
			ps.setString(32,slncpf.getZepflag());
			ps.setString(33,slncpf.getZcvflag());
			ps.setString(34,slncpf.getZcontsex());
			ps.setInt(35,slncpf.getZcontdob());
			ps.setString(36,slncpf.getZcontaddr());
			ps.setString(37,slncpf.getLifnamkj());
			ps.setString(38,slncpf.getOwnnamkj());
			ps.setString(39,slncpf.getWflga());
			ps.setString(40,slncpf.getZerrflg());
			ps.setInt(41,slncpf.getZoccdate());
			ps.setInt(42,slncpf.getZtrdate());
			ps.setInt(43,slncpf.getZadrddte());
			ps.setInt(44,slncpf.getZexdrddte());
			ps.setString(45, slncpf.getUsrprf());
			ps.setString(46, slncpf.getJobnm());
			ps.setTimestamp(47, new Timestamp(System.currentTimeMillis()));
			ps.setLong(48, slncpf.getUniqueNumber());
			ps.executeUpdate();
		}
		catch (SQLException e) {
			LOGGER.error("updateSlncData()", e);	
		} finally {
			close(ps, null);			
		}
	}
	
	public Slncpf readSlncpfData(String chdrnum) {
		Slncpf slncpf = null;
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM ").append(TABLE).append(" WHERE ZHOSPOPT=? ORDER BY UNIQUE_NUMBER ASC");
		LOGGER.info("readSlncpfData {}", sql);
		PreparedStatement ps = getPrepareStatement(sql.toString());
        ResultSet rs = null;
        try {
        	ps.setString(1, chdrnum);
        	rs = executeQuery(ps);
            if (rs.next()) {
            	slncpf = new Slncpf(); 
            	slncpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
            	slncpf.setZdatacls(rs.getString("ZDATACLS"));
        		slncpf.setZreclass(rs.getString("ZRECLASS"));
        		slncpf.setZmodtype(rs.getString("ZMODTYPE"));
        		slncpf.setZrsltcde(rs.getString("ZRSLTCDE"));
        		slncpf.setZrsltrsn(rs.getString("ZRSLTRSN"));
        		slncpf.setZerrorno(rs.getString("ZERRORNO"));
        		slncpf.setZnofrecs(rs.getString("ZNOFRECS"));
        		slncpf.setKlinckey(rs.getString("KLINCKEY"));
        		slncpf.setHcltnam(rs.getString("HCLTNAM"));
        		slncpf.setCltsex(rs.getString("CLTSEX"));
        		slncpf.setZclntdob(rs.getInt("ZCLNTDOB"));
        		slncpf.setZeffdate(rs.getInt("ZEFFDATE"));
        		slncpf.setFlag(rs.getString("FLAG"));
        		slncpf.setKglcmpcd(rs.getString("KGLCMPCD"));
        		slncpf.setAddrss(rs.getString("ADDRSS"));
        		slncpf.setZhospbnf(rs.getBigDecimal("ZHOSPBNF"));
        		slncpf.setZsickbnf(rs.getBigDecimal("ZSICKBNF"));
        		slncpf.setZcancbnf(rs.getBigDecimal("ZCANCBNF"));
        		slncpf.setZothsbnf(rs.getBigDecimal("ZOTHSBNF"));
        		slncpf.setZregdate(rs.getInt("ZREGDATE"));
        		slncpf.setZlincdte(rs.getInt("ZLINCDTE"));
        		slncpf.setKjhcltnam(rs.getString("KJHCLTNAM"));
        		slncpf.setZhospopt(rs.getString("ZHOSPOPT"));
        		slncpf.setZrevdate(rs.getInt("ZREVDATE"));
        		slncpf.setZlownnam(rs.getString("ZLOWNNAM"));
        		slncpf.setZcsumins(rs.getBigDecimal("ZCSUMINS"));
        		slncpf.setZasumins(rs.getBigDecimal("ZASUMINS"));
        		slncpf.setZdethopt(rs.getString("ZDETHOPT"));
        		slncpf.setZrqflag(rs.getString("ZRQFLAG"));
        		slncpf.setZrqsetdte(rs.getInt("ZRQSETDTE"));
        		slncpf.setZrqcandte(rs.getInt("ZRQCANDTE"));
        		slncpf.setZepflag(rs.getString("ZEPFLAG"));
        		slncpf.setZcvflag(rs.getString("ZCVFLAG"));
        		slncpf.setZcontsex(rs.getString("ZCONTSEX"));
        		slncpf.setZcontdob(rs.getInt("ZCONTDOB"));
        		slncpf.setZcontaddr(rs.getString("ZCONTADDR"));
        		slncpf.setLifnamkj(rs.getString("LIFNAMKJ"));
        		slncpf.setOwnnamkj(rs.getString("OWNNAMKJ"));
        		slncpf.setWflga(rs.getString("WFLGA"));
        		slncpf.setZerrflg(rs.getString("ZERRFLG"));
        		slncpf.setZoccdate(rs.getInt("ZOCCDATE"));
        		slncpf.setZtrdate(rs.getInt("ZTRDATE"));
        		slncpf.setZadrddte(rs.getInt("ZADRDDTE"));
        		slncpf.setZexdrddte(rs.getInt("ZEXDRDDTE"));
        		slncpf.setUsrprf(rs.getString("USRPRF"));
        		slncpf.setJobnm(rs.getString("JOBNM"));
            }
        }
        catch (SQLException e) {
            LOGGER.error("readSlncpfData {}", e);
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
		return slncpf;
	}
}
