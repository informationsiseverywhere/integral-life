package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr626screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 5, 18, 24, 15, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sr626screensfl";
		lrec.subfileClass = Sr626screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.endSubfileString = "*MORE";
		lrec.sizeSubfile = 7;
		lrec.pageSubfile = 6;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 14, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr626ScreenVars sv = (Sr626ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr626screenctlWritten, sv.Sr626screensflWritten, av, sv.sr626screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr626ScreenVars screenVars = (Sr626ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.descrip.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.clntname01.setClassString("");
		screenVars.payrnum.setClassString("");
		screenVars.clntname02.setClassString("");
		screenVars.agntnum.setClassString("");
		screenVars.clntname03.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.billfreq.setClassString("");
	}

/**
 * Clear all the variables in Sr626screenctl
 */
	public static void clear(VarModel pv) {
		Sr626ScreenVars screenVars = (Sr626ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.descrip.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.rstate.clear();
		screenVars.pstate.clear();
		screenVars.cownnum.clear();
		screenVars.clntname01.clear();
		screenVars.payrnum.clear();
		screenVars.clntname02.clear();
		screenVars.agntnum.clear();
		screenVars.clntname03.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.mop.clear();
		screenVars.billfreq.clear();
	}
}
