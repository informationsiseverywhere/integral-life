package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6378
 * @version 1.0 generated on 30/08/09 06:53
 * @author Quipoz
 */
public class S6378ScreenVars extends SmartVarModel { 


//	public FixedLengthStringData dataArea = new FixedLengthStringData(546);   
	//public FixedLengthStringData dataFields = new FixedLengthStringData(274).isAPartOf(dataArea, 0);
	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData billcurr = DD.billcurr.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,11);
	public ZonedDecimalData cntfee = DD.cntfee.copyToZonedDecimal().isAPartOf(dataFields,14);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,31);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,34);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,42);
	public ZonedDecimalData exrat = DD.exrat.copyToZonedDecimal().isAPartOf(dataFields,72);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,90);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,107);
	public ZonedDecimalData premCurr = DD.premcurr.copyToZonedDecimal().isAPartOf(dataFields,154);
	public ZonedDecimalData premsusp = DD.premsusp.copyToZonedDecimal().isAPartOf(dataFields,171);
	public ZonedDecimalData prmdepst = DD.prmdepst.copyToZonedDecimal().isAPartOf(dataFields,188);
	public ZonedDecimalData pufee = DD.pufee.copyToZonedDecimal().isAPartOf(dataFields,205);
	public ZonedDecimalData totlprm = DD.totlprm.copyToZonedDecimal().isAPartOf(dataFields,222);
	public FixedLengthStringData taxamts = new FixedLengthStringData(34).isAPartOf(dataFields, 240);
	public ZonedDecimalData[] taxamt = ZDArrayPartOfStructure(2, 17, 2, taxamts, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(34).isAPartOf(taxamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData taxamt01 = DD.taxamt.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData taxamt02 = DD.taxamt.copyToZonedDecimal().isAPartOf(filler,17);
	
	
	//public FixedLengthStringData errorIndicators = new FixedLengthStringData(68).isAPartOf(dataArea, 274); //256  64
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData billcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cntfeeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData exratErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData premcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData premsuspErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData prmdepstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData pufeeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData taxamtsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData[] taxamtErr = FLSArrayPartOfStructure(2, 4, taxamtsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(taxamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData taxamt01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData taxamt02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData totlprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	
//	public FixedLengthStringData outputIndicators = new FixedLengthStringData(204).isAPartOf(dataArea, 342); //192  320
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] billcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cntfeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] exratOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] premcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] premsuspOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] prmdepstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] pufeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData taxamtsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 168);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(2, 12, taxamtsOut, 0);
	public FixedLengthStringData[][] taxamtO = FLSDArrayPartOfArrayStructure(12, 1, taxamtOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(taxamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] taxamt01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] taxamt02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] totlprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);

	/*public FixedLengthStringData subfileArea = new FixedLengthStringData(151);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(37).isAPartOf(subfileArea, 0);*/
	public FixedLengthStringData subfileArea = new FixedLengthStringData(getSubfileAreaSize());
	public FixedLengthStringData subfileFields = new FixedLengthStringData(getSubfileFieldsSize()).isAPartOf(subfileArea, 0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData erordsc = DD.erordescnew.copy().isAPartOf(subfileFields,2);
	public FixedLengthStringData errcde = DD.errcde.copy().isAPartOf(subfileFields,132);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(subfileFields,136);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,138);
	public ZonedDecimalData payrseqno = DD.payrseqno.copyToZonedDecimal().isAPartOf(subfileFields,140);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,141);
	//public FixedLengthStringData errorSubfile = new FixedLengthStringData(28).isAPartOf(subfileArea, 37);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(getErrorSubfileSize()).isAPartOf(subfileArea, getSubfileFieldsSize());
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData erordscErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData errcdeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData payrseqnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	//public FixedLengthStringData outputSubfile = new FixedLengthStringData(84).isAPartOf(subfileArea, 65);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(getOutputSubfileSize()).isAPartOf(subfileArea,getErrorSubfileSize()+getSubfileFieldsSize());
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] erordscOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] errcdeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] payrseqnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	//public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 149);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea,getSubfileFieldsSize()+getErrorSubfileSize()+getOutputSubfileSize());
		/*Indicator Area*/
	//public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
		/*Subfile record no*/
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S6378screensflWritten = new LongData(0);
	public LongData S6378screenctlWritten = new LongData(0);
	public LongData S6378screenWritten = new LongData(0);
	public LongData S6378protectWritten = new LongData(0);
	public GeneralTable s6378screensfl = new GeneralTable(AppVars.getInstance());

	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 20, 21, 22, 23, 24};
	public static int[] affectedInds = new int[] {41}; 
	
	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6378screensfl;
	}

	public S6378ScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	{
		screenIndicArea = DD.indicarea.copy();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(taxamt01Out,new String[] {null, null, null, "41",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamt02Out,new String[] {null, null, null, "41",null, null, null, null, null, null, null, null});
	/*	screenSflFields = new BaseData[] {errcde, erordsc, life, jlife, coverage, rider, payrseqno};
		screenSflOutFields = new BaseData[][] {errcdeOut, erordscOut, lifeOut, jlifeOut, coverageOut, riderOut, payrseqnoOut};
		screenSflErrFields = new BaseData[] {errcdeErr, erordscErr, lifeErr, jlifeErr, coverageErr, riderErr, payrseqnoErr};*/
		
		screenSflFields = getscreenLSflFields();
		screenSflOutFields = getscreenSflOutFields();
		screenSflErrFields = getscreenSflErrFields();
		
		
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};
		
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();


		/*screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cownnum, ownername, cntcurr, billcurr, exrat, instPrem, premCurr, cntfee, pufee, premsusp, prmdepst, taxamt01, taxamt02};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cownnumOut, ownernameOut, cntcurrOut, billcurrOut, exratOut, instprmOut, premcurrOut, cntfeeOut, pufeeOut, premsuspOut, prmdepstOut, taxamt01Out, taxamt02Out};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cownnumErr, ownernameErr, cntcurrErr, billcurrErr, exratErr, instprmErr, premcurrErr, cntfeeErr, pufeeErr, premsuspErr, prmdepstErr, taxamt01Err, taxamt02Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};*/

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6378screen.class;
		screenSflRecord = S6378screensfl.class;
		screenCtlRecord = S6378screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6378protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6378screenctl.lrec.pageSubfile);
	}
	
	public int getDataAreaSize()
	{
		return 546;
	}
	
	public int getDataFieldsSize()
	{
		return 274;
	}
	
	public int getErrorIndicatorSize()
	{
		return 68;
	}

	public int getOutputFieldSize()
	{
		return 204;
	}

	public int getSubfileAreaSize()
	{
		return 358;
	}
	
	public int getSubfileFieldsSize()
	{
		return 143;
	}

	public int getErrorSubfileSize()
	{
		return 28;
	}

	public int getOutputSubfileSize()
	{
		return 84;
	}
	
	public BaseData[] getscreenFields()
	{
		return new BaseData[] {chdrnum, cnttype, ctypedes, cownnum, ownername, cntcurr, billcurr, exrat, instPrem, premCurr, cntfee, pufee, premsusp, prmdepst, taxamt01, taxamt02};
	}
	
	public BaseData[][] getscreenOutFields()
	{
		return new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cownnumOut, ownernameOut, cntcurrOut, billcurrOut, exratOut, instprmOut, premcurrOut, cntfeeOut, pufeeOut, premsuspOut, prmdepstOut, taxamt01Out, taxamt02Out};
	}
	
	public BaseData[] getscreenErrFields()
	{
		return new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cownnumErr, ownernameErr, cntcurrErr, billcurrErr, exratErr, instprmErr, premcurrErr, cntfeeErr, pufeeErr, premsuspErr, prmdepstErr, taxamt01Err, taxamt02Err};
	}
	
	public BaseData[] getscreenLSflFields()
	{
		return new BaseData[] {errcde, erordsc, life, jlife, coverage, rider, payrseqno};
	}
	
	public BaseData[][] getscreenSflOutFields()
	{
		return new BaseData[][] {errcdeOut, erordscOut, lifeOut, jlifeOut, coverageOut, riderOut, payrseqnoOut};
	}
	
	
	public BaseData[] getscreenSflErrFields()
	{
		return new BaseData[] {errcdeErr, erordscErr, lifeErr, jlifeErr, coverageErr, riderErr, payrseqnoErr};
	}
	

	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {};
	}
	

	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {};
	}
	
	
	public BaseData[] getscreenDateErrFields()
	{
		return new BaseData[] {};
	}
	

	public static int[] getScreenSflPfInds()
	{
		return pfInds;
	}
	
	
	public static int[] getScreenSflAffectedInds()
	{
		return affectedInds;
	}

}
