package com.csc.life.newbusiness.dataaccess.model;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
@Entity
@Table(name = "UNLTPF", schema = "VM1DTA")
public class Unltpf implements java.io.Serializable {
	@Id


	private String chdrcoy; 
	private String chdrnum; 
	private String validflag; 
	private String life; 
	private String coverage; 
	private String rider; 
	private int currfrom; 
	private int tranno; 
	private String ualfnd01; 
	private String ualfnd02; 
	private String ualfnd03; 
	private String ualfnd04; 
	private String ualfnd05; 
	private String ualfnd06;
	private String ualfnd07; 
	private String ualfnd08; 
	private String ualfnd09;
	private String ualfnd10; 
	private BigDecimal ualprc01;
	private BigDecimal ualprc02;
	private BigDecimal ualprc03; 
	private BigDecimal ualprc04;
	private BigDecimal ualprc05;
	private BigDecimal ualprc06; 
	private BigDecimal ualprc07;
	private BigDecimal ualprc08;
	private BigDecimal ualprc09; 
	private BigDecimal ualprc10;
	private String udafnd01;
	private String udafnd02;
	private String udafnd03;
	private String udafnd04;
	private String udafnd05;
	private String udafnd06;
	private String udafnd07;
	private String udafnd08; 
	private String udafnd09;
	private String udafnd10;
	private BigDecimal udalpr01;
	private BigDecimal udalpr02;
	private BigDecimal udalpr03;
	private BigDecimal udalpr04;
	private BigDecimal udalpr05;
	private BigDecimal udalpr06;
	private BigDecimal udalpr07;
	private BigDecimal udalpr08;
	private BigDecimal udalpr09;
	private BigDecimal udalpr10;
	private BigDecimal uspcpr01;
	private BigDecimal uspcpr02;
	private BigDecimal uspcpr03;
	private BigDecimal uspcpr04;
	private BigDecimal uspcpr05;
	private BigDecimal uspcpr06;
	private BigDecimal uspcpr07;
	private BigDecimal uspcpr08;
	private BigDecimal uspcpr09;
	private BigDecimal uspcpr10;
	private String prcamtind;
	private int currto;
	private String ptopup;
	private int seqnbr;
	private int numapp;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
	
	public Unltpf(){
	}
	
	public Unltpf(String chdrcoy, String chdrnum, String validflag,String life, String coverage, String rider,  int currfrom, int tranno, String ualfnd01,
			String ualfnd02,String ualfnd03,String ualfnd04,String ualfnd05, String ualfnd06,String ualfnd07,String ualfnd08,
			String ualfnd09,String ualfnd10, BigDecimal ualprc01, BigDecimal ualprc02, BigDecimal ualprc03, BigDecimal ualprc04,
			 BigDecimal ualprc05, BigDecimal ualprc06, BigDecimal ualprc07, BigDecimal ualprc08, BigDecimal ualprc09,
			 BigDecimal ualprc10,String udafnd01,String udafnd02,String udafnd03,String udafnd04,String udafnd05,
			 String udafnd06,String udafnd07,String udafnd08,String udafnd09,String udafnd10,BigDecimal udalpr01,
			 BigDecimal udalpr02,BigDecimal udalpr03,BigDecimal udalpr04,BigDecimal udalpr05,BigDecimal udalpr06,
			 BigDecimal udalpr07,BigDecimal udalpr08,BigDecimal udalpr09,BigDecimal udalpr10,BigDecimal uspcpr01,
			 BigDecimal uspcpr02,BigDecimal uspcpr03,BigDecimal uspcpr04,BigDecimal uspcpr05, BigDecimal uspcpr06,BigDecimal uspcpr07,
			 BigDecimal uspcpr08,BigDecimal uspcpr09,BigDecimal uspcpr10,String prcamtind,int currto,String  ptopup,int seqnbr,int numapp,String usrprf, 
			 String jobnm, Timestamp datime){
	
		this.chdrcoy = chdrcoy; 
		this.chdrnum =chdrnum; 
		this.validflag = validflag; 
		this.life = life; 
		this.coverage = coverage; 
		this.rider = rider; 
		this.currfrom = currfrom; 
		this.tranno = tranno; 
		this.ualfnd01  = ualfnd01; 
		this.ualfnd02 = ualfnd02; 
		this.ualfnd03 = ualfnd03; 
		this.ualfnd04 = ualfnd04; 
		this.ualfnd05 = ualfnd05; 
		this.ualfnd06 = ualfnd06;
		this.ualfnd07 = ualfnd07; 
		this.ualfnd08 = ualfnd08; 
		this.ualfnd09 = ualfnd09;
		this.ualfnd10 = ualfnd10; 
		this.ualprc01 = ualprc01;
		this.ualprc02 = ualprc02;
		this.ualprc03 = ualprc03; 
		this.ualprc04 = ualprc04;
		this.ualprc05 = ualprc05;
		this.ualprc06 = ualprc06; 
		this.ualprc07 = ualprc07;
		this.ualprc08 = ualprc08;
		this.ualprc09 = ualprc09; 
		this.ualprc10 = ualprc10;
		this.udafnd01 = udafnd01;
		this.udafnd02 = udafnd02;
		this.udafnd03 = udafnd03;
		this.udafnd04 = udafnd04;
		this.udafnd05 = udafnd05;
		this.udafnd06 = udafnd06;
		this.udafnd07 = udafnd07;
		this.udafnd08 = udafnd08; 
		this.udafnd09 = udafnd09;
		this.udafnd10 = udafnd10;
		this.udalpr01 = udalpr01;
		this.udalpr02 = udalpr02;
		this.udalpr03 = udalpr03;
		this.udalpr04 = udalpr04;
		this.udalpr05 = udalpr05;
		this.udalpr06 = udalpr06;
		this.udalpr07 = udalpr07;
		this.udalpr08 = udalpr08;
		this.udalpr09 = udalpr09;
		this.udalpr10 = udalpr10;
		this.uspcpr01 = uspcpr01;
		this.uspcpr02 = uspcpr02;
		this.uspcpr03 = uspcpr03;
		this.uspcpr04 = uspcpr04;
		this.uspcpr05 = uspcpr05;
		this.uspcpr06 = uspcpr06;
		this.uspcpr07 = uspcpr07;
		this.uspcpr08 = uspcpr08;
		this.uspcpr09 = uspcpr09;
		this.uspcpr10 = uspcpr10;
		this.prcamtind = prcamtind;
		this.currto = currto;
		this.ptopup = ptopup;
		this.seqnbr = seqnbr;
		this.numapp = numapp;
		this.usrprf = usrprf;
		this.jobnm = jobnm;
		this.datime = new Timestamp(datime.getTime());//IJTI-314
		
	}
		

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	


	
	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}
	
	public int getCurrfrom() {
		return currfrom;
	}

	public void setCurrfrom(int currfrom) {
		this.currfrom = currfrom;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public String getUalfnd01() {
		return ualfnd01;
	}

	public void setUalfnd01(String ualfnd01) {
		this.ualfnd01 = ualfnd01;
	}
	public String getUalfnd02() {
		return ualfnd02;
	}

	public void setUalfnd02(String ualfnd02) {
		this.ualfnd02 = ualfnd02;
	}
	public String getUalfnd03() {
		return ualfnd03;
	}

	public void setUalfnd03(String ualfnd03) {
		this.ualfnd03 = ualfnd03;
	}
	public String getUalfnd04() {
		return ualfnd04;
	}

	public void setUalfnd04(String ualfnd04) {
		this.ualfnd04 = ualfnd04;
	}
	public String getUalfnd05() {
		return ualfnd05;
	}

	public void setUalfnd05(String ualfnd05) {
		this.ualfnd05 = ualfnd05;
	}
	public String getUalfnd06() {
		return ualfnd06;
	}

	public void setUalfnd06(String ualfnd06) {
		this.ualfnd06 = ualfnd06;
	}
	public String getUalfnd07() {
		return ualfnd07;
	}

	public void setUalfnd07(String ualfnd07) {
		this.ualfnd07 = ualfnd07;
	}
	public String getUalfnd08() {
		return ualfnd08;
	}

	public void setUalfnd08(String ualfnd08) {
		this.ualfnd08 = ualfnd08;
	}
	public String getUalfnd09() {
		return ualfnd09;
	}

	public void setUalfnd09(String ualfnd09) {
		this.ualfnd09 = ualfnd09;
	}
	public String getUalfnd10() {
		return ualfnd10;
	}

	public void setUalfnd10(String ualfnd10) {
		this.ualfnd10 = ualfnd10;
	}
	
	public String getUalfnd(BaseData indx) {
		return getUalfnd(indx.toInt());
	}
	
	public BigDecimal getUspcpr(BaseData indx) {
		return getUspcpr(indx.toInt());
	}
	public BigDecimal getUspcpr(int indx) {

		switch (indx) {
			case 1 : return uspcpr01;
			case 2 : return uspcpr02;
			case 3 : return uspcpr03;
			case 4 : return uspcpr04;
			case 5 : return uspcpr05;
			case 6 : return uspcpr06;
			case 7 : return uspcpr07;
			case 8 : return uspcpr08;
			case 9 : return uspcpr09;
			case 10 : return uspcpr10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setUspcpr(BaseData indx, Object what) {
		setUspcpr(indx, what, false);
	}
	public void setUspcpr(BaseData indx, Object what, boolean rounded) {
		setUspcpr(indx.toInt(), what, rounded);
	}
	public void setUspcpr(int indx, Object what) {
		setUspcpr(indx, what, false);
	}
	public void setUspcpr(int indx, Object what, boolean rounded) {

		

		switch (indx) {
			case 1 : setUspcpr01((BigDecimal) what, rounded);
					 break;
			case 2 : setUspcpr02((BigDecimal) what, rounded);
					 break;
			case 3 : setUspcpr03((BigDecimal) what, rounded);
					 break;
			case 4 : setUspcpr04((BigDecimal) what, rounded);
					 break;
			case 5 : setUspcpr05((BigDecimal) what, rounded);
					 break;
			case 6 : setUspcpr06((BigDecimal)what, rounded);
					 break;
			case 7 : setUspcpr07((BigDecimal)what, rounded);
					 break;
			case 8 : setUspcpr08((BigDecimal) what, rounded);
					 break;
			case 9 : setUspcpr09((BigDecimal) what, rounded);
					 break;
			case 10 : setUspcpr10((BigDecimal) what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public BigDecimal getUalprc(BaseData indx) {
		return getUalprc(indx.toInt());
	}
	public BigDecimal getUalprc(int indx) {

		switch (indx) {
			case 1 : return ualprc01;
			case 2 : return ualprc02;
			case 3 : return ualprc03;
			case 4 : return ualprc04;
			case 5 : return ualprc05;
			case 6 : return ualprc06;
			case 7 : return ualprc07;
			case 8 : return ualprc08;
			case 9 : return ualprc09;
			case 10 : return ualprc10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setUalprc(BaseData indx, Object what) {
		setUalprc(indx, what, false);
	}
	public void setUalprc(BaseData indx, Object what, boolean rounded) {
		setUalprc(indx.toInt(), what, rounded);
	}
	public void setUalprc(int indx, Object what) {
		setUalprc(indx, what, false);
	}
	public void setUalprc(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setUalprc01((BigDecimal) what, rounded);
					 break;
			case 2 : setUalprc02((BigDecimal) what, rounded);
					 break;
			case 3 : setUalprc03((BigDecimal) what, rounded);
					 break;
			case 4 : setUalprc04((BigDecimal) what, rounded);
					 break;
			case 5 : setUalprc05((BigDecimal) what, rounded);
					 break;
			case 6 : setUalprc06((BigDecimal) what, rounded);
					 break;
			case 7 : setUalprc07((BigDecimal) what, rounded);
					 break;
			case 8 : setUalprc08((BigDecimal) what, rounded);
					 break;
			case 9 : setUalprc09((BigDecimal) what, rounded);
					 break;
			case 10 : setUalprc10((BigDecimal) what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	
	public String getUalfnd(int indx) {

		switch (indx) {
			case 1 : return ualfnd01;
			case 2 : return ualfnd02;
			case 3 : return ualfnd03;
			case 4 : return ualfnd04;
			case 5 : return ualfnd05;
			case 6 : return ualfnd06;
			case 7 : return ualfnd07;
			case 8 : return ualfnd08;
			case 9 : return ualfnd09;
			case 10 : return ualfnd10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setUalfnd(BaseData indx, String what) {
		setUalfnd(indx.toInt(), what);
	}
	public void setUalfnd(int indx, String what) {

		switch (indx) {
			case 1 : setUalfnd01(what);
					 break;
			case 2 : setUalfnd02(what);
					 break;
			case 3 : setUalfnd03(what);
					 break;
			case 4 : setUalfnd04(what);
					 break;
			case 5 : setUalfnd05(what);
					 break;
			case 6 : setUalfnd06(what);
					 break;
			case 7 : setUalfnd07(what);
					 break;
			case 8 : setUalfnd08(what);
					 break;
			case 9 : setUalfnd09(what);
					 break;
			case 10 : setUalfnd10(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}
	
	public BigDecimal getUalprc01() {
		return ualprc01;
	}

	public void setUalprc01(BigDecimal ualprc01) {
		this.ualprc01 = ualprc01;
	}
	public BigDecimal getUalprc02() {
		return ualprc02;
	}

	public void setUalprc02(BigDecimal ualprc02) {
		this.ualprc02 = ualprc02;
	}

	public BigDecimal getUalprc03() {
		return ualprc03;
	}

	public void setUalprc03(BigDecimal ualprc03) {
		this.ualprc03 = ualprc03;
	}

	public BigDecimal getUalprc04() {
		return ualprc04;
	}

	public void setUalprc04(BigDecimal ualprc04) {
		this.ualprc04 = ualprc04;
	}

	public BigDecimal getUalprc05() {
		return ualprc05;
	}

	public void setUalprc05(BigDecimal ualprc05) {
		this.ualprc05 = ualprc05;
	}

	public BigDecimal getUalprc06() {
		return ualprc06;
	}

	public void setUalprc06(BigDecimal ualprc06) {
		this.ualprc06 = ualprc06;
	}
	public BigDecimal getUalprc07() {
		return ualprc07;
	}

	public void setUalprc07(BigDecimal ualprc07) {
		this.ualprc07 = ualprc07;
	}

	public BigDecimal getUalprc08() {
		return ualprc08;
	}

	public void setUalprc08(BigDecimal ualprc08) {
		this.ualprc08 = ualprc08;
	}

	public BigDecimal getUalprc09() {
		return ualprc09;
	}

	public void setUalprc09(BigDecimal ualprc09) {
		this.ualprc09 = ualprc09;
	}

	public BigDecimal getUalprc10() {
		return ualprc10;
	}

	public void setUalprc10(BigDecimal ualprc10) {
		this.ualprc10 = ualprc10;
	}


	public String getUdafnd01() {
		return udafnd01;
	}

	public void setUdafnd01(String udafnd01) {
		this.udafnd01 = udafnd01;
	}

	public String getUdafnd02() {
		return udafnd02;
	}

	public void setUdafnd02(String udafnd02) {
		this.udafnd02 = udafnd02;
	}

	public String getUdafnd03() {
		return udafnd03;
	}

	public void setUdafnd03(String udafnd03) {
		this.udafnd03 = udafnd03;
	}

	public String getUdafnd04() {
		return udafnd04;
	}

	public void setUdafnd04(String udafnd04) {
		this.udafnd04 = udafnd04;
	}

	public String getUdafnd05() {
		return udafnd05;
	}

	public void setUdafnd05(String udafnd05) {
		this.udafnd05 = udafnd05;
	}

	public String getUdafnd06() {
		return udafnd06;
	}

	public void setUdafnd06(String udafnd06) {
		this.udafnd06 = udafnd06;
	}

	public String getUdafnd07() {
		return udafnd07;
	}

	public void setUdafnd07(String udafnd07) {
		this.udafnd07 = udafnd07;
	}

	public String getUdafnd08() {
		return udafnd08;
	}

	public void setUdafnd08(String udafnd08) {
		this.udafnd08 = udafnd08;
	}

	public String getUdafnd09() {
		return udafnd09;
	}

	public void setUdafnd09(String udafnd09) {
		this.udafnd09 = udafnd09;
	}

	public String getUdafnd10() {
		return udafnd10;
	}

	public void setUdafnd10(String udafnd10) {
		this.udafnd10 = udafnd10;
	}
	

	public BigDecimal getUdalpr01() {
		return udalpr01;
	}

	public void setUdalpr01(BigDecimal udalpr01) {
		this.udalpr01 = udalpr01;
	}

	public BigDecimal getUdalpr02() {
		return udalpr02;
	}

	public void setUdalpr02(BigDecimal udalpr02) {
		this.udalpr02 = udalpr02;
	}

	public BigDecimal getUdalpr03() {
		return udalpr03;
	}

	public void setUdalpr03(BigDecimal udalpr03) {
		this.udalpr03 = udalpr03;
	}

	public BigDecimal getUdalpr04() {
		return udalpr04;
	}

	public void setUdalpr04(BigDecimal udalpr04) {
		this.udalpr04 = udalpr04;
	}
	public BigDecimal getUdalpr05() {
		return udalpr05;
	}

	public void setUdalpr05(BigDecimal udalpr05) {
		this.udalpr05 = udalpr05;
	}

	public BigDecimal getUdalpr06() {
		return udalpr06;
	}

	public void setUdalpr06(BigDecimal udalpr06) {
		this.udalpr06 = udalpr06;
	}

	public BigDecimal getUdalpr07() {
		return udalpr07;
	}

	public void setUdalpr07(BigDecimal udalpr07) {
		this.udalpr07 = udalpr07;
	}

	public BigDecimal getUdalpr08() {
		return udalpr08;
	}

	public void setUdalpr08(BigDecimal udalpr08) {
		this.udalpr08 = udalpr08;
	}

	public BigDecimal getUdalpr09() {
		return udalpr09;
	}

	public void setUdalpr09(BigDecimal udalpr09) {
		this.udalpr09 = udalpr09;
	}

	public BigDecimal getUdalpr10() {
		return udalpr10;
	}

	public void setUdalpr10(BigDecimal udalpr10) {
		this.udalpr10 = udalpr10;
	}

	public BigDecimal getUspcpr01() {
		return uspcpr01;
	}

	public void setUspcpr01(BigDecimal uspcpr01) {
		this.uspcpr01 = uspcpr01;
	}

	public BigDecimal getUspcpr02() {
		return uspcpr02;
	}

	public void setUspcpr02(BigDecimal uspcpr02) {
		this.uspcpr02 = uspcpr02;
	}

	public BigDecimal getUspcpr03() {
		return uspcpr03;
	}

	public void setUspcpr03(BigDecimal uspcpr03) {
		this.uspcpr03 = uspcpr03;
	}

	public BigDecimal getUspcpr04() {
		return uspcpr04;
	}

	public void setUspcpr04(BigDecimal uspcpr04) {
		this.uspcpr04 = uspcpr04;
	}

	public BigDecimal getUspcpr05() {
		return uspcpr05;
	}

	public void setUspcpr05(BigDecimal uspcpr05) {
		this.uspcpr05 = uspcpr05;
	}

	public BigDecimal getUspcpr06() {
		return uspcpr06;
	}

	public void setUspcpr06(BigDecimal uspcpr06) {
		this.uspcpr06 = uspcpr06;
	}

	public BigDecimal getUspcpr07() {
		return uspcpr07;
	}

	public void setUspcpr07(BigDecimal uspcpr07) {
		this.uspcpr06 = uspcpr07;
	}

	public BigDecimal getUspcpr08() {
		return uspcpr08;
	}

	public void setUspcpr08(BigDecimal uspcpr08) {
		this.uspcpr08 = uspcpr08;
	}

	public BigDecimal getUspcpr09() {
		return uspcpr09;
	}

	public void setUspcpr09(BigDecimal uspcpr09) {
		this.uspcpr09 = uspcpr09;
	}

	public BigDecimal getUspcpr10() {
		return uspcpr10;
	}

	public void setUspcpr10(BigDecimal uspcpr10) {
		this.uspcpr10 = uspcpr10;
	}


	public String getPrcamtind() {
		return prcamtind;
	}

	public void setPrcamtind(String prcamtind) {
		this.prcamtind = prcamtind;
	}
	
	public int getCurrto() {
		return currto;
	}

	public void setCurrto(int currto) {
		this.currto = currto;
	}
	public String getPtopup() {
		return ptopup;
	}

	public void setPtopup(String ptopup) {
		this.ptopup = ptopup;
	}

	public int getSeqnbr() {
		return seqnbr;
	}

	public void setSeqnbr(int seqnbr) {
		this.seqnbr = seqnbr;
	}

	
	public int getNumapp() {
		return numapp;
	}

	public void setNumapp(int numapp) {
		this.numapp = numapp;
	}
	
	
	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public Timestamp getDatime() {
		return new Timestamp(datime.getTime());//IJTI-316
	}

	public void setDatime(Timestamp datime) {
		this.datime = new Timestamp(datime.getTime());//IJTI-314
	}

	public void setUalprc01(BigDecimal what, boolean rounded) {
		if (rounded)
			this.ualprc01 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.ualprc01 = what;
	}	
	public void setUalprc02(BigDecimal what, boolean rounded) {
		if (rounded)
			ualprc02 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.ualprc02 = what;
	}	
	public void setUalprc03(BigDecimal what, boolean rounded) {
		if (rounded)
			ualprc03 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.ualprc03 = what;
	}
	public void setUalprc04(BigDecimal what, boolean rounded) {
		if (rounded)
			ualprc04 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.ualprc04 = what;;
	}
	public void setUalprc05(BigDecimal what, boolean rounded) {
		if (rounded)
			ualprc05 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.ualprc05 = what;
	}
	public void setUalprc06(BigDecimal what, boolean rounded) {
		if (rounded)
			ualprc06 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.ualprc06 = what;
	}public void setUalprc07(BigDecimal what, boolean rounded) {
		if (rounded)
			ualprc07 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.ualprc07 = what;
	}public void setUalprc08(BigDecimal what, boolean rounded) {
		if (rounded)
			ualprc08 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.ualprc08 = what;
	}public void setUalprc09(BigDecimal what, boolean rounded) {
		if (rounded)
			ualprc09 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.ualprc09 = what;
	}public void setUalprc10(BigDecimal what, boolean rounded) {
		if (rounded)
			ualprc10 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.ualprc10 = what;
	}
	
	
	public void setUspcpr01(BigDecimal what, boolean rounded) {
		if (rounded)
			uspcpr01 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.uspcpr01 = what;
	}	
	public void setUspcpr02(BigDecimal what, boolean rounded) {
		if (rounded)
			uspcpr02 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.uspcpr02 = what;
	}	
	public void setUspcpr03(BigDecimal what, boolean rounded) {
		if (rounded)
			uspcpr03 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.uspcpr03 = what;
	}	
	public void setUspcpr04(BigDecimal what, boolean rounded) {
		if (rounded)
			uspcpr04 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.uspcpr04 = what;
	}	
	public void setUspcpr05(BigDecimal what, boolean rounded) {
		if (rounded)
			uspcpr05 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.uspcpr05 = what;
	}	
	public void setUspcpr06(BigDecimal what, boolean rounded) {
		if (rounded)
			uspcpr06 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.uspcpr06 = what;
	}	
	public void setUspcpr07(BigDecimal what, boolean rounded) {
		if (rounded)
			uspcpr07 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.uspcpr07 = what;
	}	
	public void setUspcpr08(BigDecimal what, boolean rounded) {
		if (rounded)
			uspcpr08 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.uspcpr08 = what;
	}	
	public void setUspcpr09(BigDecimal what, boolean rounded) {
		if (rounded)
			uspcpr09 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.uspcpr09 = what;
	}
	public void setUspcpr10(BigDecimal what, boolean rounded) {
		if (rounded)
			uspcpr10 =  what.setScale(-1, BigDecimal.ROUND_HALF_UP);
		else
			this.uspcpr10 = what;
	}
	
	
	
	public String toString() {

		return "Unltpf info::: " + 
		chdrcoy+":"+
		chdrnum+":"+
		validflag+":"+
		life+":"+
		coverage+":"+
        rider+":"+
        currfrom+":"+
        tranno+":"+
        ualfnd01+":"+
        ualfnd02+":"+
        ualfnd03+":"+
        ualfnd04+":"+
        ualfnd05+":"+
        ualfnd06+":"+
        ualfnd07+":"+
        ualfnd08+":"+
        ualfnd09+":"+
        ualfnd10+":"+
        ualprc01+":"+
        ualprc02+":"+
        ualprc03+":"+
        ualprc04+":"+
        ualprc05+":"+
        ualprc06+":"+
        ualprc07+":"+
        ualprc08+":"+
        ualprc09+":"+
        ualprc10+":"+
        udafnd01+":"+
        udafnd02+":"+
        udafnd03+":"+
        udafnd04+":"+
        udafnd05+":"+
        udafnd06+":"+
        udafnd07+":"+
        udafnd08+":"+
        udafnd09+":"+
        udafnd10+":"+
        udalpr01+":"+
        udalpr02+":"+
        udalpr03+":"+
        udalpr04+":"+
        udalpr05+":"+
        udalpr06+":"+
        udalpr07+":"+
        udalpr08+":"+
        udalpr09+":"+
        udalpr10+":"+
        uspcpr01+":"+
        uspcpr02+":"+
        uspcpr03+":"+
        uspcpr04+":"+
        uspcpr05+":"+
        uspcpr06+":"+
        uspcpr07+":"+
        uspcpr08+":"+
        uspcpr09+":"+
        uspcpr10+":"+
        prcamtind+":"+
        currto+":"+
        ptopup+":"+
        seqnbr+":"+
        numapp+":"+
        usrprf+":"+
        jobnm+":" +
        datime;

	}
	
	
	


}
