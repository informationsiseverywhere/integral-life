package com.csc.life.newbusiness.dataaccess;

import com.csc.fsu.general.dataaccess.LetcpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: LetclblTableDAM.java
 * Date: Sun, 30 Aug 2009 03:42:13
 * Class transformed from LETCLBL.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LetclblTableDAM extends LetcpfTableDAM {

	public LetclblTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("LETCLBL");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "REQCOY"
		             + ", LETTYPE"
		             + ", LETSEQNO"
		             + ", CLNTCOY"
		             + ", CLNTNUM";
		
		QUALIFIEDCOLUMNS = 
		            "REQCOY, " +
		            "LETTYPE, " +
		            "LETSEQNO, " +
		            "CLNTCOY, " +
		            "CLNTNUM, " +
		            "LETSTAT, " +
		            "RDOCPFX, " +
		            "RDOCCOY, " +
		            "RDOCNUM, " +
		            "LETOKEY, " +
		            "HSUBLET, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "REQCOY ASC, " +
		            "LETTYPE ASC, " +
		            "LETSEQNO ASC, " +
		            "CLNTCOY ASC, " +
		            "CLNTNUM ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "REQCOY DESC, " +
		            "LETTYPE DESC, " +
		            "LETSEQNO DESC, " +
		            "CLNTCOY DESC, " +
		            "CLNTNUM DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               requestCompany,
                               letterType,
                               letterSeqno,
                               clntcoy,
                               clntnum,
                               letterStatus,
                               rdocpfx,
                               rdoccoy,
                               rdocnum,
                               otherKeys,
                               hsublet,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(234);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getLetcrecKeyData() {
		return getRecKeyData();
	}
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getRequestCompany().toInternal()
					+ getLetterType().toInternal()
					+ getLetterSeqno().toInternal()
					+ getClntcoy().toInternal()
					+ getClntnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setLetcrecKeyData(Object obj) {
		return setRecKeyData(obj);
	}
	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, requestCompany);
			what = ExternalData.chop(what, letterType);
			what = ExternalData.chop(what, letterSeqno);
			what = ExternalData.chop(what, clntcoy);
			what = ExternalData.chop(what, clntnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(requestCompany.toInternal());
	nonKeyFiller20.setInternal(letterType.toInternal());
	nonKeyFiller30.setInternal(letterSeqno.toInternal());
	nonKeyFiller40.setInternal(clntcoy.toInternal());
	nonKeyFiller50.setInternal(clntnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getLetcrecNonKeyData() {
		return getRecNonKeyData();
	}
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(189);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ getLetterStatus().toInternal()
					+ getRdocpfx().toInternal()
					+ getRdoccoy().toInternal()
					+ getRdocnum().toInternal()
					+ getOtherKeys().toInternal()
					+ getHsublet().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setLetcrecNonKeyData(Object obj) {
		return setRecNonKeyData(obj);
	}
	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, letterStatus);
			what = ExternalData.chop(what, rdocpfx);
			what = ExternalData.chop(what, rdoccoy);
			what = ExternalData.chop(what, rdocnum);
			what = ExternalData.chop(what, otherKeys);
			what = ExternalData.chop(what, hsublet);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getRequestCompany() {
		return requestCompany;
	}
	public void setRequestCompany(Object what) {
		requestCompany.set(what);
	}
	public FixedLengthStringData getLetterType() {
		return letterType;
	}
	public void setLetterType(Object what) {
		letterType.set(what);
	}
	public PackedDecimalData getLetterSeqno() {
		return letterSeqno;
	}
	public void setLetterSeqno(Object what) {
		setLetterSeqno(what, false);
	}
	public void setLetterSeqno(Object what, boolean rounded) {
		if (rounded)
			letterSeqno.setRounded(what);
		else
			letterSeqno.set(what);
	}
	public FixedLengthStringData getClntcoy() {
		return clntcoy;
	}
	public void setClntcoy(Object what) {
		clntcoy.set(what);
	}
	public FixedLengthStringData getClntnum() {
		return clntnum;
	}
	public void setClntnum(Object what) {
		clntnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getLetterStatus() {
		return letterStatus;
	}
	public void setLetterStatus(Object what) {
		letterStatus.set(what);
	}	
	public FixedLengthStringData getRdocpfx() {
		return rdocpfx;
	}
	public void setRdocpfx(Object what) {
		rdocpfx.set(what);
	}	
	public FixedLengthStringData getRdoccoy() {
		return rdoccoy;
	}
	public void setRdoccoy(Object what) {
		rdoccoy.set(what);
	}	
	public FixedLengthStringData getRdocnum() {
		return rdocnum;
	}
	public void setRdocnum(Object what) {
		rdocnum.set(what);
	}	
	public FixedLengthStringData getOtherKeys() {
		return otherKeys;
	}
	public void setOtherKeys(Object what) {
		otherKeys.set(what);
	}	
	public FixedLengthStringData getHsublet() {
		return hsublet;
	}
	public void setHsublet(Object what) {
		hsublet.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		requestCompany.clear();
		letterType.clear();
		letterSeqno.clear();
		clntcoy.clear();
		clntnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		letterStatus.clear();
		rdocpfx.clear();
		rdoccoy.clear();
		rdocnum.clear();
		otherKeys.clear();
		hsublet.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}