/*
 * File: P5011.java
 * Date: 29 August 2009 23:54:46
 * Author: Quipoz Limited
 * 
 * Class transformed from P5011.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import com.csc.fsu.clients.dataaccess.dao.ClftpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clftpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Clntrlsrec;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ClrrpfDAO;
import com.csc.fsu.general.dataaccess.model.Clrrpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.dao.FupepfDAO;
import com.csc.life.contractservicing.dataaccess.model.Fupepf;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.FluplnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.HxclpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.newbusiness.dataaccess.model.Hxclpf;
import com.csc.life.newbusiness.screens.S5011ScreenVars;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
* Retrieve the contract header information from the CHDRLNB I/O
* module.
* Read the clients name.
*
* Do the following :
*    - If ROLU -> * First page = 'N'
*                 * Start with the last processed follow-up
*    - If ROLD -> * Read the first follow-up of current screen
*                 * Read back SUPFILE-PAGE + 1 times
*                 * Check if first page
*                 * Start with current follow-up
*    - ELSE       * First page = 'Y'
*                 * Start with first follow-up
*
* Fill the subfile page with follow-up records for the contract.
* If the page is not full -> add blank lines to the subfile in
* order to display a full page.
*
* Set SCRN-SUBFILE-MORE to Y or N to display a '+' if there are
* more follow-ups to show.
*
* Retrieve todays date (DATCON1) and store for later use.
*
*
* Validation
* ----------
*
* Validate the changed fields in the subfile page
*
* If ROLD and First page
*    - Error ## roldown not allowed ##
* If ROLU
*    - If any new blank subfile lines
*         - Error ## rolup not allowed ##
*
* Update
* ------
*
* Delete/write/rewrite follow-ups
*
* Store the first follow-up key of current screen ## for ROLU ##
*
* Add Validation to  presence of follow up letter type for
* printing of either standard follow up letter or a reminder
*
* Add the option of supplying Doctor details when entering
* Follow Ups.
*
* When a Doctor is required, output a warning message, but
* enable user to exit without aborting.
*****************************************************************
* </pre>
*/
public class P5011 extends ScreenProgCS {
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	public int ncount;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5011");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private ZonedDecimalData wsaaRrn = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaOptionCode = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFoundSelection = new FixedLengthStringData(1);
	private Validator foundSelection = new Validator(wsaaFoundSelection, "Y");
	private PackedDecimalData ix = new PackedDecimalData(2, 0).init(0);
	private ZonedDecimalData iy = new ZonedDecimalData(2, 0);
	private FixedLengthStringData wsaaUpdchdr = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaDocseq = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
		/* TABLES */
	private static final String t5688 = "T5688";
	private static final String t5661 = "T5661";
	private static final String t5660 = "T5660";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private FluplnbTableDAM fluplnbIO = new FluplnbTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Clntrlsrec clntrlsrec = new Clntrlsrec();
	private T5661rec t5661rec = new T5661rec();
	private Optswchrec optswchrec = new Optswchrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5011ScreenVars sv = getPScreenVars();//ScreenProgram.getScreenVars( S5011ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaUserFieldsInner wsaaUserFieldsInner = new WsaaUserFieldsInner();
	private boolean isFatcaAllowed=false;//IFSU-1148
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private ClftpfDAO clftpfDAO = getApplicationContext().getBean("clftpfDAO", ClftpfDAO.class);
	private ChdrpfDAO chdrpfDAO=getApplicationContext().getBean("chdrDAO", ChdrpfDAO.class);
	private LinkedHashSet<String> fupstsList = new LinkedHashSet<String>();
	private static final String formTypeW9 = "W9      ";
	private static final String formTypeW8 = "W8-BEN  ";
	private static final String fatcastsUSP = "USP";
	private static final String fatcastsNUSWI = "NUSWI";
	private static final String fatcastsNUSWOI = "NUSWOI";
	private Descpf descpf;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private Clntpf clntpf;
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	protected HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class); 
  	private Hpadpf hpadpf;
  	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
  	private List<Itempf> itempfList;
  	private HxclpfDAO hxclpfDAO = getApplicationContext().getBean("hxclpfDAO", HxclpfDAO.class); 
  	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
  	private FupepfDAO fupepfDAO = getApplicationContext().getBean("fupepfDAO", FupepfDAO.class); 
  	private List<Fupepf> fupeList;
  	private ArrayList<Fluppf> fluppfList = new ArrayList<>();  
  	private Fluppf fluppf;
  	private ClrrpfDAO clrrpfDAO = getApplicationContext().getBean("clrrpfDAO", ClrrpfDAO.class);
  	//IBPLIFE-5299 Start
  	private boolean cnmtn056Permission;
  	//IBPLIFE-5299 End
  	
  	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkStatuz2070, 
		continue2620, 
		updateErrorIndicators2670, 
		switch4170
	}

	public P5011() {
		super();
		screenVars = sv;
		new ScreenModel("S5011", AppVars.getInstance(), sv);
	}
	protected S5011ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( S5011ScreenVars.class); 
	}
	
	

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

	protected void plainname() {
		/* PLAIN-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();

		} else if (isNE(clntpf.getGivname(), SPACES)) {
			String firstName = clntpf.getGivname();/* IJTI-1523 */
			String lastName = clntpf.getSurname();/* IJTI-1523 */
			String delimiter = ",";

			String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
			wsspcomn.longconfname.set(fullName);

		} else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/* PLAIN-EXIT */
	}

	protected void corpname() {
		/* PAYEE-1001 */
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME DELIMITED SIZE */
		/* CLTS-GIVNAME DELIMITED ' ' */
		String firstName = clntpf.getLgivname();/* IJTI-1523 */
		String lastName = clntpf.getLsurname();/* IJTI-1523 */
		String delimiter = "";

		// this way we can override StringUtil behaviour in formatName()
		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);
		/* CORP-EXIT */
	}


	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
			initialise1010();
		}

protected void initialise1010()
	{
		if (wsspcomn.secActn[wsspcomn.programPtr.toInt()].equals("*")) {
			scrnparams.subfileRrn.set(wsaaRrn);
			scrnparams.function.set(varcom.sread);
			processScreen("S5011", sv);
			wsaaUserFieldsInner.wsaaFupno.set(sv.fupno);
			checkHxcl1400();
			scrnparams.function.set(varcom.supd);
			processScreen("S5011", sv);
			return ;
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.language.set(wsspcomn.language);
		sv.dataArea.set(" ");
		sv.subfileArea.set(" ");
		wsaaOptionCode.set(" ");
		wsaaFoundSelection.set("N");
		/* Dummy subfile initalisation for prototype - relpace with SCLR*/
		scrnparams.function.set(varcom.sclr);
		processScreen("S5011", sv);
		if (!scrnparams.statuz.equals(varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/* Dummy field initilisation for prototype version.*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (!chdrlnbIO.getStatuz().equals(varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		sv.cnttype.set(chdrlnbIO.getCnttype());
		sv.cownnum.set(chdrlnbIO.getCownnum());
		/* Read the contract definition description from table*/
		/* T5688 for the contract held on the client header record.*/
		descpf=descDAO.getdescData("IT", t5688, chdrlnbIO.getCnttype().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf==null) {
			/*       MOVE ALL '?'             TO S5011-FUPREMK                 */
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		clntpf=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), chdrlnbIO.getCownnum().toString());
		
		/* Get the confirmation name.*/
		if (clntpf==null
		|| !clntpf.getValidflag().equals("1")) {
			sv.ownernameErr.set(errorsInner.e040);
			sv.ownername.set(" ");
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		/* Retrieve Doctor details, if present on HPAD file.               */
		wsaaUserFieldsInner.wsaaFirstDisplay.set("Y");
		hpadpf=hpadpfDAO.getHpadData(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		
		
		/* Get the Doctor confirmation name.                               */
		if (hpadpf.getZdoctor()!=null && !hpadpf.getZdoctor().trim().equals("")) {
			sv.zdoctor.set(hpadpf.getZdoctor());
			clntpf=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), hpadpf.getZdoctor());
			if (clntpf==null) {
				sv.zdoctorErr.set(errorsInner.e040);
			}
			else {
				namadrsrec.namadrsRec.set(" ");
				namadrsrec.clntPrefix.set("CN");
				namadrsrec.clntCompany.set(wsspcomn.fsuco);
				namadrsrec.clntNumber.set(hpadpf.getZdoctor());
				namadrsrec.language.set(wsspcomn.language);
				namadrsrec.function.set(wsaaUserFieldsInner.wsaaPayeeName);
				callProgram(Namadrs.class, namadrsrec.namadrsRec);
				if (!namadrsrec.statuz.equals(varcom.oK)) {
					syserrrec.statuz.set(namadrsrec.statuz);
					fatalError600();
				}
				sv.zdocname.set(namadrsrec.name);
			}
		}
		
		/* Set follow-up record to the start position*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (wsaaUserFieldsInner.wsaaRoluRold.equals (varcom.rold)) {
			fluplnbIO.setRecKeyData(wsaaUserFieldsInner.wsaaFirstFlupKey);
		}
		else {
			if (!wsaaUserFieldsInner.wsaaRoluRold.equals(varcom.rolu)) {
				fluplnbIO.setParams(" ");
				fluplnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
				fluplnbIO.setChdrnum(chdrlnbIO.getChdrnum());
				fluplnbIO.setFupno(0);
			}
		}
		wsaaUserFieldsInner.wsaaFirstPage.set("Y");
		fluplnbIO.setFunction(varcom.begn);
		if (wsaaUserFieldsInner.wsaaRoluRold.equals(varcom.rolu)) {
			fluplnbIO.setFupno(fluplnbIO.getFupno().toInt()+1);
			wsaaUserFieldsInner.wsaaFirstPage.set("N");
		}
		fluplnbIO.setFormat(formatsInner.fluplnbrec);
		SmartFileCode.execute(appVars, fluplnbIO);
		/* Check for I/O error. Note that the ENDP condition is tested*/
		/* for later.*/
		if (!fluplnbIO.getStatuz().equals(varcom.oK)
		&& !fluplnbIO.getStatuz().equals(varcom.endp)) {
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
		/* Go one page back.*/
		if (wsaaUserFieldsInner.wsaaRoluRold.equals(varcom.rold)) {
			resetTopRecord1100();
		}
		/* Load subfile, starting with current follow-up*/
		wsaaUserFieldsInner.wsaaCount.set(0);
		while ( !(wsaaUserFieldsInner.wsaaCount.toInt()==sv.subfilePage.toInt())) {
			loadSubfile1300();
		}
		
		if (fluplnbIO.getStatuz().equals(varcom.endp)
		|| !fluplnbIO.getChdrcoy().equals(chdrlnbIO.getChdrcoy())
		|| !fluplnbIO.getChdrnum().equals(chdrlnbIO.getChdrnum())) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* Reset WSAA-ROLU-ROLD here because if F3, F15, F16,F17 or F18 is*/
		/* pressed in the subfile-screen WSAA-ROLU-ROLD is not set to the*/
		/* SCRN-STATUZ.*/
		wsaaUserFieldsInner.wsaaRoluRold.set(varcom.oK);
		/* Intialise Call to OPTSWCH.                                      */
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(0);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (!optswchrec.optsStatuz.equals(varcom.oK)) {
			syserrrec.params.set(optswchrec.rec);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			fatalError600();
		}
		
		isFatcaAllowed= FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "CLMTN008", appVars, "IT");
		cnmtn056Permission = FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "CNMTN056",appVars, smtpfxcpy.item.toString());
	}

protected void resetTopRecord1100()
	{
		resetTop1110();
	}

protected void resetTop1110()
	{
		/* If ENDP -> start with last follow-up for contract*/
		/* ELSE    -> read-prev from current follow-up.*/
		if (fluplnbIO.getStatuz().equals(varcom.endp)) {
			fluplnbIO.setFunction(varcom.endr);
			fluplnbIO.setStatuz(varcom.oK);
		}
		else {
			fluplnbIO.setFunction(varcom.nextp);
		}
		/* Read back #records in one page + 1.*/
		for (wsaaUserFieldsInner.wsaaCount.set(1); !((wsaaUserFieldsInner.wsaaCount.toInt()>sv.subfilePage.toInt()+1)
		|| fluplnbIO.getStatuz().equals(varcom.endp)); wsaaUserFieldsInner.wsaaCount.add(1)){
			readPrevRecord1200();
		}
		/* See if this is the first page and reset the top record for this*/
		/* page.*/
		if (fluplnbIO.getStatuz().equals(varcom.endp)
		|| !fluplnbIO.getChdrcoy().equals(chdrlnbIO.getChdrcoy())
		|| !fluplnbIO.getChdrnum().equals(chdrlnbIO.getChdrnum())) {
			wsaaUserFieldsInner.wsaaFirstPage.set("Y");
			fluplnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
			fluplnbIO.setChdrnum(chdrlnbIO.getChdrnum());
			fluplnbIO.setFupno(0);
			fluplnbIO.setFunction(varcom.begn);
			fluplnbIO.setFormat(formatsInner.fluplnbrec);
			SmartFileCode.execute(appVars, fluplnbIO);
			/*   Check for I/O error. Note that the ENDP*/
			/*   condition is tested for later.*/
			if (!fluplnbIO.getStatuz().equals(varcom.oK)
			&& !fluplnbIO.getStatuz().equals(varcom.endp)) {
				syserrrec.params.set(fluplnbIO.getParams());
				fatalError600();
			}
		}
		else {
			if ( wsaaUserFieldsInner.wsaaCount.toInt()>sv.subfilePage.toInt()+1) {
				wsaaUserFieldsInner.wsaaFirstPage.set("N");
				fluplnbIO.setFunction(varcom.nextr);
				fluplnbIO.setFormat(formatsInner.fluplnbrec);
				SmartFileCode.execute(appVars, fluplnbIO);
				/*     Check for I/O error. Note that the ENDP*/
				/*     condition is tested for later.*/
				if (!fluplnbIO.getStatuz().equals(varcom.oK)
				&& !fluplnbIO.getStatuz().equals(varcom.endp)) {
					syserrrec.params.set(fluplnbIO.getParams());
					fatalError600();
				}
			}
		}
	}

protected void readPrevRecord1200()
	{
		/*READ-PREV*/
		fluplnbIO.setFormat(formatsInner.fluplnbrec);
		SmartFileCode.execute(appVars, fluplnbIO);
		/* Check for I/O error. Note that the ENDP condition is tested*/
		/* for later.*/
		if (!fluplnbIO.getStatuz().equals(varcom.oK)
		&& !fluplnbIO.getStatuz().equals(varcom.endp)) {
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
		fluplnbIO.setFunction(varcom.nextp);
		/*EXIT*/
	}

protected void loadSubfile1300()
	{
					load1310();
					addSubfileRecord1320();
				}

protected void load1310()
	{
		wsaaUserFieldsInner.wsaaCount.add(1);
		/* Record belong to this contract or ENDP ? If not, empty line*/
		/* on in the subfile page.*/
		if (!fluplnbIO.getChdrcoy().equals(chdrlnbIO.getChdrcoy())
		|| !fluplnbIO.getChdrnum().equals(chdrlnbIO.getChdrnum())
		|| fluplnbIO.getStatuz().equals(varcom.endp)) {
			sv.subfileFields.set(" ");
			sv.fupremdt.set(99999999);
			sv.lifeno.set(0);
			sv.jlife.set(" ");
			wsaaUserFieldsInner.wsaaFupno.add(1);
			sv.fupno.set(wsaaUserFieldsInner.wsaaFupno);
			sv.zitem.set(" ");
			sv.indic.set(" ");
			sv.crtuser.set(wsspcomn.userid);
			sv.language.set(wsspcomn.language);
			sv.crtdate.set(99999999);/*ILIFE-4455*/
			sv.crtdateOut[varcom.nd.toInt()].set("Y");
			sv.fuptype.set(" ");/*ILIFE-4455*/
			sv.fuptypOut[varcom.nd.toInt()].set("Y");
			sv.fuprcvd.set(99999999);
			sv.exprdate.set(99999999);
			return ;
		}
		/* Record belongs to this contract, put details in the current*/
		/* line of the subfile page.*/
		/*    MOVE FLUPLNB-FUPCODE        TO S5011-FUPCODE.                */
		sv.fupcdes.set(fluplnbIO.getFupcode());
		sv.fupremdt.set(fluplnbIO.getFupremdt());
		sv.fupno.set(fluplnbIO.getFupno());
		wsaaUserFieldsInner.wsaaFupno.set(fluplnbIO.getFupno());
		sv.lifeno.set(fluplnbIO.getLife());
		sv.jlife.set(fluplnbIO.getJlife());
		sv.fupremk.set(fluplnbIO.getFupremk());
		sv.fuptype.set(fluplnbIO.getFuptype());
		sv.fupstat.set(fluplnbIO.getFupstat());
		sv.uprflag.set("N");
		sv.zitem.set(fluplnbIO.getFupcode());
		sv.fuprcvd.set(fluplnbIO.getFuprcvd());
		sv.exprdate.set(fluplnbIO.getExprdate());
		//smalchi2 for ILIFE-1032 STARTS
		sv.crtdateOut[varcom.nd.toInt()].set("Y");
		//ENDS
		fluppf = new Fluppf();
		fluppf.setFupCde(fluplnbIO.getFupcode().toString());
		fluppf.setLife(fluplnbIO.getLife().toString());
		fluppf.setjLife(fluplnbIO.getJlife().toString());
		fluppf.setFupSts(fluplnbIO.getFupstat().charat(0));
		fluppf.setFupDt(fluplnbIO.getFupremdt().toInt());
		fluppf.setFuprcvd(fluplnbIO.getFuprcvd().toInt());
		fluppf.setExprDate(fluplnbIO.getExprdate().toInt());
		fluppfList.add(fluppf);
		
		
		readFupe1600();
		if (fupeList.size()>0) {
			sv.ind.set("+");
		}
		else {
			sv.ind.set(" ");
		}
		sv.crtuser.set(fluplnbIO.getCrtuser());
		sv.language.set(wsspcomn.language);
			sv.crtdate.set(fluplnbIO.getCrtdate());
		if (sv.crtdate.toInt()==0) {
			sv.crtdate.set(varcom.vrcmMaxDate);
		}
		checkHxcl1400();
		fluplnbIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, fluplnbIO);
		/* Check for I/O error. Note that the ENDP condition is tested*/
		/* for later.*/
		if (!fluplnbIO.getStatuz().equals(varcom.oK)
		&& !fluplnbIO.getStatuz().equals(varcom.endp)) {
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
	}

protected void addSubfileRecord1320()
	{
		if (wsspcomn.flag.equals("I")) {
			/*        MOVE 'Y'                TO S5011-FUPCDE-OUT(PR)  <LA2104>*/
			sv.fupcdesOut[varcom.pr.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S5011", sv);
		if (!scrnparams.statuz.equals(varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkHxcl1400()
	{
	 //IBPLIFE-504 start
		Hxclpf hxclpf=hxclpfDAO.readHxcl(fluplnbIO.getChdrcoy().toString(), 
				fluplnbIO.getChdrnum().toString(), wsaaUserFieldsInner.wsaaFupno.toInt());
		if (hxclpf==null) {
			return ;
		}
		 //IBPLIFE-504 end
		sv.ind.set("+");
	
		}

	/**
	* <pre>
	* Read FUPE to determine whether any record exists                
	* </pre>
	*/
protected void readFupe1600()
	{
	fupeList=fupepfDAO.readRecord(fluplnbIO.getChdrcoy().toString(), fluplnbIO.getChdrnum().toString(), fluplnbIO.getFupno().toInt());
	}
	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (wsspcomn.secActn[wsspcomn.programPtr.toInt()].equals("*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (wsspcomn.flag.equals("I")) {
			sv.zdoctorOut[varcom.pr.toInt()].set("Y");
		}
		/*    MOVE PROT                TO SCRN-FUNCTION.                */
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validateSubfile2060();
				case checkStatuz2070: 
					checkStatuz2070();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S5011IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                S5011-DATA-AREA                  */
		/*                                S5011-SUBFILE-AREA.              */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		wsaaUserFieldsInner.wsaaRoluRold.set(scrnparams.statuz);
		/*VALIDATE-SCREEN*/
		if (scrnparams.statuz.equals(varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (scrnparams.statuz.equals(varcom.kill)) {
			/* OR WSSP-FLAG                = 'I'*/
			goTo(GotoLabel.checkStatuz2070);
		}
		//IBPLIFE-5299 Start
		if (cnmtn056Permission) {
			validateDoctor();
		}
		//IBPLIFE-5299 End
		/*CHECK-FOR-ERRORS*/
		if (!sv.errorIndicators.equals(" ")) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2060()
	{
		/*    IF WSSP-FLAG                = 'I'                    <LA4463>*/
		/*       GO TO 2070-CHECK-STATUZ.                          <LA4463>*/
		/*    MOVE SRNCH                  TO SCRN-FUNCTION.                */
		/* If a field is in error and you do not ammend it but simply      */
		/* press enter again,the line would not be validated and the       */
		/* error would not be found.                                       */
		/* When in enquiry mode,if user select 1 or 2 on empty line,       */
		/* then error will occur after user press ENTER twice.             */
		/* So change to use SSTRT to validate every line in subfile        */
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5011", sv);
		if (!scrnparams.statuz.equals(varcom.oK)
		&& !scrnparams.statuz.equals(varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (scrnparams.statuz.equals("ENDP")) {
			goTo(GotoLabel.checkStatuz2070);
		}
		ncount = 0;
		while ( !(scrnparams.statuz.equals(varcom.endp))) {
			validateSubfile2600();
		}
		
		/*  If no errors have been detected, ensure a Doctor is input      */
		/*  for a Follow Up which requires this information.               */
		if (sv.errorSubfile.equals(" ")
		&& !wsspcomn.flag.equals("I")) {
			wsaaUserFieldsInner.wsaaDoctFlup.set(" ");
			scrnparams.statuz.set(varcom.oK);
			scrnparams.function.set(varcom.sstrt);
			while ( !(wsaaUserFieldsInner.wsaaDoctFlup.equals("Y")
			|| scrnparams.statuz.equals(varcom.endp))) {
				findDoctFlup2700();
			}
			
			if (wsaaUserFieldsInner.wsaaDoctFlup.equals("Y")
			&& sv.zdoctor.equals(" ")) {
				sv.zdoctorErr.set(errorsInner.hl43);
				if (wsaaUserFieldsInner.wsaaFirstDisplay.equals("Y")) {
					wsspcomn.edterror.set("Y");
					wsaaUserFieldsInner.wsaaFirstDisplay.set("N");
				}
			}
			/* Check nominated Doctor is valid                                 */
			if (wsaaUserFieldsInner.wsaaDoctFlup.equals("Y")
			&& !sv.zdoctor.equals(" ")) {
				Clrrpf	clrrpf=clrrpfDAO.getClrfRecord(clntrlsrec.doctor.toString(), 
						wsspcomn.company.toString(), sv.zdoctor.toString(), clntrlsrec.doctor.toString());
				if (clrrpf==null || !clrrpf.getUsed2b().equals(" ")) {
					sv.zdoctorErr.set(errorsInner.hl43);
					wsspcomn.edterror.set("Y");
				}
			}
			if(!sv.zdoctor.equals(" ")){
				namadrsrec.namadrsRec.set(" ");
				namadrsrec.clntPrefix.set("CN");
				namadrsrec.clntCompany.set(wsspcomn.fsuco);
				namadrsrec.clntNumber.set(sv.zdoctor);
				namadrsrec.language.set(wsspcomn.language);
				namadrsrec.function.set(wsaaUserFieldsInner.wsaaPayeeName);
				callProgram(Namadrs.class, namadrsrec.namadrsRec);
				if (!namadrsrec.statuz.equals(varcom.oK)) {
					sv.zdoctorErr.set(errorsInner.hl43);	//ILIFE-6217
					wsspcomn.edterror.set("Y");				//ILIFE-6217
		}
				sv.zdocname.set(namadrsrec.name);
	}
			else
				sv.zdocname.set(" ");
		}
	}

protected void checkStatuz2070()
	{
		/*  Do not allow any action on the screen if an error was found*/
		/*  in the above VALIDATE section.*/
		if (!wsspcomn.edterror.equals("Y")) {
			if (wsaaUserFieldsInner.wsaaRoluRold.equals(varcom.rolu)
			|| wsaaUserFieldsInner.wsaaRoluRold.equals(varcom.rold)) {
				checkRoluRold2100();
			}
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*               (Screen validation)
	* </pre>
	*/
protected void checkRoluRold2100()
	{
					check2110();
					checkForErrors2180();
				}

protected void check2110()
	{
		if (wsaaUserFieldsInner.wsaaRoluRold.equals(varcom.rold)) {
			if (wsaaUserFieldsInner.wsaaFirstPage.equals("Y")) {
				scrnparams.errorCode.set(errorsInner.f498);
			}
			return ;
		}
		/* If this is a ROLU.*/
		if (wsspcomn.flag.equals("I")) {
			if (scrnparams.subfileMore.equals("N")) {
				scrnparams.errorCode.set(errorsInner.f499);
			}
			return ;
		}
		for (wsaaUserFieldsInner.wsaaCount.set(1); !(wsaaUserFieldsInner.wsaaCount.toInt()>sv.subfilePage.toInt()); wsaaUserFieldsInner.wsaaCount.add(1)){
			checkSubfile2110();
		}
	}

protected void checkForErrors2180()
	{
		if (!scrnparams.errorCode.equals(" ")) {
			wsspcomn.edterror.set("Y");
			wsaaUserFieldsInner.wsaaRoluRold.set(varcom.calc);
		}
		/*EXIT*/
	}

protected void checkSubfile2110()
	{
		/*CHECK*/
		scrnparams.subfileRrn.set(wsaaUserFieldsInner.wsaaCount);
		scrnparams.function.set(varcom.sread);
		processScreen("S5011", sv);
		if (!scrnparams.statuz.equals(varcom.oK)
		&& !scrnparams.statuz.equals(varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (sv.uprflag.equals(" ")) {
			scrnparams.errorCode.set(errorsInner.f499);
		}
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validation2610();
				case continue2620: 
					continue2620();
				case updateErrorIndicators2670: 
					updateErrorIndicators2670();
					readNextModifiedRecord2680();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		if (sv.select.equals(" ")
		&& sv.fupcdes.equals(" ")
		&& sv.lifeno.toInt()==0
		&& sv.jlife.equals(" ")
		&& sv.fupstat.equals(" ")
		&& sv.fupremdt.toInt()==99999999
		&& sv.fupremk.equals(" ")
		&& sv.zitem.equals(" ")
		&& sv.uprflag.equals(" ")) {
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Check if fields have been blanked out i.e. deleted.*/
		/*    IF  S5011-FUPCODE           = " "                         */
		if (sv.fupcdes.equals(" ")
		//&& sv.fupremk.equals(" ")//ILIFE-5657(No need to check this field as it is autopopulated
		&& sv.fupstat.equals(" ")
		&& !sv.zitem.equals(" ")
		&& (sv.fupremdt.toInt()==0
		|| sv.fupremdt.toInt()==99999999
		|| sv.fupremdt.equals(" "))
		&& sv.lifeno.toInt()==0
		&& sv.jlife.equals(" ")
		&& sv.uprflag.equals("N")) {
			sv.uprflag.set("D");
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Invalid action for in blank line                                */
		if (sv.fupcdes.equals(" ")
		&& sv.fupremk.equals(" ")
		&& sv.fupstat.equals(" ")
		&& sv.zitem.equals(" ")
		&& (sv.fupremdt.toInt()==0
		|| sv.fupremdt.toInt()==99999999
		|| sv.fupremdt.equals(" "))
		&& sv.lifeno.toInt()==0
		&& sv.jlife.equals(" ")
		&& sv.uprflag.equals(" ")
		&& !sv.select.equals(" ")) {
			sv.selectErr.set(errorsInner.e005);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* No need to check other fields when enquiry                      */
		if (wsspcomn.flag.equals("I")) {
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Fields have been blanked out for a new record*/
		/*    IF  S5011-FUPCODE           = " "                         */
		if (sv.fupcdes.equals(" ")
		&& sv.fupremk.equals(" ")
		&& sv.fupstat.equals(" ")
		&& sv.zitem.equals(" ")
		&& (sv.fupremdt.toInt()==0
		|| sv.fupremdt.toInt()==99999999
		|| sv.fupremdt.equals(" "))
		&& sv.lifeno.toInt()==0
		&& sv.jlife.equals(" ")
		&& (sv.uprflag.equals(" ")
		|| sv.uprflag.equals("X"))) {
			sv.uprflag.set("X");
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* After "select" & page up / page down will cause program bomb    */
		if (wsaaUserFieldsInner.wsaaRoluRold.equals(varcom.rolu)
		|| wsaaUserFieldsInner.wsaaRoluRold.equals(varcom.rold)) {
			if (!sv.select.equals(" ")) {
				sv.selectErr.set(errorsInner.g892);
				goTo(GotoLabel.updateErrorIndicators2670);
			}
		}
		/* Check follow up status.*/
		/* MOVE " "                 TO ITEM-PARAMS.                  */
		/* MOVE 'IT'                   TO ITEM-ITEMPFX.                 */
		/* MOVE WSSP-COMPANY           TO ITEM-ITEMCOY.                 */
		/* MOVE T5660                  TO ITEM-ITEMTABL.                */
		/* MOVE S5011-FUPSTAT          TO ITEM-ITEMITEM.                */
		/* MOVE READR                  TO ITEM-FUNCTION.                */
		/* CALL 'ITEMIO'               USING ITEM-PARAMS.               */
		/* IF ITEM-STATUZ              NOT = O-K                        */
		/*                             AND NOT = MRNF                   */
		/*    MOVE ITEM-PARAMS         TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                  */
		/* IF ITEM-STATUZ              = MRNF                           */
		/*    MOVE E558                TO S5011-FUPSTS-ERR.             */
		/* Check follow up code.*/
		/*    MOVE S5011-FUPCODE          TO ITEM-ITEMITEM.                */
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(sv.fupcdes);
		itempfList=itempfDAO.getAllItemitem("IT",wsspcomn.company.toString(),t5661, wsaaT5661Key.toString());
		if (itempfList.isEmpty()) {
			/*       MOVE E557                TO S5011-FUPCDE-ERR              */
			sv.fupcdesErr.set(errorsInner.e557);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Check presence of letter type in case of printing of either     */
		/* standard follow up letter or reminder.                          */
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		/*    IF S5011-FUPSTAT                = 'L'                <LA4463>*/
		if (sv.fupstat.equals("L")
		|| sv.fupstat.equals(" ")) {
			if (t5661rec.zlettype.equals(" ")) {
				/*          MOVE HL41                 TO S5011-FUPCDE-ERR  <LA2103>*/
				sv.fupcdesErr.set(errorsInner.hl41);
				goTo(GotoLabel.updateErrorIndicators2670);
			}
		}
		/*    IF S5011-FUPSTAT                = 'M'                <LA4463>*/
		if (sv.fupstat.equals("M")
		|| sv.fupstat.equals(" ")) {
			if (t5661rec.zlettyper.equals(" ")) {
				/*          MOVE HL41                 TO S5011-FUPCDE-ERR  <LA2103>*/
				sv.fupcdesErr.set(errorsInner.hl41);
				goTo(GotoLabel.updateErrorIndicators2670);
			}
		}
		if (!sv.zitem.equals(" ")
		&& !sv.fupcdes.equals(" ")
		&& !sv.fupcdes.equals(sv.zitem)) {
			sv.fupcdesErr.set(errorsInner.w431);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* If the remark is blank then default to desc from T5661.*/
		if (!sv.fupremk.equals(" ")) {
			goTo(GotoLabel.continue2620);
		}
		descpf=descDAO.getdescData("IT", t5661, wsaaT5661Key.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf==null) {
			sv.fuprmkErr.set(errorsInner.e557);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		else {
			/* Don't reset the follow-up status if it is called from remote    */
			/* program, e.g. BO.                                               */
			/*       MOVE T5661-FUPSTAT       TO S5011-FUPSTAT         <WEBNQ1>*/
			if (!scrnparams.deviceInd.equals("*RMT")) {	//ILIFE-6943
				sv.fupstat.set(t5661rec.fupstat);
			}
			if (sv.jlife.equals(" ")) {
				sv.jlife.set("00");
			}
			if (sv.lifeno.toInt()==0) {
				sv.lifeno.set(1);
			}
			sv.fupremk.set(descpf.getLongdesc());
		}
		/* Check follow up status.                                         */
		itempfList=itempfDAO.getAllItemitem("IT",wsspcomn.company.toString(),t5660, sv.fupstat.toString());
		if (itempfList.isEmpty()) {
	    	sv.fupstsErr.set(errorsInner.e558);
		}
	}

protected void continue2620()
	{
		/* If the date is blank the use 'todays' date.*/
		if ((sv.fupremdt.toInt()==0
		|| sv.fupremdt.equals(" ")
		|| sv.fupremdt.toInt()==99999999)
		&& sv.fupdtErr.equals(" ")) {
			if (sv.fupstat.equals("L")) {
				setReminderDate5000();
			}
			else {
				sv.fupremdt.set(datcon1rec.intDate);
			}
		}
		/* Check Received Date and Expiry Date                             */
		if (sv.exprdate.toInt()!=varcom.vrcmMaxDate.toInt()
		&& sv.exprdate.toInt()<datcon1rec.intDate.toInt()) {
			sv.exprdateErr.set(errorsInner.g914);
		}
		if (sv.fuprcvd.toInt()!=varcom.vrcmMaxDate.toInt()
		&& sv.fuprcvd.toInt()>datcon1rec.intDate.toInt()) {
			sv.fuprcvdErr.set(errorsInner.f073);
		}
		if (sv.fuprcvd.toInt()==varcom.vrcmMaxDate.toInt()) {
			for (ix.set(1); !(ix.toInt()>10
			|| t5661rec.fuposs[ix.toInt()].equals(" ")); ix.add(1)){
				if (t5661rec.fuposs[ix.toInt()].equals(sv.fupstat)) {
					sv.fuprcvdErr.set(errorsInner.e186);
				}
			}
		}
		if(isLT(sv.fuprcvd.toInt(),sv.crtdate.toInt()))
		{
		scrnparams.errorCode.set(errorsInner.ruue);
		}
		/* Validate that the life number is not ZEROES.*/
		if (sv.lifeno.toInt()==0) {
			sv.lifenoErr.set(errorsInner.h031);
		}
		/* Validate joint life, if entered.                                */
		if (!sv.jlife.equals(" ")) {
			List<Lifepf>lifeList = lifepfDAO.getLifeData(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString(), sv.lifeno.toString(), sv.jlife.toString());
			if (lifeList.isEmpty()) {
				sv.jlifeErr.set(errorsInner.e350);
			}
		}
		if (!sv.select.equals(" ")) {
			optswchrec.optsFunction.set("CHCK");
			optswchrec.optsCallingProg.set(wsaaProg);
			optswchrec.optsDteeff.set(0);
			optswchrec.optsCompany.set(wsspcomn.company);
			optswchrec.optsItemCompany.set(wsspcomn.company);
			optswchrec.optsLanguage.set(wsspcomn.language);
			varcom.vrcmTranid.set(wsspcomn.tranid);
			optswchrec.optsUser.set(varcom.vrcmUser);
			optswchrec.optsSelType.set("L");
			optswchrec.optsSelOptno.set(sv.select);
			callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
			if (!optswchrec.optsStatuz.equals(varcom.oK)) {
				sv.selectErr.set(optswchrec.optsStatuz);
			}
		}
		/* Cannot switch to Follow-up Extended Text if no follow-up code   */
		if ((sv.select.equals("1")
		|| sv.select.equals("2"))
		&& sv.fupcdes.equals(" ")) {
			sv.selectErr.set(errorsInner.rf00);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if (sv.zitem.equals(" ")) {
			if (sv.fupcdes.equals(" ")) {
				sv.ind.set(" ");
			}
			else {
				if (!t5661rec.messages.equals(" ")) {
					sv.ind.set("+");
				}
			}
		}
		/* The subfile record has been changed so update the hidden*/
		/* flag.*/
		/*IBPLIFE_7937 Starts*/
		if(fluppfList!=null && !fluppfList.isEmpty()
		/*IBPLIFE_7666 Starts*/	&& ncount<fluppfList.size() /*IBPLIFE_7666 Starts*/)/*IBPLIFE_7937 Ends*/
		{
			if (sv.uprflag.equals("N") && (
					isNE(sv.fupcdes,fluppfList.get(ncount).getFupCde()) ||
					isNE(sv.lifeno,fluppfList.get(ncount).getLife())||
					isNE(sv.jlife,fluppfList.get(ncount).getjLife())||
					isNE(sv.fupstat,fluppfList.get(ncount).getFupSts())||
					isNE(sv.fupremdt,fluppfList.get(ncount).getFupDt())||
					isNE(sv.fuprcvd,fluppfList.get(ncount).getFuprcvd()) ||
					isNE(sv.exprdate,fluppfList.get(ncount).getExprDate()) )) {
				sv.uprflag.set("U");
			}
		
		}
		ncount++;
		if (sv.uprflag.equals(" ")) {
			sv.crtdate.set(datcon1rec.intDate);
			sv.fuptype.set("P");/*ILIFE-4455*/
			sv.uprflag.set("A");
		}
	}

protected void updateErrorIndicators2670()
	{
		if (!sv.errorSubfile.equals(" ")) {
			wsspcomn.edterror.set("Y");
		}
		/* PERFORM 1400-CHECK-HXCL.                                     */
		scrnparams.function.set(varcom.supd);
		processScreen("S5011", sv);
		if (!scrnparams.statuz.equals(varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		/*    MOVE SRNCH                  TO SCRN-FUNCTION.                */
		/* If a field is in error and you do not ammend it but simply      */
		/* press enter again,the line would not be validated and the       */
		/* error would not be found.                                       */
		/* When in enquiry mode,if user select 1 or 2 on empty line,       */
		/* then error will occur after user press ENTER twice.             */
		/* So change to use SRDN to validate every line in subfile when    */
		/* enquiry                                                         */
		scrnparams.function.set(varcom.srdn);
		processScreen("S5011", sv);
		if (!scrnparams.statuz.equals(varcom.oK)
		&& !scrnparams.statuz.equals(varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void findDoctFlup2700()
	{
			read2700();
		}

protected void read2700()
	{
		/*    CALL 'S5011IO'           USING SCRN-SCREEN-PARAMS    <FUPLET>*/
		/*                                   S5011-DATA-AREA       <FUPLET>*/
		/*                                   S5011-SUBFILE-AREA.   <FUPLET>*/
		processScreen("S5011", sv);
		if (!scrnparams.statuz.equals(varcom.oK)
		&& !scrnparams.statuz.equals(varcom.endp)) {
			syserrrec.params.set(scrnparams.screenParams);
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (scrnparams.statuz.equals(varcom.endp)) {
			return ;
		}
		/* Check if this Follow Up requires a Doctor to be supplied.       */
		/*    IF S5011-FUPCODE         NOT = SPACE                 <FUPLET>*/
		if (!sv.fupcdes.equals(" ")) {
			
			/*       MOVE S5011-FUPCODE       TO ITEM-ITEMITEM         <FUPLET>*/
			wsaaT5661Lang.set(wsspcomn.language);
			wsaaT5661Fupcode.set(sv.fupcdes);
			itempfList=itempfDAO.getAllItemitem("IT",wsspcomn.company.toString(),t5661, wsaaT5661Key.toString());
			if (!itempfList.isEmpty()) {
			t5661rec.t5661Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
			/* Set indicator if this Follow Up requires Doctor details.        */
			if (t5661rec.zdocind.equals("Y")) {
				wsaaUserFieldsInner.wsaaDoctFlup.set("Y");
			}
		  }
		}
		scrnparams.function.set(varcom.srdn);
	}

	/**
	* <pre>
	*    Sections performed from the 2600 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
			updateDatabase3010();
		}

protected void updateDatabase3010()
	{
		if (wsspcomn.secActn[wsspcomn.programPtr.toInt()].equals("*")) {
			return ;
		}
		/*  Update database files as required*/
		if (scrnparams.statuz.equals(varcom.kill)) {
			return ;
		}
		wsaaUpdchdr.set(" ");
		for (wsaaUserFieldsInner.wsaaCount.set(1); !(wsaaUserFieldsInner.wsaaCount.toInt()>sv.subfilePage.toInt()); wsaaUserFieldsInner.wsaaCount.add(1)){
			updateFollowUp3100();
		}
		/*  Update CHDRPF timestamp if update to FLUPPF                    */
		if (wsaaUpdchdr.equals("Y")) {
			chdrpfDAO.updateChdrTimestamp(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		}
		/*  Update HPAD with Doctor details, if applicable                 */
		if (hpadpf.getZdoctor()==null || !hpadpf.getZdoctor().trim().equals(sv.zdoctor.trim())) {
			hpadpfDAO.updateDoctor(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString(),sv.zdoctor.toString());
		}
		
	}

	/**
	* <pre>
	*     DO THE FOLLOW UP FILE UPDATING
	* </pre>
	*/
protected void updateFollowUp3100()
	{
			updateFollowUp3110();
		}

protected void updateFollowUp3110()
	{
		/* Get the subfile record using the relative record number.*/
		scrnparams.subfileRrn.set(wsaaUserFieldsInner.wsaaCount);
		scrnparams.function.set(varcom.sread);
		processScreen("S5011", sv);
		if (!scrnparams.statuz.equals(varcom.oK)
		&& !scrnparams.statuz.equals(varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/* Set up key.*/
		fluplnbIO.setParams(" ");
		fluplnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		fluplnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		fluplnbIO.setFupno(sv.fupno);
		/* Save first record in the subfile for ROLD.*/
		if (wsaaUserFieldsInner.wsaaCount.toInt()==1) {
			wsaaUserFieldsInner.wsaaFirstFlupKey.set(fluplnbIO.getRecKeyData());
		}
		/* The subfile record has not been changed.*/
		if (sv.uprflag.equals(" ")
		|| sv.uprflag.equals("N")
		|| wsspcomn.flag.equals("I")) {
			return ;
		}
		/* If the update is a delete.*/
		if (sv.uprflag.equals("D")) {
			wsaaUpdchdr.set("Y");
			deleteAllHxcl3300();
			fluplnbIO.setFunction(varcom.readh);
			flupFile3200();
			deleteFupe3400();
			fluplnbIO.setFormat(formatsInner.fluplnbrec);
			fluplnbIO.setFunction(varcom.delet);
			flupFile3200();
		}
		/* If the update is an addition.*/
		if (sv.uprflag.equals("A")) {
			wsaaUpdchdr.set("Y");
			fluplnbIO.setFuptype("P");
			fluplnbIO.setTranno(chdrlnbIO.getTranno());
			fluplnbIO.setLife(sv.lifeno);
			if (sv.jlife.equals(" ")) {
				fluplnbIO.setJlife("00");
			}
			else {
				fluplnbIO.setJlife(sv.jlife);
			}
			/*       MOVE S5011-FUPCODE       TO FLUPLNB-FUPCODE               */
			fluplnbIO.setFupcode(sv.fupcdes);
			fluplnbIO.setFupstat(sv.fupstat);
			fluplnbIO.setFupremdt(sv.fupremdt);
			fluplnbIO.setFuptype(sv.fuptype);
			fluplnbIO.setFupremk(sv.fupremk);
			fluplnbIO.setTermid(varcom.vrcmTermid);
			fluplnbIO.setUser(varcom.vrcmUser);
			fluplnbIO.setCrtuser(wsspcomn.userid);
			fluplnbIO.setTransactionDate(varcom.vrcmDate);
			fluplnbIO.setTransactionTime(varcom.vrcmTime);
			fluplnbIO.setEffdate(datcon1rec.intDate);
			fluplnbIO.setFormat(formatsInner.fluplnbrec);
			fluplnbIO.setFunction(varcom.writr);
			fluplnbIO.setCrtdate(datcon1rec.intDate);
			fluplnbIO.setFuprcvd(sv.fuprcvd);
			fluplnbIO.setExprdate(sv.exprdate);
			/*    MOVE VRCM-MAX-DATE       TO FLUPLNB-EFFDATE       <LA4592>*/
			fluplnbIO.setZlstupdt(varcom.vrcmMaxDate);
			fluplnbIO.setFormat(formatsInner.fluplnbrec);
			writeFupe3500();
			fluplnbIO.setFunction(varcom.writr);
			Fluppf fluppf = new Fluppf();
			fluppf.setChdrcoy(chdrlnbIO.getChdrcoy().toString().charAt(0));
			fluppf.setChdrnum(chdrlnbIO.getChdrnum().toString());
			fluppf.setFupSts(fluplnbIO.getFupstat().charat(0));
			fluppf.setFupNo(fluplnbIO.getFupno().toInt());
			fluppf.setFupCde(fluplnbIO.getFupcode().toString());
			fluppf.setTranNo(fluplnbIO.getTranno().toInt());
			if(!fluppfDAO.isCheckExistFluppf(fluppf)){
				flupFile3200();
			}
			
		}
		/* If the update is an update.*/
		if (sv.uprflag.equals("U")) {
			wsaaUpdchdr.set("Y");
			fluplnbIO.setFunction(varcom.readh);
			flupFile3200();
			fluplnbIO.setLife(sv.lifeno);
			if (sv.jlife.equals(" ")) {
				fluplnbIO.setJlife("00");
			}
			else {
				fluplnbIO.setJlife(sv.jlife);
			}
			/*       MOVE S5011-FUPCODE       TO FLUPLNB-FUPCODE               */
			fluplnbIO.setFupcode(sv.fupcdes);
			fluplnbIO.setFupstat(sv.fupstat);
			fluplnbIO.setFupremdt(sv.fupremdt);
			fluplnbIO.setFuptype(sv.fuptype);
			fluplnbIO.setFuprcvd(sv.fuprcvd);
			fluplnbIO.setExprdate(sv.exprdate);
			fluplnbIO.setFupremk(sv.fupremk);
			fluplnbIO.setTermid(varcom.vrcmTermid);
			fluplnbIO.setUser(varcom.vrcmUser);
			fluplnbIO.setTransactionDate(varcom.vrcmDate);
			fluplnbIO.setTransactionTime(varcom.vrcmTime);
			fluplnbIO.setFormat(formatsInner.fluplnbrec);
			fluplnbIO.setLstupuser(wsspcomn.userid);
			fluplnbIO.setZlstupdt(datcon1rec.intDate);
			fluplnbIO.setFormat(formatsInner.fluplnbrec);
			fluplnbIO.setFunction(varcom.rewrt);
			flupFile3200();
		}
		
		
		
		 if(isFatcaAllowed)
			  {
			 Clftpf	clft=clftpfDAO.getRecordByClntnum(chdrlnbIO.getCownpfx().toString(),"9",chdrlnbIO.getCownnum().toString());
					fupstsList = fluppfDAO.getFollowUps(chdrlnbIO.getCownnum().toString(),"9");
				
				if(clft != null)//ILIFE-6157
				{
					if((fupstsList.size()==1) && (fupstsList.contains("R")) )
		
					{

					     if(clft.getFftax().equals("Y") 
					     && clft.getFfftype().equals(formTypeW9)
					     && clft.getFftin().compareTo(BigDecimal.ZERO)>0)
					     { 
					    	 clft.setFfsts(fatcastsUSP);
					    	
					     }
					     else if(clft.getFftax().equals("N")
					     && clft.getFfftype().equals(formTypeW8)
					     && clft.getExdate()!=32)
					     { 
					    	 clft.setFfsts(fatcastsNUSWI);
					    	
					     }
			
					   else
					     { 
					    	 clft.setFfsts(fatcastsNUSWOI);
					    	
					     }
					
					}
					else if((fupstsList.isEmpty()) && fluplnbIO.getFupstat().equals("R"))
					{

						     if(clft.getFftax().equals("Y") 
						     && clft.getFfftype().equals(formTypeW9)
						     && clft.getFftin().compareTo(BigDecimal.ZERO)>0)
						     { 
						    	 clft.setFfsts(fatcastsUSP);
						    	
						     }
						     else if(clft.getFftax().equals("N")
						     && clft.getFfftype().equals(formTypeW8)
						     && clft.getExdate()!=32)
						     { 
						    	 clft.setFfsts(fatcastsNUSWI);
						    	
						     }
						   else
						     { 
						    	 clft.setFfsts(fatcastsNUSWOI);
						     }			
					}
		
					else	
						
					{  
						clft.setFfsts("PEND");
					}
					clftpfDAO.updateClftRecord(clft);	
				
				}//ILIFE-6157
			  }
		
		
     }

	/**
	* <pre>
	*     ACCESS THE FLUPLNB FILE
	* </pre>
	*/
protected void flupFile3200()
	{
		/*FLUP-FILE*/
		SmartFileCode.execute(appVars, fluplnbIO);
		if (!fluplnbIO.getStatuz().equals(varcom.oK)) {
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void deleteAllHxcl3300()
	{
	hxclpfDAO.deleteRecords(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString(), sv.fupno.toInt());
	}

protected void deleteFupe3400()
	{
		fupepfDAO.deleteRecords(fluplnbIO.getChdrcoy().toString(), fluplnbIO.getChdrnum().toString(), sv.fupno.toInt());
	}

protected void writeFupe3500()
	{
		start3510();
		nextExtendedText3530();
	}

protected void start3510()
	{
		wsaaDocseq.set(0);
		/*    MOVE S5011-FUPCODE          TO ITEM-ITEMITEM.                */
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(sv.fupcdes);
		itempfList=itempfDAO.getAllItemitem(smtpfxcpy.item.toString(),wsspcomn.company.toString(),t5661, wsaaT5661Key.toString());
	    t5661rec.t5661Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		iy.set(0);
		for (ix.set(5); !(ix.toInt()==0
		|| iy.toInt()!=0); ix.add(-1)){
			if (!t5661rec.message[ix.toInt()].equals(" ")) {
				iy.set(ix);
			}
		}
		
		ix.set(0);
	}

protected void nextExtendedText3530()
	{
		ix.add(1);
		if (ix.toInt()>5
		|| ix.toInt()>iy.toInt()) {
			return ;
		}
		wsaaDocseq.add(1);
		Fupepf fupepf=new Fupepf();
		fupepf.setChdrcoy(fluplnbIO.getChdrcoy().toString());
		fupepf.setChdrnum(fluplnbIO.getChdrnum().toString());
		fupepf.setFupno(sv.fupno.toInt());
		fupepf.setClamnum(" ");
		fupepf.setTranno(fluplnbIO.getTranno().toInt());
		fupepf.setDocseq(wsaaDocseq.toString());
		fupepf.setMessage(t5661rec.message[ix.toInt()].toString());
		fupepfDAO.insertRecord(fupepf);
		nextExtendedText3530();
		return ;
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (scrnparams.statuz.equals(varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.function.set("HIDEW");
			return ;
		}
		if (wsaaUserFieldsInner.wsaaRoluRold.equals(varcom.rolu)
		|| wsaaUserFieldsInner.wsaaRoluRold.equals(varcom.rold)) {
			wsspcomn.nextprog.set(wsaaProg);
			return ;
		}
		else {
			optswchCall4100();
		}
		/*EXIT*/
	}

protected void optswchCall4100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					call4110();
					lineSwitching4120();
				case switch4170: 
					switch4170();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void call4110()
	{
		wsspcomn.nextprog.set(wsaaProg);
	}

	/**
	* <pre>
	* Check for Line Switching                                        
	* </pre>
	*/
protected void lineSwitching4120()
	{
		if (!wsspcomn.secActn[wsspcomn.programPtr.toInt()].equals("*")) {
			scrnparams.subfileRrn.set(0);
		}
		else {
			scrnparams.subfileRrn.set(wsaaRrn);
		}
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5011", sv);
		if (!scrnparams.statuz.equals(varcom.oK)
		&& !scrnparams.statuz.equals(varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(!sv.select.equals(" ")
		|| scrnparams.statuz.equals(varcom.endp))) {
			if (sv.fupno.toInt()!=32
			&& !sv.fupcdes.equals(" ")) {
				fupeList=fupepfDAO.readRecord(fluplnbIO.getChdrcoy().toString(), fluplnbIO.getChdrnum().toString(), sv.fupno.toInt());
				if (fupeList.isEmpty()) {
					sv.ind.set(" ");
				}
				else {
					sv.ind.set("+");
				}
				wsaaUserFieldsInner.wsaaFupno.set(sv.fupno);
				checkHxcl1400();
			}
			sv.zitem.set(sv.fupcdes);
			scrnparams.function.set(varcom.supd);
			processScreen("S5011", sv);
			if (!scrnparams.statuz.equals(varcom.oK)
			&& !scrnparams.statuz.equals(varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			scrnparams.function.set(varcom.srdn);
			processScreen("S5011", sv);
			if (!scrnparams.statuz.equals(varcom.oK)
			&& !scrnparams.statuz.equals(varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		
		if (scrnparams.statuz.equals(varcom.endp)) {
			goTo(GotoLabel.switch4170);
		}
		/* ...if found, "select" the subfile line to switch to PopUp       */
		/* program.                                                        */
		/* READR and KEEPS the FLUP record for the next program            */
		fluplnbIO.setParams(" ");
		fluplnbIO.setChdrcoy(wsspcomn.company);
		fluplnbIO.setChdrnum(sv.chdrnum);
		fluplnbIO.setFupno(sv.fupno);
		fluplnbIO.setFormat(formatsInner.fluplnbrec);
		fluplnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, fluplnbIO);
		if (!fluplnbIO.getStatuz().equals(varcom.oK)) {
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
		fluplnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, fluplnbIO);
		if (!fluplnbIO.getStatuz().equals(varcom.oK)) {
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
		wsaaRrn.set(scrnparams.subfileRrn);
		optswchrec.optsSelType.set("L");
		optswchrec.optsSelOptno.set(sv.select);
		optswchrec.optsSelCode.set(" ");
		/* Update the subfile line.                                        */
		sv.select.set(" ");
		sv.zitem.set(sv.fupcdes);
		sv.uprflag.set("N");
		scrnparams.function.set(varcom.supd);
		processScreen("S5011", sv);
		if (!scrnparams.statuz.equals(varcom.oK)
		&& !scrnparams.statuz.equals(varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void switch4170()
	{
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (!optswchrec.optsStatuz.equals(varcom.oK)
		&& !optswchrec.optsStatuz.equals(varcom.endp)) {
			syserrrec.params.set(optswchrec.rec);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			fatalError600();
		}
		/*    MOVE OPTS-IND(1)            TO S5011-ZSAIND.                 */
		if (optswchrec.optsStatuz.equals(varcom.oK)) {
			wsspcomn.programPtr.add(1);
			/* release the soft lock only if this is the last program         */
			if (wsspcomn.secProg[wsspcomn.programPtr.toInt()].equals("COMIT")) {
				rlseSoftlock5000();
			}
			scrnparams.subfileRrn.set(wsaaRrn);
		}
		else {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void rlseSoftlock5000()
	{
		release5010();
	}

protected void release5010()
	{
		/* Release the soft lock on the contract.                          */
		sftlockrec.sftlockRec.set(" ");
		sftlockrec.company.set(chdrlnbIO.getChdrcoy());
		sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		sftlockrec.enttyp.set(chdrlnbIO.getChdrpfx());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(" ");
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (!sftlockrec.statuz.equals(varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}


protected void lineSels4124()
	{
			startSelection4124();
		}

protected void startSelection4124()
	{
		scrnparams.subfileRrn.add(1);
		scrnparams.function.set(varcom.sread);
		processScreen("S5011", sv);
		if (scrnparams.statuz.equals(varcom.mrnf)) {
			return ;
		}
		if (!scrnparams.statuz.equals(varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (sv.select.equals(" ")) {
			return ;
		}
		/* Check valid line selection                                      */
		wsaaOptionCode.set(sv.select);
		for (ix.set(1); !(ix.toInt()>20
		|| foundSelection.isTrue()); ix.add(1)){
			if (optswchrec.optsType[ix.toInt()].equals("L")
			&& optswchrec.optsCode[ix.toInt()].equals(wsaaOptionCode)) {
				optswchrec.optsInd[ix.toInt()].set("X");
				wsaaFoundSelection.set("Y");
			}
		}
		sv.select.set(" ");
		scrnparams.function.set(varcom.supd);
		processScreen("S5011", sv);
		if (!scrnparams.statuz.equals(varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaRrn.set(scrnparams.subfileRrn);
		/* READD and KEEPS the FLUP record for the next program            */
		fluplnbIO.setParams(" ");
		fluplnbIO.setChdrcoy(wsspcomn.company);
		fluplnbIO.setChdrnum(sv.chdrnum);
		fluplnbIO.setFupno(sv.fupno);
		fluplnbIO.setFormat(formatsInner.fluplnbrec);
		fluplnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, fluplnbIO);
		if (!fluplnbIO.getStatuz().equals(varcom.oK)) {
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
		fluplnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, fluplnbIO);
		if (!fluplnbIO.getStatuz().equals(varcom.oK)) {
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
	}

protected void setReminderDate5000()
	{
		setRemdt5010();
	}

protected void setRemdt5010()
	{
		/*  Calculate the date of issuing reminder letter                  */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		
		/*    MOVE S5011-FUPCODE          TO ITEM-ITEMITEM.        <FUPLET>*/
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(sv.fupcdes);
		itempfList=itempfDAO.getAllItemitem("IT",wsspcomn.company.toString(),t5661, wsaaT5661Key.toString());
	    t5661rec.t5661Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		datcon2rec.datcon2Rec.set(" ");
		datcon2rec.intDate1.set(datcon1rec.intDate);
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(t5661rec.zelpdays);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		sv.fupremdt.set(datcon2rec.intDate2);
	}

//IBPLIFE-5299 Start
protected void validateDoctor() {
	if (isNE(sv.zdoctor, SPACES)) {
		Clrrpf clrrpf=clrrpfDAO.getClrfRecord(clntrlsrec.doctor.toString(), 
				wsspcomn.company.toString(), sv.zdoctor.toString(), clntrlsrec.doctor.toString());
		if (clrrpf==null || !clrrpf.getUsed2b().equals(" ")) {
			sv.zdoctorErr.set(errorsInner.hl43);
			wsspcomn.edterror.set("Y");
		}
	}
}
//IBPLIFE-5299 End

public ChdrlnbTableDAM getChdrlnbIO() {
	return chdrlnbIO;
}
public void setChdrlnbIO(ChdrlnbTableDAM chdrlnbIO) {
	this.chdrlnbIO = chdrlnbIO;
}
public Hpadpf getHpadpf() {
	return hpadpf;
}
public void setHpadpf(Hpadpf hpadpf) {
	this.hpadpf = hpadpf;
}
public HpadpfDAO getHpadpfDAO() {
	return hpadpfDAO;
}
public void setHpadpfDAO(HpadpfDAO hpadpfDAO) {
	this.hpadpfDAO = hpadpfDAO;
}
/*
 * Class transformed  from Data Structure WSAA-USER-FIELDS--INNER
 */
private static final class WsaaUserFieldsInner { 
		/* WSAA-USER-FIELDS */
	private FixedLengthStringData wsaaDoctFlup = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPayeeName = new FixedLengthStringData(5).init("PYNMN");
	private FixedLengthStringData wsaaFirstDisplay = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaRoluRold = new FixedLengthStringData(4).init(" ");
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(5, 0).setUnsigned();
	private FixedLengthStringData wsaaFirstPage = new FixedLengthStringData(1);
	private PackedDecimalData wsaaFupno = new PackedDecimalData(2, 0).init(0);

	private FixedLengthStringData wsaaFirstFlupKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaFlupKeyChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaFirstFlupKey, 0);
	private FixedLengthStringData wsaaFlupKeyChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaFirstFlupKey, 1);
	private PackedDecimalData wsaaFlupKeyFupno = new PackedDecimalData(2, 0).isAPartOf(wsaaFirstFlupKey, 9);
	private FixedLengthStringData filler = new FixedLengthStringData(53).isAPartOf(wsaaFirstFlupKey, 11, FILLER).init(" ");
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e005 = new FixedLengthStringData(4).init("E005");
	private FixedLengthStringData e040 = new FixedLengthStringData(4).init("E040");
	private FixedLengthStringData f498 = new FixedLengthStringData(4).init("F498");
	private FixedLengthStringData f499 = new FixedLengthStringData(4).init("F499");
	private FixedLengthStringData e558 = new FixedLengthStringData(4).init("E558");
	private FixedLengthStringData e557 = new FixedLengthStringData(4).init("E557");
	private FixedLengthStringData e350 = new FixedLengthStringData(4).init("E350");
	private FixedLengthStringData h031 = new FixedLengthStringData(4).init("H031");
	private FixedLengthStringData hl41 = new FixedLengthStringData(4).init("HL41");
	private FixedLengthStringData hl43 = new FixedLengthStringData(4).init("HL43");
	private FixedLengthStringData rf00 = new FixedLengthStringData(4).init("RF00");
	private FixedLengthStringData w431 = new FixedLengthStringData(4).init("W431");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData f073 = new FixedLengthStringData(4).init("F073");
	private FixedLengthStringData g914 = new FixedLengthStringData(4).init("G914");
	private FixedLengthStringData g892 = new FixedLengthStringData(4).init("G892");
	private FixedLengthStringData ruue = new FixedLengthStringData(4).init("RUUE");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData fluplnbrec = new FixedLengthStringData(10).init("FLUPLNBREC");
}

public StringUtil getStringUtil() {
	return stringUtil;
}
}