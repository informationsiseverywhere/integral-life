package com.csc.life.newbusiness.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RH599.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class Rh599Report extends SMARTReportLayout { 

	private ZonedDecimalData amnt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData amnt01 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData amnt02 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData amnt03 = new ZonedDecimalData(17, 2);
	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData cntdesc = new FixedLengthStringData(30);
	private FixedLengthStringData cnttyp = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData datetexc = new FixedLengthStringData(22);
	private ZonedDecimalData hpolcnt = new ZonedDecimalData(5, 0);
	private ZonedDecimalData hpolcnt01 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private ZonedDecimalData tgtpcnt = new ZonedDecimalData(5, 2);
	private ZonedDecimalData tgtpcnt01 = new ZonedDecimalData(5, 2);
	private FixedLengthStringData time = new FixedLengthStringData(8);
	private FixedLengthStringData yymmtxt = new FixedLengthStringData(16);
	private FixedLengthStringData yymmtxt01 = new FixedLengthStringData(16);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rh599Report() {
		super();
	}


	/**
	 * Print the XML for Rh599d01
	 */
	public void printRh599d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		yymmtxt.setFieldName("yymmtxt");
		yymmtxt.setInternal(subString(recordData, 1, 16));
		amnt.setFieldName("amnt");
		amnt.setInternal(subString(recordData, 17, 17));
		tgtpcnt.setFieldName("tgtpcnt");
		tgtpcnt.setInternal(subString(recordData, 34, 5));
		hpolcnt.setFieldName("hpolcnt");
		hpolcnt.setInternal(subString(recordData, 39, 5));
		yymmtxt01.setFieldName("yymmtxt01");
		yymmtxt01.setInternal(subString(recordData, 44, 16));
		amnt01.setFieldName("amnt01");
		amnt01.setInternal(subString(recordData, 60, 17));
		tgtpcnt01.setFieldName("tgtpcnt01");
		tgtpcnt01.setInternal(subString(recordData, 77, 5));
		hpolcnt01.setFieldName("hpolcnt01");
		hpolcnt01.setInternal(subString(recordData, 82, 5));
		amnt02.setFieldName("amnt02");
		amnt02.setInternal(subString(recordData, 87, 17));
		amnt03.setFieldName("amnt03");
		amnt03.setInternal(subString(recordData, 104, 17));
		printLayout("Rh599d01",			// Record name
			new BaseData[]{			// Fields:
				yymmtxt,
				amnt,
				tgtpcnt,
				hpolcnt,
				yymmtxt01,
				amnt01,
				tgtpcnt01,
				hpolcnt01,
				amnt02,
				amnt03
			}
		);

		currentPrintLine.add(3);
	}

	/**
	 * Print the XML for Rh599e01
	 */
	public void printRh599e01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		printLayout("Rh599e01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rh599h01
	 */
	public void printRh599h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rh599h01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

		currentPrintLine.set(4);
	}

	/**
	 * Print the XML for Rh599h02
	 */
	public void printRh599h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rh599h02",			// Record name
			new BaseData[]{			// Fields:
			}
		);

		currentPrintLine.set(4);
	}

	/**
	 * Print the XML for Rh599h03
	 */
	public void printRh599h03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(14).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		datetexc.setFieldName("datetexc");
		datetexc.setInternal(subString(recordData, 1, 22));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		company.setFieldName("company");
		company.setInternal(subString(recordData, 23, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 24, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 54, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 64, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 66, 30));
		time.setFieldName("time");
		time.set(getTime());
		cnttyp.setFieldName("cnttyp");
		cnttyp.setInternal(subString(recordData, 96, 3));
		cntdesc.setFieldName("cntdesc");
		cntdesc.setInternal(subString(recordData, 99, 30));
		printLayout("Rh599h03",			// Record name
			new BaseData[]{			// Fields:
				datetexc,
				pagnbr,
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time,
				cnttyp,
				cntdesc
			}
			, new Object[] {			// indicators
				new Object[]{"ind13", indicArea.charAt(13)},
				new Object[]{"ind14", indicArea.charAt(14)},
				new Object[]{"ind11", indicArea.charAt(11)},
				new Object[]{"ind12", indicArea.charAt(12)}
			}
		);

		currentPrintLine.add(7);
	}

	/**
	 * Print the XML for Rh599h04
	 */
	public void printRh599h04(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rh599h04",			// Record name
			new BaseData[]{			// Fields:
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rh599h05
	 */
	public void printRh599h05(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rh599h05",			// Record name
			new BaseData[]{			// Fields:
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rh599h06
	 */
	public void printRh599h06(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rh599h06",			// Record name
			new BaseData[]{			// Fields:
			}
		);

		currentPrintLine.add(1);
	}


}
