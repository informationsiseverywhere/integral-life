package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for PROTECT
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5002protect extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	//public static final int[] pfInds = new int[] {1, 2, 3, 4, 6, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static  final RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {-1, -1, -1, -1}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5002ScreenVars sv = (S5002ScreenVars) pv;
		clearInds(av, sv.getScreenPfInds());
		write(lrec, sv.S5002protectWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5002ScreenVars screenVars = (S5002ScreenVars)pv;
	}

/**
 * Clear all the variables in S5002protect
 */
	public static void clear(VarModel pv) {
		S5002ScreenVars screenVars = (S5002ScreenVars) pv;
	}
}
