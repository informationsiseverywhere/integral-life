/*
 * File: P5004.java
 * Date: 29 August 2009 23:52:52
 * Author: Quipoz Limited
 *
 * Class transformed from P5004.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.common.DD;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.agents.dataaccess.dao.AgntpfDAO;
import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.model.Agntpf;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.clients.dataaccess.dao.AnsopfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClftpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClrfpfDAO;
import com.csc.fsu.clients.dataaccess.dao.SoinDAO;
import com.csc.fsu.clients.dataaccess.dao.impl.ClftpfDAOImpl;
import com.csc.fsu.clients.dataaccess.model.Ansopf;
import com.csc.fsu.clients.dataaccess.model.Clftpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.dataaccess.model.Clrfpf;
import com.csc.fsu.clients.dataaccess.model.Soinpf;
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.fsu.general.dataaccess.dao.GrpspfDAO;
import com.csc.fsu.general.dataaccess.dao.MandpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.SchmpfDAO;
import com.csc.fsu.general.dataaccess.model.Grpspf;
import com.csc.fsu.general.dataaccess.model.Mandpf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Schmpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.tablestructures.T3620rec;
import com.csc.fsu.reinsurance.dataaccess.TtrcTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.dao.ZctxpfDAO;
import com.csc.life.agents.dataaccess.model.Zctxpf;
import com.csc.life.annuities.tablestructures.T6697rec;
import com.csc.life.contractservicing.dataaccess.dao.AglflnbpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Aglflnbpf;
import com.csc.life.enquiries.dataaccess.AsgnenqTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.general.dataaccess.AnrlcntTableDAM;
import com.csc.life.newbusiness.dataaccess.BnfylnbTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CtrsTableDAM;
import com.csc.life.newbusiness.dataaccess.FluplnbTableDAM;
import com.csc.life.newbusiness.dataaccess.InctTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.BnkoutpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.PcddpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.RlvrpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Bnkoutpf;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.newbusiness.dataaccess.model.Pcddpf;
import com.csc.life.newbusiness.dataaccess.model.Rlvrpf;
import com.csc.life.newbusiness.procedures.Antisocl;
import com.csc.life.newbusiness.recordstructures.Antisoclkey;
import com.csc.life.newbusiness.screens.S5004ScreenVars;
import com.csc.life.newbusiness.tablestructures.NBProposalRec;
import com.csc.life.newbusiness.tablestructures.T3615rec;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.newbusiness.tablestructures.Tjl05rec;
import com.csc.life.newbusiness.tablestructures.Tr52qrec;
import com.csc.life.productdefinition.dataaccess.PnteTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.MrtapfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Mrtapf;
import com.csc.life.productdefinition.procedures.FirstRcpPrem;
import com.csc.life.productdefinition.recordstructures.FirstRcpPremRec;
import com.csc.life.productdefinition.tablestructures.T5620rec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T5689rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.productdefinition.tablestructures.Tr59xrec;
import com.csc.life.productdefinition.tablestructures.Tr5awrec;
import com.csc.life.regularprocessing.tablestructures.T5654rec;
import com.csc.life.regularprocessing.tablestructures.T6660rec;
import com.csc.life.terminationclaims.tablestructures.Tr5b2rec;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1698rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.impl.DescDAOImpl;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
//import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.smart.recordstructures.Varcom;



/**
 * <pre>
 * Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
 *REMARKS.
 *
 *               NEW LIFE CONTRACT HEADER
 *
 * Initialise
 * ----------
 *
 * Skip this section  if  returning  from  an optional selection
 * (current stack position action flag = '*').
 *
 * The details of the contract being worked on will be stored in
 * the CHDRLNB I/O module.  Retrieve the details.
 *
 * Look up the following always:
 *       Contract type from T5688,
 *       Contract status from T3623.
 *       Servicing branch from T1692 (default from sign-on branch)
 *
 * Look up the  contract  definition  details from T5688. If the
 * minimum  number of  policies  in  the  plan  is  blank,  plan
 * processing is not  applicable  to the contract. In this case,
 * non-display and protect the prompt for the number of policies
 * in the plan.  Otherwise,  if the number of policies is blank,
 * move in the default value from the table.
 *
 * Look up the  contract  billing   details from T5689. If there
 * is only one possible payment method and/or billing  frequency
 * default and protect it/them.
 *
 * Check WSSP-FLAG.  If  'I'(enquiry  mode),  set  indicator  to
 * protect  all  input  capable  fields  except  the  indicators
 * controlling where to switch to next.
 *
 * Check  the  Original  Commencement  Date.  If  is  zero,  the
 * contract header is being  set  up for the FIRST TIME. In this
 * case, the kill function  is  not allowed, and the rest of the
 * look-ups are not required.
 *
 * For all cases other  than  the  very  first  time  into a new
 * contract:
 *
 *  Look up the following:
 *       Premium status from T3588,
 *       The owner's client (CLTS) details.  Call FMTCLTN to
 *            format the client name for confirmation.
 *       For the Proposal Owner check to see if they
 *            are a life on another proposal (use LFCLLNB),
 *       Check to  see   if   there   are   any  outstanding
 *            follow-ups (FLUP) attached to the proposal and
 *            set the  flag accordingly.  Also, if there are
 *            any  follow-ups at  all,  put  a  '+'  in  the
 *            follow-up selection field,
 *       The agent (AGNT)  to  get  the  client  number, and
 *            hence the  client (agent's) name. Call FMTCLTN
 *            to format the client name for confirmation.
 *
 * Check the following contract header fields. If they are blank
 * or the same as  the owner (if a client number), blank out the
 * appropriate selection field.  Otherwise  put  a  '+'  in  the
 * selection  field  to  indicate  that  there  are  appropriate
 * details attached to the contract header.
 *
 *       Joint owner,
 *       Despatch address,
 *       Assignee,
 *       Payer,
 *       Direct debit details.
 *       Group membership details.
 *
 * Check the beneficiaries  (BNFY).  If  there  are  any for the
 * contract, put a plus in the beneficiary selection field.
 *
 * Check the commission split (PCDD). If there  are  any for the
 * contract, put a plus in the commission selection field.
 *
 * Check the contract  suspense sub-account balance. If it is in
 * credit (less than  zero),  put  a  plus  in  the  Apply  Cash
 * selection field.
 *
 * Validation
 * ----------
 *
 * Skip this section  if  returning  from  an optional selection
 * (current stack position action flag = '*').
 *
 * If the 'KILL'  function  key  was  pressed,  and this is "the
 * first time" (original commencement date zero), the request is
 * invalid. Otherwise, skip all the validation.
 *
 * If in enquiry mode, skip all field validation.  The selection
 * fields must still be validated though.
 *
 * Validate the screen  according  to  the  rules defined by the
 * field help. For all fields which have descriptions, look them
 * up again if  the  field  has been changed (CHG indicator from
 * DDS keyword).
 *
 * Validation of Method  of  Payment  includes looking up table
 * T3620.  Cross check the bank account details set up on the
 * contract header against this table.
 * If  the  details are not required, blank out the relevant
 * contract  header details.  Also, if their selection
 * field is  '+',  blank it out also.  Otherwise, if the details
 * are required  and  have not been previously set up, force the
 * relevant details  to  be  selected  (by  moving an "X" to the
 * appropriate selection field).
 *
 * If 'CALC' was pressed, re-display the screen.
 *
 * Updating
 * --------
 *
 * Skip this section  if  returning  from  an optional selection
 * (current stack position action flag = '*').
 *
 * If the 'KILL' function key was pressed or if in enquiry mode,
 * skip the updating.
 *
 * Store the contract header details in the CHDRLNB I/O module.
 *
 * If this is the first time the header is being set up, look up
 * the entry in T5679 (Statuses for transaction) for the current
 * transaction. Set the  contract  status code to the value from
 * this table.
 *
 * If the  Original  Commencement  Date,  Billing  Frequency  or
 * Billing Channel are  actually changed, call FLAGPRP to ensure
 * the appropriate parts of the proposal are re-visited.
 *
 * Next Program
 * ------------
 *
 * Check each 'selection'  indicator  in  turn.  If an option is
 * selected,  call  GENSSWCH  with  the  appropriate  action  to
 * retrieve the optional program switching:
 *       A - Beneficiary
 *       B - Despatch address
 *       C - Assignee
 *       D - Payor
 *       E - Direct debit details
 *       F - Follow-ups
 *       G - Apply cash
 *       H - Commission split
 *       I - Joint owner
 *       j - Group membership details
 *       K - Anniversary rules
 *
 * For the first one selected, save the next set of programs (8)
 * from the program  stack  and  flag the current positions with
 *  '*'.
 *
 * If a selection  had  been  made  previously, its select field
 * would contain a  '?'.  In  this case, cross check the details
 * according to the rules defined in the initialisation section.
 * Set the selection to  blank  if  there are now no details, or
 * '+' if there are.
 *
 * If a selection  is made (selection field X), load the program
 * stack with the programs returned by GENSSWCH, replace the 'X'
 * in the selection field  with  a '?', add 1 to the pointer and
 * exit.
 *
 * Once all the selections have been serviced, re-load the saved
 * programs onto the stack,  and blank out the '*'. Set the next
 * program name to  the  current  screen name (to re-display the
 * screen).
 *
 * If nothing is  selected,  continue  by just adding one to the
 * program pointer.
 *
 ******************Enhancements for Life Asia 1.0****************
 *
 * Enhancement 1:
 *
 * This enhancement gives the flexibility of defaulting the
 * next billing date to a number of frequencies ahead rather
 * that the option of the risk commencement date or the
 * risk commencement date plus one frequency. The number of
 * frequencies by which the next billing date is to be advanced
 * is specified on T6654. New fields have been added giving the
 * frequencies and the number of frequencies to be added to the
 * risk commencement date to obtain the next billing date.
 * The processing is as follows :
 *
 * - If T6654-RENWDATE-1 is 'Y' then use the risk commencemnt date
 *   as the default next billing date and adjust for DD cases.
 *
 * - If T6654-RENWDATE-2 is 'Y' then find that frequency on
 *   T6654. Calculate the next billing date using DATCON4.
 *   DTC4-INT-DATE-1 is the RCD and DTC4-FREQ-FACTOR is the
 *   number of frequencies that is to be added to the RCD. This
 *   default next billing date can now be adjusted for DD cases.
 *
 * ==============
 * Enhancement 2:
 *
 * This enhancement allows more contract details to be maintained.
 *       Proposal Date
 *       Proposal Received Date
 *       Policy Issue Date
 *       First Issue Date(generated on 1st issue,not change after)
 *       Underwriting Decision Date
 *
 * The Underwriting Decision Date is the date that the cover
 * actually starts, ie the date the company accepted the risk
 * after going through the underwriting process.
 *
 * New file HPADPF is created to keep track of these additional
 * details.
 *
 *****************************************************************
 * </pre>
 */
public class P5004 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5004");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private static final int wsaaAll = 99;
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	protected PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	protected PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0);
	private static final int wsaaMaxOcc = 30;
	private ZonedDecimalData wsaa3MonthsAgo = new ZonedDecimalData(8, 0);
	public ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	protected String wsaaCntcurrMatch = "N";
	protected String wsaaBillcurrMatch = "N";
	protected PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).init(0);
	protected PackedDecimalData wsaaHorizontal = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaOwnerselSave = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaOwnerselStore = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaPayer = new FixedLengthStringData(10);

	private FixedLengthStringData wsaaKill = new FixedLengthStringData(1).init(SPACES);
	private Validator firstTime = new Validator(wsaaKill, "N");
	private PackedDecimalData wsaaCltdod = new PackedDecimalData(8, 0);
	/* WSAA-PLAN */
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaPlanSuff = new FixedLengthStringData(4).isAPartOf(wsaaPlanSuffix, 0, REDEFINE);
	private ZonedDecimalData wsaaPlsuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanSuff, 2).setUnsigned();

	private FixedLengthStringData wsaaMandateType = new FixedLengthStringData(1);
	private Validator creditCardMand = new Validator(wsaaMandateType, "C");
	private Validator bankAccountMand = new Validator(wsaaMandateType, "D");

	private FixedLengthStringData wserOwnersel = new FixedLengthStringData(1);
	private Validator wserOwnerselInvalid = new Validator(wserOwnersel, "Y");

	private FixedLengthStringData wserAgntsel = new FixedLengthStringData(1);
	private Validator wserAgntselInvalid = new Validator(wserAgntsel, "Y");

	private FixedLengthStringData wserCntbranch = new FixedLengthStringData(1);
	private Validator wserCntbranchInvalid = new Validator(wserCntbranch, "Y");

	private FixedLengthStringData wserBilling = new FixedLengthStringData(1);
	protected Validator wserBillingInvalid = new Validator(wserBilling, "Y");
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private FixedLengthStringData wsaaPayorInd = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaAiindInd = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaIndxflgCmp = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaAnniflg = new FixedLengthStringData(1).init(SPACES);
	/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler4 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler4, 0);

	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(32);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(32, 1, wsaaHeading, 0);

	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	private FixedLengthStringData wsaaEndOfMonth = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBillcdOk = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaRendd = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaMopFreqOk = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaRcd = new ZonedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaDate = new FixedLengthStringData(8).isAPartOf(wsaaRcd, 0, REDEFINE);
	private ZonedDecimalData wsaaMth = new ZonedDecimalData(2, 0).isAPartOf(wsaaDate, 4).setUnsigned();
	private ZonedDecimalData wsaaDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaDate, 6).setUnsigned();

	private FixedLengthStringData wsaaT6654Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6654Mop = new FixedLengthStringData(1).isAPartOf(wsaaT6654Key, 0);
	private FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Key, 1);

	private FixedLengthStringData wsaaOccdate = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaOccdateDd = new FixedLengthStringData(2).isAPartOf(wsaaOccdate, 6);
	private ZonedDecimalData wsaaOccdate9 = new ZonedDecimalData(8, 0).isAPartOf(wsaaOccdate, 0, REDEFINE).setUnsigned();

	private FixedLengthStringData wsaaBtdate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaBtdateYyyy = new ZonedDecimalData(4, 0).isAPartOf(wsaaBtdate, 0).setUnsigned();
	private ZonedDecimalData wsaaBtdateMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaBtdate, 4).setUnsigned();
	private ZonedDecimalData wsaaBtdateDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaBtdate, 6).setUnsigned();
	protected ZonedDecimalData wsaaBtdate9 = new ZonedDecimalData(8, 0).isAPartOf(wsaaBtdate, 0, REDEFINE).setUnsigned();

	private FixedLengthStringData wsaaBillcd = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaBillcdYyyy = new ZonedDecimalData(4, 0).isAPartOf(wsaaBillcd, 0).setUnsigned();
	private ZonedDecimalData wsaaBillcdMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillcd, 4).setUnsigned();
	private ZonedDecimalData wsaaBillcdDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillcd, 6).setUnsigned();
	private ZonedDecimalData wsaaBillcd9 = new ZonedDecimalData(8, 0).isAPartOf(wsaaBillcd, 0, REDEFINE).setUnsigned();

	private FixedLengthStringData wsaaDateDue = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaDateYyyy = new ZonedDecimalData(4, 0).isAPartOf(wsaaDateDue, 0).setUnsigned();
	private ZonedDecimalData wsaaDateMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaDateDue, 4).setUnsigned();
	private ZonedDecimalData wsaaDateDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaDateDue, 6).setUnsigned();
	/* WSAA-MOVEMENT */
	private ZonedDecimalData wsaaEarliest = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaNextUp = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaNextDown = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaT5689Count = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsddBillfreq = new FixedLengthStringData(2);
	private FixedLengthStringData wsddMop = new FixedLengthStringData(1);
	private PackedDecimalData wsaaBtdateStored = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaSplitFreq = new ZonedDecimalData(11, 5);
	private FixedLengthStringData wsaaUSA = new FixedLengthStringData(3).init("USA");
	private FixedLengthStringData wsaaSplitFreqx = new FixedLengthStringData(11).isAPartOf(wsaaSplitFreq, 0, REDEFINE);
	private ZonedDecimalData wsaaInteger = new ZonedDecimalData(6, 0).isAPartOf(wsaaSplitFreqx, 0);
	private ZonedDecimalData wsaaDecimal = new ZonedDecimalData(5, 5).isAPartOf(wsaaSplitFreqx, 6);

	protected FixedLengthStringData wsaaOdate = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaOdateYyyy = new FixedLengthStringData(4).isAPartOf(wsaaOdate, 0);
	protected FixedLengthStringData wsaaOdateMm = new FixedLengthStringData(2).isAPartOf(wsaaOdate, 4);
	protected FixedLengthStringData wsaaOdateDd = new FixedLengthStringData(2).isAPartOf(wsaaOdate, 6);
	private FixedLengthStringData wsaaLastAgntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaZsredtrm = new FixedLengthStringData(1);
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private AnrlcntTableDAM anrlcntIO = new AnrlcntTableDAM();
	private AsgnenqTableDAM asgnenqIO = new AsgnenqTableDAM();
	private BnfylnbTableDAM bnfylnbIO = new BnfylnbTableDAM();
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CtrsTableDAM ctrsIO = new CtrsTableDAM();
	private FluplnbTableDAM fluplnbIO = new FluplnbTableDAM();
	private Hpadpf hpadIO = new Hpadpf();
	private InctTableDAM inctIO = new InctTableDAM();
	protected PayrTableDAM payrIO = new PayrTableDAM();
	private PnteTableDAM pnteIO = new PnteTableDAM();
	private TtrcTableDAM ttrcIO = new TtrcTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private T5620rec t5620rec = new T5620rec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private T5689rec t5689rec = new T5689rec();
	protected T6660rec t6660rec = new T6660rec();
	private T6654rec t6654rec = new T6654rec();
	private T6697rec t6697rec = new T6697rec();
	private T5654rec t5654rec = new T5654rec();
	private T3620rec t3620rec = new T3620rec();
	private Th506rec th506rec = new Th506rec();
	private Th605rec th605rec = new Th605rec();
	private Tr52qrec tr52qrec = new Tr52qrec();
	private Tr59xrec tr59xrec = new Tr59xrec();
	private Tr5b2rec tr5b2rec = new Tr5b2rec();//ILIFE-3997
	private Tjl05rec tjl05rec = new Tjl05rec();//ILJ-40
	private T3615rec t3615rec = new T3615rec();
	
	//MIBT-58
	private T1698rec T1698rec = new T1698rec();
	protected Gensswrec gensswrec = new Gensswrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	protected Datcon4rec datcon4rec = new Datcon4rec();
	private Cltrelnrec cltrelnrec = new Cltrelnrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
//	private S5004ScreenVars sv = ScreenProgram.getScreenVars( S5004ScreenVars.class);
	private S5004ScreenVars sv =   getLScreenVars();
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	//MIBT-58 starts
	private String wsaaFound = "";
	private String wsaaFound1 = "";
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(8);
	public ZonedDecimalData mnth1 = DD.mnth.copyToZonedDecimal() ;
	public ZonedDecimalData mnth2 = DD.mnth.copyToZonedDecimal() ;
	public ZonedDecimalData year = DD.year.copyToZonedDecimal() ;
	public ZonedDecimalData datefrm = DD.datefrm.copyToZonedDecimal();
	public ZonedDecimalData dateto = DD.datefrm.copyToZonedDecimal();
	//MIBT-58 ends
	//	MIBT-239 & MIBT-173
	private FixedLengthStringData wsaaPayclt = new FixedLengthStringData(8); //ILIFE-2472
	private ClftpfDAO clftpfdao=new ClftpfDAOImpl();
	private boolean isFatcaAllowed=false;
	private boolean isBancassurance=false;
	private DescDAO descdao =new DescDAOImpl(); 
	private Map<String,Descpf> Fatcastatus;

	private boolean ctaxPermission = false; 
	private boolean multOwnerFlag = false;
	private boolean contDtCalcFlag = false;//ILJ-40
	private boolean superprod=true;
	private Schmpf schmpf=null;
	private SchmpfDAO  schmpfDAO =  getApplicationContext().getBean("schmpfDAO", SchmpfDAO.class);
	private Zctxpf zctxpf=null;
	private ZctxpfDAO zctxpfDAO = getApplicationContext().getBean("zctxpfDAO", ZctxpfDAO.class);
	private boolean mrtaPermission = false;//BRD-139
	private boolean isbillday=false;

	private Rlvrpf rlvrpf=null;
	private RlvrpfDAO rlvrpfDAO = getApplicationContext().getBean("rlvrpfDAO", RlvrpfDAO.class);
	private ClftpfDAO clftpfDAO = getApplicationContext().getBean("clftpfDAO", ClftpfDAO.class);
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class); 
	private Clftpf clft;
	private Fluppf fluppf;
	private String nationality;
	private String birthplace;
	private String usTaxPayor;
	private Tr5awrec tr5awrec = new Tr5awrec();
	private Character natlty;
	private IntegerData wsaaTr5awIx = new IntegerData();
	private static final int wsaaTr5awSize = 1000;
	private WsaaTr5awArrayInner wsaaTr5awArrayInner = new WsaaTr5awArrayInner();
	private Character birthp;
	private boolean statFlag = false;
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList;
	protected Itempf itempf = null;
	protected Descpf descpf;
	protected DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private List<Lifepf> lifeList =new ArrayList<Lifepf>(); 
	private Lifepf lifepf;
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	protected boolean wsaaEof;
	Iterator<Lifepf> iterator ; 
	private List<Itempf> tr5awList = null;
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	protected Clntpf clntpf;
	private Payrpf payrpf = new Payrpf();
	private PayrpfDAO payrDAO = getApplicationContext().getBean("payrDAO", PayrpfDAO.class);
	private List<Payrpf> payrpfList = new ArrayList<Payrpf>();
	private Agntpf agntpf = null;
	private AgntpfDAO agntDAO = getApplicationContext().getBean("agntpfDAO", AgntpfDAO.class);
	private BnkoutpfDAO bnkoutpfDAO = getApplicationContext().getBean("bnkoutpfDAO", BnkoutpfDAO.class);
	private Bnkoutpf bnkoutpf;
	private Bnkoutpf bnkoutpfChk;
	private static final String rrbu = "RRBU";
	private static final String rrbo = "RRB0";
	private static final String rrdo = "RRDO";
	private static final String rrdp = "RRDP";
	private static final String rrgy = "RRGY";
	private static final String rrgz = "RRGZ";
	private static final String rrgx = "RRGX";
	private String Banassurance="BA";
	private String getTellername="BT";
	boolean success = false;
	private ClrfpfDAO clrfpfDAO = getApplicationContext().getBean("clrfpfDAO", ClrfpfDAO.class);
	private Clrfpf clrfpf;
	private List<Clrfpf> clrfpfList = null;
	private Clntpf clts;
	private AglflnbpfDAO aglflnbpfDAO = getApplicationContext().getBean("aglflnbpfDAOP5079",AglflnbpfDAO.class);
	private Aglflnbpf aglflnbpf  = null;
	
	//ILIFE-6590 wli31
	private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	private SoinDAO soinDAO = getApplicationContext().getBean("soinDAO", SoinDAO.class);
	private PcddpfDAO pcddpfDAO = getApplicationContext().getBean("pcddpfDAO", PcddpfDAO.class);
	private MrtapfDAO mrtapfDAO = getApplicationContext().getBean("mrtapfDAO", MrtapfDAO.class);
	private MandpfDAO mandpfDAO = getApplicationContext().getBean("mandpfDAO", MandpfDAO.class);
	private GrpspfDAO grpspfDAO = getApplicationContext().getBean("grpspfDAO", GrpspfDAO.class);
	private Soinpf soinpf = null;
	private Mrtapf mrtapf = null;
	private Mandpf mandlnb = null;
	private Mandpf mandccd = null;
	private List<Fluppf> fluplnbList= null;
	String bnkManagerName = null;
	String bnkTellerName = null;
	protected boolean afiFlag=false; //ILIFE-7743
	private static final String  AFI_FEATURE_ID="NBPRP092"; //ILIFE-7743
	boolean CSMIN004Permission = false;  //ICIL-658
	private boolean nonSuper=false;
	private boolean mopFlag=false;
	private static final String  MOP_FEATURE_ID = "NBPRP011";
	private int count = 0;
	private String billDay = "";
	private List<Integer> billDayList = new ArrayList<Integer>();
	private ZonedDecimalData wsaaOccDate = new ZonedDecimalData(8, 0).setUnsigned(); //ILJ-40
	
	protected FixedLengthStringData wsaaNewDate = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaNewDateYyyy = new FixedLengthStringData(4).isAPartOf(wsaaNewDate, 0); //ILJ-40
	protected FixedLengthStringData wsaaNewDateMm = new FixedLengthStringData(2).isAPartOf(wsaaNewDate, 4);
	protected FixedLengthStringData wsaaNewDateDd = new FixedLengthStringData(2).isAPartOf(wsaaNewDate, 6);
	//ILJ-62
	private boolean NBPRP115Permission = false;
	private NBProposalRec nbproposalRec = new NBProposalRec();
	private ExternalisedRules er = new ExternalisedRules();
	
	SimpleDateFormat integralDateFormat = new SimpleDateFormat("yyyyMMdd");
	
	
	private T5661rec t5661rec = new T5661rec();
	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
	
	 private Antisoclkey antisoclkey = new Antisoclkey();
	 private ZonedDecimalData fupno = new ZonedDecimalData(2, 0).setUnsigned();
	 private ZonedDecimalData fupOldno = new ZonedDecimalData(2, 0).setUnsigned();
	 List<Fluppf> fluppfList;
	 private List<String> followList = new ArrayList<String>();
		
	private String policyOwner="ZOW";
	private AnsopfDAO ansopfDAO = getApplicationContext().getBean("AnsopfDAO", AnsopfDAO.class);
	private Ansopf ansopf = null;
	private List<Ansopf> ansopflist = new ArrayList<Ansopf>();
		
	private static final String t5661 = "T5661";
	private FixedLengthStringData name = new FixedLengthStringData(240);
	private boolean NBPRP117permission = false;
	private boolean onePflag = false; //IBPLIFE-2264
	private static final String  ONE_P_CASHLESS="NBPRP124"; //IBPLIFE-2264
	private ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	private boolean isAllowBillday = false;// IBPLIFE-5785
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	
	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {

		exit1190,
		exit1390,
		exit1690,
		exit3090
	}

	public P5004() {
		super();
		screenVars = sv;
		new ScreenModel("S5004", AppVars.getInstance(), sv);
	}

	protected S5004ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S5004ScreenVars.class);
	}
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}


	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {//TMLII-268
			/* Initialization*/
			scrnparams.version.set(getWsaaVersion());
			wsspcomn.version.set(getWsaaVersion());
			scrnparams.errorline.set(varcom.vrcmMsg);
			//TMLII-268
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}


	/**
	 * <pre>
	 *      INITIALISE FIELDS FOR SHOWING ON SCREEN
	 * </pre>
	 */
	protected void initialise1000()
	{
		/* if returning from an optional selection skip this section.*/
		/* KEEP CHDRLNB AND EXIT.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*") && isEQ(wsspcomn.flag, "C")) {
			//goTo(GotoLabel.exit1090);
			calcRskCommDate();
			wsspcomn.typinj.set(SPACES);
			return;
		}
		/* First time in only, read accounting rules for transaction*/
		/*                     look up the singed-on branch description*/
		/*                     get todays date*/
		/*                     work out the date three months ago*/
		wsaaNextUp.set(99);
		wsaaNextDown.set(ZERO);
		wsaaPayclt.set(SPACES); //ILIFE-2472
		if(isEQ(sv.ownersel, SPACES)){
			wsspcomn.chdrCownnum.set(SPACES);
		}
		/* Release the PAYR record, as if program has previously           */
		/* been cancelled the lock will remain, causing WRITR to fail.     */
		payrIO.setFunction(varcom.rlse);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/* Always reset WSAA-BATCKEY with the current WSSP-BATCHKEY *      */
		/*    IF WSAA-BATCKEY NOT = SPACES                                 */
		/*       GO TO 1020-BLANK-OUT-FIELDS.                              */
		ctaxPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPROP07", appVars, "IT"); //ALS-313
		multOwnerFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP078", appVars, "IT");//ALS-4555
		contDtCalcFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP113", appVars, "IT");//ILJ-40
		NBPRP115Permission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP115", appVars, "IT");//ILJ-62
		NBPRP117permission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP117", appVars, "IT"); 
		onePflag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), ONE_P_CASHLESS, appVars, "IT"); //IBPLIFE-2264
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		wsaaBatckey.set(wsspcomn.batchkey);

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.t5645.toString());
		itempf.setItemitem(wsaaProg.toString());
		itempf = itempfDAO.getItempfRecord(itempf);
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));

		/*  Get Underwriting Decision indicator                            */

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.th605.toString());
		itempf.setItemitem(wsspcomn.company.toString());
		itempf = itempfDAO.getItempfRecord(itempf);
		th605rec.th605Rec.set(StringUtil.rawToString(itempf.getGenarea()));


		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		datcon2rec.intDatex1.set(datcon1rec.intDate);
		datcon2rec.frequency.set("12");
		datcon2rec.freqFactor.set(-3);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaToday.set(datcon1rec.intDate);
		/* IFSU-1202 by nnaveenkumar */
		isFatcaAllowed= FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "CLMTN008", appVars, "IT");
		if(!isFatcaAllowed){
			sv.fatcastatusFlag.set("Y");
		}
		else{
			sv.fatcastatusFlag.set("N");
		}
		/* IFSU-1202 by nnaveenkumar */
		mrtaPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP010", appVars, "IT");
		isbillday = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP011", appVars, "IT");

		isBancassurance = FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "AGMTN015", appVars, "IT");
		//ILIFE-8544 Start
		if(isBancassurance)
			sv.bancAssuranceFlag.set("Y");
		else
			sv.bancAssuranceFlag.set("N");
		//ILIFE-8544 End
		
		
		blankOutFields1020();
		//ICIL-658 start
        CSMIN004Permission  = FeaConfg.isFeatureExist("2", "CSMIN004", appVars, "IT");
        if(!CSMIN004Permission) {
        	sv.rfundflgOut[varcom.nd.toInt()].set("Y");
        }
        if(!onePflag) {
        	sv.rskComOptOut[Varcom.nd.toInt()].set("Y");
        }
        if(onePflag && isNE(sv.mop,"D")) {
        	sv.rskComOptOut[Varcom.pr.toInt()].set("Y");
        }
	}
	
	protected void readschmnme(){

		schmpf=schmpfDAO.getSchmpfRecord(sv.schmno.toString());
		if(schmpf!=null){
			sv.schmnme.set(schmpf.getSchmNme());
		}
		else{
			sv.schmnoErr.set(errorsInner.rfx9);
		}
	}
	protected void checkMultOwner()
	{
		if(isNE(chdrlnbIO.getCownnum2(),SPACE) || isNE(chdrlnbIO.getCownnum3(),SPACE) || isNE(chdrlnbIO.getCownnum4(),SPACE) || isNE(chdrlnbIO.getCownnum5(),SPACE)) 
			sv.zmultOwner.set("+");
		else
			sv.zmultOwner.set(SPACES);		
	}
	protected void m200checkzctx(){
		zctxpf=zctxpfDAO.readRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		if(zctxpf!=null)
			sv.zctaxind.set("+");
		else
			sv.zctaxind.set("X");
	}

	protected void checkRollover(){
		rlvrpf = rlvrpfDAO.readRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		if(rlvrpf!=null)
			sv.zroloverind.set("+");
		else
			sv.zroloverind.set(SPACES);
	}
	protected void blankOutFields1020()
	{
		sv.dataArea.set(SPACES);
		if (isEQ(th605rec.uwind, "Y")) {
			sv.huwdcdteOut[varcom.pr.toInt()].set("Y");
		}
		sv.billcd.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		if(contDtCalcFlag){
			sv.rskcommdate.set(datcon1rec.intDate);//ILJ-40
			sv.decldate.set(varcom.vrcmMaxDate);//ILJ-40
			sv.concommflg.set(SPACES);//ILJ-40
		}
		
		if(NBPRP115Permission) {
			sv.premRcptDate.set(varcom.vrcmMaxDate);
			sv.cnfirmtnIntentDate.set(varcom.vrcmMaxDate);
			sv.ownerOccupation.set(SPACES);
		}
		sv.plansuff.set(ZERO);
		sv.ttmprcdte.set(varcom.vrcmMaxDate);
		sv.incomeSeqNo.set(ZERO);
		sv.reqntype.set("0"); //ILIFE-2472
		sv.billday.set(SPACES);
		wsaaIndxflgCmp.set(SPACES);
		wserOwnersel.set("Y");
		wserAgntsel.set("Y");
		wserCntbranch.set("Y");
		wserBilling.set("Y");
		wsaaOwnerselSave.set(SPACES);
		wsaaLastAgntnum.set(SPACES);
		sv.refundOverpay.set("N");// fwang3 ICIL-4
		sv.custrefnum.set(SPACES);//ILIFE-8583
		sv.custrefnumOut[varcom.nd.toInt()].set("Y");//ILIFE-8583
		/* MOVE SPACES                 TO DESC-DATA-KEY.                */
		/* MOVE 'IT'                   TO DESC-DESCPFX.                 */
		/* MOVE WSSP-COMPANY           TO DESC-DESCCOY.                 */
		/* MOVE T1692                  TO DESC-DESCTABL.                */
		/* MOVE WSSP-BRANCH            TO DESC-DESCITEM,                */
		/*                                S5004-CNTBRANCH.              */
		/* MOVE WSSP-LANGUAGE          TO DESC-LANGUAGE.                */
		/* MOVE READR                  TO DESC-FUNCTION.                */
		/* CALL 'DESCIO' USING DESC-PARAMS.                             */
		/* IF DESC-STATUZ              NOT = O-K                        */
		/*                         AND NOT = MRNF                       */
		/*     MOVE DESC-PARAMS        TO SYSR-PARAMS                   */
		/*     PERFORM 600-FATAL-ERROR.                                 */
		/* IF DESC-STATUZ              = MRNF                           */
		/*     MOVE ALL '?'            TO S5004-SBRDESC                 */
		/* ELSE                                                         */
		/*     MOVE DESC-LONGDESC      TO S5004-SBRDESC.                */
		/* Retrieve contract fields from I/O module*/
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		/*IBPLIFE-12973 STARTS*/
		if(isNE(chdrlnbIO.getBillcd(),0))
				sv.billcd.set(chdrlnbIO.getBillcd());	/*IBPLIFE-12973 ENDS*/
		/*MOVE CHDRLNB-COWNNUM        TO WSAA-OWNERSEL-STORE.     <012>*/
		wsaaOwnerselStore.set(chdrlnbIO.getCownnum());
		wsaaPayer.set(chdrlnbIO.getCownnum());
		if (isNE(chdrlnbIO.getOccdate(), ZERO)) {
			sv.occdate.set(chdrlnbIO.getOccdate());
		}
		if(onePflag)
			sv.rskComOpt.set(chdrlnbIO.getRcopt());
		t5688rec.polmin.set(ZERO);
		/*  Get the policy dispatch details.                               */
		readTr52q1200();
		sv.dlvrmode.set(tr52qrec.dlvrmode01);
		/* Retrieve contract type from T5688*/
		/*       validation rules from T5689*/
		readContractTables1600();
		if (isNE(sv.occdateErr, SPACES)) {
			wserBilling.set("Y");
		}


		descpf=descDAO.getdescData("IT", tablesInner.t5688.toString(), chdrlnbIO.getCnttype().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf==null) 
			wsaaHedline.fill("?");
		else 
			wsaaHedline.set(descpf.getLongdesc());	

		loadHeading1100();
		if(mrtaPermission){
			m200CheckReducingTerm();
			sv.zsredtrmOut[varcom.pr.toInt()].set("Y");
			if (isEQ(wsaaZsredtrm, "A") || (isEQ(wsaaZsredtrm,"M"))) {
				sv.zsredtrmOut[varcom.pr.toInt()].set("N");
			}
		}

		descpf=descDAO.getdescData("IT", tablesInner.t3623.toString(), chdrlnbIO.getStatcode().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf==null) 
			sv.statdsc.fill("?");
		else
			sv.statdsc.set(descpf.getLongdesc());

        //ILIFE-6467
		descpf=descDAO.getdescData("IT", tablesInner.t3588.toString(), chdrlnbIO.getPstatcode().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf==null) 
			sv.premStatDesc.fill("?");
		else
			sv.premStatDesc.set(descpf.getLongdesc());	

		if(!isbillday){
			sv.billdayOut[varcom.nd.toInt()].set("Y");
		}
		if(!ctaxPermission || isNE(sv.mop,"S")) {
			sv.zroloverindOut[varcom.nd.toInt()].set("Y");
		}else {
			sv.zroloverindOut[varcom.nd.toInt()].set(" ");
		}
		/*ILJ-40 : Start*/
		if(!contDtCalcFlag) {
			sv.concommflgOut[varcom.nd.toInt()].set("Y");
			sv.rskcommdateOut[varcom.nd.toInt()].set("Y");
			sv.decldateOut[varcom.nd.toInt()].set("Y");
		}
		/*ILJ-40 : End*/
		/*ILJ-42 : Start*/
		if(contDtCalcFlag) {			
			sv.rskcommdateOut[varcom.pr.toInt()].set("Y");			
		}
		/*ILJ-42 : End*/
		//ILJ-62 
		if(!NBPRP115Permission) {
			sv.premRcptDateOut[varcom.nd.toInt()].set("Y");
			sv.cnfirmtnIntentDateOut[varcom.nd.toInt()].set("Y");
			sv.ownerOccupationOut[varcom.nd.toInt()].set("Y");		
			
		}
		
		if(NBPRP115Permission) {			
			sv.premRcptDateOut[varcom.pr.toInt()].set("Y");			
		}
		/* If this is not a personal pension contract (ie An entry         */
		/* does not exist on T6697 for this contract type) then            */
		/* non display the source of income field.                         */




		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.t6697.toString());
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf = itempfDAO.getItempfRecord(itempf);


		if (itempf == null) {
			sv.incseqnoOut[varcom.nd.toInt()].set("Y");
			sv.incseqnoOut[varcom.pr.toInt()].set("Y");
			sv.incomeSeqNo.set(ZERO);
		}
		if (itempf != null) {
			t6697rec.t6697Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			t6697rec.t6697Rec.set(SPACES);
		}
		/* If the Optional Automatic Increase indicator set to             */
		/* Optional (not Yes or NO) then this field will appear.           */



		itempfList = itempfDAO.getItdmByFrmdate(wsspcomn.company.toString(),tablesInner.t5654.toString(),chdrlnbIO.getCnttype().toString(),sv.occdate.toInt());


		if (itempfList.size() >= 1 )

			t5654rec.t5654Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));

		else {
			sv.indxflgErr.set(errorsInner.h501);
		}
		/* IF  T5654-INDXFLG      NOT   = 'O'                      <040>*/
		/*     MOVE 'Y'                TO S5004-INDXFLG-OUT(PR)    <040>*/
		/*     MOVE 'Y'                TO S5004-INDXFLG-OUT(ND)    <040>*/
		/*     MOVE SPACES             TO S5004-INDXFLG            <040>*/
		/* ELSE                                                    <040>*/
		/*     MOVE 'N'                TO S5004-INDXFLG.           <040>*/

		// the following commented code has been paste after the line no 1058 regarding #ILIFE - 4647
		/*from
		sv.indxflg.set(SPACES);
		if (isNE(t5654rec.indxflg, "O")) {	
			sv.indxflgOut[varcom.pr.toInt()].set("Y");
			sv.indxflgOut[varcom.nd.toInt()].set("Y");
		} else {		
			sv.indxflg.set("Y");
		}

	//ILIFE-1900 starts by slakkala 
		if(isEQ(t5654rec.indxflg,"Y")){
			sv.indxflg.set("Y");			
		}
	/*ILIFE-1900 ends */	
		/*end */

		/*  Read Anniversary Rules to check if product exists. If yes      */
		/*  Anniversary window is displayed. */



		itempfList = itempfDAO.getItdmByFrmdate(wsspcomn.company.toString(),tablesInner.t5604.toString(),chdrlnbIO.getCnttype().toString(),sv.occdate.toInt());

		if (itempfList.size() >= 1 ) {
			wsaaAnniflg.set("Y");
		}
		else {
			wsaaAnniflg.set(SPACES);
			sv.aiind.set(SPACES);
			sv.aiindOut[varcom.pr.toInt()].set("Y");
			sv.aiindOut[varcom.nd.toInt()].set("Y");
		}

		inctIO.setChdrnum(chdrlnbIO.getChdrnum());
		inctIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		inctIO.setFunction(varcom.readr);
		inctIO.setFormat(formatsInner.inctrec);
		SmartFileCode.execute(appVars, inctIO);

		// #ILIFE-4647 C@checked U@Unchecked

		sv.indxflg.set(SPACES);
		if (isNE(t5654rec.indxflg, "O")) {
			sv.indxflgOut[varcom.pr.toInt()].set("Y");
			sv.indxflgOut[varcom.nd.toInt()].set("Y");
		} else if(isEQ(t5654rec.indxflg,"O") && isEQ(inctIO.getStatuz(),varcom.mrnf)){
			sv.indxflg.set("U");			
		}else if(isEQ(t5654rec.indxflg,"O") && isNE(inctIO.getStatuz(),varcom.mrnf)){
			sv.indxflg.set("C");	
		}

		//ILIFE-1900 starts by slakkala 
		if(isEQ(t5654rec.indxflg,"Y")){
			sv.indxflg.set("C");			
		}

		/*ILIFE-1900 ends */


		if (isNE(inctIO.getStatuz(), varcom.oK)
				&& isNE(inctIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(inctIO.getParams());
			fatalError600();
		}	

		if (isEQ(inctIO.getStatuz(), varcom.oK) && isNE(t5654rec.indxflg,"N")) {
			/*    MOVE 'Y'                TO S5004-INDXFLG.            <040>*/
			sv.indxflg.set("C");
		}else{
			sv.indxflg.set("U");
		}

		//check its new proposal and O related
		if(isEQ(t5654rec.indxflg,"O") && isEQ(chdrlnbIO.getOccdate(), ZERO)){
			sv.indxflg.set("C");			
		}


		/* IF  T5654-INDXFLG            = 'Y'                      <040>*/
		/*     MOVE 'Y'                TO S5004-INDXFLG.           <040>*/
		if (isEQ(wsaaAnniflg, "Y")
				&& isNE(wsspcomn.flag, "C")) {
			/*NEXT_SENTENCE*/
		}
		else {
			/* IF  T5654-INDXFLG           NOT = SPACE                 <053>*/
			if (isEQ(t5654rec.indxflg, "Y")) {
				sv.indxflg.set("C");
			}
		}
		/*   Check to see if plan processing is applicable.*/
		if (isEQ(t5688rec.polmin, t5688rec.polmax)) {
			if (isNE(t5688rec.polmin, ZERO)) {
				sv.plansuff.set(t5688rec.polmin);
				sv.plansuffOut[varcom.pr.toInt()].set("Y");
			}
		}
		/*      MOVE T5688-POLMIN TO S5004-POLINC                      */
		/*    MOVE 'Y'                TO S5004-POLINC-OUT(PR)          */
		if (isEQ(t5688rec.polmin, ZERO)) {
			sv.plansuffOut[varcom.nd.toInt()].set("Y");
			sv.plansuffOut[varcom.pr.toInt()].set("Y");
			/******    MOVE 'Y'                TO S5004-POLINC-OUT(ND)          */
			/******    MOVE 'Y'                TO S5004-POLINC-OUT(PR)          */
		}
		else {
			if (isEQ(chdrlnbIO.getPolinc(), ZERO)) {
				sv.plansuff.set(t5688rec.poldef);
			}
		}
		/*       MOVE T5688-POLDEF    TO S5004-POLINC.                 */
		/*   Set up additional new fields.                                 */
		sv.znfopt.set(t5688rec.znfopt);
		sv.hprrcvdt.set(datcon1rec.intDate);
		blnkFld1020CustomerSpecific();
		sv.hpropdte.set(varcom.vrcmMaxDate);
		sv.huwdcdte.set(varcom.vrcmMaxDate);
		sv.hoissdte.set(varcom.vrcmMaxDate);
		/*    Read table TH506 to determine, whether dates about the       */
		/*    proposal is to be mandatory and the latest date acceptable   */
		/*    for back dated proposals                                     */





		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.th506.toString());
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf = itempfDAO.getItempfRecord(itempf);


		if (itempf == null) {
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(tablesInner.th506.toString());
			if (itempf.getItemitem() != "***") {
				itempf.setItemitem("***");

				itempf = itempfDAO.getItempfRecord(itempf);
			}
		}
		if (itempf != null) {
			th506rec.th506Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			th506rec.th506Rec.set(SPACES);
		}
		/*Reading TR59X for checking the product type is
		 * Superannuation or not //ALS-313*/

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.tr59x.toString());
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf.setItmfrm(sv.occdate.getbigdata());
		itempf.setItmto(sv.occdate.getbigdata());
		
		itempfList = itempfDAO.findByItemDates(itempf);
		if(itempfList.isEmpty() || !ctaxPermission || !itempfList.get(0).getValidflag().equals("1")){ //ILIFE-6971
			sv.zctaxindOut[varcom.nd.toInt()].set("Y");
			sv.schmnoOut[varcom.nd.toInt()].set("Y");
			superprod=false;
		}
		else{
			tr59xrec.tr59xrec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));		
			}
		if(superprod && isEQ(chdrlnbIO.getOccdate(), ZERO)){
			sv.zctaxind.set("X");
			if(isNE(tr59xrec.defaultScheme,SPACES)){
				sv.schmno.set(tr59xrec.defaultScheme);
				readschmnme();
				if(isNE(tr59xrec.otherscheallowed,"Y"))
					sv.schmnoOut[varcom.pr.toInt()].set("Y");}
			else
				sv.schmno.set(SPACES);
		}
		//ALS-4555 starts
		if(superprod){
			sv.multipleOwnerFlag.set("Y");
		}
		else{
			if(multOwnerFlag) {
				sv.multipleOwnerFlag.set("N");
			}
			else
				sv.multipleOwnerFlag.set("Y");
		}
		//ALS-4555 ends
		//ILIFE-3997 Starts
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.tr5b2.toString());
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf = itempfDAO.getItempfRecord(itempf);


		if(itempf != null){
			tr5b2rec.tr5b2Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		if (isEQ(tr5b2rec.nlg, "Y")) {

			if (isEQ(chdrlnbIO.getNlgflg(), SPACES)) {

				if (isEQ(wsspcomn.flag, "C")) {
					sv.nlgflg.set(tr5b2rec.nlg);
				} else if (isEQ(wsspcomn.flag, "M") || isEQ(wsspcomn.flag, "I")) {
					sv.nlgflg.set(chdrlnbIO.getNlgflg().toString().trim());
				}

			}else{
				sv.nlgflg.set(chdrlnbIO.getNlgflg().toString().trim());
			}

		} else {
			sv.nlgflg.set("N");
			sv.nlgflgOut[varcom.pr.toInt()].set("Y");
		}
		//ILIFE-3997 End
		/*   If in enquiry mode, protect fields*/
		if (isEQ(wsspcomn.flag, "I")) {
			sv.ttmprcdteOut[varcom.pr.toInt()].set("Y");
			sv.ttmprcnoOut[varcom.pr.toInt()].set("Y");
			sv.dlvrmodeOut[varcom.pr.toInt()].set("Y");
			sv.ownerselOut[varcom.pr.toInt()].set("Y");
			sv.billfreqOut[varcom.pr.toInt()].set("Y");
			sv.mopOut[varcom.pr.toInt()].set("Y");
			sv.plansuffOut[varcom.pr.toInt()].set("Y");
			sv.agntselOut[varcom.pr.toInt()].set("Y");
			/*   MOVE 'Y'                TO S5004-CAMPAIGN-OUT(PR).       */
			sv.campaignOut[varcom.pr.toInt()].set("Y");
			sv.indxflgOut[varcom.pr.toInt()].set("Y");
			sv.incseqnoOut[varcom.pr.toInt()].set("Y");
			sv.huwdcdteOut[varcom.pr.toInt()].set("Y");
			sv.reqntypeOut[varcom.pr.toInt()].set("Y"); //ILIFE-2472
			sv.schmnoOut[varcom.pr.toInt()].set("Y");//ALS-318
			sv.billdayOut[varcom.pr.toInt()].set("Y");
			sv.nlgflgOut[varcom.pr.toInt()].set("Y");//ILIFE-3997
			sv.refundOverpayOut[varcom.pr.toInt()].set("Y");//fwang3 China localization
			sv.bnkoutOut[varcom.pr.toInt()].set("Y");//ICIL-147
			sv.bnktelOut[varcom.pr.toInt()].set("Y");//ICIL-147
			if(contDtCalcFlag){
			sv.concommflgOut[varcom.pr.toInt()].set("Y");//ILJ-40
			}
			//ILJ-62
			if(NBPRP115Permission) {
				sv.cnfirmtnIntentDateOut[varcom.pr.toInt()].set("Y");
				sv.ownerOccupationOut[varcom.pr.toInt()].set("Y");
			}
			if(onePflag) {
				sv.rskComOptOut[Varcom.pr.toInt()].set("Y");//IBPLIFE-2264
				sv.concommflgOut[Varcom.pr.toInt()].set("Y");
				sv.decldateOut[Varcom.pr.toInt()].set("Y");
			}
		}
		
		setFieldsCustomerSpecific();
		/*   Check for any follow-ups.*/
		//ILIFE-6590 wli31
		fluplnbList = fluppfDAO.getFluplnbRecordForPerformance(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
		if (fluplnbList == null || fluplnbList.size() <= 0) {
			//goTo(GotoLabel.checkOccdate1040);
			checkOccdate1040();
			return;
		}
		/*  There are some follow-ups*/
		sv.fupflg.set("+");
		checkOccdate1040();
	}
	
	protected void blnkFld1020CustomerSpecific(){
		sv.occdate.set(datcon1rec.intDate);
	}

	protected void loadTr5aw1500(List<Itempf> tr5awList)

	{

		wsaaTr5awIx.set(1);
		for (Itempf itempf : tr5awList) {
			tr5awrec.tr5awRec.set(StringUtil.rawToString(itempf.getGenarea()));
			wsaaTr5awArrayInner.wsaacngfrmtaxpyr[wsaaTr5awIx.toInt()].set(tr5awrec.cngfrmtaxpyr);
			wsaaTr5awArrayInner.wsaacngfrmbrthctry[wsaaTr5awIx.toInt()].set(tr5awrec.cngfrmbrthctry);
			wsaaTr5awArrayInner.wsaacngfrmnatlty[wsaaTr5awIx.toInt()].set(tr5awrec.cngfrmnatlty);
			wsaaTr5awArrayInner.wsaacngtotaxpyr[wsaaTr5awIx.toInt()].set(tr5awrec.cngtotaxpyr);
			wsaaTr5awArrayInner.wsaacngtobrthctry[wsaaTr5awIx.toInt()].set(tr5awrec.cngtobrthctry);
			wsaaTr5awArrayInner.wsaacngtonatlty[wsaaTr5awIx.toInt()].set(tr5awrec.cngtonatlty);
			wsaaTr5awArrayInner.wsaaFupCde[wsaaTr5awIx.toInt()].set(itempf.getItemitem());
			wsaaTr5awIx.add(1);
		}

	}

	/**
	 * <pre>
	 *    Check if contract being set up for first time
	 *    Default the branch from the WSSP.
	 * </pre>
	 */
	protected void checkOccdate1040()
	{
		if (isEQ(chdrlnbIO.getOccdate(), ZERO)) {
			payrIO.setParams(SPACES);
			wsaaKill.set("N");
			sv.cntbranch.set(wsspcomn.branch);
			return ;
		}
		else {
			wsaaKill.set("Y");
		}
		/*  Contract not being set up for first time.                    **/
		wserOwnersel.set(SPACES);
		wserAgntsel.set(SPACES);
		wserCntbranch.set(SPACES);
		wserBilling.set(SPACES);
		/*  Load all fields from the Contract Header to the Screen*/
		sv.plansuff.set(chdrlnbIO.getPolinc());
		sv.ownersel.set(chdrlnbIO.getCownnum());
		if(contDtCalcFlag){
		sv.concommflg.set(chdrlnbIO.getConcommflg());//ILJ-40
		}
		//ILJ-62
		if(NBPRP115Permission) {
			sv.ownerOccupation.set(chdrlnbIO.getOwneroccup());
		}
		wsaaOwnerselStore.set(chdrlnbIO.getCownnum());
		sv.schmno.set(chdrlnbIO.getSchmno());
		/*                               WSAA-OWNERSEL-STORE.     <012>*/
		sv.occdate.set(chdrlnbIO.getOccdate());
		/*  MOVE CHDRLNB-BILLFREQ       TO S5004-BILLFREQ.               */
		/*  MOVE CHDRLNB-BILLCHNL       TO S5004-MOP.                    */
		/*  MOVE CHDRLNB-BILLCD         TO S5004-BILLCD.                 */
		/*  MOVE CHDRLNB-CNTCURR        TO S5004-CNTCURR.                */
		/*  MOVE CHDRLNB-BILLCURR       TO S5004-BILLCURR.               */
		sv.register.set(chdrlnbIO.getRegister());
		sv.srcebus.set(chdrlnbIO.getSrcebus());
		sv.reptype.set(chdrlnbIO.getReptype());
		sv.lrepnum.set(chdrlnbIO.getRepnum());
		sv.cntbranch.set(chdrlnbIO.getCntbranch());
		sv.agntsel.set(chdrlnbIO.getAgntnum());
		sv.campaign.set(chdrlnbIO.getCampaign());
		sv.stcal.set(chdrlnbIO.getChdrstcda());
		sv.stcbl.set(chdrlnbIO.getChdrstcdb());
		sv.stccl.set(chdrlnbIO.getChdrstcdc());
		sv.stcdl.set(chdrlnbIO.getChdrstcdd());
		sv.stcel.set(chdrlnbIO.getChdrstcde());
		sv.custrefnum.set(chdrlnbIO.getCustrefnum());//ILIFE-8583
		//ILIFE-2472 Starts
		sv.reqntype.set(chdrlnbIO.getReqntype());


		if(isFatcaAllowed){
			Fatcastatus= descdao.getItems("IT", wsspcomn.fsuco.toString(), "TR2DH", wsspcomn.language.toString());
			if(Fatcastatus.size() > 0) {
				if(isEQ(th506rec.fatcaFlag,"Y"))	{
					Clftpf clftValue=clftpfdao.getRecordByClntnum("CN", wsspcomn.fsuco.toString(), chdrlnbIO.getCownnum().toString());
					if(clftValue==null){

						sv.fatcastatus.set(Fatcastatus.get("NUSWOI").getDescitem());
						sv.fatcastatusdesc.set(Fatcastatus.get("NUSWOI").getLongdesc());

					}else{
						sv.fatcastatus.set(clftValue.getFfsts().trim());
						sv.fatcastatusdesc.set(Fatcastatus.get(clftValue.getFfsts().trim()).getLongdesc());
					}
				}
				else{
					sv.fatcastatus.set(Fatcastatus.get("N/A").getDescitem());
					sv.fatcastatusdesc.set(Fatcastatus.get("N/A").getLongdesc());
				}
			}
		}
		if (isEQ(chdrlnbIO.getZmandref(), SPACES)) {
			sv.zdpind.set(SPACES);
			sv.zdcind.set(SPACES);
		} else {
			if (isEQ(chdrlnbIO.getReqntype(),"C")) {
				sv.zdcind.set("+");
			}
			if (isEQ(chdrlnbIO.getReqntype(),"4")) {
				sv.zdpind.set("+");
			}
		}
		//ILIFE-2472 Ends



		descpf=descDAO.getdescData("IT", tablesInner.t1692.toString(), chdrlnbIO.getCntbranch().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.sbrdesc.fill("?");
		}
		else {
			sv.sbrdesc.set(descpf.getLongdesc());
		}



		/*   Get any other details from HPADPF.                    <CAS2.0>*/
		/*   Non-forfeiture option is also stored in HPADPF,               */
		/*   if HPAD not exist, default from T5688.                        */
		hpadIO = this.hpadpfDAO.getHpadData(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());

		if(hpadIO == null){
			sv.znfopt.set(t5688rec.znfopt);
			sv.hpropdte.set(varcom.vrcmMaxDate);
			sv.hprrcvdt.set(varcom.vrcmMaxDate);
			sv.huwdcdte.set(varcom.vrcmMaxDate);
			sv.dlvrmode.set(tr52qrec.dlvrmode01);
			sv.refundOverpay.set("N");// fwang3 ICIL-4
			if(contDtCalcFlag){
			sv.rskcommdate.set(varcom.vrcmMaxDate);//ILJ-40
			sv.decldate.set(varcom.vrcmMaxDate);//ILJ-40
			}
			//ILJ-62
			if(NBPRP115Permission) {
				sv.cnfirmtnIntentDate.set(varcom.vrcmMaxDate);
			}
		}
		else {
			if (null == hpadIO.getRefundOverpay() || isEQ(hpadIO.getRefundOverpay(), SPACES)) {//fwang3 ICIL-4 for fix null pointer defect
				sv.refundOverpay.set("N");
			} else {
				sv.refundOverpay.set(hpadIO.getRefundOverpay());// fwang3 ICIL-4
			}
			sv.znfopt.set(hpadIO.getZnfopt());
			if (isEQ(wsspcomn.flag, "P")) {
				sv.znfopt.set(t5688rec.znfopt);
			}
			sv.hpropdte.set(hpadIO.getHpropdte());
			sv.hprrcvdt.set(hpadIO.getHprrcvdt());
			sv.hoissdte.set(hpadIO.getHoissdte());
			/*ILJ-42 : Start*/
			if(contDtCalcFlag) {		
				sv.decldate.set(hpadIO.getDecldate());//ILJ-40	
				if(onePflag && (isEQ(wsspcomn.flag, "I") || isEQ(wsspcomn.flag, "M") || isEQ(wsspcomn.flag, "C"))){
					sv.rskcommdate.set(hpadIO.getRskcommdate());
					sv.premRcptDate.set(hpadIO.getFirstprmrcpdate());
				}
				else {
					calcRskCommDate();//ILJ-42
					wsspcomn.typinj.set(SPACES);
				}
			}
			/*ILJ-42 : End*/
			//ILJ-62
			if(NBPRP115Permission) {
				sv.cnfirmtnIntentDate.set(hpadIO.getCnfirmtnIntentDate());
			}
			
			if (null == hpadIO.getDlvrmode()
					||(isEQ(hpadIO.getDlvrmode(), SPACES) 
							|| isEQ(wsspcomn.flag, "P"))) {
				sv.dlvrmode.set(tr52qrec.dlvrmode01);
			}
			else {
				sv.dlvrmode.set(hpadIO.getDlvrmode());
			}
			sv.huwdcdte.set(hpadIO.getHuwdcdte());
		}
		
		setFieldsCustomerSpecific();
		
		/* Get Temporary receipt number and date.                          */
		ttrcIO.setParams(SPACES);
		ttrcIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		ttrcIO.setChdrnum(chdrlnbIO.getChdrnum());
		ttrcIO.setEffdate(varcom.vrcmMaxDate);
		ttrcIO.setFunction(varcom.begn);
		ttrcIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ttrcIO.setFitKeysSearch("CHDRCOY" , "CHDRNUM");
		ttrcIO.setFormat(formatsInner.ttrcrec);
		SmartFileCode.execute(appVars, ttrcIO);
		if (isNE(ttrcIO.getStatuz(), varcom.oK)
				&& isNE(ttrcIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(ttrcIO.getStatuz());
			syserrrec.params.set(ttrcIO.getParams());
			fatalError600();
		}
		if (isEQ(ttrcIO.getStatuz(), varcom.endp)
				|| isNE(ttrcIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
				|| isNE(ttrcIO.getChdrnum(), chdrlnbIO.getChdrnum())) {
			sv.ttmprcno.set(SPACES);
			sv.ttmprcdte.set(varcom.vrcmMaxDate);
		}
		else {
			sv.ttmprcno.set(ttrcIO.getTtmprcno());
			sv.ttmprcdte.set(ttrcIO.getTtmprcdte());
		}
		/* Load the billing details from the payr file.                    */
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(sv.chdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction("READR");
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		sv.incomeSeqNo.set(payrIO.getIncomeSeqNo());
		if (isEQ(wsspcomn.flag, "P")) {
			if (isEQ(wsddBillfreq, "??")) {
				sv.billfreq.set(payrIO.getBillfreq());
			}
			if (isEQ(wsddMop, "?")) {
				sv.mop.set(payrIO.getBillchnl());
			}
		}
		else {
			sv.billfreq.set(payrIO.getBillfreq());
			sv.mop.set(payrIO.getBillchnl());
		}
		/* MOVE PAYR-BILLFREQ          TO S5004-BILLFREQ.       <V74L05>*/
		/* MOVE PAYR-BILLCHNL          TO S5004-MOP.            <V74L05>*/
//		sv.billcd.set(payrIO.getBillcd());	//IBPLIFE-6341
		if(isbillday)//ILIFE-8583
			sv.billday.set(payrIO.getBillday());
		sv.cntcurr.set(payrIO.getCntcurr());
		sv.billcurr.set(payrIO.getBillcurr());
		if(superprod){
			if(isEQ(sv.schmno,SPACES))
				sv.schmno.set(tr59xrec.defaultScheme);
			readschmnme();
		}
		/*   Look up all other descriptions*/
		/*       - owner name*/
		/*       - branch name*/
		/*       - agent name*/
		formatClientName1700();
		if (isNE(sv.ownerselErr, SPACES)) {
			wserOwnersel.set("Y");
		}
		if (isNE(sv.cntbranchErr, SPACES)) {
			wserCntbranch.set("Y");
		}
		formatAgentName1900();
		if (isNE(sv.agntselErr, SPACES)) {
			wserAgntsel.set("Y");
		}
		/* Keep the on-screen agent number in working storage.          */
		wsaaLastAgntnum.set(sv.agntsel);
		/*   Check for lifes assured on other proposals.*/
		/*MOVE SPACES                TO LIFELNB-DATA-KEY.             */
		/*MOVE WSSP-COMPANY          TO LIFELNB-CHDRCOY.              */
		/*MOVE CHDRLNB-CHDRNUM       TO LIFELNB-CHDRNUM.              */
		/*MOVE BEGN                  TO LIFELNB-FUNCTION.             */
		/* CALL 'LIFELNBIO'  USING LIFELNB-PARAMS.                      */
		/*IF (LIFELNB-STATUZ          NOT = O-K) AND                   */
		/*(LIFELNB-STATUZ          NOT = ENDP)                      */
		/*MOVE LIFELNB-PARAMS     TO SYSR-PARAMS                   */
		/*PERFORM 600-FATAL-ERROR.                                 */
		/*    MOVE 'N'                    TO S5004-CONPROSAL.              */
		sv.conprosal.set(SPACES);
		//lfcllnbIO.setStatuz(varcom.oK);
		while ( !((isEQ(wsaaEof, true))
				|| (isNE(sv.conprosal, SPACES)))) {
			countLives1300();
		}

		/*(LIFELNB-CHDRCOY    NOT = WSSP-COMPANY) OR               */
		/*(LIFELNB-CHDRNUM    NOT = CHDRLNB-CHDRNUM) OR            */
		/*(LIFELNB-STATUZ          = ENDP) OR                      */
		/*(S5004-CONPROSAL         = 'Y').                         */
		/*  Check additional contract header's fields for existance.*/
		if (isEQ(chdrlnbIO.getJownnum(), SPACES)
				|| isEQ(chdrlnbIO.getJownnum(), chdrlnbIO.getCownnum())) {
			sv.jownind.set(SPACES);
		}
		else {
			sv.jownind.set("+");
		}
		if (isEQ(chdrlnbIO.getDespnum(), SPACES)
				|| isEQ(chdrlnbIO.getDespnum(), chdrlnbIO.getCownnum())) {
			sv.addtype.set(SPACES);
		}
		else {
			sv.addtype.set("+");
		}
		/* IF CHDRLNB-ASGNNUM = SPACES OR                               */
		/*    CHDRLNB-ASGNNUM  = CHDRLNB-COWNNUM                        */
		/*    MOVE SPACES              TO S5004-ASGNIND                 */
		/* ELSE                                                         */
		/*    MOVE '+'                 TO S5004-ASGNIND.                */
		checkAssignee3600();
		/*IF CHDRLNB-PAYRNUM = SPACES OR                               */
		/* CHDRLNB-PAYRNUM  = CHDRLNB-COWNNUM                        */
		/* Check if the existing mandate is for a credit card or a         */
		/* bank account.                                                   */
		readClrf3950();
		wsaaMandateType.set(SPACES);
		//		MIBT-239 And MIBT- 173
		if (isNE(chdrlnbIO.getMandref(), SPACES)) {
			//ILIFE-6590 wli31
			String payrnum = "";
			if (isEQ(clrfpf.getClntnum(), SPACES)) {
				payrnum = chdrlnbIO.getCownnum().toString().trim();
			}
			else {
				payrnum = clrfpf.getClntnum().trim();//IJTI-1410
			}
			mandlnb = mandpfDAO.searchMandpfRecord(wsspcomn.fsuco.toString().trim(), payrnum, chdrlnbIO.getMandref().toString().trim(),"Mandlnb");
			
			//			MIBT-239
			if (mandlnb != null && (isNE(mandlnb.getCrcind(), SPACES)||  isEQ(sv.mop,"R"))) {
				creditCardMand.setTrue();
			}
			else {
				bankAccountMand.setTrue();
			}
		}
		/* PERFORM 3950-READ-CLRF                               <V76F13>*/
		if (isEQ(clrfpf.getClntnum(), chdrlnbIO.getCownnum())) {
			sv.payind.set(SPACES);
		}
		else {
			sv.payind.set("+");
			wsaaPayorInd.set("Y");
		}
		m200CheckReducingTerm();
		if (isEQ(wsaaAnniflg, "Y")) {
			anrlcntIO.setParams(SPACES);
			anrlcntIO.setChdrcoy(chdrlnbIO.getChdrcoy());
			anrlcntIO.setChdrnum(chdrlnbIO.getChdrnum());
			anrlcntIO.setFunction(varcom.begnh);
			readAnniRules3400();
			if (isEQ(wsaaAiindInd, "Y")) {
				sv.aiind.set("+");
			}
			else {
				sv.aiind.set(SPACES);
			}
		}
		/*IF CHDRLNB-BANKKEY = SPACES                                  */
		if (isEQ(chdrlnbIO.getMandref(), SPACES)) {
			sv.ddind.set(SPACES);
			sv.crcind.set(SPACES);
		}
		else {
			if (creditCardMand.isTrue()) {
				sv.crcind.set("+");
			}
			else {
				sv.ddind.set("+");
			}
		}
		/*    MOVE '+'                 TO S5004-DDIND.                  */
		if (isEQ(chdrlnbIO.getGrupkey(), SPACES)) {
			sv.grpind.set(SPACES);
		}
		else {
			sv.grpind.set("+");
		}
		/*  Check to see if there are any commission split records.*/
		/*  Check to see if there are any beneficiary records.*/
		/*  Check the contract suspense account (Posting 02).*/
		/*      NB - these two sections are more often performed from the*/
		/*           4000 section, so they have been given*/
		/*           3nnn section numbers.*/
		checkCommission3700();
		checkBeneficiaries3800();
		checkContractSuspense3900();
		a3100CheckTrustee();
		/*                                                         <V71L13>*/
		a3200CheckPolicyNotes();
		if(superprod){
			m200checkzctx();
		}
		if(ctaxPermission){
			checkRollover();
		}
		if(multOwnerFlag)
			checkMultOwner();
	}

	protected void loadHeading1100()
	{
		try {
			loadScreen1110();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void loadScreen1110()
	{
		wsaaHeading.set(SPACES);
		/*   Count the number of spaces at the end of the line*/
		/*     (this is assuming there are none at the beginning)*/
		for (wsaaX.set(30); !(isLT(wsaaX, 1)
				|| isNE(wsaaHead[wsaaX.toInt()], SPACES)); wsaaX.add(-1))
		{
			/* No processing required. */
		}
		/* VARYING WSAA-X FROM 24 BY -1*/
		/*SUBTRACT WSAA-X FROM 24     GIVING WSAA-Y.                   */
		compute(wsaaY, 0).set(sub(30, wsaaX));
		if (isNE(wsaaY, ZERO)) {
			wsaaY.divide(2);
		}
		/* ILIFE-4209 starts */
		//wsaaY.add(1);
		/* ILIFE-4209 ends */
		/*   WSAA-X is the size of the heading string*/
		/*   WSAA-Y is the number of spaces in the front*/
		/*   WSAA-Z is the position in the FROM string.*/
		wsaaZ.set(ZERO);
		//wsaaHeadingChar[wsaaY.toInt()].set(wsaaStartUnderline);
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaX);
		for (int loopVar1 = 0; !(isEQ(loopVar1, loopEndVar1.toInt())); loopVar1 += 1){
			moveChar1130();
		}
		/*   WSAA-X reused as the position in the TO string.*/
		wsaaX.add(1);
		//wsaaHeadingChar[wsaaX.toInt()].set(wsaaEndUnderline);
		if (wsaaHeading != null && wsaaHeading.getFormData() != null) {
			wsaaHeading.set(wsaaHeading.getFormData().trim());
		}
		sv.heading.set(wsaaHeading);

		// ICIL-12 Start
		if(isBancassurance)
		{
		bnkoutpf = bnkoutpfDAO.getBankRecord(sv.chdrnum.toString());
		if (bnkoutpf == null) {
			sv.bnkout.set(SPACES);
			sv.bnksm.set(SPACES);
			sv.bnktel.set(SPACES);
			sv.bnkoutname.set(SPACES);
			sv.bnksmname.set(SPACES);
			sv.bnktelname.set(SPACES);

		} else {
			sv.bnkout.set(bnkoutpf.getBnkout());//IJTI-1410
			sv.bnkoutname.set(bnkoutpf.getBnkoutname());//IJTI-1410
			sv.bnksm.set(bnkoutpf.getBnksm());//IJTI-1410

			sv.bnksmname.set(bnkoutpf.getBnksmname());//IJTI-1410
			sv.bnktel.set(bnkoutpf.getBnktel());//IJTI-1410
			sv.bnktelname.set(bnkoutpf.getBnktelname());//IJTI-1410
		}
		}
		if(!isBancassurance){
			sv.bnkoutOut[varcom.nd.toInt()].set("Y");
			sv.bnksmOut[varcom.nd.toInt()].set("Y");
			sv.bnktelOut[varcom.nd.toInt()].set("Y");			
			
		}else{
			sv.bnkoutOut[varcom.nd.toInt()].set("N");
			sv.bnksmOut[varcom.nd.toInt()].set("N");
			sv.bnktelOut[varcom.nd.toInt()].set("N");			
		} 
		// ICIL-12 End

		goTo(GotoLabel.exit1190);

	}

	protected void moveChar1130()
	{
		wsaaZ.add(1);
		compute(wsaaX, 0).set(add(wsaaY, wsaaZ));
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
	}

	protected void readTr52q1200()
	{
		begin1210();
	}

	protected void begin1210()
	{


		itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.tr52q.toString());
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf = itempfDAO.getItempfRecord(itempf);


		if (null == itempf) {
			itempf = new Itempf();
			itempf.setItempfx(smtpfxcpy.item.toString());
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(tablesInner.tr52q.toString());
			itempf.setItemitem("***");
			itempf = itempfDAO.getItempfRecord(itempf);

			if (null == itempf) {
				tr52qrec.tr52qRec.set(SPACES);
				sv.dlvrmodeErr.set(errorsInner.rla8);
			}
			else {
				tr52qrec.tr52qRec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
		}
		else {
			tr52qrec.tr52qRec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}

	protected void countLives1300()
	{
		try {
			count1300();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void count1300()
	{
		/*  Check for a current proposal for the same client no.*/
		do{
			lifeList = lifepfDAO.getLfcllnbRecords(wsspcomn.company.toString(),sv.ownersel.toString());
			iterator =   lifeList.iterator();
			if(null == lifeList || lifeList.size()<1)
				wsaaEof = true;

			if (iterator.hasNext()) {
				lifepf = iterator.next();
				if(lifepf.getChdrnum() != chdrlnbIO.getChdrnum().toString() ){
					sv.conprosal.set("X");
					goTo(GotoLabel.exit1390);
				}
			}
			else {
				if (iterator.hasNext()) 
					lifepf = iterator.next();
			}
		}
		while ( !(isEQ(wsaaEof, true)));
	}

	protected void readContractTables1600()
	{
		try {
			readContractDefinition1610();
			readBillingValidation1650();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void readContractDefinition1610()
	{


		itempfList = itempfDAO.getItdmByFrmdate(wsspcomn.company.toString(),tablesInner.t5688.toString(),chdrlnbIO.getCnttype().toString(),sv.occdate.toInt());   

		if (itempfList.size() <1 || null == itempfList) {
			sv.occdateErr.set(errorsInner.f290);
		}
		else {
			t5688rec.t5688Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
		if (isEQ(t5688rec.revacc, "Y")) {
			chdrlnbIO.setAcctmeth("R");
		}
		else {
			chdrlnbIO.setAcctmeth("C");
		}
	}

	protected void readBillingValidation1650()
	{

		itempfList = itempfDAO.getItdmByFrmdate(wsspcomn.company.toString(),tablesInner.t5689.toString(),chdrlnbIO.getCnttype().toString(),sv.occdate.toInt()); 

		if (itempfList.size() <1 || null == itempfList) {
			sv.occdateErr.set(errorsInner.e368);
			goTo(GotoLabel.exit1690);
		}
		else {
			t5689rec.t5689Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
		/*  Check for single MoP and/or frequency*/
		wsddBillfreq.set(SPACES);
		wsddMop.set(SPACES);
		wsaaX.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 30); loopVar2 += 1){
			checkTable1660();
		}
		if (isNE(wsddBillfreq, "??")) {
			sv.billfreq.set(wsddBillfreq);
			sv.billfreqOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.billfreqOut[varcom.pr.toInt()].set(SPACES);
		}
		if (isNE(wsddMop, "?")) {
			sv.mop.set(wsddMop);
			sv.mopOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.mopOut[varcom.pr.toInt()].set(SPACES);
		}
		sv.refundOverpayOut[varcom.pr.toInt()].set(SPACES);
		goTo(GotoLabel.exit1690);
	}

	protected void checkTable1660()
	{
		if (isNE(t5689rec.billfreq[wsaaX.toInt()], SPACES)
				&& isNE(wsddBillfreq, "??")) {
			if (isEQ(wsddBillfreq, SPACES)) {
				wsddBillfreq.set(t5689rec.billfreq[wsaaX.toInt()]);
			}
			else {
				if (isNE(t5689rec.billfreq[wsaaX.toInt()], wsddBillfreq)) {
					wsddBillfreq.set("??");
				}
			}
		}
		if (isNE(t5689rec.mop[wsaaX.toInt()], SPACES)
				&& isNE(wsddMop, "?")) {
			if (isEQ(wsddMop, SPACES)) {
				wsddMop.set(t5689rec.mop[wsaaX.toInt()]);
			}
			else {
				if (isNE(t5689rec.mop[wsaaX.toInt()], wsddMop)) {
					wsddMop.set("?");
				}
			}
		}
		wsaaX.add(1);
	}

	protected void formatClientName1700()
	{
		readClientRecord1710();
	}

	protected void readClientRecord1710()
	{
		if (isEQ(sv.ownersel, SPACES)) {
			sv.ownerselErr.set(errorsInner.e186);
			wsaaCltdod.set(varcom.vrcmMaxDate);
			return ;
		}
		wsaaOwnerselSave.set(sv.ownersel);
		clntpf=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), sv.ownersel.toString().trim());


		if ((null == clntpf)
				|| isNE(clntpf.getValidflag(), 1)) {
			sv.ownerselErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
			wsaaCltdod.set(varcom.vrcmMaxDate);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
			wsaaCltdod.set(clntpf.getCltdod());
		}
	}

	protected void formatAgentName1900()
	{
		readAgent1910();
	}

	protected void readAgent1910()
	{
		if (isEQ(sv.agntsel, SPACES)) {
			sv.agntselErr.set(errorsInner.e186);
			if ((isBancassurance && isNE(sv.srcebus,SPACES)) && isEQ(sv.srcebus, Banassurance))
				sv.agntselErr.clear();
			return ;
		}
		sv.agentname.set(SPACES);
		aglflnbpf = aglflnbpfDAO.getAglflnb(sv.agntsel.toString().trim(), wsspcomn.company.toString().trim());
		if (aglflnbpf == null) {
			sv.agntselErr.set(errorsInner.e305);
			return ;
		}
		if (isNE(sv.occdate, ZERO)
				&& isNE(sv.occdate, varcom.vrcmMaxDate)) {
			/*     IF AGLFLNB-DTETRM       <  S5004-OCCDATE                  */
			if (isLTE(aglflnbpf.getDtetrm(), sv.occdate)
					|| isLT(aglflnbpf.getDteexp(), sv.occdate)
					|| isGT(aglflnbpf.getDteapp(), sv.occdate)) {
				sv.agntselErr.set(errorsInner.e475);
			}
		}
		if (isNE(aglflnbpf.getAgntbr(), sv.cntbranch)) {
			sv.agntselErr.set(errorsInner.e455);
		}
		clntpf=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), aglflnbpf.getClntnum());//IJTI-1410
		plainname();
		sv.agentname.set(wsspcomn.longconfname);
		/* Check if agent is not dead before or at risk commencement date  */
		if (isNE(clntpf.getCltdod(), 99999999)
				&& isLTE(clntpf.getCltdod(), sv.occdate)) {
			sv.agntselErr.set(errorsInner.t089);
		}
	}

	protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clts.getSurname());
		/*LGNM-EXIT*/
	}

	protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}

	protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(clts.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clts.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clts.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clts.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clts.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clts.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clts.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

	protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	 * <pre>
	 *  END OF CONFNAME **********************************************
	 *     RETRIEVE SCREEN FIELDS AND EDIT
	 * </pre>
	 */
	protected void preScreenEdit()
	{
		/*PRE-START*/
		/* If returning from an optional selection skip this section.      */
		wsspcomn.edterror.set(varcom.oK);
		if(!ctaxPermission || isNE(sv.mop,"S")) {
			sv.zroloverindOut[varcom.nd.toInt()].set("Y");
		}else {
			sv.zroloverindOut[varcom.nd.toInt()].set(" ");
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.sectionno.set("3000");
		}
	}

	protected void screenEdit2000()
	{
		screenIo2010();
	}

	protected void screenIo2010() {
		/* CALL 'S5004IO' USING SCRN-SCREEN-PARAMS */
		/* S5004-DATA-AREA. */
		/* Screen errors are now handled in the calling program. */
		/*
		 * PERFORM 200-SCREEN-ERRORS.
		 */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "KILL")) {
			if (firstTime.isTrue()) {
				scrnparams.errorCode.set(errorsInner.curs);
				wsspcomn.edterror.set("Y");
				//goTo(GotoLabel.exit2090);
				return;
			}
			else {
				//goTo(GotoLabel.exit2090);
				return;
			}
		}
		if(onePflag) {
			sv.rskComOptOut[Varcom.pr.toInt()].set("Y");
		}
		validate2020();
	}

	protected void validate2020()
	{
		/* Read Table T3625                                           <014>*/
		/*                                                         <014>*/
		/* MOVE SPACES                 TO ITEM-DATA-KEY.           <014>*/
		/* MOVE 'IT'                   TO ITEM-ITEMPFX.            <014>*/
		/* MOVE WSSP-COMPANY           TO ITEM-ITEMCOY.            <014>*/
		/* MOVE T3625                  TO ITEM-ITEMTABL.           <014>*/
		/* STRING 'LP'                                             <014>*/
		/*        S5004-MOP                                        <014>*/
		/*        S5004-BILLFREQ       DELIMITED SIZE              <014>*/
		/*                             INTO ITEM-ITEMITEM.         <014>*/
		/* MOVE 'READR'                TO ITEM-FUNCTION.           <014>*/
		/*                                                         <014>*/
		/* CALL 'ITEMIO' USING ITEM-PARAMS.                        <014>*/
		/* IF ITEM-STATUZ              NOT = O-K                   <014>*/
		/*                         AND NOT = MRNF                       */
		/*     MOVE ITEM-PARAMS        TO SYSR-PARAMS              <015>*/
		/*     MOVE ITEM-STATUZ        TO SYSR-STATUZ              <014>*/
		/*     PERFORM 600-FATAL-ERROR.                            <014>*/
		/*                                                         <014>*/
		/* IF ITEM-STATUZ              = MRNF                      <014>*/
		/*     MOVE H051               TO S5004-MOP-ERR,           <014>*/
		/*                                S5004-BILLFREQ-ERR       <014>*/
		/* ELSE                                                    <014>*/
		/*    MOVE ITEM-GENAREA           TO T3625-T3625-REC.      <014>*/
		/* Read Table T3620                                                */



		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.t3620.toString());
		itempf.setItemitem(sv.mop.toString());
		itempf = itempfDAO.getItempfRecord(itempf);

		if (null == itempf) {
			sv.mopErr.set(errorsInner.e398);
		}
		else {
			t3620rec.t3620Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		if(onePflag && (isEQ(t3620rec.rskComOpt,"O"))) {
			sv.rskComOptOut[Varcom.pr.toInt()].set("N");
			if(isEQ(sv.rskComOpt,SPACES))
				sv.rskComOpt.set("N");
		}
		if(onePflag) {
			sv.flag.set(t3620rec.rskComOpt);
			if(isEQ(sv.rskComOpt,SPACES) || isEQ(t3620rec.rskComOpt,SPACES) || isEQ(t3620rec.rskComOpt,"N"))
				sv.rskComOpt.set("N");
			
			if(isEQ(sv.rskComOpt,"Y") && isEQ(sv.concommflg, "Y")) {
				sv.concommflgErr.set(errorsInner.jl22);
			}
			
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(tablesInner.t3615.toString());
			itempf.setItemitem(sv.srcebus.toString());
			itempf.setValidflag("1");
			itempf = itempfDAO.getItemRecordByItemkey(itempf);
			if (itempf == null) {
				itempf = new Itempf();
				itempf.setItempfx("IT");
				itempf.setItemcoy(wsspcomn.company.toString());
				itempf.setItemtabl(tablesInner.t3615.toString());
				itempf.setItemitem(sv.srcebus.toString());
				itempf.setValidflag("1");
				itempf = itempfDAO.getItemRecordByItemkey(itempf);
					if (itempf == null) {
						t3615rec.t3615Rec.set(SPACES);
					}
				
			}
			if (itempf != null) {
				t3615rec.t3615Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
			
			if(isEQ(sv.rskComOpt,"Y") && isEQ(sv.mop,"D") && isEQ(t3615rec.onepcashless,"Y")) {
				sv.rskComOptErr.set(errorsInner.rukz);
				sv.srcebusErr.set(errorsInner.rukz);
			}
			if(isEQ(sv.rskComOpt,"N") && isNE(sv.mop,"D") && isEQ(t3615rec.onepcashless,"Y") ) {
				sv.mopErr.set(errorsInner.ruky);
			}
		}
		if(onePflag){
			sv.billcd.set(varcom.vrcmMaxDate);
		}
		if (isEQ(wsspcomn.flag, "I")) {
			//goTo(GotoLabel.validateSelectionFields2070);
			validateSelectionFields2070();
			return;
		}
		setOccuranceDate2050();
		/*  Validate owner*/
		/*    IF S5004-OWNERSEL-OUT (CHG) = 'Y'                            */
		if (isEQ(wsaaOwnerselSave, SPACES)
				|| isNE(sv.ownersel, wsaaOwnerselSave)
				|| wserOwnerselInvalid.isTrue()) {
			formatClientName1700();
		}
		if (isNE(sv.ownerselErr, SPACES)) {
			wserOwnersel.set("Y");
		}
		else {
			wserOwnersel.set(SPACES);
		}
		/*  Validate proposal date.                                        */
		/*  - if it is mandatory,                                          */
		/*  - if back dated proposal in range                              */
		if (isEQ(sv.hpropdte, varcom.vrcmMaxDate)) {
			if (isEQ(th506rec.mandatory01, "Y")) {
				sv.hpropdteErr.set(errorsInner.f665);
			}
		}
		else {
			if (isEQ(sv.hpropdte, 0)) {
				sv.hpropdteErr.set(errorsInner.e032);
			}
			else {
				if (isGT(sv.hpropdte, wsaaToday)) {
					sv.hpropdteErr.set(errorsInner.hl01);
				}
			}
		}
		/*  Validate proposal received date.                               */
		if (isNE(sv.hprrcvdt, 0)) {
			if (isGT(sv.hprrcvdt, wsaaToday)) {
				sv.hprrcvdtErr.set(errorsInner.hl01);
			}
		}
		/*  Proposal received date cannot be less than proposal date       */
		if (isEQ(sv.hprrcvdtErr, SPACES)
				&& isEQ(sv.hpropdteErr, SPACES)
				&& isNE(sv.hpropdte, varcom.vrcmMaxDate)
				&& isNE(sv.hprrcvdt, 0)) {
			if (isLT(sv.hprrcvdt, sv.hpropdte)) {
				sv.hprrcvdtErr.set(errorsInner.hl02);
			}
		}
		/*  Validate underwriting decision date.                           */
		/*  - if it is < proposal received date                            */
		if (isNE(th605rec.uwind, "Y")) {
			if (isNE(sv.huwdcdte, varcom.vrcmMaxDate)) {
				if (isEQ(sv.huwdcdte, 0)) {
					sv.huwdcdteErr.set(errorsInner.e032);
				}
				else {
					if (isLT(sv.huwdcdte, sv.hprrcvdt)) {
						sv.huwdcdteErr.set(errorsInner.hl03);
					}
					if (isGT(sv.huwdcdte, wsaaToday)) {
						sv.huwdcdteErr.set(errorsInner.hl01);
					}
				}
			}
		}
		
		/*  Validate original commencement date.*/
		
		if (isEQ(sv.occdateErr, SPACES)) {
			if (isEQ(sv.occdate, varcom.vrcmMaxDate)) {
				sv.occdateErr.set(errorsInner.f665);
			}
		}
		
		//ILJ-62
		if(NBPRP115Permission 
				&& (AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("NBPROPOSALVALD")))
		{
			nbproposalRec = new NBProposalRec();
			nbproposalRec.setCnttype(chdrlnbIO.getCnttype().toString());	
			nbproposalRec.setCrtable("");	
			nbproposalRec.setNoField(5);
			nbproposalRec.setScreeId("S5004");
			nbproposalRec.getFieldName().add("Contract Date");
			nbproposalRec.getFieldName().add("Declaration Date");
			nbproposalRec.getFieldName().add("Owner Occupation");
			nbproposalRec.getFieldName().add("Date of Confirmation Intent");
			nbproposalRec.getFieldName().add("Proposal Date");
			
			nbproposalRec.getFieldValue().add(sv.occdate.toString());
			nbproposalRec.getFieldValue().add(sv.decldate.toString());
			nbproposalRec.getFieldValue().add(sv.ownerOccupation.toString());
			nbproposalRec.getFieldValue().add(sv.cnfirmtnIntentDate.toString());
			nbproposalRec.getFieldValue().add(sv.hpropdte.toString());
			callProgram("NBPROPOSALVALD",nbproposalRec);
			
			for(int i = 0; i < nbproposalRec.getOutputFieldName().size() ; i++) {
				if(nbproposalRec.getOutputFieldName().get(i).equals("Contract Date") 
					&& isNE(nbproposalRec.getOuputErrorCode().get(i),"") && isEQ(sv.occdateErr, SPACES) ) {
						wsspcomn.edterror.set("Y");
						sv.occdateErr.set(nbproposalRec.getOuputErrorCode().get(i));
				}
				if(nbproposalRec.getOutputFieldName().get(i).equals("Declaration Date")
					&& isNE(nbproposalRec.getOuputErrorCode().get(i),"") && isEQ(sv.decldateErr, SPACES)) {
						wsspcomn.edterror.set("Y");
						sv.decldateErr.set(nbproposalRec.getOuputErrorCode().get(i));
				}
				if(nbproposalRec.getOutputFieldName().get(i).equals("Owner Occupation")
					&& isNE(nbproposalRec.getOuputErrorCode().get(i),"")) {
						wsspcomn.edterror.set("Y");
						sv.ownerOccupationErr.set(nbproposalRec.getOuputErrorCode().get(i));
					}
				if(nbproposalRec.getOutputFieldName().get(i).equals("Date of Confirmation Intent")
					&& isNE(nbproposalRec.getOuputErrorCode().get(i),"")) {
						wsspcomn.edterror.set("Y");
						sv.cnfirmtnIntentDateErr.set(nbproposalRec.getOuputErrorCode().get(i));
				}
			}
		}
	
		if(contDtCalcFlag) {		//ILJ-40
			if(isNE(sv.occdate,varcom.vrcmMaxDate)){
				calcRskCommDate();
				wsspcomn.typinj.set(SPACES);
			}
			if (isEQ(sv.billfreq, "00") && isNE(sv.occdate, sv.billcd)) {
				sv.billcd.set(sv.occdate);				
			}
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(tablesInner.tjl05.toString());
			itempf.setItemitem(chdrlnbIO.getCnttype().toString());
			itempf.setValidflag("1");
			itempf = itempfDAO.getItemRecordByItemkey(itempf);
			if (itempf == null) {
				itempf = new Itempf();
				itempf.setItempfx("IT");
				itempf.setItemcoy(wsspcomn.company.toString());
				itempf.setItemtabl(tablesInner.tjl05.toString());
				itempf.setItemitem("***");
				itempf.setValidflag("1");
				itempf = itempfDAO.getItemRecordByItemkey(itempf);
					if (itempf == null) {
						tjl05rec.tjl05Rec.set(SPACES);
					}
				
			}
			if (itempf != null) {
				tjl05rec.tjl05Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				
			
			for (int i = 1; i <= 10; i++) {
				if (isEQ(tjl05rec.billfreq[i].toString(),sv.billfreq.toString()) && isEQ(tjl05rec.mop[i], sv.mop)) {
			
						wsaaNewDate.set(sv.rskcommdate);
					
				        Calendar Date1 = Calendar.getInstance();
				        Calendar Date2 = Calendar.getInstance();
				        try
				        {
				        	Date1.setTime(integralDateFormat.parse(wsaaNewDate.toString()));
				    	    Date2.set(Date1.get(Calendar.YEAR), Date1.get(Calendar.MONTH)+1, 01);
				    	    
				        }catch (ParseException e) {
							
							e.printStackTrace();
						}
				    	
				        String Date3 = integralDateFormat.format(Date2.getTime());
				        wsaaOccDate.set(Date3);
					
					if(isEQ(tjl05rec.fwcondt[i].toString(),"Y") && isEQ(tjl05rec.concommflg[i], "Y"))
					{
						 if(isEQ(sv.occdate, sv.rskcommdate) || isEQ(sv.occdate, wsaaOccDate))
						 {
							if(isEQ(sv.concommflg,"Y"))
							{
								 if(isEQ(sv.occdateErr, SPACES) && isNE(sv.occdate, sv.rskcommdate))
								 {
									 sv.occdateErr.set(errorsInner.jl23);
								 }
								
							}
							else{
								
								 if(isEQ(sv.occdateErr, SPACES) && isNE(sv.occdate, wsaaOccDate))
								 {
									 sv.occdateErr.set(errorsInner.jl23);
								 }
							}
						 }
						 else
						 {
							 if(isEQ(sv.occdateErr, SPACES)){
							 sv.occdateErr.set(errorsInner.jl23);
							 }
						 }
					}
					else if (isEQ(tjl05rec.fwcondt[i].toString(),"Y") && isEQ(tjl05rec.concommflg[i], "N"))
					{
					     if(isEQ(sv.occdateErr, SPACES) && isNE(sv.occdate, wsaaOccDate))
					     {
					    	 sv.occdateErr.set(errorsInner.jl23);
					     }
						
						 if(isEQ(sv.concommflg, "Y"))
						 {
							 sv.concommflgErr.set(errorsInner.jl22); 
						 }
					}
					else if (isEQ(tjl05rec.fwcondt[i].toString(),"N") && isEQ(tjl05rec.concommflg[i], "Y"))
					{
						 if(isEQ(sv.occdateErr, SPACES) && isNE(sv.occdate, sv.rskcommdate))
						 {
							 sv.occdateErr.set(errorsInner.jl23);
						 }
					}
					else if (isEQ(tjl05rec.fwcondt[i].toString(),"N") && isEQ(tjl05rec.concommflg[i], "N"))
					{
						 if(isEQ(sv.occdateErr, SPACES) && isNE(sv.occdate, sv.rskcommdate))
						 {
							 sv.occdateErr.set(errorsInner.jl23);
						 }
						 if(isEQ(sv.concommflg, "Y"))
						 {
							 sv.concommflgErr.set(errorsInner.jl22); 
						 }
					}
				}
			}
		}
	
	}
	if(onePflag){
		if(isEQ(sv.rskComOpt,"Y") && isEQ(sv.mop,"D") && isNE(t3615rec.onepcashless,"Y")){
			sv.billcd.set(sv.occdate);
		  }
	}
		if(isFatcaAllowed){
			Fatcastatus= descdao.getItems("IT", wsspcomn.fsuco.toString(), "TR2DH", wsspcomn.language.toString());
			if(Fatcastatus.size() > 0) {
				if(isEQ(th506rec.fatcaFlag,"Y"))	{
					//ILIFE-3663 getting the client number from screenvars
					Clftpf clftValue=clftpfdao.getRecordByClntnum("CN", wsspcomn.fsuco.toString(), sv.ownersel.toString().trim());
					if(clftValue==null){
						sv.fatcastatus.set(Fatcastatus.get("NUSWOI").getDescitem());
						sv.fatcastatusdesc.set(Fatcastatus.get("NUSWOI").getLongdesc());

					}else{
						sv.fatcastatus.set(clftValue.getFfsts());
						sv.fatcastatusdesc.set(Fatcastatus.get(clftValue.getFfsts().trim()).getLongdesc());
					}
				}
				else{
					sv.fatcastatus.set(Fatcastatus.get("N/A").getDescitem());
					sv.fatcastatusdesc.set(Fatcastatus.get("N/A").getLongdesc());
				}
			}
		}

	
		/*ILJ-42 : Starts*/
		/*  Validate Declaration and Risk commencement date.*/
	if(contDtCalcFlag){
		if (isEQ(sv.decldateErr, SPACES)) {
			if (isEQ(sv.decldate, varcom.vrcmMaxDate)) {
				sv.decldateErr.set(errorsInner.f665);
			}
		}
		if (isEQ(sv.rskcommdateErr, SPACES)) {
			if (isEQ(sv.rskcommdate, varcom.vrcmMaxDate)) {
				sv.rskcommdateErr.set(errorsInner.f665);
			}
		}
		
			datcon1rec.function.set(varcom.tday);  //ILJ-58
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			
			if (isEQ(sv.decldateErr, SPACES) && isGT(sv.decldate, datcon1rec.intDate)) {
				sv.decldateErr.set(errorsInner.hl01);
		}
	}
		
		/*ILJ-42 : Ends*/
		/*        MOVE F665            TO S5004-OCCDATE-ERR        <035>*/
		/*        MOVE E186            TO S5004-OCCDATE-ERR             */
		/*    ELSE                                                      */
		/*        IF S5004-OCCDATE < WSAA-3-MONTHS-AGO                  */
		/*        MOVE E480            TO S5004-OCCDATE-ERR.            */


		itempfList=itempfDAO.getItdmByFrmdate(wsspcomn.company.toString(),
				tablesInner.t5688.toString(),chdrlnbIO.getCnttype().toString(),sv.occdate.toInt());

		if ((itempfList.size() <1 || null == itempfList) && sv.occdateErr.equals(SPACES)){
			sv.occdateErr.set(errorsInner.f290);
		}

		//MIBT-58
		checkbackdate4520();
		/*  - if risk commencement date is < X months prior to             */
		/*  - proposal date, determined by TH506                           */
		if (isNE(wsspcomn.edterror, "Y")
				&& isNE(sv.hpropdte, varcom.vrcmMaxDate)
				&& isNE(th506rec.th506Rec, SPACES)) {
			datcon2rec.intDatex1.set(sv.hpropdte);
			datcon2rec.frequency.set(th506rec.freqcy);
			datcon2rec.freqFactor.set(th506rec.cnt);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if ((isLT(sv.occdate, datcon2rec.intDatex2)) && sv.occdateErr.equals(SPACES)) {
				sv.occdateErr.set(errorsInner.hl00);
				wsspcomn.edterror.set("Y");
			}
		}
		if(isEQ(sv.zmultOwner,"X"))
		{
			clntpf =clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), sv.ownersel.toString());
			if(clntpf!=null && isEQ(clntpf.getClttype(),"C"))
			{
				sv.ownerselErr.set(rrgy);
				sv.zmultOwner.set(SPACES);
			}
		}
		/*  Is the owner dead ?*/
		if (isNE(sv.occdate, varcom.vrcmMaxDate)) {
			if (isLT(wsaaCltdod, sv.occdate)) {
				sv.ownerselErr.set(errorsInner.f782);
			}
		}
		/*    IF WSAA-CLTDOD           NOT > S5004-OCCDATE              */
		/*        MOVE F782             TO S5004-OWNERSEL-ERR.          */
		/*  Validate no of policies in plan.*/
		if (isLT(sv.plansuff, t5688rec.polmin)
				|| isGT(sv.plansuff, t5688rec.polmax)) {
			/*         MOVE U030            TO S5004-PLANSUFF-ERR.           */
			sv.plansuffErr.set(errorsInner.h041);
		}
		/*  Validate agent.*/
		/* IF S5004-AGNTSEL-OUT (CHG) = 'Y'                             */
		/*  OR WSER-AGNTSEL-INVALID                                     */
		/*    PERFORM 1900-FORMAT-AGENT-NAME.                           */
		if(isBancassurance) { // ICIL-1380
			 validateBanassurancefields();
			 if (isNE(sv.errorIndicators, SPACES)) 
			 {
				 wsspcomn.edterror.set("Y");
				 return;
			 }	
		}
		
		if (isNE(sv.agntsel, wsaaLastAgntnum)
				|| wserAgntselInvalid.isTrue()) {
			formatAgentName1900();
			wsaaLastAgntnum.set(sv.agntsel);
		}
		else {
			/*     IF  AGLFLNB-DTETRM      <  S5004-OCCDATE  OR    <LFA1064>*/
				if(isLTE(aglflnbpf.getDtetrm(), sv.occdate)
					|| isLT(aglflnbpf.getDteexp(), sv.occdate)
						|| isGT(aglflnbpf.getDteapp(), sv.occdate)) {
					sv.agntselErr.set(errorsInner.e475);
				}		
		}
		if (isNE(sv.agntselErr, SPACES)) {
			wserAgntsel.set("Y");
		}
		else {
			wserAgntsel.set(SPACES);
		}
		/*  Validate contract currency.*/
		if (isEQ(sv.cntcurr, SPACES)) {
			getDefaultCurrency2100();
		}
		/*  Validate billing currency.*/
		if (isEQ(sv.billcurr, SPACES)) {
			sv.billcurr.set(sv.cntcurr);
		}
		/*  Cross checking the two currencies.*/
		if ((isNE(sv.billcurr, SPACES))
				&& (isNE(sv.cntcurr, SPACES))) {
			/*    (S5004-CNTCURR NOT = SPACE) AND                           */
			/*    (S5004-CNTCURR NOT = S5004-BILLCURR)                      */
			crossCheckCurr2500();
		}
		/*  Validate that the billing currency on the group is the         */
		/*  same as that on the contract header proposal.                  */
		if (isNE(chdrlnbIO.getGrupkey(), SPACES)) {
			//ILIFE-6590 wli31
			Grpspf grpspf = grpspfDAO.searchGrpspfRecord(wsspcomn.company.toString().trim(), chdrlnbIO.getGrupkey().toString().trim());
			if(grpspf != null && !sv.billcurr.toString().trim().equals(grpspf.getCurrcode().trim())){//IJTI-1410
				sv.billcurrErr.set(errorsInner.e679);
			}
		}
		/*  Validate register.*/
		if (isEQ(sv.register, SPACES)) {
			sv.registerErr.set(errorsInner.e186);
		}
		/*  Validate source of business*/
		if (isEQ(sv.srcebus, SPACES)) {
			sv.srcebusErr.set(errorsInner.e186);
		}
		
		/*  Validate cross reference number*/
		if (isEQ(sv.lrepnum, SPACES)
				&& isNE(sv.reptype, SPACES)) {
			sv.lrepnumErr.set(errorsInner.e186);
		}
		if (isNE(sv.lrepnum, SPACES)
				&& isEQ(sv.reptype, SPACES)) {
			sv.lrepnumErr.set(errorsInner.e492);
		}
		/*  A cross reference type of CH has a special meaning*/
		/*    - validate the reference field against an existing contract.*/
		if (isEQ(sv.reptype, "CH")
				&& isNE(sv.lrepnum, SPACES)) {
			checkOldContract2300();
		}
		/*  Validate billing frequency.*/
		if (isEQ(sv.billfreq, SPACES)) {
			sv.billfreqErr.set(errorsInner.e186);
		}
		/*  Validate method of payment.*/
		if (isEQ(sv.mop, SPACES)) {
			sv.mopErr.set(errorsInner.e186);
		}
		//ILIFE-2472 Starts
		if (isEQ(sv.reqntype, SPACES)) {
			sv.reqntypeErr.set(errorsInner.e186);
		} 
		if (isEQ(sv.reqntype, "C")
				&& isEQ(sv.zdcind, SPACE)) {
			sv.zdcind.set("X");
		}
		if (isEQ(sv.reqntype, "4")
				&& isEQ(sv.zdpind, SPACE)) {
			sv.zdpind.set("X");
		}
		if(isNE(sv.reqntype, "4")) {
			sv.zdpind.set(SPACE);
		}
		if(isNE(sv.reqntype, "C")) {
			sv.zdcind.set(SPACE);
		}
		//ILIFE-2472 Ends
		/*  Validate Automatic Increase indicator to only allow Y/N.       */
		/* IF  T5654-INDXFLG            = 'O'                      <040>*/
		/*     IF  S5004-INDXFLG            NOT = 'Y'              <040>*/
		/*     AND S5004-INDXFLG            NOT = 'N'              <040>*/
		/*         MOVE G616               TO S5004-INDXFLG-ERR.   <040>*/
		/* Validate the source of income field if entered.                 */
		//ILIFE-4108 starts
		
		// ILIFE-6049 STARTS
		//if (isEQ(t5654rec.indxflg, "O") && isEQ(chdrlnbIO.getOccdate(), ZERO)) {
		if(isEQ(sv.indxflg, "Y")){// || isEQ(sv.indxflg, "O")){    ILIFE-6075
			sv.indxflg.set("C");
		} // ILIFE-6049 ENDS
			
		//ILIFE-4108 ends
		if (isNE(sv.incomeSeqNo, ZERO)) {
			//ILIFE-6590 wli3
			soinpf = soinDAO.searchSoinpfRecord(wsspcomn.fsuco.toString().trim(), sv.ownersel.toString().trim(), sv.incomeSeqNo.toInt());
			if (soinpf == null) {
				sv.incseqnoErr.set(errorsInner.i068);
			}
		}
		if (isEQ(sv.dlvrmode, SPACES)) {
			sv.dlvrmodeErr.set(errorsInner.e186);
		}
		/*  Anniversary rules depend on Automatic Increase flag.           */
		if (isEQ(wsaaAnniflg, "Y")
				&& isNE(sv.indxflg, SPACES)
				&& isNE(sv.indxflg, wsaaIndxflgCmp)
				&& isEQ(sv.aiind, SPACES)) {
			sv.aiind.set("X");
		}
		/*  Cross validate billing frequency/*/
		/*                 method of payment/*/
		/*                 billing commencement*/
		/*                 contract commencement*/
		//MIBT-77
		//Setting the value "Y" in wserBilling, for enter into below validateBilling2200 Method.
		
			
				
		valdteCustomerSpecific2171();
			
			
		//ILIFE-7820-Ends
		wserBilling.set("Y");
		if (isEQ(sv.billcd, varcom.vrcmMaxDate)
				&& isEQ(sv.billfreqErr, SPACES)) {
			defaultBilling2250();
			mopFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(),MOP_FEATURE_ID, appVars, "IT");
			if(mopFlag && isNE(sv.billday,SPACE)){
				validateBilling2200();
			}
		}
		else {
			if ((isEQ(sv.billfreqOut[varcom.chg.toInt()], "Y")
					|| isEQ(sv.mopOut[varcom.chg.toInt()], "Y")
					|| isEQ(sv.billcdOut[varcom.chg.toInt()], "Y")
					|| isEQ(sv.occdateOut[varcom.chg.toInt()], "Y")
					|| wserBillingInvalid.isTrue())
					&& isEQ(sv.billfreqErr, SPACES)
					&& isEQ(sv.mopErr, SPACES)
					&& isEQ(sv.billcdErr, SPACES)
					&& isEQ(sv.occdateErr, SPACES)) {
				validateBilling2200();
			}
		}
		//ILIFE-3735
		if(isbillday){
			// IBPLIFE-5785 start
			isAllowBillday = false;
			if(isEQ(sv.billfreqErr, SPACES) && isEQ(sv.mopErr, SPACES)) {
				for (int i = 1; i <= 30; i++) {
					if (isEQ(sv.mop, t5689rec.mop[i]) && isEQ(sv.billfreq, t5689rec.billfreq[i])) {
						if(isEQ(t5689rec.allowBillday[i], "Y")) {
							isAllowBillday = true;
						}
						break;
					}
				}
			}
			if (!isAllowBillday) {
				if (isNE(sv.billday, SPACES) 
						&& isEQ(sv.billfreqErr, SPACES) && isEQ(sv.mopErr, SPACES)) {// IBPLIFE-5785
					sv.billdayErr.set(errorsInner.rupu);	
				}
			}
				else{
					//ILIFE-7820-Start
					checkAgainstT6654();
					if (isEQ(t6654rec.renwdate2, "Y")) {
						wsaaBillcd.set(sv.billcd);
						
						int m=(sv.billcd.toString().charAt(4)-48)*10+(sv.billcd.toString().charAt(5)-48);
						String mnth;
						if(m==12){
							mnth="12";
							int y=(sv.billcd.toString().charAt(0)-48)*1000+(sv.billcd.toString().charAt(1)-48)*100+(sv.billcd.toString().charAt(2)-48)*10+(sv.billcd.toString().charAt(3)-48);
							if(isNE(sv.billday,SPACES) && isLT(sv.billday,wsaaBillcdDd)) {
								mnth="01";
								y++;
							}
							wsaaBillcdYyyy.set(String.valueOf(y));
						}
						else{
							
						/*	1. If RCD is 15/01/2018 is user selects billing day as 15 then first billing date should be 15/01/2018
							2. If RCD is 15/01/2018 user selects billing day as 25 then first billing date should be 25/01/2018 
							3. If RCD is 15/01/2018 user selects billing day as 14 then first billing date should be 14/02/2018 */
							//ILIFE-6986
							if(isNE(sv.billday,SPACES) && isLT(sv.billday,wsaaBillcdDd)){
								m++;
							}
							if(m<10)
								mnth= "0"+String.valueOf(m);
							else
								mnth=String.valueOf(m);
							}
						wsaaBillcdMm.set(mnth);
						if(isNE(sv.billday,SPACES)) {
							if(isEQ(sv.billday,"31") && (isEQ(mnth,"04")||isEQ(mnth,"06")||isEQ(mnth,"09")||isEQ(mnth,"11"))){
								wsaaBillcdDd.set(30);
							}
							else if(isEQ(mnth,"02")&&(isEQ(sv.billday,"31")||isEQ(sv.billday,"30")||isEQ(sv.billday,"29"))){
								if((Math.floorMod(wsaaBillcdYyyy.toInt(),400)==0)||((Math.floorMod(wsaaBillcdYyyy.toInt(),4)==0)&&(Math.floorMod(wsaaBillcdYyyy.toInt(),100)!=0))){
									wsaaBillcdDd.set(29);
								}
								else
									wsaaBillcdDd.set(28);
							}
							else {
								wsaaBillcdDd.set(sv.billday);
							}
						}
						sv.billcd.set(wsaaBillcd);
					}
					if (isEQ(t6654rec.renwdate1, "Y")) {
						wsaaBillcd.set(sv.occdate);

						int m=(sv.occdate.toString().charAt(4)-48)*10+(sv.occdate.toString().charAt(5)-48);
						String mnth;
						if(m==12){
							mnth="12";
							int y=(sv.occdate.toString().charAt(0)-48)*1000+(sv.occdate.toString().charAt(1)-48)*100+(sv.occdate.toString().charAt(2)-48)*10+(sv.occdate.toString().charAt(3)-48);
							if(isNE(sv.billday,SPACES) && isLT(sv.billday,wsaaBillcdDd)) {
								mnth="01";
								y++;
							}
							wsaaBillcdYyyy.set(String.valueOf(y));
						}
						else{
							
						/*	1. If RCD is 15/01/2018 is user selects billing day as 15 then first billing date should be 15/01/2018
							2. If RCD is 15/01/2018 user selects billing day as 25 then first billing date should be 25/01/2018 
							3. If RCD is 15/01/2018 user selects billing day as 14 then first billing date should be 14/02/2018 */
							//ILIFE-6986
							if(isNE(sv.billday,SPACES) && isLT(sv.billday,wsaaBillcdDd)){
								m++;
							}
							if(m<10)
								mnth= "0"+String.valueOf(m);
							else
								mnth=String.valueOf(m);}
						wsaaBillcdMm.set(mnth);
						if(isNE(sv.billday,SPACES)) {
							if(isEQ(sv.billday,"31") && (isEQ(mnth,"04")||isEQ(mnth,"06")||isEQ(mnth,"09")||isEQ(mnth,"11"))){
								wsaaBillcdDd.set(30);
							}
							else if(isEQ(mnth,"02")&&(isEQ(sv.billday,"31")||isEQ(sv.billday,"30")||isEQ(sv.billday,"29"))){
								if((Math.floorMod(wsaaBillcdYyyy.toInt(),400)==0)||((Math.floorMod(wsaaBillcdYyyy.toInt(),4)==0)&&(Math.floorMod(wsaaBillcdYyyy.toInt(),100)!=0))){
									wsaaBillcdDd.set(29);
								}
								else
									wsaaBillcdDd.set(28);
							}
							else
								wsaaBillcdDd.set(sv.billday);
						}
						sv.billcd.set(wsaaBillcd);
					}
					
					//ILIFE-7820-Ends
					
				}
			
		}
		if (isNE(sv.billfreqErr, SPACES)
				|| isNE(sv.mopErr, SPACES)
				|| isNE(sv.billcdErr, SPACES)
				|| isNE(sv.occdateErr, SPACES)) {
			wserBilling.set("Y");
		}
		else {
			wserBilling.set(SPACES);
		}
		/* Cross check payment plan and Bank Account details*/
		if (!wserBillingInvalid.isTrue()) {
			checkBankRequired2400();
		}
		vldt2020CustomerSpecific();	
		/* Has the owner been changed ?*/
		/* IF T3625-DDIND = 'Y'                                         */
		if (isNE(t3620rec.ddind, SPACES)) {
			if (isEQ(wsaaOwnerselStore, SPACES)) {
				//goTo(GotoLabel.validateSelectionFields2070);
				validateSelectionFields2070();
				return;
				/* UNREACHABLE CODE
				if (isNE(sv.ownersel, wsaaOwnerselStore)) {
					sv.ddind.set("X");
					if (isEQ(wsaaPayorInd, "Y")) {
						wsaaPayorInd.set(SPACES);
						sv.payind.set("X");
						wsaaOwnerselStore.set(sv.ownersel);
					}
				}
				 */
			}
		}
		if (isNE(t3620rec.crcind, SPACES)) {
			if (isEQ(wsaaOwnerselStore, SPACES)) {
				//goTo(GotoLabel.validateSelectionFields2070);
				validateSelectionFields2070();
				return;
			}
			if (isNE(sv.ownersel, wsaaOwnerselStore)) {
				sv.crcind.set("X");
				if (isEQ(wsaaPayorInd, "Y")) {
					wsaaPayorInd.set(SPACES);
					sv.payind.set("X");
					wsaaOwnerselStore.set(sv.ownersel);
				}
			}
		}
		if(superprod){
			if(isEQ(sv.schmno, SPACES))
				sv.schmnmeErr.set(errorsInner.rfxb);
			readschmnme();
		}
		if(multOwnerFlag) {
			if (isEQ(sv.ownersel, SPACES)) {
				sv.ownerselErr.set(errorsInner.e186);			
			}else
			if(isEQ(sv.ownersel,chdrlnbIO.getCownnum2()) || isEQ(sv.ownersel,chdrlnbIO.getCownnum3()) || isEQ(sv.ownersel,chdrlnbIO.getCownnum4()) || isEQ(sv.ownersel,chdrlnbIO.getCownnum5()))
				sv.ownerselErr.set(rrgz);
			if(isEQ(sv.zmultOwner,"+"))
			{
				clntpf = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), sv.ownersel.toString());
				if(clntpf!=null)
				{
					if(isEQ(clntpf.getClttype(),"C"))
						sv.ownerselErr.set(rrgx);
				}
			}
		}
		if(onePflag){
			if(isEQ(sv.rskComOpt,"Y") && isEQ(sv.mop,"D") && isNE(t3615rec.onepcashless,"Y")){
				sv.billcd.set(sv.occdate);
			}
		}
		validateSelectionFields2070();
	}
	
	protected void valdteCustomerSpecific2171(){
		if (afiFlag && isEQ(wsspcomn.flag, "M") &&  isNE(sv.occdate, SPACES)) {
			checkAgainstT6654();
			
			if (isEQ(t6654rec.renwdate2, "Y")) {
				if(!BTPRO028Permission) {	
				wsaaSub.set(1);
				for (wsaaSub.set(1); !(isGT(wsaaSub, 6)
						|| isEQ(t6654rec.zrfreq[wsaaSub.toInt()], sv.billfreq)); wsaaSub.add(1)){
					readFreqs5000();
				}
				if (isGT(wsaaSub, 6)) {
					sv.billcdErr.set(errorsInner.r081);
					//goTo(GotoLabel.exit2290);
					return;
				}
				}
				wsaaOdate.set(sv.occdate);
				datcon4rec.datcon4Rec.set(SPACES);
				datcon4rec.billday.set(wsaaOdateDd);
				datcon4rec.billmonth.set(wsaaOdateMm);
				datcon4rec.intDate1.set(sv.occdate);
				datcon4rec.intDate2.set(ZERO);
				datcon4rec.frequency.set(sv.billfreq);
				if(BTPRO028Permission) {
					wsaaSub.set(1);
					datcon4rec.freqFactor.set(t6654rec.zrincr[wsaaSub.toInt()]);	
				}
				else {
				datcon4rec.freqFactor.set(t6654rec.zrincr[wsaaSub.toInt()]);
				}			
				callProgram(Datcon4.class, datcon4rec.datcon4Rec);
				if (isEQ(datcon4rec.statuz, varcom.oK)) {
					chdrlnbIO.setBtdate(datcon4rec.intDate2);
					payrIO.setBtdate(datcon4rec.intDate2);
					wsaaBtdate9.set(datcon4rec.intDate2);
					sv.billcd.set(datcon4rec.intDate2);
				}
				else {
					sv.occdateErr.set(errorsInner.sc72);
					//goTo(GotoLabel.exit2290);
					return;
				}
			}
			else {
				if (isEQ(t6654rec.renwdate1, "Y")) {
					chdrlnbIO.setBtdate(sv.occdate);
					payrIO.setBtdate(sv.occdate);
					wsaaBtdate9.set(sv.occdate);
					sv.billcd.set(sv.occdate);
				}
			
		}
			
		}	
	}
	protected void setOccuranceDate2050() {
	}
	protected void vldt2020CustomerSpecific(){		
	}

	protected void validateSelectionFields2070()
	{
		if (isNE(sv.jownind, "X")
				&& isNE(sv.jownind, "+")
				&& isNE(sv.jownind, " ")) {
			sv.jownindErr.set(errorsInner.g620);
		}
		if (isNE(sv.comind, "X")
				&& isNE(sv.comind, "+")
				&& isNE(sv.comind, " ")) {
			sv.comindErr.set(errorsInner.g620);
		}
		if (isNE(sv.addtype, "X")
				&& isNE(sv.addtype, "+")
				&& isNE(sv.addtype, " ")) {
			sv.addtypeErr.set(errorsInner.g620);
		}
		if (isNE(sv.asgnind, "X")
				&& isNE(sv.asgnind, "+")
				&& isNE(sv.asgnind, " ")) {
			sv.asgnindErr.set(errorsInner.g709);
		}
		if (isNE(sv.payind, "X")
				&& isNE(sv.payind, "+")
				&& isNE(sv.payind, " ")) {
			sv.payindErr.set(errorsInner.g620);
		}
		if (isNE(sv.aiind, "X")
				&& isNE(sv.aiind, "+")
				&& isNE(sv.aiind, " ")) {
			sv.aiindErr.set(errorsInner.g620);
		}
		/* IF S5004-INDXFLG NOT  = 'Y'                             <053>*/
		if (isEQ(sv.indxflg, SPACES)
				&& isNE(sv.aiind, SPACES)) {
			sv.aiindErr.set(errorsInner.e492);
		}
		if (isNE(sv.ddind, "X")
				&& isNE(sv.ddind, "+")
				&& isNE(sv.ddind, " ")) {
			sv.ddindErr.set(errorsInner.g620);
		}
		vldtfld2070CustomerSpecific();
		if (isNE(sv.crcind, "X")
				&& isNE(sv.crcind, "+")
				&& isNE(sv.crcind, " ")) {
			sv.crcindErr.set(errorsInner.g620); //MIBT-239
		}
		if (isNE(sv.grpind, "X")
				&& isNE(sv.grpind, "+")
				&& isNE(sv.grpind, " ")) {
			sv.grpindErr.set(errorsInner.g620);
		}
		if (isNE(sv.fupflg, "X")
				&& isNE(sv.fupflg, "+")
				&& isNE(sv.fupflg, " ")) {
			sv.fupflgErr.set(errorsInner.g620);
		}
		if (isNE(sv.apcind, "X")
				&& isNE(sv.apcind, "+")
				&& isNE(sv.apcind, " ")) {
			sv.apcindErr.set(errorsInner.g620);
		}
		if (isNE(sv.bnfying, "X")
				&& isNE(sv.bnfying, "+")
				&& isNE(sv.bnfying, " ")) {
			sv.bnfyingErr.set(errorsInner.g620);
		}
		//ILIFE-7792
		nonSuper= FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(),"NBPRP095", appVars, "IT");
		if(nonSuper){
		if (readTR59X() && isEQ(sv.bnfying, "X")){
			sv.bnfyingErr.set(errorsInner.rrmr);
			sv.bnfying.set(SPACES);
		}
		}
		if (isNE(sv.zsredtrm, "X")
				&& isNE(sv.zsredtrm, "+")
				&& isNE(sv.zsredtrm, " ")) {
			sv.zsredtrmErr.set(errorsInner.g620);
		}
		/* Validate the Transaction History field.                         */
		if (isNE(sv.transhist, "X")
				&& isNE(sv.transhist, "+")
				&& isNE(sv.transhist, " ")) {
			sv.transhistErr.set(errorsInner.g620);
		}
		/* Validate Trustee field.                                         */
		if (isNE(sv.ctrsind, "X")
				&& isNE(sv.ctrsind, "+")
				&& isNE(sv.ctrsind, " ")) {
			sv.ctrsindErr.set(errorsInner.g709);
		}
		if (isNE(sv.ind, "X")
				&& isNE(sv.ind, "+")
				&& isNE(sv.ind, " ")) {
			sv.indErr.set(errorsInner.g620);
		}
		//ILIFE-2472 Starts
		if (isNE(sv.zdpind, "X") 
				&& isNE(sv.zdpind, "+")
				&& isNE(sv.zdpind, " ")) {
			sv.zdpindErr.set(errorsInner.g620);
		} 
		if (isNE(sv.zdcind, "X") 
				&& isNE(sv.zdcind, "+")
				&& isNE(sv.zdcind, " ")) {
			sv.zdcindErr.set(errorsInner.g620);
		}
		//ILIFE-2472 Ends
		if (superprod &&isNE(sv.zctaxind, "X") //ALS-313
				&& isNE(sv.zctaxind, "+")
				&& isNE(sv.zctaxind, " ")) {
			sv.zctaxindErr.set(errorsInner.g620);
		}
		if (ctaxPermission && isNE(sv.zroloverind, "X") //ALS-700
				&& isNE(sv.zroloverind, "+")
				&& isNE(sv.zroloverind, " ")) {
			sv.zroloverindErr.set(errorsInner.g620);
		}
		if(multOwnerFlag  && isNE(sv.zmultOwner,"X") && isNE(sv.zmultOwner,"+") && isNE(sv.zmultOwner, " "))
		{
			sv.zmultOwnerErr.set(errorsInner.g620);
		}
		
		checkForErrors2080();
	}
protected void vldtfld2070CustomerSpecific(){
		
	}
protected void validateBanassurancefields()
{	
	if (isEQ(sv.srcebus, Banassurance)) {
		if(isEQ(sv.bnkout,SPACES)) {		
			sv.bnkoutErr.set(errorsInner.e186);
			return;
		}
		else if((sv.bnkout.toString().trim().length() < 14))
		{
			sv.bnkoutErr.set(rrbo);
			return;
		}
		else {
			checkBanassrance();
		}
		if(isEQ(sv.bnktel,SPACES)) {
			sv.bnktelErr.set(errorsInner.e186);			
			return;
		} 
		else if((sv.bnktel.toString().trim().length() < 10) )
		{
			sv.bnktelErr.set(rrdp);
			return;
		}
		else {
			bnkTellerName = agntDAO.getBanassuranceDetails(sv.bnktel.toString().trim(),getTellername);
			if (bnkTellerName != null) {
				sv.bnktelname.set(bnkTellerName);
			} else {
				sv.bnktelErr.set(rrdo);
			}
		}
	}
}
	protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			if (isEQ(chdrlnbIO.getCnttype(), "RTA") || isEQ(chdrlnbIO.getCnttype(), "SUM") || isEQ(chdrlnbIO.getCnttype(), "LCP")){
				//ILIFE-5172
				if(isEQ(sv.zsredtrm, SPACES) && isNE(sv.zsredtrm, "+")){
					sv.zsredtrm.set("X");
				}//ILIFE-5172
			}else{
				sv.zsredtrm.set(SPACES);
			}
		}
		if (isEQ(chdrlnbIO.getCnttype(), "RTA") || isEQ(chdrlnbIO.getCnttype(), "SUM") || isEQ(chdrlnbIO.getCnttype(), "LCP"))
		{
			if (isEQ(sv.errorIndicators, SPACES) && isNE(sv.zsredtrm, "+")) {
				sv.zsredtrm.set("X");
			} 
			// ILIFE-5127
			/*else if(isEQ(sv.zsredtrm, "+") && isEQ(sv.errorIndicators, SPACES)){
					sv.zsredtrm.set(SPACES);
			}*/// ILIFE-5127
			else if(isEQ(sv.zsredtrm, SPACES) && isEQ(sv.errorIndicators, SPACES)){
				sv.zsredtrm.set("X");
			}
		}
		/*    If the refresh key is pressed, check to see if the Contract*/
		/*    Owner is the same as the Life insured.*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			/*       MOVE 'N'                   TO S5004-CONPROSAL        <033>*/
			sv.conprosal.set(SPACES);
			//	lfcllnbIO.setStatuz(varcom.oK);
			while ( !((isEQ(wsaaEof, true))
					|| (isNE(sv.conprosal, SPACES)))) {
				countLives1300();
			}

			wsspcomn.edterror.set("Y");
			/*********MOVE 'Y'                   TO WSSP-EDTERROR.              */
		}
	}
protected void checkBanassrance()
{
	// ICIL-147 start			
	bnkoutpfChk = bnkoutpfDAO.getAgentRefcode(sv.bnkout.toString().trim());
	if (bnkoutpfChk != null) {
		sv.bnkout.set(bnkoutpfChk.getBnkout());
		sv.bnkoutname.set(bnkoutpfChk.getBnkoutname());
		sv.bnksm.set(bnkoutpfChk.getBnksm());
		if (isEQ(sv.srcebus, Banassurance)) {
			sv.agntsel.set(bnkoutpfChk.getAgntnum());
		}
	} else {
		sv.bnkoutErr.set(rrbu);
	}

	if (!isEQ(sv.bnksm, SPACES)) {
		bnkManagerName = agntDAO.getBanassuranceDetails(sv.bnksm.toString().trim(),null);
		if (bnkManagerName != null) {
			sv.bnksmname.set(bnkManagerName);
		}
	}
								
	// ICIL-147 end
	
}
	protected void getDefaultCurrency2100()
	{
		currency2110();
	}

	protected void currency2110()
	{   

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.t5620.toString());
		itempf.setItemitem(sv.cntbranch.toString());
		itempf = itempfDAO.getItempfRecord(itempf);


		if (null == itempf) {
			/*      MOVE U022               TO S5004-CNTCURR-ERR             */
			/*     MOVE G820               TO S5004-CNTCURR-ERR        <026>*/
			sv.cntcurrErr.set(errorsInner.f955);
		}
		else {
			t5620rec.t5620Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			sv.cntcurr.set(t5620rec.cntcurr);
			chdrlnbIO.setCntcurr(sv.cntcurr);
		}
	}

	protected void validateBilling2200()
	{

		lookUpTable2210();

	}

	protected void lookUpTable2210()
	{
		/*  The version of the contract table item is dependant on*/
		/*     the effective date of the risk. So, if this date changes*/
		/*      re-read them in case another version now applys.*/
		if (isEQ(sv.occdateOut[varcom.chg.toInt()], "Y")) {
			readContractTables1600();
			if (isNE(sv.occdateErr, SPACES)) {
				//goTo(GotoLabel.exit2240);
				return;
			}
		}
		/*   If the next billing date is already present, then don't       */
		/*   default it even if anything else has changed.                 */
		/*    IF (S5004-BILLFREQ-OUT (CHG) = 'Y'                  <CAS1.0> */
		/*        OR S5004-MOP-OUT (CHG) = 'Y'                    <CAS1.0> */
		/*        OR S5004-OCCDATE-OUT (CHG) = 'Y')               <CAS1.0> */
		/*        MOVE VRCM-MAX-DATE       TO S5004-BILLCD        <CAS1.0> */
		/*        PERFORM 2250-DEFAULT-BILLING                    <CAS1.0> */
		/*    END-IF.                                             <CAS1.0> */
		/*                                                        <030>*/
		/*Work out the Billed To Date - it will either be equal to  <030>*/
		/*the Risk Commencement Date, (OCCDATE) or 1 frequency great<030>*/
		/*than it.                                                  <030>*/
		/*                                                        <030>*/
		/*MOVE SPACES                 TO ITEM-DATA-KEY.           <030>*/
		/*MOVE CHDRLNB-CHDRCOY        TO ITEM-ITEMCOY.            <030>*/
		/*MOVE SMTP-ITEM              TO ITEM-ITEMPFX.            <030>*/
		/*MOVE T6654                  TO ITEM-ITEMTABL.           <030>*/
		/*MOVE SPACES                 TO WSAA-T6654-KEY.          <030>*/
		/*MOVE S5004-MOP              TO WSAA-T6654-MOP.          <030>*/
		/*MOVE CHDRLNB-CNTTYPE        TO WSAA-T6654-CNTTYPE.      <030>*/
		/*MOVE WSAA-T6654-KEY         TO ITEM-ITEMITEM.           <030>*/
		/*MOVE READR                  TO ITEM-FUNCTION.           <030>*/
		/*                                                        <030>*/
		/*CALL 'ITEMIO'               USING ITEM-PARAMS.          <030>*/
		/*                                                        <030>*/
		/*IF ITEM-STATUZ              NOT = O-K                   <030>*/
		/*                        AND NOT = MRNF                  <030>*/
		/*    MOVE ITEM-PARAMS        TO SYSR-PARAMS              <030>*/
		/*    PERFORM 600-FATAL-ERROR.                            <030>*/
		/*                                                        <030>*/
		/*IF ITEM-STATUZ                  = O-K                   <030>*/
		/*   MOVE ITEM-GENAREA            TO T6654-T6654-REC      <030>*/
		/*ELSE                                                    <030>*/
		/*   MOVE '***'                   TO WSAA-T6654-CNTTYPE   <030>*/
		/*   MOVE WSAA-T6654-KEY          TO ITEM-ITEMITEM        <030>*/
		/*                                                        <030>*/
		/*   CALL 'ITEMIO'                USING ITEM-PARAMS       <030>*/
		/*                                                        <030>*/
		/*   IF ITEM-STATUZ            NOT = O-K                  <030>*/
		/*                         AND NOT = MRNF                 <030>*/
		/*      MOVE ITEM-PARAMS       TO SYSR-PARAMS             <030>*/
		/*      PERFORM 600-FATAL-ERROR                           <030>*/
		/*   ELSE                                                 <030>*/
		/*   IF ITEM-STATUZ                = O-K                  <030>*/
		/*      MOVE ITEM-GENAREA         TO T6654-T6654-REC      <030>*/
		/*   ELSE                                                 <030>*/
		/*      MOVE E840                 TO S5004-MOP-ERR        <030>*/
		/*      GO TO                     2290-EXIT.              <030>*/
		/*                                                        <030>*/
		/*IF T6654-RENWDATE-2              = 'Y'                  <030>*/
		/*   MOVE S5004-OCCDATE           TO DTC2-INT-DATE-1      <030>*/
		/*   MOVE ZEROES                  TO DTC2-INT-DATE-2      <030>*/
		/*   MOVE S5004-BILLFREQ          TO DTC2-FREQUENCY       <030>*/
		/*   MOVE 1                       TO DTC2-FREQ-FACTOR     <030>*/
		/*                                                        <030>*/
		/*   CALL 'DATCON2'               USING DTC2-DATCON2-REC  <030>*/
		/*                                                        <030>*/
		/*   IF DTC2-STATUZ                = O-K                  <030>*/
		/*      MOVE DTC2-INT-DATE-2      TO CHDRLNB-BTDATE,      <030>*/
		/*                                   PAYR-BTDATE          <030>*/
		/*   END-IF                                               <030>*/
		/*ELSE                                                    <030>*/
		/*   MOVE S5004-OCCDATE           TO CHDRLNB-BTDATE,      <030>*/
		/*                                   PAYR-BTDATE.         <030>*/
		/*                                                        <030>*/
		/*                                                        <030>*/
		/*Default billing commencement date if blank.               <030>*/
		/*                                                        <030>*/
		/*IF S5004-BILLCD = VRCM-MAX-DATE                         <030>*/
		/*   MOVE 'Y'                 TO WSAA-DEFAULT-BILLCD      <030>*/
		/*   GO TO 2220-CHECK-AGAINST-TABLE.                      <030>*/
		/*                                                        <030>*/
		/*MOVE SPACE                  TO WSAA-DEFAULT-BILLCD.     <030>*/
		/*                                                            <030>*/
		/*   Determin if the required date is the "end of month"      <030>*/
		/*                                                            <030>*/
		wsaaEndOfMonth.set(SPACES);
		wsaaRcd.set(sv.billcd);
		wsaaRendd.set(wsaaDay);
		if (isEQ(wsaaMth, 4)
				|| isEQ(wsaaMth, 6)
				|| isEQ(wsaaMth, 9)
				|| isEQ(wsaaMth, 11)
				&& isEQ(wsaaDay, 30)) {
			wsaaEndOfMonth.set("Y");
		}
		if (isEQ(wsaaMth, 2)
				&& isEQ(wsaaDay, 28)
				|| isEQ(wsaaDay, 29)) {
			wsaaEndOfMonth.set("Y");
		}
		/*                                                        <030>*/
		/*Skip this code if MOP is = 'B'                            <030>*/
		/*                                                        <030>*/
		/*IF S5004-MOP                 = 'B'                      <011>*/
		/*IF T3625-DDIND               = 'Y'                      <030>*/
		/*    GO TO 2215-CHECK-DD-DATE                            <030>*/
		/*END-IF.                                                 <030>*/
		/*  If NOT bank billing                                            */
		/*  Billing date must be > or = Contract Commencement Date*/
		/*     and < Contract Commencement Date + 2*Billing Frequency - 1*/
		/*  Otherwise (when bank billing)                                  */
		/*  Billing date must be > or = 1st of Contract Commencement month */
		/*      and < 1st of Contract Commencement month + 1 Billing Freq. */
		/*                                               + 1 month         */
		wsaaOccdate9.set(sv.occdate);
		/* IF T3625-DDIND = 'Y'                                    <030>*/
		if (isNE(t3620rec.ddind, SPACES)) {
			wsaaOccdateDd.set("01");
		}
		if (isEQ(sv.billfreq, "00"))
		{
			sv.billcd.set(sv.occdate);
		}
		if (isLT(sv.billcd, wsaaOccdate9)) {
			sv.billcdErr.set(errorsInner.e481);
			//goTo(GotoLabel.checkAgainstTable2220);
			checkAgainstTable2220();
			return;

		}
		if (isEQ(sv.billfreq, "00")) {
			if (isNE(sv.occdate, sv.billcd)) {
				/*        MOVE U047             TO S5004-BILLCD-ERR              */
				sv.billcdErr.set(errorsInner.h078);
				//goTo(GotoLabel.checkAgainstTable2220);
				checkAgainstTable2220();
				return;
			}
			else {
				//goTo(GotoLabel.checkAgainstTable2220);
				checkAgainstTable2220();
				return;
			}
		}
		/*   Rather than commenting out the code below, go to the next     */
		/*   paragraph. The code forces the user to enter a specific       */
		/*   number of billing frequencies ahead. This should not be the   */
		/*   case if the user has specified the next billing date.         */

		checkAgainstTable2220();

	}

	/**
	 * <pre>
	 *****                                                        <030>
	 **Dates are validated before continuing processing           <030>
	 *****                                                        <030>
	 *2215-CHECK-DD-DATE.                                         <030>
	 *****                                                        <030>
	 *****MOVE SPACES                      TO S5004-BILLCD-ERR.   <030>
	 *****MOVE CHDRLNB-BTDATE              TO WSAA-BTDATE-9.      <030>
	 *****MOVE S5004-BILLCD                TO WSAA-BILLCD-9.      <030>
	 *****MOVE SPACES                      TO WSAA-CHK-FINISH.    <030>
	 *****PERFORM 2295-VALIDATE-BILLCD VARYING                    <030>
	 *****    WSAA-T5689-COUNT        FROM 1 BY 1 UNTIL           <030>
	 *****    (WSAA-CHK-FINISH         = 'Y') OR                  <030>
	 *****    (WSAA-T5689-COUNT > 30).                            <030>
	 *********(WSAA-T5689-COUNT > 29).                            <009>
	 *****                                                        <030>
	 *****IF WSAA-CHK-FINISH       NOT = 'Y'                      <030>
	 *****   MOVE E483                TO S5004-BILLCD-ERR         <030>
	 *****   GO TO 2290-EXIT                                      <030>
	 *****ELSE                                                    <030>
	 *****   MOVE S5004-OCCDATE       TO DTC3-INT-DATE-1          <030>
	 *****   MOVE S5004-BILLCD        TO DTC3-INT-DATE-2          <030>
	 *****   MOVE S5004-BILLFREQ      TO DTC3-FREQUENCY           <030>
	 *****                                                        <030>
	 *****   CALL 'DATCON3'           USING DTC3-DATCON3-REC      <030>
	 *****                                                        <030>
	 *****   IF DTC3-FREQ-FACTOR    NOT < 2                       <030>
	 *****      MOVE SC72             TO S5004-BILLCD-ERR         <030>
	 *****      GO TO 2290-EXIT                                   <030>
	 *****   END-IF                                               <030>
	 *****END-IF.                                                 <030>
	 *  Check the payment method/frequency/date are valid combination.
	 * </pre>
	 */
	protected void checkAgainstTable2220()
	{
		wsaaX.set(ZERO);
		wsaaBillcdOk.set(SPACES);
		wsaaMopFreqOk.set(SPACES);
		wsaaBillcd9.set(sv.billcd);
		PackedDecimalData loopEndVar2 = new PackedDecimalData(7, 0);
		loopEndVar2.set(wsaaMaxOcc);
		mopFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(),MOP_FEATURE_ID, appVars, "IT");
		if(mopFlag){
		for (int loopVar3 = 0; !(isEQ(loopVar3, loopEndVar2.toInt())); loopVar3 += 1){
			wsaaX.add(1);
			
			if (isEQ(t5689rec.mop[wsaaX.toInt()], sv.mop)){
				billDayList.add(t5689rec.rendd[wsaaX.toInt()].toInt());
			}
			if(!billDayList.equals(null) && !billDayList.isEmpty())
			Collections.sort(billDayList);
			
		}
		wsaaX.set(ZERO);
	}
		for (int loopVar3 = 0; !(isEQ(loopVar3, loopEndVar2.toInt())); loopVar3 += 1){
			findMopFreq2230();
		}
		count=0;
		/*IF WSAA-DEFAULT-BILLCD NOT = 'Y'                        <030>*/
		if (isEQ(wsaaBillcdOk, SPACES)) {
			sv.billcdErr.set(errorsInner.e487);
			//goTo(GotoLabel.exit2240);
			return;
		}
		if (isEQ(wsaaMopFreqOk, SPACES)) {
			sv.mopErr.set(errorsInner.e484);
			sv.billfreqErr.set(errorsInner.e484);
			if(ctaxPermission && isEQ(sv.mop,"S")) {
				sv.billfreqErr.set(errorsInner.rpj2);
				sv.mopErr.set(errorsInner.rpj2);
			}
			//goTo(GotoLabel.exit2240);
			return;
		}
		/*  FOR DD CASES ONLY                                              */
		/*  Calculate the Billed To date according to the Billing          */
		/*   Commencement date. If it is in the same month/year as the     */
		/*   Contract Commencement date + 1 billing frequence, then the    */
		/*   Billed To date is 1 frequence on from the Contract            */
		/*   Commencement date. Otherwise these two dates are the same.    */
		/*  OTHERWISE, FOR NON DD CASES                                    */
		/*  The billed to date is the requestred billing commencement      */
		/*   date.                                                         */
		/*IF WSAA-DEFAULT-BILLCD = 'Y'                            <030>*/
		/*   GO TO 2240-DEFAULT-BILLCD                            <030>*/
		/*ELSE                                                    <030>*/
		/* IF T3625-DDIND = 'Y'                                    <030>*/
		if(onePflag &&isEQ(sv.rskComOpt,"Y") && isEQ(sv.mop,"D") && isNE(t3615rec.onepcashless,"Y")){
			sv.billcd.set(sv.occdate);
			chdrlnbIO.setBtdate(sv.billcd);
			payrIO.setBtdate(sv.billcd);
		}
		else{
		if (isNE(t3620rec.ddind, SPACES) || isNE(t3620rec.crcind, SPACES)) {	//ILIFE-8322
			/*IF S5004-BILLCD          > S5004-OCCDATE             <030>*/
			datcon3rec.datcon3Rec.set(SPACES);
			datcon3rec.intDate1.set(sv.occdate);
			datcon3rec.intDate2.set(sv.billcd);
			datcon3rec.frequency.set(sv.billfreq);
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz, varcom.oK)) {
				sv.billcdErr.set(datcon3rec.statuz);
				//goTo(GotoLabel.exit2240);
				return;
			}
			if (isLT(datcon3rec.freqFactor, 0)) {
				sv.billcdErr.set(errorsInner.f738);
				//goTo(GotoLabel.exit2240);
				return;
			}
			wsaaSplitFreq.set(datcon3rec.freqFactor);
			datcon2rec.freqFactor.set(wsaaInteger);
			datcon2rec.intDate1.set(sv.occdate);
			/*MOVE S5004-BILLCD     TO DTC3-INT-DATE-2          <030>*/
			datcon2rec.frequency.set(sv.billfreq);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				sv.billcdErr.set(datcon2rec.statuz);
				//goTo(GotoLabel.exit2240);
				return;
			}
			/*       MOVE DTC2-INT-DATE-2  TO WSAA-OCCDATE-9        <CAS2.0>*/
			/*       MOVE S5004-BILLCD     TO WSAA-BILLCD-9         <CAS2.0>*/
			/*       IF WSAA-OCCDATE-MM    = WSAA-BILLCD-MM         <CAS2.0>*/
			/*        AND WSAA-OCCDATE-YYYY = WSAA-BILLCD-YYYY      <CAS2.0>*/
			chdrlnbIO.setBtdate(datcon2rec.intDate2);
			payrIO.setBtdate(datcon2rec.intDate2);
			/*****       ELSE                                           <CAS2.0>*/
			/*****          MOVE S5004-OCCDATE TO CHDRLNB-BTDATE,       <CAS2.0>*/
			/*****                                PAYR-BTDATE           <CAS2.0>*/
			/*****       END-IF                                         <CAS2.0>*/
			/************                                                  <030>*/
			/************IF DTC3-FREQ-FACTOR   NOT < 1                          */
			/************   NEXT SENTENCE                                  <030>*/
			/************ELSE                                              <030>*/
			/************   MOVE DTC3-FREQ-FACTOR TO WSAA-FACTOR           <030>*/
			/************   MOVE S5004-OCCDATE    TO DTC2-INT-DATE-1       <030>*/
			/************   MOVE ZEROES           TO DTC2-INT-DATE-2       <030>*/
			/************   MOVE S5004-BILLFREQ   TO DTC2-FREQUENCY        <030>*/
			/************   MOVE WSAA-FACTOR      TO DTC2-FREQ-FACTOR      <030>*/
			/************                                                  <030>*/
			/************   CALL 'DATCON2'        USING DTC2-DATCON2-REC   <030>*/
			/************                                                  <030>*/
			/************   IF DTC2-STATUZ        = O-K                    <030>*/
			/************      MOVE DTC2-INT-DATE-2 TO CHDRLNB-BTDATE,     <030>*/
			/************                              PAYR-BTDATE         <030>*/
			/************   ELSE                                           <030>*/
			/************      MOVE DTC2-STATUZ     TO S5004-BILLCD-ERR    <030>*/
			/************      GO TO 2240-EXIT                             <030>*/
			/************   END-IF                                         <030>*/
			/************END-IF                                            <030>*/
			/*********END-IF                                                    */
			/*********GO TO 2290-EXIT                                           */
		}
		else if(!(isbillday && isNE(sv.billday,SPACES))) {	//IJS-160
			chdrlnbIO.setBtdate(sv.billcd);
			payrIO.setBtdate(sv.billcd);
		}
		}
		//goTo(GotoLabel.exit2240);
	}

	protected void findMopFreq2230()
	{
		if (isNE(wsaaBillcdOk, "Y")) {
			wsaaX.add(1);
			if (isEQ(t5689rec.mop[wsaaX.toInt()], sv.mop)
					&& isEQ(t5689rec.billfreq[wsaaX.toInt()], sv.billfreq)) {
				wsaaMopFreqOk.set("Y");
				/*IF WSAA-DEFAULT-BILLCD = 'Y'                           */
				/*   MOVE T5689-RENDD(WSAA-X) TO WSAA-RENDD              */
				/*   MOVE 'Y'           TO WSAA-BILLCD-OK                */
				if(!mopFlag){
				if ((isEQ(t5689rec.rendd[wsaaX.toInt()], wsaaAll)
						|| isEQ(t5689rec.rendd[wsaaX.toInt()], wsaaRendd))
						|| (isEQ(t5689rec.rendd[wsaaX.toInt()], "31")
								&& isEQ(wsaaEndOfMonth, "Y"))
						|| (isEQ(t5689rec.rendd[wsaaX.toInt()], "30")
								|| isEQ(t5689rec.rendd[wsaaX.toInt()], "29")
								&& isEQ(wsaaMth, 2)
								&& isEQ(wsaaEndOfMonth, "Y"))) {
					wsaaBillcdOk.set("Y");
					}
				}else{
				setBillDay();
				}
			}
		}
	}

	/**
	 * <pre>
	 *2240-DEFAULT-BILLCD.
	 * </pre>
	 */
	protected void defaultBilling2250()
	{

		lookUpTable2260();

	}
	
	protected boolean readT6654(String key) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(chdrlnbIO.getChdrcoy().toString());
		itempf.setItemtabl(tablesInner.t6654.toString());
		itempf.setItemitem(key);
		itempf = itempfDAO.getItempfRecord(itempf);
		if (null != itempf) {
			t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			return true;
		}
		return false;
	}

	protected void lookUpTable2260()
	{
		/*  The version of the contract table item is dependant on         */
		/*     the effective date of the risk. So, if this date changes    */
		/*      re-read them in case another version now applys.           */
		if (isEQ(sv.occdateOut[varcom.chg.toInt()], "Y")) {
			readContractTables1600();
			if (isNE(sv.occdateErr, SPACES)) {
				//goTo(GotoLabel.exit2290);
				return;
			}
		}
		/*  Work out the Billed To Date - it will either be equal to       */
		/*  the Risk Commencement Date, (OCCDATE) or 1 frequency greater   */
		/*  than it (or there abouts depending on whether this is a        */
		/*  billing day)                                                   */
	String key;
	if(BTPRO028Permission) {
		key = sv.mop.toString().trim() + chdrlnbIO.getCnttype().toString().trim() + sv.billfreq.toString().toString().trim();
		if(!readT6654(key)) {
			key = sv.mop.toString().trim().concat("**");
			if(!readT6654(key)) {
				key = sv.mop.toString().trim().concat("*****");
				if(!readT6654(key)) {
					sv.mopErr.set(errorsInner.e840);
					return;
				}
			}
		}
		
	}
	else {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(chdrlnbIO.getChdrcoy().toString());
		itempf.setItemtabl(tablesInner.t6654.toString());
		wsaaT6654Key.set(SPACES);
		wsaaT6654Mop.set(sv.mop);
		wsaaT6654Cnttype.set(chdrlnbIO.getCnttype());
		itempf.setItemitem(wsaaT6654Key.toString());
		itempf = itempfDAO.getItempfRecord(itempf);

		if (null != itempf) {
			t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(chdrlnbIO.getChdrcoy().toString());
			itempf.setItemtabl(tablesInner.t6654.toString());
			wsaaT6654Cnttype.set("***");
			itempf.setItemitem(wsaaT6654Key.toString());
			itempf = itempfDAO.getItempfRecord(itempf);

			if (null != itempf) {
				t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
			else {
				sv.mopErr.set(errorsInner.e840);
				//goTo(GotoLabel.exit2290);
				return;

			}
		}
	}
		/* IF T6654-RENWDATE-2              = 'Y'                  <009>*/
		/*    MOVE S5004-OCCDATE           TO DTC2-INT-DATE-1      <009>*/
		/*    MOVE ZEROES                  TO DTC2-INT-DATE-2      <009>*/
		/*    MOVE S5004-BILLFREQ          TO DTC2-FREQUENCY       <009>*/
		/*    MOVE 1                       TO DTC2-FREQ-FACTOR     <009>*/
		/*    CALL 'DATCON2'               USING DTC2-DATCON2-REC  <009>*/
		/*    IF DTC2-STATUZ                = O-K                  <009>*/
		/*       MOVE DTC2-INT-DATE-2      TO CHDRLNB-BTDATE,      <009>*/
		/*                                    PAYR-BTDATE          <017>*/
		/*                                    WSAA-BTDATE-9        <035>*/
		if (isEQ(t6654rec.renwdate2, "Y")) {
			wsaaOdate.set(sv.occdate);
			datcon4rec.datcon4Rec.set(SPACES);
			datcon4rec.billday.set(wsaaOdateDd);
			datcon4rec.billmonth.set(wsaaOdateMm);
			datcon4rec.intDate1.set(sv.occdate);
			datcon4rec.intDate2.set(ZERO);
			datcon4rec.frequency.set(sv.billfreq);
			datcon4rec.freqFactor.set(1);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isEQ(datcon4rec.statuz, varcom.oK)) {
				chdrlnbIO.setBtdate(datcon4rec.intDate2);
				payrIO.setBtdate(datcon4rec.intDate2);
				wsaaBtdate9.set(datcon4rec.intDate2);
			}
			else {
				sv.billcdErr.set(errorsInner.sc72);
			}
		}
		else {
			if (isEQ(t6654rec.renwdate1, "Y")) {
				chdrlnbIO.setBtdate(sv.occdate);
				payrIO.setBtdate(sv.occdate);
				wsaaBtdate9.set(sv.occdate);
			}
			else {
				sv.billcdErr.set(errorsInner.f665);
				//goTo(GotoLabel.exit2290);
				return;
			}
		}
		/* The following code replaces the above code. Instead of       */
		/* the billing date being defaulted to risk commencement        */
		/* date or the RCD + 1 frequency, there is now the option       */
		/* to specify a number of frequencies ahead.                    */
		if (isEQ(t6654rec.renwdate1, "Y")) {
			chdrlnbIO.setBtdate(sv.occdate);
			payrIO.setBtdate(sv.occdate);
			wsaaBtdate9.set(sv.occdate);
			//goTo(GotoLabel.ddCases2265);
			ddCases2265();
			return;
		}
		if (isNE(t6654rec.renwdate2, "Y")) {
			sv.billcdErr.set(errorsInner.f665);
			//goTo(GotoLabel.exit2290);
			return;
		}
		if(!BTPRO028Permission) {
		wsaaSub.set(1);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 6)
				|| isEQ(t6654rec.zrfreq[wsaaSub.toInt()], sv.billfreq)); wsaaSub.add(1)){
			readFreqs5000();
		}
		if (isGT(wsaaSub, 6)) {
			sv.billcdErr.set(errorsInner.r081);
			//goTo(GotoLabel.exit2290);
			return;
		}
		}
		wsaaOdate.set(sv.occdate);
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.billday.set(wsaaOdateDd);
		datcon4rec.billmonth.set(wsaaOdateMm);
		datcon4rec.intDate1.set(sv.occdate);
		datcon4rec.intDate2.set(ZERO);
		datcon4rec.frequency.set(sv.billfreq);
		if(BTPRO028Permission) {
			wsaaSub.set(1);
			datcon4rec.freqFactor.set(t6654rec.zrincr[wsaaSub.toInt()]);	
		}
		else {
		datcon4rec.freqFactor.set(t6654rec.zrincr[wsaaSub.toInt()]);
		}
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isEQ(datcon4rec.statuz, varcom.oK)) {
			chdrlnbIO.setBtdate(datcon4rec.intDate2);
			payrIO.setBtdate(datcon4rec.intDate2);
			wsaaBtdate9.set(datcon4rec.intDate2);
		}
		else {
			sv.occdateErr.set(errorsInner.sc72);
			//goTo(GotoLabel.exit2290);
			return;
		}
		ddCases2265();
	}

	/**
	 * <pre>
	 **** The following code gets the default billing date from   <030>
	 **********                                                   <030>
	 *******default billing commencement date is                  <030>
	 **********contract commencement plus one frequency.          <030>
	 ***  MOVE S5004-BILLFREQ         TO DTC2-FREQUENCY.
	 ***  MOVE 1                      TO DTC2-FREQ-FACTOR.
	 ***  MOVE S5004-OCCDATE          TO DTC2-INT-DATE-1.
	 ***  CALL 'DATCON2' USING DTC2-DATCON2-REC.
	 * </pre>
	 */
	protected void ddCases2265()
	{
		/*  If all billing dates are not allowed ("day" not 99),*/
		/*    this, or the next "day" immediatly after this is used.*/
		/*    After calculating this date, check it is not off the end*/
		/*    of the month (e.g. 30 Feb). If it is, move one day*/
		/*    backwards until it is a valid date.*/
		/*  FOR DIRECT DEBIT CASES,*/
		/*    only days within the applicable month are allowed, so if*/
		/*    there is no billing day after the default date, use the*/
		/*    first one prior to it.*/
		wsaaEarliest.set(99);
		wsaaNextUp.set(99);
		wsaaNextDown.set(ZERO);
		/*IF WSAA-DEFAULT-BILLCD           = 'Y'                  <030>*/
		for (wsaaT5689Count.set(1); !((isNE(sv.billcd, varcom.vrcmMaxDate))
				|| (isGT(wsaaT5689Count, 30))); wsaaT5689Count.add(1)){
			searchForTableMatch2800();
		}
		/*(WSAA-T5689-COUNT = 30)                            <009>*/
		/*END-IF.                                                 <030>*/
		/*  If the defaulted billing date is not available as a billing*/
		/*   day .....*/
		if (isEQ(sv.billcd, varcom.vrcmMaxDate)) {
			if (isEQ(wsaaNextUp, 99)) {
				if (isEQ(wsaaNextDown, 0)) {
					sv.billfreqErr.set(errorsInner.e484);
					sv.mopErr.set(errorsInner.e484);
					if(ctaxPermission && isEQ(sv.mop,"S")) {
						sv.billfreqErr.set(errorsInner.rpj2);
						sv.mopErr.set(errorsInner.rpj2); //todo
					}
					return ;
				}
				else {
					/*IF T3625-DDIND = 'Y'                           <030>*/
					if (isNE(t3620rec.ddind, SPACES)
							|| isNE(t3620rec.grpind, SPACES)) {
						/*                MOVE WSAA-NEXT-DOWN  TO WSAA-BTDATE-DD    <CAS1.0*/
						wsaaBtdateDd.set(wsaaNextUp);
						/*  The following probably needs some explanation. To ensure that  */
						/*  for DD cases we get the following default next billing dates   */
						/*  (assuming no. of frequencies is 3) :                           */
						/*      OCCDATE            BILLCD                                  */
						/*      02/06/96           15/09/96                                */
						/*      28/06/96           01/10/96                                */
						/*  we always move the next day into the billing day. If however   */
						/*  it is 99, then move the earliest day in. We have now gone into */
						/*  a new month so update the month. If the the month is greater   */
						/*  than 12 we are now in a new year so add 1 to the year.         */
						if (isEQ(wsaaNextUp, "99")) {
							wsaaBtdateDd.set(wsaaEarliest);
							wsaaBtdateMm.add(1);
							if (isGT(wsaaBtdateMm, 12)) {
								wsaaBtdateMm.set(1);
								wsaaBtdateYyyy.add(1);
							}
						}
					}
					else {
						wsaaBtdateDd.set(wsaaEarliest);
						wsaaBtdateMm.add(1);
						if (isGT(wsaaBtdateMm, 12)) {
							wsaaBtdateMm.set(1);
							wsaaBtdateYyyy.add(1);
						}
					}
					sv.billcd.set(wsaaBtdate);
				}
			}
			else {
				wsaaBtdateDd.set(wsaaNextUp);
				sv.billcd.set(wsaaBtdate);
			}
		}
		/*MOVE CHDRLNB-BTDATE              TO WSAA-BTDATE-9.      <030>*/
		/*MOVE S5004-BILLCD                TO WSAA-BILLCD-9.      <030>*/
		/*IF WSAA-BTDATE-MM             NOT = WSAA-BILLCD-MM      <030>*/
		/*   MOVE I012                     TO S5004-BILLCD-ERR    <030>*/
		/*ELSE                                                    <030>*/
		/*   IF WSAA-BTDATE-YYYY        NOT = WSAA-BILLCD-YYYY    <030>*/
		/*      MOVE J002                  TO S5004-BILLCD-ERR.   <030>*/
		/*                                                        <030>*/
		/*  IF WSAA-RENDD NOT = WSAA-ALL                                 */
		/*     GO TO CONTINUE-ON.                                  <009> */
		/*                                                         <009> */
		/*  IF WSAA-RENDD = WSAA-ALL                                     */
		/*     MOVE DTC2-INT-DATE-2     TO S5004-BILLCD            <009> */
		/*                                 WSAA-RCD.               <009> */
		/*MOVE WSAA-BTDATE         TO WSAA-BILLCD             <030> */
		/*                            WSAA-RCD.               <030> */
		/*   IF WSAA-MTH = 4 OR 6 OR 9 OR 11                      <030>*/
		/*      AND WSAA-DAY = 30                                 <030>*/
		/*      MOVE 'Y'                 TO WSAA-END-OF-MONTH     <030>*/
		/*   ELSE                                                 <030>*/
		/*      MOVE SPACE TO WSAA-END-OF-MONTH.                  <030>*/
		/*   IF WSAA-MTH = 2                                      <030>*/
		/*      AND WSAA-DAY = 28 OR 29                           <030>*/
		/*      MOVE 'Y'                 TO WSAA-END-OF-MONTH     <030>*/
		/*   ELSE                                                 <030>*/
		/*      MOVE SPACE TO WSAA-END-OF-MONTH.                  <030>*/
		/*                                                        <030>*/
		/*  ELSE                                                         */
		/*CONTINUE-ON.                                                <009>*/
		/*  IF WSAA-RENDD = WSAA-ALL                                <009>*/
		/*     GO TO 2290-EXIT.                                     <009>*/
		/*     MOVE DTC2-INT-DATE-2     TO WSAA-RCD                      */
		/*     MOVE WSAA-BTDATE         TO WSAA-RCD                      */
		/*     IF WSAA-RENDD NOT < WSAA-DAY                              */
		/*        MOVE WSAA-RENDD       TO WSAA-DAY                      */
		/*        MOVE WSAA-RCD         TO S5004-BILLCD                  */
		/*     ELSE                                                      */
		/*        MOVE '12'             TO DTC2-FREQUENCY                */
		/*        MOVE DTC2-INT-DATE-2  TO DTC2-INT-DATE-1               */
		/*        CALL 'DATCON2' USING DTC2-DATCON2-REC                  */
		/*        MOVE DTC2-INT-DATE-2  TO WSAA-RCD                      */
		/*        MOVE WSAA-RENDD       TO WSAA-DAY                      */
		/*        MOVE WSAA-RCD         TO S5004-BILLCD.                 */
		/*   Adjust for month end                                          */
		wsaaBillcd.set(sv.billcd);
		wsaaRcd.set(sv.billcd);
		if ((isEQ(wsaaMth, 4)
				|| isEQ(wsaaMth, 6)
				|| isEQ(wsaaMth, 9)
				|| isEQ(wsaaMth, 11))
				&& (isEQ(wsaaRendd, "31"))) {
			wsaaDay.set(30);
			sv.billcd.set(wsaaRcd);
		}
		if ((isEQ(wsaaMth, 2))
				&& (isEQ(wsaaRendd, "29")
						|| isEQ(wsaaRendd, "30")
						|| isEQ(wsaaRendd, "31"))) {
			wsaaDay.set(29);
			sv.billcd.set(wsaaRcd);
			datcon1rec.intDate.set(wsaaRcd);
			datcon1rec.function.set(varcom.conv);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz, varcom.oK)) {
				wsaaDay.set(28);
				sv.billcd.set(wsaaRcd);
			}
		}
		/* If not DD, set billed to date to the adjusted billing date.     */
		/* IF T3625-DDIND              NOT = 'Y'                   <030>*/
		if (isEQ(t3620rec.ddind, SPACES)) {
			chdrlnbIO.setBtdate(sv.billcd);
			payrIO.setBtdate(sv.billcd);
		}
	}

	/**
	 * <pre>
	 *****                                                        <030>
	 ******************************************                   <030>
	 *2295-VALIDATE-BILLCD            SECTION.                    <030>
	 ******************************************                   <030>
	 ******                                                       <030>
	 *MATCH-MOP-BILLFREQ.                                         <030>
	 *****IF T5689-BILLFREQ(WSAA-T5689-COUNT) = S5004-BILLFREQ AND<030>
	 *****   T5689-MOP(WSAA-T5689-COUNT)   = S5004-MOP            <030>
	 *****   GO TO FIND-DATE-MATCH                                <030>
	 *****ELSE                                                    <030>
	 *****   GO TO 2295-EXIT                                      <030>
	 *****END-IF.                                                 <030>
	 *****                                                        <030>
	 *FIND-DATE-MATCH.                                            <030>
	 *****IF T5689-RENDD(WSAA-T5689-COUNT) = '99'                 <030>
	 *****   MOVE 'Y'                     TO WSAA-CHK-FINISH      <030>
	 *****   GO TO 2295-EXIT                                      <030>
	 *****ELSE                                                    <030>
	 *****   MOVE S5004-BILLCD            TO WSAA-BILLCD-9        <030>
	 *****   GO TO DAY-CHECK                                      <030>
	 *****END-IF.                                                 <030>
	 *****                                                        <030>
	 *DAY-CHECK.                                                  <030>
	 *****IF T5689-RENDD(WSAA-T5689-COUNT) = WSAA-BILLCD-DD       <030>
	 *****   MOVE 'Y'                     TO WSAA-CHK-FINISH      <030>
	 *****END-IF.                                                 <030>
	 *****                                                        <030>
	 *2295-EXIT.                                                  <030>
	 *****EXIT.                                                   <030>
	 *****                                                        <030>
	 *****                                                        <030>
	 * </pre>
	 */
	protected void checkOldContract2300()
	{
		readHeader2310();
	}

	protected void readHeader2310()
	{
		/*  Save current contract details.*/
		chdrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/*  Read old contract.*/
		chdrlnbIO.setDataKey(SPACES);
		chdrlnbIO.setChdrcoy(wsspcomn.company);
		chdrlnbIO.setChdrnum(sv.lrepnum);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
				&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/* IF CHDRLNB-STATUZ              = ENDP                        */
		if (isEQ(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			sv.lrepnumErr.set(errorsInner.f849);
		}
		/*  Retrieve current contract details.*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}

	protected void checkBankRequired2400()
	{
		readPaymentPlan2410();
	}

	protected void readPaymentPlan2410()
	{
		/* MOVE SPACES                 TO ITEM-DATA-KEY.                */
		/* MOVE 'IT'                   TO ITEM-ITEMPFX.                 */
		/* MOVE WSSP-COMPANY           TO ITEM-ITEMCOY.                 */
		/* MOVE T3625                  TO ITEM-ITEMTABL.                */
		/* STRING 'LP'                                                  */
		/*        S5004-MOP                                             */
		/*        S5004-BILLFREQ       DELIMITED SIZE                   */
		/*                             INTO ITEM-ITEMITEM.              */
		/* MOVE 'READR'                TO ITEM-FUNCTION.                */
		/* CALL 'ITEMIO' USING ITEM-PARAMS.                             */
		/* IF ITEM-STATUZ              NOT = O-K                        */
		/*                         AND NOT = MRNF                       */
		/*     MOVE ITEM-PARAMS        TO SYSR-PARAMS              <015>*/
		/*     MOVE ITEM-STATUZ        TO SYSR-STATUZ                   */
		/*     PERFORM 600-FATAL-ERROR.                                 */
		/* IF ITEM-STATUZ              = MRNF                           */
		/*     MOVE U023               TO S5004-MOP-ERR,           <026>*/
		/*     MOVE F941               TO S5004-MOP-ERR,           <026>*/
		/*                                S5004-BILLFREQ-ERR            */
		/*     GO TO 2490-EXIT.                                         */
		/* MOVE ITEM-GENAREA           TO T3625-T3625-REC.              */
		/* Read Table T3620.*/

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.t3620.toString());
		itempf.setItemitem(sv.mop.toString());
		itempf = itempfDAO.getItempfRecord(itempf);
		if (null == itempf) {
			sv.mopErr.set(errorsInner.f921);
			return ;
		}
		t3620rec.t3620Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		/* Bank details only allowed for certain types of billing*/
		/* If they are allowed, they are mandatory.*/
		if (isEQ(t3620rec.crcind, SPACES)
				&& isNE(chdrlnbIO.getMandref(), SPACES)
				&& creditCardMand.isTrue()) {
			chdrlnbIO.setMandref(SPACES);
			payrIO.setMandref(SPACES);
			chdrlnbIO.setBankkey(SPACES);
			chdrlnbIO.setBankacckey(SPACES);
		}
		/* IF T3625-DDIND = 'Y'                                         */
		if (isNE(t3620rec.ddind, SPACES)) {
			/*AND CHDRLNB-BANKKEY = SPACES                                */
			if (isEQ(chdrlnbIO.getMandref(), SPACES)) {
				sv.ddind.set("X");
			}
		}
		/* IF T3625-DDIND              NOT = 'Y' AND               <004>*/
		if (isEQ(t3620rec.ddind, SPACES)
				&& bankAccountMand.isTrue()
				&& isNE(chdrlnbIO.getMandref(), SPACES)) {
			chdrlnbIO.setMandref(SPACES);
			payrIO.setMandref(SPACES);
			chdrlnbIO.setBankkey(SPACES);
			chdrlnbIO.setBankacckey(SPACES);
		}
		/* IF T3625-DDIND              NOT = 'Y'                   <035>*/
		if (isEQ(t3620rec.ddind, SPACES)) {
			//MIBT-53
			if (isEQ(sv.ddind, "X") && isEQ(sv.mop, "D")) {
				sv.ddindErr.set(errorsInner.e492);
			}
			else {
				sv.ddind.set(SPACES);
			}
		}
		/*    IF S5004-DDIND = 'X'                                      */
		/*       MOVE E492             TO S5004-DDIND-ERR               */
		/*    ELSE                                                      */
		/*       MOVE SPACE            TO S5004-DDIND.                  */
		/* Credit card details only allowed for certain types of billing   */
		/* If they are allowed, they are mandatory.                        */
		if (isNE(t3620rec.crcind, SPACES)
				&& isEQ(chdrlnbIO.getMandref(), SPACES)) {
			sv.crcind.set("X");
		}
		if (isEQ(t3620rec.crcind, SPACES)) {
			/*bug #ILIFE-974 start*/
			if (isEQ(sv.crcind, "X")&& isEQ(sv.mop, "R")) {
				/*bug #ILIFE-974 END*/
				sv.crcindErr.set(errorsInner.e492);
			}
			else {
				sv.crcind.set(SPACES);
			}
		}
		/* Group details only allowed for certain types of billing*/
		/* If they are allowed, they are mandatory.*/
		/* IF T3625-GRPIND = 'Y'                                        */
		if (isNE(t3620rec.grpind, SPACES)
				&& isEQ(chdrlnbIO.getGrupkey(), SPACES)) {
			sv.grpind.set("X");
		}
		/*    IF T3625-GRPIND NOT = 'Y'                                    */
		/*     AND CHDRLNB-GRUPKEY NOT = SPACES                            */
		/*       MOVE SPACES              TO CHDRLNB-GRUPKEY               */
		/*       MOVE SPACES              TO CHDRLNB-MEMBSEL               */
		/*       IF S5004-GRPIND = 'X'                                     */
		/*          MOVE E492             TO S5004-GRPIND-ERR              */
		/*       ELSE                                                      */
		/*          MOVE SPACE            TO S5004-GRPIND.                 */
		/* IF T3625-GRPIND NOT = 'Y'                               <003>*/
		if (isEQ(t3620rec.grpind, SPACES)
				&& isNE(chdrlnbIO.getGrupkey(), SPACES)) {
			chdrlnbIO.setGrupkey(SPACES);
			payrIO.setGrupnum(SPACES);
			payrIO.setMembsel(SPACES);
			chdrlnbIO.setMembsel(SPACES);
		}
		/* IF T3625-GRPIND NOT = 'Y'                               <003>*/
		if (isEQ(t3620rec.grpind, SPACES)) {
			payrIO.setGrupcoy(SPACES);
			payrIO.setGrupnum(SPACES);
			payrIO.setMembsel(SPACES);
			payrIO.setBillnet(SPACES);
			if (isNE(sv.grpind, SPACES)) {
				sv.grpindErr.set(errorsInner.e492);
			}
		}
		/* Trans. History will only allow after rec been added/updated     */
		/* into CHDR. Hence, for proposal creation, the access to Trans.   */
		/* History should not allow.                                       */
		if (isEQ(wsspcomn.flag, "C")
				&& isEQ(sv.transhist, "X")) {
			sv.transhistErr.set(errorsInner.e492);
		}
	}

	protected void crossCheckCurr2500()
	{

		para2501();

	}

	protected void para2501()
	{   


		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.t6660.toString());
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf = itempfDAO.getItempfRecord(itempf);

		if (null != itempf) {
			//goTo(GotoLabel.itemFound2510);
			itemFound2510();
			return;
		}
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.t6660.toString());
		itempf.setItemitem("***");
		itempf = itempfDAO.getItempfRecord(itempf);
		if (null != itempf) {
			//goTo(GotoLabel.itemFound2510);
			itemFound2510();
			return;
		}
		/* MOVE F955                   TO S5004-CNTCURR-ERR             */
		sv.cntcurrErr.set(errorsInner.f059);
		sv.billcurrErr.set(errorsInner.f059);
		//goTo(GotoLabel.exit2590);
	}

	protected void itemFound2510()
	{
		t6660rec.t6660Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		wsaaY.set(1);
		/* Match contract currency vertically.*/
		wsaaCntcurrMatch = "N";
		while ( !((isGT(wsaaY, 9))
				|| (isEQ(wsaaCntcurrMatch, "Y")))) {
			matchCntcurr2600();
		}

		if (isEQ(wsaaCntcurrMatch, "N")) {
			sv.cntcurrErr.set(errorsInner.f059);
			/*     MOVE F955               TO S5004-CNTCURR-ERR             */
			/*                                S5004-BILLCURR-ERR            */
			return ;
		}
		/* Once contract currency is found, match the billing currency*/
		/* with the corresponding valid entries(horizontal) in the table.*/
		wsaaIndex.set(1);
		compute(wsaaHorizontal, 0).set(mult((sub(wsaaY, 1)), 10));
		wsaaBillcurrMatch = "N";
		while ( !((isGT(wsaaIndex, 10))
				|| (isEQ(wsaaBillcurrMatch, "Y")))) {
			matchBillcurr2700();
		}

		if (isEQ(wsaaBillcurrMatch, "N")) {
			sv.billcurrErr.set(errorsInner.f059);
			/*     MOVE F955               TO S5004-CNTCURR-ERR             */
			/*                                S5004-BILLCURR-ERR            */
		}
	}

	protected void matchCntcurr2600()
	{
		/*PARA*/
		if (isEQ(t6660rec.cntcurr[wsaaY.toInt()], sv.cntcurr)) {
			wsaaCntcurrMatch = "Y";
			return ;
		}
		wsaaY.add(1);
		/*EXIT*/
	}

	protected void matchBillcurr2700()
	{
		/*PARA*/
		compute(wsaaX, 0).set(add(wsaaHorizontal, wsaaIndex));
		if (isEQ(t6660rec.acctccy[wsaaX.toInt()], sv.billcurr)) {
			wsaaBillcurrMatch = "Y";
			return ;
		}
		wsaaIndex.add(1);
		/*EXIT*/
	}

	protected void searchForTableMatch2800()
	{
		lookUpT56892810();
	}

	protected void lookUpT56892810()
	{
		if (isEQ(t5689rec.billfreq[wsaaT5689Count.toInt()], sv.billfreq)
				&& isEQ(t5689rec.mop[wsaaT5689Count.toInt()], sv.mop)) {
			//goTo(GotoLabel.findMatch2830);
			findMatch2830();
		}
	}

	protected void findMatch2830()
	{
		if (isEQ(t5689rec.rendd[wsaaT5689Count.toInt()], "99")) {
			sv.billcd.set(chdrlnbIO.getBtdate());
			//goTo(GotoLabel.exit2890);
		}
		else {
			wsaaBtdate9.set(chdrlnbIO.getBtdate());
			//goTo(GotoLabel.dayCheck2840);
			dayCheck2840();
		}
	}

	protected void dayCheck2840()
	{
		if (isEQ(t5689rec.rendd[wsaaT5689Count.toInt()], wsaaBtdateDd)) {
			sv.billcd.set(chdrlnbIO.getBtdate());
			//goTo(GotoLabel.exit2890);
		}
		else {
			if (isLT(t5689rec.rendd[wsaaT5689Count.toInt()], wsaaEarliest)) {
				wsaaEarliest.set(t5689rec.rendd[wsaaT5689Count.toInt()]);
			}
			if (isLT(wsaaBtdateDd, t5689rec.rendd[wsaaT5689Count.toInt()])) {
				//goTo(GotoLabel.dayMoveUp2850);
				dayMoveUp2850();
			}
			else {
				//goTo(GotoLabel.dayMoveDown2860);
				dayMoveDown2860();
			}
		}
	}

	protected void dayMoveUp2850()
	{
		if (isLT(t5689rec.rendd[wsaaT5689Count.toInt()], wsaaNextUp)) {
			wsaaNextUp.set(t5689rec.rendd[wsaaT5689Count.toInt()]);
			//goTo(GotoLabel.exit2890);
		}
	}

	protected void dayMoveDown2860()
	{
		if (isGT(t5689rec.rendd[wsaaT5689Count.toInt()], wsaaNextDown)) {
			wsaaNextDown.set(t5689rec.rendd[wsaaT5689Count.toInt()]);
		}
	}

	
	
	
	
//ILIFE-7820-Start
protected void checkAgainstT6654() {
	String key;
	if(BTPRO028Permission) {
		key = sv.mop.toString().trim() + chdrlnbIO.getCnttype().toString().trim() + sv.billfreq.toString().toString().trim();
		if(!readT6654(key)) {
			key = sv.mop.toString().trim().concat("**");
			if(!readT6654(key)) {
				key = sv.mop.toString().trim().concat("*****");
				if(!readT6654(key)) {
					sv.mopErr.set(errorsInner.e840);
					return;
				}
			}
		}
		
	}
	else {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(chdrlnbIO.getChdrcoy().toString());
		itempf.setItemtabl(tablesInner.t6654.toString());
		wsaaT6654Key.set(SPACES);
		wsaaT6654Mop.set(sv.mop);
		wsaaT6654Cnttype.set(chdrlnbIO.getCnttype());
		itempf.setItemitem(wsaaT6654Key.toString());
		itempf = itempfDAO.getItempfRecord(itempf);

		if (null != itempf) {
			t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(chdrlnbIO.getChdrcoy().toString());
			itempf.setItemtabl(tablesInner.t6654.toString());
			wsaaT6654Cnttype.set("***");
			itempf.setItemitem(wsaaT6654Key.toString());
			itempf = itempfDAO.getItempfRecord(itempf);

			if (null != itempf) {
				t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
			else {
				sv.mopErr.set(errorsInner.e840);
				//goTo(GotoLabel.exit2290);

			}
		}
	}
}
//ILIFE-7820-Ends
	/**
	 * <pre>
	 *     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	 * </pre>
	 */
	protected void update3000()
	{
		try {
			updateDatabase3010();
			keepsChdrlnb3030();
			if(isBancassurance) {
			insertupdatebnkoutpf();
			}
			if(NBPRP117permission)
			{
			   checkFollowup();
			}
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

	//ICIL-147 start
	protected void insertupdatebnkoutpf()
	{
		if (isEQ(wsspcomn.flag, "C") 
				|| (isEQ(wsspcomn.flag, "M") && bnkoutpf == null)) {
			bnkoutpf = new Bnkoutpf();
			bnkoutpf.setChdrnum(sv.chdrnum.toString());
			bnkoutpf.setBnkout(sv.bnkout.toString());
			bnkoutpf.setBnkoutname(sv.bnkoutname.toString());
			bnkoutpf.setBnksm(sv.bnksm.toString());
			bnkoutpf.setBnksmname(sv.bnksmname.toString());
			bnkoutpf.setBnktel(sv.bnktel.toString());
			bnkoutpf.setBnktelname(sv.bnktelname.toString());
			bnkoutpf.setAgntnum(sv.agntsel.toString().trim());
			bnkoutpfDAO.insertBnkoutpf(bnkoutpf);
		}
		if (isEQ(wsspcomn.flag, "M")) {
			
			bnkoutpf.setChdrnum(sv.chdrnum.toString());
			bnkoutpf.setBnkout(sv.bnkout.toString());
			bnkoutpf.setBnkoutname(sv.bnkoutname.toString());
			bnkoutpf.setBnksm(sv.bnksm.toString());
			bnkoutpf.setBnksmname(sv.bnksmname.toString());
			bnkoutpf.setBnktel(sv.bnktel.toString());
			bnkoutpf.setBnktelname(sv.bnktelname.toString());
			bnkoutpf.setAgntnum(sv.agntsel.toString().trim());
			bnkoutpfDAO.updateBnkoutRecord(bnkoutpf);
			
		}
	}
	//ICIL-147 end



	protected void updateDatabase3010()
	{
		/* if returning from an optional selection skip this section.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit3090);

		}
		if(isEQ(sv.ownersel.trim(), wsspcomn.chdrCownnum.trim()))
		{
			//goTo(GotoLabel.exit3090);
			return;  	/* For ILIFE-5477, exit statement has been commented and return is used. */
		}

		/**
		 * <pre>
		 *  These fields are set in the 2000 section during billing
		 *  validation. Thus, the  moves are not required here as well.
		 *****IF S5004-MOP             NOT =  'B'                     <011>
		 *****IF*T3625-DDIND           NOT = 'Y'                      <030>
		 ********MOVE SPACES              TO CHDRLNB-MANDREF,         <030>
		 *********                           PAYR-MANDREF.            <030>
		 *****IF*(WSSP-FLAG                 = 'M')                    <030>
		 ********MOVE CHDRLNB-BTDATE      TO WSAA-BTDATE-9            <030>
		 ********MOVE S5004-BILLCD        TO WSAA-BILLCD              <030>
		 ********MOVE WSAA-BILLCD-MM      TO WSAA-BTDATE-MM           <030>
		 ********MOVE WSAA-BILLCD-YYYY    TO WSAA-BTDATE-YYYY         <030>
		 ********MOVE WSAA-BTDATE-9       TO CHDRLNB-BTDATE,          <030>
		 *********                           PAYR-BTDATE              <030>
		 *****END-IF.                                                 <030>
		 *****IF*S5004-MOP             NOT =  'B'                     <011>
		 *****IF*T3625-DDIND           NOT = 'Y'                      <030>
		 *********MOVE S5004-BILLCD       TO CHDRLNB-BTDATE,          <030>
		 *********                           PAYR-BTDATE.             <030>
		 *********                                                    <030>
		 * </pre>
		 */


		/* FATCA Auto Follow ups */

		if(isFatcaAllowed){

			wsspcomn.chdrCownnum.set(sv.ownersel);

			if(isEQ(th506rec.fatcaFlag,"Y"))	{	 
				clts = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString().trim(), sv.ownersel.toString().trim());
				if(clts != null){
					nationality = clts.getNatlty();//IJTI-1410
					birthplace = clts.getCtryorig();//IJTI-1410
				}else{
					fatalError600();
				}

				if(isEQ(nationality, wsaaUSA))
					natlty = 'Y';
				else
					natlty = 'N';

				if(isEQ(birthplace, wsaaUSA))
					birthp = 'Y';
				else
					birthp = 'N';
				statFlag = false;
				clft = new Clftpf();
				clft=clftpfDAO.getRecordByClntnum("CN","9",sv.ownersel.toString().trim());  //ILIFE-6293
				if(null != clft )
				{
					usTaxPayor = clft.getFftax();

					if(isEQ(usTaxPayor,'Y'))
					{
						statFlag = true; 
					}
					tr5awList = itempfDAO.getAllitems(smtpfxcpy.item.toString(), wsspcomn.company.toString(), "TR5AW");

					if (tr5awList.size() > wsaaTr5awSize) {
						fatalError600();
					}
					loadTr5aw1500(tr5awList);


					wsaaTr5awIx.set(1);
					fluppf = new Fluppf();

					for (; isLT(wsaaTr5awIx, wsaaTr5awArrayInner.wsaaTr5awRec.length); wsaaTr5awIx.add(1)){


						if(statFlag)
						{
							fluppf.setFupCde(wsaaTr5awArrayInner.wsaaFupCde[wsaaTr5awIx.toInt()].toString());
							break;
						}
						else if(isEQ(wsaaTr5awArrayInner.wsaacngtotaxpyr[wsaaTr5awIx.toInt()],usTaxPayor)
								&& isEQ(wsaaTr5awArrayInner.wsaacngtobrthctry[wsaaTr5awIx.toInt()],birthp))
						{
							fluppf.setFupCde(wsaaTr5awArrayInner.wsaaFupCde[wsaaTr5awIx.toInt()].toString());
							break;
						}
					}


					/* insert follow ups */

					fluppf.setChdrcoy(wsspcomn.company.charat(0));
					fluppf.setChdrnum(chdrlnbIO.getChdrnum().toString());
					fluppf.setFupNo(0);
					fluppf.setFupNo(1);
					fluppf.setTranNo(chdrlnbIO.getTranno().toInt());
					fluppf.setFupTyp('P');
					fluppf.setLife("01");
					fluppf.setjLife("");
					fluppf.setFupSts('L');
					fluppf.setFupDt(wsaaToday.toInt());
					fluppf.setFupRmk(descpf.getLongdesc());
					fluppf.setClamNum("");
					fluppf.setUserT(varcom.vrcmUser.toInt());
					fluppf.setTermId(varcom.vrcmTermid.toString());
					fluppf.setTrdt(varcom.vrcmDate.toInt());
					fluppf.setTrtm(varcom.vrcmTime.toInt());
					fluppf.setzAutoInd('Y');
					fluppf.setUsrPrf(varcom.vrcmUser.toString());
					fluppf.setJobNm(wsspcomn.userid.toString());
					fluppf.setEffDate(wsaaToday.toInt());
					fluppf.setCrtUser(varcom.vrcmUser.toString());
					fluppf.setCrtDate(wsaaToday.toInt());
					fluppf.setLstUpUser(varcom.vrcmUser.toString());
					fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
					fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
					fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
					fluppfDAO.insertFlupRecord(fluppf);
					/* update clft to set the status of client to pend */
					clft.setFfsts("PEND");
					clftpfDAO.updateClftRecord(clft);
				}
			}
		}
		
		
		
	}

	protected void keepsChdrlnb3030()
	{
		/*  Move fields from the screen to the contract header.*/
		chdrlnbIO.setCownpfx("CN");
		chdrlnbIO.setCowncoy(wsspcomn.fsuco);
		wsaaPlsuff.set(sv.plansuff);
		chdrlnbIO.setPolinc(wsaaPlanSuffix);
		chdrlnbIO.setCownnum(sv.ownersel);
		chdrlnbIO.setOccdate(sv.occdate);
		chdrlnbIO.setBillfreq(sv.billfreq);
		chdrlnbIO.setBillchnl(sv.mop);
		chdrlnbIO.setCollchnl(sv.mop);
		chdrlnbIO.setReqntype(sv.reqntype);//ILIFE-2472
		chdrlnbIO.setBillcd(sv.billcd);
		chdrlnbIO.setCurrfrom(sv.occdate);
		chdrlnbIO.setCntcurr(sv.cntcurr);
		chdrlnbIO.setBillcurr(sv.billcurr);
		chdrlnbIO.setRegister(sv.register);
		chdrlnbIO.setSrcebus(sv.srcebus);
		chdrlnbIO.setReptype(sv.reptype);
		chdrlnbIO.setRepnum(sv.lrepnum);
		chdrlnbIO.setCntbranch(sv.cntbranch);
		chdrlnbIO.setAgntpfx("AG");
		chdrlnbIO.setAgntcoy(wsspcomn.company);
		chdrlnbIO.setAgntnum(sv.agntsel);
		chdrlnbIO.setCampaign(sv.campaign);
		chdrlnbIO.setChdrstcda(sv.stcal);
		chdrlnbIO.setChdrstcdb(sv.stcbl);
		chdrlnbIO.setChdrstcdc(sv.stccl);
		chdrlnbIO.setChdrstcdd(sv.stcdl);
		chdrlnbIO.setChdrstcde(sv.stcel);
		if(contDtCalcFlag){
			chdrlnbIO.setConcommflg(sv.concommflg);//ILJ-40
		}
		//ILJ-62
		if(NBPRP115Permission) {
			chdrlnbIO.setOwneroccup(sv.ownerOccupation);
		}
		if(isNE(wsaaPayclt, SPACES)){
			chdrlnbIO.setPayclt(wsaaPayclt);//ILIFE-2472
		} 
		chdrlnbIO.setSchmno(sv.schmno);
		chdrlnbIO.setNlgflg(sv.nlgflg); //ILIFE-3997
		chdrlnbIO.setCustrefnum(sv.custrefnum); //ILIFE-8583
		if(onePflag) {
			chdrlnbIO.setRcopt(sv.rskComOpt);//IBPLIFE-2262
		}
		chdrlnbIO.setFunction("KEEPS");
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/* Do not do any updates if in enquiry mode.                       */
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/* Set policy issue date and first issue date to VRCM-MAX-DATE     */
		/* in HPAD and update                                              */

		hpadIO = this.hpadpfDAO.getHpadData(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		boolean isInst = false;
		if(hpadIO == null){

			hpadIO = new Hpadpf();
			hpadIO.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
			hpadIO.setChdrnum(chdrlnbIO.getChdrnum().toString());
			hpadIO.setHoissdte(varcom.vrcmMaxDate.toInt());
			hpadIO.setFirstprmrcpdate(varcom.vrcmMaxDate.toInt());
			hpadIO.setTntapdate(varcom.vrcmMaxDate.toInt());
			isInst=true;
		}

		this.setHpadIO();

		
		if(isInst){
			this.hpadpfDAO.insertRecord(hpadIO);
		}else{
			this.hpadpfDAO.updateRecord(hpadIO);
		}
		/* Update Temp receipt no and date in TTRC file.                   */
		ttrcIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		ttrcIO.setChdrnum(chdrlnbIO.getChdrnum());
		ttrcIO.setEffdate(varcom.vrcmMaxDate);
		ttrcIO.setFormat(formatsInner.ttrcrec);
		ttrcIO.setFunction(varcom.begn);
		ttrcIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ttrcIO.setFitKeysSearch("CHDRCOY" , "CHDRNUM");
		SmartFileCode.execute(appVars, ttrcIO);
		if (isNE(ttrcIO.getStatuz(), varcom.oK)
				&& isNE(ttrcIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(ttrcIO.getStatuz());
			syserrrec.params.set(ttrcIO.getParams());
			fatalError600();
		}
		if (isEQ(ttrcIO.getStatuz(), varcom.endp)
				|| isNE(ttrcIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
				|| isNE(ttrcIO.getChdrnum(), chdrlnbIO.getChdrnum())) {
			ttrcIO.setChdrcoy(chdrlnbIO.getChdrcoy());
			ttrcIO.setChdrnum(chdrlnbIO.getChdrnum());
			ttrcIO.setEffdate(wsaaToday);
			ttrcIO.setFunction(varcom.writr);
		}
		else {
			ttrcIO.setFunction(varcom.updat);
		}
		/* The date should only be updated when writing a new record       */
		/* to prvent the UPDAT function from writing a new TTRC record     */
		/* each time the proposal is modifed on a different business date. */
		/* MOVE WSAA-TODAY             TO TTRC-EFFDATE.         <LA5113>*/
		ttrcIO.setTtmprcno(sv.ttmprcno);
		ttrcIO.setTtmprcdte(sv.ttmprcdte);
		SmartFileCode.execute(appVars, ttrcIO);
		if (isNE(ttrcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ttrcIO.getParams());
			fatalError600();
		}
		inctIO.setChdrnum(chdrlnbIO.getChdrnum());
		inctIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		inctIO.setFunction(varcom.readr);
		inctIO.setFormat(formatsInner.inctrec);
		SmartFileCode.execute(appVars, inctIO);
		if (isNE(inctIO.getStatuz(), varcom.oK)
				&& isNE(inctIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(inctIO.getParams());
			fatalError600();
		}
		/* IF S5004-INDXFLG                = 'N'                   <040>*/
		if (isEQ(sv.indxflg, SPACES)
				&& isEQ(inctIO.getStatuz(), varcom.oK)) {
			inctIO.setChdrnum(chdrlnbIO.getChdrnum());
			inctIO.setChdrcoy(chdrlnbIO.getChdrcoy());
			inctIO.setFunction(varcom.readh);
			inctIO.setFormat(formatsInner.inctrec);
			SmartFileCode.execute(appVars, inctIO);
			if (isNE(inctIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(inctIO.getParams());
				fatalError600();
			}
			inctIO.setFunction(varcom.delet);
			inctIO.setFormat(formatsInner.inctrec);
			SmartFileCode.execute(appVars, inctIO);
			if (isNE(inctIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(inctIO.getParams());
				fatalError600();
			}
		}
		/* IF S5004-INDXFLG            = 'N'                       <053>*/

		if (isEQ(wsaaAnniflg, "Y")
				&& isEQ(sv.indxflg, SPACES)) {
			anrlcntIO.setParams(SPACES);
			anrlcntIO.setChdrcoy(chdrlnbIO.getChdrcoy());
			anrlcntIO.setChdrnum(chdrlnbIO.getChdrnum());
			anrlcntIO.setFunction(varcom.begnh);
			readAnniRules3400();
			if (isEQ(wsaaAiindInd, "Y")) {
				sv.aiind.set(SPACES);
				anrlcntIO.setFunction(varcom.delet);
				anrlcntIO.setFormat(formatsInner.anrlcntrec);
				SmartFileCode.execute(appVars, inctIO);
				if (isNE(anrlcntIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(anrlcntIO.getParams());
					fatalError600();
				}
			}
		}

		if (isEQ(sv.indxflg, "U")){
			sv.indxflg.set(SPACES);
		}

		/* IF S5004-INDXFLG                = 'Y'                   <040>*/
		if (isNE(sv.indxflg, SPACES)
				&& isEQ(inctIO.getStatuz(), varcom.mrnf)) {		
			inctIO.setChdrnum(chdrlnbIO.getChdrnum());
			inctIO.setChdrcoy(chdrlnbIO.getChdrcoy());
			inctIO.setIndxflg("Y");
			inctIO.setFunction(varcom.writr);
			inctIO.setFormat(formatsInner.inctrec);
			SmartFileCode.execute(appVars, inctIO);
			if (isNE(inctIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(inctIO.getParams());
				fatalError600();
			}
		}
		/*  Read table T6654 to the number of lead billing days            */
		String key;
		if(BTPRO028Permission) {
			key = sv.mop.toString().trim() + chdrlnbIO.getCnttype().toString().trim() + sv.billfreq.toString().trim();
			if(!readT6654(key)) {
				key = sv.mop.toString().trim().concat(chdrlnbIO.getCnttype().toString().trim()).concat("**");
				if(!readT6654(key)) {
					key = sv.mop.toString().trim().concat("*****");
					if(!readT6654(key)) {
						fatalError600();
					}
				}
			}
			
		}
		else {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(chdrlnbIO.getChdrcoy().toString());
		itempf.setItemtabl(tablesInner.t6654.toString());
		wsaaT6654Key.set(SPACES);
		wsaaT6654Mop.set(sv.mop);
		wsaaT6654Cnttype.set(chdrlnbIO.getCnttype());
		itempf.setItemitem(wsaaT6654Key.toString());
		itempf = itempfDAO.getItempfRecord(itempf);
		if (null != itempf) {
			t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(chdrlnbIO.getChdrcoy().toString());
			itempf.setItemtabl(tablesInner.t6654.toString());
			wsaaT6654Cnttype.set("***");
			itempf.setItemitem(wsaaT6654Key.toString());
			itempf = itempfDAO.getItempfRecord(itempf);
			if (null ==  itempf) {
				//syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			else {
				if (null != itempf) {
					t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				}
			}
		}
	}

		/* Read the PAYR file to see if a PAYR record exists               */
		/* for this contract and payer sequence no. 1 .                    */
		/* If a payr record does not exist write a new record.             */
		/* If a payr record does exist update the existing payr record.    */
		wsaaBtdateStored.set(payrIO.getBtdate());
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(sv.chdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction("READR");
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)
				&& isNE(payrIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/*   Set up payer details.                                         */
		//ILIFE-6110 Starts
		payrpf = new Payrpf();
		payrpf.setChdrcoy(wsspcomn.company.toString());
		payrpf.setChdrnum(sv.chdrnum.toString());
		payrpf.setPayrseqno(1);
		/* MOVE S5004-OCCDATE          TO PAYR-EFFDATE.            <022>*/
		/*MOVE 0                      TO PAYR-EFFDATE.            <022>*/
		payrpf.setEffdate(sv.occdate.toInt());
		payrpf.setValidflag("1");
		payrpf.setBillchnl(sv.mop.toString());
		payrpf.setBillfreq(sv.billfreq.toString());
		payrpf.setBillcd(sv.billcd.toInt());
		/* We need to find out whether the 1st premium is going to be      */
		/*  a complete one or a partial one ie. pro-rata'd                 */
		/* So, use DATCON3 to find the difference between the BILLCD       */
		/*   and OCCDATE.                                                  */
		/*   IF the decimal portion of the difference is not ZEROS,        */
		/*     then set the following new PAYR fields from the BILLCD date */
		/*    ELSE                                                         */
		/*     set the following fields from the OCCDATE date.             */
		/*    END IF.                                                      */
		/*    The fields in question are as follows...                     */
		/*      BILLDAY, BILLMONTH, DUEDD and DUEMM.                       */
		datcon3rec.datcon3Rec.set(SPACES);
		/*MOVE PAYR-BILLCD            TO DTC3-INT-DATE-1.         <042>*/
		/*MOVE S5004-OCCDATE          TO DTC3-INT-DATE-2.         <042>*/
		datcon3rec.intDate1.set(sv.occdate);
		datcon3rec.intDate2.set(payrpf.getBillcd());
		datcon3rec.frequency.set(payrpf.getBillfreq());
		datcon3rec.freqFactor.set(ZERO);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		wsaaSplitFreq.set(datcon3rec.freqFactor);
		/* It should be noted here that pro-rata-ing is not permitted on   */
		/*   Direct Debit or Group-billed contracts, so we only need to    */
		/*   do the tests IF we are not a DD or Group billed contract      */
		if (isEQ(t3620rec.ddind, SPACES)
				&& isEQ(t3620rec.grpind, SPACES) && !isbillday) {
			if (isNE(wsaaDecimal, ZERO)) {
				/* Split the BILLCD into DD,MM and YYYY and move the month and     */
				/* day to the PAYRPF file separately.                              */
				wsaaBillcd9.set(payrpf.getBillcd());
				payrpf.setBillmonth(wsaaBillcdMm.toString());
				payrpf.setBillday(wsaaBillcdDd.toString());
				payrpf.setDuemm(wsaaBillcdMm.toString());
				payrpf.setDuedd(wsaaBillcdDd.toString());
			}
			else {
				/* Split the OCCDATE to DD,MM and YYYY and move the month and      */
				/* day to the PAYRPF file separately.                              */
				wsaaDateDue.set(sv.occdate);
				payrpf.setDuemm(wsaaDateMm.toString());
				payrpf.setDuedd(wsaaDateDd.toString());
				payrpf.setBillday(wsaaDateDd.toString());
				payrpf.setBillmonth(wsaaDateMm.toString());
			}
		}
		else {
			wsaaBillcd9.set(payrpf.getBillcd());
			wsaaDateDue.set(sv.occdate);
			payrpf.setDuemm(wsaaDateMm.toString());
			payrpf.setDuedd(wsaaDateDd.toString());
			payrpf.setBillmonth(wsaaBillcdMm.toString());
			payrpf.setBillday(wsaaBillcdDd.toString());
		}
		// IBPLIFE-5785 start
		if (isbillday) {
			payrpf.setBillday(sv.billday.toString());
		}
		payrpf.setOrgbillcd(0);

		// IBPLIFE-5785 end
		/* Calculate the next billing extract date                         */
		/* NEXTDATE = BILLCD less the No of lead billing days.             */
		datcon2rec.intDate1.set(chdrlnbIO.getBillcd());
		datcon2rec.frequency.set("DY");
		compute(datcon2rec.freqFactor, 0).set(mult(t6654rec.leadDays, -1));
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		payrpf.setNextdate(datcon2rec.intDate2.toInt());
		payrpf.setBtdate(wsaaBtdateStored.toInt());
		payrpf.setPtdate(ZERO.intValue());
		payrpf.setBillcurr(sv.billcurr.toString());
		payrpf.setCntcurr(sv.cntcurr.toString());
		payrpf.setSinstamt01(BigDecimal.ZERO);
		payrpf.setSinstamt02(BigDecimal.ZERO);
		payrpf.setSinstamt03(BigDecimal.ZERO);
		payrpf.setSinstamt04(BigDecimal.ZERO);
		payrpf.setSinstamt05(BigDecimal.ZERO);
		payrpf.setSinstamt06(BigDecimal.ZERO);
		payrpf.setOutstamt(BigDecimal.ZERO);
		if (isEQ(wsspcomn.flag, "M")) {		
			payrpf.setTranno(add(payrpf.getTranno(), 1).toInt());
		}
		else {
			payrpf.setTranno(1);
		}
		/* If the salary may be used to fund pensions net of tax and if    */
		/* the contract may bill net of tax move the tax relief method on  */
		/* T5688 to PAYR-TAXRELMTH.                                        */
		if (isNE(sv.incomeSeqNo, ZERO)) {
			if (isEQ(soinpf.getPrasind(), "N")
					&& isEQ(t6697rec.empcon, "N")) {
				payrpf.setTaxrelmth(t5688rec.taxrelmth.toString());
			}
			else {				
				payrpf.setTaxrelmth(" ");
			}
		}
		
		/* MOVE T5688-TAXRELMTH        TO PAYR-TAXRELMTH           <024>*/
		/* IF T3625-DDIND              NOT = 'Y' AND               <031>*/
		payrpf.setMandref(payrIO.getMandref().toString());
		if (isEQ(t3620rec.ddind, SPACES)
				&& bankAccountMand.isTrue()
				&& !payrpf.getMandref().equals(" ")) {
			payrpf.setMandref(" ");
		}
		if (isEQ(t3620rec.crcind, SPACES)
				&& creditCardMand.isTrue()
				&& !payrpf.getMandref().equals(" ")) {
			payrpf.setMandref(" ");
		}
		payrpf.setGrupnum(payrIO.getGrupnum().toString());
		payrpf.setGrupcoy(payrIO.getGrupcoy().toString());
		if (isEQ(t3620rec.grpind, SPACES)
				&& isNE(payrpf.getGrupnum(), SPACES)) {
			payrpf.setGrupcoy(" ");
			payrpf.setGrupnum(" ");
			payrpf.setMembsel(" ");
		}
		payrpf.setBillsupr(" ");
		payrpf.setAplsupr(" ");
		payrpf.setNotssupr(" ");
		payrpf.setBillspfrom(ZERO.intValue());
		payrpf.setAplspfrom(ZERO.intValue());
		payrpf.setNotsspfrom(ZERO.intValue());
		payrpf.setBillspto(ZERO.intValue());
		payrpf.setAplspto(ZERO.intValue());
		payrpf.setNotsspto(ZERO.intValue());
		payrpf.setIncomeSeqNo(sv.incomeSeqNo.toInt());		
		payrpf.setBillnet(" ");		
		payrpf.setTermid(varcom.vrcmTermid.toString());
		payrpf.setTransactionDate(varcom.vrcmDate.toInt());
		payrpf.setTransactionTime(varcom.vrcmTime.toInt());
		payrpf.setUser(varcom.vrcmUser.toInt());
		payrpf.setPstatcode(chdrlnbIO.getPstatcode().toString());
		payrpfList.add(payrpf);
		if (isEQ(payrIO.getStatuz(), varcom.oK)) {
			payrIO.setFunction(varcom.rlse);
			payrIO.setFormat(formatsInner.payrrec);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(payrIO.getStatuz());
				syserrrec.params.set(payrIO.getParams());
				fatalError600();
			}
			payrpf.setUniqueNumber(payrIO.unique_number.toLong());
			success = payrDAO.updatePayr(payrpfList);
		}
		if (isEQ(payrIO.getStatuz(), varcom.mrnf)) {
			success = payrDAO.insertPayrpfList(payrpfList);
		}
		
		//SmartFileCode.execute(appVars, payrIO);
		/*if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}*/
		if (!success) {
			syserrrec.params.set(payrpf);
			fatalError600();
		}
		/* If this is a new contract create a client role of PAYER         */
		/* for the contract owner.                                         */
		if (isEQ(wsaaPayer, SPACES)) {
			cltrelnrec.cltrelnRec.set(SPACES);
			cltrelnrec.clrrrole.set("PY");
			cltrelnrec.forepfx.set("CH");
			cltrelnrec.forecoy.set(payrIO.getChdrcoy());
			wsaaChdrnum.set(sv.chdrnum);
			wsaaPayrseqno.set(payrIO.getPayrseqno());
			cltrelnrec.forenum.set(wsaaForenum);
			cltrelnrec.clntpfx.set("CN");
			cltrelnrec.clntcoy.set(wsspcomn.fsuco);
			cltrelnrec.clntnum.set(sv.ownersel);
			cltrelnrec.function.set("ADD  ");
			callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
			if (isNE(cltrelnrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(cltrelnrec.statuz);
				fatalError600();
			}
			/*        Update WSAA-PAYER to prevevt creating a client role      */
			/*        the second time through this section.                    */
			wsaaPayer.set(sv.ownersel);
		}
		else {
			/*       If the contract owner is changed delete the old client    */
			/*       role record and create a new one.                         */
			if (isEQ(sv.payind, "+")
					|| isEQ(wsaaPayorInd, "Y")) {
				wsaaPayorInd.set(SPACES);
			}
			else {
				if (isNE(sv.ownersel, wsaaPayer)) {
					cltrelnrec.cltrelnRec.set(SPACES);
					cltrelnrec.clrrrole.set("PY");
					cltrelnrec.forepfx.set("CH");
					cltrelnrec.forecoy.set(payrIO.getChdrcoy());
					wsaaChdrnum.set(sv.chdrnum);
					wsaaPayrseqno.set(payrIO.getPayrseqno());
					cltrelnrec.forenum.set(wsaaForenum);
					cltrelnrec.clntpfx.set("CN");
					cltrelnrec.clntcoy.set(wsspcomn.fsuco);
					cltrelnrec.clntnum.set(wsaaPayer);
					cltrelnrec.function.set("DEL  ");
					callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
					if (isNE(cltrelnrec.statuz, varcom.oK)) {
						syserrrec.statuz.set(cltrelnrec.statuz);
						fatalError600();
					}
					cltrelnrec.cltrelnRec.set(SPACES);
					cltrelnrec.clrrrole.set("PY");
					cltrelnrec.forepfx.set("CH");
					cltrelnrec.forecoy.set(payrIO.getChdrcoy());
					wsaaChdrnum.set(sv.chdrnum);
					wsaaPayrseqno.set(payrIO.getPayrseqno());
					cltrelnrec.forenum.set(wsaaForenum);
					cltrelnrec.clntpfx.set("CN");
					cltrelnrec.clntcoy.set(wsspcomn.fsuco);
					cltrelnrec.clntnum.set(sv.ownersel);
					cltrelnrec.function.set("ADD  ");
					callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
					if (isNE(cltrelnrec.statuz, varcom.oK)) {
						syserrrec.statuz.set(cltrelnrec.statuz);
						fatalError600();
					}
					wsaaPayer.set(sv.ownersel);
				}
			}
		}
	}
	
	
	protected void checkFollowup()
	{
		//ILJ
		
			wsaaT5661Lang.set(wsspcomn.language);
			wsaaT5661Fupcode.set(policyOwner.trim());
			
			readT5661();
		
			
				descpf=descDAO.getdescData("IT", "T5661", wsaaT5661Key.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
				
				clntpf = new Clntpf();
				clntpf.setClntpfx("CN");
				clntpf.setClntcoy(wsspcomn.fsuco.toString());
				clntpf.setClntnum(sv.ownersel.toString());
				clntpf = clntpfDAO.getCltsRecordByKey(clntpf); 
				
				initialize(antisoclkey.antisocialKey);
				antisoclkey.kjSName.set(clntpf.getLsurname());
				antisoclkey.kjGName.set(clntpf.getLgivname());
				antisoclkey.clntype.set(clntpf.getClttype());

				
				callProgram(Antisocl.class, antisoclkey.antisocialKey);
				
				List<Fluppf> fluppfobj = fluppfDAO.searchFlupRecordByChdrNum(wsspcomn.company.toString(), chdrlnbIO.getChdrnum().toString());
				
				if (isNE(antisoclkey.statuz, varcom.oK)) {		
					
						fupOldno.set(0);
						fupno.set(0);
						
						if(fluppfobj.size()>0)
						{
							int i=0;
					    	for(Fluppf fluppf: fluppfobj)
						    {
						    	
						    	if(isEQ(fluppf.getFupCde().trim(),policyOwner.trim()))	
						     	{
						    		i++;
						    		fluppfDAO.deleteRecordbyCode(fluppf);
									List<Fluppf> fluppfobj1 = fluppfDAO.searchFlupRecordByChdrNum(wsspcomn.company.toString(), chdrlnbIO.getChdrnum().toString());
									if(fluppfobj1.size()>=1)
									{
									  fupOldno.set(fluppfobj1.get(fluppfobj1.size()-1).getFupNo());
									  fupno.add(fupOldno);
									}
									else
									{
										fupno.add(fupOldno);
									}
						    		updateFluppf();
						    		break;
							    }
						    }
						    	
						    	if(i==0)
						    	{
						    		fupOldno.set(fluppfobj.get(fluppfobj.size()-1).getFupNo());
									fupno.add(fupOldno);
									updateFluppf();
						    	}
						    	
					    	
						}
						else
						{
							fupno.add(fupOldno);
							updateFluppf();
						}		
		      }
				
				else
				{
					if(fluppfobj.size()>0)
					{
						for(Fluppf fluppf: fluppfobj)
					    {
					    	if(isEQ(fluppf.getFupCde().trim(),policyOwner.trim()))	
					     	{
					    		fluppfDAO.deleteRecordbyCode(fluppf);
					     	}
					    }
					}
				}
	}
	
	protected void updateFluppf()
	{
		name.set(clntpf.getLsurname().trim().replace(".", "").replace("-", "").concat(clntpf.getLgivname().trim().replace(".", "").replace("-", "")));
		ansopflist = ansopfDAO.getRecord(name.toString().trim());
		
		
		if(ansopflist.size()>0)
		{
			for(Ansopf ansopf : ansopflist)
			 {
				
				fupno.add(1);
				fluppf = new Fluppf();
				
				fluppf.setChdrcoy(wsspcomn.company.charat(0));
				fluppf.setChdrnum(chdrlnbIO.getChdrnum().toString());
				fluppf.setTranNo(chdrlnbIO.getTranno().toInt());
				fluppf.setFupTyp(clntpf.getClttype().charAt(0));
				fluppf.setLife("01");
				fluppf.setjLife("");
				fluppf.setFupNo(fupno.toInt());
				fluppf.setFupCde(policyOwner.trim());
				fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
				fluppf.setFupDt(wsaaToday.toInt());
				fluppf.setFupRmk(descpf.getLongdesc().trim() + ansopf.getAnsonum() +"/"+ sv.ownersel);
				fluppf.setClamNum("");
				fluppf.setUserT(varcom.vrcmUser.toInt());
				fluppf.setTermId(varcom.vrcmTermid.toString());
				fluppf.setTrdt(varcom.vrcmDate.toInt());
				fluppf.setTrtm(varcom.vrcmTime.toInt());
				fluppf.setzAutoInd(' ');
				fluppf.setUsrPrf(varcom.vrcmUser.toString());
				fluppf.setJobNm(wsspcomn.userid.toString());
				fluppf.setEffDate(wsaaToday.toInt());
				fluppf.setCrtUser(varcom.vrcmUser.toString());
				fluppf.setCrtDate(wsaaToday.toInt());
				fluppf.setLstUpUser(varcom.vrcmUser.toString());
				fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
				fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
				fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
				fluppfDAO.insertFlupRecord(fluppf);
						 
			}
	   }
		
	}
	
	protected void readT5661()
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5661);
		itempf.setItemitem(wsaaT5661Key.toString());
		itempf = itempfDAO.getItempfRecord(itempf);

		if (itempf == null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T5661").concat(wsaaT5661Key.toString()));
			fatalError600();
		}
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));

	}

	private void setHpadIO() {
		hpadIO.setHpropdte(sv.hpropdte.toInt());
		hpadIO.setHprrcvdt(sv.hprrcvdt.toInt());
		hpadIO.setHissdte(varcom.vrcmMaxDate.toInt());
		hpadIO.setHuwdcdte(sv.huwdcdte.toInt());
		hpadIO.setZnfopt(sv.znfopt.toString());
		hpadIO.setZsufcdte(99999999);
		hpadIO.setValidflag("3");
		hpadIO.setDlvrmode(sv.dlvrmode.toString());
		hpadIO.setDespdate(varcom.vrcmMaxDate.toInt());
		hpadIO.setPackdate(varcom.vrcmMaxDate.toInt());
		hpadIO.setRemdte(varcom.vrcmMaxDate.toInt());
		hpadIO.setDeemdate(varcom.vrcmMaxDate.toInt());
		hpadIO.setNxtdte(varcom.vrcmMaxDate.toInt());
		hpadIO.setRefundOverpay(sv.refundOverpay.toString());//fwang3 ICIL-4
		if(contDtCalcFlag){
		hpadIO.setRskcommdate(sv.rskcommdate.toInt());//ILJ-40
		hpadIO.setDecldate(sv.decldate.toInt());//ILJ-40
		}
		//ILJ-62
		if(NBPRP115Permission) {
			hpadIO.setCnfirmtnIntentDate(sv.cnfirmtnIntentDate.toInt());
		}
		if(onePflag) {
			hpadIO.setFirstprmrcpdate(sv.premRcptDate.toInt());
		}
	}

	/**
	 * <pre>
	 *3300-UPDATE-CLIENT-ROLES    SECTION.                        <012>
	 *************************************                        <012>
	 *                                                            <012>
	 *3310-OLD-OWNER.                                             <012>
	 *****                                                        <012>
	 *****MOVE SPACES                 TO CLRR-DATA-KEY.           <012>
	 *****MOVE CLRRREC                TO CLRR-FORMAT.             <012>
	 *****MOVE 'CN'                   TO CLRR-CLNTPFX.            <012>
	 *****MOVE WSSP-FSUCO             TO CLRR-CLNTCOY.            <012>
	 *****MOVE WSAA-OWNERSEL-STORE    TO CLRR-CLNTNUM.            <012>
	 *****MOVE 'OW'                   TO CLRR-CLRRROLE.           <012>
	 *****MOVE 'CH'                   TO CLRR-FOREPFX.            <012>
	 *****MOVE WSSP-COMPANY           TO CLRR-FORECOY.            <012>
	 *****MOVE CHDRLNB-CHDRNUM        TO CLRR-FORENUM.            <012>
	 *****                                                        <012>
	 *****MOVE READH                  TO CLRR-FUNCTION.           <012>
	 *****                                                        <012>
	 *****CALL 'CLRRIO'     USING CLRR-PARAMS.                    <012>
	 *****IF CLRR-STATUZ              = O-K                       <012>
	 *****   NEXT SENTENCE                                        <012>
	 *****ELSE                                                    <012>
	 *****IF CLRR-STATUZ              = MRNF                      <012>
	 *****   GO TO 3390-EXIT                                      <012>
	 *****ELSE                                                    <012>
	 *****   MOVE CLRR-PARAMS         TO SYSR-PARAMS              <012>
	 *****   PERFORM 600-FATAL-ERROR.                             <012>
	 *****                                                        <012>
	 *****MOVE 'X'                    TO CLRR-USED-TO-BE.         <012>
	 *****MOVE REWRT                  TO CLRR-FUNCTION.           <012>
	 *****                                                        <012>
	 *****CALL 'CLRRIO'     USING CLRR-PARAMS.                    <012>
	 *****IF CLRR-STATUZ              NOT = O-K                   <012>
	 *****   MOVE CLRR-PARAMS         TO SYSR-PARAMS              <012>
	 *****   PERFORM 600-FATAL-ERROR.                             <012>
	 *****                                                        <012>
	 *3350-NEW-OWNER.                                             <012>
	 *                                                            <012>
	 *****MOVE SPACES                 TO CLRR-DATA-KEY.           <012>
	 *****MOVE 'CN'                   TO CLRR-CLNTPFX.            <012>
	 *****MOVE WSSP-FSUCO             TO CLRR-CLNTCOY.            <012>
	 *****MOVE S5004-OWNERSEL         TO CLRR-CLNTNUM.            <012>
	 *****MOVE 'OW'                   TO CLRR-CLRRROLE.           <012>
	 *****MOVE 'CH'                   TO CLRR-FOREPFX.            <012>
	 *****MOVE WSSP-COMPANY           TO CLRR-FORECOY.            <012>
	 *****MOVE CHDRLNB-CHDRNUM        TO CLRR-FORENUM.            <012>
	 *****MOVE SPACES                 TO CLRR-USED-TO-BE.         <012>
	 *****                                                        <012>
	 *****MOVE WRITR                  TO CLRR-FUNCTION.           <012>
	 *****                                                        <012>
	 *****CALL 'CLRRIO'     USING CLRR-PARAMS.                    <012>
	 *****IF CLRR-STATUZ              NOT = O-K                   <012>
	 *****   MOVE CLRR-PARAMS         TO SYSR-PARAMS              <012>
	 *****   PERFORM 600-FATAL-ERROR.                             <012>
	 *****                                                        <012>
	 *3390-EXIT.                                                  <012>
	 ***** EXIT.                                                  <012>
	 * The following two sections are used by both the 1000 and 4000
	 * sections. As they will be used more often in the 4000, they
	 * have been put here.
	 * </pre>
	 */
	protected void readAnniRules3400()
	{


		anrlcntIO.setFormat(formatsInner.anrlcntrec);
		SmartFileCode.execute(appVars, anrlcntIO);
		if (isNE(anrlcntIO.getStatuz(), varcom.oK)
				&& isNE(anrlcntIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(anrlcntIO.getParams());
			fatalError600();
		}
		wsaaAiindInd.set(SPACES);
		if (isEQ(anrlcntIO.getStatuz(), varcom.endp)
				|| isNE(anrlcntIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
				|| isNE(anrlcntIO.getChdrnum(), chdrlnbIO.getChdrnum())) {
			return ;
		}
		if (isEQ(anrlcntIO.getValidflag(), "1")) {
			wsaaAiindInd.set("Y");
			return ;
		}
		anrlcntIO.setFunction(varcom.nextr);
		readAnniRules3400();
	}

	protected void checkAssignee3600()

	{
		/*   Check for ASSIGNEE                                            */
		asgnenqIO.setDataKey(SPACES);
		asgnenqIO.setChdrcoy(wsspcomn.company);
		asgnenqIO.setChdrnum(chdrlnbIO.getChdrnum());
		asgnenqIO.setSeqno(ZERO);
		asgnenqIO.setFunction(varcom.begn);
		asgnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		asgnenqIO.setFitKeysSearch("CHDRCOY" , "CHDRNUM");
		SmartFileCode.execute(appVars, asgnenqIO);
		if ((isNE(asgnenqIO.getStatuz(), varcom.oK))
				&& (isNE(asgnenqIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(asgnenqIO.getParams());
			fatalError600();
		}
		if ((isNE(asgnenqIO.getChdrcoy(), chdrlnbIO.getChdrcoy()))
				|| (isNE(asgnenqIO.getChdrnum(), chdrlnbIO.getChdrnum()))
				|| (isEQ(asgnenqIO.getStatuz(), varcom.endp))) {
			sv.asgnind.set(SPACES);
		}
		else {
			sv.asgnind.set("+");
		}
	}

	protected void m200CheckReducingTerm()
	{
		/*BRD-139 by starts slakkala*/
		if(mrtaPermission){
			if (isEQ(chdrlnbIO.getCnttype(), "MRT")) {
				wsaaZsredtrm.set("M");
			}
			/*BRD-139 by ends slakkala*/
		}
		//ILIFE-6590 wli31
		mrtapf=mrtapfDAO.getMrtaRecord(chdrlnbIO.getChdrcoy().toString().trim(), chdrlnbIO.getChdrnum().toString().trim());
		if (mrtapf != null) {
			sv.zsredtrm.set("+");
			/*BRD-139 by starts slakkala*/
			if(mrtaPermission){
				wsaaZsredtrm.set("A");
			}
			/*BRD-139 by ends slakkala*/
		}
		else {
			sv.zsredtrm.set(SPACES);
		}
		/*M200-EXIT*/
		/*BRD-139 by starts slakkala*/
		if(mrtaPermission){
			if (isEQ(wsaaZsredtrm, "M")) {
				sv.zsredtrm.set("X");
			}
		}
		/*BRD-139 by ends slakkala*/
	}

//ILIFE-6590 wli31
protected void checkCommission3700(){
	List<Pcddpf> pcddpfList = pcddpfDAO.searchPcddRecordByChdrNum(wsspcomn.company.toString().trim(), chdrlnbIO.getChdrnum().toString().trim());
	if(pcddpfList.size() <= 0){
		sv.comind.set(SPACES);
	}else{
		sv.comind.set("+");
	}

}

	protected void checkBeneficiaries3800()
	{
		bnfylnbIO.setDataKey(SPACES);
		bnfylnbIO.setChdrcoy(wsspcomn.company);
		bnfylnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		bnfylnbIO.setFunction(varcom.begn);
		bnfylnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		bnfylnbIO.setFitKeysSearch("CHDRCOY" , "CHDRNUM");
		SmartFileCode.execute(appVars, bnfylnbIO);
		if ((isNE(bnfylnbIO.getStatuz(), varcom.oK))
				&& (isNE(bnfylnbIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(bnfylnbIO.getParams());
			fatalError600();
		}
		if ((isNE(bnfylnbIO.getChdrcoy(), chdrlnbIO.getChdrcoy()))
				|| (isNE(bnfylnbIO.getChdrnum(), chdrlnbIO.getChdrnum()))
				|| (isEQ(bnfylnbIO.getStatuz(), varcom.endp))) {
			sv.bnfying.set(SPACES);
		}
		else {
			sv.bnfying.set("+");
			if(isEQ(sv.zsredtrm, SPACES) && sv.bnfying.set("+") && (isEQ(chdrlnbIO.getCnttype(), "RTA") || isEQ(chdrlnbIO.getCnttype(), "SUM") ||isEQ(chdrlnbIO.getCnttype(), "LCP"))){
				sv.zsredtrm.set("+");
			}
		}
	}

protected void checkContractSuspense3900(){
	//ILIFE-6590 wli31
	Acblpf acblpf = acblDao.loadDataByBatch(wsspcomn.company.toString().trim(),t5645rec.sacscode[2].toString().trim(),
			chdrlnbIO.getChdrnum().toString().trim(),chdrlnbIO.getCntcurr().toString().trim(),t5645rec.sacstype[2].toString().trim());
	
	if(acblpf == null || isEQ(acblpf.getSacscurbal(), ZERO)){
		sv.apcind.set(SPACES);
	}else{
		sv.apcind.set("+");
		if(isEQ(sv.zsredtrm, SPACES) && sv.apcind.set("+")  && (isEQ(chdrlnbIO.getCnttype(), "RTA") || isEQ(chdrlnbIO.getCnttype(), "SUM") || isEQ(chdrlnbIO.getCnttype(), "LCP"))){
			sv.zsredtrm.set("+");
		}
		return;
	}
	acblpf = acblDao.loadDataByBatch(wsspcomn.company.toString().trim(),t5645rec.sacscode[2].toString().trim(),
			chdrlnbIO.getChdrnum().toString().trim(),chdrlnbIO.getBillcurr().toString().trim(),t5645rec.sacstype[2].toString().trim());
	if (acblpf == null || isEQ(acblpf.getSacscurbal(), ZERO)) {
		sv.apcind.set(SPACES);
	}
	else {
		sv.apcind.set("+");
	}
	
}

	/**
	 * <pre>
	 *3910-READ-SUB-ACCOUNT.
	 *****MOVE SPACES                 TO SACSLNB-DATA-KEY.
	 *****MOVE WSSP-COMPANY           TO SACSLNB-CHDRCOY.
	 *****MOVE CHDRLNB-CHDRNUM        TO SACSLNB-CHDRNUM.
	 *****MOVE CHDRLNB-CNTCURR        TO SACSLNB-CNTCURR.
	 *****MOVE T5645-SACSCODE(02)     TO SACSLNB-SACSCODE.
	 *****MOVE T5645-SACSTYPE(02)     TO SACSLNB-SACSTYP.
	 *****MOVE READR                  TO SACSLNB-FUNCTION.
	 *****CALL 'SACSLNBIO'  USING SACSLNB-PARAMS.
	 *****IF (SACSLNB-STATUZ          NOT = O-K) AND
	 *****   (SACSLNB-STATUZ          NOT = MRNF)
	 *****    MOVE SACSLNB-PARAMS        TO SYSR-PARAMS
	 *****    PERFORM 600-FATAL-ERROR.
	 *****IF SACSLNB-STATUZ = MRNF
	 ***** OR SACSLNB-SACSCURBAL = ZERO
	 *****   MOVE SPACE               TO S5004-APCIND
	 *****ELSE
	 *****    MOVE '+'                TO S5004-APCIND.
	 * </pre>
	 */
	protected void readClrf3950() {
		clrfpf = new Clrfpf();
		clrfpf.setForepfx("CH");
		clrfpf.setForecoy(payrIO.getChdrcoy().toString().trim());
		wsaaChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		clrfpf.setForenum(wsaaForenum.toString());
		clrfpf.setClrrrole("PY");
		clrfpfList = clrfpfDAO.readClrfpfData(clrfpf);
		if (clrfpfList == null
				|| (clrfpfList != null && clrfpfList.size() == 0)) {
			fatalError600();
		} else {
			clrfpf = clrfpfList.get(0);
		}
	}

	protected void a3100CheckTrustee()
	{
		ctrsIO.setDataKey(SPACES);
		ctrsIO.setChdrcoy(wsspcomn.company);
		ctrsIO.setChdrnum(chdrlnbIO.getChdrnum());
		ctrsIO.setSeqno(ZERO);
		ctrsIO.setFunction(varcom.begn);
		ctrsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ctrsIO.setFitKeysSearch("CHDRCOY" , "CHDRNUM");
		SmartFileCode.execute(appVars, ctrsIO);
		if ((isNE(ctrsIO.getStatuz(), varcom.oK))
				&& (isNE(ctrsIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(ctrsIO.getParams());
			fatalError600();
		}
		if ((isNE(ctrsIO.getChdrcoy(), chdrlnbIO.getChdrcoy()))
				|| (isNE(ctrsIO.getChdrnum(), chdrlnbIO.getChdrnum()))
				|| (isEQ(ctrsIO.getStatuz(), varcom.endp))) {
			sv.ctrsind.set(SPACES);
		}
		else {
			sv.ctrsind.set("+");
		}
	}

	/**
	 * <pre>
	 *                                                         <V71L13>
	 * </pre>
	 */
	protected void a3200CheckPolicyNotes()
	{
		pnteIO.setDataKey(SPACES);
		pnteIO.setStatuz(varcom.oK);
		pnteIO.setChdrcoy(wsspcomn.chdrChdrcoy);
		pnteIO.setChdrnum(wsspcomn.chdrChdrnum);
		pnteIO.setTrandate(varcom.vrcmMaxDate);
		pnteIO.setTrantime(varcom.maxtime);
		pnteIO.setSeqno(ZERO);
		pnteIO.setFunction(varcom.begn);
		pnteIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pnteIO.setFitKeysSearch("CHDRCOY" , "CHDRNUM");
		pnteIO.setFormat(formatsInner.pnterec);
		SmartFileCode.execute(appVars, pnteIO);
		if (isNE(pnteIO.getStatuz(), varcom.oK)
				&& isNE(pnteIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(pnteIO.getStatuz());
			syserrrec.params.set(pnteIO.getParams());
			fatalError600();
		}
		if (isEQ(pnteIO.getStatuz(), varcom.oK)
				&& isEQ(pnteIO.getChdrcoy(), wsspcomn.chdrChdrcoy)
				&& isEQ(pnteIO.getChdrnum(), wsspcomn.chdrChdrnum)) {
			sv.ind.set("+");
		}
		else {
			sv.ind.set(SPACES);
		}
	}

	/**
	 * <pre>
	 *     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	 * </pre>
	 */
	protected void whereNext4000()
	{

		nextProgram4010();
	}

	protected void nextProgram4010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			chdrlnbIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlnbIO.getParams());
				fatalError600();
			}
		}
		wsaaPayclt.set(chdrlnbIO.getPayclt()); //ILIFE-2472
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		wsaaKill.set("Y");
		wsspcomn.nextprog.set(wsaaProg);
		/*  If first time into this section (stack action blank)*/
		/*  save next eight programs in stack*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(1);
			for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
				saveProgram4100();
			}
		}
		/*  Check if beneficiary selected previously*/
		if (isEQ(sv.bnfying, "?")) {
			checkBeneficiaries3800();
		}
		/*  Check if commission selected previously*/
		if (isEQ(sv.comind, "?")) {
			checkCommission3700();
		}
		/*  Check if assignee selected previously*/
		if (isEQ(sv.asgnind, "?")) {
			checkAssignee3600();
		}
		/*    IF CHDRLNB-ASGNNUM = SPACES OR                            */
		/*       CHDRLNB-ASGNNUM  = CHDRLNB-COWNNUM                     */
		/*       MOVE SPACES           TO S5004-ASGNIND                 */
		/*    ELSE                                                      */
		/*       MOVE '+'              TO S5004-ASGNIND.                */
		/*  Check if apply cash selected previously*/
		if (isEQ(sv.apcind, "?")) {
			checkContractSuspense3900();
		}
		/*  Check if trustee selected previously                           */
		if (isEQ(sv.ctrsind, "?")) {
			a3100CheckTrustee();
		}
		/*  Check if policy notes selected previously                      */
		if (isEQ(sv.ind, "?")) {
			a3200CheckPolicyNotes();
		}
		/*  Check if joint owner selected previously*/
		if (isEQ(sv.jownind, "?")) {
			if (isEQ(chdrlnbIO.getJownnum(), SPACES)
					|| isEQ(chdrlnbIO.getJownnum(), chdrlnbIO.getCownnum())) {
				sv.jownind.set(SPACES);
			}
			else {
				sv.jownind.set("+");
			}
		}
		/*  Check if dispatch address selected previously*/
		if (isEQ(sv.addtype, "?")) {
			if (isEQ(chdrlnbIO.getDespnum(), SPACES)
					|| isEQ(chdrlnbIO.getDespnum(), chdrlnbIO.getCownnum())) {
				sv.addtype.set(SPACES);
			}
			else {
				sv.addtype.set("+");
			}
		}
		/*  Check if payor selected previously*/
		/*  Read the client role file to get the payer number.             */
		if (isEQ(sv.payind, "?")) {
			readClrf3950();
			/*IF CHDRLNB-PAYRNUM = SPACES OR                            */
			/*  CHDRLNB-PAYRNUM = CHDRLNB-COWNNUM                      */
			if (isEQ(clrfpf.getClntnum(), chdrlnbIO.getCownnum())) {
				sv.payind.set(SPACES);
			}
			else {
				/*  MOVE '+'              TO S5004-PAYIND.                 */
				wsaaPayorInd.set("Y");
				sv.payind.set("+");
			}
		}
		/*  Check if Anniversary rules selected previously.                */
		/*  Read the Anniversary rules file to establish existence.        */
		if (isEQ(sv.aiind, "?")) {
			anrlcntIO.setParams(SPACES);
			anrlcntIO.setChdrcoy(chdrlnbIO.getChdrcoy());
			anrlcntIO.setChdrnum(chdrlnbIO.getChdrnum());
			anrlcntIO.setFunction(varcom.begnh);
			readAnniRules3400();
			if (isEQ(wsaaAiindInd, "Y")) {
				sv.aiind.set("+");
			}
			else {
				sv.aiind.set(SPACES);
			}
		}
		/*  Check if bank details selected previously*/
		if (isEQ(sv.ddind, "?")) {
			readClrf3950();
			/*IF CHDRLNB-BANKKEY = SPACES                               */
			if (isEQ(chdrlnbIO.getMandref(), SPACES)) {
				sv.ddind.set(SPACES);
			}
			else {
				if (isEQ(sv.mop, "D")) {
					/*IF CHDRLNB-PAYRCOY     = SPACES AND               <009>*/
					/* CHDRLNB-PAYRNUM     = SPACES                   <009>*/
					/* MOVE CHDRLNB-COWNCOY  TO MANDLNB-PAYRCOY       <009>*/
					/* MOVE CHDRLNB-COWNNUM  TO MANDLNB-PAYRNUM       <009>*/
					/* ELSE                                              <009>*/
					/*MOVE CHDRLNB-PAYRCOY  TO MANDLNB-PAYRCOY       <009>*/
					/*MOVE CHDRLNB-PAYRNUM  TO MANDLNB-PAYRNUM       <009>*/
					/* END-IF                                            <009>*/
					/*  MOVE CHDRLNB-MANDREF  TO MANDLNB-MANDREF          <009>*/

					
					mandlnb = mandpfDAO.searchMandpfRecord(wsspcomn.fsuco.toString().trim(), clrfpf.getClntnum().trim(), //IJTI-1410
							chdrlnbIO.getMandref().toString().trim(),"Mandlnb");
					if (mandlnb != null&& isEQ(mandlnb.getCrcind(), SPACES)) {
						sv.ddind.set("+");
						bankAccountMand.setTrue();
					}
					else {
						sv.ddind.set("X");
						sv.payind.set("X");
					}
				}
				else {
					sv.ddind.set(SPACES);
				}
			}
		}
		/*  Check if Credit Card details selected previously.              */
		if (isEQ(sv.crcind, "?")) {
			readClrf3950();
			if (isEQ(chdrlnbIO.getMandref(), SPACES)) {
				sv.crcind.set(SPACES);
			}
			else {
				if (isEQ(sv.mop, "R")) {
					String payrnum = isEQ(clrfpf.getClntnum(), SPACES)?chdrlnbIO.getCownnum().toString().trim():clrfpf.getClntnum().trim();//IJTI-1410

					mandccd = mandpfDAO.searchMandpfRecord(wsspcomn.fsuco.toString().trim(), payrnum, chdrlnbIO.getMandref().toString().trim(),"Mandccd");
					// isEQ is changed to isNE as crcind would have value when credit card is added
					if (mandccd != null && isNE(mandccd.getCrcind(), SPACES)) {
						sv.crcind.set("+");
						creditCardMand.setTrue();
					}
					else {
						sv.crcind.set("X");
						sv.payind.set("X");
					}

				}
				else {
					sv.crcind.set(SPACES);
				}
			}
		}
		/*  Check if group details selected previously*/
		if (isEQ(sv.grpind, "?")) {
			if (isEQ(chdrlnbIO.getGrupkey(), SPACES)) {
				sv.grpind.set(SPACES);
			}
			else {
				sv.grpind.set("+");
			}
		}
		nxtProg4010CustomerSpecific();
		
		/*  check if Transaction History selected previously               */
		if (isEQ(sv.transhist, "?")) {
			sv.transhist.set("+");
		}
		/*  Check if reducing term details selected previously             */
		if (isEQ(sv.zsredtrm, "?")) {
			m200CheckReducingTerm();
		}
		if(superprod && isEQ(sv.zctaxind, "?")){
			m200checkzctx();
		}
		if(multOwnerFlag  && isEQ(sv.zmultOwner,"?"))
		{
			checkMultOwner();/*ALS-4555*/
		}

		if(ctaxPermission && isEQ(sv.zroloverind, "?")){
			checkRollover();
		}
		/**
		 * ILIFE-2472 -START
		 */
		/* Check if direct bank credit details selected previously */
		if (isEQ(sv.zdpind, "?")) {
			readClrf3950();
			if (isEQ(chdrlnbIO.getZmandref(), SPACES)) {
				sv.zdpind.set(SPACES);
			} else {
				if (isEQ(sv.reqntype, "4")) {
					//ILIFE-6590 wli31
					mandlnb = mandpfDAO.searchMandpfRecord(wsspcomn.fsuco.toString().trim(), chdrlnbIO.getPayclt().toString().trim(),
							chdrlnbIO.getZmandref().toString().trim(),"Mandlnb");
					if (mandlnb != null && isEQ(mandlnb.getCrcind(), SPACES)) {
						sv.zdpind.set("+");
					} else {
						sv.zdpind.set("X");
					}

				} else {
					sv.zdpind.set(SPACES);
				}
			}
		}
		/* Check if Credit Card details selected previously. */
		if (isEQ(sv.zdcind, "?")) {
			readClrf3950();
			if (isEQ(chdrlnbIO.getZmandref(), SPACES)) {
				sv.zdcind.set(SPACES);
			} else {
				if (isEQ(sv.reqntype, "C")) {
					mandccd = mandpfDAO.searchMandpfRecord(wsspcomn.fsuco.toString().trim(), chdrlnbIO.getPayclt().toString().trim(), 
							chdrlnbIO.getZmandref().toString().trim(),"Mandccd");
					if (mandccd != null && isNE(mandccd.getCrcind(), SPACES)) {
						sv.zdcind.set("+");
					} else {
						sv.zdcind.set("X");
					}
				} else {
					sv.zdcind.set(SPACES);
				}
			}
			/**
			 * ILIFE-2472 -END
			 */
		}
		/*   Check if follow-ups selected previously*/
		if (isNE(sv.fupflg, "?")) {
			//goTo(GotoLabel.checkSelections4050);
			checkSelections4050();
			return;

		}
		//ILFIE-6590
		fluplnbList = fluppfDAO.getFluplnbRecordForPerformance(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
		if (fluplnbList == null || fluplnbList.size() <= 0) {
			sv.fupflg.set(SPACES);
		}else {
			sv.fupflg.set("+");
			if(isEQ(sv.zsredtrm, SPACES) && sv.fupflg.set("+") && isEQ(chdrlnbIO.getCnttype(), "RTA")){
				sv.zsredtrm.set("+");
			}
		}

		checkSelections4050();
	}
	
	protected void nxtProg4010CustomerSpecific(){
		
	}

	protected void checkSelections4050()
	{
		/*  check if joint owner is selected*/
		if (isEQ(sv.jownind, "X")) {
			gensswrec.function.set("I");
			sv.jownind.set("?");
			callGenssw4300();
			return ;
		}
		if (isEQ(sv.addtype, "X")) {
			gensswrec.function.set("B");
			sv.addtype.set("?");
			callGenssw4300();
			return ;
		}
		if (isEQ(sv.asgnind, "X")) {
			gensswrec.function.set("C");
			sv.asgnind.set("?");
			callGenssw4300();
			return ;
		}
		if (isEQ(sv.payind, "X")) {
			gensswrec.function.set("D");
			sv.payind.set("?");
			keepsPayr4500();
			callGenssw4300();
			/*IF CHDRLNB-BANKKEY = SPACES                          <009>*/
			if (isEQ(chdrlnbIO.getMandref(), SPACES)) {
				return ;
			}
			else {
				if (isEQ(sv.crcind, "+")) {
					sv.crcind.set("X");
					return ;
				}
				sv.ddind.set("X");
				return ;
			}
		}
		if (isEQ(sv.comind, "X")) {
			gensswrec.function.set("H");
			sv.comind.set("?");
			callGenssw4300();
			return ;
		}
		if (isEQ(sv.ddind, "X")) {
			gensswrec.function.set("E");
			sv.ddind.set("?");
			keepsPayr4500();
			callGenssw4300();
			return ;
		}
		if(chkslctn4050CustomerSpecific()){
			return ;
		}
		/* Set up switching for Credit Card Mandates.                      */
		if (isEQ(sv.crcind, "X") && isNE(sv.mop,"S")) {
			gensswrec.function.set("P");
			sv.crcind.set("?");
			keepsPayr4500();
			callGenssw4300();
			return ;
		}

		//ILIFE-2472 Starts
		if (isEQ(sv.zdcind, "X")) {
			gensswrec.function.set("R");
			sv.zdcind.set("?");
			keepsPayr4500();
			callGenssw4300();
			return ;
		}
		if (isEQ(sv.zdpind, "X")) {
			gensswrec.function.set("Q");
			sv.zdpind.set("?");
			keepsPayr4500();
			callGenssw4300();
			return ;
		}

		if (isEQ(sv.crcind, "X") && isEQ(sv.mop,"S")) {
			gensswrec.function.set("T");
			sv.crcind.set("?");
			keepsPayr4500();
			callGenssw4300();
			return ;
		}
		//ILIFE-2472 Ends
		if (isEQ(sv.grpind, "X")) {
			gensswrec.function.set("J");
			sv.grpind.set("?");
			keepsPayr4500();
			callGenssw4300();
			return ;
		}
		if (isEQ(sv.fupflg, "X")) {
			gensswrec.function.set("F");
			sv.fupflg.set("?");
			callGenssw4300();
			return ;
		}
		if (isEQ(sv.apcind, "X")) {
			gensswrec.function.set("G");
			sv.apcind.set("?");
			callGenssw4300();
			return ;
		}
		if (isEQ(sv.aiind, "X")) {
			wsaaIndxflgCmp.set(sv.indxflg);
			wsspcomn.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
			wsspcomn.chdrChdrnum.set(chdrlnbIO.getChdrnum());
			wsspcomn.chdrCnttype.set(chdrlnbIO.getCnttype());
			wsspcomn.chdrValidflag.set(chdrlnbIO.getValidflag());
			wsspcomn.currfrom.set(chdrlnbIO.getCurrfrom());
			gensswrec.function.set("K");
			sv.aiind.set("?");
			callGenssw4300();
			return ;
		}
		/*  check if beneficiary is selected*/
		if (isEQ(sv.bnfying, "X")) {
			gensswrec.function.set("A");
			sv.bnfying.set("?");
			callGenssw4300();
			return ;
		}
		/*  Check if Transaction History is selected.                      */
		/*  Then KEEPS the rec into CHDRENQ to access to S6233             */
		if (isEQ(sv.transhist, "X")) {
			chdrenqIO.setChdrcoy(wsspcomn.company);
			chdrenqIO.setChdrnum(sv.chdrnum);
			chdrenqIO.setCntcurr(sv.cntcurr);
			chdrenqIO.setRegister(chdrlnbIO.getRegister());
			chdrenqIO.setCnttype(chdrlnbIO.getCnttype());
			chdrenqIO.setStatcode(chdrlnbIO.getStatcode());
			chdrenqIO.setPstatcode(chdrlnbIO.getPstatcode());
			chdrenqIO.setValidflag(chdrlnbIO.getValidflag());
			chdrenqIO.setFunction(varcom.keeps);
			chdrenqIO.setFormat(formatsInner.chdrenqrec);
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrenqIO.getParams());
				fatalError600();
			}
			gensswrec.function.set("M");
			sv.transhist.set("?");
			callGenssw4300();
			return ;
		}
		/* Check if trustee selected                                       */
		if (isEQ(sv.ctrsind, "X")) {
			gensswrec.function.set("N");
			sv.ctrsind.set("?");
			callGenssw4300();
			return ;
		}
		/*  check if policy notes is selected                              */
		if (isEQ(sv.ind, "X")) {
			gensswrec.function.set("O");
			sv.ind.set("?");
			callGenssw4300();
			return ;
		}
		/*  check if reducing term details                                 */
		if (isEQ(sv.zsredtrm, "X")) {
			gensswrec.function.set("L");
			sv.zsredtrm.set("?");
			callGenssw4300();
			return ;
		}
		if (superprod && isEQ(sv.zctaxind, "X")) {
			gensswrec.function.set("S");
			sv.zctaxind.set("?");
			wsspcomn.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
			wsspcomn.chdrChdrnum.set(chdrlnbIO.getChdrnum());
			wsspcomn.chdrCnttype.set(chdrlnbIO.getCnttype());
			wsspcomn.chdrValidflag.set(chdrlnbIO.getValidflag());
			wsspcomn.currfrom.set(chdrlnbIO.getCurrfrom());	
			wsspcomn.chdrCownnum.set(chdrlnbIO.getCownnum());
			wsspcomn.occdate.set(chdrlnbIO.getOccdate());
			callGenssw4300();
			return ;
		}
		if (ctaxPermission && isEQ(sv.zroloverind, "X")) {
			gensswrec.function.set("T");
			wsspcomn.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
			wsspcomn.chdrChdrnum.set(chdrlnbIO.getChdrnum());
			sv.zroloverind.set("?");
			callGenssw4300();
			return ;
		}
		if(multOwnerFlag  && isEQ(sv.zmultOwner,"X"))
		{
			gensswrec.function.set("U");
			wsspcomn.chdrChdrnum.set(sv.chdrnum);
			wsspcomn.chdrCownnum.set(chdrlnbIO.getCownnum());
			wsspcomn.chdrOwnername.set(sv.ownername);
			sv.zmultOwner.set("?");
			callGenssw4300();
			return ;
		}
		/*   No more selected (or none)*/
		/*      - restore stack form wsaa to wssp*/
		wsaaX.set(wsspcomn.programPtr);
		wsaaY.set(1);
		for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
			restoreProgram4200();
		}
		/*  If current stack action is * then re-display screen*/
		/*     (in this case, some other option(s) were requested)*/
		/*  Otherwise continue as normal.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

	protected void saveProgram4100()
	{
		/*SAVE*/
		wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

	protected void restoreProgram4200()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

	protected void callGenssw4300()
	{
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
				&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*MOVE V045                TO SCRN-ERROR-CODE          <026>*/
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			return ;
		}
		/*    load from gensw to wssp*/
		compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
		wsaaY.set(1);
		for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
			loadProgram4400();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

	protected void loadProgram4400()
	{
		/*RESTORE1*/
		wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT1*/
	}

	protected void keepsPayr4500()
	{
		payrIO.setChdrnum(sv.chdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.readr);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(sv.chdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFunction(varcom.keeps);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
	}

	/**
	 * MIBT-58
	 */
	protected void  checkbackdate4520()
	{
		getEffectiveFrom2100();
		
		afiFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(),AFI_FEATURE_ID, appVars, "IT"); //ILIFE-7743
		if(!afiFlag) { //ILIFE-7743
			if ((isLT(sv.occdate, datefrm)) && sv.occdateErr.equals(SPACES)) {
				sv.occdateErr.set(errorsInner.E016);
				wsspcomn.edterror.set("Y");
			}

		if ((isLT(sv.hpropdte,datefrm)) && sv.hpropdteErr.equals(SPACES))
		{
			sv.hpropdteErr.set(errorsInner.E016);
			wsspcomn.edterror.set("Y");
		}

		}
	}

	/**
	 * MIBT-58
	 */
	protected void getEffectiveFrom2100()
	{
		start2100();
	}


	/**
	 * MIBT-58
	 */
	protected void start2100()
	{
		wsaaFound = "N";
		wsaaCompany.set(wsspcomn.company);
		wsaaOdate.set(datcon1rec.intDate);
		year.set(wsaaOdateYyyy);
		setYearCustomerSpecific();
		mnth1.set(1);
		mnth2.set(12);
		loop2100();
	}

	/**
	 * MIBT-58
	 */
	protected void loop2100() {
		Map<String, String> t1698ListMap = itempfDAO.getAllItem("IT", wsspcomn.company.toString(), "T1698");
		String keyItemitem = wsaaCompany.toString().trim();
		if (t1698ListMap.isEmpty()) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T1698").concat(wsaaProg.toString()));
			fatalError600();
		}
		while (keyItemitem != null) {
			
			if (!t1698ListMap.containsKey(keyItemitem)) {
				syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T1698").concat(keyItemitem).concat(wsaaProg.toString()));
				fatalError600();
			}
			
			T1698rec.t1698Rec.set(t1698ListMap.get(keyItemitem.trim()));
			keyItemitem = null;
			
			for (wsaaIndex.set(1); !(isGT(wsaaIndex, 13) || isEQ(wsaaFound, "Y")); wsaaIndex.add(1)) {
				if (isEQ(T1698rec.acctyr[wsaaIndex.toInt()], year)
						&& isEQ(T1698rec.acctmnth[wsaaIndex.toInt()], mnth1)) {
					wsaaFound = "Y";

					datefrm.set(T1698rec.frmdate[wsaaIndex.toInt()]);
				}
			}

			for (wsaaIndex.set(1); !(isGT(wsaaIndex, 13) || isEQ(wsaaFound1, "Y")); wsaaIndex.add(1)) {

				if (isEQ(T1698rec.acctyr[wsaaIndex.toInt()], year)
						&& isEQ(T1698rec.acctmnth[wsaaIndex.toInt()], mnth2)) {
					wsaaFound1 = "Y";
					// wsaaIndex.add(-1);
					dateto.set(T1698rec.todate[wsaaIndex.toInt()]);
				}
			}

			if ((isNE(wsaaFound, "Y") || isNE(wsaaFound1, "Y")) && isNE(T1698rec.contitem, SPACES)) {
				keyItemitem = T1698rec.contitem.toString().trim();
				continue;
		}
			break;
		}
	}

	protected void readFreqs5000()
	{
		/*PARA*/
		/*EXIT*/
	}
	protected boolean readTR59X(){
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("TR59X");
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf.setValidflag("1");
		itempf = itempfDAO.getItempfRecord(itempf);
		if(itempf==null){
			return true;
		}else{
			return false;
		}
	}
	//ILIFE-7913
	protected void setBillDay(){

		if (!billDay.equals(sv.billday.toString())&&  isNE(sv.billday,SPACE)) {
			if (isNE(t5689rec.rendd[wsaaX.toInt()], wsaaAll)
					&& ((count == 0) && ((isGT(t5689rec.rendd[wsaaX.toInt()],
							sv.billday)|| isEQ(t5689rec.rendd[wsaaX.toInt()],
									sv.billday)) || isNE(
							t5689rec.mop[wsaaX.toInt() + 1], sv.mop)))) {
				wsaaBillcdDd.set(t5689rec.rendd[wsaaX.toInt()]);
				sv.billday.set(wsaaBillcdDd);
				count++;
				wsaaBillcdOk.set("Y");
				billDay = wsaaBillcdDd.toString();
			} else if (isNE(t5689rec.rendd[wsaaX.toInt()], wsaaAll)
					&& ((count == 0) && ((isLT(billDayList.get(billDayList.size()-1),
							sv.billday) || isNE(
							t5689rec.mop[wsaaX.toInt() + 1], sv.mop))))) {
						wsaaBillcdDd.set(billDayList.get(0));
						sv.billday.set(wsaaBillcdDd);
						count++;
						wsaaBillcdOk.set("Y");
						billDay = wsaaBillcdDd.toString();
				
				
				
			}
    	} else if (isEQ(t5689rec.rendd[wsaaX.toInt()], wsaaAll)) {

			if ((isEQ(t5689rec.rendd[wsaaX.toInt()], wsaaAll) || isEQ(
					t5689rec.rendd[wsaaX.toInt()], wsaaRendd))
					|| (isEQ(t5689rec.rendd[wsaaX.toInt()], "31") && isEQ(
							wsaaEndOfMonth, "Y"))
					|| (isEQ(t5689rec.rendd[wsaaX.toInt()], "30") || isEQ(
							t5689rec.rendd[wsaaX.toInt()], "29")
							&& isEQ(wsaaMth, 2) && isEQ(wsaaEndOfMonth, "Y"))) {
				wsaaBillcdOk.set("Y");

			}
		}if (billDay.equals(sv.billday.toString()) || isEQ(t5689rec.rendd[wsaaX.toInt()], wsaaAll)){
			wsaaBillcdOk.set("Y");
		}
		if(isEQ(sv.billday,SPACE)){
			wsaaBillcdOk.set("N");
		}
	}
	/*
	 * Class transformed  from Data Structure ERRORS--INNER
	 */
	private static final class ErrorsInner {
		/* ERRORS */
		private FixedLengthStringData curs = new FixedLengthStringData(4).init("CURS");
		private FixedLengthStringData f955 = new FixedLengthStringData(4).init("F955");
		private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
		private FixedLengthStringData e368 = new FixedLengthStringData(4).init("E368");
		private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
		private FixedLengthStringData e305 = new FixedLengthStringData(4).init("E305");
		private FixedLengthStringData e455 = new FixedLengthStringData(4).init("E455");
		private FixedLengthStringData e475 = new FixedLengthStringData(4).init("E475");
		private FixedLengthStringData e481 = new FixedLengthStringData(4).init("E481");
		private FixedLengthStringData e484 = new FixedLengthStringData(4).init("E484");
		private FixedLengthStringData e487 = new FixedLengthStringData(4).init("E487");
		private FixedLengthStringData e492 = new FixedLengthStringData(4).init("E492");
		private FixedLengthStringData e525 = new FixedLengthStringData(4).init("E525");
		private FixedLengthStringData f290 = new FixedLengthStringData(4).init("F290");
		private FixedLengthStringData f849 = new FixedLengthStringData(4).init("F849");
		private FixedLengthStringData f782 = new FixedLengthStringData(4).init("F782");
		private FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
		private FixedLengthStringData g709 = new FixedLengthStringData(4).init("G709");
		private FixedLengthStringData h041 = new FixedLengthStringData(4).init("H041");
		private FixedLengthStringData h078 = new FixedLengthStringData(4).init("H078");
		private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
		private FixedLengthStringData e840 = new FixedLengthStringData(4).init("E840");
		private FixedLengthStringData sc72 = new FixedLengthStringData(4).init("SC72");
		private FixedLengthStringData i068 = new FixedLengthStringData(4).init("I068");
		private FixedLengthStringData f059 = new FixedLengthStringData(4).init("F059");
		private FixedLengthStringData f665 = new FixedLengthStringData(4).init("F665");
		private FixedLengthStringData h501 = new FixedLengthStringData(4).init("H501");
		private FixedLengthStringData e679 = new FixedLengthStringData(4).init("E679");
		private FixedLengthStringData e398 = new FixedLengthStringData(4).init("E398");
		private FixedLengthStringData f921 = new FixedLengthStringData(4).init("F921");
		private FixedLengthStringData t089 = new FixedLengthStringData(4).init("T089");
		private FixedLengthStringData e032 = new FixedLengthStringData(4).init("E032");
		private FixedLengthStringData hl00 = new FixedLengthStringData(4).init("HL00");
		private FixedLengthStringData hl01 = new FixedLengthStringData(4).init("HL01");
		private FixedLengthStringData hl02 = new FixedLengthStringData(4).init("HL02");
		private FixedLengthStringData hl03 = new FixedLengthStringData(4).init("HL03");
		private FixedLengthStringData r081 = new FixedLengthStringData(4).init("R081");
		private FixedLengthStringData f738 = new FixedLengthStringData(4).init("F738");
		private FixedLengthStringData rla8 = new FixedLengthStringData(4).init("RLA8");
		//MIBT-58
		private FixedLengthStringData E016 = new FixedLengthStringData(4).init("E016");
		private FixedLengthStringData rfx9 = new FixedLengthStringData(4).init("RFX9");
		private FixedLengthStringData rfxb = new FixedLengthStringData(4).init("RFXB");
		private FixedLengthStringData rfy0 = new FixedLengthStringData(4).init("RFY0");

		private FixedLengthStringData rpj2 = new FixedLengthStringData(4).init("RPJ2");
		private FixedLengthStringData rrmr = new FixedLengthStringData(4).init("RRMR");//ALS-6426
		
		private FixedLengthStringData jl23 = new FixedLengthStringData(4).init("JL23"); //ILJ-40
		private FixedLengthStringData jl22 = new FixedLengthStringData(4).init("JL22"); //ILJ-40
		private FixedLengthStringData g914 = new FixedLengthStringData(4).init("G914"); //ILJ-58
		private FixedLengthStringData ruky = new FixedLengthStringData(4).init("RUKY");
		private FixedLengthStringData rukz = new FixedLengthStringData(4).init("RUKZ");
		private FixedLengthStringData rupu = new FixedLengthStringData(4).init("RUPU");// IBPLIFE-5785
		
		
	}
	/*
	 * Class transformed  from Data Structure TABLES--INNER
	 */
	private static final class TablesInner {
		/* TABLES */
		private FixedLengthStringData t1692 = new FixedLengthStringData(5).init("T1692");
		private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
		private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");//ILIFE-6467
		private FixedLengthStringData t5620 = new FixedLengthStringData(5).init("T5620");
		private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
		private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
		private FixedLengthStringData t5689 = new FixedLengthStringData(5).init("T5689");
		private FixedLengthStringData t6660 = new FixedLengthStringData(5).init("T6660");
		private FixedLengthStringData t6654 = new FixedLengthStringData(5).init("T6654");
		private FixedLengthStringData t6697 = new FixedLengthStringData(5).init("T6697");
		private FixedLengthStringData t5654 = new FixedLengthStringData(5).init("T5654");
		private FixedLengthStringData t3620 = new FixedLengthStringData(5).init("T3620");
		private FixedLengthStringData t5604 = new FixedLengthStringData(5).init("T5604");
		private FixedLengthStringData th506 = new FixedLengthStringData(5).init("TH506");
		private FixedLengthStringData th605 = new FixedLengthStringData(5).init("TH605");
		private FixedLengthStringData tr52q = new FixedLengthStringData(5).init("TR52Q");
		//MIBT-58
		private FixedLengthStringData T1698 = new FixedLengthStringData(5).init("T1698");
		private FixedLengthStringData tr59x = new FixedLengthStringData(5).init("TR59X");
		private FixedLengthStringData tr5b2 = new FixedLengthStringData(5).init("TR5B2");//ILIFE-3997
		private FixedLengthStringData tjl05 = new FixedLengthStringData(5).init("TJL05");//ILJ-40
		private FixedLengthStringData t3615 = new FixedLengthStringData(5).init("T3615");
	}
	/*
	 * Class transformed  from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner {
		private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
		private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
		private FixedLengthStringData mandlnbrec = new FixedLengthStringData(10).init("MANDLNBREC");
		private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
		private FixedLengthStringData clrfrec = new FixedLengthStringData(10).init("CLRFREC");
		private FixedLengthStringData inctrec = new FixedLengthStringData(10).init("INCTREC");
		private FixedLengthStringData anrlcntrec = new FixedLengthStringData(10).init("ANRLCNTREC");
		private FixedLengthStringData hpadrec = new FixedLengthStringData(10).init("HPADREC");
		private FixedLengthStringData mrtarec = new FixedLengthStringData(10).init("MRTAREC");
		private FixedLengthStringData ttrcrec = new FixedLengthStringData(10).init("TTRCREC");
		private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
		private FixedLengthStringData pnterec = new FixedLengthStringData(10).init("PNTEREC");
		private FixedLengthStringData mandccdrec = new FixedLengthStringData(10).init("MANDCCDREC");//MIBT-239 and MIBT-173

	}

	/*
	 * Class transformed  from Data Structure WSAA-T6658-ARRAY--INNER
	 */
	private static final class WsaaTr5awArrayInner {

		private FixedLengthStringData wsaaTr5awArray = new FixedLengthStringData(36);
		private FixedLengthStringData[] wsaaTr5awRec = FLSArrayPartOfStructure(4, 9, wsaaTr5awArray, 0);
		private FixedLengthStringData[] wsaacngfrmtaxpyr = FLSDArrayPartOfArrayStructure(1, wsaaTr5awRec, 0);
		private FixedLengthStringData[] wsaacngfrmbrthctry = FLSDArrayPartOfArrayStructure(1, wsaaTr5awRec, 1);
		private FixedLengthStringData[] wsaacngfrmnatlty = FLSDArrayPartOfArrayStructure(1, wsaaTr5awRec, 2);
		private FixedLengthStringData[] wsaacngtotaxpyr = FLSDArrayPartOfArrayStructure(1, wsaaTr5awRec, 3);
		private FixedLengthStringData[] wsaacngtobrthctry = FLSDArrayPartOfArrayStructure(1,wsaaTr5awRec, 4);
		private FixedLengthStringData[] wsaacngtonatlty = FLSDArrayPartOfArrayStructure(1, wsaaTr5awRec, 5);
		private FixedLengthStringData[] wsaaFupCde = FLSDArrayPartOfArrayStructure(3, wsaaTr5awRec, 6);
	}
	


	protected void setFieldsCustomerSpecific()
	{
	}
	
	public Itempf getItempf() {
		return itempf;
	}


	public void setItempf(Itempf itempf) {
		this.itempf = itempf;
	}


	public ChdrlnbTableDAM getChdrlnbIO() {
		return chdrlnbIO;
	}


	public void setChdrlnbIO(ChdrlnbTableDAM chdrlnbIO) {
		this.chdrlnbIO = chdrlnbIO;
	}
	
	protected boolean chkslctn4050CustomerSpecific(){
		return false;
	}
	
	protected void setYearCustomerSpecific(){};
	
	protected void calcRskCommDate()
	{	
		if(isEQ(sv.rskComOpt,"Y") && isEQ(sv.mop,"D") && isNE(t3615rec.onepcashless,"Y")) {
			if (isGT(sv.hprrcvdt, sv.decldate)) {
				sv.rskcommdate.set(sv.hprrcvdt);
			}
			else {
				sv.rskcommdate.set(sv.decldate);
			}
		}
		else {
		 if(isEQ(sv.rskComOpt,"N")) {
				calcFirstPremDate();
			}
			else {
				sv.premRcptDateDisp.set(varcom.vrcmMaxDate);
				sv.premRcptDate.set(varcom.vrcmMaxDate);
			}
		}	
	}

	protected void calcFirstPremDate() {
		FirstRcpPremRec firstRcpPremRec = new FirstRcpPremRec();
		firstRcpPremRec.setChdrlnbIO(getChdrlnbIO());
		firstRcpPremRec.setBatcBatctrcde(wsaaBatckey.batcBatctrcde);	    	
		firstRcpPremRec.setOccdate(sv.occdate);
		firstRcpPremRec.setMop(sv.mop);
		firstRcpPremRec.setSrcebus(sv.srcebus);
		firstRcpPremRec.setOnePCashlessFlag(t3615rec.onepcashless);
		
		if(isEQ(wsspcomn.flag, "M") || isEQ(wsspcomn.flag, "I")) {
			List<Mandpf> mandpflist = mandpfDAO.getMandpfList(wsspcomn.fsuco.toString(), chdrlnbIO.getCownnum().toString());
			
			if(!mandpflist.isEmpty()) { 
				Clbapf clbapf = clbapfDAO.searchClblData(mandpflist.get(0).getBankkey(), mandpflist.get(0).getBankacckey(),
						wsspcomn.fsuco.toString(), chdrlnbIO.getCownnum().toString());
						
				wsspcomn.typinj.set(clbapf.getFacthous());
			}
		}
			
		if(wsspcomn.typinj !=null)
				firstRcpPremRec.setFacthouse(wsspcomn.typinj);	
		callProgram(FirstRcpPrem.class, firstRcpPremRec);		
		int firstpremdate = firstRcpPremRec.getFirstpremdate();		
		if(NBPRP115Permission) {
			if(firstpremdate == 0) {
				sv.premRcptDateDisp.set(varcom.vrcmMaxDate);
				sv.premRcptDate.set(varcom.vrcmMaxDate);
			}
			else {
				sv.premRcptDate.set(firstpremdate);
			}
		}
		if (isGT(sv.hpropdte, sv.decldate)) {
			if(isGT(sv.hpropdte, firstpremdate)){
				sv.rskcommdate.set(sv.hpropdte);
			}else{
				sv.rskcommdate.set(firstpremdate);
			}
		}else{
			if(isGT(sv.decldate, firstpremdate)){
				sv.rskcommdate.set(sv.decldate);
			}else{
				sv.rskcommdate.set(firstpremdate);
			}
		}
	}


}