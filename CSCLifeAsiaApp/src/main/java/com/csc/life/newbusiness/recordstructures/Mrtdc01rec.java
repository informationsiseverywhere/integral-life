package com.csc.life.newbusiness.recordstructures;

import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Mon, 17 Dec 2015 03:10:06
 * Description:
 * Copybook name: Mrtdc01rec
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */

public class Mrtdc01rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
	public FixedLengthStringData mrtdc01Rec = new FixedLengthStringData(getMrtdc01RecSize());
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(2).isAPartOf(mrtdc01Rec, 0);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(mrtdc01Rec, 2);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(mrtdc01Rec, 10);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(mrtdc01Rec, 12);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(mrtdc01Rec, 14);
  	public ZonedDecimalData riskTerm = new ZonedDecimalData(2, 0).isAPartOf(mrtdc01Rec, 16).setUnsigned();
  	public PackedDecimalData sumins = new PackedDecimalData(17, 2).isAPartOf(mrtdc01Rec, 18).setUnsigned();
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(mrtdc01Rec, 27);
public FixedLengthStringData embededBenftype = new FixedLengthStringData(4).isAPartOf(mrtdc01Rec, 31);
	public FixedLengthStringData currcode = new FixedLengthStringData(4).isAPartOf(mrtdc01Rec, 35);
public PackedDecimalData actualVal = new PackedDecimalData(17, 2).isAPartOf(mrtdc01Rec, 39).setUnsigned();
public PackedDecimalData occDate = new PackedDecimalData(8, 0).isAPartOf(mrtdc01Rec, 48).setUnsigned();
public FixedLengthStringData jlife = new FixedLengthStringData(2).isAPartOf(mrtdc01Rec, 56);
public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(mrtdc01Rec, 58);
public FixedLengthStringData status = new FixedLengthStringData(4).isAPartOf(mrtdc01Rec, 62);
public FixedLengthStringData endf = new FixedLengthStringData(1).isAPartOf(mrtdc01Rec, 66);
	public FixedLengthStringData batckey = new FixedLengthStringData(64).isAPartOf(mrtdc01Rec, 67);
	public FixedLengthStringData element = new FixedLengthStringData(4).isAPartOf(mrtdc01Rec, 131);
	public FixedLengthStringData fieldType = new FixedLengthStringData(1).isAPartOf(mrtdc01Rec, 135);
	public FixedLengthStringData virtualFundStore = new FixedLengthStringData(4).isAPartOf(mrtdc01Rec, 136);
  	public FixedLengthStringData unitTypeStore = new FixedLengthStringData(1).isAPartOf(mrtdc01Rec, 140);
	public FixedLengthStringData processInd = new FixedLengthStringData(1).isAPartOf(mrtdc01Rec, 141);
	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(mrtdc01Rec, 142);
	public PackedDecimalData estimatedVal = new PackedDecimalData(17, 2).isAPartOf(mrtdc01Rec, 143);
	public FixedLengthStringData description = new FixedLengthStringData(30).isAPartOf(mrtdc01Rec, 160);
	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(mrtdc01Rec, 190);

 


	public void initialize() {
		COBOLFunctions.initialize(mrtdc01Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			mrtdc01Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}

	public int getMrtdc01RecSize()
	{
		return 198;
	}

}
