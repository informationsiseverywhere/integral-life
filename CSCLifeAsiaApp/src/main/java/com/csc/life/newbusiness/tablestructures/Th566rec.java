package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:26
 * Description:
 * Copybook name: TH566REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th566rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th566Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData cnt = new ZonedDecimalData(2, 0).isAPartOf(th566Rec, 0);
  	public FixedLengthStringData ztranind = new FixedLengthStringData(1).isAPartOf(th566Rec, 2);
  	public FixedLengthStringData filler = new FixedLengthStringData(497).isAPartOf(th566Rec, 3, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th566Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th566Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}