package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:20
 * Description:
 * Copybook name: COVRAFIKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrafikey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrafiFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrafiKey = new FixedLengthStringData(64).isAPartOf(covrafiFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrafiChdrcoy = new FixedLengthStringData(1).isAPartOf(covrafiKey, 0);
  	public FixedLengthStringData covrafiChdrnum = new FixedLengthStringData(8).isAPartOf(covrafiKey, 1);
  	public FixedLengthStringData covrafiLife = new FixedLengthStringData(2).isAPartOf(covrafiKey, 9);
  	public FixedLengthStringData covrafiCoverage = new FixedLengthStringData(2).isAPartOf(covrafiKey, 11);
  	public FixedLengthStringData covrafiRider = new FixedLengthStringData(2).isAPartOf(covrafiKey, 13);
  	public PackedDecimalData covrafiPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrafiKey, 15);
  	public PackedDecimalData covrafiTranno = new PackedDecimalData(5, 0).isAPartOf(covrafiKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(covrafiKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrafiFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrafiFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}