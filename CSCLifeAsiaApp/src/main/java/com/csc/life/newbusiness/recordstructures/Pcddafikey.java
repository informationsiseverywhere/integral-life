package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:36
 * Description:
 * Copybook name: PCDDAFIKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Pcddafikey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData pcddafiFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData pcddafiKey = new FixedLengthStringData(64).isAPartOf(pcddafiFileKey, 0, REDEFINE);
  	public FixedLengthStringData pcddafiChdrcoy = new FixedLengthStringData(1).isAPartOf(pcddafiKey, 0);
  	public FixedLengthStringData pcddafiChdrnum = new FixedLengthStringData(8).isAPartOf(pcddafiKey, 1);
  	public FixedLengthStringData pcddafiAgntnum = new FixedLengthStringData(8).isAPartOf(pcddafiKey, 9);
  	public PackedDecimalData pcddafiTranno = new PackedDecimalData(5, 0).isAPartOf(pcddafiKey, 17);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(pcddafiKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(pcddafiFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		pcddafiFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}