package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;


public class Tr59srec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr59sRec = new FixedLengthStringData(3);
	public FixedLengthStringData growth = DD.gcdblind.copy().isAPartOf(tr59sRec,0);
	public FixedLengthStringData stepup = DD.gcdthclm.copy().isAPartOf(tr59sRec,1);
	public FixedLengthStringData higherGrowth = DD.gcsetlmd.copy().isAPartOf(tr59sRec,2);

	public void initialize() {
		COBOLFunctions.initialize(tr59sRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			tr59sRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}