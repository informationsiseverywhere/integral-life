package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S5704screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5704ScreenVars sv = (S5704ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5704screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5704ScreenVars screenVars = (S5704ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.unitpkg01.setClassString("");
		screenVars.sinsbsd01.setClassString("");
		screenVars.mbaseunt.setClassString("");
		screenVars.mbaseinc.setClassString("");
		screenVars.crtable01.setClassString("");
		screenVars.rtable01.setClassString("");
		screenVars.unitpct01.setClassString("");
		screenVars.crtable02.setClassString("");
		screenVars.rtable02.setClassString("");
		screenVars.unitpct02.setClassString("");
		screenVars.crtable03.setClassString("");
		screenVars.rtable03.setClassString("");
		screenVars.unitpct03.setClassString("");
		screenVars.crtable04.setClassString("");
		screenVars.rtable04.setClassString("");
		screenVars.unitpct04.setClassString("");
		screenVars.crtable05.setClassString("");
		screenVars.rtable05.setClassString("");
		screenVars.unitpct05.setClassString("");
		screenVars.crtable06.setClassString("");
		screenVars.rtable06.setClassString("");
		screenVars.unitpct06.setClassString("");
		screenVars.crtable07.setClassString("");
		screenVars.rtable07.setClassString("");
		screenVars.unitpct07.setClassString("");
		screenVars.crtable08.setClassString("");
		screenVars.rtable08.setClassString("");
		screenVars.unitpct08.setClassString("");
		screenVars.crtable09.setClassString("");
		screenVars.rtable09.setClassString("");
		screenVars.unitpct09.setClassString("");
		screenVars.crtable10.setClassString("");
		screenVars.rtable10.setClassString("");
		screenVars.unitpct10.setClassString("");
		screenVars.contitem.setClassString("");
		screenVars.sinsbsd02.setClassString("");
		screenVars.sinsbsd03.setClassString("");
		screenVars.sinsbsd04.setClassString("");
		screenVars.sinsbsd05.setClassString("");
		screenVars.sinsbsd06.setClassString("");
		screenVars.sinsbsd07.setClassString("");
		screenVars.sinsbsd08.setClassString("");
		screenVars.sinsbsd09.setClassString("");
		screenVars.sinsbsd10.setClassString("");
		screenVars.unitpkg02.setClassString("");
		screenVars.unitpkg03.setClassString("");
		screenVars.unitpkg04.setClassString("");
		screenVars.unitpkg05.setClassString("");
		screenVars.unitpkg06.setClassString("");
		screenVars.unitpkg07.setClassString("");
		screenVars.unitpkg08.setClassString("");
		screenVars.unitpkg09.setClassString("");
		screenVars.unitpkg10.setClassString("");
	}

/**
 * Clear all the variables in S5704screen
 */
	public static void clear(VarModel pv) {
		S5704ScreenVars screenVars = (S5704ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.unitpkg01.clear();
		screenVars.sinsbsd01.clear();
		screenVars.mbaseunt.clear();
		screenVars.mbaseinc.clear();
		screenVars.crtable01.clear();
		screenVars.rtable01.clear();
		screenVars.unitpct01.clear();
		screenVars.crtable02.clear();
		screenVars.rtable02.clear();
		screenVars.unitpct02.clear();
		screenVars.crtable03.clear();
		screenVars.rtable03.clear();
		screenVars.unitpct03.clear();
		screenVars.crtable04.clear();
		screenVars.rtable04.clear();
		screenVars.unitpct04.clear();
		screenVars.crtable05.clear();
		screenVars.rtable05.clear();
		screenVars.unitpct05.clear();
		screenVars.crtable06.clear();
		screenVars.rtable06.clear();
		screenVars.unitpct06.clear();
		screenVars.crtable07.clear();
		screenVars.rtable07.clear();
		screenVars.unitpct07.clear();
		screenVars.crtable08.clear();
		screenVars.rtable08.clear();
		screenVars.unitpct08.clear();
		screenVars.crtable09.clear();
		screenVars.rtable09.clear();
		screenVars.unitpct09.clear();
		screenVars.crtable10.clear();
		screenVars.rtable10.clear();
		screenVars.unitpct10.clear();
		screenVars.contitem.clear();
		screenVars.sinsbsd02.clear();
		screenVars.sinsbsd03.clear();
		screenVars.sinsbsd04.clear();
		screenVars.sinsbsd05.clear();
		screenVars.sinsbsd06.clear();
		screenVars.sinsbsd07.clear();
		screenVars.sinsbsd08.clear();
		screenVars.sinsbsd09.clear();
		screenVars.sinsbsd10.clear();
		screenVars.unitpkg02.clear();
		screenVars.unitpkg03.clear();
		screenVars.unitpkg04.clear();
		screenVars.unitpkg05.clear();
		screenVars.unitpkg06.clear();
		screenVars.unitpkg07.clear();
		screenVars.unitpkg08.clear();
		screenVars.unitpkg09.clear();
		screenVars.unitpkg10.clear();
	}
}
