package com.csc.life.newbusiness.dataaccess.dao;

import java.util.List;

import com.csc.life.newbusiness.dataaccess.model.Payrpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface PayrlifDAO extends BaseDAO<Payrpf> {

	public List<Payrpf> getUniqueAndFlag(String company, String chdrnum);
	public boolean deletByUnqiue(long uniqueNumber);
	public boolean updateValidflag(long uniqueNumber, String validflag);
}
