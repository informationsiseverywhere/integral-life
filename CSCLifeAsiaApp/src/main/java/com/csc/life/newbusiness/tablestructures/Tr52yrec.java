package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:09:26
 * Description:
 * Copybook name: TR52YREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr52yrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr52yRec = new FixedLengthStringData(500);
  	public FixedLengthStringData multind = new FixedLengthStringData(1).isAPartOf(tr52yRec, 0);
  	public FixedLengthStringData relto = new FixedLengthStringData(1).isAPartOf(tr52yRec, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(498).isAPartOf(tr52yRec, 2, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr52yRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr52yRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}