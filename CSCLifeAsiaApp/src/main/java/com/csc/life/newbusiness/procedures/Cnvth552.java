/*
 * File: Cnvth552.java
 * Date: December 3, 2013 2:17:40 AM ICT
 * Author: CSC
 * 
 * Class transformed from CNVTH552.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.tablestructures.Th552rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table TH552, Replace INSTPR with INSTPR 1,
*   add new fields INSTPR02 to INSTPR11.
*
*
****************************************************************** ****
* </pre>
*/
public class Cnvth552 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVTH552");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String th552 = "TH552";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData iy = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Th552rec th552rec = new Th552rec();
	private Varcom varcom = new Varcom();
	private Th552RecOldInner th552RecOldInner = new Th552RecOldInner();

	public Cnvth552() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(th552);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT TH552 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(), th552)) {
			getAppVars().addDiagnostic("Table TH552 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itemIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT TH552 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteTh5522080();
	}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		th552RecOldInner.th552RecOld.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		th552rec.currcode.set(th552RecOldInner.th552CurrcodeOld);
		for (ix.set(1); !(isGT(ix, 8)); ix.add(1)){
			th552rec.ageIssageFrm[ix.toInt()].set(th552RecOldInner.th552AgfrmOld[ix.toInt()]);
			th552rec.ageIssageTo[ix.toInt()].set(th552RecOldInner.th552AgtoOld[ix.toInt()]);
		}
		iy.set(1);
		for (ix.set(1); !(isGT(ix, 64)); ix.add(1)){
			th552rec.defFupMeth[ix.toInt()].set(th552RecOldInner.th552DefFupMethOld[iy.toInt()]);
			/*    As field bumber is  not consecutive in SH552,so*/
			/*    skip fields that has been deleted in SH552 change.*/
			if (isEQ(iy, 8)
			|| isEQ(iy, 17)
			|| isEQ(iy, 26)
			|| isEQ(iy, 35)
			|| isEQ(iy, 44)
			|| isEQ(iy, 53)
			|| isEQ(iy, 62)) {
				iy.add(2);
			}
			else {
				iy.add(1);
			}
		}
		for (ix.set(1); !(isGT(ix, 8)); ix.add(1)){
			th552rec.zsuminfr[ix.toInt()].set(th552RecOldInner.th552ZsuminfrOld[ix.toInt()]);
			th552rec.zsuminto[ix.toInt()].set(th552RecOldInner.th552ZsumintoOld[ix.toInt()]);
		}
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(th552rec.th552Rec);
	}

protected void rewriteTh5522080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(), th552)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for TH552");
		}
	}
/*
 * Class transformed  from Data Structure TH552-REC-OLD--INNER
 */
private static final class Th552RecOldInner { 

	private FixedLengthStringData th552RecOld = new FixedLengthStringData(500);
	private FixedLengthStringData th552AgfrmsOld = new FixedLengthStringData(18).isAPartOf(th552RecOld, 0);
	private ZonedDecimalData[] th552AgfrmOld = ZDArrayPartOfStructure(9, 2, 0, th552AgfrmsOld, 0);
	private FixedLengthStringData th552AgtosOld = new FixedLengthStringData(18).isAPartOf(th552RecOld, 18);
	private ZonedDecimalData[] th552AgtoOld = ZDArrayPartOfStructure(9, 2, 0, th552AgtosOld, 0);
	private FixedLengthStringData th552CurrcodeOld = new FixedLengthStringData(3).isAPartOf(th552RecOld, 36);
	private FixedLengthStringData th552DefFupMethsOld = new FixedLengthStringData(288).isAPartOf(th552RecOld, 39);
	private FixedLengthStringData[] th552DefFupMethOld = FLSArrayPartOfStructure(72, 4, th552DefFupMethsOld, 0);
	private FixedLengthStringData th552ZsuminfrsOld = new FixedLengthStringData(64).isAPartOf(th552RecOld, 327);
	private ZonedDecimalData[] th552ZsuminfrOld = ZDArrayPartOfStructure(8, 8, 0, th552ZsuminfrsOld, 0);
	private FixedLengthStringData th552ZsumintosOld = new FixedLengthStringData(64).isAPartOf(th552RecOld, 391);
	private ZonedDecimalData[] th552ZsumintoOld = ZDArrayPartOfStructure(8, 8, 0, th552ZsumintosOld, 0);
}
}
