package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.dao.UnltpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Unltpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public  class UnltpfDAOImpl extends BaseDAOImpl<Unltpf> implements UnltpfDAO {


	private static final Logger LOGGER = LoggerFactory.getLogger(UnltpfDAOImpl.class);
	PreparedStatement stmt ;  
	
    public List<Unltpf> readUnltpf(Unltpf Unltpf)
{
		List<Unltpf> UnltpfList = null;
	    String query = "select chdrcoy,chdrnum,life,coverage,rider,validflag,currfrom,tranno,ualfnd01,ualfnd02,ualfnd03,ualfnd04,ualfnd05,ualfnd06,ualfnd07,ualfnd08,ualfnd09,"
	    + "ualfnd10, ualprc01,ualprc02,ualprc03,ualprc04,ualprc05,ualprc06,ualprc07,ualprc08,ualprc09,ualprc10,"
	    + "udafnd01,udafnd02,udafnd03,udafnd04,udafnd05,udafnd06,udafnd07,udafnd08,udafnd09,udafnd10,"
	    + "udalpr01,udalpr02,udalpr03,udalpr04,udalpr05,udalpr06,udalpr07,udalpr08,udalpr09,udalpr10,"
	    + "uspcpr01,uspcpr02,uspcpr03,uspcpr04,uspcpr05,uspcpr06,uspcpr07,uspcpr08,uspcpr09,uspcpr10,prcamtind,currto,ptopup,seqnbr, numapp,"
	    + "usrprf,jobnm,datime from unltpf where chdrcoy = ? and chdrnum = ? and validflag = ? order by chdrcoy ASC, chdrnum ASC, life ASC, coverage ASC, rider ASC, seqnbr ASC, UNIQUE_NUMBER DESC";
		

       ResultSet rs = null;
       Unltpf UnltpfData = null;
       stmt = null;
       try 
       {
         UnltpfList = new ArrayList<Unltpf>();
         stmt = getConnection().prepareStatement(query);/* IJTI-1523 */
         stmt.setString(1, Unltpf.getChdrcoy());
         stmt.setString(2, Unltpf.getChdrnum());
         stmt.setString(3, Unltpf.getValidflag());
   
          rs = stmt.executeQuery();
          while (rs.next()) {
    	  UnltpfData= new Unltpf();
             if (rs.getString(1) != null) {
            	 UnltpfData.setChdrcoy(Unltpf.getChdrcoy());
            	 UnltpfData.setChdrnum(Unltpf.getChdrnum());
            	 UnltpfData.setValidflag(Unltpf.getValidflag());
            	 UnltpfData.setChdrcoy(rs.getString(1).trim());
            	 UnltpfData.setChdrnum(rs.getString(2));
            	 UnltpfData.setLife(rs.getString(3));
            	 UnltpfData.setCoverage(rs.getString(4));
            	 UnltpfData.setRider(rs.getString(5));
            	 UnltpfData.setValidflag(rs.getString(6));
            	 UnltpfData.setCurrfrom(rs.getInt(7));
            	 UnltpfData.setTranno(rs.getInt(8));
            	 UnltpfData.setUalfnd01(rs.getString(9));
            	 UnltpfData.setUalfnd02(rs.getString(10));
            	 UnltpfData.setUalfnd03(rs.getString(11));
            	 UnltpfData.setUalfnd04(rs.getString(12));
            	 UnltpfData.setUalfnd05(rs.getString(13));
            	 UnltpfData.setUalfnd06(rs.getString(14));
            	 UnltpfData.setUalfnd07(rs.getString(15));
            	 UnltpfData.setUalfnd08(rs.getString(16));
            	 UnltpfData.setUalfnd09(rs.getString(17));
            	 UnltpfData.setUalfnd10(rs.getString(18));
            	 UnltpfData.setUalprc01(rs.getBigDecimal(19));
            	 UnltpfData.setUalprc02(rs.getBigDecimal(20));
            	 UnltpfData.setUalprc03(rs.getBigDecimal(21));
            	 UnltpfData.setUalprc04(rs.getBigDecimal(22));
            	 UnltpfData.setUalprc05(rs.getBigDecimal(23));
            	 UnltpfData.setUalprc06(rs.getBigDecimal(24));
            	 UnltpfData.setUalprc07(rs.getBigDecimal(25));
            	 UnltpfData.setUalprc08(rs.getBigDecimal(26));
            	 UnltpfData.setUalprc09(rs.getBigDecimal(27));
            	 UnltpfData.setUalprc10(rs.getBigDecimal(28));
            	 UnltpfData.setUdafnd01(rs.getString(29));
            	 UnltpfData.setUdafnd02(rs.getString(30));
            	 UnltpfData.setUdafnd03(rs.getString(31));
            	 UnltpfData.setUdafnd04(rs.getString(32));
            	 UnltpfData.setUdafnd05(rs.getString(33));
            	 UnltpfData.setUdafnd06(rs.getString(34));
            	 UnltpfData.setUdafnd07(rs.getString(35));
            	 UnltpfData.setUdafnd08(rs.getString(36));
            	 UnltpfData.setUdafnd09(rs.getString(37));
            	 UnltpfData.setUdafnd10(rs.getString(38));
            	 UnltpfData.setUdalpr01(rs.getBigDecimal(39));
            	 UnltpfData.setUdalpr02(rs.getBigDecimal(40));
            	 UnltpfData.setUdalpr03(rs.getBigDecimal(41));
            	 UnltpfData.setUdalpr04(rs.getBigDecimal(42));
            	 UnltpfData.setUdalpr05(rs.getBigDecimal(43));
            	 UnltpfData.setUdalpr06(rs.getBigDecimal(44));
            	 UnltpfData.setUdalpr07(rs.getBigDecimal(45));
            	 UnltpfData.setUdalpr08(rs.getBigDecimal(46));
            	 UnltpfData.setUdalpr09(rs.getBigDecimal(47));
            	 UnltpfData.setUdalpr10(rs.getBigDecimal(48));
            	 UnltpfData.setUspcpr01(rs.getBigDecimal(49));
            	 UnltpfData.setUspcpr02(rs.getBigDecimal(50));
            	 UnltpfData.setUspcpr03(rs.getBigDecimal(51));
            	 UnltpfData.setUspcpr04(rs.getBigDecimal(52));
            	 UnltpfData.setUspcpr05(rs.getBigDecimal(53));
            	 UnltpfData.setUspcpr06(rs.getBigDecimal(54));
            	 UnltpfData.setUspcpr07(rs.getBigDecimal(55));
            	 UnltpfData.setUspcpr08(rs.getBigDecimal(56));
            	 UnltpfData.setUspcpr09(rs.getBigDecimal(57));
            	 UnltpfData.setUspcpr10(rs.getBigDecimal(58));
            	 UnltpfData.setPrcamtind(rs.getString(59));
            	 UnltpfData.setCurrto(rs.getInt(60));
            	 UnltpfData.setPtopup(rs.getString(61));
            	 UnltpfData.setSeqnbr(rs.getInt(62));
            	 UnltpfData.setNumapp(rs.getInt(63));
            	 UnltpfData.setUsrprf(rs.getString(64));
            	 UnltpfData.setJobnm(rs.getString(65));
            	 UnltpfData.setDatime(rs.getTimestamp(66));
            	 UnltpfList.add(UnltpfData);
             }
       }
    }
       catch (SQLException e) 
         {
          LOGGER.error("readUnltpf()", e);//IJTI-1561
          throw new SQLRuntimeException(e);
         }        
       finally
       {
       close(stmt, rs);
       }

      return UnltpfList;
}
     // ILIFE-4970 start 
      public List<Unltpf> getFundUnltpf(Unltpf Unltpf)
{
       List<Unltpf> UnltpfList = null;
       String query = "select chdrcoy,chdrnum,life,coverage,rider,validflag,currfrom,tranno,ualfnd01,ualfnd02,ualfnd03,"
       	+ "ualfnd04,ualfnd05,ualfnd06,ualfnd07,ualfnd08,ualfnd09,"
		+ "ualfnd10, ualprc01,ualprc02,ualprc03,ualprc04,ualprc05,ualprc06,ualprc07,ualprc08,ualprc09,ualprc10,"
		+ "prcamtind,currto,ptopup,seqnbr, numapp,usrprf,jobnm from unltpf  where chdrcoy = ? and chdrnum = ? "
		+ " order by chdrcoy ASC, chdrnum ASC, life ASC, coverage ASC, rider ASC, seqnbr ASC, UNIQUE_NUMBER DESC";
		

        ResultSet rs = null;
        Unltpf UnltpfData = null;
        stmt = null;
        try {

	   UnltpfList = new ArrayList<Unltpf>();
       stmt = getConnection().prepareStatement(query);/* IJTI-1523 */
       stmt.setString(1, Unltpf.getChdrcoy());
       stmt.setString(2, Unltpf.getChdrnum());
  
       rs = stmt.executeQuery();
       while (rs.next()) {
    	   	UnltpfData= new Unltpf();
             if (rs.getString(1) != null)
             {
            	 UnltpfData.setChdrcoy(rs.getString(1).trim());
            	 UnltpfData.setChdrnum(rs.getString(2));
            	 UnltpfData.setLife(rs.getString(3));
            	 UnltpfData.setCoverage(rs.getString(4));
            	 UnltpfData.setRider(rs.getString(5));
            	 UnltpfData.setValidflag(rs.getString(6));
            	 UnltpfData.setCurrfrom(rs.getInt(7));
            	 UnltpfData.setTranno(rs.getInt(8));
            	 UnltpfData.setUalfnd01(rs.getString(9));
            	 UnltpfData.setUalfnd02(rs.getString(10));
            	 UnltpfData.setUalfnd03(rs.getString(11));
            	 UnltpfData.setUalfnd04(rs.getString(12));
            	 UnltpfData.setUalfnd05(rs.getString(13));
            	 UnltpfData.setUalfnd06(rs.getString(14));
            	 UnltpfData.setUalfnd07(rs.getString(15));
            	 UnltpfData.setUalfnd08(rs.getString(16));
            	 UnltpfData.setUalfnd09(rs.getString(17));
            	 UnltpfData.setUalfnd10(rs.getString(18));
            	 UnltpfData.setPrcamtind(rs.getString(19));
            	 UnltpfData.setCurrto(rs.getInt(20));
            	 UnltpfData.setPtopup(rs.getString(21));
            	 UnltpfData.setSeqnbr(rs.getInt(22));
            	 UnltpfData.setNumapp(rs.getInt(23));
            	 UnltpfData.setUsrprf(rs.getString(24));
            	 UnltpfData.setJobnm(rs.getString(25));
            	 UnltpfList.add(UnltpfData);
             }
       }
}
       catch (SQLException e) 
        {
       LOGGER.error("getFundUnltpf()", e);//IJTI-1561
       throw new SQLRuntimeException(e);
        }
       finally 
       {
       close(stmt, rs);
       }

      return UnltpfList;
}
    //ILIFE-4970 end
      
      
	public void insertUnltpfList(List<Unltpf> insertList) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO UNLTPF(CHDRCOY,CHDRNUM,VALIDFLAG,LIFE,COVERAGE,RIDER,CURRFROM,TRANNO,UALFND01,UALFND02,UALFND03,UALFND04,UALFND05,UALFND06,UALFND07,UALFND08,UALFND09,UALFND10,UALPRC01,UALPRC02,UALPRC03,UALPRC04,UALPRC05,UALPRC06,UALPRC07,UALPRC08,UALPRC09,UALPRC10,UDAFND01,UDAFND02,UDAFND03,UDAFND04,UDAFND05,UDAFND06,UDAFND07,UDAFND08,UDAFND09,UDAFND10,UDALPR01,UDALPR02,UDALPR03,UDALPR04,UDALPR05,UDALPR06,UDALPR07,UDALPR08,UDALPR09,UDALPR10,USPCPR01,USPCPR02,USPCPR03,USPCPR04,USPCPR05,USPCPR06,USPCPR07,USPCPR08,USPCPR09,USPCPR10,PRCAMTIND,CURRTO,PTOPUP,SEQNBR,NUMAPP,USRPRF,JOBNM,DATIME)");
		sb.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = getPrepareStatement(sb.toString());
		ResultSet rs = null;
		try {
			for (Unltpf u : insertList) {
				int i = 1;
				ps.setString(i++, u.getChdrcoy());
				ps.setString(i++, u.getChdrnum());
				ps.setString(i++, u.getValidflag());
				ps.setString(i++, u.getLife());
				ps.setString(i++, u.getCoverage());
				ps.setString(i++, u.getRider());
				ps.setInt(i++, u.getCurrfrom());
				ps.setInt(i++, u.getTranno());
				ps.setString(i++, u.getUalfnd01());
				ps.setString(i++, u.getUalfnd02());
				ps.setString(i++, u.getUalfnd03());
				ps.setString(i++, u.getUalfnd04());
				ps.setString(i++, u.getUalfnd05());
				ps.setString(i++, u.getUalfnd06());
				ps.setString(i++, u.getUalfnd07());
				ps.setString(i++, u.getUalfnd08());
				ps.setString(i++, u.getUalfnd09());
				ps.setString(i++, u.getUalfnd10());
				ps.setBigDecimal(i++, u.getUalprc01());
				ps.setBigDecimal(i++, u.getUalprc02());
				ps.setBigDecimal(i++, u.getUalprc03());
				ps.setBigDecimal(i++, u.getUalprc04());
				ps.setBigDecimal(i++, u.getUalprc05());
				ps.setBigDecimal(i++, u.getUalprc06());
				ps.setBigDecimal(i++, u.getUalprc07());
				ps.setBigDecimal(i++, u.getUalprc08());
				ps.setBigDecimal(i++, u.getUalprc09());
				ps.setBigDecimal(i++, u.getUalprc10());
				ps.setString(i++, u.getUdafnd01());
				ps.setString(i++, u.getUdafnd02());
				ps.setString(i++, u.getUdafnd03());
				ps.setString(i++, u.getUdafnd04());
				ps.setString(i++, u.getUdafnd05());
				ps.setString(i++, u.getUdafnd06());
				ps.setString(i++, u.getUdafnd07());
				ps.setString(i++, u.getUdafnd08());
				ps.setString(i++, u.getUdafnd09());
				ps.setString(i++, u.getUdafnd10());
				ps.setBigDecimal(i++, u.getUdalpr01());
				ps.setBigDecimal(i++, u.getUdalpr02());
				ps.setBigDecimal(i++, u.getUdalpr03());
				ps.setBigDecimal(i++, u.getUdalpr04());
				ps.setBigDecimal(i++, u.getUdalpr05());
				ps.setBigDecimal(i++, u.getUdalpr06());
				ps.setBigDecimal(i++, u.getUdalpr07());
				ps.setBigDecimal(i++, u.getUdalpr08());
				ps.setBigDecimal(i++, u.getUdalpr09());
				ps.setBigDecimal(i++, u.getUdalpr10());
				ps.setBigDecimal(i++, u.getUspcpr01());
				ps.setBigDecimal(i++, u.getUspcpr02());
				ps.setBigDecimal(i++, u.getUspcpr03());
				ps.setBigDecimal(i++, u.getUspcpr04());
				ps.setBigDecimal(i++, u.getUspcpr05());
				ps.setBigDecimal(i++, u.getUspcpr06());
				ps.setBigDecimal(i++, u.getUspcpr07());
				ps.setBigDecimal(i++, u.getUspcpr08());
				ps.setBigDecimal(i++, u.getUspcpr09());
				ps.setBigDecimal(i++, u.getUspcpr10());
				ps.setString(i++, u.getPrcamtind());
				ps.setInt(i++, u.getCurrto());
				ps.setString(i++, u.getPtopup());
				ps.setInt(i++, u.getSeqnbr());
				ps.setInt(i++, u.getNumapp());
				ps.setString(i++, getUsrprf());
				ps.setString(i++, getJobnm());
				ps.setTimestamp(i++, getDatime());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertUnltpfList()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
	}	
}
