/******************************************************************************
 * File Name 		: AckdpfDAOImpl.java
 * Author			: sbatra8
 * Creation Date	: 17 April 2020
 * Project			: Integral Life
 * Description		: The DAO Implementation Class for ACKDPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.dao.AckdpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Ackdpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AckdpfDAOImpl extends BaseDAOImpl<Ackdpf> implements AckdpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(AckdpfDAOImpl.class);

	public List<Ackdpf> searchAckdpfRecord(String tableId, String memName,
			int batchExtractSize, int batchID){

		StringBuilder  sqlStr = new StringBuilder();

		 	sqlStr.append("SELECT * FROM (");
	        sqlStr.append("SELECT FLOOR((ROW_NUMBER()OVER(ORDER BY TE.UNIQUE_NUMBER DESC)-1)/?) ROWNM, TE.* FROM ");
	        sqlStr.append(tableId);
	        sqlStr.append(" TE");
	        sqlStr.append(" WHERE MEMBER_NAME = ? )cc WHERE cc.ROWNM = ?");
			PreparedStatement psAckdpfSelect = getPrepareStatement(sqlStr.toString());
			ResultSet sqlAckdpfRs = null;
			List<Ackdpf> outputList = new ArrayList<>();

		try {

			psAckdpfSelect.setInt(1, batchExtractSize);
			psAckdpfSelect.setString(2, memName);
			psAckdpfSelect.setInt(3, batchID);

			sqlAckdpfRs = executeQuery(psAckdpfSelect);

			while (sqlAckdpfRs.next()) {

				Ackdpf ackdpf = new Ackdpf();

				ackdpf.setUniqueNumber(sqlAckdpfRs.getInt("UNIQUE_NUMBER"));
				ackdpf.setChdrcoy(sqlAckdpfRs.getString("CHDRCOY"));
				ackdpf.setChdrnum(sqlAckdpfRs.getString("CHDRNUM"));
				ackdpf.setDlvrmode(sqlAckdpfRs.getString("DLVRMODE"));
				ackdpf.setDespdate(sqlAckdpfRs.getInt("DESPDATE"));
				ackdpf.setPackdate(sqlAckdpfRs.getInt("PACKDATE"));
				ackdpf.setDeemdate(sqlAckdpfRs.getInt("DEEMDATE"));
				ackdpf.setNextActDate(sqlAckdpfRs.getInt("NXTDTE"));
				ackdpf.setHpaduqn(sqlAckdpfRs.getInt("HPADUQN"));
				outputList.add(ackdpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchExprpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psAckdpfSelect, sqlAckdpfRs);
		}

		return outputList;
	}

}
