/*
 * File: Br5b5.java
 * Date: 29 August 2009 21:28:37
 * Author: Quipoz Limited
 * 
 * Class transformed from BH519.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.impl.ChdrpfDAOImpl;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.model.Matypf;
import com.csc.life.terminationclaims.tablestructures.Tr5b2rec;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.tablestructures.T1688rec;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.ItempfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
/**
* 
* </pre>
*/
public class Br5b5 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR5B5");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	/*
	 * These fields are required by MAINB processing and should not be deleted.
	 */
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRiskCessDate = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaMaxAge = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaMaxYear = new PackedDecimalData(2, 0);
	private FixedLengthStringData wsaaNlgflag = new FixedLengthStringData(1);
	private PackedDecimalData wsaaLifeAge = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaNewTranNo = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaYear = new PackedDecimalData(2, 0);
	
	private FixedLengthStringData wsaaChdxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(wsaaChdxFn, 0, FILLER).init("CHDX");
	private FixedLengthStringData wsaaChdxRunid = new FixedLengthStringData(2).isAPartOf(wsaaChdxFn, 4);
	private ZonedDecimalData wsaaChdxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaChdxFn, 6).setUnsigned();

	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaValidStatus = "";
	private String wsaaCrtableFound = "N";
	/* CONTROL-TOTALS */
	private static final int ct01 = 1;

	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	
	private T1688rec t1688rec = new T1688rec();
	private T5679rec t5679rec = new T5679rec();
	private Tr5b2rec tr5b2rec = new Tr5b2rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon3rec datcon3rec = new Datcon3rec();

	
	
	private Chdrpf chdrpf = new Chdrpf();
	private Chdrpf chdrpfnlg = new Chdrpf();
	private List<Chdrpf> chdrlist;
	private List<Chdrpf> chdrlist1;

	private Lifepf lifepf = new Lifepf();
	private List<Matypf> insertMatypfList = new ArrayList<Matypf>();
	private List<Ptrnpf> ptrnpfList = new ArrayList<Ptrnpf>();

	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrDAO", ChdrpfDAOImpl.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);

	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO",LifepfDAO.class);
	private Map<String, List<Itempf>> t1688Map = new HashMap<String, List<Itempf>>();
	private Map<String, List<Itempf>> tr5b2Map = new HashMap<String, List<Itempf>>();
	private Map<String, List<Itempf>> t5679Map = new HashMap<String, List<Itempf>>();

	private Agecalcrec agecalcrec = new Agecalcrec();
	private Iterator<Chdrpf> iteratorList1;
	
	private int wsaacount;
	private boolean noRecordFound;
	
	private boolean updateRequired=true;

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private static final Logger LOGGER = LoggerFactory.getLogger(Br5b5.class);

	
	
	
	
	
	public Br5b5() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		readT56791100();
	}

protected void initialise1010()
	{
		wsaaChdxRunid.set(bprdIO.getSystemParam04());
		wsaaChdxJobno.set(bsscIO.getScheduleNumber());
		wsaaCompany.set(bsprIO.getCompany().toString());
		/* The next thing we must do is construct the name of the */
		/* temporary file which we will be working with. */
		noRecordFound = false;
		chdrlist = new ArrayList<Chdrpf>();
		chdrlist1 = new ArrayList<Chdrpf>();

		wsaaEffdate.set(bsscIO.getEffectiveDate());
		wsaaMaxAge.set(ZERO);
		wsaaMaxYear.set(ZERO);

		String coy = bsprIO.getCompany().toString();
		String pfx = smtpfxcpy.item.toString();
		t1688Map = itemDAO.loadSmartTable(pfx, coy, "T1688");
		t5679Map = itemDAO.loadSmartTable(pfx, coy, "T5679");
		tr5b2Map = itemDAO.loadSmartTable(pfx, coy, "TR5B2");
		
		chdrlist1 = chdrpfDAO.getchdrnlgrecord(coy);
		if (chdrlist1.isEmpty())
			noRecordFound = true;
		else {
			wsaacount = chdrlist1.size();
			iteratorList1 = chdrlist1.iterator();
		}
	}

protected void readT56791100() {
	if (t5679Map.containsKey(bprdIO.getAuthCode())) {
		List<Itempf> t5679ItemList = t5679Map.get(bprdIO.getAuthCode());
		Itempf t5679Item = t5679ItemList.get(0);
		t5679rec.t5679Rec.set(StringUtil.rawToString(t5679Item.getGenarea()));
		return;
	}
	fatalError600();
}

		
		
		
protected void readFile2000() {

	if (wsaacount > 0) {
		wsaacount = wsaacount-1; 
		wsaaEof.set("N");
	} else {
		wsspEdterror.set(varcom.endp);
	}
}

protected void edit2500() {
	wsspEdterror.set(varcom.oK);
	
		wsspEdterror.set(varcom.oK);
		updateRequired=true;
		readnlgtData3100();
}
protected void update3000()
{	
				while (!(endOfFile.isTrue())) {
				if (isNE(chdrpf.getChdrnum(), wsaaChdrChdrnum)){
					wsaaChdrChdrcoy.set(chdrpf.getChdrcoy());
					wsaaChdrChdrnum.set(chdrpf.getChdrnum());
				readTr5b2();
				if (isEQ(wsaaMaxAge,ZERO)
				&& isEQ(wsaaMaxYear,ZERO)){
					updateRequired = false;
					return;
				}
				if (isNE(wsaaMaxAge,ZERO)){
					checkAge2860();
				}
				if (isLT(wsaaLifeAge,wsaaMaxAge)){
					updateRequired = false;
					return;
				}
				if (isNE(wsaaMaxYear,ZERO)){
					checkYear2880();
				}
				if (isLT(wsaaYear,wsaaMaxYear)){
					updateRequired = false;
					return;
				}
				softlockContract2890();
				
				if (updateRequired){
							updateChdr3200();
						}
	}
				else{
					return;
					
				}
				}
}
protected void readnlgtData3100() {
	
	if(iteratorList1.hasNext()) {
		chdrpf = new Chdrpf();
		chdrpf = iteratorList1.next();
		} 
	else {
			wsaaEof.set("Y");
		}
}

protected void CheckChdrStatus2810() {
	
	initialRisk2820();
	loopRisk2830();

}
protected void initialRisk2820() {
	wsaaSub1.set(ZERO);
}

protected void loopRisk2830() {
	wsaaSub1.add(1);
	if (isGT(wsaaSub1, 12)) {
		wsaaValidStatus = "N";
		return;
	}
	if (isEQ(chdrpf.getStatcode(), t5679rec.cnRiskStat[wsaaSub1.toInt()])) {
		initialPrem2840();
		if(isEQ(wsaaValidStatus,"Y")){
			return;
		}
	}
	loopRisk2830();
	return;
}

protected void initialPrem2840() { 
	wsaaSub1.set(ZERO);
	loopPrem2850();	
}

protected void loopPrem2850() {
	wsaaSub1.add(1);
	if (isGT(wsaaSub1, 12)) {
		wsaaValidStatus = "N";
		return;
	}
	if (isEQ(chdrpf.getPstcde(), t5679rec.cnPremStat[wsaaSub1.toInt()])) {
		wsaaValidStatus = "Y";
		return;
	}
	loopPrem2850();
	return;
}
protected void readTr5b2() {
	
	ItempfDAO itempfDAO = DAOFactory.getItempfDAO();
	/*Itempf itempf = new Itempf();
	itempf.setItemtabl("TR5B2");
	itempf.setItemitem(chdrpf.getCnttype().toString());
	itempf.setItemcoy(bsprIO.getCompany().toString().trim());
	itempf.setItempfx("IT");
	itempf.setValidflag("1");
	itempf.setItmfrm(new BigDecimal(chdrpf.getOccdate().toString()));
	itempf.setItmto(new BigDecimal(varcom.vrcmMaxDate.toString()));
	List<Itempf> itempfList = itempfDAO.findByItemDates(itempf);*/
	List<Itempf> itempfList = itempfDAO.findItemByTable("IT", bsprIO.getCompany().toString().trim(), "TR5B2",chdrpf.getCnttype(), "1");/* IJTI-1523 */
	if(itempfList.size() > 0) {
		for (Itempf it : itempfList) {
			tr5b2rec.tr5b2Rec.set(new String(it.getGenarea()));
		}
	}
	else{
		return;
	}
	
	wsaaNlgflag.set(tr5b2rec.nlg);
	wsaaMaxAge.set(tr5b2rec.nlgczag);
	wsaaMaxYear.set(tr5b2rec.maxnlgyrs);
	
}
protected void checkAge2860()
{
	wsaaLifeAge.set(ZERO);
	lifepf = lifepfDAO.getLifeRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "01", "00");
	if (lifepf== null ) {
		syserrrec.params.set(lifepf);
		fatalError600();
	}
	calcAge2870();
	
}
protected void calcAge2870()
{
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(bsscIO.getLanguage());
		agecalcrec.cnttype.set(chdrpf.getCnttype());
		agecalcrec.intDate1.set(lifepf.getCltdob());
		agecalcrec.intDate2.set(bsscIO.getEffectiveDate());
		agecalcrec.company.set(bsprIO.getFsuco());
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			syserrrec.params.set(agecalcrec.agecalcRec);
			fatalError600();
		}
		wsaaLifeAge.set(agecalcrec.agerating);
}
protected void checkYear2880(){
	
	wsaaYear.set(ZERO);
	datcon3rec.intDate1.set(chdrpf.getOccdate());
	datcon3rec.intDate2.set(bsscIO.getEffectiveDate());
	datcon3rec.frequency.set("01");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz,varcom.oK)) {
		syserrrec.params.set(datcon3rec.datcon3Rec);
		syserrrec.statuz.set(datcon3rec.statuz);
		fatalError600();
	}
	wsaaYear.set(datcon3rec.freqFactor);
}

protected void softlockContract2890()
{
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(bsprIO.getCompany());
		sftlockrec.enttyp.set(chdrpf.getChdrpfx());
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(99999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
				&& isNE(sftlockrec.statuz, "LOCK")) {
					syserrrec.params.set(sftlockrec.sftlockRec);
					syserrrec.statuz.set(sftlockrec.statuz);
					fatalError600();
				}
}
protected void updateChdr3200()
{
		if(chdrpf == null)
			chdrpf= new Chdrpf();
		chdrpf.setChdrcoy(chdrpf.getChdrcoy());
		chdrpf.setChdrnum(chdrpf.getChdrnum());
		chdrpf.setNlgflg('E');
		chdrlist.add(chdrpf);
	
}

	

protected void commit3500(){
	
		if (chdrlist != null){
			chdrpfDAO.updateChdrNlgflg(chdrlist);
		}
	
}
protected void rollback3600() {
	/* ROLLBACK */
	/* EXIT */

}
protected void close4000() {
	
			if (chdrlist != null){
				chdrlist.clear();
			}
			chdrlist = null;
			
			if (t5679Map != null) {
				t5679Map.clear();
		    }
			t5679Map = null;
			
			if (t1688Map != null) {
				t1688Map.clear();
		    }
			t1688Map = null;
			if (tr5b2Map != null) {
				tr5b2Map.clear();
		    }
			tr5b2Map = null;
	
	
}


}

