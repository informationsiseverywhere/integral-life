/**
 * 
 */
package com.csc.life.newbusiness.procedures;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.AgecalcPojo;
import com.csc.fsu.general.procedures.AgecalcUtils;
import com.csc.fsu.general.procedures.Datcon3Pojo;
import com.csc.fsu.general.procedures.Datcon3Utils;
import com.csc.life.contractservicing.recordstructures.LincpfHelper;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.dao.LincpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.newbusiness.dataaccess.model.Lincpf;
import com.csc.life.newbusiness.dataaccess.model.Slncpf;
import com.csc.life.newbusiness.tablestructures.Tjl06rec;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * @author gsaluja2
 *
 */
@Service("lincCommonsUtil")
public class LincCommonsUtilImpl implements LincCommonsUtil{
	
	@Autowired
	private AgecalcUtils agecalcUtil;
	
	@Autowired
	private Datcon3Utils datcon3Object;
	
	@Autowired
	private DescDAO descpfDAO;
	
	@Autowired
	private ItempfDAO itempfDAO;
	
	@Autowired
	private FluppfDAO fluppfDAO;
	
	@Autowired
	private LincpfDAO lincpfDAO;
	
	private static final String TJL06 = "TJL06";
	
	private static final String TH605 = "TH605";
	
	private static final String T5661 = "T5661";
	
	private Tjl06rec tjl06rec = new Tjl06rec();
	
	private BigDecimal sumInsured;
	
	private BigDecimal hospitalBenfitSumInsured;
	
	private List<BigDecimal> covtSuminsList;
	
	private List<BigDecimal> hosbensSuminsList;
	
	private BigDecimal covtItemSumin;
	
	private BigDecimal hosbensItemSumin;
	
	private Lincpf lincpf;
	
	private boolean flag;
	
	private static final String ZERO = "0";
	
	private static final String SPACE = " ";
	
	private static final String ITEMPFX = "IT";
	
	private StringBuilder address;
	
	private StringBuilder kanaName;
	
	private StringBuilder kanjiName;
	
	private Th605rec th605rec = new Th605rec();
	
	private Slncpf slncpfRecord;
	
	private Fluppf fluppf;
	
	private Descpf descpf;
	
	private T5661rec t5661rec = new T5661rec();
	
	private Varcom varcom = new Varcom();
	
	private int clntKanaNameLength = 42;
	
	private int lifeKanaNameLength = 32;
	
	private int kanjiNameLength = 60;
	
	public LincCommonsUtilImpl() {
		//does nothing
	}
	
	public void validateLifeAssuredAge(LincpfHelper lincpfHelper, int effDate) {
		AgecalcPojo agecalcPojo = new AgecalcPojo();
		agecalcPojo.setFunction("CALCP");
		agecalcPojo.setLanguage(lincpfHelper.getLanguage());
		agecalcPojo.setCnttype(lincpfHelper.getCnttype());
		agecalcPojo.setIntDate1(String.valueOf(lincpfHelper.getLcltdob()));
		agecalcPojo.setIntDate2(String.valueOf(effDate));
		agecalcPojo.setCompany(lincpfHelper.getOwncoy());
		agecalcUtil.calcAge(agecalcPojo);
		if(!agecalcPojo.getStatuz().equals(Varcom.oK.toString())) {
			lincpfHelper.setLifeage(0);
			return;
		}
		lincpfHelper.setLifeage(agecalcPojo.getAgerating());
	}

	@Override
	public BigDecimal getTermDiff(int occdate, int transactionDate) {
		Datcon3Pojo datcon3Pojo = new Datcon3Pojo();
		datcon3Pojo.setIntDate1(String.valueOf(occdate));
		datcon3Pojo.setIntDate2(String.valueOf(transactionDate));
		datcon3Pojo.setFrequency("01");
		datcon3Object.calDatcon3(datcon3Pojo);
		if(!datcon3Pojo.getStatuz().equals(Varcom.oK.toString()))
			return BigDecimal.ZERO;
		return datcon3Pojo.getDurationOut();
	}

	@Override
	public void checkCriteria(Map<String, BigDecimal> crtableSuminsMap, LincpfHelper lincpfHelper) {
		init();
		readTables(lincpfHelper);
		lincpfHelper.setCritSatisfied(false);
		if(!tjl06rec.tjl06Rec.toString().trim().isEmpty()) {
			List<String> hosbensCvgList = createList(tjl06rec.hosben);
			List<String> ctablesCvgList = createList(tjl06rec.ctable);
			if(crtableSuminsMap!=null && !crtableSuminsMap.isEmpty())
				for(Map.Entry<String, BigDecimal> crtableSumins: crtableSuminsMap.entrySet())
					performCovgChecks(hosbensCvgList, ctablesCvgList, crtableSumins);
			lincpfHelper = validateSumInsured(lincpfHelper);
			lincpfHelper.setCdateduration(tjl06rec.cdateduration.toInt());
			lincpfHelper.setJuvenileage(tjl06rec.juvenileage.toInt());
		}
	}
	
	public void init() {
		covtItemSumin = BigDecimal.ZERO;
		hosbensItemSumin = BigDecimal.ZERO;
		sumInsured = BigDecimal.ZERO;
		hospitalBenfitSumInsured = BigDecimal.ZERO;
		covtSuminsList = new ArrayList<>();
		hosbensSuminsList = new ArrayList<>();
	}
	
	public void readTables(LincpfHelper lincpfHelper) {
		Itempf itempf;
		itempf = itempfDAO.readItemItem(ITEMPFX, lincpfHelper.getChdrcoy(), TH605, lincpfHelper.getChdrcoy());
		if(itempf != null)
			th605rec.th605Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		itempf = itempfDAO.readItdmpf(ITEMPFX, lincpfHelper.getChdrcoy(), TJL06, 
				lincpfHelper.getTransactionDate(), lincpfHelper.getTranscd());
		if(itempf != null)
			tjl06rec.tjl06Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		else {
			itempf = itempfDAO.readItdmpf(ITEMPFX, lincpfHelper.getChdrcoy(), TJL06, lincpfHelper.getTransactionDate(), "****");
			if(itempf != null)
				tjl06rec.tjl06Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		if(!tjl06rec.tjl06Rec.toString().trim().isEmpty()) {
			if(lincpfHelper.getLifeage() >= tjl06rec.juvenileage.toInt())
				covtItemSumin = tjl06rec.minsumin.getbigdata();
			else
				covtItemSumin = tjl06rec.minsum.getbigdata();
			hosbensItemSumin = tjl06rec.minDhb.getbigdata();
		}
	}
	
	public List<String> createList(FixedLengthStringData[] input) {
		List<String> output = new ArrayList<>();
		for(int i=1; i<input.length; i++) {
			if(!input[i].toString().trim().isEmpty())
				output.add(input[i].toString());
			else
				break;
		}
		return output;
	}
	
	public void performCovgChecks(List<String> hosbensCvgList, List<String> ctablesCvgList, 
			Map.Entry<String, BigDecimal> crtableSumins){
		if(hosbensCvgList.contains(crtableSumins.getKey()))
			if(crtableSumins.getValue().compareTo(hosbensItemSumin) >= 0)
				hospitalBenfitSumInsured = hospitalBenfitSumInsured.add(crtableSumins.getValue());
			else
				hosbensSuminsList.add(crtableSumins.getValue());
		else if(ctablesCvgList.contains(crtableSumins.getKey()))
			if (crtableSumins.getValue().compareTo(covtItemSumin) >= 0)
				sumInsured = sumInsured.add(crtableSumins.getValue());
			else
				covtSuminsList.add(crtableSumins.getValue());
	}
	
	public LincpfHelper validateSumInsured(LincpfHelper lincpfHelper) {
		if(!covtSuminsList.isEmpty())
			sumInsured = sumInsured.add(covtSuminsList.stream().reduce(BigDecimal.ZERO, BigDecimal::add));
		if(!hosbensSuminsList.isEmpty())
			hospitalBenfitSumInsured = hospitalBenfitSumInsured.add(
					hosbensSuminsList.stream().reduce(BigDecimal.ZERO, BigDecimal::add));
		if((sumInsured.compareTo(BigDecimal.ZERO) > 0 && sumInsured.compareTo(covtItemSumin) >= 0) 
				|| ((hospitalBenfitSumInsured.compareTo(BigDecimal.ZERO) > 0) 
						&& hospitalBenfitSumInsured.compareTo(hosbensItemSumin) >= 0))
			lincpfHelper.setCritSatisfied(true);
		lincpfHelper.setSumInsured(sumInsured);
		lincpfHelper.setHosbensSI(hospitalBenfitSumInsured);
		return lincpfHelper;
	}
	
	public Lincpf setupLincpf(LincpfHelper lincpfHelper, Lincpf lincpf) {
		if(lincpf == null)
			this.lincpf = new Lincpf();
		else
			this.lincpf = lincpf;
		flag = false;
		setLifeDetails(lincpfHelper);
		setClientDetails(lincpfHelper);
		setRemainingDetails(lincpfHelper);
		return this.lincpf;
	}
	
	public void setLifeDetails(LincpfHelper lincpfHelper) {
		lincpf.setLifcnum(lincpfHelper.getLifcnum().trim());
		lincpf.setZclntdob(lincpfHelper.getLcltdob());
		if("M".equals(lincpfHelper.getLcltsex()))
			lincpf.setCltsex("1");
		else if("F".equals(lincpfHelper.getLcltsex()))
			lincpf.setCltsex("2");
		setLifeClntDetails(lincpfHelper);
		if(lincpfHelper.getCownnum().equals(lincpfHelper.getLifcnum()))
			flag = true;
	}
	
	public void setLifeClntDetails(LincpfHelper lincpfHelper) {
		address = new StringBuilder();
		kanaName = new StringBuilder();
		kanjiName = new StringBuilder();
		getLifeNameAddress(lincpfHelper);
		if(!address.toString().trim().isEmpty())
			lincpf.setAddrss(address.toString());
		if(kanaName.toString().trim().length() >= lifeKanaNameLength)
			lincpf.setKjhcltnam(kanaName.toString().trim().substring(0, lifeKanaNameLength));
		else
			lincpf.setKjhcltnam(kanaName.toString().trim());
		if(kanjiName.toString().trim().length() >= kanjiNameLength)
			lincpf.setLifnamkj(kanjiName.toString().trim().substring(0, kanjiNameLength));
		else
			lincpf.setLifnamkj(kanjiName.toString().trim());
	}
	
	public void setClientDetails(LincpfHelper lincpfHelper) {
		address = new StringBuilder();
		kanaName = new StringBuilder();
		kanjiName = new StringBuilder();
		getClientNameAddress(lincpfHelper);
		if("M".equals(lincpfHelper.getCltsex()))
			lincpf.setZcontsex("1");
		else if("F".equals(lincpfHelper.getCltsex()))
			lincpf.setZcontsex("2");
		else if("C".equals(lincpfHelper.getClttype()))
			lincpf.setZcontsex("3");
		lincpf.setCownnum(lincpfHelper.getCownnum());
		if(!address.toString().trim().isEmpty())
			lincpf.setZcontaddr(address.toString());
		if(kanaName.toString().trim().length() >= clntKanaNameLength)
			lincpf.setZlownnam(kanaName.toString().trim().substring(0, clntKanaNameLength));
		else
			lincpf.setZlownnam(kanaName.toString().trim());
		if(kanjiName.toString().trim().length() >= kanjiNameLength)
			lincpf.setOwnnamkj(kanjiName.toString().trim().substring(0, kanjiNameLength));
		else
			lincpf.setOwnnamkj(kanjiName.toString().trim());
		lincpf.setZcontdob(lincpfHelper.getCltdob());
	}
	
	public void getClientNameAddress(LincpfHelper lincpfHelper) {
		if(lincpfHelper.getZkanaddr01() != null) 
			address.append(lincpfHelper.getZkanaddr01().substring(0, 30));
		if(lincpfHelper.getZkanasnm()!= null) 
			kanaName.append(lincpfHelper.getZkanasnm().trim()).append(SPACE);
		if(lincpfHelper.getZkanagnm()!=null) 
			kanaName.append(lincpfHelper.getZkanagnm().trim());
		if(lincpfHelper.getSurname()!= null)
			kanjiName.append(lincpfHelper.getSurname().trim()).append(SPACE);
		if(lincpfHelper.getGivname()!=null)
			kanjiName.append(lincpfHelper.getGivname().trim());
	}
	
	public void getLifeNameAddress(LincpfHelper lincpfHelper) {
		if(lincpfHelper.getLzkanaddr01() != null) 
			address.append(lincpfHelper.getLzkanaddr01().substring(0, 30));
		if(lincpfHelper.getLzkanasnm()!= null)
			kanaName.append(lincpfHelper.getLzkanasnm().trim()).append(SPACE);
		if(lincpfHelper.getLzkanagnm()!=null)
			kanaName.append(lincpfHelper.getLzkanagnm().trim());
		if(lincpfHelper.getLsurname()!= null)
			kanjiName.append(lincpfHelper.getLsurname().trim()).append(SPACE);
		if(lincpfHelper.getLgivname()!=null)
			kanjiName.append(lincpfHelper.getLgivname().trim());
	}
		
	public void setRemainingDetails(LincpfHelper lincpfHelper) {
		lincpf.setBatctrcde(lincpfHelper.getTranscd());
		lincpf.setZrqflag(StringUtils.leftPad(StringUtils.EMPTY, 1, ZERO));
		lincpf.setZrqsetdte(0);
		lincpf.setSenddate(0);
		lincpf.setRecveror(SPACE);
		if(flag)
			lincpf.setFlag("1");
		else
			lincpf.setFlag("0");
		lincpf.setKglcmpcd(th605rec.liacoy.toString());
		lincpf.setZhospbnf(BigDecimal.ZERO);
		lincpf.setZsickbnf(BigDecimal.ZERO);
		lincpf.setZcancbnf(BigDecimal.ZERO);
		lincpf.setZothsbnf(lincpfHelper.getHosbensSI());
		lincpf.setZcsumins(lincpfHelper.getSumInsured());
		lincpf.setChdrnum(lincpfHelper.getChdrnum());
		lincpf.setZhospopt(lincpfHelper.getChdrnum());
		lincpf.setZregdate(lincpfHelper.getTransactionDate());
		lincpf.setZlincdte(0);
		lincpf.setZrevdate(0);
		lincpf.setZasumins(BigDecimal.ZERO);
		lincpf.setZdethopt(lincpfHelper.getChdrnum());
		lincpf.setZrqcandte(0);
		lincpf.setZepflag(SPACE);
		lincpf.setZcvflag(SPACE);
	}
	
	public void setupCommonLinc(Lincpf lincpf, LincpfHelper lincpfHelper) {
		lincpf.setBatctrcde(lincpfHelper.getTranscd());
		lincpf.setZrsltcde(StringUtils.leftPad(StringUtils.EMPTY, 1, ZERO));
		lincpf.setZrsltrsn(StringUtils.leftPad(StringUtils.EMPTY, 2, ZERO));
		lincpf.setZerrorno(StringUtils.leftPad(StringUtils.EMPTY, 3, ZERO));
		lincpf.setZnofrecs(StringUtils.leftPad(StringUtils.EMPTY, 3, ZERO));
		lincpf.setHcltnam(SPACE);
		lincpf.setWflga(StringUtils.leftPad(StringUtils.EMPTY, 1, ZERO));
		lincpf.setZerrflg(StringUtils.leftPad(StringUtils.EMPTY, 1, ZERO));
		lincpf.setUsrprf(lincpfHelper.getUsrprf().toUpperCase());
		lincpf.setJobnm(lincpfHelper.getJobnm().toUpperCase());
	}
	
	public void setupLincDates(Lincpf lincpf, int effdate, int occdate, int dtetrmntd, 
			int ztrdate, int zadrddte, int zexdrddte) {
		lincpf.setZeffdate(effdate);
		lincpf.setZoccdate(occdate);
		lincpf.setZtrdate(ztrdate);
		lincpf.setDtetrmntd(dtetrmntd);
		lincpf.setZadrddte(zadrddte);
		lincpf.setZexdrddte(zexdrddte);
	}
		
	public Slncpf setupSlncpf(Lincpf lincpf, Slncpf slncpf) {
		if(slncpf == null)
			slncpfRecord = new Slncpf();
		else
			slncpfRecord = slncpf;
		slncpfRecord.setZdatacls("2");
		slncpfRecord.setZreclass(lincpf.getZreclass());
		slncpfRecord.setZmodtype(lincpf.getZmodtype());
		slncpfRecord.setZrsltcde(lincpf.getZrsltcde());
		slncpfRecord.setZrsltrsn(lincpf.getZrsltrsn());
		slncpfRecord.setZerrorno(lincpf.getZerrorno());
		slncpfRecord.setZnofrecs(lincpf.getZnofrecs());
		slncpfRecord.setKlinckey(lincpf.getKlinckey());
		slncpfRecord.setHcltnam(lincpf.getHcltnam());
		slncpfRecord.setCltsex(lincpf.getCltsex());
		slncpfRecord.setZclntdob(lincpf.getZclntdob());
		slncpfRecord.setZeffdate(lincpf.getZeffdate());
		slncpfRecord.setFlag(lincpf.getFlag());
		slncpfRecord.setKglcmpcd(lincpf.getKglcmpcd());
		slncpfRecord.setAddrss(lincpf.getAddrss());
		slncpfRecord.setZhospbnf(lincpf.getZhospbnf());
		slncpfRecord.setZsickbnf(lincpf.getZsickbnf());
		slncpfRecord.setZcancbnf(lincpf.getZcancbnf());
		slncpfRecord.setZothsbnf(lincpf.getZothsbnf());
		slncpfRecord.setZregdate(lincpf.getZregdate());
		slncpfRecord.setZlincdte(lincpf.getZlincdte());
		slncpfRecord.setKjhcltnam(lincpf.getKjhcltnam());
		slncpfRecord.setZhospopt(lincpf.getZhospopt());
		slncpfRecord.setZrevdate(lincpf.getZrevdate());
		slncpfRecord.setZlownnam(lincpf.getZlownnam());
		slncpfRecord.setZcsumins(lincpf.getZcsumins());
		slncpfRecord.setZasumins(lincpf.getZasumins());
		slncpfRecord.setZdethopt(lincpf.getZdethopt());
		slncpfRecord.setZrqflag(lincpf.getZrqflag());
		slncpfRecord.setZrqsetdte(lincpf.getZrqsetdte());
		slncpfRecord.setZrqcandte(lincpf.getZrqcandte());
		slncpfRecord.setZepflag(lincpf.getZepflag());
		slncpfRecord.setZcvflag(lincpf.getZcvflag());
		slncpfRecord.setZcontsex(lincpf.getZcontsex());
		slncpfRecord.setZcontdob(lincpf.getZcontdob());
		slncpfRecord.setZcontaddr(lincpf.getZcontaddr());
		slncpfRecord.setLifnamkj(lincpf.getLifnamkj());
		slncpfRecord.setOwnnamkj(lincpf.getOwnnamkj());
		slncpfRecord.setWflga(lincpf.getWflga());
		slncpfRecord.setZerrflg(lincpf.getZerrflg());
		slncpfRecord.setZoccdate(lincpf.getZoccdate());
		slncpfRecord.setZtrdate(lincpf.getZtrdate());
		slncpfRecord.setZadrddte(lincpf.getZadrddte());
		slncpfRecord.setZexdrddte(lincpf.getZexdrddte());
		slncpfRecord.setUsrprf(lincpf.getUsrprf());
		slncpfRecord.setJobnm(lincpf.getJobnm());
		return slncpfRecord;
	}
	
	public void generateFollowUp(LincpfHelper lincpfHelper) {
		List<Fluppf> fluppfList;
		Itempf itempf;
		fluppf = null;
		fluppfList = fluppfDAO.searchFlupRecordByChdrNum(lincpfHelper.getChdrcoy(), 
				lincpfHelper.getChdrnum());
		descpf=descpfDAO.getdescData(ITEMPFX, T5661, 
			 lincpfHelper.getLanguage()+tjl06rec.fupcde01.toString(), 
			 lincpfHelper.getChdrcoy(), lincpfHelper.getLanguage());
		itempf=itempfDAO.readItemItem(ITEMPFX, lincpfHelper.getChdrcoy(), 
			 T5661, lincpfHelper.getLanguage()+tjl06rec.fupcde01.toString());
		if(itempf!=null)
			t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		if(!checkFlupFound(fluppfList)) {
			Fluppf existingFluppf = null;
			if(!fluppfList.isEmpty()) {
				int latestRec = fluppfList.size()-1;
				existingFluppf = fluppfList.get(latestRec);
			}
			setupFluppf(existingFluppf, tjl06rec.fupcde01.toString().trim(), t5661rec.fupstat.trim().charAt(0), lincpfHelper);
		}
		if(fluppf != null) 
			fluppfDAO.insertFlupRecord(fluppf);
	}
	
	protected boolean checkFlupFound(List<Fluppf> fluppfList) {
		 boolean flupFound = false;
		 List<Character> fupCompleteStatusList = new ArrayList<>();
		 fupCompleteStatusList = getFollowUpCompleteStatuses(fupCompleteStatusList);
		 if(!fluppfList.isEmpty()) {
			 for(Fluppf flupItem: fluppfList) {
				 if(flupItem.getFupCde().equals(tjl06rec.fupcde01.toString().trim())
						 && fupCompleteStatusList != null && !fupCompleteStatusList.contains(flupItem.getFupSts())) {
					 flupFound = true;
					 break;
				 }
			 }
		 }
		 return flupFound;
	 }
	
	protected List<Character> getFollowUpCompleteStatuses(List<Character> fupCompleteStatusList){
		for(int i=1;i<=10;i++) {
			if(!t5661rec.fuposs[i].trim().isEmpty())
				fupCompleteStatusList.add(t5661rec.fuposs[i].trim().charAt(0));
			else
				break;
		}
		return fupCompleteStatusList;
	}
	 
	 protected Fluppf setupFluppf(Fluppf flupItem, String fupcde, Character fupstat, LincpfHelper lincpfHelper) {
		 fluppf = new Fluppf();
		 fluppf.setChdrcoy(lincpfHelper.getChdrcoy().charAt(0));
		 fluppf.setChdrnum(lincpfHelper.getChdrnum());
		 if(flupItem != null)
			 fluppf.setFupNo(flupItem.getFupNo()+1);
		 else
			 fluppf.setFupNo(1);
		 fluppf.setTranNo(lincpfHelper.getTranno());
		 fluppf.setFupTyp('P');
		 fluppf.setLife("01");
		 fluppf.setjLife("00");
		 fluppf.setFupCde(fupcde);
		 fluppf.setFupSts(fupstat);
		 fluppf.setFupDt(lincpfHelper.getTransactionDate());
		 fluppf.setFupRmk(descpf.getLongdesc());
		 fluppf.setClamNum(SPACE);
		 fluppf.setUserT(Integer.parseInt(lincpfHelper.getUsrprf()));
		 fluppf.setTermId(lincpfHelper.getTermId());
		 fluppf.setTrdt(lincpfHelper.getTrdt());
		 fluppf.setTrtm(lincpfHelper.getTrtm());
		 fluppf.setzAutoInd('Y');
		 fluppf.setUsrPrf(lincpfHelper.getUsrprf());
		 fluppf.setJobNm(lincpfHelper.getJobnm());
		 fluppf.setEffDate(lincpfHelper.getTransactionDate());
		 fluppf.setCrtUser(lincpfHelper.getUsrprf());
		 fluppf.setCrtDate(lincpfHelper.getTransactionDate());
		 fluppf.setLstUpUser(lincpfHelper.getUsrprf());
		 fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
		 fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
		 fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
		 return fluppf;
	 }
	 	 
	@Override
	public String validateLincRecord(String chdrnum) {
		 String jl61 = "JL61";
		 String jl27 = "JL27";
		 Lincpf lincpfRecord = lincpfDAO.readLincpfData(chdrnum.trim());
		 if(lincpfRecord!=null) {
			 if(lincpfRecord.getSenddate() == 0 || lincpfRecord.getRecvdate() == 0)
				 return jl61;
			 else if(lincpfRecord.getSenddate() < lincpfRecord.getRecvdate() 
					 && !StringUtils.isEmpty(lincpfRecord.getRecveror().trim()))
				 return jl27;
		}
		return null;
	}
	
	@Override
	public boolean validateDataForUpdate(Lincpf lincpf, LincpfHelper lincpfHelper) {
		if(lincpf != null) {
			if(!lincpf.getCownnum().equals(lincpfHelper.getCownnum()) 
					|| !lincpf.getLifcnum().equals(lincpfHelper.getLifcnum())) {
				return true;
			}
			if(!checkLifeChanges(lincpf, lincpfHelper) 
					&& !checkClientChanges(lincpf, lincpfHelper)
					&& !checkOtherChanges(lincpf, lincpfHelper))
				return false;
		}
		return true;
	 }
	
	public boolean checkLifeChanges(Lincpf lincpf, LincpfHelper lincpfHelper) {
		boolean isNameChanged = false;
		address = new StringBuilder();
		kanaName = new StringBuilder();
		kanjiName = new StringBuilder();
		getLifeNameAddress(lincpfHelper);
		if(lincpf.getAddrss() != null && !lincpf.getAddrss().equals(address.toString()))
			return true;
		if(lincpf.getKjhcltnam() != null && lincpf.getLifnamkj() != null)
			isNameChanged = checkNameChange(lincpf.getKjhcltnam(), lincpf.getLifnamkj(), lifeKanaNameLength);
		if(isNameChanged || (lincpf.getZclntdob() != lincpfHelper.getLcltdob())
				|| (checkGender(lincpf.getCltsex(), lincpfHelper.getLcltsex())))
			return true;
		return false;
	}
	
	public boolean checkNameChange(String lincKanaName, String lincKanjiName, 
			int length) {
		if((kanaName.toString().trim().length() >= length && !lincKanaName.trim().equals(kanaName.substring(0, length)))
				|| (kanaName.toString().trim().length() < length && !lincKanaName.trim().equals((kanaName.toString().trim()))))
			return true;
		if((kanjiName.toString().trim().length() >= kanjiNameLength 
				&& !lincKanjiName.trim().equals(kanjiName.substring(0, kanjiNameLength)))
				|| (kanjiName.toString().trim().length() < kanjiNameLength 
						&& !lincKanjiName.trim().equals((kanjiName.toString().trim()))))
			return true;
		return false;
	}
	
	public boolean checkClientChanges(Lincpf lincpf, LincpfHelper lincpfHelper) {
		boolean isNameChanged = false;
		address = new StringBuilder();
		kanaName = new StringBuilder();
		kanjiName = new StringBuilder();
		getClientNameAddress(lincpfHelper);
		if(lincpf.getZcontaddr() != null && !lincpf.getZcontaddr().equals(address.toString()))
			return true;
		if(lincpf.getKjhcltnam() != null && lincpf.getLifnamkj() != null)
			isNameChanged = checkNameChange(lincpf.getZlownnam(), lincpf.getOwnnamkj(), clntKanaNameLength);
		if(isNameChanged || (lincpf.getZcontdob() != lincpfHelper.getCltdob())
				|| (checkGender(lincpf.getCltsex(), lincpfHelper.getLcltsex())))
			return true;
		return false;
	}
	
	public boolean checkGender(String lincpfGender, String lincpfHelperGender) {
		if(("1".equals(lincpfGender) && !("M").equals(lincpfHelperGender)) 
				|| ("2".equals(lincpfGender) && !("F").equals(lincpfHelperGender)))
			return true;
		return false;
	}
	
	public boolean checkOtherChanges(Lincpf lincpf, LincpfHelper lincpfHelper) {
		if((lincpf.getZothsbnf().compareTo(lincpfHelper.getHosbensSI()) != 0)
				|| lincpf.getZcsumins().compareTo(lincpfHelper.getSumInsured()) != 0)
			return true;
		if(lincpf.getZregdate() != lincpfHelper.getTransactionDate())
			return true;
		return false;
	}
}
