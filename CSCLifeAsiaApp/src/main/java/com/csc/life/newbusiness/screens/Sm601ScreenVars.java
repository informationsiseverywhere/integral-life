package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SM601
 * @version 1.0 generated on 30/08/09 07:07
 * @author Quipoz
 */
public class Sm601ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1008);
	public FixedLengthStringData dataFields = new FixedLengthStringData(464).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData genlcdexs = new FixedLengthStringData(420).isAPartOf(dataFields, 1);
	public FixedLengthStringData[] genlcdex = FLSArrayPartOfStructure(30, 14, genlcdexs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(420).isAPartOf(genlcdexs, 0, FILLER_REDEFINE);
	public FixedLengthStringData genlcdex01 = DD.genlcdex.copy().isAPartOf(filler,0);
	public FixedLengthStringData genlcdex02 = DD.genlcdex.copy().isAPartOf(filler,14);
	public FixedLengthStringData genlcdex03 = DD.genlcdex.copy().isAPartOf(filler,28);
	public FixedLengthStringData genlcdex04 = DD.genlcdex.copy().isAPartOf(filler,42);
	public FixedLengthStringData genlcdex05 = DD.genlcdex.copy().isAPartOf(filler,56);
	public FixedLengthStringData genlcdex06 = DD.genlcdex.copy().isAPartOf(filler,70);
	public FixedLengthStringData genlcdex07 = DD.genlcdex.copy().isAPartOf(filler,84);
	public FixedLengthStringData genlcdex08 = DD.genlcdex.copy().isAPartOf(filler,98);
	public FixedLengthStringData genlcdex09 = DD.genlcdex.copy().isAPartOf(filler,112);
	public FixedLengthStringData genlcdex10 = DD.genlcdex.copy().isAPartOf(filler,126);
	public FixedLengthStringData genlcdex11 = DD.genlcdex.copy().isAPartOf(filler,140);
	public FixedLengthStringData genlcdex12 = DD.genlcdex.copy().isAPartOf(filler,154);
	public FixedLengthStringData genlcdex13 = DD.genlcdex.copy().isAPartOf(filler,168);
	public FixedLengthStringData genlcdex14 = DD.genlcdex.copy().isAPartOf(filler,182);
	public FixedLengthStringData genlcdex15 = DD.genlcdex.copy().isAPartOf(filler,196);
	public FixedLengthStringData genlcdex16 = DD.genlcdex.copy().isAPartOf(filler,210);
	public FixedLengthStringData genlcdex17 = DD.genlcdex.copy().isAPartOf(filler,224);
	public FixedLengthStringData genlcdex18 = DD.genlcdex.copy().isAPartOf(filler,238);
	public FixedLengthStringData genlcdex19 = DD.genlcdex.copy().isAPartOf(filler,252);
	public FixedLengthStringData genlcdex20 = DD.genlcdex.copy().isAPartOf(filler,266);
	public FixedLengthStringData genlcdex21 = DD.genlcdex.copy().isAPartOf(filler,280);
	public FixedLengthStringData genlcdex22 = DD.genlcdex.copy().isAPartOf(filler,294);
	public FixedLengthStringData genlcdex23 = DD.genlcdex.copy().isAPartOf(filler,308);
	public FixedLengthStringData genlcdex24 = DD.genlcdex.copy().isAPartOf(filler,322);
	public FixedLengthStringData genlcdex25 = DD.genlcdex.copy().isAPartOf(filler,336);
	public FixedLengthStringData genlcdex26 = DD.genlcdex.copy().isAPartOf(filler,350);
	public FixedLengthStringData genlcdex27 = DD.genlcdex.copy().isAPartOf(filler,364);
	public FixedLengthStringData genlcdex28 = DD.genlcdex.copy().isAPartOf(filler,378);
	public FixedLengthStringData genlcdex29 = DD.genlcdex.copy().isAPartOf(filler,392);
	public FixedLengthStringData genlcdex30 = DD.genlcdex.copy().isAPartOf(filler,406);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,421);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,429);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,459);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(136).isAPartOf(dataArea, 464);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData genlcdexsErr = new FixedLengthStringData(120).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] genlcdexErr = FLSArrayPartOfStructure(30, 4, genlcdexsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(120).isAPartOf(genlcdexsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData genlcdex01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData genlcdex02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData genlcdex03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData genlcdex04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData genlcdex05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData genlcdex06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData genlcdex07Err = new FixedLengthStringData(4).isAPartOf(filler1, 24);
	public FixedLengthStringData genlcdex08Err = new FixedLengthStringData(4).isAPartOf(filler1, 28);
	public FixedLengthStringData genlcdex09Err = new FixedLengthStringData(4).isAPartOf(filler1, 32);
	public FixedLengthStringData genlcdex10Err = new FixedLengthStringData(4).isAPartOf(filler1, 36);
	public FixedLengthStringData genlcdex11Err = new FixedLengthStringData(4).isAPartOf(filler1, 40);
	public FixedLengthStringData genlcdex12Err = new FixedLengthStringData(4).isAPartOf(filler1, 44);
	public FixedLengthStringData genlcdex13Err = new FixedLengthStringData(4).isAPartOf(filler1, 48);
	public FixedLengthStringData genlcdex14Err = new FixedLengthStringData(4).isAPartOf(filler1, 52);
	public FixedLengthStringData genlcdex15Err = new FixedLengthStringData(4).isAPartOf(filler1, 56);
	public FixedLengthStringData genlcdex16Err = new FixedLengthStringData(4).isAPartOf(filler1, 60);
	public FixedLengthStringData genlcdex17Err = new FixedLengthStringData(4).isAPartOf(filler1, 64);
	public FixedLengthStringData genlcdex18Err = new FixedLengthStringData(4).isAPartOf(filler1, 68);
	public FixedLengthStringData genlcdex19Err = new FixedLengthStringData(4).isAPartOf(filler1, 72);
	public FixedLengthStringData genlcdex20Err = new FixedLengthStringData(4).isAPartOf(filler1, 76);
	public FixedLengthStringData genlcdex21Err = new FixedLengthStringData(4).isAPartOf(filler1, 80);
	public FixedLengthStringData genlcdex22Err = new FixedLengthStringData(4).isAPartOf(filler1, 84);
	public FixedLengthStringData genlcdex23Err = new FixedLengthStringData(4).isAPartOf(filler1, 88);
	public FixedLengthStringData genlcdex24Err = new FixedLengthStringData(4).isAPartOf(filler1, 92);
	public FixedLengthStringData genlcdex25Err = new FixedLengthStringData(4).isAPartOf(filler1, 96);
	public FixedLengthStringData genlcdex26Err = new FixedLengthStringData(4).isAPartOf(filler1, 100);
	public FixedLengthStringData genlcdex27Err = new FixedLengthStringData(4).isAPartOf(filler1, 104);
	public FixedLengthStringData genlcdex28Err = new FixedLengthStringData(4).isAPartOf(filler1, 108);
	public FixedLengthStringData genlcdex29Err = new FixedLengthStringData(4).isAPartOf(filler1, 112);
	public FixedLengthStringData genlcdex30Err = new FixedLengthStringData(4).isAPartOf(filler1, 116);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(408).isAPartOf(dataArea, 600);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData genlcdexsOut = new FixedLengthStringData(360).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] genlcdexOut = FLSArrayPartOfStructure(30, 12, genlcdexsOut, 0);
	public FixedLengthStringData[][] genlcdexO = FLSDArrayPartOfArrayStructure(12, 1, genlcdexOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(360).isAPartOf(genlcdexsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] genlcdex01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] genlcdex02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] genlcdex03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] genlcdex04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] genlcdex05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] genlcdex06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] genlcdex07Out = FLSArrayPartOfStructure(12, 1, filler2, 72);
	public FixedLengthStringData[] genlcdex08Out = FLSArrayPartOfStructure(12, 1, filler2, 84);
	public FixedLengthStringData[] genlcdex09Out = FLSArrayPartOfStructure(12, 1, filler2, 96);
	public FixedLengthStringData[] genlcdex10Out = FLSArrayPartOfStructure(12, 1, filler2, 108);
	public FixedLengthStringData[] genlcdex11Out = FLSArrayPartOfStructure(12, 1, filler2, 120);
	public FixedLengthStringData[] genlcdex12Out = FLSArrayPartOfStructure(12, 1, filler2, 132);
	public FixedLengthStringData[] genlcdex13Out = FLSArrayPartOfStructure(12, 1, filler2, 144);
	public FixedLengthStringData[] genlcdex14Out = FLSArrayPartOfStructure(12, 1, filler2, 156);
	public FixedLengthStringData[] genlcdex15Out = FLSArrayPartOfStructure(12, 1, filler2, 168);
	public FixedLengthStringData[] genlcdex16Out = FLSArrayPartOfStructure(12, 1, filler2, 180);
	public FixedLengthStringData[] genlcdex17Out = FLSArrayPartOfStructure(12, 1, filler2, 192);
	public FixedLengthStringData[] genlcdex18Out = FLSArrayPartOfStructure(12, 1, filler2, 204);
	public FixedLengthStringData[] genlcdex19Out = FLSArrayPartOfStructure(12, 1, filler2, 216);
	public FixedLengthStringData[] genlcdex20Out = FLSArrayPartOfStructure(12, 1, filler2, 228);
	public FixedLengthStringData[] genlcdex21Out = FLSArrayPartOfStructure(12, 1, filler2, 240);
	public FixedLengthStringData[] genlcdex22Out = FLSArrayPartOfStructure(12, 1, filler2, 252);
	public FixedLengthStringData[] genlcdex23Out = FLSArrayPartOfStructure(12, 1, filler2, 264);
	public FixedLengthStringData[] genlcdex24Out = FLSArrayPartOfStructure(12, 1, filler2, 276);
	public FixedLengthStringData[] genlcdex25Out = FLSArrayPartOfStructure(12, 1, filler2, 288);
	public FixedLengthStringData[] genlcdex26Out = FLSArrayPartOfStructure(12, 1, filler2, 300);
	public FixedLengthStringData[] genlcdex27Out = FLSArrayPartOfStructure(12, 1, filler2, 312);
	public FixedLengthStringData[] genlcdex28Out = FLSArrayPartOfStructure(12, 1, filler2, 324);
	public FixedLengthStringData[] genlcdex29Out = FLSArrayPartOfStructure(12, 1, filler2, 336);
	public FixedLengthStringData[] genlcdex30Out = FLSArrayPartOfStructure(12, 1, filler2, 348);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sm601screenWritten = new LongData(0);
	public LongData Sm601protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sm601ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, genlcdex03, genlcdex04, genlcdex02, genlcdex01, genlcdex05, genlcdex06, genlcdex09, genlcdex10, genlcdex07, genlcdex08, genlcdex11, genlcdex12, genlcdex13, genlcdex14, genlcdex15, genlcdex16, genlcdex17, genlcdex18, genlcdex19, genlcdex20, genlcdex21, genlcdex22, genlcdex23, genlcdex24, genlcdex25, genlcdex26, genlcdex27, genlcdex28, genlcdex29, genlcdex30};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, genlcdex03Out, genlcdex04Out, genlcdex02Out, genlcdex01Out, genlcdex05Out, genlcdex06Out, genlcdex09Out, genlcdex10Out, genlcdex07Out, genlcdex08Out, genlcdex11Out, genlcdex12Out, genlcdex13Out, genlcdex14Out, genlcdex15Out, genlcdex16Out, genlcdex17Out, genlcdex18Out, genlcdex19Out, genlcdex20Out, genlcdex21Out, genlcdex22Out, genlcdex23Out, genlcdex24Out, genlcdex25Out, genlcdex26Out, genlcdex27Out, genlcdex28Out, genlcdex29Out, genlcdex30Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, genlcdex03Err, genlcdex04Err, genlcdex02Err, genlcdex01Err, genlcdex05Err, genlcdex06Err, genlcdex09Err, genlcdex10Err, genlcdex07Err, genlcdex08Err, genlcdex11Err, genlcdex12Err, genlcdex13Err, genlcdex14Err, genlcdex15Err, genlcdex16Err, genlcdex17Err, genlcdex18Err, genlcdex19Err, genlcdex20Err, genlcdex21Err, genlcdex22Err, genlcdex23Err, genlcdex24Err, genlcdex25Err, genlcdex26Err, genlcdex27Err, genlcdex28Err, genlcdex29Err, genlcdex30Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sm601screen.class;
		protectRecord = Sm601protect.class;
	}

}
