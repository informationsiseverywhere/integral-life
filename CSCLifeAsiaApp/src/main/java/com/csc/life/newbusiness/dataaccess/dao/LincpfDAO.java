package com.csc.life.newbusiness.dataaccess.dao;

import com.csc.life.newbusiness.dataaccess.model.Lincpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

/**
 * @author gsaluja2
 *
 */
public interface LincpfDAO extends BaseDAO<Lincpf>{
	public Lincpf readLincpfData(String chdrnum);
	public void insertLincData(Lincpf lincpf);
	public void updateLincData(Lincpf lincpf);
}
