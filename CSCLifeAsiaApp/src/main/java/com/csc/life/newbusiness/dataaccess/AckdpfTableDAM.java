package com.csc.life.newbusiness.dataaccess;

import com.quipoz.framework.datatype.*;
import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;


/**
 * 	
 * File: AckdpfTableDAM.java
 * Date: Fri April 17 2020
 * Class transformed from ACKDPF
 * Author: Sdavis88
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AckdpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 53;
	public FixedLengthStringData ackdec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData ackdpfRecord = ackdec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(ackdec);//1
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(ackdec);//8
	public FixedLengthStringData dlvrmode = DD.dlvrmode.copy().isAPartOf(ackdec);//4
	public PackedDecimalData despdate = DD.despdate.copy().isAPartOf(ackdec);//8
	public PackedDecimalData packdate = DD.packdate.copy().isAPartOf(ackdec);//8
	public PackedDecimalData deemdate = DD.deemdate.copy().isAPartOf(ackdec);//8
	public PackedDecimalData nextActDate = DD.nxtdte.copy().isAPartOf(ackdec);//8
	public PackedDecimalData hpaduqn = DD.unique_number.copy().isAPartOf(ackdec);
	
	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AckdpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for AckdpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AckdpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AckdpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AckdpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AckdpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AckdpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ACKDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"DLVRMODE, " +
							"DESPDATE, " +
							"PACKDATE, " +
							"DEEMDATE, " +
							"NXTDTE, " +
							"HPADUQN,"+
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     dlvrmode,
                                     despdate,
                                     packdate,
                                     deemdate,
                                     nextActDate,
                                     hpaduqn,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		dlvrmode.clear();
        despdate.clear();
        packdate.clear();
        deemdate.clear();
        nextActDate.clear();
        hpaduqn.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getackdrec() {
  		return ackdec;
	}

	public FixedLengthStringData getAckdpfRecord() {
  		return ackdpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setackdec(what);
	}

	public void setackdec(Object what) {
  		this.ackdec.set(what);
	}

	public void setMatypfRecord(Object what) {
  		this.ackdpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(ackdec.getLength());
		result.set(ackdec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}