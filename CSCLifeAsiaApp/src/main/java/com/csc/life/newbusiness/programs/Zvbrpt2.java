package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.csv.CSVGenerator;
import com.csc.fsu.clients.dataaccess.ClntTableDAM;

import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.tablestructures.Zvbrptrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
 * This subroutine is required to produce the report file(s) to be sent to the Muamalat bank.
 * 
 */

public class Zvbrpt2  extends SMARTCodeModel{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Zvbrpt2.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Zvbrptrec zvbrptrec = new Zvbrptrec();
    private FixedLengthStringData virtualAccountNo =new FixedLengthStringData(18);
    //TMLII-2300 START
//	FixedLengthStringData clintName= new FixedLengthStringData(30);
    String clientName = "";
    //END
    String issuebank ="";
    List<String> reportlist=new ArrayList<String>();
    ClntTableDAM clntIO= new ClntTableDAM();
    ChdrlnbTableDAM chdrIO= new ChdrlnbTableDAM();
    private String chdrrec = "CHDRREC";
    private String clntrec = "CLNTREC";
    private FixedLengthStringData statuz = new FixedLengthStringData(5);
    String fileName="";
	String prefix="";
	String detailFormat="";
	private FixedLengthStringData datime = new FixedLengthStringData(14);
	private FixedLengthStringData date = new FixedLengthStringData(8).isAPartOf(datime,0);
	private FixedLengthStringData time = new FixedLengthStringData(6).isAPartOf(datime,8);
    long sinstamnt;
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
		}

		/**
		* The mainline method is the default entry point of the program when called by other programs using the
		* Quipoz runtime framework.
		*/
	public void mainline(Object... parmArray)
		{
		datime = convertAndSetParam(datime, parmArray, 2);
		statuz = convertAndSetParam(statuz, parmArray, 1);
		zvbrptrec.zvbrptrec = convertAndSetParam(zvbrptrec.zvbrptrec, parmArray, 0);
			try {
				initialiseSection();
			}
			catch (COBOLExitProgramException e) {
				LOGGER.debug("Flow Ended");
			}
		}

	protected void initialiseSection()
	{
		try {

			process();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
			
		}
		finally{
			exit090();
		}
		
	}
	
	protected void process()
	{
		zvbrptrec.statuz.set(Varcom.oK);
		issuebank = zvbrptrec.issuebnk.trim();		 		
		if(isEQ(statuz,"CLOSE"))
		{
			printTextFile();
			return;
		}
		if(isNE(zvbrptrec.chdrnum,SPACE))
		{		
			//TMLII-1159 FT team asked us to change the 0000 with x000.
			//virtualAccountNo.set(issuebank+"0000"+zvbrptrec.chdrnum);
			virtualAccountNo.set(issuebank+"x000"+zvbrptrec.chdrnum);
			//TMLII-1839 Ticket ask us to change the x000 with 0100.
			virtualAccountNo.set(issuebank+"0100"+zvbrptrec.chdrnum);
			zvbrptrec.zvrtbnkac.set(virtualAccountNo);
				if(isEQ(statuz,"HEADR"))
				{
					writeHeader();
				}
				else if(isEQ(statuz,"DTAIL"))
				{
					writeDetailProcess();
				}
						
		}
	}	

	protected void writeHeader()
	{
	
		if(isEQ(zvbrptrec.rectype,"C"))
		{
			prefix="ADD";
		}
		else if(isEQ(zvbrptrec.rectype,"U"))
		{
			prefix="UPDATE";
		}
		else if(isEQ(zvbrptrec.rectype,"D"))
		{
			prefix="DELETE";
		}
		writeDetailProcess();
	}
	protected void writeDetailProcess()
	{

		if(isNE(zvbrptrec.rectype,"D")){
			chdrIO.setDataArea(SPACES);
			chdrIO.setChdrcoy(zvbrptrec.chdrcoy);
			chdrIO.setChdrnum(zvbrptrec.chdrnum);
			chdrIO.setFormat(chdrrec);
			chdrIO.setFunction(Varcom.readr);
			SmartFileCode.execute(appVars, chdrIO);
			if (isNE(chdrIO.getStatuz(), Varcom.oK)) {
				zvbrptrec.statuz.set(chdrIO.getStatuz());
				return;		
			}
			else{				
				clntIO.setDataArea(SPACES);
				clntIO.setClntnum(chdrIO.getCownnum());
				clntIO.setClntcoy(chdrIO.getCowncoy());
				clntIO.setClntpfx(chdrIO.getCownpfx());
				clntIO.setFormat(clntrec);
				clntIO.setFunction(Varcom.readr);
				SmartFileCode.execute(appVars, clntIO);
				if (isNE(clntIO.getStatuz(), Varcom.oK)) {
					zvbrptrec.statuz.set(clntIO.getStatuz());	
					return;
				}
			}
			//TMLII-2300 START
//			clintName.set(clntIO.getGivname().trim()+" "+clntIO.getSurname().trim());
			clientName = clntIO.getGivname().toString().trim()+ " " +clntIO.getSurname().toString().trim();
			//END
		}
		if(isEQ(zvbrptrec.rectype,"U") || isEQ(zvbrptrec.rectype,"C")){
			sinstamnt  = (long) (zvbrptrec.sinstamt.getbigdata().doubleValue()*100);
			//TMLII-2300 START
//			detailFormat= virtualAccountNo.trim()+","+clintName+","+sinstamnt+",premi asuransi";   //E.g.  1ABCDEABCDE00012345678  000000John Smith                    21062023IDR
			detailFormat= virtualAccountNo.trim()+";"+clientName+";"+sinstamnt+";Pembayaran Kontribusi";   //E.g.  1ABCDEABCDE00012345678  000000John Smith  21062023IDR
			//END
			reportlist.add(detailFormat);	
		}
		else if(isEQ(zvbrptrec.rectype,"D")) {					
			reportlist.add(virtualAccountNo.trim());	
		}	
	}

	 protected void printTextFile(){		 
		 fileName=prefix+"_"+zvbrptrec.issuebnk.trim()+"_"+date+"_"+time;
		 CSVGenerator.produceCSV(reportlist, fileName);		 
	 }


	protected void exit090()
	{
		exitProgram();
	}

	
}
