/*
 * File: Tm600pt.java
 * Date: 30 August 2009 2:38:21
 * Author: Quipoz Limited
 * 
 * Class transformed from TM600PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.newbusiness.tablestructures.Tm600rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TM600.
*
*
*****************************************************************
* </pre>
*/
public class Tm600pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(50).isAPartOf(wsaaPrtLine001, 26, FILLER).init("OCC Guidelines Percentage                    SM600");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(54);
	private FixedLengthStringData filler7 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 9, FILLER).init("Description :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine003, 24);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(30);
	private FixedLengthStringData filler9 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine004, 9, FILLER).init("OCC%        :");
	private ZonedDecimalData fieldNo006 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine004, 24).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(73);
	private FixedLengthStringData filler11 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine005, 9, FILLER).init("Banding Required :");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 28);
	private FixedLengthStringData filler13 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 29, FILLER).init(" From");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine005, 36).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 51, FILLER).init("   to");
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine005, 58).setPattern("ZZZZZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(48);
	private FixedLengthStringData filler15 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler16 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine006, 9, FILLER).init("GL Accounts for PREMIUM");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine006, 34);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(48);
	private FixedLengthStringData filler17 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler18 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine007, 9, FILLER).init("GL Accounts for ACTUAL");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine007, 34);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tm600rec tm600rec = new Tm600rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tm600pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tm600rec.tm600Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(tm600rec.genldesc);
		fieldNo006.set(tm600rec.occpct);
		fieldNo007.set(tm600rec.ynflag);
		fieldNo008.set(tm600rec.statfrom);
		fieldNo009.set(tm600rec.statto);
		fieldNo010.set(tm600rec.genlcdex);
		fieldNo011.set(tm600rec.genlcdey);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
