/*
 * File: Cnvt5537.java
 * Date: December 3, 2013 2:17:08 AM ICT
 * Author: CSC
 * 
 * Class transformed from CNVT5537.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.unitlinkedprocessing.tablestructures.T5537rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table T5537, Replace INSTPR with INSTPR 1,
*   add new fields INSTPR02 to INSTPR11.
*
*
****************************************************************** ****
* </pre>
*/
public class Cnvt5537 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVT5537");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String t5537 = "T5537";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5537rec t5537rec = new T5537rec();
	private Varcom varcom = new Varcom();
	private T5537RecOldInner t5537RecOldInner = new T5537RecOldInner();

	public Cnvt5537() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(t5537);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT T5537 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(), t5537)) {
			getAppVars().addDiagnostic("Table T5537 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itemIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT T5537 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteT55372080();
	}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		t5537RecOldInner.t5537RecOld.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		t5537rec.agecont.set(t5537RecOldInner.t5537AgecontOld);
		t5537rec.trmcont.set(t5537RecOldInner.t5537TrmcontOld);
		for (ix.set(1); !(isGT(ix, 90)); ix.add(1)){
			t5537rec.actAllocBasis[ix.toInt()].set(t5537RecOldInner.t5537ActAllocBasisOld[ix.toInt()]);
		}
		for (ix.set(1); !(isGT(ix, 10)); ix.add(1)){
			t5537rec.ageIssageTo[ix.toInt()].set(t5537RecOldInner.t5537ToageOld[ix.toInt()]);
		}
		for (ix.set(1); !(isGT(ix, 9)); ix.add(1)){
			t5537rec.toterm[ix.toInt()].set(t5537RecOldInner.t5537TotermOld[ix.toInt()]);
		}
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(t5537rec.t5537Rec);
	}

protected void rewriteT55372080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(), t5537)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for T5537");
		}
	}
/*
 * Class transformed  from Data Structure T5537-REC-OLD--INNER
 */
private static final class T5537RecOldInner { 

	private FixedLengthStringData t5537RecOld = new FixedLengthStringData(500);
	private FixedLengthStringData t5537ActAllocBasissOld = new FixedLengthStringData(360).isAPartOf(t5537RecOld, 0);
	private FixedLengthStringData[] t5537ActAllocBasisOld = FLSArrayPartOfStructure(90, 4, t5537ActAllocBasissOld, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(360).isAPartOf(t5537ActAllocBasissOld, 0, FILLER_REDEFINE);
	private FixedLengthStringData t5537ActAllocBasis01Old = new FixedLengthStringData(4).isAPartOf(filler, 0);
	private FixedLengthStringData t5537ActAllocBasis02Old = new FixedLengthStringData(4).isAPartOf(filler, 4);
	private FixedLengthStringData t5537ActAllocBasis03Old = new FixedLengthStringData(4).isAPartOf(filler, 8);
	private FixedLengthStringData t5537ActAllocBasis04Old = new FixedLengthStringData(4).isAPartOf(filler, 12);
	private FixedLengthStringData t5537ActAllocBasis05Old = new FixedLengthStringData(4).isAPartOf(filler, 16);
	private FixedLengthStringData t5537ActAllocBasis06Old = new FixedLengthStringData(4).isAPartOf(filler, 20);
	private FixedLengthStringData t5537ActAllocBasis07Old = new FixedLengthStringData(4).isAPartOf(filler, 24);
	private FixedLengthStringData t5537ActAllocBasis08Old = new FixedLengthStringData(4).isAPartOf(filler, 28);
	private FixedLengthStringData t5537ActAllocBasis09Old = new FixedLengthStringData(4).isAPartOf(filler, 32);
	private FixedLengthStringData t5537ActAllocBasis10Old = new FixedLengthStringData(4).isAPartOf(filler, 36);
	private FixedLengthStringData t5537ActAllocBasis11Old = new FixedLengthStringData(4).isAPartOf(filler, 40);
	private FixedLengthStringData t5537ActAllocBasis12Old = new FixedLengthStringData(4).isAPartOf(filler, 44);
	private FixedLengthStringData t5537ActAllocBasis13Old = new FixedLengthStringData(4).isAPartOf(filler, 48);
	private FixedLengthStringData t5537ActAllocBasis14Old = new FixedLengthStringData(4).isAPartOf(filler, 52);
	private FixedLengthStringData t5537ActAllocBasis15Old = new FixedLengthStringData(4).isAPartOf(filler, 56);
	private FixedLengthStringData t5537ActAllocBasis16Old = new FixedLengthStringData(4).isAPartOf(filler, 60);
	private FixedLengthStringData t5537ActAllocBasis17Old = new FixedLengthStringData(4).isAPartOf(filler, 64);
	private FixedLengthStringData t5537ActAllocBasis18Old = new FixedLengthStringData(4).isAPartOf(filler, 68);
	private FixedLengthStringData t5537ActAllocBasis19Old = new FixedLengthStringData(4).isAPartOf(filler, 72);
	private FixedLengthStringData t5537ActAllocBasis20Old = new FixedLengthStringData(4).isAPartOf(filler, 76);
	private FixedLengthStringData t5537ActAllocBasis21Old = new FixedLengthStringData(4).isAPartOf(filler, 80);
	private FixedLengthStringData t5537ActAllocBasis22Old = new FixedLengthStringData(4).isAPartOf(filler, 84);
	private FixedLengthStringData t5537ActAllocBasis23Old = new FixedLengthStringData(4).isAPartOf(filler, 88);
	private FixedLengthStringData t5537ActAllocBasis24Old = new FixedLengthStringData(4).isAPartOf(filler, 92);
	private FixedLengthStringData t5537ActAllocBasis25Old = new FixedLengthStringData(4).isAPartOf(filler, 96);
	private FixedLengthStringData t5537ActAllocBasis26Old = new FixedLengthStringData(4).isAPartOf(filler, 100);
	private FixedLengthStringData t5537ActAllocBasis27Old = new FixedLengthStringData(4).isAPartOf(filler, 104);
	private FixedLengthStringData t5537ActAllocBasis28Old = new FixedLengthStringData(4).isAPartOf(filler, 108);
	private FixedLengthStringData t5537ActAllocBasis29Old = new FixedLengthStringData(4).isAPartOf(filler, 112);
	private FixedLengthStringData t5537ActAllocBasis30Old = new FixedLengthStringData(4).isAPartOf(filler, 116);
	private FixedLengthStringData t5537ActAllocBasis31Old = new FixedLengthStringData(4).isAPartOf(filler, 120);
	private FixedLengthStringData t5537ActAllocBasis32Old = new FixedLengthStringData(4).isAPartOf(filler, 124);
	private FixedLengthStringData t5537ActAllocBasis33Old = new FixedLengthStringData(4).isAPartOf(filler, 128);
	private FixedLengthStringData t5537ActAllocBasis34Old = new FixedLengthStringData(4).isAPartOf(filler, 132);
	private FixedLengthStringData t5537ActAllocBasis35Old = new FixedLengthStringData(4).isAPartOf(filler, 136);
	private FixedLengthStringData t5537ActAllocBasis36Old = new FixedLengthStringData(4).isAPartOf(filler, 140);
	private FixedLengthStringData t5537ActAllocBasis37Old = new FixedLengthStringData(4).isAPartOf(filler, 144);
	private FixedLengthStringData t5537ActAllocBasis38Old = new FixedLengthStringData(4).isAPartOf(filler, 148);
	private FixedLengthStringData t5537ActAllocBasis39Old = new FixedLengthStringData(4).isAPartOf(filler, 152);
	private FixedLengthStringData t5537ActAllocBasis40Old = new FixedLengthStringData(4).isAPartOf(filler, 156);
	private FixedLengthStringData t5537ActAllocBasis41Old = new FixedLengthStringData(4).isAPartOf(filler, 160);
	private FixedLengthStringData t5537ActAllocBasis42Old = new FixedLengthStringData(4).isAPartOf(filler, 164);
	private FixedLengthStringData t5537ActAllocBasis43Old = new FixedLengthStringData(4).isAPartOf(filler, 168);
	private FixedLengthStringData t5537ActAllocBasis44Old = new FixedLengthStringData(4).isAPartOf(filler, 172);
	private FixedLengthStringData t5537ActAllocBasis45Old = new FixedLengthStringData(4).isAPartOf(filler, 176);
	private FixedLengthStringData t5537ActAllocBasis46Old = new FixedLengthStringData(4).isAPartOf(filler, 180);
	private FixedLengthStringData t5537ActAllocBasis47Old = new FixedLengthStringData(4).isAPartOf(filler, 184);
	private FixedLengthStringData t5537ActAllocBasis48Old = new FixedLengthStringData(4).isAPartOf(filler, 188);
	private FixedLengthStringData t5537ActAllocBasis49Old = new FixedLengthStringData(4).isAPartOf(filler, 192);
	private FixedLengthStringData t5537ActAllocBasis50Old = new FixedLengthStringData(4).isAPartOf(filler, 196);
	private FixedLengthStringData t5537ActAllocBasis51Old = new FixedLengthStringData(4).isAPartOf(filler, 200);
	private FixedLengthStringData t5537ActAllocBasis52Old = new FixedLengthStringData(4).isAPartOf(filler, 204);
	private FixedLengthStringData t5537ActAllocBasis53Old = new FixedLengthStringData(4).isAPartOf(filler, 208);
	private FixedLengthStringData t5537ActAllocBasis54Old = new FixedLengthStringData(4).isAPartOf(filler, 212);
	private FixedLengthStringData t5537ActAllocBasis55Old = new FixedLengthStringData(4).isAPartOf(filler, 216);
	private FixedLengthStringData t5537ActAllocBasis56Old = new FixedLengthStringData(4).isAPartOf(filler, 220);
	private FixedLengthStringData t5537ActAllocBasis57Old = new FixedLengthStringData(4).isAPartOf(filler, 224);
	private FixedLengthStringData t5537ActAllocBasis58Old = new FixedLengthStringData(4).isAPartOf(filler, 228);
	private FixedLengthStringData t5537ActAllocBasis59Old = new FixedLengthStringData(4).isAPartOf(filler, 232);
	private FixedLengthStringData t5537ActAllocBasis60Old = new FixedLengthStringData(4).isAPartOf(filler, 236);
	private FixedLengthStringData t5537ActAllocBasis61Old = new FixedLengthStringData(4).isAPartOf(filler, 240);
	private FixedLengthStringData t5537ActAllocBasis62Old = new FixedLengthStringData(4).isAPartOf(filler, 244);
	private FixedLengthStringData t5537ActAllocBasis63Old = new FixedLengthStringData(4).isAPartOf(filler, 248);
	private FixedLengthStringData t5537ActAllocBasis64Old = new FixedLengthStringData(4).isAPartOf(filler, 252);
	private FixedLengthStringData t5537ActAllocBasis65Old = new FixedLengthStringData(4).isAPartOf(filler, 256);
	private FixedLengthStringData t5537ActAllocBasis66Old = new FixedLengthStringData(4).isAPartOf(filler, 260);
	private FixedLengthStringData t5537ActAllocBasis67Old = new FixedLengthStringData(4).isAPartOf(filler, 264);
	private FixedLengthStringData t5537ActAllocBasis68Old = new FixedLengthStringData(4).isAPartOf(filler, 268);
	private FixedLengthStringData t5537ActAllocBasis69Old = new FixedLengthStringData(4).isAPartOf(filler, 272);
	private FixedLengthStringData t5537ActAllocBasis70Old = new FixedLengthStringData(4).isAPartOf(filler, 276);
	private FixedLengthStringData t5537ActAllocBasis71Old = new FixedLengthStringData(4).isAPartOf(filler, 280);
	private FixedLengthStringData t5537ActAllocBasis72Old = new FixedLengthStringData(4).isAPartOf(filler, 284);
	private FixedLengthStringData t5537ActAllocBasis73Old = new FixedLengthStringData(4).isAPartOf(filler, 288);
	private FixedLengthStringData t5537ActAllocBasis74Old = new FixedLengthStringData(4).isAPartOf(filler, 292);
	private FixedLengthStringData t5537ActAllocBasis75Old = new FixedLengthStringData(4).isAPartOf(filler, 296);
	private FixedLengthStringData t5537ActAllocBasis76Old = new FixedLengthStringData(4).isAPartOf(filler, 300);
	private FixedLengthStringData t5537ActAllocBasis77Old = new FixedLengthStringData(4).isAPartOf(filler, 304);
	private FixedLengthStringData t5537ActAllocBasis78Old = new FixedLengthStringData(4).isAPartOf(filler, 308);
	private FixedLengthStringData t5537ActAllocBasis79Old = new FixedLengthStringData(4).isAPartOf(filler, 312);
	private FixedLengthStringData t5537ActAllocBasis80Old = new FixedLengthStringData(4).isAPartOf(filler, 316);
	private FixedLengthStringData t5537ActAllocBasis81Old = new FixedLengthStringData(4).isAPartOf(filler, 320);
	private FixedLengthStringData t5537ActAllocBasis82Old = new FixedLengthStringData(4).isAPartOf(filler, 324);
	private FixedLengthStringData t5537ActAllocBasis83Old = new FixedLengthStringData(4).isAPartOf(filler, 328);
	private FixedLengthStringData t5537ActAllocBasis84Old = new FixedLengthStringData(4).isAPartOf(filler, 332);
	private FixedLengthStringData t5537ActAllocBasis85Old = new FixedLengthStringData(4).isAPartOf(filler, 336);
	private FixedLengthStringData t5537ActAllocBasis86Old = new FixedLengthStringData(4).isAPartOf(filler, 340);
	private FixedLengthStringData t5537ActAllocBasis87Old = new FixedLengthStringData(4).isAPartOf(filler, 344);
	private FixedLengthStringData t5537ActAllocBasis88Old = new FixedLengthStringData(4).isAPartOf(filler, 348);
	private FixedLengthStringData t5537ActAllocBasis89Old = new FixedLengthStringData(4).isAPartOf(filler, 352);
	private FixedLengthStringData t5537ActAllocBasis90Old = new FixedLengthStringData(4).isAPartOf(filler, 356);
	private FixedLengthStringData t5537AgecontOld = new FixedLengthStringData(8).isAPartOf(t5537RecOld, 360);
	private FixedLengthStringData t5537ToagesOld = new FixedLengthStringData(20).isAPartOf(t5537RecOld, 368);
	private ZonedDecimalData[] t5537ToageOld = ZDArrayPartOfStructure(10, 2, 0, t5537ToagesOld, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(t5537ToagesOld, 0, FILLER_REDEFINE);
	private ZonedDecimalData t5537Toage01Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 0);
	private ZonedDecimalData t5537Toage02Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 2);
	private ZonedDecimalData t5537Toage03Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4);
	private ZonedDecimalData t5537Toage04Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 6);
	private ZonedDecimalData t5537Toage05Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 8);
	private ZonedDecimalData t5537Toage06Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 10);
	private ZonedDecimalData t5537Toage07Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 12);
	private ZonedDecimalData t5537Toage08Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 14);
	private ZonedDecimalData t5537Toage09Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 16);
	private ZonedDecimalData t5537Toage10Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 18);
	private FixedLengthStringData t5537TotermsOld = new FixedLengthStringData(18).isAPartOf(t5537RecOld, 388);
	private ZonedDecimalData[] t5537TotermOld = ZDArrayPartOfStructure(9, 2, 0, t5537TotermsOld, 0);
	private FixedLengthStringData filler2 = new FixedLengthStringData(18).isAPartOf(t5537TotermsOld, 0, FILLER_REDEFINE);
	private ZonedDecimalData t5537Toterm01Old = new ZonedDecimalData(2, 0).isAPartOf(filler2, 0);
	private ZonedDecimalData t5537Toterm02Old = new ZonedDecimalData(2, 0).isAPartOf(filler2, 2);
	private ZonedDecimalData t5537Toterm03Old = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4);
	private ZonedDecimalData t5537Toterm04Old = new ZonedDecimalData(2, 0).isAPartOf(filler2, 6);
	private ZonedDecimalData t5537Toterm05Old = new ZonedDecimalData(2, 0).isAPartOf(filler2, 8);
	private ZonedDecimalData t5537Toterm06Old = new ZonedDecimalData(2, 0).isAPartOf(filler2, 10);
	private ZonedDecimalData t5537Toterm07Old = new ZonedDecimalData(2, 0).isAPartOf(filler2, 12);
	private ZonedDecimalData t5537Toterm08Old = new ZonedDecimalData(2, 0).isAPartOf(filler2, 14);
	private ZonedDecimalData t5537Toterm09Old = new ZonedDecimalData(2, 0).isAPartOf(filler2, 16);
	private FixedLengthStringData t5537TrmcontOld = new FixedLengthStringData(8).isAPartOf(t5537RecOld, 406);
}
}
