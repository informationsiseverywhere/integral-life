package com.csc.life.newbusiness.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

public class HpavTableDAM extends HpavpfTableDAM{

	public HpavTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("HPAV");
	}
	
	public String getTable() {
		return TABLE;
	}


public void setKeyConstants() {

	KEYCOLUMNS = ""
	             +  "CHDRCOY";
	
	QUALIFIEDCOLUMNS = 
			"CHDRCOY, " +
			"CHDRNUM, " +
			"LINSNAME, " +
			"HOISSDTE, " +
		    "ANETPRE, " + 
		    "ASPTDTY, " +
			"CURR, " +
			"UNIQUE_NUMBER";
	
	ORDERBY = 
            "CURR ASC";
	
	setStatics();
	
	qualifiedColumns = new BaseData[] {
            chdrcoy,
            chdrnum,
            linsname,
            hoissdte,
            anetpre,
            asptdty,
            curr,
            unique_number 
            };
}
public FixedLengthStringData getBaseString() {
	FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
	result.set(getParams());
		return result;
}

public String toString() {
		return getBaseString().toString();
}

/*****************************************************************************************************/
/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
/*****************************************************************************************************/

public FixedLengthStringData getHeader() {
	return getShortHeader();		
}

public FixedLengthStringData setHeader(Object what) {
	
	return setShortHeader(what);
}

private FixedLengthStringData keyFiller = new FixedLengthStringData(1);	// FILLER field under -KEY-DATA
/****************************************************************/
/* Getters and setters for key SKM group fields            */
/****************************************************************/
public FixedLengthStringData getRecKeyData() {
	FixedLengthStringData keyData = new FixedLengthStringData(1);
	
	keyData.set(getChdrcoy().toInternal()
				+ keyFiller.toInternal());

	return keyData;
}

public FixedLengthStringData setRecKeyData(Object obj) {
	clearRecKeyData();
	if (isClearOperation(obj)) {
		return null;
	} else {
		FixedLengthStringData what = new FixedLengthStringData(obj.toString());

		what = ExternalData.chop(what, chdrcoy);
		what = ExternalData.chop(what, keyFiller);
	
		return what;
	}
}

// FILLER fields under -NON-KEY-DATA
private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);



/****************************************************************/
/* After a DB read we need to set the FILLERs under             */
/* -NON-KEY-DATA to equal the key values                        */
/****************************************************************/
public void setNonKeyFillers() {

nonKeyFiller10.setInternal(chdrcoy.toInternal());


}

/****************************************************************/
/* Getters and setters for non-key SKM group fields             */
/****************************************************************/
public FixedLengthStringData getRecNonKeyData() {
	FixedLengthStringData nonKeyData = new FixedLengthStringData(91);
	
	nonKeyData.set(
				nonKeyFiller10.toInternal()
				
				+ getChdrnum().toInternal()
			    + getLinsname().toInternal()
				+ getHoissdte().toInternal()
				+ getAnetpre().toInternal()
				+ getAsptdty().toInternal()
				+ getCurr().toInternal());
	return nonKeyData;
}

public FixedLengthStringData setRecNonKeyData(Object obj) {
	clearRecNonKeyData();
	if (isClearOperation(obj)) {
		return null;
	} else {
		FixedLengthStringData what = new FixedLengthStringData(obj.toString());

		what = ExternalData.chop(what, nonKeyFiller10);
	    what = ExternalData.chop(what, chdrnum);
		what = ExternalData.chop(what, linsname);
		what = ExternalData.chop(what, hoissdte);
		what = ExternalData.chop(what, anetpre);
		what = ExternalData.chop(what, asptdty);
		what = ExternalData.chop(what, curr);
		return what;
	}
}

/****************************************************************/
/* Getters and setters for key SKM fields                       */
/****************************************************************/

public FixedLengthStringData getChdrcoy() {
	return chdrcoy;
}
public void setChdrcoy(Object what) {
	chdrcoy.set(what);
}
public FixedLengthStringData getChdrnum() {
	return chdrnum;
}
public void setChdrnum(Object what) {
	chdrnum.set(what);
}
public FixedLengthStringData getLinsname() {
	return linsname;
}
public void setLinsname(Object what) {
	linsname.set(what);
}
public PackedDecimalData getHoissdte() {
	return hoissdte;
}
public void setHoissdte(Object what) {
	hoissdte.set(what);
}
public PackedDecimalData getAnetpre() {
	return anetpre;
}
public void setAnetpre(Object what) {
	anetpre.set(what);
}


public PackedDecimalData getAsptdty() {
	return asptdty;
}
public void setAsptdty(Object what) {
	asptdty.set(what);
}	

public FixedLengthStringData getCurr() {
	return curr;
}
public void setCurr(Object what) {
	curr.set(what);
}	



/****************************************************************/
/* Group Clear Methods                                          */
/****************************************************************/
public void clearRecKeyData() {

	chdrcoy.clear();
    keyFiller.clear();
}

public void clearRecNonKeyData() {

	nonKeyFiller10.clear();

	chdrnum.clear();
	linsname.clear();
	hoissdte.clear();
	anetpre.clear();
    asptdty.clear();
    curr.clear();
		
}


}



	