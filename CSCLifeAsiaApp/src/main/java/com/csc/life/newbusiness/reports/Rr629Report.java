package com.csc.life.newbusiness.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RR629.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:52
 * @author Quipoz
 */
public class Rr629Report extends SMARTReportLayout { 

	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData clntlname = new FixedLengthStringData(40);
	private FixedLengthStringData cltdob = new FixedLengthStringData(10);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData cownnum = new FixedLengthStringData(8);
	private FixedLengthStringData cownum = new FixedLengthStringData(8);
	private FixedLengthStringData effdates = new FixedLengthStringData(10);
	private FixedLengthStringData lifcnum = new FixedLengthStringData(8);
	private FixedLengthStringData lname = new FixedLengthStringData(40);
	private FixedLengthStringData occdate = new FixedLengthStringData(10);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData repdate = new FixedLengthStringData(10);
	private FixedLengthStringData rname = new FixedLengthStringData(40);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData zseqno = new ZonedDecimalData(4, 0);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rr629Report() {
		super();
	}


	/**
	 * Print the XML for Rr629d01
	 */
	public void printRr629d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		zseqno.setFieldName("zseqno");
		zseqno.setInternal(subString(recordData, 1, 4));
		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 5, 8));
		cnttype.setFieldName("cnttype");
		cnttype.setInternal(subString(recordData, 13, 3));
		lifcnum.setFieldName("lifcnum");
		lifcnum.setInternal(subString(recordData, 16, 8));
		lname.setFieldName("lname");
		lname.setInternal(subString(recordData, 24, 40));
		cownum.setFieldName("cownum");
		cownum.setInternal(subString(recordData, 64, 8));
		rname.setFieldName("rname");
		rname.setInternal(subString(recordData, 72, 40));
		cownnum.setFieldName("cownnum");
		cownnum.setInternal(subString(recordData, 112, 8));
		clntlname.setFieldName("clntlname");
		clntlname.setInternal(subString(recordData, 120, 40));
		cltdob.setFieldName("cltdob");
		cltdob.setInternal(subString(recordData, 160, 10));
		occdate.setFieldName("occdate");
		occdate.setInternal(subString(recordData, 170, 10));
		effdates.setFieldName("effdates");
		effdates.setInternal(subString(recordData, 180, 10));
		printLayout("Rr629d01",			// Record name
			new BaseData[]{			// Fields:
				zseqno,
				chdrnum,
				cnttype,
				lifcnum,
				lname,
				cownum,
				rname,
				cownnum,
				clntlname,
				cltdob,
				occdate,
				effdates
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rr629e01
	 */
	public void printRr629e01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		printLayout("Rr629e01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rr629h01
	 */
	public void printRr629h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.set(3);

		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 1, 10));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 11, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 12, 30));
		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 42, 10));
		time.setFieldName("time");
		time.set(getTime());
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 52, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 54, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rr629h01",			// Record name
			new BaseData[]{			// Fields:
				sdate,
				company,
				companynm,
				repdate,
				time,
				branch,
				branchnm,
				pagnbr
			}
		);

		currentPrintLine.add(8);
	}


}
