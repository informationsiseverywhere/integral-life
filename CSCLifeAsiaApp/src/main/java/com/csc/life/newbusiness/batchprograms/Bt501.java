/*
 * File: Bt501.java
 * Date: 29 August 2009 22:37:14
 * Author: Quipoz Limited
 * 
 * Class transformed from BT501.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;

import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.life.contractservicing.procedures.Intcalc;
import com.csc.life.contractservicing.recordstructures.Intcalcrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.recordstructures.Pt501par;
import com.csc.life.newbusiness.reports.Rt501Report;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* COMPILE-OPTIONS-SQL   CSRSQLCSR(*ENDJOB)        <Do not delete>
*
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*   Policy Loan Report.
*
*   The basic procedure division logic is for reading via SQL and
*     printing a simple input primary file.
*
***********************************************************************
* </pre>
*/
public class Bt501 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlloanpf1rs = null;
	private java.sql.PreparedStatement sqlloanpf1ps = null;
	private java.sql.Connection sqlloanpf1conn = null;
	private String sqlloanpf1 = "";
	private Rt501Report printerFile = new Rt501Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(198);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BT501");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
	private String chdrenqrec = "CHDRENQREC";
	private String acblrec = "ACBLREC";
	private String t1693 = "T1693";
	private String t3629 = "T3629";
	private String t5645 = "T5645";
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler1, 5);

		/* SQL-LOANPF */
	private FixedLengthStringData loanrec = new FixedLengthStringData(57);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(loanrec, 0);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(loanrec, 1);
	private FixedLengthStringData sqlLoancurr = new FixedLengthStringData(3).isAPartOf(loanrec, 9);
	private PackedDecimalData sqlLoannumber = new PackedDecimalData(2, 0).isAPartOf(loanrec, 12);
	private FixedLengthStringData sqlLoantype = new FixedLengthStringData(1).isAPartOf(loanrec, 14);
	private PackedDecimalData sqlLoanstdate = new PackedDecimalData(8, 0).isAPartOf(loanrec, 15);
	private PackedDecimalData sqlLoanorigamt = new PackedDecimalData(17, 2).isAPartOf(loanrec, 20);
	private PackedDecimalData sqlLoanlintbdate = new PackedDecimalData(8, 0).isAPartOf(loanrec, 29);
	private PackedDecimalData sqlLoancapamt = new PackedDecimalData(17, 2).isAPartOf(loanrec, 34);
	private PackedDecimalData sqlLoanlcapdate = new PackedDecimalData(8, 0).isAPartOf(loanrec, 43);
	private PackedDecimalData sqlTplstmdty = new PackedDecimalData(17, 2).isAPartOf(loanrec, 48);

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData wsaaFirstHeading = new FixedLengthStringData(1).init("Y");
	private Validator firstHeading = new Validator(wsaaFirstHeading, "Y");
	private Validator notFirstHeading = new Validator(wsaaFirstHeading, "N");
	private FixedLengthStringData wsaaChdrno = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaLoancurr = new FixedLengthStringData(3).init(SPACES);
	private ZonedDecimalData wsaaDatefrom = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaDateto = new ZonedDecimalData(8, 0);
	private PackedDecimalData wsaaT5645Idx = new PackedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaLoanNumber = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
	private PackedDecimalData wsaaCoyCnt = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaCoyOrig = new PackedDecimalData(11, 2);
	private PackedDecimalData wsaaCoyCurb = new PackedDecimalData(11, 2);
	private PackedDecimalData wsaaCoyPint = new PackedDecimalData(11, 2);
	private PackedDecimalData wsaaCoyDuty = new PackedDecimalData(9, 2);

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rt501H01 = new FixedLengthStringData(84);
	private FixedLengthStringData rt501h01O = new FixedLengthStringData(84).isAPartOf(rt501H01, 0);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rt501h01O, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rt501h01O, 10);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rt501h01O, 11);
	private FixedLengthStringData rh01Repdate = new FixedLengthStringData(10).isAPartOf(rt501h01O, 41);
	private FixedLengthStringData rh01Currcode = new FixedLengthStringData(3).isAPartOf(rt501h01O, 51);
	private FixedLengthStringData rh01Currencynm = new FixedLengthStringData(30).isAPartOf(rt501h01O, 54);

	private FixedLengthStringData rt501D01 = new FixedLengthStringData(134);
	private FixedLengthStringData rt501d01O = new FixedLengthStringData(134).isAPartOf(rt501D01, 0);
	private FixedLengthStringData rd01Chdrno = new FixedLengthStringData(8).isAPartOf(rt501d01O, 0);
	private FixedLengthStringData rd01Linsname = new FixedLengthStringData(47).isAPartOf(rt501d01O, 8);
	private FixedLengthStringData rd01Cnttype = new FixedLengthStringData(3).isAPartOf(rt501d01O, 55);
	private FixedLengthStringData rd01Occdate = new FixedLengthStringData(10).isAPartOf(rt501d01O, 58);
	private ZonedDecimalData rd01Loannumber = new ZonedDecimalData(2, 0).isAPartOf(rt501d01O, 68);
	private FixedLengthStringData rd01Loantype = new FixedLengthStringData(1).isAPartOf(rt501d01O, 70);
	private FixedLengthStringData rd01Loanstdate = new FixedLengthStringData(10).isAPartOf(rt501d01O, 71);
	private ZonedDecimalData rd01Linstamt = new ZonedDecimalData(11, 2).isAPartOf(rt501d01O, 81);
	private ZonedDecimalData rd01Zbamt = new ZonedDecimalData(11, 2).isAPartOf(rt501d01O, 92);
	private ZonedDecimalData rd01Plint = new ZonedDecimalData(11, 2).isAPartOf(rt501d01O, 103);
	private ZonedDecimalData rd01Ztamt = new ZonedDecimalData(11, 2).isAPartOf(rt501d01O, 114);
	private ZonedDecimalData rd01Stpdty = new ZonedDecimalData(9, 2).isAPartOf(rt501d01O, 125);

	private FixedLengthStringData rt501T01 = new FixedLengthStringData(58);
	private FixedLengthStringData rt501t01O = new FixedLengthStringData(58).isAPartOf(rt501T01, 0);
	private ZonedDecimalData rt01Gcnt = new ZonedDecimalData(5, 0).isAPartOf(rt501t01O, 0);
	private ZonedDecimalData rt01Linstamt = new ZonedDecimalData(11, 2).isAPartOf(rt501t01O, 5);
	private ZonedDecimalData rt01Zbamt = new ZonedDecimalData(11, 2).isAPartOf(rt501t01O, 16);
	private ZonedDecimalData rt01Plint = new ZonedDecimalData(11, 2).isAPartOf(rt501t01O, 27);
	private ZonedDecimalData rt01Ztamt = new ZonedDecimalData(11, 2).isAPartOf(rt501t01O, 38);
	private ZonedDecimalData rt01Stpdty = new ZonedDecimalData(9, 2).isAPartOf(rt501t01O, 49);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Subsidiary account balance*/
	private AcblTableDAM acblIO = new AcblTableDAM();
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Intcalcrec intcalcrec = new Intcalcrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Pt501par pt501par = new Pt501par();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T5645rec t5645rec = new T5645rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endOfFile2080, 
		exit2090, 
		exit2790, 
		next013120
	}

	public Bt501() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		syserrrec.syserrStatuz.set(sqlStatuz);
		fatalError600();
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingCompany1020();
		setUpHeadingDates1040();
		defineCursor1060();
	}

protected void initialise1010()
	{
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
		pt501par.parmRecord.set(bupaIO.getParmarea());
		wsaaDatefrom.set(pt501par.datefrom);
		wsaaDateto.set(pt501par.dateto);
		wsaaCoyCnt.set(ZERO);
		wsaaCoyOrig.set(ZERO);
		wsaaCoyCurb.set(ZERO);
		wsaaCoyPint.set(ZERO);
		wsaaCoyDuty.set(ZERO);
	}

protected void setUpHeadingCompany1020()
	{
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(bsprIO.getCompany());
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(pt501par.dateto);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Repdate.set(datcon1rec.extDate);
	}

protected void defineCursor1060()
	{
		sqlloanpf1 = " SELECT  CHDRCOY, CHDRNUM, LOANCURR, LOANNUMBER, LOANTYPE, LOANSTDATE, LOANORIGAM, LSTINTBDTE, LSTCAPLAMT, LSTCAPDATE, TPLSTMDTY" +
" FROM   " + appVars.getTableNameOverriden("LOANPF") + " " +
" WHERE LOANSTDATE BETWEEN ? AND ?" +
" ORDER BY LOANCURR, CHDRNUM, LOANNUMBER";
		sqlerrorflag = false;
		try {
			sqlloanpf1conn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.contractservicing.dataaccess.LoanpfTableDAM());
			sqlloanpf1ps = appVars.prepareStatementEmbeded(sqlloanpf1conn, sqlloanpf1, "LOANPF");
			appVars.setDBDouble(sqlloanpf1ps, 1, wsaaDatefrom.toDouble());
			appVars.setDBDouble(sqlloanpf1ps, 2, wsaaDateto.toDouble());
			sqlloanpf1rs = appVars.executeQuery(sqlloanpf1ps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*EXIT*/
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case endOfFile2080: {
					endOfFile2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (sqlloanpf1rs.next()) {
				appVars.getDBObject(sqlloanpf1rs, 1, sqlChdrcoy);
				appVars.getDBObject(sqlloanpf1rs, 2, sqlChdrnum);
				appVars.getDBObject(sqlloanpf1rs, 3, sqlLoancurr);
				appVars.getDBObject(sqlloanpf1rs, 4, sqlLoannumber);
				appVars.getDBObject(sqlloanpf1rs, 5, sqlLoantype);
				appVars.getDBObject(sqlloanpf1rs, 6, sqlLoanstdate);
				appVars.getDBObject(sqlloanpf1rs, 7, sqlLoanorigamt);
				appVars.getDBObject(sqlloanpf1rs, 8, sqlLoanlintbdate);
				appVars.getDBObject(sqlloanpf1rs, 9, sqlLoancapamt);
				appVars.getDBObject(sqlloanpf1rs, 10, sqlLoanlcapdate);
				appVars.getDBObject(sqlloanpf1rs, 11, sqlTplstmdty);
			}
			else {
				goTo(GotoLabel.endOfFile2080);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		goTo(GotoLabel.exit2090);
	}

protected void endOfFile2080()
	{
		wsspEdterror.set(varcom.endp);
		if (isNE(wsaaChdrno,SPACES)) {
			indOn.setTrue(1);
			rt01Gcnt.set(wsaaCoyCnt);
			rt01Linstamt.set(wsaaCoyOrig);
			rt01Zbamt.set(wsaaCoyCurb);
			rt01Plint.set(wsaaCoyPint);
			rt01Ztamt.set(wsaaCoyCurb);
			rt01Ztamt.add(wsaaCoyPint);
			rt01Stpdty.set(wsaaCoyDuty);
			printerFile.printRt501t01(rt501T01, indicArea);
		}
	}

protected void edit2500()
	{
		edit2510();
	}

protected void edit2510()
	{
		wsspEdterror.set(varcom.oK);
		if (firstHeading.isTrue()) {
			prepareHead2600();
			wsaaLoancurr.set(sqlLoancurr);
		}
		if (isNE(sqlLoancurr,wsaaLoancurr)) {
			indOn.setTrue(1);
			rt01Gcnt.set(wsaaCoyCnt);
			rt01Linstamt.set(wsaaCoyOrig);
			rt01Zbamt.set(wsaaCoyCurb);
			rt01Plint.set(wsaaCoyPint);
			rt01Ztamt.set(wsaaCoyCurb);
			rt01Ztamt.add(wsaaCoyPint);
			rt01Stpdty.set(wsaaCoyDuty);
			printerFile.printRt501t01(rt501T01, indicArea);
			wsaaCoyCnt.set(0);
			wsaaCoyOrig.set(0);
			wsaaCoyCurb.set(0);
			wsaaCoyPint.set(0);
			wsaaCoyCurb.set(0);
			wsaaCoyDuty.set(0);
			wsaaLoancurr.set(sqlLoancurr);
			wsaaOverflow.set("Y");
			prepareHead2600();
		}
		wsaaLoancurr.set(sqlLoancurr);
		if (isNE(sqlChdrnum,wsaaChdrno)) {
			wsaaCoyCnt.add(1);
			wsaaChdrno.set(sqlChdrnum);
			preparePolicy2700();
			indOn.setTrue(91);
		}
		else {
			indOff.setTrue(91);
		}
	}

protected void prepareHead2600()
	{
		prepare2610();
	}

protected void prepare2610()
	{
		wsaaFirstHeading.set("N");
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(sqlLoancurr);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Currcode.set(sqlLoancurr);
		rh01Currencynm.set(descIO.getLongdesc());
	}

protected void preparePolicy2700()
	{
		try {
			prepare2710();
		}
		catch (GOTOException e){
		}
	}

protected void prepare2710()
	{
		chdrenqIO.setRecKeyData(SPACES);
		chdrenqIO.setChdrcoy(sqlChdrcoy);
		chdrenqIO.setChdrnum(sqlChdrnum);
		chdrenqIO.setFunction(varcom.readr);
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)
		&& isNE(chdrenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			rd01Chdrno.set(SPACES);
			rd01Linsname.set(SPACES);
			rd01Cnttype.set(SPACES);
			rd01Occdate.set(SPACES);
			goTo(GotoLabel.exit2790);
		}
		rd01Chdrno.set(sqlChdrnum);
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(chdrenqIO.getCowncoy());
		namadrsrec.clntNumber.set(chdrenqIO.getCownnum());
		namadrsrec.language.set(bsscIO.getLanguage());
		namadrsrec.function.set("PYNMA");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isEQ(namadrsrec.statuz,varcom.oK)) {
			rd01Linsname.set(namadrsrec.name);
		}
		else {
			rd01Linsname.set(SPACES);
		}
		rd01Cnttype.set(chdrenqIO.getCnttype());
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(chdrenqIO.getOccdate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rd01Occdate.set(datcon1rec.extDate);
	}

protected void update3000()
	{
		/*UPDATE*/
		/*WRITE-DETAIL*/
		if (newPageReq.isTrue()) {
			printerFile.printRt501h01(rt501H01, indicArea);
			wsaaOverflow.set("N");
		}
		moveData3100();
		printerFile.printRt501d01(rt501D01, indicArea);
		/*EXIT*/
	}

protected void moveData3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					moveData3110();
				}
				case next013120: {
					next013120();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void moveData3110()
	{
		rd01Loannumber.set(sqlLoannumber);
		rd01Loantype.set(sqlLoantype);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(sqlLoanstdate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rd01Loanstdate.set(datcon1rec.extDate);
		rd01Linstamt.set(sqlLoanorigamt);
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(sqlChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem("HRTOTLON");
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			acblIO.setSacscurbal(ZERO);
			goTo(GotoLabel.next013120);
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		wsaaT5645Idx.set(1);
		acblIO.setParams(SPACES);
		acblIO.setRldgcoy(sqlChdrcoy);
		wsaaRldgacct.set(SPACES);
		wsaaChdrnum.set(sqlChdrnum);
		wsaaLoanNumber.set(sqlLoannumber);
		acblIO.setRldgacct(wsaaRldgacct);
		acblIO.setSacscode(t5645rec.sacscode[wsaaT5645Idx.toInt()]);
		acblIO.setSacstyp(t5645rec.sacstype[wsaaT5645Idx.toInt()]);
		acblIO.setOrigcurr(sqlLoancurr);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			fatalError600();
		}
		if (isNE(acblIO.getStatuz(),varcom.oK)) {
			acblIO.setSacscurbal(ZERO);
			goTo(GotoLabel.next013120);
		}
	}

protected void next013120()
	{
		if (isLT(sqlLoanlintbdate,wsaaDateto)) {
			intcalcrec.intcalcRec.set(SPACES);
			intcalcrec.statuz.set(varcom.oK);
			intcalcrec.loanNumber.set(sqlLoannumber);
			intcalcrec.chdrcoy.set(sqlChdrcoy);
			intcalcrec.chdrnum.set(sqlChdrnum);
			intcalcrec.cnttype.set(chdrenqIO.getCnttype());
			intcalcrec.interestTo.set(wsaaDateto);
			intcalcrec.interestFrom.set(sqlLoanlintbdate);
			intcalcrec.loanorigam.set(sqlLoancapamt);
			intcalcrec.lastCaplsnDate.set(sqlLoanlcapdate);
			intcalcrec.loanStartDate.set(sqlLoanstdate);
			intcalcrec.interestAmount.set(ZERO);
			intcalcrec.loanCurrency.set(sqlLoancurr);
			intcalcrec.loanType.set(sqlLoantype);
			callProgram(Intcalc.class, intcalcrec.intcalcRec);
			if (isNE(intcalcrec.statuz,varcom.oK)) {
				intcalcrec.interestAmount.set(0);
			}
		}
		else {
			intcalcrec.interestAmount.set(0);
		}
		rd01Zbamt.set(acblIO.getSacscurbal());
		rd01Ztamt.set(acblIO.getSacscurbal());
		rd01Plint.set(intcalcrec.interestAmount);
		rd01Ztamt.add(intcalcrec.interestAmount);
		rd01Stpdty.set(sqlTplstmdty);
		wsaaCoyOrig.add(sqlLoanorigamt);
		wsaaCoyCurb.add(acblIO.getSacscurbal());
		wsaaCoyPint.add(intcalcrec.interestAmount);
		wsaaCoyDuty.add(sqlTplstmdty);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		appVars.freeDBConnectionIgnoreErr(sqlloanpf1conn, sqlloanpf1ps, sqlloanpf1rs);
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
