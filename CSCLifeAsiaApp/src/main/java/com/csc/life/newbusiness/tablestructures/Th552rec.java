package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:19
 * Description:
 * Copybook name: TH552REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th552rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th552Rec = new FixedLengthStringData(564);
  	public FixedLengthStringData ageIssageFrms = new FixedLengthStringData(24).isAPartOf(th552Rec, 0);
  	public ZonedDecimalData[] ageIssageFrm = ZDArrayPartOfStructure(8, 3, 0, ageIssageFrms, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(ageIssageFrms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData ageIssageFrm01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData ageIssageFrm02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData ageIssageFrm03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData ageIssageFrm04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public ZonedDecimalData ageIssageFrm05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData ageIssageFrm06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData ageIssageFrm07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData ageIssageFrm08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
  	public FixedLengthStringData ageIssageTos = new FixedLengthStringData(24).isAPartOf(th552Rec, 24);
  	public ZonedDecimalData[] ageIssageTo = ZDArrayPartOfStructure(8, 3, 0, ageIssageTos, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(ageIssageTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData ageIssageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData ageIssageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 3);
  	public ZonedDecimalData ageIssageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 6);
  	public ZonedDecimalData ageIssageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 9);
  	public ZonedDecimalData ageIssageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData ageIssageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 15);
  	public ZonedDecimalData ageIssageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 18);
  	public ZonedDecimalData ageIssageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 21);
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(th552Rec, 48);
  	public FixedLengthStringData defFupMeths = new FixedLengthStringData(256).isAPartOf(th552Rec, 51);
  	public FixedLengthStringData[] defFupMeth = FLSArrayPartOfStructure(64, 4, defFupMeths, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(256).isAPartOf(defFupMeths, 0, FILLER_REDEFINE);
  	public FixedLengthStringData defFupMeth01 = new FixedLengthStringData(4).isAPartOf(filler2, 0);
  	public FixedLengthStringData defFupMeth02 = new FixedLengthStringData(4).isAPartOf(filler2, 4);
  	public FixedLengthStringData defFupMeth03 = new FixedLengthStringData(4).isAPartOf(filler2, 8);
  	public FixedLengthStringData defFupMeth04 = new FixedLengthStringData(4).isAPartOf(filler2, 12);
  	public FixedLengthStringData defFupMeth05 = new FixedLengthStringData(4).isAPartOf(filler2, 16);
  	public FixedLengthStringData defFupMeth06 = new FixedLengthStringData(4).isAPartOf(filler2, 20);
  	public FixedLengthStringData defFupMeth07 = new FixedLengthStringData(4).isAPartOf(filler2, 24);
  	public FixedLengthStringData defFupMeth08 = new FixedLengthStringData(4).isAPartOf(filler2, 28);
  	public FixedLengthStringData defFupMeth09 = new FixedLengthStringData(4).isAPartOf(filler2, 32);
  	public FixedLengthStringData defFupMeth10 = new FixedLengthStringData(4).isAPartOf(filler2, 36);
  	public FixedLengthStringData defFupMeth11 = new FixedLengthStringData(4).isAPartOf(filler2, 40);
  	public FixedLengthStringData defFupMeth12 = new FixedLengthStringData(4).isAPartOf(filler2, 44);
  	public FixedLengthStringData defFupMeth13 = new FixedLengthStringData(4).isAPartOf(filler2, 48);
  	public FixedLengthStringData defFupMeth14 = new FixedLengthStringData(4).isAPartOf(filler2, 52);
  	public FixedLengthStringData defFupMeth15 = new FixedLengthStringData(4).isAPartOf(filler2, 56);
  	public FixedLengthStringData defFupMeth16 = new FixedLengthStringData(4).isAPartOf(filler2, 60);
  	public FixedLengthStringData defFupMeth17 = new FixedLengthStringData(4).isAPartOf(filler2, 64);
  	public FixedLengthStringData defFupMeth18 = new FixedLengthStringData(4).isAPartOf(filler2, 68);
  	public FixedLengthStringData defFupMeth19 = new FixedLengthStringData(4).isAPartOf(filler2, 72);
  	public FixedLengthStringData defFupMeth20 = new FixedLengthStringData(4).isAPartOf(filler2, 76);
  	public FixedLengthStringData defFupMeth21 = new FixedLengthStringData(4).isAPartOf(filler2, 80);
  	public FixedLengthStringData defFupMeth22 = new FixedLengthStringData(4).isAPartOf(filler2, 84);
  	public FixedLengthStringData defFupMeth23 = new FixedLengthStringData(4).isAPartOf(filler2, 88);
  	public FixedLengthStringData defFupMeth24 = new FixedLengthStringData(4).isAPartOf(filler2, 92);
  	public FixedLengthStringData defFupMeth25 = new FixedLengthStringData(4).isAPartOf(filler2, 96);
  	public FixedLengthStringData defFupMeth26 = new FixedLengthStringData(4).isAPartOf(filler2, 100);
  	public FixedLengthStringData defFupMeth27 = new FixedLengthStringData(4).isAPartOf(filler2, 104);
  	public FixedLengthStringData defFupMeth28 = new FixedLengthStringData(4).isAPartOf(filler2, 108);
  	public FixedLengthStringData defFupMeth29 = new FixedLengthStringData(4).isAPartOf(filler2, 112);
  	public FixedLengthStringData defFupMeth30 = new FixedLengthStringData(4).isAPartOf(filler2, 116);
  	public FixedLengthStringData defFupMeth31 = new FixedLengthStringData(4).isAPartOf(filler2, 120);
  	public FixedLengthStringData defFupMeth32 = new FixedLengthStringData(4).isAPartOf(filler2, 124);
  	public FixedLengthStringData defFupMeth33 = new FixedLengthStringData(4).isAPartOf(filler2, 128);
  	public FixedLengthStringData defFupMeth34 = new FixedLengthStringData(4).isAPartOf(filler2, 132);
  	public FixedLengthStringData defFupMeth35 = new FixedLengthStringData(4).isAPartOf(filler2, 136);
  	public FixedLengthStringData defFupMeth36 = new FixedLengthStringData(4).isAPartOf(filler2, 140);
  	public FixedLengthStringData defFupMeth37 = new FixedLengthStringData(4).isAPartOf(filler2, 144);
  	public FixedLengthStringData defFupMeth38 = new FixedLengthStringData(4).isAPartOf(filler2, 148);
  	public FixedLengthStringData defFupMeth39 = new FixedLengthStringData(4).isAPartOf(filler2, 152);
  	public FixedLengthStringData defFupMeth40 = new FixedLengthStringData(4).isAPartOf(filler2, 156);
  	public FixedLengthStringData defFupMeth41 = new FixedLengthStringData(4).isAPartOf(filler2, 160);
  	public FixedLengthStringData defFupMeth42 = new FixedLengthStringData(4).isAPartOf(filler2, 164);
  	public FixedLengthStringData defFupMeth43 = new FixedLengthStringData(4).isAPartOf(filler2, 168);
  	public FixedLengthStringData defFupMeth44 = new FixedLengthStringData(4).isAPartOf(filler2, 172);
  	public FixedLengthStringData defFupMeth45 = new FixedLengthStringData(4).isAPartOf(filler2, 176);
  	public FixedLengthStringData defFupMeth46 = new FixedLengthStringData(4).isAPartOf(filler2, 180);
  	public FixedLengthStringData defFupMeth47 = new FixedLengthStringData(4).isAPartOf(filler2, 184);
  	public FixedLengthStringData defFupMeth48 = new FixedLengthStringData(4).isAPartOf(filler2, 188);
  	public FixedLengthStringData defFupMeth49 = new FixedLengthStringData(4).isAPartOf(filler2, 192);
  	public FixedLengthStringData defFupMeth50 = new FixedLengthStringData(4).isAPartOf(filler2, 196);
  	public FixedLengthStringData defFupMeth51 = new FixedLengthStringData(4).isAPartOf(filler2, 200);
  	public FixedLengthStringData defFupMeth52 = new FixedLengthStringData(4).isAPartOf(filler2, 204);
  	public FixedLengthStringData defFupMeth53 = new FixedLengthStringData(4).isAPartOf(filler2, 208);
  	public FixedLengthStringData defFupMeth54 = new FixedLengthStringData(4).isAPartOf(filler2, 212);
  	public FixedLengthStringData defFupMeth55 = new FixedLengthStringData(4).isAPartOf(filler2, 216);
  	public FixedLengthStringData defFupMeth56 = new FixedLengthStringData(4).isAPartOf(filler2, 220);
  	public FixedLengthStringData defFupMeth57 = new FixedLengthStringData(4).isAPartOf(filler2, 224);
  	public FixedLengthStringData defFupMeth58 = new FixedLengthStringData(4).isAPartOf(filler2, 228);
  	public FixedLengthStringData defFupMeth59 = new FixedLengthStringData(4).isAPartOf(filler2, 232);
  	public FixedLengthStringData defFupMeth60 = new FixedLengthStringData(4).isAPartOf(filler2, 236);
  	public FixedLengthStringData defFupMeth61 = new FixedLengthStringData(4).isAPartOf(filler2, 240);
  	public FixedLengthStringData defFupMeth62 = new FixedLengthStringData(4).isAPartOf(filler2, 244);
  	public FixedLengthStringData defFupMeth63 = new FixedLengthStringData(4).isAPartOf(filler2, 248);
  	public FixedLengthStringData defFupMeth64 = new FixedLengthStringData(4).isAPartOf(filler2, 252);
  	public FixedLengthStringData zsuminfrs = new FixedLengthStringData(96).isAPartOf(th552Rec, 307);
  	public ZonedDecimalData[] zsuminfr = ZDArrayPartOfStructure(8, 12, 0, zsuminfrs, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(96).isAPartOf(zsuminfrs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData zsuminfr01 = new ZonedDecimalData(12, 0).isAPartOf(filler3, 0);
  	public ZonedDecimalData zsuminfr02 = new ZonedDecimalData(12, 0).isAPartOf(filler3, 12);
  	public ZonedDecimalData zsuminfr03 = new ZonedDecimalData(12, 0).isAPartOf(filler3, 24);
  	public ZonedDecimalData zsuminfr04 = new ZonedDecimalData(12, 0).isAPartOf(filler3, 36);
  	public ZonedDecimalData zsuminfr05 = new ZonedDecimalData(12, 0).isAPartOf(filler3, 48);
  	public ZonedDecimalData zsuminfr06 = new ZonedDecimalData(12, 0).isAPartOf(filler3, 60);
  	public ZonedDecimalData zsuminfr07 = new ZonedDecimalData(12, 0).isAPartOf(filler3, 72);
  	public ZonedDecimalData zsuminfr08 = new ZonedDecimalData(12, 0).isAPartOf(filler3, 84);
  	public FixedLengthStringData zsumintos = new FixedLengthStringData(96).isAPartOf(th552Rec, 403);
  	public ZonedDecimalData[] zsuminto = ZDArrayPartOfStructure(8, 12, 0, zsumintos, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(96).isAPartOf(zsumintos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData zsuminto01 = new ZonedDecimalData(12, 0).isAPartOf(filler4, 0);
  	public ZonedDecimalData zsuminto02 = new ZonedDecimalData(12, 0).isAPartOf(filler4, 12);
  	public ZonedDecimalData zsuminto03 = new ZonedDecimalData(12, 0).isAPartOf(filler4, 24);
  	public ZonedDecimalData zsuminto04 = new ZonedDecimalData(12, 0).isAPartOf(filler4, 36);
  	public ZonedDecimalData zsuminto05 = new ZonedDecimalData(12, 0).isAPartOf(filler4, 48);
  	public ZonedDecimalData zsuminto06 = new ZonedDecimalData(12, 0).isAPartOf(filler4, 60);
  	public ZonedDecimalData zsuminto07 = new ZonedDecimalData(12, 0).isAPartOf(filler4, 72);
  	public ZonedDecimalData zsuminto08 = new ZonedDecimalData(12, 0).isAPartOf(filler4, 84);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(65).isAPartOf(th552Rec, 499, FILLER);//ILJ-108


	public void initialize() {
		COBOLFunctions.initialize(th552Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th552Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}