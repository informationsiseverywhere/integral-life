package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH599
 * @version 1.0 generated on 30/08/09 07:05
 * @author Quipoz
 */
public class Sh599ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(282);
	public FixedLengthStringData dataFields = new FixedLengthStringData(58).isAPartOf(dataArea, 0);
	public ZonedDecimalData acctmonth = DD.acctmonth.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData acctyear = DD.acctyear.copyToZonedDecimal().isAPartOf(dataFields,2);
	public FixedLengthStringData bbranch = DD.bbranch.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData bcompany = DD.bcompany.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData branch = DD.branch.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData scheduleName = DD.bschednam.copy().isAPartOf(dataFields,11);
	public ZonedDecimalData scheduleNumber = DD.bschednum.copyToZonedDecimal().isAPartOf(dataFields,21);
	public FixedLengthStringData chdrtype = DD.chdrtype.copy().isAPartOf(dataFields,29);
	public FixedLengthStringData ctrlbrks = new FixedLengthStringData(2).isAPartOf(dataFields, 32);
	public FixedLengthStringData[] ctrlbrk = FLSArrayPartOfStructure(2, 1, ctrlbrks, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(ctrlbrks, 0, FILLER_REDEFINE);
	public FixedLengthStringData ctrlbrk01 = DD.ctrlbrk.copy().isAPartOf(filler,0);
	public FixedLengthStringData ctrlbrk02 = DD.ctrlbrk.copy().isAPartOf(filler,1);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,34);
	public FixedLengthStringData jobq = DD.jobq.copy().isAPartOf(dataFields,42);
	public ZonedDecimalData mnth = DD.mnth.copyToZonedDecimal().isAPartOf(dataFields,52);
	public ZonedDecimalData year = DD.year.copyToZonedDecimal().isAPartOf(dataFields,54);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(56).isAPartOf(dataArea, 58);
	public FixedLengthStringData acctmonthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acctyearErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bcompanyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData branchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bschednamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData bschednumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData chdrtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData ctrlbrksErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData[] ctrlbrkErr = FLSArrayPartOfStructure(2, 4, ctrlbrksErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(ctrlbrksErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ctrlbrk01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData ctrlbrk02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData jobqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData mnthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData yearErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(168).isAPartOf(dataArea, 114);
	public FixedLengthStringData[] acctmonthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] acctyearOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bcompanyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] branchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bschednamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] bschednumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] chdrtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData ctrlbrksOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 96);
	public FixedLengthStringData[] ctrlbrkOut = FLSArrayPartOfStructure(2, 12, ctrlbrksOut, 0);
	public FixedLengthStringData[][] ctrlbrkO = FLSDArrayPartOfArrayStructure(12, 1, ctrlbrkOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(ctrlbrksOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ctrlbrk01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] ctrlbrk02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] jobqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] mnthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] yearOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData Sh599screenWritten = new LongData(0);
	public LongData Sh599protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh599ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(branchOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctrlbrk01Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrtypeOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctrlbrk02Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mnthOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(yearOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {effdate, acctmonth, acctyear, scheduleName, bbranch, scheduleNumber, bcompany, jobq, branch, ctrlbrk01, chdrtype, ctrlbrk02, mnth, year};
		screenOutFields = new BaseData[][] {effdateOut, acctmonthOut, acctyearOut, bschednamOut, bbranchOut, bschednumOut, bcompanyOut, jobqOut, branchOut, ctrlbrk01Out, chdrtypeOut, ctrlbrk02Out, mnthOut, yearOut};
		screenErrFields = new BaseData[] {effdateErr, acctmonthErr, acctyearErr, bschednamErr, bbranchErr, bschednumErr, bcompanyErr, jobqErr, branchErr, ctrlbrk01Err, chdrtypeErr, ctrlbrk02Err, mnthErr, yearErr};
		screenDateFields = new BaseData[] {effdate};
		screenDateErrFields = new BaseData[] {effdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh599screen.class;
		protectRecord = Sh599protect.class;
	}

}
