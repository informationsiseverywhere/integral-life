/*
 * File: BR51P.java
 * Date: 17 April 2020 18:00:00
 * Author: Quipoz Limited
 * 
 * Class transformed from BR51P.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.AckdpfTableDAM;

import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;

/**
 * <pre>
*
*REMARKS.
*
*  SPLITTER PROGRAM (For Policy Acknowledgement Letters batch processing)
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* This program is to be run before the Policy Acknowledgement Letters
* (BR52P) program.It is run in single-thread, and writes the selected ACKD
* records to multiple files . Each of these files will be read
* by a copy of BR52P run in multi-thread.
*
* SQL will be used to access the ACKD physical file. The
* splitter program will extract ACKD records that meet the
* following criteria:
*
* i    validflag  =  '1'
* ii   chdrnum    between  <p6671-chdrnumfrom>
*                     and  <p6671-chdrnumto>
* iii  chdroy     =  <run-company>
*
* Control totals used in this program:
*
*    01  -  No. of thread members
*    02  -  No. of extracted records
*
*
* Splitter programs should be simple. They should only deal
* with a single file. If there is insufficient data on the
* primary file to completely identify a transaction then
* further editing should be done in the subsequent process.
* The objective of a splitter is to rapidily isolate potential
* transactions, not process the transaction. The primary concern
* for splitter program design is elasped time speed and this is
* achieved by minimising disk IO.
*
* Before the Splitter process is invoked the, the program CRTTMPF
* should be run. The CRTTMPF (Create Temporary File) will create
* a duplicate of a physical file (created under Smart and defined
* as a Query file) which will have as many members as are defined
* in that process's subsequent threads field. The temporary file
* will have the name XXXXDD9999 where XXXX is the first four
* characters of the file which was duplicated, DD is the first
* two characters of the 4th system parameter and 9999 is the
* schedule run number. Each member added to the temporary file
* will be called THREAD999 where 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read records from a primary file
* and for each record it selects it will write a record to a
* temporary file member. Exactly which temporary file member
* is written to is decided by a working storage count. The
* objective of the count is spread the transaction records
* evenly across the thread members.
*
* The Splitter program should always have a restart method of
* '1' indicating a re-run. In the event of a restart the Splitter
* program will always start with empty file members. Apart from
* writing records to the temporary file members Splitter programs
* should not be doing any further updates.
*
* There should be no non-standard CL commands in the CL Pgm which
* calls this program.
*
* Notes for programs which use Splitter Program Temporary Files.
* --------------------------------------------------------------
*
* The MTBE will be able to submit multiple versions of the program
* which use the file members created from this program. It is
* important that the process which runs the splitter program has
* the same 'number of subsequent threads' as the following
* process has defined in 'number of threads this process'.
*
* The subsequent program need only point itself to one member
* based around the field BSPR-SCHEDULE-THREAD-NO which indicates
* in which thread the copy of the subsequent program is running.
*
* Only the following code should be needed to perform an OVRDBF
* to point to the correct member:
 *
 *
 *****************************************************************
 * </pre>
 */
public class Br51p extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final Logger logger = LoggerFactory.getLogger(Br51p.class);

	private boolean sqlerrorflag;
	private int ackdCursorLoopIndex = 0;
	private DiskFileDAM ackd01 = new DiskFileDAM("ACKD01");
	private DiskFileDAM ackd02 = new DiskFileDAM("ACKD02");
	private DiskFileDAM ackd03 = new DiskFileDAM("ACKD03");
	private DiskFileDAM ackd04 = new DiskFileDAM("ACKD04");
	private DiskFileDAM ackd05 = new DiskFileDAM("ACKD05");
	private DiskFileDAM ackd06 = new DiskFileDAM("ACKD06");
	private DiskFileDAM ackd07 = new DiskFileDAM("ACKD07");
	private DiskFileDAM ackd08 = new DiskFileDAM("ACKD08");
	private DiskFileDAM ackd09 = new DiskFileDAM("ACKD09");
	private DiskFileDAM ackd10 = new DiskFileDAM("ACKD10");
	private DiskFileDAM ackd11 = new DiskFileDAM("ACKD11");
	private DiskFileDAM ackd12 = new DiskFileDAM("ACKD12");
	private DiskFileDAM ackd13 = new DiskFileDAM("ACKD13");
	private DiskFileDAM ackd14 = new DiskFileDAM("ACKD14");
	private DiskFileDAM ackd15 = new DiskFileDAM("ACKD15");
	private DiskFileDAM ackd16 = new DiskFileDAM("ACKD16");
	private DiskFileDAM ackd17 = new DiskFileDAM("ACKD17");
	private DiskFileDAM ackd18 = new DiskFileDAM("ACKD18");
	private DiskFileDAM ackd19 = new DiskFileDAM("ACKD19");
	private DiskFileDAM ackd20 = new DiskFileDAM("ACKD20");
	private AckdpfTableDAM ackdpfData = new AckdpfTableDAM();
	/* (Change the record length to that of the temporary file). */
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR51P");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaN = new FixedLengthStringData(2).init("N ");
	private FixedLengthStringData wsaa1 = new FixedLengthStringData(1).init("1");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

	/* ACKD member parametres. */
	private FixedLengthStringData wsaaACKDFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaACKDFn, 0, FILLER).init("ACKD");
	private FixedLengthStringData wsaaACKDRunid = new FixedLengthStringData(2).isAPartOf(wsaaACKDFn, 4);
	private ZonedDecimalData wsaaACKDJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaACKDFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER)
			.init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER)
			.init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	/*
	 * 01 ACKDPF-DATA. COPY DDS-ALL-FORMATS OF ACKDPF. Pointers to point to the
	 * member to read (IY) and write (IZ).
	 */
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
	/* Define the number of rows in the block to be fetched. */
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);


	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";
	/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private int contot02;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
	private P6671par p6671par = new P6671par();
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).init(0);
	private FixedLengthStringData wsaaExclude = new FixedLengthStringData(1).init("E");
	private StringBuilder sQuery;
	private java.sql.ResultSet sqllinsCursorrs;
	private java.sql.PreparedStatement sqllinsCursorps ;
	private java.sql.Connection sqllinsCursorconn ;

	/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaAckdpfData = FLSInittedArray(1000, 53);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaAckdpfData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaAckdpfData, 1);
	private FixedLengthStringData[] wsaadlvrmode = FLSDArrayPartOfArrayStructure(4, wsaaAckdpfData, 9);
	private PackedDecimalData[] wsaadespdate = PDArrayPartOfArrayStructure(8, 0, wsaaAckdpfData, 13);
	private PackedDecimalData[] wsaapackdate = PDArrayPartOfArrayStructure(8, 0, wsaaAckdpfData, 21);
	private PackedDecimalData[] wsaadeemdate = PDArrayPartOfArrayStructure(8, 0, wsaaAckdpfData, 29);
	private PackedDecimalData[] wsaanextActDate = PDArrayPartOfArrayStructure(8, 0, wsaaAckdpfData, 37);
	private PackedDecimalData[] wsaaUniqNumber = PDArrayPartOfArrayStructure(8, 0, wsaaAckdpfData, 45);

	public Br51p() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}

	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}

	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray) {
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void restart0900() {
		/* RESTART */
		/** The restart section is being handled in section 1100 . */
		/** This is because the code is applicable to every run, */
		/** not just restarts. */
		/* EXIT */
	}

	protected void initialise1000() {
		initialise1010();
	}

	protected void initialise1010() {
		/* Check that the restart method from the process definition */
		/* is compatible with the program (of type '1'). In the event */
		/* of failure, the program will be re-run from the beginning. */
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/* Do a ovrdbf for each temporary file member. (Note we have */
		/* allowed for a maximum of 20.) */

		wsaaACKDRunid.set(bprdIO.getSystemParam04());
		wsaaACKDJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		
		/* Since this program runs from a parameter screen move the */
		/* internal parameter area to the original parameter copybook */
		/* to retrieve the contract range */
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES) && isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		} else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		
		/* The next thing we must do is construct the name of the */
		/* temporary file which we will be working with. */

		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		/* Open the required number of temporary files, depending */
		/* on IZ and determined by the number of threads specified */
		/* for the subsequent process. */
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)) {
			openThreadMember1100();
		}
		/* Log the number of threads being used */
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		wsaaCompany.set(bsprIO.getCompany());
		wsaaEffdate.set(bsscIO.getEffectiveDate());
		readChunkRecord();

	}

	private void readChunkRecord() {
		iy.set(1);
		sQuery = new StringBuilder();
		sQuery.append(
				"SELECT HD.CHDRCOY, HD.CHDRNUM, HD.DLVRMODE, HD.DESPDATE, HD.PACKDATE, HD.DEEMDATE, HD.NXTDTE, HD.UNIQUE_NUMBER ");
		sQuery.append("FROM VM1DTA.HPADPF HD ");
		sQuery.append("WHERE HD.CHDRCOY=? AND HD.INCEXC <> ? AND HD.NXTDTE <= ? AND HD.CHDRNUM BETWEEN ? AND ? ");
		sQuery.append("ORDER BY HD.CHDRNUM");

		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)) {
			initialize(wsaaAckdpfData[wsaaInd.toInt()]);
		}
		wsaaInd.set(1);
		sqlerrorflag = false;
		try {
			sqllinsCursorconn = getAppVars()
					.getDBConnectionForTable(new com.csc.life.newbusiness.dataaccess.AckdpfTableDAM());
			sqllinsCursorps = getAppVars().prepareStatementEmbeded(sqllinsCursorconn, sQuery.toString());
			
			getAppVars().setDBString(sqllinsCursorps, 1, wsaaCompany);
			getAppVars().setDBString(sqllinsCursorps, 2, wsaaExclude);
			getAppVars().setDBString(sqllinsCursorps, 3, wsaaEffdate);
			getAppVars().setDBString(sqllinsCursorps, 4, wsaaChdrnumFrom);
			getAppVars().setDBString(sqllinsCursorps, 5, wsaaChdrnumTo);
			sqllinsCursorrs = getAppVars().executeQuery(sqllinsCursorps);
		} catch (SQLException ex) {
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}

	}

	protected void openThreadMember1100() {
		openThreadMember1110();
	}

	protected void openThreadMember1110() {
		/* The following part is used to initialise all members. */
		/* It is also used in restart mode. */
		/* Temporary files, previously used are cleared. */
		/* MOVE IZ TO WSAA-THREAD-NUMBER. */
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(), 1), iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaACKDFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz, Varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.params.set("CLRTMPF");
			fatalError600();
		}
		/* Do the override. */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(ACKD");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaACKDFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/* Open the file. */
		if (isEQ(iz, 1)) {
			ackd01.openOutput();
		}
		if (isEQ(iz, 2)) {
			ackd02.openOutput();
		}
		if (isEQ(iz, 3)) {
			ackd03.openOutput();
		}
		if (isEQ(iz, 4)) {
			ackd04.openOutput();
		}
		if (isEQ(iz, 5)) {
			ackd05.openOutput();
		}
		if (isEQ(iz, 6)) {
			ackd06.openOutput();
		}
		if (isEQ(iz, 7)) {
			ackd07.openOutput();
		}
		if (isEQ(iz, 8)) {
			ackd08.openOutput();
		}
		if (isEQ(iz, 9)) {
			ackd09.openOutput();
		}
		if (isEQ(iz, 10)) {
			ackd10.openOutput();
		}
		if (isEQ(iz, 11)) {
			ackd11.openOutput();
		}
		if (isEQ(iz, 12)) {
			ackd12.openOutput();
		}
		if (isEQ(iz, 13)) {
			ackd13.openOutput();
		}
		if (isEQ(iz, 14)) {
			ackd14.openOutput();
		}
		if (isEQ(iz, 15)) {
			ackd15.openOutput();
		}
		if (isEQ(iz, 16)) {
			ackd16.openOutput();
		}
		if (isEQ(iz, 17)) {
			ackd17.openOutput();
		}
		if (isEQ(iz, 18)) {
			ackd18.openOutput();
		}
		if (isEQ(iz, 19)) {
			ackd19.openOutput();
		}
		if (isEQ(iz, 20)) {
			ackd20.openOutput();
		}
	}

	/**
	 * Implemented for MTL 398 - MPTD-45
	 * 
	 * @return
	 */

	protected void readFile2000() {
		readFile2010();
	}

	protected void readFile2010() {
		/* Now a block of records is fetched into the array. */
		/* Also on the first entry into the program we must set up the */
		/* WSAA-PREV-CHDRNUM = the present chdrnum. */
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(Varcom.endp);
			return;
		}
		if (isNE(wsaaInd, 1)) {
			return;
		}
		sqlerrorflag = false;
		try {
			for (ackdCursorLoopIndex = 1; isLTE(ackdCursorLoopIndex, wsaaRowsInBlock.toInt())
					&& getAppVars().fetchNext(sqllinsCursorrs); ackdCursorLoopIndex++) {
				getAppVars().getDBObject(sqllinsCursorrs, 1, wsaaChdrcoy[ackdCursorLoopIndex]);
				getAppVars().getDBObject(sqllinsCursorrs, 2, wsaaChdrnum[ackdCursorLoopIndex]);
				getAppVars().getDBObject(sqllinsCursorrs, 3, wsaadlvrmode[ackdCursorLoopIndex]);
				getAppVars().getDBObject(sqllinsCursorrs, 4, wsaadespdate[ackdCursorLoopIndex]);
				getAppVars().getDBObject(sqllinsCursorrs, 5, wsaapackdate[ackdCursorLoopIndex]);
				getAppVars().getDBObject(sqllinsCursorrs, 6, wsaadeemdate[ackdCursorLoopIndex]);
				getAppVars().getDBObject(sqllinsCursorrs, 7, wsaanextActDate[ackdCursorLoopIndex]);
				getAppVars().getDBObject(sqllinsCursorrs, 8, wsaaUniqNumber[ackdCursorLoopIndex]);
				logger.info("Fetch ChdrNum: {} ", wsaaChdrnum[ackdCursorLoopIndex].toString());

			}
		} catch (SQLException ex) {
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/* We must detect :- */
		/* a) no rows returned on the first fetch */
		/* b) the last row returned (in the block) */
		/* IF Either of the above cases occur, then an */
		/* SQLCODE = +100 is returned. */
		/* The 3000 section is continued for case (b). */
		if (isEQ(getAppVars().getSqlErrorCode(), 100) && isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(Varcom.endp);
		}
		// tom chi modified, bug 1030
		else if (isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(Varcom.endp);
		}
		// end
		else {
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}
	}

	protected void edit2500() {
		/* READ */
		wsspEdterror.set(Varcom.oK);
		/* EXIT */
	}

	protected void update3000() {
		/* UPDATE */
		wsaaInd.set(1);
		while (!(isGT(wsaaInd, wsaaRowsInBlock) || eofInBlock.isTrue())) {
			loadThreads3100();
		}

		/* Re-initialise the block for next fetch and point to first row. */
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)) {
			initialize(wsaaAckdpfData[wsaaInd.toInt()]);
		}
		wsaaInd.set(1);
		/* EXIT */
	}

	protected void loadThreads3100() {
		/* START */
		/* If the the CHDRNUM being processed is not equal to the */
		/* to the previous we should move to the next output file member. */
		/* The condition is here to allow for checking the last chdrnum */
		/* of the old block with the first of the new and to move to the */
		/* next ACKD member to write to if they have changed. */
		if (isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)) {
			wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
			iy.add(1);
		}
		/* Load from storage all PAYX data for the same contract until */
		/* the CHDRNUM on PAYX has changed, */
		/* OR */
		/* The end of an incomplete block is reached. */
		while (!(isGT(wsaaInd, wsaaRowsInBlock) || isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)
				|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}

		/* EXIT */
	}

	protected void loadSameContracts3200() {
		start3200();
	}

	protected void start3200() {
		ackdpfData.chdrcoy.set(wsaaChdrcoy[wsaaInd.toInt()]);
		ackdpfData.chdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
		ackdpfData.dlvrmode.set(wsaadlvrmode[wsaaInd.toInt()]);
		ackdpfData.despdate.set(wsaadespdate[wsaaInd.toInt()]);
		ackdpfData.packdate.set(wsaapackdate[wsaaInd.toInt()]);
		ackdpfData.deemdate.set(wsaadeemdate[wsaaInd.toInt()]);
		ackdpfData.nextActDate.set(wsaanextActDate[wsaaInd.toInt()]);
		ackdpfData.hpaduqn.set(wsaaUniqNumber[wsaaInd.toInt()]);
		if (isGT(iy, bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		logger.info("Writer ChdrNum: {} into ackd {} ", wsaaChdrnum[wsaaInd.toInt()].toString(), iy.toInt());
		/* Write records to their corresponding temporary file */
		/* members, determined by the working storage count, IY. */
		/* This spreads the transaction members evenly across */
		/* the thread members. */
		if (isEQ(iy, 1)) {
			ackd01.write(ackdpfData);
		} else if (isEQ(iy, 2)) {
			ackd02.write(ackdpfData);
		} else if (isEQ(iy, 3)) {
			ackd03.write(ackdpfData);
		} else if (isEQ(iy, 4)) {
			ackd04.write(ackdpfData);
		} else if (isEQ(iy, 5)) {
			ackd05.write(ackdpfData);
		} else if (isEQ(iy, 6)) {
			ackd06.write(ackdpfData);
		} else if (isEQ(iy, 7)) {
			ackd07.write(ackdpfData);
		} else if (isEQ(iy, 8)) {
			ackd08.write(ackdpfData);
		} else if (isEQ(iy, 9)) {
			ackd09.write(ackdpfData);
		} else if (isEQ(iy, 10)) {
			ackd10.write(ackdpfData);
		} else if (isEQ(iy, 11)) {
			ackd11.write(ackdpfData);
		} else if (isEQ(iy, 12)) {
			ackd12.write(ackdpfData);
		} else if (isEQ(iy, 13)) {
			ackd13.write(ackdpfData);
		} else if (isEQ(iy, 14)) {
			ackd14.write(ackdpfData);
		} else if (isEQ(iy, 15)) {
			ackd15.write(ackdpfData);
		} else if (isEQ(iy, 16)) {
			ackd16.write(ackdpfData);
		} else if (isEQ(iy, 17)) {
			ackd17.write(ackdpfData);
		} else if (isEQ(iy, 18)) {
			ackd18.write(ackdpfData);
		} else if (isEQ(iy, 19)) {
			ackd19.write(ackdpfData);
		} else if (isEQ(iy, 20)) {
			ackd20.write(ackdpfData);
		}
		/* Log the number of extacted records. */
		contot02++;
//		if (contot02 == wsaaRowsInBlock.toInt()) {
//			contotrec.totval.set(contot02);
//			contotrec.totno.set(ct02);
//			callContot001();
//			contot02 = 0;
//		}
		/* Set up the array for the next block of records. */
		wsaaInd.add(1);
		/* Check for an incomplete block retrieved. */
		if (isLTE(wsaaInd, wsaaRowsInBlock) && isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsaaEofInBlock.set("Y");
		}
	}

	protected void commit3500() {
		if (contot02 > 0) {
			contotrec.totval.set(contot02);
			contotrec.totno.set(ct02);
			callContot001();
			contot02 = 0;
		}
	}

	protected void rollback3600() {
		/* ROLLBACK */
		/** There is no pre-rollback processing to be done */
		/* EXIT */
	}

	protected void close4000() {
		/* CLOSE-FILES */
		/* Close the open files and remove the overrides. */
		getAppVars().freeDBConnectionIgnoreErr(sqllinsCursorconn, sqllinsCursorps, sqllinsCursorrs);
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)) {
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(Varcom.oK);
		/* EXIT */
	}

	protected void close4100() {
		close4110();
	}

	protected void close4110() {
		if (isEQ(iz, 1)) {
			ackd01.close();
		}
		if (isEQ(iz, 2)) {
			ackd02.close();
		}
		if (isEQ(iz, 3)) {
			ackd03.close();
		}
		if (isEQ(iz, 4)) {
			ackd04.close();
		}
		if (isEQ(iz, 5)) {
			ackd05.close();
		}
		if (isEQ(iz, 6)) {
			ackd06.close();
		}
		if (isEQ(iz, 7)) {
			ackd07.close();
		}
		if (isEQ(iz, 8)) {
			ackd08.close();
		}
		if (isEQ(iz, 9)) {
			ackd09.close();
		}
		if (isEQ(iz, 10)) {
			ackd10.close();
		}
		if (isEQ(iz, 11)) {
			ackd11.close();
		}
		if (isEQ(iz, 12)) {
			ackd12.close();
		}
		if (isEQ(iz, 13)) {
			ackd13.close();
		}
		if (isEQ(iz, 14)) {
			ackd14.close();
		}
		if (isEQ(iz, 15)) {
			ackd15.close();
		}
		if (isEQ(iz, 16)) {
			ackd16.close();
		}
		if (isEQ(iz, 17)) {
			ackd17.close();
		}
		if (isEQ(iz, 18)) {
			ackd18.close();
		}
		if (isEQ(iz, 19)) {
			ackd19.close();
		}
		if (isEQ(iz, 20)) {
			ackd20.close();
		}
	}

	protected void sqlError500() {
		/* CALL-SYSTEM-ERROR */
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
	private static final class WsaaT5679VarInner {
		FixedLengthStringData cnPremStats = new FixedLengthStringData(24);
		FixedLengthStringData[] cnPremStat = FLSArrayPartOfStructure(12, 2, cnPremStats, 0);
		FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(cnPremStats, 0, FILLER_REDEFINE);
		FixedLengthStringData cnPstat01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
		FixedLengthStringData cnPstat02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
		FixedLengthStringData cnPstat03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
		FixedLengthStringData cnPstat04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
		FixedLengthStringData cnPstat05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
		FixedLengthStringData cnPstat06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
		FixedLengthStringData cnPstat07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
		FixedLengthStringData cnPstat08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
		FixedLengthStringData cnPstat09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
		FixedLengthStringData cnPstat10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
		FixedLengthStringData cnPstat11 = new FixedLengthStringData(2).isAPartOf(filler, 20);
		FixedLengthStringData cnPstat12 = new FixedLengthStringData(2).isAPartOf(filler, 22);

		FixedLengthStringData cnRiskStats = new FixedLengthStringData(24);
		FixedLengthStringData[] cnRiskStat = FLSArrayPartOfStructure(12, 2, cnRiskStats, 0);
		FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(cnRiskStats, 0, FILLER_REDEFINE);
		FixedLengthStringData cnRiskStat01 = new FixedLengthStringData(2).isAPartOf(filler1, 0);
		FixedLengthStringData cnRiskStat02 = new FixedLengthStringData(2).isAPartOf(filler1, 2);
		FixedLengthStringData cnRiskStat03 = new FixedLengthStringData(2).isAPartOf(filler1, 4);
		FixedLengthStringData cnRiskStat04 = new FixedLengthStringData(2).isAPartOf(filler1, 6);
		FixedLengthStringData cnRiskStat05 = new FixedLengthStringData(2).isAPartOf(filler1, 8);
		FixedLengthStringData cnRiskStat06 = new FixedLengthStringData(2).isAPartOf(filler1, 10);
		FixedLengthStringData cnRiskStat07 = new FixedLengthStringData(2).isAPartOf(filler1, 12);
		FixedLengthStringData cnRiskStat08 = new FixedLengthStringData(2).isAPartOf(filler1, 14);
		FixedLengthStringData cnRiskStat09 = new FixedLengthStringData(2).isAPartOf(filler1, 16);
		FixedLengthStringData cnRiskStat10 = new FixedLengthStringData(2).isAPartOf(filler1, 18);
		FixedLengthStringData cnRiskStat11 = new FixedLengthStringData(2).isAPartOf(filler1, 20);
		FixedLengthStringData cnRiskStat12 = new FixedLengthStringData(2).isAPartOf(filler1, 22);
	}

}
