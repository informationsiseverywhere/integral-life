package com.csc.life.newbusiness.dataaccess;

import com.csc.life.reassurance.dataaccess.RactpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: RactlnbTableDAM.java
 * Date: Sun, 30 Aug 2009 03:45:30
 * Class transformed from RACTLNB.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class RactlnbTableDAM extends RactpfTableDAM {

	public RactlnbTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("RACTLNB");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", RASNUM"
		             + ", RATYPE";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "RASNUM, " +
		            "RATYPE, " +
		            "CURRFROM, " +
		            "VALIDFLAG, " +
		            "RAPAYFRQ, " +
		            "INSTPREM, " +
		            "SINGP, " +
		            "RAAMOUNT, " +
		            "CURRCODE, " +
		            "SEX, " +
		            "CRRCD, " +
		            "RCESDTE, " +
		            "BTDATE, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "TERMID, " +
		            "TRANNO, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "RASNUM ASC, " +
		            "RATYPE ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "RASNUM DESC, " +
		            "RATYPE DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               rasnum,
                               ratype,
                               currfrom,
                               validflag,
                               rapayfrq,
                               instprem,
                               singp,
                               raAmount,
                               currcode,
                               sex,
                               crrcd,
                               riskCessDate,
                               btdate,
                               transactionDate,
                               transactionTime,
                               user,
                               termid,
                               tranno,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(37);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getRasnum().toInternal()
					+ getRatype().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, rasnum);
			what = ExternalData.chop(what, ratype);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller7 = new FixedLengthStringData(4);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(life.toInternal());
	nonKeyFiller4.setInternal(coverage.toInternal());
	nonKeyFiller5.setInternal(rider.toInternal());
	nonKeyFiller6.setInternal(rasnum.toInternal());
	nonKeyFiller7.setInternal(ratype.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(146);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ nonKeyFiller5.toInternal()
					+ nonKeyFiller6.toInternal()
					+ nonKeyFiller7.toInternal()
					+ getCurrfrom().toInternal()
					+ getValidflag().toInternal()
					+ getRapayfrq().toInternal()
					+ getInstprem().toInternal()
					+ getSingp().toInternal()
					+ getRaAmount().toInternal()
					+ getCurrcode().toInternal()
					+ getSex().toInternal()
					+ getCrrcd().toInternal()
					+ getRiskCessDate().toInternal()
					+ getBtdate().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getTermid().toInternal()
					+ getTranno().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, nonKeyFiller7);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, rapayfrq);
			what = ExternalData.chop(what, instprem);
			what = ExternalData.chop(what, singp);
			what = ExternalData.chop(what, raAmount);
			what = ExternalData.chop(what, currcode);
			what = ExternalData.chop(what, sex);
			what = ExternalData.chop(what, crrcd);
			what = ExternalData.chop(what, riskCessDate);
			what = ExternalData.chop(what, btdate);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public FixedLengthStringData getRasnum() {
		return rasnum;
	}
	public void setRasnum(Object what) {
		rasnum.set(what);
	}
	public FixedLengthStringData getRatype() {
		return ratype;
	}
	public void setRatype(Object what) {
		ratype.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getRapayfrq() {
		return rapayfrq;
	}
	public void setRapayfrq(Object what) {
		rapayfrq.set(what);
	}	
	public PackedDecimalData getInstprem() {
		return instprem;
	}
	public void setInstprem(Object what) {
		setInstprem(what, false);
	}
	public void setInstprem(Object what, boolean rounded) {
		if (rounded)
			instprem.setRounded(what);
		else
			instprem.set(what);
	}	
	public PackedDecimalData getSingp() {
		return singp;
	}
	public void setSingp(Object what) {
		setSingp(what, false);
	}
	public void setSingp(Object what, boolean rounded) {
		if (rounded)
			singp.setRounded(what);
		else
			singp.set(what);
	}	
	public PackedDecimalData getRaAmount() {
		return raAmount;
	}
	public void setRaAmount(Object what) {
		setRaAmount(what, false);
	}
	public void setRaAmount(Object what, boolean rounded) {
		if (rounded)
			raAmount.setRounded(what);
		else
			raAmount.set(what);
	}	
	public FixedLengthStringData getCurrcode() {
		return currcode;
	}
	public void setCurrcode(Object what) {
		currcode.set(what);
	}	
	public FixedLengthStringData getSex() {
		return sex;
	}
	public void setSex(Object what) {
		sex.set(what);
	}	
	public PackedDecimalData getCrrcd() {
		return crrcd;
	}
	public void setCrrcd(Object what) {
		setCrrcd(what, false);
	}
	public void setCrrcd(Object what, boolean rounded) {
		if (rounded)
			crrcd.setRounded(what);
		else
			crrcd.set(what);
	}	
	public PackedDecimalData getRiskCessDate() {
		return riskCessDate;
	}
	public void setRiskCessDate(Object what) {
		setRiskCessDate(what, false);
	}
	public void setRiskCessDate(Object what, boolean rounded) {
		if (rounded)
			riskCessDate.setRounded(what);
		else
			riskCessDate.set(what);
	}	
	public PackedDecimalData getBtdate() {
		return btdate;
	}
	public void setBtdate(Object what) {
		setBtdate(what, false);
	}
	public void setBtdate(Object what, boolean rounded) {
		if (rounded)
			btdate.setRounded(what);
		else
			btdate.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		rasnum.clear();
		ratype.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		nonKeyFiller5.clear();
		nonKeyFiller6.clear();
		nonKeyFiller7.clear();
		currfrom.clear();
		validflag.clear();
		rapayfrq.clear();
		instprem.clear();
		singp.clear();
		raAmount.clear();
		currcode.clear();
		sex.clear();
		crrcd.clear();
		riskCessDate.clear();
		btdate.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		termid.clear();
		tranno.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}