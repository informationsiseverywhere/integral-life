package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:00
 * Description:
 * Copybook name: TT515REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tt515rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tt515Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData tdaynos = new FixedLengthStringData(285).isAPartOf(tt515Rec, 0);
  	public ZonedDecimalData[] tdayno = ZDArrayPartOfStructure(95, 3, 0, tdaynos, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(285).isAPartOf(tdaynos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData tdayno01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData tdayno02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData tdayno03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData tdayno04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public ZonedDecimalData tdayno05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData tdayno06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData tdayno07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData tdayno08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
  	public ZonedDecimalData tdayno09 = new ZonedDecimalData(3, 0).isAPartOf(filler, 24);
  	public ZonedDecimalData tdayno10 = new ZonedDecimalData(3, 0).isAPartOf(filler, 27);
  	public ZonedDecimalData tdayno11 = new ZonedDecimalData(3, 0).isAPartOf(filler, 30);
  	public ZonedDecimalData tdayno12 = new ZonedDecimalData(3, 0).isAPartOf(filler, 33);
  	public ZonedDecimalData tdayno13 = new ZonedDecimalData(3, 0).isAPartOf(filler, 36);
  	public ZonedDecimalData tdayno14 = new ZonedDecimalData(3, 0).isAPartOf(filler, 39);
  	public ZonedDecimalData tdayno15 = new ZonedDecimalData(3, 0).isAPartOf(filler, 42);
  	public ZonedDecimalData tdayno16 = new ZonedDecimalData(3, 0).isAPartOf(filler, 45);
  	public ZonedDecimalData tdayno17 = new ZonedDecimalData(3, 0).isAPartOf(filler, 48);
  	public ZonedDecimalData tdayno18 = new ZonedDecimalData(3, 0).isAPartOf(filler, 51);
  	public ZonedDecimalData tdayno19 = new ZonedDecimalData(3, 0).isAPartOf(filler, 54);
  	public ZonedDecimalData tdayno20 = new ZonedDecimalData(3, 0).isAPartOf(filler, 57);
  	public ZonedDecimalData tdayno21 = new ZonedDecimalData(3, 0).isAPartOf(filler, 60);
  	public ZonedDecimalData tdayno22 = new ZonedDecimalData(3, 0).isAPartOf(filler, 63);
  	public ZonedDecimalData tdayno23 = new ZonedDecimalData(3, 0).isAPartOf(filler, 66);
  	public ZonedDecimalData tdayno24 = new ZonedDecimalData(3, 0).isAPartOf(filler, 69);
  	public ZonedDecimalData tdayno25 = new ZonedDecimalData(3, 0).isAPartOf(filler, 72);
  	public ZonedDecimalData tdayno26 = new ZonedDecimalData(3, 0).isAPartOf(filler, 75);
  	public ZonedDecimalData tdayno27 = new ZonedDecimalData(3, 0).isAPartOf(filler, 78);
  	public ZonedDecimalData tdayno28 = new ZonedDecimalData(3, 0).isAPartOf(filler, 81);
  	public ZonedDecimalData tdayno29 = new ZonedDecimalData(3, 0).isAPartOf(filler, 84);
  	public ZonedDecimalData tdayno30 = new ZonedDecimalData(3, 0).isAPartOf(filler, 87);
  	public ZonedDecimalData tdayno31 = new ZonedDecimalData(3, 0).isAPartOf(filler, 90);
  	public ZonedDecimalData tdayno32 = new ZonedDecimalData(3, 0).isAPartOf(filler, 93);
  	public ZonedDecimalData tdayno33 = new ZonedDecimalData(3, 0).isAPartOf(filler, 96);
  	public ZonedDecimalData tdayno34 = new ZonedDecimalData(3, 0).isAPartOf(filler, 99);
  	public ZonedDecimalData tdayno35 = new ZonedDecimalData(3, 0).isAPartOf(filler, 102);
  	public ZonedDecimalData tdayno36 = new ZonedDecimalData(3, 0).isAPartOf(filler, 105);
  	public ZonedDecimalData tdayno37 = new ZonedDecimalData(3, 0).isAPartOf(filler, 108);
  	public ZonedDecimalData tdayno38 = new ZonedDecimalData(3, 0).isAPartOf(filler, 111);
  	public ZonedDecimalData tdayno39 = new ZonedDecimalData(3, 0).isAPartOf(filler, 114);
  	public ZonedDecimalData tdayno40 = new ZonedDecimalData(3, 0).isAPartOf(filler, 117);
  	public ZonedDecimalData tdayno41 = new ZonedDecimalData(3, 0).isAPartOf(filler, 120);
  	public ZonedDecimalData tdayno42 = new ZonedDecimalData(3, 0).isAPartOf(filler, 123);
  	public ZonedDecimalData tdayno43 = new ZonedDecimalData(3, 0).isAPartOf(filler, 126);
  	public ZonedDecimalData tdayno44 = new ZonedDecimalData(3, 0).isAPartOf(filler, 129);
  	public ZonedDecimalData tdayno45 = new ZonedDecimalData(3, 0).isAPartOf(filler, 132);
  	public ZonedDecimalData tdayno46 = new ZonedDecimalData(3, 0).isAPartOf(filler, 135);
  	public ZonedDecimalData tdayno47 = new ZonedDecimalData(3, 0).isAPartOf(filler, 138);
  	public ZonedDecimalData tdayno48 = new ZonedDecimalData(3, 0).isAPartOf(filler, 141);
  	public ZonedDecimalData tdayno49 = new ZonedDecimalData(3, 0).isAPartOf(filler, 144);
  	public ZonedDecimalData tdayno50 = new ZonedDecimalData(3, 0).isAPartOf(filler, 147);
  	public ZonedDecimalData tdayno51 = new ZonedDecimalData(3, 0).isAPartOf(filler, 150);
  	public ZonedDecimalData tdayno52 = new ZonedDecimalData(3, 0).isAPartOf(filler, 153);
  	public ZonedDecimalData tdayno53 = new ZonedDecimalData(3, 0).isAPartOf(filler, 156);
  	public ZonedDecimalData tdayno54 = new ZonedDecimalData(3, 0).isAPartOf(filler, 159);
  	public ZonedDecimalData tdayno55 = new ZonedDecimalData(3, 0).isAPartOf(filler, 162);
  	public ZonedDecimalData tdayno56 = new ZonedDecimalData(3, 0).isAPartOf(filler, 165);
  	public ZonedDecimalData tdayno57 = new ZonedDecimalData(3, 0).isAPartOf(filler, 168);
  	public ZonedDecimalData tdayno58 = new ZonedDecimalData(3, 0).isAPartOf(filler, 171);
  	public ZonedDecimalData tdayno59 = new ZonedDecimalData(3, 0).isAPartOf(filler, 174);
  	public ZonedDecimalData tdayno60 = new ZonedDecimalData(3, 0).isAPartOf(filler, 177);
  	public ZonedDecimalData tdayno61 = new ZonedDecimalData(3, 0).isAPartOf(filler, 180);
  	public ZonedDecimalData tdayno62 = new ZonedDecimalData(3, 0).isAPartOf(filler, 183);
  	public ZonedDecimalData tdayno63 = new ZonedDecimalData(3, 0).isAPartOf(filler, 186);
  	public ZonedDecimalData tdayno64 = new ZonedDecimalData(3, 0).isAPartOf(filler, 189);
  	public ZonedDecimalData tdayno65 = new ZonedDecimalData(3, 0).isAPartOf(filler, 192);
  	public ZonedDecimalData tdayno66 = new ZonedDecimalData(3, 0).isAPartOf(filler, 195);
  	public ZonedDecimalData tdayno67 = new ZonedDecimalData(3, 0).isAPartOf(filler, 198);
  	public ZonedDecimalData tdayno68 = new ZonedDecimalData(3, 0).isAPartOf(filler, 201);
  	public ZonedDecimalData tdayno69 = new ZonedDecimalData(3, 0).isAPartOf(filler, 204);
  	public ZonedDecimalData tdayno70 = new ZonedDecimalData(3, 0).isAPartOf(filler, 207);
  	public ZonedDecimalData tdayno71 = new ZonedDecimalData(3, 0).isAPartOf(filler, 210);
  	public ZonedDecimalData tdayno72 = new ZonedDecimalData(3, 0).isAPartOf(filler, 213);
  	public ZonedDecimalData tdayno73 = new ZonedDecimalData(3, 0).isAPartOf(filler, 216);
  	public ZonedDecimalData tdayno74 = new ZonedDecimalData(3, 0).isAPartOf(filler, 219);
  	public ZonedDecimalData tdayno75 = new ZonedDecimalData(3, 0).isAPartOf(filler, 222);
  	public ZonedDecimalData tdayno76 = new ZonedDecimalData(3, 0).isAPartOf(filler, 225);
  	public ZonedDecimalData tdayno77 = new ZonedDecimalData(3, 0).isAPartOf(filler, 228);
  	public ZonedDecimalData tdayno78 = new ZonedDecimalData(3, 0).isAPartOf(filler, 231);
  	public ZonedDecimalData tdayno79 = new ZonedDecimalData(3, 0).isAPartOf(filler, 234);
  	public ZonedDecimalData tdayno80 = new ZonedDecimalData(3, 0).isAPartOf(filler, 237);
  	public ZonedDecimalData tdayno81 = new ZonedDecimalData(3, 0).isAPartOf(filler, 240);
  	public ZonedDecimalData tdayno82 = new ZonedDecimalData(3, 0).isAPartOf(filler, 243);
  	public ZonedDecimalData tdayno83 = new ZonedDecimalData(3, 0).isAPartOf(filler, 246);
  	public ZonedDecimalData tdayno84 = new ZonedDecimalData(3, 0).isAPartOf(filler, 249);
  	public ZonedDecimalData tdayno85 = new ZonedDecimalData(3, 0).isAPartOf(filler, 252);
  	public ZonedDecimalData tdayno86 = new ZonedDecimalData(3, 0).isAPartOf(filler, 255);
  	public ZonedDecimalData tdayno87 = new ZonedDecimalData(3, 0).isAPartOf(filler, 258);
  	public ZonedDecimalData tdayno88 = new ZonedDecimalData(3, 0).isAPartOf(filler, 261);
  	public ZonedDecimalData tdayno89 = new ZonedDecimalData(3, 0).isAPartOf(filler, 264);
  	public ZonedDecimalData tdayno90 = new ZonedDecimalData(3, 0).isAPartOf(filler, 267);
  	public ZonedDecimalData tdayno91 = new ZonedDecimalData(3, 0).isAPartOf(filler, 270);
  	public ZonedDecimalData tdayno92 = new ZonedDecimalData(3, 0).isAPartOf(filler, 273);
  	public ZonedDecimalData tdayno93 = new ZonedDecimalData(3, 0).isAPartOf(filler, 276);
  	public ZonedDecimalData tdayno94 = new ZonedDecimalData(3, 0).isAPartOf(filler, 279);
  	public ZonedDecimalData tdayno95 = new ZonedDecimalData(3, 0).isAPartOf(filler, 282);
  	public FixedLengthStringData tyearnos = new FixedLengthStringData(190).isAPartOf(tt515Rec, 285);
  	public ZonedDecimalData[] tyearno = ZDArrayPartOfStructure(95, 2, 0, tyearnos, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(190).isAPartOf(tyearnos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData tyearno01 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData tyearno02 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 2);
  	public ZonedDecimalData tyearno03 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4);
  	public ZonedDecimalData tyearno04 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 6);
  	public ZonedDecimalData tyearno05 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 8);
  	public ZonedDecimalData tyearno06 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 10);
  	public ZonedDecimalData tyearno07 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData tyearno08 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 14);
  	public ZonedDecimalData tyearno09 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 16);
  	public ZonedDecimalData tyearno10 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 18);
  	public ZonedDecimalData tyearno11 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 20);
  	public ZonedDecimalData tyearno12 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 22);
  	public ZonedDecimalData tyearno13 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 24);
  	public ZonedDecimalData tyearno14 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 26);
  	public ZonedDecimalData tyearno15 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 28);
  	public ZonedDecimalData tyearno16 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 30);
  	public ZonedDecimalData tyearno17 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 32);
  	public ZonedDecimalData tyearno18 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 34);
  	public ZonedDecimalData tyearno19 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 36);
  	public ZonedDecimalData tyearno20 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 38);
  	public ZonedDecimalData tyearno21 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 40);
  	public ZonedDecimalData tyearno22 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 42);
  	public ZonedDecimalData tyearno23 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 44);
  	public ZonedDecimalData tyearno24 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 46);
  	public ZonedDecimalData tyearno25 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 48);
  	public ZonedDecimalData tyearno26 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 50);
  	public ZonedDecimalData tyearno27 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 52);
  	public ZonedDecimalData tyearno28 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 54);
  	public ZonedDecimalData tyearno29 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 56);
  	public ZonedDecimalData tyearno30 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 58);
  	public ZonedDecimalData tyearno31 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 60);
  	public ZonedDecimalData tyearno32 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 62);
  	public ZonedDecimalData tyearno33 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 64);
  	public ZonedDecimalData tyearno34 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 66);
  	public ZonedDecimalData tyearno35 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 68);
  	public ZonedDecimalData tyearno36 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 70);
  	public ZonedDecimalData tyearno37 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 72);
  	public ZonedDecimalData tyearno38 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 74);
  	public ZonedDecimalData tyearno39 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 76);
  	public ZonedDecimalData tyearno40 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 78);
  	public ZonedDecimalData tyearno41 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 80);
  	public ZonedDecimalData tyearno42 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 82);
  	public ZonedDecimalData tyearno43 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 84);
  	public ZonedDecimalData tyearno44 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 86);
  	public ZonedDecimalData tyearno45 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 88);
  	public ZonedDecimalData tyearno46 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 90);
  	public ZonedDecimalData tyearno47 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 92);
  	public ZonedDecimalData tyearno48 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 94);
  	public ZonedDecimalData tyearno49 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 96);
  	public ZonedDecimalData tyearno50 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 98);
  	public ZonedDecimalData tyearno51 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 100);
  	public ZonedDecimalData tyearno52 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 102);
  	public ZonedDecimalData tyearno53 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 104);
  	public ZonedDecimalData tyearno54 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 106);
  	public ZonedDecimalData tyearno55 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 108);
  	public ZonedDecimalData tyearno56 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 110);
  	public ZonedDecimalData tyearno57 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 112);
  	public ZonedDecimalData tyearno58 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 114);
  	public ZonedDecimalData tyearno59 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 116);
  	public ZonedDecimalData tyearno60 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 118);
  	public ZonedDecimalData tyearno61 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 120);
  	public ZonedDecimalData tyearno62 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 122);
  	public ZonedDecimalData tyearno63 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 124);
  	public ZonedDecimalData tyearno64 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 126);
  	public ZonedDecimalData tyearno65 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 128);
  	public ZonedDecimalData tyearno66 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 130);
  	public ZonedDecimalData tyearno67 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 132);
  	public ZonedDecimalData tyearno68 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 134);
  	public ZonedDecimalData tyearno69 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 136);
  	public ZonedDecimalData tyearno70 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 138);
  	public ZonedDecimalData tyearno71 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 140);
  	public ZonedDecimalData tyearno72 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 142);
  	public ZonedDecimalData tyearno73 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 144);
  	public ZonedDecimalData tyearno74 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 146);
  	public ZonedDecimalData tyearno75 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 148);
  	public ZonedDecimalData tyearno76 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 150);
  	public ZonedDecimalData tyearno77 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 152);
  	public ZonedDecimalData tyearno78 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 154);
  	public ZonedDecimalData tyearno79 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 156);
  	public ZonedDecimalData tyearno80 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 158);
  	public ZonedDecimalData tyearno81 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 160);
  	public ZonedDecimalData tyearno82 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 162);
  	public ZonedDecimalData tyearno83 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 164);
  	public ZonedDecimalData tyearno84 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 166);
  	public ZonedDecimalData tyearno85 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 168);
  	public ZonedDecimalData tyearno86 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 170);
  	public ZonedDecimalData tyearno87 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 172);
  	public ZonedDecimalData tyearno88 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 174);
  	public ZonedDecimalData tyearno89 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 176);
  	public ZonedDecimalData tyearno90 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 178);
  	public ZonedDecimalData tyearno91 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 180);
  	public ZonedDecimalData tyearno92 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 182);
  	public ZonedDecimalData tyearno93 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 184);
  	public ZonedDecimalData tyearno94 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 186);
  	public ZonedDecimalData tyearno95 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 188);
  	public ZonedDecimalData unit = new ZonedDecimalData(9, 0).isAPartOf(tt515Rec, 475);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(16).isAPartOf(tt515Rec, 484, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tt515Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tt515Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}