package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
public class Sr5ahScreenVars extends SmartVarModel {
	public FixedLengthStringData dataArea = new FixedLengthStringData(245);
	public FixedLengthStringData dataFields = new FixedLengthStringData(117).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData optdscs = new FixedLengthStringData(75).isAPartOf(dataFields, 42);
	public FixedLengthStringData[] optdsc = FLSArrayPartOfStructure(5, 15, optdscs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(75).isAPartOf(optdscs, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01 = DD.optdsc.copy().isAPartOf(filler,0);
	public FixedLengthStringData optdsc02 = DD.optdsc.copy().isAPartOf(filler,15);
	public FixedLengthStringData optdsc03 = DD.optdsc.copy().isAPartOf(filler,30);
	public FixedLengthStringData optdsc04 = DD.optdsc.copy().isAPartOf(filler,45);
	public FixedLengthStringData optdsc05 = DD.optdsc.copy().isAPartOf(filler,60);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(32).isAPartOf(dataArea, 117);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData optdscsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] optdscErr = FLSArrayPartOfStructure(5, 4, optdscsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(optdscsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData optdsc02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData optdsc03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData optdsc04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData optdsc05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 149);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData optdscsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] optdscOut = FLSArrayPartOfStructure(5, 12, optdscsOut, 0);
	public FixedLengthStringData[][] optdscO = FLSDArrayPartOfArrayStructure(12, 1, optdscOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(60).isAPartOf(optdscsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optdsc01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] optdsc02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] optdsc03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] optdsc04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] optdsc05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(163);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(65).isAPartOf(subfileArea, 0);
	public FixedLengthStringData excda = DD.excda.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,8);
	public ZonedDecimalData seqnbr = DD.seqnbr.copyToZonedDecimal().isAPartOf(subfileFields,9);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(subfileFields,12);
	public FixedLengthStringData prntstat = DD.prntstat.copy().isAPartOf(subfileFields,42);
	public ZonedDecimalData uniqueNum = DD.unique_number.copyToZonedDecimal().isAPartOf(subfileFields,57);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(24).isAPartOf(subfileArea, 65);
	public FixedLengthStringData excdaErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData seqnbrErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData prntstatErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData uniqueNumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(72).isAPartOf(subfileArea, 89);
	public FixedLengthStringData[] excdaOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] seqnbrOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] prntstatOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] uniqueNumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
    public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 161);
    
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	public LongData Sr5ahscreensflWritten = new LongData(0);
	public LongData Sr5ahscreenctlWritten = new LongData(0);
	public LongData Sr5ahscreenWritten = new LongData(0);
	public LongData Sr5ahprotectWritten = new LongData(0);
	public GeneralTable sr5ahscreensfl = new GeneralTable(AppVars.getInstance());
	
	public FixedLengthStringData noRecord = new FixedLengthStringData(1);
	public FixedLengthStringData isEnquiryMode = new FixedLengthStringData(1);
	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr5ahscreensfl;
	}

	public Sr5ahScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(excdaOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(selectOut,new String[] {"06","07","-06","07",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc01Out,new String[] {null,"08",null,null,null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc02Out,new String[] {null,"09",null,null,null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc03Out,new String[] {null,"10",null,null,null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {seqnbr, excda, longdesc, select,prntstat};
		screenSflOutFields = new BaseData[][] {seqnbrOut, excdaOut, longdescOut, selectOut,prntstatOut};
		screenSflErrFields = new BaseData[] {seqnbrErr, excdaErr, longdescErr, selectErr, prntstatErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, crtable, crtabdesc,optdsc01,optdsc02,optdsc03,optdsc04,optdsc05};
		screenOutFields = new BaseData[][] {chdrnumOut, crtableOut, crtabdescOut,optdsc01Out,optdsc02Out,optdsc03Out,optdsc04Out,optdsc05Out};
		screenErrFields = new BaseData[] {chdrnumErr, crtableErr, crtabdescErr,optdsc01Err,optdsc02Err,optdsc03Err,optdsc04Err,optdsc05Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr5ahscreen.class;
		screenSflRecord = Sr5ahscreensfl.class;
		screenCtlRecord = Sr5ahscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr5ahprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr5ahscreenctl.lrec.pageSubfile);
	}


}
