package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.dao.PayrlifDAO;
import com.csc.life.newbusiness.dataaccess.model.Payrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class PayrlifDAOImpl extends BaseDAOImpl<Payrpf> implements PayrlifDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(PayrlifDAOImpl.class);

	private static final String getUniqueAndFlagSQl = "Select UNIQUE_NUMBER, VALIDFLAG From PAYRLIF Where CHDRCOY = ? and CHDRNUM = ? order by UNIQUE_NUMBER desc";
	@SuppressWarnings("null")
	@Override
	public List<Payrpf> getUniqueAndFlag(String company, String chdrnum) {
		PreparedStatement ps = getPrepareStatement(getUniqueAndFlagSQl);
		ResultSet sqlRes = null;
		List<Payrpf> result = new ArrayList<Payrpf>();
		try {
			ps.setString(1, company);
			ps.setString(2, chdrnum);
			sqlRes = ps.executeQuery();
			while (sqlRes.next()) {
				Payrpf pf = new Payrpf();
				pf.setUnique_number(sqlRes.getLong("UNIQUE_NUMBER"));
				pf.setValidflag(sqlRes.getString("VALIDFLAG"));
				result.add(pf);
			}
		} catch (SQLException e) {
			LOGGER.error("getUniqueAndFlag()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, sqlRes);
		}
		
		return result;
	}

	private static final String deletByUnqiueSQL = "delete from PAYRPF where UNIQUE_NUMBER = ?";
	@Override
	public boolean deletByUnqiue(long uniqueNumber) {
		PreparedStatement ps = getPrepareStatement(deletByUnqiueSQL);
		int impactRec = 0;
		try {
			ps.setLong(1, uniqueNumber);
			impactRec = ps.executeUpdate();
			if (impactRec == 0) {
				return false;
			}
		} catch (SQLException e) {
			LOGGER.error("deletByUnqiue()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
		
		return true;
	}

	private static final String updateValidflag = "update PAYRPF set VALIDFLAG = ? where UNIQUE_NUMBER = ?";
	@Override
	public boolean updateValidflag(long uniqueNumber, String validflag) {
		PreparedStatement ps = getPrepareStatement(updateValidflag);
		int impactRec = 0;
		try {
			ps.setString(1, validflag);
			ps.setLong(2, uniqueNumber);
			impactRec = ps.executeUpdate();
			if (impactRec == 0) {
				return false;
			}
		} catch (SQLException e) {
			LOGGER.error("updateValidflag()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
		
		return true;
	}

}
