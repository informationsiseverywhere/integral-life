/*
 * File: Ph606.java
 * Date: 30 August 2009 1:10:31
 * Author: Quipoz Limited
 * 
 * Class transformed from PH606.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.screens.Sh606ScreenVars;
import com.csc.life.newbusiness.tablestructures.Th606rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* TH606 - BASIC ANNUAL PREMIUM PARAMETERS ( TERM ).
*
* * * * * * * * * * * *  W A R N I N G  * * * * * * * * * * * *
*
* EXTRA DATA SCREEN CONTAINS MORE THAN 500 BYTES - ( MAX SIZE OF
* ITEM-GENAREA ). - THEREFORE FIELDS HAVE BEEN CHANGED TO COMP-3.
*
* THIS GENERATED PROGRAM HAS BEEN AMENDED TO MAKE INDIVIDUAL FIELD
* MOVES TO AVOID CONVERSION OF COMP-3 FIELDS WHICH WAS CAUSING
* DECIMAL DATA ERRORS.
*
*****************************************************************
* </pre>
*/
public class Ph606 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH606");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private Th606rec th606rec = new Th606rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sh606ScreenVars sv = ScreenProgram.getScreenVars( Sh606ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1020, 
		exit3090
	}

	public Ph606() {
		super();
		screenVars = sv;
		new ScreenModel("Sh606", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case generalArea1020: 
					generalArea1020();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		th606rec.th606Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1020);
		}
		wsaaSub1.set(0);
		for (int loopVar1 = 0; !(loopVar1 == 99); loopVar1 += 1){
			initInsprms1500();
		}
		wsaaSub1.set(0);
		for (int loopVar2 = 0; !(loopVar2 == 11); loopVar2 += 1){
			initInstprs1700();
		}
		/*    MOVE ZERO                   TO TH606-INSTPR                  */
		/*                                   TH606-INSPREM         <V73L03>*/
		th606rec.insprem.set(ZERO);
		th606rec.mfacthm.set(ZERO);
		th606rec.mfacthy.set(ZERO);
		th606rec.mfactm.set(ZERO);
		th606rec.mfactq.set(ZERO);
		th606rec.mfactw.set(ZERO);
		th606rec.mfact2w.set(ZERO);
		th606rec.mfact4w.set(ZERO);
		th606rec.mfacty.set(ZERO);
		th606rec.premUnit.set(ZERO);
		th606rec.unit.set(ZERO);
	}

protected void generalArea1020()
	{
		sv.disccntmeth.set(th606rec.disccntmeth);
		wsaaSub1.set(0);
		for (int loopVar3 = 0; !(loopVar3 == 99); loopVar3 += 1){
			moveInsprms1600();
		}
		wsaaSub1.set(0);
		for (int loopVar4 = 0; !(loopVar4 == 11); loopVar4 += 1){
			moveInstprs1800();
		}
		/*    MOVE TH606-INSTPR           TO SH606-INSTPR.                 */
		sv.insprem.set(th606rec.insprem);
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.mfacthm.set(th606rec.mfacthm);
		sv.mfacthy.set(th606rec.mfacthy);
		sv.mfactm.set(th606rec.mfactm);
		sv.mfactq.set(th606rec.mfactq);
		sv.mfactw.set(th606rec.mfactw);
		sv.mfact2w.set(th606rec.mfact2w);
		sv.mfact4w.set(th606rec.mfact4w);
		sv.mfacty.set(th606rec.mfacty);
		sv.premUnit.set(th606rec.premUnit);
		sv.unit.set(th606rec.unit);
	}

protected void initInsprms1500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		th606rec.insprm[wsaaSub1.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void moveInsprms1600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		sv.insprm[wsaaSub1.toInt()].set(th606rec.insprm[wsaaSub1.toInt()]);
		/*EXIT*/
	}

protected void initInstprs1700()
	{
		/*PARA*/
		wsaaSub1.add(1);
		th606rec.instpr[wsaaSub1.toInt()].set(ZERO);
		/*EXIT*/
		}

protected void moveInstprs1800()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isEQ(th606rec.instpr[wsaaSub1.toInt()], NUMERIC)) {
			sv.instpr[wsaaSub1.toInt()].set(th606rec.instpr[wsaaSub1.toInt()]);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
	}

protected void screenEdit2000()
	{
					screenIo2010();
					exit2090();
				}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag,"I")) {
			return ;
		}
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			preparation3010();
		}
		catch (GOTOException e){
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
		checkChanges3100();
		itmdIO.setItemGenarea(th606rec.th606Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.disccntmeth,th606rec.disccntmeth)) {
			th606rec.disccntmeth.set(sv.disccntmeth);
		}
		wsaaSub1.set(0);
		for (int loopVar5 = 0; !(loopVar5 == 99); loopVar5 += 1){
			updateInsprms3500();
		}
		wsaaSub1.set(0);
		for (int loopVar6 = 0; !(loopVar6 == 11); loopVar6 += 1){
			updateInstprs3600();
		}
		/*    IF SH606-INSTPR             NOT = TH606-INSTPR               */
		/*       MOVE SH606-INSTPR        TO TH606-INSTPR                  */
		/*       MOVE 'Y' TO WSAA-UPDATE-FLAG.                             */
		if (isNE(sv.insprem,th606rec.insprem)) {
			th606rec.insprem.set(sv.insprem);
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
		}
		if (isNE(sv.mfacthm,th606rec.mfacthm)) {
			th606rec.mfacthm.set(sv.mfacthm);
		}
		if (isNE(sv.mfacthy,th606rec.mfacthy)) {
			th606rec.mfacthy.set(sv.mfacthy);
		}
		if (isNE(sv.mfactm,th606rec.mfactm)) {
			th606rec.mfactm.set(sv.mfactm);
		}
		if (isNE(sv.mfactq,th606rec.mfactq)) {
			th606rec.mfactq.set(sv.mfactq);
		}
		if (isNE(sv.mfactw,th606rec.mfactw)) {
			th606rec.mfactw.set(sv.mfactw);
		}
		if (isNE(sv.mfact2w,th606rec.mfact2w)) {
			th606rec.mfact2w.set(sv.mfact2w);
		}
		if (isNE(sv.mfact4w,th606rec.mfact4w)) {
			th606rec.mfact4w.set(sv.mfact4w);
		}
		if (isNE(sv.mfacty,th606rec.mfacty)) {
			th606rec.mfacty.set(sv.mfacty);
		}
		if (isNE(sv.premUnit,th606rec.premUnit)) {
			th606rec.premUnit.set(sv.premUnit);
		}
		if (isNE(sv.unit,th606rec.unit)) {
			th606rec.unit.set(sv.unit);
		}
	}

protected void updateInsprms3500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.insprm[wsaaSub1.toInt()],th606rec.insprm[wsaaSub1.toInt()])) {
			th606rec.insprm[wsaaSub1.toInt()].set(sv.insprm[wsaaSub1.toInt()]);
		}
		/*EXIT*/
	}

protected void updateInstprs3600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.instpr[wsaaSub1.toInt()], th606rec.instpr[wsaaSub1.toInt()])) {
			th606rec.instpr[wsaaSub1.toInt()].set(sv.instpr[wsaaSub1.toInt()]);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
