package com.csc.life.newbusiness.programs;

import com.csc.smart400framework.parent.Mainf;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class Pr58n extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR58N");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		
		/* FORMATS */
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	
	public Pr58n() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
{
	/*START*/
	wsspcomn.edterror.set(varcom.oK);
	/*EXIT*/
}

protected void screenEdit2000()
{
	/*START*/
	wsspcomn.edterror.set(varcom.oK);
	/*EXIT*/
}

protected void update3000()
{
	/*START*/
	wsspcomn.edterror.set(varcom.oK);
	/*EXIT*/
}

protected void whereNext4000()
{
	/*START*/
	wsspcomn.programPtr.add(1);
	/*EXIT*/
}
}