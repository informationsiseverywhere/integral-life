package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:31
 * Description:
 * Copybook name: TH601REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th601rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th601Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData lapdays = new FixedLengthStringData(16).isAPartOf(th601Rec, 0);
  	public ZonedDecimalData[] lapday = ZDArrayPartOfStructure(8, 2, 0, lapdays, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(lapdays, 0, FILLER_REDEFINE);
  	public ZonedDecimalData lapday01 = new ZonedDecimalData(2, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData lapday02 = new ZonedDecimalData(2, 0).isAPartOf(filler, 2);
  	public ZonedDecimalData lapday03 = new ZonedDecimalData(2, 0).isAPartOf(filler, 4);
  	public ZonedDecimalData lapday04 = new ZonedDecimalData(2, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData lapday05 = new ZonedDecimalData(2, 0).isAPartOf(filler, 8);
  	public ZonedDecimalData lapday06 = new ZonedDecimalData(2, 0).isAPartOf(filler, 10);
  	public ZonedDecimalData lapday07 = new ZonedDecimalData(2, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData lapday08 = new ZonedDecimalData(2, 0).isAPartOf(filler, 14);
  	public FixedLengthStringData weekss = new FixedLengthStringData(20).isAPartOf(th601Rec, 16);
  	public ZonedDecimalData[] weeks = ZDArrayPartOfStructure(10, 2, 0, weekss, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(weekss, 0, FILLER_REDEFINE);
  	public ZonedDecimalData weeks01 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData weeks02 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 2);
  	public ZonedDecimalData weeks03 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4);
  	public ZonedDecimalData weeks04 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 6);
  	public ZonedDecimalData weeks05 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 8);
  	public ZonedDecimalData weeks06 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 10);
  	public ZonedDecimalData weeks07 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData weeks08 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 14);
  	public ZonedDecimalData weeks09 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 16);
  	public ZonedDecimalData weeks10 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 18);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(464).isAPartOf(th601Rec, 36, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th601Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th601Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}