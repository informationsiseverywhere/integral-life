package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:43
 * Description:
 * Copybook name: HMTRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hmtrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hmtrFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hmtrKey = new FixedLengthStringData(64).isAPartOf(hmtrFileKey, 0, REDEFINE);
  	public FixedLengthStringData hmtrChdrcoy = new FixedLengthStringData(1).isAPartOf(hmtrKey, 0);
  	public FixedLengthStringData hmtrCntbranch = new FixedLengthStringData(2).isAPartOf(hmtrKey, 1);
  	public FixedLengthStringData hmtrCnttype = new FixedLengthStringData(3).isAPartOf(hmtrKey, 3);
  	public PackedDecimalData hmtrYear = new PackedDecimalData(4, 0).isAPartOf(hmtrKey, 6);
  	public PackedDecimalData hmtrMnth = new PackedDecimalData(2, 0).isAPartOf(hmtrKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(53).isAPartOf(hmtrKey, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hmtrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hmtrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}