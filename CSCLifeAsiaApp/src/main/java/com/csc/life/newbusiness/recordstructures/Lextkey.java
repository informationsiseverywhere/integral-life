package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:03
 * Description:
 * Copybook name: LEXTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lextkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lextFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lextKey = new FixedLengthStringData(64).isAPartOf(lextFileKey, 0, REDEFINE);
  	public FixedLengthStringData lextChdrcoy = new FixedLengthStringData(1).isAPartOf(lextKey, 0);
  	public FixedLengthStringData lextChdrnum = new FixedLengthStringData(8).isAPartOf(lextKey, 1);
  	public FixedLengthStringData lextLife = new FixedLengthStringData(2).isAPartOf(lextKey, 9);
  	public FixedLengthStringData lextCoverage = new FixedLengthStringData(2).isAPartOf(lextKey, 11);
  	public FixedLengthStringData lextRider = new FixedLengthStringData(2).isAPartOf(lextKey, 13);
  	public PackedDecimalData lextSeqnbr = new PackedDecimalData(3, 0).isAPartOf(lextKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(lextKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lextFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lextFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}