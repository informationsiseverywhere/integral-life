package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.tablestructures.Zvbrptrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.text.TextFileGenerator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
//import com.csc.text.file.TextFileGenerator;
/**
 * This subroutine is required to produce the report file(s) to be sent to the BCA bank.  
 */
public class Zvbrpt1  extends SMARTCodeModel{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Zvbrpt1.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Zvbrptrec zvbrptrec = new Zvbrptrec();
	//TMLII-2300 - increased length by 5 so that now have 7 space in format
//    private FixedLengthStringData virtualAccountNo =new FixedLengthStringData(18);
	private FixedLengthStringData virtualAccountNo =new FixedLengthStringData(23);
    FixedLengthStringData clintName = new FixedLengthStringData(30);
    String issuebank ="";
    private ZonedDecimalData noOfRecord = new ZonedDecimalData(7);
    private FixedLengthStringData statuz = new FixedLengthStringData(5);
    
    //TMLII-2300
    private static final String CONSTANT_ZEROES_16 = "0000000000000000";

    List<String> reportlist=new ArrayList<String>();
    ClntTableDAM clntIO= new ClntTableDAM();
    ChdrlnbTableDAM chdrIO= new ChdrlnbTableDAM();
    private String chdrrec = "CHDRREC";
    private String clntrec = "CLNTREC";
    File textFile = null;
    private FixedLengthStringData datime = new FixedLengthStringData(14);
    private FixedLengthStringData date = new FixedLengthStringData(8).isAPartOf(datime,0);
    private FixedLengthStringData year = new FixedLengthStringData(4).isAPartOf(date,0);
    private FixedLengthStringData month = new FixedLengthStringData(2).isAPartOf(date,4);
    private FixedLengthStringData dat = new FixedLengthStringData(2).isAPartOf(date,6);
	private FixedLengthStringData time = new FixedLengthStringData(6).isAPartOf(datime,8);
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
		}

		/**
		* The mainline method is the default entry point of the program when called by other programs using the
		* Quipoz runtime framework.
		*/
	public void mainline(Object... parmArray)
		{		
		datime = convertAndSetParam(datime, parmArray, 2);
		statuz = convertAndSetParam(statuz, parmArray, 1);
		zvbrptrec.zvbrptrec = convertAndSetParam(zvbrptrec.zvbrptrec, parmArray, 0);
		
		
			try {
				initialiseSection();
			}
			catch (COBOLExitProgramException e) {
				LOGGER.debug("Flow Ended");
			}
		}

	protected void initialiseSection()
	{

			process();
		
		
	}
	
	protected void process()
	{
		zvbrptrec.statuz.set(Varcom.oK);
		
		/**
		* The Virtual Account Number should be 23 characters long so 7 spaces are added to the end of the format to achieve this. 
		*/
		issuebank = zvbrptrec.issuebnk.trim();
		if(isEQ(statuz,"CLOSE"))
		{
			close();
			return;
		}
		if(isNE(zvbrptrec.chdrnum,SPACE))
		{
			virtualAccountNo.set(issuebank+"000"+zvbrptrec.chdrnum); 
			zvbrptrec.zvrtbnkac.set(virtualAccountNo);
				if(isEQ(statuz,"HEADR"))
				{
					writeHeader();
				}
				else if(isEQ(statuz,"DTAIL"))
				{
					writeDetailProcess();
				}				
		}	
	}	

	protected void writeHeader()
	{
		//TMLII-2311 START
//		String headerFormat= "0"+issuebank+"C"+dat+month+year;  //E.g. 0ABCDEC21062013
		String headerFormat= "0"+issuebank+"I"+dat+month+year;  //E.g. 0ABCDEC21062013
		//END
		reportlist.add(headerFormat);
		writeDetailProcess();
	}
	protected void writeDetailProcess()
	{
		noOfRecord.add(1);
		chdrIO.setDataArea(SPACES);
		chdrIO.setChdrcoy(zvbrptrec.chdrcoy);
		chdrIO.setChdrnum(zvbrptrec.chdrnum);
		chdrIO.setFormat(chdrrec);
		chdrIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, chdrIO);
		if (isNE(chdrIO.getStatuz(), Varcom.oK)) {
			zvbrptrec.statuz.set(clntIO.getStatuz());
			return;
		}
		else{
			
			clntIO.setDataArea(SPACES);
			clntIO.setClntnum(chdrIO.getCownnum());
			clntIO.setClntcoy(chdrIO.getCowncoy());
			clntIO.setClntpfx(chdrIO.getCownpfx());
			clntIO.setFormat(clntrec);
			clntIO.setFunction(Varcom.readr);
			SmartFileCode.execute(appVars, clntIO);
			if (isNE(clntIO.getStatuz(), Varcom.oK)) {
				zvbrptrec.statuz.set(clntIO.getStatuz());
				return;
			}
		}
		clintName.set(clntIO.getGivname().trim()+" "+clntIO.getSurname().trim());
		date.set(zvbrptrec.exprdate);
		//TMLII-2300 START
//		String detailFormat= "1"+issuebank+virtualAccountNo+"00000"+clintName+dat+month+year+zvbrptrec.billcurr.trim();   //E.g.  1ABCDEABCDE00012345678  000000John Smith                    21062023IDR
		String detailFormat= "1"+virtualAccountNo+"00000"+clintName+dat+month+year+zvbrptrec.billcurr.trim();   //E.g.  1ABCDE00012345678       000000John Smith                    21062023IDR
		//END
		reportlist.add(detailFormat);	
		
	}
	protected void writeFooter()
	{		
		//TMLII-2300
//		String footerFormat= "2"+issuebank.trim()+noOfRecord.toString();  //E.g. 2ABCDE1234567
		String footerFormat= "2"+issuebank.trim()+noOfRecord.toString()+CONSTANT_ZEROES_16;  //E.g. 2ABCDE12345670000000000000000

        reportlist.add(footerFormat);	
		
	}
	protected void close()
	{
		writeFooter();
		printTextFile();
	}
	
	 protected void printTextFile(){
		 
		 TextFileGenerator.produceTextFile(reportlist, "UPLDREQ"+dat+month+year+"_"+time);		 
	 }

	
}
