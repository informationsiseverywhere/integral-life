/*
 * File: P5306.java
 * Date: 30 August 2009 0:23:49
 * Author: Quipoz Limited
 * 
 * Class transformed from P5306.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.general.dataaccess.PovrgenTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.FluplnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HxclTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.UwrspfDAO;
import com.csc.life.newbusiness.dataaccess.model.Uwrspf;
import com.csc.life.newbusiness.recordstructures.Compdelrec;
import com.csc.life.newbusiness.screens.S5306ScreenVars;
import com.csc.life.productdefinition.dataaccess.MbnsTableDAM;
import com.csc.life.productdefinition.dataaccess.MinsTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.reassurance.dataaccess.RacdTableDAM;
import com.csc.life.reassurance.dataaccess.RacdlnbTableDAM;
import com.csc.life.terminationclaims.dataaccess.HbnfTableDAM;
import com.csc.life.terminationclaims.tablestructures.Tr686rec;
import com.csc.life.underwriting.dataaccess.UndcTableDAM;
import com.csc.life.underwriting.dataaccess.UndlTableDAM;
import com.csc.life.underwriting.dataaccess.UndqTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UnltunlTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*                      DELETE PROPOSAL COMPONENTS
*
*
*  PROGRAMMING NOTES
* --------------------
*
* INITIALISE
*
*    This program may be called to delete components down to
*    three levels.  These are as follows:
*
*    1) Delete all components for a life.
*
*    2) Delete all components for a coverage.
*
*    3) Delete all components for a rider.
*
* Work out which level of deletion is required as follows:
*
*    - attempt to retrieve the coverage/rider to be deleted
*      (covtlnb).
*
*    - if there is no coverage/rider record stored, components
*      for a life are to be deleted. Retrieve the key of the
*      life to be deleted (lifelnb).
*
*    If there is a coverage/rider record stored:
*       - if the rider-no is zero, components for a coverage
*         are to be deleted,
*       - if the rider-no is not zero, components for a rider
*         are to be deleted.
*
* Load the confirmation contract-no for the screen by stringing
* the appropriate parts of the "key" together with "/"s:
*
*      pppppp/ll/cc/rr
*
*  where:
*          pppppp = contract-no
*          ll     = life-no
*          cc     = coverage-no (not output if deleting a life)
*          rr     = rider-no (only output if deleting a rider)
*
* VALIDATION
* -----------
*
* If kill, skip the validation.
* The "are you sure" must be y or n.
*
*
* UPDATING
* --------
*
* If kill or "are you sure" was n, skip the updating.
*
* If life level:
*
*    - delete all lifelnb records for the company, contract-no,
*      life-no passed.
*
*    - delete any follow-ups which apply to the life number in
*      question (fluplnb).
*
*    - delete the file roles.
*
*
* For all levels:
*
*    - for (1) each coverage and rider attached to the life
*      or  (2) the coverage and all riders attached to it
*      or  (3) the relevant rider:
*
*    - delete all covtlnb records for the company, contract-no,
*      life-no, coverage-no, rider-no,
*
*    - delete any Reassurance records attached (RACTLNB),
*
*    - delete any special terms (LEXT),
*
*    - look up the transaction code/coverage/rider code on the
*      generic component control table (T5671), and call all
*      subroutines (which are not blank) passing the standard
*      linkage area:
*
*      company
*      contract number
*      life number
*      coverage number
*      rider number
*      status
*
* NEXT
* ----
*
* Add one to the program pointer and exit.
*
* Note that any records deletion introduced here should be in-line
* with the proposal plan type change program PR52U.
*
*
*****************************************************************
* </pre>
*/
public class P5306 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5306");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
		/* TABLES */
	private static final String t5671 = "T5671";
	private static final String tr686 = "TR686";
		/* ERRORS */
	private static final String e005 = "E005";
	private PackedDecimalData wsaaX = new PackedDecimalData(1, 0).setUnsigned();

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(21);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaForenum, 8).init(SPACES);
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2).isAPartOf(wsaaForenum, 10).init(SPACES);
	private FixedLengthStringData filler = new FixedLengthStringData(9).isAPartOf(wsaaForenum, 12, FILLER).init(SPACES);

	private FixedLengthStringData wsaaCurrentComponent = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCurrentComponent, 0);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).isAPartOf(wsaaCurrentComponent, 2);

	private FixedLengthStringData wsaaConcatName = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrancode = new FixedLengthStringData(4).isAPartOf(wsaaConcatName, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaConcatName, 4);
		/* WSAA-DELETE-KEY */
	private FixedLengthStringData wsaaChdrChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLifeLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCrtableDeleted = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaHbComponentFlag = new FixedLengthStringData(1);
	private Validator wsaaHbComponent = new Validator(wsaaHbComponentFlag, "Y");

	private FixedLengthStringData wsaaKey = new FixedLengthStringData(17);
	private FixedLengthStringData wsaaChdrnumo = new FixedLengthStringData(8).isAPartOf(wsaaKey, 0);
	private FixedLengthStringData wsaaX1 = new FixedLengthStringData(1).isAPartOf(wsaaKey, 8).init("/");
	private FixedLengthStringData wsaaLifeo = new FixedLengthStringData(2).isAPartOf(wsaaKey, 9);
	private FixedLengthStringData wsaaX2 = new FixedLengthStringData(1).isAPartOf(wsaaKey, 11);
	private FixedLengthStringData wsaaCovero = new FixedLengthStringData(2).isAPartOf(wsaaKey, 12);
	private FixedLengthStringData wsaaX3 = new FixedLengthStringData(1).isAPartOf(wsaaKey, 14);
	private FixedLengthStringData wsaaRidero = new FixedLengthStringData(2).isAPartOf(wsaaKey, 15);
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private FluplnbTableDAM fluplnbIO = new FluplnbTableDAM();
	private HbnfTableDAM hbnfIO = new HbnfTableDAM();
	private HxclTableDAM hxclIO = new HxclTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private MbnsTableDAM mbnsIO = new MbnsTableDAM();
	private MinsTableDAM minsIO = new MinsTableDAM();
	private PovrgenTableDAM povrgenIO = new PovrgenTableDAM();
	private RacdTableDAM racdIO = new RacdTableDAM();
	private RacdlnbTableDAM racdlnbIO = new RacdlnbTableDAM();
	private UndcTableDAM undcIO = new UndcTableDAM();
	private UndlTableDAM undlIO = new UndlTableDAM();
	private UndqTableDAM undqIO = new UndqTableDAM();
	private UnltunlTableDAM unltunlIO = new UnltunlTableDAM();
	private T5671rec t5671rec = new T5671rec();
	private Tr686rec tr686rec = new Tr686rec();
	private Compdelrec compdelrec = new Compdelrec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S5306ScreenVars sv = ScreenProgram.getScreenVars( S5306ScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	private UwrspfDAO uwrspfDAO = getApplicationContext().getBean("uwrspfDAO", UwrspfDAO.class);
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit3790, 
		exit3d90, 
		exit3e90
	}

	public P5306() {
		super();
		screenVars = sv;
		new ScreenModel("S5306", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		retrieveCovtlnb1020();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		wsaaCovrRider.set(SPACES);
		wsaaCovrCoverage.set(SPACES);
		wsaaX2.set(SPACES);
		wsaaX3.set(SPACES);
		wsaaCrtableDeleted.set(SPACES);
		wsaaBatckey.batcFileKey.set(wsspcomn.batchkey);
	}

	/**
	* <pre>
	*  Attempt to retrieve coverage/rider transaction record.
	*  If not found, this is a life level deletion.
	* </pre>
	*/
protected void retrieveCovtlnb1020()
	{
		covtlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)
		&& isNE(covtlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(covtlnbIO.getStatuz(), varcom.mrnf)) {
			retrieveLifelnb1300();
		}
		else {
			/* component record found*/
			/*  - component level deletion.*/
			wsaaChdrChdrcoy.set(covtlnbIO.getChdrcoy());
			wsaaChdrChdrnum.set(covtlnbIO.getChdrnum());
			wsaaLifeLife.set(covtlnbIO.getLife());
			wsaaCovrCoverage.set(covtlnbIO.getCoverage());
			wsaaCovrRider.set(covtlnbIO.getRider());
			covtlnbIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, covtlnbIO);
			if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covtlnbIO.getParams());
				fatalError600();
			}
		}
		wsaaCrtableDeleted.set(covtlnbIO.getCrtable());
		/*  Set up details of element to be deleted for confirmation.*/
		if (isEQ(wsaaCovrRider, "00")) {
			wsaaCovrRider.set(SPACES);
		}
		wsaaChdrnumo.set(wsaaChdrChdrnum);
		wsaaLifeo.set(wsaaLifeLife);
		wsaaCovero.set(wsaaCovrCoverage);
		wsaaRidero.set(wsaaCovrRider);
		if (isNE(wsaaCovrCoverage, SPACES)) {
			wsaaX2.set("/");
		}
		if (isNE(wsaaCovrRider, SPACES)) {
			wsaaX3.set("/");
		}
		sv.deltkey.set(wsaaKey);
	}

protected void retrieveLifelnb1300()
	{
		retrv1310();
	}

protected void retrv1310()
	{
		lifelnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		wsaaChdrChdrcoy.set(lifelnbIO.getChdrcoy());
		wsaaChdrChdrnum.set(lifelnbIO.getChdrnum());
		wsaaLifeLife.set(lifelnbIO.getLife());
		lifelnbIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
	}

protected void screenIo2010()
	{
		/*    CALL 'S5306IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5306-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "KILL")) {
			return ;
		}
		if (isNE(sv.select, "Y")
		&& isNE(sv.select, "N")) {
			sv.selectErr.set(e005);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*CHECK-REQUIRED*/
		/*   If kill or "are you sure" is 'n', skip the updating otherwise*/
		/*   update database.*/
		if (isEQ(scrnparams.statuz, varcom.kill)
		|| isNE(sv.select, "Y")) {
			return ;
		}
		if (isEQ(wsaaCovrRider, SPACES)
		&& isEQ(wsaaCovrCoverage, SPACES)) {
			deleteLife3100();
		}
		else {
			deleteAllComponents3400();
		}
		/*EXIT*/
	}

protected void deleteLife3100()
	{
		deleteLife3110();
	}

protected void deleteLife3110()
	{
		/*  - delete life*/
		lifelnbIO.setDataKey(SPACES);
		lifelnbIO.setChdrcoy(wsaaChdrChdrcoy);
		lifelnbIO.setChdrnum(wsaaChdrChdrnum);
		lifelnbIO.setLife(wsaaLifeLife);
		lifelnbIO.setJlife("00");
		lifeDeletion3200();
		/* Delete Life Underwriting record - if there is one               */
		undlIO.setDataKey(SPACES);
		undlIO.setChdrcoy(wsaaChdrChdrcoy);
		undlIO.setChdrnum(wsaaChdrChdrnum);
		undlIO.setLife(wsaaLifeLife);
		undlIO.setJlife("00");
		deleteUndl3a00();
		/*  Delete any Underwriting Question records for the Life          */
		undqIO.setDataKey(SPACES);
		undqIO.setChdrcoy(wsaaChdrChdrcoy);
		undqIO.setChdrnum(wsaaChdrChdrnum);
		undqIO.setLife("0");
		undqIO.setJlife("0");
		undqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		undqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		undqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		undqIO.setStatuz(varcom.oK);
		while ( !(isEQ(undqIO.getStatuz(), varcom.endp))) {
			deleteUndq3b00();
		}
		
		/*  - delete joint life (if it exits)*/
		lifelnbIO.setJlife("01");
		lifeDeletion3200();
		/*  - delete UNDL - joint life (if it exits)                       */
		undlIO.setJlife("01");
		deleteUndl3a00();
		/*  Delete any follow-ups set up specifically for the life*/
		fluplnbIO.setDataKey(SPACES);
		fluplnbIO.setChdrcoy(wsaaChdrChdrcoy);
		fluplnbIO.setChdrnum(wsaaChdrChdrnum);
		fluplnbIO.setFupno(0);
		fluplnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fluplnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fluplnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		fluplnbIO.setStatuz(varcom.oK);
		while ( !(isEQ(fluplnbIO.getStatuz(),varcom.endp))) {
			checkFollowups3300();
		}
		
		/*  Delete all coverages (and riders)*/
		deleteAllComponents3400();
	}

protected void lifeDeletion3200()
	{
			readLifeRecord3210();
		}

protected void readLifeRecord3210()
	{
		/*  - read  the life details using LIFELNB.*/
		lifelnbIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, lifelnbIO);
		/*  - skip deletion if optional joint life is not there.*/
		if (isEQ(lifelnbIO.getStatuz(), varcom.mrnf)
		&& isEQ(lifelnbIO.getJlife(), "01")) {
			return ;
		}
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		/* set up key to read client role record for deletion*/
		clrrIO.setClntpfx("CN");
		clrrIO.setClntcoy(wsspcomn.fsuco);
		clrrIO.setClntnum(lifelnbIO.getLifcnum());
		clrrIO.setClrrrole("LF");
		wsaaForenum.set(SPACES);
		wsaaChdrnum.set(lifelnbIO.getChdrnum());
		wsaaLife.set(lifelnbIO.getLife());
		wsaaJlife.set(SPACES);
		clrrIO.setForenum(wsaaForenum);
		clrrIO.setForepfx("CH");
		clrrIO.setForecoy(wsspcomn.company);
		/* delete life record previously read*/
		lifelnbIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		/* delete role record*/
		clrrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clrrIO.getParams());
			fatalError600();
		}
		clrrIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clrrIO.getParams());
			fatalError600();
		}
	}

protected void checkFollowups3300()
	{
		readFluplnb3310();
	}

protected void readFluplnb3310()
	{
		SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(), varcom.oK)
		&& isNE(fluplnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
		fluplnbIO.setFunction(varcom.nextr);
		if (isNE(fluplnbIO.getChdrcoy(), wsaaChdrChdrcoy)
		|| isNE(fluplnbIO.getChdrnum(), wsaaChdrChdrnum)) {
			fluplnbIO.setStatuz(varcom.endp);
		}
		if (isEQ(fluplnbIO.getStatuz(), varcom.endp)
		|| isNE(fluplnbIO.getLife(), wsaaLifeLife)) {
			/*    GO TO 3390-EXIT                                           */
			return ;
		}
		deleteAllHxcl4300();
		fluplnbIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
		fluplnbIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
		fluplnbIO.setFunction(varcom.nextr);
	}

protected void deleteAllComponents3400()
	{
		setUpKey3410();
	}

protected void setUpKey3410()
	{
		if (isEQ(wsaaCovrCoverage, SPACES)) {
			wsaaCovrCoverage.set("00");
		}
		if (isEQ(wsaaCovrRider, SPACES)) {
			wsaaCovrRider.set("00");
		}
		/* Delete all applicable coverage/rider transaction records.*/
		covtlnbIO.setDataKey(SPACES);
		wsaaCurrentComponent.set(SPACES);
		covtlnbIO.setChdrcoy(wsaaChdrChdrcoy);
		covtlnbIO.setChdrnum(wsaaChdrChdrnum);
		covtlnbIO.setLife(wsaaLifeLife);
		covtlnbIO.setCoverage(wsaaCovrCoverage);
		covtlnbIO.setRider(wsaaCovrRider);
		covtlnbIO.setSeqnbr(0);
		covtlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
		covtlnbIO.setStatuz(varcom.oK);
		while ( !(isEQ(covtlnbIO.getStatuz(),varcom.endp))) {
			componentDelet3500();
		}
		
		/* Delete all applicable Underwriting Coverage records             */
		undcIO.setDataKey(SPACES);
		undcIO.setChdrcoy(wsaaChdrChdrcoy);
		undcIO.setChdrnum(wsaaChdrChdrnum);
		undcIO.setLife(wsaaLifeLife);
		undcIO.setCoverage(wsaaCovrCoverage);
		undcIO.setRider(wsaaCovrRider);
		undcIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		undcIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		undcIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
		undcIO.setStatuz(varcom.oK);
		while ( !(isEQ(undcIO.getStatuz(),varcom.endp))) {
			deleteUndc3c00();
		}
		
		/* Delete record away from HBNF if any......                       */
		/* only for the component deleted is Hospital Benefit              */
		wsaaHbComponentFlag.set("N");
		checkHb8000();
		if (wsaaHbComponent.isTrue()) {
			hbnfDelete3600();
		}
		/* Delete the Benefit Schedule and Installment Payment records.    */
		mbnsIO.setParams(SPACES);
		mbnsIO.setChdrcoy(wsaaChdrChdrcoy);
		mbnsIO.setChdrnum(wsaaChdrChdrnum);
		mbnsIO.setLife(wsaaLifeLife);
		mbnsIO.setCoverage(wsaaCovrCoverage);
		mbnsIO.setRider(wsaaCovrRider);
		mbnsIO.setYrsinf(ZERO);
		mbnsIO.setFormat(formatsInner.mbnsrec);
		mbnsIO.setFunction(varcom.begn);
		while ( !(isEQ(mbnsIO.getStatuz(), varcom.endp))) {
			deleteMbns3d00();
		}
		
		deleteMins3e00();
		/* Delete all applicable special terms records.*/
		lextIO.setDataKey(SPACES);
		lextIO.setChdrcoy(wsaaChdrChdrcoy);
		lextIO.setChdrnum(wsaaChdrChdrnum);
		lextIO.setLife(wsaaLifeLife);
		lextIO.setCoverage(wsaaCovrCoverage);
		lextIO.setRider(wsaaCovrRider);
		lextIO.setSeqnbr(0);
		lextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
		lextIO.setStatuz(varcom.oK);
		while ( !(isEQ(lextIO.getStatuz(),varcom.endp))) {
			extrasDelet3800();
		}
		
		racdlnbIO.setParams(SPACES);
		racdlnbIO.setChdrcoy(wsaaChdrChdrcoy);
		racdlnbIO.setChdrnum(wsaaChdrChdrnum);
		racdlnbIO.setLife(wsaaLifeLife);
		racdlnbIO.setCoverage(wsaaCovrCoverage);
		racdlnbIO.setRider(wsaaCovrRider);
		racdlnbIO.setPlanSuffix(ZERO);
		racdlnbIO.setSeqno(ZERO);
		racdlnbIO.setFormat(formatsInner.racdlnbrec);
		racdlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "PLNSFX","SEQNO");
		while ( !(isEQ(racdlnbIO.getStatuz(),varcom.endp))) {
			reassuranceDelete3900();
		}
		
		/*                                                      <R96REA>*/
		/* Get the RACT record                                  <R96REA>*/
		/*                                                      <R96REA>*/
		/* MOVE WSAA-CHDR-CHDRCOY      TO RACTLNB-CHDRCOY.      <R96REA>*/
		/* MOVE WSAA-CHDR-CHDRNUM      TO RACTLNB-CHDRNUM.      <R96REA>*/
		/* MOVE WSAA-LIFE-LIFE         TO RACTLNB-LIFE.         <R96REA>*/
		/* MOVE WSAA-COVR-COVERAGE     TO RACTLNB-COVERAGE.     <R96REA>*/
		/* MOVE WSAA-COVR-RIDER        TO RACTLNB-RIDER.        <R96REA>*/
		/* MOVE RACTLNBREC             TO RACTLNB-FORMAT.       <R96REA>*/
		/* MOVE BEGN                   TO RACTLNB-FUNCTION.     <R96REA>*/
		/*                                                      <R96REA>*/
		/* MOVE O-K                    TO RACTLNB-STATUZ.       <R96REA>*/
		/*                                                      <R96REA>*/
		/* PERFORM 3900-REASSURANCE-DELETE                      <R96REA>*/
		/*                             UNTIL RACTLNB-STATUZ = EN<R96REA>*/
		/*                                                      <R96REA>*/
		/* MOVE WSAA-CHDR-CHDRCOY       TO RACT-CHDRCOY.           <001>*/
		/* MOVE WSAA-CHDR-CHDRNUM       TO RACT-CHDRNUM.           <001>*/
		/* MOVE WSAA-LIFE-LIFE          TO RACT-LIFE.              <001>*/
		/* MOVE WSAA-COVR-COVERAGE      TO RACT-COVERAGE.          <001>*/
		/* MOVE WSAA-COVR-RIDER         TO RACT-RIDER.             <001>*/
		/* MOVE RACTREC                 TO RACT-FORMAT.            <001>*/
		/* MOVE BEGN                    TO RACT-FUNCTION.          <001>*/
		/*                                                         <001>*/
		/* MOVE O-K                     TO RACT-STATUZ.            <001>*/
		/* PERFORM 3900-REASSURANCE-DELETE.                        <001>*/
		/*            UNTIL RACT-STATUZ    = ENDP.                         */
		/* Delete all applicable unit linked fund records.                 */
		unltunlIO.setChdrcoy(wsaaChdrChdrcoy);
		unltunlIO.setChdrnum(wsaaChdrChdrnum);
		unltunlIO.setLife(wsaaLifeLife);
		unltunlIO.setCoverage(wsaaCovrCoverage);
		unltunlIO.setRider(wsaaCovrRider);
		unltunlIO.setSeqnbr(ZERO);
		unltunlIO.setFormat(formatsInner.unltunlrec);
		unltunlIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		unltunlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		unltunlIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		unitFundDelet4100();
		/* Delete all associated premium breakdown records......           */
		povrgenIO.setParams(SPACES);
		povrgenIO.setChdrcoy(wsaaChdrChdrcoy);
		povrgenIO.setChdrnum(wsaaChdrChdrnum);
		povrgenIO.setLife(wsaaLifeLife);
		povrgenIO.setCoverage(wsaaCovrCoverage);
		povrgenIO.setRider(wsaaCovrRider);
		povrgenIO.setPlanSuffix(ZERO);
		povrgenIO.setFormat(formatsInner.povrgenrec);
		povrgenIO.setFunction(varcom.begnh);
		while ( !(isEQ(povrgenIO.getStatuz(), varcom.endp))) {
			getPovrgen4200();
		}
		
		/* Release the record regardless......                             */
		povrgenIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, povrgenIO);
		if (isNE(povrgenIO.getStatuz(), varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
	}

protected void componentDelet3500()
	{
		readCovt3510();
	}

protected void readCovt3510()
	{
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)
		&& isNE(covtlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isNE(wsaaChdrChdrcoy, covtlnbIO.getChdrcoy())
		|| isNE(wsaaChdrChdrnum, covtlnbIO.getChdrnum())
		|| isNE(wsaaLifeLife, covtlnbIO.getLife())
		|| (isNE(wsaaCovrCoverage, "00")
		&& isNE(wsaaCovrCoverage, covtlnbIO.getCoverage()))
		|| (isNE(wsaaCovrRider, "00")
		&& isNE(wsaaCovrRider, covtlnbIO.getRider()))) {
			covtlnbIO.setStatuz(varcom.endp);
		}
		if (isEQ(covtlnbIO.getStatuz(), varcom.endp)) {
			genericDeletion3700();
			return ;
		}
		/*  If very first component read, save component key.*/
		if (isEQ(wsaaCurrentComponent, SPACES)) {
			wsaaCoverage.set(covtlnbIO.getCoverage());
			wsaaRider.set(covtlnbIO.getRider());
			readGenericTable3600();
		}
		/*  On change of component, call generic deletion programs.*/
		if (isNE(covtlnbIO.getCoverage(), wsaaCoverage)
		|| isNE(covtlnbIO.getRider(), wsaaRider)) {
			genericDeletion3700();
			wsaaCoverage.set(covtlnbIO.getCoverage());
			wsaaRider.set(covtlnbIO.getRider());
			readGenericTable3600();
		}
		/*  Delete coverage transaction record*/
		covtlnbIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		covtlnbIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		// IBPLIFE-1490
		Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
		boolean uwFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP123", appVars, smtpfxcpy.item.toString());
		if(uwFlag) {
			deleteUWRec();
		}
		covtlnbIO.setFunction(varcom.nextr);
	}
private void deleteUWRec() {
	Uwrspf uwrspf = new Uwrspf();
	uwrspf.setChdrcoy(covtlnbIO.getChdrcoy().toString());
	uwrspf.setChdrnum(covtlnbIO.getChdrnum().toString());
	uwrspf.setCrtable(covtlnbIO.getCrtable().toString());
	uwrspfDAO.deleteUwrsData(uwrspf);
}
protected void hbnfDelete3600()
	{
		delHbnf3610();
	}

protected void delHbnf3610()
	{
		hbnfIO.setFunction(varcom.rlse);
		hbnfIO.setFormat(formatsInner.hbnfrec);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(), varcom.oK)
		&& isNE(hbnfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}
		hbnfIO.setChdrcoy(wsaaChdrChdrcoy);
		hbnfIO.setChdrnum(wsaaChdrChdrnum);
		hbnfIO.setLife(covtlnbIO.getLife());
		hbnfIO.setCoverage(covtlnbIO.getCoverage());
		hbnfIO.setRider(covtlnbIO.getRider());


		hbnfIO.setFunction(varcom.readr);
		hbnfIO.setFormat(formatsInner.hbnfrec);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(), varcom.oK)
		&& isNE(hbnfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}
		if (isEQ(hbnfIO.getStatuz(), varcom.oK)) {
			hbnfIO.setFunction(varcom.delet);
			SmartFileCode.execute(appVars, hbnfIO);
			if (isNE(hbnfIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(hbnfIO.getParams());
				fatalError600();
			}
		}
	}

protected void readGenericTable3600()
	{
		readT56713600();
	}

protected void readT56713600()
	{
		/*    Read the  Program  table T5671 for  the components programs*/
		/*    to be executed next.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsaaChdrChdrcoy);
		itemIO.setItemtabl(t5671);
		wsaaTrancode.set(wsaaBatckey.batcBatctrcde);
		wsaaCrtable.set(covtlnbIO.getCrtable());
		itemIO.setItemitem(wsaaConcatName);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.mrnf)
		&& isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
	}

protected void genericDeletion3700()
	{
		try {
			setUpLinkage3710();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void setUpLinkage3710()
	{
		compdelrec.compdelRec.set(SPACES);
		compdelrec.chdrcoy.set(wsaaChdrChdrcoy);
		compdelrec.chdrnum.set(wsaaChdrChdrnum);
		compdelrec.life.set(wsaaLifeLife);
		compdelrec.coverage.set(wsaaCoverage);
		compdelrec.rider.set(wsaaRider);
		wsaaX.set(0);
		for (int loopVar1 = 0; !(loopVar1 == 4); loopVar1 += 1){
			callProgram3720();
		}
		goTo(GotoLabel.exit3790);
	}

protected void callProgram3720()
	{
		wsaaX.add(1);
		if (isNE(t5671rec.subprog[wsaaX.toInt()], SPACES)) {
			callProgram(t5671rec.subprog[wsaaX.toInt()], compdelrec.compdelRec);
			if (isNE(compdelrec.statuz, varcom.oK)) {
				syserrrec.params.set(compdelrec.compdelRec);
				syserrrec.statuz.set(compdelrec.statuz);
				fatalError600();
			}
		}
	}

protected void extrasDelet3800()
	{
		readLext3810();
	}

protected void readLext3810()
	{
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)
		&& isNE(lextIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		if (isNE(wsaaChdrChdrcoy, lextIO.getChdrcoy())
		|| isNE(wsaaChdrChdrnum, lextIO.getChdrnum())
		|| isNE(wsaaLifeLife, lextIO.getLife())
		|| (isNE(wsaaCovrCoverage, "00")
		&& isNE(wsaaCovrCoverage, lextIO.getCoverage()))
		|| (isNE(wsaaCovrRider, "00")
		&& isNE(wsaaCovrRider, lextIO.getRider()))) {
			lextIO.setStatuz(varcom.endp);
		}
		if (isEQ(lextIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/*  Delete extras record*/
		lextIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		lextIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		lextIO.setFunction(varcom.nextr);
	}

protected void reassuranceDelete3900()
	{
		racd3910();
	}

	/**
	* <pre>
	*********************************                         <R96REA>
	* </pre>
	*/
protected void racd3910()
	{
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(), varcom.oK)
		&& isNE(racdlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(racdlnbIO.getParams());
			fatalError600();
		}
		if (isNE(racdlnbIO.getChdrcoy(), wsaaChdrChdrcoy)
		|| isNE(racdlnbIO.getChdrnum(), wsaaChdrChdrnum)
		|| isNE(racdlnbIO.getLife(), wsaaLifeLife)
		|| (isNE(racdlnbIO.getCoverage(), wsaaCovrCoverage)
		&& isNE(wsaaCovrCoverage, "00"))
		|| (isNE(racdlnbIO.getRider(), wsaaCovrRider)
		&& isNE(wsaaCovrRider, "00"))
		|| isNE(racdlnbIO.getPlanSuffix(), ZERO)
		|| isNE(racdlnbIO.getSeqno(), ZERO)
		|| isEQ(racdlnbIO.getStatuz(), varcom.endp)) {
			racdlnbIO.setStatuz(varcom.endp);
			return ;
		}
		racdIO.setParams(SPACES);
		racdIO.setRrn(racdlnbIO.getRrn());
		racdIO.setFormat(formatsInner.racdrec);
		racdIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, racdIO);
		if (isNE(racdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdIO.getParams());
			fatalError600();
		}
		racdIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, racdIO);
		if (isNE(racdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdIO.getParams());
			fatalError600();
		}
		racdlnbIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	*3900-REASSURANCE-DELETE SECTION.                         <R96REA>
	*********************************                         <R96REA>
	****                                                      <R96REA>
	*3901-PARA.                                               <R96REA>
	****                                                      <R96REA>
	**** CALL 'RACTLNBIO'            USING RACTLNB-PARAMS.    <R96REA>
	****                                                      <R96REA>
	**** IF RACTLNB-STATUZ           NOT = O-K                <R96REA>
	****                             AND NOT = ENDP           <R96REA>
	***      MOVE RACTLNB-PARAMS     TO SYSR-PARAMS           <R96REA>
	****     MOVE RACTLNB-STATUZ     TO SYSR-STATUZ           <R96REA>
	****     PERFORM 600-FATAL-ERROR.                         <R96REA>
	****                                                      <R96REA>
	**** IF WSAA-CHDR-CHDRCOY        NOT = RACTLNB-CHDRCOY OR <R96REA>
	****    WSAA-CHDR-CHDRNUM        NOT = RACTLNB-CHDRNUM OR <R96REA>
	****    WSAA-LIFE-LIFE           NOT = RACTLNB-LIFE OR    <R96REA>
	****    ( WSAA-COVR-COVERAGE     NOT = '00' AND           <R96REA>
	****      WSAA-COVR-COVERAGE     NOT = RACTLNB-COVERAGE ) <R96REA>
	****    ( WSAA-COVR-RIDER        NOT = '00' AND           <R96REA>
	****      WSAA-COVR-RIDER        NOT = RACTLNB-RIDER)     <R96REA>
	****     MOVE ENDP               TO RACTLNB-STATUZ.       <R96REA>
	****                                                      <R96REA>
	**** If RACT record not found then go to exit.            <R96REA>
	****                                                      <R96REA>
	**** IF RACTLNB-STATUZ           = ENDP                   <R96REA>
	****    GO TO 3990-EXIT.                                  <R96REA>
	****                                                      <R96REA>
	**** If the record was found then we must delete the RACT <R96REA>
	**** and in order to delete it we must hold it.           <R96REA>
	****                                                      <R96REA>
	**** MOVE READH                  TO RACTLNB-FUNCTION.     <R96REA>
	****                                                      <R96REA>
	**** CALL 'RACTLNBIO'            USING RACTLNB-PARAMS.    <R96REA>
	****                                                      <R96REA>
	**** IF RACTLNB-STATUZ           NOT = O-K                <R96REA>
	****     MOVE RACTLNB-PARAMS     TO SYSR-PARAMS           <R96REA>
	****     MOVE RACTLNB-STATUZ     TO SYSR-STATUZ           <R96REA>
	****     PERFORM 600-FATAL-ERROR.                         <R96REA>
	****                                                      <R96REA>
	**** MOVE DELET                  TO RACTLNB-FUNCTION.     <R96REA>
	****                                                      <R96REA>
	**** CALL 'RACTLNBIO'            USING RACTLNB-PARAMS.    <R96REA>
	****                                                      <R96REA>
	**** IF RACTLNB-STATUZ           NOT = O-K                <R96REA>
	****     MOVE RACTLNB-PARAMS     TO SYSR-PARAMS           <R96REA>
	****     MOVE RACTLNB-STATUZ     TO SYSR-STATUZ           <R96REA>
	****     PERFORM 600-FATAL-ERROR.                         <R96REA>
	****                                                      <R96REA>
	**** MOVE NEXTR                  TO RACTLNB-FUNCTION.     <R96REA>
	****                                                      <R96REA>
	****                                                      <R96REA>
	**** CALL 'RACTIO'                USING RACT-PARAMS.         <001>
	****                                                         <001>
	**** IF RACT-STATUZ               NOT = O-K                  <001>
	****                          AND NOT = ENDP                 <001>
	****    MOVE RACT-PARAMS          TO SYSR-PARAMS             <001>
	****    PERFORM                   600-FATAL-ERROR.           <001>
	****                                                         <001>
	**** IF WSAA-CHDR-CHDRCOY   NOT = RACT-CHDRCOY OR            <001>
	****    WSAA-CHDR-CHDRNUM   NOT = RACT-CHDRNUM OR            <001>
	****    WSAA-LIFE-LIFE      NOT = RACT-LIFE OR               <001>
	****    (WSAA-COVR-COVERAGE  NOT = '00' AND                  <001>
	****     WSAA-COVR-COVERAGE  NOT = RACT-COVERAGE) OR         <001>
	****    (WSAA-COVR-RIDER     NOT = '00' AND                  <001>
	****     WSAA-COVR-RIDER     NOT = RACT-RIDER)               <001>
	****    MOVE ENDP                TO RACT-STATUZ.             <001>
	****                                                         <001>
	**                                                           <001>
	**   If RACT record not found then go to exit.               <001>
	**                                                           <001>
	**** IF RACT-STATUZ               = ENDP                     <001>
	****    GO                        TO 3990-EXIT.              <001>
	****                                                         <001>
	**                                                           <001>
	**   If the record was found then we must delete the RACT  record 
	**   and in order to delete it we must hold it.              <001>
	**                                                           <001>
	****                                                         <001>
	**** MOVE READH                   TO RACT-FUNCTION.          <001>
	****                                                         <001>
	**** CALL 'RACTIO'                USING RACT-PARAMS.         <001>
	****                                                         <001>
	**** IF RACT-STATUZ               NOT = O-K                  <001>
	****    MOVE RACT-PARAMS          TO SYSR-PARAMS             <001>
	****    PERFORM                   600-FATAL-ERROR.           <001>
	****                                                         <001>
	****                                                         <001>
	**** MOVE DELET                   TO RACT-FUNCTION.          <001>
	****                                                         <001>
	**** CALL 'RACTIO'                USING RACT-PARAMS.         <001>
	****                                                         <001>
	**** IF RACT-STATUZ               NOT = O-K                  <001>
	****    MOVE RACT-PARAMS          TO SYSR-PARAMS             <001>
	****    PERFORM                   600-FATAL-ERROR.           <001>
	*                                                            <001>
	*3990-EXIT.                                                  <001>
	*     EXIT.                                                  <001>
	* </pre>
	*/
protected void deleteUndl3a00()
	{
		init3a10();
	}

protected void init3a10()
	{
		/*  - read the Underwriting Life details using UNDL.               */
		undlIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, undlIO);
		/*  - skip deletion if record not there                            */
		if (isEQ(undlIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		if (isNE(undlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(undlIO.getParams());
			fatalError600();
		}
		/* delete underwriting life record previously read                 */
		undlIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, undlIO);
		if (isNE(undlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(undlIO.getParams());
			fatalError600();
		}
	}

protected void deleteUndq3b00()
	{
		init3b10();
	}

protected void init3b10()
	{
		SmartFileCode.execute(appVars, undqIO);
		if (isNE(undqIO.getStatuz(), varcom.oK)
		&& isNE(undqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(undqIO.getParams());
			fatalError600();
		}
		undqIO.setFunction(varcom.nextr);
		if (isNE(undqIO.getChdrcoy(), wsaaChdrChdrcoy)
		|| isNE(undqIO.getChdrnum(), wsaaChdrChdrnum)) {
			undqIO.setStatuz(varcom.endp);
		}
		if (isEQ(undqIO.getStatuz(), varcom.endp)
		|| isNE(undqIO.getLife(), wsaaLifeLife)) {
			return ;
		}
		undqIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, undqIO);
		if (isNE(undqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(undqIO.getParams());
			fatalError600();
		}
		undqIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, undqIO);
		if (isNE(undqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(undqIO.getParams());
			fatalError600();
		}
		undqIO.setFunction(varcom.nextr);
	}

protected void deleteUndc3c00()
	{
		init3c10();
	}

protected void init3c10()
	{
		SmartFileCode.execute(appVars, undcIO);
		if (isNE(undcIO.getStatuz(), varcom.oK)
		&& isNE(undcIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(undcIO.getParams());
			fatalError600();
		}
		if (isNE(wsaaChdrChdrcoy, undcIO.getChdrcoy())
		|| isNE(wsaaChdrChdrnum, undcIO.getChdrnum())
		|| isNE(wsaaLifeLife, undcIO.getLife())
		|| (isNE(wsaaCovrCoverage, "00")
		&& isNE(wsaaCovrCoverage, undcIO.getCoverage()))
		|| (isNE(wsaaCovrRider, "00")
		&& isNE(wsaaCovrRider, undcIO.getRider()))) {
			undcIO.setStatuz(varcom.endp);
		}
		if (isEQ(undcIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/*  Delete UNDC record                                             */
		undcIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, undcIO);
		if (isNE(undcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(undcIO.getParams());
			fatalError600();
		}
		undcIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, undcIO);
		if (isNE(undcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(undcIO.getParams());
			fatalError600();
		}
		undcIO.setFunction(varcom.nextr);
	}

protected void deleteMbns3d00()
	{
		try {
			call3d10();
			delete3d20();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void call3d10()
	{
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(), varcom.oK)
		&& isNE(mbnsIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(mbnsIO.getStatuz());
			syserrrec.params.set(mbnsIO.getParams());
			fatalError600();
		}
		if (isEQ(mbnsIO.getStatuz(), varcom.endp)
		|| isNE(mbnsIO.getRider(), wsaaCovrRider)
		|| isNE(mbnsIO.getCoverage(), wsaaCovrCoverage)
		|| isNE(mbnsIO.getLife(), wsaaLifeLife)
		|| isNE(mbnsIO.getChdrnum(), wsaaChdrChdrnum)
		|| isNE(mbnsIO.getChdrcoy(), wsaaChdrChdrcoy)) {
			mbnsIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3d90);
		}
	}

protected void delete3d20()
	{
		mbnsIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(mbnsIO.getStatuz());
			syserrrec.params.set(mbnsIO.getParams());
			fatalError600();
		}
		mbnsIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(mbnsIO.getStatuz());
			syserrrec.params.set(mbnsIO.getParams());
			fatalError600();
		}
		mbnsIO.setFunction(varcom.nextr);
	}

protected void deleteMins3e00()
	{
		try {
			init3e10();
			call3e20();
			delete3e30();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void init3e10()
	{
		minsIO.setParams(SPACES);
		minsIO.setChdrcoy(wsaaChdrChdrcoy);
		minsIO.setChdrnum(wsaaChdrChdrnum);
		minsIO.setLife(wsaaLifeLife);
		minsIO.setCoverage(wsaaCovrCoverage);
		minsIO.setRider(wsaaCovrRider);
		minsIO.setFormat(formatsInner.minsrec);
		minsIO.setFunction(varcom.readh);
	}

protected void call3e20()
	{
		SmartFileCode.execute(appVars, minsIO);
		if (isNE(minsIO.getStatuz(), varcom.oK)
		&& isNE(minsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(minsIO.getStatuz());
			syserrrec.params.set(minsIO.getParams());
			fatalError600();
		}
		if (isEQ(minsIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.exit3e90);
		}
	}

protected void delete3e30()
	{
		minsIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, minsIO);
		if (isNE(minsIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(minsIO.getStatuz());
			syserrrec.params.set(minsIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void unitFundDelet4100()
	{
		readUnltunl4110();
	}

protected void readUnltunl4110()
	{
		SmartFileCode.execute(appVars, unltunlIO);
		if (isNE(unltunlIO.getStatuz(), varcom.oK)
		&& isNE(unltunlIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(unltunlIO.getParams());
			fatalError600();
		}
		if (isNE(wsaaChdrChdrcoy, unltunlIO.getChdrcoy())
		|| isNE(wsaaChdrChdrnum, unltunlIO.getChdrnum())
		|| isNE(wsaaLifeLife, unltunlIO.getLife())
		|| isNE(wsaaCovrCoverage, unltunlIO.getCoverage())
		|| isNE(wsaaCovrRider, unltunlIO.getRider())
		|| isEQ(unltunlIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/*  Delete unit fund record                                        */
		unltunlIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, unltunlIO);
		if (isNE(unltunlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(unltunlIO.getParams());
			fatalError600();
		}
		unltunlIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, unltunlIO);
		if (isNE(unltunlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(unltunlIO.getParams());
			fatalError600();
		}
	}

protected void getPovrgen4200()
	{
		start4210();
	}

protected void start4210()
	{
		/* Note that if a POVR record exists for this coverage then        */
		/* delete it.......                                                */
		SmartFileCode.execute(appVars, povrgenIO);
		if (isNE(povrgenIO.getStatuz(), varcom.oK)
		&& isNE(povrgenIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(povrgenIO.getParams());
			syserrrec.statuz.set(povrgenIO.getStatuz());
			fatalError600();
		}
		else {
			povrgenIO.setFunction(varcom.nextr);
		}
		if (isEQ(povrgenIO.getStatuz(), varcom.endp)
		|| isNE(povrgenIO.getChdrcoy(), wsaaChdrChdrcoy)
		|| isNE(povrgenIO.getChdrnum(), wsaaChdrChdrnum)
		|| isNE(povrgenIO.getLife(), wsaaLifeLife)
		|| isNE(povrgenIO.getCoverage(), wsaaCovrCoverage)
		|| isNE(povrgenIO.getRider(), wsaaCovrRider)) {
			povrgenIO.setStatuz(varcom.endp);
			return ;
		}
		/* Delete the record......                                         */
		povrgenIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, povrgenIO);
		if (isNE(povrgenIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(povrgenIO.getParams());
			syserrrec.statuz.set(povrgenIO.getStatuz());
			fatalError600();
		}
		else {
			povrgenIO.setStatuz(varcom.endp);
		}
	}

protected void deleteAllHxcl4300()
	{
		startDelete4310();
	}

protected void startDelete4310()
	{
		hxclIO.setParams(SPACES);
		hxclIO.setChdrcoy(fluplnbIO.getChdrcoy());
		hxclIO.setChdrnum(fluplnbIO.getChdrnum());
		hxclIO.setFupno(fluplnbIO.getFupno());
		hxclIO.setHxclseqno(1);
		while ( !(isEQ(hxclIO.getStatuz(), varcom.mrnf)
		|| isEQ(hxclIO.getStatuz(), varcom.endp))) {
			hxclIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, hxclIO);
			if (isNE(hxclIO.getStatuz(), varcom.oK)
			&& isNE(hxclIO.getStatuz(), varcom.mrnf)
			&& isNE(hxclIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(hxclIO.getParams());
				fatalError600();
			}
			if (isEQ(hxclIO.getStatuz(), varcom.oK)) {
				hxclIO.setFunction(varcom.delet);
				SmartFileCode.execute(appVars, hxclIO);
				if (isNE(hxclIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(hxclIO.getParams());
					fatalError600();
				}
			}
			setPrecision(hxclIO.getHxclseqno(), 0);
			hxclIO.setHxclseqno(add(hxclIO.getHxclseqno(), 1));
		}
		
	}

protected void checkHb8000()
	{
		start8000();
	}

protected void start8000()
	{
		/*  Read TR686 for identification of Hospital Benefit              */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr686);
		itemIO.setItemitem(wsaaCrtableDeleted);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr686rec.tr686Rec.set(itemIO.getGenarea());
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			tr686rec.waivercode.set(SPACES);
			wsaaHbComponentFlag.set("N");
		}
		else {
			wsaaHbComponentFlag.set("Y");
		}
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData unltunlrec = new FixedLengthStringData(10).init("UNLTUNLREC");
	private FixedLengthStringData povrgenrec = new FixedLengthStringData(10).init("POVRGENREC");
	private FixedLengthStringData racdlnbrec = new FixedLengthStringData(10).init("RACDLNBREC");
	private FixedLengthStringData racdrec = new FixedLengthStringData(10).init("RACDREC");
	private FixedLengthStringData hbnfrec = new FixedLengthStringData(10).init("HBNFREC");
	private FixedLengthStringData mbnsrec = new FixedLengthStringData(10).init("MBNSREC");
	private FixedLengthStringData minsrec = new FixedLengthStringData(10).init("MINSREC");
}
}
