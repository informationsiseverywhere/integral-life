package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:47
 * Description:
 * Copybook name: BNFYLNBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Bnfylnbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData bnfylnbFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData bnfylnbKey = new FixedLengthStringData(256).isAPartOf(bnfylnbFileKey, 0, REDEFINE);
  	public FixedLengthStringData bnfylnbChdrcoy = new FixedLengthStringData(1).isAPartOf(bnfylnbKey, 0);
  	public FixedLengthStringData bnfylnbChdrnum = new FixedLengthStringData(8).isAPartOf(bnfylnbKey, 1);
  	public FixedLengthStringData bnfylnbBnyclt = new FixedLengthStringData(8).isAPartOf(bnfylnbKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(239).isAPartOf(bnfylnbKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(bnfylnbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bnfylnbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}