/******************************************************************************
 * File Name 		: BnfypfDAO.java
 * Author			: nloganathan5
 * Creation Date	: 28 October 2016
 * Project			: Integral Life
 * Description		: The DAO Interface for BNFYPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.newbusiness.dataaccess.dao;

import java.util.List;

import com.csc.life.newbusiness.dataaccess.model.Bnfypf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface BnfypfDAO extends BaseDAO<Bnfypf> {

	public List<Bnfypf> searchBnfypfRecord(String coy, String chdrNum) throws SQLRuntimeException;
	public void updateBnfyRecord(List<Bnfypf> updateBnfypflist);
	public List<Bnfypf> getBnfymnaByCoyAndNum(String coy, String chdrNum) throws SQLRuntimeException;
	public List<Bnfypf> getBnfymnaByCoyAndNumAndBtype(String coy, String chdrNum, String btype) throws SQLRuntimeException;
	public List<Bnfypf> getBnfymnaByKeyAndTranno(Bnfypf bnfypf) throws SQLRuntimeException;
	public int updateBnfymnaByUnqueNum(Bnfypf bnfypf) throws SQLRuntimeException;
	public int insertBnfypf(Bnfypf bnfypf);
}
