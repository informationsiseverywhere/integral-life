package com.csc.life.newbusiness.dataaccess.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "FLUPPF", schema = "VM1DTA")
public class Fluppf implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="UNIQUE_NUMBER")
	private long uniqueNumber;
	private Character chdrcoy;
	private String chdrnum;
	private Integer fupNo;
	private Integer tranNo;
	private Character fupTyp;
	private String life;
	private String jLife;
	private String fupCde;
	private Character fupSts;
	private Integer fupDt;
	private String fupRmk;
	private String clamNum;
	@Column(name="USER_T")
	private Integer userT;
	private String termId;
	private Integer trdt;
	private Integer trtm;
	private Character zAutoInd;
	private String usrPrf;
	private String jobNm;
	private Date datime;
	private Integer effDate;
	private String crtUser;
	private Integer crtDate;
	private String lstUpUser;
	private Integer zLstUpDt;
	private Integer fuprcvd;
	private Integer exprDate;
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public Character getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Character chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public Integer getFupNo() {
		return fupNo;
	}
	public void setFupNo(Integer fupNo) {
		this.fupNo = fupNo;
	}
	public Integer getTranNo() {
		return tranNo;
	}
	public void setTranNo(Integer tranNo) {
		this.tranNo = tranNo;
	}
	public Character getFupTyp() {
		return fupTyp;
	}
	public void setFupTyp(Character fupTyp) {
		this.fupTyp = fupTyp;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getjLife() {
		return jLife;
	}
	public void setjLife(String jLife) {
		this.jLife = jLife;
	}
	public Character getFupSts() {
		return fupSts;
	}
	public void setFupSts(Character fupSts) {
		this.fupSts = fupSts;
	}
	public Integer getFupDt() {
		return fupDt;
	}
	public void setFupDt(Integer fupDt) {
		this.fupDt = fupDt;
	}
	public String getFupRmk() {
		return fupRmk;
	}
	public void setFupRmk(String fupRmk) {
		this.fupRmk = fupRmk;
	}
	public String getClamNum() {
		return clamNum;
	}
	public void setClamNum(String clamNum) {
		this.clamNum = clamNum;
	}
	public Integer getUserT() {
		return userT;
	}
	public void setUserT(Integer userT) {
		this.userT = userT;
	}
	public String getTermId() {
		return termId;
	}
	public void setTermId(String termId) {
		this.termId = termId;
	}
	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	public Integer getTrtm() {
		return trtm;
	}
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	public Character getzAutoInd() {
		return zAutoInd;
	}
	public void setzAutoInd(Character zAutoInd) {
		this.zAutoInd = zAutoInd;
	}
	public String getUsrPrf() {
		return usrPrf;
	}
	public void setUsrPrf(String usrPrf) {
		this.usrPrf = usrPrf;
	}
	public String getJobNm() {
		return jobNm;
	}
	public void setJobNm(String jobNm) {
		this.jobNm = jobNm;
	}
	public Date getDatime() {
		return new Date(datime.getTime());//IJTI-316
	}
	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());//IJTI-314
	}
	public Integer getEffDate() {
		return effDate;
	}
	public void setEffDate(Integer effDate) {
		this.effDate = effDate;
	}
	public String getCrtUser() {
		return crtUser;
	}
	public void setCrtUser(String crtUser) {
		this.crtUser = crtUser;
	}
	public Integer getCrtDate() {
		return crtDate;
	}
	public void setCrtDate(Integer crtDate) {
		this.crtDate = crtDate;
	}
	public String getLstUpUser() {
		return lstUpUser;
	}
	public void setLstUpUser(String lstUpUser) {
		this.lstUpUser = lstUpUser;
	}
	public Integer getzLstUpDt() {
		return zLstUpDt;
	}
	public void setzLstUpDt(Integer zLstUpDt) {
		this.zLstUpDt = zLstUpDt;
	}
	public Integer getFuprcvd() {
		return fuprcvd;
	}
	public void setFuprcvd(Integer fuprcvd) {
		this.fuprcvd = fuprcvd;
	}
	public Integer getExprDate() {
		return exprDate;
	}
	public void setExprDate(Integer exprDate) {
		this.exprDate = exprDate;
	}
	public String getFupCde() {
		return fupCde;
	}
	public void setFupCde(String fupCde) {
		this.fupCde = fupCde;
	}
}
