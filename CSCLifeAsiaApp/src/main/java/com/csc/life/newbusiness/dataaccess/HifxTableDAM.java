package com.csc.life.newbusiness.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HifxTableDAM.java
 * Date: Sun, 30 Aug 2009 03:40:46
 * Class transformed from HIFX.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HifxTableDAM extends HifxpfTableDAM {

	public HifxTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("HIFX");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "COMPANY"
		             + ", YEAR"
		             + ", MNTH"
		             + ", CNTBRANCH"
		             + ", CNTTYPE"
		             + ", CNTCURR"
		             + ", TYPE_T";
		
		QUALIFIEDCOLUMNS = 
		            "MNTH, " +
		            "YEAR, " +
		            "COMPANY, " +
		            "CNTBRANCH, " +
		            "CNTTYPE, " +
		            "CNTCURR, " +
		            "SUMINS, " +
		            "ANNPREM, " +
		            "SINGP, " +
		            "CNTCOUNT, " +
		            "TYPE_T, " +
		            "EFFDATE, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "COMPANY ASC, " +
		            "YEAR ASC, " +
		            "MNTH ASC, " +
		            "CNTBRANCH ASC, " +
		            "CNTTYPE ASC, " +
		            "CNTCURR ASC, " +
		            "TYPE_T ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "COMPANY DESC, " +
		            "YEAR DESC, " +
		            "MNTH DESC, " +
		            "CNTBRANCH DESC, " +
		            "CNTTYPE DESC, " +
		            "CNTCURR DESC, " +
		            "TYPE_T DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               mnth,
                               year,
                               company,
                               cntbranch,
                               cnttype,
                               cntcurr,
                               sumins,
                               annprem,
                               singp,
                               cntcount,
                               fieldType,
                               effdate,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(49);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getCompany().toInternal()
					+ getYear().toInternal()
					+ getMnth().toInternal()
					+ getCntbranch().toInternal()
					+ getCnttype().toInternal()
					+ getCntcurr().toInternal()
					+ getFieldType().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, company);
			what = ExternalData.chop(what, year);
			what = ExternalData.chop(what, mnth);
			what = ExternalData.chop(what, cntbranch);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, fieldType);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller11 = new FixedLengthStringData(1);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(mnth.toInternal());
	nonKeyFiller2.setInternal(year.toInternal());
	nonKeyFiller3.setInternal(company.toInternal());
	nonKeyFiller4.setInternal(cntbranch.toInternal());
	nonKeyFiller5.setInternal(cnttype.toInternal());
	nonKeyFiller6.setInternal(cntcurr.toInternal());
	nonKeyFiller11.setInternal(fieldType.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(97);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ nonKeyFiller5.toInternal()
					+ nonKeyFiller6.toInternal()
					+ getSumins().toInternal()
					+ getAnnprem().toInternal()
					+ getSingp().toInternal()
					+ getCntcount().toInternal()
					+ nonKeyFiller11.toInternal()
					+ getEffdate().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, sumins);
			what = ExternalData.chop(what, annprem);
			what = ExternalData.chop(what, singp);
			what = ExternalData.chop(what, cntcount);
			what = ExternalData.chop(what, nonKeyFiller11);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getCompany() {
		return company;
	}
	public void setCompany(Object what) {
		company.set(what);
	}
	public PackedDecimalData getYear() {
		return year;
	}
	public void setYear(Object what) {
		setYear(what, false);
	}
	public void setYear(Object what, boolean rounded) {
		if (rounded)
			year.setRounded(what);
		else
			year.set(what);
	}
	public PackedDecimalData getMnth() {
		return mnth;
	}
	public void setMnth(Object what) {
		setMnth(what, false);
	}
	public void setMnth(Object what, boolean rounded) {
		if (rounded)
			mnth.setRounded(what);
		else
			mnth.set(what);
	}
	public FixedLengthStringData getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(Object what) {
		cntbranch.set(what);
	}
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}
	public FixedLengthStringData getFieldType() {
		return fieldType;
	}
	public void setFieldType(Object what) {
		fieldType.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getSumins() {
		return sumins;
	}
	public void setSumins(Object what) {
		setSumins(what, false);
	}
	public void setSumins(Object what, boolean rounded) {
		if (rounded)
			sumins.setRounded(what);
		else
			sumins.set(what);
	}	
	public PackedDecimalData getAnnprem() {
		return annprem;
	}
	public void setAnnprem(Object what) {
		setAnnprem(what, false);
	}
	public void setAnnprem(Object what, boolean rounded) {
		if (rounded)
			annprem.setRounded(what);
		else
			annprem.set(what);
	}	
	public PackedDecimalData getSingp() {
		return singp;
	}
	public void setSingp(Object what) {
		setSingp(what, false);
	}
	public void setSingp(Object what, boolean rounded) {
		if (rounded)
			singp.setRounded(what);
		else
			singp.set(what);
	}	
	public PackedDecimalData getCntcount() {
		return cntcount;
	}
	public void setCntcount(Object what) {
		setCntcount(what, false);
	}
	public void setCntcount(Object what, boolean rounded) {
		if (rounded)
			cntcount.setRounded(what);
		else
			cntcount.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		company.clear();
		year.clear();
		mnth.clear();
		cntbranch.clear();
		cnttype.clear();
		cntcurr.clear();
		fieldType.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		nonKeyFiller5.clear();
		nonKeyFiller6.clear();
		sumins.clear();
		annprem.clear();
		singp.clear();
		cntcount.clear();
		nonKeyFiller11.clear();
		effdate.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}