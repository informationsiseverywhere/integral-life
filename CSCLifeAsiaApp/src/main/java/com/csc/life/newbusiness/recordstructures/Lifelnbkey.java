package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:07
 * Description:
 * Copybook name: LIFELNBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lifelnbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lifelnbFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData lifelnbKey = new FixedLengthStringData(256).isAPartOf(lifelnbFileKey, 0, REDEFINE);
  	public FixedLengthStringData lifelnbChdrcoy = new FixedLengthStringData(1).isAPartOf(lifelnbKey, 0);
  	public FixedLengthStringData lifelnbChdrnum = new FixedLengthStringData(8).isAPartOf(lifelnbKey, 1);
  	public FixedLengthStringData lifelnbLife = new FixedLengthStringData(2).isAPartOf(lifelnbKey, 9);
  	public FixedLengthStringData lifelnbJlife = new FixedLengthStringData(2).isAPartOf(lifelnbKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(lifelnbKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lifelnbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lifelnbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}