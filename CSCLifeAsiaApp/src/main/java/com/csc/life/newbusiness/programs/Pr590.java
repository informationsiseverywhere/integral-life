/*
 * File: Pr590.java
 * Date: 30 August 2009 1:48:22
 * Author: Quipoz Limited
 *
 * Class transformed from PR590.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.ObjectUtils;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO; //ILIFE-8234
import com.csc.fsu.clients.dataaccess.model.Clntpf; //ILIFE-8234
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Clntrlsrec;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ClrrpfDAO; //ILIFE-8234
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO; //ILIFE-8234
import com.csc.fsu.general.dataaccess.model.Clrrpf; //ILIFE-8234
import com.csc.fsu.general.dataaccess.model.Ptrnpf; //ILIFE-8234
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM; //ILIFE-8234
import com.csc.life.contractservicing.dataaccess.FupeTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.FupepfDAO; //ILIFE-8234
import com.csc.life.contractservicing.dataaccess.model.Fupepf; //ILIFE-8234
import com.csc.life.newbusiness.dataaccess.FlupaltTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.HxclTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO; //ILIFE-8234
import com.csc.life.newbusiness.dataaccess.dao.HxclpfDAO; //ILIFE-8234
import com.csc.life.newbusiness.dataaccess.model.Hpadpf; //ILIFE-8234
import com.csc.life.newbusiness.dataaccess.model.Hxclpf; //ILIFE-8234
import com.csc.life.newbusiness.screens.Sr590ScreenVars;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO; //ILIFE-8234
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO; //ILIFE-8234
import com.csc.life.productdefinition.dataaccess.model.Lifepf; //ILIFE-8234
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.model.Slckpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.smart400framework.dataaccess.dao.SlckpfDAO;
import com.csc.smart400framework.dataaccess.dao.impl.SlckpfDAOImpl;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
* Retrieve the contract header information from the CHDRMJA I/O
* module.
* Read the clients name.
*
* Do the following :
*    - If ROLU -> * First page = 'N'
*                 * Start with the last processed follow-up
*    - If ROLD -> * Read the first follow-up of current screen
*                 * Read back SUPFILE-PAGE + 1 times
*                 * Check if first page
*                 * Start with current follow-up
*    - ELSE       * First page = 'Y'
*                 * Start with first follow-up
*
* Fill the subfile page with follow-up records for the contract.
* If the page is not full -> add blank lines to the subfile in
* order to display a full page.
*
* Set SCRN-SUBFILE-MORE to Y or N to display a '+' if there are
* more follow-ups to show.
*
* Retrieve todays date (DATCON1) and store for later use.
*
*
* Validation
* ----------
*
* Validate the changed fields in the subfile page
*
* If ROLD and First page
*    - Error ## roldown not allowed ##
* If ROLU
*    - If any new blank subfile lines
*         - Error ## rolup not allowed ##
*
* Update
* ------
*
* Delete/write/rewrite follow-ups
*
* Store the first follow-up key of current screen ## for ROLU ##
*
* Add Validation to  presence of follow up letter type for
* printing of either standard follow up letter or a reminder
*
* Add the option of supplying Doctor details when entering
* Follow Ups.
*
* When a Doctor is required, output a warning message, but
* enable user to exit without aborting.
*                                                                     *
*****************************************************************
* </pre>
*/
public class Pr590 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR590");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private FixedLengthStringData wsaaUpdPtrnFlag = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaFinishUpdFlag = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaChdrmjaTranno = new ZonedDecimalData(5, 0);
	private ZonedDecimalData wsaaRrn = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaOptionCode = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFoundSelection = new FixedLengthStringData(1);
	private Validator foundSelection = new Validator(wsaaFoundSelection, "Y");
	private PackedDecimalData ix = new PackedDecimalData(2, 0).init(ZERO);
	private ZonedDecimalData iy = new ZonedDecimalData(2, 0);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);

	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4);
	private Validator pendingPhReinstate = new Validator(wsaaBatctrcde, "TR7B");
	private ZonedDecimalData wsaaDocseq = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(4).isAPartOf(wsaaT5661Key, 1);
		/* TABLES */
 //ILIFE-8234 start
	private static final String t5688 = "T5688";
	private static final String t5661 = "T5661";
	private static final String t5660 = "T5660";
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String clrfrec = "CLRFREC";
	private static final String flupaltrec = "FLUPALTREC";
	private static final String hpadrec = "HPADREC";
	private static final String hxclrec = "HXCLREC";
	private static final String ptrnrec = "PTRNREC";
	private static final String fuperec = "FUPEREC";
	private static final String itemrec = "ITEMREC";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
//	private ClrfTableDAM clrfIO = new ClrfTableDAM();
//	private CltsTableDAM cltsIO = new CltsTableDAM();
//	private DescTableDAM descIO = new DescTableDAM();
	private FlupaltTableDAM flupaltIO = new FlupaltTableDAM();
	private FupeTableDAM fupeIO = new FupeTableDAM();
//	private HpadTableDAM hpadIO = new HpadTableDAM();
//	private HxclTableDAM hxclIO = new HxclTableDAM();
//	private ItemTableDAM itemIO = new ItemTableDAM();
//	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
//	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
 //ILIFE-8234 end
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Clntrlsrec clntrlsrec = new Clntrlsrec();
	private T5661rec t5661rec = new T5661rec();
	private Optswchrec optswchrec = new Optswchrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr590ScreenVars sv = ScreenProgram.getScreenVars( Sr590ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaUserFieldsInner wsaaUserFieldsInner = new WsaaUserFieldsInner();
	boolean CSMIN003Permission  = false;
	

	

	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private Descpf descpf = null;
	private Itempf itempf = null;
	//ILB-8234 start 
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);	
	private Clntpf clts= new Clntpf();
	private HpadpfDAO hpadpfDao = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);
	private Hpadpf hpadpf = new Hpadpf();
	private HxclpfDAO hxclpfDAO = getApplicationContext().getBean("hxclpfDAO", HxclpfDAO.class); 
  	private Hxclpf hxclpf = new Hxclpf();
  	private FupepfDAO fupepfDAO = getApplicationContext().getBean("fupepfDAO", FupepfDAO.class); 
  	private Fupepf fupepf = new Fupepf();
  	private List<Fupepf> fupeList;
	private ClrrpfDAO clrrpfDAO = getApplicationContext().getBean("clrrpfDAO", ClrrpfDAO.class); 
  	private Clrrpf clrrpf = new Clrrpf();
  
  	private List<Itempf> itempfList;
  	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
  	private List<Lifepf> lifeList;
  	protected HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class); 
  	private Ptrnpf ptrnpf = new Ptrnpf();
	private List<Ptrnpf> ptrnpfList = null;
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	//ILIFE-8234 end
	private Sftlockrec sftlockrec = new Sftlockrec();
	private List<Slckpf> slckList = new ArrayList<Slckpf>();
	private SlckpfDAO slckpfDAO = new SlckpfDAOImpl();
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		checkStatuz2070,
		continue2620,
		updateErrorIndicators2670,
		switch4170
	}

	public Pr590() {
		super();
		screenVars = sv;
		new ScreenModel("Sr590", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clts.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(clts.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clts.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clts.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clts.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(clts.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clts.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clts.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clts.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clts.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clts.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clts.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clts.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clts.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		wsaaFinishUpdFlag.set(SPACES);
		sv.language.set(wsspcomn.language);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaFoundSelection.set("N");
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaBatctrcde.set(wsaaBatckey.batcBatctrcde);
		/* Dummy subfile initalisation for prototype - relpace with SCLR*/
		scrnparams.function.set(varcom.sclr);
		processScreen("SR590", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/* Dummy field initilisation for prototype version.*/
		//ILIFE-8234 start
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				/*else {
					chdrpfDAO.setCacheObject(chdrpf);
				}*/
			}
		}
		
		//ILIFE-8234 end 
		/* Read the contract definition description from table*/
		/* T5688 for the contract held on the client header record.*/
		descpf=descDAO.getdescData("IT", "T5688", chdrpf.getCnttype(), wsspcomn.company.toString(), wsspcomn.language.toString());
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cownnum.set(chdrpf.getCownnum());
		if (descpf == null) {
			sv.fupremk.fill("?");
		}
		else {
			sv.fupremk.set(descpf.getLongdesc());
		}
		/* Look up the contract details of the client owner (CLTS)*/
		/* and format the name as a CONFIRMATION NAME.*/
 //ILIFE-8234 start
		clts.setClntpfx("CN");
		clts.setClntcoy(wsspcomn.fsuco.toString());
		clts.setClntnum(chdrpf.getCownnum());
		clts = clntpfDAO.getCltsRecordByKey(clts);
		if ((clts == null)|| isNE(clts.getValidflag(), 1)) {
			sv.ownernameErr.set(errorsInner.e040);
			sv.ownername.set(SPACES);
		} 
		
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		/* Retrieve Doctor details, if present on HPAD file.*/
		wsaaUserFieldsInner.wsaaFirstDisplay.set("Y");
		
		hpadpf=hpadpfDao.getHpadData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());/* IJTI-1523 */
		if(hpadpf == null){
			syserrrec.statuz.set("hpadpf MRNF");
			fatalError600();
		} 
		
		
		/* Get the Doctor confirmation name.*/
		if (isNE(hpadpf.getZdoctor(), SPACES)) {
			sv.zdoctor.set(hpadpf.getZdoctor());
			
			clts.setClntcoy(wsspcomn.fsuco.toString());
			clts.setClntpfx("CN");
			clts.setClntnum(hpadpf.getZdoctor());
			clts = clntpfDAO.getCltsRecordByKey(clts);
			if(clts == null ) {
				sv.zdoctorErr.set(errorsInner.e040);
			}
			/*cltsIO.setClntpfx("CN");
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntnum(hpadIO.getZdoctor());
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)
			&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(cltsIO.getParams());
				syserrrec.statuz.set(cltsIO.getStatuz());
				fatalError600();
			}
			if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
				sv.zdoctorErr.set(errorsInner.e040);
			}*/
			else {
				namadrsrec.namadrsRec.set(SPACES);
			//	namadrsrec.clntkey.set(cltsIO.getDataKey());
				namadrsrec.clntkey.set(""+clts.getClntpfx()+clts.getClntcoy()+clts.getClntnum());
				namadrsrec.language.set(wsspcomn.language);
				namadrsrec.function.set(wsaaUserFieldsInner.wsaaPayeeName);
				callProgram(Namadrs.class, namadrsrec.namadrsRec);
				if (isNE(namadrsrec.statuz, varcom.oK)) {
					syserrrec.params.set(""+clts.getClntcoy()+clts.getClntpfx()+clts.getClntnum());
					syserrrec.statuz.set(namadrsrec.statuz);
					fatalError600();
				}
				sv.zdocname.set(namadrsrec.name);
			}
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/* Set follow-up record to the start position*/
		if (isEQ(wsaaUserFieldsInner.wsaaRoluRold, varcom.rold)) {
			flupaltIO.setRecKeyData(wsaaUserFieldsInner.wsaaFirstFlupKey);
		}
		else {
			if (isNE(wsaaUserFieldsInner.wsaaRoluRold, varcom.rolu)) {
				flupaltIO.setParams(SPACES);
				flupaltIO.setChdrcoy(chdrpf.getChdrcoy());
				flupaltIO.setChdrnum(chdrpf.getChdrnum());
				flupaltIO.setFupno(ZERO);
			}
		}
		wsaaUserFieldsInner.wsaaFirstPage.set("Y");
		flupaltIO.setFunction(varcom.begn);
		if (isEQ(wsaaUserFieldsInner.wsaaRoluRold, varcom.rolu)) {
			setPrecision(flupaltIO.getFupno(), 0);
			flupaltIO.setFupno(add(flupaltIO.getFupno(), 1));
			wsaaUserFieldsInner.wsaaFirstPage.set("N");
		}
		flupaltIO.setFormat(flupaltrec);
		SmartFileCode.execute(appVars, flupaltIO);
		/* Check for I/O error. Note that the ENDP condition is tested*/
		/* for later.*/
		if (isNE(flupaltIO.getStatuz(), varcom.oK)
		&& isNE(flupaltIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(flupaltIO.getParams());
			fatalError600();
		}
		/* Go one page back.*/
		if (isEQ(wsaaUserFieldsInner.wsaaRoluRold, varcom.rold)) {
			resetTopRecord1100();
		}
		/* Load subfile, starting with current follow-up*/
		wsaaUserFieldsInner.wsaaCount.set(ZERO);
		while ( !(isEQ(wsaaUserFieldsInner.wsaaCount, sv.subfilePage))) {
			loadSubfile1300();
		}

		if (isEQ(flupaltIO.getStatuz(), varcom.endp)
		|| isNE(flupaltIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(flupaltIO.getChdrnum(), chdrpf.getChdrnum())) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		/*    MOVE TDAY                   TO DTC1-FUNCTION.                */
		/*    CALL 'DATCON1'              USING DTC1-DATCON1-REC.          */
		/*    MOVE DTC1-INT-DATE          TO WSAA-TODAY.                   */
		/* Reset WSAA-ROLU-ROLD here because if F3, F15, F16,F17 or F18 is*/
		/* pressed in the subfile-screen WSAA-ROLU-ROLD is not set to the*/
		/* SCRN-STATUZ.*/
		wsaaUserFieldsInner.wsaaRoluRold.set(varcom.oK);
		/* Intialise Call to OPTSWCH.*/
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)) {
			syserrrec.params.set(optswchrec.rec);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			fatalError600();
		}
		CSMIN003Permission  = FeaConfg.isFeatureExist("2", "CSMIN003", appVars, "IT");
	}

protected void resetTopRecord1100()
	{
		resetTop1110();
	}

protected void resetTop1110()
	{
		/* If ENDP -> start with last follow-up for contract*/
		/* ELSE    -> read-prev from current follow-up.*/
		if (isEQ(flupaltIO.getStatuz(), varcom.endp)) {
			flupaltIO.setFunction(varcom.endr);
			flupaltIO.setStatuz(varcom.oK);
		}
		else {
			flupaltIO.setFunction(varcom.nextp);
		}
		/* Read back #records in one page + 1.*/
		for (wsaaUserFieldsInner.wsaaCount.set(1); !((setPrecision(wsaaUserFieldsInner.wsaaCount, 0)
		&& isGT(wsaaUserFieldsInner.wsaaCount, add(sv.subfilePage, 1)))
		|| isEQ(flupaltIO.getStatuz(), varcom.endp)); wsaaUserFieldsInner.wsaaCount.add(1)){
			readPrevRecord1200();
		}
		/* See if this is the first page and reset the top record for this*/
		/* page.*/
		if (isEQ(flupaltIO.getStatuz(), varcom.endp)
		|| isNE(flupaltIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(flupaltIO.getChdrnum(), chdrpf.getChdrnum())) {
			wsaaUserFieldsInner.wsaaFirstPage.set("Y");
			flupaltIO.setChdrcoy(chdrpf.getChdrcoy());
			flupaltIO.setChdrnum(chdrpf.getChdrnum());
			flupaltIO.setFupno(ZERO);
			flupaltIO.setFunction(varcom.begn);
			flupaltIO.setFormat(flupaltrec);
			SmartFileCode.execute(appVars, flupaltIO);
			/*   Check for I/O error. Note that the ENDP*/
			/*   condition is tested for later.*/
			if (isNE(flupaltIO.getStatuz(), varcom.oK)
			&& isNE(flupaltIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(flupaltIO.getParams());
				fatalError600();
			}
		}
		else {
			if ((setPrecision(wsaaUserFieldsInner.wsaaCount, 0)
			&& isGT(wsaaUserFieldsInner.wsaaCount, add(sv.subfilePage, 1)))) {
				wsaaUserFieldsInner.wsaaFirstPage.set("N");
				flupaltIO.setFunction(varcom.nextr);
				flupaltIO.setFormat(flupaltrec);
				SmartFileCode.execute(appVars, flupaltIO);
				/*     Check for I/O error. Note that the ENDP*/
				/*     condition is tested for later.*/
				if (isNE(flupaltIO.getStatuz(), varcom.oK)
				&& isNE(flupaltIO.getStatuz(), varcom.endp)) {
					syserrrec.params.set(flupaltIO.getParams());
					fatalError600();
				}
			}
		}
	}

protected void readPrevRecord1200()
	{
		/*READ-PREV*/
		flupaltIO.setFormat(flupaltrec);
		SmartFileCode.execute(appVars, flupaltIO);
		/* Check for I/O error. Note that the ENDP condition is tested*/
		/* for later.*/
		if (isNE(flupaltIO.getStatuz(), varcom.oK)
		&& isNE(flupaltIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(flupaltIO.getParams());
			fatalError600();
		}
		flupaltIO.setFunction(varcom.nextp);
		/*EXIT*/
	}

protected void loadSubfile1300()
	{
		load1310();
		addSubfileRecord1320();
	}

protected void load1310()
	{
		wsaaUserFieldsInner.wsaaCount.add(1);
		/* Record belong to this contract or ENDP ? If not, empty line*/
		/* on in the subfile page.*/
		if (isNE(flupaltIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(flupaltIO.getChdrnum(), chdrpf.getChdrnum())
		|| isEQ(flupaltIO.getStatuz(), varcom.endp)) {
			sv.subfileFields.set(SPACES);
			sv.fupremdt.set(99999999);
			sv.lifeno.set(ZERO);
			sv.jlife.set(SPACES);
			wsaaUserFieldsInner.wsaaFupno.add(1);
			sv.fupno.set(wsaaUserFieldsInner.wsaaFupno);
			sv.zitem.set(SPACES);
			sv.actn.set(SPACES);
			sv.indic.set(SPACES);
			sv.select.set(SPACES);
			sv.crtuser.set(wsspcomn.userid);
			sv.language.set(wsspcomn.language);
			sv.crtdate.set(datcon1rec.intDate);
			sv.crtdateOut[varcom.nd.toInt()].set("Y");
			sv.fuptype.set("A");
			sv.fuptypOut[varcom.nd.toInt()].set("Y");
			sv.fuprcvd.set(99999999);
			sv.exprdate.set(99999999);
			return ;
		}
		/* Record belongs to this contract, put details in the current*/
		/* line of the subfile page.*/
		/*    MOVE FLUPALT-FUPCODE        TO SR590-FUPCODE.                */
		sv.fupcdes.set(flupaltIO.getFupcode());
		sv.fupremdt.set(flupaltIO.getFupremdt());
		sv.fupno.set(flupaltIO.getFupno());
		wsaaUserFieldsInner.wsaaFupno.set(flupaltIO.getFupno());
		sv.lifeno.set(flupaltIO.getLife());
		sv.jlife.set(flupaltIO.getJlife());
		sv.fupremk.set(flupaltIO.getFupremk());
		sv.fupstat.set(flupaltIO.getFupstat());
		sv.uprflag.set("N");
		sv.fuptype.set(flupaltIO.getFuptype());
		sv.zitem.set(flupaltIO.getFupcode());
		sv.fuprcvd.set(flupaltIO.getFuprcvd());
		if (isEQ(sv.fuprcvd, ZERO)) {
			sv.fuprcvd.set(varcom.vrcmMaxDate);
		}
		sv.exprdate.set(flupaltIO.getExprdate());
		if (isEQ(sv.exprdate, ZERO)) {
			sv.exprdate.set(varcom.vrcmMaxDate);
		}
		readFupe1500();
		if (isEQ(fupeIO.getStatuz(), varcom.oK)) {
			sv.actn.set("+");
			sv.indic.set("+");
		}
		else {
			sv.actn.set(SPACES);
			sv.indic.set(SPACES);
		}
		sv.crtuser.set(flupaltIO.getCrtuser());
		sv.language.set(wsspcomn.language);
		sv.crtdate.set(flupaltIO.getCrtdate());
		sv.crtdateOut[varcom.nd.toInt()].set("N");
		if (isEQ(flupaltIO.getCrtdate(), ZERO)) {
			sv.crtdate.set(datcon1rec.intDate);
		}
		sv.crtdateOut[varcom.nd.toInt()].set("N");
		checkHxcl1400();
		sv.select.set(SPACES);
		flupaltIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, flupaltIO);
		/* Check for I/O error. Note that the ENDP condition is tested*/
		/* for later.*/
		if (isNE(flupaltIO.getStatuz(), varcom.oK)
		&& isNE(flupaltIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(flupaltIO.getParams());
			fatalError600();
		}
	}

protected void addSubfileRecord1320()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			/*        MOVE 'Y'                TO SR590-FUPCDE-OUT(PR)          */
			sv.fupcdesOut[varcom.pr.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("SR590", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkHxcl1400()
	{
		hxcl1410();
	}

protected void hxcl1410()
	{
	 //ILIFE-8234 start
		hxclpf=hxclpfDAO.readRecord(flupaltIO.getChdrcoy().toString(), flupaltIO.getChdrnum().toString(), flupaltIO.getFupno().toInt());
		if (hxclpf==null) {
			return ;
		}
	 //ILIFE-8234 end	
		sv.actn.set("+");
		sv.indic.set("+");
	}

protected void readFupe1500()
	{
		readFupe1510();
	}

protected void readFupe1510()
	{
		/* Read FUPE to determine whether any record exists                */
	 //ILIFE-8234 start
		fupeList=fupepfDAO.readRecord(flupaltIO.getChdrcoy().toString(), flupaltIO.getChdrnum().toString(), flupaltIO.getFupno().toInt());
	 //ILIFE-8234 end
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*bug ILIFE-777 start*/	
//		if (isEQ(wsspcomn.flag, "I")) {
//			sv.zdoctorOut[varcom.pr.toInt()].set("Y");
//		}
		/*bug ILIFE-777 end*/	
		/*    MOVE PROT                TO SCRN-FUNCTION.*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					screenIo2010();
					validateSubfile2060();
				case checkStatuz2070:
					checkStatuz2070();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'SR590IO'              USING SCRN-SCREEN-PARAMS*/
		/*                                SR590-DATA-AREA*/
		/*                                SR590-SUBFILE-AREA.*/
		/* Screen errors are now handled in the calling program.*/
		/*    PERFORM 200-SCREEN-ERRORS.*/
		wsspcomn.edterror.set(varcom.oK);
		wsaaUserFieldsInner.wsaaRoluRold.set(scrnparams.statuz);
		/*VALIDATE-SCREEN*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			goTo(GotoLabel.checkStatuz2070);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2060() 
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.checkStatuz2070);
		}
		/*    MOVE SRNCH                  TO SCRN-FUNCTION.                */
		/* If a field is in error and you do not ammend it but simply      */
		/* press enter again,the line would not be validated and the       */
		/* error would not be found.                                       */
		/* When in enquiry mode,if user select 1 or 2 on empty line,       */
		/* then error will occur after user press ENTER twice.             */
		/* So change to use SSTRT to validate every line in subfile        */
		scrnparams.function.set(varcom.sstrt);
		processScreen("SR590", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, "ENDP")) {
			goTo(GotoLabel.checkStatuz2070);
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}

		/*  If no errors have been detected, ensure a Doctor is input*/
		/*  for a Follow Up which requires this information.*/
		if (isEQ(sv.errorSubfile, SPACES)
		&& isNE(wsspcomn.flag, "I")) {
			wsaaUserFieldsInner.wsaaDoctFlup.set(SPACES);
			scrnparams.statuz.set(varcom.oK);
			scrnparams.function.set(varcom.sstrt);
			while ( !(isEQ(wsaaUserFieldsInner.wsaaDoctFlup, "Y")
			|| isEQ(scrnparams.statuz, varcom.endp))) {
				findDoctFlup2700();
			}

			if (isEQ(wsaaUserFieldsInner.wsaaDoctFlup, "Y")
			&& isEQ(sv.zdoctor, SPACES)) {
				sv.zdoctorErr.set(errorsInner.hl43);
				if (isEQ(wsaaUserFieldsInner.wsaaFirstDisplay, "Y")) {
					wsspcomn.edterror.set("Y");
					wsaaUserFieldsInner.wsaaFirstDisplay.set("N");
				}
			}
			/* Check nominated Doctor is valid*/
			if (isEQ(wsaaUserFieldsInner.wsaaDoctFlup, "Y")
			&& !sv.zdoctor.equals(" ")) {
			clrrpf=clrrpfDAO.getClrfRecord(clntrlsrec.doctor.toString(), wsspcomn.company.toString(), sv.zdoctor.toString(), clntrlsrec.doctor.toString());
			if (clrrpf==null || !clrrpf.getUsed2b().equals(" ")) {
				sv.zdoctorErr.set(errorsInner.hl43);
				wsspcomn.edterror.set("Y");
			}
			}
		}
	}

protected void checkStatuz2070()
	{
		/*  Do not allow any action on the screen if an error was found*/
		/*  in the above VALIDATE section.*/
		if (isNE(wsspcomn.edterror, "Y")) {
			if (isEQ(wsaaUserFieldsInner.wsaaRoluRold, varcom.rolu)
			|| isEQ(wsaaUserFieldsInner.wsaaRoluRold, varcom.rold)) {
				checkRoluRold2100();
			}
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*               (Screen validation)
	* </pre>
	*/
protected void checkRoluRold2100()
	{
		check2110();
		checkForErrors2180();
	}

protected void check2110()
	{
		if (isEQ(wsaaUserFieldsInner.wsaaRoluRold, varcom.rold)) {
			if (isEQ(wsaaUserFieldsInner.wsaaFirstPage, "Y")) {
				scrnparams.errorCode.set(errorsInner.f498);
			}
			return ;
		}
		/* If this is a ROLU.*/
		if (isEQ(wsspcomn.flag, "I")) {
			if (isEQ(scrnparams.subfileMore, "N")) {
				scrnparams.errorCode.set(errorsInner.f499);
			}
			return ;
		}
		for (wsaaUserFieldsInner.wsaaCount.set(1); !(isGT(wsaaUserFieldsInner.wsaaCount, sv.subfilePage)); wsaaUserFieldsInner.wsaaCount.add(1)){
			checkSubfile2110();
		}
	}

protected void checkForErrors2180()
	{
		if (isNE(scrnparams.errorCode, SPACES)) {
			wsspcomn.edterror.set("Y");
			wsaaUserFieldsInner.wsaaRoluRold.set(varcom.calc);
		}
		/*EXIT*/
	}

protected void checkSubfile2110()
	{
		/*CHECK*/
		scrnparams.subfileRrn.set(wsaaUserFieldsInner.wsaaCount);
		scrnparams.function.set(varcom.sread);
		processScreen("SR590", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(sv.uprflag, SPACES)) {
			scrnparams.errorCode.set(errorsInner.f499);
		}
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					validation2610();
				case continue2620:
					continue2620();
				case updateErrorIndicators2670:
					updateErrorIndicators2670();
					readNextModifiedRecord2680();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		/* Check option switching for follow-up extended text screen       */
		if (isEQ(wsspcomn.flag, "I")
		|| isNE(sv.actnErr, SPACES)) {
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Check if fields have been blanked out i.e. deleted.*/
		/*    IF  SR590-FUPCODE           = SPACES                         */
		if (isEQ(sv.fupcdes, SPACES)
		&& isEQ(sv.fupremk, SPACES)
		&& isEQ(sv.fupstat, SPACES)
		&& (isEQ(sv.fupremdt, ZERO)
		|| isEQ(sv.fupremdt, 99999999)
		|| isEQ(sv.fupremdt, SPACES))
		&& isEQ(sv.lifeno, ZERO)
		&& isEQ(sv.jlife, SPACES)
		&& isEQ(sv.uprflag, "N")) {
			sv.uprflag.set("D");
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Fields have been blanked out for a new record*/
		/*    IF  SR590-FUPCODE           = SPACES                         */
		if (isEQ(sv.fupcdes, SPACES)
		&& isEQ(sv.fupremk, SPACES)
		&& isEQ(sv.fupstat, SPACES)
		&& (isEQ(sv.fupremdt, ZERO)
		|| isEQ(sv.fupremdt, 99999999)
		|| isEQ(sv.fupremdt, SPACES))
		&& isEQ(sv.lifeno, ZERO)
		&& isEQ(sv.jlife, SPACES)
		&& (isEQ(sv.uprflag, " ")
		|| isEQ(sv.uprflag, "X"))) {
			sv.uprflag.set("X");
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Check follow up status.*/
		/*    MOVE SPACES                 TO ITEM-PARAMS.                  */
		/*    MOVE 'IT'                   TO ITEM-ITEMPFX.                 */
		/*    MOVE WSSP-COMPANY           TO ITEM-ITEMCOY.                 */
		/*    MOVE T5660                  TO ITEM-ITEMTABL.                */
		/*    MOVE SR590-FUPSTAT          TO ITEM-ITEMITEM.                */
		/*    MOVE READR                  TO ITEM-FUNCTION.                */
		/*    CALL 'ITEMIO'               USING ITEM-PARAMS.               */
		/*    IF ITEM-STATUZ              NOT = O-K                        */
		/*                                AND NOT = MRNF                   */
		/*       MOVE ITEM-PARAMS         TO SYSR-PARAMS                   */
		/*       PERFORM 600-FATAL-ERROR.                                  */
		/*    IF ITEM-STATUZ              = MRNF                           */
		/*       MOVE E558                TO SR590-FUPSTS-ERR.             */
		/* Check follow up code.*/
		
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(sv.fupcdes);
		itempf = new Itempf();
	    itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5661");
		itempf.setItemitem(wsaaT5661Key.toString().trim());
	    itempf = itemDAO.getItempfRecord(itempf);
	    if (itempf == null) {
	    	sv.fupcdesErr.set(errorsInner.e557);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
	    t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		/*    IF SR590-FUPSTAT                = 'L'                        */
		if (isEQ(sv.fupstat, "L")
		|| isEQ(sv.fupstat, " ")) {
			if (isEQ(t5661rec.zlettype, SPACES)) {
				/*          MOVE HL41                 TO SR590-FUPCDE-ERR          */
				sv.fupcdesErr.set(errorsInner.hl41);
				goTo(GotoLabel.updateErrorIndicators2670);
			}
		}
		/*    IF SR590-FUPSTAT                = 'M'                        */
		if (isEQ(sv.fupstat, "M")
		|| isEQ(sv.fupstat, " ")) {
			if (isEQ(t5661rec.zlettyper, SPACES)) {
				/*          MOVE HL41                 TO SR590-FUPCDE-ERR          */
				sv.fupcdesErr.set(errorsInner.hl41);
				goTo(GotoLabel.updateErrorIndicators2670);
			}
		}
		if (isNE(sv.zitem, SPACES)
		&& isNE(sv.fupcdes, SPACES)
		&& isNE(sv.fupcdes, sv.zitem)) {
			/*        MOVE W431                   TO SR590-FUPCDE-ERR          */
			sv.fupcdesErr.set(errorsInner.w431);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* If the remark is blank then default to desc from T5661.*/
		if (isNE(sv.fupremk, SPACES)) {
			goTo(GotoLabel.continue2620);
		}
		descpf=descDAO.getdescData("IT", "T5661", wsaaT5661Key.toString().trim(), wsspcomn.company.toString(), wsspcomn.language.toString());
		
		if (descpf == null) {
			sv.fuprmkErr.set(errorsInner.e557);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		else {
			sv.fupremk.set(descpf.getLongdesc());
		}
		/* Set default values for JLIFE and LIFENO                         */
		/* Don't reset the follow-up status if it is called from remote    */
		/* program, e.g. BO.                                               */
		if (isNE(scrnparams.deviceInd, "*RMT")) {
			sv.fupstat.set(t5661rec.fupstat);
		}
		if (isEQ(sv.jlife, SPACES)) {
			sv.jlife.set("00");
		}
		if (isEQ(sv.lifeno, 0)) {
			sv.lifeno.set(1);
		}
		/* Check follow up status.                                         */
		itempf = new Itempf();
	    itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5660");
		itempf.setItemitem(sv.fupstat.toString().trim());
	    itempf = itemDAO.getItempfRecord(itempf);
	    if (itempf == null) {
	    	sv.fupstsErr.set(errorsInner.e558);
		}
	}

protected void continue2620()
	{
		/* If the date is blank the use 'todays' date.*/
		if ((isEQ(sv.fupremdt, ZERO)
		|| isEQ(sv.fupremdt, SPACES)
		|| isEQ(sv.fupremdt, 99999999))
		&& isEQ(sv.fupdtErr, SPACES)) {
			if (isEQ(sv.fupstat, "L")) {
				setReminderDate5000();
			}
			else {
				sv.fupremdt.set(datcon1rec.intDate);
			}
		}
		/* Check Received Date and Expiry Date                             */
		if (isNE(sv.fuprcvd, varcom.vrcmMaxDate)
		&& isGT(sv.fuprcvd, datcon1rec.intDate)) {
			sv.fuprcvdErr.set(errorsInner.f073);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if (isNE(sv.exprdate, varcom.vrcmMaxDate)
		&& isLT(sv.exprdate, datcon1rec.intDate)) {
			sv.exprdateErr.set(errorsInner.g914);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if (isEQ(sv.fuprcvd, varcom.vrcmMaxDate)) {
			for (ix.set(1); !(isGT(ix, 10)
			|| isEQ(t5661rec.fuposs[ix.toInt()], SPACES)); ix.add(1)){
				if (isEQ(t5661rec.fuposs[ix.toInt()], sv.fupstat)) {
					sv.fuprcvdErr.set(errorsInner.e186);
				}
			}
		}
		/* Validate that the life number is not ZEROES.*/
		if (isEQ(sv.lifeno, ZERO)) {
			sv.lifenoErr.set(errorsInner.h031);
		}
		/* Validate joint life, if entered.*/
		
		if (!sv.jlife.equals(" ")) {
			lifeList = lifepfDAO.getLifeData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), sv.lifeno.toString(), sv.jlife.toString());/* IJTI-1523 */
			if (lifeList.size()==0) {
				sv.jlifeErr.set(errorsInner.e350);
			}
		}
		
		
		if (isNE(sv.select, SPACES)) {
			optswchrec.optsInd[1].set(SPACES);
			optswchrec.optsSelCode.set(SPACES);
			optswchrec.optsFunction.set("CHCK");
			optswchrec.optsCallingProg.set(wsaaProg);
			optswchrec.optsDteeff.set(ZERO);
			optswchrec.optsCompany.set(wsspcomn.company);
			optswchrec.optsItemCompany.set(wsspcomn.company);
			optswchrec.optsLanguage.set(wsspcomn.language);
			varcom.vrcmTranid.set(wsspcomn.tranid);
			optswchrec.optsUser.set(varcom.vrcmUser);
			optswchrec.optsSelType.set("L");
			optswchrec.optsSelOptno.set(sv.select);
			callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
			if (isNE(optswchrec.optsStatuz, varcom.oK)) {
				sv.selectErr.set(optswchrec.optsStatuz);
			}
		}
		/* Cannot switch to Follow-up Extended Text if no follow-up code   */
		if (isNE(sv.actn, SPACES)
		&& isNE(sv.actn, "+")
		&& isEQ(sv.fupcdes, SPACES)) {
			sv.actnErr.set(errorsInner.rf00);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if (isEQ(sv.zitem, SPACES)) {
			if (isEQ(sv.fupcdes, SPACES)) {
				sv.actn.set(SPACES);
				sv.indic.set(SPACES);
			}
			else {
				if (isNE(t5661rec.messages, SPACES)) {
					sv.indic.set("+");
				}
			}
		}
		if (isEQ(sv.actn, SPACES)
		&& isNE(sv.indic, SPACES)) {
			sv.actn.set(sv.indic);
		}
		/* The subfile record has been changed so update the hidden*/
		/* flag.*/
		if (isEQ(sv.uprflag, "N")) {
			checkUpdate2800();
		}
		/*       MOVE 'U'                 TO SR590-UPRFLAG.*/
		//MIBT-251 STARTS
		//if (isEQ(sv.uprflag, SPACES)) {
		if (isEQ(sv.uprflag, SPACES) || ( isNE(sv.fupcdes, SPACES)  &&  isEQ(sv.uprflag, "X") )) {
		//MIBT-251 ENDS
			sv.crtdate.set(datcon1rec.intDate);
			sv.uprflag.set("A");
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR590", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		/*    MOVE SRNCH                  TO SCRN-FUNCTION.                */
		/* If a field is in error and you do not ammend it but simply      */
		/* press enter again,the line would not be validated and the       */
		/* error would not be found.                                       */
		/* When in enquiry mode,if user select 1 or 2 on empty line,       */
		/* then error will occur after user press ENTER twice.             */
		/* So change to use SRDN to validate every line in subfile when    */
		/* enquiry                                                         */
		scrnparams.function.set(varcom.srdn);
		processScreen("SR590", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void findDoctFlup2700()
	{
		read2700();
	}

protected void read2700()
	{
		processScreen("SR590", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.params.set(scrnparams.screenParams);
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			return ;
		}
		/* Check if this Follow Up requires a Doctor to be supplied.*/
		/*    IF SR590-FUPCODE         NOT = SPACE                         */
		if (isNE(sv.fupcdes, SPACES)) {
			wsaaT5661Lang.set(wsspcomn.language);
			wsaaT5661Fupcode.set(sv.fupcdes);
			itempf = new Itempf();
		    itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl("T5661");
			itempf.setItemitem(wsaaT5661Key.toString().trim());
		    itempf = itemDAO.getItempfRecord(itempf);
		    if (itempf == null) {
		    	t5661rec.t5661Rec.set(SPACES);
			}else{
				t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
		    
			/* Set indicator if this Follow Up requires Doctor details.*/
			if (isEQ(t5661rec.zdocind, "Y")) {
				wsaaUserFieldsInner.wsaaDoctFlup.set("Y");
			}
		}
		scrnparams.function.set(varcom.srdn);
	}

protected void checkUpdate2800()
	{
		/*CHECK*/
		flupaltIO.setParams(SPACES);
		flupaltIO.setChdrcoy(chdrpf.getChdrcoy());
		flupaltIO.setChdrnum(chdrpf.getChdrnum());
		flupaltIO.setFupno(sv.fupno);
		flupaltIO.setFunction(varcom.readr);
		flupFile3200();
		/*    IF  SR590-FUPCODE           = FLUPALT-FUPCODE                */
		if (isEQ(sv.fupcdes, flupaltIO.getFupcode())
		&& isEQ(sv.fupremk, flupaltIO.getFupremk())
		&& isEQ(sv.fupstat, flupaltIO.getFupstat())
		&& isEQ(sv.fupremdt, flupaltIO.getFupremdt())
		&& isEQ(sv.lifeno, flupaltIO.getLife())
		&& isEQ(sv.jlife, flupaltIO.getJlife())) {
			/*NEXT_SENTENCE*/
		}
		else {
			sv.uprflag.set("U");
		}
		/*EXIT*/
	}

protected void updateSubfile3500()
	{
		/*UPDATE*/
		scrnparams.function.set(varcom.supd);
		processScreen("SR590", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 2600 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
		if (isEQ(wsspcomn.programPtr.toInt(),"1")) {  //IBPLIFE-3300
		rlseSftlock3200();//IBPLIFE-2353
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		/*  Update database files as required*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		wsaaChdrmjaTranno.set(chdrpf.getTranno());
		wsaaChdrmjaTranno.add(1);
		if (pendingPhReinstate.isTrue()
		&& isEQ(wsspcomn.flag, "M")) {
			wsaaChdrmjaTranno.set(chdrpf.getTranno());
		}
		wsaaUpdPtrnFlag.set(SPACES);
		for (wsaaUserFieldsInner.wsaaCount.set(1); !(isGT(wsaaUserFieldsInner.wsaaCount, sv.subfilePage)); wsaaUserFieldsInner.wsaaCount.add(1)){
			updateFollowUp3100();
		}
		/*  If there is any update done to follow up file, log the*/
		/*  transaction in the history...*/
		if (isEQ(wsaaUpdPtrnFlag, "Y")
		&& isNE(wsaaFinishUpdFlag, "Y")
		&& !pendingPhReinstate.isTrue()
		&& !CSMIN003Permission) {
			updatePtrn3400();
		}
		/*  Update HPAD with Doctor details, if applicable*/
		if (hpadpf.getZdoctor()==null || !hpadpf.getZdoctor().trim().equals(sv.zdoctor.trim())) {
			hpadpfDAO.updateDoctor(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum(),sv.zdoctor.toString());/* IJTI-1523 */
		}
		
	}

	/**
	* <pre>
	*     DO THE FOLLOW UP FILE UPDATING
	* </pre>
	*/
protected void updateFollowUp3100()
	{
		updateFollowUp3110();
	}

protected void updateFollowUp3110()
	{
		/* Get the subfile record using the relative record number.*/
		scrnparams.subfileRrn.set(wsaaUserFieldsInner.wsaaCount);
		scrnparams.function.set(varcom.sread);
		processScreen("SR590", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/* Set up key.*/
		flupaltIO.setParams(SPACES);
		flupaltIO.setChdrcoy(chdrpf.getChdrcoy());
		flupaltIO.setChdrnum(chdrpf.getChdrnum());
		flupaltIO.setFupno(sv.fupno);
		/* Save first record in the subfile for ROLD.*/
		if (isEQ(wsaaUserFieldsInner.wsaaCount, 1)) {
			wsaaUserFieldsInner.wsaaFirstFlupKey.set(flupaltIO.getRecKeyData());
		}
		/* The subfile record has not been changed.*/
		if (isEQ(sv.uprflag, SPACES)
		|| isEQ(sv.uprflag, "N")
		|| isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/* If the update is a delete.*/
		if (isEQ(sv.uprflag, "D")) {
			wsaaUpdPtrnFlag.set("Y");
			deleteAllHxcl3300();
			flupaltIO.setFunction(varcom.readh);
			flupFile3200();
			deleteFupe3600();
			flupaltIO.setFormat(flupaltrec);
			flupaltIO.setFunction(varcom.delet);
			flupFile3200();
			sv.uprflag.set("N");
			updateSubfile3500();
		}
		/* If the update is an addition.*/
		if (isEQ(sv.uprflag, "A")) {
			wsaaUpdPtrnFlag.set("Y");
			flupaltIO.setFuptype("A");
			flupaltIO.setTranno(wsaaChdrmjaTranno);
			flupaltIO.setLife(sv.lifeno);
			if (isEQ(sv.jlife, SPACES)) {
				flupaltIO.setJlife("00");
			}
			else {
				flupaltIO.setJlife(sv.jlife);
			}
			/*       MOVE SR590-FUPCODE       TO FLUPALT-FUPCODE               */
			flupaltIO.setFupcode(sv.fupcdes);
			flupaltIO.setFupstat(sv.fupstat);
			flupaltIO.setFupremdt(sv.fupremdt);
			flupaltIO.setFuptype(sv.fuptype);
			flupaltIO.setFupremk(sv.fupremk);
			flupaltIO.setTermid(varcom.vrcmTermid);
			flupaltIO.setUser(varcom.vrcmUser);
			flupaltIO.setTransactionDate(varcom.vrcmDate);
			flupaltIO.setTransactionTime(varcom.vrcmTime);
			flupaltIO.setEffdate(wsaaToday);
			flupaltIO.setZlstupdt(varcom.vrcmMaxDate);
			flupaltIO.setCrtuser(sv.crtuser);
			flupaltIO.setCrtdate(sv.crtdate);
			flupaltIO.setFuprcvd(sv.fuprcvd);
			flupaltIO.setExprdate(sv.exprdate);
			/*    MOVE VRCM-MAX-DATE       TO FLUPALT-EFFDATE       <LA4592>*/
			flupaltIO.setFormat(flupaltrec);
			writeFupe3700();
			flupaltIO.setFunction(varcom.writr);
			flupFile3200();
			sv.uprflag.set("N");
			updateSubfile3500();
		}
		/* If the update is an update.*/
		if (isEQ(sv.uprflag, "U")) {
			flupaltIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, flupaltIO);
			if (isNE(flupaltIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(flupaltIO.getParams());
				fatalError600();
			}
			wsaaUpdPtrnFlag.set("Y");
			flupaltIO.setFunction(varcom.readh);
			flupFile3200();
			flupaltIO.setLife(sv.lifeno);
			if (isEQ(sv.jlife, SPACES)) {
				flupaltIO.setJlife("00");
			}
			else {
				flupaltIO.setJlife(sv.jlife);
			}
			/*       MOVE SR590-FUPCODE       TO FLUPALT-FUPCODE               */
			flupaltIO.setFupcode(sv.fupcdes);
			flupaltIO.setFupstat(sv.fupstat);
			flupaltIO.setFupremdt(sv.fupremdt);
			flupaltIO.setFuptype(sv.fuptype);
			flupaltIO.setFuprcvd(sv.fuprcvd);
			flupaltIO.setExprdate(sv.exprdate);
			flupaltIO.setFupremk(sv.fupremk);
			flupaltIO.setTermid(varcom.vrcmTermid);
			flupaltIO.setUser(varcom.vrcmUser);
			flupaltIO.setTransactionDate(varcom.vrcmDate);
			flupaltIO.setTransactionTime(varcom.vrcmTime);
			flupaltIO.setLstupuser(wsspcomn.userid);
			flupaltIO.setZlstupdt(datcon1rec.intDate);
			flupaltIO.setFormat(flupaltrec);
			flupaltIO.setFunction(varcom.rewrt);
			flupFile3200();
			sv.uprflag.set("N");
			updateSubfile3500();
		}
	}

	/**
	* <pre>
	*     ACCESS THE FLUPALT FILE
	* </pre>
	*/
protected void flupFile3200()
	{
		/*FLUP-FILE*/
		SmartFileCode.execute(appVars, flupaltIO);
		if (isNE(flupaltIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(flupaltIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void deleteAllHxcl3300()
	{
		startDelete3310();
	}

protected void startDelete3310()
	{
	 //ILIFE-8234 start
	hxclpfDAO.deleteRecords(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), sv.fupno.toInt());/* IJTI-1523 */
	 //ILIFE-8234 end	

	}

protected void updatePtrn3400()
	{
		List<Chdrpf> chdrList = new ArrayList<>();
		chdrList.add(chdrpf);
		chdrpfDAO.updateInvalidChdrRecords(chdrList);
		
		
		/* Write new record.*/
		chdrpf.setValidflag('1');
		setPrecision(chdrpf.getTranno(), 0);
		chdrpf.setTranno(chdrpf.getTranno() + 1);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrpf.setTranid(varcom.vrcmCompTranid.toString());
		chdrpfDAO.insertChdrRecords(chdrpf);
		
		/* Write PTRN record.*/
		 //ILIFE-8234 start
		ptrnpf = new Ptrnpf();
		ptrnpf.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
		ptrnpf.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
		ptrnpf.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
		ptrnpf.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
		ptrnpf.setBatccoy(wsaaBatckey.batcBatccoy.toString());
		ptrnpf.setBatcpfx(wsaaBatckey.batcBatcpfx.toString());
		ptrnpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
	//	ptrnpf.setDataKey(wsspcomn.batchkey);
		ptrnpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrpf.getChdrnum());
	
		ptrnpf.setTranno(chdrpf.getTranno());
		ptrnpf.setDatesub(wsaaToday.toInt());
		ptrnpf.setTrdt(varcom.vrcmDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setTermid(varcom.vrcmTermid.toString());
		ptrnpf.setUserT(varcom.vrcmUser.toInt());
		ptrnpf.setPtrneff(wsaaToday.toInt());
		ptrnpfDAO.insertPtrnPF(ptrnpf);
		 //ILIFE-8234 end
		wsaaFinishUpdFlag.set("Y");
	}

protected void deleteFupe3600()
	{
		start3610();
		callFupeio3620();
	}

protected void start3610()
	{
		fupeIO.setParams(SPACES);
		fupeIO.setStatuz(varcom.oK);
		fupeIO.setChdrcoy(flupaltIO.getChdrcoy());
		fupeIO.setChdrnum(flupaltIO.getChdrnum());
		fupeIO.setFupno(sv.fupno);
		fupeIO.setClamnum(SPACES);
		fupeIO.setTranno(flupaltIO.getTranno());
		fupeIO.setDocseq(ZERO);
		fupeIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fupeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fupeIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "FUPNO", "TRANNO");
		fupeIO.setFormat(fuperec);
	}

protected void callFupeio3620()
	{
		SmartFileCode.execute(appVars, fupeIO);
		if (isNE(fupeIO.getStatuz(), varcom.oK)
		&& isNE(fupeIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(fupeIO.getParams());
			syserrrec.statuz.set(fupeIO.getStatuz());
			fatalError600();
		}
		if (isEQ(fupeIO.getStatuz(), varcom.endp)
		|| isNE(fupeIO.getChdrcoy(), flupaltIO.getChdrcoy())
		|| isNE(fupeIO.getChdrnum(), flupaltIO.getChdrnum())
		|| isNE(fupeIO.getFupno(), sv.fupno)
		|| isNE(fupeIO.getClamnum(), flupaltIO.getClamnum())
		|| isNE(fupeIO.getTranno(), flupaltIO.getTranno())) {
			return ;
		}
		fupeIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, fupeIO);
		if (isNE(fupeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fupeIO.getParams());
			syserrrec.statuz.set(fupeIO.getStatuz());
			fatalError600();
		}
		fupeIO.setFunction(varcom.nextr);
		callFupeio3620();
		return ;
	}

protected void writeFupe3700()
	{
		start3710();
		nextExtendedText3730();
	}

protected void start3710()
	{
		wsaaDocseq.set(ZERO);
		
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(sv.fupcdes);
		itempf = new Itempf();
	    itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5661");
		itempf.setItemitem(wsaaT5661Key.toString().trim());
	    itempf = itemDAO.getItempfRecord(itempf);
	    if (itempf == null) {
	    	fatalError600();
		}
	    t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		iy.set(ZERO);
		for (ix.set(5); !(isEQ(ix, ZERO)
		|| isNE(iy, ZERO)); ix.add(-1)){
			if (isNE(t5661rec.message[ix.toInt()], SPACES)) {
				iy.set(ix);
			}
		}
		ix.set(ZERO);
	}

protected void nextExtendedText3730()
	{
		ix.add(1);
		if (isGT(ix, 5)
		|| isGT(ix, iy)) {
			return ;
		}
		wsaaDocseq.add(1);
		fupeIO.setParams(SPACES);
		fupeIO.setStatuz(varcom.oK);
		fupeIO.setChdrcoy(flupaltIO.getChdrcoy());
		fupeIO.setChdrnum(flupaltIO.getChdrnum());
		fupeIO.setFupno(sv.fupno);
		fupeIO.setClamnum(SPACES);
		fupeIO.setTranno(flupaltIO.getTranno());
		fupeIO.setDocseq(wsaaDocseq);
		fupeIO.setMessage(t5661rec.message[ix.toInt()]);
		fupeIO.setFunction(varcom.writr);
		fupeIO.setFormat(fuperec);
		SmartFileCode.execute(appVars, fupeIO);
		if (isNE(fupeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fupeIO.getParams());
			syserrrec.statuz.set(fupeIO.getStatuz());
			fatalError600();
		}
		nextExtendedText3730();
		return ;
	}

//IBPLIFE-2353------Start
protected void rlseSftlock3200()
{
	Slckpf slck = new Slckpf();
	slckList = new ArrayList<Slckpf>();
	slck.setEntity(chdrpf.getChdrnum());
	slck.setEnttyp("CH"); 	 
	slck.setUsrprf(((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER));
	slckList = slckpfDAO.searchSlck(slck);
	if(!ObjectUtils.isEmpty(slckList))
	{
	unlockContract3210();
	}
}

protected void unlockContract3210()
{
	sftlockrec.sftlockRec.set(SPACES);
	sftlockrec.statuz.set(varcom.oK);
	sftlockrec.company.set(wsspcomn.company);
	sftlockrec.entity.set(chdrpf.getChdrnum());//ILIFE-7968 //IJTI-1485
	sftlockrec.enttyp.set("CH");
	sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
	sftlockrec.user.set(varcom.vrcmUser);
	sftlockrec.function.set("UNLK");
	callProgram(Sftlock.class, sftlockrec.sftlockRec);
	if (isNE(sftlockrec.statuz,varcom.oK)) {
		syserrrec.params.set(sftlockrec.sftlockRec);
		syserrrec.statuz.set(sftlockrec.statuz);
		fatalError600();
	}
	//IBPLIFE-2353------End
}


	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			scrnparams.function.set("HIDEW");
			return ;
		}
		if (isEQ(wsaaUserFieldsInner.wsaaRoluRold, varcom.rolu)
		|| isEQ(wsaaUserFieldsInner.wsaaRoluRold, varcom.rold)) {
			wsspcomn.nextprog.set(wsaaProg);
			return ;
		}
		else {
			optswchCall4100();
		}
		/*EXIT*/
	}

protected void optswchCall4100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					call4110();
					lineSwitching4120();
				case switch4170:
					switch4170();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void call4110()
	{
		wsspcomn.nextprog.set(wsaaProg);
	}

	/**
	* <pre>
	* Check for Line Switching
	* </pre>
	*/
protected void lineSwitching4120()
	{
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			scrnparams.subfileRrn.set(ZERO);
		}
		else {
			scrnparams.subfileRrn.set(wsaaRrn);
		}
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("SR590", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isNE(sv.select, SPACES)
		|| isEQ(scrnparams.statuz, varcom.endp))) {
			if (isNE(sv.fupno, SPACES)
			&& isNE(sv.fupcdes, SPACES)
			&& isNE(sv.lifeno, SPACES)
			&& isNE(sv.jlife, SPACES)) {
				fupeIO.setParams(SPACES);
				fupeIO.setChdrcoy(wsspcomn.company);
				fupeIO.setChdrnum(sv.chdrnum);
				fupeIO.setFupno(sv.fupno);
				fupeIO.setClamnum(SPACES);
				fupeIO.setTranno(ZERO);
				fupeIO.setDocseq(ZERO);
				fupeIO.setFunction(varcom.begn);
				//performance improvement -- Anjali
				fupeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
				fupeIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "FUPNO");
				fupeIO.setFormat(fuperec);
				SmartFileCode.execute(appVars, fupeIO);
				if (isNE(fupeIO.getStatuz(), varcom.oK)
				&& isNE(fupeIO.getStatuz(), varcom.endp)) {
					syserrrec.statuz.set(fupeIO.getStatuz());
					syserrrec.params.set(fupeIO.getParams());
					fatalError600();
				}
				if (isNE(fupeIO.getChdrcoy(), wsspcomn.company)
				|| isNE(fupeIO.getChdrnum(), sv.chdrnum)
				|| isNE(fupeIO.getFupno(), sv.fupno)) {
					fupeIO.setStatuz(varcom.endp);
				}
				if (isEQ(fupeIO.getStatuz(), varcom.endp)) {
					sv.actn.set(SPACES);
					sv.indic.set(SPACES);
				}
				else {
					sv.actn.set("+");
					sv.indic.set("+");
				}
				flupaltIO.setChdrcoy(wsspcomn.company);
				flupaltIO.setChdrnum(sv.chdrnum);
				flupaltIO.setFupno(sv.fupno);
				checkHxcl1400();
			}
			sv.zitem.set(sv.fupcdes);
			scrnparams.function.set(varcom.supd);
			processScreen("SR590", sv);
			if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			scrnparams.function.set(varcom.srdn);
			processScreen("SR590", sv);
			if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}

		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.switch4170);
		}
		/* ...if found, "select" the subfile line to switch to PopUp*/
		/* program.*/
		/* READR and KEEPS the FLUP record for the next program*/
		flupaltIO.setParams(SPACES);
		flupaltIO.setChdrcoy(wsspcomn.company);
		flupaltIO.setChdrnum(sv.chdrnum);
		flupaltIO.setFupno(sv.fupno);
		flupaltIO.setFormat(flupaltrec);
		flupaltIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, flupaltIO);
		if (isNE(flupaltIO.getStatuz(), varcom.oK)
		&& isNE(flupaltIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(flupaltIO.getParams());
			fatalError600();
		}
		if (isEQ(flupaltIO.getStatuz(), varcom.mrnf)) {
			/*        IF  SR590-FUPCODE    = SPACES                            */
			if (isEQ(sv.fupcdes, SPACES)
			&& isEQ(sv.fupremk, SPACES)
			&& isEQ(sv.fupstat, SPACES)
			&& (isEQ(sv.fupremdt, ZERO)
			|| isEQ(sv.fupremdt, 99999999)
			|| isEQ(sv.fupremdt, SPACES))
			&& isEQ(sv.lifeno, ZERO)
			&& isEQ(sv.jlife, SPACES)) {
				goTo(GotoLabel.switch4170);
			}
		}
		flupaltIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, flupaltIO);
		if (isNE(flupaltIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(flupaltIO.getParams());
			fatalError600();
		}
		wsaaRrn.set(scrnparams.subfileRrn);
		optswchrec.optsSelType.set("L");
		optswchrec.optsSelOptno.set(sv.select);
		optswchrec.optsSelCode.set(SPACES);
		/* Update the subfile line.*/
		sv.select.set(SPACES);
		/*    MOVE SR590-FUPCODE          TO SR590-ZITEM.                  */
		sv.zitem.set(sv.fupcdes);
		sv.uprflag.set("N");
		scrnparams.function.set(varcom.supd);
		processScreen("SR590", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void switch4170()
	{
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)
		&& isNE(optswchrec.optsStatuz, varcom.endp)) {
			syserrrec.params.set(optswchrec.rec);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			fatalError600();
		}
		/*    MOVE OPTS-IND(1)            TO SR590-ZSAIND.*/
		if (isEQ(optswchrec.optsStatuz, varcom.oK)) {
			wsspcomn.programPtr.add(1);
			scrnparams.subfileRrn.set(wsaaRrn);
		}
		else {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void setReminderDate5000()
	{
		setRemdt5010();
	}

protected void setRemdt5010()
	{
		/*  Calculate the date of issuing reminder letter*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(sv.fupcdes);
		itempf = new Itempf();
	    itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5661");
		itempf.setItemitem(wsaaT5661Key.toString().trim());
	    itempf = itemDAO.getItempfRecord(itempf);
	    if (itempf == null) {
	    	t5661rec.t5661Rec.set(SPACES);
		}else{
			t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	    
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(datcon1rec.intDate);
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(t5661rec.zelpdays);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		sv.fupremdt.set(datcon2rec.intDate2);
	}
/*
 * Class transformed  from Data Structure WSAA-USER-FIELDS--INNER
 */
private static final class WsaaUserFieldsInner {
		/* WSAA-USER-FIELDS */
	private FixedLengthStringData wsaaDoctFlup = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPayeeName = new FixedLengthStringData(5).init("PYNMN");
	private FixedLengthStringData wsaaFirstDisplay = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaRoluRold = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(5, 0).setUnsigned();
	private FixedLengthStringData wsaaFirstPage = new FixedLengthStringData(1);
	private PackedDecimalData wsaaFupno = new PackedDecimalData(2, 0).init(ZERO);

	private FixedLengthStringData wsaaFirstFlupKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaFlupKeyChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaFirstFlupKey, 0);
	private FixedLengthStringData wsaaFlupKeyChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaFirstFlupKey, 1);
	private PackedDecimalData wsaaFlupKeyFupno = new PackedDecimalData(2, 0).isAPartOf(wsaaFirstFlupKey, 9);
	private FixedLengthStringData filler = new FixedLengthStringData(53).isAPartOf(wsaaFirstFlupKey, 11, FILLER).init(SPACES);
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner {
	private FixedLengthStringData e040 = new FixedLengthStringData(4).init("E040");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData f498 = new FixedLengthStringData(4).init("F498");
	private FixedLengthStringData f499 = new FixedLengthStringData(4).init("F499");
	private FixedLengthStringData e558 = new FixedLengthStringData(4).init("E558");
	private FixedLengthStringData e557 = new FixedLengthStringData(4).init("E557");
	private FixedLengthStringData e350 = new FixedLengthStringData(4).init("E350");
	private FixedLengthStringData h031 = new FixedLengthStringData(4).init("H031");
	private FixedLengthStringData hl41 = new FixedLengthStringData(4).init("HL41");
	private FixedLengthStringData hl43 = new FixedLengthStringData(4).init("HL43");
	private FixedLengthStringData rf00 = new FixedLengthStringData(4).init("RF00");
	private FixedLengthStringData w431 = new FixedLengthStringData(4).init("W431");
	private FixedLengthStringData f073 = new FixedLengthStringData(4).init("F073");
	private FixedLengthStringData g914 = new FixedLengthStringData(4).init("G914");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData clrfrec = new FixedLengthStringData(10).init("CLRFREC");
	private FixedLengthStringData fluplnbrec = new FixedLengthStringData(10).init("FLUPLNBREC");
	private FixedLengthStringData hpadrec = new FixedLengthStringData(10).init("HPADREC");
	private FixedLengthStringData hxclrec = new FixedLengthStringData(10).init("HXCLREC");
	private FixedLengthStringData fuperec = new FixedLengthStringData(10).init("FUPEREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
}
}