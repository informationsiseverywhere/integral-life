/*
 * File: Bm501.java
 * Date: 29 August 2009 21:47:23
 * Author: Quipoz Limited
 *
 * Class transformed from BM501.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.accounting.tablestructures.T3627rec;
import com.csc.fsu.agents.dataaccess.GenvTableDAM;
import com.csc.life.newbusiness.reports.Rm501Report;
import com.csc.life.newbusiness.tablestructures.Tm600rec;
import com.csc.life.newbusiness.tablestructures.Tm601rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.dataaccess.ItemsmtTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*   This is a skeleton for a batch mainline program.
*
*   The basic procedure division logic is for reading and
*     printing a simple input primary file. The overall structure
*     is as follows:
*
*   Initialise
*     - retrieve and set up standard report headings.
*
*    Read
*     - read first primary file record.
*
*    Perform    Until End of File
*
*      Edit
*       - Check if the primary file record is required
*       - Softlock the record if it is to be updated
*
*      Update
*       - update database files
*       - write details to report while not primary file EOF
*       - look up referred to records for output details
*       - if new page, write headings
*       - write details
*
*      Read next primary file record
*
*    End Perform
*
*   Control totals:
*     01  -  Number of pages printed
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*   These remarks must be replaced by what the program actually
*     does.
*
*****************************************************************
* </pre>
*/
public class Bm501 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Rm501Report printerFile = new Rm501Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BM501");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaPremium = new ZonedDecimalData(9, 0).init(ZERO);
	private ZonedDecimalData wsaaActual = new ZonedDecimalData(9, 0).init(ZERO);
	private ZonedDecimalData wsaaAllow = new ZonedDecimalData(9, 0).init(ZERO);
	private ZonedDecimalData wsaaOccpct01Totl = new ZonedDecimalData(5, 2).init(ZERO);
	private ZonedDecimalData wsaaOccpct02Totl = new ZonedDecimalData(11, 2).init(ZERO);
	private ZonedDecimalData wsaaOccpct = new ZonedDecimalData(11, 2).init(ZERO);
	private ZonedDecimalData wsaaPremiumTotl = new ZonedDecimalData(9, 0).init(ZERO);
	private ZonedDecimalData wsaaActualTotl = new ZonedDecimalData(9, 0).init(ZERO);
	private ZonedDecimalData wsaaAllowTotl = new ZonedDecimalData(9, 0).init(ZERO);
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaNdx = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaMonth = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaItemCount = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaDateFmt = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaDateFmtRedf = new FixedLengthStringData(8).isAPartOf(wsaaDateFmt, 0, REDEFINE);
	private ZonedDecimalData wsaaFmtYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaDateFmtRedf, 0).setUnsigned();
	private String descrec = "DESCREC";
	private String genvrec = "GENVREC";
	private String itemrec = "ITEMREC";
	private String itemsmtrec = "ITEMSMTREC";
		/* TABLES */
	private String t1692 = "T1692";
	private String t1693 = "T1693";
	private String t3627 = "T3627";
	private String tm600 = "TM600";
	private String tm601 = "TM601";

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rm501H01 = new FixedLengthStringData(87);
	private FixedLengthStringData rm501h01O = new FixedLengthStringData(87).isAPartOf(rm501H01, 0);
	private FixedLengthStringData rh01Mthldesc = new FixedLengthStringData(10).isAPartOf(rm501h01O, 0);
	private ZonedDecimalData rh01Year = new ZonedDecimalData(4, 0).isAPartOf(rm501h01O, 10);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rm501h01O, 14);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rm501h01O, 15);
	private FixedLengthStringData rh01Repdate = new FixedLengthStringData(10).isAPartOf(rm501h01O, 45);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(rm501h01O, 55);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(rm501h01O, 57);

	private FixedLengthStringData rm501H02 = new FixedLengthStringData(30);
	private FixedLengthStringData rm501h02O = new FixedLengthStringData(30).isAPartOf(rm501H02, 0);
	private FixedLengthStringData rh02Genldesc = new FixedLengthStringData(30).isAPartOf(rm501h02O, 0);

	private FixedLengthStringData rm501D01 = new FixedLengthStringData(67);
	private FixedLengthStringData rm501d01O = new FixedLengthStringData(67).isAPartOf(rm501D01, 0);
	private FixedLengthStringData rd01Genldesc = new FixedLengthStringData(30).isAPartOf(rm501d01O, 0);
	private ZonedDecimalData rd01Occpct = new ZonedDecimalData(5, 2).isAPartOf(rm501d01O, 30);
	private ZonedDecimalData rd01Occprem01 = new ZonedDecimalData(9, 0).isAPartOf(rm501d01O, 35);
	private ZonedDecimalData rd01Occprem02 = new ZonedDecimalData(9, 0).isAPartOf(rm501d01O, 44);
	private ZonedDecimalData rd01Occprem03 = new ZonedDecimalData(9, 0).isAPartOf(rm501d01O, 53);
	private ZonedDecimalData rd01Occpct02 = new ZonedDecimalData(5, 2).isAPartOf(rm501d01O, 62);

	private FixedLengthStringData rm501S01 = new FixedLengthStringData(37);
	private FixedLengthStringData rm501s01O = new FixedLengthStringData(37).isAPartOf(rm501S01, 0);
	private ZonedDecimalData rs01Occpct = new ZonedDecimalData(5, 2).isAPartOf(rm501s01O, 0);
	private ZonedDecimalData rs01Occprem01 = new ZonedDecimalData(9, 0).isAPartOf(rm501s01O, 5);
	private ZonedDecimalData rs01Occprem02 = new ZonedDecimalData(9, 0).isAPartOf(rm501s01O, 14);
	private ZonedDecimalData rs01Occprem03 = new ZonedDecimalData(9, 0).isAPartOf(rm501s01O, 23);
	private ZonedDecimalData rs01Occpct02 = new ZonedDecimalData(5, 2).isAPartOf(rm501s01O, 32);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: General Ledger monthly bal*/
	private GenvTableDAM genvIO = new GenvTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Smart Product Item file*/
	private ItemsmtTableDAM itemsmtIO = new ItemsmtTableDAM();
	private T3627rec t3627rec = new T3627rec();
	private Tm600rec tm600rec = new Tm600rec();
	private Tm601rec tm601rec = new Tm601rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextTm6002080,
		exit2099,
		process2110,
		exit2190,
		process2210,
		exit2290,
		continue2310
	}

	public Bm501() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingCompany1020();
		setUpHeadingBranch1030();
		setUpHeadingDates1040();
		setUpCalenderDate1050();
		printHeader1050();
		initMain1070();
	}

protected void initialise1010()
	{
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
		wsaaItemitem.set(SPACES);
		wsaaCount.set(ZERO);
	}

protected void setUpHeadingCompany1020()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(bsprIO.getCompany());
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch1030()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		descIO.setDescitem(bprdIO.getDefaultBranch());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Branch.set(bprdIO.getDefaultBranch());
		rh01Branchnm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Repdate.set(datcon1rec.extDate);
		wsaaDateFmt.set(datcon1rec.intDate);
	}

protected void setUpCalenderDate1050()
	{
		wsaaDateFmt.set(bsscIO.getEffectiveDate());
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t3627);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(bsscIO.getLanguage().toString());
		stringVariable1.append(bsprIO.getCompany().toString());
		itemIO.getItemitem().setLeft(stringVariable1.toString());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3627rec.t3627Rec.set(itemIO.getGenarea());
		rh01Mthldesc.set(t3627rec.mthldesc[bsscIO.getAcctMonth().toInt()]);
		rh01Year.set(wsaaFmtYear);
	}

protected void printHeader1050()
	{
		printerFile.printRm501h01(rm501H01);
		wsaaOverflow.set("N");
	}

protected void initMain1070()
	{
		itemsmtIO.setDataKey(SPACES);
		itemsmtIO.setItempfx("IT");
		itemsmtIO.setItemcoy(bsprIO.getCompany());
		itemsmtIO.setItemtabl(tm600);
		itemsmtIO.setItemitem(SPACES);
		itemsmtIO.setItemitem(bsscIO.getLanguage());
		itemsmtIO.setItemseq(SPACES);
		itemsmtIO.setFormat(itemsmtrec);
		itemsmtIO.setFunction(varcom.begn);
		/*EXIT*/
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2010();
				}
				case nextTm6002080: {
					nextTm6002080();
				}
				case exit2099: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2010()
	{
		SmartFileCode.execute(appVars, itemsmtIO);
		if (isNE(itemsmtIO.getStatuz(),varcom.oK)
		&& isNE(itemsmtIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itemsmtIO.getStatuz());
			syserrrec.params.set(itemsmtIO.getParams());
			fatalError600();
		}
		if (isNE(itemsmtIO.getItemtabl(),tm600)
		|| isNE(subString(itemsmtIO.getItemitem(), 1, 1),bsscIO.getLanguage())
		|| isNE(itemsmtIO.getItemcoy(),bsprIO.getCompany())) {
			itemsmtIO.setStatuz(varcom.endp);
		}
		if (isEQ(itemsmtIO.getStatuz(),varcom.endp)) {
			if (isNE(wsaaCount,ZERO)) {
				printSubtotal3100();
			}
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit2099);
		}
		if (isEQ(itemsmtIO.getStatuz(),varcom.oK)) {
			tm600rec.tm600Rec.set(itemsmtIO.getGenarea());
			if (isEQ(tm600rec.genldesc,SPACES)) {
				if (isNE(wsaaCount,0)) {
					printSubtotal3100();
				}
				printSubheader3200();
				goTo(GotoLabel.nextTm6002080);
			}
			processPremium2100();
			processActual2200();
			calcFigure2300();
		}
	}

protected void nextTm6002080()
	{
		itemsmtIO.setFunction(varcom.nextr);
	}

protected void processPremium2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2100();
				}
				case process2110: {
					process2110();
					nextr2180();
				}
				case exit2190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2100()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tm601);
		itemIO.setItemitem(tm600rec.genlcdex);
		wsaaItemitem.set(tm600rec.genlcdex);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMTABL", "ITEMITEM");
	}

protected void process2110()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.endp)
		|| isNE(itemIO.getItemtabl(),tm601)
		|| isNE(itemIO.getItemitem(),wsaaItemitem)
		|| isNE(itemIO.getItemcoy(),"7")) {
			goTo(GotoLabel.exit2190);
		}
		tm601rec.tm601Rec.set(itemIO.getGenarea());
		wsaaCount.add(1);
		for (wsaaNdx.set(1); !(isGT(wsaaNdx,30)); wsaaNdx.add(1)){
			calcPremium2150();
		}
	}

protected void nextr2180()
	{
		itemIO.setFunction(varcom.nextr);
		goTo(GotoLabel.process2110);
	}

protected void calcPremium2150()
	{
		/*CALCULATE*/
		genvIO.setGenvcde(tm601rec.genlcdex[wsaaNdx.toInt()]);
		readGenv2400();
		if (isEQ(genvIO.getStatuz(),varcom.oK)) {
			for (wsaaMonth.set(1); !(isGT(wsaaMonth,bsscIO.getAcctMonth())); wsaaMonth.add(1)){
				compute(wsaaPremium, 1).setRounded(add(wsaaPremium,genvIO.getMonth(wsaaMonth)));
			}
		}
		/*EXIT*/
	}

protected void processActual2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2200();
				}
				case process2210: {
					process2210();
					nextr12180();
				}
				case exit2290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2200()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tm601);
		itemIO.setItemitem(tm600rec.genlcdey);
		wsaaItemitem.set(tm600rec.genlcdey);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMTABL", "ITEMITEM");
	}

protected void process2210()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.endp)
		|| isNE(itemIO.getItemtabl(),tm601)
		|| isNE(itemIO.getItemitem(),wsaaItemitem)
		|| isNE(itemIO.getItemcoy(),"7")) {
			goTo(GotoLabel.exit2290);
		}
		tm601rec.tm601Rec.set(itemIO.getGenarea());
		wsaaCount.add(1);
		for (wsaaNdx.set(1); !(isGT(wsaaNdx,30)); wsaaNdx.add(1)){
			calcActual2250();
		}
	}

protected void nextr12180()
	{
		itemIO.setFunction(varcom.nextr);
		goTo(GotoLabel.process2210);
	}

protected void calcActual2250()
	{
		/*CALCULATE*/
		genvIO.setGenvcde(tm601rec.genlcdex[wsaaNdx.toInt()]);
		readGenv2400();
		if (isEQ(genvIO.getStatuz(),varcom.oK)) {
			for (wsaaMonth.set(1); !(isGT(wsaaMonth,bsscIO.getAcctMonth())); wsaaMonth.add(1)){
				compute(wsaaActual, 1).setRounded(add(wsaaActual,genvIO.getMonth(wsaaMonth)));
			}
		}
		/*EXIT*/
	}

protected void calcFigure2300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					calculate2300();
				}
				case continue2310: {
					continue2310();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void calculate2300()
	{
		compute(wsaaPremium, 1).setRounded(mult(wsaaPremium,-1));
		if (isEQ(tm600rec.ynflag,"Y")) {
			if (isLT(wsaaPremium,tm600rec.statfrom)) {
				wsaaPremium.set(ZERO);
				goTo(GotoLabel.continue2310);
			}
			if (isGT(wsaaPremium,tm600rec.statto)) {
				compute(wsaaPremium, 0).set(add(sub(tm600rec.statto,tm600rec.statfrom),1));
				goTo(GotoLabel.continue2310);
			}
			if (isGT(wsaaPremium,tm600rec.statfrom)
			&& isLT(wsaaPremium,tm600rec.statto)) {
				compute(wsaaPremium, 0).set(sub(wsaaPremium,tm600rec.statfrom));
				goTo(GotoLabel.continue2310);
			}
		}
	}

protected void continue2310()
	{
		compute(wsaaPremium, 1).setRounded(div(wsaaPremium,1000));
		compute(wsaaAllow, 1).setRounded(div(wsaaAllow,1000));
		compute(wsaaActual, 1).setRounded(div(wsaaActual,1000));
		if (isNE(tm600rec.occpct,ZERO)) {
			compute(wsaaAllow, 3).setRounded(div((mult(wsaaPremium,tm600rec.occpct)),100));
		}
		else {
			wsaaAllow.set(ZERO);
		}
		if (isNE(wsaaAllow,ZERO)) {
			compute(wsaaOccpct, 3).setRounded(mult((div(wsaaActual,wsaaAllow)),100));
		}
		else {
			wsaaOccpct.set(ZERO);
		}
		wsaaPremiumTotl.add(wsaaPremium);
		wsaaAllowTotl.add(wsaaAllow);
		wsaaActualTotl.add(wsaaActual);
		wsaaOccpct01Totl.add(tm600rec.occpct);
		rd01Genldesc.set(tm600rec.genldesc);
		rd01Occpct.set(tm600rec.occpct);
		rd01Occprem01.set(wsaaPremium);
		rd01Occprem02.set(wsaaAllow);
		rd01Occprem03.set(wsaaActual);
		rd01Occpct02.set(wsaaOccpct);
		printerFile.printRm501d01(rm501D01, indicArea);
		if (isEQ(wsaaOverflow,"Y")) {
			printerFile.printRm501h01(rm501H01);
			wsaaOverflow.set("N");
		}
		wsaaPremium.set(ZERO);
		wsaaAllow.set(ZERO);
		wsaaActual.set(ZERO);
		wsaaOccpct.set(ZERO);
	}

protected void readGenv2400()
	{
		edit2400();
	}

protected void edit2400()
	{
		genvIO.setGenvpfx("GE");
		genvIO.setGenvcoy(itemIO.getItemcoy());
		genvIO.setGenvcur("RM");
		genvIO.setGenvtyp("TY");
		genvIO.setGenvyr(bsscIO.getAcctYear());
		genvIO.setFormat(genvrec);
		genvIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, genvIO);
		if (isNE(genvIO.getStatuz(),varcom.oK)
		&& isNE(genvIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(genvIO.getStatuz());
			syserrrec.params.set(genvIO.getParams());
			fatalError600();
		}
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/*WRITE-DETAIL*/
		/*EXIT*/
	}

protected void printSubtotal3100()
	{
		edit3100();
	}

protected void edit3100()
	{
		rs01Occpct.set(wsaaOccpct01Totl);
		rs01Occprem01.set(wsaaPremiumTotl);
		rs01Occprem02.set(wsaaAllowTotl);
		rs01Occprem03.set(wsaaActualTotl);
		compute(wsaaOccpct02Totl, 3).setRounded(mult((div(wsaaActualTotl,wsaaAllowTotl)),100));
		rs01Occpct02.set(wsaaOccpct02Totl);
		printerFile.printRm501s01(rm501S01, indicArea);
		if (isEQ(wsaaOverflow,"Y")) {
			printerFile.printRm501h01(rm501H01);
			wsaaOverflow.set("N");
		}
		wsaaPremiumTotl.set(ZERO);
		wsaaAllowTotl.set(ZERO);
		wsaaActualTotl.set(ZERO);
		wsaaOccpct.set(ZERO);
		wsaaOccpct01Totl.set(ZERO);
		wsaaOccpct02Totl.set(ZERO);
		wsaaCount.set(ZERO);
	}

protected void printSubheader3200()
	{
		edit3200();
	}

protected void edit3200()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl("TM600");
		descIO.setDescitem(itemsmtIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage("E");
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh02Genldesc.set(descIO.getLongdesc());
		printerFile.printRm501h02(rm501H02, indicArea);
		if (isEQ(wsaaOverflow,"Y")) {
			printerFile.printRm501h01(rm501H01);
			wsaaOverflow.set("N");
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
