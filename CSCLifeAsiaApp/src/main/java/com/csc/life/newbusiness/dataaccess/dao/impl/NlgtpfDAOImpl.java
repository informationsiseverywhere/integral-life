package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.dao.NlgtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Nlgtpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class NlgtpfDAOImpl extends BaseDAOImpl<Nlgtpf> implements NlgtpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(NlgtpfDAOImpl.class);
	
	public List<Nlgtpf> readNlgtpf(String chdrcoy,String chdrnum) {
	Nlgtpf nlgtpfData = null;
	List<Nlgtpf> nlgtpflist =new ArrayList<Nlgtpf>();

	String sql_select  = "SELECT TRANNO,BATCTRCDE,TRANDESC,EFFDATE,NLGFLAG,TRANAMT,NLGBAL,AMT03,AMT04"
	    		+ " FROM NLGTPF WHERE(CHDRCOY = ? AND CHDRNUM = ? )";
	    
	    ResultSet rs = null;
	    PreparedStatement psSelect=null;
	    try {

	    	psSelect = getPrepareStatement(sql_select);/* IJTI-1523 */
	    	
	    	psSelect.setString(1, chdrcoy);
	    	psSelect.setString(2, chdrnum);
	    	
	        rs = psSelect.executeQuery();
	           while (rs.next()) {
	               
	                	nlgtpfData= new Nlgtpf();
	                	nlgtpfData.setTranno(rs.getInt(1));
	                	nlgtpfData.setBatctrcde(rs.getString(2));
	                	nlgtpfData.setTrandesc(rs.getString(3));
	                	nlgtpfData.setEffdate(rs.getInt(4));
	                	/*nlgtpfData.setBatcactmn(rs.getInt(5));//BRD-009
	                	nlgtpfData.setBatcactyr(rs.getInt(6));//BRD-NBP-011
*/	                	nlgtpfData.setNlgflag(rs.getString(5));
	                	/*nlgtpfData.setFromdate(rs.getInt(8));
	                	nlgtpfData.setTodate(rs.getInt(9));*/
	                	nlgtpfData.setTranamt(rs.getBigDecimal(6));
	                	nlgtpfData.setNlgbal(rs.getBigDecimal(7));
	                	nlgtpfData.setAmnt03(rs.getBigDecimal(8));
	                	nlgtpfData.setAmnt04(rs.getBigDecimal(9));
	                	nlgtpflist.add(nlgtpfData);
	                }
	           
	    } catch (SQLException e) {
	           LOGGER.error("readNlgtpf()", e);//IJTI-1561
	           throw new SQLRuntimeException(e);
	    } finally {
	           close(psSelect, rs);
	    }

	    return nlgtpflist;

}


	
	public Nlgtpf getNlgtRecord(String chdrcoy,String chdrnum) {
		Nlgtpf nlgtpfData = null;
		    String sql_select  = "SELECT TRANNO,EFFDATE,BATCACTYR,BATCACTMN,BATCTRCDE,TRANDESC,TRANAMT,FROMDATE,TODATE,NLGFLAG,YRSINF,AGE,NLGBAL,AMT01,AMT02,AMT03,AMT04"
		    		+ " FROM NLGTPF WHERE(CHDRCOY = ? AND CHDRNUM = ? AND VALIDFLAG='1')";
		    
		    ResultSet rs = null;
		    PreparedStatement psSelect=null;
		    try {

		    	psSelect = getPrepareStatement(sql_select);/* IJTI-1523 */
		    	psSelect.setString(1, chdrcoy);
		    	psSelect.setString(2, chdrnum);
		    	
		        rs = psSelect.executeQuery();
		           while (rs.next()) {
		               
		                	nlgtpfData= new Nlgtpf();
		                	nlgtpfData.setTranno(rs.getInt(1));
		                	nlgtpfData.setEffdate(rs.getInt(2));
		                	nlgtpfData.setBatcactyr(rs.getInt(3));
		                	nlgtpfData.setBatcactmn(rs.getInt(4));
		                	nlgtpfData.setBatctrcde(rs.getString(5));
		                	nlgtpfData.setTrandesc(rs.getString(6));
		                	nlgtpfData.setTranamt(rs.getBigDecimal(7));
		                	nlgtpfData.setFromdate(rs.getInt(8));
		                	nlgtpfData.setTodate(rs.getInt(9));
		                	nlgtpfData.setNlgflag(rs.getString(10));
		                	nlgtpfData.setYrsinf(rs.getInt(11));
		                	nlgtpfData.setAge(rs.getInt(12));
		                	nlgtpfData.setNlgbal(rs.getBigDecimal(13));
		                	nlgtpfData.setAmnt01(rs.getBigDecimal(14));
		                	nlgtpfData.setAmnt02(rs.getBigDecimal(15));
		                	nlgtpfData.setAmnt03(rs.getBigDecimal(16));
		                	nlgtpfData.setAmnt04(rs.getBigDecimal(17));
		                	
		                	
		                }
		           
		    } catch (SQLException e) {
		           LOGGER.error("getNlgtRecord()", e);//IJTI-1561
		           throw new SQLRuntimeException(e);
		    } finally {
		           close(psSelect, rs);
		    }

		    return nlgtpfData;

	}
	public void insertNlgtpf(Nlgtpf nlgtpf) {
		if (nlgtpf != null) {
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO NLGTPF (CHDRCOY, CHDRNUM, TRANNO, EFFDATE, BATCACTYR, BATCACTMN, BATCTRCDE, TRANDESC, TRANAMT, FROMDATE, TODATE, NLGFLAG, YRSINF, AGE, NLGBAL, AMT01, AMT02, AMT03, AMT04, validflag, USRPRF, JOBNM ,DATIME) ");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "); 
		LOGGER.info(sb.toString());
		PreparedStatement ps = getPrepareStatement(sb.toString());

		try {
				ps.setString(1, nlgtpf.getChdrcoy());
				ps.setString(2, nlgtpf.getChdrnum());
				ps.setInt(3, nlgtpf.getTranno());
				ps.setInt(4, nlgtpf.getEffdate());
				ps.setInt(5, nlgtpf.getBatcactyr());
				ps.setInt(6, nlgtpf.getBatcactmn());
				ps.setString(7, nlgtpf.getBatctrcde());
				ps.setString(8, nlgtpf.getTrandesc());
				ps.setBigDecimal(9, nlgtpf.getTranamt());
				ps.setInt(10, nlgtpf.getFromdate());
				ps.setInt(11, nlgtpf.getTodate());
				ps.setString(12, nlgtpf.getNlgflag());
				ps.setInt(13, nlgtpf.getYrsinf());
				ps.setInt(14, nlgtpf.getAge());
				ps.setBigDecimal(15, nlgtpf.getNlgbal());
				ps.setBigDecimal(16, nlgtpf.getAmnt01());
				ps.setBigDecimal(17, nlgtpf.getAmnt02());
				ps.setBigDecimal(18, nlgtpf.getAmnt03());
				ps.setBigDecimal(19, nlgtpf.getAmnt04());
				ps.setString(20, nlgtpf.getValidflag());
				ps.setString(21, this.getUsrprf());
				ps.setString(22, this.getJobnm());
				ps.setTimestamp(23, new Timestamp(System.currentTimeMillis()));
				ps.addBatch();
				int[] rowCount=ps.executeBatch();	
			}
		catch (SQLException e) {
            LOGGER.error("insertNlgtpf()", e);//IJTI-1561
            
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
		
}
	}
	public Nlgtpf getNlgRecord(String chdrcoy,String chdrnum,String validflag) {
		Nlgtpf nlgtpfData = null;
		    String sql_select  = "SELECT TRANNO,EFFDATE,BATCACTYR,BATCACTMN,BATCTRCDE,TRANDESC,TRANAMT,FROMDATE,TODATE,NLGFLAG,YRSINF,AGE,NLGBAL,AMT01,AMT02,AMT03,AMT04"
		    		+ " FROM NLGTPF WHERE(CHDRCOY = ? AND CHDRNUM = ? AND VALIDFLAG = ? ) order by TRANNO ASC ";
		    
		    ResultSet rs = null;
		    PreparedStatement psSelect=null;
		    try {

		    	psSelect = getPrepareStatement(sql_select);/* IJTI-1523 */
		    	psSelect.setString(1, chdrcoy);
		    	psSelect.setString(2, chdrnum);
		    	psSelect.setString(3, validflag);
		    	
		        rs = psSelect.executeQuery();
		           while (rs.next()) {
		               
		                	nlgtpfData= new Nlgtpf();
		                	nlgtpfData.setTranno(rs.getInt(1));
		                	nlgtpfData.setEffdate(rs.getInt(2));
		                	nlgtpfData.setBatcactyr(rs.getInt(3));
		                	nlgtpfData.setBatcactmn(rs.getInt(4));
		                	nlgtpfData.setBatctrcde(rs.getString(5));
		                	nlgtpfData.setTrandesc(rs.getString(6));
		                	nlgtpfData.setTranamt(rs.getBigDecimal(7));
		                	nlgtpfData.setFromdate(rs.getInt(8));
		                	nlgtpfData.setTodate(rs.getInt(9));
		                	nlgtpfData.setNlgflag(rs.getString(10));
		                	nlgtpfData.setYrsinf(rs.getInt(11));
		                	nlgtpfData.setAge(rs.getInt(12));
		                	nlgtpfData.setNlgbal(rs.getBigDecimal(13));
		                	nlgtpfData.setAmnt01(rs.getBigDecimal(14));
		                	nlgtpfData.setAmnt02(rs.getBigDecimal(15));
		                	nlgtpfData.setAmnt03(rs.getBigDecimal(16));
		                	nlgtpfData.setAmnt04(rs.getBigDecimal(17));
		                	
		                	
		                }
		           
		    } catch (SQLException e) {
		           LOGGER.error("getNlgRecord()", e);//IJTI-1561
		           throw new SQLRuntimeException(e);
		    } finally {
		           close(psSelect, rs);
		    }

		    return nlgtpfData;

	}
	
	public Nlgtpf getnlgtran(String chdrcoy,String chdrnum, String tranno) {
		Nlgtpf nlgtpfData = null;
		    String sql_select  = "SELECT  NLGBAL,AMT01,AMT02,AMT03,AMT04, NLGFLAG "
		    		+ " FROM NLGTPF WHERE(CHDRCOY = ? AND CHDRNUM = ? AND VALIDFLAG = '1' AND TRANNO = ?)";
		    
		    ResultSet rs = null;
		    PreparedStatement psSelect=null;
		    try {

		    	psSelect = getPrepareStatement(sql_select);/* IJTI-1523 */
		    	psSelect.setString(1, chdrcoy);
		    	psSelect.setString(2, chdrnum);
		    	psSelect.setString(3, tranno);
		    	
		        rs = psSelect.executeQuery();
		           while (rs.next()) {
		               
		                	nlgtpfData= new Nlgtpf();
		                	nlgtpfData.setNlgbal(rs.getBigDecimal(1));
		                	nlgtpfData.setAmnt01(rs.getBigDecimal(2));
		                	nlgtpfData.setAmnt02(rs.getBigDecimal(3));
		                	nlgtpfData.setAmnt03(rs.getBigDecimal(4));
		                	nlgtpfData.setAmnt04(rs.getBigDecimal(5));
		                	nlgtpfData.setNlgflag(rs.getString(6));
		                	
		                	
		                }
		           
		    } catch (SQLException e) {
		           LOGGER.error("getNlgRecord()", e);//IJTI-1561
		           throw new SQLRuntimeException(e);
		    } finally {
		           close(psSelect, rs);
		    }

		    return nlgtpfData;

	}
	
	public Nlgtpf getnlgmaxtran(String chdrcoy,String chdrnum) {
		Nlgtpf nlgtpfData = null;
		    String sql_select  = "SELECT MAX(Tranno) as TRANNO  FROM NLGTPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND VALIDFLAG = '1' ";
		    
		    ResultSet rs = null;
		    PreparedStatement psSelect=null;
		    try {

		    	psSelect = getPrepareStatement(sql_select);/* IJTI-1523 */
		    	psSelect.setString(1, chdrcoy);
		    	psSelect.setString(2, chdrnum);
		    	
		    	
		        rs = psSelect.executeQuery();
		           while (rs.next()) {
		               
		                	nlgtpfData= new Nlgtpf();
		                	nlgtpfData.setTranno(rs.getInt(1));	                	
		                	
		                }
		           
		    } catch (SQLException e) {
		           LOGGER.error("getNlgRecord()", e);//IJTI-1561
		           throw new SQLRuntimeException(e);
		    } finally {
		           close(psSelect, rs);
		    }

		    return nlgtpfData;

	}
	
}
