package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:28
 * Description:
 * Copybook name: COVTCOVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covtcovkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covtcovFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covtcovKey = new FixedLengthStringData(64).isAPartOf(covtcovFileKey, 0, REDEFINE);
  	public FixedLengthStringData covtcovChdrcoy = new FixedLengthStringData(1).isAPartOf(covtcovKey, 0);
  	public FixedLengthStringData covtcovChdrnum = new FixedLengthStringData(8).isAPartOf(covtcovKey, 1);
  	public FixedLengthStringData covtcovLife = new FixedLengthStringData(2).isAPartOf(covtcovKey, 9);
  	public FixedLengthStringData covtcovCrtable = new FixedLengthStringData(4).isAPartOf(covtcovKey, 11);
  	public FixedLengthStringData covtcovCoverage = new FixedLengthStringData(2).isAPartOf(covtcovKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(covtcovKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covtcovFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covtcovFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}