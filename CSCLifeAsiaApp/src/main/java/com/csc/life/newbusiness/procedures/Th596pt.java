/*
 * File: Th596pt.java
 * Date: 30 August 2009 2:36:23
 * Author: Quipoz Limited
 * 
 * Class transformed from TH596PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.newbusiness.tablestructures.Th596rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TH596.
*
*
*
***********************************************************************
* </pre>
*/
public class Th596pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine001, 28, FILLER).init("Monthly Policy Status Condition            SH596");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(23);
	private FixedLengthStringData filler7 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine003, 5, FILLER).init("Transaction Codes:");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(74);
	private FixedLengthStringData filler9 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 5);
	private FixedLengthStringData filler10 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 9, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 10);
	private FixedLengthStringData filler11 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 15);
	private FixedLengthStringData filler12 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 20);
	private FixedLengthStringData filler13 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 25);
	private FixedLengthStringData filler14 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 30);
	private FixedLengthStringData filler15 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 35);
	private FixedLengthStringData filler16 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 40);
	private FixedLengthStringData filler17 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 45);
	private FixedLengthStringData filler18 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 50);
	private FixedLengthStringData filler19 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 54, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 55);
	private FixedLengthStringData filler20 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 59, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 60);
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 64, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 65);
	private FixedLengthStringData filler22 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 70);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(74);
	private FixedLengthStringData filler23 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 5);
	private FixedLengthStringData filler24 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 9, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 10);
	private FixedLengthStringData filler25 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 15);
	private FixedLengthStringData filler26 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 20);
	private FixedLengthStringData filler27 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 25);
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 30);
	private FixedLengthStringData filler29 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 35);
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 40);
	private FixedLengthStringData filler31 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 45);
	private FixedLengthStringData filler32 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 50);
	private FixedLengthStringData filler33 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 54, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 55);
	private FixedLengthStringData filler34 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 59, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 60);
	private FixedLengthStringData filler35 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 64, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 65);
	private FixedLengthStringData filler36 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 70);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(21);
	private FixedLengthStringData filler37 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler38 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine006, 5, FILLER).init("Contract Status:");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(51);
	private FixedLengthStringData filler39 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 5);
	private FixedLengthStringData filler40 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 7, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 9);
	private FixedLengthStringData filler41 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 11, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 13);
	private FixedLengthStringData filler42 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 15, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 17);
	private FixedLengthStringData filler43 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 21);
	private FixedLengthStringData filler44 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 25);
	private FixedLengthStringData filler45 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 27, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 29);
	private FixedLengthStringData filler46 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 33);
	private FixedLengthStringData filler47 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 37);
	private FixedLengthStringData filler48 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 41);
	private FixedLengthStringData filler49 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 45);
	private FixedLengthStringData filler50 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 49);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(21);
	private FixedLengthStringData filler51 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler52 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine008, 5, FILLER).init("Follow-up Codes:");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(79);
	private FixedLengthStringData filler53 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 5);
	private FixedLengthStringData filler54 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 9, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 10);
	private FixedLengthStringData filler55 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 15);
	private FixedLengthStringData filler56 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 20);
	private FixedLengthStringData filler57 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 25);
	private FixedLengthStringData filler58 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 30);
	private FixedLengthStringData filler59 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 35);
	private FixedLengthStringData filler60 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 40);
	private FixedLengthStringData filler61 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 45);
	private FixedLengthStringData filler62 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo054 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 50);
	private FixedLengthStringData filler63 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 54, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 55);
	private FixedLengthStringData filler64 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 59, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 60);
	private FixedLengthStringData filler65 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 64, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 65);
	private FixedLengthStringData filler66 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 70);
	private FixedLengthStringData filler67 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 74, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo059 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 75);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(79);
	private FixedLengthStringData filler68 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo060 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 5);
	private FixedLengthStringData filler69 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 9, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo061 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 10);
	private FixedLengthStringData filler70 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo062 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 15);
	private FixedLengthStringData filler71 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo063 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 20);
	private FixedLengthStringData filler72 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo064 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 25);
	private FixedLengthStringData filler73 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo065 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 30);
	private FixedLengthStringData filler74 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo066 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 35);
	private FixedLengthStringData filler75 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo067 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 40);
	private FixedLengthStringData filler76 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo068 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 45);
	private FixedLengthStringData filler77 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo069 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 50);
	private FixedLengthStringData filler78 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 54, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo070 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 55);
	private FixedLengthStringData filler79 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 59, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo071 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 60);
	private FixedLengthStringData filler80 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 64, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo072 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 65);
	private FixedLengthStringData filler81 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo073 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 70);
	private FixedLengthStringData filler82 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 74, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo074 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 75);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(79);
	private FixedLengthStringData filler83 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo075 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 5);
	private FixedLengthStringData filler84 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 9, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo076 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 10);
	private FixedLengthStringData filler85 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo077 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 15);
	private FixedLengthStringData filler86 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo078 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 20);
	private FixedLengthStringData filler87 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo079 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 25);
	private FixedLengthStringData filler88 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo080 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 30);
	private FixedLengthStringData filler89 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo081 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 35);
	private FixedLengthStringData filler90 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo082 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 40);
	private FixedLengthStringData filler91 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo083 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 45);
	private FixedLengthStringData filler92 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo084 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 50);
	private FixedLengthStringData filler93 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 54, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo085 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 55);
	private FixedLengthStringData filler94 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 59, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo086 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 60);
	private FixedLengthStringData filler95 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 64, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo087 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 65);
	private FixedLengthStringData filler96 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo088 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 70);
	private FixedLengthStringData filler97 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 74, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo089 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 75);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(79);
	private FixedLengthStringData filler98 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo090 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 5);
	private FixedLengthStringData filler99 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 9, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo091 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 10);
	private FixedLengthStringData filler100 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 14, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo092 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 15);
	private FixedLengthStringData filler101 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 19, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo093 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 20);
	private FixedLengthStringData filler102 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo094 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 25);
	private FixedLengthStringData filler103 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo095 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 30);
	private FixedLengthStringData filler104 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo096 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 35);
	private FixedLengthStringData filler105 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo097 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 40);
	private FixedLengthStringData filler106 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo098 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 45);
	private FixedLengthStringData filler107 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo099 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 50);
	private FixedLengthStringData filler108 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 54, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo100 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 55);
	private FixedLengthStringData filler109 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 59, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo101 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 60);
	private FixedLengthStringData filler110 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 64, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo102 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 65);
	private FixedLengthStringData filler111 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo103 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 70);
	private FixedLengthStringData filler112 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 74, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo104 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 75);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(59);
	private FixedLengthStringData filler113 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler114 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine013, 27, FILLER).init("Duration evaluation required:");
	private FixedLengthStringData fieldNo105 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 58);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Th596rec th596rec = new Th596rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Th596pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		th596rec.th596Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(th596rec.trcode01);
		fieldNo006.set(th596rec.trcode02);
		fieldNo007.set(th596rec.trcode03);
		fieldNo008.set(th596rec.trcode04);
		fieldNo009.set(th596rec.trcode05);
		fieldNo010.set(th596rec.trcode06);
		fieldNo011.set(th596rec.trcode07);
		fieldNo012.set(th596rec.trcode08);
		fieldNo013.set(th596rec.trcode09);
		fieldNo014.set(th596rec.trcode10);
		fieldNo015.set(th596rec.trcode11);
		fieldNo016.set(th596rec.trcode12);
		fieldNo017.set(th596rec.trcode13);
		fieldNo018.set(th596rec.trcode14);
		fieldNo019.set(th596rec.trcode15);
		fieldNo020.set(th596rec.trcode16);
		fieldNo021.set(th596rec.trcode17);
		fieldNo022.set(th596rec.trcode18);
		fieldNo023.set(th596rec.trcode19);
		fieldNo024.set(th596rec.trcode20);
		fieldNo025.set(th596rec.trcode21);
		fieldNo026.set(th596rec.trcode22);
		fieldNo027.set(th596rec.trcode23);
		fieldNo028.set(th596rec.trcode24);
		fieldNo029.set(th596rec.trcode25);
		fieldNo030.set(th596rec.trcode26);
		fieldNo031.set(th596rec.trcode27);
		fieldNo032.set(th596rec.trcode28);
		fieldNo033.set(th596rec.cnRiskStat01);
		fieldNo034.set(th596rec.cnRiskStat02);
		fieldNo035.set(th596rec.cnRiskStat03);
		fieldNo036.set(th596rec.cnRiskStat04);
		fieldNo037.set(th596rec.cnRiskStat05);
		fieldNo038.set(th596rec.cnRiskStat06);
		fieldNo039.set(th596rec.cnRiskStat07);
		fieldNo040.set(th596rec.cnRiskStat08);
		fieldNo041.set(th596rec.cnRiskStat09);
		fieldNo042.set(th596rec.cnRiskStat10);
		fieldNo043.set(th596rec.cnRiskStat11);
		fieldNo044.set(th596rec.cnRiskStat12);
		fieldNo045.set(th596rec.fupcdes01);
		fieldNo046.set(th596rec.fupcdes02);
		fieldNo047.set(th596rec.fupcdes03);
		fieldNo048.set(th596rec.fupcdes04);
		fieldNo049.set(th596rec.fupcdes05);
		fieldNo050.set(th596rec.fupcdes06);
		fieldNo051.set(th596rec.fupcdes07);
		fieldNo052.set(th596rec.fupcdes08);
		fieldNo053.set(th596rec.fupcdes09);
		fieldNo054.set(th596rec.fupcdes10);
		fieldNo055.set(th596rec.fupcdes11);
		fieldNo056.set(th596rec.fupcdes12);
		fieldNo057.set(th596rec.fupcdes13);
		fieldNo058.set(th596rec.fupcdes14);
		fieldNo059.set(th596rec.fupcdes15);
		fieldNo060.set(th596rec.fupcdes16);
		fieldNo061.set(th596rec.fupcdes17);
		fieldNo062.set(th596rec.fupcdes18);
		fieldNo063.set(th596rec.fupcdes19);
		fieldNo064.set(th596rec.fupcdes20);
		fieldNo065.set(th596rec.fupcdes21);
		fieldNo066.set(th596rec.fupcdes22);
		fieldNo067.set(th596rec.fupcdes23);
		fieldNo068.set(th596rec.fupcdes24);
		fieldNo069.set(th596rec.fupcdes25);
		fieldNo070.set(th596rec.fupcdes26);
		fieldNo071.set(th596rec.fupcdes27);
		fieldNo072.set(th596rec.fupcdes28);
		fieldNo073.set(th596rec.fupcdes29);
		fieldNo074.set(th596rec.fupcdes30);
		fieldNo075.set(th596rec.fupcdes31);
		fieldNo076.set(th596rec.fupcdes32);
		fieldNo077.set(th596rec.fupcdes33);
		fieldNo078.set(th596rec.fupcdes34);
		fieldNo079.set(th596rec.fupcdes35);
		fieldNo080.set(th596rec.fupcdes36);
		fieldNo081.set(th596rec.fupcdes37);
		fieldNo082.set(th596rec.fupcdes38);
		fieldNo083.set(th596rec.fupcdes39);
		fieldNo084.set(th596rec.fupcdes40);
		fieldNo085.set(th596rec.fupcdes41);
		fieldNo086.set(th596rec.fupcdes42);
		fieldNo087.set(th596rec.fupcdes43);
		fieldNo088.set(th596rec.fupcdes44);
		fieldNo089.set(th596rec.fupcdes45);
		fieldNo090.set(th596rec.fupcdes46);
		fieldNo091.set(th596rec.fupcdes47);
		fieldNo092.set(th596rec.fupcdes48);
		fieldNo093.set(th596rec.fupcdes49);
		fieldNo094.set(th596rec.fupcdes50);
		fieldNo095.set(th596rec.fupcdes51);
		fieldNo096.set(th596rec.fupcdes52);
		fieldNo097.set(th596rec.fupcdes53);
		fieldNo098.set(th596rec.fupcdes54);
		fieldNo099.set(th596rec.fupcdes55);
		fieldNo100.set(th596rec.fupcdes56);
		fieldNo101.set(th596rec.fupcdes57);
		fieldNo102.set(th596rec.fupcdes58);
		fieldNo103.set(th596rec.fupcdes59);
		fieldNo104.set(th596rec.fupcdes60);
		fieldNo105.set(th596rec.hcondate);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
