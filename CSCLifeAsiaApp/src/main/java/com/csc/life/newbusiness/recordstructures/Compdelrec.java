package com.csc.life.newbusiness.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:17
 * Description:
 * Copybook name: COMPDELREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Compdelrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData compdelRec = new FixedLengthStringData(19);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(compdelRec, 0);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(compdelRec, 4);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(compdelRec, 5);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(compdelRec, 13);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(compdelRec, 15);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(compdelRec, 17);


	public void initialize() {
		COBOLFunctions.initialize(compdelRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		compdelRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}