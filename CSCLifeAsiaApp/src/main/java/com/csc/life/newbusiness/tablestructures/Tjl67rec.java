package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class Tjl67rec extends ExternalData{
	
	public FixedLengthStringData tjl67Rec = new FixedLengthStringData(18);
	public FixedLengthStringData resultcds = new FixedLengthStringData(9).isAPartOf(tjl67Rec, 0);
	public FixedLengthStringData[] resultcd = FLSArrayPartOfStructure(9, 1, resultcds, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(9).isAPartOf(resultcds, 0, FILLER_REDEFINE);
	public FixedLengthStringData resultcd01 = new FixedLengthStringData(1).isAPartOf(filler1, 0);
	public FixedLengthStringData resultcd02 = new FixedLengthStringData(1).isAPartOf(filler1, 1);
	public FixedLengthStringData resultcd03 = new FixedLengthStringData(1).isAPartOf(filler1, 2);
	public FixedLengthStringData resultcd04 = new FixedLengthStringData(1).isAPartOf(filler1, 3);
	public FixedLengthStringData resultcd05 = new FixedLengthStringData(1).isAPartOf(filler1, 4);
	public FixedLengthStringData resultcd06 = new FixedLengthStringData(1).isAPartOf(filler1, 5);
	public FixedLengthStringData resultcd07 = new FixedLengthStringData(1).isAPartOf(filler1, 6);
	public FixedLengthStringData resultcd08 = new FixedLengthStringData(1).isAPartOf(filler1, 7);
	public FixedLengthStringData resultcd09 = new FixedLengthStringData(1).isAPartOf(filler1, 8);
	public FixedLengthStringData rcverors = new FixedLengthStringData(9).isAPartOf(tjl67Rec, 9);
	public FixedLengthStringData[] rcveror = FLSArrayPartOfStructure(9, 1, rcverors, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(9).isAPartOf(rcverors, 0, FILLER_REDEFINE);
	public FixedLengthStringData rcveror01 = new FixedLengthStringData(1).isAPartOf(filler, 0);
	public FixedLengthStringData rcveror02 = new FixedLengthStringData(1).isAPartOf(filler, 1);
	public FixedLengthStringData rcveror03 = new FixedLengthStringData(1).isAPartOf(filler, 2);
	public FixedLengthStringData rcveror04 = new FixedLengthStringData(1).isAPartOf(filler, 3);
	public FixedLengthStringData rcveror05 = new FixedLengthStringData(1).isAPartOf(filler, 4);
	public FixedLengthStringData rcveror06 = new FixedLengthStringData(1).isAPartOf(filler, 5);
	public FixedLengthStringData rcveror07 = new FixedLengthStringData(1).isAPartOf(filler, 6);
	public FixedLengthStringData rcveror08 = new FixedLengthStringData(1).isAPartOf(filler, 7);
	public FixedLengthStringData rcveror09 = new FixedLengthStringData(1).isAPartOf(filler, 8);

	public void initialize() {
		COBOLFunctions.initialize(tjl67Rec);
	}

	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());
			tjl67Rec.isAPartOf(baseString, true);
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}

}
