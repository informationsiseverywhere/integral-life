/*
 * File: Pt504.java
 * Date: 30 August 2009 2:00:07
 * Author: Quipoz Limited
 * 
 * Class transformed from PT504.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.procedures.Tt504pt;
import com.csc.life.newbusiness.screens.St504ScreenVars;
import com.csc.life.newbusiness.tablestructures.Tt504rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
* FACTORS OF PAYER BENEFIT - TERM BASED
*
*
*****************************************************************
* </pre>
*/
public class Pt504 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PT504");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSub2 = new PackedDecimalData(3, 0);
	private DescTableDAM descIO = new DescTableDAM();
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private Tt504rec tt504rec = new Tt504rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private St504ScreenVars sv = ScreenProgram.getScreenVars( St504ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		other3080, 
		exit3090
	}

	public Pt504() {
		super();
		screenVars = sv;
		new ScreenModel("St504", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				case generalArea1045: 
					generalArea1045();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setParams(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		tt504rec.tt504Rec.set(itmdIO.getItemGenarea());
		/*    IF THE EXTRA DATA AREA WAS NOT USED BEFORE THE NUMERIC*/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.*/
		if (isNE(itmdIO.getItemGenarea(), SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		wsaaSub1.set(0);
		for (int loopVar1 = 0; !(loopVar1 == 99); loopVar1 += 1){
			initLfacts1500();
		}
		wsaaSub2.set(0);
		for (int loopVar2 = 0; !(loopVar2 == 11); loopVar2 += 1){
			initModfacs1700();
		}
		/*    MOVE ZERO                                                    */
		/*      TO TT504-MODFAC                   .                        */
		tt504rec.lfactor.set(ZERO);
		tt504rec.riskunit.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.disccntmeth.set(tt504rec.disccntmeth);
		if (isEQ(itmdIO.getItemItmfrm(), 0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(), 0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		wsaaSub1.set(0);
		for (int loopVar3 = 0; !(loopVar3 == 99); loopVar3 += 1){
			moveLfacts1600();
		}
		wsaaSub2.set(0);
		for (int loopVar4 = 0; !(loopVar4 == 11); loopVar4 += 1){
			moveModfacs1800();
		}
		/*    MOVE TT504-MODFAC                                            */
		/*      TO ST504-MODFAC                  .                         */
		sv.lfactor.set(tt504rec.lfactor);
		sv.riskunit.set(tt504rec.riskunit);
	}

protected void initLfacts1500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		tt504rec.lfact[wsaaSub1.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void moveLfacts1600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		sv.lfact[wsaaSub1.toInt()].set(tt504rec.lfact[wsaaSub1.toInt()]);
		/*EXIT*/
	}

protected void initModfacs1700()
	{
		/*PARA*/
		wsaaSub2.add(1);
		tt504rec.modfac[wsaaSub2.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void moveModfacs1800()
	{
		/*PARA*/
		wsaaSub2.add(1);
		if (isEQ(tt504rec.modfac[wsaaSub2.toInt()], NUMERIC)) {
			sv.modfac[wsaaSub2.toInt()].set(tt504rec.modfac[wsaaSub2.toInt()]);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		screenIo2010();
		exit2090();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080: 
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag, "C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(tt504rec.tt504Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.disccntmeth, tt504rec.disccntmeth)) {
			tt504rec.disccntmeth.set(sv.disccntmeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm, itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto, itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		wsaaSub1.set(0);
		for (int loopVar5 = 0; !(loopVar5 == 99); loopVar5 += 1){
			updateLfacts3500();
		}
		wsaaSub2.set(0);
		for (int loopVar6 = 0; !(loopVar6 == 11); loopVar6 += 1){
			updateModfacs3600();
		}
		/*    IF ST504-MODFAC                  NOT =                       */
		/*       TT504-MODFAC                                              */
		/*        MOVE ST504-MODFAC                                        */
		/*          TO TT504-MODFAC                                        */
		/*        MOVE 'Y' TO WSAA-UPDATE-FLAG.                            */
		if (isNE(sv.lfactor, tt504rec.lfactor)) {
			tt504rec.lfactor.set(sv.lfactor);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.riskunit, tt504rec.riskunit)) {
			tt504rec.riskunit.set(sv.riskunit);
			wsaaUpdateFlag = "Y";
		}
	}

protected void updateLfacts3500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.lfact[wsaaSub1.toInt()], tt504rec.lfact[wsaaSub1.toInt()])) {
			tt504rec.lfact[wsaaSub1.toInt()].set(sv.lfact[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*EXIT*/
	}

protected void updateModfacs3600()
	{
		/*PARA*/
		wsaaSub2.add(1);
		if (isNE(sv.modfac[wsaaSub2.toInt()], tt504rec.modfac[wsaaSub2.toInt()])) {
			tt504rec.modfac[wsaaSub2.toInt()].set(sv.modfac[wsaaSub2.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DUMMY CALL TO GENERATED PRINT PROGRAM TO ENSURE THAT
	*      IT IS TRANSFERED, TA/TR, ALONG WITH REST OF SUITE.
	* </pre>
	*/
protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Tt504pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
