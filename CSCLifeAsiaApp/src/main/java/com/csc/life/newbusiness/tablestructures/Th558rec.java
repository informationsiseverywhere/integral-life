package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:24
 * Description:
 * Copybook name: TH558REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th558rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th558Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData adsc = new FixedLengthStringData(50).isAPartOf(th558Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(450).isAPartOf(th558Rec, 50, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th558Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th558Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}