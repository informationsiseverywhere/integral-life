package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:40
 * Description:
 * Copybook name: FLUPLNBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Fluplnbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData fluplnbFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData fluplnbKey = new FixedLengthStringData(256).isAPartOf(fluplnbFileKey, 0, REDEFINE);
  	public FixedLengthStringData fluplnbChdrcoy = new FixedLengthStringData(1).isAPartOf(fluplnbKey, 0);
  	public FixedLengthStringData fluplnbChdrnum = new FixedLengthStringData(8).isAPartOf(fluplnbKey, 1);
  	public PackedDecimalData fluplnbFupno = new PackedDecimalData(2, 0).isAPartOf(fluplnbKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(245).isAPartOf(fluplnbKey, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(fluplnbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		fluplnbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}