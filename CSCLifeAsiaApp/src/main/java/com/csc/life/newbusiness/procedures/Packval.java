/*
 * File: Packval.java
 * Date: December 3, 2013 3:16:20 AM ICT
 * Author: CSC
 * 
 * Class transformed from PACKVAL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Getfsu;
import com.csc.fsu.general.recordstructures.Getfsurec;
import com.csc.fsu.general.recordstructures.Hvalletrec;
import com.csc.fsu.general.tablestructures.Tr271rec;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*         PACKVAL - Policy Acknowledgement Validateion
*
*   This letter validation routine is created to check whether
*   acknowledgement letters should be generated or not.
*
****************************************************************** ****
* </pre>
*/
public class Packval extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "PACKVAL";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private static final String hpadrec = "HPADREC";
	private static final String itemrec = "ITEMREC";
		/* TABLES */
	private static final String tr271 = "TR271";
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Tr271rec tr271rec = new Tr271rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Getfsurec getfsurec = new Getfsurec();
	private Hvalletrec hvalletrec = new Hvalletrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		errorProg9020
	}

	public Packval() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		hvalletrec.params = convertAndSetParam(hvalletrec.params, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		/*BEGIN*/
		hvalletrec.hvalStatuz.set(varcom.oK);
		/*  Get the policy acknowledgement details.*/
		readHpad1000();
		if (isEQ(hpadIO.getDlvrmode(), SPACES)) {
			return ;
		}
		/*  Get the FSU company.*/
		getFsuCoy2000();
		/*  Check whether letter is to be suppressed for the delivery mode*/
		/*  or not.*/
		readTr2713000();
		if (isEQ(tr271rec.suprflg, "Y")) {
			hvalletrec.hvalStatuz.set(varcom.mrnf);
		}
		/*EXIT*/
		exitProgram();
	}

protected void readHpad1000()
	{
		/*BEGIN*/
		hpadIO.setParams(SPACES);
		hpadIO.setChdrcoy(hvalletrec.hvalChdrcoy);
		hpadIO.setChdrnum(hvalletrec.hvalChdrnum);
		hpadIO.setFormat(hpadrec);
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(hpadIO.getStatuz());
			syserrrec.params.set(hpadIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void getFsuCoy2000()
	{
		/*BEGIN*/
		getfsurec.company.set(hvalletrec.hvalChdrcoy);
		getfsurec.function.set("GETFSUCO");
		callProgram(Getfsu.class, getfsurec.getfsuRec);
		if (isNE(getfsurec.statuz, varcom.oK)) {
			syserrrec.statuz.set(getfsurec.statuz);
			syserrrec.params.set(getfsurec.getfsuRec);
			fatalError9000();
		}
		/*EXIT*/
	}

protected void readTr2713000()
	{
		begin3010();
	}

protected void begin3010()
	{
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(getfsurec.fsuco);
		itemIO.setItemtabl(tr271);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemitem(hpadIO.getDlvrmode());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		tr271rec.tr271Rec.set(itemIO.getGenarea());
	}

protected void fatalError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					fatalError9010();
				case errorProg9020: 
					errorProg9020();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError9010()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			goTo(GotoLabel.errorProg9020);
		}
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg9020()
	{
		hvalletrec.hvalStatuz.set(varcom.bomb);
		exitProgram();
	}

protected void exit9090()
	{
		exitProgram();
	}
}
