package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:50
 * Description:
 * Copybook name: RESNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Resnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData resnFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData resnKey = new FixedLengthStringData(256).isAPartOf(resnFileKey, 0, REDEFINE);
  	public FixedLengthStringData resnChdrcoy = new FixedLengthStringData(1).isAPartOf(resnKey, 0);
  	public FixedLengthStringData resnChdrnum = new FixedLengthStringData(8).isAPartOf(resnKey, 1);
  	public PackedDecimalData resnTranno = new PackedDecimalData(5, 0).isAPartOf(resnKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(244).isAPartOf(resnKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(resnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		resnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}