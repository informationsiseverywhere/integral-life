/*
 * File: Ct501.java
 * Date: 30 August 2009 2:59:23
 * Author: $Id$
 * 
 * Class transformed from CT501.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.cls;

import com.csc.life.newbusiness.batchprograms.Bt501;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.printing.PrintManager;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class Ct501 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData statuz = new FixedLengthStringData(4);
	private FixedLengthStringData bsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData bsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData bprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData buparec = new FixedLengthStringData(1024);

	public Ct501() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		buparec = convertAndSetParam(buparec, parmArray, 4);
		bprdrec = convertAndSetParam(bprdrec, parmArray, 3);
		bsprrec = convertAndSetParam(bsprrec, parmArray, 2);
		bsscrec = convertAndSetParam(bsscrec, parmArray, 1);
		statuz = convertAndSetParam(statuz, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			try {
				switch (qState) {
				case QS_START: {
					new PrintManager().overridePrinterFile("RT501", "RT501", new String[] {"LENGTH", "WIDTH", "LPI", "CPI", "OVRFLW"}, new Object[] {"66", "198", "6", "15", "60"});
					callProgram(Bt501.class, new Object[] {statuz, bsscrec, bsprrec, bprdrec, buparec});
					appVars.deleteOverride("RT501", ROUTINE);
				}
				case returnVar: {
					return ;
				}
				case error: {
					appVars.sendMessageToQueue("Unexpected errors occurred", "*");
					statuz.set("BOMB");
					qState = returnVar;
					break;
				}
				default:{
					qState = QS_END;
				}
				}
			}
			catch (ExtMsgException ex){
				if (ex.messageMatches("CPF0000")
				|| ex.messageMatches("LBE0000")) {
					qState = error;
				}
				else {
					throw ex;
				}
			}
		}
		
	}
}
