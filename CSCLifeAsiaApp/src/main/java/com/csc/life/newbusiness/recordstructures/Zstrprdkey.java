package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:26
 * Description:
 * Copybook name: ZSTRPRDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zstrprdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zstrprdFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData zstrprdKey = new FixedLengthStringData(64).isAPartOf(zstrprdFileKey, 0, REDEFINE);
  	public FixedLengthStringData zstrprdChdrcoy = new FixedLengthStringData(1).isAPartOf(zstrprdKey, 0);
  	public FixedLengthStringData zstrprdCntbranch = new FixedLengthStringData(2).isAPartOf(zstrprdKey, 1);
  	public FixedLengthStringData zstrprdCnttype = new FixedLengthStringData(3).isAPartOf(zstrprdKey, 3);
  	public PackedDecimalData zstrprdEffdate = new PackedDecimalData(8, 0).isAPartOf(zstrprdKey, 6);
  	public FixedLengthStringData zstrprdBatctrcde = new FixedLengthStringData(4).isAPartOf(zstrprdKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(49).isAPartOf(zstrprdKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zstrprdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zstrprdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}