package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr589screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {17, 4, 22, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21, 20}; 
	public static int maxRecords = 12;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {39, 38}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {11, 15, 2, 74}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr589ScreenVars sv = (Sr589ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr589screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr589screensfl, 
			sv.Sr589screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr589ScreenVars sv = (Sr589ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr589screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr589ScreenVars sv = (Sr589ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr589screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr589screensflWritten.gt(0))
		{
			sv.sr589screensfl.setCurrentIndex(0);
			sv.Sr589screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr589ScreenVars sv = (Sr589ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr589screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr589ScreenVars screenVars = (Sr589ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.fupno.setFieldName("fupno");
				screenVars.transactionDate.setFieldName("transactionDate");
				screenVars.uprflag.setFieldName("uprflag");
				screenVars.crtuser.setFieldName("crtuser");
				screenVars.fuptype.setFieldName("fuptype");
				screenVars.fupcode.setFieldName("fupcode");
				screenVars.lifeno.setFieldName("lifeno");
				screenVars.jlife.setFieldName("jlife");
				screenVars.fupstat.setFieldName("fupstat");
				screenVars.date_var.setFieldName("date_var");
				screenVars.crtdateDisp.setFieldName("crtdateDisp");
				screenVars.tranno.setFieldName("tranno");
				screenVars.ind.setFieldName("ind");
				screenVars.select.setFieldName("select");
				screenVars.fupremk.setFieldName("fupremk");
				screenVars.fuprcvdDisp.setFieldName("fuprcvdDisp");
				screenVars.exprdateDisp.setFieldName("exprdateDisp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.fupno.set(dm.getField("fupno"));
			screenVars.transactionDate.set(dm.getField("transactionDate"));
			screenVars.uprflag.set(dm.getField("uprflag"));
			screenVars.crtuser.set(dm.getField("crtuser"));
			screenVars.fuptype.set(dm.getField("fuptype"));
			screenVars.fupcode.set(dm.getField("fupcode"));
			screenVars.lifeno.set(dm.getField("lifeno"));
			screenVars.jlife.set(dm.getField("jlife"));
			screenVars.fupstat.set(dm.getField("fupstat"));
			screenVars.date_var.set(dm.getField("date_var"));
			screenVars.crtdateDisp.set(dm.getField("crtdateDisp"));
			screenVars.tranno.set(dm.getField("tranno"));
			screenVars.ind.set(dm.getField("ind"));
			screenVars.select.set(dm.getField("select"));
			screenVars.fupremk.set(dm.getField("fupremk"));
			screenVars.fuprcvdDisp.set(dm.getField("fuprcvdDisp"));
			screenVars.exprdateDisp.set(dm.getField("exprdateDisp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr589ScreenVars screenVars = (Sr589ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.fupno.setFieldName("fupno");
				screenVars.transactionDate.setFieldName("transactionDate");
				screenVars.uprflag.setFieldName("uprflag");
				screenVars.crtuser.setFieldName("crtuser");
				screenVars.fuptype.setFieldName("fuptype");
				screenVars.fupcode.setFieldName("fupcode");
				screenVars.lifeno.setFieldName("lifeno");
				screenVars.jlife.setFieldName("jlife");
				screenVars.fupstat.setFieldName("fupstat");
				screenVars.date_var.setFieldName("date_var");
				screenVars.crtdateDisp.setFieldName("crtdateDisp");
				screenVars.tranno.setFieldName("tranno");
				screenVars.ind.setFieldName("ind");
				screenVars.select.setFieldName("select");
				screenVars.fupremk.setFieldName("fupremk");
				screenVars.fuprcvdDisp.setFieldName("fuprcvdDisp");
				screenVars.exprdateDisp.setFieldName("exprdateDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("fupno").set(screenVars.fupno);
			dm.getField("transactionDate").set(screenVars.transactionDate);
			dm.getField("uprflag").set(screenVars.uprflag);
			dm.getField("crtuser").set(screenVars.crtuser);
			dm.getField("fuptype").set(screenVars.fuptype);
			dm.getField("fupcode").set(screenVars.fupcode);
			dm.getField("lifeno").set(screenVars.lifeno);
			dm.getField("jlife").set(screenVars.jlife);
			dm.getField("fupstat").set(screenVars.fupstat);
			dm.getField("date_var").set(screenVars.date_var);
			dm.getField("crtdateDisp").set(screenVars.crtdateDisp);
			dm.getField("tranno").set(screenVars.tranno);
			dm.getField("ind").set(screenVars.ind);
			dm.getField("select").set(screenVars.select);
			dm.getField("fupremk").set(screenVars.fupremk);
			dm.getField("fuprcvdDisp").set(screenVars.fuprcvdDisp);
			dm.getField("exprdateDisp").set(screenVars.exprdateDisp);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr589screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr589ScreenVars screenVars = (Sr589ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.fupno.clearFormatting();
		screenVars.transactionDate.clearFormatting();
		screenVars.uprflag.clearFormatting();
		screenVars.crtuser.clearFormatting();
		screenVars.fuptype.clearFormatting();
		screenVars.fupcode.clearFormatting();
		screenVars.lifeno.clearFormatting();
		screenVars.jlife.clearFormatting();
		screenVars.fupstat.clearFormatting();
		screenVars.date_var.clearFormatting();
		screenVars.crtdateDisp.clearFormatting();
		screenVars.tranno.clearFormatting();
		screenVars.ind.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.fupremk.clearFormatting();
		screenVars.fuprcvdDisp.clearFormatting();
		screenVars.exprdateDisp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr589ScreenVars screenVars = (Sr589ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.fupno.setClassString("");
		screenVars.transactionDate.setClassString("");
		screenVars.uprflag.setClassString("");
		screenVars.crtuser.setClassString("");
		screenVars.fuptype.setClassString("");
		screenVars.fupcode.setClassString("");
		screenVars.lifeno.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.fupstat.setClassString("");
		screenVars.date_var.setClassString("");
		screenVars.crtdateDisp.setClassString("");
		screenVars.tranno.setClassString("");
		screenVars.ind.setClassString("");
		screenVars.select.setClassString("");
		screenVars.fupremk.setClassString("");
		screenVars.fuprcvdDisp.setClassString("");
		screenVars.exprdateDisp.setClassString("");
	}

/**
 * Clear all the variables in Sr589screensfl
 */
	public static void clear(VarModel pv) {
		Sr589ScreenVars screenVars = (Sr589ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.fupno.clear();
		screenVars.transactionDate.clear();
		screenVars.uprflag.clear();
		screenVars.crtuser.clear();
		screenVars.fuptype.clear();
		screenVars.fupcode.clear();
		screenVars.lifeno.clear();
		screenVars.jlife.clear();
		screenVars.fupstat.clear();
		screenVars.date_var.clear();
		screenVars.crtdateDisp.clear();
		screenVars.crtdate.clear();
		screenVars.tranno.clear();
		screenVars.ind.clear();
		screenVars.select.clear();
		screenVars.fupremk.clear();
		screenVars.fuprcvdDisp.clear();
		screenVars.fuprcvd.clear();
		screenVars.exprdateDisp.clear();
		screenVars.exprdate.clear();
	}
}
