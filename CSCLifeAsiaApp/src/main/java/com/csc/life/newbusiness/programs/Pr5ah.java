package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import com.csc.smart.recordstructures.Optswchrec;
import java.util.LinkedList;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.newbusiness.screens.Sr5ahScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.impl.DescDAOImpl;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;

public class Pr5ah extends ScreenProgCS{
	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR5AH");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private Sr5ahScreenVars sv = ScreenProgram.getScreenVars( Sr5ahScreenVars.class);
	private Wssplife wssplife = new Wssplife();
	private FixedLengthStringData wsaaEntryFound = new FixedLengthStringData(1);
	private DescDAO descdao =new DescDAOImpl();
	Descpf desc=null;
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private Optswchrec optswchrec = new Optswchrec();
	LinkedList<Exclpf> exclpflist = null;
	private ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class);
	private PackedDecimalData wsaaCount = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaFoundSelection = new FixedLengthStringData(1).init("N");
	private Validator foundSelection = new Validator(wsaaFoundSelection, "Y");
	private FixedLengthStringData wsaaOptionCode = new FixedLengthStringData(1);
	private Covrpf covr;
	private Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	protected CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private Chdrpf chdrpf = new Chdrpf();
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		updateErrorIndicators2670, 
		lineSelection4020, 
		stckOptswch4030, 
		next4100, 
		exit4100
	}
	public Pr5ah() {
		super();
		screenVars = sv;
		new ScreenModel("Sr5ah", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
protected void initialise1000()
{
	initialise1010();
}
protected void initialise1010()
{
	if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
		return ;
	}
	if(isEQ(wsspcomn.flag,"I"))
	{
		sv.isEnquiryMode.set("1");
	}
	else
	{
		sv.isEnquiryMode.set("0");
	}
	covrpf = new Covrpf();
	covrpf = covrpfDAO.getCacheObject(covrpf);
	if (null == covrpf) {
		covtlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		} else {
			covrpf = new Covrpf();
			covrpf.setLife(covtlnbIO.getLife().toString());
			covrpf.setCoverage(covtlnbIO.getCoverage().toString());
			covrpf.setRider(covtlnbIO.getRider().toString());
			}
	}
	sv.dataArea.set(SPACES);
	sv.subfileArea.set(SPACES);
	sv.seqnbr.set(ZERO);
	wsaaEntryFound.set("N");
	scrnparams.function.set(varcom.sclr);
	screenIo9000();
	sv.chdrnum.set(wsspcomn.chdrChdrnum);
	sv.crtable.set(wsspcomn.crtable.toString());
	desc=descdao.getdescData("IT", "T5687", sv.crtable.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
	if (desc==null) {
		fatalError600();
		}
	else
		sv.crtabdesc.set(desc.getLongdesc());
	scrnparams.subfileRrn.set(1);
	initOptswch1100();
	loadSubfile1200();
	if(isEQ(wsspcomn.cmode.trim(),"IFE")){
		sv.optdsc01Out[varcom.pr.toInt()].set("Y");
		sv.optdsc02Out[varcom.pr.toInt()].set("Y");
		sv.optdsc03Out[varcom.pr.toInt()].set("Y");
		}
}
protected void loadSubfile1200()
{
	begin1200();
}

protected void begin1200()
{
	exclpflist = exclpfDAO.getExclpfRecord(sv.chdrnum.toString(),sv.crtable.toString(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider());
	if(exclpflist.size() > 0 ){
		sv.noRecord.set("N");
	}else{
		sv.noRecord.set("Y");
	}
	wsaaCount.set(1);
	populateSubfile(exclpflist, sv.subfilePage.toInt());
	if (exclpflist != null && exclpflist.size() == 0) {
		scrnparams.subfileMore.set("N");
	}
	else {
		scrnparams.subfileMore.set("Y");
	}
	if (isEQ(wsaaCount, 1)
			&& exclpflist != null && exclpflist.size() == 0) {
		
		blankScreen2100();
	}
}
protected void blankScreen2100()
{
	sv.subfileArea.set(SPACES);
	sv.noRecord.set("Y");
	sv.select.set(SPACES);
	sv.excda.set(SPACES);
	sv.longdesc.set(SPACES);
	sv.prntstat.set(SPACES);
	sv.uniqueNum.set(SPACES);
	scrnparams.function.set(varcom.sadd);
	screenIo9000();
}
private void populateSubfile(LinkedList<Exclpf> exclpflist, int subFileSize){

	int recordCount = 0;
	while(exclpflist.size() > 0 && recordCount < subFileSize){
		Exclpf exclpfItem = exclpflist.removeFirst();
		sv.select.set(ZERO);
		sv.excda.set(exclpfItem.getExcda());
		Descpf desc=null;
		desc=descdao.getdescData("IT", "TR5AG", sv.excda.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if(desc!=null)
		sv.longdesc.set(desc.getLongdesc());
		sv.prntstat.set(exclpfItem.getPrntstat());
		sv.uniqueNum.set(exclpfItem.getUnique_number());
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
		wsaaCount.add(1);
		recordCount++;
	}
}
protected void screenIo9000()
{
	processScreen("Sr5ah", sv);
	if (isNE(scrnparams.statuz, varcom.oK)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
}
protected void initOptswch1100()
{
	begin1100();
}
protected void begin1100()
{
	optswchrec.optsFunction.set("INIT");
	optswchrec.optsCallingProg.set(wsaaProg);
	optswchrec.optsDteeff.set(ZERO);
	optswchrec.optsCompany.set(wsspcomn.company);
	optswchrec.optsLanguage.set(wsspcomn.language);
	varcom.vrcmTranid.set(wsspcomn.tranid);
	optswchrec.optsUser.set(varcom.vrcmUser);
	callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
	if (isNE(optswchrec.optsStatuz, varcom.oK)) {
		syserrrec.function.set("INIT");
		syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
		syserrrec.statuz.set(optswchrec.optsStatuz);
		syserrrec.iomod.set("OPTSWCH");
		fatalError600();
	}
	for (wsaaCount.set(1); !(isGT(wsaaCount, 20)
			|| isEQ(optswchrec.optsType[wsaaCount.toInt()], SPACES)); wsaaCount.add(1)){
		if (isEQ(optswchrec.optsType[wsaaCount.toInt()], "L")) {
			sv.optdsc[wsaaCount.toInt()].set(optswchrec.optsDsc[wsaaCount.toInt()]);
		}
	}
}
protected void preScreenEdit()
{
	try {
		preStart();
	}
	catch (GOTOException e){
	}
}

protected void preStart()
{
	if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
		wsspcomn.edterror.set(varcom.oK);
		wsspcomn.sectionno.set("3000");
		return ;
	}
	/*SCREEN-IO*/
	return ;
}
protected void screenEdit2000()
{
	screenIo2010();
	checkForErrors2050();
	/*EXIT*/
}

protected void screenIo2010()
{
	wsspcomn.edterror.set(varcom.oK);
	wsaaScrnStatuz.set(scrnparams.statuz);
	if (isEQ(scrnparams.statuz, varcom.rolu)) {
		wsaaCount.set(1);
		populateSubfile(exclpflist, sv.subfilePage.toInt());
		if (exclpflist != null && exclpflist.size() == 0) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		wsspcomn.edterror.set("Y");
		return;
	}
}


protected void checkForErrors2050()
{
	if (isNE(sv.errorIndicators, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	/*VALIDATE-SUBFILE*/
	scrnparams.function.set(varcom.sstrt);
	processScreen("Sr5ah", sv);
	if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	
	while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
		validateSubfile2600();
	}

}
protected void validateSubfile2600()
{
				validation2610();
				updateErrorIndicators2670();
				readNextModifiedRecord2680();
	
}

protected void validation2610()
{
	if (isNE(sv.select, ZERO)) {
		chckOptswch2700();
	}
}

protected void chckOptswch2700()
{
	begin2700();
}

protected void begin2700()
{
	optswchrec.optsFunction.set("CHCK");
	optswchrec.optsCallingProg.set(wsaaProg);
	optswchrec.optsDteeff.set(ZERO);
	optswchrec.optsCompany.set(wsspcomn.company);
	optswchrec.optsLanguage.set(wsspcomn.language);
	varcom.vrcmTranid.set(wsspcomn.tranid);
	optswchrec.optsUser.set(varcom.vrcmUser);
	optswchrec.optsSelOptno.set(sv.select);
	optswchrec.optsSelType.set("L");
	optswchrec.optsSelCode.set(SPACES);
	callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
	if (isNE(optswchrec.optsStatuz, varcom.oK)) {
		syserrrec.statuz.set(optswchrec.optsStatuz);
		wsspcomn.edterror.set("Y");
	}
}
protected void updateErrorIndicators2670()
{
	if (isNE(sv.errorSubfile, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	scrnparams.function.set(varcom.supd);
	processScreen("Sr5ah", sv);
	if (isNE(scrnparams.statuz, varcom.oK)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
}

protected void readNextModifiedRecord2680()
{
	scrnparams.function.set(varcom.srdn);
	processScreen("Sr5ah", sv);
	if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
}
protected void update3000()
{
	/*UPDATE-DATABASE*/
	/**  No database updates are required.*/
	/*EXIT*/
}
protected void whereNext4000()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: 
				nextProgram4010();
			case lineSelection4020: 
				lineSelection4020();
			case stckOptswch4030: 
				stckOptswch4030();
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
	
}
protected void nextProgram4010()
{
	if (isEQ(wsaaScrnStatuz, varcom.oK)) {
		goTo(GotoLabel.lineSelection4020);
	}
	optswchrec.optsSelType.set("F");
	optswchrec.optsSelCode.set(wsaaScrnStatuz);
	wsaaStatuz.set(wsaaScrnStatuz);
	wsaaScrnStatuz.set(varcom.oK);
	optswchrec.optsSelOptno.set(ZERO);
	goTo(GotoLabel.stckOptswch4030);

}

protected void lineSelection4020()
{
	/*   Subfile, read through all subfile line on the screen*/
	/*   and determine whether a line selection has been made.*/
	scrnparams.statuz.set(varcom.oK);
	scrnparams.function.set(varcom.sstrt);
	wsaaFoundSelection.set("N");
	while ( !(isEQ(scrnparams.statuz, varcom.endp)
			|| foundSelection.isTrue())) {
		lineSels4100();
	}

	if (foundSelection.isTrue()) {
		goTo(GotoLabel.stckOptswch4030);
	}
	if (isEQ(wsspcomn.flag, "C")
			|| isEQ(wsspcomn.flag, "R")) {
		scrnparams.function.set(varcom.sclr);
		screenIo9000();
		loadSubfile1200();
	}
}

protected void stckOptswch4030()
{
	optswchrec.optsFunction.set("STCK");
	callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
	if (isNE(optswchrec.optsStatuz, varcom.oK)
			&& isNE(optswchrec.optsStatuz, varcom.endp)) {
		syserrrec.function.set("STCK");
		syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
		syserrrec.statuz.set(optswchrec.optsStatuz);
		syserrrec.iomod.set("OPTSWCH");
		fatalError600();
	}
	if (isEQ(optswchrec.optsStatuz, varcom.endp)) {
		wsspcomn.nextprog.set(scrnparams.scrname);
		if (isEQ(wsaaStatuz, varcom.oK)) {
			/*scrnparams.subfileRrn.set(wssplife.subfileRrn);*/
			/*scrnparams.subfileEnd.set(wssplife.subfileEnd);*/
		}
	}
	else {
		wsspcomn.programPtr.add(1);
	}
}

protected void lineSels4100()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: 
				begin4100();
			case next4100: 
				next4100();
			case exit4100: 
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void begin4100()
{
	processScreen("Sr5ah", sv);
	if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	if (isEQ(scrnparams.statuz, varcom.endp)) {
		goTo(GotoLabel.exit4100);
	}
	
	if (isEQ(sv.select, ZERO)) {
		goTo(GotoLabel.next4100);
	}
	/*wssplife.subfileEnd.set(scrnparams.subfileEnd);*/
	/*wssplife.subfileRrn.set(scrnparams.subfileRrn);*/
	
	wsaaStatuz.set(wsaaScrnStatuz);
	
	optswchrec.optsSelType.set("L");
	optswchrec.optsSelOptno.set(sv.select);
	optswchrec.optsSelCode.set(SPACES);
	wsaaOptionCode.set(sv.select);
	for (wsaaCount.set(1); !(isGT(wsaaCount, 20)); wsaaCount.add(1)){
		if (isEQ(optswchrec.optsType[wsaaCount.toInt()], "L")
				&& isEQ(optswchrec.optsCode[wsaaCount.toInt()], wsaaOptionCode)) {
			optswchrec.optsInd[wsaaCount.toInt()].set("X");
			wsaaFoundSelection.set("Y");
		}
	}
	
	sv.select.set(ZERO);
	scrnparams.function.set(varcom.supd);
	screenIo9000();
}

protected void next4100()
{
	scrnparams.function.set(varcom.srdn);
}


}
