package com.csc.life.newbusiness.dataaccess.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.csc.life.newbusiness.dataaccess.model.Hpavpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface HpavpfDAO   extends BaseDAO<Hpavpf>{

	public List<Hpavpf> searchHpavpfRecord(String chdrcoy);
}
