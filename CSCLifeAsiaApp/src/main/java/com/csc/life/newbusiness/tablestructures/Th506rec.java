package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:46
 * Description:
 * Copybook name: TH506REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th506rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th506Rec = new FixedLengthStringData(getRecSize());
  	public FixedLengthStringData branchs = new FixedLengthStringData(20).isAPartOf(th506Rec, 0);
  	public FixedLengthStringData[] branch = FLSArrayPartOfStructure(10, 2, branchs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(branchs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData branch01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData branch02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData branch03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData branch04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData branch05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData branch06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData branch07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData branch08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData branch09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData branch10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData cflg = new FixedLengthStringData(1).isAPartOf(th506Rec, 20);
  	public ZonedDecimalData cnt = new ZonedDecimalData(2, 0).isAPartOf(th506Rec, 21);
  	public FixedLengthStringData confirm = new FixedLengthStringData(1).isAPartOf(th506Rec, 23);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(th506Rec, 24);
  	public ZonedDecimalData expdays = new ZonedDecimalData(3, 0).isAPartOf(th506Rec, 28);
  	public FixedLengthStringData freqcy = new FixedLengthStringData(2).isAPartOf(th506Rec, 31);
  	public FixedLengthStringData ind = new FixedLengthStringData(1).isAPartOf(th506Rec, 33);
  	public FixedLengthStringData indic = new FixedLengthStringData(1).isAPartOf(th506Rec, 34);
  	public FixedLengthStringData mandatorys = new FixedLengthStringData(2).isAPartOf(th506Rec, 35);
  	public FixedLengthStringData[] mandatory = FLSArrayPartOfStructure(2, 1, mandatorys, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(2).isAPartOf(mandatorys, 0, FILLER_REDEFINE);
  	public FixedLengthStringData mandatory01 = new FixedLengthStringData(1).isAPartOf(filler1, 0);
  	public FixedLengthStringData mandatory02 = new FixedLengthStringData(1).isAPartOf(filler1, 1);
  	public ZonedDecimalData nofbbisf = new ZonedDecimalData(2, 0).isAPartOf(th506Rec, 37);
  	public ZonedDecimalData nofbbwrn = new ZonedDecimalData(2, 0).isAPartOf(th506Rec, 39);
  	public FixedLengthStringData znfopts = new FixedLengthStringData(6).isAPartOf(th506Rec, 41);
  	public FixedLengthStringData[] znfopt = FLSArrayPartOfStructure(2, 3, znfopts, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(6).isAPartOf(znfopts, 0, FILLER_REDEFINE);
  	public FixedLengthStringData znfopt01 = new FixedLengthStringData(3).isAPartOf(filler2, 0);
  	public FixedLengthStringData znfopt02 = new FixedLengthStringData(3).isAPartOf(filler2, 3);
  	public FixedLengthStringData zzsrce = new FixedLengthStringData(2).isAPartOf(th506Rec, 47);
  	public ZonedDecimalData yearInforce = new ZonedDecimalData(2, 0).isAPartOf(th506Rec, 49);
  	public FixedLengthStringData calcmeth = new FixedLengthStringData(4).isAPartOf(th506Rec, 51);
  	//TMLII-294 AC-01-007 START
  	public FixedLengthStringData zdmsion = new FixedLengthStringData(15).isAPartOf(th506Rec,55);
  	//public FixedLengthStringData filler3 = new FixedLengthStringData(445).isAPartOf(th506Rec, 55, FILLER);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(430).isAPartOf(th506Rec, 70, FILLER);
  	//TMLII-294 AC-01-007 END
  	public FixedLengthStringData fatcaFlag = new FixedLengthStringData(1).isAPartOf(th506Rec, 500);

	public void initialize() {
		COBOLFunctions.initialize(th506Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th506Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	
	public int getRecSize() {
		return 501;
	}


}
