package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5070screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {8, 23, 11, 73}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5070ScreenVars sv = (S5070ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5070screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5070ScreenVars screenVars = (S5070ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.currcode.setClassString("");
		screenVars.numsel.setClassString("");
		screenVars.billcdDisp.setClassString("");
		screenVars.payrnum.setClassString("");
		screenVars.payorname.setClassString("");
		screenVars.mandref.setClassString("");
		screenVars.facthous.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.bankkey.setClassString("");
		screenVars.bankdesc.setClassString("");
		screenVars.branchdesc.setClassString("");
		screenVars.bankacckey.setClassString("");
		screenVars.bankaccdsc.setClassString("");
		screenVars.mrbnk.setClassString("");//ILIFE-2476-Starts
		screenVars.bnkbrn.setClassString("");
		screenVars.bnkbrndesc.setClassString("");//ILIFE-2476-Ends
	}

/**
 * Clear all the variables in S5070screen
 */
	public static void clear(VarModel pv) {
		S5070ScreenVars screenVars = (S5070ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.currcode.clear();
		screenVars.numsel.clear();
		screenVars.billcdDisp.clear();
		screenVars.billcd.clear();
		screenVars.payrnum.clear();
		screenVars.payorname.clear();
		screenVars.mandref.clear();
		screenVars.facthous.clear();
		screenVars.longdesc.clear();
		screenVars.bankkey.clear();
		screenVars.bankdesc.clear();
		screenVars.branchdesc.clear();
		screenVars.bankacckey.clear();
		screenVars.bankaccdsc.clear();
		screenVars.mrbnk.clear();//ILIFE-2476-Starts
		screenVars.bnkbrn.clear();
		screenVars.bnkbrndesc.clear();//ILIFE-2476-Ends
	}
}
