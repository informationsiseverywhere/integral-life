/*
 * File: P5106.java
 * Date: 30 August 2009 0:07:50
 * Author: Quipoz Limited
 * 
 * Class transformed from P5106.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.SoinTableDAM;
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.T3620rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.cashdividends.tablestructures.Th505rec;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovttrmTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.screens.S5106ScreenVars;
import com.csc.life.newbusiness.tablestructures.T5608rec;
import com.csc.life.newbusiness.tablestructures.T5703rec;
import com.csc.life.newbusiness.tablestructures.T5704rec;
import com.csc.life.newbusiness.tablestructures.T5705rec;
import com.csc.life.newbusiness.tablestructures.Tr52qrec;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T5689rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.life.unitlinkedprocessing.dataaccess.UnltunlTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5510rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*   An option 'Fast Track Issue' is included on the New Contract
*   Proposal Submenu P5002. Selection would require the entry of
*   the required contract type. Contract type would designate a
*   table item containing information defaults relative to the
*   type.
*
*   Selecting Fast Track will invoke a generic fast track screen/
*   Program(S5106/P5106) for data capture. This program reads
*   T5671 with 5105 + product type, e.g. 5105IM1* (this is non-
*   standard) which then invokes P5106. P5105 is a program
*   (without screen) that is invoked via normal program switching
*   (T1690).
*
*   Fast Track will provide default data from tables T5703, T5704
*   and T5705 in the appropriate places on the screen.
*
*   The system will allow entry of required information. The
*   screen information provided may be overwritten. All
*   information (entered and default) will be used to simulate
*   entry to normal proposal screens.
*
*   Pressing the Calculate Function Key or the Enter key will
*   will invoke proposal processing, calculation and validations.
*   Errors found when validating will be displayed. The facility
*   to alter the screen information will be provided and the
*   process will be repeated. Errors will prevent further
*   processing.
*
*   An entry in the 'Adjustment' position would result in the
*   invoking of the proposal modify system 'Work With Proposal'
*   screen S5003. Changes to S5106 will not be allowed on return
*   from this option.
*
*   An entry in the 'Apply Cash' position will result in the
*   invoking of the 'Apply cash from suspense to contract' screen
*   S5009.
*
*   Proceeding (Press Enter) with no errors will result in:
*      (a) Invoking of the 'Apply Cash from suspense to contract'
*          screen S5009, if it has not been invoked as an option
*          above and first billing date is not equal to commence-
*          ment date or T5688 indicate a minimum deposit is
*          required (applicable).
*      (b) Exiting the program to continue with the transaction
*          processing.
*
*   If it was decided not to issue the contract, the proposal
*   information will not be retained. Invoking of this option
*   will be by use of the Exit Function Key.
*
*   After all data has been captured and validated, pre issue
*   validation will be performed by P6378. Continuation
*   to the next program will be permitted when P6378 returns
*   a value of 'Y' in the 'ISSUE FLAG'.
*
*   On completion of a successful pre-issue validation, program
*   P6229 will be called to write the proposal details.
*
*   The contract will then be issued by invoking the AT program
*   P5074AT from program P5107.
*
*   COMMIT to finish.
*
*
* Initialise - Section 1000..
* ===========================
*
* Skip this section  if  returning  from  an optional selection
* (current stack position action flag = '*') or not the first
* time into this program (SCRN-SCRNAME not = spaces).
*
* Retrieve the contract being worked on by calling the CHDRLNB
* with a function of RETRV.
*
* Retrieve the life record that the contract is attached to by
* calling the LIFELNB with a function of RETRV.
*
* Read the default table T5705 to retrieve the header details
* and table T5679 to retrieve the contract status of issue.
*
* Get the contract description from T5688 by calling DESCIO.
*
*
* VALIDATION - Section 2000-...
* =============================
*
* Skip this section  if  returning  from  an optional selection
* (current stack position action flag = '*').
*
* If returning from 'Work with proposal' (Adjustment is selected)
* protect the screen and if the sum assured has changed then
* display an error message.
*
*
* Validation
* ==========
*
* Fast track subsystem processing specifications.
* Input, processing, output.
*
*    Input
*       New contract header from P5002 CHDRLNB I/O module
*       New contract life from P5002 LIFELNB I/O module
*          Table T5703
*          Table T5704
*          Table T5705
*          Screen S5016
*          Client.  Provide client scrolling to determine agent,
*                   life assured and owner.
*
*    Processing.
*       (a) Display screen with values from tables T5703,
*           T5704 and T5705.
*           Total premium will not be displayed first time
*           through
*       (b) Allow additions/amendments to be applied.
*
*       (c) Re-display amended screen including premium.
*           Screen details will be from output files. Show
*           error/warning comments and highlight where re-entries
*           required. Allow 'CASH' option.
*           Reprocess from (b) until nil errors.
*       (d) 'Adjustment' option taken. On return.
*           Calculate/validate package as described below.
*           Re-display amended screen 1.2 including total
*           premium as for (c).
*           Screen details will be from output files.
*           Show error comments.
*           Allow cash option.
*           Allow adjustment option.
*           Protect all other fields from input.
*           Reprocess using adjustment option until nil errors.
*
*    Validate and calculate
*       Initial, before input or change to screen.
*          (a)  Input record commencement date must not be zeroes
*          (b)  T3623 must contain contract status code.
*          (c)  T5705 must be in agreement with T5673, i.e.
*               product must be valid on T5673 too.
*       After input or change to screen.
*          (a)  Package Face Value/Units must be greater or
*               equal to T5704 Min. Base Unit.
*               Else output an error.
*          (b)  Package Face Value/Units less (T5704 Min. Base
*               Unit * T5704 Base unit) / Min. Base increment
*               must be an integer.
*               Else output an error.
*          (c) Validate income source.
*              If income source applicable, verify that it
*              exists on the SOINPF file else output an error.
*
*          (d) Validate life
*              If life died prior to Risk Commencement Date
*              output an error.
*          (e) Validate owner
*              If owner died prior to Risk Commencement Date
*              output an error.
*          (f) Validate no of policies.
*              Must be in range T5688 minimum and maximum
*          (g) Validate J/life.
*              If T5688 Joint life minimum > zero,
*              It  must be joint life.
*              else warning, will not allow 'ISSUE'
*          (h) Validate smoker code.
*              If not required (T5688) can be space
*              Else allowable on table T5668.
*          (i) Validate occupation code.
*              If not required (T5688) can be space
*              Else allowable on table T3644.
*          (j) Validate pursuit code.
*              If applicable, allowable on table T5665.
*          (k) Validate mortality code.
*              If applicable, allowable on table T5650.
*          (l) Validate medical evidence.
*              Allowable on table T5699.
*          (m) Validate contract currency.
*              Allowable on table T6660.
*          (n) Validate billing currency.
*              Accounting/billing currency allowable on table
*              T6660.
*          (o) Validate method of payment and frequency.
*              Frequency allowable on table T3590.
*              Method of payment allowable on table T3620 and
*              T6654. Must have entry on T5689.
*          (p) Validate register.
*              Allowable on table T3589.
*          (q) Validate source of business.
*              Allowable on table T3615.
*          (r) Validate campaign.
*              Allowable on table T3614.
*          (s) Validate statistical codes.
*              Allowable on table T3596.
*          (t) Validate contract branch.
*              Allowable on table T1692.
*          (u) Validate cross reference type and number.
*              If type not applicable number must be not
*              applicable.
*              If number not applicable type must be not
*              applicable.
*              If type applicable it must be allowable on
*              T3586 and number must contain a value.
*          (v) Validate agency.
*              Agency must be entered.
*              Agency must be a recognised system agency(AGLFPF)
*              Agency must be current.
*              Agency branch must be contract branch.
*          (w) Calculate age next birthday
*              Use 'DATCON3' with a frequency of 01 to
*              determine current age. Round up to higher
*              year(+.99999).
*              Validate age from T5688 Age next birthday
*              required, age must be calculable else error.
*          (x) Calculate/validate package.
*              1.  Calculate total premium.
*                  Total premium = total INSTPREM on all COVTLNB
*                  records for the contract, plus the contract
*                  fee is calculated using the subroutine from
*                  a T5674 item named in T5688. If the item is
*                  not named (ie spaces), contract fee is zero.
*              2.  If sum assured based, validate the sum
*                  assured, using parameters from table T5704
*                  appropriate to the benefit, and Sum Assured
*                  for each COVT will be calculated as follows
*                     Package Face Value/Unit * T5704 Base Unit
*                     * (T5704 component % of Base / 100)
*                     .5 rounded up.
*                  Otherwise output an error.
*              3.  If not sum assured based, validate the premium
*                  of each benefit, using parameters from table
*                  T5704 appropriate to the benefit, and
*                  Instalment premium must be as follows
*                     Package Face Value/Unit * T5704 Base Unit
*                     * (T5704 component % of Base / 100)
*                     .5 rounded up.
*                  Otherwise output an error.
*          (y) If first time or a change to billing method,
*              frequency or Risk Commencement date.
*              Calculate first billing date (bill commence)
*                 If frequency = '00'
*                    First billing date = Risk Commencement Date.
*                 If frequency not = '00' (i.e. not single prem)
*                    Calculate basic date
*                       where T5689 day is '99'
*                       If T6654 indicates commencement
*                          basic date = "Risk Commencement Date"
*                       else
*                          basic date = "Risk Commencement Date
*                          plus 1 frequency" period (use DATCON2)
*                          and month, and day from T5689.
*                    Establish bill commencement date.
*                       Bill commence date = the first
*                       legitimate day of month of basic date.
*                    Establish first billing date.
*                       where T5689 day is not '99'
*                       If T6654 indicates commencement date
*                       or bank account required T3620
*                          First billing date = the first
*                          legitimate date (day of month, if
*                          beyond end of month assume end of
*                          month) after the basic date with a
*                          day equal an entry on T5689.
*                       else,
*                          First billing date = the first
*                          legitimate date (day of month, if
*                          beyond end of month assume end of
*                          month) before the basic date with
*                          day equal an entry on T5689
*          (z) Validate first billing date (bill commence)
*              If frequency is single premium
*                 first billing date = Risk Commencement Date
*              If frequency is not single premium
*              and (not bank account required T3620
*              or T5689 days = '99'),
*                 first billing date >= Risk Commencement Date
*                 and < = Risk Commencement date plus 1 frequency
*              If frequency is not single premium
*              and bank account required T3620
*              and T5689 days not '99',
*                 first billing date >= Risk Commencement Date
*                 and < Risk Commencement Date plus 2 frequency
*                 with days = an entry on T5689.
*         (a1) Validate group number
*              Group required (T3620 group req = 'Y')
*              Group number not entered.
*                 Window to S5090 to allow entry/validation of
*                 group number.
*              Group number entered
*                 Use P5090 to validate group number.
*              Output error if invalid.
*              Group not required (T3620 group req not = 'Y')
*                 Group number not entered. OK.
*                 Group number entered output ERROR.
*         (b1) Validate mandate ref number.
*              Mandate required (T3620 bank account req = 'Y')
*              Mandate number not entered.
*                 Window to S5070 to allow entry/validation of
*                 mandate number.
*              Mandate number entered
*                 Use P5070 to  validate mandate number
*              Output error if invalid.
*              Mandate not required (T3620 bank account req not
*              = 'Y').
*              Mandate number not entered. OK.
*              Mandate number entered
*                  Output ERROR.
*
*
* UPDATING - 3000-Section...
* =========================
*
*  Only update the files if the optional selection (current stack
*  position action flag = '*') or it is the first time through.
*
*  If returning from work with proposal read the current contract
*  (Updated contract) and do a KEEPS and relock the contract
*  header because work with proposal released the softlock on the
*  contract header.
*
*  (a) Contract header update CHDRLNB I/O module.
*      Update CHDRLNB I/O module header.
*         Billing frequency from screen.
*         Billing channel from Method of payment screen
*         Collection channel from Method of payment screen
*          Bill to date as calculation above.
*         Bill commence date from screen
*         Polsum from no of policies Table T5705
*         Polinc same as Polsum
*         Current from date, from RCD of screen
*         Contract owner from screen
*         Original commencement date from RCD of Screen
*         Contract currency from Table T5705 if applicable
*         Billing currency  as for contract currency
*         Register from screen
*         Source of business from screen
*         Cross reference type from Table T5705
*         Cross reference number from Table T5705
*         Contract branch from screen
*         Agent number from screem
*         Campaign from Table T5705
*         Statistical codes A-E from Table T5705
*         Group number from screen
*         Mandate ref number from screen
*         Accounting method from T5688
*            If Revenue accounting = 'Y'
*                 method = 'R'
*            else method = 'C'
*
*  (b) Life  output LIFEPF.  Processing.
*      If first time or screen amended, update life record
*         Life assured is the life from screen
*         Age next birthday from screen
*         Smoker from Screen
*         Mortality class from screen
*         Sex, date of birth and occupation from life client
*            record.
*         Selection and relation = spaces.
*         Age adm, occupation, pursuit 1 and pursuit 2 from table
*            T5705.
*
*  (c) Create three client roles records in CLRRPF.
*
*      (1) Client payer roles relationship. Output CLRRPF.
*          Client number is owner number
*          client role is 'PY'
*
*      (2) Client owner roles relationship. Output CLRRPF.
*          Client number is owner number
*          client role is 'OW'
*
*      (3) Client life roles relationship. Output CLRRPF.
*          Client number is life number
*          client role is 'LF'
*
*  (d) Coverage.  Output a COVTPF for each benefit.
*      If not first time and screen package unit has been
*      amended, update existing COVT records.
*      If first time or screen package Face Value/Units has been
*      amended, create COVT records.
*          Create a COVT for each entry on table T5704.
*          (a)  Accept input from Table T5705 in place of the
*               generic screen P5123
*               1. If sum assured based, calculate sum assured
*                  using parameters from table T5704 appropriate
*                  to the benefit, and input to screen.
*               Sum assured will be calculated as follows
*                  Package Face Value/Units * T5704 Base Unit *
*                  (T5704 component % of units / 100)
*                  .5 rounded up.
*               2. If not sum assured based, calculate premium
*                   using parameters from table T5704 appropriate
*                   to the benefit, and input to screen.
*               Premium will be calculated as follows
*                  Package Face Value/Units * T5704 Base unit *
*                  (T5704 component % of units / 100)
*                  .5 rounded up.
*               Contract fee is calculated using the subroutine
*               from a T5674 item named in T5688.
*               If the item is not named (ie spaces), contract
*               fee is zero.
*               Once the base premium or sum assured has been
*               obtained, the relevant premium calculation
*               routine is called to determine the appropriate
*               premium or sum assure related to the obtained
*               values. E.g. sum assured obtained, then now
*               calculate premium; premium obtained, then now
*               calculate sum assured. Once all the values have
*               been determined, they are validated. T5671 is
*               read to obtain the Validation Item Key necessary
*               to read T5608 (Edit Rules Table).
*
*  (e)  Payer, output PAYRPF record.
*       If not first time and screen amended, update existing
*          payer records.
*       If first time or screen amended, create payer record
*           Effective date is Risk Commencment date
*           Billing channel from Billing Method
*           Billing frequency from Frequency
*           Bill commence date from First Billing Date
*           Next date = Bill commence date less lead billing
*              days T6654
*           Bill to date as calculated above
*           Paid to date = zeros
*           Bill currency from Table T5705
*           Sinstamts = zero
*           Out standing amount = zero
*           Tranno = '1'
*           Tax relief method from T5688 if income source
*               applicable and SOINPF PRAS IND = 'N' and T6697
*               T6697 EMPCON = 'N'.
*           Bill suppression = space
*           Bill suppression dates = zeros
*           Apl suppression = space
*           Apl suppression dates = zeros
*           Notice suppression = space
*           Notice suppression dates = zeros
*           Income seq number = income source
*           Mandate reference is mandate
*           Group key is group
*
*
* NEXT PROGRAM - Section 4000-...
* ===============================
*
* Check each 'selection'  indicator  in  turn.  If an option is
* selected, or automatic apply cash invoked, or direct billing
* (T3620 Bank Account Details Required = 'Y'), or Group (T3620
* Group Details Required = 'Y'), call GENSSWCH with the
* appropriate  action  to retrieve the optional program
* switching:
*    A - Adjustment
*    B - Apply cash
*    C - Direct Billing
*    D - Group
*
* For the first one selected, save the next set of programs (8)
* from the program  stack  and  flag the current positions with
* '*'.
*
* If no selection is entered and no error found and premium due
* then the apply cash screen S5009 will be invoked. Contract
* will not issue if there is any outstanding amount due.
*
* If the Risk Commencement Date and the First Billing Date are
* the same then pre-validation screen S6378 will be invoked with
* out invoking the apply cash program.
*
*
*****************************************************************
* </pre>
*/
public class P5106 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5106");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaY = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSubC = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSubR = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaNextUp = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaNextDown = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaIntToday = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaExtToday = new FixedLengthStringData(10);
	private ZonedDecimalData wsaa3MonthsAgo = new ZonedDecimalData(8, 0);
	private String wsaaDefaultBillcd = "";
	private String wsaaBillcdOk = "";
	private String wsaaMopFreqOk = "";
	private String wsaaEndOfMonth = "";
	private ZonedDecimalData wsaaLastEntryno = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaRendd = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaFactor = new PackedDecimalData(6, 0);
	private ZonedDecimalData wsaaRiskCessDate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaPremCessDate = new ZonedDecimalData(8, 0);
	private PackedDecimalData wsaaPremTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSuminTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSumin = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAdjSumin = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAdjPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPremReqd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCntfeeReqd = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaUnitbaseDiffFv = new ZonedDecimalData(8, 0);
	private PackedDecimalData wsaaAmtDue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSuspenseTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCntfee = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCalcTolerance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCalcTolerance2 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaToleranceApp = new PackedDecimalData(4, 2);
	private PackedDecimalData wsaaToleranceApp2 = new PackedDecimalData(4, 2);
	private PackedDecimalData wsaaToleranceTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaToleranceTot2 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAmountLimit = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAmountLimit2 = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaRemainder = new ZonedDecimalData(18, 9).setUnsigned();
	private ZonedDecimalData wsaaDiff = new ZonedDecimalData(16, 8);
	private FixedLengthStringData wsaaOwnersel = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2);
	private PackedDecimalData wsaaRiskCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wsaaPremCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wsaaRiskCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData wsaaPremCessTerm = new PackedDecimalData(11, 5);
	private final int wsaaMaxOcc = 8;

	private FixedLengthStringData wsaaItemFound = new FixedLengthStringData(1);
	private Validator itemNotFound = new Validator(wsaaItemFound, "N");
	private Validator itemFound = new Validator(wsaaItemFound, "Y");
		/* WSAA-ERROR-INDICATORS */
	private String wsaaOccdateInvd = "N";
		/* WSAA-COV-RID-NO */
	private FixedLengthStringData wsaaFillerCov = new FixedLengthStringData(20).init("01020304050607080910");
	private FixedLengthStringData[] wsaaCovNo = FLSArrayPartOfStructure(10, wsaaFillerCov, 0, REDEFINE);
	private FixedLengthStringData wsaaFillerRid = new FixedLengthStringData(20).init("00010203040506070809");
	private FixedLengthStringData[] wsaaRidNo = FLSArrayPartOfStructure(10, wsaaFillerRid, 0, REDEFINE);
		/*                                jmg                              */
	private PackedDecimalData wsaaCntfeeTaxReqd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCntfeeTax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPremTaxReqd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPremTax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaTempDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaTempDate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaTempMm = new ZonedDecimalData(2, 0).isAPartOf(filler, 4).setUnsigned();
	private ZonedDecimalData wsaaTempDd = new ZonedDecimalData(2, 0).isAPartOf(filler, 6).setUnsigned();
	private ZonedDecimalData wsaaBillcd = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(wsaaBillcd, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaBillcdYyyy = new ZonedDecimalData(4, 0).isAPartOf(filler1, 0).setUnsigned();
	private ZonedDecimalData wsaaBillcdMm = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4).setUnsigned();
	private ZonedDecimalData wsaaBillcdDd = new ZonedDecimalData(2, 0).isAPartOf(filler1, 6).setUnsigned();
	private ZonedDecimalData wsaaBtdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(wsaaBtdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaBtdateYyyy = new ZonedDecimalData(4, 0).isAPartOf(filler2, 0).setUnsigned();
	private ZonedDecimalData wsaaBtdateMm = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4).setUnsigned();
	private ZonedDecimalData wsaaBtdateDd = new ZonedDecimalData(2, 0).isAPartOf(filler2, 6).setUnsigned();
		/* WSAA-SEC-PROGRAMS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);

	private FixedLengthStringData wsaaT5703Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5703Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT5703Key, 0);
	private FixedLengthStringData wsaaT5703Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5703Key, 3);

		/*01  WSAA-T3625-KEY.                                              */
	private FixedLengthStringData wsaaT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsaaT5667Key, 0);
	private FixedLengthStringData wsaaT5667Curr = new FixedLengthStringData(4).isAPartOf(wsaaT5667Key, 4);

	private FixedLengthStringData wsaaT6654Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6654Mop = new FixedLengthStringData(1).isAPartOf(wsaaT6654Key, 0);
	private FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Key, 1);

		/*01  WSAA-T5658-KEY.                                              
		01  WSAA-T5659-KEY.                                              */
	private FixedLengthStringData wsaaT5608Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5608Tran = new FixedLengthStringData(5).isAPartOf(wsaaT5608Key, 0);
	private FixedLengthStringData wsaaT5608Cntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT5608Key, 5);

	private FixedLengthStringData wsaaT5606Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5606Tran = new FixedLengthStringData(5).isAPartOf(wsaaT5606Key, 0);
	private FixedLengthStringData wsaaT5606Cntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT5606Key, 5);

	private FixedLengthStringData wsaaTh505Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTh505Tran = new FixedLengthStringData(5).isAPartOf(wsaaTh505Key, 0);
	private FixedLengthStringData wsaaTh505Cntcurr = new FixedLengthStringData(3).isAPartOf(wsaaTh505Key, 5);

	private FixedLengthStringData wsaaT5551Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5551Tran = new FixedLengthStringData(5).isAPartOf(wsaaT5551Key, 0);
	private FixedLengthStringData wsaaT5551Cntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT5551Key, 5);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Transcd = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);

	private FixedLengthStringData wsaaLifeForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaLifeChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaLifeForenum, 0);
	private FixedLengthStringData wsaaLifeLife = new FixedLengthStringData(2).isAPartOf(wsaaLifeForenum, 8);

	private FixedLengthStringData wsaaMatch = new FixedLengthStringData(1);
	private Validator matchFound = new Validator(wsaaMatch, "Y");
	private Validator matchNotFound = new Validator(wsaaMatch, "N");

	private FixedLengthStringData wsaaError = new FixedLengthStringData(1);
	private Validator errorFound = new Validator(wsaaError, "Y");
	private Validator noErrorFound = new Validator(wsaaError, "N");

	private FixedLengthStringData wsaaSkipComplete = new FixedLengthStringData(1);
	private Validator skipComplete = new Validator(wsaaSkipComplete, "Y");
	private Validator skipNotComplete = new Validator(wsaaSkipComplete, "N");

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaAdjustment = new FixedLengthStringData(1).init("N");
	private Validator adjustment = new Validator(wsaaAdjustment, "Y");

	private FixedLengthStringData wsaaRelockChdrlnb = new FixedLengthStringData(1).init("N");
	private Validator relockChdrlnb = new Validator(wsaaRelockChdrlnb, "Y");

	private FixedLengthStringData wsaaInsufficientCash = new FixedLengthStringData(1).init("Y");
	private Validator insufficientCash = new Validator(wsaaInsufficientCash, "Y");

	private FixedLengthStringData wsaaSkipApCash = new FixedLengthStringData(1).init("N");
	private Validator skipApCash = new Validator(wsaaSkipApCash, "Y");
	private Validator notSkipApCash = new Validator(wsaaSkipApCash, "N");

	private FixedLengthStringData wsaaApplyCash = new FixedLengthStringData(1).init("N");
	private Validator applyCashSel = new Validator(wsaaApplyCash, "S");
	private Validator applyCashCont = new Validator(wsaaApplyCash, "C");
	private Validator notApplyCash = new Validator(wsaaApplyCash, "N");

	private FixedLengthStringData wsaaFirstKeepsLifelnb = new FixedLengthStringData(1).init("Y");
	private Validator firstKeepsLifelnb = new Validator(wsaaFirstKeepsLifelnb, "Y");

	private FixedLengthStringData wsaaPremiumType = new FixedLengthStringData(1).init(SPACES);
	private Validator singlePremium = new Validator(wsaaPremiumType, "Y");

	private FixedLengthStringData wsaaRcessageErr = new FixedLengthStringData(1);
	private Validator rcessageErr = new Validator(wsaaRcessageErr, "Y");

	private FixedLengthStringData wsaaPcessageErr = new FixedLengthStringData(1);
	private Validator pcessageErr = new Validator(wsaaPcessageErr, "Y");

	private FixedLengthStringData wsaaRcesstrmErr = new FixedLengthStringData(1);
	private Validator rcesstrmErr = new Validator(wsaaRcesstrmErr, "Y");

	private FixedLengthStringData wsaaPcesstrmErr = new FixedLengthStringData(1);
	private Validator pcesstrmErr = new Validator(wsaaPcesstrmErr, "Y");

	private FixedLengthStringData wsaaChnlChk = new FixedLengthStringData(1);
	private Validator creditCard = new Validator(wsaaChnlChk, "R");
	private Validator directDebit = new Validator(wsaaChnlChk, "D");

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);
		/* Dummy user wssp. Replace with required version.*/
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private AcblTableDAM acblIO = new AcblTableDAM();
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovttrmTableDAM covttrmIO = new CovttrmTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private SoinTableDAM soinIO = new SoinTableDAM();
	private UnltunlTableDAM unltunlIO = new UnltunlTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Freqcpy freqcpy = new Freqcpy();
	private Gensswrec gensswrec = new Gensswrec();
	private Cltrelnrec cltrelnrec = new Cltrelnrec();
	private Premiumrec premiumrec = new Premiumrec();
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private T2240rec t2240rec = new T2240rec();
	private T3695rec t3695rec = new T3695rec();
	private T5608rec t5608rec = new T5608rec();
	private T5606rec t5606rec = new T5606rec();
	private Th505rec th505rec = new Th505rec();
	private T5551rec t5551rec = new T5551rec();
	private T5645rec t5645rec = new T5645rec();
	private T5667rec t5667rec = new T5667rec();
	private T5671rec t5671rec = new T5671rec();
	private T5674rec t5674rec = new T5674rec();
	private T5675rec t5675rec = new T5675rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private T5689rec t5689rec = new T5689rec();
	private T5703rec t5703rec = new T5703rec();
	private T5704rec t5704rec = new T5704rec();
	private T5705rec t5705rec = new T5705rec();
	private T6654rec t6654rec = new T6654rec();
	private T3620rec t3620rec = new T3620rec();
	private T5510rec t5510rec = new T5510rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Tr52qrec tr52qrec = new Tr52qrec();
	private Bldenrlrec bldenrlrec = new Bldenrlrec();
	private S5106ScreenVars sv = ScreenProgram.getScreenVars( S5106ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaEditRuleItemsInner wsaaEditRuleItemsInner = new WsaaEditRuleItemsInner();
	private WsbbCovttrmInner wsbbCovttrmInner = new WsbbCovttrmInner();
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1009, 
		validateSelections2070, 
		checkForErrors2080, 
		loop2013, 
		compareSumins2018, 
		nextT56082130, 
		nextT56062130, 
		nextTh5052130, 
		nextT55512130, 
		continue2402, 
		exit2409, 
		loop2422, 
		checkDay2424, 
		exit2429, 
		loop2441, 
		checkDay2443, 
		exit2449, 
		loop2462, 
		exit2469, 
		chkSmoking2802, 
		exit2819, 
		exit2909, 
		keepsChdrlnb3008, 
		exit3309, 
		exit4009, 
		exit4509, 
		validateOwner5205, 
		exit5399, 
		writeOwnerRole5335, 
		exit5339, 
		loop5402, 
		checkDay5406, 
		exit5409, 
		exit6009, 
		exit6209, 
		loop6302, 
		checkTermFields6303, 
		continue6305, 
		exit6309, 
		exit6409, 
		chkRiskCessTerm6505, 
		callDatcon26506, 
		premCessTerm6605, 
		callDatcon26606, 
		sumValues6807, 
		exit6809, 
		sumValues6907, 
		exit6909
	}

	public P5106() {
		super();
		screenVars = sv;
		new ScreenModel("S5106", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		try {
			initialize1000();
			initializeScreen1003();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void initialize1000()
	{
		/* Always reset WSAA-BATCKEY with the current WSSP-BATCHKEY*/
		wsaaBatckey.set(wsspcomn.batchkey);
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
				if(cntDteFlag)	{
					sv.iljCntDteFlag.set("Y");
				} else {
					sv.iljCntDteFlag.set("N");
				}
		//ILJ-49 End
		/* If returning from an optional selection skip this section.*/
		/* Keep CHDRLNB and Exit.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			/*    OR SCRN-SCRNAME(2:4)        NOT = SPACES                     */
			/*    OR SCRN-SCRNAME(2:4)        NOT = SPACES             <S9503>>*/
			/*OR SCRN-SCRNAME             NOT = SPACES             <D509CS>*/
			goTo(GotoLabel.exit1009);
		}
		/* Release the PAYR record, because if the program                 */
		/* has previously been cancelled the lock remains                  */
		/* causing the WRITR on PAYR record to fail.                       */
		payrIO.setFunction(varcom.rlse);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaIntToday.set(datcon1rec.intDate);
		datcon2rec.intDatex1.set(datcon1rec.intDate);
		datcon2rec.frequency.set("12");
		datcon2rec.freqFactor.set(-3);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaa3MonthsAgo.set(datcon2rec.intDatex2);
		wsaaFirstTime.set("Y");
		wsaaFirstKeepsLifelnb.set("Y");
		wsaaInsufficientCash.set("Y");
		wsaaAdjustment.set("N");
		wsaaApplyCash.set("N");
		wsaaRelockChdrlnb.set("N");
		wsaaSuspenseTot.set(ZERO);
		wsaaCntfeeTax.set(ZERO);
		wsaaPremTax.set(ZERO);
		wsaaCntfeeTaxReqd.set(ZERO);
		wsaaPremTaxReqd.set(ZERO);
		wsaaTaxamt.set(ZERO);
		wsaaCntfee.set(ZERO);
		wsaaOwnersel.set(SPACES);
		/* Retrieve contract fields from I/O module*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrlnbIO.getOccdate(), ZERO)) {
			chdrlnbIO.setOccdate(varcom.vrcmMaxDate);
		}
		/* Retrieve life fields from I/O module*/
		lifelnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		/* Read the Fast Track - Contract/Life Details table to retrieve*/
		/* the default values for the contract header and the life.*/
		readTableT57051200();
		/* Get the policy dispatch details.                                */
		readTr52q1500();
		/* Get the contract description on table T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrlnbIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			descIO.setLongdesc("??????????????????????????????");
		}
		/*  Read T2240 for age definition                                  */
		readTableT22401400();
	}

protected void initializeScreen1003()
	{
		sv.dataArea.set(SPACES);
		sv.descrip.set(descIO.getLongdesc());
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		sv.register.set(t5705rec.register);
		sv.cntbranch.set(wsspcomn.branch);
		sv.srcebus.set(t5705rec.srcebus);
		sv.billcd.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.mop.set(t5705rec.mop);
		sv.billfreq.set(t5705rec.billfreq);
		sv.dlvrmode.set(tr52qrec.dlvrmode01);
		if (isNE(t2240rec.agecode01, SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02, SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03, SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
			/*MTL002*/	
			else {
				if (isNE(t2240rec.agecode04,SPACES)) {
					sv.zagelit.set(t2240rec.zagelit04);
				}
			}
			/*MTL002*/	
			}
		}
		sv.unitpkg.set(ZERO);
		sv.prmamt.set(ZERO);
		sv.taxamt.set(ZERO);
		sv.anbage.set(ZERO);
		sv.prmamtOut[varcom.nd.toInt()].set("Y");
		/* MOVE 'Y'                    TO S5106-PREM-OUT(ND).           */
		wsaaPremTot.set(ZERO);
		readTableT56791300();
		/* Initialize the array. It is assumed that they are no more*/
		/* than components on a contract. To cater for more than 20*/
		/* components on a contract increase the size to the array*/
		/* appropriately.*/
		wsaaSub.set(1);
		while ( !(isGT(wsaaSub, 20))) {
			wsbbCovttrmInner.wsbbCoverage[wsaaSub.toInt()].set(SPACES);
			wsbbCovttrmInner.wsbbRider[wsaaSub.toInt()].set(SPACES);
			wsbbCovttrmInner.wsbbCrtable[wsaaSub.toInt()].set(SPACES);
			wsbbCovttrmInner.wsbbRcessdte[wsaaSub.toInt()].set(ZERO);
			wsbbCovttrmInner.wsbbPcessdte[wsaaSub.toInt()].set(ZERO);
			wsbbCovttrmInner.wsbbRcessage[wsaaSub.toInt()].set(ZERO);
			wsbbCovttrmInner.wsbbPcessage[wsaaSub.toInt()].set(ZERO);
			wsbbCovttrmInner.wsbbPcesstrm[wsaaSub.toInt()].set(ZERO);
			wsbbCovttrmInner.wsbbRcesstrm[wsaaSub.toInt()].set(ZERO);
			wsbbCovttrmInner.wsbbSumin[wsaaSub.toInt()].set(ZERO);
			wsbbCovttrmInner.wsbbSingp[wsaaSub.toInt()].set(ZERO);
			wsbbCovttrmInner.wsbbInstprem[wsaaSub.toInt()].set(ZERO);
			wsbbCovttrmInner.wsbbZbinstprem[wsaaSub.toInt()].set(ZERO);
			wsbbCovttrmInner.wsbbZlinstprem[wsaaSub.toInt()].set(ZERO);
			wsaaSub.add(1);
		}
		
	}

	/**
	* <pre>
	* Fast Track Contract/Life Default table.
	* </pre>
	*/
protected void readTableT57051200()
	{
		readT57051200();
	}

protected void readT57051200()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5705);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmfrm(wsaaIntToday);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t5705, itdmIO.getItemtabl())
		|| isNE(chdrlnbIO.getCnttype(), itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(chdrlnbIO.getCnttype());
			itdmIO.setItemtabl(tablesInner.t5705);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.t029);
			fatalError600();
		}
		t5705rec.t5705Rec.set(itdmIO.getGenarea());
	}

	/**
	* <pre>
	* Component status table.
	* </pre>
	*/
protected void readTableT56791300()
	{
		readT56791300();
	}

protected void readT56791300()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sv.chdrnumErr.set(errorsInner.f321);
			sv.chdrnumOut[varcom.ri.toInt()].set("Y");
		}
		else {
			t5679rec.t5679Rec.set(itemIO.getGenarea());
		}
	}

protected void readTableT22401400()
	{
		para1400();
	}

protected void para1400()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(tablesInner.t2240);
		/* MOVE CHDRLNB-CNTTYPE        TO ITEM-ITEMITEM.        <AGEBTH>*/
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrlnbIO.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(tablesInner.t2240);
			/*    MOVE '***'               TO ITEM-ITEMITEM         <AGEBTH>*/
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void readTr52q1500()
	{
		begin1510();
	}

protected void begin1510()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52q);
		itemIO.setItemitem(chdrlnbIO.getCnttype());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52q);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				tr52qrec.tr52qRec.set(SPACES);
			}
			else {
				tr52qrec.tr52qRec.set(itemIO.getGenarea());
			}
		}
		else {
			tr52qrec.tr52qRec.set(itemIO.getGenarea());
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		/* If returning from an optional selection skip this section.      */
		/* BUT                                                            */
		/* If the optional selection that the system has returned from     */
		/*  was the pre-issue validation for fast track, then this         */
		/*  section should not be skipped.                                 */
		/*  INIT needs to be moved to SCRN-FUNCTION, because if the        */
		/*  refresh key had been entered previously, the refresh           */
		/*  causes NORML to be moved to SCRN-FUNCTION, where as it was     */
		/*  INIT the first time in.                                        */
		if (isEQ(wsspcomn.flag, "Z")
		&& isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.flag.set("M");
			scrnparams.function.set("INIT");
		}
		else {
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
				wsspcomn.sectionno.set("3000");
				return ;
			}
		}
		/* If returning from work with proposal and not cash has been      */
		/* applied protect the screen except the selection options.        */
		/* If the sum assured has changed display error.                   */
		if (adjustment.isTrue()
		|| !notApplyCash.isTrue()) {
			sv.occdateOut[varcom.pr.toInt()].set("Y");
			if (adjustment.isTrue()) {
				chkAdjSumin2010();
			}
		}
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2000();
					validate2020();
				case validateSelections2070: 
					validateSelections2070();
				case checkForErrors2080: 
					checkForErrors2080();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2000()
	{
		/*    CALL 'S5106IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                      S5106-DATA-AREA.           */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/* Skip the validation if the screen is protected.*/
		if (isEQ(sv.occdateOut[varcom.pr.toInt()], "Y")) {
			goTo(GotoLabel.validateSelections2070);
		}
	}

protected void validate2020()
	{
		/* Validate Risk Comm. Date. If invalid date found skip all*/
		/* validation.*/
		wsaaError.set("N");
		readTr52d2160();
		validateRiskCommDate5000();
		if (isEQ(wsaaOccdateInvd, "Y")) {
			goTo(GotoLabel.checkForErrors2080);
		}
		readTableT56882140();
		validateDefaultInput5100();
		if (errorFound.isTrue()) {
			goTo(GotoLabel.checkForErrors2080);
		}
		validateAgentOwner5200();
		validateGroupMandate5300();
		/* IF (S5106-BILLFREQ-OUT(CHG) = 'Y'                            */
		/* OR S5106-MOP-OUT(CHG)       = 'Y'                            */
		/* OR S5106-BILLCD-OUT(CHG)    = 'Y'                            */
		/* OR S5106-OCCDATE-OUT(CHG)   = 'Y'                            */
		/* OR S5106-BILLCD             = VRCM-MAX-DATE)                 */
		/* AND S5106-BILLFREQ-ERR      = SPACES                         */
		if (isEQ(sv.billfreqErr, SPACES)
		&& isEQ(sv.mopErr, SPACES)
		&& isEQ(sv.billcdErr, SPACES)
		&& isEQ(sv.occdateErr, SPACES)) {
			validateBillingDate2400();
		}
		processLifeSection2800();
		if (errorFound.isTrue()) {
			goTo(GotoLabel.validateSelections2070);
		}
		/* IF T5704-SINSBSD            NOT = 'Y'                        */
		if (isNE(t5704rec.sinsbsd[1], "Y")) {
			writsUpdateLifelnb2900();
		}
		validateAmounts6000();
	}

protected void validateSelections2070()
	{
		if (isNE(sv.adjind, "X")
		&& isNE(sv.adjind, "+")
		&& isNE(sv.adjind, " ")) {
			sv.adjindErr.set(errorsInner.g620);
			sv.adjindOut[varcom.ri.toInt()].set("Y");
		}
		if (isNE(sv.apcind, "X")
		&& isNE(sv.apcind, "+")
		&& isNE(sv.apcind, " ")) {
			sv.apcindErr.set(errorsInner.g620);
			sv.apcindOut[varcom.ri.toInt()].set("Y");
		}
		if (isEQ(sv.dlvrmode, SPACES)) {
			sv.dlvrmodeErr.set(errorsInner.e186);
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(wsaaPremTot, ZERO)) {
			sv.prmamt.set(wsaaPremTot);
			/*     MOVE WSAA-PREM-TOT      TO S5106-PREM                    */
			/*     MOVE SPACES             TO S5106-PREM-OUT(ND).           */
			sv.prmamtOut[varcom.nd.toInt()].set(SPACES);
		}
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void writeHpad2095()
	{
		writeHpadPara2095();
	}

protected void writeHpadPara2095()
	{
		/* Create a record  in HPAD.                                       */
		hpadIO.setParams(SPACES);
		hpadIO.setChdrcoy(wsspcomn.company);
		hpadIO.setChdrnum(sv.chdrnum);
		hpadIO.setFormat(formatsInner.hpadrec);
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)
		&& isNE(hpadIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hpadIO.getParams());
			fatalError600();
		}
		if (isEQ(hpadIO.getStatuz(), varcom.mrnf)) {
			hpadIO.setParams(SPACES);
			hpadIO.setChdrcoy(wsspcomn.company);
			hpadIO.setChdrnum(sv.chdrnum);
			hpadIO.setFormat(formatsInner.hpadrec);
			hpadIO.setHpropdte(sv.occdate);
			hpadIO.setHprrcvdt(sv.occdate);
			hpadIO.setHissdte(varcom.vrcmMaxDate);
			hpadIO.setZsufcdte(varcom.vrcmMaxDate);
			hpadIO.setHuwdcdte(sv.occdate);
			hpadIO.setHoissdte(sv.occdate);
			/* MOVE SPACES                 TO HPAD-ZNFOPT           <LA3511>*/
			hpadIO.setZnfopt(t5688rec.znfopt);
			hpadIO.setZdoctor(SPACES);
			hpadIO.setValidflag("3");
			hpadIO.setDlvrmode(sv.dlvrmode);
			hpadIO.setDespdate(varcom.vrcmMaxDate);
			hpadIO.setPackdate(varcom.vrcmMaxDate);
			hpadIO.setRemdte(varcom.vrcmMaxDate);
			hpadIO.setDeemdate(varcom.vrcmMaxDate);
			hpadIO.setNextActDate(varcom.vrcmMaxDate);
			hpadIO.setProcflg("Y");
			hpadIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, hpadIO);
			if (isNE(hpadIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(hpadIO.getParams());
				fatalError600();
			}
		}
	}

protected void chkAdjSumin2010()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initial2010();
				case loop2013: 
					loop2013();
					nextrCovttrm2017();
				case compareSumins2018: 
					compareSumins2018();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initial2010()
	{
		wsaaAdjSumin.set(ZERO);
		wsaaAdjPrem.set(ZERO);
		covttrmIO.setDataArea(SPACES);
		covttrmIO.setChdrcoy(wsspcomn.company);
		covttrmIO.setChdrnum(chdrlnbIO.getChdrnum());
		covttrmIO.setSeqnbr(ZERO);
		covttrmIO.setFormat(formatsInner.covttrmrec);
		covttrmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covttrmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covttrmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void loop2013()
	{
		SmartFileCode.execute(appVars, covttrmIO);
		if (isNE(covttrmIO.getStatuz(), varcom.oK)
		&& isNE(covttrmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covttrmIO.getParams());
			fatalError600();
		}
		if (isEQ(covttrmIO.getStatuz(), varcom.endp)
		|| isNE(wsspcomn.company, covttrmIO.getChdrcoy())
		|| isNE(chdrlnbIO.getChdrnum(), covttrmIO.getChdrnum())) {
			goTo(GotoLabel.compareSumins2018);
		}
		else {
			wsaaAdjSumin.add(covttrmIO.getSumins());
			compute(wsaaAdjPrem, 2).add(add(covttrmIO.getSingp(), covttrmIO.getInstprem()));
		}
	}

protected void nextrCovttrm2017()
	{
		covttrmIO.setFormat(formatsInner.covttrmrec);
		covttrmIO.setFunction(varcom.nextr);
		goTo(GotoLabel.loop2013);
	}

protected void compareSumins2018()
	{
		if (isNE(wsaaSuminTot, wsaaAdjSumin)) {
			sv.unitpkgErr.set(errorsInner.t032);
			sv.unitpkgOut[varcom.ri.toInt()].set("Y");
			sv.adjind.set("X");
		}
		wsaaPrem.set(wsaaAdjPrem);
		sv.prmamt.set(wsaaAdjPrem);
		/*EXIT*/
	}

protected void readT56712030()
	{
		readT56872030();
	}

protected void readT56872030()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5671);
		wsaaT5671Transcd.set(wsaaBatckey.batcBatctrcde);
		wsaaT5671Crtable.set(wsaaCrtable);
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
	}

protected void readTableT56872100()
	{
		readT56872100();
	}

protected void readT56872100()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(wsaaCrtable);
		itdmIO.setItmfrm(sv.occdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t5687, itdmIO.getItemtabl())
		|| isNE(wsaaCrtable, itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			t5687rec.t5687Rec.set(SPACES);
			scrnparams.errorCode.set(errorsInner.f294);
			wsspcomn.edterror.set("Y");
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

	/**
	* <pre>
	*  Use the  premium method  selected  from  T5687, if not blank
	*  access T5675.  This gives the subroutine to use for the
	*  calculation.
	* </pre>
	*/
protected void readTableT56752110()
	{
		readT56752110();
	}

protected void readT56752110()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5675);
		itemIO.setItemitem(t5687rec.premmeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5675rec.t5675Rec.set(SPACES);
		}
		else {
			/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
			if(AppVars.getInstance().getAppConfig().isVpmsEnable())
			{
				premiumrec.premMethod.set(itemIO.getItemitem());
			}
			/* ILIFE-3142 End*/
			t5675rec.t5675Rec.set(itemIO.getGenarea());
		}
	}

	/**
	* <pre>
	* Read the Package Unit/Component Type table T5704 to retrieve
	* the Package Face Value and Units Percentage.
	* </pre>
	*/
protected void readTableT57042120()
	{
		readT57042120();
	}

protected void readT57042120()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5704);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmfrm(sv.occdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t5704, itdmIO.getItemtabl())
		|| isNE(chdrlnbIO.getCnttype(), itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(chdrlnbIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.t027);
			fatalError600();
		}
		t5704rec.t5704Rec.set(itdmIO.getGenarea());
	}

protected void readTableT56082130()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
		readT56712130();
				case nextT56082130: 
		nextT56082130();
	}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readT56712130()
	{
		/* MOVE 'IT'                   TO ITEM-ITEMPFX.                 */
		/* MOVE WSSP-COMPANY           TO ITEM-ITEMCOY.                 */
		/* MOVE T5671                  TO ITEM-ITEMTABL.                */
		/* MOVE WSKY-BATC-BATCTRCDE    TO WSAA-T5671-TRANSCD.           */
		/* MOVE WSAA-CRTABLE           TO WSAA-T5671-CRTABLE.           */
		/* MOVE WSAA-T5671-KEY         TO ITEM-ITEMITEM.                */
		/* MOVE READR                  TO ITEM-FUNCTION.                */
		/* CALL 'ITEMIO'               USING ITEM-PARAMS.               */
		/* IF ITEM-STATUZ              NOT = O-K                        */
		/*     MOVE ITEM-PARAMS        TO SYSR-PARAMS                   */
		/*     PERFORM 600-FATAL-ERROR.                                 */
		/* MOVE ITEM-GENAREA           TO T5671-T5671-REC.              */
		wsaaIndex.set(ZERO);
	}

protected void nextT56082130()
	{
		wsaaIndex.add(1);
		if (isEQ(t5671rec.edtitm[wsaaIndex.toInt()], SPACES)
		|| isGT(wsaaIndex, 4)) {
			return ;
		}
		itdmIO.setStatuz(SPACES);
		itdmIO.setItemtabl(tablesInner.t5608);
		wsaaT5608Cntcurr.set(t5705rec.cntcurr);
		itdmIO.setItmfrm(sv.occdate);
		wsaaT5608Tran.set(t5671rec.edtitm[wsaaIndex.toInt()]);
		itdmIO.setItemitem(wsaaT5608Key);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t5608, itdmIO.getItemtabl())
		|| isNE(wsaaT5608Key, itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.nextT56082130);
		}
		/* Setup working storage                                        */
		wsaaItemFound.set("Y");
		t5608rec.t5608Rec.set(itdmIO.getGenarea());
		wsaaEditRuleItemsInner.wsaaEditRuleItems.set(SPACES);
		wsaaEditRuleItemsInner.wsaaAgeIssageFrms.set(t5608rec.ageIssageFrms);
		wsaaEditRuleItemsInner.wsaaAgeIssageTos.set(t5608rec.ageIssageTos);
		wsaaEditRuleItemsInner.wsaaTermIssageFrms.set(t5608rec.termIssageFrms);
		wsaaEditRuleItemsInner.wsaaTermIssageTos.set(t5608rec.termIssageTos);
		wsaaEditRuleItemsInner.wsaaPremCessageFroms.set(t5608rec.premCessageFroms);
		wsaaEditRuleItemsInner.wsaaPremCessageTos.set(t5608rec.premCessageTos);
		wsaaEditRuleItemsInner.wsaaPremCesstermFroms.set(t5608rec.premCesstermFroms);
		wsaaEditRuleItemsInner.wsaaPremCesstermTos.set(t5608rec.premCesstermTos);
		wsaaEditRuleItemsInner.wsaaRiskCessageFroms.set(t5608rec.riskCessageFroms);
		wsaaEditRuleItemsInner.wsaaRiskCessageTos.set(t5608rec.riskCessageTos);
		wsaaEditRuleItemsInner.wsaaRiskCesstermFroms.set(t5608rec.riskCesstermFroms);
		wsaaEditRuleItemsInner.wsaaRiskCesstermTos.set(t5608rec.riskCesstermTos);
		wsaaEditRuleItemsInner.wsaaSumInsMax.set(t5608rec.sumInsMax);
		wsaaEditRuleItemsInner.wsaaSumInsMin.set(t5608rec.sumInsMin);
		wsaaEditRuleItemsInner.wsaaEaage.set(t5608rec.eaage);
	}

	/**
	* <pre>
	**** MOVE T5608                  TO ITDM-ITEMTABL.                
	**** MOVE T5671-EDTITM(01)       TO WSAA-T5608-TRAN.              
	**** MOVE T5705-CNTCURR          TO WSAA-T5608-CNTCURR.           
	**** MOVE WSAA-T5608-KEY         TO ITDM-ITEMITEM.                
	**** MOVE S5106-OCCDATE          TO ITDM-ITMFRM.                  
	**** CALL 'ITDMIO'               USING ITDM-PARAMS.               
	**** IF WSSP-COMPANY             NOT = ITDM-ITEMCOY               
	**** OR T5608                    NOT = ITDM-ITEMTABL              
	**** OR WSAA-T5608-KEY           NOT = ITDM-ITEMITEM              
	****     MOVE ENDP               TO ITDM-STATUZ.                  
	**** IF ITDM-STATUZ              NOT = O-K                        
	**** AND ITDM-STATUZ             NOT = ENDP                       
	****     MOVE ITDM-PARAMS        TO SYSR-PARAMS                   
	****     PERFORM 600-FATAL-ERROR.                                 
	**** IF ITDM-STATUZ              = O-K                            
	****     MOVE ITDM-GENAREA       TO T5608-T5608-REC               
	**** ELSE                                                         
	****     MOVE SPACES             TO T5608-T5608-REC               
	****     MOVE ALL '0'            TO T5608-AGE-ISSAGE-FRMS         
	****     MOVE ALL '0'            TO T5608-AGE-ISSAGE-TOS          
	****     MOVE ALL '0'            TO T5608-TERM-ISSAGE-FRMS        
	****     MOVE ALL '0'            TO T5608-TERM-ISSAGE-TOS         
	****     MOVE ALL '0'            TO T5608-PREM-CESSAGE-FROMS      
	****     MOVE ALL '0'            TO T5608-PREM-CESSAGE-TOS        
	****     MOVE ALL '0'            TO T5608-PREM-CESSTERM-FROMS     
	****     MOVE ALL '0'            TO T5608-PREM-CESSTERM-TOS       
	****     MOVE ALL '0'            TO T5608-RISK-CESSAGE-FROMS      
	****     MOVE ALL '0'            TO T5608-RISK-CESSAGE-TOS        
	****     MOVE ALL '0'            TO T5608-RISK-CESSTERM-FROMS     
	****     MOVE ALL '0'            TO T5608-RISK-CESSTERM-TOS       
	****     MOVE ZEROES             TO T5608-SUM-INS-MAX             
	****                                T5608-SUM-INS-MIN.            
	*2139-EXIT.                                                       
	**** EXIT.                                                        
	* </pre>
	*/
protected void readTableT56062130()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
		readT56062130();
				case nextT56062130: 
		nextT56062130();
	}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readT56062130()
	{
		wsaaIndex.set(ZERO);
	}

protected void nextT56062130()
	{
		wsaaIndex.add(1);
		if (isEQ(t5671rec.edtitm[wsaaIndex.toInt()], SPACES)
		|| isGT(wsaaIndex, 4)) {
			return ;
		}
		itdmIO.setStatuz(SPACES);
		itdmIO.setItemtabl(tablesInner.t5606);
		wsaaT5606Cntcurr.set(t5705rec.cntcurr);
		itdmIO.setItmfrm(sv.occdate);
		wsaaT5606Tran.set(t5671rec.edtitm[wsaaIndex.toInt()]);
		itdmIO.setItemitem(wsaaT5606Key);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t5606, itdmIO.getItemtabl())
		|| isNE(wsaaT5606Key, itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.nextT56062130);
		}
		/* Setup working storage                                        */
		wsaaItemFound.set("Y");
		t5606rec.t5606Rec.set(itdmIO.getGenarea());
		wsaaEditRuleItemsInner.wsaaEditRuleItems.set(SPACES);
		wsaaEditRuleItemsInner.wsaaAgeIssageFrms.set(t5606rec.ageIssageFrms);
		wsaaEditRuleItemsInner.wsaaAgeIssageTos.set(t5606rec.ageIssageTos);
		wsaaEditRuleItemsInner.wsaaTermIssageFrms.set(t5606rec.termIssageFrms);
		wsaaEditRuleItemsInner.wsaaTermIssageTos.set(t5606rec.termIssageTos);
		wsaaEditRuleItemsInner.wsaaPremCessageFroms.set(t5606rec.premCessageFroms);
		wsaaEditRuleItemsInner.wsaaPremCessageTos.set(t5606rec.premCessageTos);
		wsaaEditRuleItemsInner.wsaaPremCesstermFroms.set(t5606rec.premCesstermFroms);
		wsaaEditRuleItemsInner.wsaaPremCesstermTos.set(t5606rec.premCesstermTos);
		wsaaEditRuleItemsInner.wsaaRiskCessageFroms.set(t5606rec.riskCessageFroms);
		wsaaEditRuleItemsInner.wsaaRiskCessageTos.set(t5606rec.riskCessageTos);
		wsaaEditRuleItemsInner.wsaaRiskCesstermFroms.set(t5606rec.riskCesstermFroms);
		wsaaEditRuleItemsInner.wsaaRiskCesstermTos.set(t5606rec.riskCesstermTos);
		wsaaEditRuleItemsInner.wsaaSumInsMax.set(t5606rec.sumInsMax);
		wsaaEditRuleItemsInner.wsaaSumInsMin.set(t5606rec.sumInsMin);
		wsaaEditRuleItemsInner.wsaaEaage.set(t5606rec.eaage);
	}

protected void readTableTh5052130()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
		readTh5052130();
				case nextTh5052130: 
		nextTh5052130();
	}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readTh5052130()
	{
		wsaaIndex.set(ZERO);
	}

protected void nextTh5052130()
	{
		wsaaIndex.add(1);
		if (isEQ(t5671rec.edtitm[wsaaIndex.toInt()], SPACES)
		|| isGT(wsaaIndex, 4)) {
			return ;
		}
		itdmIO.setStatuz(SPACES);
		itdmIO.setItemtabl(tablesInner.th505);
		wsaaTh505Cntcurr.set(t5705rec.cntcurr);
		itdmIO.setItmfrm(sv.occdate);
		wsaaTh505Tran.set(t5671rec.edtitm[wsaaIndex.toInt()]);
		itdmIO.setItemitem(wsaaTh505Key);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.th505, itdmIO.getItemtabl())
		|| isNE(wsaaTh505Key, itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.nextTh5052130);
		}
		/* Setup working storage                                        */
		wsaaItemFound.set("Y");
		th505rec.th505Rec.set(itdmIO.getGenarea());
		wsaaEditRuleItemsInner.wsaaEditRuleItems.set(SPACES);
		wsaaEditRuleItemsInner.wsaaAgeIssageFrms.set(th505rec.ageIssageFrms);
		wsaaEditRuleItemsInner.wsaaAgeIssageTos.set(th505rec.ageIssageTos);
		wsaaEditRuleItemsInner.wsaaTermIssageFrms.set(th505rec.termIssageFrms);
		wsaaEditRuleItemsInner.wsaaTermIssageTos.set(th505rec.termIssageTos);
		wsaaEditRuleItemsInner.wsaaPremCessageFroms.set(th505rec.premCessageFroms);
		wsaaEditRuleItemsInner.wsaaPremCessageTos.set(th505rec.premCessageTos);
		wsaaEditRuleItemsInner.wsaaPremCesstermFroms.set(th505rec.premCesstermFroms);
		wsaaEditRuleItemsInner.wsaaPremCesstermTos.set(th505rec.premCesstermTos);
		wsaaEditRuleItemsInner.wsaaRiskCessageFroms.set(th505rec.riskCessageFroms);
		wsaaEditRuleItemsInner.wsaaRiskCessageTos.set(th505rec.riskCessageTos);
		wsaaEditRuleItemsInner.wsaaRiskCesstermFroms.set(th505rec.riskCesstermFroms);
		wsaaEditRuleItemsInner.wsaaRiskCesstermTos.set(th505rec.riskCesstermTos);
		wsaaEditRuleItemsInner.wsaaSumInsMax.set(th505rec.sumInsMax);
		wsaaEditRuleItemsInner.wsaaSumInsMin.set(th505rec.sumInsMin);
		wsaaEditRuleItemsInner.wsaaEaage.set(th505rec.eaage);
	}

protected void readTableT55512130()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
		readT55512130();
				case nextT55512130: 
		nextT55512130();
	}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readT55512130()
	{
		wsaaIndex.set(ZERO);
	}

protected void nextT55512130()
	{
		wsaaIndex.add(1);
		if (isEQ(t5671rec.edtitm[wsaaIndex.toInt()], SPACES)
		|| isGT(wsaaIndex, 4)) {
			return ;
		}
		itdmIO.setStatuz(SPACES);
		itdmIO.setItemtabl(tablesInner.t5551);
		wsaaT5551Cntcurr.set(t5705rec.cntcurr);
		itdmIO.setItmfrm(sv.occdate);
		wsaaT5551Tran.set(t5671rec.edtitm[wsaaIndex.toInt()]);
		itdmIO.setItemitem(wsaaT5551Key);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t5551, itdmIO.getItemtabl())
		|| isNE(wsaaT5551Key, itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.nextT55512130);
		}
		/* Setup working storage                                        */
		wsaaItemFound.set("Y");
		t5551rec.t5551Rec.set(itdmIO.getGenarea());
		wsaaEditRuleItemsInner.wsaaEditRuleItems.set(SPACES);
		wsaaEditRuleItemsInner.wsaaAgeIssageFrms.set(t5551rec.ageIssageFrms);
		wsaaEditRuleItemsInner.wsaaAgeIssageTos.set(t5551rec.ageIssageTos);
		wsaaEditRuleItemsInner.wsaaTermIssageFrms.set(t5551rec.termIssageFrms);
		wsaaEditRuleItemsInner.wsaaTermIssageTos.set(t5551rec.termIssageTos);
		wsaaEditRuleItemsInner.wsaaPremCessageFroms.set(t5551rec.premCessageFroms);
		wsaaEditRuleItemsInner.wsaaPremCessageTos.set(t5551rec.premCessageTos);
		wsaaEditRuleItemsInner.wsaaPremCesstermFroms.set(t5551rec.premCesstermFroms);
		wsaaEditRuleItemsInner.wsaaPremCesstermTos.set(t5551rec.premCesstermTos);
		wsaaEditRuleItemsInner.wsaaRiskCessageFroms.set(t5551rec.maturityAgeFroms);
		wsaaEditRuleItemsInner.wsaaRiskCessageTos.set(t5551rec.maturityAgeTos);
		wsaaEditRuleItemsInner.wsaaRiskCesstermFroms.set(t5551rec.maturityTermFroms);
		wsaaEditRuleItemsInner.wsaaRiskCesstermTos.set(t5551rec.maturityTermTos);
		wsaaEditRuleItemsInner.wsaaSumInsMax.set(t5551rec.sumInsMax);
		wsaaEditRuleItemsInner.wsaaSumInsMin.set(t5551rec.sumInsMin);
		wsaaEditRuleItemsInner.wsaaEaage.set(t5551rec.eaage);
	}

	/**
	* <pre>
	* Read  the  contract  definition  details  from  T5688 for the
	* contract  type  held  on  CHDRLNB. Access the version of this
	* item for the original commencement date of the risk.
	* </pre>
	*/
protected void readTableT56882140()
	{
		readT56882140();
	}

protected void readT56882140()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmfrm(sv.occdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(), chdrlnbIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setStatuz(varcom.endp);
			sv.chdrnumErr.set(errorsInner.f290);
			sv.chdrnumOut[varcom.ri.toInt()].set("Y");
			return ;
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		if (isEQ(t5688rec.revacc, "Y")) {
			chdrlnbIO.setAcctmeth("R");
		}
		else {
			chdrlnbIO.setAcctmeth("C");
		}
	}

protected void readTableT56892150()
	{
		readT56892150();
	}

protected void readT56892150()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5689);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmfrm(sv.occdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5689)
		|| isNE(itdmIO.getItemitem(), chdrlnbIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			sv.mopErr.set(errorsInner.e368);
			sv.mopOut[varcom.ri.toInt()].set("Y");
		}
		else {
			t5689rec.t5689Rec.set(itdmIO.getGenarea());
		}
	}

protected void readTr52d2160()
	{
		start2160();
	}

protected void start2160()
	{
		/* Read TR52D for Taxcode                                          */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(sv.register);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		/* Protect TAXAMT field                                            */
		if (isEQ(tr52drec.txcode, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("Y");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("N");
		}
	}

protected void validateBillingDate2400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					lookUpT66542400();
				case continue2402: 
					continue2402();
				case exit2409: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void lookUpT66542400()
	{
		/*  Work out the Billed To Date - it will either be equal to*/
		/*  the Risk Commencement Date, (OCCDATE) or 1 frequency greater*/
		/*  than it.*/
		
		if (BTPRO028Permission) {
			String key = sv.mop.toString().trim()+chdrlnbIO.getCnttype().toString().trim()+sv.billfreq.toString().trim();
			readT6654600(key);
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				key = sv.mop.toString().trim().concat(chdrlnbIO.getCnttype().toString().trim()).concat("**");
				readT6654600(key);
				if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
					key= sv.mop.toString().trim().concat("*****");
					readT6654600(key);
					if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
						sv.mopErr.set(errorsInner.e840);
						goTo(GotoLabel.exit2409);
					}
				}
			}
		}
		else {
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemtabl(tablesInner.t6654);
		wsaaT6654Key.set(SPACES);
		wsaaT6654Mop.set(sv.mop);
		wsaaT6654Cnttype.set(chdrlnbIO.getCnttype());
		itemIO.setItemitem(wsaaT6654Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			t6654rec.t6654Rec.set(itemIO.getGenarea());
			goTo(GotoLabel.continue2402);
		}
		wsaaT6654Cnttype.set("***");
		itemIO.setItemitem(wsaaT6654Key);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			t6654rec.t6654Rec.set(itemIO.getGenarea());
		}
		else {
			sv.mopErr.set(errorsInner.e840);
			goTo(GotoLabel.exit2409);
		}
		}
	}

protected void readT6654600(String key)
{
	/*CALL-ITEMIO*/
	itemIO.setDataKey(SPACES);
	itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
	itemIO.setItempfx(smtpfxcpy.item);
	itemIO.setItemtabl(tablesInner.t6654);
	itemIO.setItemitem(key);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(), varcom.oK)
	&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
	if(isEQ(itemIO.getStatuz(), varcom.oK)) {
		t6654rec.t6654Rec.set(itemIO.getGenarea());
	}
	/*EXIT*/
}

	/**
	* <pre>
	* Check for default premium renewal date:
	* </pre>
	*/
protected void continue2402()
	{
		if (isEQ(t6654rec.renwdate2, "Y")) {
			datcon2rec.intDate1.set(sv.occdate);
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.frequency.set(sv.billfreq);
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isEQ(datcon2rec.statuz, varcom.oK)) {
				chdrlnbIO.setBtdate(datcon2rec.intDate2);
				payrIO.setBtdate(datcon2rec.intDate2);
			}
		}
		else {
			chdrlnbIO.setBtdate(sv.occdate);
			payrIO.setBtdate(sv.occdate);
		}
		readTableT56892150();
		/*  Default billing commencement date if blank.*/
		if (isEQ(sv.billcd, varcom.vrcmMaxDate)) {
			wsaaDefaultBillcd = "Y";
			checkAgainstTable2410();
			return ;
		}
		wsaaDefaultBillcd = "N";
		wsaaEndOfMonth = "N";
		wsaaTempDate.set(sv.billcd);
		wsaaRendd.set(wsaaTempDd);
		if (isEQ(wsaaTempMm, 4)
		|| isEQ(wsaaTempMm, 6)
		|| isEQ(wsaaTempMm, 9)
		|| isEQ(wsaaTempMm, 11)
		&& isEQ(wsaaTempDd, 30)) {
			wsaaEndOfMonth = "Y";
		}
		if (isEQ(wsaaTempMm, 2)
		&& isEQ(wsaaTempDd, 28)
		|| isEQ(wsaaTempDd, 29)) {
			wsaaEndOfMonth = "Y";
		}
		/*  Skip this code if MOP is = 'Y'*/
		/* IF T3625-DDIND               = 'Y'                           */
		if (isNE(t3620rec.ddind, SPACES)) {
			checkDdDate2450();
			return ;
		}
		/*  Billing date must be > or = Contract Commencement Date*/
		/*  and < Contract Commencement Date + 2*Billing Frequency - 1*/
		if (isLT(sv.billcd, sv.occdate)) {
			sv.billcdErr.set(errorsInner.e481);
			checkAgainstTable2410();
			return ;
		}
		if (isEQ(sv.billfreq, "00")) {
			if (isNE(sv.occdate, sv.billcd)) {
				sv.billcdErr.set(errorsInner.t034);
			}
			checkAgainstTable2410();
			return ;
		}
		datcon2rec.frequency.set(sv.billfreq);
		datcon2rec.freqFactor.set(2);
		datcon2rec.intDate1.set(sv.occdate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		datcon2rec.intDate2.subtract(1);
		if (isGT(sv.billcd, datcon2rec.intDate2)) {
			sv.billcdErr.set(errorsInner.e525);
		}
		else {
			checkDdDate2450();
		}
	}

	/**
	* <pre>
	*  Check the payment method/frequency/date are valid combination.
	* </pre>
	*/
protected void checkAgainstTable2410()
	{
		validateMopFreq2410();
	}

protected void validateMopFreq2410()
	{
		validateMopFreq2420();
		if (isNE(wsaaDefaultBillcd, "Y")) {
			if (isEQ(wsaaBillcdOk, "N")) {
				sv.billcdErr.set(errorsInner.e487);
				return ;
			}
		}
		if (isEQ(wsaaMopFreqOk, "N")) {
			sv.mopErr.set(errorsInner.e484);
			sv.billfreqErr.set(errorsInner.e484);
			return ;
		}
		/*  Calculate frequency factor between Contract Commencement*/
		/*   date and Billing Commencement date. If it is greater than*/
		/*   1 frequency factor, advance the Bill To Date by the number*/
		/*   of integer frequencies.*/
		if (isEQ(wsaaDefaultBillcd, "Y")) {
			defaultBillcd2430();
		}
		return ;
		/* UNREACHABLE CODE
		if (!(isGT(sv.billcd, sv.occdate))) {
			return ;
		}
		datcon3rec.intDate1.set(sv.occdate);
		datcon3rec.intDate2.set(sv.billcd);
		datcon3rec.frequency.set(sv.billfreq);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			sv.billcdErr.set(datcon3rec.statuz);
			return ;
		}
		if (isGT(datcon3rec.freqFactor, 1)
		|| isEQ(datcon3rec.freqFactor, 1)) {
			wsaaFactor.set(datcon3rec.freqFactor);
			datcon2rec.intDate1.set(chdrlnbIO.getBtdate());
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.frequency.set(sv.billfreq);
			datcon2rec.freqFactor.set(wsaaFactor);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isEQ(datcon2rec.statuz, varcom.oK)) {
				chdrlnbIO.setBtdate(datcon2rec.intDate2);
				payrIO.setBtdate(datcon2rec.intDate2);
			}
			else {
				sv.billcdErr.set(datcon2rec.statuz);
			}
		}
		 */
	}

protected void validateMopFreq2420()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialize2420();
				case loop2422: 
					loop2422();
				case checkDay2424: 
					checkDay2424();
				case exit2429: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialize2420()
	{
		wsaaSub.set(ZERO);
		wsaaMopFreqOk = "N";
		wsaaBillcdOk = "N";
	}

protected void loop2422()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 30)
		|| isEQ(wsaaBillcdOk, "Y")) {
			goTo(GotoLabel.exit2429);
		}
		/*CHECKING*/
		if (isEQ(t5689rec.billfreq[wsaaSub.toInt()], sv.billfreq)
		&& isEQ(t5689rec.mop[wsaaSub.toInt()], sv.mop)) {
			wsaaMopFreqOk = "Y";
			goTo(GotoLabel.checkDay2424);
		}
		goTo(GotoLabel.loop2422);
	}

protected void checkDay2424()
	{
		if (isEQ(wsaaDefaultBillcd, "Y")) {
			wsaaRendd.set(t5689rec.rendd[wsaaSub.toInt()]);
			wsaaBillcdOk = "Y";
		}
		else {
			if (isEQ(t5689rec.rendd[wsaaSub.toInt()], "99")
			|| isEQ(t5689rec.rendd[wsaaSub.toInt()], wsaaRendd)) {
				wsaaBillcdOk = "Y";
			}
			else {
				if (isEQ(wsaaEndOfMonth, "Y")) {
					if (isEQ(t5689rec.rendd[wsaaSub.toInt()], "31")) {
						wsaaBillcdOk = "Y";
					}
					else {
						if (isEQ(t5689rec.rendd[wsaaSub.toInt()], "29")
						|| isEQ(t5689rec.rendd[wsaaSub.toInt()], "30")
						&& isEQ(wsaaTempMm, 2)) {
							wsaaBillcdOk = "Y";
						}
					}
				}
			}
		}
		goTo(GotoLabel.loop2422);
	}

	/**
	* <pre>
	*  The default billing commencement date is
	*         contract commencement plus one frequency.
	*  If all billing dates are not allowed ("day" not 99),
	*    this, or the next "day" immediatly after this is used.
	*    After calculating this date, check it is not off the end
	*    of the month (e.g. 30 Feb). If it is, move one day
	*    backwards until it is a valid date.
	* </pre>
	*/
protected void defaultBillcd2430()
	{
		getBillingDay2430();
	}

protected void getBillingDay2430()
	{
		searchForTableMatch2440();
		if (isEQ(sv.billcd, varcom.vrcmMaxDate)) {
			if (isEQ(wsaaNextUp, 99)) {
				if (isEQ(wsaaNextDown, 0)) {
					sv.billcdErr.set(errorsInner.e483);
				}
				else {
					wsaaBtdateDd.set(wsaaNextDown);
					sv.billcd.set(wsaaBtdate);
				}
			}
			else {
				wsaaBtdateDd.set(wsaaNextUp);
				sv.billcd.set(wsaaBtdate);
			}
		}
		wsaaBtdate.set(chdrlnbIO.getBtdate());
		wsaaBillcd.set(sv.billcd);
		if (isNE(wsaaBtdateMm, wsaaBillcdMm)) {
			sv.billcdErr.set(errorsInner.i012);
		}
		else {
			if (isNE(wsaaBtdateYyyy, wsaaBillcdYyyy)) {
				sv.billcdErr.set(errorsInner.j002);
			}
		}
		wsaaBillcd.set(wsaaBtdate);
		wsaaTempDate.set(wsaaBtdate);
		if (isEQ(wsaaTempMm, 4)
		|| isEQ(wsaaTempMm, 6)
		|| isEQ(wsaaTempMm, 9)
		|| isEQ(wsaaTempMm, 11)
		&& isEQ(wsaaTempDd, 30)) {
			wsaaEndOfMonth = "Y";
		}
		else {
			wsaaEndOfMonth = "N";
		}
		if (isEQ(wsaaTempMm, 2)
		&& isEQ(wsaaTempDd, 28)
		|| isEQ(wsaaTempDd, 29)) {
			wsaaEndOfMonth = "Y";
		}
		else {
			wsaaEndOfMonth = "N";
		}
		if ((isEQ(wsaaTempMm, 4)
		|| isEQ(wsaaTempMm, 6)
		|| isEQ(wsaaTempMm, 9)
		|| isEQ(wsaaTempMm, 11))
		&& (isEQ(wsaaRendd, "31"))) {
			wsaaTempDd.set(30);
			sv.billcd.set(wsaaTempDate);
		}
		if ((isEQ(wsaaTempMm, 2))
		&& (isEQ(wsaaRendd, "29")
		|| isEQ(wsaaRendd, "30")
		|| isEQ(wsaaRendd, "31"))) {
			wsaaTempDd.set(29);
			sv.billcd.set(wsaaTempDate);
			datcon1rec.intDate.set(wsaaTempDate);
			datcon1rec.function.set(varcom.conv);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz, varcom.oK)) {
				wsaaTempDd.set(28);
				sv.billcd.set(wsaaTempDate);
			}
		}
	}

protected void searchForTableMatch2440()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialize2440();
				case loop2441: 
					loop2441();
				case checkDay2443: 
					checkDay2443();
				case exit2449: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialize2440()
	{
		wsaaSub.set(ZERO);
		wsaaNextDown.set(ZERO);
		wsaaNextUp.set(99);
	}

protected void loop2441()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 30)
		|| isNE(sv.billcd, varcom.vrcmMaxDate)) {
			goTo(GotoLabel.exit2449);
		}
		/*CHECKING*/
		if (isEQ(t5689rec.billfreq[wsaaSub.toInt()], sv.billfreq)
		&& isEQ(t5689rec.mop[wsaaSub.toInt()], sv.mop)) {
			goTo(GotoLabel.checkDay2443);
		}
		goTo(GotoLabel.loop2441);
	}

protected void checkDay2443()
	{
		if (isEQ(t5689rec.rendd[wsaaSub.toInt()], "99")) {
			sv.billcd.set(chdrlnbIO.getBtdate());
			return ;
		}
		wsaaBtdate.set(chdrlnbIO.getBtdate());
		if (isEQ(t5689rec.rendd[wsaaSub.toInt()], wsaaBtdateDd)) {
			sv.billcd.set(chdrlnbIO.getBtdate());
			return ;
		}
		if (isLT(wsaaBtdateDd, t5689rec.rendd[wsaaSub.toInt()])) {
			if (isLT(t5689rec.rendd[wsaaSub.toInt()], wsaaNextUp)) {
				wsaaNextUp.set(t5689rec.rendd[wsaaSub.toInt()]);
			}
		}
		else {
			if (isGT(t5689rec.rendd[wsaaSub.toInt()], wsaaNextDown)) {
				wsaaNextDown.set(t5689rec.rendd[wsaaSub.toInt()]);
			}
		}
		goTo(GotoLabel.loop2441);
	}

	/**
	* <pre>
	* Dates are validated before continuing processing
	* </pre>
	*/
protected void checkDdDate2450()
	{
		validateBillcd2450();
	}

protected void validateBillcd2450()
	{
		validateBillcd2460();
		if (matchNotFound.isTrue()) {
			sv.billcdErr.set(errorsInner.e483);
			return ;
		}
		datcon3rec.intDate1.set(sv.occdate);
		datcon3rec.intDate2.set(sv.billcd);
		datcon3rec.frequency.set(sv.billfreq);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isGTE(datcon3rec.freqFactor, 2)) {
			sv.billcdErr.set(errorsInner.e032);
		}
		else {
			checkAgainstTable2410();
		}
	}

protected void validateBillcd2460()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialize2460();
				case loop2462: 
					loop2462();
					checking2463();
				case exit2469: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialize2460()
	{
		wsaaSub.set(ZERO);
	}

protected void loop2462()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 30)) {
			wsaaMatch.set("N");
			goTo(GotoLabel.exit2469);
		}
	}

protected void checking2463()
	{
		if (isNE(t5689rec.billfreq[wsaaSub.toInt()], sv.billfreq)) {
			goTo(GotoLabel.loop2462);
		}
		if (isNE(t5689rec.mop[wsaaSub.toInt()], sv.mop)) {
			goTo(GotoLabel.loop2462);
		}
		if (isEQ(t5689rec.rendd[wsaaSub.toInt()], "99")) {
			wsaaMatch.set("Y");
			return ;
		}
		wsaaBillcd.set(sv.billcd);
		if (isEQ(t5689rec.rendd[wsaaSub.toInt()], wsaaBillcdDd)) {
			wsaaMatch.set("Y");
			return ;
		}
		goTo(GotoLabel.loop2462);
	}

protected void processLifeSection2800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialize2800();
					calcAnb2801();
				case chkSmoking2802: 
					chkSmoking2802();
					chkForError2806();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialize2800()
	{
		readTableT57042120();
		/*CHK-LIFE*/
		if (isEQ(sv.lifesel, SPACES)) {
			if (isEQ(sv.ownerselErr, SPACES)) {
				sv.lifesel.set(sv.ownersel);
				sv.lifename.set(sv.ownername);
			}
			else {
				sv.lifeselErr.set(errorsInner.e186);
			}
		}
		if (isEQ(sv.lifeselErr, SPACES)) {
			validateLife2810();
		}
	}

protected void calcAnb2801()
	{
		if (isNE(sv.lifeselErr, SPACES)) {
			goTo(GotoLabel.chkSmoking2802);
		}
		/*    MOVE CLTS-CLTDOB            TO DTC3-INT-DATE-1.              */
		/*    MOVE S5106-OCCDATE          TO DTC3-INT-DATE-2.              */
		/*    MOVE '01'                   TO DTC3-FREQUENCY.               */
		/*    CALL 'DATCON3'              USING DTC3-DATCON3-REC.          */
		/* Round up the age.*/
		/*    IF DTC3-STATUZ              = O-K                            */
		/*        COMPUTE S5106-ANBAGE ROUNDED                             */
		/*                                = DTC3-FREQ-FACTOR + 0.5         */
		/*        MOVE S5106-ANBAGE       TO LIFELNB-ANB-AT-CCD.           */
		/* If there is no Date of Birth on the client record, display   */
		/* an error to this effect and skip the age calculation.        */
		if (isEQ(cltsIO.getCltdob(), varcom.vrcmMaxDate)) {
			sv.lifeselErr.set(errorsInner.f859);
			goTo(GotoLabel.chkSmoking2802);
		}
		/* Routine to calculate Age next/nearest/last birthday             */
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(wsspcomn.language);
		agecalcrec.cnttype.set(chdrlnbIO.getCnttype());
		agecalcrec.intDate1.set(cltsIO.getCltdob());
		agecalcrec.intDate2.set(sv.occdate);
		agecalcrec.company.set(wsspcomn.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
		sv.anbage.set(agecalcrec.agerating);
		lifelnbIO.setAnbAtCcd(agecalcrec.agerating);
	}

protected void chkSmoking2802()
	{
		if ((isEQ(t5705rec.smkrqd, "Y")
		|| isEQ(t5705rec.smkrqd, "R"))
		&& isEQ(sv.smoking, SPACES)) {
			sv.smokingErr.set(errorsInner.e186);
			sv.smokingOut[varcom.ri.toInt()].set("Y");
		}
		/*CHK-MORTALITY*/
		if ((isEQ(t5705rec.mortality, "Y")
		|| isEQ(t5705rec.mortality, "R"))
		&& isEQ(sv.mortcls, SPACES)) {
			sv.mortclsErr.set(errorsInner.e186);
			sv.mortclsOut[varcom.ri.toInt()].set("Y");
		}
		/*CHK-JLIFE*/
		if (isGT(t5688rec.jlifemin, ZERO)) {
			sv.lifeselErr.set(errorsInner.t030);
			sv.lifeselOut[varcom.ri.toInt()].set("Y");
		}
	}

protected void chkForError2806()
	{
		if (isNE(sv.lifeselErr, SPACES)
		|| isNE(sv.smokingErr, SPACES)
		|| isNE(sv.mortclsErr, SPACES)) {
			wsaaError.set("Y");
		}
		/*EXIT*/
	}

protected void validateLife2810()
	{
		try {
			readClst2810();
			chkOccpcode2815();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readClst2810()
	{
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(sv.lifesel);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.lifeselErr.set(errorsInner.h143);
			sv.lifename.set(SPACES);
			goTo(GotoLabel.exit2819);
		}
		lifelnbIO.setCltdob(cltsIO.getCltdob());
		lifelnbIO.setCltsex(cltsIO.getCltsex());
		/* if SOE & CLTDOB exist default 'Age admitted' to non-space       */
		if (isEQ(cltsIO.getSoe(), SPACES)
		|| isEQ(cltsIO.getCltdob(), varcom.vrcmMaxDate)
		|| isEQ(cltsIO.getCltdob(), ZERO)) {
			lifelnbIO.setAgeadm(SPACES);
		}
		else {
			lifelnbIO.setAgeadm("X");
		}
		lifelnbIO.setOccup(cltsIO.getOccpcode());
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/*  Is the life dead?*/
		if (isNE(sv.occdate, varcom.vrcmMaxDate)) {
			if (isLTE(cltsIO.getCltdod(), sv.occdate)) {
				sv.lifeselErr.set(errorsInner.f782);
			}
		}
		/*  Check for personal client and client not an alias.*/
		if (isNE(cltsIO.getClttype(), "P")) {
			sv.lifeselErr.set(errorsInner.g844);
		}
		else {
			if (isNE(cltsIO.getCltind(), "C")) {
				sv.lifeselErr.set(errorsInner.g499);
			}
		}
	}

protected void chkOccpcode2815()
	{
		if (isEQ(t5688rec.occrqd, "R")
		&& isEQ(cltsIO.getOccpcode(), SPACES)) {
			sv.lifeselErr.set(errorsInner.g788);
		}
	}

protected void writsUpdateLifelnb2900()
	{
		try {
			chkChangeInd2900();
			readhLifelnb2901();
			updateLifelnb2904();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void chkChangeInd2900()
	{
		/* IF S5106-OCCDATE-OUT(CHG)   NOT = 'Y'                        */
		/* AND S5106-LIFESEL-OUT(CHG)  NOT = 'Y'                        */
		/* AND S5106-SMOKING-OUT(CHG)  NOT = 'Y'                        */
		/*     GO TO                   2909-EXIT.                       */
		if (firstKeepsLifelnb.isTrue()) {
			wsaaFirstKeepsLifelnb.set("N");
			/* Set up the VRCM variables before writing a LIFELNB record*/
			/* because the VRCM variables are not set up at this point.*/
			opstatsrec.opsrecTim3000.set(getCobolTime());
			varcom.vrcmTranid.set(wsspcomn.tranid);
			varcom.vrcmTimex.set(opstatsrec.opsrecTim3000);
			varcom.vrcmTime.set(varcom.vrcmTimen);
			varcom.vrcmDate.set(getCobolDate());
			wsspcomn.tranid.set(varcom.vrcmTranid);
			keepsWritsLifelnb3200();
			goTo(GotoLabel.exit2909);
		}
	}

protected void readhLifelnb2901()
	{
		lifelnbIO.setDataKey(SPACES);
		lifelnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
		lifelnbIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
	}

protected void updateLifelnb2904()
	{
		lifelnbIO.setCurrfrom(sv.occdate);
		lifelnbIO.setLifeCommDate(sv.occdate);
		lifelnbIO.setLifcnum(sv.lifesel);
		lifelnbIO.setSmoking(sv.smoking);
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
		lifelnbIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateDatabase3000();
					writeChdrlnb3001();
					writrPayr3002();
					writrCovttrms3003();
					writePayorRole3004();
					writeOwnerRole3004();
					writeClientRole3004();
					writrLifelnb3005();
					callBldenrl3006();
				case keepsChdrlnb3008: 
					keepsChdrlnb3008();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateDatabase3000()
	{
		/* If returning from an optional selection skip this section.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")
		|| !firstTime.isTrue()) {
			goTo(GotoLabel.keepsChdrlnb3008);
		}
	}

	/**
	* <pre>
	*  Move fields from the screen to the contract header.
	* </pre>
	*/
protected void writeChdrlnb3001()
	{
		/*MOVE T5679-CN-RISK-STAT-01  TO CHDRLNB-STATCODE.             */
		chdrlnbIO.setStatcode(t5679rec.setCnRiskStat);
		if (singlePremium.isTrue()) {
			chdrlnbIO.setPstatcode(t5679rec.setSngpCnStat);
		}
		else {
			chdrlnbIO.setPstatcode(t5679rec.setCnRiskStat);
		}
		chdrlnbIO.setPolinc(t5705rec.plansuff);
		chdrlnbIO.setPolsum(ZERO);
		chdrlnbIO.setCurrfrom(sv.occdate);
		chdrlnbIO.setOccdate(sv.occdate);
		chdrlnbIO.setReptype(t5705rec.reptype);
		chdrlnbIO.setRepnum(t5705rec.lrepnum);
		chdrlnbIO.setCownpfx("CN");
		chdrlnbIO.setCowncoy(wsspcomn.fsuco);
		chdrlnbIO.setCownnum(sv.ownersel);
		chdrlnbIO.setCntbranch(sv.cntbranch);
		chdrlnbIO.setAgntpfx("AG");
		chdrlnbIO.setAgntcoy(wsspcomn.company);
		chdrlnbIO.setAgntnum(sv.agntsel);
		chdrlnbIO.setCntcurr(t5705rec.cntcurr);
		chdrlnbIO.setBillfreq(sv.billfreq);
		chdrlnbIO.setBillchnl(sv.mop);
		chdrlnbIO.setCollchnl(sv.mop);
		chdrlnbIO.setBillcd(sv.billcd);
		chdrlnbIO.setBtdate(sv.billcd);
		chdrlnbIO.setBillcurr(t5705rec.billcurr);
		chdrlnbIO.setCampaign(t5705rec.campaign);
		chdrlnbIO.setSrcebus(sv.srcebus);
		chdrlnbIO.setChdrstcda(t5705rec.stcal);
		chdrlnbIO.setChdrstcdb(t5705rec.stcbl);
		chdrlnbIO.setChdrstcdc(t5705rec.stccl);
		chdrlnbIO.setChdrstcdd(t5705rec.stcdl);
		chdrlnbIO.setChdrstcde(t5705rec.stcel);
		chdrlnbIO.setRegister(sv.register);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		chdrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		chdrlnbIO.setFunction(varcom.writs);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		chdrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	* The Lead Billing Days is from T6654 which was read in
	* Section 2400-.....
	* </pre>
	*/
protected void writrPayr3002()
	{
		/*   Set up payer details.*/
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(sv.chdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setSinstamt01(ZERO);
		payrIO.setSinstamt02(ZERO);
		payrIO.setSinstamt03(ZERO);
		payrIO.setSinstamt04(ZERO);
		payrIO.setSinstamt05(ZERO);
		payrIO.setSinstamt06(ZERO);
		payrIO.setBillsupr(SPACES);
		payrIO.setAplsupr(SPACES);
		payrIO.setNotssupr(SPACES);
		payrIO.setBillspfrom(ZERO);
		payrIO.setBillspto(ZERO);
		payrIO.setAplspfrom(ZERO);
		payrIO.setAplspto(ZERO);
		payrIO.setNotsspfrom(ZERO);
		payrIO.setNotsspto(ZERO);
		payrIO.setEffdate(sv.occdate);
		payrIO.setValidflag("1");
		payrIO.setBillchnl(sv.mop);
		payrIO.setBillfreq(sv.billfreq);
		payrIO.setBillcd(sv.billcd);
		payrIO.setBtdate(sv.billcd);
		wsaaBillcd.set(sv.billcd);
		/* Split the BILLCD into DD,MM and YYYY and move the month and     */
		/* day to the PAYRPF file separately.                              */
		payrIO.setBillmonth(wsaaBillcdMm);
		payrIO.setDuemm(wsaaBillcdMm);
		payrIO.setBillday(wsaaBillcdDd);
		payrIO.setDuedd(wsaaBillcdDd);
		/* Calculate the next billing extract date*/
		/* NEXTDATE = BILLCD less the No of lead billing days.*/
		datcon2rec.intDate1.set(payrIO.getBillcd());
		datcon2rec.frequency.set("DY");
		compute(datcon2rec.freqFactor, 0).set(mult(t6654rec.leadDays, -1));
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		payrIO.setNextdate(datcon2rec.intDate2);
		payrIO.setPtdate(ZERO);
		payrIO.setBillcurr(t5705rec.billcurr);
		payrIO.setCntcurr(t5705rec.cntcurr);
		payrIO.setOutstamt(ZERO);
		payrIO.setTaxrelmth(SPACES);
		payrIO.setIncomeSeqNo(t5705rec.incomeSeqNo);
		payrIO.setTranno(chdrlnbIO.getTranno());
		payrIO.setTermid(varcom.vrcmTermid);
		payrIO.setTransactionDate(varcom.vrcmDate);
		payrIO.setTransactionTime(varcom.vrcmTime);
		payrIO.setUser(varcom.vrcmUser);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/* Write HPADPF record.                                         */
		writeHpad2095();
	}

	/**
	* <pre>
	* write the coverages record
	* </pre>
	*/
protected void writrCovttrms3003()
	{
		wsaaSub.set(1);
		while ( !(isGT(wsaaSub, wsaaLastEntryno))) {
			writrCovttrms3100();
		}
		
	}

	/**
	* <pre>
	* Create a new client payor role record.
	* The default for payor is the owner.
	* </pre>
	*/
protected void writePayorRole3004()
	{
		cltrelnrec.cltrelnRec.set(SPACES);
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(payrIO.getChdrcoy());
		wsaaChdrnum.set(sv.chdrnum);
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		cltrelnrec.forenum.set(wsaaForenum);
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.clntnum.set(sv.ownersel);
		cltrelnrec.clrrrole.set("PY");
		cltrelnrec.function.set("ADD  ");
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(cltrelnrec.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	* Create a new client owner role record.
	* </pre>
	*/
protected void writeOwnerRole3004()
	{
		cltrelnrec.cltrelnRec.set(SPACES);
		cltrelnrec.forecoy.set(payrIO.getChdrcoy());
		cltrelnrec.forenum.set(sv.chdrnum);
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.clrrrole.set("OW");
		cltrelnrec.clntnum.set(sv.ownersel);
		cltrelnrec.function.set("ADD  ");
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(cltrelnrec.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	* Create a new client life role record.
	* </pre>
	*/
protected void writeClientRole3004()
	{
		cltrelnrec.cltrelnRec.set(SPACES);
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(payrIO.getChdrcoy());
		wsaaLifeChdrnum.set(sv.chdrnum);
		wsaaLifeLife.set(lifelnbIO.getLife());
		cltrelnrec.forenum.set(wsaaLifeForenum);
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.clntnum.set(sv.lifesel);
		cltrelnrec.clrrrole.set("LF");
		cltrelnrec.function.set("ADD  ");
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(cltrelnrec.statuz);
			fatalError600();
		}
	}

protected void writrLifelnb3005()
	{
		/* IF T5704-SINSBSD            = 'Y'                            */
		if (isEQ(t5704rec.sinsbsd[1], "Y")) {
			keepsWritsLifelnb3200();
		}
	}

protected void callBldenrl3006()
	{
		initialize(bldenrlrec.bldenrlrec);
		bldenrlrec.prefix.set(chdrlnbIO.getChdrpfx());
		bldenrlrec.company.set(chdrlnbIO.getChdrcoy());
		bldenrlrec.uentity.set(chdrlnbIO.getChdrnum());
		callProgram(Bldenrl.class, bldenrlrec.bldenrlrec);
		if (isNE(bldenrlrec.statuz, varcom.oK)) {
			syserrrec.params.set(bldenrlrec.bldenrlrec);
			syserrrec.statuz.set(bldenrlrec.statuz);
			fatalError600();
		}
	}

protected void keepsChdrlnb3008()
	{
		if (isEQ(sv.ddind, "?")) {
			sv.ddind.set("+");
			retrvChdrlnb3400();
			return ;
		}
		if (isEQ(sv.crcind, "?")) {
			sv.crcind.set("+");
			retrvChdrlnb3400();
			return ;
		}
		if (isEQ(sv.grpind, "?")) {
			sv.grpind.set("+");
			retrvChdrlnb3400();
			return ;
		}
		chdrlnbIO.setChdrcoy(wsspcomn.company);
		chdrlnbIO.setChdrnum(sv.chdrnum);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		chdrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/*    Soft lock contract.*/
		if (!relockChdrlnb.isTrue()) {
			return ;
		}
		refreshScreenValues3300();
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrnumErr.set(errorsInner.f910);
			wsspcomn.edterror.set("Y");
		}
	}

protected void writrCovttrms3100()
	{
		initializeCovttrm3100();
	}

protected void initializeCovttrm3100()
	{
		covttrmIO.setDataArea(SPACES);
		covttrmIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		covttrmIO.setChdrnum(chdrlnbIO.getChdrnum());
		covttrmIO.setCntcurr(chdrlnbIO.getCntcurr());
		covttrmIO.setLife(lifelnbIO.getLife());
		covttrmIO.setCoverage(wsbbCovttrmInner.wsbbCoverage[wsaaSub.toInt()]);
		covttrmIO.setRider(wsbbCovttrmInner.wsbbRider[wsaaSub.toInt()]);
		covttrmIO.setSeqnbr(1);
		covttrmIO.setSex01(lifelnbIO.getCltsex());
		covttrmIO.setSex02(SPACES);
		covttrmIO.setAnbAtCcd01(lifelnbIO.getAnbAtCcd());
		covttrmIO.setAnbAtCcd02(ZERO);
		covttrmIO.setTermid(varcom.vrcmTermid);
		covttrmIO.setTransactionDate(varcom.vrcmDate);
		covttrmIO.setTransactionTime(varcom.vrcmTime);
		covttrmIO.setUser(varcom.vrcmUser);
		covttrmIO.setCrtable(wsbbCovttrmInner.wsbbCrtable[wsaaSub.toInt()]);
		covttrmIO.setRiskCessDate(wsbbCovttrmInner.wsbbRcessdte[wsaaSub.toInt()]);
		covttrmIO.setPremCessDate(wsbbCovttrmInner.wsbbPcessdte[wsaaSub.toInt()]);
		covttrmIO.setRiskCessAge(wsbbCovttrmInner.wsbbRcessage[wsaaSub.toInt()]);
		covttrmIO.setPremCessAge(wsbbCovttrmInner.wsbbPcessage[wsaaSub.toInt()]);
		covttrmIO.setRiskCessTerm(wsbbCovttrmInner.wsbbRcesstrm[wsaaSub.toInt()]);
		covttrmIO.setPremCessTerm(wsbbCovttrmInner.wsbbPcesstrm[wsaaSub.toInt()]);
		covttrmIO.setSumins(wsbbCovttrmInner.wsbbSumin[wsaaSub.toInt()]);
		covttrmIO.setSingp(wsbbCovttrmInner.wsbbSingp[wsaaSub.toInt()]);
		covttrmIO.setInstprem(wsbbCovttrmInner.wsbbInstprem[wsaaSub.toInt()]);
		covttrmIO.setZbinstprem(wsbbCovttrmInner.wsbbZbinstprem[wsaaSub.toInt()]);
		covttrmIO.setZlinstprem(wsbbCovttrmInner.wsbbZlinstprem[wsaaSub.toInt()]);
		covttrmIO.setMortcls(sv.mortcls);
		covttrmIO.setLiencd(SPACES);
		covttrmIO.setJlife(SPACES);
		covttrmIO.setPolinc(t5705rec.plansuff);
		covttrmIO.setNumapp(t5705rec.plansuff);
		covttrmIO.setBillfreq(sv.billfreq);
		covttrmIO.setBillchnl(sv.mop);
		covttrmIO.setEffdate(sv.occdate);
		covttrmIO.setPayrseqno(1);
		covttrmIO.setBenCessDate(varcom.vrcmMaxDate);
		covttrmIO.setBenCessAge(ZERO);
		covttrmIO.setBenCessTerm(ZERO);
		covttrmIO.setFunction(varcom.writr);
		covttrmIO.setFormat(formatsInner.covttrmrec);
		SmartFileCode.execute(appVars, covttrmIO);
		if (isNE(covttrmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covttrmIO.getParams());
			fatalError600();
		}
		setupFundSplit3150();
		wsaaSub.add(1);
	}

protected void setupFundSplit3150()
	{
		start3150();
	}

protected void start3150()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5703);
		wsaaT5703Cnttype.set(chdrlnbIO.getCnttype());
		wsaaT5703Crtable.set(wsbbCovttrmInner.wsbbCrtable[wsaaSub.toInt()]);
		itdmIO.setItemitem(wsaaT5703Key);
		itdmIO.setItmfrm(sv.occdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t5703, itdmIO.getItemtabl())
		|| isNE(wsaaT5703Key, itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.t024);
			fatalError600();
		}
		t5703rec.t5703Rec.set(itdmIO.getGenarea());
		/* Setup UNLTPF records only if default fund split is found     */
		if (isNE(t5703rec.virtFundSplitMethod, SPACES)) {
			setupUnlt3155();
		}
	}

protected void setupUnlt3155()
	{
		start3155();
	}

protected void start3155()
	{
		/* Read T5510 to get the fund split details.                    */
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5510);
		itdmIO.setItemitem(t5703rec.virtFundSplitMethod);
		itdmIO.setItmfrm(sv.occdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t5510, itdmIO.getItemtabl())
		|| isNE(t5703rec.virtFundSplitMethod, itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.g095);
			fatalError600();
		}
		t5510rec.t5510Rec.set(itdmIO.getGenarea());
		/* Check that the default fund split is non-empty.              */
		if (isEQ(t5510rec.unitVirtualFunds, SPACES)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.g144);
			fatalError600();
		}
		setupUnlt3160();
		writeUnlt3170();
	}

protected void setupUnlt3160()
	{
		/*START*/
		/* Setup field value for UNLTUNL                                */
		unltunlIO.setDataArea(SPACES);
		unltunlIO.setDataKey(covttrmIO.getDataKey());
		unltunlIO.setCurrto(varcom.vrcmMaxDate);
		unltunlIO.setCurrfrom(chdrlnbIO.getOccdate());
		unltunlIO.setTranno(chdrlnbIO.getTranno());
		unltunlIO.setNumapp(covttrmIO.getNumapp());
		unltunlIO.setPercOrAmntInd("P");
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 10)); wsaaIndex.add(1)){
			zeroise3167();
		}
		/*EXIT*/
	}

protected void zeroise3167()
	{
		/*START*/
		unltunlIO.setUalprc(wsaaIndex, ZERO);
		unltunlIO.setUspcpr(wsaaIndex, ZERO);
		/*EXIT*/
	}

protected void writeUnlt3170()
	{
		start3170();
	}

protected void start3170()
	{
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 10)); wsaaIndex.add(1)){
			if (isNE(t5510rec.unitVirtualFund[wsaaIndex.toInt()], SPACES)
			&& isNE(t5510rec.unitPremPercent[wsaaIndex.toInt()], ZERO)) {
				unltunlIO.setUalprc(wsaaIndex, t5510rec.unitPremPercent[wsaaIndex.toInt()]);
				unltunlIO.setUalfnd(wsaaIndex, t5510rec.unitVirtualFund[wsaaIndex.toInt()]);
			}
		}
		/* Write UNLTUNL                                                */
		unltunlIO.setStatuz(SPACES);
		unltunlIO.setFormat(formatsInner.unltunlrec);
		unltunlIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, unltunlIO);
		if (isNE(unltunlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(unltunlIO.getParams());
			syserrrec.statuz.set(unltunlIO.getStatuz());
			fatalError600();
		}
	}

protected void keepsWritsLifelnb3200()
	{
		keepsLifelnb3200();
		writsLifelnb3205();
	}

protected void keepsLifelnb3200()
	{
		lifelnbIO.setPursuit01(t5705rec.pursuit01);
		lifelnbIO.setPursuit02(t5705rec.pursuit02);
		lifelnbIO.setCurrfrom(sv.occdate);
		lifelnbIO.setLifeCommDate(sv.occdate);
		lifelnbIO.setLifcnum(sv.lifesel);
		/* MOVE T5705-AGEADM           TO LIFELNB-AGEADM.               */
		lifelnbIO.setSelection(t5705rec.selection);
		lifelnbIO.setSmoking(sv.smoking);
		lifelnbIO.setTermid(varcom.vrcmTermid);
		lifelnbIO.setTransactionDate(varcom.vrcmDate);
		lifelnbIO.setTransactionTime(varcom.vrcmTime);
		lifelnbIO.setUser(varcom.vrcmUser);
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
		lifelnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
	}

protected void writsLifelnb3205()
	{
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
		lifelnbIO.setFunction(varcom.writs);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void refreshScreenValues3300()
	{
		try {
			moveValues3300();
			readPayr3302();
			readLifelnb3304();
			readHpad3306();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void moveValues3300()
	{
		sv.occdate.set(chdrlnbIO.getOccdate());
		sv.register.set(chdrlnbIO.getRegister());
		sv.srcebus.set(chdrlnbIO.getSrcebus());
		sv.agntsel.set(chdrlnbIO.getAgntnum());
		aglflnbIO.setDataKey(SPACES);
		aglflnbIO.setAgntcoy(wsspcomn.company);
		aglflnbIO.setAgntnum(sv.agntsel);
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(), varcom.oK)
		&& isNE(aglflnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(aglflnbIO.getParams());
			fatalError600();
		}
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(aglflnbIO.getClntnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.agntname.set(wsspcomn.longconfname);
		sv.ownersel.set(chdrlnbIO.getCownnum());
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(sv.ownersel);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
	}

	/**
	* <pre>
	* Read the PAYR record to retrieve the changes make in
	* work with proposal
	* </pre>
	*/
protected void readPayr3302()
	{
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(sv.chdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)
		&& isNE(payrIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		if (isEQ(payrIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.exit3309);
		}
		sv.mop.set(payrIO.getBillchnl());
		sv.billfreq.set(payrIO.getBillfreq());
		sv.billcd.set(payrIO.getBillcd());
	}

	/**
	* <pre>
	* Read the LIFELNB record to retrieve the changes make in
	* work with proposal
	* </pre>
	*/
protected void readLifelnb3304()
	{
		lifelnbIO.setChdrcoy(wsspcomn.company);
		lifelnbIO.setChdrnum(sv.chdrnum);
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		&& isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isEQ(lifelnbIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.exit3309);
		}
		sv.lifesel.set(lifelnbIO.getLifcnum());
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(sv.lifesel);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

	/**
	* <pre>
	* Read the HPAD record to retrieve the changes make in            
	* work with proposal                                              
	* </pre>
	*/
protected void readHpad3306()
	{
		hpadIO.setParams(SPACES);
		hpadIO.setChdrcoy(wsspcomn.company);
		hpadIO.setChdrnum(sv.chdrnum);
		hpadIO.setFormat(formatsInner.hpadrec);
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)
		&& isNE(hpadIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hpadIO.getParams());
			fatalError600();
		}
		if (isEQ(hpadIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		sv.dlvrmode.set(hpadIO.getDlvrmode());
	}

protected void retrvChdrlnb3400()
	{
		retrv3400();
	}

protected void retrv3400()
	{
		/* Retrieve contract fields from I/O module*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/* Store Contract fields from I/O module                           */
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		chdrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		/* Update Contract with details from I/O module                    */
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		chdrlnbIO.setFunction(varcom.writs);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		try {
			nextProgram4000();
			checkSelections4050();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void nextProgram4000()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		wsspcomn.nextprog.set(wsaaProg);
		wsaaFirstTime.set("N");
		/*  If first time into this section (stack action blank)*/
		/*  save next eight programs in stack*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				saveProgram4100();
			}
		}
		if (isEQ(sv.ddind, "X")) {
			gensswrec.function.set("C");
			sv.ddind.set("?");
			sv.occdateOut[varcom.pr.toInt()].set("Y");
			keepsPayr4600();
			callGenssw4300();
			keepsChdrlnb4700();
			goTo(GotoLabel.exit4009);
		}
		if (isEQ(sv.crcind, "X")) {
			gensswrec.function.set("E");
			sv.crcind.set("?");
			sv.occdateOut[varcom.pr.toInt()].set("Y");
			keepsPayr4600();
			callGenssw4300();
			keepsChdrlnb4700();
			goTo(GotoLabel.exit4009);
		}
		if (isEQ(sv.grpind, "X")) {
			gensswrec.function.set("D");
			sv.grpind.set("?");
			sv.occdateOut[varcom.pr.toInt()].set("Y");
			keepsPayr4600();
			callGenssw4300();
			keepsChdrlnb4700();
			goTo(GotoLabel.exit4009);
		}
		wsaaRelockChdrlnb.set("N");
	}

protected void checkSelections4050()
	{
		if (isEQ(sv.adjind, "X")) {
			wsaaAdjustment.set("Y");
			wsaaSkipApCash.set("Y");
			wsaaRelockChdrlnb.set("Y");
			sv.adjind.set(SPACES);
			gensswrec.function.set("A");
			callGenssw4300();
			keepsChdrlnb4700();
			return ;
		}
		if (isEQ(sv.apcind, "X")) {
			sv.apcind.set(" ");
			wsaaApplyCash.set("S");
			wsaaSkipApCash.set("Y");
			gensswrec.function.set("B");
			callGenssw4300();
			keepsChdrlnb4700();
			return ;
		}
		if (isNE(sv.occdate, sv.billcd)
		&& isEQ(wsaaApplyCash, "N")
		&& notSkipApCash.isTrue()) {
			sv.apcind.set(" ");
			wsaaApplyCash.set("C");
			gensswrec.function.set("B");
			callGenssw4300();
			keepsChdrlnb4700();
			return ;
		}
		if (isEQ(sv.occdate, sv.billcd)) {
			if (isEQ(chdrlnbIO.getBillfreq(), "00")
			&& isEQ(wsaaApplyCash, "N")
			&& notSkipApCash.isTrue()) {
				sv.apcind.set(" ");
				wsaaApplyCash.set("C");
				gensswrec.function.set("B");
				callGenssw4300();
				keepsChdrlnb4700();
				return ;
			}
		}
		wsaaSkipApCash.set("N");
		/*   No more selected (or none)*/
		/*      - restore stack form wsaa to wssp*/
		wsaaX.set(wsspcomn.programPtr);
		wsaaY.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			restoreProgram4200();
		}
		/*  If current stack action is * then re-display screen*/
		/*     (in this case, some other option(s) were requested)*/
		/*  Otherwise continue as normal.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")
		&& !applyCashCont.isTrue()
		&& insufficientCash.isTrue()) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
			return ;
		}
		/*  Check if apply cash selected previously*/
		if (insufficientCash.isTrue()) {
			chkContractSuspense4500();
			compute(wsaaAmtDue, 2).set(add(add(sub(add(wsaaPremReqd, wsaaCntfeeReqd), wsaaToleranceTot), wsaaPremTaxReqd), wsaaCntfeeTaxReqd));
			if (isGT(wsaaAmtDue, wsaaSuspenseTot)) {
				compute(wsaaAmtDue, 2).set(sub(add(wsaaPremReqd, wsaaCntfeeReqd), wsaaToleranceTot2));
				if (isGT(wsaaAmtDue, wsaaSuspenseTot)) {
					sv.apcindErr.set(errorsInner.h030);
					wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
					wsspcomn.nextprog.set(scrnparams.scrname);
					return ;
				}
				else {
					wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
					sv.apcind.set("+");
					wsaaInsufficientCash.set("N");
				}
			}
			else {
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
				sv.apcind.set("+");
				wsaaInsufficientCash.set("N");
			}
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.programPtr.add(1);
		wsaaFirstTime.set("Y");
		wsspcomn.flag.set("Z");
	}

protected void saveProgram4100()
	{
		/*SAVE*/
		wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void restoreProgram4200()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void callGenssw4300()
	{
		callSubroutine4310();
	}

protected void callSubroutine4310()
	{
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.edterror.set("Y");
			wsspcomn.nextprog.set(scrnparams.scrname);
			return ;
		}
		/*    load from gensw to wssp*/
		compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
		wsaaY.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgram4400();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void loadProgram4400()
	{
		/*LOAD*/
		wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void chkContractSuspense4500()
	{
		try {
			readT56454500();
			readT36954501();
			readSubAccount4502();
			adjustPremium4503();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	*    Read  the sub account balance for  the contract and apply
	*    the sign for the sub account type.
	* </pre>
	*/
protected void readT56454500()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void readT36954501()
	{
		/*    Read the table T3695 for the field sign of the 01 posting.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t3695);
		itemIO.setItemitem(t5645rec.sacstype01);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sv.prmamtErr.set(errorsInner.g228);
			/*     MOVE G228               TO S5106-PREM-ERR                */
			sv.prmamtOut[varcom.ri.toInt()].set("Y");
			/*     MOVE 'Y'                TO S5106-PREM-OUT(RI)            */
			goTo(GotoLabel.exit4509);
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
	}

protected void readSubAccount4502()
	{
		acblIO.setDataArea(SPACES);
		acblIO.setRldgacct(ZERO);
		acblIO.setRldgcoy(wsspcomn.company);
		acblIO.setRldgacct(chdrlnbIO.getChdrnum());
		acblIO.setOrigcurr(chdrlnbIO.getCntcurr());
		acblIO.setSacscode(t5645rec.sacscode01);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFormat(formatsInner.acblrec);
		acblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(), varcom.oK)
		&& isNE(acblIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			fatalError600();
		}
		if (isEQ(acblIO.getStatuz(), varcom.mrnf)) {
			wsaaSuspenseTot.set(ZERO);
		}
		else {
			if (isEQ(t3695rec.sign, "-")) {
				compute(wsaaSuspenseTot, 2).set(mult(acblIO.getSacscurbal(), -1));
			}
			else {
				wsaaSuspenseTot.set(acblIO.getSacscurbal());
			}
		}
	}

protected void adjustPremium4503()
	{
		if (singlePremium.isTrue()) {
			wsaaPremReqd.set(wsaaPremTot);
		}
		else {
			calcPremiumDue4530();
		}
		/*CALC-CONTRACT-FEE*/
		/*    Calculate the contract fee by using the subroutine found in*/
		/*    table T5674.*/
		if (isNE(t5688rec.feemeth, SPACES)) {
			calcPolicyFee4510();
		}
		else {
			wsaaCntfeeReqd.set(ZERO);
		}
		/*CALC-TOLERANCE*/
		calcTolerance4520();
	}

protected void calcPolicyFee4510()
	{
		readT56744500();
	}

protected void readT56744500()
	{
		/*    Reference T5674 to obtain the subroutine required to work*/
		/*    out the Fee amount by the correct method.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5674);
		itemIO.setItemitem(t5688rec.feemeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sv.prmamtErr.set(errorsInner.f151);
			/*     MOVE F151               TO S5106-PREM-ERR                */
			sv.prmamtOut[varcom.ri.toInt()].set("Y");
			/*     MOVE 'Y'                TO S5106-PREM-OUT(RI)            */
			return ;
		}
		t5674rec.t5674Rec.set(itemIO.getGenarea());
		/* Check subroutine NOT = SPACES before attempting call.*/
		if (isEQ(t5674rec.commsubr, SPACES)) {
			return ;
		}
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(chdrlnbIO.getCnttype());
		mgfeelrec.billfreq.set(sv.billfreq);
		/* MOVE CHDRLNB-OCCDATE        TO MGFL-EFFDATE.                 */
		mgfeelrec.effdate.set(sv.occdate);
		/* MOVE CHDRLNB-CNTCURR        TO MGFL-CNTCURR.                 */
		mgfeelrec.cntcurr.set(t5705rec.cntcurr);
		mgfeelrec.company.set(wsspcomn.company);
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz, varcom.oK)
		&& isNE(mgfeelrec.statuz, varcom.endp)) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			fatalError600();
		}
		wsaaCntfee.set(mgfeelrec.mgfee);
		compute(wsaaCntfeeReqd, 2).set(mult(wsaaCntfee, wsaaFactor));
		/* Calculate contract fee tax                                      */
		checkCalcContTax7100();
		compute(wsaaCntfeeTaxReqd, 3).setRounded(mult(wsaaCntfeeTax, wsaaFactor));
	}

protected void calcTolerance4520()
	{
		readT56674520();
	}

	/**
	* <pre>
	*  Look up tolerance applicable
	* </pre>
	*/
protected void readT56674520()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5667);
		wsaaT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		wsaaT5667Curr.set(chdrlnbIO.getCntcurr());
		itemIO.setItemitem(wsaaT5667Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaToleranceTot.set(ZERO);
			return ;
		}
		t5667rec.t5667Rec.set(itemIO.getGenarea());
		wsaaToleranceApp.set(ZERO);
		wsaaAmountLimit.set(ZERO);
		wsaaSub.set(1);
		while ( !(isGT(wsaaSub, 11))) {
			if (isEQ(sv.billfreq, t5667rec.freq[wsaaSub.toInt()])) {
				wsaaToleranceApp.set(t5667rec.prmtol[wsaaSub.toInt()]);
				wsaaToleranceApp2.set(t5667rec.prmtoln[wsaaSub.toInt()]);
				wsaaAmountLimit.set(t5667rec.maxAmount[wsaaSub.toInt()]);
				wsaaAmountLimit2.set(t5667rec.maxamt[wsaaSub.toInt()]);
				wsaaSub.set(11);
			}
			wsaaSub.add(1);
		}
		
		/* Calculate the  tolerance applicable.*/
		compute(wsaaCalcTolerance, 2).set(div((mult(wsaaToleranceApp, wsaaAmtDue)), 100));
		/*    Check % amount is less than Limit on T5667, if so use this*/
		/*    else use Limit.*/
		if (isLT(wsaaCalcTolerance, wsaaAmountLimit)) {
			wsaaToleranceTot.set(wsaaCalcTolerance);
		}
		else {
			wsaaToleranceTot.set(wsaaAmountLimit);
		}
		/*    Calculate of 2nd premium shortfall tolerance limit.          */
		wsaaToleranceTot2.set(ZERO);
		if (isGT(wsaaAmountLimit2, ZERO)) {
			compute(wsaaCalcTolerance2, 2).set(div((mult(wsaaToleranceApp2, wsaaAmtDue)), 100));
			if (isLT(wsaaCalcTolerance2, wsaaAmountLimit2)) {
				wsaaToleranceTot2.set(wsaaCalcTolerance2);
			}
			else {
				wsaaToleranceTot2.set(wsaaAmountLimit2);
			}
		}
	}

protected void calcPremiumDue4530()
	{
		/*ADJUST-PREMIUM*/
		/*    Get the frequency Factor from DATCON3.*/
		datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
		datcon3rec.intDate2.set(chdrlnbIO.getBtdate());
		datcon3rec.frequency.set(chdrlnbIO.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		wsaaFactor.set(datcon3rec.freqFactor);
		/*    Calculate the instalment premium.*/
		compute(wsaaPremReqd, 2).set(mult(wsaaPremTot, wsaaFactor));
		/*    Calculate the instalment premium tax.                        */
		compute(wsaaPremTaxReqd, 2).set(mult(wsaaPremTax, wsaaFactor));
		/*EXIT*/
	}

protected void keepsPayr4600()
	{
		/*KEEPS*/
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(sv.chdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFunction(varcom.keeps);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void keepsChdrlnb4700()
	{
		/*KEEPS*/
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		chdrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void validateRiskCommDate5000()
	{
		validateRegister5000();
	}

protected void validateRegister5000()
	{
		wsaaOccdateInvd = "N";
		if (isEQ(sv.occdate, varcom.vrcmMaxDate)) {
			sv.occdateErr.set(errorsInner.e186);
			sv.occdateOut[varcom.ri.toInt()].set("Y");
			wsaaOccdateInvd = "Y";
			return ;
		}
		if (isNE(sv.occdateOut[varcom.chg.toInt()], "Y")) {
			return ;
		}
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(sv.occdate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			sv.occdateErr.set(errorsInner.e032);
			sv.occdateOut[varcom.ri.toInt()].set("Y");
			wsaaOccdateInvd = "Y";
			return ;
		}
		if (isLT(sv.occdate, wsaa3MonthsAgo)) {
			sv.occdateErr.set(errorsInner.e480);
		}
	}

protected void validateDefaultInput5100()
	{
		validateRegister5100();
		validateIncomeSource5102();
		validateCrossRef5103();
		chkCurrency5104();
	}

protected void validateRegister5100()
	{
		if (isEQ(sv.register, SPACES)) {
			sv.registerErr.set(errorsInner.e186);
			sv.registerOut[varcom.ri.toInt()].set("Y");
		}
		/*VALIDATE-BUSINESS-SOURCE*/
		if (isEQ(sv.srcebus, SPACES)) {
			sv.srcebusErr.set(errorsInner.e186);
			sv.srcebusOut[varcom.ri.toInt()].set("Y");
		}
	}

	/**
	* <pre>
	* Validate the source of income field if entered on T5705.
	* </pre>
	*/
protected void validateIncomeSource5102()
	{
		if (isNE(t5705rec.incomeSeqNo, ZERO)) {
			soinIO.setDataKey(SPACES);
			soinIO.setClntcoy(wsspcomn.fsuco);
			soinIO.setClntnum(sv.ownersel);
			soinIO.setIncomeSeqNo(t5705rec.incomeSeqNo);
			soinIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, soinIO);
			if (isNE(soinIO.getStatuz(), varcom.oK)
			&& isNE(soinIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(soinIO.getParams());
				fatalError600();
			}
			if (isEQ(soinIO.getStatuz(), varcom.mrnf)) {
				scrnparams.errorCode.set(errorsInner.i068);
				wsspcomn.edterror.set("Y");
			}
		}
	}

	/**
	* <pre>
	*  Validate cross reference number
	* </pre>
	*/
protected void validateCrossRef5103()
	{
		if (isEQ(t5705rec.lrepnum, SPACES)
		&& isNE(t5705rec.reptype, SPACES)) {
			scrnparams.errorCode.set(errorsInner.t030);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(t5705rec.lrepnum, SPACES)
		&& isEQ(t5705rec.reptype, SPACES)) {
			scrnparams.errorCode.set(errorsInner.t030);
			wsspcomn.edterror.set("Y");
		}
		/*  A cross reference type of CH has a special meaning*/
		/*    - validate the reference field against an existing contract.*/
		if (isEQ(t5705rec.reptype, "CH")
		&& isNE(t5705rec.lrepnum, SPACES)) {
			checkOldContract5110();
		}
	}

protected void chkCurrency5104()
	{
		if (isEQ(t5705rec.cntcurr, SPACES)) {
			scrnparams.errorCode.set(errorsInner.t030);
			wsspcomn.edterror.set("Y");
			wsaaError.set("Y");
		}
		else {
			if (isEQ(t5705rec.billcurr, SPACES)) {
				t5705rec.billcurr.set(t5705rec.cntcurr);
			}
		}
		/*VALIDATE-PLANSUFF*/
		if (isLT(t5705rec.plansuff, 1)
		|| isLT(t5705rec.plansuff, t5688rec.polmin)
		|| isGT(t5705rec.plansuff, t5688rec.polmax)) {
			scrnparams.errorCode.set(errorsInner.t033);
			wsspcomn.edterror.set("Y");
			wsaaError.set("Y");
		}
		/*EXIT*/
	}

protected void checkOldContract5110()
	{
		keepsHeader5110();
	}

protected void keepsHeader5110()
	{
		/*  Save current contract details.*/
		chdrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/*  Read old contract.*/
		chdrlnbIO.setDataKey(SPACES);
		chdrlnbIO.setChdrcoy(wsspcomn.company);
		chdrlnbIO.setChdrnum(t5705rec.lrepnum);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrlnbIO.getStatuz(), varcom.endp)) {
			sv.chdrnumErr.set(errorsInner.f849);
			sv.chdrnumOut[varcom.ri.toInt()].set("Y");
		}
		/*  Retrieve current contract details.*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}

protected void validateAgentOwner5200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validateAgent5200();
				case validateOwner5205: 
					validateOwner5205();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateAgent5200()
	{
		if (isEQ(sv.agntsel, SPACES)) {
			sv.agntselErr.set(errorsInner.e186);
			sv.agntselOut[varcom.ri.toInt()].set("Y");
			goTo(GotoLabel.validateOwner5205);
		}
		aglflnbIO.setDataKey(SPACES);
		aglflnbIO.setAgntcoy(wsspcomn.company);
		aglflnbIO.setAgntnum(sv.agntsel);
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(), varcom.oK)
		&& isNE(aglflnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(aglflnbIO.getParams());
			fatalError600();
		}
		if (isEQ(aglflnbIO.getStatuz(), varcom.mrnf)) {
			sv.agntselErr.set(errorsInner.e305);
			sv.agntname.set(SPACES);
			goTo(GotoLabel.validateOwner5205);
		}
		/* Agency must be current agent.*/
		if (isLT(aglflnbIO.getDtetrm(), sv.occdate)
		|| isLT(aglflnbIO.getDteexp(), sv.occdate)
		|| isGT(aglflnbIO.getDteapp(), sv.occdate)) {
			sv.agntselErr.set(errorsInner.e475);
			sv.agntselOut[varcom.ri.toInt()].set("Y");
			goTo(GotoLabel.validateOwner5205);
		}
		/* Agency branch must be a contract branch.*/
		if (isNE(aglflnbIO.getAgntbr(), sv.cntbranch)) {
			sv.agntselErr.set(errorsInner.e455);
			sv.agntselOut[varcom.ri.toInt()].set("Y");
			goTo(GotoLabel.validateOwner5205);
		}
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(aglflnbIO.getClntnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.agntname.set(wsspcomn.longconfname);
	}

protected void validateOwner5205()
	{
		if (isEQ(sv.ownersel, SPACES)) {
			sv.ownerselErr.set(errorsInner.e186);
			sv.ownerselOut[varcom.ri.toInt()].set("Y");
			return ;
		}
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(sv.ownersel);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), "1")) {
			sv.ownerselErr.set(errorsInner.e304);
			sv.ownerselOut[varcom.ri.toInt()].set("Y");
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
			/*  Is the owner dead?*/
			if (isLTE(cltsIO.getCltdod(), sv.occdate)) {
				sv.ownerselErr.set(errorsInner.f782);
				sv.ownerselOut[varcom.ri.toInt()].set("Y");
			}
		}
	}

protected void validateGroupMandate5300()
	{
		try {
			readTableT36205300();
			validateGroup5310();
			validateMandate5320();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	*5300-READ-TABLE-T3625.                                           
	**** MOVE SPACES                 TO ITEM-DATA-KEY.                
	**** MOVE 'IT'                   TO ITEM-ITEMPFX.                 
	**** MOVE WSSP-COMPANY           TO ITEM-ITEMCOY.                 
	**** MOVE T3625                  TO ITEM-ITEMTABL.                
	**** STRING 'LP'                                                  
	****        S5106-MOP                                             
	****        S5106-BILLFREQ       DELIMITED SIZE                   
	****                             INTO ITEM-ITEMITEM.              
	**** MOVE READR                  TO ITEM-FUNCTION.                
	**** CALL 'ITEMIO'               USING ITEM-PARAMS.               
	**** IF ITEM-STATUZ              NOT = O-K                        
	**** AND ITEM-STATUZ             NOT = MRNF                       
	****     MOVE ITEM-PARAMS        TO SYSR-PARAMS                   
	****     MOVE ITEM-STATUZ        TO SYSR-STATUZ                   
	****     PERFORM 600-FATAL-ERROR.                                 
	**** IF ITEM-STATUZ              = MRNF                           
	****     MOVE H051               TO S5106-MOP-ERR                 
	****                                S5106-BILLFREQ-ERR            
	****     GO TO                   5399-EXIT.                       
	**** MOVE ITEM-GENAREA           TO T3625-T3625-REC.              
	* </pre>
	*/
protected void readTableT36205300()
	{
		/* Read Table T3620 for the Bank Details Required and the          */
		/* Group Details Required.                                         */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t3620);
		itemIO.setItemitem(sv.mop);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sv.mopErr.set(errorsInner.e398);
			goTo(GotoLabel.exit5399);
		}
		t3620rec.t3620Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	* Group details only allowed for certain types of billing
	* If they are allowed, they are mandatory.
	* </pre>
	*/
protected void validateGroup5310()
	{
		/* IF T3625-GRPIND             = 'Y'                            */
		if (isNE(t3620rec.grpind, SPACES)
		&& isEQ(chdrlnbIO.getGrupkey(), SPACES)) {
			sv.grpind.set("X");
		}
		/* IF T3625-GRPIND             NOT = 'Y'                        */
		if (isEQ(t3620rec.grpind, SPACES)) {
			sv.grpind.set(SPACES);
			if (isNE(chdrlnbIO.getGrupkey(), SPACES)) {
				chdrlnbIO.setGrupkey(SPACES);
				payrIO.setGrupkey(SPACES);
				chdrlnbIO.setMembsel(SPACES);
			}
		}
	}

	/**
	* <pre>
	* Bank details only allowed for certain types of billing
	* If they are allowed, they are mandatory.
	* </pre>
	*/
protected void validateMandate5320()
	{
		wsaaChnlChk.set(sv.mop);
		/* IF T3625-DDIND              = 'Y'                            */
		if (isNE(t3620rec.ddind, SPACES)
		&& isEQ(chdrlnbIO.getMandref(), SPACES)) {
			sv.ddind.set("X");
		}
		/* IF T3625-DDIND              NOT = 'Y'                        */
		if (isEQ(t3620rec.ddind, SPACES)
		&& directDebit.isTrue()) {
			chdrlnbIO.setMandref(SPACES);
			payrIO.setMandref(SPACES);
			chdrlnbIO.setBankkey(SPACES);
			chdrlnbIO.setBankacckey(SPACES);
			sv.ddind.set(SPACES);
		}
		/* Credit Card details only allowed for certain types of billing   */
		/* If they are allowed, they are mandatory.                        */
		if (isNE(t3620rec.crcind, SPACES)
		&& isEQ(chdrlnbIO.getMandref(), SPACES)) {
			sv.crcind.set("X");
		}
		if (isEQ(t3620rec.crcind, SPACES)
		&& creditCard.isTrue()) {
			chdrlnbIO.setMandref(SPACES);
			payrIO.setMandref(SPACES);
			chdrlnbIO.setBankkey(SPACES);
			chdrlnbIO.setBankacckey(SPACES);
			sv.crcind.set(SPACES);
		}
	}

protected void createOwnerRole5330()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					chkExistingOwner5330();
				case writeOwnerRole5335: 
					writeOwnerRole5335();
				case exit5339: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void chkExistingOwner5330()
	{
		if (isEQ(wsaaOwnersel, SPACES)
		&& isNE(sv.ownersel, SPACES)) {
			wsaaOwnersel.set(sv.ownersel);
			goTo(GotoLabel.writeOwnerRole5335);
		}
		if (isEQ(sv.ownersel, wsaaOwnersel)) {
			goTo(GotoLabel.exit5339);
		}
		cltrelnrec.data.set(SPACES);
		cltrelnrec.forecoy.set(wsspcomn.company);
		cltrelnrec.forenum.set(sv.chdrnum);
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.clrrrole.set("OW");
		cltrelnrec.clntnum.set(wsaaOwnersel);
		cltrelnrec.function.set("DEL  ");
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.params.set(cltrelnrec.cltrelnRec);
			syserrrec.statuz.set(cltrelnrec.statuz);
			fatalError600();
		}
		wsaaOwnersel.set(sv.ownersel);
	}

	/**
	* <pre>
	* Create a new client owner role record.
	* </pre>
	*/
protected void writeOwnerRole5335()
	{
		cltrelnrec.cltrelnRec.set(SPACES);
		cltrelnrec.forecoy.set(wsspcomn.company);
		cltrelnrec.forenum.set(sv.chdrnum);
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.clrrrole.set("OW");
		cltrelnrec.clntnum.set(sv.ownersel);
		cltrelnrec.function.set("ADD  ");
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(cltrelnrec.statuz);
			fatalError600();
		}
	}

protected void validateMopFreq5400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialize5400();
				case loop5402: 
					loop5402();
				case checkDay5406: 
					checkDay5406();
				case exit5409: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialize5400()
	{
		wsaaSub.set(ZERO);
		wsaaMopFreqOk = "N";
		wsaaBillcdOk = "N";
		readTableT56892150();
	}

protected void loop5402()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 30)
		|| isEQ(wsaaBillcdOk, "Y")) {
			/*    GO TO                   2429-EXIT.               <D509CS>*/
			goTo(GotoLabel.exit5409);
		}
		/*CHECKING*/
		if (isEQ(t5689rec.billfreq[wsaaSub.toInt()], sv.billfreq)
		&& isEQ(t5689rec.mop[wsaaSub.toInt()], sv.mop)) {
			wsaaMopFreqOk = "Y";
			goTo(GotoLabel.checkDay5406);
		}
		goTo(GotoLabel.loop5402);
	}

protected void checkDay5406()
	{
		if (isEQ(wsaaDefaultBillcd, "Y")) {
			wsaaRendd.set(t5689rec.rendd[wsaaSub.toInt()]);
			wsaaBillcdOk = "Y";
		}
		else {
			if (isEQ(t5689rec.rendd[wsaaSub.toInt()], "99")
			|| isEQ(t5689rec.rendd[wsaaSub.toInt()], wsaaRendd)) {
				wsaaBillcdOk = "Y";
			}
			else {
				if (isEQ(wsaaEndOfMonth, "Y")) {
					if (isEQ(t5689rec.rendd[wsaaSub.toInt()], "31")) {
						wsaaBillcdOk = "Y";
					}
					else {
						if (isEQ(t5689rec.rendd[wsaaSub.toInt()], "29")
						|| isEQ(t5689rec.rendd[wsaaSub.toInt()], "30")
						&& isEQ(wsaaTempMm, 2)) {
							wsaaBillcdOk = "Y";
						}
					}
				}
			}
		}
		goTo(GotoLabel.loop5402);
	}

protected void validateAmounts6000()
	{
		try {
			validateUnitpkgT57046000();
			validateComponents6003();
			validateCalcBenefits6004();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateUnitpkgT57046000()
	{
		/* IF T5704-UNITPKG            < 1                              */
		/*     MOVE T061               TO SCRN-ERROR-CODE               */
		/*     MOVE 'Y'                TO WSSP-EDTERROR                 */
		/*                                WSAA-ERROR.                   */
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 10)); wsaaIndex.add(1)){
			if ((isNE(t5704rec.crtable[wsaaIndex.toInt()], SPACES)
			|| isNE(t5704rec.rtable[wsaaIndex.toInt()], SPACES))) {
				if (isLT(t5704rec.unitpkg[wsaaIndex.toInt()], 1)) {
					scrnparams.errorCode.set(errorsInner.t061);
					wsspcomn.edterror.set("Y");
					wsaaError.set("Y");
					wsaaIndex.set(11);
				}
			}
		}
		if (isLT(t5704rec.mbaseunt, 1)) {
			scrnparams.errorCode.set(errorsInner.t061);
			wsspcomn.edterror.set("Y");
			wsaaError.set("Y");
		}
		if (isLT(t5704rec.mbaseinc, 1)) {
			scrnparams.errorCode.set(errorsInner.t061);
			wsspcomn.edterror.set("Y");
			wsaaError.set("Y");
		}
	}

protected void validateComponents6003()
	{
		if (isEQ(t5704rec.crtable01, SPACES)) {
			scrnparams.errorCode.set(errorsInner.t028);
			wsspcomn.edterror.set("Y");
			wsaaError.set("Y");
			goTo(GotoLabel.exit6009);
		}
		wsaaSub.set(10);
		wsaaSkipComplete.set("N");
		while ( !(skipComplete.isTrue()
		|| isLT(wsaaSub, 1))) {
			skipBlankEntries6100();
		}
		
		compute(wsaaLastEntryno, 0).set(add(wsaaSub, 1));
		wsaaSub.set(ZERO);
		while ( !(isGT(wsaaSub, wsaaLastEntryno)
		|| errorFound.isTrue())) {
			validateEntries6200();
		}
		
	}

protected void validateCalcBenefits6004()
	{
		if (errorFound.isTrue()) {
			return ;
		}
		wsaaSub.set(1);
		while ( !(isGT(wsaaSub, 20))) {
			wsbbCovttrmInner.wsbbSumin[wsaaSub.toInt()].set(ZERO);
			wsbbCovttrmInner.wsbbSingp[wsaaSub.toInt()].set(ZERO);
			wsbbCovttrmInner.wsbbInstprem[wsaaSub.toInt()].set(ZERO);
			wsbbCovttrmInner.wsbbZbinstprem[wsaaSub.toInt()].set(ZERO);
			wsbbCovttrmInner.wsbbZlinstprem[wsaaSub.toInt()].set(ZERO);
			wsaaSub.add(1);
		}
		
		wsaaSub.set(1);
		wsaaSubR.set(1);
		wsaaSubC.set(ZERO);
		wsaaPremTot.set(ZERO);
		wsaaSuminTot.set(ZERO);
		wsaaPremTax.set(ZERO);
		while ( !(isGT(wsaaSub, wsaaLastEntryno)
		|| errorFound.isTrue())) {
			validateCalcBenefits6300();
		}
		
		if (isNE(t5688rec.feemeth, SPACES)) {
			calcPolicyFee4510();
		}
		compute(wsaaTaxamt, 3).setRounded(add(add(add(wsaaPremTot, wsaaPremTax), wsaaCntfee), wsaaCntfeeTax));
		if (isGT(wsaaTaxamt, ZERO)) {
			sv.taxamt.set(wsaaTaxamt);
		}
	}

protected void skipBlankEntries6100()
	{
		/*SKIP-ENTRIES*/
		if (isNE(t5704rec.crtable[wsaaSub.toInt()], SPACES)
		|| isNE(t5704rec.rtable[wsaaSub.toInt()], SPACES)
		|| isNE(t5704rec.unitpct[wsaaSub.toInt()], ZERO)) {
			wsaaSkipComplete.set("Y");
		}
		wsaaSub.subtract(1);
		/*EXIT*/
	}

protected void validateEntries6200()
	{
		try {
			initailize6200();
			chkRiders6202();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void initailize6200()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, wsaaLastEntryno)) {
			goTo(GotoLabel.exit6209);
		}
	}

protected void chkRiders6202()
	{
		if (isEQ(t5704rec.rtable[wsaaSub.toInt()], SPACES)) {
			if (isEQ(t5704rec.crtable[wsaaSub.toInt()], SPACES)) {
				scrnparams.errorCode.set(errorsInner.t028);
				wsspcomn.edterror.set("Y");
				wsaaError.set("Y");
			}
		}
		else {
			if (isNE(t5704rec.crtable[wsaaSub.toInt()], SPACES)) {
				scrnparams.errorCode.set(errorsInner.t028);
				wsspcomn.edterror.set("Y");
				wsaaError.set("Y");
			}
		}
	}

protected void validateCalcBenefits6300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validateT57046300();
				case loop6302: 
					loop6302();
				case checkTermFields6303: 
					checkTermFields6303();
				case continue6305: 
					continue6305();
				case exit6309: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateT57046300()
	{
		if (isNE(t5704rec.crtable[wsaaSub.toInt()], SPACES)) {
			wsaaSubC.add(1);
			wsaaCoverage.set(wsaaCovNo[wsaaSubC.toInt()]);
			wsaaSubR.set(1);
			wsaaRider.set(wsaaRidNo[wsaaSubR.toInt()]);
			wsaaCrtable.set(t5704rec.crtable[wsaaSub.toInt()]);
		}
		else {
			wsaaSubR.add(1);
			wsaaRider.set(wsaaRidNo[wsaaSubR.toInt()]);
			wsaaCrtable.set(t5704rec.rtable[wsaaSub.toInt()]);
		}
		validateT57036400();
		if (errorFound.isTrue()) {
			goTo(GotoLabel.exit6309);
		}
		/* Determine which edit rule table to use                       */
		/* T5606 - Regular Benefit Edit Rules                           */
		/* T5608 - Standard Coverage Edit Rules                         */
		/* TH505 - Cash Dividend Edit Rules                             */
		/* T5551 - Unit Linked Edit Rules                               */
		wsaaItemFound.set("N");
		readT56712030();
		readTableT56082130();
		if (itemNotFound.isTrue()) {
			readTableT56062130();
			if (itemNotFound.isTrue()) {
				readTableTh5052130();
				if (itemNotFound.isTrue()) {
					readTableT55512130();
					if (itemNotFound.isTrue()) {
						scrnparams.errorCode.set(errorsInner.f718);
						wsspcomn.edterror.set("Y");
						wsaaError.set("Y");
					}
				}
			}
		}
		if (errorFound.isTrue()) {
			goTo(GotoLabel.exit6309);
		}
		calcRiskCessDate6500();
		if (errorFound.isTrue()) {
			goTo(GotoLabel.exit6309);
		}
		calcPremCessDate6600();
		if (errorFound.isTrue()) {
			goTo(GotoLabel.exit6309);
		}
		chkFv6700();
		if (errorFound.isTrue()) {
			goTo(GotoLabel.exit6309);
		}
		/* IF T5704-SINSBSD            = 'Y'                            */
		if (isEQ(t5704rec.sinsbsd[wsaaSub.toInt()], "Y")) {
			calcSumAssuredBase6800();
		}
		else {
			calcPremiumBase6900();
		}
		checkCalcCompTax7000();
		/* Change Reference T5608                                       */
		/* IF WSAA-SUMIN               < T5608-SUM-INS-MIN              */
		if (isLT(wsaaSumin, wsaaEditRuleItemsInner.wsaaSumInsMin)) {
			scrnparams.errorCode.set(errorsInner.e416);
			wsspcomn.edterror.set("Y");
			wsaaError.set("Y");
		}
		/* IF WSAA-SUMIN               > T5608-SUM-INS-MAX              */
		if (isGT(wsaaSumin, wsaaEditRuleItemsInner.wsaaSumInsMax)) {
			scrnparams.errorCode.set(errorsInner.e417);
			wsspcomn.edterror.set("Y");
			wsaaError.set("Y");
		}
		if (isLT(wsaaRiskCessDate, wsaaPremCessDate)) {
			scrnparams.errorCode.set(errorsInner.e566);
			wsspcomn.edterror.set("Y");
			wsaaError.set("Y");
		}
		if (errorFound.isTrue()) {
			goTo(GotoLabel.exit6309);
		}
		/*  Calculate cessasion age and term.*/
		datcon3rec.intDate1.set(lifelnbIO.getCltdob());
		datcon3rec.intDate2.set(wsaaRiskCessDate);
		callDatcon36310();
		wsaaRiskCessAge.set(datcon3rec.freqFactor);
		datcon3rec.intDate2.set(wsaaPremCessDate);
		callDatcon36310();
		wsaaPremCessAge.set(datcon3rec.freqFactor);
		datcon3rec.intDate1.set(sv.occdate);
		datcon3rec.intDate2.set(wsaaRiskCessDate);
		callDatcon36310();
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		datcon3rec.intDate2.set(wsaaPremCessDate);
		callDatcon36310();
		wsaaPremCessTerm.set(datcon3rec.freqFactor);
		wsaaSub1.set(ZERO);
		wsaaRcessageErr.set("Y");
		wsaaPcessageErr.set("Y");
		wsaaRcesstrmErr.set("Y");
		wsaaPcesstrmErr.set("Y");
	}

protected void loop6302()
	{
		wsaaSub1.add(1);
		if (isGT(wsaaSub1, wsaaMaxOcc)) {
			goTo(GotoLabel.continue6305);
		}
		/* Change Reference T5608                                       */
		/* IF (T5608-AGE-ISSAGE-FRM(WSAA-SUB1) = ZEROES                 */
		/* AND T5608-AGE-ISSAGE-TO(WSAA-SUB1)  = ZEROES)                */
		/* OR T5608-AGE-ISSAGE-FRM(WSAA-SUB1)  > S5106-ANBAGE           */
		/* OR T5608-AGE-ISSAGE-TO(WSAA-SUB1)   < S5106-ANBAGE           */
		if ((isEQ(wsaaEditRuleItemsInner.wsaaAgeIssageFrm[wsaaSub1.toInt()], ZERO)
		&& isEQ(wsaaEditRuleItemsInner.wsaaAgeIssageTo[wsaaSub1.toInt()], ZERO))
		|| isGT(wsaaEditRuleItemsInner.wsaaAgeIssageFrm[wsaaSub1.toInt()], sv.anbage)
		|| isLT(wsaaEditRuleItemsInner.wsaaAgeIssageTo[wsaaSub1.toInt()], sv.anbage)) {
			goTo(GotoLabel.checkTermFields6303);
		}
		if (isGTE(wsaaRiskCessAge, wsaaEditRuleItemsInner.wsaaRiskCessageFrom[wsaaSub1.toInt()])
		&& isLTE(wsaaRiskCessAge, wsaaEditRuleItemsInner.wsaaRiskCessageTo[wsaaSub1.toInt()])) {
			/*                           T5608-RISK-CESSAGE-TO(WSAA-SUB1)   */
			wsaaRcessageErr.set(SPACES);
		}
		if (isGTE(wsaaPremCessAge, wsaaEditRuleItemsInner.wsaaPremCessageFrom[wsaaSub1.toInt()])
		&& isLTE(wsaaPremCessAge, wsaaEditRuleItemsInner.wsaaPremCessageTo[wsaaSub1.toInt()])) {
			/*                           T5608-PREM-CESSAGE-TO(WSAA-SUB1)   */
			wsaaPcessageErr.set(SPACES);
		}
	}

protected void checkTermFields6303()
	{
		/* IF (T5608-TERM-ISSAGE-FRM(WSAA-SUB1) = ZEROES                */
		/* AND T5608-TERM-ISSAGE-TO (WSAA-SUB1) = ZEROES)               */
		/* OR S5106-ANBAGE           < T5608-TERM-ISSAGE-FRM(WSAA-SUB1) */
		/* OR S5106-ANBAGE           > T5608-TERM-ISSAGE-TO(WSAA-SUB1)  */
		if ((isEQ(wsaaEditRuleItemsInner.wsaaTermIssageFrm[wsaaSub1.toInt()], ZERO)
		&& isEQ(wsaaEditRuleItemsInner.wsaaTermIssageTo[wsaaSub1.toInt()], ZERO))
		|| isLT(sv.anbage, wsaaEditRuleItemsInner.wsaaTermIssageFrm[wsaaSub1.toInt()])
		|| isGT(sv.anbage, wsaaEditRuleItemsInner.wsaaTermIssageTo[wsaaSub1.toInt()])) {
			goTo(GotoLabel.loop6302);
		}
		if (isGTE(wsaaRiskCessTerm, wsaaEditRuleItemsInner.wsaaRiskCesstermFrom[wsaaSub1.toInt()])
		&& isLTE(wsaaRiskCessTerm, wsaaEditRuleItemsInner.wsaaRiskCesstermTo[wsaaSub1.toInt()])) {
			/*                         T5608-RISK-CESSTERM-TO(WSAA-SUB1)    */
			wsaaRcesstrmErr.set(SPACES);
		}
		if (isGTE(wsaaPremCessTerm, wsaaEditRuleItemsInner.wsaaPremCesstermFrom[wsaaSub1.toInt()])
		&& isLTE(wsaaPremCessTerm, wsaaEditRuleItemsInner.wsaaPremCesstermTo[wsaaSub1.toInt()])) {
			/*                         T5608-PREM-CESSTERM-TO(WSAA-SUB1)    */
			wsaaPcesstrmErr.set(SPACES);
		}
		goTo(GotoLabel.loop6302);
	}

protected void continue6305()
	{
		if (rcessageErr.isTrue()) {
			scrnparams.errorCode.set(errorsInner.e519);
			wsspcomn.edterror.set("Y");
			wsaaError.set("Y");
		}
		if (pcessageErr.isTrue()) {
			scrnparams.errorCode.set(errorsInner.e562);
			wsspcomn.edterror.set("Y");
			wsaaError.set("Y");
		}
		if (rcesstrmErr.isTrue()) {
			scrnparams.errorCode.set(errorsInner.e551);
			wsspcomn.edterror.set("Y");
			wsaaError.set("Y");
		}
		if (pcesstrmErr.isTrue()) {
			scrnparams.errorCode.set(errorsInner.e563);
			wsspcomn.edterror.set("Y");
			wsaaError.set("Y");
		}
		if (errorFound.isTrue()) {
			return ;
		}
		wsbbCovttrmInner.wsbbCoverage[wsaaSub.toInt()].set(wsaaCoverage);
		wsbbCovttrmInner.wsbbRider[wsaaSub.toInt()].set(wsaaRider);
		wsbbCovttrmInner.wsbbCrtable[wsaaSub.toInt()].set(wsaaCrtable);
		wsbbCovttrmInner.wsbbRcessdte[wsaaSub.toInt()].set(wsaaRiskCessDate);
		wsbbCovttrmInner.wsbbPcessdte[wsaaSub.toInt()].set(wsaaPremCessDate);
		wsbbCovttrmInner.wsbbRcessage[wsaaSub.toInt()].set(t5703rec.riskCessAge);
		wsbbCovttrmInner.wsbbPcessage[wsaaSub.toInt()].set(t5703rec.premCessAge);
		wsbbCovttrmInner.wsbbRcesstrm[wsaaSub.toInt()].set(t5703rec.riskCessTerm);
		wsbbCovttrmInner.wsbbPcesstrm[wsaaSub.toInt()].set(t5703rec.premCessTerm);
		wsaaSub.add(1);
	}

protected void callDatcon36310()
	{
		/*PARA*/
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Read the Coverage/Rider benefit details T5703.
	* </pre>
	*/
protected void validateT57036400()
	{
		try {
			readT57036400();
			validateT57036405();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readT57036400()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5703);
		wsaaT5703Cnttype.set(chdrlnbIO.getCnttype());
		wsaaT5703Crtable.set(wsaaCrtable);
		itdmIO.setItemitem(wsaaT5703Key);
		itdmIO.setItmfrm(sv.occdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t5703, itdmIO.getItemtabl())
		|| isNE(wsaaT5703Key, itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			scrnparams.errorCode.set(errorsInner.t024);
			wsspcomn.edterror.set("Y");
			wsaaError.set("Y");
			goTo(GotoLabel.exit6409);
		}
		t5703rec.t5703Rec.set(itdmIO.getGenarea());
	}

protected void validateT57036405()
	{
		if (isEQ(t5703rec.riskCessTerm, ZERO)) {
			if (isEQ(t5703rec.riskCessAge, ZERO)) {
				scrnparams.errorCode.set(errorsInner.t026);
				wsspcomn.edterror.set("Y");
				wsaaError.set("Y");
			}
		}
		else {
			if (isNE(t5703rec.riskCessAge, ZERO)) {
				scrnparams.errorCode.set(errorsInner.t026);
				wsspcomn.edterror.set("Y");
				wsaaError.set("Y");
			}
		}
		if (isEQ(t5703rec.premCessTerm, ZERO)) {
			if (isEQ(t5703rec.premCessAge, ZERO)) {
				scrnparams.errorCode.set(errorsInner.t026);
				wsspcomn.edterror.set("Y");
				wsaaError.set("Y");
			}
		}
		else {
			if (isNE(t5703rec.premCessAge, ZERO)) {
				scrnparams.errorCode.set(errorsInner.t026);
				wsspcomn.edterror.set("Y");
				wsaaError.set("Y");
			}
		}
	}

protected void calcRiskCessDate6500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					chkRiskCessAge6500();
				case chkRiskCessTerm6505: 
					chkRiskCessTerm6505();
				case callDatcon26506: 
					callDatcon26506();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void chkRiskCessAge6500()
	{
		/* Setup WSAA-EAAGE depending on the edit rule table used.      */
		if (isEQ(t5703rec.riskCessAge, ZERO)) {
			goTo(GotoLabel.chkRiskCessTerm6505);
		}
		/* IF T5608-EAAGE              = 'A'                            */
		if (isEQ(wsaaEditRuleItemsInner.wsaaEaage, "A")) {
//			datcon2rec.intDate1.set(sv.occdate);
			datcon2rec.intDate1.set(lifelnbIO.getCltdob());
			compute(datcon2rec.freqFactor, 0).set(sub(t5703rec.riskCessAge, lifelnbIO.getAnbAtCcd()));
		}
		/* IF T5608-EAAGE              = 'E' OR SPACES                  */
		if (isEQ(wsaaEditRuleItemsInner.wsaaEaage, "E")
		|| isEQ(wsaaEditRuleItemsInner.wsaaEaage, SPACES)) {
			datcon2rec.intDate1.set(lifelnbIO.getCltdob());
			datcon2rec.freqFactor.set(t5703rec.riskCessAge);
		}
		goTo(GotoLabel.callDatcon26506);
	}

protected void chkRiskCessTerm6505()
	{
		/* IF T5608-EAAGE              = 'A' OR SPACES                  */
		if (isEQ(wsaaEditRuleItemsInner.wsaaEaage, "A")
		|| isEQ(wsaaEditRuleItemsInner.wsaaEaage, SPACES)) {
			datcon2rec.freqFactor.set(t5703rec.riskCessTerm);
			datcon2rec.intDate1.set(sv.occdate);
		}
		/* IF T5608-EAAGE              = 'E'                            */
		if (isEQ(wsaaEditRuleItemsInner.wsaaEaage, "E")) {
			datcon2rec.intDate1.set(lifelnbIO.getCltdob());
			compute(datcon2rec.freqFactor, 0).set(add(t5703rec.riskCessTerm, lifelnbIO.getAnbAtCcd()));
			if (isNE(sv.occdate, lifelnbIO.getCltdob())) {
				datcon2rec.freqFactor.subtract(1);
			}
		}
	}

protected void callDatcon26506()
	{
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			scrnparams.errorCode.set(datcon2rec.statuz);
			wsspcomn.edterror.set("Y");
			wsaaError.set("Y");
		}
		else {
			wsaaRiskCessDate.set(datcon2rec.intDate2);
		}
		/*EXIT*/
	}

protected void calcPremCessDate6600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					chkPremCessAge6600();
				case premCessTerm6605: 
					premCessTerm6605();
				case callDatcon26606: 
					callDatcon26606();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void chkPremCessAge6600()
	{
		/* Setup WSAA-EAAGE depending on the edit rule table used.      */
		if (isEQ(t5703rec.premCessAge, ZERO)) {
			goTo(GotoLabel.premCessTerm6605);
		}
		/* IF T5608-EAAGE              = 'A'                            */
		if (isEQ(wsaaEditRuleItemsInner.wsaaEaage, "A")) {
//			datcon2rec.intDate1.set(sv.occdate);
			datcon2rec.intDate1.set(lifelnbIO.getCltdob());
			compute(datcon2rec.freqFactor, 0).set(sub(t5703rec.premCessAge, lifelnbIO.getAnbAtCcd()));
		}
		/* IF T5608-EAAGE              = 'E' OR SPACES                  */
		if (isEQ(wsaaEditRuleItemsInner.wsaaEaage, "E")
		|| isEQ(wsaaEditRuleItemsInner.wsaaEaage, SPACES)) {
			datcon2rec.intDate1.set(lifelnbIO.getCltdob());
			datcon2rec.freqFactor.set(t5703rec.premCessAge);
		}
		goTo(GotoLabel.callDatcon26606);
	}

protected void premCessTerm6605()
	{
		/* IF T5608-EAAGE              = 'A' OR SPACE                   */
		if (isEQ(wsaaEditRuleItemsInner.wsaaEaage, "A")
		|| isEQ(wsaaEditRuleItemsInner.wsaaEaage, SPACES)) {
			datcon2rec.freqFactor.set(t5703rec.premCessTerm);
			datcon2rec.intDate1.set(sv.occdate);
		}
		/* IF T5608-EAAGE              = 'E'                            */
		if (isEQ(wsaaEditRuleItemsInner.wsaaEaage, "E")) {
			datcon2rec.intDate1.set(lifelnbIO.getCltdob());
			compute(datcon2rec.freqFactor, 0).set(add(t5703rec.premCessTerm, lifelnbIO.getAnbAtCcd()));
			if (isNE(sv.occdate, lifelnbIO.getCltdob())) {
				datcon2rec.freqFactor.subtract(1);
			}
		}
	}

protected void callDatcon26606()
	{
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			scrnparams.errorCode.set(datcon2rec.statuz);
			wsspcomn.edterror.set("Y");
			wsaaError.set("Y");
		}
		else {
			wsaaPremCessDate.set(datcon2rec.intDate2);
		}
		/*EXIT*/
	}

protected void chkFv6700()
	{
		/*CHECKING*/
		if (isGT(t5704rec.mbaseunt, sv.unitpkg)) {
			sv.unitpkgErr.set(errorsInner.t059);
			sv.unitpkgOut[varcom.ri.toInt()].set("Y");
			wsaaError.set("Y");
			return ;
		}
		/* COMPUTE WSAA-INPUT-FV ROUNDED = S5106-UNITPKG                */
		/*                               * T5704-UNITPKG.               */
		/* COMPUTE WSAA-TABLE-FV ROUNDED = T5704-UNITPKG                */
		/*                               * T5704-MBASEUNT.              */
		/* COMPUTE WSAA-DIFF-FV ROUNDED  = WSAA-INPUT-FV                */
		/*                               - WSAA-TABLE-FV.               */
		/*                             - WSAA-TABLE-FV.            <001>*/
		/*                             - T5704-MBASEUNT.           <001>*/
		compute(wsaaDiff, 8).set(sub(sv.unitpkg, t5704rec.mbaseunt));
		compute(wsaaUnitbaseDiffFv, 8).setDivide(wsaaDiff, (t5704rec.mbaseinc));
		wsaaRemainder.setRemainder(wsaaUnitbaseDiffFv);
		if (isNE(wsaaRemainder, ZERO)) {
			sv.unitpkgErr.set(errorsInner.f270);
			sv.unitpkgOut[varcom.ri.toInt()].set("Y");
			wsaaError.set("Y");
		}
		/*EXIT*/
	}

protected void calcSumAssuredBase6800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialize6800();
					initializePara6805();
				case sumValues6807: 
					sumValues6807();
				case exit6809: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialize6800()
	{
		/* Calculate the sum assured from Fast Track Table T5704*/
		/* COMPUTE WSAA-SUMIN ROUNDED  = (T5704-UNITPKG                 */
		compute(wsaaSumin, 3).setRounded(mult((mult(mult(t5704rec.unitpkg[wsaaSub.toInt()], sv.unitpkg), t5705rec.plansuff)), (div(t5704rec.unitpct[wsaaSub.toInt()], 100))));
		/* If benefit billed, do not calculate premium*/
		readTableT56872100();
		if (isNE(t5687rec.bbmeth, SPACES)) {
			premiumrec.calcPrem.set(ZERO);
			premiumrec.calcBasPrem.set(ZERO);
			premiumrec.calcLoaPrem.set(ZERO);
		}
		/*     MOVE ZEROES             TO CPRM-CALC-LOA-PREM    <LA3511>*/
		/*     GO TO                   6807-SUM-VALUES.                 */
		if (isNE(t5687rec.premmeth, SPACES)) {
			readTableT56752110();
			if (isEQ(t5675rec.premsubr, SPACES)) {
				premiumrec.calcPrem.set(ZERO);
				goTo(GotoLabel.sumValues6807);
			}
		}
		else {
			goTo(GotoLabel.sumValues6807);
		}
	}

	/**
	* <pre>
	****         GO TO               6807-SUM-VALUES.                 
	* PREMIUM CALCULATION
	* The  premium amount is  required  on  all  products  and  all
	* validating  must  be  successfully  completed  before  it  is
	* calculated. If there is  no  premium  method defined (i.e the
	* relevant code was blank), the premium amount must be entered.
	* Otherwise, it is optional and always calculated.
	* Note that a premium calculation subroutine may calculate the
	* premium from the sum insured OR the sum insured from the
	* premium.
	* To calculate  it,  call  the  relevant calculation subroutine
	* worked out above passing:
	* </pre>
	*/
protected void initializePara6805()
	{
		premiumrec.function.set("CALC");
		premiumrec.crtable.set(wsaaCrtable);
		premiumrec.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(lifelnbIO.getLife());
		premiumrec.lifeJlife.set("00");
		premiumrec.covrCoverage.set(wsaaCoverage);
		premiumrec.covrRider.set(wsaaRider);
		premiumrec.effectdt.set(sv.occdate);
		premiumrec.termdate.set(wsaaPremCessDate);
		premiumrec.lsex.set(lifelnbIO.getCltsex());
		premiumrec.lage.set(lifelnbIO.getAnbAtCcd());
		premiumrec.jlsex.set(SPACES);
		premiumrec.jlage.set(ZERO);
		datcon3rec.intDate1.set(sv.occdate);
		datcon3rec.intDate2.set(wsaaPremCessDate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		premiumrec.currcode.set(t5705rec.cntcurr);
		premiumrec.sumin.set(wsaaSumin);
		premiumrec.mortcls.set(sv.mortcls);
		premiumrec.billfreq.set(sv.billfreq);
		/* MOVE T3625-BILLCHNL         TO CPRM-MOP                      */
		premiumrec.mop.set(sv.mop);
		premiumrec.ratingdate.set(sv.occdate);
		premiumrec.reRateDate.set(sv.occdate);
		premiumrec.calcPrem.set(ZERO);
		premiumrec.calcBasPrem.set(ZERO);
		premiumrec.calcLoaPrem.set(ZERO);
		/* PERFORM 6810-VALIDATE-DISCMETH.                              */
		/* IF ERROR-FOUND                                               */
		/*     MOVE ZEROES             TO CPRM-CALC-PREM                */
		/*     GO TO                   6807-SUM-VALUES.                 */
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
		premiumrec.language.set(wsspcomn.language);
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/		
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{
		
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			premiumrec.cnttype.set(chdrlnbIO.getCnttype());
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End		*/
		//****Ticket #ILIFE-2005 end
		if (isEQ(premiumrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz, varcom.oK)) {
			scrnparams.errorCode.set(premiumrec.statuz);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit6809);
		}
	}

protected void sumValues6807()
	{
		wsaaPrem.set(premiumrec.calcPrem);
		wsaaPremTot.add(premiumrec.calcPrem);
		wsaaSuminTot.add(wsaaSumin);
		wsbbCovttrmInner.wsbbSumin[wsaaSub.toInt()].set(wsaaSumin);
		if (isEQ(sv.billfreq, "00")) {
			wsbbCovttrmInner.wsbbSingp[wsaaSub.toInt()].set(premiumrec.calcPrem);
			wsbbCovttrmInner.wsbbZbinstprem[wsaaSub.toInt()].set(premiumrec.calcBasPrem);
			wsbbCovttrmInner.wsbbZlinstprem[wsaaSub.toInt()].set(premiumrec.calcLoaPrem);
			wsaaPremiumType.set("Y");
		}
		else {
			wsbbCovttrmInner.wsbbInstprem[wsaaSub.toInt()].set(premiumrec.calcPrem);
			wsbbCovttrmInner.wsbbZbinstprem[wsaaSub.toInt()].set(premiumrec.calcBasPrem);
			wsbbCovttrmInner.wsbbZlinstprem[wsaaSub.toInt()].set(premiumrec.calcLoaPrem);
			wsaaPremiumType.set("N");
		}
	}

	/**
	* <pre>
	* Validate the following tables below before calling the premium
	* calculatation subroutine. This is to prevent this program to
	* bomb out.
	* CALCULATION SUBROUTINES HAS BEEN AMENDED TO NOT BOMB OUT IF     
	* T5659 NOT THERE, SO THIS IS NOW A REDUNDANT ROUTINE AND HAS     
	* BEEN COMMENTED OUT.                                             
	*6810-VALIDATE-DISCMETH SECTION.                                  
	*6810-READ-T5658.                                                 
	* Build a key.
	* This key (see below)
	* will read table T5658. This table contains the parameters to be
	* used in the calculation of the Basic Annual Premium for
	* Coverage/Rider components.
	* The key is a concatenation of the following fields:-
	* Coverage/Rider table code
	* Term                     (calculated)
	* Sex
	* Mortality Class
	* Access the required table by reading the table directly (ITDM).
	* The contents of the table are then stored. This table is dated
	* use:
	*  1) Rating Date
	**** MOVE CPRM-DURATION          TO WSAA-T5658-AGETERM.           
	*6812-CONT-READ-T5658.                                            
	**** MOVE CPRM-CHDR-CHDRCOY      TO ITDM-ITEMCOY.                 
	**** MOVE T5658                  TO ITDM-ITEMTABL.                
	**** MOVE CPRM-CRTABLE           TO WSAA-T5658-CRTABLE.           
	**** MOVE CPRM-MORTCLS           TO WSAA-T5658-MORTCLS.           
	**** MOVE CPRM-LSEX              TO WSAA-T5658-SEX.               
	**** MOVE WSAA-T5658-KEY         TO ITDM-ITEMITEM.                
	**** IF CPRM-RATINGDATE          = ZEROS                          
	****     MOVE VRCM-MAX-DATE      TO ITDM-ITMFRM                   
	**** ELSE                                                         
	****     MOVE CPRM-RATINGDATE    TO ITDM-ITMFRM.                  
	**** MOVE BEGN                   TO ITDM-FUNCTION.                
	**** CALL 'ITDMIO'               USING ITDM-PARAMS.               
	**** IF ITDM-STATUZ              NOT = O-K                        
	**** AND ITDM-STATUZ             NOT = ENDP                       
	****     MOVE ITDM-PARAMS        TO SYSR-PARAMS                   
	****     PERFORM 600-FATAL-ERROR.                                 
	**** IF WSAA-T5658-KEY           NOT = ITDM-ITEMITEM              
	**** OR CPRM-CHDR-CHDRCOY        NOT = ITDM-ITEMCOY               
	**** OR ITDM-ITEMTABL            NOT = T5658                      
	**** OR ITDM-STATUZ              = ENDP                           
	****     IF WSAA-T5658-AGETERM   = '**'                           
	****         MOVE F265           TO SCRN-ERROR-CODE               
	****         MOVE 'Y'            TO WSSP-EDTERROR                 
	****                                WSAA-ERROR                    
	****         GO TO               6819-EXIT                        
	****     ELSE                                                     
	****         MOVE SPACES         TO WSAA-T5658-KEY                
	****         MOVE '**'           TO WSAA-T5658-AGETERM            
	****         GO TO               6812-CONT-READ-T5658             
	****     END-IF                                                   
	**** END-IF.                                                      
	**** MOVE ITDM-GENAREA           TO T5658-T5658-REC.              
	*6814-READ-T5659.                                                 
	* APPLY-DISCOUNT.
	* Access the discount table T5659 using the key:-
	* - Discount method from T5658 concatenated with currency.
	**** MOVE CPRM-CHDR-CHDRCOY      TO ITDM-ITEMCOY.                 
	**** MOVE T5659                  TO ITDM-ITEMTABL.                
	**** MOVE T5658-DISCCNTMETH      TO WSAA-T5659-DISCCNTMETH.       
	**** MOVE CPRM-CURRCODE          TO WSAA-T5659-CURRCODE.          
	**** MOVE WSAA-T5659-KEY         TO ITDM-ITEMITEM.                
	**** IF CPRM-RATINGDATE          = ZEROS                          
	****     MOVE  VRCM-MAX-DATE     TO ITDM-ITMFRM                   
	**** ELSE                                                         
	****     MOVE CPRM-RATINGDATE    TO ITDM-ITMFRM.                  
	**** MOVE BEGN                   TO ITDM-FUNCTION.                
	**** CALL 'ITDMIO'               USING ITDM-PARAMS.               
	**** IF ITDM-STATUZ              NOT = O-K                        
	**** AND ITDM-STATUZ             NOT = ENDP                       
	****     MOVE ITDM-PARAMS        TO SYSR-PARAMS                   
	****     PERFORM 600-FATAL-ERROR.                                 
	**** IF CPRM-CHDR-CHDRCOY        NOT =  ITDM-ITEMCOY              
	**** OR ITDM-ITEMTABL            NOT =  T5659                     
	**** OR WSAA-T5659-KEY           NOT =  ITDM-ITEMITEM             
	**** OR ITDM-STATUZ              = ENDP                           
	****     MOVE F264               TO SCRN-ERROR-CODE               
	****     MOVE 'Y'                TO WSSP-EDTERROR                 
	****                                WSAA-ERROR.                   
	*6819-EXIT.                                                       
	**** EXIT.                                                        
	* </pre>
	*/
protected void calcPremiumBase6900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialize6900();
					initializePara6905();
				case sumValues6907: 
					sumValues6907();
				case exit6909: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialize6900()
	{
		/* Calculate the premium from Fast Track Table T5704               */
		compute(wsaaPrem, 3).setRounded(mult((mult(t5704rec.unitpkg[wsaaSub.toInt()], sv.unitpkg)), (div(t5704rec.unitpct[wsaaSub.toInt()], 100))));
		/* If benefit billed, do not calculate premium*/
		readTableT56872100();
		if (isNE(t5687rec.bbmeth, SPACES)) {
			wsaaSumin.set(ZERO);
		}
		/*                                WSAA-PREM                     */
		/*     GO TO 6907-SUM-VALUES.                                   */
		if (isNE(t5687rec.premmeth, SPACES)) {
			readTableT56752110();
			if (isEQ(t5675rec.premsubr, SPACES)) {
				premiumrec.sumin.set(ZERO);
				/*                                WSAA-PREM                     */
				goTo(GotoLabel.sumValues6907);
			}
		}
		else {
			goTo(GotoLabel.sumValues6907);
		}
	}

	/**
	* <pre>
	****         GO TO               6907-SUM-VALUES.                 
	**** !!! MOVE TO THE BEGINNING OF THIS PARAGRAPH !!!              
	* Calculate the sum assured from Fast Track Table T5704
	**** COMPUTE WSAA-PREM ROUNDED   = (T5704-UNITPKG                 
	****                             * S5106-UNITPKG)                 
	****                             * (T5704-UNITPCT(WSAA-SUB)       
	****                             / 100).                          
	* SUM ASSURED CALCULATION
	* The  premium amount is  required  on  all  products  and  all
	* validating  must  be  successfully  completed  before  it  is
	* calculated. If there is  no  premium  method defined (i.e the
	* relevant code was blank), the premium amount must be entered.
	* Otherwise, it is optional and always calculated.
	* Note that a premium calculation subroutine may calculate the
	* premium from the sum insured OR the sum insured from the
	* premium.
	* To calculate  it,  call  the  relevant calculation subroutine
	* worked out above passing:
	* </pre>
	*/
protected void initializePara6905()
	{
		premiumrec.function.set("CALC");
		premiumrec.crtable.set(wsaaCrtable);
		premiumrec.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(lifelnbIO.getLife());
		premiumrec.lifeJlife.set("00");
		premiumrec.covrCoverage.set(wsaaCoverage);
		premiumrec.covrRider.set(wsaaRider);
		premiumrec.effectdt.set(sv.occdate);
		premiumrec.termdate.set(wsaaPremCessDate);
		premiumrec.lsex.set(lifelnbIO.getCltsex());
		premiumrec.lage.set(lifelnbIO.getAnbAtCcd());
		premiumrec.jlsex.set(SPACES);
		premiumrec.jlage.set(ZERO);
		datcon3rec.intDate1.set(sv.occdate);
		datcon3rec.intDate2.set(wsaaPremCessDate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		premiumrec.currcode.set(t5705rec.cntcurr);
		premiumrec.sumin.set(wsaaSumin);
		premiumrec.mortcls.set(sv.mortcls);
		premiumrec.billfreq.set(sv.billfreq);
		/* MOVE T3625-BILLCHNL         TO CPRM-MOP.                     */
		premiumrec.mop.set(sv.mop);
		premiumrec.ratingdate.set(sv.occdate);
		premiumrec.reRateDate.set(sv.occdate);
		premiumrec.calcPrem.set(wsaaPrem);
		premiumrec.calcBasPrem.set(wsaaPrem);
		premiumrec.calcLoaPrem.set(ZERO);
		premiumrec.language.set(wsspcomn.language);
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/		
		// ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{
		
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			premiumrec.cnttype.set(chdrlnbIO.getCnttype());
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End		*/
		//****Ticket #ILIFE-2005 end
		if (isEQ(premiumrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz, varcom.oK)) {
			sv.prmamtErr.set(premiumrec.statuz);
			/*     MOVE CPRM-STATUZ        TO S5106-PREM-ERR                */
			sv.prmamtOut[varcom.ri.toInt()].set("Y");
			/*     MOVE 'Y'                TO S5106-PREM-OUT(RI)            */
			goTo(GotoLabel.exit6909);
		}
	}

protected void sumValues6907()
	{
		wsaaSumin.set(premiumrec.sumin);
		wsaaPremTot.add(wsaaPrem);
		wsaaSuminTot.add(premiumrec.sumin);
		wsbbCovttrmInner.wsbbSumin[wsaaSub.toInt()].set(premiumrec.sumin);
		if (isEQ(sv.billfreq, "00")) {
			wsbbCovttrmInner.wsbbSingp[wsaaSub.toInt()].set(wsaaPrem);
			wsaaPremiumType.set("Y");
		}
		else {
			wsbbCovttrmInner.wsbbInstprem[wsaaSub.toInt()].set(wsaaPrem);
			wsaaPremiumType.set("N");
		}
	}

protected void checkCalcCompTax7000()
	{
		start7010();
	}

protected void start7010()
	{
		if (isEQ(premiumrec.calcPrem, 0)
		|| isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set(covttrmIO.getCrtable());
		readTr52e7200();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			readTr52e7200();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e7200();
		}
		if (isNE(tr52erec.taxind01, "Y")) {
			return ;
		}
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		txcalcrec.life.set(covttrmIO.getLife());
		txcalcrec.coverage.set(covttrmIO.getCoverage());
		txcalcrec.rider.set(covttrmIO.getRider());
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.crtable.set(covttrmIO.getCrtable());
		txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
		txcalcrec.register.set(sv.register);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(t5705rec.cntcurr);
		wsaaCntCurr.set(t5705rec.cntcurr);
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.transType.set("PREM");
		if (isEQ(tr52erec.zbastyp, "Y")) {
			txcalcrec.amountIn.set(premiumrec.calcBasPrem);
		}
		else {
			txcalcrec.amountIn.set(premiumrec.calcPrem);
		}
		txcalcrec.effdate.set(chdrlnbIO.getOccdate());
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaPremTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaPremTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}

protected void checkCalcContTax7100()
	{
		start7100();
	}

protected void start7100()
	{
		if (isEQ(tr52drec.txcode, SPACES)
		|| isEQ(wsaaCntfee, 0)) {
			return ;
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set("****");
		readTr52e7200();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e7200();
		}
		if (isNE(tr52erec.taxind02, "Y")) {
			return ;
		}
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		txcalcrec.life.set(SPACES);
		txcalcrec.coverage.set(SPACES);
		txcalcrec.rider.set(SPACES);
		txcalcrec.crtable.set(SPACES);
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
		txcalcrec.register.set(sv.register);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(t5705rec.cntcurr);
		wsaaCntCurr.set(t5705rec.cntcurr);
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.amountIn.set(wsaaCntfee);
		txcalcrec.transType.set("CNTF");
		txcalcrec.effdate.set(chdrlnbIO.getOccdate());
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaCntfeeTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaCntfeeTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}

protected void readTr52e7200()
	{
		start7210();
	}

protected void start7210()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isNE(itdmIO.getItemcoy(), wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.params.set(wsaaTr52eKey);
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isEQ(itdmIO.getItemcoy(), wsspcomn.company))
		&& (isEQ(itdmIO.getItemtabl(), tablesInner.tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}
/*
 * Class transformed  from Data Structure WSBB-COVTTRM--INNER
 */
private static final class WsbbCovttrmInner { 
		/* WSBB-COVTTRM */
	private FixedLengthStringData[] wsbbCoverage = FLSInittedArray(20, 2);
	private FixedLengthStringData[] wsbbRider = FLSInittedArray(20, 2);
	private FixedLengthStringData[] wsbbCrtable = FLSInittedArray(20, 4);
	private PackedDecimalData[] wsbbRcessdte = PDInittedArray(20, 8, 0);
	private PackedDecimalData[] wsbbPcessdte = PDInittedArray(20, 8, 0);
	private PackedDecimalData[] wsbbRcessage = PDInittedArray(20, 3, 0);
	private PackedDecimalData[] wsbbPcessage = PDInittedArray(20, 3, 0);
	private PackedDecimalData[] wsbbRcesstrm = PDInittedArray(20, 3, 0);
	private PackedDecimalData[] wsbbPcesstrm = PDInittedArray(20, 3, 0);
	private PackedDecimalData[] wsbbSumin = PDInittedArray(20, 17, 2);
	private PackedDecimalData[] wsbbSingp = PDInittedArray(20, 17, 2);
	private PackedDecimalData[] wsbbInstprem = PDInittedArray(20, 17, 2);
	private PackedDecimalData[] wsbbZbinstprem = PDInittedArray(20, 17, 2);
	private PackedDecimalData[] wsbbZlinstprem = PDInittedArray(20, 17, 2);
}
/*
 * Class transformed  from Data Structure WSAA-EDIT-RULE-ITEMS--INNER
 */
private static final class WsaaEditRuleItemsInner { 

	private FixedLengthStringData wsaaEditRuleItems = new FixedLengthStringData(319);
	private FixedLengthStringData wsaaEaage = new FixedLengthStringData(1).isAPartOf(wsaaEditRuleItems, 0);
	private ZonedDecimalData wsaaSumInsMin = new ZonedDecimalData(15, 0).isAPartOf(wsaaEditRuleItems, 1);
	private ZonedDecimalData wsaaSumInsMax = new ZonedDecimalData(15, 0).isAPartOf(wsaaEditRuleItems, 16);
	private FixedLengthStringData wsaaAgeIssageFrms = new FixedLengthStringData(24).isAPartOf(wsaaEditRuleItems, 31);
		/*                                         OCCURS 09 .     <V73L03>*/
	private ZonedDecimalData[] wsaaAgeIssageFrm = ZDArrayPartOfStructure(8, 3, 0, wsaaAgeIssageFrms, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(wsaaAgeIssageFrms, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaAgeIssageFrm01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
	private ZonedDecimalData wsaaAgeIssageFrm02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
	private ZonedDecimalData wsaaAgeIssageFrm03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
	private ZonedDecimalData wsaaAgeIssageFrm04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
	private ZonedDecimalData wsaaAgeIssageFrm05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
	private ZonedDecimalData wsaaAgeIssageFrm06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
	private ZonedDecimalData wsaaAgeIssageFrm07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
	private ZonedDecimalData wsaaAgeIssageFrm08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
	private FixedLengthStringData wsaaAgeIssageTos = new FixedLengthStringData(24).isAPartOf(wsaaEditRuleItems, 55);
		/*                                         OCCURS 09 .     <V73L03>*/
	private ZonedDecimalData[] wsaaAgeIssageTo = ZDArrayPartOfStructure(8, 3, 0, wsaaAgeIssageTos, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(wsaaAgeIssageTos, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaAgeIssageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 0);
	private ZonedDecimalData wsaaAgeIssageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 3);
	private ZonedDecimalData wsaaAgeIssageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 6);
	private ZonedDecimalData wsaaAgeIssageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 9);
	private ZonedDecimalData wsaaAgeIssageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 12);
	private ZonedDecimalData wsaaAgeIssageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 15);
	private ZonedDecimalData wsaaAgeIssageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 18);
	private ZonedDecimalData wsaaAgeIssageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 21);
	private FixedLengthStringData wsaaTermIssageFrms = new FixedLengthStringData(24).isAPartOf(wsaaEditRuleItems, 79);
		/*                                           OCCURS 09 .   <V73L03>*/
	private ZonedDecimalData[] wsaaTermIssageFrm = ZDArrayPartOfStructure(8, 3, 0, wsaaTermIssageFrms, 0);
	private FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(wsaaTermIssageFrms, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaTermIssageFrm01 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 0);
	private ZonedDecimalData wsaaTermIssageFrm02 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 3);
	private ZonedDecimalData wsaaTermIssageFrm03 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 6);
	private ZonedDecimalData wsaaTermIssageFrm04 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 9);
	private ZonedDecimalData wsaaTermIssageFrm05 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 12);
	private ZonedDecimalData wsaaTermIssageFrm06 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 15);
	private ZonedDecimalData wsaaTermIssageFrm07 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 18);
	private ZonedDecimalData w5aaTermIssageFrm08 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 21);
	private FixedLengthStringData wsaaTermIssageTos = new FixedLengthStringData(24).isAPartOf(wsaaEditRuleItems, 103);
		/*                                           OCCURS 09 .   <V73L03>*/
	private ZonedDecimalData[] wsaaTermIssageTo = ZDArrayPartOfStructure(8, 3, 0, wsaaTermIssageTos, 0);
	private FixedLengthStringData filler3 = new FixedLengthStringData(24).isAPartOf(wsaaTermIssageTos, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaTermIssageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 0);
	private ZonedDecimalData wsaaTermIssageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 3);
	private ZonedDecimalData wsaaTermIssageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 6);
	private ZonedDecimalData wsaaTermIssageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 9);
	private ZonedDecimalData wsaaTermIssageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 12);
	private ZonedDecimalData wsaaTermIssageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 15);
	private ZonedDecimalData wsaaTermIssageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 18);
	private ZonedDecimalData wsaaTermIssageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 21);
	private FixedLengthStringData wsaaRiskCessageFroms = new FixedLengthStringData(24).isAPartOf(wsaaEditRuleItems, 127);
		/*                                         OCCURS 09 .     <V73L03>*/
	private ZonedDecimalData[] wsaaRiskCessageFrom = ZDArrayPartOfStructure(8, 3, 0, wsaaRiskCessageFroms, 0);
	private FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(wsaaRiskCessageFroms, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRiskCessageFrom01 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 0);
	private ZonedDecimalData wsaaRiskCessageFrom02 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 3);
	private ZonedDecimalData wsaaRiskCessageFrom03 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 6);
	private ZonedDecimalData wsaaRiskCessageFrom04 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 9);
	private ZonedDecimalData wsaaRiskCessageFrom05 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 12);
	private ZonedDecimalData wsaaRiskCessageFrom06 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 15);
	private ZonedDecimalData wsaaRiskCessageFrom07 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 18);
	private ZonedDecimalData wsaaRiskCessageFrom08 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 21);
	private FixedLengthStringData wsaaRiskCessageTos = new FixedLengthStringData(24).isAPartOf(wsaaEditRuleItems, 151);
		/*                                         OCCURS 09 .     <V73L03>*/
	private ZonedDecimalData[] wsaaRiskCessageTo = ZDArrayPartOfStructure(8, 3, 0, wsaaRiskCessageTos, 0);
	private FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(wsaaRiskCessageTos, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRiskCessageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 0);
	private ZonedDecimalData wsaaRiskCessageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 3);
	private ZonedDecimalData wsaaRiskCessageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 6);
	private ZonedDecimalData wsaaRiskCessageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 9);
	private ZonedDecimalData wsaaRiskCessageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 12);
	private ZonedDecimalData wsaaRiskCessageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 15);
	private ZonedDecimalData wsaaRiskCessageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 18);
	private ZonedDecimalData wsaaRiskCessageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 21);
	private FixedLengthStringData wsaaRiskCesstermFroms = new FixedLengthStringData(24).isAPartOf(wsaaEditRuleItems, 175);
		/*                                         OCCURS 09 .     <V73L03>*/
	private ZonedDecimalData[] wsaaRiskCesstermFrom = ZDArrayPartOfStructure(8, 3, 0, wsaaRiskCesstermFroms, 0);
	private FixedLengthStringData filler6 = new FixedLengthStringData(24).isAPartOf(wsaaRiskCesstermFroms, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRiskCesstermFrom01 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 0);
	private ZonedDecimalData wsaaRiskCesstermFrom02 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 3);
	private ZonedDecimalData wsaaRiskCesstermFrom03 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 6);
	private ZonedDecimalData wsaaRiskCesstermFrom04 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 9);
	private ZonedDecimalData wsaaRiskCesstermFrom05 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 12);
	private ZonedDecimalData wsaaRiskCesstermFrom06 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 15);
	private ZonedDecimalData wsaaRiskCesstermFrom07 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 18);
	private ZonedDecimalData wsaaRiskCesstermFrom08 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 21);
	private FixedLengthStringData wsaaRiskCesstermTos = new FixedLengthStringData(24).isAPartOf(wsaaEditRuleItems, 199);
		/*                                         OCCURS 09 .     <V73L03>*/
	private ZonedDecimalData[] wsaaRiskCesstermTo = ZDArrayPartOfStructure(8, 3, 0, wsaaRiskCesstermTos, 0);
	private FixedLengthStringData filler7 = new FixedLengthStringData(24).isAPartOf(wsaaRiskCesstermTos, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRiskCesstermTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 0);
	private ZonedDecimalData wsaaRiskCesstermTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 3);
	private ZonedDecimalData wsaaRiskCesstermTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 6);
	private ZonedDecimalData wsaaRiskCesstermTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 9);
	private ZonedDecimalData wsaaRiskCesstermTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 12);
	private ZonedDecimalData wsaaRiskCesstermTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 15);
	private ZonedDecimalData wsaaRiskCesstermTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 18);
	private ZonedDecimalData wsaaRiskCesstermTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler7, 21);
	private FixedLengthStringData wsaaPremCessageFroms = new FixedLengthStringData(24).isAPartOf(wsaaEditRuleItems, 223);
		/*                                         OCCURS 09 .     <V73L03>*/
	private ZonedDecimalData[] wsaaPremCessageFrom = ZDArrayPartOfStructure(8, 3, 0, wsaaPremCessageFroms, 0);
	private FixedLengthStringData filler8 = new FixedLengthStringData(24).isAPartOf(wsaaPremCessageFroms, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaPremCessageFrom01 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 0);
	private ZonedDecimalData wsaaPremCessageFrom02 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 3);
	private ZonedDecimalData wsaaPremCessageFrom03 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 6);
	private ZonedDecimalData wsaaPremCessageFrom04 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 9);
	private ZonedDecimalData wsaaPremCessageFrom05 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 12);
	private ZonedDecimalData wsaaPremCessageFrom06 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 15);
	private ZonedDecimalData wsaaPremCessageFrom07 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 18);
	private ZonedDecimalData wsaaPremCessageFrom08 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 21);
	private FixedLengthStringData wsaaPremCessageTos = new FixedLengthStringData(24).isAPartOf(wsaaEditRuleItems, 247);
		/*                                         OCCURS 09 .     <V73L03>*/
	private ZonedDecimalData[] wsaaPremCessageTo = ZDArrayPartOfStructure(8, 3, 0, wsaaPremCessageTos, 0);
	private FixedLengthStringData filler9 = new FixedLengthStringData(24).isAPartOf(wsaaPremCessageTos, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaPremCessageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 0);
	private ZonedDecimalData wsaaPremCessageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 3);
	private ZonedDecimalData wsaaPremCessageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 6);
	private ZonedDecimalData wsaaPremCessageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 9);
	private ZonedDecimalData wsaaPremCessageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 12);
	private ZonedDecimalData wsaaPremCessageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 15);
	private ZonedDecimalData wsaaPremCessageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 18);
	private ZonedDecimalData wsaaPremCessageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 21);
	private FixedLengthStringData wsaaPremCesstermFroms = new FixedLengthStringData(24).isAPartOf(wsaaEditRuleItems, 271);
		/*                                         OCCURS 09 .     <V73L03>*/
	private ZonedDecimalData[] wsaaPremCesstermFrom = ZDArrayPartOfStructure(8, 3, 0, wsaaPremCesstermFroms, 0);
	private FixedLengthStringData filler10 = new FixedLengthStringData(24).isAPartOf(wsaaPremCesstermFroms, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaPremCesstermFrom01 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 0);
	private ZonedDecimalData wsaaPremCesstermFrom02 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 3);
	private ZonedDecimalData wsaaPremCesstermFrom03 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 6);
	private ZonedDecimalData wsaaPremCesstermFrom04 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 9);
	private ZonedDecimalData wsaaPremCesstermFrom05 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 12);
	private ZonedDecimalData wsaaPremCesstermFrom06 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 15);
	private ZonedDecimalData wsaaPremCesstermFrom07 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 18);
	private ZonedDecimalData wsaaPremCesstermFrom08 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 21);
	private FixedLengthStringData wsaaPremCesstermTos = new FixedLengthStringData(24).isAPartOf(wsaaEditRuleItems, 295);
		/*                                         OCCURS 09 .     <V73L03>*/
	private ZonedDecimalData[] wsaaPremCesstermTo = ZDArrayPartOfStructure(8, 3, 0, wsaaPremCesstermTos, 0);
	private FixedLengthStringData filler11 = new FixedLengthStringData(24).isAPartOf(wsaaPremCesstermTos, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaPremCesstermTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 0);
	private ZonedDecimalData wsaaPremCesstermTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 3);
	private ZonedDecimalData wsaaPremCesstermTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 6);
	private ZonedDecimalData wsaaPremCesstermTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 9);
	private ZonedDecimalData wsaaPremCesstermTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 12);
	private ZonedDecimalData wsaaPremCesstermTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 15);
	private ZonedDecimalData wsaaPremCesstermTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 18);
	private ZonedDecimalData wsaaPremCesstermTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 21);
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e032 = new FixedLengthStringData(4).init("E032");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
	private FixedLengthStringData e305 = new FixedLengthStringData(4).init("E305");
	private FixedLengthStringData e368 = new FixedLengthStringData(4).init("E368");
	private FixedLengthStringData e416 = new FixedLengthStringData(4).init("E416");
	private FixedLengthStringData e417 = new FixedLengthStringData(4).init("E417");
	private FixedLengthStringData e455 = new FixedLengthStringData(4).init("E455");
	private FixedLengthStringData e475 = new FixedLengthStringData(4).init("E475");
	private FixedLengthStringData e480 = new FixedLengthStringData(4).init("E480");
	private FixedLengthStringData e481 = new FixedLengthStringData(4).init("E481");
	private FixedLengthStringData e483 = new FixedLengthStringData(4).init("E483");
	private FixedLengthStringData e484 = new FixedLengthStringData(4).init("E484");
	private FixedLengthStringData e487 = new FixedLengthStringData(4).init("E487");
	private FixedLengthStringData e519 = new FixedLengthStringData(4).init("E519");
	private FixedLengthStringData e525 = new FixedLengthStringData(4).init("E525");
	private FixedLengthStringData e551 = new FixedLengthStringData(4).init("E551");
	private FixedLengthStringData e562 = new FixedLengthStringData(4).init("E562");
	private FixedLengthStringData e563 = new FixedLengthStringData(4).init("E563");
	private FixedLengthStringData e566 = new FixedLengthStringData(4).init("E566");
	private FixedLengthStringData e840 = new FixedLengthStringData(4).init("E840");
	private FixedLengthStringData f151 = new FixedLengthStringData(4).init("F151");
	private FixedLengthStringData f290 = new FixedLengthStringData(4).init("F290");
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData f321 = new FixedLengthStringData(4).init("F321");
	private FixedLengthStringData f718 = new FixedLengthStringData(4).init("F718");
	private FixedLengthStringData f782 = new FixedLengthStringData(4).init("F782");
	private FixedLengthStringData f849 = new FixedLengthStringData(4).init("F849");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData g095 = new FixedLengthStringData(4).init("G095");
	private FixedLengthStringData g144 = new FixedLengthStringData(4).init("G144");
	private FixedLengthStringData g228 = new FixedLengthStringData(4).init("G228");
	private FixedLengthStringData g499 = new FixedLengthStringData(4).init("G499");
	private FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
	private FixedLengthStringData g788 = new FixedLengthStringData(4).init("G788");
	private FixedLengthStringData g844 = new FixedLengthStringData(4).init("G844");
	private FixedLengthStringData h030 = new FixedLengthStringData(4).init("H030");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData h143 = new FixedLengthStringData(4).init("H143");
	private FixedLengthStringData i012 = new FixedLengthStringData(4).init("I012");
	private FixedLengthStringData i068 = new FixedLengthStringData(4).init("I068");
	private FixedLengthStringData j002 = new FixedLengthStringData(4).init("J002");
	private FixedLengthStringData t024 = new FixedLengthStringData(4).init("T024");
	private FixedLengthStringData t026 = new FixedLengthStringData(4).init("T026");
	private FixedLengthStringData t027 = new FixedLengthStringData(4).init("T027");
	private FixedLengthStringData t028 = new FixedLengthStringData(4).init("T028");
	private FixedLengthStringData t029 = new FixedLengthStringData(4).init("T029");
	private FixedLengthStringData t030 = new FixedLengthStringData(4).init("T030");
	private FixedLengthStringData t032 = new FixedLengthStringData(4).init("T032");
	private FixedLengthStringData t033 = new FixedLengthStringData(4).init("T033");
	private FixedLengthStringData t034 = new FixedLengthStringData(4).init("T034");
	private FixedLengthStringData t059 = new FixedLengthStringData(4).init("T059");
	private FixedLengthStringData t061 = new FixedLengthStringData(4).init("T061");
	private FixedLengthStringData e398 = new FixedLengthStringData(4).init("E398");
	private FixedLengthStringData f270 = new FixedLengthStringData(4).init("F270");
	private FixedLengthStringData f859 = new FixedLengthStringData(4).init("F859");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t2240 = new FixedLengthStringData(5).init("T2240");
	private FixedLengthStringData t3695 = new FixedLengthStringData(5).init("T3695");
	private FixedLengthStringData t5608 = new FixedLengthStringData(5).init("T5608");
	private FixedLengthStringData t5606 = new FixedLengthStringData(5).init("T5606");
	private FixedLengthStringData th505 = new FixedLengthStringData(5).init("TH505");
	private FixedLengthStringData t5551 = new FixedLengthStringData(5).init("T5551");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5667 = new FixedLengthStringData(5).init("T5667");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5674 = new FixedLengthStringData(5).init("T5674");
	private FixedLengthStringData t5675 = new FixedLengthStringData(5).init("T5675");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5689 = new FixedLengthStringData(5).init("T5689");
	private FixedLengthStringData t5703 = new FixedLengthStringData(5).init("T5703");
	private FixedLengthStringData t5704 = new FixedLengthStringData(5).init("T5704");
	private FixedLengthStringData t5705 = new FixedLengthStringData(5).init("T5705");
	private FixedLengthStringData t6654 = new FixedLengthStringData(5).init("T6654");
	private FixedLengthStringData t3620 = new FixedLengthStringData(5).init("T3620");
	private FixedLengthStringData t5510 = new FixedLengthStringData(5).init("T5510");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData tr52q = new FixedLengthStringData(5).init("TR52Q");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData covttrmrec = new FixedLengthStringData(10).init("COVTTRMREC");
	private FixedLengthStringData lifelnbrec = new FixedLengthStringData(10).init("LIFELNBREC");
	private FixedLengthStringData acblrec = new FixedLengthStringData(10).init("ACBLREC   ");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC   ");
	private FixedLengthStringData hpadrec = new FixedLengthStringData(10).init("HPADREC   ");
	private FixedLengthStringData unltunlrec = new FixedLengthStringData(10).init("UNLTUNLREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
}
}
