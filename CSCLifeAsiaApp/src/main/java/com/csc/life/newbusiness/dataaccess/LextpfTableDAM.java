package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: LextpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:42
 * Class transformed from LEXTPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LextpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 141;
	public FixedLengthStringData lextrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData lextpfRecord = lextrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(lextrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(lextrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(lextrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(lextrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(lextrec);
	public PackedDecimalData seqnbr = DD.seqnbr.copy().isAPartOf(lextrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(lextrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(lextrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(lextrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(lextrec);
	public FixedLengthStringData opcda = DD.opcda.copy().isAPartOf(lextrec);
	public PackedDecimalData oppc = DD.oppc.copy().isAPartOf(lextrec);
	//public PackedDecimalData insprm = DD.insprm.copy().isAPartOf(lextrec);
	public PackedDecimalData insprm = initiateInsprm();
	public PackedDecimalData agerate = DD.agerate.copy().isAPartOf(lextrec);
	public FixedLengthStringData sbstdl = DD.sbstdl.copy().isAPartOf(lextrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(lextrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(lextrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(lextrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(lextrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(lextrec);
	public PackedDecimalData extCommDate = DD.extcd.copy().isAPartOf(lextrec);
	public PackedDecimalData extCessTerm = DD.ecestrm.copy().isAPartOf(lextrec);
	public PackedDecimalData extCessDate = DD.ecesdte.copy().isAPartOf(lextrec);
	public FixedLengthStringData reasind = DD.reasind.copy().isAPartOf(lextrec);
	public PackedDecimalData znadjperc = DD.znadjperc.copy().isAPartOf(lextrec);
	public PackedDecimalData zmortpct = DD.zmortpct.copy().isAPartOf(lextrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(lextrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(lextrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(lextrec);
	public PackedDecimalData premadj = DD.zpremiumAdjusted.copy().isAPartOf(lextrec);
	public FixedLengthStringData uwoverwrite = DD.uwoverwrite.copy().isAPartOf(lextrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public LextpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}
	
	protected PackedDecimalData initiateInsprm(){
	
		return DD.insprm.copy().isAPartOf(lextrec);
	}

	/**
	* Constructor for LextpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public LextpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for LextpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public LextpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for LextpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public LextpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("LEXTPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"SEQNBR, " +
							"VALIDFLAG, " +
							"TRANNO, " +
							"CURRFROM, " +
							"CURRTO, " +
							"OPCDA, " +
							"OPPC, " +
							"INSPRM, " +
							"AGERATE, " +
							"SBSTDL, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"JLIFE, " +
							"EXTCD, " +
							"ECESTRM, " +
							"ECESDTE, " +
							"REASIND, " +
							"ZNADJPERC, " +
							"PREMADJ, " +
							"ZMORTPCT, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UWOVERWRITE, "+
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     seqnbr,
                                     validflag,
                                     tranno,
                                     currfrom,
                                     currto,
                                     opcda,
                                     oppc,
                                     insprm,
                                     agerate,
                                     sbstdl,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     jlife,
                                     extCommDate,
                                     extCessTerm,
                                     extCessDate,
                                     reasind,
                                     znadjperc,
                                     premadj,
                                     zmortpct,
                                     userProfile,
                                     jobName,
                                     datime,
                                     uwoverwrite,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		seqnbr.clear();
  		validflag.clear();
  		tranno.clear();
  		currfrom.clear();
  		currto.clear();
  		opcda.clear();
  		oppc.clear();
  		insprm.clear();
  		agerate.clear();
  		sbstdl.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		jlife.clear();
  		extCommDate.clear();
  		extCessTerm.clear();
  		extCessDate.clear();
  		reasind.clear();
  		znadjperc.clear();
  		premadj.clear();
  		zmortpct.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		uwoverwrite.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getLextrec() {
  		return lextrec;
	}

	public FixedLengthStringData getLextpfRecord() {
  		return lextpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setLextrec(what);
	}

	public void setLextrec(Object what) {
  		this.lextrec.set(what);
	}

	public void setLextpfRecord(Object what) {
  		this.lextpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(lextrec.getLength());
		result.set(lextrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}