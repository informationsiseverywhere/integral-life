package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.newbusiness.tablestructures.Tjl06rec;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.life.newbusiness.screens.Sjl06ScreenVars;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Itmdkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.util.SmartTableDataFactory;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pjl06 extends ScreenProgCS {
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl06.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL06");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Tjl06rec tjl06rec = new Tjl06rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sjl06ScreenVars sv = ScreenProgram.getScreenVars( Sjl06ScreenVars.class);
	private Descpf descpf = new Descpf();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private final ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);	
	private Desckey wsaaDesckey = new Desckey();
	private Itemkey wsaaItemkey = new Itemkey();
	private Itmdkey wsaaItmdkey = new Itmdkey();
	private Itempf itempf = new Itempf();
	Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private String jl28 = "JL28";
	public Pjl06() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl06", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

@Override
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.info("mainline:", e);
		}
	}

@Override
protected void initialise1000()
			{		
					initialise1010();
			}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		wsaaItemkey.set(wsspsmart.itemkey);
		wsaaDesckey.descDescpfx.set(wsaaItemkey.itemItempfx);
		wsaaDesckey.descDesccoy.set(wsaaItemkey.itemItemcoy);
		wsaaDesckey.descDesctabl.set(wsaaItemkey.itemItemtabl);
		wsaaDesckey.descDescitem.set(wsaaItemkey.itemItemitem);
		wsaaDesckey.descItemseq.set(wsaaItemkey.itemItemseq);
		wsaaDesckey.descLanguage.set(wsspcomn.language);
		sv.company.set(wsaaItemkey.itemItemcoy);
		sv.tabl.set(wsaaItemkey.itemItemtabl);
		sv.item.set(wsaaItemkey.itemItemitem);
		wsaaItmdkey.set(wsspsmart.itmdkey);
		descpf=descDAO.getdescData("IT", wsaaItemkey.itemItemtabl.toString(), 
				wsaaItemkey.itemItemitem.toString(), wsaaItemkey.itemItemcoy.toString(), 
				wsspcomn.language.toString());
		if (descpf==null) {
			fatalError600();
		}
		sv.longdesc.set(descpf.getLongdesc());

		itempf = itempfDAO.findItemByItdm(wsaaItemkey.itemItemcoy.toString(), 
				wsaaItemkey.itemItemtabl.toString(),wsaaItemkey.itemItemitem.toString());

		if (itempf == null ) {
			fatalError600();
		}
		
		tjl06rec.tjl06Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		if (isNE(tjl06rec.tjl06Rec, SPACES)) {
			generalArea1045();
		}
		sv.language.set(wsspcomn.language);
		
		
	}


protected void generalArea1045()
	{
	if (isEQ(itempf.getItmfrm(), 0)) {
		sv.itmfrm.set(varcom.vrcmMaxDate);
	} else {
		sv.itmfrm.set(itempf.getItmfrm());
	}
	if (isEQ(itempf.getItmto(), 0)) {
		sv.itmto.set(varcom.vrcmMaxDate);
	} else {
		sv.itmto.set(itempf.getItmto());
	}
	sv.mops.set(tjl06rec.mops);
	sv.billfreqs.set(tjl06rec.billfreqs);
	sv.contitem.set(tjl06rec.contitem);
	sv.minsumin.set(tjl06rec.minsumin);
	sv.minDhb.set(tjl06rec.minDhb);
	sv.minsum.set(tjl06rec.minsum);
	sv.instprms.set(tjl06rec.instprms);
	sv.ctables.set(tjl06rec.ctables);
	sv.hosbens.set(tjl06rec.hosbens);	
	sv.fupcdes.set(tjl06rec.fupcdes);
	sv.cdateduration.set(tjl06rec.cdateduration);
	sv.juvenileage.set(tjl06rec.juvenileage);
	}

@Override
protected void preScreenEdit()
	{
			preStart();
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(Varcom.prot);
		}
		return;
	}

@Override
protected void screenEdit2000()
	{
		screenIo2010();
		exit2090();
				
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(Varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			exit2090();
		}
		if(isEQ(sv.cdateduration, ZERO) || isEQ(sv.cdateduration, SPACES))
			sv.cdatedurationErr.set(jl28);
		if(isEQ(sv.juvenileage, ZERO) || isEQ(sv.juvenileage, SPACES))
			sv.juvenileageErr.set(jl28);
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

@Override
protected void update3000()
	{
		preparation3010();
		updatePrimaryRecord3050();
		updateRecord3055();
				
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			return;
		}
	}

protected void updatePrimaryRecord3050()
	{
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itempf.setTranid(varcom.vrcmCompTranid.trim());
	}

protected void updateRecord3055()
	{
		checkChanges3100();
		itempf.setGenarea(tjl06rec.tjl06Rec.toString().getBytes());
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemtabl(wsaaItemkey.itemItemtabl.toString());
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(wsaaItemkey.itemItemitem.toString());
		itempf.setGenareaj(SmartTableDataFactory.getInstance(appVars.getAppConfig().getSmartTableDataFormat())
				.getGENAREAJString(tjl06rec.tjl06Rec.toString().getBytes(), tjl06rec));
		itempfDAO.updateByKey(itempf, "ITEMPFX, ITEMCOY, ITEMTABL, ITEMITEM");
	}

protected void checkChanges3100()
	{
		/*CHECK*/
	if (isNE(sv.billfreqs,tjl06rec.billfreqs)) {
		tjl06rec.billfreqs.set(sv.billfreqs);
	}
	if (isNE(sv.mops,tjl06rec.mops)) {
		tjl06rec.mops.set(sv.mops);
	}
	if (isNE(sv.itmfrm,itempf.getItmfrm())) {
		itempf.setItmfrm(sv.itmfrm.toInt());
	}
	if (isNE(sv.itmto,itempf.getItmto())) {
		itempf.setItmto(sv.itmto.toInt());
	}
	if (isNE(sv.contitem,tjl06rec.contitem)) {
		tjl06rec.contitem.set(sv.contitem);
	}
	if (isNE(sv.minsumin,tjl06rec.minsumin)) {
		tjl06rec.minsumin.set(sv.minsumin);
	}
	if (isNE(sv.minDhb,tjl06rec.minDhb)) {
		tjl06rec.minDhb.set(sv.minDhb);
	}
	checkAdditionalChanges();
	
		/*EXIT*/
	}

protected void checkAdditionalChanges() {
	if (isNE(sv.minsum,tjl06rec.minsum)) {
		tjl06rec.minsum.set(sv.minsum);
	}
	if (isNE(sv.instprms,tjl06rec.instprms)) {
		tjl06rec.instprms.set(sv.instprms);
	}
	if (isNE(sv.ctables,tjl06rec.ctables)) {
		tjl06rec.ctables.set(sv.ctables);
	}
	if (isNE(sv.hosbens,tjl06rec.hosbens)) {
		tjl06rec.hosbens.set(sv.hosbens);
	}
	if(isNE(sv.fupcdes, tjl06rec.fupcdes)) {
		tjl06rec.fupcdes.set(sv.fupcdes);
	}
	if(isNE(sv.cdateduration, tjl06rec.cdateduration)) {
		tjl06rec.cdateduration.set(sv.cdateduration);
	}
	if(isNE(sv.juvenileage, tjl06rec.juvenileage)) {
		tjl06rec.juvenileage.set(sv.juvenileage);
	}
}

@Override
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
