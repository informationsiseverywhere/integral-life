/*
 * File: Revissai.java
 * Date: 30 August 2009 2:08:47
 * Author: Quipoz Limited
 * 
 * Class transformed from REVISSAI.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.ArrayList; 
import java.util.List; 
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.fsu.general.dataaccess.BextrevTableDAM;
import com.csc.fsu.general.dataaccess.dao.ZfmcpfDAO;
import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.tablestructures.T3620rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AgpyagtTableDAM;
import com.csc.life.agents.dataaccess.AgpydocTableDAM;
import com.csc.life.agents.dataaccess.ZctnTableDAM;
import com.csc.life.agents.dataaccess.ZctnrevTableDAM;
import com.csc.life.agents.dataaccess.ZptnTableDAM;
import com.csc.life.agents.dataaccess.ZptnrevTableDAM;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmrevTableDAM;
import com.csc.life.contractservicing.dataaccess.FluprevTableDAM;
import com.csc.life.contractservicing.dataaccess.RtrnrevTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdrevTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.RskppfDAO;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.flexiblepremium.dataaccess.FpcoTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FprmTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrafiTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrafiTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.InctTableDAM;
import com.csc.life.newbusiness.dataaccess.LextafiTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeafiTableDAM;
import com.csc.life.newbusiness.dataaccess.LinscfiTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrrevTableDAM;
import com.csc.life.newbusiness.dataaccess.PcddafiTableDAM;
import com.csc.life.productdefinition.dataaccess.MliaTableDAM;
import com.csc.life.productdefinition.dataaccess.MliachdTableDAM;
import com.csc.life.productdefinition.dataaccess.model.Zswrpf;//BRD-411
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5689rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.underwriting.dataaccess.UndcafiTableDAM;
import com.csc.life.underwriting.dataaccess.UndlafiTableDAM;
import com.csc.life.underwriting.dataaccess.UndqafiTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrstnudTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.productdefinition.dataaccess.dao.ZswrpfDAO;//BRD-411
import com.csc.life.productdefinition.dataaccess.model.Zswrpf; //BRD-411
/**
* <pre>
*
*REMARKS.
*
*                ALTER FROM INCEPTION
*                --------------------
*
* Overview.
* ---------
*
* This subroutine will perform all the processing necessary
*  to Alter a Contract from Inception. This routine will be
*  called from the Full Contract Reversal AT module, REVGENAT,
*  and will recreate the contract to be in proposal stage.
*
*
* Processing.
* -----------
*
* Update CHDR Contract Header details:
*
*  Read the CHDR file using the CHDRAFI logical view with a key
*  of Company and Contract number from the Linkage
*
*     Set the earliest CHDR record found back to a 'Proposal'
*      state and then DELETE all subsequent CHDR records
*
*     Set status to whatever is set on T5679 entry
*     Update Transaction number
*     Set Paid-to date to zeros
*     Set Billing Renewal date to Billed-to date
*     Set Billed-to date to whatever is specified on T6654
*     Set Transaction Date, Time, User and Terminal-ID
*     Set the 'Available for Issue' flag to 'N'
*
* Reverse Accounting movements ( ACMVs ):
*
*  Read the Account movements file (ACMVPF) using the logical
*  view ACMVREV with a key of contract number/transaction number
*  from linkage.
*
*  For each record found:
*   Write a new record with the following...
*   - Set the accounting month/year with the batch
*   - accounting month/year from linkage.
*   - Set the tranno with the NEW tranno from linkage.
*   - Set the batch key with new batch key from linkage.
*   - Multiply the original currency amount by -1.
*   - Multiply the accounting currency amount by -1.
*   - Set the transaction reference with the NEW
*   - transaction code from linkage.
*   - Set write the transaction description with the long
*   - description of the new trans code from T1688.
*   - Set the effective date with todays date.
*   - Write the new record to the database by calling the
*   - subroutine 'LIFACMV'.
*
* Delete Agent commission records ( AGCMs ):
*
* Read the Agents commission file (AGCMPF) with the logical
*  view AGCMREV using the contract number as the key.
*
*     For each record read which matches on Company & Contract
*     number, DELETE record
*
* Delete Flexible Premium records ( FPRMs ):
*
* Read the Flexible Premium file (FPRMPF) with the logical
*  view FPRM using the contract number as the key.
*
*     For each record read which matches on Company & Contract
*     number, DELETE record
*
* Delete BEXT Billing Extract records:
*
*  Read the BEXT file using the BEXTREV logical view with a key
*  of Company and Contract number from the Linkage and the
*  Billed-to date set to MAX-DATE
*
*     For each record read which matches on Company & Contract
*      number, DELETE record.
*
* Delete Flexible Premium Coverage records ( FPCOs ):
*
*  Read the Flexible Premium Coverage file (FPCOPF) with the
*  logical view FPCO using the contract number as the key.
*
*     For each record read which matches on Company & Contract
*      number, DELETE record
*
* Update PAYR Payer records:
*
*  Read the PAYR file using the PAYRREV logical view with a key
*  of Company and Contract number from the Linkage, the Payer
*  sequence number and transaction number both set to zeros
*
*     Keep the earliest PAYR record found back and then
*      then DELETE all subsequent PAYR records
*      Set paid-to and billed-to dates as per Contract Header
*
* Update COVR Coverage/Rider detail records:
*
*  We want to windback all the COVRs to a proposal state...
*  Read the COVR file using the COVRAFI logical view with a key
*  of Company and Contract number from the Linkage, Life,
*  Coverage,Rider set to Spaces, Plan suffix and Transaction
*  number set to zeros
*
*     For each unique COVR record found,
*
*         1) Call any Generic processing subroutines that
*            are specified in T5671
*
*         2) Write a COVT record using the COVTLNB logical
*             ( Proposal stage coverage/rider detail record )
*
*     Then DELETE all COVRs associated with the Contract being
*      processed.
*
* Delete FLUP Follow up records:
*
*  Read the FLUP file using the FLUPREV logical view with a key
*  of Company and Contract number from the Linkage and the
*  Transaction number set to zeros
*
*     For each record read which matches on Company & Contract
*      number, DELETE record.
*
* Update CLRF Client Roles records:
*
*  Read the CLRF file using the CLRF logical view and DELETE
*   any Beneficiary roles found,
*   the Despatch Address role,
*   the Assignee role if it exists
*
* Update LEXT Life Extras/Options records:
*
*  Read the LEXT file using the LEXTAFI logical view with a key
*  of Company and Contract number from the Linkage, the Life,
*  Coverage and Rider set to spaces, the sequence number and
*  the transaction number set to zeros
*
*     For each unique LEXT record found, keep the earliest and
*      DELETE the rest.
*
* Update LIFE Life detail records:
*
*  Read the LIFE file using the LIFEAFI logical view with a key
*  of Company and Contract number from the Linkage, the Life,
*  and Joint-life set to spaces and the transaction number
*  set to zeros
*
*     For each unique LIFE record found, keep the earliest and
*      set it to a proposal state. DELETE all other LIFE
*      records associated with the contract being processed.
*
* Delete LINS Life Installment records:
*
*  Read the LINS file using the LINSCFI logical view with a key
*  of Company and Contract number from the Linkage.
*
*     For each record read which matches on Company & Contract
*      number, DELETE record.
*
* Update PCDD Proposed Agent Commission/Bonus points split recs:
*
*  Read the PCDD file using the PCDDAFI logical view with a key
*  of Company and Contract number from the Linkage, the Agent
*  number set to spaces and the transaction number set to zeros
*
*     For each unique PCDD record found, keep the earliest and
*      DELETE all other PCDD records associated with the
*      contract being processed.
*
* Reverse Accounting Movement ( RTRN ):
*
* Read the file (RTRNPF) using the logical view RTRNREV with
* a key of contract number/transaction number from linkage.
*
*  For each record found:
*   Write a new record with the following...
*   - Set the accounting month/year with the batch
*   - accounting month/year from linkage.
*   - Set the tranno with the NEW tranno from linkage.
*   - Set the batch key with new batch key from linkage.
*   - Multiply the original currency amount by -1.
*   - Multiply the accounting currency amount by -1.
*   - Set the transaction reference with the NEW
*   - transaction code from linkage.
*   - Set the transaction description with the long
*   - description of the new trans code from T1688.
*   - Set the effective date with todays date.
*   - Write the new record to the database by calling the
*     subroutine 'LIFRTRN'.
*
*
* NOTE.
*   It should be noted that the PTRN record for this transaction,
*    the 'Alter from Inception' one, will be written by the AT
*    module REVGENAT.
*
*  PROGRAMMING NOTE.
********************
*    This subroutine uses the relatively 'new' I/O function
*     WRITD.
*     a) WRITD is used to update a record in place which has
*         had its KEY changed - the record does not need to be
*         LOCKED first
*         Because the Relative Record number hasn't changed by
*          performing the WRITD, NEXTR will then read the next
*          record in the file ( until end-of-file ) and won't
*          pick up and reprocess the one we have just written.
*
*
* Tables used.
* ------------
*
* T1688 - Transaction Codes
* T5671 - Generic processing subroutines
* T5679 - Valid Transaction Statii
* T6654 - Billing Control tablesi
*
******************Enhancements for Life Asia 1.0****************
*
* The table T6654 has been changed to allow a flexible number of
* frequencies to be added to the RCD in order to calculate the
* next billing date. This table is used here as follows :
*
* - If T6654-RENWDATE-1 is 'Y' then use the risk commencement
*   date as BILLCD and adjust for DD cases using T3620 and
*   T5679.
*
* - If T6654-RENWDATE-2 is spaces then use the risk commencement
*   date as BILLCD.
*
* - If T6654-RENWDATE-2 is 'Y' then find that frequency on
*   T6654. Calculate the next billing date using DATCON4.
*   DTC4-INT-DATE-1 is the RCD and DTC4-FREQ-FACTOR is the
*   number of frequencies that is to be added to the RCD. This
*   default next billing date can now be adjusted for DD cases.
*
*
*   * * * * * * *  LIFE ASIA V2.0 ENHANCEMENTS. * * * * * * * *
*
*
*   Delete cash dividend record from HCSD during reversal
*
*   Basic and loaded premium fields added to COVTPF should
*   be updated when writing a new COVT record.
*
*****************************************************************
* </pre>
*/
public class Revissai extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVISSAI";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaFirstChdrDone = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaFirstPayrDone = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaFirstCovrRead = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaFirstPcddRead = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsbbCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsbbRider = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsbbQuestidf = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaSeqnbr = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaNewSeqnbr = new PackedDecimalData(3, 0).init(ZERO);
	private FixedLengthStringData wsaaRole = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaNextBillDate = new PackedDecimalData(8, 0).init(ZERO);
	private PackedDecimalData wsaaConStartDate = new PackedDecimalData(8, 0).init(ZERO);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaPayrBillfreq = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaPayrBillchnl = new FixedLengthStringData(2).init(SPACES);
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaFirstTranno = new PackedDecimalData(5, 0).init(ZERO);
	private PackedDecimalData wsaaPolsum = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaPolinc = new PackedDecimalData(4, 0).init(ZERO);
	private String wsaaInctCreate = "";
	private PackedDecimalData wsaaT6654Sub = new PackedDecimalData(3, 0).init(0);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);

	private FixedLengthStringData wsaaT6661Exist = new FixedLengthStringData(1);
	private Validator t6661Found = new Validator(wsaaT6661Exist, "Y");
	private Validator t6661Nfnd = new Validator(wsaaT6661Exist, "N");
	private FixedLengthStringData wsaaDespnum = new FixedLengthStringData(8).init(SPACES);
		/* WSAA-BILLING-FREQUENCY */
	private ZonedDecimalData wsaaBillfreq = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaChdrPstatcode = new FixedLengthStringData(2).init(SPACES);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4).init(SPACES);

	private FixedLengthStringData wsaaT6654Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6654BillMeth = new FixedLengthStringData(1).isAPartOf(wsaaT6654Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Key, 1).init(SPACES);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT6654Key, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaBtdate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaBtdateYyyy = new ZonedDecimalData(4, 0).isAPartOf(wsaaBtdate, 0).setUnsigned();
	private ZonedDecimalData wsaaBtdateMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaBtdate, 4).setUnsigned();
	private ZonedDecimalData wsaaBtdateDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaBtdate, 6).setUnsigned();
	private ZonedDecimalData wsaaBtdate9 = new ZonedDecimalData(8, 0).isAPartOf(wsaaBtdate, 0, REDEFINE).setUnsigned();
	private ZonedDecimalData wsaaRcd = new ZonedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaDate = new FixedLengthStringData(8).isAPartOf(wsaaRcd, 0, REDEFINE);
	private ZonedDecimalData wsaaMth = new ZonedDecimalData(2, 0).isAPartOf(wsaaDate, 4).setUnsigned();
	private ZonedDecimalData wsaaDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaDate, 6).setUnsigned();
		/* WSAA-MOVEMENT */
	private ZonedDecimalData wsaaEarliest = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaNextUp = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaNextDown = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaT5689Count = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";
		/* ERRORS */
	private static final String e792 = "E792";
	private static final String f035 = "F035";
	private static final String r081 = "R081";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5671 = "T5671";
	private static final String t5679 = "T5679";
	private static final String t6654 = "T6654";
	private static final String t5689 = "T5689";
	private static final String t3620 = "T3620";
	private static final String t6661 = "T6661";
	private static final String th605 = "TH605";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private AgcmrevTableDAM agcmrevIO = new AgcmrevTableDAM();
	private AgpyagtTableDAM agpyagtIO = new AgpyagtTableDAM();
	private AgpydocTableDAM agpydocIO = new AgpydocTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private BextrevTableDAM bextrevIO = new BextrevTableDAM();
	private ChdrafiTableDAM chdrafiIO = new ChdrafiTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	protected CovrafiTableDAM covrafiIO = new CovrafiTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FluprevTableDAM fluprevIO = new FluprevTableDAM();
	private FpcoTableDAM fpcoIO = new FpcoTableDAM();
	private FprmTableDAM fprmIO = new FprmTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	protected HpadTableDAM hpadIO = new HpadTableDAM();
	private InctTableDAM inctIO = new InctTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextafiTableDAM lextafiIO = new LextafiTableDAM();
	private LifeafiTableDAM lifeafiIO = new LifeafiTableDAM();
	private LinscfiTableDAM linscfiIO = new LinscfiTableDAM();
	private MliaTableDAM mliaIO = new MliaTableDAM();
	private MliachdTableDAM mliachdIO = new MliachdTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PayrrevTableDAM payrrevIO = new PayrrevTableDAM();
	private PcddafiTableDAM pcddafiIO = new PcddafiTableDAM();
	private RtrnrevTableDAM rtrnrevIO = new RtrnrevTableDAM();
	private TaxdrevTableDAM taxdrevIO = new TaxdrevTableDAM();
	private UndcafiTableDAM undcafiIO = new UndcafiTableDAM();
	private UndlafiTableDAM undlafiIO = new UndlafiTableDAM();
	private UndqafiTableDAM undqafiIO = new UndqafiTableDAM();
	private ZctnTableDAM zctnIO = new ZctnTableDAM();
	private ZctnrevTableDAM zctnrevIO = new ZctnrevTableDAM();
	private ZptnTableDAM zptnIO = new ZptnTableDAM();
	private ZptnrevTableDAM zptnrevIO = new ZptnrevTableDAM();
	private ZrstnudTableDAM zrstnudIO = new ZrstnudTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	protected Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private Greversrec greversrec = new Greversrec();
	protected Syserrrec syserrrec = new Syserrrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Bldenrlrec bldenrlrec = new Bldenrlrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private T6654rec t6654rec = new T6654rec();
	private T5689rec t5689rec = new T5689rec();
	private T3620rec t3620rec = new T3620rec();
	private Th605rec th605rec = new Th605rec();
	protected Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();
	private ZswrpfDAO  zswrDAO =  getApplicationContext().getBean("ZswrpfDAO", ZswrpfDAO.class);//BRD-411
	
	private boolean isbillday=false;//ILIFE-7820
	private boolean riskPremflag = false; //ILIFE-7845 
	private static final String  RISKPREM_FEATURE_ID="NBPRP094";//ILIFE-7845
	private RskppfDAO rskppfDAO =  getApplicationContext().getBean("rskppfDAO", RskppfDAO.class);
	private static final String  FMC_FEATURE_ID = "BTPRO026";	//IBPLIFE-1433
	private boolean fmcOnFlag = false;	//IBPLIFE-1433
	private ZfmcpfDAO zfmcpfDAO = getApplicationContext().getBean("zfmcpfDAO", ZfmcpfDAO.class);	//IBPLIFE-1433
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected Itempf itempf = null;
	

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		m200Exit, 
		continue3520, 
		exit3590, 
		findMatch3800, 
		dayCheck3800, 
		dayMoveUp3800, 
		dayMoveDown3800, 
		exit3800, 
		continue4100, 
		readNextAcmv4180, 
		exit7190, 
		getNextLext11180, 
		exit11190, 
		getNextPcdd14180, 
		exit14190, 
		call19020, 
		exit19090, 
		call20020, 
		exit20090, 
		m300Call, 
		m300Exit
	}

	public Revissai() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		initialise2000();
		processChdr3000();
		processAcmv4000();
		processAcmvOptical4010();
		processAgcm5000();
		processFprm5500();
		processBext6000();
		processFpco6500();
		processPayr7000();
		processCovr8000();
		processFlup9000();
		processClrf10000();
		processLext11000();
		processLife12000();
		processLins13000();
		processTaxd13200();
		processPcdd14000();
		processZafrN200();//BRD-411
		/* PERFORM 15000-PROCESS-RACT.                                  */
		processRtrn16000();
		processUndl1a000();
		processUndc1b000();
		processUndq1c000();
		if (isEQ(wsaaInctCreate, "Y")) {
			processInct17000();
		}
		processZrst18000();
		//IBPLIFE-1433 Starts
		fmcOnFlag = FeaConfg.isFeatureExist(reverserec.company.toString(), FMC_FEATURE_ID, appVars, "IT");
		if (fmcOnFlag) {
			processzfmcN100();
		}
		//IBPLIFE-1433 End
		processZdivCustSpecific();
		/* Read table TH605 to see if Bonus Workbench Extraction used      */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(th605);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemitem(reverserec.company);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError99000();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
		if (isNE(th605rec.bonusInd, "Y")) {
			return ;
		}
		/* Bonus Workbench  *                                              */
		processZptn19000();
		processZctn20000();
		riskPremflag = FeaConfg.isFeatureExist(reverserec.company.toString(), RISKPREM_FEATURE_ID, appVars, "IT");
        if(riskPremflag) {
        	resetRiskPrem();
        }
	}
//BRD-411
protected void resetRiskPrem() {
	rskppfDAO.reverseContract(reverserec.company.toString(), reverserec.chdrnum.toString());
}

protected void processzfmcN100() {
	zfmcpfDAO.deleteZfmcPF(reverserec.company.toString(), reverserec.chdrnum.toString()); //IBPLIFE-1433
}

protected void processZafrN200() { 

	List<Zswrpf> zswrpfiList=new ArrayList<>();
    Zswrpf zw=new Zswrpf();
    zw.setChdrpfx("CH");
    zw.setChdrcoy(reverserec.company.toString());
    zw.setChdrnum(reverserec.chdrnum.toString()); 
    zw.setValidflag("1" );
    zswrpfiList=zswrDAO.getZswrpfData( zw.getChdrnum(),zw.getChdrcoy(),  zw.getChdrpfx(),  zw.getValidflag());/* IJTI-1523 */       

     if (zswrpfiList.size()>0 )
    {
           for (Zswrpf p : zswrpfiList) 
            {                    
        	   zswrDeletesN300();
           }
    }
}
protected void zswrDeletesN300()
{
	startN310();
}

protected void startN310()
{
	
	
		    List<Zswrpf> zswrpfiList=new ArrayList<>();
		    Zswrpf zw=new Zswrpf();
		    zw.setChdrpfx("CH");
		    zw.setChdrcoy(reverserec.company.toString());
		    zw.setChdrnum(reverserec.chdrnum.toString()); 
		    zw.setValidflag("1" );
		    boolean m=zswrDAO.deleteZswrpf( zw.getChdrnum(),zw.getChdrcoy(),  zw.getChdrpfx());/* IJTI-1523 */

			
}//BRD-411

protected void exit1090()
	{
		exitProgram();
	}

protected void initialise2000()
	{
		start2000();
	}

protected void start2000()
	{
		/* MOVE ZEROS                  TO CLNK-CURRTO.          <V5L007>*/
		/* MOVE ZEROS                  TO CLNK-TARGET-PREM.     <V5L007>*/
		/* MOVE ZEROS                  TO CLNK-SEQNO.           <V5L007>*/
		wsaaTransDesc.set(SPACES);
		wsaaBatckey.set(reverserec.batchkey);
		wsaaFirstChdrDone.set(SPACES);
		wsaaFirstPayrDone.set(SPACES);
		wsaaFirstPcddRead.set(SPACES);
		wsaaLife.set(SPACES);
		wsaaJlife.set(SPACES);
		wsaaCoverage.set(SPACES);
		wsaaRider.set(SPACES);
		wsaaPlanSuffix.set(ZERO);
		wsaaSeqnbr.set(ZERO);
		wsaaNewSeqnbr.set(ZERO);
		wsaaRole.set(SPACES);
		wsaaChdrPstatcode.set(SPACES);
		/* get todays date*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			systemError99000();
		}
		wsaaToday.set(datcon1rec.intDate);
		/* get transaction description for putting on ACMV later on.*/
		getDescription2100();
		/* read T5679 to get valid transaction status codes*/
		readT56792200();
		
		isbillday = FeaConfg.isFeatureExist(reverserec.company.toString(), "NBPRP011", appVars, "IT");//ILIFE-7820
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
	}

protected void getDescription2100()
	{
		start2100();
	}

protected void start2100()
	{
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			wsaaTransDesc.fill("?");
		}
		else {
			wsaaTransDesc.set(descIO.getLongdesc());
		}
	}

protected void readT56792200()
	{
		start2200();
	}

protected void start2200()
	{
		/* Read table T5679 to get transaction statii*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5679);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemitem(reverserec.batctrcde);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void processChdr3000()
	{
		/*START*/
		chdrafiIO.setParams(SPACES);
		chdrafiIO.setChdrcoy(reverserec.company);
		chdrafiIO.setChdrnum(reverserec.chdrnum);
		chdrafiIO.setTranno(ZERO);
		chdrafiIO.setFormat(formatsInner.chdrafirec);
		chdrafiIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrafiIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdrafiIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(chdrafiIO.getStatuz(),varcom.endp))) {
			updDelChdrs3100();
		}
		
		m100UpdHpad();
		m300UpdateAgpy();
		/*EXIT*/
	}

protected void updDelChdrs3100()
	{
		start3100();
	}

protected void start3100()
	{
		SmartFileCode.execute(appVars, chdrafiIO);
		if (isNE(chdrafiIO.getStatuz(), varcom.oK)
		&& isNE(chdrafiIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrafiIO.getParams());
			syserrrec.statuz.set(chdrafiIO.getStatuz());
			systemError99000();
		}
		if (isEQ(chdrafiIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* Check Contract Header CHDR record is associated with the*/
		/*  contract we are processing.*/
		if (isNE(reverserec.company, chdrafiIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, chdrafiIO.getChdrnum())) {
			chdrafiIO.setStatuz(varcom.endp);
			return ;
		}
		/* A correct match on Company & Contract number has now been*/
		/*  found - IF this is the 1st time through, then we have found*/
		/*  the earliest CHDR record for this Contract so reset to a*/
		/*  proposal stage. If this isn't the 1st record found, then*/
		/*  DELETe it.*/
		wsaaDespnum.set(chdrafiIO.getDespnum());
		getAppVars().addDiagnostic("WSAA-DESPNUM: "+wsaaDespnum);
		if (isEQ(wsaaFirstChdrDone, SPACES)) {
			chdrProposal3200();
			wsaaFirstChdrDone.set("Y");
			chdrafiIO.setFunction(varcom.writd);
		}
		else {
			/*        lock record before deleting it*/
			chdrafiIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, chdrafiIO);
			if (isNE(chdrafiIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrafiIO.getParams());
				syserrrec.statuz.set(chdrafiIO.getStatuz());
				systemError99000();
			}
			chdrafiIO.setFunction(varcom.delet);
		}
		SmartFileCode.execute(appVars, chdrafiIO);
		if (isNE(chdrafiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrafiIO.getParams());
			syserrrec.statuz.set(chdrafiIO.getStatuz());
			systemError99000();
		}
		chdrafiIO.setFunction(varcom.nextr);
	}

protected void chdrProposal3200()
	{
		start3200();
	}

protected void start3200()
	{
		chdrafiIO.setValidflag("3");
		if (isNE(t5679rec.setCnRiskStat, SPACES)) {
			chdrafiIO.setStatcode(t5679rec.setCnRiskStat);
		}
		if (isEQ(chdrafiIO.getBillfreq(), "00")) {
			if (isNE(t5679rec.setSngpCnStat, SPACES)) {
				chdrafiIO.setPstatcode(t5679rec.setSngpCnStat);
			}
		}
		else {
			if (isNE(t5679rec.setCnPremStat, SPACES)) {
				chdrafiIO.setPstatcode(t5679rec.setCnPremStat);
			}
		}
		wsaaChdrPstatcode.set(chdrafiIO.getPstatcode());
		chdrafiIO.setTranno(reverserec.newTranno);
		chdrafiIO.setAvlisu("N");
		chdrafiIO.setStattran(reverserec.newTranno);
		chdrafiIO.setPstattran(reverserec.newTranno);
		chdrafiIO.setStatdate(reverserec.effdate1);
		chdrafiIO.setPstatdate(reverserec.effdate1);
		chdrafiIO.setPtdate(ZERO);
		wsaaConStartDate.set(chdrafiIO.getCcdate());
		chdrafiIO.setStatementDate(ZERO);
		varcom.vrcmCompTermid.set(reverserec.termid);
		varcom.vrcmUser.set(reverserec.user);
		varcom.vrcmDate.set(reverserec.transDate);
		varcom.vrcmTime.set(reverserec.transTime);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrafiIO.setTranid(varcom.vrcmCompTranid);
		wsaaCnttype.set(chdrafiIO.getCnttype());
		wsaaCntcurr.set(chdrafiIO.getCntcurr());
		chdrafiIO.setSinstfrom(ZERO);
		chdrafiIO.setSinstto(ZERO);
		chdrafiIO.setInstfrom(ZERO);
		chdrafiIO.setInstto(ZERO);
		chdrafiIO.setNofoutinst(ZERO);
		chdrafiIO.setOutstamt(ZERO);
		chdrafiIO.setLastSwitchDate(ZERO);
		chdrafiIO.setFreeSwitchesUsed(ZERO);
		chdrafiIO.setFreeSwitchesLeft(ZERO);
		wsaaPolsum.set(chdrafiIO.getPolsum());
		wsaaPolinc.set(chdrafiIO.getPolinc());
		for (wsaaCount.set(1); !(isGT(wsaaCount, 6)); wsaaCount.add(1)){
			chdrafiIO.setSinstamt(wsaaCount, ZERO);
			chdrafiIO.setInsttot(wsaaCount, ZERO);
			chdrafiIO.setInstpast(wsaaCount, ZERO);
			chdrafiIO.setIsam(wsaaCount, ZERO);
		}
		for (wsaaCount.set(1); !(isGT(wsaaCount, 4)); wsaaCount.add(1)){
			chdrafiIO.setBilldate(wsaaCount, ZERO);
			chdrafiIO.setBillamt(wsaaCount, ZERO);
		}
		m100ReadMliachd();
		currentPayrDetails3400();
		billingDates3500();
	}

protected void m100ReadMliachd()
	{
		/*M100-START*/
		mliachdIO.setStatuz(varcom.oK);
		mliachdIO.setMlentity(chdrafiIO.getChdrnum());
		mliachdIO.setFormat(formatsInner.mliachdrec);
		mliachdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		mliachdIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		while ( !(isEQ(mliachdIO.getStatuz(),varcom.endp))) {
			m200ReadMliachdio();
		}
		
		/*M100-EXIT*/
	}

protected void m200ReadMliachdio()
	{
		try {
			m200Start();
			m200Nextr();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void m200Start()
	{
		SmartFileCode.execute(appVars, mliachdIO);
		if (isNE(mliachdIO.getStatuz(), varcom.oK)
		&& isNE(mliachdIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(mliachdIO.getStatuz());
			syserrrec.params.set(mliachdIO.getParams());
			systemError99000();
		}
		if (isNE(chdrafiIO.getChdrnum(), mliachdIO.getMlentity())) {
			mliachdIO.setStatuz(varcom.endp);
		}
		if (isEQ(mliachdIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.m200Exit);
		}
		mliaIO.setSecurityno(mliachdIO.getSecurityno());
		mliaIO.setMlentity(mliachdIO.getMlentity());
		mliaIO.setFormat(formatsInner.mliarec);
		mliaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, mliaIO);
		if (isNE(mliaIO.getStatuz(), varcom.oK)
		&& isNE(mliaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(mliaIO.getStatuz());
			syserrrec.params.set(mliaIO.getParams());
			systemError99000();
		}
		if (isEQ(mliaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(mliaIO.getStatuz());
			syserrrec.params.set(mliaIO.getParams());
			systemError99000();
		}
		if (isEQ(mliaIO.getMind(), "N")) {
			mliaIO.setFunction(varcom.delet);
		}
		else {
			/*       MOVE MLIACHD-MLCOYCDE    TO MLIA-MLCOYCDE                 */
			/*       MOVE MLIACHD-MLOLDIC     TO MLIA-MLOLDIC                  */
			/*       MOVE MLIACHD-MLOTHIC     TO MLIA-MLOTHIC                  */
			/*       MOVE MLIACHD-CLNTNAML    TO MLIA-CLNTNAML                 */
			/*       MOVE MLIACHD-DOB         TO MLIA-DOB                      */
			/*       MOVE MLIACHD-RSKFLG      TO MLIA-RSKFLG                   */
			/*       MOVE MLIACHD-HPROPDTE    TO MLIA-HPROPDTE                 */
			/*       MOVE MLIACHD-MLHSPERD    TO MLIA-MLHSPERD                 */
			/*       MOVE MLIACHD-MLMEDCDE01  TO MLIA-MLMEDCDE01               */
			/*       MOVE MLIACHD-MLMEDCDE02  TO MLIA-MLMEDCDE02               */
			/*       MOVE MLIACHD-MLMEDCDE03  TO MLIA-MLMEDCDE03               */
			mliaIO.setIndc("Y");
			mliaIO.setMind("N");
			mliaIO.setActn("3");
			/*       MOVE TDAY                TO DTC1-FUNCTION                 */
			/*       CALL 'DATCON1' USING DTC1-DATCON1-REC                     */
			/*       MOVE DTC1-INT-DATE       TO WSAA-TODAY                    */
			/*       MOVE WSAA-TODAY          TO MLIA-EFFDATE                  */
			/*       MOVE MLIAREC             TO MLIA-FORMAT                   */
			mliaIO.setFunction(varcom.rewrt);
		}
		SmartFileCode.execute(appVars, mliaIO);
		if (isNE(mliaIO.getStatuz(), varcom.oK)
		&& isNE(mliaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(mliaIO.getStatuz());
			syserrrec.params.set(mliaIO.getParams());
			systemError99000();
		}
	}

protected void m200Nextr()
	{
		mliachdIO.setFunction(varcom.nextr);
	}

protected void currentPayrDetails3400()
	{
		start3400();
	}

protected void start3400()
	{
		/* Read PAYR file to get billing channel & frequency etc..*/
		payrrevIO.setParams(SPACES);
		payrrevIO.setChdrcoy(reverserec.company);
		payrrevIO.setChdrnum(reverserec.chdrnum);
		payrrevIO.setPayrseqno(ZERO);
		payrrevIO.setTranno(ZERO);
		payrrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		payrrevIO.setFormat(formatsInner.payrrevrec);
		SmartFileCode.execute(appVars, payrrevIO);
		if (isNE(payrrevIO.getStatuz(), varcom.oK)
		&& isNE(payrrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrrevIO.getParams());
			syserrrec.statuz.set(payrrevIO.getStatuz());
			systemError99000();
		}
		if (isNE(reverserec.company, payrrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, payrrevIO.getChdrnum())
		|| isEQ(payrrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrrevIO.getParams());
			syserrrec.statuz.set(e792);
			systemError99000();
		}
	}

protected void billingDates3500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3500();
				case continue3520: 
					continue3520();
				case exit3590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected boolean readT6654600(String key)
{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(reverserec.company.toString());
	itempf.setItemtabl(t6654);
	itempf.setItemitem(key);
	itempf = itempfDAO.getItempfRecord(itempf);
	if (null != itempf) {
		t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		return true;
	}
	return false;
}

protected void start3500()
	{
		/* Read T6654 Billing control table so that we can set the*/
		/*  BTDATE and the BILLCD correctly.*/
		String key = payrrevIO.getBillchnl().toString().trim()+wsaaCnttype.toString().trim();
		if (BTPRO028Permission) {
			key = key + payrrevIO.getBillfreq().toString().trim(); 
			if (!readT6654600(key)) {
				key = payrrevIO.getBillchnl().toString().trim()+wsaaCnttype.toString().trim();
				key= key.concat("**");
				if (!readT6654600(key)) {
					key = payrrevIO.getBillchnl().toString().trim().concat("*****");
					if (!readT6654600(key)) {
						syserrrec.params.set(itemIO.getParams());
						syserrrec.statuz.set(f035);
						systemError99000();
					}
				}
			}
		}
		else {
			itemIO.setParams(SPACES);
			itemIO.setItemtabl(t6654);
			itemIO.setItemcoy(reverserec.company);
			itemIO.setItempfx("IT");
			itemIO.setFunction(varcom.readr);
			itemIO.setFormat(formatsInner.itemrec);
			wsaaT6654Key.set(SPACES);
			wsaaT6654BillMeth.set(payrrevIO.getBillchnl());
			wsaaT6654Cnttype.set(wsaaCnttype);
			itemIO.setItemitem(wsaaT6654Key);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				systemError99000();
			}
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				/*     do generic read instead*/
				itemIO.setParams(SPACES);
				itemIO.setItemtabl(t6654);
				itemIO.setItemcoy(reverserec.company);
				itemIO.setItempfx("IT");
				itemIO.setFunction(varcom.readr);
				itemIO.setFormat(formatsInner.itemrec);
				wsaaT6654Key.set(SPACES);
				wsaaT6654BillMeth.set(payrrevIO.getBillchnl());
				wsaaT6654Cnttype.set("***");
				itemIO.setItemitem(wsaaT6654Key);
				SmartFileCode.execute(appVars, itemIO);
				if (isNE(itemIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(itemIO.getParams());
					syserrrec.statuz.set(f035);
					systemError99000();
				}
			}
			t6654rec.t6654Rec.set(itemIO.getGenarea());
		}
		/* SIMPLIFY THE CALCULATION OF FIRST BILLING DATE TO               */
		/* RISK COMMENCEMENT DATE IF SINGLE PREMIIM.                       */
		if (isEQ(chdrafiIO.getBillfreq(), "00")) {
			chdrafiIO.setBillcd(chdrafiIO.getOccdate());
			wsaaNextBillDate.set(chdrafiIO.getOccdate());
			goTo(GotoLabel.exit3590);
		}
		/* The rest of the processing replaces the commented out           */
		/* processing. Now the table T6654 is now more flexible and the    */
		/* dates will be calculated using the frequency and increments.    */
		if (isEQ(t6654rec.renwdate1, "Y")) {
			wsaaBtdate9.set(chdrafiIO.getCcdate());
			goTo(GotoLabel.continue3520);
		}
		if (isNE(t6654rec.renwdate2, "Y")) {
			chdrafiIO.setBtdate(chdrafiIO.getCcdate());
			chdrafiIO.setBillcd(chdrafiIO.getCcdate());
			wsaaNextBillDate.set(chdrafiIO.getCcdate());
			goTo(GotoLabel.continue3520);
		}
		wsaaT6654Sub.set(1);
		if(!BTPRO028Permission) {
		for (wsaaT6654Sub.set(1); !(isGT(wsaaT6654Sub, 6)
		|| isEQ(t6654rec.zrfreq[wsaaT6654Sub.toInt()], payrrevIO.getBillfreq())); wsaaT6654Sub.add(1)){
			a100ReadFreqs();
		}
		if (isGT(wsaaT6654Sub, 6)) {
			syserrrec.params.set(chdrafiIO.getParams());
			syserrrec.statuz.set(r081);
			systemError99000();
		}
		}
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(chdrafiIO.getCcdate());
		datcon2rec.intDate2.set(ZERO);
		datcon2rec.frequency.set(t6654rec.zrfreq[wsaaT6654Sub.toInt()]);
		datcon2rec.freqFactor.set(t6654rec.zrincr[wsaaT6654Sub.toInt()]);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			systemError99000();
		}
		wsaaBtdate9.set(datcon2rec.intDate2);
	}

protected void continue3520()
	{
		readT36203600();
		readT56893700();
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itemIO.getStatuz(), varcom.oK)) {
			chdrafiIO.setBtdate(wsaaBtdate9);
			chdrafiIO.setBillcd(wsaaBtdate9);
			wsaaNextBillDate.set(wsaaBtdate9);
			return ;
		}
		wsaaEarliest.set(99);
		wsaaNextUp.set(99);
		wsaaNextDown.set(ZERO);
		chdrafiIO.setBtdate(varcom.vrcmMaxDate);
		for (wsaaT5689Count.set(1); !((isNE(chdrafiIO.getBtdate(), varcom.vrcmMaxDate))
		|| (isGT(wsaaT5689Count, 30))); wsaaT5689Count.add(1)){
			searchForTableMatch3800();
		}
		if (isEQ(chdrafiIO.getBtdate(), varcom.vrcmMaxDate)) {
			if (isEQ(wsaaNextUp, 99)) {
				if (isEQ(wsaaNextDown, 0)) {
					/*NEXT_SENTENCE*/
				}
				else {
					if (isNE(t3620rec.ddind, SPACES)
					|| isNE(t3620rec.grpind, SPACES)) {
						/*             MOVE WSAA-NEXT-DOWN  TO WSAA-BTDATE-DD           */
						wsaaBtdateDd.set(wsaaNextUp);
						if (isEQ(wsaaNextUp, "99")) {
							wsaaBtdateDd.set(wsaaEarliest);
							wsaaBtdateMm.add(1);
							if (isGT(wsaaBtdateMm, 12)) {
								wsaaBtdateMm.set(1);
								wsaaBtdateYyyy.add(1);
							}
						}
					}
					else {
						wsaaBtdateDd.set(wsaaEarliest);
						wsaaBtdateMm.add(1);
						if (isGT(wsaaBtdateMm, 12)) {
							wsaaBtdateMm.set(1);
							wsaaBtdateYyyy.add(1);
						}
					}
					chdrafiIO.setBtdate(wsaaBtdate9);
				}
			}
			else {
				wsaaBtdateDd.set(wsaaNextUp);
				chdrafiIO.setBtdate(wsaaBtdate9);
			}
		}
		/*    MOVE WSAA-BTDATE              TO CHDRAFI-BTDATE.          */
		/*   Adjust for month end                                          */
		wsaaRcd.set(chdrafiIO.getBtdate());
		if ((isEQ(wsaaMth, 4)
		|| isEQ(wsaaMth, 6)
		|| isEQ(wsaaMth, 9)
		|| isEQ(wsaaMth, 11))
		&& isEQ(wsaaDay, 31)) {
			wsaaDay.set(30);
			chdrafiIO.setBtdate(wsaaRcd);
		}
		if (isEQ(wsaaMth, 2)
		&& (isEQ(wsaaDay, 29)
		|| isEQ(wsaaDay, 30)
		|| isEQ(wsaaDay, 31))) {
			wsaaDay.set(29);
			chdrafiIO.setBtdate(wsaaRcd);
			datcon1rec.intDate.set(wsaaRcd);
			datcon1rec.function.set(varcom.conv);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz, varcom.oK)) {
				wsaaDay.set(28);
				chdrafiIO.setBtdate(wsaaRcd);
			}
		}
		//ILIFE-7820
		if(!isbillday) {
			chdrafiIO.setBillcd(chdrafiIO.getBtdate());
		}
		wsaaNextBillDate.set(chdrafiIO.getBtdate());
	}

protected void readT36203600()
	{
		start3600();
	}

protected void start3600()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t3620);
		itemIO.setItemitem(payrrevIO.getBillchnl());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			t3620rec.t3620Rec.set(itemIO.getGenarea());
		}
	}

protected void readT56893700()
	{
		start3700();
	}

protected void start3700()
	{
		itdmIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itdmIO.setItemcoy(reverserec.company);
		itdmIO.setItemtabl(t5689);
		itdmIO.setItemitem(wsaaCnttype);
		itdmIO.setItmfrm(chdrafiIO.getCcdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError99500();
		}
		if (isNE(itdmIO.getItemcoy(), reverserec.company)
		|| isNE(itdmIO.getItemtabl(), t5689)
		|| isNE(itdmIO.getItemitem(), wsaaCnttype)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setStatuz(varcom.endp);
		}
		else {
			t5689rec.t5689Rec.set(itdmIO.getGenarea());
		}
	}

protected void searchForTableMatch3800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3800();
				case findMatch3800: 
					findMatch3800();
				case dayCheck3800: 
					dayCheck3800();
				case dayMoveUp3800: 
					dayMoveUp3800();
				case dayMoveDown3800: 
					dayMoveDown3800();
				case exit3800: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3800()
	{
		if (isEQ(t5689rec.billfreq[wsaaT5689Count.toInt()], chdrafiIO.getBillfreq())
		&& isEQ(t5689rec.mop[wsaaT5689Count.toInt()], payrrevIO.getBillchnl())) {
			goTo(GotoLabel.findMatch3800);
		}
		else {
			goTo(GotoLabel.exit3800);
		}
	}

protected void findMatch3800()
	{
		if (isEQ(t5689rec.rendd[wsaaT5689Count.toInt()], "99")) {
			chdrafiIO.setBtdate(wsaaBtdate9);
			goTo(GotoLabel.exit3800);
		}
		else {
			goTo(GotoLabel.dayCheck3800);
		}
	}

protected void dayCheck3800()
	{
		if (isEQ(t5689rec.rendd[wsaaT5689Count.toInt()], wsaaBtdateDd)) {
			chdrafiIO.setBtdate(wsaaBtdate9);
			goTo(GotoLabel.exit3800);
		}
		else {
			if (isLT(t5689rec.rendd[wsaaT5689Count.toInt()], wsaaEarliest)) {
				wsaaEarliest.set(t5689rec.rendd[wsaaT5689Count.toInt()]);
			}
			if (isLT(wsaaBtdateDd, t5689rec.rendd[wsaaT5689Count.toInt()])) {
				goTo(GotoLabel.dayMoveUp3800);
			}
			else {
				goTo(GotoLabel.dayMoveDown3800);
			}
		}
	}

protected void dayMoveUp3800()
	{
		if (isLT(t5689rec.rendd[wsaaT5689Count.toInt()], wsaaNextUp)) {
			wsaaNextUp.set(t5689rec.rendd[wsaaT5689Count.toInt()]);
			goTo(GotoLabel.exit3800);
		}
		else {
			goTo(GotoLabel.exit3800);
		}
	}

protected void dayMoveDown3800()
	{
		if (isGT(t5689rec.rendd[wsaaT5689Count.toInt()], wsaaNextDown)) {
			wsaaNextDown.set(t5689rec.rendd[wsaaT5689Count.toInt()]);
		}
		else {
			return ;
		}
	}

protected void processAcmv4000()
	{
		start4000();
	}

protected void start4000()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs4100();
		}
		
	}

protected void processAcmvOptical4010()
	{
		start4010();
	}

protected void start4010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			databaseError99500();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs4100();
		}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				databaseError99500();
			}
		}
	}

protected void reverseAcmvs4100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start4100();
				case continue4100: 
					continue4100();
				case readNextAcmv4180: 
					readNextAcmv4180();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start4100()
	{
		/* check If ACMV just read has same trancode as transaction*/
		/*  we are trying to reverse*/
		itemIO.setParams(SPACES);
		wsaaT6661Exist.set("N");
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(acmvrevIO.getRldgcoy());
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(acmvrevIO.getBatctrcde());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT6661Exist.set("N");
		}
		else {
			if (isEQ(itemIO.getStatuz(), varcom.oK)) {
				wsaaT6661Exist.set("Y");
			}
		}
		if (t6661Nfnd.isTrue()) {
			goTo(GotoLabel.continue4100);
		}
		if (isNE(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			goTo(GotoLabel.readNextAcmv4180);
		}
	}

	/**
	* <pre>
	* post equal & opposite amount to ACMV just read
	* </pre>
	*/
protected void continue4100()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), -1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), -1));
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		/* MOVE ACMVREV-FRCDATE        TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		if (t6661Nfnd.isTrue()) {
			lifacmvrec.suprflag.set("Y");
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError99000();
		}
	}

protected void readNextAcmv4180()
	{
		/*  Read the next ACMV record.*/
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'            USING ACMVREV-PARAMS.            */
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processAgcm5000()
	{
		/*START*/
		agcmrevIO.setParams(SPACES);
		agcmrevIO.setChdrnum(reverserec.chdrnum);
		agcmrevIO.setChdrcoy(reverserec.company);
		agcmrevIO.setPlanSuffix(ZERO);
		agcmrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(agcmrevIO.getStatuz(),varcom.endp))) {
			deleteAgcms5100();
		}
		
		/*EXIT*/
	}

protected void deleteAgcms5100()
	{
		start5100();
	}

protected void start5100()
	{
		SmartFileCode.execute(appVars, agcmrevIO);
		if (isNE(agcmrevIO.getStatuz(), varcom.oK)
		&& isNE(agcmrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmrevIO.getParams());
			syserrrec.statuz.set(agcmrevIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(agcmrevIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* check AGCM record read is associated with contract*/
		/* - If not, Release record*/
		if (isNE(reverserec.company, agcmrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, agcmrevIO.getChdrnum())) {
			agcmrevIO.setStatuz(varcom.endp);
			return ;
		}
		/* Get here, so AGCM exists for contract, so we will now read*/
		/*  lock record and then delete it.*/
		agcmrevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, agcmrevIO);
		if (isNE(agcmrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmrevIO.getParams());
			syserrrec.statuz.set(agcmrevIO.getStatuz());
			databaseError99500();
		}
		agcmrevIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, agcmrevIO);
		if (isNE(agcmrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmrevIO.getParams());
			syserrrec.statuz.set(agcmrevIO.getStatuz());
			databaseError99500();
		}
		/* Now get next record*/
		agcmrevIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	**********************************************************<D9604>
	* </pre>
	*/
protected void processFprm5500()
	{
		/*START*/
		/*                                                         <D9604>*/
		fprmIO.setParams(SPACES);
		fprmIO.setChdrcoy(reverserec.company);
		fprmIO.setChdrnum(reverserec.chdrnum);
		fprmIO.setPayrseqno(ZERO);
		fprmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fprmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fprmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(fprmIO.getStatuz(),varcom.endp))) {
			deleteFprms5550();
		}
		
		/*EXIT*/
	}

	/**
	* <pre>
	*                                                         <D9604>
	**********************************************************<D9604>
	* </pre>
	*/
protected void deleteFprms5550()
	{
		start5550();
	}

	/**
	* <pre>
	**********************************************************<D9604>
	*                                                         <D9604>
	* </pre>
	*/
protected void start5550()
	{
		/*                                                         <D9604>*/
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)
		&& isNE(fprmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(fprmIO.getParams());
			syserrrec.statuz.set(fprmIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(fprmIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/*                                                         <D9604>*/
		/* check FPRM record read is associated with contract      <D9604>*/
		/* - If not, Release record                                <D9604>*/
		/*                                                         <D9604>*/
		if (isNE(reverserec.company, fprmIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, fprmIO.getChdrnum())) {
			fprmIO.setStatuz(varcom.endp);
			return ;
		}
		/*                                                         <D9604>*/
		/* Get here, so FPRM exists for contract, so we will now re<D9604>*/
		/*  lock record and then delete it.                        <D9604>*/
		/*                                                         <D9604>*/
		fprmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fprmIO.getParams());
			syserrrec.statuz.set(fprmIO.getStatuz());
			databaseError99500();
		}
		/*                                                         <D9604>*/
		fprmIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fprmIO.getParams());
			syserrrec.statuz.set(fprmIO.getStatuz());
			databaseError99500();
		}
		/*                                                         <D9604>*/
		/* Now get next record                                     <D9604>*/
		/*                                                         <D9604>*/
		fprmIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	*                                                         <D9604>
	* </pre>
	*/
protected void processBext6000()
	{
		/*START*/
		/* Delete any Billing Extract (BEXT) records associated with*/
		/*  the contract we are CFIing.*/
		bextrevIO.setParams(SPACES);
		bextrevIO.setFormat(formatsInner.bextrevrec);
		bextrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		bextrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		bextrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		bextrevIO.setChdrcoy(reverserec.company);
		bextrevIO.setChdrnum(reverserec.chdrnum);
		bextrevIO.setBtdate(varcom.vrcmMaxDate);
		while ( !(isEQ(bextrevIO.getStatuz(), varcom.endp))) {
			bextDeletes6100();
		}
		
		/*EXIT*/
	}

protected void bextDeletes6100()
	{
		start6100();
	}

protected void start6100()
	{
		SmartFileCode.execute(appVars, bextrevIO);
		if (isNE(bextrevIO.getStatuz(), varcom.oK)
		&& isNE(bextrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(bextrevIO.getParams());
			syserrrec.statuz.set(bextrevIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(bextrevIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* check BEXT record read is associated with contract*/
		/* - If not, Release record*/
		if (isNE(reverserec.company, bextrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, bextrevIO.getChdrnum())) {
			bextrevIO.setStatuz(varcom.endp);
			return ;
		}
		/* Get here, so BEXT exists for contract, so we will now read*/
		/*  lock record and then delete it*/
		bextrevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, bextrevIO);
		if (isNE(bextrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bextrevIO.getParams());
			syserrrec.statuz.set(bextrevIO.getStatuz());
			databaseError99500();
		}
		bextrevIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, bextrevIO);
		if (isNE(bextrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bextrevIO.getParams());
			syserrrec.statuz.set(bextrevIO.getStatuz());
			databaseError99500();
		}
		/* Now get next record*/
		bextrevIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	**********************************************************<D9604>
	* </pre>
	*/
protected void processFpco6500()
	{
		/*START*/
		/*                                                         <D9604>*/
		fpcoIO.setParams(SPACES);
		fpcoIO.setChdrcoy(reverserec.company);
		fpcoIO.setChdrnum(reverserec.chdrnum);
		fpcoIO.setPlanSuffix(ZERO);
		fpcoIO.setTargfrom(ZERO);
		fpcoIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fpcoIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fpcoIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(fpcoIO.getStatuz(),varcom.endp))) {
			deleteFpcos6550();
		}
		
		/*EXIT*/
	}

	/**
	* <pre>
	*                                                         <D9604>
	**********************************************************<D9604>
	* </pre>
	*/
protected void deleteFpcos6550()
	{
		start6550();
	}

	/**
	* <pre>
	**********************************************************<D9604>
	*                                                         <D9604>
	* </pre>
	*/
protected void start6550()
	{
		/*                                                         <D9604>*/
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)
		&& isNE(fpcoIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(fpcoIO.getParams());
			syserrrec.statuz.set(fpcoIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(fpcoIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/*                                                         <D9604>*/
		/* check FPCO record read is associated with contract      <D9604>*/
		/* - If not, Release record                                <D9604>*/
		/*                                                         <D9604>*/
		if (isNE(reverserec.company, fpcoIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, fpcoIO.getChdrnum())) {
			fpcoIO.setStatuz(varcom.endp);
			return ;
		}
		/*                                                         <D9604>*/
		/* Get here, so FPCO exists for contract, so we will now re<D9604>*/
		/*  lock record and then delete it.                        <D9604>*/
		/*                                                         <D9604>*/
		fpcoIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcoIO.getParams());
			syserrrec.statuz.set(fpcoIO.getStatuz());
			databaseError99500();
		}
		/*                                                         <D9604>*/
		fpcoIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcoIO.getParams());
			syserrrec.statuz.set(fpcoIO.getStatuz());
			databaseError99500();
		}
		/*                                                         <D9604>*/
		/* Now get next record                                     <D9604>*/
		/*                                                         <D9604>*/
		fpcoIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	*                                                         <D9604>
	* </pre>
	*/
protected void processPayr7000()
	{
		/*START*/
		/* Delete all Payer (PAYR) records associated with*/
		/*  the contract we are AFIing except for the 1st.*/
		payrrevIO.setParams(SPACES);
		payrrevIO.setFormat(formatsInner.payrrevrec);
		payrrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		payrrevIO.setChdrcoy(reverserec.company);
		payrrevIO.setChdrnum(reverserec.chdrnum);
		payrrevIO.setPayrseqno(ZERO);
		payrrevIO.setTranno(ZERO);
		while ( !(isEQ(payrrevIO.getStatuz(), varcom.endp))) {
			payrDeletes7100();
		}
		
		/*EXIT*/
	}

protected void payrDeletes7100()
	{
		try {
			start7100();
			getNextPayr7180();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start7100()
	{
		SmartFileCode.execute(appVars, payrrevIO);
		if (isNE(payrrevIO.getStatuz(), varcom.oK)
		&& isNE(payrrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrrevIO.getParams());
			syserrrec.statuz.set(payrrevIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(payrrevIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit7190);
		}
		/* check PAYR record read is associated with contract*/
		/* - If not, Release record*/
		if (isNE(reverserec.company, payrrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, payrrevIO.getChdrnum())) {
			payrrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit7190);
		}
		/* A correct match on Company & Contract number has now been*/
		/*  found - IF this is the 1st time through, then we have found*/
		/*  the earliest PAYR record for this Contract so reset to a*/
		/*  proposal stage. If this isn't the 1st record found, then*/
		/*  DELETe it.*/
		if (isEQ(wsaaFirstPayrDone, SPACES)) {
			payrProposal7200();
			wsaaFirstPayrDone.set("Y");
			payrrevIO.setFunction(varcom.writd);
		}
		else {
			/*    IF PAYRREV-TRANNO       = REVE-NEW-TRANNO                */
			/*       this is the new PAYR we have just written earlier     */
			/*       so don't delete it                                    */
			/*        GO TO 7180-GET-NEXT-PAYR                             */
			/*    END-IF                                                   */
			/*        lock record before deleting it*/
			/*        or rewriting the new transaction with Valid Flag 1       */
			payrrevIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, payrrevIO);
			if (isNE(payrrevIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(payrrevIO.getParams());
				syserrrec.statuz.set(payrrevIO.getStatuz());
				databaseError99500();
			}
			/*    MOVE DELET              TO PAYRREV-FUNCTION              */
			if (isEQ(payrrevIO.getTranno(), reverserec.newTranno)) {
				/*           this is the new PAYR we have just written earlier*/
				/*           so rewrite it with a valid flag of 1*/
				payrrevIO.setPstatcode(wsaaChdrPstatcode);
				payrrevIO.setValidflag("1");
				payrrevIO.setFunction(varcom.rewrt);
			}
			else {
				payrrevIO.setFunction(varcom.delet);
			}
		}
		SmartFileCode.execute(appVars, payrrevIO);
		if (isNE(payrrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrrevIO.getParams());
			syserrrec.statuz.set(payrrevIO.getStatuz());
			databaseError99500();
		}
	}

	/**
	* <pre>
	* Now get next record
	* </pre>
	*/
protected void getNextPayr7180()
	{
		payrrevIO.setFunction(varcom.nextr);
	}

protected void payrProposal7200()
	{
		start7200();
	}

protected void start7200()
	{
		payrrevIO.setTranno(reverserec.newTranno);
		payrrevIO.setBtdate(wsaaNextBillDate);
		//ILIFE-7820
		if(isbillday) {
			payrrevIO.setBillcd(chdrafiIO.getBillcd());
		}
		else {
			payrrevIO.setBillcd(wsaaNextBillDate);
		}
		
		payrrevIO.setPtdate(ZERO);
		payrrevIO.setTermid(reverserec.termid);
		payrrevIO.setUser(reverserec.user);
		payrrevIO.setTransactionDate(reverserec.transDate);
		payrrevIO.setTransactionTime(reverserec.transTime);
		payrrevIO.setOutstamt(ZERO);
		payrrevIO.setIncomeSeqNo(ZERO);
		for (wsaaCount.set(1); !(isGT(wsaaCount, 6)); wsaaCount.add(1)){
			payrrevIO.setSinstamt(wsaaCount, ZERO);
		}
		wsaaPayrBillfreq.set(payrrevIO.getBillfreq());
		wsaaPayrBillchnl.set(payrrevIO.getBillchnl());
		/* the NEXTDATE on the PAYR is the next billing date minus*/
		/*  the number of Lead days held on T6654.*/
		if (isEQ(t6654rec.leadDays, ZERO)) {
			payrrevIO.setNextdate(wsaaNextBillDate);
			return ;
		}
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(wsaaNextBillDate);
		datcon2rec.intDate2.set(ZERO);
		datcon2rec.frequency.set("DY");
		compute(datcon2rec.freqFactor, 0).set(mult(t6654rec.leadDays, -1));
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			systemError99000();
		}
		payrrevIO.setNextdate(datcon2rec.intDate2);
	}

protected void processCovr8000()
	{
		/*START*/
		wsaaInctCreate = "N";
		covrafiIO.setParams(SPACES);
		covrafiIO.setChdrcoy(reverserec.company);
		covrafiIO.setChdrnum(reverserec.chdrnum);
		covrafiIO.setPlanSuffix(ZERO);
		covrafiIO.setTranno(ZERO);
		covrafiIO.setFormat(formatsInner.covrafirec);
		covrafiIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrafiIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrafiIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covrafiIO.getStatuz(),varcom.endp))) {
			deletCovrs8100();
		}
		
		/*EXIT*/
	}

protected void deletCovrs8100()
	{
		start8100();
	}

protected void start8100()
	{
		SmartFileCode.execute(appVars, covrafiIO);
		if (isNE(covrafiIO.getStatuz(), varcom.oK)
		&& isNE(covrafiIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrafiIO.getParams());
			syserrrec.statuz.set(covrafiIO.getStatuz());
			systemError99000();
		}
		if (isEQ(covrafiIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* Check Coverage/rider COVR record is associated with the*/
		/*  contract we are processing.*/
		if (isNE(reverserec.company, covrafiIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, covrafiIO.getChdrnum())) {
			covrafiIO.setStatuz(varcom.endp);
			return ;
		}
		/* A correct match on Company & Contract number has now been*/
		/*  found. For each unique combination of Life, Coverage, Rider,*/
		/*  Plan-suffix found, use the earliest record to produce a COVT*/
		/*  proposal record for each combination and then delete all*/
		/*  the COVR records associated with this contract.*/
		/*IF WSAA-FIRST-COVR-READ     = SPACES                         */
		/*    MOVE COVRAFI-TRANNO     TO WSAA-FIRST-TRANNO             */
		/*    MOVE 'Y'                TO WSAA-FIRST-COVR-READ          */
		/*END-IF.                                                      */
		if (isNE(covrafiIO.getLife(), wsaaLife)
		|| isNE(covrafiIO.getCoverage(), wsaaCoverage)
		|| isNE(covrafiIO.getRider(), wsaaRider)
		|| isNE(covrafiIO.getPlanSuffix(), wsaaPlanSuffix)) {
			genericProcessing8200();
			writeCovt8400();
			/*    IF COVRAFI-TRANNO       = WSAA-FIRST-TRANNO              */
			/*        PERFORM 8400-WRITE-COVT                              */
			/*    END-IF                                                   */
			wsaaLife.set(covrafiIO.getLife());
			wsaaCoverage.set(covrafiIO.getCoverage());
			wsaaRider.set(covrafiIO.getRider());
			wsaaPlanSuffix.set(covrafiIO.getPlanSuffix());
		}
		if (isNE(covrafiIO.getCpiDate(), 99999999)
		&& isNE(covrafiIO.getCpiDate(), ZERO)) {
			wsaaInctCreate = "Y";
		}
		/*    Lock COVR record before deleting it.*/
		covrafiIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrafiIO);
		if (isNE(covrafiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrafiIO.getParams());
			syserrrec.statuz.set(covrafiIO.getStatuz());
			systemError99000();
		}
		covrafiIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, covrafiIO);
		if (isNE(covrafiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrafiIO.getParams());
			syserrrec.statuz.set(covrafiIO.getStatuz());
			systemError99000();
		}
		covrafiIO.setFunction(varcom.nextr);
	}

protected void genericProcessing8200()
	{
		start8200();
	}

protected void start8200()
	{
		/* Read T5671 to see if any Generic processing is required for*/
		/*  the coverage/rider we are processing*/
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaT5671Key.set(SPACES);
		wsaaT5671Crtable.set(covrafiIO.getCrtable());
		wsaaT5671Batctrcde.set(reverserec.oldBatctrcde);
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		/* Set up linkage area for calls to any of the S/routines on*/
		/*  T5671*/
		greversrec.reverseRec.set(SPACES);
		greversrec.chdrcoy.set(covrafiIO.getChdrcoy());
		greversrec.chdrnum.set(covrafiIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		greversrec.planSuffix.set(covrafiIO.getPlanSuffix());
		greversrec.life.set(covrafiIO.getLife());
		greversrec.coverage.set(covrafiIO.getCoverage());
		greversrec.rider.set(covrafiIO.getRider());
		greversrec.crtable.set(covrafiIO.getCrtable());
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.effdate.set(ZERO);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.contractAmount.set(ZERO);
		wsaaSub.set(ZERO);
		greversrec.language.set(reverserec.language);
		/* Now call all the S/Routines specified in the T5671 entry*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
			callTrevsubs8300();
		}
	}

protected void callTrevsubs8300()
	{
		/*START*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()], SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz, varcom.oK)) {
				syserrrec.params.set(greversrec.reverseRec);
				syserrrec.statuz.set(greversrec.statuz);
				systemError99000();
			}
		}
		/*EXIT*/
	}

protected void writeCovt8400()
	{
		start8400();
	}

protected void start8400()
	{
		covtlnbIO.setParams(SPACES);
		covtlnbIO.setChdrcoy(covrafiIO.getChdrcoy());
		covtlnbIO.setChdrnum(covrafiIO.getChdrnum());
		covtlnbIO.setLife(covrafiIO.getLife());
		covtlnbIO.setCoverage(covrafiIO.getCoverage());
		covtlnbIO.setRider(covrafiIO.getRider());
		covtlnbIO.setCrtable(covrafiIO.getCrtable());
		covtlnbIO.setTermid(reverserec.termid);
		covtlnbIO.setUser(reverserec.user);
		covtlnbIO.setTransactionTime(reverserec.transTime);
		covtlnbIO.setTransactionDate(reverserec.transDate);
		covtlnbIO.setRiskCessDate(covrafiIO.getRiskCessDate());
		covtlnbIO.setPremCessDate(covrafiIO.getPremCessDate());
		covtlnbIO.setBenCessDate(covrafiIO.getBenCessDate());
		covtlnbIO.setRiskCessAge(covrafiIO.getRiskCessAge());
		covtlnbIO.setPremCessAge(covrafiIO.getPremCessAge());
		covtlnbIO.setBenCessAge(covrafiIO.getBenCessAge());
		covtlnbIO.setRiskCessTerm(covrafiIO.getRiskCessTerm());
		covtlnbIO.setPremCessTerm(covrafiIO.getPremCessTerm());
		covtlnbIO.setBenCessTerm(covrafiIO.getBenCessTerm());
		covtlnbIO.setSumins(covrafiIO.getSumins());
		covtlnbIO.setMortcls(covrafiIO.getMortcls());
		covtlnbIO.setLiencd(covrafiIO.getLiencd());
		covtlnbIO.setJlife(covrafiIO.getJlife());
		covtlnbIO.setReserveUnitsInd(covrafiIO.getReserveUnitsInd());
		covtlnbIO.setReserveUnitsDate(covrafiIO.getReserveUnitsDate());
		covtlnbIO.setSingpremtype(covrafiIO.getSingpremtype()); /*ILIFE-8098 END*/
		covtlnbIO.setPolinc(wsaaPolinc);
		wsaaNewSeqnbr.add(1);
		covtlnbIO.setSeqnbr(wsaaNewSeqnbr);
		/* IF Plan suffix isn't zeros, then we are processing one policy*/
		/* IF Plan suffix is zeros, then we are processing a number of*/
		/*  summarised policies, so set the NUMAPP field on the COVT*/
		/*  by using the POLSUM value from the Contract Header.*/
		/*  Don't forget if there is only 1 policy in the plan, the*/
		/*  POLSUM value is Zero.*/
		if (isNE(covrafiIO.getPlanSuffix(), ZERO)) {
			covtlnbIO.setNumapp(1);
		}
		else {
			if (isGT(wsaaPolinc, 1)) {
				covtlnbIO.setNumapp(wsaaPolsum);
			}
			else {
				covtlnbIO.setNumapp(1);
			}
		}
		covtlnbIO.setEffdate(covrafiIO.getCrrcd());
		/* sort out Life details*/
		covtlnbIO.setJlife(covrafiIO.getJlife());
		lifeJlifeDetails8500();
		covtlnbIO.setBillfreq(wsaaPayrBillfreq);
		covtlnbIO.setBillchnl(wsaaPayrBillchnl);
		covtlnbIO.setSingp(covrafiIO.getSingp());
		covtlnbIO.setInstprem(covrafiIO.getInstprem());
		covtlnbIO.setPayrseqno(covrafiIO.getPayrseqno());
		covtlnbIO.setCntcurr(wsaaCntcurr);
		covtlnbIO.setZbinstprem(covrafiIO.getZbinstprem());
		covtlnbIO.setZlinstprem(covrafiIO.getZlinstprem());
		covtlnbIO.setZstpduty01(covrafiIO.getZstpduty01());//PINNACLE-2453
		cashDividendDet8600();
		covtlnbIO.setFunction(varcom.writr);
		covtlnbIO.setFormat(formatsInner.covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			systemError99000();
		}
	}

protected void lifeJlifeDetails8500()
	{
		start8500();
	}

protected void start8500()
	{
		/* get life details first*/
		lifeafiIO.setParams(SPACES);
		lifeafiIO.setChdrcoy(covrafiIO.getChdrcoy());
		lifeafiIO.setChdrnum(covrafiIO.getChdrnum());
		lifeafiIO.setLife(covrafiIO.getLife());
		lifeafiIO.setJlife("00");
		lifeafiIO.setTranno(ZERO);
		lifeafiIO.setFormat(formatsInner.lifeafirec);
		lifeafiIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifeafiIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeafiIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
		SmartFileCode.execute(appVars, lifeafiIO);
		if (isNE(lifeafiIO.getStatuz(), varcom.oK)
		&& isNE(lifeafiIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeafiIO.getParams());
			syserrrec.statuz.set(lifeafiIO.getStatuz());
			systemError99000();
		}
		/* If no main Life details, then major error !*/
		if (isNE(covrafiIO.getChdrcoy(), lifeafiIO.getChdrcoy())
		|| isNE(covrafiIO.getChdrnum(), lifeafiIO.getChdrnum())
		|| isNE(covrafiIO.getLife(), lifeafiIO.getLife())
		|| isEQ(lifeafiIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeafiIO.getParams());
			syserrrec.statuz.set(lifeafiIO.getStatuz());
			systemError99000();
		}
		covtlnbIO.setSex01(lifeafiIO.getCltsex());
		covtlnbIO.setAnbAtCcd01(lifeafiIO.getAnbAtCcd());
		/* check for Jlife details*/
		lifeafiIO.setParams(SPACES);
		lifeafiIO.setChdrcoy(covrafiIO.getChdrcoy());
		lifeafiIO.setChdrnum(covrafiIO.getChdrnum());
		lifeafiIO.setLife(covrafiIO.getLife());
		lifeafiIO.setJlife("01");
		lifeafiIO.setTranno(ZERO);
		lifeafiIO.setFormat(formatsInner.lifeafirec);
		lifeafiIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifeafiIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeafiIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
		SmartFileCode.execute(appVars, lifeafiIO);
		if (isNE(lifeafiIO.getStatuz(), varcom.oK)
		&& isNE(lifeafiIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeafiIO.getParams());
			syserrrec.statuz.set(lifeafiIO.getStatuz());
			systemError99000();
		}
		if (isNE(covrafiIO.getChdrcoy(), lifeafiIO.getChdrcoy())
		|| isNE(covrafiIO.getChdrnum(), lifeafiIO.getChdrnum())
		|| isNE(covrafiIO.getLife(), lifeafiIO.getLife())
		|| isEQ(lifeafiIO.getStatuz(), varcom.endp)) {
			covtlnbIO.setSex02(SPACES);
			covtlnbIO.setAnbAtCcd02(ZERO);
			return ;
		}
		/* get here so there must be a joint life*/
		covtlnbIO.setSex02(lifeafiIO.getCltsex());
		covtlnbIO.setAnbAtCcd02(lifeafiIO.getAnbAtCcd());
	}

protected void cashDividendDet8600()
	{
		cashDivDet8600();
	}

protected void cashDivDet8600()
	{
		/* Read HCSD to obtain the cash dividend record, set up COV<CSDV01>*/
		/* and delete HCSD.                                                */
		hcsdIO.setDataKey(SPACES);
		hcsdIO.setChdrcoy(covrafiIO.getChdrcoy());
		hcsdIO.setChdrnum(covrafiIO.getChdrnum());
		hcsdIO.setLife(covrafiIO.getLife());
		hcsdIO.setCoverage(covrafiIO.getCoverage());
		hcsdIO.setRider(covrafiIO.getRider());
		hcsdIO.setPlanSuffix(covrafiIO.getPlanSuffix());
		hcsdIO.setFunction(varcom.readh);
		hcsdIO.setFormat(formatsInner.hcsdrec);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)
		&& isNE(hcsdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			systemError99000();
		}
		if (isEQ(hcsdIO.getStatuz(), varcom.mrnf)) {
			covtlnbIO.setZdivopt(SPACES);
			covtlnbIO.setPaycoy(SPACES);
			covtlnbIO.setPayclt(SPACES);
			covtlnbIO.setPaymth(SPACES);
			covtlnbIO.setBankkey(SPACES);
			covtlnbIO.setBankacckey(SPACES);
			covtlnbIO.setPaycurr(SPACES);
			covtlnbIO.setFacthous(SPACES);
			return ;
		}
		else {
			covtlnbIO.setZdivopt(hcsdIO.getZdivopt());
			covtlnbIO.setPaycoy(hcsdIO.getPaycoy());
			covtlnbIO.setPayclt(hcsdIO.getPayclt());
			covtlnbIO.setPaymth(hcsdIO.getPaymth());
			covtlnbIO.setBankkey(hcsdIO.getBankkey());
			covtlnbIO.setBankacckey(hcsdIO.getBankacckey());
			covtlnbIO.setPaycurr(hcsdIO.getPaycurr());
			covtlnbIO.setFacthous(hcsdIO.getFacthous());
		}
		hcsdIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			systemError99000();
		}
	}

protected void processFlup9000()
	{
		/*START*/
		/* Delete any Follow-up ( FLUP ) records associated with*/
		/*  the contract we are AFIing.*/
		fluprevIO.setParams(SPACES);
		fluprevIO.setFormat(formatsInner.fluprevrec);
		fluprevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fluprevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fluprevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		fluprevIO.setChdrcoy(reverserec.company);
		fluprevIO.setChdrnum(reverserec.chdrnum);
		fluprevIO.setTranno(ZERO);
		while ( !(isEQ(fluprevIO.getStatuz(), varcom.endp))) {
			flupDeletes9100();
		}
		
		/*EXIT*/
	}

protected void flupDeletes9100()
	{
		start9100();
	}

protected void start9100()
	{
		SmartFileCode.execute(appVars, fluprevIO);
		if (isNE(fluprevIO.getStatuz(), varcom.oK)
		&& isNE(fluprevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(fluprevIO.getParams());
			syserrrec.statuz.set(fluprevIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(fluprevIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* check FLUP record read is associated with contract*/
		/* - If not, Release record*/
		if (isNE(reverserec.company, fluprevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, fluprevIO.getChdrnum())) {
			fluprevIO.setStatuz(varcom.endp);
			return ;
		}
		/* Get here, so FLUP exists for contract, so we will now read*/
		/*  lock record and then delete it.*/
		fluprevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fluprevIO);
		if (isNE(fluprevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fluprevIO.getParams());
			syserrrec.statuz.set(fluprevIO.getStatuz());
			databaseError99500();
		}
		fluprevIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, fluprevIO);
		if (isNE(fluprevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fluprevIO.getParams());
			syserrrec.statuz.set(fluprevIO.getStatuz());
			databaseError99500();
		}
		/* Now get next record*/
		fluprevIO.setFunction(varcom.nextr);
	}

protected void processClrf10000()
	{
		start10000();
	}

protected void start10000()
	{
		/* We will leave most of the CLRF Client Roles as they are,*/
		/*  except for the following:*/
		/*     Beneficiaries     'BN'*/
		/* MOVE SPACES                 TO CLRF-PARAMS.                  */
		/* MOVE 'CH'                   TO CLRF-FOREPFX.                 */
		/* MOVE 'BN'                   TO WSAA-ROLE.                    */
		/* MOVE WSAA-ROLE              TO CLRF-CLRRROLE.                */
		/* MOVE REVE-COMPANY           TO CLRF-FORECOY.                 */
		/* MOVE REVE-CHDRNUM           TO CLRF-FORENUM.                 */
		/* MOVE CLRFREC                TO CLRF-FORMAT.                  */
		/* MOVE BEGN                   TO CLRF-FUNCTION.                */
		/* PERFORM 10100-DELETE-ROLES  UNTIL CLRF-STATUZ = ENDP.        */
		/*     Despatch Address  'DA'*/
		/*     For Despatch Address, if it has not specified it will       */
		/*     generate when contract issue, so we need to delete it       */
		if (isNE(wsaaDespnum, SPACES)) {
			return ;
		}
		clrfIO.setParams(SPACES);
		clrfIO.setForepfx("CH");
		wsaaRole.set("DA");
		clrfIO.setClrrrole(wsaaRole);
		clrfIO.setForecoy(reverserec.company);
		clrfIO.setForenum(reverserec.chdrnum);
		clrfIO.setFormat(formatsInner.clrfrec);
		clrfIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		clrfIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		while ( !(isEQ(clrfIO.getStatuz(),varcom.endp))) {
			deleteRoles10100();
		}
		
		callBldenrl10200();
	}

protected void deleteRoles10100()
	{
		start10100();
	}

protected void start10100()
	{
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)
		&& isNE(clrfIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(clrfIO.getParams());
			syserrrec.statuz.set(clrfIO.getStatuz());
			systemError99000();
		}
		if (isEQ(clrfIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(clrfIO.getForepfx(), "CH")
		|| isNE(clrfIO.getClrrrole(), wsaaRole)
		|| isNE(clrfIO.getForecoy(), reverserec.company)
		|| isNE(clrfIO.getForenum(), reverserec.chdrnum)) {
			clrfIO.setStatuz(varcom.endp);
			return ;
		}
		clrfIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clrfIO.getParams());
			syserrrec.statuz.set(clrfIO.getStatuz());
			systemError99000();
		}
		clrfIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clrfIO.getParams());
			syserrrec.statuz.set(clrfIO.getStatuz());
			systemError99000();
		}
		clrfIO.setFunction(varcom.nextr);
	}

protected void callBldenrl10200()
	{
		/*START*/
		initialize(bldenrlrec.bldenrlrec);
		bldenrlrec.prefix.set(fsupfxcpy.chdr);
		bldenrlrec.company.set(reverserec.company);
		bldenrlrec.uentity.set(reverserec.chdrnum);
		callProgram(Bldenrl.class, bldenrlrec.bldenrlrec);
		if (isNE(bldenrlrec.statuz, varcom.oK)) {
			syserrrec.params.set(bldenrlrec.bldenrlrec);
			syserrrec.statuz.set(bldenrlrec.statuz);
			systemError99000();
		}
		/*EXIT*/
	}

protected void processLext11000()
	{
		start11000();
	}

protected void start11000()
	{
		wsaaLife.set(SPACES);
		wsaaCoverage.set(SPACES);
		wsaaRider.set(SPACES);
		wsaaSeqnbr.set(ZERO);
		lextafiIO.setParams(SPACES);
		lextafiIO.setChdrcoy(reverserec.company);
		lextafiIO.setChdrnum(reverserec.chdrnum);
		lextafiIO.setSeqnbr(ZERO);
		lextafiIO.setTranno(ZERO);
		lextafiIO.setFormat(formatsInner.lextafirec);
		lextafiIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextafiIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextafiIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(lextafiIO.getStatuz(),varcom.endp))) {
			updDelLext11100();
		}
		
	}

protected void updDelLext11100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start11100();
				case getNextLext11180: 
					getNextLext11180();
				case exit11190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start11100()
	{
		SmartFileCode.execute(appVars, lextafiIO);
		if (isNE(lextafiIO.getStatuz(), varcom.oK)
		&& isNE(lextafiIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextafiIO.getParams());
			syserrrec.statuz.set(lextafiIO.getStatuz());
			systemError99000();
		}
		if (isEQ(lextafiIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit11190);
		}
		/* Check Life Options/Extras LEXT record is associated with the*/
		/*  contract we are processing.*/
		if (isNE(reverserec.company, lextafiIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, lextafiIO.getChdrnum())) {
			lextafiIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit11190);
		}
		/* A correct match on Company & Contract number has now been*/
		/*  found. For each unique combination of Life, Coverage, Rider,*/
		/*  sequence number and Transaction number, keep earliest.*/
		if (isNE(lextafiIO.getLife(), wsaaLife)
		|| isNE(lextafiIO.getCoverage(), wsaaCoverage)
		|| isNE(lextafiIO.getRider(), wsaaRider)
		|| isNE(lextafiIO.getSeqnbr(), wsaaSeqnbr)) {
			lextafiIO.setValidflag("1");
			lextafiIO.setTranno(reverserec.newTranno);
			lextafiIO.setCurrfrom(ZERO);
			lextafiIO.setCurrto(varcom.vrcmMaxDate);
			lextafiIO.setTermid(reverserec.termid);
			lextafiIO.setUser(reverserec.user);
			lextafiIO.setTransactionTime(reverserec.transTime);
			lextafiIO.setTransactionDate(reverserec.transDate);
			lextafiIO.setFunction(varcom.writd);
			wsaaLife.set(lextafiIO.getLife());
			wsaaCoverage.set(lextafiIO.getCoverage());
			wsaaRider.set(lextafiIO.getRider());
			wsaaSeqnbr.set(lextafiIO.getSeqnbr());
		}
		else {
			if (isEQ(lextafiIO.getTranno(), reverserec.newTranno)) {
				/*           this is the new LEXT we have just written earlier*/
				/*           so don't delete it*/
				goTo(GotoLabel.getNextLext11180);
			}
			/*        lock record before deleting it*/
			lextafiIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, lextafiIO);
			if (isNE(lextafiIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(lextafiIO.getParams());
				syserrrec.statuz.set(lextafiIO.getStatuz());
				databaseError99500();
			}
			lextafiIO.setFunction(varcom.delet);
		}
		SmartFileCode.execute(appVars, lextafiIO);
		if (isNE(lextafiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lextafiIO.getParams());
			syserrrec.statuz.set(lextafiIO.getStatuz());
			systemError99000();
		}
	}

protected void getNextLext11180()
	{
		lextafiIO.setFunction(varcom.nextr);
	}

protected void processLife12000()
	{
		start12000();
	}

protected void start12000()
	{
		wsaaLife.set(SPACES);
		wsaaJlife.set(SPACES);
		lifeafiIO.setParams(SPACES);
		lifeafiIO.setChdrcoy(reverserec.company);
		lifeafiIO.setChdrnum(reverserec.chdrnum);
		lifeafiIO.setLife(SPACES);
		lifeafiIO.setJlife(SPACES);
		lifeafiIO.setTranno(ZERO);
		lifeafiIO.setFormat(formatsInner.lifeafirec);
		lifeafiIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifeafiIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeafiIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(lifeafiIO.getStatuz(),varcom.endp))) {
			deletLife12100();
		}
		
	}

protected void deletLife12100()
	{
		start12100();
	}

protected void start12100()
	{
		SmartFileCode.execute(appVars, lifeafiIO);
		if (isNE(lifeafiIO.getStatuz(), varcom.oK)
		&& isNE(lifeafiIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeafiIO.getParams());
			syserrrec.statuz.set(lifeafiIO.getStatuz());
			systemError99000();
		}
		if (isEQ(lifeafiIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* Check Life LIFE record is associated with the*/
		/*  contract we are processing.*/
		if (isNE(reverserec.company, lifeafiIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, lifeafiIO.getChdrnum())) {
			lifeafiIO.setStatuz(varcom.endp);
			return ;
		}
		/* A correct match on Company & Contract number has now been*/
		/*  found. For each unique combination of Life and Jlife*/
		/*  found, keep earliest.*/
		if (isNE(lifeafiIO.getLife(), wsaaLife)
		|| isNE(lifeafiIO.getJlife(), wsaaJlife)) {
			lifeafiIO.setValidflag("3");
			lifeafiIO.setTranno(reverserec.newTranno);
			lifeafiIO.setCurrfrom(wsaaConStartDate);
			lifeafiIO.setCurrto(varcom.vrcmMaxDate);
			lifeafiIO.setTermid(reverserec.termid);
			lifeafiIO.setUser(reverserec.user);
			lifeafiIO.setTransactionDate(reverserec.transDate);
			lifeafiIO.setTransactionTime(reverserec.transTime);
			lifeafiIO.setFunction(varcom.writd);
			wsaaLife.set(lifeafiIO.getLife());
			wsaaJlife.set(lifeafiIO.getJlife());
		}
		else {
			lifeafiIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, lifeafiIO);
			if (isNE(lifeafiIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(lifeafiIO.getParams());
				syserrrec.statuz.set(lifeafiIO.getStatuz());
				systemError99000();
			}
			lifeafiIO.setFunction(varcom.delet);
		}
		SmartFileCode.execute(appVars, lifeafiIO);
		if (isNE(lifeafiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeafiIO.getParams());
			syserrrec.statuz.set(lifeafiIO.getStatuz());
			systemError99000();
		}
		lifeafiIO.setFunction(varcom.nextr);
	}

protected void processLins13000()
	{
		/*START*/
		/* Delete any Life Installment ( LINS ) records associated with*/
		/*  the contract we are CFIing.*/
		linscfiIO.setParams(SPACES);
		linscfiIO.setFormat(formatsInner.linscfirec);
		linscfiIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		linscfiIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		linscfiIO.setChdrcoy(reverserec.company);
		linscfiIO.setChdrnum(reverserec.chdrnum);
		while ( !(isEQ(linscfiIO.getStatuz(), varcom.endp))) {
			linsDeletes13100();
		}
		
		/*EXIT*/
	}

protected void linsDeletes13100()
	{
		start13100();
	}

protected void start13100()
	{
		SmartFileCode.execute(appVars, linscfiIO);
		if (isNE(linscfiIO.getStatuz(), varcom.oK)
		&& isNE(linscfiIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(linscfiIO.getParams());
			syserrrec.statuz.set(linscfiIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(linscfiIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* check LINS record read is associated with contract*/
		/* - If not, Release record*/
		if (isNE(reverserec.company, linscfiIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, linscfiIO.getChdrnum())) {
			linscfiIO.setStatuz(varcom.endp);
			return ;
		}
		/* Get here, so LINS exists for contract, so we will now read*/
		/*  lock record and then delete it*/
		linscfiIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, linscfiIO);
		if (isNE(linscfiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(linscfiIO.getParams());
			syserrrec.statuz.set(linscfiIO.getStatuz());
			databaseError99500();
		}
		linscfiIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, linscfiIO);
		if (isNE(linscfiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(linscfiIO.getParams());
			syserrrec.statuz.set(linscfiIO.getStatuz());
			databaseError99500();
		}
		/* Now get next record*/
		linscfiIO.setFunction(varcom.nextr);
	}

protected void processTaxd13200()
	{
		/*START*/
		/* Delete all TAXDREV records under the same contract              */
		taxdrevIO.setParams(SPACES);
		taxdrevIO.setFormat(formatsInner.taxdrevrec);
		/* MOVE BEGNH                  TO TAXDREV-FUNCTION.     <LA4758>*/
		taxdrevIO.setFunction(varcom.begn);
		taxdrevIO.setChdrcoy(reverserec.company);
		taxdrevIO.setChdrnum(reverserec.chdrnum);
		taxdrevIO.setEffdate(ZERO);
		while ( !(isEQ(taxdrevIO.getStatuz(), varcom.endp))) {
			taxdDeletes13300();
		}
		
		/*EXIT*/
	}

protected void taxdDeletes13300()
	{
		start13310();
	}

protected void start13310()
	{
		SmartFileCode.execute(appVars, taxdrevIO);
		if (isNE(taxdrevIO.getStatuz(), varcom.oK)
		&& isNE(taxdrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(taxdrevIO.getParams());
			syserrrec.statuz.set(taxdrevIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(taxdrevIO.getStatuz(), varcom.endp)) {
			taxdrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(reverserec.company, taxdrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, taxdrevIO.getChdrnum())) {
			taxdrevIO.setStatuz(varcom.endp);
			return ;
		}
		/* IF TAXDREV-STATUZ           = ENDP                   <LA4758>*/
		/*     GO TO 13390-EXIT                                 <LA4758>*/
		/* END-IF.                                              <LA4758>*/
		/*                                                      <LA4758>*/
		/* If TAXDREV exists for contract, issue DELETE                    */
		/* MOVE DELET                  TO TAXDREV-FUNCTION.     <LA4758>*/
		taxdrevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, taxdrevIO);
		if (isNE(taxdrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdrevIO.getParams());
			syserrrec.statuz.set(taxdrevIO.getStatuz());
			databaseError99500();
		}
		/* Now get next record                                             */
		taxdrevIO.setFunction(varcom.nextr);
	}

protected void processPcdd14000()
	{
		/*START*/
		pcddafiIO.setParams(SPACES);
		pcddafiIO.setChdrcoy(reverserec.company);
		pcddafiIO.setChdrnum(reverserec.chdrnum);
		pcddafiIO.setAgntnum(SPACES);
		pcddafiIO.setTranno(ZERO);
		pcddafiIO.setFormat(formatsInner.pcddafirec);
		pcddafiIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		pcddafiIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcddafiIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(pcddafiIO.getStatuz(),varcom.endp))) {
			updDelPcdd14100();
		}
		
		/*EXIT*/
	}

protected void updDelPcdd14100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start14100();
				case getNextPcdd14180: 
					getNextPcdd14180();
				case exit14190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start14100()
	{
		SmartFileCode.execute(appVars, pcddafiIO);
		if (isNE(pcddafiIO.getStatuz(), varcom.oK)
		&& isNE(pcddafiIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcddafiIO.getParams());
			syserrrec.statuz.set(pcddafiIO.getStatuz());
			systemError99000();
		}
		if (isEQ(pcddafiIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit14190);
		}
		/* Check proposed Commission/Bonus points split record PCDD is*/
		/* associated with the contract we are processing.*/
		if (isNE(reverserec.company, pcddafiIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, pcddafiIO.getChdrnum())) {
			pcddafiIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit14190);
		}
		/*A correct match on Company & Contract number has now been     */
		/*found. For each unique combination of Agent number and        */
		/*transaction number found, keep earliest.                      */
		/* IF PCDDAFI-AGNTNUM          NOT = WSAA-AGNTNUM               */
		/*     MOVE '1'                TO PCDDAFI-VALIDFLAG             */
		/*     MOVE REVE-NEW-TRANNO    TO PCDDAFI-TRANNO                */
		/*     MOVE WSAA-CON-START-DATE TO PCDDAFI-CURRFROM             */
		/*     MOVE VRCM-MAX-DATE      TO PCDDAFI-CURRTO                */
		/*     MOVE REVE-TERMID        TO PCDDAFI-TERMID                */
		/*     MOVE REVE-USER          TO PCDDAFI-USER                  */
		/*     MOVE REVE-TRANS-TIME    TO PCDDAFI-TRANSACTION-TIME      */
		/*     MOVE REVE-TRANS-DATE    TO PCDDAFI-TRANSACTION-DATE      */
		/*     MOVE WRITD              TO PCDDAFI-FUNCTION              */
		/*     MOVE PCDDAFI-AGNTNUM    TO WSAA-AGNTNUM                  */
		/* ELSE                                                         */
		/*     IF PCDDAFI-TRANNO       = REVE-NEW-TRANNO                */
		/*        this is the new PCDD we have just written earlier     */
		/*        so don't delete it                                    */
		/*         GO TO 14180-GET-NEXT-PCDD                            */
		/*     END-IF                                                   */
		/*     MOVE READH              TO PCDDAFI-FUNCTION              */
		/*     CALL 'PCDDAFIIO'        USING PCDDAFI-PARAMS             */
		/*     IF PCDDAFI-STATUZ       NOT = O-K                        */
		/*         MOVE PCDDAFI-PARAMS TO SYSR-PARAMS                   */
		/*         MOVE PCDDAFI-STATUZ TO SYSR-STATUZ                   */
		/*         PERFORM 99000-SYSTEM-ERROR                           */
		/*     END-IF                                                   */
		/*     MOVE DELET              TO PCDDAFI-FUNCTION              */
		/* END-IF.                                                      */
		/*  Correction to the above: if the earliest, unique PCDDAFI       */
		/*  agent has a split commission and split bonus adding up to      */
		/*  100 - delete it.                                               */
		if (isEQ(wsaaFirstPcddRead, SPACES)) {
			if (isEQ(pcddafiIO.getSplitBcomm(), 100)
			&& isEQ(pcddafiIO.getSplitBpts(), 100)) {
				pcddafiIO.setFunction(varcom.readh);
				SmartFileCode.execute(appVars, pcddafiIO);
				if (isNE(pcddafiIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(pcddafiIO.getParams());
					syserrrec.statuz.set(pcddafiIO.getStatuz());
					systemError99000();
				}
				pcddafiIO.setFunction(varcom.delet);
			}
			else {
				pcddafiIO.setValidflag("1");
				pcddafiIO.setTranno(reverserec.newTranno);
				pcddafiIO.setCurrfrom(wsaaConStartDate);
				pcddafiIO.setCurrto(varcom.vrcmMaxDate);
				pcddafiIO.setTermid(reverserec.termid);
				pcddafiIO.setUser(reverserec.user);
				pcddafiIO.setTransactionTime(reverserec.transTime);
				pcddafiIO.setTransactionDate(reverserec.transDate);
				pcddafiIO.setFunction(varcom.writd);
			}
			wsaaFirstPcddRead.set("Y");
		}
		else {
			goTo(GotoLabel.getNextPcdd14180);
		}
		SmartFileCode.execute(appVars, pcddafiIO);
		if (isNE(pcddafiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcddafiIO.getParams());
			syserrrec.statuz.set(pcddafiIO.getStatuz());
			systemError99000();
		}
	}

protected void getNextPcdd14180()
	{
		pcddafiIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	*15000-PROCESS-RACT SECTION.                                      
	*15000-START.                                                     
	**** MOVE SPACES                 TO RACTAFI-PARAMS.               
	**** MOVE REVE-COMPANY           TO RACTAFI-CHDRCOY.              
	**** MOVE REVE-CHDRNUM           TO RACTAFI-CHDRNUM.              
	**** MOVE BEGN                   TO RACTAFI-FUNCTION.             
	**** MOVE RACTAFIREC             TO RACTAFI-FORMAT.               
	**** MOVE SPACES                 TO WSAA-LIFE.                    
	**** MOVE SPACES                 TO WSAA-COVERAGE.                
	**** MOVE SPACES                 TO WSAA-RIDER.                   
	**** MOVE SPACES                 TO WSAA-RASNUM.                  
	**** MOVE SPACES                 TO WSAA-RATYPE.                  
	**** PERFORM 15100-UPD-DEL-RACT  UNTIL RACTAFI-STATUZ = ENDP.     
	*15090-EXIT.                                                      
	**** EXIT.                                                        
	*15100-UPD-DEL-RACT SECTION.                                      
	*15100-START.                                                     
	**** CALL 'RACTAFIIO'            USING RACTAFI-PARAMS.            
	**** IF RACTAFI-STATUZ           NOT = O-K                        
	****    AND RACTAFI-STATUZ       NOT = ENDP                       
	****     MOVE RACTAFI-PARAMS     TO SYSR-PARAMS                   
	****     MOVE RACTAFI-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99000-SYSTEM-ERROR                               
	**** END-IF.                                                      
	**** IF RACTAFI-STATUZ           = ENDP                           
	****     GO TO 15190-EXIT                                         
	**** END-IF.                                                      
	**Check Reassurance record RACT is associated with                
	***the contract we are processing.                                
	**** IF REVE-COMPANY             NOT = RACTAFI-CHDRCOY            
	****    OR REVE-CHDRNUM          NOT = RACTAFI-CHDRNUM            
	****     MOVE ENDP               TO RACTAFI-STATUZ                
	****     GO TO 15190-EXIT                                         
	**** END-IF.                                                      
	**A*correct match on Company & Contract number has now been       
	***found. For each unique combination of Life, Coverage,          
	***Rider, Reassurance Account number, Reassurance type found,     
	****keep earliest.                                                
	**** IF RACTAFI-LIFE             NOT = WSAA-LIFE                  
	****    OR RACTAFI-COVERAGE      NOT = WSAA-COVERAGE              
	****    OR RACTAFI-RIDER         NOT = WSAA-RIDER                 
	****    OR RACTAFI-RASNUM        NOT = WSAA-RASNUM                
	****    OR RACTAFI-RATYPE        NOT = WSAA-RATYPE                
	****     MOVE '3'                TO RACTAFI-VALIDFLAG             
	****     MOVE WSAA-CON-START-DATE TO RACTAFI-CURRFROM             
	****     MOVE REVE-USER          TO RACTAFI-USER                  
	****     MOVE REVE-TRANS-TIME    TO RACTAFI-TRANSACTION-TIME      
	****     MOVE REVE-TRANS-DATE    TO RACTAFI-TRANSACTION-DATE      
	****     MOVE WSAA-NEXT-BILL-DATE TO RACTAFI-BTDATE               
	****     MOVE WRITD              TO RACTAFI-FUNCTION              
	****     MOVE RACTAFI-LIFE       TO WSAA-LIFE                     
	****     MOVE RACTAFI-COVERAGE   TO WSAA-COVERAGE                 
	****     MOVE RACTAFI-RIDER      TO WSAA-RIDER                    
	****     MOVE RACTAFI-RASNUM     TO WSAA-RASNUM                   
	****     MOVE RACTAFI-RATYPE     TO WSAA-RATYPE                   
	**** ELSE                                                         
	****     MOVE READH              TO RACTAFI-FUNCTION              
	****     CALL 'RACTAFIIO'        USING RACTAFI-PARAMS             
	****     IF RACTAFI-STATUZ       NOT = O-K                        
	****         MOVE RACTAFI-PARAMS TO SYSR-PARAMS                   
	****         MOVE RACTAFI-STATUZ TO SYSR-STATUZ                   
	****         PERFORM 99000-SYSTEM-ERROR                           
	****     END-IF                                                   
	****     MOVE DELET              TO RACTAFI-FUNCTION              
	**** END-IF.                                                      
	**** CALL 'RACTAFIIO'            USING RACTAFI-PARAMS.            
	**** IF RACTAFI-STATUZ           NOT = O-K                        
	****     MOVE RACTAFI-PARAMS     TO SYSR-PARAMS                   
	****     MOVE RACTAFI-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99000-SYSTEM-ERROR                               
	**** END-IF.                                                      
	**** MOVE NEXTR                  TO RACTAFI-FUNCTION.             
	*15190-EXIT.                                                      
	**** EXIT.                                                        
	* </pre>
	*/
protected void processRtrn16000()
	{
		start16000();
	}

protected void start16000()
	{
		rtrnrevIO.setParams(SPACES);
		rtrnrevIO.setRldgcoy(reverserec.company);
		rtrnrevIO.setRdocnum(reverserec.chdrnum);
		rtrnrevIO.setTranno(reverserec.tranno);
		rtrnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		rtrnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, rtrnrevIO);
		if (isNE(rtrnrevIO.getStatuz(), varcom.oK)
		&& isNE(rtrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(rtrnrevIO.getParams());
			syserrrec.statuz.set(rtrnrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.chdrnum, rtrnrevIO.getRdocnum())
		|| isNE(reverserec.company, rtrnrevIO.getRldgcoy())
		|| isNE(reverserec.tranno, rtrnrevIO.getTranno())) {
			rtrnrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(rtrnrevIO.getStatuz(), varcom.endp))) {
			reverseRtrns16100();
		}
		
	}

protected void reverseRtrns16100()
	{
		start16100();
		nextRtrnrev16180();
	}

protected void start16100()
	{
		/* check if transaction codes match before processing RTRN*/
		if (isNE(reverserec.oldBatctrcde, rtrnrevIO.getBatctrcde())) {
			return ;
		}
		/*     Write a new record to the RTRN file*/
		/*     by calling the subroutine LIFRTRN.*/
		lifrtrnrec.lifrtrnRec.set(SPACES);
		lifrtrnrec.function.set("PSTW");
		lifrtrnrec.batckey.set(reverserec.batchkey);
		lifrtrnrec.rdocnum.set(reverserec.chdrnum);
		lifrtrnrec.rldgacct.set(reverserec.chdrnum);
		lifrtrnrec.tranno.set(reverserec.newTranno);
		lifrtrnrec.jrnseq.set(rtrnrevIO.getTranseq());
		lifrtrnrec.rldgcoy.set(reverserec.company);
		lifrtrnrec.sacscode.set(rtrnrevIO.getSacscode());
		lifrtrnrec.sacstyp.set(rtrnrevIO.getSacstyp());
		lifrtrnrec.trandesc.set(wsaaTransDesc);
		lifrtrnrec.crate.set(rtrnrevIO.getCrate());
		lifrtrnrec.transactionTime.set(rtrnrevIO.getTrandate());
		lifrtrnrec.transactionDate.set(rtrnrevIO.getTrantime());
		lifrtrnrec.user.set(reverserec.user);
		lifrtrnrec.termid.set(reverserec.termid);
		compute(lifrtrnrec.acctamt, 2).set(mult(rtrnrevIO.getAcctamt(), -1));
		compute(lifrtrnrec.origamt, 2).set(mult(rtrnrevIO.getOrigamt(), -1));
		lifrtrnrec.genlcur.set(rtrnrevIO.getGenlcur());
		lifrtrnrec.origcurr.set(rtrnrevIO.getOrigccy());
		lifrtrnrec.genlcoy.set(rtrnrevIO.getGenlcoy());
		lifrtrnrec.glcode.set(rtrnrevIO.getGlcode());
		lifrtrnrec.glsign.set(rtrnrevIO.getGlsign());
		lifrtrnrec.postyear.set(rtrnrevIO.getPostyear());
		lifrtrnrec.postmonth.set(rtrnrevIO.getPostmonth());
		/* MOVE DTC1-INT-DATE          TO LIFR-EFFDATE.                 */
		lifrtrnrec.effdate.set(rtrnrevIO.getEffdate());
		lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
		lifrtrnrec.rcamt.set(ZERO);
		lifrtrnrec.contot.set(1);
		lifrtrnrec.substituteCode[1].set(wsaaCnttype);
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifrtrnrec.lifrtrnRec);
			syserrrec.statuz.set(lifrtrnrec.statuz);
			systemError99000();
		}
	}

protected void nextRtrnrev16180()
	{
		rtrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, rtrnrevIO);
		if (isNE(rtrnrevIO.getStatuz(), varcom.oK)
		&& isNE(rtrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(rtrnrevIO.getParams());
			syserrrec.statuz.set(rtrnrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.chdrnum, rtrnrevIO.getRdocnum())
		|| isNE(reverserec.company, rtrnrevIO.getRldgcoy())
		|| isNE(reverserec.tranno, rtrnrevIO.getTranno())) {
			rtrnrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processInct17000()
	{
		/*START*/
		inctIO.setParams(SPACES);
		inctIO.setChdrcoy(reverserec.company);
		inctIO.setChdrnum(reverserec.chdrnum);
		inctIO.setIndxflg("Y");
		inctIO.setFunction(varcom.writr);
		inctIO.setFormat(formatsInner.inctrec);
		SmartFileCode.execute(appVars, inctIO);
		if (isNE(inctIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(inctIO.getParams());
			databaseError99500();
		}
		/*EXIT*/
	}

protected void processZrst18000()
	{
		/*START*/
		zrstnudIO.setParams(SPACES);
		zrstnudIO.setChdrnum(reverserec.chdrnum);
		zrstnudIO.setChdrcoy(reverserec.company);
		zrstnudIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zrstnudIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zrstnudIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(zrstnudIO.getStatuz(),varcom.endp))) {
			deleteZrsts18100();
		}
		
		/*EXIT*/
	}

protected void deleteZrsts18100()
	{
		start18100();
	}

protected void start18100()
	{
		SmartFileCode.execute(appVars, zrstnudIO);
		if (isNE(zrstnudIO.getStatuz(), varcom.oK)
		&& isNE(zrstnudIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(zrstnudIO.getParams());
			syserrrec.statuz.set(zrstnudIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(zrstnudIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* check ZRST record read is associated with contract              */
		/* - If not, Release record                                        */
		if (isNE(reverserec.company, zrstnudIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, zrstnudIO.getChdrnum())) {
			zrstnudIO.setStatuz(varcom.endp);
			return ;
		}
		/* Get here, so ZRST exists for contract, so we will now read      */
		/*  lock record and then delete it.                                */
		zrstnudIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, zrstnudIO);
		if (isNE(zrstnudIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zrstnudIO.getParams());
			syserrrec.statuz.set(zrstnudIO.getStatuz());
			databaseError99500();
		}
		zrstnudIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, zrstnudIO);
		if (isNE(zrstnudIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zrstnudIO.getParams());
			syserrrec.statuz.set(zrstnudIO.getStatuz());
			databaseError99500();
		}
		/* Now get next record                                             */
		zrstnudIO.setFunction(varcom.nextr);
	}

protected void processZptn19000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					proc19010();
				case call19020: 
					call19020();
					writ19030();
					nextr19080();
				case exit19090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void proc19010()
	{
		zptnrevIO.setDataArea(SPACES);
		zptnrevIO.setChdrcoy(reverserec.company);
		zptnrevIO.setChdrnum(reverserec.chdrnum);
		zptnrevIO.setTranno(reverserec.tranno);
		zptnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		zptnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		zptnrevIO.setFormat(formatsInner.zptnrevrec);
	}

protected void call19020()
	{
		SmartFileCode.execute(appVars, zptnrevIO);
		if (isNE(zptnrevIO.getStatuz(), varcom.oK)
		&& isNE(zptnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zptnrevIO.getStatuz());
			syserrrec.params.set(zptnrevIO.getParams());
			databaseError99500();
		}
		if (isEQ(zptnrevIO.getStatuz(), varcom.endp)
		|| isNE(zptnrevIO.getChdrcoy(), reverserec.company)
		|| isNE(zptnrevIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(zptnrevIO.getTranno(), reverserec.tranno)) {
			zptnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit19090);
		}
	}

protected void writ19030()
	{
		zptnIO.setDataArea(SPACES);
		zptnIO.setChdrcoy(zptnrevIO.getChdrcoy());
		zptnIO.setChdrnum(zptnrevIO.getChdrnum());
		zptnIO.setLife(zptnrevIO.getLife());
		zptnIO.setCoverage(zptnrevIO.getCoverage());
		zptnIO.setRider(zptnrevIO.getRider());
		zptnIO.setTranno(reverserec.newTranno);
		setPrecision(zptnIO.getOrigamt(), 2);
		zptnIO.setOrigamt(mult(zptnrevIO.getOrigamt(), -1));
		zptnIO.setTransCode(reverserec.batctrcde);
		zptnIO.setEffdate(zptnrevIO.getEffdate());
		zptnIO.setBillcd(zptnrevIO.getBillcd());
		zptnIO.setInstfrom(zptnrevIO.getInstfrom());
		zptnIO.setInstto(zptnrevIO.getInstto());
		zptnIO.setTrandate(datcon1rec.intDate);
		zptnIO.setZprflg(zptnrevIO.getZprflg());
		zptnIO.setFormat(formatsInner.zptnrec);
		zptnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zptnIO);
		if (isNE(zptnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zptnIO.getStatuz());
			syserrrec.params.set(zptnIO.getParams());
			databaseError99500();
		}
	}

protected void nextr19080()
	{
		zptnrevIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call19020);
	}

protected void processZctn20000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					proc20010();
				case call20020: 
					call20020();
					writ20030();
					nextr20080();
				case exit20090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void proc20010()
	{
		zctnrevIO.setDataArea(SPACES);
		zctnrevIO.setChdrcoy(reverserec.company);
		zctnrevIO.setChdrnum(reverserec.chdrnum);
		zctnrevIO.setTranno(reverserec.tranno);
		zctnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		zctnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		zctnrevIO.setFormat(formatsInner.zctnrevrec);
	}

protected void call20020()
	{
		SmartFileCode.execute(appVars, zctnrevIO);
		if (isNE(zctnrevIO.getStatuz(), varcom.oK)
		&& isNE(zctnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zctnrevIO.getStatuz());
			syserrrec.params.set(zctnrevIO.getParams());
			databaseError99500();
		}
		if (isEQ(zctnrevIO.getStatuz(), varcom.endp)
		|| isNE(zctnrevIO.getChdrcoy(), reverserec.company)
		|| isNE(zctnrevIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(zctnrevIO.getTranno(), reverserec.tranno)) {
			zctnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit20090);
		}
	}

protected void writ20030()
	{
		zctnIO.setDataArea(SPACES);
		zctnIO.setAgntcoy(zctnrevIO.getAgntcoy());
		zctnIO.setAgntnum(zctnrevIO.getAgntnum());
		zctnIO.setChdrcoy(zctnrevIO.getChdrcoy());
		zctnIO.setChdrnum(zctnrevIO.getChdrnum());
		zctnIO.setLife(zctnrevIO.getLife());
		zctnIO.setCoverage(zctnrevIO.getCoverage());
		zctnIO.setRider(zctnrevIO.getRider());
		zctnIO.setTranno(reverserec.newTranno);
		setPrecision(zctnIO.getCommAmt(), 2);
		zctnIO.setCommAmt(mult(zctnrevIO.getCommAmt(), -1));
		setPrecision(zctnIO.getPremium(), 2);
		zctnIO.setPremium(mult(zctnrevIO.getPremium(), -1));
		zctnIO.setSplitBcomm(zctnrevIO.getSplitBcomm());
		zctnIO.setTransCode(reverserec.batctrcde);
		zctnIO.setEffdate(zctnrevIO.getEffdate());
		zctnIO.setTrandate(datcon1rec.intDate);
		zctnIO.setZprflg(zctnrevIO.getZprflg());
		zctnIO.setFormat(formatsInner.zctnrec);
		zctnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zctnIO);
		if (isNE(zctnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zctnIO.getStatuz());
			syserrrec.params.set(zctnIO.getParams());
			databaseError99500();
		}
	}

protected void nextr20080()
	{
		zctnrevIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call20020);
	}

protected void processUndl1a000()
	{
		start1a000();
	}

	/**
	* <pre>
	* For each unique UNDL (Underwriting Life) record, keep the       
	* earliest and set it to a proposal. Delete all other records     
	* associated with the contract.                                   
	* </pre>
	*/
protected void start1a000()
	{
		wsaaLife.set(SPACES);
		wsaaJlife.set(SPACES);
		undlafiIO.setParams(SPACES);
		undlafiIO.setChdrcoy(reverserec.company);
		undlafiIO.setChdrnum(reverserec.chdrnum);
		undlafiIO.setLife(covrafiIO.getLife());
		undlafiIO.setJlife("00");
		undlafiIO.setTranno(ZERO);
		undlafiIO.setFormat(formatsInner.undlafirec);
		undlafiIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		undlafiIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		undlafiIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(undlafiIO.getStatuz(),varcom.endp))) {
			deletUndl1a100();
		}
		
	}

protected void deletUndl1a100()
	{
		start1a100();
	}

protected void start1a100()
	{
		SmartFileCode.execute(appVars, undlafiIO);
		if (isNE(undlafiIO.getStatuz(), varcom.oK)
		&& isNE(undlafiIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(undlafiIO.getParams());
			syserrrec.statuz.set(undlafiIO.getStatuz());
			systemError99000();
		}
		if (isEQ(undlafiIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* Check Life UNDL record is associated with the                   */
		/*  contract we are processing.                                    */
		if (isNE(reverserec.company, undlafiIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, undlafiIO.getChdrnum())) {
			undlafiIO.setStatuz(varcom.endp);
			return ;
		}
		/* A correct match on Company & Contract number has now been       */
		/*  found. For each unique combination of Life and Jlife           */
		/*  found, keep earliest.                                          */
		if (isNE(undlafiIO.getLife(), wsaaLife)
		|| isNE(undlafiIO.getJlife(), wsaaJlife)) {
			undlafiIO.setValidflag("3");
			undlafiIO.setTranno(reverserec.newTranno);
			undlafiIO.setCurrfrom(wsaaConStartDate);
			undlafiIO.setCurrto(varcom.vrcmMaxDate);
			undlafiIO.setFunction(varcom.writd);
			wsaaLife.set(undlafiIO.getLife());
			wsaaJlife.set(undlafiIO.getJlife());
		}
		else {
			undlafiIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, undlafiIO);
			if (isNE(undlafiIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(undlafiIO.getParams());
				syserrrec.statuz.set(undlafiIO.getStatuz());
				systemError99000();
			}
			undlafiIO.setFunction(varcom.delet);
		}
		SmartFileCode.execute(appVars, undlafiIO);
		if (isNE(undlafiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(undlafiIO.getParams());
			syserrrec.statuz.set(undlafiIO.getStatuz());
			systemError99000();
		}
		undlafiIO.setFunction(varcom.nextr);
	}

protected void processUndc1b000()
	{
		start1b000();
	}

	/**
	* <pre>
	* For each unique UNDC (U/wrting Component) record, keep the      
	* earliest and set it to a proposal. Delete all other records     
	* associated with the contract.                                   
	* </pre>
	*/
protected void start1b000()
	{
		wsaaLife.set(SPACES);
		wsaaJlife.set(SPACES);
		wsbbCoverage.set(SPACES);
		wsbbRider.set(SPACES);
		undcafiIO.setParams(SPACES);
		undcafiIO.setChdrcoy(reverserec.company);
		undcafiIO.setChdrnum(reverserec.chdrnum);
		undcafiIO.setLife(covrafiIO.getLife());
		undcafiIO.setJlife(SPACES);
		undcafiIO.setCoverage("00");
		undcafiIO.setRider("00");
		undcafiIO.setTranno(ZERO);
		undcafiIO.setFormat(formatsInner.undcafirec);
		undcafiIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		undcafiIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		undcafiIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(undcafiIO.getStatuz(),varcom.endp))) {
			deletUndc1b100();
		}
		
	}

protected void deletUndc1b100()
	{
		start1b100();
	}

protected void start1b100()
	{
		SmartFileCode.execute(appVars, undcafiIO);
		if (isNE(undcafiIO.getStatuz(), varcom.oK)
		&& isNE(undcafiIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(undcafiIO.getParams());
			syserrrec.statuz.set(undcafiIO.getStatuz());
			systemError99000();
		}
		if (isEQ(undcafiIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* Check Component UNDC record is associated with the              */
		/*  contract we are processing.                                    */
		if (isNE(reverserec.company, undcafiIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, undcafiIO.getChdrnum())) {
			undcafiIO.setStatuz(varcom.endp);
			return ;
		}
		/* A correct match on Company & Contract number has now been       */
		/*  found. For each unique combination of Life, Jlife, Coverage    */
		/*  and Rider found, keep earliest.                                */
		if (isNE(undcafiIO.getLife(), wsaaLife)
		|| isNE(undcafiIO.getJlife(), wsaaJlife)
		|| isNE(undcafiIO.getCoverage(), wsbbCoverage)
		|| isNE(undcafiIO.getRider(), wsbbRider)) {
			undcafiIO.setValidflag("3");
			undcafiIO.setTranno(reverserec.newTranno);
			undcafiIO.setCurrfrom(wsaaConStartDate);
			undcafiIO.setCurrto(varcom.vrcmMaxDate);
			undcafiIO.setFunction(varcom.writd);
			wsaaLife.set(undcafiIO.getLife());
			wsaaJlife.set(undcafiIO.getJlife());
			wsbbCoverage.set(undcafiIO.getCoverage());
			wsbbRider.set(undcafiIO.getRider());
		}
		else {
			undcafiIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, undcafiIO);
			if (isNE(undcafiIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(undcafiIO.getParams());
				syserrrec.statuz.set(undcafiIO.getStatuz());
				systemError99000();
			}
			undcafiIO.setFunction(varcom.delet);
		}
		SmartFileCode.execute(appVars, undcafiIO);
		if (isNE(undcafiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(undcafiIO.getParams());
			syserrrec.statuz.set(undcafiIO.getStatuz());
			systemError99000();
		}
		undcafiIO.setFunction(varcom.nextr);
	}

protected void processUndq1c000()
	{
		start1c000();
	}

	/**
	* <pre>
	* For each unique UNDQ (U/wrting Question) record, keep the       
	* earlist and set it to a proposal. Delete all other records      
	* associated with the contract.                                   
	* </pre>
	*/
protected void start1c000()
	{
		wsaaLife.set(SPACES);
		wsaaJlife.set(SPACES);
		undqafiIO.setParams(SPACES);
		undqafiIO.setChdrcoy(reverserec.company);
		undqafiIO.setChdrnum(reverserec.chdrnum);
		undqafiIO.setLife(covrafiIO.getLife());
		undqafiIO.setJlife("00");
		undqafiIO.setQuestidf(SPACES);
		undqafiIO.setTranno(ZERO);
		undqafiIO.setFormat(formatsInner.undqafirec);
		undqafiIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		undqafiIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		undqafiIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(undqafiIO.getStatuz(),varcom.endp))) {
			deletUndq1c100();
		}
		
	}

protected void deletUndq1c100()
	{
		start1c100();
	}

protected void start1c100()
	{
		SmartFileCode.execute(appVars, undqafiIO);
		if (isNE(undqafiIO.getStatuz(), varcom.oK)
		&& isNE(undqafiIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(undqafiIO.getParams());
			syserrrec.statuz.set(undqafiIO.getStatuz());
			systemError99000();
		}
		if (isEQ(undqafiIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* Check Question  UNDQ record is associated with the              */
		/*  contract we are processing.                                    */
		if (isNE(reverserec.company, undqafiIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, undqafiIO.getChdrnum())) {
			undqafiIO.setStatuz(varcom.endp);
			return ;
		}
		/* A correct match on Company & Contract number has now been       */
		/*  found. For each unique combination of Life and Jlife           */
		/*  found, keep earliest.                                          */
		if (isNE(undqafiIO.getLife(), wsaaLife)
		|| isNE(undqafiIO.getJlife(), wsaaJlife)
		|| isNE(undqafiIO.getQuestidf(), wsbbQuestidf)) {
			undqafiIO.setValidflag("3");
			undqafiIO.setTranno(reverserec.newTranno);
			undqafiIO.setCurrfrom(wsaaConStartDate);
			undqafiIO.setCurrto(varcom.vrcmMaxDate);
			undqafiIO.setFunction(varcom.writd);
			wsaaLife.set(undqafiIO.getLife());
			wsaaJlife.set(undqafiIO.getJlife());
			wsbbQuestidf.set(undqafiIO.getQuestidf());
		}
		else {
			undqafiIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, undqafiIO);
			if (isNE(undqafiIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(undqafiIO.getParams());
				syserrrec.statuz.set(undqafiIO.getStatuz());
				systemError99000();
			}
			undqafiIO.setFunction(varcom.delet);
		}
		SmartFileCode.execute(appVars, undqafiIO);
		if (isNE(undqafiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(undqafiIO.getParams());
			syserrrec.statuz.set(undqafiIO.getStatuz());
			systemError99000();
		}
		undqafiIO.setFunction(varcom.nextr);
	}

protected void systemError99000()
	{
		start99000();
		exit99490();
	}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError99500()
	{
		start99500();
		exit99990();
	}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99990()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void a100ReadFreqs()
	{
		/*A100-PARA*/
		/*A199-EXIT*/
	}

protected void m100UpdHpad()
	{
		m100Begin();
	}

protected void m100Begin()
	{
		hpadIO.setDataArea(SPACES);
		hpadIO.setChdrcoy(reverserec.company);
		hpadIO.setChdrnum(reverserec.chdrnum);
		hpadIO.setFormat(formatsInner.hpadrec);
		hpadIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hpadIO.getParams());
			syserrrec.statuz.set(hpadIO.getStatuz());
			databaseError99500();
		}
		hpadIO.setHuwdcdte(ZERO);
		hpadIO.setHuwdcdte(varcom.vrcmMaxDate);		
		hpadIO.setHissdte(varcom.vrcmMaxDate);		
		//hpadIO.setHoissdte(varcom.vrcmMaxDate);
		hpadIO.setIncexc(SPACES);
		hpadIO.setDespdate(varcom.vrcmMaxDate);
		hpadIO.setPackdate(varcom.vrcmMaxDate);
		hpadIO.setRemdte(varcom.vrcmMaxDate);
		hpadIO.setDeemdate(varcom.vrcmMaxDate);
		hpadIO.setNextActDate(varcom.vrcmMaxDate);
		hpadIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hpadIO.getParams());
			syserrrec.statuz.set(hpadIO.getStatuz());
			databaseError99500();
		}
	}

protected void m300UpdateAgpy()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					m300Begin();
				case m300Call: 
					m300Call();
					m300Next();
				case m300Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void m300Begin()
	{
		agpydocIO.setParams(SPACES);
		agpydocIO.setBatccoy(hpadIO.getChdrcoy());
		agpydocIO.setRdocnum(hpadIO.getChdrnum());
		agpydocIO.setTranno(0);
		agpydocIO.setJrnseq(0);
		agpydocIO.setFormat(formatsInner.agpydocrec);
		agpydocIO.setFunction(varcom.begn);
	}

protected void m300Call()
	{
		SmartFileCode.execute(appVars, agpydocIO);
		if (isNE(agpydocIO.getStatuz(), varcom.oK)
		&& isNE(agpydocIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(agpydocIO.getStatuz());
			syserrrec.params.set(agpydocIO.getParams());
			databaseError99500();
		}
		if (isEQ(agpydocIO.getStatuz(), varcom.endp)
		|| isNE(agpydocIO.getBatccoy(), hpadIO.getChdrcoy())
		|| isNE(agpydocIO.getRdocnum(), hpadIO.getChdrnum())) {
			agpydocIO.setStatuz(varcom.endp);
			goTo(GotoLabel.m300Exit);
		}
		if (isEQ(agpydocIO.getEffdate(), 0)
		|| isEQ(agpydocIO.getEffdate(), varcom.vrcmMaxDate)) {
			agpyagtIO.setParams(SPACES);
			agpyagtIO.setRrn(agpydocIO.getRrn());
			agpyagtIO.setFormat(formatsInner.agpyagtrec);
			agpyagtIO.setFunction(varcom.readd);
			SmartFileCode.execute(appVars, agpyagtIO);
			if (isNE(agpyagtIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(agpyagtIO.getParams());
				syserrrec.statuz.set(agpyagtIO.getStatuz());
				databaseError99500();
			}
			agpyagtIO.setEffdate(wsaaToday);
			agpyagtIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, agpyagtIO);
			if (isNE(agpyagtIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(agpyagtIO.getParams());
				syserrrec.statuz.set(agpyagtIO.getStatuz());
				databaseError99500();
			}
		}
	}

protected void m300Next()
	{
		agpydocIO.setFunction(varcom.nextr);
		goTo(GotoLabel.m300Call);
	}
    protected void processZdivCustSpecific()
    {
		
    }
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData bextrevrec = new FixedLengthStringData(10).init("BEXTREVREC");
	private FixedLengthStringData chdrafirec = new FixedLengthStringData(10).init("CHDRAFIREC");
	private FixedLengthStringData clrfrec = new FixedLengthStringData(10).init("CLRFREC   ");
	private FixedLengthStringData covrafirec = new FixedLengthStringData(10).init("COVRAFIREC");
	private FixedLengthStringData covtlnbrec = new FixedLengthStringData(10).init("COVTLNBREC");
	private FixedLengthStringData fluprevrec = new FixedLengthStringData(10).init("FLUPREVREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData lextafirec = new FixedLengthStringData(10).init("LEXTAFIREC");
	private FixedLengthStringData lifeafirec = new FixedLengthStringData(10).init("LIFEAFIREC");
	private FixedLengthStringData linscfirec = new FixedLengthStringData(10).init("LINSCFIREC");
	private FixedLengthStringData payrrevrec = new FixedLengthStringData(10).init("PAYRREVREC");
	private FixedLengthStringData pcddafirec = new FixedLengthStringData(10).init("PCDDAFIREC");
	private FixedLengthStringData inctrec = new FixedLengthStringData(10).init("INCTREC");
	private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
	private FixedLengthStringData hcsdrec = new FixedLengthStringData(10).init("HCSDREC");
	private FixedLengthStringData mliarec = new FixedLengthStringData(10).init("MLIAREC");
	private FixedLengthStringData mliachdrec = new FixedLengthStringData(10).init("MLIACHDREC");
	private FixedLengthStringData hpadrec = new FixedLengthStringData(10).init("HPADREC");
	private FixedLengthStringData zptnrec = new FixedLengthStringData(10).init("ZPTNREC");
	private FixedLengthStringData zctnrec = new FixedLengthStringData(10).init("ZCTNREC");
	private FixedLengthStringData zptnrevrec = new FixedLengthStringData(10).init("ZPTNREVREC");
	private FixedLengthStringData zctnrevrec = new FixedLengthStringData(10).init("ZCTNREVREC");
	private FixedLengthStringData undcafirec = new FixedLengthStringData(10).init("UNDCAFIREC");
	private FixedLengthStringData undlafirec = new FixedLengthStringData(10).init("UNDLAFIREC");
	private FixedLengthStringData undqafirec = new FixedLengthStringData(10).init("UNDQAFIREC");
	private FixedLengthStringData taxdrevrec = new FixedLengthStringData(10).init("TAXDREVREC");
	private FixedLengthStringData agpydocrec = new FixedLengthStringData(10).init("AGPYDOCREC");
	private FixedLengthStringData agpyagtrec = new FixedLengthStringData(10).init("AGPYAGTREC");
}
}