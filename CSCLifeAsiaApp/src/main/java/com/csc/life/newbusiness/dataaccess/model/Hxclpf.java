package com.csc.life.newbusiness.dataaccess.model;

import java.util.Date;

public class Hxclpf {
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private int fupno;
	private int hxclseqno;
	private String hxclnote01;
	private String hxclnote02;
	private String hxclnote03;
	private String hxclnote04;
	private String hxclnote05;
	private String hxclnote06;
	private String userProfile;
	private String jobName;
	private Date datime;
	private int user;
	private String fupcode;
	private String hxcllettyp;
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public int getFupno() {
		return fupno;
	}
	public void setFupno(int fupno) {
		this.fupno = fupno;
	}
	public int getHxclseqno() {
		return hxclseqno;
	}
	public void setHxclseqno(int hxclseqno) {
		this.hxclseqno = hxclseqno;
	}
	public String getHxclnote01() {
		return hxclnote01;
	}
	public void setHxclnote01(String hxclnote01) {
		this.hxclnote01 = hxclnote01;
	}
	public String getHxclnote02() {
		return hxclnote02;
	}
	public void setHxclnote02(String hxclnote02) {
		this.hxclnote02 = hxclnote02;
	}
	public String getHxclnote03() {
		return hxclnote03;
	}
	public void setHxclnote03(String hxclnote03) {
		this.hxclnote03 = hxclnote03;
	}
	public String getHxclnote04() {
		return hxclnote04;
	}
	public void setHxclnote04(String hxclnote04) {
		this.hxclnote04 = hxclnote04;
	}
	public String getHxclnote05() {
		return hxclnote05;
	}
	public void setHxclnote05(String hxclnote05) {
		this.hxclnote05 = hxclnote05;
	}
	public String getHxclnote06() {
		return hxclnote06;
	}
	public void setHxclnote06(String hxclnote06) {
		this.hxclnote06 = hxclnote06;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	//IJTI-461 START
	public Date getDatime() {
		if (this.datime==null){
			return null;
		} else {
			return (Date) this.datime.clone();
		}
	} //IJTI-461 END
	public void setDatime(Date datime) {
		this.datime = (Date) datime.clone(); //IJTI-460
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public String getFupcode() {
		return fupcode;
	}
	public void setFupcode(String fupcode) {
		this.fupcode = fupcode;
	}
	public String getHxcllettyp() {
		return hxcllettyp;
	}
	public void setHxcllettyp(String hxcllettyp) {
		this.hxcllettyp = hxcllettyp;
	}
	

}
