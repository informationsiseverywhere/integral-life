package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:36
 * Description:
 * Copybook name: CTRSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ctrskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ctrsFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData ctrsKey = new FixedLengthStringData(256).isAPartOf(ctrsFileKey, 0, REDEFINE);
  	public FixedLengthStringData ctrsChdrcoy = new FixedLengthStringData(1).isAPartOf(ctrsKey, 0);
  	public FixedLengthStringData ctrsChdrnum = new FixedLengthStringData(8).isAPartOf(ctrsKey, 1);
  	public PackedDecimalData ctrsSeqno = new PackedDecimalData(2, 0).isAPartOf(ctrsKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(245).isAPartOf(ctrsKey, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ctrsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ctrsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}