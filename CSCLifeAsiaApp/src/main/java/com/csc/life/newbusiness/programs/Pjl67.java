package com.csc.life.newbusiness.programs;

import com.csc.smart.dataaccess.dao.DescpfDAO;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.util.SmartTableDataFactory;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import com.csc.life.newbusiness.screens.Sjl67ScreenVars;
import com.csc.life.newbusiness.tablestructures.Tjl67rec;
import com.csc.smart.recordstructures.Varcom;

public class Pjl67 extends ScreenProgCS{

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL67");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sjl67ScreenVars sv = ScreenProgram.getScreenVars(Sjl67ScreenVars.class);
	private String wsaaItempfx;
	private String wsaaItemcoy;
	private String wsaaItemitem;
	private String wsaaItemtabl;
	private String wsaaItemseq;
	private String wsaaItemlang;
	private Itemkey wsaaItemkey = new Itemkey();
	private Tjl67rec tjl67rec = new Tjl67rec();
	private DescpfDAO iafDescDAO = getApplicationContext().getBean("iafDescDAO", DescpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao",ItemDAO.class);
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	
	public Pjl67() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl67", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			throw e;
		}
	}

	@Override
	protected void initialise1000() {		
		initialise1010();
	}
	
	protected void initialise1010() {
		sv.dataArea.set(SPACES);
		wsaaItemkey.set(wsspsmart.itemkey);
		wsaaItempfx = wsaaItemkey.itemItempfx.toString();
		wsaaItemcoy = wsaaItemkey.itemItemcoy.toString();
		wsaaItemitem = wsaaItemkey.itemItemitem.toString();
		wsaaItemtabl = wsaaItemkey.itemItemtabl.toString();
		wsaaItemseq = wsaaItemkey.itemItemseq.toString();
		wsaaItemlang = wsspcomn.language.toString();
		sv.company.set(wsaaItemkey.itemItemcoy);
		sv.tabl.set(wsaaItemkey.itemItemtabl);
		sv.item.set(wsaaItemkey.itemItemitem);
		String longdesc = iafDescDAO.getItemLongDesc(wsaaItempfx, wsaaItemcoy, wsaaItemtabl, wsaaItemitem,
				wsaaItemlang);
		sv.longdesc.set(longdesc);
		Itempf itempf = itemDAO.findItemBySeq(wsaaItempfx, wsaaItemcoy, wsaaItemtabl, wsaaItemitem, "1", wsaaItemseq);
		if (itempf == null) {
			return;
		} else {
			tjl67rec.tjl67Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		if (isNE(tjl67rec.tjl67Rec, SPACES)) {
			sv.resultcds.set(tjl67rec.resultcds);
			sv.rcverors.set(tjl67rec.rcverors);
		}
	}
	
	@Override
	protected void preScreenEdit() {
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(Varcom.prot);
		}
		return;
	}

	@Override
	protected void screenEdit2000() {
		screenIo2010();
		exit2090();
	}
	
	protected void screenIo2010() {
		wsspcomn.edterror.set(Varcom.oK);
		if (isEQ(wsspcomn.flag,"I")) {
			exit2090();
		}
	}

	protected void exit2090() {
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}
	
	@Override
	protected void update3000()	{
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		if (isNE(sv.resultcds,tjl67rec.resultcds) || isNE(sv.rcverors,tjl67rec.rcverors)) {
			tjl67rec.resultcds.set(sv.resultcds);
			tjl67rec.rcverors.set(sv.rcverors);
		}
		com.csc.smart400framework.dataaccess.model.Itempf item = new com.csc.smart400framework.dataaccess.model.Itempf();
		item.setGenarea(tjl67rec.tjl67Rec.toString().getBytes());
		item.setItempfx(smtpfxcpy.item.toString());
		item.setItemtabl(wsaaItemkey.itemItemtabl.toString());
		item.setItemcoy(wsspcomn.company.toString());
		item.setItemitem(wsaaItemkey.itemItemitem.toString());
		item.setItemseq(wsaaItemseq);
		item.setGenareaj(SmartTableDataFactory.getInstance(appVars.getAppConfig().getSmartTableDataFormat())
				.getGENAREAJString(tjl67rec.tjl67Rec.toString().getBytes(), tjl67rec));
		itemDAO.updateSmartTableItem(item);
	}
	
	@Override
	protected void whereNext4000() {
		/*NEXT-PROGRAM*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
