package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:27
 * Description:
 * Copybook name: COVRTRBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrtrbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrtrbFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrtrbKey = new FixedLengthStringData(64).isAPartOf(covrtrbFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrtrbChdrcoy = new FixedLengthStringData(1).isAPartOf(covrtrbKey, 0);
  	public FixedLengthStringData covrtrbChdrnum = new FixedLengthStringData(8).isAPartOf(covrtrbKey, 1);
  	public FixedLengthStringData covrtrbLife = new FixedLengthStringData(2).isAPartOf(covrtrbKey, 9);
  	public FixedLengthStringData covrtrbCoverage = new FixedLengthStringData(2).isAPartOf(covrtrbKey, 11);
  	public FixedLengthStringData covrtrbRider = new FixedLengthStringData(2).isAPartOf(covrtrbKey, 13);
  	public PackedDecimalData covrtrbPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrtrbKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covrtrbKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrtrbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrtrbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}